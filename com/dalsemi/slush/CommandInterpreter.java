/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: CommandInterpreter.java
*/
package com.dalsemi.slush;

import com.dalsemi.slush.command.*;
import java.io.*;
import java.util.*;
import com.dalsemi.comm.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.shell.server.serial.*;
import com.dalsemi.system.*;

/**
 * The CommandInterpreter class is responsible for dispatching any commands received
 * by the slush shell to their appropriate handlers.  
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class CommandInterpreter
{
    private static Hashtable    slushCommands = new Hashtable(60, 5);
    private static Hashtable    commandUsers  = new Hashtable(60, 5);
    
    public static String FILE_IN_REDIRECTED    = "inputRedirected";
    public static String FILE_OUT_REDIRECTED   = "outputRedirected";
    public static String RUNNING_IN_BACKGROUND = "background";

    /**
     * Adds a command to the list of those recognized by the slush shell.
     * After a command has been added, it can be executed from any instance
     * of the shell including those that were started before the command was
     * added.  If an attempt is made to add a command that already exists, the
     * operation will fail unless the current user is an administrator or that
     * user is the one who added the command.
     *
     * @param name  The string that will be typed from the shell command line to execute the command.
     * @param executer  An instance of the command to be added.
     *
     * @return  The success of the add operation.
     */
    public static final boolean addCommand(String name, SlushCommand executer)
    {
        if (canChangeCommand(name))
        {
            commandUsers.put(name, new Byte(TINIOS.getCurrentUID()));
            slushCommands.put(name, executer);
            
            HelpCommand.clearCommandListCache();

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Removes the named command from the list of those recognized by the
     * slush shell.  Once a command has been removed, it can no longer be
     * executed from any instance of the shell including those that were
     * started before the command was removed.  Only administrators and the
     * user that added the command will be allowed to remove it.  NOTE:
     * Currently all users have administrative rights.
     *
     * @param name  The name of the command to remove.
     *
     * @return  The success of the operation.
     */
    public static final boolean removeCommand(String name)
    {
        if(canChangeCommand(name))
        {
            commandUsers.remove(name);
            slushCommands.remove(name);

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Executes a slush command.
     *
     * @param commandLine  An Object array containing the command in the first element,
     * followed by any parameters need for
     * that command in a String[] in the second element.
     * @param in   The stream the command will use to get input.
     * @param out  The stream used to report non-critical messages.
     * @param err  The stream used to report critical messages.
     * @param env  A table of environment variables.
     *
     * @throws Exception Any exception raised by the command.  The shell will report the
     * result of Exception.getMessage() to the user.
     */
    public static void execute(Object[] commandLine, SystemInputStream in, 
                               SystemPrintStream out, SystemPrintStream err, 
                               Hashtable env) throws Exception
    {
        SlushCommand executer = (SlushCommand)slushCommands.get(commandLine[0]);
        
        if (executer != null)
        {
            String[] args = (String[])commandLine[1];
            
            //Now, replace any $ENV_VARS in the arg list...
            int pos    = 0;
            int endPos = -1;
            for (int i=0; i<args.length; i++)
            {
                while (pos < args[i].length())
                {
                    endPos = -1;
                    if ((pos = args[i].indexOf('$', pos)) != -1)
                    {
                        for (int j=pos+1; j<args[i].length(); j++)
                        {
                            char c = args[i].charAt(j);
                            if ((c == '$') || ( c == ' '))
                            {
                                endPos = j;
                                break;
                            }
                        }
                        if (endPos == -1)
                        {
                            endPos = args[i].length();
                        }
                        String envVar = args[i].substring(pos+1, endPos);
                        String value;
                        if ((value = (String)env.get(envVar)) != null)
                        {
                            args[i] = args[i].substring(0,pos) + value + args[i].substring(endPos);
                            pos += value.length();
                        }
                        else
                            pos = endPos;
                    }
                    else
                        pos = args[i].length();
                }
            }
            
            int         firstSpecial      = -1;
            boolean     fileInRedirect    = false;
            boolean     fileOutRedirect   = false;
            boolean     serialOutRedirect = false;
            boolean     serialInRedirect  = false;
            boolean     inBG              = false;

            SystemInputStream psIn  = in;
            SystemPrintStream psOut = out;
            SystemPrintStream psErr = err;
            
            //Parse out the commandline options.  ('>', '>>', '>&', '>>&', '<', etc.)
            boolean append = false;
            boolean errRedirect = false;
            String fileOutName = null;
            String fileInName = null;
            int len = args.length;
            for (int i = 0; i < len; i++)
            {
                try
                {
                    if (args[i].equals("&"))    //Check for background
                    {
                        if (firstSpecial == -1)
                            firstSpecial = i;

                        inBG = true;
                        
                        if (!fileOutRedirect)
                        {
                            if (Thread.currentThread() instanceof SerialSession)
                            {
                                //DEBUG ==> Beta 2.1 Hack.  We need to send null to the new process
                                //so that the stream won't be collected.
/*                                if (commandLine[0].equals("java"))
                                {
                                    psOut = null;
                                    psErr = null;
                                }
                                else*/
                                {
                                    psOut = new SystemPrintStream(new NullOutputStream());
                                    psErr = psOut;
                                }
                            }
                        }
                    }
                    else if (args[i].startsWith(">"))
                    {
                        if (firstSpecial == -1)
                            firstSpecial = i;
                        
                        if (args[i].length() > 1)
                        {
                            errRedirect = (args[i].indexOf('&', 1) != -1);
                            append = (args[i].indexOf('>', 1) != -1);
                        }
                        
                        if (args[++i].equals("S0"))
                        {
                            serialOutRedirect = true;
                        }
                        else if (args[i].equals("null"))
                        {
                            psOut = new SystemPrintStream(new NullOutputStream());
                        }
                        else
                        {
                            fileOutRedirect = true;
                            fileOutName = args[i];
                        }
                        
                        if (errRedirect)
                            psErr = psOut;
                    }
                    else if (args[i].equals("<"))
                    {
                        if (firstSpecial == -1)
                            firstSpecial = i;

                        fileInRedirect = true;

                        if (args[++i].equals("S0"))
                        {
                            serialInRedirect = true;
                        }
                        else
                        {
                            fileInName = args[i];
                        }
                    }
                }
                catch (ArrayIndexOutOfBoundsException ae)
                {
                    throw new ArrayIndexOutOfBoundsException("No filename for redirect.");
                }
            }
            
            if (fileOutRedirect)
            {
                if (inBG)
                {
                    //Add a check for writable here?
                    psOut = new SystemPrintStream(new NullOutputStream(), fileOutName, append);
                    if (!errRedirect)
                        psErr = new SystemPrintStream(new NullOutputStream());
                }
                else
                {
                    try
                    {
                        psOut = new SystemPrintStream(new FileOutputStream(fileOutName, append));
                    }
                    catch(IOException ioe)
                    {
                        throw new IOException(fileOutName + " could not be created.");
                    }
                }
                if (errRedirect)
                    psErr = psOut;
            }
            
            if (fileInRedirect)
            {
                if (inBG)
                {
                    //Add a check for file existance here?
                    psIn = new SystemInputStream(new NullInputStream(), null, fileInName);
                }
                else
                {
                    try
                    {
                        psIn = new SystemInputStream(new FileInputStream(fileInName), null);
                    }
                    catch(IOException ioe)
                    {
                        throw new IOException(fileInName + " does not exist.");
                    }
                }
            }
            
            if (inBG && !fileOutRedirect)
            {
                if(psOut != null)
                    psOut.shieldsUp = true;
                if(psErr != null)
                    psErr.shieldsUp = true;
            }

            if (firstSpecial != -1)
            {
                //DEBUG ==> Wow, this is really inefficient.
                String[]    temp = new String[firstSpecial];

                System.arraycopy(args, 0, temp, 0, firstSpecial);

                args = new String[firstSpecial];
                System.arraycopy(temp, 0, args, 0, firstSpecial);
            }

            //Setup the environment to reflect the commandline options
            env.put(FILE_IN_REDIRECTED, (fileInRedirect ? "true" : "false"));
            env.put(FILE_OUT_REDIRECTED, (fileOutRedirect ? "true" : "false"));
            env.put(RUNNING_IN_BACKGROUND, (inBG ? "true" : "false"));
            
            TINISerialPort serialPort = null;
            
            boolean needToRestart = false;
            if (serialInRedirect || serialOutRedirect)
            {
                if  (Thread.currentThread() instanceof SerialSession)
                {
                    //If you are at a serial prompt, the user shouldn't redirect
                    //to S0.  This will kick the user off of his current session.
                    serialInRedirect = false;
                    serialOutRedirect = false;
                }
                else
                {
                    if(!TINIOS.isCurrentUserAdmin())
                    {
                        err.println("You must have admin privileges to access the Serial port.");
                        return;
                    }

                    needToRestart = ((Slush)TINIOS.getShell()).shutDownSerialServer();

                    serialPort = new TINISerialPort("serial0");
                    if (serialInRedirect)
                    {
                        psIn = new SystemInputStream(serialPort.getInputStream(), null); //DEBUG
                    }
                    if (serialOutRedirect)
                    {
                        psOut = new SystemPrintStream(serialPort.getOutputStream());
                    }
                }
            }
            
            //Now, execute the command.
            executer.execute(psIn, psOut, psErr, args, env);

            //Clean up
            if ((serialInRedirect || serialOutRedirect) &&!inBG)
            {
                if (serialInRedirect)
                {
                    psIn.close();
                }

                if (serialOutRedirect)
                {
                    psOut.close();
                }
                
                if(serialPort != null)
                {
                    serialPort.close();
                }

                if (needToRestart)
                {
                    ((Slush)TINIOS.getShell()).startupSerialServer();
                }
            }
            if ((fileInRedirect || fileOutRedirect) &&!inBG)
            {
                if (fileInRedirect)
                {
                    psIn.close();
                }

                if (fileOutRedirect)
                {
                    psOut.close();
                }
            }
        }
        else
        {
            err.println("ERROR: Unknown Command: " + commandLine[0]);
        }
    }

    /**
     * Returns an instance of the specified command.  Note:  This will not
     * create a new instance of the command--it simply searches the list of
     * current command.
     *
     * @param str  The name of the command to find.
     *
     * @return  an instance of the command or null if the command has not been added to the list of available commands.
     *
     * @see CommandInterpreter#addCommand(String, SlushCommand)
     */
    public static SlushCommand getSlushCommand(String str)
    {
        return (SlushCommand)slushCommands.get(str);
    }

    /**
     * Enumerates all of the commands that have been added to the list.
     *
     * @return An enumeration of Strings representing the command names.
     *
     * @see CommandInterpreter#addCommand(String, SlushCommand)
     */
    public static Enumeration getAvailableCommands()
    {
        return slushCommands.keys();
    }

    /**
     * Gets the number of active commands.
     *
     * @return the number of register system commands.
     */
    public static int getNumberOfCommands()
    {
        return slushCommands.size();
    }

    /**
     * Returns whether or not the current user can remove or
     * "copy over" a set system command.
     */
    private static boolean canChangeCommand(String commandName)
    {
        if (commandUsers.containsKey(commandName))
        {
            if (!TINIOS.isCurrentUserAdmin())
            {
                if (TINIOS.getCurrentUID() != ((Byte) commandUsers.get(commandName)).byteValue())
                {
                    return false;
                }
            }
        }

        return true;
    }
}