/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation.
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: RebootCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;

/**
 * Cleanly reboots the system.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class RebootCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "reboot [option]\r\n\r\nShuts down all servers and cleanly reboots the system." +
               "\r\n [-f] Don't prompt for confirmation" +
               "\r\n [-h] Clear heap on reboot." +
               "\r\n [-a] Clear heap and system memory on reboot.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err,
                        String[] args, Hashtable env) throws Exception
    {
        boolean force = false;
        boolean all   = false;
        boolean heap  = false;

        if(!TINIOS.isCurrentUserAdmin())
        {
            err.println(Slush.PERM_DENIED);
            return;
        }

        int opt;
        GetOpt myopts = new GetOpt(args,"!fha");
        while ((opt = myopts.getopt()) != myopts.optEOF)
        {
            switch (opt)
            {
                case 'f':
                    force = true;
                break;

                case 'h':
                    heap = true;
                break;

                case 'a':
                    all = true;
                break;
            }
        }

        if (!force)
        {
            out.println("Warning:  About to reboot the system.");
            out.print("\r\nOK to proceed? (Y/N): ");

            char    response;

            in.setRawMode(true);
            response = (char) (in.read() & 0x0FF);
            in.setRawMode(false);

            if ((response != 'Y') && (response != 'y'))
            {
                out.println("Reboot Aborted.");

                return;
            }
        }

        if (all)
        {
            out.println("Blasting all system memory on reboot.");
            TINIOS.blastHeapOnReboot(1);
        }
        else if (heap)
        {
            out.println("Blasting heap on reboot.");
            TINIOS.blastHeapOnReboot(0);
        }

        out.print("\r\nKilling running processes...");
        int[] taskTable = TINIOS.getTaskTableIDs();
        for (int i=0; i<taskTable.length; i++)
        {
            if (taskTable[i] > 2)
            {
                killProcess(taskTable[i]);
            }
        }

        out.print(" Done.\r\nBringing down servers and rebooting...");

        Slush slush = (Slush)TINIOS.getShell();
        slush.shutDownFTPServer();
        out.print('.');
        slush.shutDownTelnetServer();
        out.print('.');
        try
        {
            //Give Servers some time to stop
            Thread.sleep(1000);
        }
        catch(InterruptedException ie)
        {
            //DRAIN
        }
        out.print('.');

        slush.stopDHCP();
        out.print('.');

        try
        {
            //Give DHCP some time to stop
            Thread.sleep(2000);
        }
        catch(InterruptedException ie)
        {
            //DRAIN
        }
        out.println('.');

        out.println("System coming down now!");
        slush.shutDownSerialServer();

        //Give the network connections time to close out...
        try
        {
            Thread.sleep(2000);
        }
        catch(InterruptedException ie)
        {
            //DRAIN
        }

        // Causes an orderly shutdown of all processes, and a reboot.
        TINIOS.reboot();
    }

    public void killProcess(int thisProcess)
    {
        TINIOS.killTask(thisProcess);

        while (TINIOS.isTaskRunning(thisProcess))
        {
            try
            {
                Thread.sleep(200);
            }
            catch(InterruptedException ie)
            {
                //DRAIN
            }
        }
    }
}