/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: JavaCommand.java
*/
package com.dalsemi.slush.command;

import java.util.*;
import java.io.*;
import com.dalsemi.fs.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.slush.*;
import com.dalsemi.system.*;

/**
 * Executes the given Java class.
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class JavaCommand implements SlushCommand
{
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "java FILE [&]\r\n\r\nExecutes the given Java class.\r\n'&' indicates a background process.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length > 0)
        {
            boolean  bg = ((String)env.get(CommandInterpreter.RUNNING_IN_BACKGROUND)).equals("true");
            boolean  inRedirect = ((String)env.get(CommandInterpreter.FILE_IN_REDIRECTED)).equals("true");

            //Now, look for -Dkey=val commandline options and put those in
            //a separate String[] for execute.  Copy the other command line
            //args into an array (without the first String which is the .tini
            //filename.)  This is a slow way to do this, but less runtime
            //memory intensive.
            String[] cmdLineEnv = null;
            String[] newargs    = null;
            
            int dOpts = 0;
            for (int i = 0; i < args.length; i++)
            {
                if (args[i].startsWith("-D"))
                    dOpts++;
                else
                    break;  //Kick out as soon as we find a normal arg.
            }
            
            int filePos = -1;
            if (dOpts == 0)
            {
                newargs = new String[args.length - 1];

                for (int i=0; i<newargs.length; i++)
                {
                    newargs[i] = args[i + 1];
                }
                filePos = 0;
            }
            else
            {
                cmdLineEnv  = new String[dOpts];
                newargs     = new String[args.length-1-dOpts];
                int pos     = 0;
                int pos2    = 0;
                for (int i=0; i<args.length; i++)
                {
                    if (filePos != -1)
                        newargs[pos2++] = args[i];
                    else
                    {
                        if (args[i].startsWith("-D"))
                            cmdLineEnv[pos++] = SetenvCommand.parseEscapeCodes(args[i].substring(2));
                        else
                        {
                            filePos = i;
                        }
                    }
                }
            }
            
            if (filePos == -1)
            {
                throw new FileNotFoundException(args[0] + " does not exist.");
            }
            
            DSFile  file = new DSFile(args[filePos]);
            
            if (!file.exists() || file.isDirectory())
            {
                throw new FileNotFoundException(args[filePos] + " does not exist.");
            }
            else if (!file.canExec())
            {
                throw new FileNotFoundException(Slush.PERM_DENIED);
            }
            else
            {
                file.executeFile(((bg && !inRedirect) ? null : in), out, err, newargs,
                                 cmdLineEnv, !bg, args[filePos]);
            }
        }
        else
        {
            throw new IllegalArgumentException(getUsageString());
        }
    }
}