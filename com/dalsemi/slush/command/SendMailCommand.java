/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: SendMailCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.slush.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.shell.server.telnet.*;
import java.io.*;
import java.util.*;
import com.dalsemi.system.*;
import com.dalsemi.tininet.*;
import com.dalsemi.protocol.mailto.*;
import java.net.*;

/**
 * Send an email to designated recipients.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class SendMailCommand implements SlushCommand
{
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "sendmail [-f fromAddr] [recipient(s)] [cc's]\r\n\r\nSend an email to designated recipients\r\n" +
               " separated by commas." + "\r\nBegin typing your email, then place a '.' on the last line by itself" +
               "\r\n and hit <Enter>.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        String line              = null;
        String recipientList     = "";
        String fromAddr          = null;
        int    count             = 0;
        
        if (TININet.getMailhost().length() == 0)
        {
            err.println("You must use 'ipconfig -h' to set a mailhost.");
            return;
        }
        
        for (int i=0; i<args.length; i++)
        {
            if (args[i].equals("-f"))
            {
                if (i < (args.length - 1))
                {
                    fromAddr = args[++i];
                    out.println("Using From address: " + fromAddr);
                }
            }
            else
            {
                count++;
                recipientList += args[i];
            }
        }

        //Only prompt if this is not a FileInputStream...  Don't forget that
        //SocketInputStream extends FileInputStream, so we test for the env variable
        //that will indicate file in redirection.
        if (((String)env.get(CommandInterpreter.FILE_IN_REDIRECTED)).equals("true"))
        {
            if (args.length == 0)
            {
                err.println("You must specify a recipient.");
                return;
            }
        }
        else
        {
            if (count < 2)
            {
                if(count == 0)
                {
                    out.print("To: ");
                    recipientList = in.readLine();
                    if (recipientList.length() == 0)
                        return;
                }
                            
                out.print("CC: ");
                recipientList += "cc:" + in.readLine();
            }
            
            out.print("Subject: ");
            line = in.readLine();
        }
        
        URL mailCon = new URL("mailto:" + recipientList);
        Connection conn = (Connection)mailCon.openConnection();
        if (fromAddr != null)
            conn.setFromAddr(fromAddr);
        PrintStream mailOut = new PrintStream(conn.getOutputStream());
         
        if ((line != null) && (line.length() != 0))
        {
            mailOut.println("Subject: " + line);
        }
            
        out.println("Message ('.' to end): ");
            
        while(!(line = in.readLine()).equals("."))
        {
            mailOut.println(line);
        }
            
        mailOut.close();

        out.println("Message Sent.");
    }
}