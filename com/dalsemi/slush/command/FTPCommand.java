package com.dalsemi.slush.command;

import java.util.*;
import java.io.*;
import java.net.*;
import com.dalsemi.shell.server.*;

public class FTPCommand implements SlushCommand
{
    static final byte[] PROMPT = "ftp> ".getBytes();
    static final byte[] CONNECT_FIRST = "Error: Open connection first\r\n".getBytes();
    static final byte[] HELP_TEXT = 
        ("open ARG   : Opens the FTp server at address ARG\r\n"+
        "user [ARG] : Logs in as user ARG\r\n"+
        "bin        : Changes to binary transfer mode\r\n"+
        "ascii      : Changes to ASCII transfer mode\r\n"+
        "list, dir  : Lists the files in the current directory\r\n"+
        "pwd        : Lists the full path of the current directory\r\n"+
        "cd ARG     : Changes the current directory to ARG\r\n"+
        "get ARG    : Gets the file ARG from the server\r\n"+
        "put ARG    : Puts the file ARG on the server\r\n"+
        "\r\nexit, bye, quit : Quit\r\n").getBytes();

    boolean debug = false;
    int result = 0;
    com.dalsemi.protocol.ftp.FTPClient ftp = null;
    int BUF_SIZE = 2048;
    byte[] buf = new byte[BUF_SIZE];
    
    public String getUsageString()
    {
        return "ftp [option] [SERVER]"+
               "\r\nWhere options include:"+
               "\r\n [-d]      Enable debug output"+
               "\r\n [-s FILE] Use FTP script file FILE"+
               "\r\nSERVER should be the full name of the ftp server to connect to";
    }
    
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err,
                        String[] args, Hashtable env) throws Exception
    {
        DataInputStream cmdIn   = null;
        OutputStream    cmdOut  = null;

        boolean done    = false;
        Socket  sock    = null;
        String  remote  = null;
        String  command = "";
        String  token   = null;
        String currentdir = "~";
        String scriptFile = null;

        if ((args != null) & (args.length != 0))
        {
            for (int i=0;i<args.length;i++)
            {
                if (args[i].equals("-d"))
                    debug = true;
                else if (args[i].equals("-s"))
                {
                    i++;
                    if (i==args.length)
                    {
                        out.println(getUsageString());
                        return;
                    }
                    scriptFile = args[i];
                }
                else
                {
                    command = "open " + args[i];
                    i = args.length;
                }
            }
        }
            
        BufferedReader sin = null;
        if (scriptFile != null)
        {
            out.println("Creating buffered reader");
            sin = new BufferedReader(new InputStreamReader(new FileInputStream(scriptFile)));
        }
                    
        try
        {
            while (!done)
            {
                if ((command!=null) && (command.length()>0))
                {
                    String com = command;
                    if (command.indexOf(" ")!=-1)
                        com = command.substring(0,command.indexOf(" "));
                    
                    if (com.equals("exit") ||
                        com.equals("bye") ||
                        com.equals("quit"))
                    {
                        if (ftp!=null)
                        {
                            out.println("Quit: "+ftp.quit());
                        }
                        done = true;
                    }
                    else if (com.equals("open"))
                    {
                        if (command.length() < 6)
                        {
                            out.println("ERROR: Must specify a server.");
                        }
                        else
                        {
                            String server = command.substring(5);
                            ftp = new com.dalsemi.protocol.ftp.FTPClient(server);
                            out.println(ftp.getResponseString());
                            com = "user";
                            command = "user";
                        }
                    }
                    //break the if/then/else here so we can get the username
                    if (com.equals("user"))
                    {
                        if (!ftpcheck(out)) break;
                        String username = null;
                        if (command.length() < 6)
                        {
                            //get the user name
                            if (sin==null)
                            {
                                out.print("User: ");
                                username = in.readLine();
                            }
                            else 
                                username = sin.readLine();
                                
                        }
                        else
                        {
                            username = command.substring(5);
                        }
                        result = ftp.userName(username);
                        doOutput(out);
                        
                        if (result < 300) //then its OK, no password needed
                        {
                            out.println("Login OK");
                            command = null;
                            continue;
                        }
                        else if (result > 399)
                        {
                            out.println("Login failed.");
                            command = null;
                            continue;
                        }
                        
                        String password = null;
                        if (sin==null)
                        {
                            out.print("Password: ");
                            in.setEcho(false);
                            try
                            {
                                password = in.readLine();
                                in.setEcho(true);
                            }
                            catch(Exception e)
                            {
                                in.setEcho(true);
                                throw e;
                            }
                        }
                        else
                            password = sin.readLine();
                        result = ftp.password(password);
                        doOutput(out);
                    }
                    else if (com.equals("bin"))
                    {
                        if (!ftpcheck(out)) break;
                        result = ftp.binary();
                        doOutput(out);
                    }
                    else if (com.equals("ascii"))
                    {
                        if (!ftpcheck(out)) break;
                        result = ftp.ascii();
                        doOutput(out);
                    }
                    else if (com.equals("list") || com.equals("dir"))
                    {
                        String dir;
                        int space = command.indexOf(' ');

                        if (space != -1)
                            dir = command.substring(space+1);
                        else
                            dir = null;

                        if (!ftpcheck(out)) break;
                        
                        result = ftp.port();
                        doOutput(out);
                        if ((result < 200) || (result > 299))
                        {
                            out.println("PORT command failed! Cannot perform file \"GET\"");
                            command = null;
                            continue;
                        }
                        
                        result = ftp.list(dir);
                        doOutput(out);
                        if (result > 399)
                        {
                            out.println("LIST command failed! Cannot get a directory listing.");
                            command = null;
                            continue;
                        }
                        
                        InputStream fin = ftp.getDataStream();
                        int count = fin.read(buf,0,buf.length);
                        while (count != -1)
                        {
                            out.write(buf,0,count);
                            count = fin.read(buf,0,buf.length);
                        }
                        fin.close();
                        result = ftp.issueCommand(null);
                        if (result > 399)
                        {
                                out.println("Final response failure! Operation may not have completed.");
                                command = null;
                                continue;
                        }
                    }
                    else if (com.equals("pwd"))
                    {
                        if (!ftpcheck(out)) break;
                        result = ftp.issueCommand("XPWD\r\n");
                        doOutput(out);
                    }
                    else if (com.equals("cd"))
                    {
                        if (!ftpcheck(out)) break;
                        String newdir = ".";
                        if (command.length()>3)
                            newdir = command.substring(3);
                        result = ftp.issueCommand("CWD "+newdir+"\r\n");
                        doOutput(out);
                    }
                    else if (com.equals("get"))
                    {
                        if (!ftpcheck(out)) break;
                        if (command.length()>4)
                        {
                            String file = command.substring(4);
                            
                            result = ftp.port();
                            doOutput(out);
                            if ((result < 200) || (result > 299))
                            {
                                out.println("PORT command failed! Cannot perform file \"GET\"");
                                command = null;
                                continue;
                            }
                            result = ftp.retr(file);
                            doOutput(out);
                            if (result > 399)
                            {
                                out.println("RETR command failed! Cannot perform file \"GET\"");
                                command = null;
                                continue;
                            }
                            InputStream fin = ftp.getDataStream();
                            try
                            {
                                FileOutputStream fout = new FileOutputStream(file);
                                long timestamp1 = System.currentTimeMillis();
                                int total = 0;
                                int len;
                                int count = fin.read(buf,0,buf.length);
                                while (count != -1)
                                {
                                    fout.write(buf,0,count);
                                    total += count;
                                    count = fin.read(buf,0,buf.length);
                                }
                                long timestamp2 = System.currentTimeMillis();
                                out.println(total+" bytes in "+(timestamp2 - timestamp1)+" milliseconds.");
                                fout.close();
                                fin.close();
                                result = ftp.issueCommand(null);
                                if (result > 399)
                                {
                                        out.println("Final response failure! Operation may not have completed.");
                                        command = null;
                                        continue;
                                }
                            }
                            catch(Exception e)
                            {
                                out.println("Client failure: Could not store the file!");
                                out.println(e.toString());
                            }
                        }
                        else
                            out.println("You must specify a file.");
                    }
                    else if (com.equals("put"))
                    {
                        if (!ftpcheck(out)) break;
                        if (command.length()>4)
                        {
                            String file = command.substring(4);
                            File f = new File(file);
                            if (f.exists())
                            {
                                result = ftp.port();
                                doOutput(out);
                                if ((result < 200) || (result > 299))
                                {
                                    out.println("PORT command failed! Cannot perform file \"GET\"");
                                    command = null;
                                    continue;
                                }
                                result = ftp.issueCommand("STOR "+file+"\r\n");
                                doOutput(out);
                                if (result > 399)
                                {
                                    out.println("STOR command failed! Cannot perform file \"PUT\"");
                                    command = null;
                                    continue;
                                }
                                OutputStream fout = ftp.getOutputStream();
                                FileInputStream fin = null;
                                try
                                {
                                    fin = new FileInputStream(f);
                                    while (fin.available()==0) out.print("*");
                                    byte[] buf = new byte[BUF_SIZE];
                                    long timestamp1 = System.currentTimeMillis();
                                    int total = 0;
                                    
                                    int available = fin.available();
                                    while (available>0)
                                    {
                                        int len = available < BUF_SIZE ? available : BUF_SIZE;
                                        
                                        total += fin.read(buf,0,len);
                                        fout.write(buf,0,len);
                                        available = fin.available();
                                    }
                                    long timestamp2 = System.currentTimeMillis();
                                    out.println(total+" bytes in "+(timestamp2 - timestamp1)+" milliseconds.");
                                    fout.close();
                                    fin.close();
                                    result = ftp.issueCommand(null);
                                    if (result > 399)
                                    {
                                        out.println("Final response failure! Operation may not have completed.");
                                        command = null;
                                        continue;
                                    }
                                }
                                catch(Exception e)
                                {
                                    out.println("Client failure: Could not send the file!");
                                    out.println(e.toString());
                                    if (fin!=null) fin.close();
                                    fout.close();
                                }
                                
                            }
                            else
                                out.println("That file does not exist.");
                        }
                        else
                            out.println("You must specify a file.");
                    }
                    else if (com.equals("help"))
                    {
                        out.write(HELP_TEXT);
                    }
                    else if (!done && (!com.equals("open"))) //unrecognized
                    {
                        out.println("Unrecognized command: "+com);
                        out.println("Type 'help' for a list of commands.");
                    }
                }                
                if (!done)
                {
                    try
                    {
                        if (sin!=null)
                        {
                            command = sin.readLine();
                            if (command==null) 
                                sin = null;
                        }
                        //if we've run out of file before the last command let's go back to the command line
                        if (sin==null)
                        {
                            out.write(PROMPT);
                            command = in.readLine();
                        }
                    }
                    catch(Exception e)
                    {
                        out.println("happened in readLine: "+e.toString());
                    }
                    //out.println("["+command+"]");
                }
                
            }
        }
        catch(Exception e)
        {
            out.println("FTP: Exception caught: "+e.toString());
        }
        if (ftp!=null) ftp.close();

    }
    
    
    
    
    boolean ftpcheck(PrintStream out)
        throws IOException
    {
        if (ftp==null)
        {
            out.write(CONNECT_FIRST);
            return false;
        }
        return true;
    }
    
    void doOutput(PrintStream out)
    {
        if (ftp!=null)
        {
            if (debug)
            {
                String last = ftp.getLastCommand();
                if (last!=null)
                    out.println("--> " + last);
            }
            out.println(result + " " + ftp.getResponseString());
        }
    }
}

