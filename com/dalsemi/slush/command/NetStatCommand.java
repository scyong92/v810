/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation.
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: NetStatCommand.java
*
*   Eventually I need to write a real netstat. This aint it. This is purely
* for my debugging purposes.
* (DWL)
*/
package com.dalsemi.slush.command;

import com.dalsemi.shell.server.*;
import com.dalsemi.slush.*;
import java.io.*;
import java.util.*;
import java.net.InetAddress;
import com.dalsemi.tininet.icmp.*;

/**
 * Sends ICMP ECHO_REQUEST packets to network hosts
 *
 * @version 1.0
 * @author  DWL
 */
public class NetStatCommand implements SlushCommand
{
    // Some useful TCB offsets
    static final int TCB_STATE_OFFSET       = 1;
    static final int TCB_REMOTE_PORT_OFFSET = 4;
    static final int TCB_REMOTE_IP_OFFSET   = 6;
    static final int TCB_LOCAL_PORT_OFFSET  = 10;

    static byte[] tcpStates = "CLOSED     LISTEN     SYN_SENT   RECEIVED   ESTABLISHEDFIN_WAIT_1 FIN_WAIT_2 CLOSE_WAIT LAST_ACK   CLOSING    TIME_WAIT  ".getBytes();

    static final byte[] buffer  = new byte[11];

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "netstat\r\n\r\nDumps all TCP connections.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream sout, SystemPrintStream err,
                        String[] args, Hashtable env) throws Exception
    {
        synchronized(buffer)
        {
            byte[] SPACER = "         ".getBytes();
            byte[] ct = com.dalsemi.tininet.TININet.getConnectionTable();

            int count = ct[0];
            int tcb_length = 0;
            if (count > 0)
            {
                tcb_length = (ct.length-1)/count;
            }

            StatsCommand.writeInt(sout, "Connection count: ".getBytes(), count);
            
            java.io.BufferedOutputStream out = new BufferedOutputStream(sout);
            
            out.write("\r\n   Local Port   Remote Port   Remote IP         State\r\n".getBytes());

            int offset;
            int digits;
            for (int i = 0; i < count; i++)
            {
                //Print out iteration number
                digits = StatsCommand.toByteArray(buffer, i+1);
                out.write(buffer, buffer.length - digits, digits);
                out.write(':');

                offset = i*tcb_length+1;

                out.write(SPACER,0,6);
                
                //Print out the Local port
                digits = ((ct[offset+TCB_LOCAL_PORT_OFFSET] << 8) | (ct[offset+TCB_LOCAL_PORT_OFFSET+1] & 0xFF)) & 0x0FFFF;
                digits = StatsCommand.toByteArray(buffer,digits & 0xFFFF);
                for (int j=digits; j<5; j++)
                    out.write(' ');
                out.write(buffer, buffer.length - digits, digits);

                out.write(SPACER,0,9);
                
                //Print out the remote port
                if (ct[offset+TCB_STATE_OFFSET] == 1)
                {
                    out.write(' ');
                    for (int j=0; j<4; j++)
                        out.write('-');
                    out.write(SPACER,0,3);
                    for (int j=0; j<9; j++)
                        out.write('-');
                    out.write(SPACER,0,6);
                }
                else
                {
                    digits = ((ct[offset+TCB_REMOTE_PORT_OFFSET] << 8) | (ct[offset+TCB_REMOTE_PORT_OFFSET+1]&0xFF)) & 0x0FFFF;
                    digits = StatsCommand.toByteArray(buffer,digits & 0xFFFF);
                    for (int j=digits; j<5; j++)
                        out.write(' ');
                    out.write(buffer, buffer.length - digits, digits);

                    out.write(SPACER,0,3);

                    //Print out RemoteIP...
                    int extra = 0;
                    for (int j=0; j<4; j++)
                    {
                        digits = StatsCommand.toByteArray(buffer,ct[offset+TCB_REMOTE_IP_OFFSET+j] & 0x0FF);
                        out.write(buffer, buffer.length - digits, digits);
                        extra += 3 - digits;
                        if (j != 3)
                            out.write('.');
                    }
                    for (int j=0; j<extra; j++)
                        out.write(' ');
                }
                out.write(SPACER,0,3);

                out.write(tcpStates, ct[offset+TCB_STATE_OFFSET] * 11, 11);

                out.write("\r\n".getBytes());
            }
            out.flush();
        }
    }
}
