/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: MoveCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.shell.server.*;

/**
 * Moves a file from source to destination.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class MoveCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "mv SRC DEST\r\n\r\nMoves the file from SRC to DEST." +
               "\r\nAlias: move";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length != 2)
        {
            err.println(getUsageString());
            return;
        }

        File oldFile = new File(args[0]);

        if (oldFile.exists())
        {
            File newFile = new File(args[1]);
            
            if(newFile.exists())
            {
                if(newFile.isDirectory())
                {
                    newFile = new File(newFile, oldFile.getName());
                }
            }
            
/*
            if (newFile.isDirectory() &&!oldFile.isDirectory())
            {
                newFile = new File(newFile.getPath() + File.separator 
                                   + oldFile.getName());
            }
*/

            if (!oldFile.renameTo(newFile))
            {
                throw new Exception("Unable to move " + args[0] + " to " 
                                    + args[1]);
            }
        }
        else
        {
            throw new Exception(args[0] + " does not exist.");
        }
    }
}