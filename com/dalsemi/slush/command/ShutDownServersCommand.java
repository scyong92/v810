/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: ShutDownServersCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;

/**
 * Shutdown a specified system server.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class ShutDownServersCommand implements SlushCommand
{
    
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "stopserver [options]\r\n\r\nShuts down the specified server." +
               "\r\n [-s] Serial Server" +
               "\r\n [-d] Disable console output.  Used only with '-s'." +
               "\r\n [-t] Telnet Server" +
               "\r\n [-f] FTP Server" +
               "\r\nAlias: downserver";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if(!TINIOS.isCurrentUserAdmin())
        {
            err.println(Slush.PERM_DENIED);
            return;
        }

        if(args.length == 0)
        {
            err.println(getUsageString());
            return;
        }
        
        boolean serial = false;
        boolean ftp = false;
        boolean telnet = false;
        
        boolean consoleOff = false;
        
        int opt;
        GetOpt myopts = new GetOpt(args,"!tfsd");
        while ((opt = myopts.getopt()) != myopts.optEOF)
        {
            switch (opt)
            {
                case 's':
                    serial = true;
                break;
                
                case 'd':
                    consoleOff = true;
                break;
                
                case 't':
                    telnet = true;
                break;

                case 'f':
                    ftp = true;
                break;
                
                default:
                    err.println(getUsageString());
                    return;
            }
        }

        out.println("Warning:  This will disconnect users on specified servers.");
        out.print("\r\nOK to proceed? (Y/N): ");

        char    response;

        in.setRawMode(true);
        response = (char) (in.read() & 0x0FF);
        in.setRawMode(false);

        if ((response != 'Y') && (response != 'y'))
        {
            out.println("Aborted.");

            return;
        }
        
        Slush slush = (Slush)TINIOS.getShell();
        if(serial)
        {
            if (consoleOff)
            {
                TINIOS.setConsoleOutputEnabled(false);
                com.dalsemi.system.TINIOS.setSerialBootMessagesState(false);
            }
            
            slush.removeCommPortListener();
            slush.shutDownSerialServer();
        }
        else if (consoleOff)
        {
            err.println("\r\nYou must specify '-s' with the '-d' option.");
            return;
        }
        
        if(telnet)
        {
            slush.shutDownTelnetServer();
        }
        
        if(ftp)
        {
            slush.shutDownFTPServer();
        }
    }
}