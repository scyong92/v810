/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: SourceCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.tininet.*;
import com.dalsemi.fs.*;
import com.dalsemi.system.*;

/**
 * Run a script.
 *
 * 12-17-00 KLA -- everybody's whining about me using BEGIN/END for compound 
 *          statements and %1 for variables, so to shut them up I changed it 
 *          to '{', '}' and $1.  
 * 11-16-00 KLA -- freakin' Tom accidently ran this with a .tini file once,
 *          and guess what he got?  binary data spew!  what a surprise!
 *          ok ok ok i'll do something about it.
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class SourceCommand implements SlushCommand
{
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "source [option] FILE\r\n\r\n"+
                    "Executes a script file.\r\n"+
                    " [-d] Don't verify the file for ASCII data first.\r\n"+
                    "      This command checks a file to see if it has\r\n"+
                    "      non-ASCII range data (bytes with the high bit\r\n"+
                    "      set).  Use this option to disable the time-\r\n"+
                    "      consumming check.\r\n";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length == 0)
        {
            err.println(getUsageString());
            return;
        }
        
        boolean checkInputFile = true;
        
        if (args[0].equals("-d"))
        {
            checkInputFile = false;
            if (args.length == 1)
            {
                err.println(getUsageString());
                return;
            }
        }
        String filename = args[ checkInputFile ? 0 : 1];
        DSFile sourceFile = new DSFile(filename);
        if (!sourceFile.exists())
        {
            err.println(sourceFile.getName() + " does not exist.");
        }
        else if (!sourceFile.canExec())
        {
            err.println(Slush.PERM_DENIED);
        }
        else if (sourceFile.isDirectory())
        {
            err.println("Cannot execute a directory");
        }
        else
        {
            //first lets check to see if this has any binary data in it...i.e.
            //that it has bytes > 127
            if (checkInputFile) 
            {
                try
                {
                    InputStream fin = new FileInputStream(sourceFile);
                    byte[] buffer = new byte[128];
                    int r = fin.read(buffer);
                    while (r!=-1)
                    {
                        for (int k=0;k<r;k++)
                        {
                            if ((buffer[k] & 0x0ff) > 127)
                            {
                                out.println("Binary data encountered.  This file is not a script.");
                                out.println("If this file is an executable, try using the 'java' command.");
                                return;
                            }
                        }
                        r = fin.read(buffer);
                    }
                    fin.close();
                }
                catch(Exception e)
                {
                    out.println("Problem reading from the input file: "+e.toString());
                    return;
                }
            }           
            
            
            //begin actually reading out of the file and executing commands...
            
            BufferedReader input = new BufferedReader(new FileReader(sourceFile));
            String command = null;
            try
            {
                
                command = input.readLine();
                while (command != null)
                {
	                int tries = 1;
	                boolean forever = false;
	                boolean inCompoundStatement;
                    boolean foundBEGIN = false;
	                Vector compoundStatementVector = new Vector();
	                int compoundStatementIndex = 0;

                    //first we've got to fill out Vector of commands to run
                    //treat all statements as compound statements
                    inCompoundStatement = true;
                    while (inCompoundStatement)
                    {
	                    command = command.trim();
        	            if (command.startsWith("#"))
                        {
                            command = input.readLine();
                	        continue;
                        }
                        
	                    int hashPos = 0;
        	            if ( (hashPos = command.indexOf('#')) != -1)
                	    {
                        	command = command.substring(0, hashPos);
	                        command = command.trim();
        	            }
                       
	                    if (command.length()==0)
                        {
                            command = input.readLine();
                	        continue;
                        }
                        
	                    //replace $1 with args[1] here
        	            int replace_index = command.indexOf(" $");
                	    if (replace_index!=-1)
	                    {	
        	                StringTokenizer st = new StringTokenizer(command);
                	        StringBuffer newCommand = new StringBuffer();
                        
	                        while (st.hasMoreTokens())
        	                {
                	            String token = st.nextToken();
                        	    if (token.charAt(0)=='$')
	                            {
        	                        int args_index = Integer.parseInt(token.substring(1));
                	                if (args_index < args.length)
                        	        {
                                	    newCommand.append(args[args_index]);
	                                    newCommand.append(' ');
        	                        }
                	            }
                        	    else
	                            {
	                                newCommand.append(token);
        	                        newCommand.append(' ');
                	            }
                        	}
	                        command = newCommand.toString().trim();
        	            }

	                    //NOW WITH SCRIPTING ABILITY!!!
                    
	                    int space_index = command.indexOf(' ');
                        boolean commandLine = true;

	                    StringTokenizer st = new StringTokenizer(command);
	                    String firstWord = st.nextToken().toUpperCase();
                        commandLine = false;
	                    if (firstWord.equals("WHILE"))
	                    {
	                        if (st.nextToken().toUpperCase().indexOf("TRUE") != -1);
	                            forever = true;
	                    }
	                    else if (firstWord.equals("FOR"))
	                    {
	                        tries = Integer.parseInt(st.nextToken());
	                    }
	                    else if (firstWord.equals("{"))
	                    {
                            foundBEGIN = true;
	                    }
                        else if (firstWord.equals("}"))
                        {
	                        inCompoundStatement = false;
                        }
	                    else //not a script command, but a real command
	                    {
                            if (!foundBEGIN)
                                inCompoundStatement = false;
                            commandLine = true;
                        }
                        if (commandLine) //add it to our vector
                            compoundStatementVector.addElement(command);

                        if (inCompoundStatement) command = input.readLine();
                    } //end while inCompoundStatement

                    //now we have a set of commands to run, so do the whole vector either forever or on a count

                    int tries_made = 0;
                    while ((tries_made < tries) || (forever))
                    try
                    {
                        tries_made++;
                        for (int k=0;k<compoundStatementVector.size();k++)
                        {
                            String com = (String)compoundStatementVector.elementAt(k);
                            CommandInterpreter.execute(Session.getParams(com), in, out, err, env);
                        }
                    }
                    catch(Throwable t)
                    {
                        err.println(t.toString());
                        err.println("Execution continuing...");
                    }

                    command = input.readLine();
                } //end the while loop
            }
            catch(Throwable t)
            {
                err.println("Exception processing file: " + t.toString());
            }
            input.close();
        }
    }
}