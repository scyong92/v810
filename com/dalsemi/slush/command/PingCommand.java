/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: PingCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.slush.*;
import java.io.*;
import java.util.*;
import java.net.InetAddress;
import com.dalsemi.tininet.icmp.Ping;
import com.dalsemi.shell.server.*;

/**
 * Sends ICMP ECHO_REQUEST packets to network hosts
 *
 *
 * @author Don Loomis
 * @version 1.0
 */
public class PingCommand implements SlushCommand
{
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "ping HOST [number of pings]\r\n\r\nSends ICMP ECHO_REQUEST packets to network hosts.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err,
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length > 0)
        {
           InetAddress addr = InetAddress.getByName(args[0]);
           
           try
           {
              int count = 1;
              
              // DEBUG: replace this with getOpt usage.
              if (args.length == 2)
              {
                 count = Integer.parseInt(args[1]);
                 if (count <= 0)
                    throw new NumberFormatException("Count must be > 0");
              }
              
              // Ugly but fast for multiple pings
              byte[] sMsg = ("Got a reply from node " + addr.toString() + "\r\n").getBytes();
              byte[] fMsg = ("Node " + addr + " was unreachable\r\n").getBytes();
              
              int successCount = 0;
              long ms = System.currentTimeMillis();
              for (int i = 0; i < count; i++)
              {
                 boolean response = Ping.pingNode(addr);
                 out.write(response ? sMsg : fMsg);
                 
                 if (response)
                    ++successCount;
                 
                 if (i != (count-1))
                    try {Thread.sleep(1000);} catch (InterruptedException e){}
              }
              
              ms = System.currentTimeMillis() - ms;
              out.println("Sent " + count + " request(s), got " + successCount + " reply(s)");
              //out.println("DEBUG: time elapsed " + ms + " milliseconds");
           }
           catch (NumberFormatException nfe)
           {
              err.println("Ping count must be a positive decimal integer");
              return;
           }
        }
        else
        {
            err.println(getUsageString());
        }
    }
}