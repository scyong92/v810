/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: WdCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.system.*;
import com.dalsemi.slush.*;
import com.dalsemi.shell.server.*;

/**
 * Configure the slush watchdog timer.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class WdCommand implements SlushCommand
{
    private Kennel kennel = null;
    private int    feedInterval;
    private int    interval;
    
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "wd [options]\r\n\r\nSet or display the slush watchdog timer settings." +
               "\r\n [-i interval] Set watchdog interval (ms).  Must be used with '-p' option." +
               "\r\n [-p interval] Set the feed interval (ms).  Must be less than watchdog" +
               "\r\n               interval." +
               "\r\n [-s]          Stop the current watchdog timer." +
               "\r\n\r\nWith no options, displays the current watchdog settings.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length == 0)
        {
            if (kennel == null)
            {
                out.println("\r\nThe slush watchdog timer is not running.");
            }
            else
            {
                out.println("The watchdog is running.");
                out.println("Watchdog interval: " + interval);
                out.println("Feed interval    : " + feedInterval);
            }
            
            return;
        }
        
        int feedInt = -1;
        int wdInt   = -1;
        
        try
        {
            int opt;
            GetOpt myopts = new GetOpt(args,"!i:p:s");
            while ((opt = myopts.getopt()) != myopts.optEOF)
            {
                switch (opt)
                {
                    case 'i':
                        wdInt = Integer.parseInt(myopts.optArgGet());
                    break;
                    
                    case 'p':
                        feedInt = Integer.parseInt(myopts.optArgGet());
                    break;
                    
                    case 's':
                        if (kennel == null)
                        {
                            err.println("\r\nThe slush watchdog timer is not running.");
                        }
                        else
                        {
                            TINIOS.setWatchdogTimeout(0);
                            kennel.shutdown();
                            kennel       = null;
                            interval     = -1;
                            feedInterval = -1;
                        }
                    return;
                    
                    default:
                        err.println(getUsageString());
                    return;
                }
            }
        }
        catch(NumberFormatException nfe)
        {
            err.println("Invalid parameter.");
            err.println(getUsageString());
            return;
        }
        
        if ((wdInt < 0) || (feedInt < 0) || (wdInt <= feedInt))
        {
            err.println(getUsageString());
        }
        else if (kennel != null)
        {
            err.println("The slush watchdog is currently running.  Use '-s' to stop first.");
        }
        else
        {
            interval = wdInt;
            feedInterval = feedInt;
            TINIOS.setWatchdogTimeout(interval);
            kennel = new Kennel(feedInterval);
            kennel.start();
        }
    }
    
    //Feeds the dog at given interval
    class Kennel extends Thread
    {
        int feedInterval;
        boolean shutdown = false;
        
        public Kennel(int interval)
        {
            feedInterval = interval;
        }
        
        public void run()
        {
            shutdown = false;
            while (!shutdown)
            {
                TINIOS.feedWatchdog();
                try
                {
                    Thread.sleep(feedInterval);
                }
                catch(InterruptedException ie)
                {
                }
            }
        }
        
        public void shutdown()
        {
            shutdown = true;
        }
    }
}