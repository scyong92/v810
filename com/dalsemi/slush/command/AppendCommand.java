/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: AppendCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.shell.server.*;

/**
 * Append a source file to a destination file.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class AppendCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "append SRC DEST\r\n\r\nAppends source file to the destination file.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length != 2)
        {
            err.println(getUsageString());
            return;
        }

        File srcFile = new File(args[0]);

        if (!srcFile.isDirectory())
        {
            File destFile = new File(args[1]);

            if (destFile.isDirectory())
            {
                err.println(args[1] + " is a directory.");
                return;
            }

            if (srcFile.exists())
            {
                if(srcFile.equals(destFile))
                {
                    err.println("Cannot append a file to itself.");
                    return;
                }
                
                FileInputStream is = null;
                FileOutputStream os = null;

                try
                {
                    try
                    {
                        is = new FileInputStream(srcFile);
                    }
                    catch(IOException ioe)
                    {
                        err.println(Slush.PERM_DENIED);
                        return;
                    }

                    try
                    {
                        os = new FileOutputStream(destFile.getPath(), true);
                    }
                    catch(IOException ioe)
                    {
                        err.println(Slush.PERM_DENIED);
                        return;
                    }

                    byte[]          fileBytes = new byte[512];
                    int temp;

                    while ((temp = is.read(fileBytes, 0, 512)) != -1)
                    {
                        os.write(fileBytes, 0, temp);
                    }
                }
                finally
                {
                    if(os != null)
                    {
                        os.close();
                    }
                    
                    if(is != null)
                    {
                        is.close();
                    }
                }
            }
            else
            {
                err.println(args[0] + " does not exist.");
            }
        }
        else
        {
            err.println(args[0] + " is a directory.");
        }
    }
}