/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: HostnameCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.system.*;
import com.dalsemi.tininet.*;
import com.dalsemi.shell.server.*;

/**
 * Displays or sets the system hostname and domain name.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class HostnameCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "hostname NAME\r\n\r\nDisplays or sets the system hostname and domain name." +
               "\r\n 'hostname tini1.dalsemi.com' sets the hostname and the domain name." +
               "\r\n 'hostname tini1' sets only the hostname.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length > 0)
        {
            if(!TINIOS.isCurrentUserAdmin())
            {
                err.println(Slush.PERM_DENIED);
                return;
            }
            
            int domainnamePos = -1;
            if ((domainnamePos = args[0].indexOf('.')) != -1)
            {
                TININet.setHostname(args[0].substring(0,domainnamePos));
                TININet.setDomainname(args[0].substring(domainnamePos+1));
            }
            else
                TININet.setHostname(args[0]);
        }
        else
        {
            out.println(TININet.getHostname() + '.' + TININet.getDomainname());
        }
    }
}