/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation.
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: StatsCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.shell.server.*;
import java.io.*;
import java.util.*;
import com.dalsemi.system.*;
import com.dalsemi.slush.*;

/**
 * Display current system status information.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class StatsCommand implements SlushCommand
{
    // HACK-O-MATIC to reduce memory consumption byte stats command
    static final byte[] EOLN   = { (byte)'\r', (byte)'\n', (byte)'\r', (byte)'\n'};
    static final byte[] NOT    = "not ".getBytes();
    static final byte[] active = "active\r\n".getBytes();

    static final byte[] slushVer = ("\r\nTINI slush " + com.dalsemi.slush.Slush.SLUSH_VERSION + "\r\n").getBytes();
    static final byte[] osVer = (TINIOS.getTINIOSFirmwareVersion() + "\r\n").getBytes();
//    static final byte[] hwVer = (TINIOS.getTINIHWVersion()+"\r\n").getBytes();
    static String hwVerStr;
    static byte[] hwVer;
    
    static final byte[] serialNumber = ("S/N "+TINIOS.getTINISerialNumber()+"\r\n").getBytes();

    static final byte[] ftpServer    = "\r\n\r\nFTP Server   :".getBytes();
    static final byte[] telnetServer = "Telnet Server:".getBytes();
    static final byte[] serialServer = "Serial Server:".getBytes();

    static final byte[] freeRAM = "\r\n\r\nFree RAM: ".getBytes();

    // Could combine upTime and hours, but ..
    static final byte[] upTime  = "\r\nSystem up time:".getBytes();
    static final byte[] days    = "\r\n      Days: ".getBytes();
    static final byte[] hours   = "\r\n     Hours: ".getBytes();
    static final byte[] minutes = "\r\n   Minutes: ".getBytes();
    static final byte[] seconds = "\r\n   Seconds: ".getBytes();

    static final byte[] lastBoot = "\r\nLast Boot Status:".getBytes();
    static final byte[] yes = "yes".getBytes();
    static final byte[] no  = "no".getBytes();

    //KLA added support for getBootState stuff from TINI OS
    static final byte[][] boot_texts =
    {
        "\r\n   Network Restored from Flash: ".getBytes(),
        "\r\n         Master Erase Occurred: ".getBytes(),
        "\r\n           Heap Clear Occurred: ".getBytes(),
        "\r\n          File System Modified: ".getBytes()
    };


    // Use this to hold all converted integers
    static byte[] intBuffer = new byte[11];

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "stats\r\n\r\nDisplay current system status information.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err,
                        String[] args, Hashtable env) throws Exception
    {
        boolean verbose = false;
        for(int i = 0; i < args.length; i++)
        {
            if(args[i].equals("-v"))
            {
                verbose = true;
            }
            else
            {
                out.println(getUsageString());
                return;
            }
        }
        
        long currMillis = (System.currentTimeMillis() - Slush.bootTime) / 1000;

        int h = (int)(currMillis/3600);
        int m = (int)((currMillis % 3600)/60);
        int s = (int)(currMillis % 60);
        int d = h / 24;
        h = h % 24;

        Slush slush = (Slush)TINIOS.getShell();

        try
        {
            // Display system version info (add API version)
            out.write(slushVer);
            out.write(osVer);
            
            if(verbose)
            {
                if(hwVerStr == null)
                {
                    hwVerStr = TINIOS.getTINIHWVersion();
                    hwVer = (hwVerStr == null ? "" : hwVerStr+"\r\n").getBytes();
                }
                out.write(hwVer);
                out.write(serialNumber);
            }

            // Display system uptime
            out.write(upTime);
            writeInt(out, days, d);
            writeInt(out, hours, h);
            writeInt(out, minutes, m);
            writeInt(out, seconds, s);

            // Display amount remaining heap
            writeInt(out, freeRAM, TINIOS.getFreeRAM());

            // Display status of all slush servers
            out.write(ftpServer);
            if (!slush.isFTPServerRunning()) out.write(NOT);
            out.write(active);

            out.write(telnetServer);
            if (!slush.isTelnetServerRunning()) out.write(NOT);
            out.write(active);

            out.write(serialServer);
            if (!slush.isSerialServerRunning()) out.write(NOT);
            out.write(active);

            if(verbose)
            {
                out.write(lastBoot);

                int state = 0;
                for (int i=0;i<4;i++)
                {
                    switch(i)
                    {
                        case 0 : state = TINIOS.NETWORK_CONFIGURATION_RESTORED;
                                break;
                        case 1 : state = TINIOS.MASTER_ERASE_OCCURRED;
                                break;
                        case 2 : state = TINIOS.HEAP_CLEAR_OCCURRED;
                                break;
                        case 3 : state = TINIOS.FS_MODIFICATION_OCCURRED;
                                break;
                    }
                    out.write(boot_texts[i]);
                    out.write(((TINIOS.getBootState() & state) == 0) ? no : yes);
                }
                out.write(EOLN);
            }
        }
        catch (IOException ioe)
        {
            out.println("Error displaying stats");
            out.println(ioe);
        }
    }

    public static void writeInt(PrintStream out, byte[] msg, int i)
        throws IOException
    {
        synchronized (intBuffer)
        {
            out.write(msg);
            int digits = toByteArray(intBuffer, i);
            out.write(intBuffer, intBuffer.length - digits, digits);
        }
    }

    /*
     * Chop i up into byte array and return number of bytes used (aka # of
     * digits + 1 if negative). This array is filled from the end.
     */
    public static int toByteArray(byte[] b, int i)
    {
        int digits = 0;
        int pos = b.length;
        boolean negative = false;

        if(i < 0)
        {
            negative = true;
            i = -i;
        }

        do
        {
            // Extract the current lsd and make it printable
            b[--pos] = (byte) (i%10 + (byte) '0');
            // Shift in the next digit
            i /= 10;
            ++digits;
        }
        while (i != 0);

        if (negative)
        {
            // prepend that pesky little '-' if need be
            b[--pos] = (byte) '-';
            ++digits;
        }

        return digits;
    }

}