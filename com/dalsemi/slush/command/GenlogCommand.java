/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: GenlogCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.slush.*;
import java.io.*;
import java.util.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.fs.*;

/**
 * Enable or disable generation of system log file.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class GenlogCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "genlog [option]\r\n\r\nToggles system log generation on boot." +
               "\r\n [-e]  Enable log." +
               "\r\n [-d]  Disable log.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        boolean enable = false;
        
        if(!TINIOS.isCurrentUserAdmin())
        {
            err.println(Slush.PERM_DENIED);
            return;
        }
        
        if (args.length == 0)
        {
            File logFile = new File(Slush.LOG_FILE);
            if (logFile.exists())
            {
                out.println("Log file is enabled.");
            }
            else
                out.println("Log file is disabled.");
                
            return;
        }
         
        int opt;
        GetOpt myopts = new GetOpt(args,"!ed");
        while ((opt = myopts.getopt()) != myopts.optEOF)
        {
            switch (opt)
            {
                case 'e':
                    enable = true;
                break;
                
                case 'd':
                    enable = false;
                break;
                
                default :
                    err.println(getUsageString());
                return;
            }
        }
        
        if (enable)
        {
            DSFile logFile = new DSFile(Slush.LOG_FILE);
            if (logFile.exists())
            {
                out.println("Log file already enabled.");
            }
            else
            {
                logFile.touch();
                
                out.println("Log file now enabled.");
            }
        }
        else
        {
            File logFile = new File(Slush.LOG_FILE);
            if (logFile.exists())
            {
                logFile.delete();
                out.println("Log file now disabled.");
            }
            else
                out.println("Log file already disabled.");
        }
    }
}