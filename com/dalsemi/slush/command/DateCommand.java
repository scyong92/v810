/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation.
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: DateCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;

/**
 * Displays or sets the system time information.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class DateCommand implements SlushCommand
{
    String[] days   = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
                       "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "date [option] [MMDDYYYYHHMMSS] [timezone]\r\n\r\nDisplay or set system time information." +
                   "\r\nWhere options include: " +
                   "\r\n [-t]  Display valid timezones." +
                   "\r\nSetting the date must be in the form of: MMDDYYYYHHMMSS<am | pm> [timezone]" +
               "\r\n\r\nExample: to set March 17, 1975 3:35:23" +
                   "\r\n  date 03171975033523pm  OR  date 03171975153523" +
               "\r\n\r\nExample: to set April 5 in Central Standard time" +
                   "\r\n  date 0405 CST" +
               "\r\n\r\nExample: to just set timezone to Pacific Standard" +
                   "\r\n  date PST" +
               "\r\n\r\nExample: to set the timezone to an offset from GMT" +
                   "\r\n date +10" +
               "\r\n\r\nNote that you can specify an offset only in hours for the timezone." +
                   "\r\n date +10 OR date 10202000102630 -6" + 
                   "\r\nThis could be useful for locations that might use a Time Zone like CST "+
                   "\r\nbut do not use Daylight Savings Time.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    // wpd - commented out the throws Exception to get rid of a jikes warning
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err,
                        String[] args, Hashtable env) //throws Exception
    {
        String SPACER   = " ";
        String EMPTY    = "";
        String AM       = "am";
        String PM       = "pm";
        String ZERO     = "0";

        Clock clock = new Clock();

        if (args.length == 0)
        {
            try
            {
                //First, write GMT time.
                out.println(days[clock.getDay() - 1] + SPACER + months[clock.getMonth()-1] + SPACER +
                            clock.getDate() + SPACER +
                            ((clock.getHour() < 10) ? ZERO : EMPTY) + clock.getHour() + ":" +
                            ((clock.getMinute() < 10) ? ZERO : EMPTY) + clock.getMinute() + ":" +
                            ((clock.getSecond() < 10) ? ZERO : EMPTY) + clock.getSecond() +
                            (clock.get12Hour() ? ((clock.getPm()) ? PM : AM) : EMPTY) + " GMT " +
                            "20" + ((clock.getYear() < 10) ? ZERO : EMPTY) + clock.getYear());
                //Now, show in local timezone if set.


                String tz = com.dalsemi.system.TINIOS.getTimeZone();
                if (!tz.equals("GMT"))
                {
                    if (!TimeZone.getDefault().getID().equals(tz))
                    {
                      // wpd - commented out the throws Exception to get rid of a jikes warning
                      // wpd - why this try catch was here in the first place is a mystery to me
                      // try
                      // {
                      TimeZone.setDefault(TimeZone.getTimeZone(tz));
                      // }
                      // catch(Exception e)
                      // {
                      //   //must not have been a valid time zone
                      // }
                    }
                    if (TimeZone.getDefault().getID().startsWith("+") || TimeZone.getDefault().getID().startsWith("-"))
                    {
                       GregorianCalendar gc = new GregorianCalendar(TimeZone.getDefault());
                       out.println(gc.getTime());
                    }
                    else
                    {
                       out.println(new Date());
                    }

                }
            }
            catch(Exception e)
            {
                out.println("You need to set the clock.  See 'help date'.");
            }

            return;
        }
        else if ((args.length == 1) && (args[0].equals("-t")))
        {
            //Display valid timezones
            String[] zones = TimeZone.getAvailableIDs();
            out.println("Available Timezones: " );
            StringBuffer sb = new StringBuffer();
            for (int i=0; i<zones.length; i++)
            {
                //out.print(zones[i]);
                sb.append(zones[i]);
            
		        TimeZone tz = TimeZone.getTimeZone(zones[i]);
		        int raw = tz.getRawOffset() / (1000*60);
		        int temp_raw = (raw < 0) ? -raw : raw;
		        int hrs = temp_raw / 60;
		        int mins = temp_raw % 60;
		        //String hrs_str = ((hrs < 10) ? "0" : "") + hrs;
		        
		        sb.append(" (");
		        sb.append((raw >= 0) ? '+' : '-');
		        
		        if (hrs<10) sb.append('0');
		        sb.append(hrs);
		        if (mins==0) sb.append('0');
		        sb.append(mins);
		        sb.append(')');
		        //String mins_str = (mins==0) ? "00" : Integer.toString(mins);

		        //out.print(((raw >= 0) ? " (+" : " (-")+hrs_str+mins_str+")"    );
		        
		        sb.append(tz.useDaylightTime() ? '*' : ' ');
		        //out.print(tz.useDaylightTime() ? "*" : " ");

                if (((i+1)%5) == 0)
                {
                    out.println(sb.toString());
                    sb.setLength(0);
                }
                else
                {
                    //out.print("    ");
                    sb.append("    ");
                }
            }
            out.println(sb.toString());
            out.println("* denotes a time zone that uses Daylight Savings");

            return;
        }

        String time = null;
        String zone = null;
        if (args.length == 1)
        {
            if (args[0].length() == 3 || args[0].length() == 2)
                zone = args[0];
            else
                time = args[0];
        }
        else if (args.length == 2)
        {
            if (args[0].length() == 3 || args[0].length() == 2)
            {
                zone = args[0];
                time = args[1];
            }
            else if (args[1].length() == 3 || args[0].length() == 2)
            {
                zone = args[1];
                time = args[0];
            }
            else
            {
                err.println(getUsageString());
                return;
            }
        }
        else
        {
            err.println(getUsageString());
            return;
        }
        if (zone != null)
        {
            
            try
            {
                TimeZone testZone = TimeZone.getTimeZone(zone);
                if (testZone==null)
                    throw new IllegalArgumentException();
                TINIOS.setTimeZone(zone);
            }
            catch(IllegalArgumentException iae)
            {
                err.println(zone + ": not a valid timezone.  Use \"date -t\" for a timezone list.");
                return;
            }
        }

        if (time != null)
        {
            try
            {
                int fullYear = clock.getYear() + 2000;
                int date = clock.getDate();
                int month = clock.getMonth();
                int hour  = clock.getHour();
                int minute = clock.getMinute();
                int second = clock.getSecond();
                boolean pm = clock.getPm();
                boolean is12Hour = clock.get12Hour();

                //Use the length of the String to indicate which options are set.
                //Notice that we don't break out of any of the cases.  If the length
                //is 16, this means they've specified an am/pm, and we walk the rest
                //of the string to extract the other data.
                switch(time.length())
                {
                    case 16:
                        String ampm = time.substring(14,16);
                        if (ampm.equalsIgnoreCase(AM))
                            pm = false;
                        else if (ampm.equalsIgnoreCase(PM))
                            pm = true;
                        else
                            throw new Exception();
                        is12Hour = true;
                    case 14:
                        second = Integer.parseInt(time.substring(12,14));
                        if(second > 59)
                            throw new Exception();
                        if (time.length() == 14)
                        {
                            //Need to assume it is a 24 hour setting.
                            is12Hour = false;
                        }
                    case 12:
                        minute = Integer.parseInt(time.substring(10,12));
                        if(minute > 59)
                            throw new Exception();
                    case 10:
                        hour = Integer.parseInt(time.substring(8,10));
                        if (is12Hour)
                        {
                            if(hour > 12)
                                throw new Exception();
                        }
                        else
                        {
                            if(hour > 23)
                                throw new Exception();
                        }
                    case 8:
                        fullYear = Integer.parseInt(time.substring(4,8));
                        if ( (fullYear < 2000) || (fullYear > 2099) )
                        {
                            err.println("You must enter a year between 2000 and 2099.");
                            return;
                        }
                    case 4:
                        date = Integer.parseInt(time.substring(2,4));
                        if(date < 1 || date > 31)
                            throw new Exception();
                    case 2:
                        month = Integer.parseInt(time.substring(0,2));
                        if(month < 1 || month > 12)
                            throw new Exception();
                    break;

                    default:
                        err.println(getUsageString());
                    return;
                }

                if (is12Hour)
                {
                    //The hour we give to the Date object should be 24 hour.
                    hour %= 12;
                    if (pm)
                    {
                        hour += 12;
                    }
                }
                //DEBUG ==> There must be a more elegant way to transform one timezone
                //          to another!

                //First, create a Date object in the default timezone.  Note that you
                //pass year-1900, 0 index month and date to the Date constructor.
                Date myDate = new Date(fullYear-1900, month-1, date, hour, minute, second);
                //Now, grab the milliseconds since Jan 1, 1970.
                long millis = myDate.getTime();
                //Take into account the delta from GMT


//                millis += (myDate.getTimezoneOffset() * 60000);

                //changes this from += to -= because we are setting to GMT.  So if time zone A is
                //is 6 hours behind GMT, its time zone offset will be -60 minutes (or whatever, but
                //it will be negative), which means that GMT is greater than time zone A, so since
                //we are going from time zone A to GMT, we should add... a - (-b) = a + b
                //---Pinky, Brain
                millis -= (myDate.getTimezoneOffset() * 60000);



                //Now, create a Date object that should represent GMT time based on the
                //timezone specific information the user just entered.
                myDate = new Date(millis);

                //Finally, pull out the information to set the real-time clock.
                clock.setDate(myDate.getDate());
                clock.setMonth(myDate.getMonth() + 1);
                clock.setYear(myDate.getYear() % 100);
                if (myDate.getHours() > 12)
                   clock.set12Hour(false);
                clock.setHour(myDate.getHours());
                clock.setMinute(myDate.getMinutes());
                clock.setSecond(myDate.getSeconds());
                clock.setDay(myDate.getDay() + 1);

                // Save difference in current time from boot time.
                long currUptime = System.currentTimeMillis() - Slush.bootTime;
                clock.setRTC();
                // Adjust for the resetting of the clock
                Slush.bootTime = System.currentTimeMillis() - currUptime;

            }
            catch(Exception e)
            {
                err.println(getUsageString());
            }
        }
    }
}
