/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: CatCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.shell.server.*;

/**
 * Displays the contents of a file.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class CatCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "cat FILE\r\n\r\nDisplays the contents of a file.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if(args.length == 0)
        {
            err.println(getUsageString());
            return;
        }

        File currFile = new File(args[0]);

        if (currFile.exists())
        {
            if (!currFile.canRead())
            {
                err.println(Slush.PERM_DENIED);
            }
            else if (!currFile.isDirectory())
            {
                FileInputStream fileInStream = new FileInputStream(currFile);
                int             fileLength = (int) currFile.length();
                int             fileStreamOffset = 0;

                byte[]          fileData = new byte[512];

                while (fileStreamOffset != fileLength)
                {
                    int bytesRead = fileInStream.read(fileData);

                    if (bytesRead <= 0)
                        break;
                        
                    out.write(fileData, 0, bytesRead);

                    fileStreamOffset += bytesRead;
                }

                fileInStream.close();
            }
            else
            {
                err.println(args[0] + " is a directory.");
            }
        }
        else
        {
            err.println(args[0] + " does not exist.");
        }
    }
}