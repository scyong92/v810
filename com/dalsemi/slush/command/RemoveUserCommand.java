/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: RemoveUserCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;

/**
 * Remove a user to the system.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class RemoveUserCommand implements SlushCommand
{
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "userdel [user(s)]\r\n\r\nRemoves this user from the system." +
               "\r\nMultiple users can be specified separated with spaces.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if(!TINIOS.isCurrentUserAdmin())
        {
            err.println(Slush.PERM_DENIED);
            return;
        }
        else if(args.length == 0)
        {
            err.println(getUsageString());
            return;
        }
        
        synchronized(Slush.passwdLock)
        {        
            Vector passwdEntries = new Vector(5, 1);
            RandomAccessFile fis = new RandomAccessFile(Slush.PASSWORD_FILE, "r");
            String line;
            while((line = fis.readLine()) != null)
                passwdEntries.addElement(line);
                
            fis.close();
            
            boolean userDeleted = false;
            boolean actionTaken = false;
            for(int i = 0; i < args.length; i++)
            {
                userDeleted = false;
                line = args[i] + ':';
                
                for (int j = passwdEntries.size() - 1; j >= 0; j--)
                {
                    if (((String)passwdEntries.elementAt(j)).startsWith(line))
                    {
                        actionTaken = true;
                        userDeleted = true;
                        passwdEntries.removeElementAt(j);
                    }
                }
                
                if (!userDeleted)
                {
                    err.println(args[i] + " not a valid user.");
                }
            }
            
            if (actionTaken)
            {
                PrintStream fos = new PrintStream(new FileOutputStream(Slush.PASSWORD_FILE));
                for (int j = 0; j < passwdEntries.size(); j++)
                {
                    fos.println((String)passwdEntries.elementAt(j));
                }
                fos.close();
                    
                ((Slush)TINIOS.getShell()).clearUserNameByUIDCache();
            }
        }
    }
}