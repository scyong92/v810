/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: SlushCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.shell.server.*;

/**
 * The interface that any commands added to the slush shell must implement.
 *
 * @see com.dalsemi.slush.CommandInterpreter#addCommand(String, SlushCommand)
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public interface SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     * @return the usage.
     */
    public String getUsageString();

    /**
     * Executes this command.
     *
     *
     * @param in  The input stream this command will use to receive input.
     * @param out  The print stream this command should use to report any
     * non-critical messages.
     * @param err  The print stream this command should use to report any
     * critical messages.
     * @param args  The list of arguments given when this command was invoked.
     * This does not include the command name itself.
     * @param env  A table of environment variables.
     *
     * @throws Exception Any exception raised by the underlying command.
     * The getMessage() method of this exception should be as descriptive
     * as possible since the SLUSH shell will use it to report the exception
     * back to the user.
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception;
}

