/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: LsCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.slush.*;
import com.dalsemi.shell.server.*;
import java.io.*;
import java.util.*;
import com.dalsemi.fs.*;

/**
 * Displays a listing of files in a directory.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class LsCommand implements SlushCommand
{
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "ls [option] FILE\r\n\r\nReturns a listing of the files." +
               "\r\n [-l]    Show file attributes" +
               "\r\nAlias: dir";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        boolean longOption = false;
        String pathRequested = "";
        
        for(int i = 0; i < args.length; i++)
        {
            if(args[i].equals("-l"))
            {
                longOption = true;
            }
            else
            {
                pathRequested = args[i];
            }
        }

        if (pathRequested.length() == 0)
        {
            pathRequested = (String) env.get(Session.CURRENT_DIRECTORY);
        }
        
        DSFile file = new DSFile(pathRequested);

        if(longOption)
        {
            boolean listing = file.listLong(out, (Thread.currentThread() instanceof com.dalsemi.shell.server.ftp.FTPSession));
            
            if(listing == false)
            {
                throw new Exception(pathRequested + ": No such file or directory.");
            }
        }
        else
        {
            String[] listing = file.list();
            
            if ((listing == null) || (listing.length == 0))
            {
                if(file.exists())
                {
                    if(file.isFile())
                    {
                        listing = new String[1];
                        listing[0] = file.getName();
                    }
                    else
                    {
                        if(listing == null)
                        {
                            listing = new String[0];
                        }
                    }
                }
                else
                {
                    throw new Exception(pathRequested + ": No such file or directory.");
                }
            }
            
            BufferedOutputStream bout = new BufferedOutputStream(out);
            for(int i = 0; i < listing.length; i++)
            {
                //out.println(listing[i]);
                
                //bufferedoutputstream, so we are not byte-banging
                bout.write(listing[i].getBytes());
                bout.write((int)'\r');
                bout.write((int)'\n');
            }
            bout.flush();
        }
    }
}