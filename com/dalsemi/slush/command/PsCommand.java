/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: PsCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.slush.*;
import java.io.*;
import java.util.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;

/**
 * List the currently running processes.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class PsCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "ps\r\n\r\nGet list of currently running processes.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        String[]    psval = com.dalsemi.system.TINIOS.getTaskTable();

        out.println(psval.length + " process" 
                    + ((psval.length == 1) ? "" : "es"));

        String owner = null;
        for (int i = 0; i < psval.length; i++)
        {
            try
            {
                int id = Integer.parseInt((new StringTokenizer(psval[i], ":")).nextToken());
                int ownerID = com.dalsemi.system.TINIOS.getOwnerIDByTaskID(id);
                owner = com.dalsemi.system.TINIOS.getUserNameByUID((byte)ownerID);
            }
            catch(Exception e)
            {
                owner = "unknown";
            }
            
            out.println(psval[i] + " (Owner "+owner+")");
        }
    }
}