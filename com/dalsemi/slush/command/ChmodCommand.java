/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: ChmodCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.fs.*;
import com.dalsemi.shell.server.*;

/**
 * Changes the permissions of a file.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class ChmodCommand implements SlushCommand
{
    static final char READ   = 'r';
    static final char WRITE  = 'w';
    static final char EXEC   = 'x';
    static final char ADD    = '+';
    static final char REMOVE = '-';
    static final char WORLD  = 'o';
    static final char USER   = 'u';

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "chmod [options] FILE\r\n\r\nChange the permissions of a file." +
               "\r\nOptions: [[u|o][+|-][r|w|x]] OR [##]" +
               "\r\nExamples:" +
               "\r\nTo remove user's read and write permission on s.txt" +
               "\r\n  chmod -r-w s.txt <OR> chmod 10 s.txt" +
               "\r\nTo remove other's execute and add read permission on s.txt" +
               "\r\n  chmod o+r-x s.txt <OR> chmod 75 s.txt";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length == 0)
        {
            err.println(getUsageString());
            return;
        }
        
        DSFile currFile = new DSFile(args[args.length-1]);
        boolean add = false;
        boolean world = false;
        int userPerm = 7;
        int worldPerm = 7;
        
        if (args.length == 2)
        {
            try
            {
                //Test to see if this is a number chmod
                int fullNumber = Integer.parseInt(args[0], 16);
                if (args[0].length() != 2)
                {
                    err.println(getUsageString());
                    return;
                }
                userPerm = (fullNumber & 0x0F0) >> 4;
                worldPerm = (fullNumber & 0x0F);

                currFile.setOtherPermissions(worldPerm);
                currFile.setUserPermissions(userPerm);
                
                return;
            }
            catch(NumberFormatException nfe)
            {
                //Must not be a number chmod, try the other format
            }
        }
        
        userPerm = currFile.getUserPermissions();
        worldPerm = currFile.getOtherPermissions();
        for (int i=0; i<args[0].length(); i++)
        {
            if (args[0].charAt(i) == WORLD)
            {
                world = true;
            }
            else if (args[0].charAt(i) == USER)
            {
                world = false;
            }
            else if (args[0].charAt(i) == ADD)
            {
                add = true;
            }
            else if (args[0].charAt(i) == REMOVE)
            {
                add = false;
            }
            else if (args[0].charAt(i) == READ)
            {
                if (add)
                {
                    if (world)
                        worldPerm |= 0x04;
                    else
                        userPerm |= 0x04;
                }
                else
                {
                    if (world)
                        worldPerm &= 0xFB;
                    else
                        userPerm &= 0xFB;
                }
            }
            else if (args[0].charAt(i) == WRITE)
            {
                if (add)
                {
                    if (world)
                        worldPerm |= 0x02;
                    else
                        userPerm |= 0x02;
                }
                else
                {
                    if (world)
                        worldPerm &= 0xFD;
                    else
                        userPerm &= 0xFD;
                }
            }
            else if (args[0].charAt(i) == EXEC)
            {
                if (add)
                {
                    if (world)
                        worldPerm |= 0x01;
                    else
                        userPerm |= 0x01;
                }
                else
                {
                    if (world)
                        worldPerm &= 0xFE;
                    else
                        userPerm &= 0xFE;
                }
            }
        }
        
        try
        {
            currFile.setOtherPermissions(worldPerm);
            currFile.setUserPermissions(userPerm);
        }
        catch(IOException ioe)
        {
            err.println(Slush.PERM_DENIED);
        }
    }
}