/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: SetenvCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.slush.*;
import java.io.*;
import java.util.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;

/**
 * Set or display environment variables
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class SetenvCommand implements SlushCommand
{

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "setenv VAR VAL\r\n\r\nSet the variable to the value in the current environment." +
               "\r\nNo parameters shows current environment.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        for(int i = 0; i < args.length; i++)
        {
            args[i] = parseEscapeCodes(args[i]);
        }
        
        if (args.length == 0)
        {
            Enumeration keys = env.keys();
            Enumeration elements = env.elements();

            while (keys.hasMoreElements())
            {
                out.println("'" + keys.nextElement() + "' " + elements.nextElement());
            }

            return;
        }
        else if (args.length == 1)
        {
            if ((args[0].indexOf('$') == -1) &&
                (args[0].indexOf(' ') == -1))
                env.put(args[0], "");
            else
            {
                err.println("\r\nSyntax error.");
            }
        }
        else
        {
            if ((args[0].indexOf('$') == -1) &&
                (args[0].indexOf(' ') == -1))
                env.put(args[0], args[1]);
            else
            {
                err.println("\r\nSyntax error.");
            }
        }
    }

    public static String parseEscapeCodes(String s)
    {
        int len = s.length();
        StringBuffer sb = new StringBuffer(len);
        
        for(int i = 0; i < len; i++)
        {
            char ch = s.charAt(i);
            switch (ch)
            {
                case '\\':
                    ch = s.charAt(++i);
                    switch(ch)
                    {
                        case '"':
                        case '\\':
                        case '\'':
                            break;
                        case 'b':
                            ch = '\b';
                            break;
                        case 'f':
                            ch = '\f';
                            break;
                        case 'n':
                            ch = '\n';
                            break;
                        case 'r':
                            ch = '\r';
                            break;
                        case 't':
                            ch = '\t';
                            break;
                        case 'u':
                            ch = (char)(Character.digit(s.charAt(++i),16) << 12);
                            ch |= Character.digit(s.charAt(++i),16) << 8;
                            ch |= Character.digit(s.charAt(++i),16) << 4;
                            ch |= Character.digit(s.charAt(++i),16);
                            break;
                        default:
                            throw new IllegalArgumentException("Invalid Escape Character: '" + ch + "'");
                    }
                default:
                    sb.append(ch);
            }
        }
        return sb.toString();
    }
}