/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: HelpCommand.java
*/
package com.dalsemi.slush.command;

import com.dalsemi.slush.*;
import java.io.*;
import java.util.*;
import com.dalsemi.shell.server.*;

/**
 * Displays all available commands, or help information for a given command.
 *
 *
 * @Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class HelpCommand implements SlushCommand
{
    static char[] newLine = { '\r', '\n' };
    
    static char[] twoSpaces = { ' ', ' ' };
    
    static byte[] commandList;
    
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "help\r\n\r\nLists all of the available commands.\r\n" 
               + "help [command]  Prints command specific usage.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env)
    {
        if (args.length > 0)
        {
            SlushCommand    c = CommandInterpreter.getSlushCommand(args[0]);

            if (c != null)
            {
                out.println(c.getUsageString());
            }
            else
            {
                out.println("Command not found.");
            }
        }
        else
        {
            if (commandList == null)
            {
                StringBuffer outBuff = new StringBuffer();
                outBuff.append("Available Commands:\r\n");

                Enumeration e = CommandInterpreter.getAvailableCommands();
                Vector cmds = new Vector(CommandInterpreter.getNumberOfCommands());

                while (e.hasMoreElements())
                {
                    //Alphabetize the list.  A little slow, but only happens
                    //the first time the user types help.
                    String cmd  = (String)e.nextElement();
                    String curr = null;
                    for (int i=0; i<cmds.size(); i++)
                    {
                        curr = (String)cmds.elementAt(i);
                        if (cmd.compareTo(curr) < 0)
                        {
                            cmds.insertElementAt(cmd, i);
                            cmd = null;
                            break;
                        }
                    }
                    if (cmd != null)
                        cmds.addElement(cmd);
                }
                
                
                //Find the largest length
                int largest = 0;
                int current = 0;
                
                // Avoid heavy weight method invocation in the following loops
                int size = cmds.size();
                for (int i=0; i<size; i++)
                {
                    current = ((String)cmds.elementAt(i)).length();
                    if (current > largest)
                        largest = current;
                }
                
                //Output each command, 4 to a line
                for (int i=0; i<size; i++)
                {
                    if ((i&3) == 0)
                    {
                        outBuff.append(newLine);
                    }
                    String str = (String)cmds.elementAt(i);
                    
                    outBuff.append(twoSpaces);
                    outBuff.append(str);
                    outBuff.append(twoSpaces);
                    
                    int padLen = largest-str.length();
                    for (int j=0; j<padLen; j++)
                    {
                        outBuff.append(' ');
                    }
                }
                
                outBuff.append(newLine);
                commandList = outBuff.toString().getBytes();
            }
            
            try
            {
                out.write(commandList);
            }
            catch (IOException ioe)
            {
                out.println(ioe);
            }
        }
    }
    
    public static void clearCommandListCache()
    {
        commandList = null;
    }
}