/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation. 
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States 
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: AddUserCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.system.*;
import com.dalsemi.fs.*;

/**
 * Add a user to the system.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class AddUserCommand implements SlushCommand
{
    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "useradd [options]\r\n\r\nAdds a user to the system.\r\n" +
               "\r\n [-n username] Username" +
               "\r\n [-p passwd]   Password" +
               "\r\n [-i userid]   Userid" +
               "\r\nValid id's range from 1 to 255.  All id's larger\r\n" +
               "than 127 will have super user privileges.\r\n" +
               "The user ID of 0 is reserved for guest login, 128 reserved for root.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err, 
                        String[] args, Hashtable env) throws Exception
    {
        if(!TINIOS.isCurrentUserAdmin())
        {
            err.println(Slush.PERM_DENIED);
            return;
        }
        
        String name = null;
        String pass = null;
        String idStr = null;
        
        int opt;
        GetOpt myopts = new GetOpt(args,"!n:p:i:");
        while ((opt = myopts.getopt()) != myopts.optEOF)
        {
            String temp = myopts.optArgGet();
            
            if (opt == 'i')
                idStr = temp;
            else if (opt == 'n')
                name = temp;
            else if (opt == 'p')
                pass = temp;
            else
            {
                err.println(getUsageString());
                return;
            }
        }
        
        if(name == null)
        {
            out.print("Enter the new user name:");
            name = in.readLine();
            if (name.length() == 0)
            {
                err.println("You must specify a user name.");
                return;
            }
        }
        
        if(name.indexOf(':') != -1)
        {
            err.println("The ':' character is not allowed.");
            return;
        }
        
        if(pass == null)
        {
            in.setEcho(false);
            out.print("Enter the password for " + name + ":");
            pass = in.readLine();
            in.setEcho(true);
            if (pass.length() == 0)
            {
                err.println("You must specify a password.");
                return;
            }
        }
        
        if(idStr == null)
        {
            out.print("Enter the user ID for " + name + ":");
            idStr = in.readLine();
            if (idStr.length() == 0)
            {
                err.println("You must specify a user ID.");
                return;
            }
        }
                
        short uid = Short.decode(idStr).shortValue();
        if(uid < 0 || uid > 255)
        {
            err.println("Invalid user ID.  Use 'help useradd' for details.");
            return;
        }
        
        synchronized (Slush.passwdLock)
        {
            RandomAccessFile passfile = new RandomAccessFile(Slush.PASSWORD_FILE, "rw");
            String line;
            while((line = passfile.readLine()) != null)
            {
                //fix the problem where you couldn't create a user called "roo"
                String thisname = line.substring(0,line.indexOf(":"));
                
                if (thisname.equals(name))
                {
                    err.println("A user already exists with that name.");
                    passfile.close();
                    return;
                }
            }
            
            passfile.writeBytes(name + ":");
            byte[] phash = com.dalsemi.system.Security.hashMessage((name + ":" + pass).getBytes());
            for(int i = 0; i < 20; i++)
            {
                int num = phash[i] & 0x0FF;
                if(num < 16)
                    passfile.write('0');
                passfile.writeBytes(Integer.toHexString(num));
            }
            passfile.writeBytes(":" + idStr + "\r\n");
            passfile.close();
            
            //Make sure other read is set.
            DSFile passwdFile = new DSFile(Slush.PASSWORD_FILE);
            passwdFile.setOtherPermissions(0x04);
        }
        
        ((Slush)TINIOS.getShell()).clearUserNameByUIDCache();
    }
}