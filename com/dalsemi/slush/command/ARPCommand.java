/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation.
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: ARPCommand.java
*
*   Eventually I need to write a real arp. This ain't it. This is purely
* for my debugging purposes.
* (DWL)
*/
package com.dalsemi.slush.command;

import com.dalsemi.shell.server.*;
import com.dalsemi.slush.*;
import java.io.*;
import java.util.*;
import java.net.InetAddress;

/**
 * Dumps all ARP cache entries.
 *
 * @version 1.0
 * @author  DWL
 */
public class ARPCommand implements SlushCommand
{
    static final int ARP_CACHE_ENTRY_LENGTH = 12;

    static final int ARP_FLAGS_OFFS    = 0;
    static final int ARP_TTL_OFFS      = 1;
    static final int ARP_ETH_ADDR_OFFS = 2;
    static final int ARP_IP_ADDR_OFFS  = 8;
    
    static final byte[] intBuffer  = new byte[11];

    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "arp\r\n\r\nDumps all ARP cache entries.";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err,
                        String[] args, Hashtable env) throws Exception
    {
        synchronized(intBuffer)
        {
            byte[] SPACER     = "   ".getBytes();
            
            byte[] ac = com.dalsemi.tininet.TININet.getARPCacheTable();

            // Kludge because it's tied to a native equate
            int count = ac.length / ARP_CACHE_ENTRY_LENGTH;

            out.write("\r\n   IP Address        Ethernet Address    TTL   Flags\r\n".getBytes());

            int offset;
            int digits;
            for (int i = 0; i < count; i++)
            {
                offset = i*ARP_CACHE_ENTRY_LENGTH;
                
                if ((ac[offset] & 0x01) != 0)
                {
                    digits = StatsCommand.toByteArray(intBuffer, i+1);
                    out.write(intBuffer, intBuffer.length - digits, digits);
                    out.write(':');
                    
                    out.write(' ');
                    
                    //Print out IP info...
                    int extra = 0;
                    for (int j=0; j<4; j++)
                    {
                        digits = StatsCommand.toByteArray(intBuffer,ac[offset+ARP_IP_ADDR_OFFS+j]&0x0FF);
                        out.write(intBuffer, intBuffer.length - digits, digits);
                        extra += 3 - digits;
                        if (j != 3)
                            out.write('.');
                    }
                    for (int j=0; j<extra; j++)
                        out.write(' ');
                    out.write(SPACER);
                    
                    //Print out ethernet info...
                    for (int j=0; j<6; j++)
                    {
                        byteHexDump(out, intBuffer, ac[offset+ARP_ETH_ADDR_OFFS+j]);
                        if (j != 5)
                            out.write(':');
                    }
                    out.write(SPACER);
                    
                    //Print out Time to live...
                    digits = StatsCommand.toByteArray(intBuffer, ac[offset+ARP_TTL_OFFS] & 0x0FF);
                    out.write(intBuffer, intBuffer.length - digits, digits);
                    for (int j=digits; j<3; j++)
                        out.write(' ');
                    out.write(SPACER);
                    
                    //Print out flags info
                    if ((ac[offset] & 0x02) != 0)
                        out.write("Pending ".getBytes());
                    if ((ac[offset] & 0x04) != 0)
                        out.write("Static".getBytes());
                        
                    out.write("\r\n".getBytes());
                }
            }
        }
    }
    
    /**
     *
     */
    private void byteHexDump(PrintStream out, byte[] intBytes, byte num) throws IOException
    {
        intBytes[0] = (byte) ((num & 0xF0) >>> 4);
        intBytes[0] = (byte) (intBytes[0] > 9 ? intBytes[0] + 55
                            : intBytes[0] + 48);
        intBytes[1] = (byte) (num & 0x0F);
        intBytes[1] = (byte) (intBytes[1] > 9 ? intBytes[1] + 55
                            : intBytes[1] + 48);

        out.write(intBytes,0,2);
    }
}