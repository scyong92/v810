/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation.
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: IpCommand.java
*/
package com.dalsemi.slush.command;

import java.io.*;
import java.util.*;
import com.dalsemi.slush.*;
import com.dalsemi.tininet.*;
import com.dalsemi.system.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.shell.server.serial.*;

/**
 * Configure or display the network settings.
 *
 *
 * @author Lorne Smith, Stephen Hess
 * @version 1.0
 */
public class IpCommand implements SlushCommand
{
    public static String IPCONFIG_FILE = "/etc/.ipconfig";

    boolean useDHCP = false;
    boolean releaseDHCPIP = false;
    String ipAddress = null;
    String gateway   = null;
    String mask      = null;
    boolean critical = false;
    String startTelnet;
    String startFTP;




    static final byte[] HASH_ERR_MSG1 = "It is likely that your password hash in the file\r\n".getBytes();
    static final byte[] HASH_ERR_MSG2 = (" is corrupted.  You can continue the\r\noperation and risk"+
               " the security of your TINI on a recovery\r\n(the system will use the default root:tini"+
               "account) or you can\r\ncancel this command and try to commit network state again later.\r\n"+
               "Continue with the operation? (y,n) ").getBytes();
    static final byte[] DHCP_RUNNING_ERR_MSG = ("DHCP is currently running.\r\n"+
               "To perform this operation, you must release your IP with\r\n"+
               "            ipconfig -r\r\n").getBytes();

    static final byte[] DHCP_COMMIT_ERR_MSG = "You must wait until DHCP has leased an IP before you can commit.\r\n".getBytes();



    /**
     * Returns a string describing the usage of the command and its arguments if any.
     *
     *
     * @return The usage statement for this command.
     */
    public String getUsageString()
    {
        return "ipconfig [options]\r\n\r\nConfigure or display the network settings." +
               "\r\n [-a xx.xx.xx.xx]    Set IP address.  Must be used with the -m option." +
               "\r\n [-n domainname]     Set domain name" +
               "\r\n [-m xx.xx.xx.xx]    Set subnet mask.  Must be used with -a option." +
               "\r\n [-g xx.xx.xx.xx]    Set gateway address" +
               "\r\n [-p xx.xx.xx.xx]    Set primary DNS address" +
               "\r\n [-s xx.xx.xx.xx]    Set secondary DNS address" +
               "\r\n [-t dnstimeout ]    Set DNS timeout (set to 0 for backoff/retry)" +
               "\r\n [-d]                Use DHCP to lease an IP address" +
               "\r\n [-r]                Release currently held DHCP IP address" +
               "\r\n [-x]                Show all Interface data" +
               "\r\n [-h xx.xx.xx.xx]    Set mailhost" +
               "\r\n [-C]                Commit current network configuration to flash"+
               "\r\n [-D]                Disable restoration of configuration from flash"+
               "\r\n [-f]                Don't prompt for confirmation";
    }

    /**
     * Method called when this command is executed from the command prompt.
     *
     *
     * @param  in   The <code>InputStream</code> this command will use to receive input.
     * @param  out  The <code>PrintStream</code> should use to report any non-critical messages.
     * @param  err  The <code>PrintStream</code> should use to report any critical messages.
     * @param  args The command line arguments passed to this command.
     * @param  env  The current environment variables for this session.
     * @throws Exception
     */
    public void execute(SystemInputStream in, SystemPrintStream out, SystemPrintStream err,
                        String[] args, Hashtable env) throws Exception
    {
        if (args.length == 0)
        {
            StringBuffer buff = new StringBuffer();
            buff.append("\r\nHostname          : ");
            buff.append(TININet.getHostname());
            buff.append(".");
            buff.append(TININet.getDomainname());
            buff.append("\r\nCurrent IP        : ");
            buff.append(TININet.getIPAddress("eth0"));

            buff.append("\r\nDefault Gateway   : ");
            buff.append(TININet.getGatewayIP("eth0"));
            buff.append("\r\nSubnet Mask       : ");
            buff.append(TININet.getSubnetMask("eth0"));

            buff.append("\r\nEthernet Address  : ");
            buff.append(TININet.getEthernetAddress());

            buff.append("\r\nPrimary DNS       : ");
            buff.append(TININet.getPrimaryDNS());
            buff.append("\r\nSecondary DNS     : ");
            buff.append(TININet.getSecondaryDNS());
            buff.append("\r\nDNS Timeout       : ");
            buff.append(TININet.getDNSTimeout());
            buff.append(" (ms)");

            buff.append("\r\nDHCP Server       : ");
            buff.append(TININet.getDHCPServerIP());
            buff.append("\r\nDHCP Enabled      : ");
            buff.append(((Slush)TINIOS.getShell()).isDHCPRunning());

            buff.append("\r\nMailhost          : ");
            buff.append(TININet.getMailhost());

            buff.append("\r\nRestore From Flash: ");

            switch(TININet.getNetworkCommitState())
            {
              case TININet.UNCOMMITTED:
                buff.append("Not Committed");
              break;

              case TININet.COMMITTED:
                buff.append("Enabled");
                buff.append("\r\nRestored last boot: ");
                buff.append(((TINIOS.getBootState()&1) == 1));
              break;

              case TININet.RESTORE_DISABLED:
                buff.append("Disabled");
              break;
            }

            out.println(buff.toString());

            return;
        }
        else if ((args.length == 1) && (args[0].equals("-x")))
        {
            byte[] data = new byte[50];

            StringBuffer buff = new StringBuffer();
            for (int i=0; i< 4; i++)
            {
                boolean active = TININet.getInterfaceInfo(i, data);

                buff.append("Interface " + i + " is " + (active ? "" : "not ") + "active.");
                if (active)
                {
                    buff.append("\r\nName         : " + (new String(data, 15, data[14])) + ((data[0] & 0x4) != 0 ? " (default)" : ""));
                    buff.append("\r\nType         : ");

                    if (data[1] == 1)
                            buff.append("Ethernet");
                    else if (data[1] == 2)
                            buff.append("Point-to-Point Protocol");
                    else if (data[1] == 3)
                            buff.append("Local Loopback");

                    buff.append("\r\nIP Address   : ");
                    for (int j=2; j<6; j++)
                    {
                        buff.append(Integer.toString(data[j]&0x0FF));
                        if (j != 5)
                            buff.append('.');
                    }
                    buff.append("\r\nSubnet Mask  : ");
                    for (int j=6; j<10; j++)
                    {
                        buff.append(Integer.toString(data[j]&0x0FF));
                        if (j != 9)
                            buff.append('.');
                    }
                    buff.append("\r\nGateway      : ");
                    for (int j=10; j<14; j++)
                    {
                        buff.append(Integer.toString(data[j]&0x0FF));
                        if (j != 13)
                            buff.append('.');
                    }
                }
                if (i != 3)
                    buff.append("\r\n\r\n");
            }

            out.println(buff.toString());

            return;
        }

        if(!TINIOS.isCurrentUserAdmin())
        {
            throw new Exception(Slush.PERM_DENIED);
        }

        synchronized(this)
        {
            boolean force = false;
            useDHCP       = false;
            releaseDHCPIP = false;
            ipAddress     = null;
            gateway       = null;
            mask          = null;
            critical      = false;
            boolean doCommit = false;

            Slush slush = (Slush)TINIOS.getShell();
            boolean dhcpIsRunning = (TININet.getDHCPServerIP().length()!=0);
            boolean dhcpError = false;

            int opt;

            GetOpt myopts = new GetOpt(args,"!fdrCDh:n:a:m:g:p:s:t:");

            while ((opt = myopts.getopt()) != myopts.optEOF)
            {
                switch (opt)
                {
                    case 'a':
                        ipAddress = myopts.optArgGet();
                        critical = true;
                        dhcpError = true;
                    break;

                    case 'h':
                        if (!TININet.setMailhost(myopts.optArgGet()))
                            out.println("Unable to set mailhost.");
                    break;

                    case 'n':
                        TININet.setDomainname(myopts.optArgGet());
                    break;

                    case 'm':
                        mask = myopts.optArgGet();
                        critical = true;
                        dhcpError = true;
                    break;

                    case 'g':
                        gateway = myopts.optArgGet();
                        critical = true;
                    break;

                    case 'p':
                        TININet.setPrimaryDNS(myopts.optArgGet());
                    break;

                    case 's':
                        TININet.setSecondaryDNS(myopts.optArgGet());
                    break;

                    case 't':
                        try
                        {
                            int timeout = Integer.parseInt(myopts.optArgGet());
                            TININet.setDNSTimeout(timeout);
                        }
                        catch (NumberFormatException e)
                        {
                            out.println("Error: Invalid DNS Timeout");
                        }
                    break;

                    case 'd':
                        useDHCP = true;
                        critical = true;
                    break;

                    case 'r':
                        releaseDHCPIP = true;
                        critical = true;
                    break;

                    case 'f':
                        force = true;
                    break;

                    case 'C':
                      doCommit = true;
                    break;

                    case 'D':
                      TININet.disableNetworkRestore();
                    break;

                    default:
                        err.println(getUsageString());
                    return;
                }
            }

            if (useDHCP && doCommit)
            {
                out.write(DHCP_COMMIT_ERR_MSG);
                return;
            }

            if (dhcpError && dhcpIsRunning)
            {
                //dont let anyone change IP or subnet mask when running DHCP
                out.write(DHCP_RUNNING_ERR_MSG);
                return;
            }


            if (doCommit)
            {

                if (slush.isDHCPRunning() && (!dhcpIsRunning))
                {
                    out.write(DHCP_COMMIT_ERR_MSG);
                    return;
                }



                //need to read for the root password and make it part of the committed state
                try
                {
                    File f = new File(slush.PASSWORD_FILE);
                    //if the file doesn't exist we'll get an exception,
                    //which is OK because then we'll end up using the default
                    //so basically, if you're boneheaded enough to delete your
                    //password file then you compromise your own security
                    BufferedReader fin = new BufferedReader(new FileReader(f));
                    //boolean rootHashFound = false;
                    while (true)//(!rootHashFound)
                    {
                        String thisline = fin.readLine();
                        if (thisline.startsWith("root"))
                        {
                            //this line looks like:
                            //   root:12jh2938oaso98r23...
                            //we want 40 characters after the COLON
                            char[] hash_chars = new char[40];
                            thisline.getChars(5,45,hash_chars,0);

                            byte[] hash_bytes = new byte[20];
                            int temp;
                            for (int k=0;k<20;k++)
                            {
                                temp = (int)hash_chars[k<<1];
                                if (temp > 0x39) //must be an alpha character
                                    temp = temp - 'a' + 10;
                                else
                                    temp = temp - 0x30;  //it is a digit character

                                hash_bytes[k] = (byte)(temp << 4);

                                temp = (int)hash_chars[(k<<1) + 1];
                                if (temp > 0x39) //must be an alpha character
                                    temp = temp - 'a' + 10;
                                else
                                    temp = temp - 0x30;  //it is a digit character

                                hash_bytes[k] = (byte)(hash_bytes[k] | (temp & 0x0f));
                            }
                            TINIOS.setRecoveryHash(hash_bytes);
                            //rootHashFound = true;
                            break;
                        }
                    }

                }
                catch(Exception e)
                {
                    //if we had a problem getting the password...
                    out.println("Could not store the hash of the root password for network recovery.\r\n");
                    try
                    {
                        //lets find out if the user wants to continue and let the default passwords
                        //be used on recovery, or if they want to bail, fix the problem, and try
                        //to commit later--this is vital if the only connection is a telnet connection,
                        //because once it is committed, your only real options are to blow the whole committal
                        //away or to reload bank 7, only possible in JavaKit

                        out.write(HASH_ERR_MSG1);
                        out.print(slush.PASSWORD_FILE);
                        out.write(HASH_ERR_MSG2);


                        in.setRawMode(true);
                        char response = (char) (in.read() & 0x0FF);
                        in.setRawMode(false);
                        out.println();
                        if ((response != 'Y') && (response != 'y'))
                        {
                            out.println("\r\nNetwork configuration commit aborted.");
                            return;
                        }
                    }
                    catch(Exception another_e)
                    {
                        //at this point things have gone horribly wrong, lets just abort the command
                        out.println("Network commit abort: "+another_e.toString());
                        return;
                    }

                }
            }

            if (!critical)
            {
              if(doCommit)
              {
                try
                {
                  TININet.commitNetworkState();
                  out.println("Network configuration committed to flash memory");
                }
                catch(CommitException ce)
                {
                  out.println(ce);
                }
              }

              return;
            }

            //Warn the user about the consequences
            if (!force)
            {
                out.println("Warning:  This will disconnect any connected network users\r\nand reset all network servers.");
                out.print("\r\nOK to proceed? (Y/N): ");

                char    response;

                in.setRawMode(true);
                response = (char) (in.read() & 0x0FF);
                in.setRawMode(false);

                if ((response != 'Y') && (response != 'y'))
                {
                    out.println("Command Aborted.");
                    return;
                }
            }

            //Set the mask and gateway only if both were specified.
            if ((mask != null) || (ipAddress != null))
            {
                if ( (mask == null) || (ipAddress == null) )
                {
                    throw new Exception("\r\nERROR:  The -a and the -m options must be used together.");
                }
            }

            if (useDHCP)
            {
                if (slush.isDHCPRunning())
                {
                    err.println("DHCP is already running.  Use 'ipconfig -r' to stop DHCP first.");
                    return;
                }
            }

            startTelnet = (String)env.get("TelnetServer");
            startFTP = (String)env.get("FTPServer");

            if (Thread.currentThread() instanceof SerialSession)
            {
                //From a serial prompt, we don't need to worry about
                //the session being killed before the changes are made.
                updateIPInfo(slush);
            }
            else
            {
                //From a Telnet prompt, we need to spin off a thread to
                //do the work.  If we didn't, the servers would be disconnected,
                //and this thread would die before we had a chance to change the
                //IP and bring back up the servers.
                Thread ipUpdater = new Thread()
                     {
                        public void run()
                        {
                            Slush mySlush = (Slush)TINIOS.getShell();
                            try
                            {
                                updateIPInfo(mySlush);
                            }
                            catch(Exception e)
                            {
                                //DEBUG ==> What do I do in an error case?
                                com.dalsemi.system.Debug.debugDump("Exception in ipcfg thread: " + e); //DEBUG
                            }
                            finally
                            {
                                mySlush.unregisterThread(this);
                            }
                        }
                     };

                     slush.registerThread(ipUpdater);
                     ipUpdater.start();
            }

            if(doCommit)
            {
              try
              {
                TININet.commitNetworkState();
                out.println("Network configuration committed to flash memory");
              }
              catch(CommitException ce)
              {
                out.println(ce);
              }
            }
        }
    }

    private void updateIPInfo(Slush slush) throws Exception
    {
        // Disconnect all TCP clients;
        slush.shutDownFTPServer();
        slush.shutDownTelnetServer();

        //Set mask and gateway if needed
        if (gateway != null)
        {
            TININet.setGatewayIP("eth0", gateway);
        }

        //Set IPAdress or DHCP if needed
        if (ipAddress != null)
        {
            TININet.setIPAddress("eth0", ipAddress);
            TININet.setSubnetMask("eth0", mask);
        }
        else if (useDHCP)
        {
            slush.initiateDHCP();
            return;
        }
        else if (releaseDHCPIP)
        {
            slush.stopDHCP();
            //Clear out any fields that could have been
            //set from DHCP.
            TININet.setDHCPServerIP("");
            TININet.setIPAddress("eth0", "");
            TININet.setPrimaryDNS("");
            TININet.setSecondaryDNS("");
            TININet.setGatewayIP("eth0", "");
            TININet.setSubnetMask("eth0", "");
            TININet.setMailhost("");
            TININet.setDomainname("");
            return;
        }

        //Now start back up the appropriate servers...
        if ((startTelnet != null) && (startTelnet.equals("enable")) )
            slush.startupTelnetServer();
        if ((startFTP != null) && (startFTP.equals("enable")) )
            slush.startupFTPServer();

        ipAddress   = null;
        gateway     = null;
        mask        = null;
        startTelnet = null;
        startFTP    = null;
    }
}