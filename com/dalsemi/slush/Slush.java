/****************************************************************************
*
*  Copyright (C) 1999,2000 Dallas Semiconductor Corporation.
*  All rights Reserved. Printed in U.S.A.
*  This software is protected by copyright laws of
*  the United States and of foreign countries.
*  This material may also be protected by patent laws of the United States
*  and of foreign countries.
*  This software is furnished under a license agreement and/or a
*  nondisclosure agreement and may only be used or copied in accordance
*  with the terms of those agreements.
*  The mere transfer of this software does not imply any licenses
*  of trade secrets, proprietary technology, copyrights, patents,
*  trademarks, maskwork rights, or any other form of intellectual
*  property whatsoever. Dallas Semiconductor retains all ownership rights.
*
*     Module Name: Slush.java
*/
package com.dalsemi.slush;

import com.dalsemi.system.*;
import com.dalsemi.shell.*;
import com.dalsemi.slush.command.*;
import com.dalsemi.shell.server.*;
import com.dalsemi.shell.server.telnet.*;
import com.dalsemi.shell.server.ftp.*;
import com.dalsemi.shell.server.serial.*;
import com.dalsemi.comm.*;
import java.util.*;
import java.io.*;
import com.dalsemi.fs.*;
import com.dalsemi.tininet.*;
import com.dalsemi.tininet.dhcp.*;
import com.dalsemi.fs.*;
import javax.comm.*;


/**
 * Slush system shell.  Provides an interface between the underlying
 * OS and multiple user login sessions.  Also performs many common
 * operating system tasks, such as providing a way to view directory
 * listings, change directories, run programs, etc.
 *
 * @author Stephen Hess, Lorne Smith
 * @version 1.02
 */
public class Slush extends TINIShell implements CommPortOwnershipListener
{
    public static long         bootTime           = System.currentTimeMillis();

    public static       String       STARTUP_FILE       = "/etc/.startup";

    private static      boolean      bypass             = false;

    private             FTPServer    ftpServer          = null;
    private             TelnetServer telnetServer       = null;
    private             SerialServer serialServer       = null;

    private static      Hashtable    userNamesByUID;

    public static final Object       passwdLock         = new Object();

    public static       String       SLUSH_VERSION      = "Version 1.02b";
    public static       String       SLUSH_NAME         = "slush";

    public static       String       PASSWORD_FILE      = "/etc/passwd";
    public static       String       LOG_FILE           = "/etc/.log";

    private static      Hashtable    userIDByThreadID   = new Hashtable(8, 8);

    private static      int          SLUSH_ID           = TINIOS.getTaskID();

    private static      DHCPClient   dhcpClient         = null;

    private static      PrintStream  logOut             = null;

    private static      boolean      bootInProgress;

    private             byte[]       pass               = new byte[20];

    /************************************************************\
     If you need to run slush on a different port, or change the
     serial port speed to 19200, change these next two variables.
    \************************************************************/
    private             String       SERIAL_PORT      = "serial0";
    private             int          SERIAL_SPEED     = 115200;
    private             int          SERIAL_DATABITS  = SerialPort.DATABITS_8;
    private             int          SERIAL_STOPBITS  = SerialPort.STOPBITS_1;
    private             int          SERIAL_PARITY    = SerialPort.PARITY_NONE;

    //Some common strings...
    public static       String       PERM_DENIED        = "Permission denied.";
    public static       String       EMPTY              = "";

    /* tells us if this boot up is a recovery */
    private boolean recovery = false;

    boolean commPortListenerAdded = false;

    /**
     * Constructor.  Initialize and bring up the system.
     */
    public Slush()
    {
        super();

        bootInProgress = true;
        TINIOS.setShell(this);

        //Stop non-admin users from being able to kill slush
        //or the GC.
        TINIOS.lockInitProcesses();

        boolean consoleActive = TINIOS.isConsoleOutputEnabled();
        if (!SERIAL_PORT.equals("serial0"))
        {
            if (SERIAL_PORT.equals("serial1"))
                TINIOS.enableSerialPort1(true);
            else if (SERIAL_PORT.equals("serial2"))
                TINIOS.setExternalSerialPortEnable(2,true);
            else if (SERIAL_PORT.equals("serial3"))
                TINIOS.setExternalSerialPortEnable(3,true);
        }
        if (consoleActive)
        {
            com.dalsemi.system.Debug.setDefaultStreams(SERIAL_PORT, SERIAL_SPEED);
        }

        sendOutput("\r\n\r\n[-=        slush " + SLUSH_VERSION + "        =-]\r\n");
        sendOutput("[          System coming up.            ]\r\n");
        sendOutput("[      Beginning initialization...      ]\r\n");

        try
        {
            // Change the OS name as we're running SLUSH now.
            (System.getProperties()).put("os.name","slush");

            initializeLogFile();

            initializeShellCommands();

            initializeFileSystem();

            parseStartupFile(consoleActive);

            if (logOut != null)
            {
                logOut.close();
                logOut = null;
            }

            sendOutput("\r\n[    slush initialization complete.     ]\r\n");

            String startSerial = (String)environment.get("SerialServer");

            com.dalsemi.system.Debug.resetDefaultStreams();

            System.gc();

            if (bypass || ((startSerial != null) && (startSerial.equals("enable"))) )
            {
                sendOutput("\r\n[      Bringing up Serial Server...     ]");
                startupSerialServer();
            }
            else
            {
                sendOutput("\r\n[    NOT bringing up Serial Server...   ]\r\n");
            }

            if (  (TININet.getNetworkCommitState()==TININet.COMMITTED) &&
                  ((TINIOS.getBootState()&1) == 1) )
                  recovery = true;

            bootInProgress = false;
        }
        catch (Throwable t)
        {
            System.out.println("Exception in Slush.class: " + t.toString());
            System.out.println("Message: " + t.getMessage());

            if (logOut != null)
                logOut.close();
        }
    }

    /**
     * Display information to the console and a log file if it exists.
     */
    private static void sendOutput(String printThis)
    {
        System.out.print(printThis);
        if (logOut != null)
            logOut.print(printThis);
    }

    /**
     * Parse the .startup file, executing each line as a slush command.
     */
    private void parseStartupFile(boolean consoleActive) throws Exception
    {
        //Now, parse the startup file and execute any commands
        File startupFile = new File(STARTUP_FILE);
        if (startupFile.exists())
        {
            //If the user hit '5' before this point, skip the startup file, but still bring up
            //the serial server.
            if (consoleActive)
            {
                InputStream system_root = ((SystemInputStream)System.in).getRootStream();

                //bad Steven! System.in.available() isn't supposed to report anything without
                // and end of line!!!
                if (system_root.available() > 0)
                {
                    //((SystemInputStream)System.in).setRawMode(true);
                    if (system_root.read() == '5') //(System.in.read() == '5')
                    {
                        sendOutput("[       Bypassing .startup file.        ]    [Skip]\r\n");
                        bypass = true;
                    }
                    //((SystemInputStream)System.in).setRawMode(false);
                }
            }

            if (!bypass)
            {
                sendOutput("\r\n[ Initializing and parsing .startup...  ]\r\n");
                DataInputStream startupIn = new DataInputStream(new BufferedInputStream(new FileInputStream(startupFile)));
                String command = null;
                SystemPrintStream outStream = new SystemPrintStream(new NullOutputStream());
                SystemInputStream inStream = new SystemInputStream(new NullInputStream(), null);
                String BACKGROUND = " &";
                try
                {
                    while ((command = startupIn.readLine()) != null)
                    {
                        command = command.trim();
                        if (command.startsWith("#"))
                            continue;

                        int hashPos = 0;
                        if ( (hashPos = command.indexOf('#')) != -1)
                        {
                            command = command.substring(0, hashPos);
                            command = command.trim();
                        }

                        if (command.length()==0)  //empty line
                            continue;

                        if (command.equals("initializeNetwork"))
                        {
                            initializeNetwork();
                            continue;
                        }

                        if (command.indexOf(BACKGROUND) == -1)
                        {
                            command += BACKGROUND;
                        }

                        if (logOut != null)
                            logOut.println("  Running: " + command);

                        CommandInterpreter.execute(Session.getParams(command), inStream, outStream,
                                                   outStream, environment);
                    }
                    sendOutput("[         System init routines          ]    [Done]\r\n");
                }
                catch(Throwable t)
                {
                    sendOutput("Exception processing startup file: " + t.toString());
                }
                if (logOut != null)
                    logOut.println("<*>Startup file processing complete<*>");
                startupIn.close();
            }
        }
        else
        {
            sendOutput("\r\n[        No startup file found.         ]    [Skip]\r\n");
        }
    }

    /**
     * Initialize the file to which to log verbose output during the boot
     * process.
     */
    private static void initializeLogFile()
    {
        //Write to the log file only if it already exists.
        try
        {
            DSFile logFile = new DSFile(LOG_FILE);
            if (logFile.exists())
            {
                sendOutput("[          Generating log file.         ]    [Info]\r\n");
                FileOutputStream logStream = new FileOutputStream(logFile);
                logOut = new PrintStream(new BufferedOutputStream(logStream));
            }
            else
            {
                sendOutput("[        Not generating log file.       ]    [Info]\r\n");
                logOut = new PrintStream(new com.dalsemi.comm.NullOutputStream());
            }
        }
        catch(Exception e)
        {
            logOut = null;
            sendOutput("\r\nError occurred creating log file:" + e.toString());
        }
    }

    /**
     * Initializes the filesystem for first time slush use.  Creates default
     * passwd and .startup files.
     */
    private static void initializeFileSystem() throws Exception
    {
        sendOutput("\r\n[        Checking system files...       ]");

        DSFile passwd = new DSFile(PASSWORD_FILE);
        if (!passwd.exists())
        {
            DSFile etc = new DSFile("/etc");
            if (!etc.exists())
            {
                etc.mkdir();
                etc.setOtherPermissions(0x05);
            }
            //Create the default passwd file
            FileOutputStream passwdStream = new FileOutputStream(passwd);
            PrintStream passwdOut = new PrintStream(new BufferedOutputStream(passwdStream));

            //KLA - first we need to see if the recovery password was stored in Tom's
            //nifty scrog-safe network memory
            byte[] hash = new byte[20];
            if (TINIOS.getRecoveryHash(hash))
            {
                //if we have recovered an old password
                char[] hex = "0123456789abcdef".toCharArray();
                StringBuffer sb = new StringBuffer();
                for (int k=0;k<20;k++)
                {
                    sb.append(hex[(hash[k] >> 4) & 0x0f]);
                    sb.append(hex[hash[k] & 0x0f]);
                }
                passwdOut.println("root:"+sb.toString()+":128");
            }
            else
            {
                //did not recover an old password, go for the default
                //new passwords to handle name+:+password security fix
                passwdOut.println("root:f8491b67e91f837c13c3444965281bcee5fca964:128");
                passwdOut.println("guest:1bb6e3a2abc20f654fe62fd139790c06394885d3:0");

                //following new passwords handle the SHA scrog (inverted word byte-order)
                //passwdOut.println("root:671b49f87c831fe94944c313ce1b286564a9fce5:128");
                //passwdOut.println("guest:a2e3b61b650fc2abd12fe64f060c7939d3854839:0");

            }

            passwdOut.close();
            passwd.setOtherPermissions(0x04);
        }

        DSFile startup = new DSFile(STARTUP_FILE);
        if (!startup.exists())
        {
            //Create the default .startup file
            //FileOutputStream startupStream = new FileOutputStream(startup);
            //PrintStream startupOut = new PrintStream(new BufferedOutputStream(startupStream));
            PrintStream startupOut = new PrintStream(new BufferedOutputStream(new FileOutputStream(startup)));
            startupOut.println("########");
            startupOut.println("#Autogen'd slush startup file");
            startupOut.println("setenv FTPServer enable");
            startupOut.println("setenv TelnetServer enable");
            startupOut.println("setenv SerialServer enable");
            startupOut.println("##");
            startupOut.println("#Add user calls to setenv here:");
            startupOut.println("##");
            startupOut.println("initializeNetwork");
            startupOut.println("########");
            startupOut.println("#Add other user additions here:");
            startupOut.close();
            startup.setOtherPermissions(0);
        }

        sendOutput("    [Done]\r\n");
    }

    /**
     * Forces the system to initialize network settings.
     * If network information is set, this function will
     * bring up the network servers.
     */
    private void initializeNetwork() throws Exception
    {
        sendOutput("[        Initializing network...        ]\r\n");

        TININet.update();

        if (!(TININet.getDHCPServerIP().length() == 0))
        {
            sendOutput("[        Starting DHCP client...        ]\r\n");
            initiateDHCP();
        }
        else
            startupNetworkServers();

        sendOutput("[         Network configuration         ]    [Done]\r\n");
    }

    /**
     * Initialize each command and add to list of known commands.
     * Also sets up aliases for commands as needed.
     */
    private void initializeShellCommands()
    {
        sendOutput("[    Initializing shell commands...     ]");

        CommandInterpreter.addCommand("arp", new ARPCommand());

        CommandInterpreter.addCommand("chown", new ChownCommand());

        CommandInterpreter.addCommand("cat", new CatCommand());

        CommandInterpreter.addCommand("cd", new CdCommand());

        SlushCommand    copy = new CopyCommand();
        CommandInterpreter.addCommand("copy", copy);
        CommandInterpreter.addCommand("cp", copy);

        SlushCommand    del = new DelCommand();
        CommandInterpreter.addCommand("del", del);
        CommandInterpreter.addCommand("rm", del);

        CommandInterpreter.addCommand("help", new HelpCommand());

        CommandInterpreter.addCommand("ipconfig", new IpCommand());

        SlushCommand    ls = new LsCommand();
        CommandInterpreter.addCommand("ls", ls);
        CommandInterpreter.addCommand("dir", ls);

        SlushCommand    md = new MdCommand();
        CommandInterpreter.addCommand("md", md);
        CommandInterpreter.addCommand("mkdir", md);

        SlushCommand    move = new MoveCommand();
        CommandInterpreter.addCommand("move", move);
        CommandInterpreter.addCommand("mv", move);

        CommandInterpreter.addCommand("pwd", new PwdCommand());

        SlushCommand    rd = new RdCommand();
        CommandInterpreter.addCommand("rd", rd);
        CommandInterpreter.addCommand("rmdir", rd);

        CommandInterpreter.addCommand("ps", new PsCommand());

        CommandInterpreter.addCommand("kill", new KillCommand());

        CommandInterpreter.addCommand("df", new FreeRamCommand());

        CommandInterpreter.addCommand("gc", new GcCommand());

        CommandInterpreter.addCommand("java", new JavaCommand());

        CommandInterpreter.addCommand("ping", new PingCommand());

        CommandInterpreter.addCommand("sendmail", new SendMailCommand());

        CommandInterpreter.addCommand("append", new AppendCommand());

        CommandInterpreter.addCommand("reboot", new RebootCommand());

        SlushCommand    downserver = new ShutDownServersCommand();
        CommandInterpreter.addCommand("downserver", downserver);
        CommandInterpreter.addCommand("stopserver", downserver);

        CommandInterpreter.addCommand("startserver", new StartUpServersCommand());

        CommandInterpreter.addCommand("stats", new StatsCommand());

        CommandInterpreter.addCommand("date", new DateCommand());

        CommandInterpreter.addCommand("chmod", new ChmodCommand());

        CommandInterpreter.addCommand("useradd", new AddUserCommand());

        CommandInterpreter.addCommand("userdel", new RemoveUserCommand());

        CommandInterpreter.addCommand("passwd", new PasswdCommand());

        CommandInterpreter.addCommand("hostname", new HostnameCommand());

        CommandInterpreter.addCommand("nslookup", new NslookupCommand());

        CommandInterpreter.addCommand("setenv", new SetenvCommand());

        CommandInterpreter.addCommand("who", new WhoCommand());

        CommandInterpreter.addCommand("whoami", new WhoamiCommand());

        CommandInterpreter.addCommand("su", new SuCommand());

        CommandInterpreter.addCommand("source", new SourceCommand());

        CommandInterpreter.addCommand("wall", new WallCommand());

        CommandInterpreter.addCommand("history", new HistoryCommand());

        CommandInterpreter.addCommand("genlog", new GenlogCommand());

        CommandInterpreter.addCommand("clear", new ClearCommand());

        CommandInterpreter.addCommand("touch", new TouchCommand());

        CommandInterpreter.addCommand("echo", new EchoCommand());

        CommandInterpreter.addCommand("wd", new WdCommand());

        CommandInterpreter.addCommand("ftp", new FTPCommand());
        //CommandInterpreter.addCommand("telnet", new TelnetCommand());
        //CommandInterpreter.addCommand("threadstat", new ThreadStatCommand());

        //Don said I could!!!!
        CommandInterpreter.addCommand("netstat", new NetStatCommand());

        sendOutput("    [Done]\r\n");
    }

    /**
     * Executes a command in the shell.
     *
     * @param commandLine  An Object array containing the command in the first element,
     * followed by any parameters need for that command in a String[] in the second element.
     * @param in  The stream the command will use to get input.
     * @param out  The stream used to report non-critical messages.
     * @param err  The stream used to report critical messages.
     * @param env  A table of environment variables.
     *
     * @throws Exception Any exception raised by the command.
     */
    public void execute(Object[] commandLine, SystemInputStream in, SystemPrintStream out,
                        SystemPrintStream err, Hashtable env) throws Exception
    {
        CommandInterpreter.execute(commandLine, in, out, err, env);
    }

    /**
     * Returns the name of the shell.
     *
     * @return  The name of the shell.
     */
    public String getName()
    {
        return SLUSH_NAME;
    }

    /**
     * Returns the version of the shell.
     *
     * @return   The shell version.
     */
    public String getVersion()
    {
        return SLUSH_VERSION;
    }

    /**
     * Gets the value of the given key from the currently executing
     * Session's environment.
     *
     * @param  key   The key to lookup in the current environment.
     *
     * @return  The value of the key, or <code>null</code> if the key
     * is not set in the current environment.
     */
    public String getFromCurrentEnvironment(String key)
    {
        Object session = Thread.currentThread();
        if (session instanceof Session)
        {
            return ((Session)session).getFromEnvironment(key);
        }
        else
        {
            return (String)environment.get(key);
        }
    }

    /**
     * Returns a copy of the currently executing Session's environment.
     *
     * @return the environment.
     */
    public Hashtable getCurrentEnvironment()
    {
        if (bootInProgress)
            return environment;

        Object session = Thread.currentThread();
        if (session instanceof Session)
            return ((Session)session).getEnvironment();
        else
            return (Hashtable)environment.clone();
    }

    /**
     * Returns the user ID of the current user.
     *
     * @return  the current user's ID.
     */
    public byte getCurrentUID()
    {
        if (bootInProgress)
            return adminUID;

        int taskID = TINIOS.getTaskID();
        //If the executing process is slush, then the UID is in the hashtable
        //keyed by ThreadID's.  Else, the UID lives in the TINIOS block.
        if (taskID == SLUSH_ID)
        {
            Byte uid = (Byte)(userIDByThreadID.get(Thread.currentThread()));
            if (uid == null)
            {
                //Don't know ==> return guest priv's
                return 0;
            }
            else
            {
                return ((Byte)uid).byteValue();
            }
        }
        else
        {
            return (byte)(TINIOS.getOwnerIDByTaskID(taskID));
        }
    }

    /**
     * Bring down the FTP server.
     * You must be an admin to call this function.
     *
     * @return <code>true</code> if the FTP server was
     * running and was shutdown correctly.  <code>false</code>
     * if the server wasn't running, or if current user isn't
     * an admin.
     */
    public void shutDownFTPServer()
    {
        if (!TINIOS.isCurrentUserAdmin())
        {
            return;
        }
        try
        {
            if (ftpServer != null)
            {
                ftpServer.shutDown();

                ftpServer = null;

                //broadcast("\r\nMessage from System: FTP server stopped.");
                systemBroadcast("FTP server stopped.");
            }
        }
        catch (java.io.IOException ioe)
        {
            com.dalsemi.system.Debug.debugDump("IOException shutting down FTP server!");
//            broadcast("\r\nMessage from System: IOException occurred shutting down FTP server.");
              systemBroadcast("IOException occurred shutting down FTP server.");
        }
    }

    /**
     * Bring down the Telnet server.
     * You must be an admin to call this function.
     *
     * @return <code>true</code> if the Telnet server was
     * running and was shutdown correctly.  <code>false</code>
     * if the server wasn't running, or if current user isn't
     * an admin.
     */
    public void shutDownTelnetServer()
    {
        if (!TINIOS.isCurrentUserAdmin())
        {
            return;
        }
        try
        {
            if (telnetServer != null)
            {
                telnetServer.shutDown();

                telnetServer = null;

//                broadcast("\r\nMessage from System: Telnet server stopped.");
                systemBroadcast("Telnet server stopped.");
            }
        }
        catch (java.io.IOException ioe)
        {
            com.dalsemi.system.Debug.debugDump("IOException shutting down Telnet server!");
//            broadcast("\r\nMessage from System: IOException occurred shutting down Telnet server.");
            systemBroadcast("IOException occurred shutting down Telnet server.");
        }
    }

    /**
     * Initialize and start the FTP server.
     * You must be an admin to call this function.
     */
    public void startupFTPServer()
    {
        if (!TINIOS.isCurrentUserAdmin())
            return;

        if(ftpServer == null)
        {
            sendOutput("[       Starting up FTP server...       ]");

            try
            {
                ftpServer = new FTPServer();
                ftpServer.start();

                sendOutput("    [Done]\r\n");

//                broadcast("\r\nMessage from System: FTP server started.");
                systemBroadcast("FTP server started.");
            }
            catch(IOException ioe)
            {
                sendOutput("    [FAIL]: " + ioe + "\r\n");
//                broadcast("\r\nMessage from System: UNABLE to start FTP server.");
                systemBroadcast("UNABLE to start FTP server.");
                ftpServer = null;
            }
        }
    }

    /**
     * Gets the usernames of all users logged in to the
     * FTP server.
     *
     * @return array of usernames logged in.
     */
    public String[] getFTPUsers()
    {
        if (ftpServer == null)
            return null;
        else
            return ftpServer.getConnectedUsers();
    }

    /**
     * Indicates whether or not the FTP server is up and running.
     *
     * @return <code>true</code> if the FTP server is running.
     */
    public boolean isFTPServerRunning()
    {
        return (ftpServer != null);
    }

    /**
     * Initialize and start the Telnet server.
     * You must be an admin to call this function.
     */
    public void startupTelnetServer()
    {
        if (!TINIOS.isCurrentUserAdmin())
        {
            return;
        }

        if(telnetServer == null)
        {
            sendOutput("[     Starting up Telnet server...      ]");
            try
            {
                telnetServer = new TelnetServer();
                telnetServer.start();

                sendOutput("    [Done]\r\n");
//                broadcast("\r\nMessage from System: Telnet server started.");
                systemBroadcast("Telnet server started.");
            }
            catch(IOException ioe)
            {
                sendOutput("    [FAIL]: " + ioe + "\r\n");
//                broadcast("\r\nMessage from System: UNABLE to start Telnet server.");
                systemBroadcast("UNABLE to start Telnet server.");
                telnetServer = null;
            }
        }
    }

    /**
     * Gets the usernames of all users logged in to the
     * Telnet server.
     *
     * @return array of usernames logged in.
     */
    public String[] getTelnetUsers()
    {
        if (telnetServer == null)
            return null;
        else
            return telnetServer.getConnectedUsers();
    }

    /**
     * Indicates whether or not the Telnet server is up and running.
     *
     * @return <code>true</code> if the Telnet server is running.
     */
    public boolean isTelnetServerRunning()
    {
        return (telnetServer != null);
    }

    /**
     * Initialize and start the Serial server.
     * You must be an admin to call this function.
     */
    public void startupSerialServer()
    {
        startupSerialServer(false);
    }

    private synchronized void startupSerialServer(boolean automatic)
    {
        if(automatic)
        {
            if(!canControlSerialServer())
            {
                return;
            }
        }
        else
        {
            if (!TINIOS.isCurrentUserAdmin())
            {
                return;
            }
        }

        if (serialServer == null)
        {
            try
            {
                serialServer = new SerialServer(SERIAL_PORT, SERIAL_SPEED, SERIAL_DATABITS,
                                                SERIAL_STOPBITS, SERIAL_PARITY);
                serialServer.start();
                needToRestart = false;

                if(!commPortListenerAdded)
                {
                    CommPortIdentifier cpid = CommPortIdentifier.getPortIdentifier(SERIAL_PORT);
                    cpid.addPortOwnershipListener(this);
                    commPortListenerAdded = true;
                }

                if (!bootInProgress)
//                    broadcast("\r\nMessage from System: Serial server started.");
                      systemBroadcast("Serial server started.");
            }
            catch(Exception ioe)
            {
                sendOutput("    [FAIL]\r\n");
//                broadcast("\r\nMessage from System: UNABLE to start Serial server.");
                systemBroadcast("UNABLE to start Serial server.");
                serialServer = null;
            }
        }
    }

    /**
     * Indicates whether or not the Serial server is up and running.
     *
     * @return <code>true</code> if the Serial server is running.
     */
    public synchronized boolean isSerialServerRunning()
    {
        return (serialServer != null);
    }

    /**
     * Gets the usernames of all users logged in to the
     * Serial server.
     *
     * @return array of usernames logged in.
     */
    public synchronized String[] getSerialUsers()
    {
        if (serialServer == null)
            return null;
        else
            return serialServer.getConnectedUsers();
    }

    /**
     * Bring down the Serial server.
     * You must be an admin to call this function.
     *
     * @return <code>true</code> if the Serial server was
     * running and was shutdown correctly.  <code>false</code>
     * if the server wasn't running, or if current user isn't
     * an admin.
     */
    public boolean shutDownSerialServer()
    {
        return shutDownSerialServer(false);
    }

    private synchronized boolean shutDownSerialServer(boolean automatic)
    {
        if(automatic)
        {
            if(!canControlSerialServer())
            {
                return false;
            }
        }
        else
        {
            if (!TINIOS.isCurrentUserAdmin())
            {
                return false;
            }
        }

        if (serialServer != null)
        {
            try
            {
                serialServer.shutDown();

                serialServer.resume();

                serialServer = null;

                systemBroadcast("Serial server stopped.");
            }
            catch (java.io.IOException ioe)
            {
                systemBroadcast("IOException shutting down Serial server.");
            }
            return true;
        }
        return false;
    }


   boolean needToRestart = false;

   public void ownershipChange(int eventType)
   {
        if (eventType == PORT_OWNERSHIP_REQUESTED)
        {
            synchronized(this)
            {
                if(serialServer != null)
                {
                    needToRestart = shutDownSerialServer(true);
                }
            }
        }
        else if(eventType == PORT_UNOWNED)
        {
            if(needToRestart)
            {
                startupSerialServer(true);
                needToRestart = false;
            }
        }
   }

    /**
     * This method is called to log a user into the system and set
     * his privilege level correctly.  The passwd file contains
     * the username and password hash pairs.
     *
     * @param  userName   The user name of the user logging in
     * @param  password   The password for this user
     *
     * @return the user ID of the newly logged in user, or -1 if
     * the userName password combination was not correct.
     */
    public int login(String userName, String password)
    {
        synchronized (passwdLock)
        {
            try
            {
                String currentLine;
                byte[] newPass = com.dalsemi.system.Security.hashMessage((userName + ":" + password).getBytes());
                DataInputStream passwdIn = new DataInputStream(new BufferedInputStream(new FileInputStream(PASSWORD_FILE)));
                while ((currentLine = passwdIn.readLine()) != null)
                {
                    //passwd file in the form of:
                    //user:password:userid
                    int pos = currentLine.indexOf(':');
                    String name = currentLine.substring(0,pos++);
                    int lastPos = currentLine.indexOf(':', pos);
                    if (userName.equals(name))
                    {
                        int userID = Integer.parseInt(currentLine.substring(lastPos + 1));
                        Byte uid = new Byte((byte)(userID & 0x0FF));
                        if (pos == lastPos) //Means no password set
                        {
                            userIDByThreadID.put(Thread.currentThread(), uid);
                            return (userID & 0x0FF);
                        }
                        for(int i = 0; i < 20; i++)
                        {
                            pass[i] = (byte)Short.parseShort(currentLine.substring(pos, pos + 2), 16);
                            pos += 2;
                        }
                        if (com.dalsemi.system.ArrayUtils.arrayComp(pass, 0, newPass, 0, 20))
                        {
                            (System.getProperties()).put("user.name", userName);
                            userIDByThreadID.put(Thread.currentThread(), uid);
                            if (recovery)
                            {
                                systemBroadcast("Network recovery routines have run.  Please check system for corruption or data loss.");
                                recovery = false;
                            }
                            return (userID & 0x0FF);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                com.dalsemi.system.Debug.debugDump(e.toString());
            }
        }
        if (recovery)
            systemBroadcast("Network recovery routines have run.");

        return -1;
    }

    /**
     * This method logs the current user out of the system, returning the
     * privilege level of the current process to the default state.
     *
     * @param  info   ThreadID of session logging out.
     */
    public void logout(Object threadID)
    {
        userIDByThreadID.remove(threadID);
    }

    /**
     * Indicate to slush that a new user has been added to the system.
     */
    public void clearUserNameByUIDCache()
    {
        userNamesByUID = null;
    }

    private void initializeUserNameCache()
    {
        if (userNamesByUID == null)
        {
            userNamesByUID = new Hashtable(10, 1);
            synchronized (passwdLock)
            {
                try
                {
                    DSFile passwdFile = new DSFile(PASSWORD_FILE);
                    DataInputStream input = new DataInputStream(new BufferedInputStream(new FileInputStream(passwdFile)));
                    String line = null;

                    while ((line = input.readLine()) != null)
                    {
                        userNamesByUID.put(line.substring(line.lastIndexOf(':') + 1), line.substring(0, line.indexOf(':')));
                    }
                    input.close();
                }
                catch(FileNotFoundException fnfe)
                {
                    com.dalsemi.system.Debug.debugDump("ERROR:  Could not find password file.");
                }
                catch(Throwable t)
                {
                    com.dalsemi.system.Debug.debugDump("Exception password file: " + t.toString());
                }
            }
        }
    }

    /**
     * Returns the name of the current user logged in.
     *
     * @return  The name of the current user.
     */
    public String getCurrentUserName()
    {
        initializeUserNameCache();
        return getUserNameByUID(getCurrentUID());
    }

    /**
     * Returns the user name of the user with the given user ID.
     *
     * @param  uid   The uid to check.
     *
     * @return  the user name of the user with the given user ID.
     */
    public String getUserNameByUID(byte uid)
    {
        initializeUserNameCache();
        return (String)userNamesByUID.get(Integer.toString(uid & 0x0FF));
    }

    /**
     * Returns the user ID of the user with the given username.
     *
     * @param  username   The username to check.
     *
     * @return  the UID of the given username, or -1 if that user
     * does not exist.
     */
    public int getUIDByUserName(String username)
    {
        initializeUserNameCache();
        //DEBUG ==> This is the epitome of inefficiency and should be
        //re-written, but it works, and right now only the ChownCommand
        //is calling it.
        for (Enumeration e = userNamesByUID.keys(); e.hasMoreElements(); )
        {
            String uidStr = (String)e.nextElement();
            if (userNamesByUID.get(uidStr).equals(username))
                return Integer.decode(uidStr).intValue();
        }

        return -1;
    }

    /**
     * Change the password for a user.  If no user is indicated, the current
     * user is assumed.
     *
     * @param in  InputStream from which to read the new password.
     * @param out PrintStream to which to display information.
     * @param err PrintStream to which to display error information.
     * @param userName Name of user, or <code>null</code> to indicate current
     * user.
     *
     * @throws IOException if an error occurs accessing the password file.
     */
    public final void changePassword(SystemInputStream in, PrintStream out,
                                     PrintStream err, String username) throws IOException
    {
        String COLON = ":";

        synchronized (passwdLock)
        {
            RandomAccessFile fis = new RandomAccessFile(PASSWORD_FILE, "r");
            String passfile = EMPTY;
            String line;
            boolean found = false;
            int userid;
            while((line = fis.readLine()) != null)
            {
                if(!((username != null && !line.startsWith(username + COLON)) ||
                    (username == null && !line.endsWith(COLON + (TINIOS.getCurrentUID() & 0x0FF))) || found))
                {
                    found = true;
                    userid = Integer.parseInt(line.substring(line.lastIndexOf(':') + 1));
                    if(username != null)
                    {
                        if((!TINIOS.isCurrentUserAdmin()) && (userid != TINIOS.getCurrentUID()))
                        {
                            err.println("You must be an admin to change the password for another user.");
                            fis.close();
                            return;
                        }
                    }
                    else
                    {
                        username = line.substring(0, line.indexOf(':'));
                    }

                    in.setEcho(false);
                    out.print("Enter the new password:");
                    String newPass = in.readLine();
                    out.print("\r\nConfirm the new password:");
                    String confirmPass = in.readLine();
                    out.println(EMPTY);
                    if(!newPass.equals(confirmPass))
                    {
                        err.println("The new passwords do not match.");
                        fis.close();
                        in.setEcho(true);
                        return;
                    }
                    in.setEcho(true);

                    line = username + COLON;
                    byte[] hash = com.dalsemi.system.Security.hashMessage((username + ":" + newPass).getBytes());
                    String zero = "0";
                    for(int i = 0; i < 20; i++)
                    {
                        int num = hash[i] & 0x0FF;
                        if(num < 16)
                            line += zero;
                        line += Integer.toHexString(num);
                    }
                    line += COLON + userid;
                }
                passfile += line + "\r\n";
            }
            fis.close();

            Byte uid = (Byte)userIDByThreadID.get(Thread.currentThread());
            userIDByThreadID.put(Thread.currentThread(), new Byte((byte)adminUID));
            FileOutputStream fos = new FileOutputStream(PASSWORD_FILE);
            userIDByThreadID.put(Thread.currentThread(), uid);
            fos.write(passfile.getBytes());
            fos.close();
        }
    }

    /**
     * Bring up the Telnet and FTP servers.  You must
     * have previously set the IP address.
     * You must be an admin to call this function.
     */
    public void startupNetworkServers()
    {
        if (!TINIOS.isCurrentUserAdmin())
        {
            return;
        }

        if (TININet.getIPAddress().length() == 0)
        {
            if (TININet.getDHCPServerIP().length() == 0)
            {
                System.out.println("[    Network configurations not set.    ]    [Skip]\r\n");
                return;
            }
            else
            {
                return;  //DEBUG ==> Print Error message here of some sort somehow?
            }
        }

        String startTelnet = TINIOS.getFromCurrentEnvironment("TelnetServer");
        if ((startTelnet != null) && (startTelnet.equals("enable")) )
        {
            startupTelnetServer();
        }
        String startFTP = TINIOS.getFromCurrentEnvironment("FTPServer");
        if ((startFTP != null) && (startFTP.equals("enable")) )
        {
            startupFTPServer();
        }
    }

    /**
     * Start a DHCP client to obtain a dynamic IP.
     * You must be an admin to call this function.
     */
    public void initiateDHCP()
    {
        if ((!TINIOS.isCurrentUserAdmin()) || dhcpClient != null)
            return;

        SlushDHCPListener dhcpListener = new SlushDHCPListener();

        if (TININet.getDHCPServerIP().length() == 0)
        {
            //Start with a fresh DHCP session
            dhcpClient = new DHCPClient(dhcpListener);
        }
        else
        {
            //Start with previous DHCP server and IP if possible
            byte[] sip = new byte[4];
            TININet.getDHCPServerIP(sip);
            byte[] yip = new byte[4];
            TININet.getIPAddress(yip);
            dhcpClient = new DHCPClient(dhcpListener, sip, yip);
        }

        //Since the DHCP Client is a separate thread, it needs to have
        //admin priv's
        userIDByThreadID.put(dhcpClient, new Byte((byte)adminUID));

        dhcpClient.start();
    }

    /**
     * Class to handle DHCP events.
     */
    private class SlushDHCPListener implements DHCPListener
    {
        public void ipLeased()
        {
            systemBroadcast("DHCP leased an IP.");
            startupNetworkServers();
        }

        public void ipRenewed()
        {
            systemBroadcast("DHCP renewed IP.");
        }

        public void ipError(String error)
        {
            systemBroadcast("DHCP IP Error: " + error);
            shutDownTelnetServer();
            shutDownFTPServer();
        }

        public void ipLost()
        {
            systemBroadcast("DHCP IP lost.");
            shutDownTelnetServer();
            shutDownFTPServer();
        }
    }

    /**
     * Right now, threads do not contain the user ID.  This might
     * change in the future, but for now, if a slush command spins off
     * a thread, it must register the thread here, or that thread will
     * run with guest privileges.
     */
    public void registerThread(Thread thisThread)
    {
        userIDByThreadID.put(thisThread, new Byte(getCurrentUID()));
    }

    /**
     * Right now, threads do not contain the user ID.  This might
     * change in the future, but for now, if a slush command spins off
     * a thread, it must register the thread with <code>registerThread</code>.
     * The slush command should unregister the thread when finished.
     */
    public void unregisterThread(Thread thisThread)
    {
        userIDByThreadID.remove(thisThread);
    }

    /**
     * Stop using DHCP and bring down all network servers.
     * You must be an admin to call this function.
     */
    public void stopDHCP()
    {
        if ((!TINIOS.isCurrentUserAdmin()) || dhcpClient == null)
            return;

        dhcpClient.stopDHCPThread();
        shutDownTelnetServer();
        shutDownFTPServer();
        userIDByThreadID.remove(dhcpClient);
        dhcpClient = null;
    }

    /**
     * Returns whether or not slush is using DHCP.
     *
     * @return <code>true</code> if DHCP is running.
     */
    public boolean isDHCPRunning()
    {
        return (dhcpClient != null);
    }

    private void systemBroadcast(String sendThis)
    {

        broadcast("\r\n[ "+ (new Date()) +" ]  Message from System: "+sendThis);
    }


    /**
     * Broadcast a message to all users connected to
     * the serial and Telnet servers.
     *
     * @param sendThis  Message to send.
     */
    public void broadcast(String sendThis)
    {
        if (telnetServer != null)
            telnetServer.broadcast(sendThis);

        synchronized(this)
        {
            if (serialServer != null)
                serialServer.broadcast(sendThis);
        }
    }

    /**
     * Create an instance of the slush shell.
     */
    public static void main(String[] args)
    {
      // wpd - the line below was added by AXI for the 5dx
      new com.agilent.xRayTest.hardware.tini.TINIFlash();
      new Slush();
    }

    private static long controlPropertiesLastModified = 0;
    private static final File controlPropertiesFile = new File("/etc/serial.server.properties");
    private static boolean canControlSerial = true;

    private static boolean canControlSerialServer()
    {
        File propFile = controlPropertiesFile;
        if(propFile.exists() && propFile.isFile())
        {
            long modTime = propFile.lastModified();
            if(modTime > controlPropertiesLastModified)
            {
                controlPropertiesLastModified = modTime;
                Properties props = new Properties();
                try
                {
                    props.load(new FileInputStream(propFile));
                    String value = props.getProperty("automatic.shutdown", "true");
                    if(value.equals("false"))
                    {
                        canControlSerial = false;
                    }
                    else
                    {
                        canControlSerial = true;
                    }
                }
                catch(IOException ioe)
                {
                    //Couldn't read (or parse) the serial.server.properties file.  Oh well, just continue with the defaults.
                }
            }
            return canControlSerial;
        }
        return true;
    }
    
    public void removeCommPortListener() throws Exception
    {
        if(commPortListenerAdded)
        {
            CommPortIdentifier cpid = CommPortIdentifier.getPortIdentifier(SERIAL_PORT);
            cpid.removePortOwnershipListener(this);
            commPortListenerAdded = false;
        }
    }
}
