package com.axi.v810.properties;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * We currently have two localization files.
 * An English one and one in Simple Chinese.
 * Whenever changes are made to the English one,
 * a similar change must be made to the Simple Chinese
 * one. This script will make sure they are in sync.
 * It will check that all the files have the same keys,
 * and anything that is still in English must match 100%
 *
 * @author George A. David
 */
public class Test_LocalizationFiles extends UnitTest
{
  static boolean _testForKeyNeedToTranslate = true;

  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LocalizationFiles());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      ConvertLocalizationToAscii.getInstance().convert();
      String usLocalizationFile = Directory.getPropertiesDir() + File.separator + "localization.properties";
      String chLocalizationFile = Directory.getPropertiesDir() + File.separator + "localization_zh_CN.properties";
      Map<String, String> usKeyToValueMap = processFile(usLocalizationFile);
      Map<String, String> chKeyToValueMap = processFile(chLocalizationFile);

      Set<String> usKeys = new HashSet<String>(usKeyToValueMap.keySet());
      Set<String> chKeys = new HashSet<String>(chKeyToValueMap.keySet());

      Set<String> extraUsKeys = new HashSet<String>(usKeys);
      extraUsKeys.removeAll(chKeys);
      for (String usKey : extraUsKeys)
        System.out.println("Error: The key " + usKey + " was found in " + usLocalizationFile + " but not in " + chLocalizationFile);

      Set<String> extraChKeys = new HashSet<String>(chKeys);
      extraChKeys.removeAll(usKeys);
      for (String chKey : extraChKeys)
        System.out.println("Error: The key " + chKey + " was found in " + chLocalizationFile + " but not in " + usLocalizationFile);

      // ok, now make sure the english keys are all equal
      for(String usKey : usKeys)
      {
        String usValue = usKeyToValueMap.get(usKey);
        String chValue = chKeyToValueMap.get(usKey);
        // if the key contains non-us characters, ignore it.

        if(chValue != null && chValue.indexOf("\\u") < 0)
          Expect.expect(usValue.equals(chValue), "The key " + usKey + " in the file " + chLocalizationFile + "was expected to have a value of \n\"" + usValue + "\"\n\nbut got\n\n\"" + chValue + "\".");

        if(chValue != null && chValue.contains("\\\\u"))
        {
          Expect.expect(false, usKey + " is invalid. check the usage of \\.");
        }

        if(chValue!= null)
          loadConvertedValue(chValue);

        // To validate whether the value in chinese localization is already translated
        // @author Wei Chin, Chong
        if(_testForKeyNeedToTranslate && chValue != null && chValue.indexOf("\\u") < 0)
        {
          if( chValue.length() != 1 &&
              chValue.equals("V/V") == false &&
              chValue.equals("SM/TH") == false &&
              chValue.equals("ms") == false &&
              chValue.equals("mV") == false &&
              chValue.equals("{0} nm") == false &&
              chValue.equals("{0} mm") == false &&
              chValue.equals("{0} mils") == false &&
              chValue.equals("{0} mils^2") == false &&
              chValue.equals("{0} mm^2") == false &&
              chValue.equals("{0} mm^3") == false &&
              chValue.equals("{0} mils^3") == false &&
              chValue.equals("{0}%") == false &&
              chValue.equals("{0}: {1} {2}\\n") == false &&
              chValue.equals("\"{0}\"") == false &&
              chValue.equals("N\\A") == false &&
              chValue.equals("SM") == false &&
              chValue.equals("SMEMA") == false &&
              chValue.equals("MB") == false &&
              chValue.equals("UPO") == false &&
              chValue.equals("GPO") == false &&
              chValue.equals("TRO") == false &&
              chValue.equals("SRO") == false &&
              chValue.equals("SRI") == false &&
              chValue.equals("TRI") == false &&
              chValue.equals("RegEx") == false &&
              chValue.equals("<fov>") == false &&
              chValue.equals("TH") == false &&
              chValue.equals("CAD") == false &&
              chValue.equals("Id") == false &&
              chValue.equals("X =") == false &&
              chValue.equals("Y =") == false &&
              chValue.equals(">>") == false &&
              chValue.equals("<<") == false &&
              chValue.equals(">") == false &&
              chValue.equals("<") == false &&
              chValue.equals("Slice Setup") == false &&
              chValue.equals("AXI System") == false &&
              chValue.equals("AXI System v810") == false&&
              chValue.equals("PSP") == false &&
              chValue.equals("BOM") == false &&
              chValue.equals("TIFF") == false&&
              chValue.equals("PNG") == false&&
              chValue.equals("X-out") == false&&
              chValue.equals("RFP") == false&&
              chValue.equals("V810 / X6000") == false&&
              chValue.equals("5DX") == false&&
              chValue.equals("5DX NDFs") == false)
            Expect.expect(false, "The key " + usKey + " in the file " + chLocalizationFile + "was expected to translate of \n\"" + usValue + "\"\n\nbut got\n\n\"" + chValue + "\".");
        }
      }

      String regEx = ".*localization.*\\.properties";
      Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);

      // check to make sure there isn't a new localization file that we are not testing
      for (String filename : FileUtil.listFiles(Directory.getPropertiesDir()))
      {
        Matcher matcher = pattern.matcher(filename);
        if (matcher.matches())
        {
          if (filename.equalsIgnoreCase(usLocalizationFile) == false &&
              filename.equalsIgnoreCase(chLocalizationFile) == false)
          {
            System.out.println("Error: The properties file " + filename + " is not being tested.");
          }
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George A. David
   */
  private Map<String,String> processFile(String filePath) throws Exception
  {
    Assert.expect(filePath != null);

    System.out.println("filePath : " + filePath);
    Map<String, String> keyToValueMap = new HashMap<String, String>();
    boolean isProcessingKeyValuePair = false;
    Pattern keyValuePattern = Pattern.compile("(.*?)=(.*)");

    String line = null;
    FileReaderUtil reader = new FileReaderUtil();
    reader.open(filePath);
    String keyValuePair = null;
    while(reader.hasNextLine())
    {
      line = reader.readNextLine();
      if(isProcessingKeyValuePair)
      {

        if(line.endsWith("\\"))
        {
          keyValuePair += line.substring(0, line.length() - 1);
        }
        else
        {
          keyValuePair += line;
          isProcessingKeyValuePair = false;
        }
      }
      else
      {
        if(line.length() == 0 || line.startsWith("#") || line.trim().length() == 0)
          continue; // ignore comments and empty lines

        if(line.endsWith("\\"))
        {
          isProcessingKeyValuePair = true;
          keyValuePair = line.substring(0, line.length() - 1);
        }
        else
        {
          keyValuePair = line;
        }
      }

      if(isProcessingKeyValuePair == false &&
         keyValuePair != null)
      {
        Matcher matcher = keyValuePattern.matcher(keyValuePair);
        if(matcher.matches() == false)
        {
          reader.getLineNumber();
          System.out.println("Error: The key value pair " + keyValuePair + " is invalid at line " + reader.getLineNumber() + " in file " + filePath);
          System.out.println("This may be because the previous line did not end with a backslash (\\) or has trailing spaces after the backslash.");
        }
        else
        {
          String key = matcher.group(1);
          String value = matcher.group(2);
          String prevValue = keyToValueMap.put(key, value);
          if(prevValue != null)
            System.out.println("Error: The key " + key + " is duplicate at line " + reader.getLineNumber() + " in file " + filePath);
//          Assert.expect(prevValue == null);
        }
        keyValuePair = null;
      }
    }
    reader.close();

    return keyToValueMap;
  }

  /*
   * Converts encoded &#92;uxxxx to unicode chars
   * and changes special saved chars to their original forms
   * @author Wei Chin, Chong
   */
  private String loadConvertedValue(String inputValue)
  {
    Assert.expect(inputValue != null);
    
    char[] convtBuf = new char[1024];
    int off = 0;
    int len = inputValue.length();

    if (convtBuf.length < len)
    {
      int newLen = len * 2;
      if (newLen < 0)
      {
        newLen = Integer.MAX_VALUE;
      }
      convtBuf = new char[newLen];
    }

    char aChar;
    char[] out = convtBuf;
    int outLen = 0;
    int end = off + len;

    while (off < end)
    {
      aChar = inputValue.charAt(off++);
      if (aChar == '\\')
      {
        aChar = inputValue.charAt(off++);
        if (aChar == 'u')
        {
          // Read the xxxx
          int value = 0;
          for (int i = 0; i < 4; i++)
          {
            aChar = inputValue.charAt(off++);
            switch (aChar)
            {
              case '0':
              case '1':
              case '2':
              case '3':
              case '4':
              case '5':
              case '6':
              case '7':
              case '8':
              case '9':
                value = (value << 4) + aChar - '0';
                break;
              case 'a':
              case 'b':
              case 'c':
              case 'd':
              case 'e':
              case 'f':
                value = (value << 4) + 10 + aChar - 'a';
                break;
              case 'A':
              case 'B':
              case 'C':
              case 'D':
              case 'E':
              case 'F':
                value = (value << 4) + 10 + aChar - 'A';
                break;
              default:
                throw new IllegalArgumentException(
                    "Malformed \\uxxxx encoding. : " + inputValue);
            }
          }
          out[outLen++] = (char)value;
        }
        else
        {
          if (aChar == 't')
            aChar = '\t';
          else if (aChar == 'r')
            aChar = '\r';
          else if (aChar == 'n')
            aChar = '\n';
          else if (aChar == 'f')
            aChar = '\f';
          out[outLen++] = aChar;
        }
      }
      else
      {
        out[outLen++] = (char)aChar;
      }
    }
    return new String(out, 0, outLen);
  }
}
