package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class will be used to compare CustomAlgorithmFamily objects alphanumerically by their names
 * @author Bill Darbie
 */
public class SubtypeAlphaNumericComparator implements Comparator<Subtype>
{
  private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();

  /**
   * @author George A. David
   */
  public SubtypeAlphaNumericComparator()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  public int compare(Subtype lhs, Subtype rhs)
  {
    // check to make sure that both objects are not null
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    return _alphaNumericComparator.compare(lhs.getLongName(), rhs.getLongName());
  }
}
