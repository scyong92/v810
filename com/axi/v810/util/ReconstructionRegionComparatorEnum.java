package com.axi.v810.util;

/**
 * <p>Title: ReconstructionRegionComparatorEnum</p>
 *
 * <p>Description: Provide keys for sorting ReconstructionRegion table entries, used in
 * conjunction with the ReconstructionRegionComparator</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Andy Mechtenberg
 */
public class ReconstructionRegionComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ReconstructionRegionComparatorEnum DESCRIPTION = new ReconstructionRegionComparatorEnum(++_index);
  public static ReconstructionRegionComparatorEnum FOCUS_METHOD = new ReconstructionRegionComparatorEnum(++_index);
  public static ReconstructionRegionComparatorEnum RETEST_STATUS = new ReconstructionRegionComparatorEnum(++_index);
  public static ReconstructionRegionComparatorEnum NOTEST_STATUS = new ReconstructionRegionComparatorEnum(++_index);

  /**
   * @author Andy Mechtenberg
   */
  private ReconstructionRegionComparatorEnum(int id)
  {
    super(id);
  }
}
