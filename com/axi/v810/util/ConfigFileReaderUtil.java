package com.axi.v810.util;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * Reads files that have the format key=value
 * Comments in these files begin with a #
 * @author George A. David
 * @author Poh Kheng
 */
public class ConfigFileReaderUtil
{
  FileReaderUtilAxi _fileReader;
  Map<String, String> _keyToValueMap = new HashMap<String, String>();
  // Change for CR30270 - original pattern is (.*)=(.*)
  Pattern _keyValuePattern = Pattern.compile("(.*?)=(.*)");

  Collection<String> _validKeys = new LinkedList<String>();
  Collection<String> _optionalKeys = new LinkedList<String>();
  boolean _checkForValidKeys = false;

  /**
   * @author George A. David
   */
  public ConfigFileReaderUtil()
  {
    _fileReader = new FileReaderUtilAxi();
  }

  /**
   * @author George A. David
   */
  public void setValidKeys(Collection<String> validKeys)
  {
    Assert.expect(validKeys != null);

    _validKeys.addAll(validKeys);
    _checkForValidKeys = (_validKeys.isEmpty() == false);
  }
  
  /**
   * Keep a list of keys that may not appears in all config files.
   * This is to handle version backward-compatibility.
   * 
   * @author Cheah Lee Herng 
   */
  public void setOptionalKeys(Collection<String> optionalKeys)
  {
    Assert.expect(optionalKeys != null);
    
    _optionalKeys.addAll(optionalKeys);
  }

  /**
   * @author George A. David
   */
  public void clearValidKeys()
  {
    _validKeys.clear();
    _checkForValidKeys = false;
  }

  /**
   * @author George A. David
   */
  public void parseFile(String filePath) throws DatastoreException
  {
    _fileReader.open(filePath);
    _keyToValueMap.clear();
    Collection<String> validKeys = new LinkedList<String>(_validKeys);
    try
    {
      while (_fileReader.hasNextLine())
      {
        String line = _fileReader.readNextLine().trim();

        // ignore comments
        if(line.startsWith("#") == false && line.length() > 0)
        {
          Matcher matcher = _keyValuePattern.matcher(line);
          if(matcher.matches())
          {
            String key = matcher.group(1);
            if(_checkForValidKeys)
            {
              // throw an exception if the file has a
              // duplicate key or an invalid key
              if (validKeys.contains(key) == false)
                throw new FileCorruptDatastoreException(filePath);
              else
                // remove the key so that later
                // we can find duplicate ones.
                validKeys.remove(key);
            }
            String value = matcher.group(2);
            _keyToValueMap.put(key, value);
          }
          else
            throw new FileCorruptDatastoreException(filePath);
        }
      }
      
      validKeys.removeAll(_optionalKeys);
      
      // throw an exception if the file is missing keys
      if(validKeys.isEmpty() == false)
      {
        throw new FileCorruptDatastoreException(filePath);
      }
    }
    finally
    {
      _fileReader.close();
    }
  }

  /**
   * @author George A. David
   */
  public Map<String, String> getKeyValueMap()
  {
    Assert.expect(_keyToValueMap != null);

    return _keyToValueMap;
  }

  /**
   * @author George A. David
   */
  public String getValue(String key)
  {
    Assert.expect(key != null);

    String value = _keyToValueMap.get(key);

    Assert.expect(value != null);
    return value;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean hasKey(String key)
  {
    Assert.expect(key != null);
    
    if (_keyToValueMap.containsKey(key))
      return true;
    else
      return false;
  }
}
