package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * Represents a coordineate in machine space.  The origin is the stage home position.
 * The x axis grows to the right and the y axis grows up.
 *
 * @author Tony Turner
 * @author Matt Wharton
 */
public class MachineCoordinate extends IntCoordinate implements Serializable
{
  /**
   * @author Matt Wharton
   */
  public MachineCoordinate()
  {
    super();
  }

  /**
   * @author Matt Wharton
   */
  public MachineCoordinate(MachineCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * @param x the x value of this coordinate.
   * @param y the y value of this coordinate.
   * @author Matt Wharton
   */
  public MachineCoordinate(int x, int y)
  {
    super(x, y);
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }
}
