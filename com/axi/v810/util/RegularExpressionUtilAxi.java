package com.axi.v810.util;

import java.util.regex.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class is a wrapper class for the com.axi.util.RegularExpressionUtil class.  It converts all the non Axi exceptions
 * thrown by the RegularExpressionUtil class into DatastoreExceptions.
 *
 * @author Laura Cormos
 */
public class RegularExpressionUtilAxi
{
  /**
   * Validate a regular expression and throw an exception with more information on why this regEx is not valid
   * @author Laura Cormos
   */
  public static void checkRegExValid(String regExPattern) throws InvalidRegularExpressionDatastoreException
  {
    Assert.expect(regExPattern != null);

    try
    {
      RegularExpressionUtil.checkRegExIsValid(regExPattern);
    }
    catch (InvalidRegularExpressionException ex)
    {
      InvalidRegularExpressionDatastoreException dex =
          new InvalidRegularExpressionDatastoreException((PatternSyntaxException)ex.getCause());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * @author Patrick Lacz
   */
  public static String createRegularExpressionFromSimpleExpression(String globExpression)
  {
    // just use * and ? as wild cards - keep it simple

    // put a \ in front of all special reg ex characters except for * and ?
    Pattern pattern = Pattern.compile("([\\W&&[^\\*\\?,]])");
    String regExString = globExpression;
    Matcher matcher = pattern.matcher(regExString);
    regExString = matcher.replaceAll("\\\\$1");

    // now change * and ? to the correct reg expressions
    // commas separate different possible strings
    regExString = regExString.replaceAll("\\*", "[^\\\\s\\\\n]*");
    regExString = regExString.replaceAll("\\?", "[^\\\\s\\\\n]");
    regExString = regExString.replaceAll(",", "|");

    return regExString;
  }

}
