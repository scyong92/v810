package com.axi.v810.util;

import java.util.*;
import com.axi.v810.datastore.*;
import com.axi.util.*;


/**
 *  This class provides a way to write data to a file.
 *
 * @author Greg Esparza
 */
public class FileWriterUtilAxi
{
  private FileWriterUtil _fileWriterUtil;


  /**
   * @author Greg Esparza
   */
  public FileWriterUtilAxi(String fileNameFullPath, boolean append)
  {
    _fileWriterUtil = new FileWriterUtil(fileNameFullPath, append);
  }

  /**
   * @author Greg Esparza
   */
  public void open() throws DatastoreException
  {
    try
    {
      _fileWriterUtil.open();
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException = new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }

  /**
   * @author Greg Esparza
   */
  public void close()
  {
    _fileWriterUtil.close();
  }

  /**
   * This method is used to write the message to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param message is the data to write to the file
   * @author Greg Esparza
   */
  public void write(String message) throws DatastoreException
  {
    //Swee Yee Wong
    try
    {
      _fileWriterUtil.write(message);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }

  /**
   * This method is used to write the message followed by a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param message is the data to write to the file
   * @author Greg Esparza
   */
  public void writeln(String message) throws DatastoreException
  {
    //Swee Yee Wong
    try
    {
      _fileWriterUtil.writeln(message);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }

  /**
   * This method is used to write the messages followed by a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @author Bill Darbie
   */
  public void writeln(List<String> messages) throws DatastoreException
  {
    //Swee Yee Wong
    try
    {
      _fileWriterUtil.writeln(messages);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }

  /**
   * This method is used to write a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * * @author George A. David
   */
  public void writeln() throws DatastoreException
  {
    //Swee Yee Wong
    try
    {
      _fileWriterUtil.writeln();
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }

  /**
   * This method is used to write the exception to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param exception is the data to write to the file
   * @author Greg Esparza
   */
  public void write(Exception exception)
  {
    _fileWriterUtil.write(exception);
  }

  /**
   * This method is used to write the exception followed by a newline to the file.
   * The file be initially opened and remain open while using this method.
   *
   * @param exception is the data to write to the file
   * @author Greg Esparza
   */
  public void writeln(Exception exception)
  {
    _fileWriterUtil.writeln(exception);
  }

  /**
   * This method is used for a quick append of one line in the file.
   * Specifically, the file is opened, the data is written, then the file is closed.
   * The file should not be opened prior to calling this method.
   *
   * @param message is the data to write to the file
   * @author Greg Esparza
   */
  public void append(String message) throws DatastoreException
  {
    try
    {
      _fileWriterUtil.append(message);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException = new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }

  /**
   * This method is used for a quick append of one line in the file.
   * Specifically, the file is opened, the data is written, then the file is closed.
   * The file should not be opened prior to calling this method.
   *
   * @param messages is the list of data to write to the file
   * @author Greg Esparza
   */
  public void append(List<String> messages) throws DatastoreException
  {
    try
    {
      _fileWriterUtil.append(messages);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException = new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }

  /**
   * This method is used for a quick append of one line in the file.
   * Specifically, the file is opened, the data is written, then the file is closed.
   * The file should not be opened prior to calling this method.
   *
   * @param exception is the data to write to the file
   * @author Greg Esparza
   */
  public void append(Exception exception) throws DatastoreException
  {
    try
    {
      _fileWriterUtil.append(exception);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException = new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
  }
}
