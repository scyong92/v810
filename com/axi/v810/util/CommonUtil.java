package com.axi.v810.util;

import java.awt.*;

/**
 *
 * @author Chong, Wei Chin
 */
public class CommonUtil
{
  public static final int _COMBO_BOX_HEIGHT = 24;
  public static final int _WIDTH = 30;
  public static final int _MAXIMUM_COMBOBOX_ROWCOUNT = 20;

  public static final Color _PRIMARY_THEMES_COLOR = new Color (255, 150, 0);
  public static final Color _SECONDARY_COLOR_OUTLINE = new Color (0, 0, 0);  
  public static final Color _SECONDARY_COLOR_BACKGROUND = new Color (240, 255, 230);
}
