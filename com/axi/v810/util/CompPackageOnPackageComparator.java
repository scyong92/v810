package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Khaw Chek Hau
 * POP Development
 */
public class CompPackageOnPackageComparator implements Comparator<CompPackageOnPackage>
{
  private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();
  private boolean _ascending;
  private CompPackageOnPackageComparatorEnum _compPackageOnPackageComparatorEnum;

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public CompPackageOnPackageComparator(boolean ascending, CompPackageOnPackageComparatorEnum compPackageOnPackageComparatorEnum)
  {
    _ascending = ascending;
    _compPackageOnPackageComparatorEnum = compPackageOnPackageComparatorEnum;
  }

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public int compare(CompPackageOnPackage lhPackage, CompPackageOnPackage rhPackage)
  {
    Assert.expect(lhPackage != null);
    Assert.expect(rhPackage != null);

    String lhName;
    String rhName;

    if (_compPackageOnPackageComparatorEnum.equals(CompPackageOnPackageComparatorEnum.NAME_ALPHANUMERICALLY)) 
    {
      lhName = lhPackage.getPOPName();
      rhName = rhPackage.getPOPName();

      if (_ascending)
        return _alphaNumericComparator.compare(lhName, rhName);
      else
        return _alphaNumericComparator.compare(rhName, lhName);
    }
    else if (_compPackageOnPackageComparatorEnum.equals(CompPackageOnPackageComparatorEnum.NAME))
    {
      lhName = lhPackage.getPOPName();
      rhName = rhPackage.getPOPName();

      if(_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else
    {
      Assert.expect(false, "Please add your new compPackage attribute to sort on to this method");
      return 0;
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setCompPackageOnPackageComparatorEnum(CompPackageOnPackageComparatorEnum compPackageOnPackageComparatorEnum)
  {
    Assert.expect(compPackageOnPackageComparatorEnum != null);
    _compPackageOnPackageComparatorEnum = compPackageOnPackageComparatorEnum;
  }
  
  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  public void setAscending(boolean ascending)
  {
    _ascending = ascending;
  }
}
