package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;


/**
 * This class defines a rectangle in stage space.  The origin is the left feature 1 on the
 * fixed rail.
 * The x-axis grows right and the y-axis grows up.
 * The unit is nanometers.
 *
 * @author Matt Wharton
 */
public class StageRectangle extends IntRectangle implements Serializable
{
  /**
   * @author Matt Wharton
   */
  public StageRectangle(int bottomLeftX, int bottomLeftY, int width, int height)
  {
    super(bottomLeftX, bottomLeftY, width, height);
  }

  /**
   * @author Matt Wharton
   */
  public StageRectangle(StageRectangle rhs)
  {
    super(rhs);
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializeRectangle(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializeRectangle(os);
  }
}
