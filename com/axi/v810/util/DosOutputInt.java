package com.axi.v810.util;

/**
 * The DOSShell requires the use of this interface.
 * Any strings that come in from a dos command prompt on standard output will be passed
 * by calling the standardOutputLine method.  Strings coming from standard error will
 * by passed by calling the standardErrorLine method.
 *
 * @see DosShell
 * @author Bill Darbie
 */
public interface DosOutputInt
{
  void standardOutputLine(String line);
  void standardErrorLine(String line);
}
