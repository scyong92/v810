package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 *
 * @author Rex Shang
 */
public class Test_Version extends UnitTest
{
  /**
   * @author Rex Shang
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Version());
  }

  /**
   * @author Rex Shang
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String version = Version.getVersionNumber();
    Expect.expect((VersionUtil.isNewerVersion(version, "1.0") || VersionUtil.isRequiredVersion(version, "1.0")));    
    Expect.expect(version != null, "Version is null.");

    String versionString = Version.getVersion();
    Expect.expect(versionString.indexOf(version) != -1, "Version number is not part of the version string.");

    Version.getBuildTimeStamp();
    Expect.expect(versionString.indexOf(Version.getBuildTimeStamp()) != -1, "Build time stamp is not part of the version string.");

    String copyRight = Version.getCopyRight();
    Calendar now = Calendar.getInstance();
    Expect.expect(copyRight.indexOf("2009") != -1);
    Expect.expect(copyRight.indexOf(Integer.toString(now.get(Calendar.YEAR))) != -1);
  }
}
