package com.axi.v810.util;

import com.axi.util.*;
//import com.axi.v810.hardware.*;
import java.io.*;


/**
 * <p>Title: Test_PanelCoordinate</p>
 * <p>Description: unit tests the PanelCoordinate class</p>
 * <p>Copyright: Copyright (c)</p>
 * <p>Company: AXI</p>
 * @author Tony Turner
 * @version 1.0
 */

public class Test_PanelCoordinate extends UnitTest
{
  // local vars for coordinate gets/sets
  PanelCoordinate _panelCoordinate = null;
  int _x = -1;
  int _y = -1;

  /**
   * @author Tony Turner
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PanelCoordinate());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    // create a new PanelCoordinate with no parameters
    try
    {
      _panelCoordinate = new PanelCoordinate();
    }
    catch (AssertException aex)
    {
      // bad constructor
      Assert.expect(false);
    }

    // set x,y to 0,0
    _panelCoordinate.setX(0);
    _panelCoordinate.setY(0);

    // ensure that we get the 0,0 back from gets
    _x = _panelCoordinate.getX();
    _y = _panelCoordinate.getY();
    Assert.expect(_x == 0);
    Assert.expect(_y == 0);

    // set x,y to 5,6
    _panelCoordinate.setX(5);
    _panelCoordinate.setY(6);

    // ensure that we get the 5,6 back from gets
    _x = _panelCoordinate.getX();
    _y = _panelCoordinate.getY();
    Assert.expect(_x == 5);
    Assert.expect(_y == 6);
  }
}

