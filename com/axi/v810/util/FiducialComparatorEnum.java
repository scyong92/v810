package com.axi.v810.util;

/**
 * @author Laura Cormos
 */
public class FiducialComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static FiducialComparatorEnum FULLY_QUALIFIED_NAME = new FiducialComparatorEnum(++_index);
  public static FiducialComparatorEnum NAME = new FiducialComparatorEnum(++_index);
  public static FiducialComparatorEnum X_COORDINATE = new FiducialComparatorEnum(++_index);
  public static FiducialComparatorEnum Y_COORDINATE = new FiducialComparatorEnum(++_index);
  public static FiducialComparatorEnum WIDTH = new FiducialComparatorEnum(++_index);
  public static FiducialComparatorEnum LENGTH = new FiducialComparatorEnum(++_index);
  public static FiducialComparatorEnum SHAPE = new FiducialComparatorEnum(++_index);
  public static FiducialComparatorEnum LOCATION = new FiducialComparatorEnum(++_index);

  /**
   * @author Laura Cormos
   */
  private FiducialComparatorEnum(int id)
  {
    super(id);
  }
}
