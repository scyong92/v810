package com.axi.v810.util;

import com.axi.util.*;
import java.io.*;


/**
 * Test the UsbUtil5dx class
 * @author George A. David
 */
public class Test_UsbUtil5dx extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UsbUtil5dx());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    System.loadLibrary("nativeAxiUtil");

    try
    {
      UsbUtil5dx.getInstance(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }


    UsbUtil5dx usbUtil = UsbUtil5dx.getInstance(UsbDriverTypeEnum.FTDI);
    Expect.expect(usbUtil == UsbUtil5dx.getInstance(UsbDriverTypeEnum.FTDI));

    try
    {
      usbUtil.connect(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationHardwareException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    UsbDeviceDescriptionEnum deviceDescription = UsbDeviceDescriptionEnum.XRAY_SOURCE;

    try
    {
      usbUtil.connect(deviceDescription);
    }
    catch(CommunicationHardwareException cex)
    {
      // do nothing
    }

    try
    {
      usbUtil.disconnect(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationHardwareException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    String message = "message";
    try
    {
      usbUtil.sendMessage(null, message);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationHardwareException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.sendMessage(deviceDescription, null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationHardwareException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.getReplies(null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationHardwareException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.sendMessageAndGetReplies(null, message);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationHardwareException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

    try
    {
      usbUtil.sendMessageAndGetReplies(deviceDescription, null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }
    catch(CommunicationHardwareException cex)
    {
      Expect.expect(false);
      cex.printStackTrace();
    }

//    try
//    {
//      usbUtil.sendMessage(deviceDescription, "message");
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    try
//    {
//      usbUtil.getReplies(deviceDescription);
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    try
//    {
//      usbUtil.sendMessageAndGetReplies(deviceDescription, "message");
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    try
//    {
//      usbUtil.disconnect(deviceDescription);
//    }
//    catch (CommunicationException ex)
//    {
//      System.out.println("key: " + ex.getKey());
//      System.out.println("message: " + ex.getMessage());
//      List parameters = ex.getParameters();
//      Iterator it = parameters.iterator();
//      while(it.hasNext())
//      {
//        System.out.println("Parameter: " + it.next());
//      }
//      ex.printStackTrace();
//    }
//    usbUtil.finalize();
  }
}
