package com.axi.v810.util;

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
public class CompPackageOnPackageComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static CompPackageOnPackageComparatorEnum NAME = new CompPackageOnPackageComparatorEnum(++_index);
  public static CompPackageOnPackageComparatorEnum NAME_ALPHANUMERICALLY = new CompPackageOnPackageComparatorEnum(++_index);

  /**
   * @author Khaw Chek Hau
   * POP Development
   */
  private CompPackageOnPackageComparatorEnum(int id)
  {
    super(id);
  }
}
