/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.util;

import com.axi.util.*;

/**
 *
 * @author Jack Hwee
 */
public class SurfaceMapPointsComparator 
{
  private AlphaNumericComparator _alphaComparator;
  private PanelCoordinate _comparingAttribute;
  private boolean _ascending;
     
    /**
   * @author George Booth
   */
  public SurfaceMapPointsComparator(boolean ascending,PanelCoordinate comparingAttribute)
  {
    Assert.expect(comparingAttribute != null);
    _ascending = ascending;
    _comparingAttribute = comparingAttribute;
    _alphaComparator = new AlphaNumericComparator(_ascending);
  }

  /**
   * @author George Booth
   
  public int compare(PanelCoordinate lhs, PanelCoordinate rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsString;
    String rhsString;
    int lhsInt;
    int rhsInt;
    Double lhsDouble;
    Double rhsDouble;

    if (_comparingAttribute.equals(ComponentTypeComparatorEnum.REFERENCE_DESIGNATOR))
    {
      lhsString = lhs.getReferenceDesignator();
      rhsString = rhs.getReferenceDesignator();
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.X_COORDINATE))
    {
      lhsInt = lhs.getCoordinateInNanoMeters().getX();
      rhsInt = rhs.getCoordinateInNanoMeters().getX();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.Y_COORDINATE))
    {
      lhsInt = lhs.getCoordinateInNanoMeters().getY();
      rhsInt = rhs.getCoordinateInNanoMeters().getY();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.DEGREES_ROTATION))
    {
      lhsDouble = lhs.getDegreesRotationRelativeToBoard();
      rhsDouble = rhs.getDegreesRotationRelativeToBoard();
      if (_ascending)
        return lhsDouble.compareTo(rhsDouble);
      else
        return rhsDouble.compareTo(lhsDouble);
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.JOINT_TYPE))
    {
      lhsString = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      rhsString = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      if (lhs.getCompPackage().usesOneJointTypeEnum())
        lhsString = lhs.getCompPackage().getJointTypeEnum().getName();
      if (rhs.getCompPackage().usesOneJointTypeEnum())
        rhsString = rhs.getCompPackage().getJointTypeEnum().getName();
      if(_ascending)
        return lhsString.compareToIgnoreCase(rhsString);
      else
        return rhsString.compareToIgnoreCase(lhsString);
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.SUBTYPE))
    {
      // sort order for subtypes is:
      //   Untestable
      //   No Test
      //   Partial Test
      //   No Load
      //   Mixed
      //   other subtypes sorted alphanumerically

      int lhsState = -1;
      lhsString = "";
      if (lhs.getComponentTypeSettings().areAllPadTypesNotTestable())
        lhsState = _UNTESTABLE;
      else if (lhs.isTestable() == false)
        lhsState = _PARTIAL_UNTESTABLE;
      else if (lhs.getComponentTypeSettings().isLoaded() == false)
        lhsState = _NO_LOAD;
      else if (lhs.getComponentTypeSettings().isInspected() == false)
        lhsState = _NO_TEST;
      else if (lhs.getComponentTypeSettings().areAllPadTypesInspectedFlagsSet() == false)
        lhsState = _PARTIAL_TEST;
      else if (lhs.getComponentTypeSettings().usesOneSubtype())
      {
        lhsState = _SUBTYPE;
        lhsString = lhs.getComponentTypeSettings().getSubtype().toString();
      }
      else
        lhsState = _MIXED;

      int rhsState = -1;
      rhsString = "";
      if (rhs.getComponentTypeSettings().areAllPadTypesNotTestable())
        rhsState = _UNTESTABLE;
      else if (rhs.isTestable() == false)
        rhsState = _PARTIAL_UNTESTABLE;
      else if (rhs.getComponentTypeSettings().isLoaded() == false)
        rhsState = _NO_LOAD;
      else if (rhs.getComponentTypeSettings().isInspected() == false)
        rhsState = _NO_TEST;
      else if (rhs.getComponentTypeSettings().areAllPadTypesInspectedFlagsSet() == false)
        rhsState = _PARTIAL_TEST;
      else if (rhs.getComponentTypeSettings().usesOneSubtype())
      {
        rhsState = _SUBTYPE;
        rhsString = rhs.getComponentTypeSettings().getSubtype().toString();
      }
      else
        rhsState = _MIXED;

      if (_ascending)
      {
        switch (lhsState)
        {
          case _UNTESTABLE:
          {
            if (rhsState == _UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _PARTIAL_UNTESTABLE:
          {
            if (rhsState == _UNTESTABLE)
              return 1;
            if (rhsState == _PARTIAL_UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _NO_TEST:
          {
            if (rhsState == _UNTESTABLE || rhsState == _PARTIAL_UNTESTABLE)
              return 1;
            if (rhsState == _NO_TEST)
              return 0;
            else
              return -1;
          }
          case _PARTIAL_TEST:
          {
            if (rhsState == _UNTESTABLE || rhsState == _PARTIAL_UNTESTABLE || rhsState == _NO_TEST)
              return 1;
            else if (rhsState == _NO_LOAD)
              return 0;
            else
              return -1;
          }
          case _NO_LOAD:
          {
            if (rhsState == _UNTESTABLE || rhsState == _PARTIAL_UNTESTABLE || rhsState == _NO_TEST || rhsState == _PARTIAL_TEST)
              return 1;
            else if (rhsState == _NO_LOAD)
              return 0;
            else
              return -1;
          }
          case _MIXED:
          {
            if (rhsState == _UNTESTABLE || rhsState == _PARTIAL_UNTESTABLE || rhsState == _NO_TEST || rhsState == _PARTIAL_TEST || rhsState == _NO_LOAD)
              return 1;
            else if (rhsState == _MIXED)
              return 0;
            else
              return -1;
          }
          case _SUBTYPE:
          {
            if (rhsState == _UNTESTABLE || rhsState == _PARTIAL_UNTESTABLE || rhsState == _NO_TEST || rhsState == _PARTIAL_TEST || rhsState == _NO_LOAD || rhsState == _MIXED)
              return 1;
            else
              return lhsString.compareTo(rhsString);
          }
          default:
          {
            Assert.expect(false);
            return 0;
          }
        }
      }
      else
      {
        switch (rhsState)
        {
          case _UNTESTABLE:
          {
            if (lhsState == _UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _PARTIAL_UNTESTABLE:
          {
            if (lhsState == _UNTESTABLE)
              return 1;
            if (lhsState == _PARTIAL_UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _NO_TEST:
          {
            if (lhsState == _UNTESTABLE || lhsState == _PARTIAL_UNTESTABLE)
              return 1;
            if (lhsState == _NO_TEST)
              return 0;
            else
              return -1;
          }
          case _PARTIAL_TEST:
          {
            if (lhsState == _UNTESTABLE || lhsState == _PARTIAL_UNTESTABLE || lhsState == _NO_TEST)
              return 1;
            else if (lhsState == _NO_LOAD)
              return 0;
            else
              return -1;
          }
          case _NO_LOAD:
          {
            if (lhsState == _UNTESTABLE || lhsState == _PARTIAL_UNTESTABLE || lhsState == _NO_TEST || lhsState == _PARTIAL_TEST)
              return 1;
            else if (lhsState == _NO_LOAD)
              return 0;
            else
              return -1;
          }
          case _MIXED:
          {
            if (lhsState == _UNTESTABLE || lhsState == _PARTIAL_UNTESTABLE || lhsState == _NO_TEST || lhsState == _PARTIAL_TEST || lhsState == _NO_LOAD)
              return 1;
            else if (lhsState == _MIXED)
              return 0;
            else
              return -1;
          }
          case _SUBTYPE:
          {
            if (lhsState == _UNTESTABLE || lhsState == _PARTIAL_UNTESTABLE || lhsState == _NO_TEST || lhsState == _PARTIAL_TEST || lhsState == _NO_LOAD || lhsState == _MIXED)
              return 1;
            else
              return rhsString.compareTo(lhsString);
          }
          default:
          {
            Assert.expect(false);
            return 0;
          }
        }
      }
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.PACKAGE))
    {
      lhsString = lhs.getCompPackage().getShortName();
      rhsString = rhs.getCompPackage().getShortName();
      if (_ascending)
        return lhsString.compareTo(rhsString);
      else
        return rhsString.compareTo(lhsString);
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.ARTIFACT_COMPENSATION))
    {
      if(lhs.usesOneArtifactCompensationSetting() == false)
        lhsString = _MIXED_STRING;
      else
        lhsString = lhs.getArtifactCompensationSetting().toString();

      if(rhs.usesOneArtifactCompensationSetting() == false)
        rhsString = _MIXED_STRING;
      else
        rhsString = rhs.getArtifactCompensationSetting().toString();

      if (_ascending)
        return lhsString.compareTo(rhsString);
      else
        return rhsString.compareTo(lhsString);

    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.SIGNAL_COMPENSATION))
    {
      if(lhs.usesOneIntegrationLevelSetting() == false)
        lhsString = _MIXED_STRING;
      else
        lhsString = lhs.getSignalCompensationLevel().toString();

      if(rhs.usesOneIntegrationLevelSetting() == false)
        rhsString = _MIXED_STRING;
      else
        rhsString = rhs.getSignalCompensationLevel().toString();

      if (_ascending)
        return lhsString.compareTo(rhsString);
      else
        return rhsString.compareTo(lhsString);

    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.GSM_COMPENSATION))
    {
      lhsString = lhs.getGlobalSurfaceModel().toString();
      rhsString = rhs.getGlobalSurfaceModel().toString();

      if (_ascending)
        return lhsString.compareTo(rhsString);
      else
        return rhsString.compareTo(lhsString);
    }
    else if (_comparingAttribute.equals(ComponentTypeComparatorEnum.USER_GAIN))
    {
      lhsString = lhs.getUserGain().toString();
      rhsString = rhs.getUserGain().toString();
      if (_ascending)
      {
        return lhsString.compareTo(rhsString);
      }
      else
      {
        return rhsString.compareTo(lhsString);
      }
    }
    else
    {
      Assert.expect(false, "Please add your new component attribute to sort on to this method");
      return 0;
    }
  }*/
}
