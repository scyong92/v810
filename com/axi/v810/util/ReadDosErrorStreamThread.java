package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * This thread is used by the DosShell class and grabs any
 * characters from its standard error stream it as soon as they appear.
 * Each line of input that is found will be sent to the DosOutputInt.standardErrorLine(String line)
 * method.
 *
 * @see DosShell
 * @author Bill Darbie
 */
class ReadDosErrorStreamThread extends Thread
{
  private static final int _SLEEP_TIME = 200;
  private BufferedReader _is;
  private DosOutputInt _dosOutputInt;
  private boolean _run = true;
  private WorkerThread _workerThread = new WorkerThread("ReadDosErrorStreamWorkerThread");

  /**
   * Contruct this class
   *
   * @author Bill Darbie
   */
  ReadDosErrorStreamThread(BufferedReader is, DosOutputInt dosOutputInt)
  {
    Assert.expect(is != null);
    // dosOutputInt can be null

    _is = is;
    _dosOutputInt = dosOutputInt;
  }

  /**
  * This method will cause this thread to exit
  *
  * @author Bill Darbie
  */
  void stopThread()
  {
    _run = false;
  }

  /**
   * This method gets called when the thread is run with the start method.
   *
   * @author Bill Darbie
   */
  public void run()
  {
    try
    {
      StringBuffer stringBuffer = new StringBuffer("");
      while(_run)
      {
        if (_is.ready())
        {
          char ch = (char)_is.read();
          stringBuffer.append(ch);
          if (ch == '\n')
          {
            // put the standardErrorLine() call on another thread to make sure that we can keep reading
            // characters off the error stream as fast as they arrive
            String nonFilteredInput = stringBuffer.toString();
            // dos ends every line with a \r\n - replace this with \n
            final String error = nonFilteredInput.substring(0, nonFilteredInput.length() - 2) + "\n";
            _workerThread.invokeLater(new Runnable()
            {
              public void run()
              {
                if (_dosOutputInt != null)
                  _dosOutputInt.standardErrorLine(error);
              }
            });
            stringBuffer.setLength(0);
          }
        }
        else
        {
          try
          {
            Thread.sleep(_SLEEP_TIME);
          }
          catch(InterruptedException e)
          {
            // do nothing
          }
        }
      }
    }
    catch(Throwable throwable)
    {
      Assert.logException(throwable);
    }
  }
}
