package com.axi.v810.util;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;

/**
 * Test FileLoggerAxi exception throwing.  All other functionality tested
 * in regression test for FileLogger.
 *
 * @author John Dutton
 */
public class Test_FileLoggerAxi extends UnitTest
{
  /**
   * @author John Dutton
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_FileLoggerAxi());
  }

  /**
   * @author John Dutton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // See that FileLoggerAxi.append throws only a DatastoreException.  If it throws
    // something else, a compile time error will occur.
    try
    {
      String logFileFullPathName = getTestDataDir() + File.separator +
                                    "testFileLogger.log";
      FileLoggerAxi fileLogger = new FileLoggerAxi(logFileFullPathName);
      fileLogger.append("foobar");
      if (FileUtilAxi.exists(logFileFullPathName))
        FileUtilAxi.delete(logFileFullPathName);
    }
    catch (DatastoreException ioe)
    {
      // do nothing
    }
  }
}
