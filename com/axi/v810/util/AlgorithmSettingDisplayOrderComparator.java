package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;

/**
 * This comparator will sort a Component by it's overscanned property
 * @author Andy Mechtenberg
 */
public class AlgorithmSettingDisplayOrderComparator implements Comparator<AlgorithmSetting>
{
  /**
   * Sorts the incoming AlgorithmSetting based on it's display order
   * @author Andy Mechtenberg
   */
  public AlgorithmSettingDisplayOrderComparator()
  {
    // do nothing
  }

  /**
   * @author Andy Mechtenberg
   */
  public int compare(AlgorithmSetting lhAlgorithmSetting, AlgorithmSetting rhAlgorithmSetting)
  {
    Assert.expect(lhAlgorithmSetting != null);
    Assert.expect(rhAlgorithmSetting != null);

    int lhDisplayOrder = lhAlgorithmSetting.getDisplayOrder();
    int rhDisplayOrder = rhAlgorithmSetting.getDisplayOrder();

    if (lhDisplayOrder == rhDisplayOrder)
      return 0;

    if (lhDisplayOrder > rhDisplayOrder)
      return 1;
    else
      return -1;
  }
}
