package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;

/**
 * This class is a wrapper class for the com.axi.util.FileUtil class.  It converts all the non 5dx exceptions
 * thrown by the FileUtil class into DatastoreExceptions.
 *
 * @author Bill Darbie
 */
public class FileUtilAxi
{
  /**
   * @author Bill Darbie
   */
  public static boolean exists(String fileOrDirectoryName)
  {
    return FileUtil.exists(fileOrDirectoryName);
  }

  /**
   * @author John Dutton
   */
  public static boolean existsDirectory(String directoryName)
  {
    return FileUtil.existsDirectory(directoryName);
  }

  /**
   * @author Andy Mechtenberg
   */
  public static boolean existsDirectoryAndAbleToReadWrite(String directoryName)
  {
    return FileUtil.existsDirectoryAndAbleToReadWrite(directoryName);
  }

  /**
   * This method will set a abort flag that will stop the zip process.
   * @author Erica Wheatcroft
   */
  public static void cancelZip()
  {
    FileUtil.cancelZip();
  }

  /**
   * @author Bill Darbie
   */
  public static void copy(String fromFile, String toFile) throws DatastoreException
  {
    try
    {
      FileUtil.copy(fromFile, toFile);
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Split one file into many smaller ones.  Each smaller file will be no larger than sizeInBytesPerFile.
   * The output file name will match the original file with _1, _2, ... appended.
   * @return a List of output files
   * @author Bill Darbie
   */
  public static List<String> splitFile(String fileName, int maxOutFileSizeInBytes) throws DatastoreException
  {
    try
    {
      return FileUtil.splitFile(fileName, maxOutFileSizeInBytes);
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException ex2 = new CannotCreateFileDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }
    catch (CouldNotReadFileException ex)
    {
      CannotReadDatastoreException ex2 = new CannotReadDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }
    catch (CouldNotWriteFileException ex)
    {
      CannotWriteDatastoreException ex2 = new CannotWriteDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }
    catch (CouldNotCloseFileException ex)
    {
      CannotCloseFileDatastoreException ex2 = new CannotCloseFileDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }
  }

  /**
   * concatenate the contents of inFile1 and inFile2 to create the concatFile.
   * @author Bill Darbie
   */
  public static void concat(String inFile1, String inFile2, String concatFile) throws FileNotFoundDatastoreException,
                                                                                      CannotCreateFileDatastoreException
  {
    try
    {
      FileUtil.concat(inFile1, inFile2, concatFile);
    }
    catch (FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException ex2 = new CannotCreateFileDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }
  }

  /**
   * Copy from fromFile to toFile and preserve the time stamp
   * @author Bill Darbie
   */
  public static void copyPreserveTimeStamp(String fromFile, String toFile) throws DatastoreException
  {
    try
    {
      FileUtil.copyPreserveTimeStamp(fromFile, toFile);
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Copy all the files contained in fromDirectory over to toDirectory.  Subdirectories
   * are not copied.  Both fromDirectory and toDirectory must already exist.
   *
   * @author Bill Darbie
   */
  public static void copyDirectory(String fromDirectory, String toDirectory) throws DatastoreException
  {
    try
    {
      FileUtil.copyDirectory(fromDirectory, toDirectory);
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Copy all the files contained in fromDirectory over to toDirectory without changing any time stamps.  Subdirectories
   * are not copied.  Both fromDirectory and toDirectory must already exist.
   *
   * @author Bill Darbie
   */
  public static void copyDirectoryPreserveTimeStamp(String fromDirectory, String toDirectory) throws DatastoreException
  {
    try
    {
      FileUtil.copyDirectoryPreserveTimeStamp(fromDirectory, toDirectory);
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursively(String fromDirectory, String toDirectory) throws DatastoreException
  {
    try
    {
      FileUtil.copyDirectoryRecursively(fromDirectory, toDirectory);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively without changing any time stamps.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursivelyPreserveTimeStamp(String fromDirectory, String toDirectory) throws DatastoreException
  {
    try
    {
      FileUtil.copyDirectoryRecursivelyPreserveTimeStamp(fromDirectory, toDirectory);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively except for the files
   * or directory listed in the dontCopyFilesOrDirectories List.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursively(String fromDirectory,
                                              String toDirectory,
                                              List<String> dontCopyFilesOrDirectories) throws DatastoreException
  {
    try
    {
      FileUtil.copyDirectoryRecursively(fromDirectory, toDirectory, dontCopyFilesOrDirectories);
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Copy all files and directories in fromDirectory to toDirectory recursively without changing
   * any time stamps except for the files or directory listed in the dontCopyFilesOrDirectories List.
   * Both fromDirectory and toDirectory must already exist.
   * @author Bill Darbie
   */
  public static void copyDirectoryRecursivelyPreserveTimeStamp(String fromDirectory,
                                                               String toDirectory,
                                                               List<String> dontCopyFilesOrDirectories) throws DatastoreException
  {
    try
    {
      FileUtil.copyDirectoryRecursivelyPreserveTimeStamp(fromDirectory, toDirectory, dontCopyFilesOrDirectories);
    }
    catch(CouldNotCopyFileException ex)
    {
      CannotCopyFileDatastoreException dex = new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
      dex.initCause(ex);
      throw dex;
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Stop copying the directory contents when this is called.  The files that were already copied
   * will be left where they are.
   * @author Bill Darbie
   */
  public static void cancelDirectoryCopy()
  {
    FileUtil.cancelDirectoryCopy();
  }

  /**
   * Move all the files and directories in the fromDirectory to the toDirectory.
   * Both the fromDirectory and the toDirectory must exist before this method is called.
   * @author Bill Darbie
   */
  public static void moveDirectoryContents(String fromDirectory, String toDirectory) throws DatastoreException
  {
    try
    {
      FileUtil.moveDirectoryContents(fromDirectory, toDirectory);
    }
    catch(CouldNotRenameFileException ex)
    {
      FileCannotBeRenamedDatastoreException dex = new FileCannotBeRenamedDatastoreException(ex.getFromFileName(), ex.getToFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Move all the files and directories in the fromDirectory to the toDirectory except for
   * the files listed in dontMoveFiles.
   * Both the fromDirectory and the toDirectory must exist before this method is called.
   * @author Bill Darbie
   */
  public static void moveDirectoryContents(String fromDirectory,
                                           String toDirectory,
                                           List<String> dontMoveDirOrFiles) throws DatastoreException
  {
    try
    {
      FileUtil.moveDirectoryContents(fromDirectory, toDirectory, dontMoveDirOrFiles);
    }
    catch(CouldNotRenameFileException ex)
    {
      FileCannotBeRenamedDatastoreException dex = new FileCannotBeRenamedDatastoreException(ex.getFromFileName(), ex.getToFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * @return a List of String of all files and directories in the directory passed in, including
   *         all subdirectories.
   * @author Bill Darbie
   */
  public static Collection<String> listAllFilesAndDirectoriesFullPath(String directoryName)
  {
    return FileUtil.listAllFilesAndDirectoriesFullPath(directoryName);
  }

  /**
   * @return a List of String of all files in the directory passed in.  Subdirectories will
   * not be listed.  The file names (not path and name) will be returned.
   * @author Bill Darbie
   */
  public static Collection<String> listAllFilesInDirectory(String directoryName)
  {
    return FileUtil.listAllFilesInDirectory(directoryName);
  }

  /**
   * @return a List of String of all files in the directory passed in.  Subdirectories will
   * not be listed.
   * @author Bill Darbie
   */
  public static Collection<String> listAllFilesFullPathInDirectory(String directoryName)
  {
    return FileUtil.listAllFilesFullPathInDirectory(directoryName);
  }

  /**
   * @return a List of String of all subdirectories in the directory passed in.
   * The file names (not path and name) will be returned.
   * @author Bill Darbie
   */
  public static Collection<String> listAllSubDirectoriesInDirectory(String directoryName)
  {
    return FileUtil.listAllSubDirectoriesInDirectory(directoryName);
  }

  /**
   * @return a List of String of all subdirectories in the directory passed in.
   * @author Bill Darbie
   */
  public static Collection<String> listAllSubDirectoriesFullPathInDirectory(String directoryName)
  {
    return FileUtil.listAllSubDirectoriesFullPathInDirectory(directoryName);
  }

  /**
   * @author George A. David
   */
  public static void delete(List<String> paths) throws DatastoreException
  {
    Assert.expect(paths != null);

    for(String path : paths)
      delete(path);
  }

  /**
   * Delete the file or directory.  Note that java.io.File.delete() only works if a file is passed
   * to it.  This method works on either files or directories.
   * @author Bill Darbie
   */
  public static void delete(String fileOrDirectoryName) throws DatastoreException
  {
    try
    {
      FileUtil.delete(fileOrDirectoryName);
    }
    catch(CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }
  
  public static void deleteFileOrDirectory(String fileOrDirectoryName) throws DatastoreException
  {
    try
    {
      FileUtil.deleteFileOrDirectory(fileOrDirectoryName);
    }
    catch(CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * This method will delete a file that is a MappedByteBuffer.
   * A MappedByteBuffer may not be immediately available to be deleted.  see Sun bug id 4715154.
   * This method will try several times to delete before throwing an exception to get around
   * the problem.
   *
   * NOTE: If your code is holding on to a reference that will not allow the MappedByteBuffer to be
   *       garbage collected, this call will always fail.
   *
   * @author Bill Darbie
   */
  public static void deleteMappedByteBufferFile(String fileName) throws DatastoreException
  {
    try
    {
      FileUtil.deleteMappedByteBufferFile(fileName);
    }
    catch(CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static void deleteFileOrDirectoryByExternalProcessIfExists(String fileOrDirectoryName) throws DatastoreException
  {
    Assert.expect(fileOrDirectoryName != null);
    
    // XCR-3302 Unable to delete result folder after complete production run
    if (FileUtil.exists(FileName.getDeleteFolderAndFileExeFullPath()) == false)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(FileName.getDeleteFolderAndFileExeFullPath());
      throw dex;
    }
    
    // XCR-3302 Unable to delete result folder after complete production run
    if (FileUtil.exists(fileOrDirectoryName) == false)
      return;
    
    ExitCodeEnum exitCode = ExitCodeEnum.NO_ERRORS;
    try
    {
      exitCode = RuntimeExec.getInstance().execProcess(FileName.getDeleteFolderAndFileExeFullPath(), fileOrDirectoryName);
    }
    catch (Exception ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(fileOrDirectoryName);
      dex.initCause(ex);
      throw dex;
    }
    
    if(exitCode != ExitCodeEnum.NO_ERRORS)
    {
      throw new CannotDeleteFileDatastoreException(fileOrDirectoryName);
    }
  }

  /**
   * Delete the contents of the directory but not the directory itself.
   * See also delete().
   * @author Vincent Wong
   */
  public static void deleteDirectoryContents(String dirName) throws DatastoreException
  {
    try
    {
      FileUtil.deleteDirectoryContents(dirName);
    }
    catch(CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Delete all contents of the directory passed in with the exception of the
   * files or directories in the dontDeleteFilesOrDirectories List.
   * @author Bill Darbie
   */
  public static void deleteDirectoryContents(String dirName,
                                             List<String> dontDeleteFilesOrDirectories) throws DatastoreException
  {
    try
    {
      FileUtil.deleteDirectoryContents(dirName, dontDeleteFilesOrDirectories);
    }
    catch(CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Rename the file to the new name.  If a file with the new name already exists it will
   * be replaced.  Note that java.io.File.renameTo() will fail rather then replace an existing
   * file.
   *
   * @author Bill Darbie
   */
  public static void rename(String fromFileOrDir, String toFileOrDir) throws DatastoreException
  {
    try
    {
      FileUtil.rename(fromFileOrDir, toFileOrDir);
    }
    catch(CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(CouldNotRenameFileException ex)
    {
      FileCannotBeRenamedDatastoreException dex = new FileCannotBeRenamedDatastoreException(ex.getFromFileName(), ex.getToFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fromFileOrDir);
      dex.initCause(ex);
      throw dex; // unable to return a valid value
    }
  }

  /**
   * See the method description below - this one is the same except it uses the default compression setting.
   * @author Bill Darbie
   */
  public static void zip(String inputFileOrDirectoryName, String zipName, String relativePath) throws DatastoreException
  {
    try
    {
      FileUtil.zip(inputFileOrDirectoryName, zipName, relativePath);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(NotEnoughDiskSpaceException ex)
    {
      NotEnoughDiskSpaceDatastoreException dex = new NotEnoughDiskSpaceDatastoreException(ex.getParentPath(), ex.getBaseFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Zip the contents of fileOrDirectoryName and put the result into zipName.  If fileOrDirectoryName
   * is a file then just that file will be zipped.  If fileOrDirectoryName is a directory then
   * all the files and directories (recursively) will be zipped.  The zip will contain the path
   * of the files relative to the relativePath parameter.
   *
   * @param inputFileOrDirectoryName is the name of the file or directory of what should be zipped
   * @param zipName is the zip file that will be created
   * @param relativePath is the relativePath that the contents of the zip will be relative to.  This
   *                     must be the path contained by inputFileOrDirectoryName.  An empty string will
   *                     cause the full path to be put into the zip (not recommended)
   * @param compressionLevel is the level of compression (1-9) 1 is the least compressed and fastest, 9 is
   *                         the best compression and slowest
   *
   * @author Bill Darbie
   */
  public static void zip(String inputFileOrDirectoryName,
                         String zipName,
                         String relativePath,
                         int compressionLevel) throws DatastoreException
  {
    try
    {
      FileUtil.zip(inputFileOrDirectoryName, zipName, relativePath, compressionLevel);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(NotEnoughDiskSpaceException ex)
    {
      NotEnoughDiskSpaceDatastoreException dex = new NotEnoughDiskSpaceDatastoreException(ex.getParentPath(), ex.getBaseFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * See the method description below - this one is the same except it uses the default compression setting.
   * @author Bill Darbie
   */
  public static void zip(List<String> fileOrDirectoryNames, String zipName, String relativePath, boolean excludeHiddenFiles) throws DatastoreException
  {
    try
    {
      FileUtil.zip(fileOrDirectoryNames, zipName, relativePath, excludeHiddenFiles);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Zip the contents of fileOrDirectoryNames passed in through the List of Strings
   * and put the result into zipName.  If fileOrDirectoryName
   * is a file then just that file will be zipped.  If fileOrDirectoryName is a directory then
   * all the files and directories (recursively) will be zipped.  The files in the zip file will
   * all be relative to the path specified in relativePath.  The path specified by relativePath must
   * be in the name of all files and directories in the fileOrDirectoryNames List.
   *
   * @param fileOrDirectoryNames is the name of the file or directory of what should be zipped
   * @param zipName is the zip file that will be created
   * @param relativePath is the relativePath that the contents of the zip will be relative to.  This
   *                     must be the path contained by inputFileOrDirectoryName.  An empty string will
   *                     cause the full path to be put into the zip (not recommended)
   * @param compressionLevel is the level of compression (1-9) 1 is the least compressed and fastest, 9 is
   *                         the best compression and slowest
   *
   * @author Bill Darbie
   */
  public static void zip(List<String> fileOrDirectoryNames,
                         String zipName,
                         String relativePath,
                         int compressionLevel) throws DatastoreException
  {
    try
    {
      FileUtil.zip(fileOrDirectoryNames, zipName, relativePath, compressionLevel);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * adds files to a zip file with a new relative path.
   * @author George A. David
   */
  public static void zipWithNewRelativePath(Map<String, List<String>> newRelativePathToPathsMap, String zipName, boolean append) throws DatastoreException
  {
    try
    {
      FileUtil.zipWithNewRelativePath(newRelativePathToPathsMap, zipName, append);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }


  /**
   * Unzip the zipFile into unzipDirectory.  The unzip will create any directories that it needs to.
   * If a file already exists that is in the zipped file, the unzip will overwrite it.
   *
   * @param zipFile is the file to be unzipped
   * @param unzipDirectory is the directory to unzip the file into
   *
   * @author Bill Darbie
   */
  public static void unZip(String zipFile, String unzipDirectory, boolean retainOriginalFileTimeStamps) throws DatastoreException
  {
    try
    {
      FileUtil.unZip(zipFile, unzipDirectory, retainOriginalFileTimeStamps);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(CouldNotCreateDirectoryException ex)
    {
      CannotCreateDirectoryDatastoreException dex = new CannotCreateDirectoryDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(BadFileFormatException ex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
  }


  /**
   * Get the list of file names and directory names zipped in the zip file.
   *
   * @param zipFile is the file to be examined.
   * @author Vincent Wong
   */
  public static Collection<String> getZipFileContents(String zipFile) throws DatastoreException
  {
    try
    {
      return FileUtil.getZipFileContents(zipFile);
    }
    catch(FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(BadFileFormatException ex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Return the extension with the dot.
   * @author Vincent Wong
   */
  public static String getExtension(String fileName)
  {
    return FileUtil.getExtension(fileName);
  }

  /**
   * Return the base name of the file (without the extension).
   * @author Vincent Wong
   */
  public static String getNameWithoutExtension(String fileName)
  {
    return FileUtil.getNameWithoutExtension(fileName);
  }

  /**
   * Return the base name with the leading path name of the file
   * (without the extension).
   * @author Vincent Wong
   */
  public static String getNameWithoutExtensionWithPath(String fileName)
  {
    return FileUtil.getNameWithoutExtensionWithPath(fileName);
  }

  /**
   * Return the base name with the leading path name of the file.
   * @author Patrick Lacz
   */
  public static String getNameWithoutPath(String fileName)
  {
    return FileUtil.getNameWithoutPath(fileName);
  }

  /**
   * Return the base path with the trailing file name.
   * @author Patrick Lacz
   */
  public static String getPathWithoutFileName(String fileNameWithPath)
  {
    return FileUtil.getPathWithoutFileName(fileNameWithPath);
  }


  /**
   * Return the parent directory name of fileName. The method returns an empty
   * string if the fileName does not have a parent.
   *
   * @author Vincent Wong
   */
  public static String getParent(String fileName)
  {
    return FileUtil.getParent(fileName);
  }

  /**
   * Return the fileName without a file separator at the end, if any.
   *
   * @author Vincent Wong
   */
  public static String removeEndFileSeparator(String fileName)
  {
    return FileUtil.removeEndFileSeparator(fileName);
  }

  /**
   * @author Bill Darbie
   */
  public static int getFloppySizeInBytes()
  {
    return FileUtil.getFloppySizeInBytes();
  }

  /**
   * Creates the directory named by this abstract pathname.
   * @author Bill Darbie
   */
  public static void mkdir(String dirName) throws DatastoreException
  {
    try
    {
      FileUtil.mkdir(dirName);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
  }


  /**
   * Creates the directory named by this abstract pathname, including any
   * necessary but nonexistent parent directories.
   * @author Bill Darbie
   */
  public static void mkdirs(String dirName) throws DatastoreException
  {
    try
    {
      FileUtil.mkdirs(dirName);
    }
    catch(CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch(FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Reads the entire content of the file into a byte array.
   * @return the byte array read.
   * @author George A. David
   */
  public static byte[] readBytes(String filePath) throws DatastoreException
  {
    byte[] fileBytes = null;
    try
    {
      fileBytes = FileUtil.readBytes(filePath);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(fnfe);
      throw dex;
    }
    catch (IOException ioe)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ioe);
      throw dex;
    }

    Assert.expect(fileBytes != null);
    return fileBytes;
  }

  /**
   * Save the object passed in as a serialized file with the name passed in.  If this method failes
   * to write the new file, any pre-existing file will be left in its original state.
   * @author Bill Darbie
   */
  public static void saveObjectToSerializedFile(Object object, String fileName) throws DatastoreException
  {
    try
    {
      FileUtil.saveObjectToSerializedFile(object, fileName);
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotRenameFileException ex)
    {
      FileCannotBeRenamedDatastoreException dex = new FileCannotBeRenamedDatastoreException(ex.getFromFileName(), ex.getToFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotDeleteFileException ex)
    {
      CannotDeleteFileDatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch (FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
  }
  
  /**
   * @author Bill Darbie
   */
  public static Object loadObjectFromSerializedFile(String fileName) throws DatastoreException
  {
    Object object = null;
    try
    {
      object = FileUtil.loadObjectFromSerializedFile(fileName);
    }
    catch (FileNotFoundException ex)
    {
      CannotOpenFileDatastoreException dex = new CannotOpenFileDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    catch (ClassNotFoundException ex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }
    catch (IOException ex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(fileName);
      dex.initCause(ex);
      throw dex;
    }

    return object;
  }

  /**
   * Creates a duplicate of the file passed in by
   * copying it and changing its extension to .backup
   * @author George A. David
   */
  public static void backupFile(String fileName) throws DatastoreException
  {
    try
    {
      FileUtil.backupFile(fileName);
    }
    catch (FileDoesNotExistException fdne)
    {
      throw new FileNotFoundDatastoreException(fdne.getFileName());
    }
    catch (CouldNotCopyFileException ex)
    {
      throw new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
    }
  }

  /**
   * Restore a backup file. The file name is the name prior to backing up.
   * @author George A. David
   */
  public static void restoreBackupFile(String fileName) throws DatastoreException
  {
    try
    {
      FileUtil.restoreBackupFile(fileName);
    }
    catch (FileDoesNotExistException fdne)
    {
      throw new FileNotFoundDatastoreException(fdne.getFileName());
    }
    catch (CouldNotCopyFileException ex)
    {
      throw new CannotCopyFileDatastoreException(ex.getFromFile(), ex.getToFile());
    }
  }

  /**
   * This method compares text or binary files to determine if they are identical or not.
   * @return true if the two files are identical, false otherwise.
   * @author Bill Darbie
   */
  public static boolean areFilesIdentical(String file1, String file2) throws FileNotFoundDatastoreException,
                                                                             CannotReadDatastoreException
  {
    boolean identical = true;
    try
    {
      identical = FileUtil.areFilesIdentical(file1, file2);
    }
    catch (FileDoesNotExistException ex)
    {
      FileNotFoundDatastoreException ex2 = new FileNotFoundDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }
    catch (CouldNotReadFileException ex)
    {
      CannotReadDatastoreException ex2 = new CannotReadDatastoreException(ex.getFileName());
      ex2.initCause(ex);
      throw ex2;
    }

    return identical;
  }

  /**
   * Create a unique temporary directory in the directory specified.
   * @author Bill Darbie
   */
  public static String createTempDir(String dirName) throws DatastoreException
  {
    try
    {
      return FileUtil.createTempDir(dirName);
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotCreateFileException ex)
    {
      CannotCreateFileDatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Given a zip file path, calculate the size of the uncompressed data.
   *
   * @param zipFilePath the path to the zip file.
   * @author George A. David
   */
  public static long getUncompressedZipFileSizeInBytes(String zipFilePath) throws DatastoreException
  {
    long uncompressedZipFileSizeInBytes = 0;
    try
    {
      // get the uncompressed file size
      uncompressedZipFileSizeInBytes = FileUtil.getUncompressedZipFileSizeInBytes(zipFilePath);
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(zipFilePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (BadFileFormatException ex)
    {
      FileCorruptDatastoreException dex = new FileCorruptDatastoreException(zipFilePath);
      dex.initCause(ex);
      throw dex;
    }

    return uncompressedZipFileSizeInBytes;
  }

  /**
   * create the directory
   * @param path the path to create
   * @author George A. David
   */
  public static void createDirectory(String path) throws DatastoreException
  {
    try
    {
      FileUtil.createDirectory(path);
    }
    catch (CouldNotCreateFileException ex)
    {
      DatastoreException dex = new CannotCreateDirectoryDatastoreException(path);
      dex.initCause(ex);
      throw dex;
    }
  }
  /**
   * @author Bill Darbie
   */
  public static String getUniqueTempFile(String fileName)
  {
    return FileUtil.getUniqueTempFile(fileName);
  }

  /**
   * @author Laura Cormos
   */
  public static boolean isDirectoryPathFileSystemLegal(String fileOrDirectoryName)
  {
    return FileUtil.isDirectoryPathFileSystemLegal(fileOrDirectoryName);
  }

  /**
   * @author Bill Darbie
   */
  public static void createEmptyFile(String fileName) throws DatastoreException
  {
    try
    {
      FileUtil.createEmtpyFile(fileName);
    }
    catch (IOException ioe)
    {
      CannotCreateFileDatastoreException ex = new CannotCreateFileDatastoreException(fileName);
      ex.initCause(ioe);
      throw ex;
    }
  }
}
