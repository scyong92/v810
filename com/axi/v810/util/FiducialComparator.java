package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.util.*;

/**
 * @author Laura Cormos
 */
public class FiducialComparator implements Comparator<Fiducial>
{
  private AlphaNumericComparator _alphaComparator;
  private FiducialComparatorEnum _comparingAttribute;
  private boolean _ascending;

  /**
    * @author Laura Cormos
    */
   public FiducialComparator(boolean ascending, FiducialComparatorEnum comparingAttribute)
   {
     Assert.expect(comparingAttribute != null);
     _ascending = ascending;
     _comparingAttribute = comparingAttribute;
     _alphaComparator = new AlphaNumericComparator(_ascending);
  }

  /**
   * @author Laura Cormos
   */
  public int compare(Fiducial lhs, Fiducial rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsString;
    String rhsString;
    int lhsInt;
    int rhsInt;
    Double lhsDouble;
    Double rhsDouble;

    if (_comparingAttribute.equals(FiducialComparatorEnum.FULLY_QUALIFIED_NAME))
    {
      lhsString = lhs.getName();
      rhsString = rhs.getName();
      if (lhs.isOnBoard())
      {
        lhsString = lhs.getSideBoard().getBoard().getName() + lhsString;
      }
      if (rhs.isOnBoard())
      {
        rhsString = rhs.getSideBoard().getBoard().getName() + rhsString;
      }
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(FiducialComparatorEnum.NAME))
    {
      lhsString = lhs.getName();
      rhsString = rhs.getName();
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(FiducialComparatorEnum.X_COORDINATE))
    {
      lhsInt = lhs.getFiducialType().getCoordinateInNanoMeters().getX();
      rhsInt = rhs.getFiducialType().getCoordinateInNanoMeters().getX();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(FiducialComparatorEnum.Y_COORDINATE))
    {
      lhsInt = lhs.getFiducialType().getCoordinateInNanoMeters().getY();
      rhsInt = rhs.getFiducialType().getCoordinateInNanoMeters().getY();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(FiducialComparatorEnum.WIDTH))
    {
      lhsInt = lhs.getWidthInNanoMeters();
      rhsInt = rhs.getWidthInNanoMeters();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(FiducialComparatorEnum.LENGTH))
    {
      lhsInt = lhs.getLengthInNanoMeters();
      rhsInt = rhs.getLengthInNanoMeters();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(FiducialComparatorEnum.SHAPE))
    {
      ShapeEnum lhsShapeEnum = lhs.getShapeEnum();
      ShapeEnum rhsShapeEnum = rhs.getShapeEnum();

      if (lhsShapeEnum.equals(rhsShapeEnum))
        return 0;
      else
      {
        if (lhsShapeEnum.equals(ShapeEnum.CIRCLE))
        {
          lhsString = StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
          rhsString = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
        }
        else
        {
          lhsString = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
          rhsString = StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
        }
        return _alphaComparator.compare(lhsString, rhsString);
      }
    }
    else if (_comparingAttribute.equals(FiducialComparatorEnum.LOCATION))
    {
      if (lhs.isOnBoard())
        lhsString = StringLocalizer.keyToString("TM_GUI_FIDUCIAL_BOARD_LOCATION_KEY") + lhs.getSideBoard().getBoard().getName();
      else
        lhsString = StringLocalizer.keyToString("TM_GUI_FIDUCIAL_PANEL_LOCATION_KEY");
      if (rhs.isOnBoard())
        rhsString = StringLocalizer.keyToString("TM_GUI_FIDUCIAL_BOARD_LOCATION_KEY") + rhs.getSideBoard().getBoard().getName();
      else
        rhsString = StringLocalizer.keyToString("TM_GUI_FIDUCIAL_PANEL_LOCATION_KEY");
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else
    {
      Assert.expect(false, "Please add your new component attribute to sort on to this method");
      return 0;
    }
  }
}
