package com.axi.v810.util;

/**
 *
 * <p>Title: IndictmentDetailsComparatorEnum</p>
 *
 * <p>Description: Enumeration of fields to be sorted by the IndictmentDetailsComparator.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class IndictmentDetailsComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static IndictmentDetailsComparatorEnum INDICTMENT = new IndictmentDetailsComparatorEnum(++_index);
  public static IndictmentDetailsComparatorEnum TYPE = new IndictmentDetailsComparatorEnum(++_index);
  public static IndictmentDetailsComparatorEnum MEASUREMENT = new IndictmentDetailsComparatorEnum(++_index);
  public static IndictmentDetailsComparatorEnum VALUE = new IndictmentDetailsComparatorEnum(++_index);
  public static IndictmentDetailsComparatorEnum SLICE = new IndictmentDetailsComparatorEnum(++_index);

  /**
   * @author George Booth
   */
  private IndictmentDetailsComparatorEnum(int id)
  {
    super(id);
  }
}
