package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class effectively delegates work to com.axi.util.CSVFileWriter and rewraps standard java exceptions
 * as XrayTesterExceptions where applicable.
 *
 * @author Matt Wharton
 */
public class CSVFileWriterAxi
{
  private CSVFileWriter _csvFileWriter;

  /**
   * @author Matt Wharton
   */
  public CSVFileWriterAxi()
  {
    _csvFileWriter = new CSVFileWriter();
  }

  /**
   * @author Matt Wharton
   */
  public CSVFileWriterAxi(Collection<String> columnHeaders)
  {
    _csvFileWriter = new CSVFileWriter(columnHeaders);
  }

  /**
   * Opens the specfied file for writing.
   *
   * @author Matt Wharton
   */
  public void open(String fileName, boolean append) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(_csvFileWriter != null);

    try
    {
      _csvFileWriter.open(fileName, append);
    }
    catch (FileNotFoundException fnfex)
    {
      // Wrap the exception as a CannotOpenFileDatastoreException.
      DatastoreException dex = new CannotCreateFileDatastoreException(fileName);
      dex.initCause(fnfex);

      throw dex;
    }
  }

  /**
   * Opens the specfied file for writing.  Assumes we're NOT appending.
   *
   * @author Matt Wharton
   */
  public void open(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    open(fileName, false);
  }

  /**
   * @author Matt Wharton
   */
  public void close()
  {
    Assert.expect(_csvFileWriter != null);

    _csvFileWriter.close();
  }

  /**
   * @author Matt Wharton
   */
  public void writeDataLine(Collection<String> dataValues)
  {
    Assert.expect(dataValues != null);
    Assert.expect(_csvFileWriter != null);

    _csvFileWriter.writeDataLine(dataValues);
  }
}
