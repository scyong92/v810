package com.axi.v810.util;

/**
 * @author Cheah Lee Herng
 */
public class SerialNumberRegularExpressionDataComparatorEnum extends com.axi.util.Enum 
{
    private static int _index = -1;
    
    public static SerialNumberRegularExpressionDataComparatorEnum PATTERN_TYPE = new SerialNumberRegularExpressionDataComparatorEnum(++_index);
    public static SerialNumberRegularExpressionDataComparatorEnum PATTERN = new SerialNumberRegularExpressionDataComparatorEnum(++_index);
    public static SerialNumberRegularExpressionDataComparatorEnum PROJECT_NAME = new SerialNumberRegularExpressionDataComparatorEnum(++_index);
    
    /**
     * @author Cheah Lee Herng
    */
    private SerialNumberRegularExpressionDataComparatorEnum(int id)
    {
        super(id);
    }
}
