package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author George A. David
 */
public class Test_ConfigFileReaderUtil extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] arguments)
  {
    UnitTest.execute(new Test_ConfigFileReaderUtil());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      ConfigFileReaderUtil reader = new ConfigFileReaderUtil();
      try
      {
        reader.getValue(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        reader.parseFile(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Expect.expect(reader.getKeyValueMap().isEmpty());

      String testDir = getTestDataDir() + File.separator + "Test_ConfigFileReaderUtil";
      String nonExistantFile = testDir + File.separator + "nonExistantFile";
      try
      {
        reader.parseFile(nonExistantFile);
        Expect.expect(false);
      }
      catch (FileNotFoundDatastoreException dex)
      {
        //do nothing
      }

      // test loading a good config file
      reader.parseFile(testDir + File.separator + "good.config");
      try
      {
        reader.getValue("badKey");
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Expect.expect(reader.getValue("fileName").equals("good.config"));
      Expect.expect(reader.getValue("dirName").equals("data/Test_FileReaderUtil"));

      Map<String, String> keyToValueMap = reader.getKeyValueMap();
      Expect.expect(keyToValueMap.containsKey("fileName"));
      Expect.expect(keyToValueMap.get("fileName").equals("good.config"));
      Expect.expect(keyToValueMap.containsKey("dirName"));
      Expect.expect(keyToValueMap.get("dirName").equals("data/Test_FileReaderUtil"));
      Expect.expect(keyToValueMap.containsKey("equalName"));
      Expect.expect(keyToValueMap.get("equalName").equals("this is to test = sign"));

      // test reading a good empty file
      reader.parseFile(testDir + File.separator + "goodEmpty.config");
      Expect.expect(reader.getKeyValueMap().isEmpty());

      // read a config file with a bad comment
      try
      {
        reader.parseFile(testDir + File.separator + "badComment.config");
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException dex)
      {
        //do nothing
      }

      // read a config file with no equal sign
      try
      {
        reader.parseFile(testDir + File.separator + "noEqualSign.config");
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException dex)
      {
        //do nothing
      }

      // now test the good file again, but this time
      // only make the fileName key valid
      List<String> validKeys = new LinkedList<String>();
      validKeys.add("fileName");
      reader.setValidKeys(validKeys);
      try
      {
        reader.parseFile(testDir + File.separator + "good.config");
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException dex)
      {
        //do nothing
      }

      // now clear the valid keys and confirm
      // that it parses the file fine
      reader.clearValidKeys();
      reader.parseFile(testDir + File.separator + "good.config");

      // now set the valid keys and confirm
      // that it parses the file fine
      validKeys.add("dirName");
      validKeys.add("equalName");
      reader.setValidKeys(validKeys);
      reader.parseFile(testDir + File.separator + "good.config");

      // now test for duplicate keys
      try
      {
        reader.parseFile(testDir + File.separator + "duplicateKeys.config");
        Expect.expect(false);
      }
      catch (FileCorruptDatastoreException dex)
      {
        //do nothing
      }
    }
    catch(Exception exception)
    {
      exception.printStackTrace();
      Expect.expect(false);
    }
  }
}
