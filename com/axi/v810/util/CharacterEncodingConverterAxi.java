package com.axi.v810.util;

import java.io.*;
import java.nio.charset.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Convert the inFile from one character encoding to another.  For example
 * from UTF16 to ascii with escaped unicode characters.
 *
 * @author Bill Darbie
 */
public class CharacterEncodingConverterAxi
{
  private CharacterEncodingConverter _characterEncodingConverter;

  /**
   * @author Bill Darbie
   */
  public CharacterEncodingConverterAxi()
  {
    _characterEncodingConverter = new CharacterEncodingConverter();
  }

  /**
   * Convert the inFile from one character encoding to another.  For example
   * from UTF16 to ascii with escaped unicode characters
   *
   * inputFileName will be read in using the decoding specified by decodingStr, then it will be written out
   * to outputFileName with the encoding defined by encodingStr
   *
   * Every Java implementation should support these encoding/decodings:
   * US-ASCII Seven-bit ASCII, a.k.a. ISO646-US, a.k.a. the Basic Latin block of the Unicode character set
   * ISO-8859-1 ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1
   * UTF-8 Eight-bit UCS Transformation Format
   * UTF-16BE Sixteen-bit UCS Transformation Format, big-endian byte order
   * UTF-16LE Sixteen-bit UCS Transformation Format, little-endian byte order
   * UTF-16 Sixteen-bit UCS Transformation Format, byte order identified by an optional byte-order mark
   *
   * @author Bill Darbie
   */
  public void convert(String inputFileName,
                      String outputFileName,
                      String decodingStr,
                      String encodingStr) throws DatastoreException

  {
    Assert.expect(inputFileName != null);
    Assert.expect(outputFileName != null);
    Assert.expect(decodingStr != null);
    Assert.expect(encodingStr != null);

    try
    {
      _characterEncodingConverter.convert(inputFileName, outputFileName, decodingStr, encodingStr);
    }
    catch (CharacterEncodingNotSupportedException ex)
    {
      DatastoreException dex = new CharacterEncodingNotSupportedDatastoreException(ex.getEncodingString());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotCreateFileException ex)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotRenameFileException ex)
    {
      DatastoreException dex = new FileCannotBeRenamedDatastoreException(ex.getFromFileName(), ex.getToFileName());
      dex.initCause(ex);
      throw dex;
    }
    catch (CouldNotDeleteFileException ex)
    {
      DatastoreException dex = new CannotDeleteFileDatastoreException(ex.getFileName());
      dex.initCause(ex);
      throw dex;
    }
      catch(FileNotFoundException ex)
    {
      FileNotFoundDatastoreException dex = new FileNotFoundDatastoreException(ex.getMessage());
      dex.initCause(ex);
      throw dex;
    }
  }
}
