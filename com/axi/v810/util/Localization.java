package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;

/**
* This class contains the current locale for all the 5dx code.  Any time
* the code for the 5dx needs to use a locale it should query this class to
* find out the locale.  This class provides one global location for the locale
* to be stored.
*
* @author Bill Darbie
*/
public class Localization
{
  // eventually this should look at a file to see what
  // language to use, for now I will just use the default
  private static Locale _locale = Locale.US;

  /**
   * @author Bill Darbie
   */
  public static void setLocale(Locale locale)
  {
    Assert.expect(locale != null);
    _locale = locale;
  }

  /**
   * @author Bill Darbie
   */
  public static Locale getLocale()
  {
    return _locale;
  }
}
