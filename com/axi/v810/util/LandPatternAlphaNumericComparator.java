package com.axi.v810.util;

import java.util.*;

import com.axi.v810.business.panelDesc.*;
import com.axi.util.*;

/**
 * This class will be used to compare LandPatterns alphabetically by their names
 * @author George A. David
 */

public class LandPatternAlphaNumericComparator implements Comparator<LandPattern>
{
  private boolean _ascending;
  /**
   * @author George A. David
   */
  public LandPatternAlphaNumericComparator(boolean ascending)
  {
    _ascending = ascending;
    // do nothing
  }

  /**
   * @author George A. David
   */
  public int compare(LandPattern lhs, LandPattern rhs)
  {
    // check to make sure that both objects are not null
    Assert.expect(lhs != null, "LandPatternAlphaNumericComparartor.compare() - left hand side object is null");
    Assert.expect(rhs != null, "LandPatternAlphaNumericComparartor.compare() - right hand side object is null");

    String lhsName = lhs.getName();
    String rhsName = rhs.getName();

    if(_ascending)
      return lhsName.compareToIgnoreCase(rhsName);
    else
      return rhsName.compareToIgnoreCase(lhsName);
  }
}
