package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
/**
 * Tests the CommunicationHardwareException class
 * @author George A. David
 */
public class Test_CommunicationHardwareException extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CommunicationHardwareException());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String key = "HW_USB_DEVICE_NOT_FOUND_KEY";
    List<String> parameters = new ArrayList<String>();
    parameters.add("Xray Source");

    try
    {
      new CommunicationHardwareException(null, parameters);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }


    try
    {
      new CommunicationHardwareException(key, null);
      Expect.expect(false);
    }
    catch(AssertException assertException)
    {
      //do nothing
    }

    CommunicationHardwareException cex = new CommunicationHardwareException(key, parameters);
    System.out.println(cex.getLocalizedMessage());

  }
}
