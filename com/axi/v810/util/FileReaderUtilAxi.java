package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * Provides a way to read data from a file.
 * @author George A. David
 */
public class FileReaderUtilAxi
{
  FileReaderUtil _fileReaderUtil;
  /**
   * @author George A. David
   */
  public FileReaderUtilAxi()
  {
    _fileReaderUtil = new FileReaderUtil();
  }

  /**
   * @author George A. David
   */
  public void open(String filePath) throws DatastoreException
  {
    try
    {
      _fileReaderUtil.open(filePath);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(_fileReaderUtil.getFilePath());
      dex.initCause(fnfe);
      throw dex;
    }
  }

  /**
   * @author George A. David
   */
  public boolean hasNextLine() throws DatastoreException
  {
    boolean hasNextLine = false;
    try
    {
      hasNextLine = _fileReaderUtil.hasNextLine();
    }
    catch (IOException ioe)
    {
      DatastoreException dex = new DatastoreException(_fileReaderUtil.getFilePath());
      dex.initCause(ioe);
      throw dex;
    }

    return hasNextLine;
  }

  /**
   * @author George A. David
   */
  public String readNextLine() throws DatastoreException
  {
    Assert.expect(_fileReaderUtil != null);

    String line = null;
    try
    {
      line = _fileReaderUtil.readNextLine();
    }
    catch (IOException ioe)
    {
      DatastoreException dex = new CannotReadDatastoreException(_fileReaderUtil.getFilePath());
      dex.initCause(ioe);
      throw dex;
    }

    return line;
  }

  /**
   * @author George A. David
   */
  public void close()
  {
    _fileReaderUtil.close();
  }

  /**
   * @author George A. David
   */
  public static String readFileContents(String filePath) throws DatastoreException
  {
    String contents = "";
    FileReaderUtilAxi reader = new FileReaderUtilAxi();
    try
    {
      reader.open(filePath);
      while (reader.hasNextLine())
        contents += reader.readNextLine() + "\n";
    }
    finally
    {
      reader.close();
    }

    return contents;
  }
}
