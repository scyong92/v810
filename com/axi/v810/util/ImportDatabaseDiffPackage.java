/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

/**
 *
 * @author swee-yee.wong
 */
public class ImportDatabaseDiffPackage
{
  private String _tagName;
  private Double _importVersion;
  private String _importDateModified;
  
  public ImportDatabaseDiffPackage(String tagName, Double importVersion, String importDateModified)
  {
    this._tagName = tagName;
    this._importVersion = importVersion;
    this._importDateModified = importDateModified;
  }
  
  public String getTagName()
  {
    return _tagName;
  }
  
  public Double getImportVersion()
  {
    return _importVersion;
  }
  
  public String getImportDateModified()
  {
    return _importDateModified;
  }
}
