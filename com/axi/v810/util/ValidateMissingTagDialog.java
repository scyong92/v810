/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import com.axi.guiUtil.*;
import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import java.util.*;
/**
 *
 * @author swee-yee.wong
 */
public class ValidateMissingTagDialog  extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _contentPanel = new JPanel();
  private JPanel _checkBoxPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private VerticalFlowLayout _contentPanelVerticalFlowLayout = new VerticalFlowLayout();
  private VerticalFlowLayout _checkBoxPanelVerticalFlowLayout = new VerticalFlowLayout();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private FlowLayout _okPanelFlowLayout = new FlowLayout();
  private JLabel _contentLabel = new JLabel();
  private JLabel _contentLabel2 = new JLabel();
  private JButton _okButton = new JButton();
  private static JScrollPane _pane;
  private JCheckBox[] _checkBoxList;
  private JCheckBox _selectAll;
  private List<String> _fullMissingTagList = new ArrayList<String>();
  private List<String> _newTagList = new ArrayList<String>();


  
  ValidateMissingTagDialog (JFrame frame, String title, boolean modal, List<String> missingTagList)
  {
    super(frame, title, modal);
    
    _fullMissingTagList = missingTagList;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      //info msg
      System.out.println("Failed to open Validate Media File Dialog.");
    }
  }
  
  private void jbInit() throws Exception
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    //_mainPanel.setPreferredSize(new Dimension(550, 300));
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _contentPanel.setLayout(_contentPanelVerticalFlowLayout);
    _checkBoxPanel.setLayout(_checkBoxPanelVerticalFlowLayout);
    _okCancelPanel.setLayout(_okPanelFlowLayout);
    _okPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _contentLabel.setPreferredSize(new Dimension(420, 20));
    _contentLabel.setText(StringLocalizer.infoHandlerKeyToString("VALIDATE_TAG_PART_1_MESSAGE_KEY"));
    
    _contentLabel2.setPreferredSize(new Dimension(420, 20));
    _contentLabel2.setText(StringLocalizer.infoHandlerKeyToString("VALIDATE_TAG_PART_2_MESSAGE_KEY"));
    
    
    _selectAll = new JCheckBox(StringLocalizer.infoHandlerKeyToString("GUI_SELECT_ALL_CHECKBOX_LABEL_KEY"));
    _selectAll.addItemListener(new CheckBoxListener());
    
    _checkBoxList = new JCheckBox[_fullMissingTagList.size()];
    int counter = 0;

    for (String tagName : _fullMissingTagList)
    {
      _checkBoxList[counter]=new JCheckBox(tagName);
      _checkBoxPanel.add(_checkBoxList[counter]);
      _checkBoxList[counter].addItemListener(new CheckBoxListener());
      counter++;
    }

    _pane = new JScrollPane(_checkBoxPanel);
    _pane.setPreferredSize(new Dimension(540, 210));
    
    
    //swee yee wong
    _okButton.setText(StringLocalizer.infoHandlerKeyToString("GUI_INFO_OK_BUTTON_KEY"));
    _okButton.setEnabled(true);
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    
    getContentPane().add(_mainPanel);
    getRootPane().setDefaultButton(_okButton);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_contentPanel, BorderLayout.NORTH);
    _contentPanel.add(_contentLabel, null);
    _contentPanel.add(_contentLabel2, null);
    _contentPanel.add(_selectAll, null);
    _contentPanel.add(_pane);
    _centerPanel.add(_okCancelPanel, BorderLayout.SOUTH);
    _okCancelPanel.add(_okButton);
  }
  
  private void okButton_actionPerformed(ActionEvent e)
  {
    InfoHandlerDatabaseManager.getInstance().insertInfoHandler(_newTagList);
    InfoHandlerDatabaseManager.getInstance().renameDatabase();
    unpopulate();
    dispose();
  }
  
  /**
   * @author swee-yee.wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      unpopulate();
      dispose();
    }
  }

  private class CheckBoxListener implements ItemListener
  {
    public void itemStateChanged(ItemEvent e)
    {
      if (e.getSource() == _selectAll)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
        {
          for (int i = 0; i < _checkBoxList.length; i++)
          {
            _checkBoxList[i].setSelected(true);
          }
        }
        else
        {
          for (int i = 0; i < _checkBoxList.length; i++)
          {
            _checkBoxList[i].setSelected(false);
          }
        }
      }
      
      else
      {
        for (int i = 0; i < _checkBoxList.length; i++)
        {
          if (e.getSource() == _checkBoxList[i])
          {
            if (e.getStateChange() == ItemEvent.SELECTED)
            {
              _newTagList.add(_checkBoxList[i].getText());
            }
            else
            {
              _newTagList.remove(_checkBoxList[i].getText());
            }
          }
        }
      }
    }
  }
  
  private void unpopulate()
  {
    _mainPanel = null;
    _centerPanel = null;
    _contentPanel = null;
    _checkBoxPanel = null;
    _okCancelPanel = null;
    _mainPanelBorderLayout = null;
    _contentPanelVerticalFlowLayout = null;
    _checkBoxPanelVerticalFlowLayout = null;
    _centerPanelBorderLayout = null;
    _okPanelFlowLayout = null;
    _contentLabel = null;
    _contentLabel2 = null;
    _okButton = null;
    _pane = null;
    _checkBoxList = null;
    _selectAll = null;
    _fullMissingTagList = null;
    _newTagList = null;
  }
}
