package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * Represents a coordineate in panel space.  The origin is the lower left corner of the panel.
 * The x axis grows to the right and the y axis grows up.
 *
 * @author Tony Turner
 * @author Matt Wharton
 */
public class PanelCoordinate extends IntCoordinate implements Serializable
{
  /**
   * @author Matt Wharton
   */
  public PanelCoordinate()
  {
    super();
  }

  /**
   * @author Matt Wharton
   */
  public PanelCoordinate(PanelCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * @author Peter Esbensen
   */
  public PanelCoordinate(DoubleCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * @author Peter Esbensen
   */
  public PanelCoordinate(IntCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * @param x the x value of this coordinate.
   * @param y the y value of this coordinate.
   * @author Matt Wharton
   */
  public PanelCoordinate(int x, int y)
  {
    super(x, y);
  }

  /**
   * @param x the x value of this coordinate.
   * @param y the y value of this coordinate.
   * @author Matt Wharton
   */
  public PanelCoordinate(long x, long y)
  {
    super(x, y);
  }

  /**
   * Return an IntCoordinate that is the result of subtracting the given
   * IntCoordinate from this one.  This is vector subtraction.
   *
   * @author Peter Esbensen
   */
  public PanelCoordinate minus(PanelCoordinate rhs)
  {
    Assert.expect(rhs != null);
    return new PanelCoordinate(super.minus(rhs));
  }

  /**
   * Return an IntCoordinate that is the result of adding the given IntCoordinate
   * to this one.  This is vector addition.
   *
   * @author Peter Esbensen
   */
  public PanelCoordinate plus(PanelCoordinate rhs)
  {
    Assert.expect(rhs != null);

    IntCoordinate ic = super.plus(rhs);

    PanelCoordinate pc = new PanelCoordinate((long)ic.getPoint2D().getX(), (long)ic.getPoint2D().getY());
    return pc;
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }
}
