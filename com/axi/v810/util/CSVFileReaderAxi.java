package com.axi.v810.util;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class effectively delegates work to com.axi.util.CSVFileReader and rewraps standard java exceptions
 * as XrayTesterExceptions where applicable.
 *
 * @author khang-wah.chnee
 */
public class CSVFileReaderAxi 
{
  private CSVFileReader _csvFileReader;
  
  /**
   * @author khang-wah.chnee
   */
  public CSVFileReaderAxi()
  {
    _csvFileReader = new CSVFileReader();
  }
  
  public List<String []> readFile(String fileName) throws IOException
  {
    return _csvFileReader.readFile(fileName);
  }
  
  /**
   * @author Bee Hoon
   */
  public void closeFile() throws IOException
  {
    _csvFileReader.closeFile();
  }
}
