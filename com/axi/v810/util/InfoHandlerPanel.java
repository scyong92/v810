/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.util;

import java.awt.Component;
import com.axi.guiUtil.*;
import com.axi.v810.datastore.config.*;
import java.util.*;



/**
 * @author bee-hoon.goh
 * @author swee-yee.wong
 */
public class InfoHandlerPanel
{

  private static InfoHandlerPanel _instance;
  static InfoHandlerDialog _infoHandlerDialog;
  private final String _ERROR_TYPE = "ERROR";
  private final String _DISPLAY_TAG = "DISPLAY";
  private final String _DISPLAY_ALL_TAG = "DISPLAYALL";
  private boolean _isDatabaseLoaded = false;
  private boolean _isDialogInitDone = false;
  private List<String> _infoList = new ArrayList<String>();
  private Map<String, String[]> _tagToValueLookUpMap = new HashMap<String, String[]>();
  private Component _parent;

  /**
   * @author bee-hoon.goh
   */
  public static synchronized InfoHandlerPanel getInstance()
  {
    if (_instance == null)
    {
      _instance = new InfoHandlerPanel();
    }

    return _instance;
  }
  
  /**
   * @author swee-yee.wong
   */
  public void executeCommand(String message, String[] value)
  {
    executeCommand(null, message, value);
  }

  /**
   * @author swee-yee.wong
   */
  public void executeCommand(Component parent, String message, String[] value)
  {
    if (Config.isInfoHandlerEnabled())
    {
      int index = message.lastIndexOf('-');
      String command = message.substring(0, index);
      String subMsg = message.substring(index + 1);
      int index2 = subMsg.lastIndexOf('|');
      String tagType = subMsg.substring(0, index2);
      final String tagName = subMsg.substring(index2 + 1);

      //      private enum COMMAND
      //      {
      //        DISPLAY, DISPLAYALL;
      //      }
      //      COMMAND commandMsg = COMMAND.valueOf(command);
      //      ***********************************************************
      //      this method cannot be used, enum.valueOf() will be obfuscated during full build.
      //      ***********************************************************
      if (command.equals(_DISPLAY_TAG))
      {
        if (tagType.equals(_ERROR_TYPE))
        {
          addTagValuePairToMap(tagName, value);
          if (_infoList.contains(tagName))
          {
            _infoList.remove(tagName);
          }
          _infoList.add(tagName);

          if (_isDialogInitDone == true || _isDatabaseLoaded == false)
          {
            _parent = parent;
            if (_parent != null)
            {
              _parent.setEnabled(false);
              _parent.setFocusable(false);
            }
            SwingUtils.invokeLater(new Runnable()
            {

              public void run()
              {
                displayTag();
              }
            });
          }
        }
      }
      else if (command.equals(_DISPLAY_ALL_TAG))
      {
        if (tagType.equals(_ERROR_TYPE))
        {
          _parent = null;
          SwingUtils.invokeLater(new Runnable()
          {

            public void run()
            {
              displayFullTagList();
            }
          });
        }
      }
    }
  }

  /**
   * @author swee-yee.wong
   */
  private void displayTag()
  {
    if(_isDatabaseLoaded == false)
    {
      _isDatabaseLoaded = true;
      initInfoHandler();
    }
  
    if (_isDialogInitDone == true)
    {
      _infoHandlerDialog.clearSearchTextField();
      _infoHandlerDialog.clearComboList();
      _infoHandlerDialog.setWarningIcon();
      _infoHandlerDialog.setVisible(true);
      SwingUtils.centerOnScreen(_infoHandlerDialog);
      _infoHandlerDialog.validateMediaFile();
      _infoHandlerDialog.validateInfoTagDialog();
      _infoHandlerDialog.setFullDisplayMode(false);
      _infoHandlerDialog.addInfoListToComboBox(_infoList);
      _infoHandlerDialog.setVisible(true);
    }
  }

  /**
   * @author swee-yee.wong
   */
  private void displayFullTagList()
  {
    if(_isDatabaseLoaded == false)
    {
      initInfoHandler();
      _isDatabaseLoaded = true;
    }
    _tagToValueLookUpMap.clear();
    _infoHandlerDialog.clearSearchTextField();
    _infoHandlerDialog.resetSearchList();
    _infoHandlerDialog.addFullListToComboBox();
    _infoHandlerDialog.setFullDisplayMode(true);
    _infoHandlerDialog.setDefaultIcon();
    _infoHandlerDialog.setVisible(true);
    SwingUtils.centerOnScreen(_infoHandlerDialog);
    _infoHandlerDialog.validateMediaFile();
    _infoHandlerDialog.validateInfoTagDialog();

  }

  /**
   * @author swee-yee.wong
   */
  public void initInfoHandler()
  {
    InfoHandlerConfig.getInstance().loadConfig();
    //InfoHandlerDatabaseManager.getInstance().createInfoHandler();
    _infoHandlerDialog = new InfoHandlerDialog(this);
    SwingUtils.centerOnScreen(_infoHandlerDialog);
  }

  /**
   * @author swee-yee.wong
   */
  public void setInfoHandlerVisible(boolean enable)
  {
    if(enable == false)
    {
      if (_parent != null)
      {
        _parent.setEnabled(true);
        _parent.setFocusable(true);
      }
      _infoList.removeAll(_infoList);
    }
  }

  /**
   * @author swee-yee.wong
   */
  public void setDialogInitStatus(boolean enable)
  {
    _isDialogInitDone = enable;
  }
  
  /**
   * @author swee-yee.wong
   */
  private void addTagValuePairToMap(String tag, String[] value)
  {
    _tagToValueLookUpMap.put(tag, value);
  }
  
  /**
   * @author swee-yee.wong
   */
  public String[] getValueFromMap(String tag)
  {
    if (tag != null)
    {
      return _tagToValueLookUpMap.get(tag);
    }
    else
    {
      return null;
    }
  }
  
}
