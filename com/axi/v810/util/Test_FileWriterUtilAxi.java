package com.axi.v810.util;

import java.io.*;
import java.util.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Greg Esparza
 */
public class Test_FileWriterUtilAxi extends UnitTest
{
  /**
   * @author Greg Esparza
   */
  public Test_FileWriterUtilAxi()
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    boolean append = true;
    String fileNameFullPath = getTestDataDir() + File.separator +  "fileWriterUtil.txt";
    FileWriterUtilAxi fileWriterUtilAxi = new FileWriterUtilAxi(fileNameFullPath, append);

    try
    {
      fileWriterUtilAxi.open();
      fileWriterUtilAxi.write("Test - line #1");
      fileWriterUtilAxi.close();

      fileWriterUtilAxi.open();
      fileWriterUtilAxi.write("Test - line #2");
      fileWriterUtilAxi.write("Test - line #3");
      fileWriterUtilAxi.write("Test - line #4");
      fileWriterUtilAxi.close();

      fileWriterUtilAxi.open();
      fileWriterUtilAxi.writeln("Test - line #5");
      String formattedStr = "\n" +
                            "A" + "B" +
                            "\n" +
                            "\n" +
                            "C" + "\tOneTab" +
                            "\n" +
                            "D" + "\t\tTwoTab";
      formattedStr = StringUtil.replace(formattedStr, "\n", "\r\n");
      fileWriterUtilAxi.writeln(formattedStr);
      fileWriterUtilAxi.close();

      // Test writing an exception
      fileWriterUtilAxi.open();
      CouldNotCopyFileException exception = new CouldNotCopyFileException(fileNameFullPath, fileNameFullPath);
      fileWriterUtilAxi.write(exception);
      fileWriterUtilAxi.writeln(exception);
      fileWriterUtilAxi.close();

      // Test appending
      fileWriterUtilAxi.append(formattedStr);
      fileWriterUtilAxi.append(exception);

      List<String> messages = new ArrayList<String>();

      String message = "";
      for (int i = 0; i < 10; ++i)
      {
        message = "message" + String.valueOf(i);
        messages.add(message);
      }
      fileWriterUtilAxi.append(messages);
    }
    catch (DatastoreException de)
    {
      de.printStackTrace(os);
      Expect.expect(false);
    }

    try
    {
      FileUtil.delete(fileNameFullPath);
    }
    catch (CouldNotDeleteFileException cndfe)
    {
      cndfe.printStackTrace(os);
      Expect.expect(false);
    }
  }

  /**
   * @author Greg Esparza
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_FileWriterUtilAxi());
  }
}
