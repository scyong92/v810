package com.axi.v810.util;

/**
 * @author Laura Cormos
 */
public class ComponentComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ComponentComparatorEnum REFERENCE_DESIGNATOR = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum FULLY_QUALIFIED_NAME = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum BOARD_NAME = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum X_COORDINATE = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum Y_COORDINATE = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum DEGREES_ROTATION = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum SIDE_BOARD = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum JOINT_TYPE = new ComponentComparatorEnum(++_index);
  public static ComponentComparatorEnum STATUS = new ComponentComparatorEnum(++_index);

  /**
   * @author Laura Cormos
   */
  private ComponentComparatorEnum(int id)
  {
    super(id);
  }
}
