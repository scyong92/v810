/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import com.axi.v810.datastore.*;
import java.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class InfoHandlerConfigEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private String _key;
  private String _fileName;
  private TypeEnum _type;
  public static List <InfoHandlerConfigEnum> _infoHandlerConfigFullList = new ArrayList<InfoHandlerConfigEnum>();
  private static final String _INFOHANDLER_CONFIG = InfoHandlerConfig.getInfoHandlerConfigDir();

  public static final InfoHandlerConfigEnum INFO_HANDLER_DATABASE_VERSION
    = new InfoHandlerConfigEnum(++_index, "infoHandlerDatabaseVersion", _INFOHANDLER_CONFIG, TypeEnum.STRING);
  
  public static final InfoHandlerConfigEnum INFO_HANDLER_DATABASE_LAST_MODIFIED_DATE 
    = new InfoHandlerConfigEnum(++_index, "infoHandlerDatabaseLastModifiedDate", _INFOHANDLER_CONFIG, TypeEnum.STRING);
  
  public static final InfoHandlerConfigEnum INFO_HANDLER_DATABASE_DIR_EN_US
    = new InfoHandlerConfigEnum(++_index, "infoHandlerDatabaseDir_en_US", _INFOHANDLER_CONFIG, TypeEnum.STRING);
  
  public static final InfoHandlerConfigEnum INFO_HANDLER_DATABASE_DIR_ZH_CN
    = new InfoHandlerConfigEnum(++_index, "infoHandlerDatabaseDir_zh_CN", _INFOHANDLER_CONFIG, TypeEnum.STRING);
  
  public static final InfoHandlerConfigEnum INFO_HANDLER_MEDIA_DIR 
    = new InfoHandlerConfigEnum(++_index, "infoHandlerMediaDir", _INFOHANDLER_CONFIG, TypeEnum.STRING);
  
  public static final InfoHandlerConfigEnum INFO_HANDLER_TEMP_DIR 
    = new InfoHandlerConfigEnum(++_index, "infoHandlerTempDir", _INFOHANDLER_CONFIG, TypeEnum.STRING);
  
  public static final InfoHandlerConfigEnum INFO_HANDLER_DEFAULT_HTML 
    = new InfoHandlerConfigEnum(++_index, "defaultHtmlContent", _INFOHANDLER_CONFIG, TypeEnum.STRING);
  
  public static final InfoHandlerConfigEnum FULL_DIALOG_EDIT_BUTTON_VISIBLE 
    = new InfoHandlerConfigEnum(++_index, "fullDisplayEditButtonVisible", _INFOHANDLER_CONFIG, TypeEnum.BOOLEAN);
  
  public static final InfoHandlerConfigEnum PROMPT_DIALOG_EDIT_BUTTON_VISIBLE 
    = new InfoHandlerConfigEnum(++_index, "promptDisplayEditButtonVisible", _INFOHANDLER_CONFIG, TypeEnum.BOOLEAN);
  
  public static final InfoHandlerConfigEnum FULL_DIALOG_EDITABLE 
    = new InfoHandlerConfigEnum(++_index, "fullDisplayEditable", _INFOHANDLER_CONFIG, TypeEnum.BOOLEAN);
  
  public static final InfoHandlerConfigEnum PROMPT_DIALOG_EDITABLE 
    = new InfoHandlerConfigEnum(++_index, "promptDisplayEditable", _INFOHANDLER_CONFIG, TypeEnum.BOOLEAN);
  
  public static final InfoHandlerConfigEnum FULL_DIALOG_TREE_VISIBLE 
    = new InfoHandlerConfigEnum(++_index, "fullDisplayTreeVisible", _INFOHANDLER_CONFIG, TypeEnum.BOOLEAN);
  
  public static final InfoHandlerConfigEnum PROMPT_DIALOG_TREE_VISIBLE 
    = new InfoHandlerConfigEnum(++_index, "promptDisplayTreevisible", _INFOHANDLER_CONFIG, TypeEnum.BOOLEAN);
  
  /**
   * @author bee-hoon.goh
   */
  private InfoHandlerConfigEnum(int id, String name, String fileName, TypeEnum type)
  {
    super(id);
    _key = name.intern();
    _fileName = fileName.intern();
    _type = type;
    _infoHandlerConfigFullList.add(this);
  }

  /**
   * Returns a String representation of the InfoHandlingEnum class
   * @return String representation of the instance of InfoHandlingEnum
   * @author bee-hoon.goh
   */
  public String getKey()
  {
    return _key;
  }
  
  public String getFileName()
  {
    return _fileName;
  }
  
  public TypeEnum getType()
  {
    return _type;
  }
  
  public static List <InfoHandlerConfigEnum> getFullConfigEnumList()
  {
    return _infoHandlerConfigFullList;
  }
  
  public static InfoHandlerConfigEnum getConfigEnum(String key)
  {

    for (InfoHandlerConfigEnum current : _infoHandlerConfigFullList)
    {
      if (key.equals(current.getKey()))
        return current;
    }

    return null;
  }

}
