package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This comparator will sort a Component by it's overscanned property
 * @author Andy Mechtenberg
 */
public class ComponentIsOverscannedComparator implements Comparator<Component>
{
  private boolean _ascending;

  /**
   * Sorts the incoming component based on it's overscan attribute
   * @author Andy Mechtenberg
   */
  public ComponentIsOverscannedComparator(boolean ascending)
  {
    _ascending = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int compare(Component lhComponent, Component rhComponent)
  {
    Assert.expect(lhComponent != null);
    Assert.expect(rhComponent != null);

    Assert.expect(false);
    return 0;
//
//    boolean lh = lhComponent.getComponentTypeSettings().isOverScanUsed();
//    boolean rh = rhComponent.getComponentTypeSettings().isOverScanUsed();
//
//    if (lh == rh)
//      return 0;
//
//    if(_ascending)
//    {
//      if ((lh == true) && (rh == false))
//        return 1;
//      else
//        return -1;
//    }
//    else
//    {
//      if((rh == true) && (lh == false))
//        return 1;
//      else
//        return -1;
//    }
//
  }
}
