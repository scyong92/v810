/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

/**
 *
 * @author swee-yee.wong
 */
public class DatabaseChangeTypeEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private String _changeType;

  public static DatabaseChangeTypeEnum INSERT = new DatabaseChangeTypeEnum(++_index, "INSERT");
  public static DatabaseChangeTypeEnum DELETE = new DatabaseChangeTypeEnum(++_index, "DELETE");
  public static DatabaseChangeTypeEnum UPDATE_VERSION = new DatabaseChangeTypeEnum(++_index, "UPDATE_VERSION");

  /**
   * @author swee-yee.wong
   */
  protected DatabaseChangeTypeEnum(int id, String changeType)
  {
    super(id);
    _changeType = changeType.intern();
  }
  
  public String toString()
  {
    return _changeType;
  }
  
  
}