/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.gui.*;
import com.axi.v810.util.*;
import java.io.*;

/**
 *
 * @author swee-yee.wong
 */
public class InfoHandlerFileUtil
{
  private static InfoHandlerFileUtil _instance;
  
  /**
   * @author Swee Yee Wong
   */
  public static synchronized InfoHandlerFileUtil getInstance()
  {
    if (_instance == null)
    {
      _instance = new InfoHandlerFileUtil();
    }
    return _instance;
  }
  
  public void writeToFile(String fullPath, String content) throws CouldNotCreateFileException
  {
    FileWriterUtil os = new FileWriterUtil(fullPath, false);

    os.open();
    try
    {
      os.write(content);
    }
    finally
    {
      if (os != null)
      {
        os.close();
      }
    }
  }
  
  public String readFromFile(String fullPath)
  {
    String line = null;
    String output = "";
    FileReaderUtil reader = new FileReaderUtil();

    try
    {
      reader.open(fullPath);
      try
      {
        while (reader.hasNextLine())
        {
          line = reader.readNextLine();
          output = output + line;
        }
      }
      catch (IOException ex)
      {
        System.out.println(ex.getMessage());
        System.out.println("Failed to read file: " + fullPath);
      }
    }
    catch (FileNotFoundException ef)
    {
      System.out.println(ef.getMessage());
      System.out.println("File not found");
    }
    return output;
  }
  
  public static String changeFullPathToRelativePath(String filePath)
  {
    String relativeFilePath = filePath;
    if (filePath.contains(Directory.getXrayTesterReleaseDir()))
    {
      int index = filePath.lastIndexOf(Directory.getXrayTesterReleaseDir());
      int directorySize = Directory.getXrayTesterReleaseDir().length();
      relativeFilePath = "$(AXI_XRAY_HOME)" + filePath.substring(index + directorySize);
    }
    
    return relativeFilePath;
  }
}
