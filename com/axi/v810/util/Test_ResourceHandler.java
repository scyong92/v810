package com.axi.v810.util;

import java.io.*;
import java.net.*;

import com.axi.util.*;

/**
* Unit test
* @author Bill Darbie
*/
public class Test_ResourceHandler extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_ResourceHandler());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      ResourceHandler.getImageIcon(new URL("com.axi.v810.gui.cAndD.pass.gif"));
    }
    catch (MalformedURLException e)
    {
      e.printStackTrace();
    }
  }  
}
