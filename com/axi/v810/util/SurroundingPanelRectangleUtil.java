package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;


/**
 * Given a list of renderes it will determine the list of renderers that fall in a rectangle
 * @author George A. David
 */
public class SurroundingPanelRectangleUtil
{
  private Map<Double, Set<PanelRectangle>> _xCoordToPanelRectangleSetMap = new TreeMap<Double, Set<PanelRectangle>>();
  private Map<Double, Set<PanelRectangle>> _yCoordToPanelRectangleSetMap = new TreeMap<Double, Set<PanelRectangle>>();

  private List<Double> _xCoords;
  private List<Double> _yCoords;

  private boolean _useBoundsForDeterminingVicinity = false;
  private double _largestWidthInNanos = -1;
  private double _largestLengthInNanos = -1;

  /**
   * @author George A. David
   */
  public void setUseBoundsForDeterminingVicinity(boolean useBoundsForDeterminigVicinity)
  {
    _useBoundsForDeterminingVicinity = useBoundsForDeterminigVicinity;
  }

  /**
   * @author George A. David
   */
  public void setPanelRectangles(Collection<PanelRectangle> panelRectangles)
  {
    Assert.expect(panelRectangles != null);

    _largestWidthInNanos = -1;
    _largestLengthInNanos = -1;
    _xCoordToPanelRectangleSetMap.clear();
    _yCoordToPanelRectangleSetMap.clear();

    for(PanelRectangle region : panelRectangles)
      addPanelRectangle(region);

    _xCoords = new ArrayList<Double>(_xCoordToPanelRectangleSetMap.keySet());
    _yCoords = new ArrayList<Double>(_yCoordToPanelRectangleSetMap.keySet());
  }

  /**
   * @author George A. David
   */
  private void addPanelRectangle(PanelRectangle rect)
  {
    if(_useBoundsForDeterminingVicinity)
    {
      addPanelRectangle(rect, rect.getMinX(), _xCoordToPanelRectangleSetMap);
      addPanelRectangle(rect, rect.getMaxX(), _xCoordToPanelRectangleSetMap);
      addPanelRectangle(rect, rect.getMinY(), _yCoordToPanelRectangleSetMap);
      addPanelRectangle(rect, rect.getMaxY(), _yCoordToPanelRectangleSetMap);
      _largestWidthInNanos = Math.max(_largestWidthInNanos, rect.getWidth());
      _largestLengthInNanos = Math.max(_largestLengthInNanos, rect.getHeight());
    }
    else
    {
      addPanelRectangle(rect, rect.getCenterX(), _xCoordToPanelRectangleSetMap);
      addPanelRectangle(rect, rect.getCenterY(), _yCoordToPanelRectangleSetMap);
    }
  }

  /**
   * @author George A. David
   */
  private void addPanelRectangle(PanelRectangle panelRect, double coordValue,  Map<Double, Set<PanelRectangle>> coordToPanelRectangleSetMap)
  {
    Assert.expect(panelRect != null);
    Assert.expect(coordToPanelRectangleSetMap != null);

    if(coordToPanelRectangleSetMap.containsKey(coordValue))
    {
      coordToPanelRectangleSetMap.get(coordValue).add(panelRect);
    }
    else
    {
      Set<PanelRectangle> panelRectSet = new HashSet<PanelRectangle>();
      panelRectSet.add(panelRect);
      coordToPanelRectangleSetMap.put(coordValue, panelRectSet);
    }
  }

  /**
   * @author George A. David
   */
  public Set<PanelRectangle> getIntersectingPanelRectanglesInRectangle(PanelRectangle rectangle)
  {
    Set<PanelRectangle> panelRects = getIntersectingPanelRectanglesBetweenValues(rectangle.getMinX(),
                                                                         rectangle.getMaxX(),
                                                                         _xCoords,
                                                                         _xCoordToPanelRectangleSetMap,
                                                                         _largestWidthInNanos);
    panelRects.retainAll(getIntersectingPanelRectanglesBetweenValues(rectangle.getMinY(),
                                                                          rectangle.getMaxY(),
                                                                          _yCoords,
                                                                          _yCoordToPanelRectangleSetMap,
                                                                          _largestLengthInNanos));
    return panelRects;
  }

  /**
   * @author George A. David
   */
  private Set<PanelRectangle> getIntersectingPanelRectanglesBetweenValues(double minValue,
                                                                                double maxValue,
                                                                                List<Double> coords,
                                                                                Map<Double, Set<PanelRectangle>> coordToPanelRectangleSetMap,
                                                                   double largestDimension)
  {
    Assert.expect(coordToPanelRectangleSetMap != null);
    Assert.expect(maxValue >= minValue);


    Set<PanelRectangle> panelRects = new HashSet<PanelRectangle>();

    int beginIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, minValue);
    int endIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, maxValue);

    if(beginIndex < 0)
      beginIndex = 0;

    if (endIndex > coords.size() - 1)
      endIndex = coords.size() - 1;

    for(int i = beginIndex; i <= endIndex; ++i)
    {
      double coord = coords.get(i);
      panelRects.addAll(coordToPanelRectangleSetMap.get(coord));
    }

    if(beginIndex > 0 && endIndex < coords.size() - 1)
    {
      // ok, now because we are using the extents, we may have a
      // reconstruciton whose extents are beyond this rectangle, but we still want
      // to report it. so let's check for any regions that cross this intersection.

      // we don't want to always start with 0 index, this slows us down too much
      // let's be smart about this, we know the max size of a reconstruction region, so
      // let's use that to help us limit the search.
      double smallestStartingCoordinate = maxValue - largestDimension;

      int smallestIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, smallestStartingCoordinate);

      Set<PanelRectangle> beginRects = new HashSet<PanelRectangle>();
      for (int i = smallestIndex; i < beginIndex; ++i)
      {
        double coord = coords.get(i);
        beginRects.addAll(coordToPanelRectangleSetMap.get(coord));
      }

      double largestEndingCoordinate = minValue + largestDimension;
      int biggestIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, largestEndingCoordinate);
      Set<PanelRectangle> endRects = new HashSet<PanelRectangle>();
      for (int i = endIndex + 1; i <= biggestIndex; ++i)
      {
        double coord = coords.get(i);
        endRects.addAll(coordToPanelRectangleSetMap.get(coord));
      }
      beginRects.retainAll(endRects);
      panelRects.addAll(beginRects);
    }

    return panelRects;
  }

  /**
   * @author George A. David
   */
  private Set<PanelRectangle> getSurroundingPanelRectangles(double coordValue, List<Double> coords,  Map<Double, Set<PanelRectangle>> coordToPanelRectangleSetMap)
  {
    Assert.expect(coordToPanelRectangleSetMap != null);

    Set<PanelRectangle> panelRects = new HashSet<PanelRectangle>();

    Pair<Integer, Integer> pair = ListUtil.findIndiciesOfSurroundingValues(coords, coordValue);
    int previousIndex = pair.getFirst();
    int nextIndex = pair.getSecond();

    for(int i = previousIndex; i <= nextIndex; ++i)
      panelRects.addAll(coordToPanelRectangleSetMap.get(coords.get(i)));

    return panelRects;
  }

  /**
   * @author George A. David
   */
  public Collection<PanelRectangle> getSurroundingPanelRectangles(PanelRectangle panelRectangle)
  {
    Assert.expect(panelRectangle != null);

    PanelCoordinate coord = panelRectangle.getCenterCoordinate();
    Set<PanelRectangle> regions = getSurroundingPanelRectangles(coord.getX(), _xCoords, _xCoordToPanelRectangleSetMap);
    regions.retainAll(getSurroundingPanelRectangles(coord.getY(), _yCoords, _yCoordToPanelRectangleSetMap));

    return regions;
  }

  /**
   * @author George A. David
   */
  public Collection<PanelRectangle> getPanelRectanglesAtXcoordinate(int xCoordinate)
  {
    Set<PanelRectangle> rects = new HashSet<PanelRectangle>();
    rects.addAll(_xCoordToPanelRectangleSetMap.get(xCoordinate));

    return rects;
  }

  /**
   * @author George A. David
   */
  public Collection<PanelRectangle> getPanelRectanglesAtYcoordinate(int yCoordinate)
  {
    Set<PanelRectangle> rects = new HashSet<PanelRectangle>();
    rects.addAll(_yCoordToPanelRectangleSetMap.get(yCoordinate));

    return rects;
  }

  /**
   * @author George A. David
   */
  public List<Double> getXcoords()
  {
    return new ArrayList<Double>(_xCoords);
  }

  /**
   * @author George A. David
   */
  public List<Double> getYcoords()
  {
    return new ArrayList<Double>(_yCoords);
  }

  /**
   * @author George A. David
   */
  public void printDebugInfo()
  {
//    System.out.println("x map");
//    for(Integer integer : _xCoordToPadSetMap.keySet())
//    {
//      System.out.println("xCoord: " + integer);
//      System.out.print("Pads: ");
//      for(K pad : _xCoordToPadSetMap.get(integer))
//      {
//        System.out.print(pad.getName() + " ");
//      }
//      System.out.println();
//    }
//
//    System.out.println("y map");
//    for(Integer integer : _yCoordToPadSetMap.keySet())
//    {
//      System.out.println("yCoord: " + integer);
//      System.out.print("Pads: ");
//      for(K pad : _yCoordToPadSetMap.get(integer))
//      {
//        System.out.print(pad.getName() + " ");
//      }
//      System.out.println();
//    }
  }
}
