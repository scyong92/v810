package com.axi.v810.util;

import com.axi.util.*;

/**
 * Enumeration of degrees for use with the 5dx.
 *
 * @author Keith Lee
 */
public class DegreesEnum extends com.axi.util.Enum
{
  public static DegreesEnum ZERO = new DegreesEnum(0);
  public static DegreesEnum NINETY = new DegreesEnum(90);
  public static DegreesEnum ONE_EIGHTY = new DegreesEnum(180);
  public static DegreesEnum TWO_SEVENTY = new DegreesEnum(270);

 /*
  * @author Bill Darbie
  */
  private DegreesEnum(int degrees)
  {
    super(degrees);
  }

  /*
   * @author Bill Darbie
   */
  public int getDegrees()
  {
    return getId();
  }

  /**
   * @return the minimum allowable degrees value
   * @author Bill Darbie
   */
  public static int getMinDegrees()
  {
    return 0;
  }

  /**
   * @return the maximum allowed degrees value
   * @author Bill Darbie
   */
  public static int getMaxDegrees()
  {
    return 359;
  }

  /*
   * @author Bill Darbie
   */
  public static boolean isValid(int degrees)
  {
    if ((degrees == 0) || (degrees == 90) || (degrees == 180) || (degrees == 270))
      return true;

    return false;
  }

  /**
   * @author Bill Darbie
   */
  public String toString()
  {
    return Integer.toString(getId());
  }
}
