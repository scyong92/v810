package com.axi.v810.util;

import java.io.*;
import java.net.*;

import com.axi.util.*;

/**
* Unit test
* @author Steve Anonson
*/
public class Test_HashUtil extends UnitTest
{
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_HashUtil());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    String str = "digby_650_dual";
    os.println( "Hash of " + str + " = " + HashUtil.hashName(str) );
    str = "families_all_rlv";
    os.println( "Hash of " + str + " = " + HashUtil.hashName(str) );
    str = "";
    os.println( "Hash of " + str + " = " + HashUtil.hashName(str) );
    str = "5dx";
    os.println( "Hash of " + str + " = " + HashUtil.hashName(str) );
    str = "shawna";
    os.println( "Hash of " + str + " = " + HashUtil.hashName(str) );
    str = null;
    try
    {
      HashUtil.hashName(str);
    }
    catch (AssertException e)
    {
      os.println("AssertException occurred as expected");
    }
  }
}
