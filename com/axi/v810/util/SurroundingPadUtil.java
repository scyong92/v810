package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;

/**
 * Given a list of pads it will determine the surrounding pads of any pad you pass in.
 * @author George A. David
 */
public class SurroundingPadUtil
{
  private Map<Double, Set<Pad>> _xCoordToPadSetMap = new TreeMap<Double, Set<Pad>>();
  private Map<Double, Set<Pad>> _yCoordToPadSetMap = new TreeMap<Double, Set<Pad>>();
  private List<Double> _xCoords;
  private List<Double> _yCoords;
  private Set<Pad> _topPads = new HashSet<Pad>();
  private Set<Pad> _bottomPads = new HashSet<Pad>();
  private boolean _useBoundsForDeterminingVicinity = false;
  private boolean _useMaximumAreaNeededForInspection = false;
  private boolean _useThroughHolePadsOnBothSides = false;
  private double _largestWidthInNanos = -1;
  private double _largestLengthInNanos = -1;
  private double _padBoundsBufferInNanos = 0;

  /**
   * @author George A. David
   */
  public void setUseBoundsForDeterminingVicinity(boolean useBoundsForDeterminigVicinity)
  {
    _useBoundsForDeterminingVicinity = useBoundsForDeterminigVicinity;
  }

  /**
   * @author George A. David
   */
  public void setUseThroughHolePadsOnBothSides(boolean useThroughHolePadsOnBothSides)
  {
    _useThroughHolePadsOnBothSides = useThroughHolePadsOnBothSides;
  }

  /**
   * @author George A. David
   */
  public void setUseMaximumAreaNeededForInspection(boolean useMaximumAreaNeededForInspection)
  {
    _useMaximumAreaNeededForInspection = useMaximumAreaNeededForInspection;
  }

  /**
   * @author George A. David
   */
  public void setPadBoundsBufferInNanos(int bufferInNanos)
  {
    Assert.expect(bufferInNanos >= 0);

    _padBoundsBufferInNanos = bufferInNanos;
  }

  /**
   * @author George A. David
   */
  public void setPads(Collection<Pad> pads)
  {
    Assert.expect(pads != null);

    _largestWidthInNanos = -1;
    _largestLengthInNanos = -1;
    _xCoordToPadSetMap.clear();
    _yCoordToPadSetMap.clear();
    _topPads.clear();
    _bottomPads.clear();

    for(Pad pad : pads)
      addPad(pad);

    _xCoords = new ArrayList<Double>(_xCoordToPadSetMap.keySet());
    _yCoords = new ArrayList<Double>(_yCoordToPadSetMap.keySet());
  }

  /**
   * @author George A. David
   */
  private void addPad(Pad pad)
  {
    if(_useBoundsForDeterminingVicinity)
    {
      PanelRectangle rect = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
      if(_useMaximumAreaNeededForInspection)
        rect = ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad);
      else
        rect = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
      rect.setRect(rect.getMinX() - _padBoundsBufferInNanos,
                   rect.getMinY() - _padBoundsBufferInNanos,
                   rect.getWidth() + (2 * _padBoundsBufferInNanos),
                   rect.getHeight() + (2 * _padBoundsBufferInNanos));

      addPad(pad, rect.getMinX(), _xCoordToPadSetMap);
      addPad(pad, rect.getMaxX(), _xCoordToPadSetMap);
      addPad(pad, rect.getMinY(), _yCoordToPadSetMap);
      addPad(pad, rect.getMaxY(), _yCoordToPadSetMap);
      _largestWidthInNanos = Math.max(_largestWidthInNanos, rect.getWidth());
      _largestLengthInNanos = Math.max(_largestLengthInNanos, rect.getHeight());
    }
    else
    {
      PanelCoordinate coord = pad.getCenterCoordinateRelativeToPanelInNanoMeters();
      addPad(pad, coord.getX(), _xCoordToPadSetMap);
      addPad(pad, coord.getY(), _yCoordToPadSetMap);
    }

    if(_useThroughHolePadsOnBothSides &&
       (pad.getSubtype().getInspectionFamilyEnum().equals(InspectionFamilyEnum.THROUGHHOLE) ||
        pad.getSubtype().getInspectionFamilyEnum().equals(InspectionFamilyEnum.PRESSFIT) ||
        pad.getSubtype().getInspectionFamilyEnum().equals(InspectionFamilyEnum.OVAL_THROUGHHOLE))) //Siew Yeng - XCR-3318
    {
      _topPads.add(pad);
      _bottomPads.add(pad);
    }
    else if(pad.isTopSide())
      _topPads.add(pad);
    else
      _bottomPads.add(pad);
  }

  /**
   * @author George A. David
   */
  private void addPad(Pad pad, double coordValue,  Map<Double, Set<Pad>> coordToPadSetMap)
  {
    Assert.expect(pad != null);
    Assert.expect(coordToPadSetMap != null);

    if(coordToPadSetMap.containsKey(coordValue))
    {
      coordToPadSetMap.get(coordValue).add(pad);
    }
    else
    {
      Set<Pad> padSet = new HashSet<Pad>();
      padSet.add(pad);
      coordToPadSetMap.put(coordValue, padSet);
    }
  }

  /**
   * @author George A. David
   */
  private Set<Pad> getSurroundingPads(double coordValue, List<Double> coords, Map<Double, Set<Pad>> coordToPadSetMap)
  {
    Assert.expect(coordToPadSetMap != null);

    Set<Pad> pads = new HashSet<Pad>();

    Pair<Integer, Integer> pair = ListUtil.findIndiciesOfSurroundingValues(coords, coordValue);
    int previousIndex = pair.getFirst();
    int nextIndex = pair.getSecond();

    for(int i = previousIndex; i <= nextIndex; ++i)
      pads.addAll(coordToPadSetMap.get(coords.get(i)));

    return pads;
  }

  /**
   * @author George A. David
   */
  public Collection<Pad> getSurroundingPads(Pad pad)
  {
    Assert.expect(pad != null);

    PanelCoordinate coord = pad.getCenterCoordinateRelativeToPanelInNanoMeters();
    Set<Pad> pads = getSurroundingPads(coord.getX(),_xCoords, _xCoordToPadSetMap);
    pads.retainAll(getSurroundingPads(coord.getY(),_yCoords, _yCoordToPadSetMap));

    return pads;
  }

  /**
   * @author George A. David
   */
  public Collection<Pad> getPadsAtXcoordinate(int xCoordinate)
  {
    Set<Pad> pads = new HashSet<Pad>();
    pads.addAll(_xCoordToPadSetMap.get(xCoordinate));

    return pads;
  }

  /**
   * @author George A. David
   */
  public Collection<Pad> getPadsAtYcoordinate(int yCoordinate)
  {
    Set<Pad> pads = new HashSet<Pad>();
    pads.addAll(_yCoordToPadSetMap.get(yCoordinate));

    return pads;
  }

  /**
   * @author George A. David
   */
  public void printDebugInfo()
  {
    System.out.println("x map");
    for(Double integer : _xCoordToPadSetMap.keySet())
    {
      System.out.println("xCoord: " + integer);
      System.out.print("Pads: ");
      for(Pad pad : _xCoordToPadSetMap.get(integer))
      {
        System.out.print(pad.getName() + " ");
      }
      System.out.println();
    }

    System.out.println("y map");
    for(Double integer : _yCoordToPadSetMap.keySet())
    {
      System.out.println("yCoord: " + integer);
      System.out.print("Pads: ");
      for(Pad pad : _yCoordToPadSetMap.get(integer))
      {
        System.out.print(pad.getName() + " ");
      }
      System.out.println();
    }
  }

  /**
   * @author George A. David
   */
  public Set<Pad> getIntersectingPadsInRectangle(PanelRectangle rectangle, boolean isTopSide)
  {
    Set<Pad> pads = getIntersectingPadsInRectangle(rectangle);
    if(isTopSide)
      pads.retainAll(_topPads);
    else
      pads.retainAll(_bottomPads);

    return pads;
  }

  /**
   * @author George A. David
   */
  public Set<Pad> getPadsInRectangle(PanelRectangle rectangle)
  {
    Set<Pad> pads = getPadsBetweenValues(rectangle.getMinX(),
                                         rectangle.getMaxX(),
                                         _xCoords,
                                         _xCoordToPadSetMap,
                                         _largestWidthInNanos);
    pads.retainAll(getPadsBetweenValues(rectangle.getMinY(),
                                        rectangle.getMaxY(),
                                        _yCoords,
                                        _yCoordToPadSetMap,
                                        _largestLengthInNanos));
    return pads;
  }

  /**
   * @author George A. David
   */
  public Set<Pad> getIntersectingPadsInRectangle(PanelRectangle rectangle)
  {
    Set<Pad> pads = getIntersectingPadsBetweenValues(rectangle.getMinX(),
                                                     rectangle.getMaxX(),
                                                     _xCoords,
                                                     _xCoordToPadSetMap,
                                                     _largestWidthInNanos);
    pads.retainAll(getIntersectingPadsBetweenValues(rectangle.getMinY(),
                                                    rectangle.getMaxY(),
                                                    _yCoords,
                                                    _yCoordToPadSetMap,
                                                    _largestLengthInNanos));
    return pads;
  }

  /**
   * @author George A. David
   */
  public Set<Pad> getPadsNotContainedInRectangle(PanelRectangle rectangle)
  {
    Set<Pad> padsInRect = getPadsInRectangle(rectangle);
    Set<Pad> allPads = new HashSet<Pad>(_topPads);
    allPads.addAll(_bottomPads);

    allPads.removeAll(padsInRect);

    return allPads;
  }

  /**
   * @author George A. David
   */
  private Set<Pad> getPadsBetweenValues(double minValue,
                                        double maxValue,
                                        List<Double> coords,
                                        Map<Double, Set<Pad>> coordToPadSetMap,
                                        double largestDimension)
  {
    Assert.expect(coordToPadSetMap != null);
    Assert.expect(maxValue >= minValue);

    Set<Pad> pads = new HashSet<Pad>();

    int beginIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, minValue);

    int endIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, maxValue);

    if(beginIndex < 0)
      beginIndex = 0;

    if (endIndex > coords.size() - 1)
      endIndex = coords.size() - 1;

    for(int i = beginIndex; i <= endIndex; ++i)
    {
      double coord = coords.get(i);
      pads.addAll(coordToPadSetMap.get(coord));
    }

    if((beginIndex > 0 || endIndex < coords.size() - 1) &&
       _useBoundsForDeterminingVicinity)
    {
      // because we are using the extents, we may have a
      // component whose extents go beyond this rectangle, and we do not want
      // to report it. so let's check for any pads that are not
      // completely contained in this rectangle

      // we don't want to always start with 0 index, this slows us down too much
      // let's be smart about this, we know the largest pad dimension, so
      // let's use that to help us limit the search.
      double smallestStartingCoordinate = minValue - largestDimension;
      int smallestIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, smallestStartingCoordinate);
      Set<Pad> outsidePads = new HashSet<Pad>();
      for (int i = smallestIndex; i < beginIndex; ++i)
        outsidePads.addAll(coordToPadSetMap.get(coords.get(i)));

      double largestEndingCoordinate = maxValue + largestDimension;
      int largestIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, largestEndingCoordinate);
      for (int i = endIndex + 1; i <= largestIndex; ++i)
        outsidePads.addAll(coordToPadSetMap.get(coords.get(i)));

      pads.removeAll(outsidePads);
    }

    return pads;
  }

  /**
   * @author George A. David
   */
  private Set<Pad> getIntersectingPadsBetweenValues(double minValue,
                                                    double maxValue,
                                                    List<Double> coords,
                                                    Map<Double, Set<Pad>> coordToPadSetMap,
                                                    double largestDimension)
  {
    Assert.expect(coordToPadSetMap != null);
    Assert.expect(maxValue >= minValue);

    Set<Pad> pads = new HashSet<Pad>();

    int beginIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, minValue);

    int endIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, maxValue);

    if(beginIndex < 0)
      beginIndex = 0;

    if (endIndex > coords.size() - 1)
      endIndex = coords.size() - 1;


    for(int i = beginIndex; i <= endIndex; ++i)
    {
      double coord = coords.get(i);
      pads.addAll(coordToPadSetMap.get(coord));
    }

    if(beginIndex > 0 && endIndex < coords.size() - 1)
    {
      // ok, now because we are using the extents, we may have a
      // component whose extents are beyond this rectangle, but we still want
      // to report it. so let's check for any components that cross this intersection.

      // we don't want to always start with 0 index, this slows us down too much
      // let's be smart about this, we know the largest pad dimension, so
      // let's use that to help us limit the search.
      double smallestStartingCoordinate = maxValue - largestDimension;
      int smallestIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, smallestStartingCoordinate);
      Set<Pad> beginPads = new HashSet<Pad>();
      for (int i = smallestIndex; i < beginIndex; ++i)
        beginPads.addAll(coordToPadSetMap.get(coords.get(i)));

      double largestEndingCoordinate = minValue + largestDimension;
      int largestIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, largestEndingCoordinate);
      Set<Pad> endPads = new HashSet<Pad>();
      for (int i = endIndex + 1; i <= largestIndex; ++i)
        endPads.addAll(coordToPadSetMap.get(coords.get(i)));

      beginPads.retainAll(endPads);
      pads.addAll(beginPads);
    }

    return pads;
  }

  /**
   * @author Matt Wharton
   */
  public Set<Pad> getContainedPadsInRectangle(PanelRectangle rectangle, boolean isTopSide)
  {
    Set<Pad> pads = getContainedPadsInRectangle(rectangle);
    if(isTopSide)
      pads.retainAll(_topPads);
    else
      pads.retainAll(_bottomPads);

    return pads;
  }

  /**
   * @author Matt Wharton
   */
  public Set<Pad> getContainedPadsInRectangle(PanelRectangle rectangle)
  {
    Assert.expect(rectangle != null);

    Set<Pad> pads = getContainedPadsBetweenValues(rectangle.getMinX(),
                                                  rectangle.getMaxX(),
                                                  _xCoords,
                                                  _xCoordToPadSetMap);
    pads.retainAll(getContainedPadsBetweenValues(rectangle.getMinY(),
                                                 rectangle.getMaxY(),
                                                 _yCoords,
                                                 _yCoordToPadSetMap));

    // Make sure our list of pads includes only those pads which are completely contained in the rectangle.
    for (Iterator<Pad> padIt = pads.iterator(); padIt.hasNext();)
    {
      Pad pad = padIt.next();
      if (rectangle.contains(pad.getShapeRelativeToPanelInNanoMeters().getBounds2D()) == false)
      {
        padIt.remove();
      }
    }

    return pads;
  }

  /**
   * @author Matt Wharton
   */
  private Set<Pad> getContainedPadsBetweenValues(double minValue,
                                                 double maxValue,
                                                 List<Double> coords,
                                                 Map<Double, Set<Pad>> coordToPadSetMap)
  {
    Assert.expect(minValue <= maxValue);
    Assert.expect(coordToPadSetMap != null);

    Set<Pad> pads = new HashSet<Pad>();

    int beginIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, minValue);
    beginIndex = Math.max(beginIndex, 0);
    int endIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, maxValue);
    endIndex = Math.min(endIndex, coords.size() - 1);

    for (int i = beginIndex; i <= endIndex; ++i)
    {
      double coord = coords.get(i);
      pads.addAll(coordToPadSetMap.get(coord));
    }

    return pads;
  }

  /**
   * @author George A. David
   */
  public double getDistanceToClosestRowOfPads(Pad pad)
  {
    Assert.expect(pad != null);
    Assert.expect(_useBoundsForDeterminingVicinity == false);

    double y = pad.getCenterCoordinateRelativeToPanelInNanoMeters().getY();

    int index = _yCoords.indexOf(y);
    Assert.expect(index >= 0);

    double distance = 0;
    if(_yCoords.size() > 1)
    {
      if (index == 0)
        distance = _yCoords.get(index + 1) - _yCoords.get(index);
      else if (index == _yCoords.size() - 1)
        distance = _yCoords.get(index) - _yCoords.get(index - 1);
      else
      {
        distance = _yCoords.get(index + 1) - _yCoords.get(index);
        distance = Math.min(distance, _yCoords.get(index) - _yCoords.get(index - 1));
      }
    }
    return distance;
  }
}
