package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.gui.algoTuner.*;

/**
 *
 * <p>Title: IndictmentDetailsComparator</p>
 *
 * <p>Description: This comparator will sort an IndictmentDetailsEntry object
 * by the desired field name</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class IndictmentDetailsComparator implements Comparator<IndictmentDetailsEntry>
{
  private boolean _ascending;
  private IndictmentDetailsComparatorEnum _field;

  private AlphaNumericComparator _alphaNumericComparator = null;

  /**
   * @author George Booth
   */
  public IndictmentDetailsComparator(boolean ascending, IndictmentDetailsComparatorEnum field)
  {
    _ascending = ascending;
    _field = field;
    _alphaNumericComparator = new AlphaNumericComparator(ascending);
  }

  /**
   * @param lhInfo lefthand IndictmentDetailsEntry
   * @param rhInfo righthand IndictmentDetailsEntry
   * @return negative if lf less than rh, zero if equal, positive if greater than
   * @author George Booth
   */
  public int compare(IndictmentDetailsEntry lhInfo, IndictmentDetailsEntry rhInfo)
  {
    Assert.expect(lhInfo != null);
    Assert.expect(rhInfo != null);

    String lhName = null;
    String rhName = null;

    if (_field.equals(IndictmentDetailsComparatorEnum.INDICTMENT))
    {
      lhName = lhInfo.getIndictment().getName();
      rhName = rhInfo.getIndictment().getName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(IndictmentDetailsComparatorEnum.TYPE))
    {
      lhName = lhInfo.getIndictmentType();
      rhName = rhInfo.getIndictmentType();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(IndictmentDetailsComparatorEnum.MEASUREMENT))
    {
      lhName = lhInfo.getMeasurement().getMeasurementEnum().getName();
      rhName = rhInfo.getMeasurement().getMeasurementEnum().getName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(IndictmentDetailsComparatorEnum.VALUE))
    {
      Float lhValue = new Float(lhInfo.getMeasurement().getValue());
      Float rhValue = new Float(rhInfo.getMeasurement().getValue());
      if (_ascending)
        return lhValue.compareTo(rhValue);
      else
        return rhValue.compareTo(lhValue);
    }
    else if (_field.equals(IndictmentDetailsComparatorEnum.SLICE))
    {
      lhName = lhInfo.getMeasurement().getSliceNameEnum().getName();
      rhName = rhInfo.getMeasurement().getSliceNameEnum().getName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else
    {
      Assert.expect(false, "unsupported comparator field enum");
      return 0;
    }
  }

}
