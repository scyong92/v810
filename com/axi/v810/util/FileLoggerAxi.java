package com.axi.v810.util;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class is a wrapper class for the com.axi.util.FileLogger class.
 * It converts all the non AXI exceptions thrown by the FileLogger class into
 * DatastoreExceptions.
 *
 * @see FileLogger
 * @author John Dutton
 */
public class FileLoggerAxi
{
  private FileLogger _fileLogger;

  /**
   * Creates logger than will grow indefinitely.
   *
   * @author John Dutton
   */
  public FileLoggerAxi(String logFileFullPathName)
  {
    _fileLogger = new FileLogger(logFileFullPathName);
  }

  /**
   * @param maxSizeOfFile specifies the largest size in bytes the log file
   * should ever reach.  Set a reasonable limit since maintaining this size requires
   * a file copy for each append after the log file is "full".
   *
   * @author John Dutton
   */
  public FileLoggerAxi(String logFileFullPathName, int maxSizeOfFile)
  {
    _fileLogger = new FileLogger(logFileFullPathName, maxSizeOfFile);
  }

  /**
   * Appends log string to log file and truncates the log file if
   * its size has grown greater than maximum size allowed.
   *
   * @author John Dutton
   */
  public void append(String logString) throws DatastoreException
  {
    try
    {
      _fileLogger.append(logString);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
    catch (FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException fileNotFoundDatastoreException =
          new FileNotFoundDatastoreException(fnfe.getMessage());
      fileNotFoundDatastoreException.initCause(fnfe);
      throw fileNotFoundDatastoreException;
    }
    catch (CouldNotDeleteFileException cndfe)
    {
      CannotDeleteFileDatastoreException cannotDeleteFileDatastoreException =
          new CannotDeleteFileDatastoreException(cndfe.getFileName());
      cannotDeleteFileDatastoreException.initCause(cndfe);
      throw cannotDeleteFileDatastoreException;
    }
    catch (CouldNotRenameFileException cnrfe)
    {
      FileCannotBeRenamedDatastoreException fileCannotBeRenamedDatastoreException =
          new FileCannotBeRenamedDatastoreException(cnrfe.getFromFileName(), cnrfe.getToFileName());
      fileCannotBeRenamedDatastoreException.initCause(cnrfe);
      throw fileCannotBeRenamedDatastoreException;
    }
    catch (CouldNotReadFileException cnreadfe)
    {
      CannotReadDatastoreException cannotReadDatastoreException =
          new CannotReadDatastoreException(cnreadfe.getFileName());
      cannotReadDatastoreException.initCause(cnreadfe);
      throw cannotReadDatastoreException;
    }
    catch (CouldNotCloseFileException cnreadfe)
    {
      CannotCloseFileDatastoreException cannotCloseFileDatastoreException =
          new CannotCloseFileDatastoreException(cnreadfe.getFileName());
      cannotCloseFileDatastoreException.initCause(cnreadfe);
      throw cannotCloseFileDatastoreException;
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex =
          new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
  }
  
   /**
   * Appends log string to log file and truncates the log file if
   * its size has grown greater than maximum size allowed.
   *
   * @author Swee Yee Wong
   */
  public void appendWithoutDateTime(String logString) throws DatastoreException
  {
    try
    {
      _fileLogger.appendWithoutDateTime(logString);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;
    }
    catch (FileNotFoundException fnfe)
    {
      FileNotFoundDatastoreException fileNotFoundDatastoreException =
          new FileNotFoundDatastoreException(fnfe.getMessage());
      fileNotFoundDatastoreException.initCause(fnfe);
      throw fileNotFoundDatastoreException;
    }
    catch (CouldNotDeleteFileException cndfe)
    {
      CannotDeleteFileDatastoreException cannotDeleteFileDatastoreException =
          new CannotDeleteFileDatastoreException(cndfe.getFileName());
      cannotDeleteFileDatastoreException.initCause(cndfe);
      throw cannotDeleteFileDatastoreException;
    }
    catch (CouldNotRenameFileException cnrfe)
    {
      FileCannotBeRenamedDatastoreException fileCannotBeRenamedDatastoreException =
          new FileCannotBeRenamedDatastoreException(cnrfe.getFromFileName(), cnrfe.getToFileName());
      fileCannotBeRenamedDatastoreException.initCause(cnrfe);
      throw fileCannotBeRenamedDatastoreException;
    }
    catch (CouldNotReadFileException cnreadfe)
    {
      CannotReadDatastoreException cannotReadDatastoreException =
          new CannotReadDatastoreException(cnreadfe.getFileName());
      cannotReadDatastoreException.initCause(cnreadfe);
      throw cannotReadDatastoreException;
    }
    catch (CouldNotCloseFileException cnreadfe)
    {
      CannotCloseFileDatastoreException cannotCloseFileDatastoreException =
          new CannotCloseFileDatastoreException(cnreadfe.getFileName());
      cannotCloseFileDatastoreException.initCause(cnreadfe);
      throw cannotCloseFileDatastoreException;
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex =
          new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
  }
}
