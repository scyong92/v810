package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;

public class Test_DosShell extends UnitTest
{
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_DosShell());
  }

  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      DosOutput dosOutput = new DosOutput();
      DosShell dos = new DosShell(dosOutput);

      // start sending dos commands
      boolean passed = dos.sendCommand("echo hello there");
      Expect.expect(passed);
      passed = dos.sendCommand("echo hi");
      Expect.expect(passed);
      passed = dos.sendCommand("cd someNonExistentDirectory");
      Expect.expect(passed == false);

      // sleep for 1 second to let the standard error message get through
      // this is poor programming - but for a unitTest it is OK
      try
      {
        Thread.sleep(1000);
      }
      catch (Exception ex)
      {
        // do nothing
      }

      dos.exit();

      dosOutput.printResults();
    }
    catch(IOException e)
    {
      e.printStackTrace();
    }
  }
}

class DosOutput implements DosOutputInt
{
  private List<String> _outputList = new ArrayList<String>();
  private List<String> _errorList = new ArrayList<String>();

  public void standardOutputLine(String line)
  {
    _outputList.add(line);
  }

  public void standardErrorLine(String line)
  {
    _errorList.add(line);
  }

  void printResults()
  {
    for (String line : _outputList)
    {
      System.out.print("standard out: " + line);
    }

    for (String line : _errorList)
    {
      System.out.print("standard error: " + line);
    }
  }
}
