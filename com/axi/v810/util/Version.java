package com.axi.v810.util;

/**
 * This class contains the version string that represents the version
 * of the entire x6000 release.  All code the displays the version
 * to the user should query this class to get that string.
 *
 * @author Bill Darbie
 */
public class Version
{
  private static final String _PERIOD = ".";
  
  /**
   * This string shows the name of the product.
   * @author Bill Darbie
   */
  public static final String getProduct()
  {
    return "v810";
  }

  /**
   * The copyright string.
   *
   * This information is set by a perl script, updateVersion.pl, located
   * in axi/buildtools/src.
   *
   * Do not modify getCopyRight(), getBuildTimeStamp() or getVersionNumber() without also
   * making appropriate changes to updateVersion.pl
   *
   * @author Rex Shang
   */
  public static final String getCopyRight()
  {
    String copyRight = "Copyright (c) 2009 - 2016";
    return copyRight;
  }

  /**
   * This string represents the version number in the format M.mm
   * where M is the major release number and mm is the minor release number
   * 8.00 would be used for the 8.0 release 7.01 for the 7.1 release.
   *
   * @author Bill Darbie
   */
  public static final String getVersion()
  {
    // NOTE that the perl script needs the line below to stay in the same format.
    // Do not change it without also updating the perl script.
    String version = getVersionNumber() + " (8765) " + getBuildTimeStamp();

    return version;
  }

  /**
   * This double value represents the version number in the format M.mm
   * where M is the major release number and mm is the minor release number
   * 8.00 would be used for the 8.0 release 7.01 for the 7.1 release.
   *
   * This function is used if the client wants to have a version number in the double format.
   * If the client needs to get the version number with the build date and time, use getVersion()
   * that is defined in the above in this class.
   *
   * This information is set by a perl script, updateVersion.pl, located
   * in axi/buildtools/src.
   *
   * Do not modify getCopyRight(), getBuildTimeStamp() or getVersionNumber() without also
   * making appropriate changes to updateVersion.pl
   *
   * @author Eugene Kim-Leigthon
   */
  public static final String getVersionNumber()
  {
    String versionNumber = Integer.toString(getMajorVersion()) + _PERIOD + Integer.toString(getMinorVersion());

    return versionNumber;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static int getMajorVersion()
  {
    int majorVersion = 5;
    return majorVersion;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static int getMinorVersion()
  {
    int minorVersion = 11;
    return minorVersion;
  }

  /**
   * This method is used by getVersion() to generate the version string.
   *
   * This information is set by a perl script, updateVersion.pl, located
   * in axi/buildtools/src.
   *
   * Do not modify getCopyRight(), getBuildTimeStamp() or getVersionNumber() without also
   * making appropriate changes to updateVersion.pl
   *
   * @author Rex Shang
   */
  public static final String getBuildTimeStamp()
  {
    String buildTimeStamp = "(Built 02/22/16 at 11:15:20)";

    return buildTimeStamp;
  }

  /**
   * Mini test harness.
   * @author Rex Shang
   */
  public static void main(String[] args)
  {
    System.out.println("Version # is " + Version.getVersion());
  }
}
