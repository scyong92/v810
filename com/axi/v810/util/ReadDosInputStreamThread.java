package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * This thread is used by the DosShell class and grabs any
 * characters from its standard input stream as soon as they appear.
 * Each line of input that is found will be sent to the DosOutputInt.standardInputLine(String line)
 * method.
 *
 * @see DosShell
 * @author Bill Darbie
 */
class ReadDosInputStreamThread extends Thread
{
  private DosShell _dos;
  private BufferedReader _is;
  private String _prompt;
  private String _commandOK;
  private String _commandFailed;
  private String _commandEnd;
  private DosOutputInt _dosOutputInt;
  private static final int _SLEEP_TIME = 200;
  private boolean _run = true;
  private boolean _captureInput = false;
  private WorkerThread _workerThread = new WorkerThread("ReadDosInputStreamWorkerThread");

  /**
   * Contruct this class
   *
   * @author Bill Darbie
   */
  ReadDosInputStreamThread(DosShell dos,
                           BufferedReader is,
                           String prompt,
                           String commandEnd,
                           String commandOK,
                           String commandFailed,
                           DosOutputInt dosOutputInt)
  {
    Assert.expect(dos != null);
    Assert.expect(is != null);
    Assert.expect(prompt != null);
    Assert.expect(commandEnd != null);
    Assert.expect(commandOK != null);
    Assert.expect(commandFailed != null);
    // dosOutputInt can be null

    _dos = dos;
    _is = is;
    _prompt = prompt;
    _commandEnd = commandEnd;
    _commandOK = commandOK;
    _commandFailed = commandFailed;
    _dosOutputInt = dosOutputInt;
  }

  /**
   * This method will cause this thread to exit
   *
   * @author Bill Darbie
   */
  void stopThread()
  {
    _run = false;
  }

  /**
   * This method gets called when the thread is run with the start method.
   *
   * @author Bill Darbie
   */
  public void run()
  {
    boolean passed = true;
    try
    {
      StringBuffer stringBuffer = new StringBuffer("");
      while(_run)
      {
        if (_is.ready())
        {
          char ch = (char)_is.read();
          stringBuffer.append(ch);
          if (stringBuffer.toString().equals(_prompt))
          {
            if (_captureInput)
            {
              _dos.readyForNextCommand(passed);
            }

            // set _captureInput to true after the first _prompt is found so we discard all the
            // misc. input that the dos prompt initially displays
            _captureInput = true;
          }
          else if (ch == '\n')
          {
            String inputString = stringBuffer.toString();
            if (inputString.startsWith(_prompt))
              inputString = stringBuffer.substring(_prompt.length(), stringBuffer.length());
            if (_captureInput)
            {
              // put the standardInputLine() call on another thread to make sure that we can keep reading
              // characters off the input stream as fast as they arrive
              String nonFilteredInput = inputString.toString();
              // dos ends every line with a \r\n - replace this with \n
              nonFilteredInput = nonFilteredInput.substring(0, nonFilteredInput.length() - 2) + "\n";
              // now strip of the "COMMAND_OK" or "COMMAND_FAILED" or "&& echo COMMAND_OK || COMMAND_FAILED"
              int startIndex = nonFilteredInput.lastIndexOf(_commandEnd);
              if (startIndex != -1)
              {
                int stopIndex = startIndex + _commandEnd.length();
                String preString = nonFilteredInput.substring(0, startIndex);
                String postString = "";
                if (stopIndex < nonFilteredInput.length())
                  postString = nonFilteredInput.substring(stopIndex, nonFilteredInput.length());
                nonFilteredInput = preString + postString;
              }
              else
              {
                startIndex = nonFilteredInput.lastIndexOf(_commandOK);
                if (startIndex != -1)
                {
                  passed = true;
                  nonFilteredInput = null;
                }
                else
                {
                  startIndex = nonFilteredInput.lastIndexOf(_commandFailed);
                  if (startIndex != -1)
                  {
                    passed = false;
                    nonFilteredInput = null;
                  }
                }
              }

              if (nonFilteredInput != null)
              {
                final String input = nonFilteredInput;
                _workerThread.invokeLater(new Runnable()
                {
                  public void run()
                  {
                    if (_dosOutputInt != null)
                      _dosOutputInt.standardOutputLine(input);
                    if (input.equalsIgnoreCase("exit\n"))
                    {
                      _dos.exitReceived();
                    }
                  }
                });
              }
            }
            stringBuffer.setLength(0);
          }
        }
        else
        {
          try
          {
            Thread.sleep(_SLEEP_TIME);
          }
          catch(InterruptedException e)
          {
            // do nothing
          }
        }
      }
    }
    catch (Throwable throwable)
    {
      Assert.logException(throwable);
    }
  }
}
