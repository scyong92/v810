package com.axi.v810.util;

/**
 * @author Laura Cormos
 */
public class PackagePinComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static PackagePinComparatorEnum JOINT_TYPE = new PackagePinComparatorEnum(++_index);
  public static PackagePinComparatorEnum JOINT_HEIGHT = new PackagePinComparatorEnum(++_index);
  public static PackagePinComparatorEnum ORIENTATION = new PackagePinComparatorEnum(++_index);
  public static PackagePinComparatorEnum PIN_NAME = new PackagePinComparatorEnum(++_index);

  /**
   * @author Laura Cormos
   */
  private PackagePinComparatorEnum(int id)
  {
    super(id);
  }
}
