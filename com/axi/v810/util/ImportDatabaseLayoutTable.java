/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;
import com.axi.guiUtil.*;
import com.axi.util.*;


/**
 *
 * @author swee-yee.wong
 */
public class ImportDatabaseLayoutTable extends JSortTable
{
  public static final int _SELECT_INDEX = 0;
  public static final int _TAG_NAME_INDEX = 1;
  public static final int _TAG_VERSION_INDEX = 2;
  public static final int _LAST_MODIFIED_DATE_INDEX = 3;
  
  //percentage of space in the table given to each column (all add up to 1.0)
  private static final double _SELECT_COLUMN_WIDTH_PERCENTAGE = .06;
  private static final double _TAG_NAME_COLUMN_WIDTH_PERCENTAGE = .44;
  private static final double _TAG_VERSION_COLUMN_WIDTH_PERCENTAGE = .25;
  private static final double _LAST_MODIFIED_DATE_COLUMN_WIDTH_PERCENTAGE = .25;

  private CheckCellEditor _checkEditor = new CheckCellEditor(getBackground(), getForeground());;
  private ImportDatabaseLayoutTableModel _importDatabaseLayoutTableModel = null;
  

  public ImportDatabaseLayoutTable(ImportDatabaseLayoutTableModel importDatabaseLayoutTableModel)
  {
    Assert.expect(importDatabaseLayoutTableModel != null);

    _importDatabaseLayoutTableModel = importDatabaseLayoutTableModel;
  }


  public void setPreferredColumnWidths()
  {
    TableColumnModel columnModel = getColumnModel();

    int totalColumnWidth = columnModel.getTotalColumnWidth();

    columnModel.getColumn(_SELECT_INDEX).setPreferredWidth((int)(totalColumnWidth * _SELECT_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_TAG_NAME_INDEX).setPreferredWidth((int)(totalColumnWidth * _TAG_NAME_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_TAG_VERSION_INDEX).setPreferredWidth((int)(totalColumnWidth * _TAG_VERSION_COLUMN_WIDTH_PERCENTAGE));
    columnModel.getColumn(_LAST_MODIFIED_DATE_INDEX).setPreferredWidth((int)(totalColumnWidth * _LAST_MODIFIED_DATE_COLUMN_WIDTH_PERCENTAGE));
  }


  public void setEditorsAndRenderers()
  {
    TableColumnModel columnModel = getColumnModel();
    columnModel.getColumn(_SELECT_INDEX).setCellEditor(_checkEditor);
    columnModel.getColumn(_SELECT_INDEX).setCellRenderer(new CheckCellRenderer());

  }


  public void setValueAt(Object value, int row, int column)
  {
    int col = convertColumnIndexToModel(column);
    if (column == _SELECT_INDEX)
    {
      int rows[] = getSelectedRows();
      for (int i = 0; i < rows.length; i++)
      {
        _importDatabaseLayoutTableModel.setValueAt(value, rows[i], col);
      }
    }
    else
      getModel().setValueAt(value, row, column);
  }


  @Override
  public String getToolTipText(MouseEvent e)
  {
    Point p = e.getPoint();
    int row = rowAtPoint(p);
    int col = columnAtPoint(p);

    if (row >= 0 && col >= 0)
    {
      return getValueAt(row, col).toString();
    }
    else
    {
      return null;
    }
  }

  public void stopCellsEditing()
  {
    TableColumnModel tableColumnModel = getColumnModel();
    tableColumnModel.getColumn(_SELECT_INDEX).getCellEditor().stopCellEditing();
  }

}
