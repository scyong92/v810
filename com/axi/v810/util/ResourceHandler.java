package com.axi.v810.util;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
* This class handles getting image and property files for the xRayTest code.
* It first tries to get the file from the xRayTest config directory (xRayTest/rxx/config/images for images and
* xRayTest/rxx/config/properties for properties).  If the file is not found there, then the file
* is grabbed from the jar file.  This allows customers to customize anything in the config
* directory.  Property files can be added to support different languages.  Images can also
* be changed for different languages.  If the customer somehow deletes or renames a file
* incorrectly then the code will go back to the known good file that exists in the jar file.
* Note that for this to work properly the classpath must be set to point to XRAYTEST_HOME.
*
* @author Bill Darbie
*/
public class ResourceHandler
{
  private static final String _IMAGES_DIR = Directory.getRelativeGuiImagesDir();
  private static final String _PROPERTIES_DIR = Directory.getRelativePropertiesDir();

  /**
  * Given an imageName find its corresponding image and return an ImageIcon.  This method will first
  * try to find the image in config/images.  If that fails then it will look in the jar file for
  * the image.  If both of these fail an exception will be logged and the method will return a null reference.
  * This method assumes that classpath points to AT5DX_HOME.
  *
  * @param jarImageURL is the URL of the image in the jar file.  Typically
  *        you would get this by using YourClass.class.getResource("image.gif")
  * @return an ImageIcon for the imageName passed in.  If none is found null is returned after
  *         an exception is logged.
  * @author Bill Darbie
  */
  public static ImageIcon getImageIcon(URL jarImageURL)
  {
    // the jarImageURL better be good
    Assert.expect(jarImageURL != null);

    // imageURL should point to the image in the jar file
    // take the imageURL and convert it to a URL that points to the same image
    // in the config/images directory
    String imageString = jarImageURL.toString();
    int index = imageString.lastIndexOf("/");
    if (index == -1)
      index = imageString.lastIndexOf("\\");
    if ((index != -1) && (index != imageString.length() - 1))
    {
      imageString = imageString.substring(index);
      // it is ok to use "/" hard-coded here since we are creating a URL
      imageString = "/" + _IMAGES_DIR + imageString;
    }
    URL imageURL = ResourceHandler.class.getResource(imageString);

    if (imageURL == null)
    {
//      DO NOT GIVE A WARNING HERE SINCE THIS IS THE NORMAL WAY THINGS HAPPEN IN JBUILDER
//      String selftests = System.getProperty("selftests", "false");
//      if (selftests.equalsIgnoreCase("false"))
//      {
//        System.out.println("WARNING: Could not find file: " + imageString);
//        System.out.println("         The classpath is needs to have XRAYTEST_HOME in it or the file is missing.");
//        System.out.println("         The file will get pulled from the jar file instead.");
//      }

      // the image is missing from the config/images directory - so use the one in the jar file
      imageURL = jarImageURL;
    }
    ImageIcon image = new ImageIcon(imageURL);

    return image;
  }

  /**
  * Given the name of a property file this method will return the ResourceBundle
  * associated with it.  This method first tries to find the property file in the
  * xRayTEst/rxx/config/properties directory.  If the file is not there then it will pick up
  * the default one that is in the jar file.  Note that the classpath must be set
  * to point to AT5DX_HOME for this to work properly.
  *
  * NOTE that to get information from the Config.properties file you should NOT use this method.
  * Use com.axi.v810.datastore.ConfigInt instead.
  *
  * @param jarPropertyName is the fully qualified name of the property file whose ResourceBundle needs to be found.
  *                          For instance com.axi.v810.properties.ServerConfig
  * @author Bill Darbie
  */
  public static ResourceBundle getPropertyBundle(String jarPropertyName)
  {
    ResourceBundle resourceBundle = null;

    String propertyName = jarPropertyName;
    int index = propertyName.lastIndexOf(".");
    if ((index != -1) && (index != propertyName.length() - 1))
    {
      // Get rid of the fully qualified package (leave only .ServerConfig)
      propertyName = propertyName.substring(index);
      propertyName = _PROPERTIES_DIR + propertyName;
    }

    // first look where the classpath is set - should be the config directory
    try
    {
      resourceBundle = ResourceBundle.getBundle(propertyName, Localization.getLocale());
    }
    catch(MissingResourceException mre)
    {
      if (System.getProperty("RUNNING_IN_IDE") == null)
      {
        if (UnitTest.unitTesting() == false)
        {
          String name = propertyName.replace('.', File.separatorChar);
          System.out.println("WARNING: Could not find file: " + name + ".properties.");
          System.out.println("         The classpath is needs to have XRAYTEST_HOME in it or the file is missing.");
          System.out.println("         The file will get pulled from the jar file instead.");
          mre.printStackTrace();
        }
      }

      // look in the jar if the classpath did not have it
      try
      {
        resourceBundle = ResourceBundle.getBundle(jarPropertyName, Localization.getLocale());
      }
      catch(MissingResourceException e)
      {
        // not good - the properties file does not exist in either the config/properties directory or the jar
        Assert.logException(e);
      }
    }

    return resourceBundle;
  }
}



