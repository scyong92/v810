package com.axi.v810.util;

/**
 * @author Laura Cormos
 */
public class LandPatternPadComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static LandPatternPadComparatorEnum NAME = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum X_COORDINATE = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum Y_COORDINATE = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum WIDTH = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum LENGTH = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum DEGREES_ROTATION = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum SHAPE = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum TYPE = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum HOLE = new LandPatternPadComparatorEnum(++_index);
  //Siew Yeng - XCR-3318
  public static LandPatternPadComparatorEnum HOLE_WIDTH = new LandPatternPadComparatorEnum(++_index);
  public static LandPatternPadComparatorEnum HOLE_LENGTH = new LandPatternPadComparatorEnum(++_index);

  /**
   * @author Laura Cormos
   */
  private LandPatternPadComparatorEnum(int id)
  {
    super(id);
  }
}
