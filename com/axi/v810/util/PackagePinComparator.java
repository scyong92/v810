package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This comparator will sort a PackagPin object based on it's algorithm family name
 * or  it's orientation
 * @author Andy Mechtenberg
 * @author Laura Cormos
 */
public class PackagePinComparator implements Comparator<PackagePin>
  {
    private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();

    private boolean _ascending;
    private PackagePinComparatorEnum _packagePinComparatorEnum;

    /**
     * @author Andy Mechtenberg
     */
    public PackagePinComparator(boolean ascending, PackagePinComparatorEnum packagePinComparatorEnum)
    {
      Assert.expect(packagePinComparatorEnum != null);

      _ascending = ascending;
      _packagePinComparatorEnum = packagePinComparatorEnum;
    }

    /**
     * @author Andy Mechtenberg
     */
    public int compare(PackagePin lhPackagePin, PackagePin rhPackagePin)
    {
      Assert.expect(lhPackagePin != null);
      Assert.expect(rhPackagePin != null);

      String lhName;
      String rhName;
      int lhInt;
      int rhInt;

      if (_packagePinComparatorEnum.equals(PackagePinComparatorEnum.JOINT_TYPE))
      {
        lhName = lhPackagePin.getJointTypeEnum().getName();
        rhName = rhPackagePin.getJointTypeEnum().getName();
      }
      else if (_packagePinComparatorEnum.equals(PackagePinComparatorEnum.JOINT_HEIGHT))
      {
        lhInt = lhPackagePin.getJointHeightInNanoMeters();
        rhInt = rhPackagePin.getJointHeightInNanoMeters();
        if (_ascending)
          return _alphaNumericComparator.compare(lhInt, rhInt);
        else
          return _alphaNumericComparator.compare(rhInt, lhInt);
      }
      else if (_packagePinComparatorEnum.equals(PackagePinComparatorEnum.ORIENTATION))
      {
        lhName = lhPackagePin.getPinOrientationEnum().toString();
        rhName = rhPackagePin.getPinOrientationEnum().toString();
      }
      else if (_packagePinComparatorEnum.equals(PackagePinComparatorEnum.PIN_NAME))
      {
        lhName = lhPackagePin.getName();
        rhName = rhPackagePin.getName();
      }
      else
      {
        Assert.expect(false, "Please add your new PackagePin attribute to sort on to this method");
        return 0;
      }

      if(_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
  }
