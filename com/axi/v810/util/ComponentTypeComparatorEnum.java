package com.axi.v810.util;

/**
 * <p>Title: ComponentTypeComparatorEnum</p>
 *
 * <p>Description: Provide keys for sorting ComponentType table entries, used in
 * conjunction with the ComponentTypeComparator</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class ComponentTypeComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ComponentTypeComparatorEnum REFERENCE_DESIGNATOR = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum X_COORDINATE = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum Y_COORDINATE = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum DEGREES_ROTATION = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum JOINT_TYPE = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum SUBTYPE = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum PACKAGE = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum ARTIFACT_COMPENSATION = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum SIGNAL_COMPENSATION = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum GSM_COMPENSATION = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum USER_GAIN = new ComponentTypeComparatorEnum(++_index);
  public static ComponentTypeComparatorEnum PSP = new ComponentTypeComparatorEnum(++_index);
  //Khaw Chek Hau - XCR3554: Package on package (PoP) development
  public static ComponentTypeComparatorEnum LAYER_ID = new ComponentTypeComparatorEnum(++_index);
  
  /**
   * @author George Booth
   */
  private ComponentTypeComparatorEnum(int id)
  {
    super(id);
  }
}
