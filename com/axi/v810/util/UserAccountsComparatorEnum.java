package com.axi.v810.util;

/**
 *
 * <p>Title: UserAccountsComparatorEnum</p>
 *
 * <p>Description: Enumeration of fields to be sorted by the UserAccountsComparator.</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class UserAccountsComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static UserAccountsComparatorEnum ACCOUNT_NAME = new UserAccountsComparatorEnum(++_index);
  public static UserAccountsComparatorEnum FULL_NAME = new UserAccountsComparatorEnum(++_index);
  public static UserAccountsComparatorEnum ACCOUNT_TYPE = new UserAccountsComparatorEnum(++_index);

  /**
   * @author George Booth
   */
  private UserAccountsComparatorEnum(int id)
  {
    super(id);
  }
}
