package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * Given two pads, determine which one is closer
 * to the reference point.
 * @author George A. David
 */
public class PadDistanceComparator implements Comparator<Pad>
{
  private double _xReferenceNanoMeters;
  private double  _yReferenceNanoMeters;

//  public static boolean _printDebugInfo = false;

  /**
   * @author George A. David
   */
  public PadDistanceComparator(double xReferenceNanoMeters, double yReferenceNanoMeters)
  {
    _xReferenceNanoMeters = xReferenceNanoMeters;
    _yReferenceNanoMeters = yReferenceNanoMeters;
  }
  /**
   * Compares its two arguments for order.
   *
   * @param lhs the first object to be compared.
   * @param rhs the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the first
   *   argument is less than, equal to, or greater than the second.
   *
   * @author George A. David
   */
  public int compare(Pad lhs, Pad rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    // they're the same exact object!!!
    if(lhs == rhs)
      return 0;

    PanelCoordinate firstCoord = lhs.getCenterCoordinateRelativeToPanelInNanoMeters();
    PanelCoordinate secondCoord = rhs.getCenterCoordinateRelativeToPanelInNanoMeters();
    double firstDistanceSquared = MathUtil.calculateDistanceSquared(firstCoord.getX(), firstCoord.getY(),
                                                                    _xReferenceNanoMeters, _yReferenceNanoMeters);
    double secondDistanceSquared = MathUtil.calculateDistanceSquared(secondCoord.getX(), secondCoord.getY(),
                                                                    _xReferenceNanoMeters, _yReferenceNanoMeters);

//    if(_printDebugInfo)
//    {
//      System.out.println("lhs pad: " + lhs.getName());
//      System.out.println("lhs distance: " + firstDistanceSquared);
//      System.out.println("lhs coord: " + firstCoord.getX() + " " + firstCoord.getY());
//      System.out.println("rhs pad: " + rhs.getName());
//      System.out.println("rhs distance: " + secondDistanceSquared);
//      System.out.println("rhs coord: " + secondCoord.getX() + " " + secondCoord.getY());
//    }

    if (firstDistanceSquared > secondDistanceSquared)
      return 1;
    else if(firstDistanceSquared < secondDistanceSquared)
      return -1;
    else
    {
      // ok, they are equal, but if we have to items in an unsorted list
      // and both are equal, then the first item in the list gets priority
      // I don' t want to leave it up to chance, so I'll try on last thing,
      // first look at x then y. The one with less x, then less y if necessary is first,
      // otherwise, just return 0.
      if(firstCoord.getX() < secondCoord.getX())
        return -1;
     else if(firstCoord.getX() > secondCoord.getX())
       return 1;
     else if(firstCoord.getY() < secondCoord.getY())
       return -1;
     else if(firstCoord.getY() > secondCoord.getY())
       return 1;
     else // they are on the same exact spot!!!
      return 0;
    }
  }
}
