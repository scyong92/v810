package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.util.*;
import com.axi.v810.gui.testDev.*;

/**
 * @author Bill Darbie
 */
public class ComponentComparator implements Comparator<Component>
{
  private AlphaNumericComparator _alphaComparator;
  private ComponentComparatorEnum _comparingAttribute;
  private boolean _ascending;

  /**
    * @author Laura Cormos
    */
   public ComponentComparator(boolean ascending, ComponentComparatorEnum comparingAttribute)
   {
     Assert.expect(comparingAttribute != null);
     _ascending = ascending;
     _comparingAttribute = comparingAttribute;
     _alphaComparator = new AlphaNumericComparator(_ascending);
  }

  /**
   * @author Laura Cormos
   */
  public int compare(Component lhs, Component rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsString;
    String rhsString;
    int lhsInt;
    int rhsInt;
    Double lhsDouble;
    Double rhsDouble;

    if (_comparingAttribute.equals(ComponentComparatorEnum.FULLY_QUALIFIED_NAME))
    {
        lhsString = lhs.getBoardNameAndReferenceDesignator();
        rhsString = rhs.getBoardNameAndReferenceDesignator();
        return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.REFERENCE_DESIGNATOR))
    {
        lhsString = lhs.getReferenceDesignator();
        rhsString = rhs.getReferenceDesignator();
        return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.BOARD_NAME))
    {
        lhsString = lhs.getBoard().getName();
        rhsString = rhs.getBoard().getName();
        return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.X_COORDINATE))
    {
        lhsInt = lhs.getCoordinateInNanoMeters().getX();
        rhsInt = rhs.getCoordinateInNanoMeters().getX();
        if (lhsInt > rhsInt)
          if (_ascending)
            return 1;
          else
            return -1;
        else if (lhsInt == rhsInt)
          return 0;
        else
        if (_ascending)
          return -1;
        else
            return 1;
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.Y_COORDINATE))
    {
        lhsInt = lhs.getCoordinateInNanoMeters().getY();
        rhsInt = rhs.getCoordinateInNanoMeters().getY();
        if (lhsInt > rhsInt)
          if (_ascending)
            return 1;
          else
            return -1;
        else if (lhsInt == rhsInt)
          return 0;
        else
        if (_ascending)
          return -1;
        else
          return 1;
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.DEGREES_ROTATION))
    {
        lhsDouble = lhs.getDegreesRotationRelativeToBoard();
        rhsDouble = rhs.getDegreesRotationRelativeToBoard();
        if (_ascending)
          return lhsDouble.compareTo(rhsDouble);
        else
          return rhsDouble.compareTo(lhsDouble);
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.SIDE_BOARD))
    {
        if (lhs.getSideBoard().isSideBoard1())
          lhsString = StringLocalizer.keyToString("CAD_SIDE1_KEY");
        else
          lhsString = StringLocalizer.keyToString("CAD_SIDE2_KEY");
        if (rhs.getSideBoard().isSideBoard1())
          rhsString = StringLocalizer.keyToString("CAD_SIDE1_KEY");
        else
          rhsString = StringLocalizer.keyToString("CAD_SIDE2_KEY");
        return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.JOINT_TYPE))
    {
      lhsString = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      rhsString = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");      
      if (lhs.getCompPackage().usesOneJointTypeEnum())
        lhsString = lhs.getCompPackage().getJointTypeEnum().getName();
      if (rhs.getCompPackage().usesOneJointTypeEnum())
        rhsString = rhs.getCompPackage().getJointTypeEnum().getName();
      if(_ascending)
        return lhsString.compareToIgnoreCase(rhsString);
      else
        return rhsString.compareToIgnoreCase(lhsString);
    }
    else if (_comparingAttribute.equals(ComponentComparatorEnum.STATUS))
    {
      lhsString = TestDev.getComponentDisplayStatus(lhs.getComponentType());
      rhsString = TestDev.getComponentDisplayStatus(rhs.getComponentType());

      if(_ascending)
        return lhsString.compareToIgnoreCase(rhsString);
      else
        return rhsString.compareToIgnoreCase(lhsString);
    }
    else
    {
      Assert.expect(false, "Please add your new component attribute to sort on to this method");
      return 0;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setComponentComparatorEnum(ComponentComparatorEnum componentComparatorEnum)
  {
    Assert.expect(componentComparatorEnum != null);
    _comparingAttribute = componentComparatorEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setAscending(boolean ascending)
  {
    _ascending = ascending;
    _alphaComparator.setAscending(ascending);
  }
}
