package com.axi.v810.util;

/**
 * @author Kristin Casterton
 */
public class HTMLDocument
{
  private String _html;

  /**
   * @author Kristin Casterton
   */
  public HTMLDocument()
  {
    _html = new String("");
  }

  /**
   * @author Kristin Casterton
   */
  public void addTitle(String title)
  {
    _html = _html + "<title>" + title + "</title>";
  }

  /**
   * @author Kristin Casterton
   */
  public void addLink(String displayName, String url)
  {
    _html = _html + "<a href=\"" + url + "\">" + displayName + "</a href>";
  }

  /**
   * @author Kristin Casterton
   */
  public void addHeader(String header)
  {
    _html = _html + "<h1>" + header + "</h1>";
  }

  /**
   * @author Kristin Casterton
   */
  public void addAnchor(String anchor)
  {
    _html = _html + "<a name=" + anchor + ">";
  }

  /**
   * @author Kristin Casterton
   */
  public void addTextWithBreak(String text)
  {
    _html = _html + text + "<BR>";
  }

  /**
   * @author Kristin Casterton
   */

   public void addText(String text)
   {
    _html = _html + text;
   }
  /**
   * @author Kristin Casterton
   */
  public String getHTML()
  {
    return _html;
  }

  /**
   * @author Kristin Casterton
   */
  public void addBeginHTMLTag()
  {
    _html = _html + "<HTML>";
  }

  /**
   * @author Kristin Casterton
   */
  public void addEndHTMLTag()
  {
    _html = _html + "</HTML>";
  }

  /**
   * @author Kristin Casterton
   */
   public void addHTML(String htmlTag)
   {
    _html = _html + htmlTag;
   }

  /**
   * @author Kristin Casterton
   */
  public void addBoldText(String text)
  {
    _html = _html + "<b>" + text + "</b>";
  }

  /**
   * @author Kristin Casterton
   */
  public void addBreak()
  {
    _html = _html + "<br>";
  }

  /**
   * @author Kristin Casterton
   */
  public void addItalicText(String text)
  {
    _html = _html + "<i>" + text + "</i>";
  }

  /**
   * @author Kristin Casterton
   */
  public void addBoldTextFontSize(String text, int fontSize)
  {
    _html = _html + "<br><br><font size=" + Integer.toString(fontSize) + ">" + text + "</font></b>";
  }
}
