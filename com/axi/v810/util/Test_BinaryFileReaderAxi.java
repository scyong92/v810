package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author Ronald Lim
 */

public class Test_BinaryFileReaderAxi extends UnitTest
{
  /**
   * @author Ronald Lim
   */

  public static void main (String[] args)
  {
    UnitTest.execute(new Test_BinaryFileReaderAxi());
  }
  public void test(BufferedReader is, PrintWriter os)
  {
    testReadFileInBytes(os);
  }

  private void testReadFileInBytes(PrintWriter os)
  {
    Assert.expect(os != null);

    String filePath = getTestDataDir() + File.separator + "Test_BinaryFileReaderAxi" + File.separator + "xrayCameraFirmware.ace";
    byte[] bytes = null;

    try
    {
      BinaryFileReaderAxi fileReader = new BinaryFileReaderAxi();
      bytes = fileReader.readFileInByte(filePath);
    }
    catch ( Exception exp)
    {
      Expect.expect(false);
      exp.printStackTrace(os);
    }

    os.println("The following is the binary contents of the file " + UnitTest.stripOffViewName(filePath));
    os.println("The length of the file is " + bytes.length + " bytes.");
    for(int i = 0; i < bytes.length; ++i)
    {
      os.println("bytes[" + i + "] = " + bytes[i]);
    }

  }
}
