package com.axi.v810.util;

/**
* This class provides strings that represent typical units used by
* the AT5dx.
*
* @author Bill Darbie
*/
public class Units
{
  public static final String NONE = "none";

  // volts
  public static final String KVOLTS = "kVolts";
  public static final String VOLTS = "volts";
  public static final String MVOLTS = "mVolts";
  public static final String UVOLTS = "uVolts";

  // amps
  public static final String KAMPS = "kAmps";
  public static final String AMPS = "amps";
  public static final String MAMPS = "mAmps";
  public static final String UAMPS = "uAmps";

  // pressure
  public static final String PSI = "PSI";

  // temperature
  public static final String CELCIUS = "degrees Celcius";
                                  
  // resolution
  public static final String MODULATION = "Modulation %";
}
