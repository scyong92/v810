package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class Test_XrayTesterException extends UnitTest
{
  /**
   * @author Bill Darbie
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_XrayTesterException());
  }

  /**
   * @author Bill Darbie
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Object[] args = new Object[]{"arg1" , "arg2"};
    LocalizedString ls = new LocalizedString("A_KEY", args);
    XrayTesterException exception = new XrayTesterException(ls);
  }
}