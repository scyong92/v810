/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.util;

import java.awt.geom.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 *
 * @author khang-shian.sham
 */
public class ProgramVerificationUtil
{

  private static String _landPatternOverlapList;

  /**
   * Determine if the given project consist of 2 pin device land pattern overlapping
   * @author Khang Shian, Sham
   */
  public static boolean isProjectHas2PinDeviceLandPatternOverlapping(Project project)
  {
    Assert.expect(project != null);

    for (LandPattern landPattern : project.getPanel().getAllLandPatterns())
    {
      if (is2PinDeviceLandPattern(landPattern))
      {
        if (is2PinDeviceLandPatternOverlap(landPattern))
        {
          _landPatternOverlapList = landPattern.getName() + "\n\r";
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * Determine if the given project consist of 2 pin device land pattern and modify if their orientation is wrong 
   * @author Jack Hwee
   */
  public static void checkAndModifyTwoPinCompPackageOrientation(Project project)
  {
    Assert.expect(project != null);

    for (CompPackage compPackage : project.getPanel().getCompPackages())
    {
      checkAndModifyCompPackageOrientation(compPackage);
    }
  }
  
   /**
   * Determine and modify if the given land pattern is in wrong orientation in a 2 pin device (cap, res or pcap)
   * @author Jack Hwee
   */
  public static void checkAndModifyCompPackageOrientation(CompPackage compPackage)
  {
    Assert.expect(compPackage != null);
   
    if (compPackage.usesOneJointTypeEnum())
    {
      if (compPackage.getJointTypeEnum().equals(JointTypeEnum.RESISTOR) 
          || compPackage.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR)
          || compPackage.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP))
      {
        PackagePin packagePinOne = compPackage.getPackagePins().get(0);
        java.awt.Rectangle padOneShape = packagePinOne.getShapeInNanoMeters().getBounds();
        PinOrientationAfterRotationEnum padOrientationEnumOne = packagePinOne.getPinOrientationAfterRotationEnum();

        PackagePin packagePinTwo = compPackage.getPackagePins().get(1);         
        java.awt.Rectangle padTwoShape = packagePinTwo.getShapeInNanoMeters().getBounds();
        PinOrientationAfterRotationEnum padOrientationEnumTwo = packagePinTwo.getPinOrientationAfterRotationEnum();

        if (padOneShape.getMinX() == padTwoShape.getMinX())
        {
           if (padOneShape.getMinY() < padTwoShape.getMinY())
           {
             if ((padOrientationEnumOne.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN) 
                     && padOrientationEnumTwo.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN)) == false)
             {                    
                packagePinOne.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
                packagePinTwo.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
             }        
           }
           else
           {
             if ((padOrientationEnumOne.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN) 
                     && padOrientationEnumTwo.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN)) == false)
             {                     
                packagePinOne.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_SOUTH_OF_PIN);
                packagePinTwo.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_NORTH_OF_PIN);
             }      
           }      
        }

        if (padOneShape.getMinY() == padTwoShape.getMinY())
        {
           if (padOneShape.getMinX() > padTwoShape.getMinX())
           {
             if ((padOrientationEnumOne.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN) 
                     && padOrientationEnumTwo.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN)) == false)
             {
                packagePinOne.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);
                packagePinTwo.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);           
             }          
           }
           else
           {
             if ((padOrientationEnumOne.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN) 
                     && padOrientationEnumTwo.equals(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN)) == false)
             {
                packagePinOne.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_EAST_OF_PIN);
                packagePinTwo.setPinOrientationAfterRotationEnum(PinOrientationAfterRotationEnum.COMPONENT_IS_WEST_OF_PIN);         
             }          
           }            
        }              
      }
    }
  }

  /**
   * Determine if the given land pattern is used by a 2 pin device (cap, res or pcap)
   * @author Laura Cormos
   */
  public static boolean is2PinDeviceLandPattern(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);

    if (landPattern.getLandPatternPads().size() == 2)
    {
      java.util.List<ComponentType> compTypesForLP = landPattern.getComponentTypes();
      for (ComponentType ct : compTypesForLP)
      {
        java.util.List<JointTypeEnum> jointTypeEnumsForCompType = ct.getJointTypeEnums();
        for (JointTypeEnum jte : jointTypeEnumsForCompType)
        {
          if (jte.equals(JointTypeEnum.RESISTOR)
            || jte.equals(JointTypeEnum.CAPACITOR)
            || jte.equals(JointTypeEnum.POLARIZED_CAP)
            // Wei Chin (tall cap)
//            || jte.equals(JointTypeEnum.TALL_CAPACITOR)
            )
          {
            return true;
          }
        }
      }
    }
    return false;
  }

  /**
   * This determines whether the shapes (bounding rectangle) of 2 LPP in a 2 pin LP are overlapping with the new
   *  LP dimensions and location.
   * @author Laura Cormos
   */
  public static boolean does2PinDeviceHaveOverlappingPads(LandPatternPad landPatternPad,
    double newXCoordInNanoMeters,
    double newYCoordInNanoMeters,
    int newXDimInNanoMeters,
    int newYDimInNanoMeters)
  {
    LandPattern landPattern = landPatternPad.getLandPattern();
    Assert.expect(landPattern.getLandPatternPads().size() == 2);
    LandPatternPad otherLPP = null;
    for (LandPatternPad pad : landPattern.getLandPatternPads())
    {
      if (pad != landPatternPad)
      {
        otherLPP = pad;
      }
    }
    Assert.expect(otherLPP != null);
    java.awt.Shape thisLandPatternPadShape = landPatternPad.getHypotheticalShapeInNanoMeters(newXCoordInNanoMeters,
      newYCoordInNanoMeters,
      newXDimInNanoMeters,
      newYDimInNanoMeters);
    java.awt.Shape otherLandPatternPadShape = otherLPP.getShapeInNanoMeters();
    if (thisLandPatternPadShape.intersects((Rectangle2D) otherLandPatternPadShape.getBounds2D()))
    {
      return true;
    }
    return false;
  }

  /**
   * This determines whether the shapes (bounding rectangle) of 2 LPP in a 2 pin LP are overlapping or not
   * @author khang shian, sham
   */
  public static boolean is2PinDeviceLandPatternOverlap(LandPattern landPattern)
  {
    Assert.expect(landPattern != null);
    Assert.expect(landPattern.getLandPatternPads().size() == 2);

    LandPatternPad padOne = landPattern.getLandPatternPads().get(0);
    LandPatternPad padTwo = landPattern.getLandPatternPads().get(1);
    java.awt.Shape shapeOne = padOne.getShapeInNanoMeters();
    java.awt.Shape shapeTwo = padTwo.getShapeInNanoMeters();

    if (shapeOne.intersects((Rectangle2D) shapeTwo.getBounds2D()))
    {
      return true;
    }

    return false;
  }

  /**
   * @author khang-shian, sham
   */
  public static String getOverlapLandPatternNameInString()
  {
    Assert.expect(_landPatternOverlapList != null);

    return _landPatternOverlapList;
  }
  
  /**
   * Determine if the given project consist of high mag subtype and change it to low mag
   * @author Jack Hwee
   */
  public static void convertSubtypeMagnificationHighToLow(Project project)
  {
    Assert.expect(project != null);

    for (Subtype subtype : project.getPanel().getSubtypes())
    {
      if (subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      {
        subtype.getSubtypeAdvanceSettings().setMagnificationType(MagnificationTypeEnum.LOW);
      }
    }
  }
}
