package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This comparator will sort a CompPackage object by either the CompPackage name
 * or the Algorithm Family Name assigned to the CompPackage
 * @author Andy Mechtenberg
 */
public class CompPackageComparator implements Comparator<CompPackage>
{
  private static AlphaNumericComparator _alphaNumericComparator = new AlphaNumericComparator();
  private boolean _ascending;
  private CompPackageComparatorEnum _compPackageComparatorEnum;

  /**
   * If sortOnPackageName is true, it will sort on name, else it will sort on Family assigned to CompPackage
   * @author Andy Mechtenberg
   */
  public CompPackageComparator(boolean ascending, CompPackageComparatorEnum compPackageComparatorEnum)
  {
    _ascending = ascending;
    _compPackageComparatorEnum = compPackageComparatorEnum;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int compare(CompPackage lhPackage, CompPackage rhPackage)
  {
    Assert.expect(lhPackage != null);
    Assert.expect(rhPackage != null);

    String lhName;
    String rhName;

    if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.LONG_NAME_ALPHANUMERICALLY))
    {
      lhName = lhPackage.getLongName();
      rhName = rhPackage.getLongName();

      if (_ascending)
        return _alphaNumericComparator.compare(lhName, rhName);
      else
        return _alphaNumericComparator.compare(rhName, lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.LONG_NAME))
    {
      lhName = lhPackage.getLongName();
      rhName = rhPackage.getLongName();

      if(_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.JOINT_TYPE))
    {
      lhName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      rhName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      if (lhPackage.usesOneJointTypeEnum())
        lhName = lhPackage.getJointTypeEnum().getName();
      if (rhPackage.usesOneJointTypeEnum())
        rhName = rhPackage.getJointTypeEnum().getName();
      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.JOINT_HEIGHT))
    {
      lhName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      rhName = StringLocalizer.keyToString("MMGUI_GROUPINGS_MIXED_KEY");
      if (lhPackage.usesOneJointHeight())
        lhName = (new Integer(lhPackage.getJointHeightInNanoMeters())).toString();
      if (rhPackage.usesOneJointHeight())
        rhName = (new Integer(rhPackage.getJointHeightInNanoMeters())).toString();
      if (_ascending)
        return _alphaNumericComparator.compare(lhName, rhName);
      else
        return _alphaNumericComparator.compare(rhName, lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.IS_IMPORTED))
    {
      lhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
      rhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");

      if(lhPackage.isImported())
          lhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
      if(rhPackage.isImported())
          rhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.LIBRARY_PATH))
    {
      lhName = StringLocalizer.keyToString("PLWK_EXPORT_NONE_SOURCE_LIBRARY_TABLE_FIELD_KEY");
      rhName = StringLocalizer.keyToString("PLWK_EXPORT_NONE_SOURCE_LIBRARY_TABLE_FIELD_KEY");

      if(lhPackage.hasLibraryPath())
        lhName = lhPackage.getLibraryPath();
      if(rhPackage.hasLibraryPath())
        rhName = rhPackage.getLibraryPath();

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.NUMBER_OF_PADS))
    {
      int lhNumberOfPads = lhPackage.getLandPattern().getNumberOfLandPatternPads();
      int rhNumberOfPads = rhPackage.getLandPattern().getNumberOfLandPatternPads();

      if (_ascending)
        return _alphaNumericComparator.compare(lhNumberOfPads, rhNumberOfPads);
      else
        return _alphaNumericComparator.compare(lhNumberOfPads, rhNumberOfPads);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.HAS_MODIFIED_SUBTYPE))
    {
      lhName = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");
      rhName = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");

      if(lhPackage.hasSourceLibrary())
      {
        if( lhPackage.hasSubtypeModified())
          lhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          lhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
      }

      if(rhPackage.hasSourceLibrary())
      {
        if( rhPackage.hasSubtypeModified())
          rhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          rhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
      }

      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.IS_MODIFIED))
    {
      lhName = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");
      rhName = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");

      if(lhPackage.hasLibraryCheckSum())
      {
        if( lhPackage.isModifiedAfterImported())
          lhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          lhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
      }

      if(rhPackage.hasLibraryCheckSum())
      {
        if( rhPackage.isModifiedAfterImported())
          rhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          rhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
      }
      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else if (_compPackageComparatorEnum.equals(CompPackageComparatorEnum.OVERWRITE))
    {
      lhName = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");
      rhName = StringLocalizer.keyToString("PLWK_NA_STRING_KEY");

      if(lhPackage.hasLibraryCheckSum() && lhPackage.isModifiedAfterImported())
      {
        if( lhPackage.isOverwriteToLibrary())
          lhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          lhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
      }

      if(rhPackage.hasLibraryCheckSum() && rhPackage.isModifiedAfterImported())
      {
        if( rhPackage.isOverwriteToLibrary())
          rhName = StringLocalizer.keyToString("PLWK_YES_STRING_KEY");
        else
          rhName = StringLocalizer.keyToString("PLWK_NO_STRING_KEY");
      }
      if (_ascending)
        return lhName.compareToIgnoreCase(rhName);
      else
        return rhName.compareToIgnoreCase(lhName);
    }
    else
    {
      Assert.expect(false, "Please add your new compPackage attribute to sort on to this method");
      return 0;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setCompPackageComparatorEnum(CompPackageComparatorEnum compPackageComparatorEnum)
  {
    Assert.expect(compPackageComparatorEnum != null);
    _compPackageComparatorEnum = compPackageComparatorEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setAscending(boolean ascending)
  {
    _ascending = ascending;
  }
}
