/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.util;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import java.util.*;
import java.io.*;
/**
 * @author swee-yee.wong
 */
public class InfoHandlerDatabaseManager
{

  private static InfoHandlerDatabaseManager _instance;
  private static InfoHandlerDatabaseManager _instance2;
  private static InfoHandlerDatabaseUtil _databaseInstance = new InfoHandlerDatabaseUtil();
  private static InfoHandlerDatabaseUtil _databaseInstance2 = new InfoHandlerDatabaseUtil();
  private List<String> _infoTagFullList = new ArrayList<String>();
  private static double _newVersion = -1.0;
  private static boolean _isNewVersionGet = false;
  private static boolean isDriverStartUp = false;
  private String _libraryPath;
  private final static String _INFO_ENUM_FULL_LIST_FILE = "infoTagFullList.txt";
  private static transient DatabaseObservable _databaseObservable;
  private final static char _OPEN_VALUE_MATCHER = '{';
  private final static char _END_VALUE_MATCHER = '}';


  /**
   * @author Swee Yee Wong
   */
  public static synchronized InfoHandlerDatabaseManager getInstance()
  {
    if (_instance == null)
    {
      _instance = new InfoHandlerDatabaseManager();
    }
    return _instance;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public static synchronized InfoHandlerDatabaseManager getInstance2()
  {
    if (_instance2 == null)
    {
      _instance2 = new InfoHandlerDatabaseManager();
    }
    return _instance2;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void openConnection()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    
    if (currentDatabase.isDatabaseConnected() == false)
    {
      try
      {
        startupDatabaseDriver();
        currentDatabase.openDatabaseConnection(_libraryPath);
        if (currentDatabase.isDatabaseConnected() == true)
        {
          populateData();
          if (this == _instance)
          {
            if (_databaseObservable == null)
            {
              _databaseObservable = DatabaseObservable.getInstance();
            }
            DatabaseChangeEvent databaseChangeEvent = null;
            databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.UPDATE_VERSION, getDatabaseVersion());
            _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
          }
        }
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void closeConnection()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    if (currentDatabase.isDatabaseConnected())
    {
      try
      {
        currentDatabase.closeDatabaseConnection();
        unpopulateData();
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to close database driver.");
      }
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void createInfoHandler()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      currentDatabase.createInfoHandlerDatabase(_libraryPath);
      currentDatabase.insertInfoHandlerDatabase(_libraryPath, 0, "version", null, null, 1.0);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void deleteInfoHandler(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    boolean check = false;
    try
    {
      check = currentDatabase.deleteInfoHandlerDatabase(_libraryPath, tagName);
      if (check == true)
      {
        if (_databaseObservable == null)
        {
          _databaseObservable = DatabaseObservable.getInstance();
        }
        DatabaseChangeEvent databaseChangeEvent = null;
        databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.DELETE, tagName);
        _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
        deleteTagFromFullList(tagName);
      }
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void printInfoHandler(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      currentDatabase.printInfoHandlerDatabase(_libraryPath, tagName);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public boolean isTagExists(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      return(currentDatabase.isTagExistsDatabase(_libraryPath, tagName));
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
      return false;
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void insertInfoHandler(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    boolean check = false;
    String defaultHtml = InfoHandlerConfig.getInstance().getInfoHandlerDefaultHtml();
    try
    {
      double newVersion = getNewVersion();
      check = currentDatabase.insertInfoHandlerDatabase(_libraryPath, 0, tagName, defaultHtml, null, newVersion);
      if (check == true)
      {
        if (_databaseObservable == null)
        {
          _databaseObservable = DatabaseObservable.getInstance();
        }
        DatabaseChangeEvent databaseChangeEvent = null;
        databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.INSERT, tagName);
        _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
        databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.UPDATE_VERSION, getDatabaseVersion());
        _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
        insertTagToFullList(tagName);
      }
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void insertInfoHandler(List<String> tagNameList)
  {
    for (String tagName : tagNameList)
    {
      insertInfoHandler(tagName);
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public String getTagDateTime(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    String dateTime = null;
    try
    {
      dateTime = currentDatabase.getTagDateTimeDatabase(_libraryPath, tagName);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
    return dateTime;
  }

  /**
   * @author Swee Yee Wong
   */
  public String getTagHtml(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if (this == _instance2)
      currentDatabase = _databaseInstance2;
    
    String htmlContent = null;
    try
    {
      htmlContent = currentDatabase.getTagHtmlDatabase(_libraryPath, tagName);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
    htmlContent = resolveValueInHtmlContent(htmlContent, 
      InfoHandlerPanel.getInstance().getValueFromMap(tagName));
    return htmlContent;
  }

  /**
   * @author Swee Yee Wong
   */
  public String getTagMediaList(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    String mediaList = null;
    try
    {
      mediaList = currentDatabase.getTagMediaListDatabase(_libraryPath, tagName);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
    return mediaList;
  }

  /**
   * @author Swee Yee Wong
   */
  public double getTagVersion(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    double tagVersion = 0.0;
    try
    {
      tagVersion = currentDatabase.getTagVersionDatabase(_libraryPath, tagName);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
    return tagVersion;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public int getTagID(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    int ID = 0;
    try
    {
      ID = currentDatabase.getTagIDDatabase(_libraryPath, tagName);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
    return ID;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public int getTagParentID(String tagName)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    int parentID = 0;
    try
    {
      parentID = currentDatabase.getTagParentIDDatabase(_libraryPath, tagName);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
    return parentID;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public double getDatabaseVersion()
  {
    return getTagVersion("version");
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void unpopulateVersion()
  {
    _newVersion = -1.0;
    _isNewVersionGet = false;
  }

  /**
   * @author Swee Yee Wong
   */
  public void updateTag(int parentID, String tagName, String htmlContent, String pictureList)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      double newVersion = getNewVersion();
      currentDatabase.updateTagDatabase(_libraryPath, parentID, tagName, htmlContent, pictureList, newVersion);
      if (_databaseObservable == null)
      {
        _databaseObservable = DatabaseObservable.getInstance();
      }
      DatabaseChangeEvent databaseChangeEvent = null;
      databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.UPDATE_VERSION, getDatabaseVersion());
      _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void updateTagHtmlContent(String tagName, String htmlContent)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      double newVersion = getNewVersion();
      currentDatabase.updateTagHtmlContentDatabase(_libraryPath, tagName, htmlContent, newVersion);
      if (_databaseObservable == null)
      {
        _databaseObservable = DatabaseObservable.getInstance();
      }
      DatabaseChangeEvent databaseChangeEvent = null;
      databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.UPDATE_VERSION, getDatabaseVersion());
      _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void updateTagMediaList(String tagName, String mediaList)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      double newVersion = getNewVersion();
      currentDatabase.updateTagMediaListDatabase(_libraryPath, tagName, mediaList, newVersion);
      if (_databaseObservable == null)
      {
        _databaseObservable = DatabaseObservable.getInstance();
      }
      DatabaseChangeEvent databaseChangeEvent = null;
      databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.UPDATE_VERSION, getDatabaseVersion());
      _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void updateTagParentID(String tagName, int parentID)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      double newVersion = getNewVersion();
      currentDatabase.updateTagParentIDDatabase(_libraryPath, tagName, parentID, newVersion);
      if (_databaseObservable == null)
      {
        _databaseObservable = DatabaseObservable.getInstance();
      }
      DatabaseChangeEvent databaseChangeEvent = null;
      databaseChangeEvent = new DatabaseChangeEvent(DatabaseChangeTypeEnum.UPDATE_VERSION, getDatabaseVersion());
      _databaseObservable.sendDatabaseChangeEvent(databaseChangeEvent);
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  private List<String> getFullPictureList()
  {
    int index, index2;
    String mediaStr;
    String mediaType;
    boolean fileExists = false;
    List<String> fullPictureList = new ArrayList<String>();
    
    _infoTagFullList = getDatabaseFullList();
    for (String tag : _infoTagFullList)
    {
      String pictureList = getTagMediaList(tag);
      if (pictureList != null)
      {
        index = pictureList.indexOf(',');
        while (index > 0)
        {
          fileExists = false;
          mediaStr = pictureList.substring(0, index);
          index2 = mediaStr.indexOf('/');
          if (index2 > 0)
          {
            mediaType = mediaStr.substring(0, index2);
            if (mediaType.equals("p"))
            {
              mediaStr = mediaStr.substring(index2 + 1);
              for (String picture : fullPictureList)
              {
                if (picture.equals(mediaStr))
                {
                  fileExists = true;
                  break;
                }
              }
              if (fileExists == false)
              {
                fullPictureList.add(mediaStr);
              }
            }
            pictureList = pictureList.substring(index + 1);
            index = pictureList.indexOf(',');
          }
        }
        index2 = pictureList.indexOf('/');
        if (index2 > 0)
        {
          fileExists = false;
          mediaType = pictureList.substring(0, index2);
          if (mediaType.equals("p"))
          {
            pictureList = pictureList.substring(index2 + 1);
            for (String picture : fullPictureList)
            {
              if (picture.equals(pictureList))
              {
                fileExists = true;
                break;
              }
            }
            if (fileExists == false)
            {
              fullPictureList.add(pictureList);
            }
          }
        }
      }
    }
    return fullPictureList;
  }

  public List<String> validateTagList()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    List<String> infoTagListInput = readTagList();
    List<String> missingTagList = new ArrayList<String>();
    if (currentDatabase.isDatabaseConnected())
    {
      boolean tagExists = false;
      if (infoTagListInput.isEmpty() == false)
      {
        for (String inputTag : infoTagListInput)
        {
          for (String databaseTag : _infoTagFullList)
          {
            if (inputTag.equals(databaseTag))
            {
              tagExists = true;
              break;
            }
          }
          if (tagExists == false)
          {
            missingTagList.add(inputTag);
          }
          tagExists = false;
        }
      }
    }
    return missingTagList;
  }
  
  public List<String> readTagList()
  {
    List<String> infoTagListInput = new ArrayList<String>();
    String filePath = Directory.getTempDir() + File.separator + _INFO_ENUM_FULL_LIST_FILE;
    File inputfile = new File(filePath);
    String tagName;
    if (inputfile.exists())
    {
      try
      {
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        tagName = br.readLine();
        while (tagName != null)
        {
          infoTagListInput.add(tagName);
          tagName = br.readLine();
        }
        br.close();
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
    return infoTagListInput;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void insertTagToFullList(String tagName)
  {
    _infoTagFullList = getDatabaseFullList();
    _infoTagFullList.add(tagName);
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void deleteTagFromFullList(String tagName)
  {
    _infoTagFullList = getDatabaseFullList();
    for (String tag : _infoTagFullList)
    {
      if (tag.equals(tagName))
      {
        _infoTagFullList.remove(tag);
        break;
      }
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void refreshDatabaseVersion(double newVersion)
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    try
    {
      _infoTagFullList = currentDatabase.getInfoTagFullListDatabase(_libraryPath);

      for (String tagName : _infoTagFullList)
      {
        currentDatabase.updateTagVersionDatabase(_libraryPath, tagName, newVersion);
      }
      currentDatabase.updateTagVersionDatabase(_libraryPath, "version", newVersion);
      InfoHandlerConfig.getInstance().saveDatabaseVersionToConfig("" + newVersion);
      try
      {
        renameDatabase(getInfoHandlerDatabaseName() + newVersion);
      }
      catch (IOException e)
      {
        System.out.println(e.getMessage());
      }
    }
    catch (DatastoreException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public List<String> getDatabaseFullList()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    if (_infoTagFullList.isEmpty())
    {
      try
      {
        _infoTagFullList = currentDatabase.getInfoTagFullListDatabase(_libraryPath);
      }
      catch (DatastoreException err)
      {
        System.out.println(err.getMessage());
      }
    }
    return _infoTagFullList;
  }
  
  public List<String> getMissingMediaList()
  {
    List<String> missingMediaList = new ArrayList<String>();

    missingMediaList = validatePictureFile(missingMediaList);
    //validate other media file as well
    return missingMediaList;
  }
  
  /**
   * @author swee-yee.wong
   */
  private List<String> validatePictureFile(List<String> missingMediaList)
  {
    List<String> fullPictureList = getFullPictureList();
    for (String pictureFile : fullPictureList)
    {
      String filePath = InfoHandlerConfig.getInstance().getImagePath() + pictureFile;
      File outputfile = new File(filePath);
      if(outputfile.exists() == false)
      {
        missingMediaList.add(filePath);
      }
    }
    return missingMediaList;
  }
  
  public void extractAndUpdateTagMediaList(String tagName, String htmlContent)
  {
    String mediaList = "";
    mediaList = extractPictureList(mediaList, htmlContent);
    //include other media type as well
    updateTagMediaList(tagName, mediaList);
  }
  
  /**
   * @author swee-yee.wong
   */
  private String extractPictureList(String mediaList, String subs)
  {
    String sourcePath = InfoHandlerConfig.getInstance().getImagePath();
    int index = subs.indexOf(sourcePath);
    while(index > 0)
    {
      if (mediaList.isEmpty() == false)
      {
        mediaList = mediaList + ",";
      }
      mediaList = mediaList + "p/";
      subs = subs.substring(index + sourcePath.length());
      index = subs.indexOf('"');
      if (index < -1)
      {
        return mediaList;
      }
      mediaList = mediaList + subs.substring(0, index);
      index = subs.indexOf(InfoHandlerConfig.getInstance().getImagePath());
    }
    return mediaList;
  }

  /**
   * @author Swee Yee Wong
   */
  public double getNewVersion()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    if (_isNewVersionGet == false)
    {
      try
      {
        double databaseVersion = currentDatabase.getTagVersionDatabase(_libraryPath, "version");
        if (databaseVersion == 0)
        {
          try
          {
            currentDatabase.insertInfoHandlerDatabase(_libraryPath, 0, "version", null, null, 1.0);
            _newVersion = 1.0;
          }
          catch (DatastoreException err)
          {
            System.out.println(err.getMessage());
          }
        }
        else
        {
          _newVersion = round(databaseVersion + 0.001, 3);
        }

      }
      catch (DatastoreException err)
      {
        System.out.println(err.getMessage());
      }

      if (_newVersion > 0)
      {
        try
        {
          currentDatabase.updateTagVersionDatabase(_libraryPath, "version", _newVersion);
        }
        catch (DatastoreException err)
        {
          System.out.println(err.getMessage());
        }
        InfoHandlerConfig.getInstance().saveDatabaseVersionToConfig("" + _newVersion);
        _isNewVersionGet = true;
      }
    }
    return _newVersion;
  }
  
  public void renameDatabase()
  {
    if (_isNewVersionGet)
    {
      try
      {
        renameDatabase(getInfoHandlerDatabaseName() + _newVersion);
      }
      catch (IOException e)
      {
        System.out.println(e.getMessage());
      }
    }
  }
  
  private void renameDatabase(String newName) throws IOException
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    if (currentDatabase.isDatabaseConnected())
    {
      closeConnection();
      shutdownDatabaseDriver();
    }
    String newPath = setNewDatabaseFilePath(newName);
    File oldfile = new File(_libraryPath);

    // File (or directory) with new name
    File newfile = new File(newPath);
    if(newfile.exists()) 
    {
      if (currentDatabase.isDatabaseConnected() == false)
      {
        openConnection();
        startupDatabaseDriver();
      }
      throw new java.io.IOException("file exists");
    }

    // Rename file (or directory)
    boolean success = oldfile.renameTo(newfile);
    if (success == false)
    {
      System.out.println("Failed to rename database file name.");
      newPath = _libraryPath;
    }
    
    setLibraryPath(newPath);
    
    InfoHandlerConfig.getInstance().saveDatabaseDirToConfig(InfoHandlerFileUtil.changeFullPathToRelativePath(newPath));
    InfoHandlerConfig.getInstance().refreshConfig();
    if (currentDatabase.isDatabaseConnected() == false)
    {
      openConnection();
      startupDatabaseDriver();
    }
  }
  
  public ArrayList<ImportDatabaseDiffPackage> getImportDatabaseDiffPackage()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    List<String> importDatabaseFullList = new ArrayList<String>();
    ArrayList<ImportDatabaseDiffPackage> importList = new ArrayList<ImportDatabaseDiffPackage>();
    if (currentDatabase.isDatabaseConnected())
    {
      try
      {
        importDatabaseFullList = currentDatabase.getInfoTagFullListDatabase(_libraryPath);
      }
      catch (DatastoreException err)
      {
        System.out.println(err.getMessage());
      }
      for (String tagName : importDatabaseFullList)
      {
        importList.add(new ImportDatabaseDiffPackage(tagName, getTagVersion(tagName), getTagDateTime(tagName)));
      }

      importList = sortImportPackageListByVersion(importList);
    }
    return importList;
  }
  
  private ArrayList<ImportDatabaseDiffPackage> sortImportPackageListByVersion(ArrayList<ImportDatabaseDiffPackage> unsortImportList)
  {
    Double version;
    ImportDatabaseDiffPackage importPackge;
    int i, j;

    for (i = 1; i < unsortImportList.size(); i++)
    {
      version = unsortImportList.get(i).getImportVersion();
      importPackge = unsortImportList.get(i);
      for (j = i; j > 0 && version > unsortImportList.get(j - 1).getImportVersion(); j--)
      {
        //unsortImportList.set(j, unsortImportList.get(j - 1));
      }
      if (i != j)
      {
        unsortImportList.add(j, importPackge);
        unsortImportList.remove(i+1);
      }
    }
    return unsortImportList;
  }
  
  public void setLibraryPath(String path)
  {
    Assert.expect(path != null);
    _libraryPath = path;
  }
  
  private String setNewDatabaseFilePath(String newName)
  {
    int index = _libraryPath.lastIndexOf(File.separator);
    if(index > -1)
    {
      return _libraryPath.substring(0, index + 1) + newName;
    }
    else
    {
      return _libraryPath;
    }
  }
  
  private void unpopulateData()
  {
    _infoTagFullList.removeAll(_infoTagFullList);
  }
  
  private void populateData()
  {
    _infoTagFullList = getDatabaseFullList();
  }

  /**
   * @author Swee Yee Wong
   */
  public static double round(double value, int places)
  {
    if (places < 0)
    {
      throw new IllegalArgumentException();
    }

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
  }
  
  public boolean getDatabaseConnection()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if(this == _instance)
      currentDatabase = _databaseInstance;
    else if(this == _instance2)
      currentDatabase = _databaseInstance2;
    return currentDatabase.isDatabaseConnected();
  }
  
  public String getInfoHandlerDatabaseName()
  {
    int startIndex = _libraryPath.lastIndexOf(File.separator);
    int endIndex = _libraryPath.lastIndexOf("_");
    if(startIndex > -1 && endIndex > -1)
    {
      return _libraryPath.substring(startIndex + 1, endIndex + 1);
    }
    else
    {
      return "infoHandler_" + InfoHandlerConfig.getLocaleLanguage() + "_";
    }
  }
  
  public String processImageFormatInHtml(String subs)
  {
    String output = "";
    String pictureFormat = "file:" + File.separator;
    String sourcePath = InfoHandlerConfig.getInstance().getImagePath();
    int index = subs.indexOf(sourcePath);
    
    while(index > 0)
    {
      String openQuatationMark = subs.substring(index-1, index);
      if(openQuatationMark.indexOf('"') > -1)
      {
        output = output + subs.substring(0, index) + pictureFormat + subs.substring(index, index + sourcePath.length());
      }
      else
      {
        output = output + subs.substring(0, index + sourcePath.length());
      }
      subs = subs.substring(index + sourcePath.length());
      index = subs.indexOf(sourcePath);
    }
    output = output + subs;
    return output;
  }
  
  private String resolveValueInHtmlContent(String htmlContent, String value[])
  {
    if (htmlContent != null && value != null)
    {
      String output = "";
      int startIndex = htmlContent.indexOf(_OPEN_VALUE_MATCHER);
      int endIndex = htmlContent.indexOf(_END_VALUE_MATCHER);
      while (endIndex > -1 && endIndex < startIndex)
      {
        endIndex = htmlContent.indexOf(_END_VALUE_MATCHER, endIndex);
      }

      while (startIndex > -1 && endIndex > -1)
      {
        String matcherContent = htmlContent.substring(startIndex + 1, endIndex);
        for (int i = 0; i < value.length; i++)
        {
          if (matcherContent.equals(String.valueOf(i)))
          {
            htmlContent = StringUtil.replace(htmlContent, value[i], startIndex, endIndex);
            endIndex = endIndex + value[i].length() - 2 - matcherContent.length();
          }
        }
        output = output + htmlContent.substring(0, endIndex + 1);
        htmlContent = htmlContent.substring(endIndex + 1);
        startIndex = endIndex = -1;
        startIndex = htmlContent.indexOf(_OPEN_VALUE_MATCHER);
        endIndex = htmlContent.indexOf(_END_VALUE_MATCHER);
        while (endIndex > -1 && endIndex < startIndex)
        {
          endIndex = htmlContent.indexOf(_END_VALUE_MATCHER, endIndex);
        }
      }
      output = output + htmlContent;
      return output;
    }

    return htmlContent;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void shutdownDatabaseDriver()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if (this == _instance)
    {
      currentDatabase = _databaseInstance;
    }
    else if (this == _instance2)
    {
      currentDatabase = _databaseInstance2;
    }

    if(isDriverStartUp == true)
    {
      try
      {
        currentDatabase.shutdownDriver();
        isDriverStartUp = false;
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to close database driver.");
      }
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void startupDatabaseDriver()
  {
    InfoHandlerDatabaseUtil currentDatabase = null;
    if (this == _instance)
    {
      currentDatabase = _databaseInstance;
    }
    else if (this == _instance2)
    {
      currentDatabase = _databaseInstance2;
    }

    if (isDriverStartUp == false)
    {
      try
      {
        currentDatabase.startupDriver();
        isDriverStartUp = true;
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to start database driver.");
      }
    }
  }
}
