package com.axi.v810.util;

import java.io.*;
import java.util.*;
/**
 *
 * @author chin-seong.kee
 */
public class DefectPackagerUtil {
    
  public static final String _DEFECT_PACKAGER_SERVICE_NAME = "Defect Packager";
  public static final String _DEFECT_PACKAGER_STARTED      = "4";
  
  public static final String _START_DEFECT_PACKAGER_COMMAND = "-s";
  public static final String _STOP_DEFECT_PACKAGER_COMMAND  = "-k";
  
  
  public DefectPackagerUtil()
  {
    //do nothing
  }
   /**
   * @author Kee, Chin Seong
   * Check the defect packager is installed or not 
   */
  public static String queryService() 
  {
    String output = new String();
    try 
    {
      Process p = Runtime.getRuntime().exec("sc query \""+_DEFECT_PACKAGER_SERVICE_NAME+"\"");
      BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
      String line;
      while((line = reader.readLine()) != null) 
      {
         if(line.trim().indexOf("STATE") == 0)
         {
             line = (line.substring(line.indexOf(":")+1)).trim();
             StringTokenizer st = new  StringTokenizer(line, " ");
             String stateNumber =  st.nextToken();
             String stateDescrip = st.nextToken();
             output = stateNumber;
         }
      }
      reader.close();
   } 
   catch (IOException e) 
   {
      e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
   }
   return output;
  }
}
