package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * @author George A. David
 */
public class Test_FileReaderUtilAxi extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] arguments)
  {
    UnitTest.execute(new Test_FileReaderUtilAxi());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      FileReaderUtilAxi fileReaderUtil = new FileReaderUtilAxi();
      try
      {
        fileReaderUtil.open(null);
        Expect.expect(false);
      }
      catch (AssertException exc)
      {
        //do nothing
      }

      try
      {
        fileReaderUtil.hasNextLine();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        fileReaderUtil.readNextLine();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      String badFilePath = getTestDataDir() + File.separator + "badFilePath";
      try
      {
        fileReaderUtil.open(badFilePath);
        Expect.expect(false);
      }
      catch (FileNotFoundDatastoreException exc)
      {
        //do nothing
      }

      String existingFilePath = getTestDataDir() + File.separator + "Test_FileReaderUtilAxi" + File.separator + "existingFile";

      fileReaderUtil.open(existingFilePath);
      Expect.expect(fileReaderUtil.hasNextLine());
      Expect.expect(fileReaderUtil.readNextLine().equals("this is an existing file"));
      Expect.expect(fileReaderUtil.hasNextLine() == false);
      try
      {
        fileReaderUtil.readNextLine();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      fileReaderUtil.close();
    }
    catch(Exception exception)
    {
      exception.printStackTrace();
      Expect.expect(false);
    }
  }
}
