package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.gui.home.*;

/**
 * This comparator will sort a AvailableProjectData object by the desired field name
 * @author George Booth
 */
public class AvailableProjectDataComparator implements Comparator<AvailableProjectData>
{
  private boolean _ascending;
  private int _field;
  private static int _index = -1;
  public static final int PROJECT_NAME = ++_index;
  public static final int CUSTOMER_NAME = ++_index;
  public static final int VERSION = ++_index;
  public static final int LAST_MOD = ++_index;
  public static final int DATABASE_VERSION  = ++_index;
  public static final int SYSTEM_TYPE  = ++_index;

  private CaseInsensitiveStringComparator _caseInsensitiveStringComparator = null;

  /**
   * Sort on the desired field
   * @author George Booth
   */
  public AvailableProjectDataComparator(boolean ascending, int field)
  {
    _ascending = ascending;
    _field = field;
    _caseInsensitiveStringComparator = new CaseInsensitiveStringComparator();
  }

  /**
   * @return negative if lf less than rh, zero if equal, positive if greater than
   * @author George Booth
   */
  public int compare(AvailableProjectData lhResult, AvailableProjectData rhResult)
  {
    Assert.expect(lhResult != null);
    Assert.expect(rhResult != null);

    String lhString = null;
    String rhString = null;

    switch (_field) {
      case 0: // PROJECT_NAME
        lhString = lhResult.getProjectName();
        rhString = rhResult.getProjectName();
        // this comparator MUST match FileName.getProjectNames()
        // we don't want to use the AlphaNumericComparator as project names are not
        // the same as C1, C10, etc.
        if (_ascending)
          return _caseInsensitiveStringComparator.compare(lhString, rhString);
        else
          return _caseInsensitiveStringComparator.compare(rhString, lhString);
      case 1: // CUSTOMER_NAME
        lhString = lhResult.getCustomerName();
        rhString = rhResult.getCustomerName();
        if (_ascending)
          return lhString.compareTo(rhString);
        else
          return rhString.compareTo(lhString);
      case 2: // VERSION
        Double lhDouble = new Double(lhResult.getVersion());
        Double rhDouble = new Double(rhResult.getVersion());
        if (_ascending)
          return lhDouble.compareTo(rhDouble);
        else
          return rhDouble.compareTo(lhDouble);
      case 3: // LAST_MOD
        Long lhLong = lhResult.getModificationTimeInMillis();
        Long rhLong = rhResult.getModificationTimeInMillis();
        if (_ascending)
          return lhLong.compareTo(rhLong);
        else
          return rhLong.compareTo(lhLong);
      case 4: // DATABASE_VERSION
        Integer lhInteger = new Integer(lhResult.getDatabaseVersion());
        Integer rhInteger = new Integer(rhResult.getDatabaseVersion());
        if (_ascending)
          return lhInteger.compareTo(rhInteger);
        else
          return rhInteger.compareTo(lhInteger);
      case 5: // SYSTEM TYPE
        lhString = lhResult.getSystemType();
        rhString = rhResult.getSystemType();
        if (_ascending)
          return _caseInsensitiveStringComparator.compare(lhString, rhString);
        else
          return _caseInsensitiveStringComparator.compare(rhString, lhString);
      default:
        return 0;
    }
  }
}
