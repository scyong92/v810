package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * Given a list of pads it will determine the surrounding pads of any pad you pass in.
 * @author George A. David
 */
public class SurroundingLandPatternPadUtil
{
  private Map<Integer, Set<LandPatternPad>> _xCoordToPadSetMap = new TreeMap<Integer, Set<LandPatternPad>>();
  private Map<Integer, Set<LandPatternPad>> _yCoordToPadSetMap = new TreeMap<Integer, Set<LandPatternPad>>();
  private List<Integer> _xCoords;
  private List<Integer> _yCoords;

  /**
   * @author George A. David
   */
  public void setLandPatternPads(Collection<LandPatternPad> pads)
  {
    Assert.expect(pads != null);

    _xCoordToPadSetMap.clear();
    _yCoordToPadSetMap.clear();

    for(LandPatternPad pad : pads)
      addPad(pad);

    _xCoords = new ArrayList<Integer>(_xCoordToPadSetMap.keySet());
    _yCoords = new ArrayList<Integer>(_yCoordToPadSetMap.keySet());
  }

  /**
   * @author George A. David
   */
  public void addPad(LandPatternPad pad)
  {
    ComponentCoordinate coord = pad.getCenterCoordinateInNanoMeters();
    addPad(pad, coord.getX(), _xCoordToPadSetMap);
    addPad(pad, coord.getY(), _yCoordToPadSetMap);

    _xCoords = new ArrayList<Integer>(_xCoordToPadSetMap.keySet());
    _yCoords = new ArrayList<Integer>(_yCoordToPadSetMap.keySet());
  }

  /**
   * @author George A. David
   */
  private void addPad(LandPatternPad pad, int coordValue,  Map<Integer, Set<LandPatternPad>> coordToPadSetMap)
  {
    Assert.expect(pad != null);
    Assert.expect(coordToPadSetMap != null);

    if(coordToPadSetMap.containsKey(coordValue))
    {
      coordToPadSetMap.get(coordValue).add(pad);
    }
    else
    {
      Set<LandPatternPad> padSet = new HashSet<LandPatternPad>();
      padSet.add(pad);
      coordToPadSetMap.put(coordValue, padSet);
    }
  }

  /**
   * @author George A. David
   */
  private Set<LandPatternPad> getSurroundingPads(int coordValue, List<Integer> coords, Map<Integer, Set<LandPatternPad>> coordToPadSetMap)
  {
    Assert.expect(coordToPadSetMap != null);

    Set<LandPatternPad> pads = new HashSet<LandPatternPad>();

    Pair<Integer, Integer> pair = ListUtil.findIndiciesOfSurroundingValues(coords, coordValue);
    int previousIndex = pair.getFirst();
    int nextIndex = pair.getSecond();

    for(int i = previousIndex; i <= nextIndex; ++i)
      pads.addAll(coordToPadSetMap.get(coords.get(i)));

    return pads;
  }

  /**
   * @author George A. David
   */
  public Collection<LandPatternPad> getSurroundingPads(LandPatternPad pad)
  {
    Assert.expect(pad != null);

    ComponentCoordinate coord = pad.getCenterCoordinateInNanoMeters();
    Set<LandPatternPad> pads = getSurroundingPads(coord.getX(), _xCoords, _xCoordToPadSetMap);
    pads.retainAll(getSurroundingPads(coord.getY(), _yCoords, _yCoordToPadSetMap));

    return pads;
  }

  /**
   * @author George A. David
   */
  public Collection<LandPatternPad> getPadsAtXcoordinate(int xCoordinate)
  {
    Set<LandPatternPad> pads = new HashSet<LandPatternPad>();
    pads.addAll(_xCoordToPadSetMap.get(xCoordinate));

    return pads;
  }

  /**
   * @author George A. David
   */
  public Collection<LandPatternPad> getPadsAtYcoordinate(int yCoordinate)
  {
    Set<LandPatternPad> pads = new HashSet<LandPatternPad>();
    pads.addAll(_yCoordToPadSetMap.get(yCoordinate));

    return pads;
  }

  /**
   * @author George A. David
   */
  public void printDebugInfo()
  {
    System.out.println("x map");
    for(Integer integer : _xCoordToPadSetMap.keySet())
    {
      System.out.println("xCoord: " + integer);
      System.out.print("Pads: ");
      for(LandPatternPad pad : _xCoordToPadSetMap.get(integer))
      {
        System.out.print(pad.getName() + " ");
      }
      System.out.println();
    }

    System.out.println("y map");
    for(Integer integer : _yCoordToPadSetMap.keySet())
    {
      System.out.println("yCoord: " + integer);
      System.out.print("Pads: ");
      for(LandPatternPad pad : _yCoordToPadSetMap.get(integer))
      {
        System.out.print(pad.getName() + " ");
      }
      System.out.println();
    }
  }
}
