package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.packageLibrary.LibrarySubtypeScheme;
import com.axi.util.Assert;

public class LibraryJointTypeComparator implements Comparator<String>
{
  private Map<String, List<LibrarySubtypeScheme>> _jointTypeNameToLibrarySubtypeSchemesMap;

  public LibraryJointTypeComparator(Map<String, List<LibrarySubtypeScheme>> jointTypeNameToLibrarySubtypeSchemesMap)
  {
    _jointTypeNameToLibrarySubtypeSchemesMap = jointTypeNameToLibrarySubtypeSchemesMap;
  }

  /**
   * Compares its two arguments for order.
   *
   * @param lhs String the first object to be compared
   * @param rhs String the second object to be compared
   * @return int a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
   *   than the second.
   * @todo Implement this java.util.Comparator method
   * @author Tan Hock Zoon
   */
  public int compare(String lhs, String rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    double lhsDouble = _jointTypeNameToLibrarySubtypeSchemesMap.get(lhs).get(0).getMatchPercentage();
    double rhsDouble = _jointTypeNameToLibrarySubtypeSchemesMap.get(rhs).get(0).getMatchPercentage();

    if (lhsDouble < rhsDouble)
    {
      return 1;
    }
    else if (lhsDouble > rhsDouble)
    {
      return -1;
    }
    else
    {
      return 0;
    }

  }
}
