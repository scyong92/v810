package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
* This Run5dxProgram class is used to run 16-bit 5DX apps that need the 2 TSR's FPDRV and DOSNTTSR loaded first.
* This is a temporary kludge to enable calls to old 16-bit program RESTEST.exe - only for 7.0 release.
*
* THIS CLASS SHOULD NO LONGER BE USED AFTER THE 7.0 RELEASE (except Manufacturing?) !!!!     MLS-FUTURE
*
* @author  Matt Snook
*/
public class Run5dxProgram
{
  private Run5dxProgram()   // don't want it instantiated -- no need to.
  {
  }

  /**
  * This execs 16-bit 5DX programs (requiring the 2 TSR's), and does not return until finished.
  *
  * Should handle a command string input with 10 items total in the string (e.g. program + 9 options).
  * (This is dependent on finding the 2 .bat files to work correctly)
  *
  * This runs a .bat file that runs a second one to create a 5DX 16-bit user
  * interface window with the 2 TSRs loaded, and runs the app command string passed to it.
  * Then it waits on standard input until finished, so at that point we know the 16-bit app
  * has finished.
  *
  * It is not able to get return values from the DOS app so we cannot tell if it is successful or not.
  * Therefore if you need data back you must have the DOS app write its output to a file and parse it later.
  * For this reason, if you will be parsing a file later, then you should delete the file before you call this
  * exec() so that you don't end up having old bogus data being parsed.
  *
  * @param command  The application command line to invoke, may have 10 items total in string, separated by spaces.
  * @author Matt Snook
  */
  public static void exec(String command) throws IOException
  {
    Assert.expect(command != null);
    Assert.expect(command.equals("") == false);

    // The run16bitApp.bat file shifts %0 item off then calls "start run16bitApp2.bat %0 %1 %2 %3 %4 %5 %6 %7 %8 %9".
    // The run16bitApp2.bat then has several lines in it to load FPDRV & DOSNTTSR, then "dcxobj -uiready",
    //  then finally it shifts %0 item off then calls: "%0 %1 %2 %3 %4 %5 %6 %7 %8 %9"
    command = "run16bitApp.bat " + command;  // depend on PATH setting to find it in the 5DX release dir
    BufferedReader din = null;
    try
    {
      Process proc = Runtime.getRuntime().exec(command);
      //proc.waitFor();  // this doesn't work -- It never spins here.

      din = new BufferedReader(new InputStreamReader(proc.getInputStream()));

      // reading all lines from din will not finish until file is completely written by asynchronous app!
      String line = din.readLine();
      while (line != null)
      {
        line = din.readLine();
      }
      // TODO: should I check for errors, like proc.exitValue() or just exceptions?
    }
    finally   // always close the file but ignore any exceptions
    {
      try
      {
        if (din != null)   // if file never opened successfully but we try to close it
        {
           din.close();
        }
      }
      catch (IOException ioe)
      {
        // ignore exception
      }
    }
  }
}
