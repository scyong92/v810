package com.axi.v810.util;

import java.util.*;

import javax.swing.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.images.*;

/**
* This class contains data needed for the Assert class.
* @author Bill Darbie
*/
public class AssertUtil
{
  private static final String _AXI_SUPPORT_EMAIL_ADDRESS = "supportxxx@xxx.com";
  private static final String _EMAIL_SUBJECT = "AXI System v810 Internal Error";
  private static final String _DESCRIPTION = "" +
    "An internal software error occurred.  " +
    "To send this information to your authorized service representative." +
//    "click the send button.  " +
//    "You can check http://www.vitrox.com/key/boardtest to see if there is a fix for this problem.  " +
//    "If your system is not able to send email you can find a log file that contains this " +
//    "information in the log directory where the 5dx software is installed.  This would " +
//    "typically be in <path> where XX is the release number, for example r10." +
    "  After exiting this dialog, do an automatic startup to continue.";

  private static String _customerCompanyName = "unknown";
  private static String _customerName = "unknown";
  private static String _customerPhoneNumber = "unknown";
  private static String _smtpServer = "unknown";
  private static String _customerEmailAddress = "unknown";
  private static String _sptEmailAddress = "unknown";
  private static String _machineDescription = "unknown";

  private static JFrame _frame = new JFrame();

  /**
   * @author Bill Darbie
   */
  static
  {
    Assert.setLogFile("internalErrors", Version.getVersion());
  }

  /**
   * Update all private variables with information from the config files.  This
   * will be called by Config.load() right after the config files have been loaded.
   *
   * @author Bill Darbie
   */
  public static void updateConfigData()
  {
    Config config = Config.getInstance();
    if (config.isLoaded())
    {
      try
      {
        _customerCompanyName = config.getStringValue(SoftwareConfigEnum.CUSTOMER_COMPANY_NAME);
      }
      catch(MissingResourceException re)
      {
        re.printStackTrace();
      }
      try
      {
        _customerName = config.getStringValue(SoftwareConfigEnum.CUSTOMER_NAME);
      }
      catch(MissingResourceException re)
      {
        re.printStackTrace();
      }
      try
      {
        _customerPhoneNumber = config.getStringValue(SoftwareConfigEnum.CUSTOMER_PHONE_NUMBER);
      }
      catch(MissingResourceException re)
      {
        re.printStackTrace();
      }
      try
      {
        _smtpServer = config.getStringValue(SoftwareConfigEnum.SMTP_SERVER);
      }
      catch(MissingResourceException re)
      {
        re.printStackTrace();
      }
      try
      {
        _customerEmailAddress = config.getStringValue(SoftwareConfigEnum.CUSTOMER_EMAIL_ADDRESS);
      }
      catch(MissingResourceException re)
      {
        re.printStackTrace();
      }
      try
      {
        _sptEmailAddress = config.getStringValue(SoftwareConfigEnum.SPT_EMAIL_ADDRESS);
      }
      catch(MissingResourceException re)
      {
        re.printStackTrace();
      }
      try
      {
        _machineDescription = config.getStringValue(HardwareConfigEnum.XRAYTESTER_SERIAL_NUMBER);
      }
      catch(MissingResourceException re)
      {
        re.printStackTrace();
    }

      if ((_sptEmailAddress == null) || (_sptEmailAddress.equals("") || _sptEmailAddress.equalsIgnoreCase("unknown")))
        _sptEmailAddress = _AXI_SUPPORT_EMAIL_ADDRESS;
    }

    setUpAssert(_frame);
  }

  /**
  * Call this method to set up asserts for any xRayTest code.
  * @author Bill Darbie
  */
  public static void setUpAssert(JFrame frame)
  {
    if (frame == null)
    {
      frame = new JFrame();
      frame.setIconImage( Image5DX.getImage( Image5DX.FRAME_X5000 ) );
    }
    Assert.sendEmail(frame,
                     _DESCRIPTION,
                     _machineDescription,
                     _customerCompanyName,
                     _customerName,
                     _customerEmailAddress,
                     _customerPhoneNumber,
                     _smtpServer,
                     _sptEmailAddress,
                     _EMAIL_SUBJECT);
  }
}
