package com.axi.v810.util;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;

import com.axi.util.*;

/**
 * This class reperesent a rectangle in panel space.  The origin is the lower left corner of the panel.
 * The x axis grows right and the y axis grows up.  The units are nanometers.
 *
 * @author Matt Wharton
 */
public class PanelRectangle extends IntRectangle implements Serializable
{
  /**
   * @author Matt Wharton
   */
  public PanelRectangle(int bottomLeftX, int bottomLeftY, int width, int height)
  {
    super(bottomLeftX, bottomLeftY, width, height);
  }

  /**
   * @author Matt Wharton
   */
  public PanelRectangle(PanelRectangle rhs)
  {
    super(rhs);
  }


  /**
   * @author Matt Wharton
   */
  public PanelRectangle(Rectangle2D rectangle2D)
  {
    super(rectangle2D);
  }

  /**
   * @author George A. David
   */
  public PanelRectangle(Shape shape)
  {
    super(shape);
  }

  /**
   * @author Bill Darbie
   */
  public PanelRectangle(IntRectangle intRectangle)
  {
    super(intRectangle);
  }

  /**
   * @author George A. David
   */
  public PanelCoordinate getCenterCoordinate()
  {
    return new PanelCoordinate(getCenterX(), getCenterY());
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializeRectangle(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializeRectangle(os);
  }

  /**
   * @author Roy Williams
   */
  public PanelRectangle createIntersectingPanelRectangle(IntRectangle rect)
  {
    Assert.expect(_rectangle != null);
    Assert.expect(rect != null);

    return new PanelRectangle(super.createIntersection(rect));
  }
}
