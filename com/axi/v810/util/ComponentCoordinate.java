package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * Represents a coordinate in Component space.
 *
 * The coordinate is relative to the reference point of the component.
 * The reference point is not necessarily the center of the component, but is the
 * point from which the comonents pad/pin coordinates are referenced.
 * @author Bill Darbie
 */
public class ComponentCoordinate extends IntCoordinate implements Serializable
{
  /**
   * @author Matt Wharton
   */
  public ComponentCoordinate()
  {
    super();
  }


  /**
   * @author Matt Wharton
   */
  public ComponentCoordinate(ComponentCoordinate rhs)
  {
    super( rhs );
  }

  /**
   * @author George A. David
   */
  public ComponentCoordinate(IntCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * @param x the x value of this coordinate.
   * @param y the y value of this coordinate.
   * @author Matt Wharton
   */
  public ComponentCoordinate(int x, int y)
  {
    super(x, y);
  }

  /**
   * @author George A. David
   */
  public ComponentCoordinate minus(ComponentCoordinate componentCoordinate)
  {
    return new ComponentCoordinate(super.minus(componentCoordinate));
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }
}
