package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.v810.datastore.*;
import com.axi.util.*;
import java.util.logging.*;

/**
 * All 5dx Exceptions extend this class.  It can log all exceptions
 * to a log file.  It also extends the LocalizedException class so that
 * all exception messages can be changed to other languages.
 *
 * @author Bill Darbie
 */
public class XrayTesterException extends LocalizedException
{
  private static int _exceptionCount = 0;
  private static boolean _logExceptions = true;

  private static Logger _logger = Logger.getLogger(XrayTesterException.class.getName());
  private static FileHandler _fileHandler;
  private static int _maxFileSize = 200 * 1024 * 1024;  // 200M in bytes
  private static int _numberOfLoggerFiles = 2;  // the number of logger files to cycle through when size limit is hit
  //private static boolean _logExceptions = false;

  /**
   * @author Andy Mechtenberg
   */
  private void initLogger() throws IOException
  {
    _fileHandler = new FileHandler(FileName.getExceptionLoggerFullName(), _maxFileSize, _numberOfLoggerFiles, true);
    _fileHandler.setFormatter(new SimpleFormatter());
    _logger.addHandler(_fileHandler);
    _logger.setLevel(Level.FINE);
  }

  /**
   * @author Andy Mechtenberg
   */
  private void closeLogger()
  {
    if (_fileHandler != null)
      _fileHandler.close();
  }

  /**
   * @author Bill Darbie
   */
  public XrayTesterException(LocalizedString localizedString)
  {
    super(localizedString);

    ++_exceptionCount;
    if (_logExceptions)
    {
      try
      {
        initLogger();
        String version = Version.getVersion();

        String headerString = "#######################################################################" + StringUtil.getLineSeparator();
        headerString += "AXI System v810 Exception" + StringUtil.getLineSeparator();
        headerString += "Exception #: " + _exceptionCount + StringUtil.getLineSeparator();
        headerString += "software version: " + version + StringUtil.getLineSeparator();

        _logger.log(Level.FINE, headerString);
        _logger.log(Level.FINE, "", this);

      }
      catch(IOException ioe)
      {
        ioe.printStackTrace();
      }
      finally
      {
        closeLogger();
      }
    }
  }

  /**
   * @author Bill Darbie
   */
  public Throwable initCause(Throwable cause)
  {
    if (_logExceptions)
    {
      try
      {
        initLogger();
        String headerString = "The exception above was caused by:" + StringUtil.getLineSeparator();
        _logger.log(Level.FINE, headerString);
        _logger.log(Level.FINE, "", cause);
      }
      catch (IOException ioe)
      {
        ioe.printStackTrace();
      }
      finally
      {
        closeLogger();
      }
    }

    return super.initCause(cause);
  }

  /**
   * @author Bill Darbie
   */
  public static void enableExceptionLogging()
  {
    _logExceptions = true;
  }

  /**
   * @author Bill Darbie
   */
  public static void disableExceptionLogging()
  {
    _logExceptions = false;
  }

  /**
   * @return a String that has been looked up in our 5dx localization file.
   * @author Bill Darbie
   */
  public String getLocalizedMessage()
  {
    LocalizedString ls = getLocalizedString();
    String key = ls.getMessageKey();

    String message = StringLocalizer.keyToString(getLocalizedString());
    if (UnitTest.unitTesting() == false)
      message += "\n\n" + "(" + key + ")";
    return message;
  }

  /**
   * @return a String that has been looked up in our 5dx localization file.
   * @author Bill Darbie
   */
  public String getMessage()
  {
    return getLocalizedMessage();
  }

  /**
   * @author Greg Esparza
   */
  public String getLocalizedMessageWithoutHeaderAndFooter()
  {
    return getLocalizedMessage();
  }
}
