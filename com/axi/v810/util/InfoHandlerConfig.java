/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 *
 * @author swee-yee.wong
 */
public class InfoHandlerConfig
{
  private ConfigFileTextReader _configFileTextReader = new ConfigFileTextReader();
  private Pattern _keyValuePattern = Pattern.compile("^\\s*(([^#=\\s]+)(\\s*=\\s*)((\\\\#|[^#=])*))\\s*([#?].*)?$");
  private Map<InfoHandlerConfigEnum, String> _configEnumToValueMap = new HashMap<InfoHandlerConfigEnum, String>();
  private static String _releaseDir = System.getenv("AXI_XRAY_HOME");
  private static final String _INFO_HANDLER_CONFIG_FILE = "infohandler.config";
  private static final String _INFO_DIR = "info";
  private static final String _INFO_HANDLER_DIR = "infoHandler";
  private static InfoHandlerConfig _instance;
  private static String _imagePath;
  private InfoHandlerConfigEnum _databaseDir;
  private String _language = null;
  
  public static synchronized InfoHandlerConfig getInstance()
  {
    if (_instance == null)
    {
      _instance = new InfoHandlerConfig();
    }

    return _instance;
  }
  
  public void loadConfig()
  {
    try
    {
      loadFile(getInfoHandlerConfigDir());
      checkForMissingKeys();
      _imagePath = getInfoHandlerMediaDir();
      _imagePath = Directory.shortenPath(_imagePath) + File.separator + "images" + File.separator;
      InfoHandlerDatabaseManager.getInstance().setLibraryPath(getInfoHandlerDatabaseDir());
    }
    catch (DatastoreException e)
    {
      System.out.println(e);
      Assert.logException(e);
    }
  }
  
  public String getImagePath()
  {
    return _imagePath;
  }
  
  public static String getInfoHandlerConfigDir()
  {
    return getInfoHandlerDir() + File.separator + _INFO_HANDLER_CONFIG_FILE;
  }
  
  public String getDatabaseVersionFromConfig()
  {
    return (String)getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_VERSION);
  }
  
  public String getDatabaseLastModifiedDateFromConfig()
  {
    return (String) getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_LAST_MODIFIED_DATE);
  }

  public String getInfoHandlerDatabaseDir()
  {
    //set default language pack to english
    String libraryPath;
    if (_language == null)
    {
      setDatabaseLanguage(Localization.getLocale().toString());
    }
    String locale = _language;
    //    private enum LOCALE_ENUM
    //    {
    //      en, zh_CN, zh_cn;
    //    }
    //    LOCALE_ENUM localizationMsg = LOCALE_ENUM.valueOf(locale);
    //    ***********************************************************
    //    this method cannot be used, enum.valueOf() will be obfuscated during full build.
    //    ***********************************************************
    libraryPath = (String) getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_DIR_EN_US);
    if (locale.equals("en"))
    {
      libraryPath = (String) getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_DIR_EN_US);
      _databaseDir = InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_DIR_EN_US;
    }
    else if (locale.equals("zh_CN"))
    {
      libraryPath = (String) getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_DIR_ZH_CN);
      _databaseDir = InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_DIR_ZH_CN;
    }
    else if (locale.equals("zh_cn"))
    {
      libraryPath = (String) getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_DIR_ZH_CN);
      _databaseDir = InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_DIR_ZH_CN;
    }

    return Directory.shortenPath(libraryPath);
  }
  
  public String getInfoHandlerMediaDir()
  {
    return (String)getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_MEDIA_DIR);
  }
  
  public String getInfoHandlerTempDir()
  {
    return (String)getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_TEMP_DIR) + File.separator;
  }
  
  public String getInfoHandlerDefaultHtml()
  {
    return (String)getValueFromMap(InfoHandlerConfigEnum.INFO_HANDLER_DEFAULT_HTML);
  }
  
  public boolean isEditButtonVisible(boolean fullDisplayMode)
  {
    if (fullDisplayMode == true)
    {
      return (boolean) getValueFromMap(InfoHandlerConfigEnum.FULL_DIALOG_EDIT_BUTTON_VISIBLE);
    }
    else
    {
      return (boolean) getValueFromMap(InfoHandlerConfigEnum.PROMPT_DIALOG_EDIT_BUTTON_VISIBLE);
    }
  }
  
  public boolean isDialogEditable(boolean fullDisplayMode)
  {
    if (fullDisplayMode == true)
    {
      return (boolean) getValueFromMap(InfoHandlerConfigEnum.FULL_DIALOG_EDITABLE);
    }
    else
    {
      return (boolean) getValueFromMap(InfoHandlerConfigEnum.PROMPT_DIALOG_EDITABLE);
    }
  }
  
  public boolean isTreeVisible(boolean fullDisplayMode)
  {
    if (fullDisplayMode == true)
    {
      return (boolean) getValueFromMap(InfoHandlerConfigEnum.FULL_DIALOG_TREE_VISIBLE);
    }
    else
    {
      return (boolean) getValueFromMap(InfoHandlerConfigEnum.PROMPT_DIALOG_TREE_VISIBLE);
    }
  }
  
  public void saveDatabaseVersionToConfig(String version)
  {
    try
    {
      saveValueToDisk(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_VERSION, version);
    }
    catch(DatastoreException e)
    {
      System.out.println(e);
      System.out.println("Failed to save database version into config file.");
    }
  }
  
  public void saveDatabaseDateToConfig(String date)
  {
    try
    {
      saveValueToDisk(InfoHandlerConfigEnum.INFO_HANDLER_DATABASE_LAST_MODIFIED_DATE, date);
    }
    catch(DatastoreException e)
    {
      System.out.println(e);
      System.out.println("Failed to save database date into config file.");
    }
  }
  
  public void saveDatabaseDirToConfig(String directory)
  {
    try
    {
      saveValueToDisk(_databaseDir, directory);
    }
    catch(DatastoreException e)
    {
      System.out.println(e);
      System.out.println("Failed to save database directory into config file.");
    }
  }
  
  public void saveDatabaseMediaDirToConfig(String directory)
  {
    try
    {
      saveValueToDisk(InfoHandlerConfigEnum.INFO_HANDLER_MEDIA_DIR, directory);
    }
    catch(DatastoreException e)
    {
      System.out.println(e);
      System.out.println("Failed to save database media directory into config file.");
    }
  }
  
  public static String getInfoHandlerDir()
  {
    return _releaseDir + File.separator + _INFO_DIR + File.separator + _INFO_HANDLER_DIR;
  }
  
  private void loadFile(String fileName) throws DatastoreException
  {
    Assert.expect(fileName != null);

    if (fileName.endsWith(".cfg") || fileName.endsWith(".config")|| fileName.endsWith(".calib"))
    {
      addKeyValuePairsToMap(fileName);
    }
  }

  private void addKeyValuePairsToMap(String configFile) throws DatastoreException
  {
    Assert.expect(configFile != null);

    Map<String, String> keyValueMap = null;

    // read the text file
    keyValueMap = _configFileTextReader.getKeyValueMapping(configFile);

    for (Map.Entry<String, String> entry : keyValueMap.entrySet())
    {
      String key = entry.getKey();
      String value = entry.getValue();
      InfoHandlerConfigEnum infoHandlerConfigEnum = InfoHandlerConfigEnum.getConfigEnum(key);

      // if object is null then the key found in the config file is not available through Config
      if (infoHandlerConfigEnum == null)
      {
        System.out.println(StringLocalizer.keyToString(new LocalizedString("DS_ERROR_INVALID_KEY_IN_CONFIG_KEY", new Object[]
        {
          key, value, configFile
        })));
      }

        //  throw new InvalidKeyInConfigDatastoreException(key, value, configFile);
      else
      {
        addKeyValuePairToMap(infoHandlerConfigEnum, key, value);
      }
    }
  }
  
  private void addKeyValuePairToMap(InfoHandlerConfigEnum infoHandlerConfigEnum, String key, String value) throws InvalidTypeDatastoreException
  {
    // enter the key value pair
    if (getValueFromMap(infoHandlerConfigEnum) == null)
    {
      value = substituteMacros(value);
      _configEnumToValueMap.put(infoHandlerConfigEnum, value);
    }

    else
    {
      System.out.println("Duplicate keys are not allowed. " + key + " was duplicated in file ");
    }
  }
  
  private Object getValueFromMap(InfoHandlerConfigEnum theEnum)
  {
    if (theEnum != null)
    {
      return extractConfigValue(theEnum, _configEnumToValueMap.get(theEnum));
    }
    else
    {
      return null;
    }
  }
  
  private String substituteMacros(String value)
  {
    int index = value.indexOf("$(");
    if(index > -1)
    {
      String subsString = value.substring(index + 2);
      index = subsString.indexOf(")");
      if(index > -1)
      {
        String macros = subsString.substring(0, index);
        value = subsString.substring(index+1);
        if(macros.equals("AXI_XRAY_HOME"))
        {
          macros = Directory.getXrayTesterReleaseDir();
        }
        value = macros + value;
      }
      
    }
    return value;
  }
  
  private void saveValueToDisk(InfoHandlerConfigEnum key, Object value) throws DatastoreException
  {
    Assert.expect(key != null);
    Assert.expect(value != null);

    Map<String, Object> keyValueMap = new HashMap<String, Object>();
    keyValueMap.put(key.getKey(), value);
    saveValuesToDisk(key.getFileName(), keyValueMap);
  }
  
  private void saveValuesToDisk(String fileName, Map<String, Object> keyToValueMap) throws DatastoreException
  {
    Assert.expect(fileName != null);
    Assert.expect(keyToValueMap != null);

    Set<String> keySet = keyToValueMap.keySet();

    int numKeys = keySet.size();
    int numModifiedLines = 0;
    // open the file for reading
    String inputFileName = fileName;
    FileReader fr = null;
    try
    {
      fr = new FileReader(inputFileName);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(inputFileName);
      ex.initCause(fnfe);
      throw ex;
    }
    BufferedReader is = new BufferedReader(fr);

    // open a temporary file for writing
    String outputFileName = inputFileName + FileName.getTempFileExtension();
    FileWriter fw = null;
    try
    {
      fw = new FileWriter(outputFileName);
    }
    catch(IOException ioe)
    {
      DatastoreException ex = new CannotCreateFileDatastoreException(outputFileName);
      ex.initCause(ioe);
      throw ex;
    }
    BufferedWriter bw = new BufferedWriter(fw);
    PrintWriter os = new PrintWriter(bw);

    try
    {
      // OK, now read in the input file and write out the modified file to
      // the output file

      String line = null;
      do
      {
        try
        {
          line = is.readLine();
        }
        catch(IOException ioe)
        {
          DatastoreException ex = new CannotReadDatastoreException(inputFileName);
          ex.initCause(ioe);
          throw ex;
        }
        if (line == null)
          continue;

        if (numModifiedLines < numKeys)
        {
          Matcher keyValueMatcher = _keyValuePattern.matcher(line);

          if (keyValueMatcher.matches())
          {
            String keyValueStr = keyValueMatcher.group(1);
            String keyStr = keyValueMatcher.group(2);
            String equalsStr = keyValueMatcher.group(3);
            String valueStr = keyValueMatcher.group(6);

            Object value = keyToValueMap.get(keyStr);

            if (value != null)
            {
              ++numModifiedLines;
              String stringValue = createStringValue(value);

              // change keyValueStr so it has \\ in front of all special characters so it will be interpreted
              // literally
              keyValueStr = keyValueStr.replaceAll("(\\W)","\\\\$1");
              // now replace the non comment part of the line with the new value
              line = line.replaceFirst(keyValueStr, keyStr + equalsStr + stringValue);
            }
          }
        }
        os.println(line);
      } while(line != null);
    }
    finally
    {
      // close the files
      if (is != null)
      {
        try
        {
          is.close();
        }
        catch (Exception ex)
        {
          // do nothing
        }
      }

      if (os != null)
        os.close();

      // a line should have been modified for each key or there was a bug
      Assert.expect(numModifiedLines == numKeys);
    }

    // now move the temporary file over to the original file
    FileUtilAxi.rename(outputFileName, inputFileName);
  }
  
  private String createStringValue(Object value)
  {
    String stringValue = value.toString();
    // change \ to \\
    stringValue = stringValue.replaceAll("\\\\", "\\\\\\\\");
    // change any # to \#
    stringValue = stringValue.replaceAll("#", "\\\\\\\\#");
    // change any $ to \$
    stringValue = stringValue.replaceAll("(\\$)", "\\\\$1");

    return stringValue;
  }
  
  private Object extractConfigValue(InfoHandlerConfigEnum theEnum, String value)
  {
    Object configValue = null;
    if (value == null)
      configValue = null;
    else
    {
      if (theEnum.getType().equals(TypeEnum.BOOLEAN))
      {
        if ((value.toString().equalsIgnoreCase("true")) || (value.toString().equalsIgnoreCase("false")))
        {
          configValue = new Boolean(value.toString());
        }
        else
        {
          System.out.println("Invalid config type for " + theEnum.toString() + " at " + theEnum.getFileName());
        }
      }
      else if (theEnum.getType().equals(TypeEnum.STRING))
        configValue = new String(value.toString());
      else if (theEnum.getType().equals(TypeEnum.BINARY))
        configValue = new String(value.toString());
      else
      {
        try
        {
          if (theEnum.getType().equals(TypeEnum.INTEGER))
            configValue = new Integer(value.toString());
          else if (theEnum.getType().equals(TypeEnum.LONG))
            configValue = new Long(value.toString());
          else if (theEnum.getType().equals(TypeEnum.DOUBLE))
            configValue = new Double(value.toString());
          else
            Assert.expect(false);
        }
        catch (NumberFormatException nfe)
        {
          System.out.println("Invalid config type for " + theEnum.toString() + " at " + theEnum.getFileName());
          System.out.println(nfe.getMessage());
        }
      }
    }
    return configValue;
  }
  
  public void setDatabaseLanguage(String language)
  {
    _language = language;
  }
  
  public String getUsingDatabaseLanguage()
  {
    return _language;
  }
  
  public static String getLocaleLanguage()
  {
    String localeLanguage = Localization.getLocale().toString();
    if(localeLanguage.equals("en"))
      localeLanguage = localeLanguage + "_US";
    return localeLanguage;
  }
  
  public boolean isUsingLanguageEqualLocale()
  {
    return getUsingDatabaseLanguage().equals(Localization.getLocale().toString());
  }
  
  public void refreshConfig()
  {
    _configEnumToValueMap.clear();
    loadConfig();
  }
  
  public void resetLanguage()
  {
    setDatabaseLanguage(Localization.getLocale().toString());
    refreshConfig();
  }
  
  /**
   * @author Swee Yee Wong
   */
  private void checkForMissingKeys() throws KeyMissingDatastoreException, InvalidTypeDatastoreException
  {
    // ConfigEnum has all the keys
    for (InfoHandlerConfigEnum configEnum : InfoHandlerConfigEnum.getFullConfigEnumList())
    {
      if (getValueFromMap(configEnum) == null)
      {
        String configFile = configEnum.getFileName();
        throw new KeyMissingDatastoreException(configEnum.getKey(), configFile);
      }
    }
  }
}

