package com.axi.v810.util;

import ij.*;
import ij.process.*;
import java.awt.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.imageio.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 *
 * @author khang-shian.sham
 */
public class ShapeMatching
{

  private static ShapeMatching _shapeMatchingAlgorithm;
  private java.util.List<Shape> _sourceShapeList;
  private java.util.List<Shape> _templateShapeList;
  private Map<AlignmentExcludedAreaEnum,Shape> _excludedAreaList;
  private double _largerAllowableMargin = 0.45;
  private double _smallerAllowableMargin = 0.45;
  private double _minWidth = 0;
  private double _minHeight = 0;
  private double _maxWidth = 0;
  private double _maxHeight = 0;
  private static boolean _log = false;
  private static boolean _partialLog = false;
  private static boolean _DEVELOPER_DEBUG;
  private long _logTime = 0;

  public ShapeMatching()
  {
    //Do nothing
  }

  public long getLogTime()
  {
    return _logTime;
  }

  public Pair<Double,ImageCoordinate> matchTemplateByShapeList(BufferedImage alignmentImage,java.util.List<Shape> templateShapeList,
          Rectangle sourceMaxBound,Map<AlignmentExcludedAreaEnum,Shape> excludedAreaList)
  {
    _partialLog = false;
    _DEVELOPER_DEBUG = Config.isDeveloperDebugModeOn();
    _logTime = System.currentTimeMillis();
//    System.out.println("//_logTime :" + _logTime);
//    System.out.println("getInstance().reset();");
//    System.out.println("sourceShapeList.clear();");
//    System.out.println("templateShapeList.clear();");
//    System.out.println("excludedAreaMap.clear();");
//    System.out.println("offsetList.clear();");
    Pair<java.util.List<java.awt.Shape>,com.axi.util.image.Image> sourceShapeListWithBinaryImage = new Pair<java.util.List<java.awt.Shape>,com.axi.util.image.Image>();
    java.util.List<Shape> sourceShapeList = new LinkedList<java.awt.Shape>();
    Pair<Double,ImageCoordinate> matchResult = new Pair<Double,ImageCoordinate>();
    java.util.List<ImageCoordinate> matchCoordinateList;
    for (Shape sp : templateShapeList)
    {
      if(_maxWidth == 0)
      {
        _maxWidth = (int) sp.getBounds().getWidth();
      }
      else if(sp.getBounds().getWidth() > _maxWidth)
      {
        _maxWidth = (int) sp.getBounds().getWidth();
      }
      if(_minWidth == 0)
      {
        _minWidth = (int) sp.getBounds().getWidth();
      }
      else if(sp.getBounds().getWidth() < _minWidth)
      {
        _minWidth = (int) sp.getBounds().getWidth();
      }
      if(_minHeight == 0)
      {
        _minHeight = (int) sp.getBounds().getHeight();
      }
      else if(sp.getBounds().getHeight() < _minHeight)
      {
        _minHeight = (int) sp.getBounds().getHeight();
      }
      if(_maxHeight == 0)
      {
        _maxHeight = (int) sp.getBounds().getHeight();
      }
      else if(sp.getBounds().getHeight() > _maxHeight)
      {
        _maxHeight = (int) sp.getBounds().getHeight();
      }
    }
//    System.out.println("//Souvala r:" + _maxWidth / 2 + "sw:" + _maxWidth);
    sourceShapeListWithBinaryImage = extractShapeFromImage(alignmentImage,_maxWidth / 2,_maxWidth);
    sourceShapeList = sourceShapeListWithBinaryImage.getFirst();
    filterSourceList(templateShapeList,sourceShapeList);
    matchCoordinateList = match(templateShapeList,sourceShapeList,sourceMaxBound,excludedAreaList);

    for(ImageCoordinate imgC : matchCoordinateList)
    {
      if(imgC.getX()==0 && imgC.getY()==0)
      {
        sourceShapeListWithBinaryImage.getSecond().decrementReferenceCount();
//        System.out.println("//Souvala r:" + _maxWidth + "sw:" + _maxWidth);
        sourceShapeListWithBinaryImage = extractShapeFromImage(alignmentImage,_maxWidth,_maxWidth);
        sourceShapeList = sourceShapeListWithBinaryImage.getFirst();
        filterSourceList(templateShapeList,sourceShapeList);
        matchCoordinateList = match(templateShapeList,sourceShapeList,sourceMaxBound,excludedAreaList);
        break;
      }
    }
//    System.out.println("//matchCoordinateList: " + matchCoordinateList);
    matchResult = findHighestMatchedPosition(templateShapeList,excludedAreaList,matchCoordinateList,sourceShapeListWithBinaryImage.getSecond());
    sourceShapeListWithBinaryImage.getSecond().decrementReferenceCount();

//    System.out.println("offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);");
//    System.out.println("System.out.println(\"expected offset x(" + matchResult.getSecond().getX() + "):\" + offsetList.get(0).getX());");
//    System.out.println("System.out.println(\"expected offset y(" + matchResult.getSecond().getY() + "):\" + offsetList.get(0).getY());");
//    System.out.println("System.out.println(\"\");");
//    System.out.println("System.out.println(\"--------------------------------------------------------------------------------------\");");
    return matchResult;
  }

  public java.util.List<ImageCoordinate> match(java.util.List<Shape> templateShapeList,java.util.List<Shape> sourceShapeList,
          Rectangle sourceMaxBound,Map<AlignmentExcludedAreaEnum,Shape> excludedAreaList)
  {
    _sourceShapeList = sourceShapeList;
    _templateShapeList = templateShapeList;
    _excludedAreaList = excludedAreaList;


//    if(_partialLog || _DEVELOPER_DEBUG || true)
//    {
//      if(_partialLog)
//      {
//
//        System.out.println("filtered _sourceShapeList");
//        for (Shape sp : _sourceShapeList)
//        {
//          System.out.println("sourceShapeList.add(new Rectangle(" + (int) sp.getBounds().getMinX() + ","
//                  + (int) sp.getBounds().getMinY() + "," + (int) sp.getBounds().getWidth() + "," + (int) sp.getBounds().getHeight() + "));");
//        }
//      }
//      for (Shape sp : _templateShapeList)
//      {
//        System.out.println("templateShapeList.add(new Rectangle(" + (int) sp.getBounds().getMinX() + ","
//                + (int) sp.getBounds().getMinY() + "," + (int) sp.getBounds().getWidth() + "," + (int) sp.getBounds().getHeight() + "));");
//      }
//
//      System.out.println("maxBound = new Rectangle(" + (int) sourceMaxBound.getBounds().getMinX() + "," + (int) sourceMaxBound.getBounds().getMinY()
//              + "," + (int) sourceMaxBound.getBounds().getWidth()
//              + "," + (int) sourceMaxBound.getBounds().getHeight() + ");");
//      if(excludedAreaList.isEmpty() == false)
//      {
//        for (AlignmentExcludedAreaEnum key : excludedAreaList.keySet())
//        {
//          System.out.println("excludedAreaMap.put(AlignmentExcludedAreaEnum." + key.getName().toUpperCase() + ",new Rectangle(" + (int) excludedAreaList.get(key).getBounds().getMinX() + ","
//                  + (int) excludedAreaList.get(key).getBounds().getMinY() + "," + (int) excludedAreaList.get(key).getBounds().getWidth() + "," + (int) excludedAreaList.get(key).getBounds().getHeight() + "));");
//        }
//      }
//    }
    Rectangle templateMaxBound = new Rectangle();
    for (Shape rect : templateShapeList)
    {
      templateMaxBound.add(rect.getBounds());
    }

    java.util.List<ImageCoordinate> roughMaxPoint = getRangeMatchRect(0 - 70,(int) sourceMaxBound.getWidth() + 70,1,0 - 70,(int) sourceMaxBound.getHeight() + 70,1,sourceMaxBound);
    if(roughMaxPoint.get(0).getX() < 0 || roughMaxPoint.get(0).getY() < 0)
    {
      roughMaxPoint.clear();
      roughMaxPoint.add(new ImageCoordinate(0,0));
    }

    return roughMaxPoint;
  }

  public java.util.List<ImageCoordinate> getRangeMatchRect(int xStepStart,int xStepEnd,int xStepSize,
          int yStepStart,int yStepEnd,int yStepSize,Rectangle sourceMaxBound)
  {
    double maxPointCount = 0;
    double minDeltaXY = sourceMaxBound.getWidth() + sourceMaxBound.getHeight();
    double maxPointX = 0;
    double maxPointY = 0;
    double pointSum = 0;
    int excludedPointSum = 0;
    int centerSizeX = 0;
    int centerSizeY = 0;
    double totalDeltaX = 0;
    double totalDeltaY = 0;
    double currentMeanDeltaXY = 0;

    java.util.List<ImageCoordinate> currentMatchPoints = new java.util.LinkedList<ImageCoordinate>();
    java.util.List<ImageCoordinate> maxPointList = new java.util.LinkedList<ImageCoordinate>();
    Rectangle searchBound = new Rectangle((int) sourceMaxBound.getMinX(),(int) sourceMaxBound.getMinY(),(int) sourceMaxBound.getWidth() + 2,(int) sourceMaxBound.getHeight() + 2);
    for (double y = yStepStart; y < yStepEnd; y = y + yStepSize)
    {
      for (double x = xStepStart; x < xStepEnd; x = x + xStepSize)
      {
        pointSum = 0;
        excludedPointSum = 0;
        centerSizeX = 1;
        centerSizeY = 1;
        currentMatchPoints.clear();
        totalDeltaX = 0;
        totalDeltaY = 0;

        for (Shape shapeT : _templateShapeList)
        {
          Rectangle2D rc = shapeT.getBounds2D();
          Rectangle2D rcCad = new Rectangle2D.Double(rc.getMinX() + x,rc.getMinY() + y,rc.getWidth(),rc.getHeight());
//          if((rc.getMinX() == 0 && rc.getMinY() == 200) && ((x == 68 && y == 68) || (x == 70 && y == 69)) && _partialLog)
//          {
//            System.out.println("rc x:" + rc.getMinX() + " y:" + rc.getMinY());
//            System.out.println("rc: " + rc);
//            System.out.println("searchBound: " + searchBound);
//            System.out.println("rcCad: " + rcCad);
//          }
          if(searchBound.contains(rcCad))
          {
            Rectangle2D rcCenterCad = null;
            if(_smallerAllowableMargin > _largerAllowableMargin)
            {
              rcCenterCad = new Rectangle2D.Double((rcCad.getMinX() + (rcCad.getWidth() - 1) / 2) - (rcCad.getWidth() * _smallerAllowableMargin) / 2,//(centerSizeX + padBufferX),
                      (rcCad.getMinY() + (rcCad.getHeight() - 1) / 2) - (rcCad.getHeight() * _smallerAllowableMargin) / 2,
                      2 * (rcCad.getWidth() / 2 * _smallerAllowableMargin),2 * (rcCad.getHeight() / 2 * _smallerAllowableMargin));
            }
            else
            {
              rcCenterCad = new Rectangle2D.Double((rcCad.getMinX() + (rcCad.getWidth() - 1) / 2) - (rcCad.getWidth() * _largerAllowableMargin) / 2,//(centerSizeX + padBufferX),
                      (rcCad.getMinY() + (rcCad.getHeight() - 1) / 2) - (rcCad.getHeight() * _largerAllowableMargin) / 2,
                      2 * (rcCad.getWidth() / 2 * _largerAllowableMargin),2 * (rcCad.getHeight() / 2 * _largerAllowableMargin));
            }
//            if(((x == 68 && y == 68) || (x == 70 && y == 69)) && _partialLog)
//            {
//              System.out.println("");
//
//              System.out.println("x:" + x + " y:" + y);
//              System.out.println("rc:" + rc);
//
//              System.out.println("rcCenterCad :" + rcCenterCad);
//            }
            for (Shape shapeS : _sourceShapeList)
            {
              Rectangle2D rcImage = shapeS.getBounds2D();
              Rectangle2D rcCenterImage = new Rectangle2D.Double(rcImage.getCenterX() - centerSizeX,
                      rcImage.getCenterY() - centerSizeY,
                      0 * centerSizeX + 1,
                      0 * centerSizeY + 1);
//              if(((x == 68 && y == 68) || (x == 70 && y == 69)) && _partialLog)
//              {
//                System.out.println("       rcCenterImage: " + rcCenterImage);
//              }
              if(rcCenterCad.contains(rcCenterImage))
              {
                double WidthRatio = rcImage.getWidth() / rcCad.getWidth();
                double HeightRatio = rcImage.getHeight() / rcCad.getHeight();
//                // if((WidthRatio > (1 - _smallerAllowableMargin)) && (WidthRatio < (1 + _largerAllowableMargin)))
//                {
//                  // if((HeightRatio > (1 - _smallerAllowableMargin)) && (HeightRatio < (1 + _largerAllowableMargin)))
//                  {
//                    if(((x == 68 && y == 68) || (x == 70 && y == 69)) && _partialLog)
//                    {
//                      System.out.println("                match!!");
//                    }
//                    pointSum++;
//                    totalDeltaX += Math.abs((rcCad.getMinX() + ((rcCad.getWidth() - 1) / 2)) - (rcImage.getCenterX()));
//                    totalDeltaY += Math.abs((rcCad.getMinY() + ((rcCad.getHeight() - 1) / 2)) - (rcImage.getCenterY()));
//                  }
//                }
              }
            }
          }
        }

        //search blob in excludedArea
        for (Shape shapeS : _sourceShapeList)
        {
          Rectangle rcImage = shapeS.getBounds();
          for (AlignmentExcludedAreaEnum key : _excludedAreaList.keySet())
          {
            Rectangle2D excludedAreaRectangle = new Rectangle2D.Double(_excludedAreaList.get(key).getBounds2D().getMinX() + x,_excludedAreaList.get(key).getBounds().getMinY() + y,(int) _excludedAreaList.get(key).getBounds().getWidth(),(int) _excludedAreaList.get(key).getBounds().getHeight());

            if(excludedAreaRectangle.intersects(rcImage))
            {
              Rectangle intersectionRec;
              intersectionRec = excludedAreaRectangle.getBounds().intersection(rcImage);
//              if(((x == 162 && y == 60) || (x == 63 && y == 61)) && false)
//              {
//                System.out.println("excludedAreaRectangle:");
//                System.out.println("rcImage:" + rcImage);
//                System.out.println("intersectionRec:" + intersectionRec);
//              }
              double totalAreaIntersectionRec = intersectionRec.getBounds().getWidth() * intersectionRec.getBounds().getHeight();
              double intersectionPercentage = (double) ((totalAreaIntersectionRec) / (shapeS.getBounds2D().getWidth() * shapeS.getBounds2D().getWidth()));
//              if(((x == 162 && y == 60) || (x == 63 && y == 61)) && false)
//              {
//                System.out.println("intersectionPercentage :" + intersectionPercentage);
//              }
              if(intersectionPercentage > 0.8)
              {
                excludedPointSum++;
              }
            }
          }
        }
        pointSum -= excludedPointSum;


        if(pointSum > maxPointCount)
        {
          maxPointCount = pointSum;
          maxPointX = x;
          maxPointY = y;
          maxPointList.clear();
          maxPointList.add(new ImageCoordinate((int) maxPointX,(int) maxPointY));
          minDeltaXY = (double) totalDeltaX / (double) pointSum + (double) totalDeltaY / (double) pointSum;
        }
        else if(pointSum == maxPointCount && pointSum != 0)
        {
          currentMeanDeltaXY = (double) totalDeltaX / (double) pointSum + (double) totalDeltaY / (double) pointSum;
          if(currentMeanDeltaXY < minDeltaXY)
          {
            maxPointList.clear();
            maxPointX = x;
            maxPointY = y;
            minDeltaXY = currentMeanDeltaXY;
            maxPointList.add(new ImageCoordinate((int) maxPointX,(int) maxPointY));
          }
        }
//        if(((x == 68 && y == 68) || (x == 70 && y == 69)) && _partialLog)
//        {
//          System.out.println("");
//          System.out.println("");
//          System.out.println("x:" + x + " y:" + y);
//          System.out.println("excludedPointSum: " + excludedPointSum);
//          System.out.println("pointSum: " + pointSum);
//          System.out.println("totalDeltaX: " + totalDeltaX);
//          System.out.println("totalDeltaY: " + totalDeltaY);
//          System.out.println("maxPointCount: " + maxPointCount);
//          System.out.println("maxPointList: " + maxPointList);
//        }
      }
    }
    if(maxPointList.isEmpty())
    {
      maxPointList.add(new ImageCoordinate(0,0));
    }

    return maxPointList;
  }

  public static ShapeMatching getInstance()
  {
    if(_shapeMatchingAlgorithm == null)
    {
      _shapeMatchingAlgorithm = new ShapeMatching();
    }
    return _shapeMatchingAlgorithm;
  }

  public Pair<Double,ImageCoordinate> findHighestMatchedPosition(java.util.List<Shape> alignmentTemplateShapeInPixelsList,Map<AlignmentExcludedAreaEnum,java.awt.Shape> alignmentExcludedAreaInTemplateRoiList,java.util.List<ImageCoordinate> matchedLocationList,com.axi.util.image.Image binaryImage)
  {
    Pair<Double,ImageCoordinate> finalMatchQuality = new Pair<Double,ImageCoordinate>(0.0,new ImageCoordinate(0,0));
    finalMatchQuality.setFirst(0.0);

    for (ImageCoordinate coor : matchedLocationList)
    {
      double matchQuality = 0;
      double totalBlackPixelsCountInTemplate = 0;
      double totalWhitePixelsCountInTemplate = 0;
      double totalBlackPixelsCountInShapeList = 0;
      double totalWhitePixelsCountInShapeList = 0;
      Rectangle maxBoundInTemplate = new Rectangle(0,0,0,0);
      double blackPixelsMatchPercentage = 0.0;
      double whitePixelsMatchPercentage = 0.0;
      double excludeAreaPixelsCount = 0;
      double totalExcludedPixels = 0;
      double includedWeight = 0.9;
      double excludedWeight = 0.1;
      int x = 0, y = 0;
      Rectangle binaryImageMaxBound = new Rectangle(0,0,binaryImage.getWidth()+1,binaryImage.getHeight()+1);
      for (Shape tplSp : alignmentTemplateShapeInPixelsList)
      {

        RegionOfInterest roi = new RegionOfInterest((int) tplSp.getBounds().getMinX() + coor.getX(),(int) tplSp.getBounds().getMinY() + coor.getY(),(int) tplSp.getBounds().getWidth(),(int) tplSp.getBounds().getHeight(),0,RegionShapeEnum.RECTANGULAR);
        if(binaryImageMaxBound.contains(roi.getRectangle2D()))
        {
          if(roi.getMaxX() > binaryImage.getWidth())
          {
            //roi.setWidthKeepingSameMinX(roi.getWidth()-(binaryImage.getWidth()-roi.getMaxX()));
          }
          if(roi.getMaxY() > binaryImage.getHeight())
          {
           // roi.setHeightKeepingSameMinY(roi.getHeight()-(binaryImage.getHeight()-roi.getMaxY()));
          }
          totalBlackPixelsCountInShapeList += Threshold.countPixelsInRange(binaryImage,roi,200,255);
          totalWhitePixelsCountInShapeList += Threshold.countPixelsInRange(binaryImage,roi,0,1);
          maxBoundInTemplate.add(roi.getRectangle2D());

        }
      }

        totalBlackPixelsCountInTemplate = Threshold.countPixelsInRange(binaryImage,new RegionOfInterest((int) maxBoundInTemplate.getMinX(),(int) maxBoundInTemplate.getMinY(),(int) maxBoundInTemplate.getWidth(),(int) maxBoundInTemplate.getHeight(),0,RegionShapeEnum.RECTANGULAR),254,255);
        totalWhitePixelsCountInTemplate = Threshold.countPixelsInRange(binaryImage,new RegionOfInterest((int) maxBoundInTemplate.getMinX(),(int) maxBoundInTemplate.getMinY(),(int) maxBoundInTemplate.getWidth(),(int) maxBoundInTemplate.getHeight(),0,RegionShapeEnum.RECTANGULAR),0,1);
        blackPixelsMatchPercentage = 100 - (((totalBlackPixelsCountInTemplate - totalBlackPixelsCountInShapeList) / totalBlackPixelsCountInTemplate) * 100);
        /*System.out.println("totalWhitePixelsCountInShapeList:" + totalWhitePixelsCountInShapeList);
        System.out.println("totalWhitePixelsCountInTemplate:" + totalWhitePixelsCountInTemplate);
        System.out.println("totalBlackPixelsCountInShapeList:" + totalBlackPixelsCountInShapeList);
        System.out.println("totalBlackPixelsCountInTemplate:" + totalBlackPixelsCountInTemplate);
        whitePixelsMatchPercentage = (((totalWhitePixelsCountInTemplate - totalWhitePixelsCountInShapeList) / totalWhitePixelsCountInTemplate) * 100);
        System.out.println("blackPixelsMatchPercentage:" + blackPixelsMatchPercentage);
        System.out.println("whitePixelsMatchPercentage:" + whitePixelsMatchPercentage);*/

        for (Shape sp : alignmentExcludedAreaInTemplateRoiList.values())
        {
          //trim
          if(sp.getBounds().getMinX() + (int) coor.getX() >= 0 && sp.getBounds().getMinX() + (int) coor.getX() + (int) sp.getBounds().getWidth() <= binaryImage.getWidth())
          {
            x = (int) sp.getBounds().getMinX() + (int) coor.getX();
          }
          else
          {
            if(sp.getBounds().getMinX() + (int) coor.getX() < 0)
            {
              x = 0;
            }
            else if(sp.getBounds().getMinX() + (int) coor.getX() + (int) sp.getBounds().getWidth() > binaryImage.getWidth()-1)
            {
              x = binaryImage.getWidth() - (int) sp.getBounds().getWidth()-1;
            }
          }
          if(sp.getBounds().getMinY() + (int) coor.getY() >= 0 && sp.getBounds().getMinY() + (int) coor.getY() + (int) sp.getBounds().getHeight() <= binaryImage.getHeight())
          {
            y = (int) sp.getBounds().getMinY() + (int) coor.getY();
          }
          else
          {
            if(sp.getBounds().getMinY() + (int) coor.getY() < 0)
            {
              y = 0;
            }
            else if(sp.getBounds().getMinY() + (int) coor.getY() + (int) sp.getBounds().getHeight() > binaryImage.getHeight() - 1)
            {
              y = binaryImage.getHeight() - (int) sp.getBounds().getHeight() - 1;
            }
          }

          excludeAreaPixelsCount += Threshold.countPixelsInRange(binaryImage,new RegionOfInterest(x,y,(int) sp.getBounds().getWidth(),(int) sp.getBounds().getHeight(),0,RegionShapeEnum.RECTANGULAR),0,1);
          totalExcludedPixels += (int) sp.getBounds().getWidth() * (int) sp.getBounds().getHeight();
        }

        matchQuality = ((((blackPixelsMatchPercentage + whitePixelsMatchPercentage) / 2) * includedWeight) + (excludeAreaPixelsCount / totalExcludedPixels * excludedWeight)) / (((blackPixelsMatchPercentage + whitePixelsMatchPercentage) / 2) + (excludeAreaPixelsCount / totalExcludedPixels));
        if(matchQuality > finalMatchQuality.getFirst())
        {
          finalMatchQuality.setFirst(matchQuality);
          finalMatchQuality.setSecond(coor);
        }
    }

    return finalMatchQuality;
  }

  /**
   *
   * @param alignmentImage
   * @param radius
   * @param searchWindow
   * @author sham
   *
   * @return List<Shape>
   */
  public Pair<java.util.List<java.awt.Shape>,com.axi.util.image.Image> extractShapeFromImage(BufferedImage alignmentImage,double radius,double searchWindow)
  {
    //extract alignment features fromalignment image
    ImagePlus imagePlus = new ImagePlus("",alignmentImage);
    //Apply Sauvola Local Threshold
    AutoLocalThreshold.getInstance().Sauvola(imagePlus,(int) (radius),0.1,(int) searchWindow,true);
    //AutoLocalThreshold.getInstance().Sauvola(imagePlus,(int) (radius),0.1,(int) searchWindow,false);
    if(_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
    {
      ij.IJ.save(imagePlus,Directory.getAlignmentLogDir() + "\\" + _logTime + "thresholdimage_" + String.valueOf(radius) + "_" + String.valueOf(searchWindow) + ".png");
    }
    //Convert to Binary
    MaskImageConverter cm = new MaskImageConverter();
    cm.convertImageToBinary(imagePlus);
    com.axi.util.image.Image binaryImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(imagePlus.getBufferedImage());
    if(_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
    {
      ij.IJ.save(imagePlus,Directory.getAlignmentLogDir() + "\\" + _logTime + "_binaryimage_" + ".png");
    }
    //convert to RGB
    ImageConverter ic = new ImageConverter(imagePlus);
    ic.convertToRGB();

    ByteArrayOutputStream byar = new ByteArrayOutputStream();
    try
    {
      ImageIO.write(imagePlus.getBufferedImage(),"jpg",byar);
      byar.flush();
    }
    catch (IOException ex)
    {
    }
    byte[] b = byar.toByteArray();
    InputStream in = new ByteArrayInputStream(b);
    BufferedImage bImageFromConvert = null;
    try
    {
      bImageFromConvert = ImageIO.read(in);
    }
    catch (IOException ex)
    {
    }
    java.util.List<java.awt.Shape> sourceShapeList = new LinkedList<java.awt.Shape>();
    sourceShapeList = BlobAnalyzer.getInstance().analyze(bImageFromConvert, false);

    return new Pair<java.util.List<java.awt.Shape>,com.axi.util.image.Image>(sourceShapeList,binaryImage);
  }

  public com.axi.util.image.Image extractSauvolaImage(BufferedImage alignmentImage,java.util.List<RegionOfInterest> alignmentFeaturesInTemplateImage)
  {

    for (RegionOfInterest sp : alignmentFeaturesInTemplateImage)
    {
      if(_maxWidth == 0)
      {
        _maxWidth = (int) sp.getWidth();
      }
      else if(sp.getWidth() > _maxWidth)
      {
        _maxWidth = (int) sp.getWidth();
      }
      if(_minWidth == 0)
      {
        _minWidth = (int) sp.getWidth();
      }
      else if(sp.getWidth() < _minWidth)
      {
        _minWidth = (int) sp.getWidth();
      }
      if(_minHeight == 0)
      {
        _minHeight = (int) sp.getHeight();
      }
      else if(sp.getHeight() < _minHeight)
      {
        _minHeight = (int) sp.getHeight();
      }
      if(_maxHeight == 0)
      {
        _maxHeight = (int) sp.getHeight();
      }
      else if(sp.getHeight() > _maxHeight)
      {
        _maxHeight = (int) sp.getHeight();
      }
    }
//    double radius=_maxHeight>_maxWidth?_maxHeight/2:_maxWidth / 2;
//    double searchWindow=_maxHeight>_maxWidth?_maxHeight:_maxWidth;
    //double radius=_minHeight<_minWidth?_minHeight/2:_minWidth / 2;
    //double searchWindow=_minHeight<_minWidth?_minHeight:_minWidth;
    
    
    double radius= (_maxHeight/8.0 +_maxWidth /8.0);
    double searchWindow= (_maxHeight+_maxWidth)/4.0;

    //extract alignment features fromalignment image
    ImagePlus imagePlus = new ImagePlus("",alignmentImage);
    
    //Apply Sauvola Local Threshold
    AutoLocalThreshold.getInstance().Sauvola(imagePlus,(int) (radius),0.15,(int)searchWindow ,true);
    //AutoLocalThreshold.getInstance().Sauvola(imagePlus,(int) (radius),0.1,(int) searchWindow,false);
    if(_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
    {
      ij.IJ.save(imagePlus,Directory.getAlignmentLogDir() + "\\" + _logTime + "thresholdimage_" + String.valueOf(radius) + "_" + String.valueOf(searchWindow) + ".png");
    }
    com.axi.util.image.Image binaryImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(imagePlus.getBufferedImage());
    return  binaryImage;
  }
  public com.axi.util.image.Image extractLAABImage(BufferedImage alignmentImage,java.util.List<RegionOfInterest> alignmentFeaturesInTemplateImage)
  {

    for (RegionOfInterest sp : alignmentFeaturesInTemplateImage)
    {
      if(_maxWidth == 0)
      {
        _maxWidth = (int) sp.getWidth();
      }
      else if(sp.getWidth() > _maxWidth)
      {
        _maxWidth = (int) sp.getWidth();
      }
      if(_minWidth == 0)
      {
        _minWidth = (int) sp.getWidth();
      }
      else if(sp.getWidth() < _minWidth)
      {
        _minWidth = (int) sp.getWidth();
      }
      if(_minHeight == 0)
      {
        _minHeight = (int) sp.getHeight();
      }
      else if(sp.getHeight() < _minHeight)
      {
        _minHeight = (int) sp.getHeight();
      }
      if(_maxHeight == 0)
      {
        _maxHeight = (int) sp.getHeight();
      }
      else if(sp.getHeight() > _maxHeight)
      {
        _maxHeight = (int) sp.getHeight();
      }
    }
//    double radius=_maxHeight>_maxWidth?_maxHeight/2:_maxWidth / 2;
//    double searchWindow=_maxHeight>_maxWidth?_maxHeight:_maxWidth;
    //double radius=_minHeight<_minWidth?_minHeight/2:_minWidth / 2;
    //double searchWindow=_minHeight<_minWidth?_minHeight:_minWidth;


    double radius= (_maxHeight +_maxWidth )/10.0;
    //double radius= 30;
    if(radius<30) radius = 30;
    if(radius>100) radius = 100;
    //extract alignment features fromalignment image
    ImagePlus imagePlus = new ImagePlus("",alignmentImage);

    //Apply Sauvola Local Threshold
    AutoLocalThreshold.getInstance().LocalAdaptiveAutomaticBinarisation(imagePlus,(int) (radius),0.520f,false);
    //AutoLocalThreshold.getInstance().Sauvola(imagePlus,(int) (radius),0.1,(int) searchWindow,false);
    //if(_DEVELOPER_DEBUG || true)
    if (_DEVELOPER_DEBUG && (UnitTest.unitTesting() == false))
    {
      ij.IJ.save(imagePlus,Directory.getAlignmentLogDir() + "\\" + _logTime + "thresholdimage_" + String.valueOf(radius) + ".png");
    }
    com.axi.util.image.Image binaryImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(imagePlus.getBufferedImage());
    return  binaryImage;
  }

  public void reset()
  {
    _largerAllowableMargin = 0.45;
    _smallerAllowableMargin = 0.45;
    _minWidth = 0;
    _minHeight = 0;
    _maxWidth = 0;
    _maxHeight = 0;
  }

  private void filterSourceList(java.util.List<Shape> templateShapeList,java.util.List<Shape> sourceShapeList)
  {
    java.util.List<Shape> filterShape = new java.util.LinkedList<Shape>();
    java.util.List<Shape> mustfilterShapeList = new java.util.LinkedList<Shape>();
    double smallerCount = 0;
    double largerCount = 0;
    double totalSmallerWidth = 0;
    double totalLargerWidth = 0;
    boolean refilter = false;
    double originalSourceShapeListSize = (double) sourceShapeList.size();
//    if(_partialLog)
//    {
//      for (Shape sp : sourceShapeList)
//      {
//        System.out.println("sourceShapeList.add(new Rectangle(" + (int) sp.getBounds().getMinX() + ","
//                + (int) sp.getBounds().getMinY() + "," + (int) sp.getBounds().getWidth() + "," + (int) sp.getBounds().getHeight() + "));");
//      }
//      System.out.println("");
//      System.out.println("");
//    }

    //filter noise
    for (Shape sp : sourceShapeList)
    {
      if(sp.getBounds().getWidth() <= _minWidth * 0.1 || sp.getBounds().getHeight() <= _minHeight * 0.1 || sp.getBounds().getWidth() > _maxWidth * 3 || sp.getBounds().getHeight() > _maxHeight * 3)
      {
        mustfilterShapeList.add(sp);
      }
      else
      {
        if(sp.getBounds().getWidth() < _minWidth * (1 - _smallerAllowableMargin)
                || sp.getBounds().getHeight() < _minHeight * (1 - _smallerAllowableMargin))
        {
          smallerCount++;
          totalSmallerWidth += sp.getBounds().getWidth();
          filterShape.add(sp);
        }
        else if(sp.getBounds().getWidth() > _maxWidth * (1 + _largerAllowableMargin)
                || sp.getBounds().getHeight() > _maxHeight * (1 + _largerAllowableMargin))
        {
          largerCount++;
          totalLargerWidth += sp.getBounds().getWidth();
          filterShape.add(sp);
        }
      }
    }
    sourceShapeList.removeAll(mustfilterShapeList);
    if((largerCount != 0 || smallerCount != 0) && (((double) sourceShapeList.size() - (double) filterShape.size()) / (double) templateShapeList.size() < 0.7))
    {
      if(((int) largerCount < (int) smallerCount) || (double) mustfilterShapeList.size() / originalSourceShapeListSize > 0.9)
      {
        _smallerAllowableMargin = ((_minWidth - (totalSmallerWidth / smallerCount)) / _minWidth) + 0.1;
//        System.out.println("//_smallerAllowableMargin:" + _smallerAllowableMargin);
        refilter = true;
      }
      else if((int) largerCount == (int) smallerCount)
      {
        filterShape.clear();
        _largerAllowableMargin = (((totalLargerWidth / largerCount) - _maxHeight) / _maxHeight) + 0.1;
        _smallerAllowableMargin = ((_minWidth - (totalSmallerWidth / smallerCount)) / _minWidth) + 0.1;
        refilter = true;
      }
      else if((int) largerCount > (int) smallerCount)
      {
        filterShape.clear();
        _largerAllowableMargin = (((totalLargerWidth / largerCount) - _maxHeight) / _maxHeight) + 0.1;
//        System.out.println("//_largerAllowableMargin:" + _largerAllowableMargin);
        refilter = true;
      }
      //refilter noise
      if(refilter)
      {
        filterShape.clear();
//        System.out.println("//refilter");
//        System.out.println("//_smallerAllowableMargin:" + _smallerAllowableMargin);
//        System.out.println("//_largerAllowableMargin:" + _largerAllowableMargin);
//        System.out.println("//_minWidth * (1 - _smallerAllowableMargin): " + _minWidth * (1 - _smallerAllowableMargin));
//        System.out.println("//_maxWidth * (1 + _largerAllowableMargin): " + _maxWidth * (1 + _largerAllowableMargin));

        for (Shape sp : sourceShapeList)
        {
          if((sp.getBounds().getWidth() < _minWidth * (1 - _smallerAllowableMargin) || sp.getBounds().getWidth() > _maxWidth * (1 + _largerAllowableMargin))
                  && (sp.getBounds().getHeight() < _minHeight * (1 - _smallerAllowableMargin) || sp.getBounds().getHeight() > _maxHeight * (1 + _largerAllowableMargin)))
          {
            filterShape.add(sp);
          }
        }
      }
    }

    //noise still exist(should apply remove outlier method here)
    /* if((sourceShapeListWithBinaryImage.getFirst().size() - filterShape.size()) > templateShapeList.size())
    {
    filterShape.clear();
    _smallerAllowableMargin = 0.35;
    _largerAllowableMargin = 0.35;
    //filter noise
    for (Shape sp : sourceShapeListWithBinaryImage.getFirst())
    {
    if((sp.getBounds().getWidth() < _minWidth * (1 - _smallerAllowableMargin) || sp.getBounds().getWidth() > _maxWidth * (1 + _largerAllowableMargin))
    && (sp.getBounds().getHeight() < _minHeight * (1 - _smallerAllowableMargin) || sp.getBounds().getHeight() > _maxHeight * (1 + _largerAllowableMargin)))
    {
    filterShape.add(sp);
    }
    }
    }*/

    sourceShapeList.removeAll(filterShape);
  }

  public java.util.List<ImageCoordinate> testMatchTemplateByShapeList(java.util.List<Shape> templateShapeList,java.util.List<Shape> sourceShapeList,
          Rectangle sourceMaxBound,Map<AlignmentExcludedAreaEnum,Shape> excludedAreaList)
  {
    for (Shape sp : templateShapeList)
    {
      if(_maxWidth == 0)
      {
        _maxWidth = (int) sp.getBounds().getWidth();
      }
      else if(sp.getBounds().getWidth() > _maxWidth)
      {
        _maxWidth = (int) sp.getBounds().getWidth();
      }
      if(_minWidth == 0)
      {
        _minWidth = (int) sp.getBounds().getWidth();
      }
      else if(sp.getBounds().getWidth() < _minWidth)
      {
        _minWidth = (int) sp.getBounds().getWidth();
      }
      if(_minHeight == 0)
      {
        _minHeight = (int) sp.getBounds().getHeight();
      }
      else if(sp.getBounds().getHeight() < _minHeight)
      {
        _minHeight = (int) sp.getBounds().getHeight();
      }
      if(_maxHeight == 0)
      {
        _maxHeight = (int) sp.getBounds().getHeight();
      }
      else if(sp.getBounds().getHeight() > _maxHeight)
      {
        _maxHeight = (int) sp.getBounds().getHeight();
      }
    }
    filterSourceList(templateShapeList,sourceShapeList);

    return match(templateShapeList,sourceShapeList,sourceMaxBound,excludedAreaList);
  }

  public static void main(String[] args)
  {
    java.util.List<Shape> templateShapeList = new java.util.LinkedList<Shape>();
    java.util.List<Shape> sourceShapeList = new java.util.LinkedList<Shape>();
    Map<AlignmentExcludedAreaEnum,Shape> excludedAreaMap = new HashMap<AlignmentExcludedAreaEnum,Shape>();
    java.util.List<ImageCoordinate> offsetList = new LinkedList<ImageCoordinate>();
    Rectangle maxBound;
    _log = false;
    _partialLog = true;

    /*
    System.out.println("ofline fake:");
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(173,120,21,22));
    sourceShapeList.add(new Rectangle(120,120,22,22));
    sourceShapeList.add(new Rectangle(173,67,21,22));
    sourceShapeList.add(new Rectangle(120,67,22,22));
    sourceShapeList.add(new Rectangle(67,67,22,22));
    sourceShapeList.add(new Rectangle(173,15,21,21));
    sourceShapeList.add(new Rectangle(120,15,22,21));
    sourceShapeList.add(new Rectangle(67,15,22,21));
    templateShapeList.add(new Rectangle(105,-53,23,23));
    templateShapeList.add(new Rectangle(105,0,23,23));
    templateShapeList.add(new Rectangle(105,52,23,23));
    templateShapeList.add(new Rectangle(52,-53,23,23));
    templateShapeList.add(new Rectangle(52,0,23,23));
    templateShapeList.add(new Rectangle(52,52,23,23));
    templateShapeList.add(new Rectangle(0,-53,23,23));
    templateShapeList.add(new Rectangle(0,0,23,23));
    maxBound = new Rectangle(0,0,210,210);
    excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,75,209,67));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,76));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(68):" + offsetList.get(0).getX());
    System.out.println("expected offset y(67):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");



    System.out.println("Fail 1:");  System.out.println("case 2:");
    //Fail 1
    //clean
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(177,534,0,0));
    sourceShapeList.add(new Rectangle(135,487,0,0));
    sourceShapeList.add(new Rectangle(178,483,0,3));
    sourceShapeList.add(new Rectangle(134,472,0,0));
    sourceShapeList.add(new Rectangle(159,409,0,0));
    sourceShapeList.add(new Rectangle(131,409,53,127));
    sourceShapeList.add(new Rectangle(152,407,1,0));
    sourceShapeList.add(new Rectangle(106,284,3,3));
    sourceShapeList.add(new Rectangle(108,280,0,0));
    sourceShapeList.add(new Rectangle(106,279,0,0));
    sourceShapeList.add(new Rectangle(188,247,0,0));
    sourceShapeList.add(new Rectangle(105,245,120,73));
    sourceShapeList.add(new Rectangle(74,67,179,91));
    templateShapeList.add(new Rectangle(134,0,53,94));
    templateShapeList.add(new Rectangle(0,0,187,94));
    templateShapeList.add(new Rectangle(0,0,53,94));
    templateShapeList.add(new Rectangle(116,177,40,74));
    templateShapeList.add(new Rectangle(31,177,125,74));
    templateShapeList.add(new Rectangle(31,177,40,74));
    templateShapeList.add(new Rectangle(53,331,80,84));
    maxBound = new Rectangle(0,0,321,549);
    //excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,414,321,67));//not suppose to be here because it have a pad there
    excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,415));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.RIGHT,new Rectangle(186,0,67,415));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.TOP,new Rectangle(-67,-67,321,67));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(71):" + offsetList.get(0).getX());
    System.out.println("expected offset y(66):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");  System.out.println("Fail 2:");

    //Fail 2 (expected : x:69.0  y:65.0)
    //clean
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();

    sourceShapeList.add(new Rectangle(162,256,0,0));
    sourceShapeList.add(new Rectangle(94,227,0,0));
    sourceShapeList.add(new Rectangle(96,210,0,0));
    sourceShapeList.add(new Rectangle(98,209,1,1));
    sourceShapeList.add(new Rectangle(88,207,0,0));
    sourceShapeList.add(new Rectangle(64,197,31,68));
    sourceShapeList.add(new Rectangle(173,196,18,69));
    sourceShapeList.add(new Rectangle(166,122,2,1));
    sourceShapeList.add(new Rectangle(94,118,0,0));
    sourceShapeList.add(new Rectangle(170,110,0,0));
    sourceShapeList.add(new Rectangle(164,76,2,2));
    sourceShapeList.add(new Rectangle(167,73,0,0));
    sourceShapeList.add(new Rectangle(168,68,3,5));
    sourceShapeList.add(new Rectangle(65,63,23,68));
    sourceShapeList.add(new Rectangle(169,62,25,68));
    sourceShapeList.add(new Rectangle(85,0,0,0));
    sourceShapeList.add(new Rectangle(81,0,0,0));
    sourceShapeList.add(new Rectangle(75,0,2,0));

    templateShapeList.add(new Rectangle(0,0,40,74));
    templateShapeList.add(new Rectangle(0,0,125,74));
    templateShapeList.add(new Rectangle(85,0,40,74));
    templateShapeList.add(new Rectangle(0,134,40,74));
    templateShapeList.add(new Rectangle(0,134,125,74));
    templateShapeList.add(new Rectangle(85,134,40,74));
    maxBound = new Rectangle(0,0,260,341);
    excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,206,260,67));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,207));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.RIGHT,new Rectangle(125,0,67,207));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.TOP,new Rectangle(-67,-67,260,67));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(68):" + offsetList.get(0).getX());
    System.out.println("expected offset y(61):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");
    System.out.println("Fail 3:");
    //Fail 3 (expected : x:69.0/70.0  y:65.0)
    //clean
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(83,123,0,0));
    sourceShapeList.add(new Rectangle(24,122,0,0));
    sourceShapeList.add(new Rectangle(29,121,0,0));
    sourceShapeList.add(new Rectangle(27,121,0,0));
    sourceShapeList.add(new Rectangle(62,113,36,34));
    sourceShapeList.add(new Rectangle(114,112,36,36));
    sourceShapeList.add(new Rectangle(10,111,36,36));
    sourceShapeList.add(new Rectangle(75,78,0,0));
    sourceShapeList.add(new Rectangle(186,72,0,0));
    sourceShapeList.add(new Rectangle(93,64,0,0));
    sourceShapeList.add(new Rectangle(168,58,36,36));
    sourceShapeList.add(new Rectangle(116,58,34,35));
    sourceShapeList.add(new Rectangle(63,58,35,35));
    sourceShapeList.add(new Rectangle(10,58,34,34));
    sourceShapeList.add(new Rectangle(22,21,1,3));
    sourceShapeList.add(new Rectangle(115,7,35,34));
    sourceShapeList.add(new Rectangle(168,6,35,35));
    sourceShapeList.add(new Rectangle(63,6,35,35));
    sourceShapeList.add(new Rectangle(10,5,36,36));
    templateShapeList.add(new Rectangle(105,-53,23,23));
    templateShapeList.add(new Rectangle(105,0,23,23));
    templateShapeList.add(new Rectangle(52,-53,23,23));
    templateShapeList.add(new Rectangle(52,0,23,23));
    templateShapeList.add(new Rectangle(52,52,23,23));
    templateShapeList.add(new Rectangle(0,-53,23,23));
    templateShapeList.add(new Rectangle(0,0,23,23));
    templateShapeList.add(new Rectangle(0,52,23,23));
    templateShapeList.add(new Rectangle(-53,-53,23,23));
    templateShapeList.add(new Rectangle(-53,0,23,23));
    templateShapeList.add(new Rectangle(-53,52,23,23));
    maxBound = new Rectangle(0,0,262,210);
    excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,75,262,67));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.RIGHT,new Rectangle(127,0,67,76));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(69/70):" + offsetList.get(0).getX());
    System.out.println("expected offset y(65):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");
    System.out.println("Fail 4:");
    //Fail 4 (expected : x:64.0  y:68.0/69.0)
    //clean
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(239,185,0,0));
    sourceShapeList.add(new Rectangle(188,181,0,0));
    sourceShapeList.add(new Rectangle(215,167,35,36));
    sourceShapeList.add(new Rectangle(163,167,35,36));
    sourceShapeList.add(new Rectangle(111,167,36,36));
    sourceShapeList.add(new Rectangle(121,137,0,0));
    sourceShapeList.add(new Rectangle(180,126,1,0));
    sourceShapeList.add(new Rectangle(129,124,1,0));
    sourceShapeList.add(new Rectangle(116,118,0,0));
    sourceShapeList.add(new Rectangle(217,114,34,37));
    sourceShapeList.add(new Rectangle(164,114,34,35));
    sourceShapeList.add(new Rectangle(111,114,35,36));
    sourceShapeList.add(new Rectangle(58,114,36,36));
    sourceShapeList.add(new Rectangle(188,80,0,1));
    sourceShapeList.add(new Rectangle(239,74,0,0));
    sourceShapeList.add(new Rectangle(164,62,35,34));
    sourceShapeList.add(new Rectangle(112,62,34,34));
    sourceShapeList.add(new Rectangle(59,62,34,34));
    sourceShapeList.add(new Rectangle(216,61,35,36));
    sourceShapeList.add(new Rectangle(236,33,0,0));
    sourceShapeList.add(new Rectangle(228,31,0,0));
    sourceShapeList.add(new Rectangle(71,29,0,0));
    sourceShapeList.add(new Rectangle(185,23,1,1));
    sourceShapeList.add(new Rectangle(238,22,0,0));
    sourceShapeList.add(new Rectangle(131,22,0,0));
    sourceShapeList.add(new Rectangle(111,10,35,34));
    sourceShapeList.add(new Rectangle(216,9,35,35));
    sourceShapeList.add(new Rectangle(164,9,35,35));
    sourceShapeList.add(new Rectangle(59,9,33,35));
    templateShapeList.add(new Rectangle(158,-53,23,23));
    templateShapeList.add(new Rectangle(158,0,23,23));
    templateShapeList.add(new Rectangle(158,52,23,23));
    templateShapeList.add(new Rectangle(158,105,23,23));
    templateShapeList.add(new Rectangle(105,-53,23,23));
    templateShapeList.add(new Rectangle(105,0,23,23));
    templateShapeList.add(new Rectangle(105,52,23,23));
    templateShapeList.add(new Rectangle(105,105,23,23));
    templateShapeList.add(new Rectangle(52,-53,23,23));
    templateShapeList.add(new Rectangle(52,0,23,23));
    templateShapeList.add(new Rectangle(52,52,23,23));
    templateShapeList.add(new Rectangle(52,105,23,23));
    templateShapeList.add(new Rectangle(0,-53,23,23));
    templateShapeList.add(new Rectangle(0,0,23,23));
    templateShapeList.add(new Rectangle(0,52,23,23));
    maxBound = new Rectangle(0,0,262,262);
    excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,127,262,67));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,128));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(64):" + offsetList.get(0).getX());
    System.out.println("expected offset y(68,69):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");               System.out.println("Fail 5:");
    //Fail 5 (expected : x:  y:)
    //clean
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(167,497,0,0));
    sourceShapeList.add(new Rectangle(171,465,1,0));
    sourceShapeList.add(new Rectangle(152,464,0,0));
    sourceShapeList.add(new Rectangle(176,441,0,0));
    sourceShapeList.add(new Rectangle(128,403,72,145));
    sourceShapeList.add(new Rectangle(103,242,126,77));
    sourceShapeList.add(new Rectangle(70,68,192,93));
    templateShapeList.add(new Rectangle(134,0,53,94));
    templateShapeList.add(new Rectangle(0,0,187,94));
    templateShapeList.add(new Rectangle(0,0,53,94));
    templateShapeList.add(new Rectangle(116,177,40,74));
    templateShapeList.add(new Rectangle(31,177,125,74));
    templateShapeList.add(new Rectangle(31,177,40,74));
    templateShapeList.add(new Rectangle(53,331,80,84));
    maxBound = new Rectangle(0,0,321,549);
    // excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,414,321,67));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,415));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.RIGHT,new Rectangle(186,0,67,415));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.TOP,new Rectangle(-67,-67,321,67));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(73):" + offsetList.get(0).getX());
    System.out.println("expected offset y(67):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");
    System.out.println("Pass 1:");
    //Pass 1
    //clean
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(132,244,0,0));
    sourceShapeList.add(new Rectangle(74,242,0,0));
    sourceShapeList.add(new Rectangle(192,237,0,0));
    sourceShapeList.add(new Rectangle(179,236,0,0));
    sourceShapeList.add(new Rectangle(129,233,0,0));
    sourceShapeList.add(new Rectangle(84,232,0,0));
    sourceShapeList.add(new Rectangle(169,221,34,34));
    sourceShapeList.add(new Rectangle(221,220,34,35));
    sourceShapeList.add(new Rectangle(116,220,34,34));
    sourceShapeList.add(new Rectangle(63,220,35,36));
    sourceShapeList.add(new Rectangle(81,191,0,0));
    sourceShapeList.add(new Rectangle(136,187,1,1));
    sourceShapeList.add(new Rectangle(134,179,1,0));
    sourceShapeList.add(new Rectangle(116,169,34,33));
    sourceShapeList.add(new Rectangle(221,168,34,35));
    sourceShapeList.add(new Rectangle(169,168,34,34));
    sourceShapeList.add(new Rectangle(63,168,34,35));
    sourceShapeList.add(new Rectangle(137,139,0,0));
    sourceShapeList.add(new Rectangle(85,129,1,1));
    sourceShapeList.add(new Rectangle(131,125,0,0));
    sourceShapeList.add(new Rectangle(169,116,34,35));
    sourceShapeList.add(new Rectangle(222,115,34,34));
    sourceShapeList.add(new Rectangle(116,115,34,37));
    sourceShapeList.add(new Rectangle(64,115,34,35));
    sourceShapeList.add(new Rectangle(133,88,1,0));
    sourceShapeList.add(new Rectangle(127,83,0,1));
    sourceShapeList.add(new Rectangle(188,81,0,0));
    sourceShapeList.add(new Rectangle(179,81,0,0));
    sourceShapeList.add(new Rectangle(233,79,0,1));
    sourceShapeList.add(new Rectangle(184,76,0,0));
    sourceShapeList.add(new Rectangle(221,63,35,34));
    sourceShapeList.add(new Rectangle(169,63,35,34));
    sourceShapeList.add(new Rectangle(117,62,35,35));

    templateShapeList.add(new Rectangle(158,0,23,23));
    templateShapeList.add(new Rectangle(158,52,23,23));
    templateShapeList.add(new Rectangle(158,105,23,23));
    templateShapeList.add(new Rectangle(158,158,23,23));
    templateShapeList.add(new Rectangle(105,0,23,23));
    templateShapeList.add(new Rectangle(105,52,23,23));
    templateShapeList.add(new Rectangle(105,105,23,23));
    templateShapeList.add(new Rectangle(105,158,23,23));
    templateShapeList.add(new Rectangle(52,0,23,23));
    templateShapeList.add(new Rectangle(52,52,23,23));
    templateShapeList.add(new Rectangle(52,105,23,23));
    templateShapeList.add(new Rectangle(52,158,23,23));
    templateShapeList.add(new Rectangle(0,52,23,23));
    templateShapeList.add(new Rectangle(0,105,23,23));
    templateShapeList.add(new Rectangle(0,158,23,23));
    maxBound = new Rectangle(0,0,262,262);
    excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,128));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.TOP,new Rectangle(-67,-67,262,67));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(70):" + offsetList.get(0).getX());
    System.out.println("expected offset y(69):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");

     */





    /*

    //clean
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(0,138,202,64));
    sourceShapeList.add(new Rectangle(173,120,21,22));
    sourceShapeList.add(new Rectangle(120,120,22,22));
    sourceShapeList.add(new Rectangle(198,86,4,37));
    sourceShapeList.add(new Rectangle(173,67,21,22));
    sourceShapeList.add(new Rectangle(120,67,22,22));
    sourceShapeList.add(new Rectangle(67,67,22,22));
    sourceShapeList.add(new Rectangle(198,33,4,38));
    sourceShapeList.add(new Rectangle(173,15,21,21));
    sourceShapeList.add(new Rectangle(120,15,22,21));
    sourceShapeList.add(new Rectangle(67,15,22,21));
    sourceShapeList.add(new Rectangle(198,0,4,18));
    templateShapeList.add(new Rectangle(105,-53,23,23));
    templateShapeList.add(new Rectangle(105,0,23,23));
    templateShapeList.add(new Rectangle(105,52,23,23));
    templateShapeList.add(new Rectangle(52,-53,23,23));
    templateShapeList.add(new Rectangle(52,0,23,23));
    templateShapeList.add(new Rectangle(52,52,23,23));
    templateShapeList.add(new Rectangle(0,-53,23,23));
    templateShapeList.add(new Rectangle(0,0,23,23));
    maxBound = new Rectangle(0,0,210,210);
    excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,75,210,67));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,76));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    /* offsetList.add(new ImageCoordinate(100,100));
    System.out.println("offsetList:" + offsetList);
    System.out.println("expected offset x(73):" + offsetList.get(0).getX());
    System.out.println("expected offset y(67):" + offsetList.get(0).getY());
    BufferedImage img = null;
    try
    {
    img = ImageIO.read(new File("C:\\Program Files\\AXI System\\v810\\temp\\onlineXrayImages\\binaryimage_top_6781_11471_156_156.png"));
    }
    catch (IOException e)
    {
    }
    com.axi.util.image.Image binaryImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(img);
    Pair<Double,ImageCoordinate> high = getInstance().findHighestMatchedPosition(templateShapeList,excludedAreaMap,offsetList,binaryImage);
    System.out.println("high match: " + high.getSecond());
    System.out.println("expected offset x(73):" + offsetList.get(0).getX());
    System.out.println("expected offset y(67):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");*/

    /*
    System.out.println("Fail 7:");
    //Fail 5 (expected : x:  y:)
    //clean
    getInstance().reset();
    sourceShapeList.clear();
    templateShapeList.clear();
    excludedAreaMap.clear();
    offsetList.clear();
    sourceShapeList.add(new Rectangle(198,302,0,0));
    sourceShapeList.add(new Rectangle(213,286,0,0));
    sourceShapeList.add(new Rectangle(198,283,0,1));
    sourceShapeList.add(new Rectangle(0,272,36,30));
    sourceShapeList.add(new Rectangle(127,271,44,31));
    sourceShapeList.add(new Rectangle(61,271,42,31));
    sourceShapeList.add(new Rectangle(213,267,0,0));
    sourceShapeList.add(new Rectangle(197,264,0,1));
    sourceShapeList.add(new Rectangle(196,244,0,2));
    sourceShapeList.add(new Rectangle(214,243,0,1));
    sourceShapeList.add(new Rectangle(198,243,0,4));
    sourceShapeList.add(new Rectangle(201,227,0,2));
    sourceShapeList.add(new Rectangle(213,226,0,0));
    sourceShapeList.add(new Rectangle(213,221,0,3));
    sourceShapeList.add(new Rectangle(127,205,44,42));
    sourceShapeList.add(new Rectangle(60,204,44,43));
    sourceShapeList.add(new Rectangle(0,204,37,43));
    sourceShapeList.add(new Rectangle(192,184,0,0));
    sourceShapeList.add(new Rectangle(194,182,0,4));
    sourceShapeList.add(new Rectangle(196,178,0,1));
    sourceShapeList.add(new Rectangle(201,159,0,1));
    sourceShapeList.add(new Rectangle(126,137,45,43));
    sourceShapeList.add(new Rectangle(60,137,44,43));
    sourceShapeList.add(new Rectangle(0,137,37,42));
    sourceShapeList.add(new Rectangle(197,110,0,3));
    sourceShapeList.add(new Rectangle(201,103,0,0));
    sourceShapeList.add(new Rectangle(196,96,19,206));
    sourceShapeList.add(new Rectangle(210,90,1,4));
    sourceShapeList.add(new Rectangle(207,85,0,0));
    sourceShapeList.add(new Rectangle(209,84,3,3));
    sourceShapeList.add(new Rectangle(206,78,0,0));
    sourceShapeList.add(new Rectangle(127,70,44,43));
    sourceShapeList.add(new Rectangle(0,70,37,43));
    sourceShapeList.add(new Rectangle(60,69,43,44));
    sourceShapeList.add(new Rectangle(204,66,0,0));
    sourceShapeList.add(new Rectangle(203,58,0,1));
    sourceShapeList.add(new Rectangle(0,12,227,70));



    templateShapeList.add(new Rectangle(67,0,35,35));
    templateShapeList.add(new Rectangle(0,0,35,35));
    templateShapeList.add(new Rectangle(-67,0,35,35));
    templateShapeList.add(new Rectangle(67,67,35,35));
    templateShapeList.add(new Rectangle(0,67,35,35));
    templateShapeList.add(new Rectangle(-67,67,35,35));
    templateShapeList.add(new Rectangle(67,134,35,35));
    templateShapeList.add(new Rectangle(0,134,35,35));
    templateShapeList.add(new Rectangle(-67,134,35,35));
    templateShapeList.add(new Rectangle(67,200,35,35));
    templateShapeList.add(new Rectangle(0,200,35,35));
    templateShapeList.add(new Rectangle(-67,200,35,35));
    maxBound = new Rectangle(0,0,236,303);
    excludedAreaMap.put(AlignmentExcludedAreaEnum.RIGHT,new Rectangle(101,0,67,169));
    excludedAreaMap.put(AlignmentExcludedAreaEnum.TOP,new Rectangle(-67,-67,235,67));
    offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("expected offset x(67):" + offsetList.get(0).getX());
    System.out.println("expected offset y(68):" + offsetList.get(0).getY());
    System.out.println("");
    System.out.println("--------------------------------------------------------------------------------------");
     */

  getInstance().reset();
sourceShapeList.clear();
templateShapeList.clear();
excludedAreaMap.clear();
offsetList.clear();
//Souvala r:16.5sw:33.0
sourceShapeList.add(new Rectangle(337,273,35,28));
sourceShapeList.add(new Rectangle(203,273,35,28));
sourceShapeList.add(new Rectangle(403,272,31,29));
sourceShapeList.add(new Rectangle(269,272,36,29));
sourceShapeList.add(new Rectangle(135,272,36,29));
sourceShapeList.add(new Rectangle(68,272,36,29));
sourceShapeList.add(new Rectangle(403,205,31,34));
sourceShapeList.add(new Rectangle(337,205,35,34));
sourceShapeList.add(new Rectangle(269,205,35,34));
sourceShapeList.add(new Rectangle(202,205,36,34));
sourceShapeList.add(new Rectangle(136,204,35,35));
sourceShapeList.add(new Rectangle(66,204,37,35));
sourceShapeList.add(new Rectangle(403,138,31,35));
sourceShapeList.add(new Rectangle(336,138,36,35));
sourceShapeList.add(new Rectangle(269,138,36,35));
sourceShapeList.add(new Rectangle(202,138,36,34));
sourceShapeList.add(new Rectangle(135,137,36,35));
sourceShapeList.add(new Rectangle(66,136,37,36));
sourceShapeList.add(new Rectangle(402,70,32,35));
sourceShapeList.add(new Rectangle(336,69,36,36));
sourceShapeList.add(new Rectangle(269,69,36,35));
sourceShapeList.add(new Rectangle(202,69,36,35));
sourceShapeList.add(new Rectangle(134,68,37,37));
sourceShapeList.add(new Rectangle(65,67,38,36));
sourceShapeList.add(new Rectangle(252,63,2,0));
sourceShapeList.add(new Rectangle(184,63,0,0));
sourceShapeList.add(new Rectangle(313,62,3,1));
sourceShapeList.add(new Rectangle(38,61,0,0));
sourceShapeList.add(new Rectangle(342,60,1,0));
sourceShapeList.add(new Rectangle(233,60,1,0));
sourceShapeList.add(new Rectangle(400,59,2,1));
sourceShapeList.add(new Rectangle(370,59,2,1));
sourceShapeList.add(new Rectangle(335,59,3,2));
sourceShapeList.add(new Rectangle(270,58,5,1));
sourceShapeList.add(new Rectangle(234,58,1,0));
sourceShapeList.add(new Rectangle(169,58,15,3));
sourceShapeList.add(new Rectangle(127,58,8,2));
sourceShapeList.add(new Rectangle(422,57,1,0));
sourceShapeList.add(new Rectangle(393,57,0,0));
sourceShapeList.add(new Rectangle(319,57,9,6));
sourceShapeList.add(new Rectangle(302,57,12,5));
sourceShapeList.add(new Rectangle(238,57,16,4));
sourceShapeList.add(new Rectangle(188,57,21,4));
sourceShapeList.add(new Rectangle(100,57,9,2));
sourceShapeList.add(new Rectangle(370,56,1,0));
sourceShapeList.add(new Rectangle(339,56,2,0));
sourceShapeList.add(new Rectangle(318,56,2,2));
sourceShapeList.add(new Rectangle(302,56,2,0));
sourceShapeList.add(new Rectangle(257,56,12,6));
sourceShapeList.add(new Rectangle(247,56,0,0));
sourceShapeList.add(new Rectangle(187,56,2,0));
sourceShapeList.add(new Rectangle(109,56,17,7));
sourceShapeList.add(new Rectangle(70,55,0,0));
sourceShapeList.add(new Rectangle(388,54,2,3));
sourceShapeList.add(new Rectangle(376,54,18,8));
sourceShapeList.add(new Rectangle(272,52,1,0));
sourceShapeList.add(new Rectangle(57,52,17,14));


templateShapeList.add(new Rectangle(0,200,33,33));
templateShapeList.add(new Rectangle(0,134,33,33));
templateShapeList.add(new Rectangle(0,67,33,33));
templateShapeList.add(new Rectangle(0,0,33,33));
templateShapeList.add(new Rectangle(67,200,33,33));
templateShapeList.add(new Rectangle(67,134,33,33));
templateShapeList.add(new Rectangle(67,67,33,33));
templateShapeList.add(new Rectangle(67,0,33,33));
templateShapeList.add(new Rectangle(134,200,33,33));
templateShapeList.add(new Rectangle(134,134,33,33));
templateShapeList.add(new Rectangle(134,67,33,33));
templateShapeList.add(new Rectangle(134,0,33,33));
templateShapeList.add(new Rectangle(200,200,33,33));
templateShapeList.add(new Rectangle(200,134,33,33));
templateShapeList.add(new Rectangle(200,67,33,33));
templateShapeList.add(new Rectangle(200,0,33,33));
templateShapeList.add(new Rectangle(267,200,33,33));
templateShapeList.add(new Rectangle(267,134,33,33));
templateShapeList.add(new Rectangle(267,67,33,33));
templateShapeList.add(new Rectangle(267,0,33,33));
templateShapeList.add(new Rectangle(334,200,33,33));
templateShapeList.add(new Rectangle(334,134,33,33));
templateShapeList.add(new Rectangle(334,67,33,33));
templateShapeList.add(new Rectangle(334,0,33,33));
maxBound = new Rectangle(0,0,435,301);
excludedAreaMap.put(AlignmentExcludedAreaEnum.BOTTOM,new Rectangle(-67,166,434,67));
excludedAreaMap.put(AlignmentExcludedAreaEnum.RIGHT,new Rectangle(300,0,67,167));
excludedAreaMap.put(AlignmentExcludedAreaEnum.TOP,new Rectangle(-67,-67,434,67));
excludedAreaMap.put(AlignmentExcludedAreaEnum.LEFT,new Rectangle(-67,0,67,167));
offsetList = getInstance().testMatchTemplateByShapeList(templateShapeList,sourceShapeList,maxBound,excludedAreaMap);
    System.out.println("offsetList:"+offsetList);
System.out.println("expected offset x(0):" + offsetList.get(0).getX());
System.out.println("expected offset y(0):" + offsetList.get(0).getY());
System.out.println("");
System.out.println("--------------------------------------------------------------------------------------");
    BufferedImage img = null;
    try
    {
    img = ImageIO.read(new File("C:\\Program Files\\AXI System\\v810\\temp\\onlineXrayImages\\1332998539302_binaryimage_.png"));
    }
    catch (IOException e)
    {
    }
    com.axi.util.image.Image binaryImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(img);
    Pair<Double,ImageCoordinate> high = getInstance().findHighestMatchedPosition(templateShapeList,excludedAreaMap,offsetList,binaryImage);
    System.out.println("high match: " + high.getSecond());
  }
}
