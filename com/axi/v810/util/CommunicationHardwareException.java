package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * This class gets thrown when a line of communication has failed.
 * For example, between the host pc and the x-ray source which uses
 * a USB connection.
 * @author George A. David
 */
public class CommunicationHardwareException extends HardwareException
{
  /**
   * @author George A. David
   */
  public CommunicationHardwareException(String key, List parameters)
  {
    super(getLocalizedString(key, parameters));
  }

  /**
   * @author George A. David
   */
  private static LocalizedString getLocalizedString(String key, List parameters)
  {
    Assert.expect(key != null);
    Assert.expect(parameters != null);

    return new LocalizedString(key, parameters.toArray());
  }
}
