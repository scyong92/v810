package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * Given a list of components it will determine the surrounding components of any component you pass in.
 * @author George A. David
 */
public class SurroundingComponentUtil
{
  private Map<Integer, Set<Component>> _xCoordToComponentSetMap = new TreeMap<Integer, Set<Component>>();
  private Map<Integer, Set<Component>> _yCoordToComponentSetMap = new TreeMap<Integer, Set<Component>>();
  private List<Integer> _xCoords;
  private List<Integer> _yCoords;
  private Set<Component> _topComponents = new HashSet<Component>();
  private Set<Component> _bottomComponents = new HashSet<Component>();
  private int _largestWidthInNanos = -1;
  private int _largestLengthInNanos = -1;

  /**
   * @author George A. David
   */
  public void setComponents(Collection<Component> components)
  {
    Assert.expect(components != null);

    _largestWidthInNanos = -1;
    _largestLengthInNanos = -1;
    _xCoordToComponentSetMap.clear();
    _yCoordToComponentSetMap.clear();
    _topComponents.clear();
    _bottomComponents.clear();

    for(Component component : components)
    {
      //Siew Yeng 
      //XCR1785 - System crash when zoom in verification image
      //if(component.isLoaded())
        addComponent(component);
    }

    _xCoords = new ArrayList<Integer>(_xCoordToComponentSetMap.keySet());
    _yCoords = new ArrayList<Integer>(_yCoordToComponentSetMap.keySet());
  }

  /**
   * @author George A. David
   */
  private void addComponent(Component component)
  {
    PanelRectangle rect = new PanelRectangle(component.getShapeSurroundingPadsRelativeToPanelInNanoMeters());
    _largestWidthInNanos = Math.max(_largestWidthInNanos, rect.getWidth());
    _largestLengthInNanos = Math.max(_largestLengthInNanos, rect.getHeight());
    addComponent(component, rect.getMinX(), _xCoordToComponentSetMap);
    addComponent(component, rect.getMaxX(), _xCoordToComponentSetMap);
    addComponent(component, rect.getMinY(), _yCoordToComponentSetMap);
    addComponent(component, rect.getMaxY(), _yCoordToComponentSetMap);
    if(component.isTopSide())
      _topComponents.add(component);
    else
      _bottomComponents.add(component);
  }

  /**
   * @author George A. David
   */
  private void addComponent(Component component, int coordValue,  Map<Integer, Set<Component>> coordToComponentSetMap)
  {
    Assert.expect(component != null);
    Assert.expect(coordToComponentSetMap != null);

    if(coordToComponentSetMap.containsKey(coordValue))
    {
      coordToComponentSetMap.get(coordValue).add(component);
    }
    else
    {
      Set<Component> componentSet = new HashSet<Component>();
      componentSet.add(component);
      coordToComponentSetMap.put(coordValue, componentSet);
    }
  }

  /**
   * @author George A. David
   */
  private void removeComponent(Component component,  Map<Integer, Set<Component>> coordToComponentSetMap)
  {
    Assert.expect(component != null);
    Assert.expect(coordToComponentSetMap != null);

    for(Set<Component> components : coordToComponentSetMap.values())
      components.remove(component);
  }


  /**
   * @author George A. David
   */
  public void updateComponent(Component component)
  {
    // find where the component is currently, and remove it
    removeComponent(component, _xCoordToComponentSetMap);
    removeComponent(component, _yCoordToComponentSetMap);

    addComponent(component);
    _xCoords = new ArrayList<Integer>(_xCoordToComponentSetMap.keySet());
    _yCoords = new ArrayList<Integer>(_yCoordToComponentSetMap.keySet());
  }

  /**
   * @author George A. David
   */
  private Set<Component> getSurroundingComponents(int coordValue, List<Integer> coords, Map<Integer, Set<Component>> coordToComponentSetMap)
  {
    Assert.expect(coordToComponentSetMap != null);

    Pair<Integer, Integer> pair = ListUtil.findIndiciesOfSurroundingValues(coords, coordValue);
    int previousIndex = pair.getFirst();
    int nextIndex = pair.getSecond();

    // ok, now because we are using the extents, we need to find the components
    // whose extents cross this point
    Set<Component> beginComponents = new HashSet<Component>();
    for (int i = 0; i <= previousIndex; ++i)
      beginComponents.addAll(coordToComponentSetMap.get(coords.get(i)));

    Set<Component> endComponents = new HashSet<Component>();

    for (int i = nextIndex; i < coords.size(); ++i)
      endComponents.addAll(coordToComponentSetMap.get(coords.get(i)));

    beginComponents.retainAll(endComponents);
    return beginComponents;
  }

  /**
   * @author George A. David
   */
  private Collection<Component> getSurroundingComponents(Component component)
  {
    Assert.expect(component != null);

    PanelCoordinate coord = component.getCenterCoordinateRelativeToPanelInNanoMeters();
    Set<Component> components = getSurroundingComponents(coord.getX(), _xCoords, _xCoordToComponentSetMap);
    components.retainAll(getSurroundingComponents(coord.getY(), _yCoords, _yCoordToComponentSetMap));

    return components;
  }

  /**
   * @author George A. David
   */
  public Collection<Component> getSurroundingComponents(int xCoord, int yCoord, boolean isTopSide)
  {
    Set<Component> components = getSurroundingComponents(xCoord, _xCoords, _xCoordToComponentSetMap);
    components.retainAll(getSurroundingComponents(yCoord, _yCoords, _yCoordToComponentSetMap));

    if(isTopSide)
      components.retainAll(_topComponents);
    else
      components.retainAll(_bottomComponents);

    return components;
  }

  /**
   * @author George A. David
   */
  public Collection<Component> getComponentsAtXcoordinate(int xCoordinate)
  {
    Set<Component> components = new HashSet<Component>();
    components.addAll(_xCoordToComponentSetMap.get(xCoordinate));

    return components;
  }

  /**
   * @author George A. David
   */
  public Collection<Component> getComponentsAtYcoordinate(int yCoordinate)
  {
    Set<Component> components = new HashSet<Component>();
    components.addAll(_yCoordToComponentSetMap.get(yCoordinate));

    return components;
  }

  /**
   * @author George A. David
   */
  public void printDebugInfo()
  {
    System.out.println("x map");
    for(Integer integer : _xCoordToComponentSetMap.keySet())
    {
      System.out.println("xCoord: " + integer);
      System.out.print("Components: ");
      for(Component component : _xCoordToComponentSetMap.get(integer))
      {
        System.out.print(component.getReferenceDesignator() + " ");
      }
      System.out.println();
    }

    System.out.println("y map");
    for(Integer integer : _yCoordToComponentSetMap.keySet())
    {
      System.out.println("yCoord: " + integer);
      System.out.print("Components: ");
      for(Component component : _yCoordToComponentSetMap.get(integer))
      {
        System.out.print(component.getReferenceDesignator() + " ");
      }
      System.out.println();
    }
  }

  /**
   * @author George A. David
   */
  public Set<Component> getIntersectingComponentsInRectangle(PanelRectangle rectangle, boolean isTopSide)
  {
    Set<Component> components = getIntersectingComponentsInRectangle(rectangle);
    if(isTopSide)
      components.retainAll(_topComponents);
    else
      components.retainAll(_bottomComponents);

    return components;
  }

  /**
   * @author George A. David
   */
  public Set<Component> getIntersectingComponentsInRectangle(PanelRectangle rectangle)
  {
    Set<Component> reconstructionRegions = getIntersectingComponentsBetweenValues(rectangle.getMinX(),
                                                                      rectangle.getMaxX(),
                                                                      _xCoords,
                                                                      _xCoordToComponentSetMap,
                                                                      _largestWidthInNanos);
    reconstructionRegions.retainAll(getIntersectingComponentsBetweenValues(rectangle.getMinY(),
                                                               rectangle.getMaxY(),
                                                               _yCoords,
                                                               _yCoordToComponentSetMap,
                                                               _largestLengthInNanos));
    return reconstructionRegions;
  }

  /**
   * @author George A. David
   */
  private Set<Component> getIntersectingComponentsBetweenValues(int minValue,
                                                                int maxValue,
                                                                List<Integer> coords,
                                                                Map<Integer, Set<Component>> coordToComponentSetMap,
                                                                int largestDimension)
  {
    Assert.expect(coordToComponentSetMap != null);
    Assert.expect(maxValue >= minValue);

    Set<Component> components = new HashSet<Component>();

    int beginIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, minValue);
    int endIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, maxValue);

    if(beginIndex < 0)
      beginIndex = 0;

    if (endIndex > coords.size() - 1)
      endIndex = coords.size() - 1;

    for(int i = beginIndex; i <= endIndex; ++i)
    {
      int coord = coords.get(i);
      components.addAll(coordToComponentSetMap.get(coord));
    }

    if(beginIndex > 0 && endIndex < coords.size() - 1)
    {
      // ok, now because we are using the extents, we may have a
      // component whose extents are beyond this rectangle, but we still want
      // to report it. so let's check for any components that cross this intersection.

      // we don't want to always start with 0 index, this slows us down too much
      // let's be smart about this, we know the largest component dimension, so
      // let's use that to help us limit the search.
      int smallestStartingCoordinate = maxValue - largestDimension;
      int smallestIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, smallestStartingCoordinate);
      Set<Component> beginComponents = new HashSet<Component>();
      for (int i = smallestIndex; i < beginIndex; ++i)
        beginComponents.addAll(coordToComponentSetMap.get(coords.get(i)));

      int largestEndingCoordinate = minValue + largestDimension;
      int largestIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, largestEndingCoordinate);
      Set<Component> endComponents = new HashSet<Component>();
      for (int i = endIndex + 1; i <= largestIndex; ++i)
        endComponents.addAll(coordToComponentSetMap.get(coords.get(i)));

      beginComponents.retainAll(endComponents);
      components.addAll(beginComponents);
    }

    return components;
  }
}
