package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * Given two reconstruction regions, determine which one is closer
 * to the reference point.
 * @author George A. David
 */
public class ReconstructionRegionDistanceComparator implements Comparator<ReconstructionRegion>
{
  private double _xReferenceNanoMeters;
  private double  _yReferenceNanoMeters;
  private boolean _useSurfaceModelingFocusRegionForDistanceComparison = false;

//  public static boolean _printDebugInfo = false;

  /**
   * @author George A. David
   */
  public void setUseSurfaceModelingFocusRegionForDistanceComparison(boolean useSurfaceModelingFocusRegionForDistanceComparison)
  {
    _useSurfaceModelingFocusRegionForDistanceComparison = useSurfaceModelingFocusRegionForDistanceComparison;
  }

  /**
   * @author George A. David
   */
  public ReconstructionRegionDistanceComparator(double xReferenceNanoMeters, double yReferenceNanoMeters)
  {
    _xReferenceNanoMeters = xReferenceNanoMeters;
    _yReferenceNanoMeters = yReferenceNanoMeters;
  }

  /**
   * Compares its two arguments for order.
   *
   * @param lhs the first object to be compared.
   * @param rhs the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the first
   *   argument is less than, equal to, or greater than the second.
   *
   * @author George A. David
   */
  public int compare(ReconstructionRegion lhs, ReconstructionRegion rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    // they're the same exact object!!!
    if(lhs == rhs)
      return 0;

    int lhsX;
    int lhsY;
    int rhsX;
    int rhsY;

    if(_useSurfaceModelingFocusRegionForDistanceComparison == false)
    {
      PanelRectangle regionRect = lhs.getRegionRectangleRelativeToPanelInNanoMeters();
      lhsX = regionRect.getCenterX();
      lhsY = regionRect.getCenterY();

      regionRect = rhs.getRegionRectangleRelativeToPanelInNanoMeters();
      rhsX = regionRect.getCenterX();
      rhsY = regionRect.getCenterY();
    }
    else
    {
      PanelRectangle regionRect = lhs.getSurfaceModelingFocusRegion().getRectangleRelativeToPanelInNanometers();
      lhsX = regionRect.getCenterX();
      lhsY = regionRect.getCenterY();

      regionRect = rhs.getSurfaceModelingFocusRegion().getRectangleRelativeToPanelInNanometers();
      rhsX = regionRect.getCenterX();
      rhsY = regionRect.getCenterY();
    }

    double firstDistanceSquared = MathUtil.calculateDistanceSquared(lhsX, lhsY,
                                                                    _xReferenceNanoMeters, _yReferenceNanoMeters);
    double secondDistanceSquared = MathUtil.calculateDistanceSquared(rhsX, rhsY,
                                                                    _xReferenceNanoMeters, _yReferenceNanoMeters);
//    if(_printDebugInfo)
//    {
//      System.out.println("lhs reconstructionRegion: " + lhs.getName());
//      System.out.println("lhs distance: " + firstDistanceSquared);
//      System.out.println("lhs coord: " + firstCoord.getX() + " " + firstCoord.getY());
//      System.out.println("rhs reconstructionRegion: " + rhs.getName());
//      System.out.println("rhs distance: " + secondDistanceSquared);
//      System.out.println("rhs coord: " + secondCoord.getX() + " " + secondCoord.getY());
//    }

    if (firstDistanceSquared > secondDistanceSquared)
      return 1;
    else if(firstDistanceSquared < secondDistanceSquared)
      return -1;
    else
    {
      // ok, they are equal, but if we have to items in an unsorted list
      // and both are equal, then the first item in the list gets priority
      // I don' t want to leave it up to chance, so I'll try on last thing,
      // first look at x then y. The one with less x, then less y if necessary is first,
      // otherwise, just return 0.
      if(lhsX < rhsX)
        return -1;
     else if(lhsX > rhsX)
       return 1;
     else if(lhsY < rhsY)
       return -1;
     else if(lhsY > rhsY)
       return 1;
     else
     {
       // they are on the same exact spot!!!
       // lets sort it by region ID
       if(lhs.getRegionId() < rhs.getRegionId())
         return -1;
       if(lhs.getRegionId() > rhs.getRegionId())
         return 1;
       else // this cannot be two reconstructin regions cannot have the same region id
       {
         Assert.expect(false);
         return 0;
       }
     }
//     if(firstCoord.getX() < secondCoord.getX())
//       return -1;
//     else if(firstCoord.getX() > secondCoord.getX())
//       return 1;
//     else if(firstCoord.getY() < secondCoord.getY())
//       return -1;
//     else if(firstCoord.getY() > secondCoord.getY())
//       return 1;
//     else // they are on the same exact spot!!!
//       return 0;
    }
  }
}
