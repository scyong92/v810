package com.axi.v810.util;

import java.util.regex.*;

import com.axi.util.*;

/** 
 * This util class handles V810 versioning operation. It includes how to 
 * get software major/minor version and version compare.
 * 
 * All version-related operations should only be called from this class.
 * 
 * @author Cheah Lee Herng
 */
public class VersionUtil
{
  /**
   * @author Cheah Lee Herng 
   */
  public static int getMajorVersion(String version)
  {
    Assert.expect(version != null);
    
    Pattern majorVersionPattern = Pattern.compile("^(\\d+)([\\.]+)\\d+$");
    Matcher matcher = majorVersionPattern.matcher(version);
    if (matcher.matches())
    {
      return Integer.parseInt(matcher.group(1));
    }
    else
      return Version.getMajorVersion();
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static int getMinorVersion(String version)
  {
    Assert.expect(version != null);
    
    Pattern majorVersionPattern = Pattern.compile("^\\d+([\\.]+)(\\d+)$");
    Matcher matcher = majorVersionPattern.matcher(version);
    if (matcher.matches())
    {
      return Integer.parseInt(matcher.group(2));
    }
    else
      return Version.getMinorVersion();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static boolean isRequiredVersion(String versionToCompare)
  {
    Assert.expect(versionToCompare != null);
    
    int currentMajorVersion = getMajorVersion(Version.getVersionNumber());
    int currentMinorVersion = getMinorVersion(Version.getVersionNumber());
        
    int compareMajorVersion = getMajorVersion(versionToCompare);
    int compareMinorVersion = getMinorVersion(versionToCompare);
    
    if ((currentMajorVersion == compareMajorVersion) && (currentMinorVersion == compareMinorVersion))
      return true;
    else
      return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static boolean isRequiredVersion(String versionToCompare, String expectedVersion)
  {
    Assert.expect(versionToCompare != null);
    Assert.expect(expectedVersion != null);
    
    int currentMajorVersion = getMajorVersion(expectedVersion);
    int currentMinorVersion = getMinorVersion(expectedVersion);
        
    int compareMajorVersion = getMajorVersion(versionToCompare);
    int compareMinorVersion = getMinorVersion(versionToCompare);
    
    if ((currentMajorVersion == compareMajorVersion) && (currentMinorVersion == compareMinorVersion))
      return true;
    else
      return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static boolean isNewerVersion(String versionToCompare)
  {
    Assert.expect(versionToCompare != null);
    
    int currentMajorVersion = getMajorVersion(Version.getVersionNumber());
    int currentMinorVersion = getMinorVersion(Version.getVersionNumber());
        
    int compareMajorVersion = getMajorVersion(versionToCompare);
    int compareMinorVersion = getMinorVersion(versionToCompare);
    
    if ((compareMajorVersion > currentMajorVersion) || 
        ((compareMajorVersion == currentMajorVersion) && (compareMinorVersion > currentMinorVersion)))
    {
      return true;
    }
    else
      return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static boolean isNewerVersion(String versionToCompare, String expectedVersion)
  {
    Assert.expect(versionToCompare != null);
    Assert.expect(expectedVersion != null);
    
    int currentMajorVersion = getMajorVersion(expectedVersion);
    int currentMinorVersion = getMinorVersion(expectedVersion);
        
    int compareMajorVersion = getMajorVersion(versionToCompare);
    int compareMinorVersion = getMinorVersion(versionToCompare);
    
    if ((compareMajorVersion > currentMajorVersion) || 
        ((compareMajorVersion == currentMajorVersion) && (compareMinorVersion > currentMinorVersion)))
    {
      return true;
    }
    else
      return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static boolean isOlderVersion(String versionToCompare)
  {
    Assert.expect(versionToCompare != null);
    
    int currentMajorVersion = getMajorVersion(Version.getVersionNumber());
    int currentMinorVersion = getMinorVersion(Version.getVersionNumber());
        
    int compareMajorVersion = getMajorVersion(versionToCompare);
    int compareMinorVersion = getMinorVersion(versionToCompare);
    
    if ((compareMajorVersion < currentMajorVersion) || 
        ((compareMajorVersion == currentMajorVersion) && (compareMinorVersion < currentMinorVersion)))
    {
      return true;
    }
    else
      return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public static boolean isOlderVersion(String versionToCompare, String expectedVersion)
  {
    Assert.expect(versionToCompare != null);
    Assert.expect(expectedVersion != null);
    
    int currentMajorVersion = getMajorVersion(expectedVersion);
    int currentMinorVersion = getMinorVersion(expectedVersion);
        
    int compareMajorVersion = getMajorVersion(versionToCompare);
    int compareMinorVersion = getMinorVersion(versionToCompare);
    
    if ((compareMajorVersion < currentMajorVersion) || 
        ((compareMajorVersion == currentMajorVersion) && (compareMinorVersion < currentMinorVersion)))
    {
      return true;
    }
    else
      return false;
  }
}
