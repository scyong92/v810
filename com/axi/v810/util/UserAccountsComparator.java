package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;

/**
 *
 * <p>Title: UserAccountsComparator</p>
 *
 * <p>Description: This comparator will sort an UserAccountsInfo object
 * by the desired field name</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class UserAccountsComparator implements Comparator<UserAccountInfo>
{
  private boolean _ascending;
  private UserAccountsComparatorEnum _field;

  private AlphaNumericComparator _alphaNumericComparator = null;

  /**
   * @author George Booth
   */
  public UserAccountsComparator(boolean ascending, UserAccountsComparatorEnum field)
  {
    _ascending = ascending;
    _field = field;
    _alphaNumericComparator = new AlphaNumericComparator(ascending);
  }

  /**
   * @param lhInfo lefthand UserAccountsInfo
   * @param rhInfo righthand UserAccountsInfo
   * @return negative if lf less than rh, zero if equal, positive if greater than
   * @author George Booth
   */
  public int compare(UserAccountInfo lhInfo, UserAccountInfo rhInfo)
  {
    Assert.expect(lhInfo != null);
    Assert.expect(rhInfo != null);

    String lhName = null;
    String rhName = null;

    if (_field.equals(UserAccountsComparatorEnum.ACCOUNT_NAME))
    {
      lhName = lhInfo.getAccountName();
      rhName = rhInfo.getAccountName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(UserAccountsComparatorEnum.FULL_NAME))
    {
      lhName = lhInfo.getFullName();
      rhName = rhInfo.getFullName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else if (_field.equals(UserAccountsComparatorEnum.ACCOUNT_TYPE))
    {
      lhName = lhInfo.getAccountType().getName();
      rhName = rhInfo.getAccountType().getName();
      return _alphaNumericComparator.compare(lhName, rhName);
    }
    else
    {
      Assert.expect(false, "unsupported comparator field enum");
      return 0;
    }
  }

}
