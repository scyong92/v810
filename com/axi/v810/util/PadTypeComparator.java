package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class PadTypeComparator implements Comparator<PadType>
{
  private AlphaNumericComparator _alphaComparator;

  /**
   * @author Bill Darbie
   */
  public PadTypeComparator()
  {
    _alphaComparator = new AlphaNumericComparator();
  }

  /**
   * @author Bill Darbie
   */
  public int compare(PadType lhs, PadType rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsName = lhs.getBoardAndComponentAndPadName();
    String rhsName = rhs.getBoardAndComponentAndPadName();

    return _alphaComparator.compare(lhsName, rhsName);
  }
}
