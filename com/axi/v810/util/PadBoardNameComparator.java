package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This comparator will sort a PadType object on either the family used to inpsect it
 * or the subtype assigned to it.
 * @author Andy Mechtenberg
 */
public class PadBoardNameComparator implements Comparator<Pad>
{
  private boolean _ascending;
  private boolean _sortOnJointType;
  private boolean _sortOnBoardName;

  // for subtype compares, these are the types of values
  private static final int _UNTESTABLE = 0;
  private static final int _NO_TEST = 1;
  private static final int _SUBTYPE = 2;

  /**
   * @author Andy Mechtenberg
   * @todo EEW we really need to clean this up. THis should have an enum instead of all these booleans.
   */
  public PadBoardNameComparator(boolean ascending,
                                boolean sortOnJointType,
                                boolean sortOnBoardName                             )
  {
    _ascending = ascending;
    _sortOnJointType = sortOnJointType;
    _sortOnBoardName = sortOnBoardName;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int compare(Pad lhPad, Pad rhPad)
  {
    Assert.expect(lhPad != null);
    Assert.expect(rhPad != null);

    String lhName = "";
    String rhName = "";

    if (_sortOnJointType)
    {
      lhName = lhPad.getSubtype().getJointTypeEnum().getName();
      rhName = rhPad.getSubtype().getJointTypeEnum().getName();
    }
    else if (_sortOnJointType == false && _sortOnBoardName == false)
    {
      lhName = lhPad.getSubtype().getShortName();
      PadTypeSettings lhPadSettings = lhPad.getPadTypeSettings();

      rhName = rhPad.getSubtype().getShortName();
      PadTypeSettings rhPadSettings = rhPad.getPadTypeSettings();

      int lhState = _SUBTYPE;
      int rhState = _SUBTYPE;

      // sort on subtype.  This can be one of 2 different types for pads:
      // 1.  Untestable
      // 2.  No Test
      // 3.  An actual subtype name
      // The behavior we want is for the ascending order to be as listed above.  Group together the No Tests, sort the subtype names alphabetically etc.

      //if (lhPadSettings.isTestable() == false)
      if (lhPad.getPadType().getComponentType().isTestable() == false)
        lhState = _UNTESTABLE;
      else if(lhPadSettings.isInspected() == false)
        lhState = _NO_TEST;
      else
      {
        lhState = _SUBTYPE;
        lhName = lhPad.getSubtype().getShortName();
      }

      //if (rhPadSettings.isTestable() == false)
      if (rhPad.getPadType().getComponentType().isTestable() == false)
        rhState = _UNTESTABLE;
      else if(rhPadSettings.isInspected() == false)
        rhState = _NO_TEST;
      else
      {
        rhState = _SUBTYPE;
        rhName = rhPad.getSubtype().getShortName();;
      }

      if(_ascending)
      {
        switch(lhState)
        {
          case _UNTESTABLE:
          {
            if(rhState == _UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _NO_TEST:
          {
            if (rhState == _UNTESTABLE)
              return 1;
            if(rhState == _NO_TEST)
              return 0;
            else if(rhState == _SUBTYPE)
              return -1;
            break;
          }
          case _SUBTYPE:
          {
            if(rhState == _UNTESTABLE || rhState == _NO_TEST)
              return 1;
            else if(rhState == _SUBTYPE)
              return lhName.compareTo(rhName);
            break;
          }
          default:
            Assert.expect(false);
        }
      }
      else // descending
      {
        switch(rhState)
        {
          case _UNTESTABLE:
          {
            if (lhState == _UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _NO_TEST:
          {
            if (lhState == _UNTESTABLE)
              return 1;
            if(lhState == _NO_TEST)
              return 0;
            else
              return -1;
          }
          case _SUBTYPE:
          {
            if(lhState == _UNTESTABLE || lhState == _NO_TEST)
              return 1;
            else if(lhState == _SUBTYPE)
              return rhName.compareTo(lhName);
            break;
          }
          default:
            Assert.expect(false);
        }
      }
      Assert.expect(false);
      return 0;
    }
    else if (_sortOnJointType == false && _sortOnBoardName)
    {
      String[] lhBoardName = lhPad.getBoardAndComponentAndPadName().split(" ");
      String[] rhBoardName = rhPad.getBoardAndComponentAndPadName().split(" ");
      lhName = lhBoardName[0];
      rhName = rhBoardName[0];
    }
    else
      Assert.expect(false);


    if (_ascending)
      return lhName.compareToIgnoreCase(rhName);
    else
      return rhName.compareToIgnoreCase(lhName);
  }
}
