package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.packageLibrary.LibraryPackage;
import com.axi.util.*;

public class LibraryPathComparator implements Comparator<String>
{
  Map<String, List<LibraryPackage>> _libraryPathToLibraryPackagesMap;

  /**
   *
   * @param libraryPathToLibraryPackagesMap Map
   * @author Tan Hock Zoon
   */
  public LibraryPathComparator(Map<String, List<LibraryPackage>> libraryPathToLibraryPackagesMap)
  {
    Assert.expect(libraryPathToLibraryPackagesMap != null);
    _libraryPathToLibraryPackagesMap = libraryPathToLibraryPackagesMap;
  }

  /**
   * Compares its two arguments for order.
   * @param lhsPath String
   * @param rhsPath String
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
   *   than the second.
   * @todo Implement this java.util.Comparator method
   */
  public int compare(String lhsPath, String rhsPath)
  {
    Assert.expect(lhsPath != null);
    Assert.expect(rhsPath != null);

    double lhDouble = _libraryPathToLibraryPackagesMap.get(lhsPath).get(0).getSelectedLibrarySubtypeScheme().getMatchPercentage();
    double rhDouble = _libraryPathToLibraryPackagesMap.get(rhsPath).get(0).getSelectedLibrarySubtypeScheme().getMatchPercentage();

    if (lhDouble < rhDouble)
    {
      return 1;
    }
    else if (lhDouble > rhDouble)
    {
      return -1;
    }
    else
    {
      return 0;
    }

  }

}
