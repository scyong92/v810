package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * Test class for PadFamilyOrSubtypeComparator
 * @author Andy Mechtenberg
 */
public class Test_PadJointTypeOrSubtypeComparator extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_PadJointTypeOrSubtypeComparator());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test( BufferedReader is, PrintWriter os )
  {
/** @todo wpd - fix when subtypes are being assigned properly */
//    // set up 3 pads with different algorithm family names and subtype names.
//    GenericAlgorithmFamily genericAlgorithmFamilyOne = new GenericAlgorithmFamily();
//    genericAlgorithmFamilyOne.setAlgorithmFamilyEnum(AlgorithmFamilyEnum.BGA);
//    CustomAlgorithmFamily customAlgorithmFamilyOne = new CustomAlgorithmFamily(genericAlgorithmFamilyOne);
//    customAlgorithmFamilyOne.setPartialCustomName("subtype AAA");
//    customAlgorithmFamilyOne.setLegacySubType(1);
//
//    GenericAlgorithmFamily genericAlgorithmFamilyTwo = new GenericAlgorithmFamily();
//    genericAlgorithmFamilyTwo.setAlgorithmFamilyEnum(AlgorithmFamilyEnum.CAP);
//    CustomAlgorithmFamily customAlgorithmFamilyTwo = new CustomAlgorithmFamily(genericAlgorithmFamilyTwo);
//    customAlgorithmFamilyTwo.setPartialCustomName("subtype BBB");
//    customAlgorithmFamilyTwo.setLegacySubType(2);
//
//    GenericAlgorithmFamily genericAlgorithmFamilyThree = new GenericAlgorithmFamily();
//    genericAlgorithmFamilyThree.setAlgorithmFamilyEnum(AlgorithmFamilyEnum.CGA);
//    CustomAlgorithmFamily customAlgorithmFamilyThree = new CustomAlgorithmFamily(genericAlgorithmFamilyThree);
//    customAlgorithmFamilyThree.setPartialCustomName("subtype CCC");
//    customAlgorithmFamilyThree.setLegacySubType(3);
//
//    Subtype tempSubtype1 = new Subtype();
//    tempSubtype1.setName("subtype AAA");
//    tempSubtype1.setInspectionFamilyEnum(InspectionFamilyEnum.GRID_ARRAY);
//    tempSubtype1.setJointTypeEnum(JointTypeEnum.BGA);
//    for (Algorithm algorithm : tempSubtype1.getInspectionFamily().getAvailableAlgorithms())
//    {
//      tempSubtype1.addAlgorithmEnum(algorithm.getAlgorithmEnum());
//    }
//
//    Subtype tempSubtype2 = new Subtype();
//    tempSubtype2.setName("subtype BBB");
//    tempSubtype2.setInspectionFamilyEnum(InspectionFamilyEnum.GRID_ARRAY);
//    tempSubtype2.setJointTypeEnum(JointTypeEnum.CHIP);
//    for (Algorithm algorithm : tempSubtype2.getInspectionFamily().getAvailableAlgorithms())
//    {
//      tempSubtype2.addAlgorithmEnum(algorithm.getAlgorithmEnum());
//    }
//
//    Subtype tempSubtype3 = new Subtype();
//    tempSubtype3.setName("subtype CCC");
//    tempSubtype3.setInspectionFamilyEnum(InspectionFamilyEnum.GRID_ARRAY);
//    tempSubtype3.setJointTypeEnum(JointTypeEnum.CONNECTOR);
//    for (Algorithm algorithm : tempSubtype3.getInspectionFamily().getAvailableAlgorithms())
//    {
//      tempSubtype3.addAlgorithmEnum(algorithm.getAlgorithmEnum());
//    }
//
//
//    Pad pad1 = new Pad();
//    PadType padType1 = new PadType();
//    pad1.setPadType(padType1);
//    PadTypeSettings padTypeSettings1 = new PadTypeSettings();
//    padTypeSettings1.setPadType(padType1);
//    PackagePin packagePin1 = new PackagePin();
//    padType1.setPackagePin(packagePin1);
//    PackagePinSettings packagePinSettings1 = new PackagePinSettings();
//    packagePin1.setPackagePinSettings(packagePinSettings1);
//    padTypeSettings1.setInspected(true);
//    padTypeSettings1.setSubtype(tempSubtype1);
//    padType1.setPadTypeSettings(padTypeSettings1);
//
//    Pad pad2 = new Pad();
//    PadType padType2 = new PadType();
//    pad2.setPadType(padType2);
//    PadTypeSettings padTypeSettings2 = new PadTypeSettings();
//    padTypeSettings2.setInspected(true);
//    padTypeSettings2.setPadType(padType2);
//    PackagePin packagePin2 = new PackagePin();
//    PackagePinSettings packagePinSettings2 = new PackagePinSettings();
//    packagePin2.setPackagePinSettings(packagePinSettings2);
//    padType2.setPackagePin(packagePin2);
//    padTypeSettings2.setSubtype(tempSubtype2);
//    padType2.setPadTypeSettings(padTypeSettings2);
//
//    Pad pad3 = new Pad();
//    PadType padType3 = new PadType();
//    pad3.setPadType(padType3);
//    PadTypeSettings padTypeSettings3 = new PadTypeSettings();
//    padTypeSettings3.setPadType(padType3);
//    PackagePin packagePin3 = new PackagePin();
//    PackagePinSettings packagePinSettings3 = new PackagePinSettings();
//    packagePin3.setPackagePinSettings(packagePinSettings3);
//    padType3.setPackagePin(packagePin3);
//    padTypeSettings3.setInspected(true);
//    padTypeSettings3.setSubtype(tempSubtype3);
//    padType3.setPadTypeSettings(padTypeSettings3);
//
//    List<PadType> pads = new ArrayList<PadType>();
//    pads.add(padType1);
//    pads.add(padType3);
//    pads.add(padType2);
//
//    Collections.sort(pads, new PadJointTypeOrSubtypeComparator(true, true));
//    PadType padType = (PadType)pads.get(0);
//    Expect.expect(padType == padType1);
//    padType = (PadType)pads.get(1);
//    Expect.expect(padType == padType2);
//    padType = (PadType)pads.get(2);
//    Expect.expect(padType == padType3);
//
//    Collections.sort(pads, new PadJointTypeOrSubtypeComparator(false, true));
//
//    padType = (PadType)pads.get(0);
//    Expect.expect(padType == padType3);
//    padType = (PadType)pads.get(1);
//    Expect.expect(padType == padType2);
//    padType = (PadType)pads.get(2);
//    Expect.expect(padType == padType1);
//
//    Collections.sort(pads, new PadJointTypeOrSubtypeComparator(true, false));
//
//    padType = (PadType)pads.get(0);
//    Expect.expect(padType == padType1);
//    padType = (PadType)pads.get(1);
//    Expect.expect(padType == padType2);
//    padType = (PadType)pads.get(2);
//    Expect.expect(padType == padType3);
//
//    Collections.sort(pads, new PadJointTypeOrSubtypeComparator(false, false));
//
//    padType = (PadType)pads.get(0);
//    Expect.expect(padType == padType3);
//    padType = (PadType)pads.get(1);
//    Expect.expect(padType == padType2);
//    padType = (PadType)pads.get(2);
//    Expect.expect(padType == padType1);
  }

}
