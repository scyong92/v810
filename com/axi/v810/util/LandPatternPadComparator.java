package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.util.*;

/**
 * @author Laura Cormos
 */
public class LandPatternPadComparator implements Comparator<LandPatternPad>
{
  private AlphaNumericComparator _alphaComparator;
  private LandPatternPadComparatorEnum _comparingAttribute;
  private boolean _ascending;

  /**
    * @author Laura Cormos
    */
   public LandPatternPadComparator(boolean ascending, LandPatternPadComparatorEnum comparingAttribute)
   {
     Assert.expect(comparingAttribute != null);
     _ascending = ascending;
     _comparingAttribute = comparingAttribute;
     _alphaComparator = new AlphaNumericComparator(_ascending);
  }

  /**
   * @author Laura Cormos
   */
  public int compare(LandPatternPad lhs, LandPatternPad rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsString;
    String rhsString;
    int lhsInt;
    int rhsInt;
    Double lhsDouble;
    Double rhsDouble;

    if (_comparingAttribute.equals(LandPatternPadComparatorEnum.NAME))
    {
      lhsString = lhs.getName();
      rhsString = rhs.getName();
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.X_COORDINATE))
    {
      lhsInt = lhs.getCoordinateInNanoMeters().getX();
      rhsInt = rhs.getCoordinateInNanoMeters().getX();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.Y_COORDINATE))
    {
      lhsInt = lhs.getCoordinateInNanoMeters().getY();
      rhsInt = rhs.getCoordinateInNanoMeters().getY();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.WIDTH))
    {
      lhsInt = lhs.getWidthInNanoMeters();
      rhsInt = rhs.getWidthInNanoMeters();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.LENGTH))
    {
      lhsInt = lhs.getLengthInNanoMeters();
      rhsInt = rhs.getLengthInNanoMeters();
      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.DEGREES_ROTATION))
    {
      lhsDouble = lhs.getDegreesRotation();
      rhsDouble = rhs.getDegreesRotation();
      if (_ascending)
        return lhsDouble.compareTo(rhsDouble);
      else
        return rhsDouble.compareTo(lhsDouble);
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.SHAPE))
    {
      ShapeEnum lhsShapeEnum = lhs.getShapeEnum();
      ShapeEnum rhsShapeEnum = rhs.getShapeEnum();

      if (lhsShapeEnum.equals(rhsShapeEnum))
        return 0;
      else
      {
        if (lhsShapeEnum.equals(ShapeEnum.CIRCLE))
        {
          lhsString = StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
          rhsString = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
        }
        else
        {
          lhsString = StringLocalizer.keyToString("CAD_RECT_SHAPE_KEY");
          rhsString = StringLocalizer.keyToString("CAD_CIRCLE_SHAPE_KEY");
        }
        return _alphaComparator.compare(lhsString, rhsString);
      }
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.TYPE))
    {
      if (lhs.isSurfaceMountPad())
        lhsString = StringLocalizer.keyToString("CAD_SM_KEY");
      else
        lhsString = StringLocalizer.keyToString("CAD_TH_KEY");
      if (rhs.isSurfaceMountPad())
        rhsString = StringLocalizer.keyToString("CAD_SM_KEY");
      else
        rhsString = StringLocalizer.keyToString("CAD_TH_KEY");
      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.HOLE_WIDTH)) //Siew Yeng - XCR-3318
    {
//      Assert.expect(lhs instanceof ThroughHoleLandPatternPad, "Cannot compare hole diameter for non throughhole pads");
//      Assert.expect(rhs instanceof ThroughHoleLandPatternPad, "Cannot compare hole diameter for non throughhole pads");
      // since a surface mount pad does not have a hole, sort it as a 0 size hole
      if (lhs instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad lhsPad = (ThroughHoleLandPatternPad)lhs;
        lhsInt = lhsPad.getHoleWidthInNanoMeters();
      }
      else
        lhsInt = 0;
      if (rhs instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad rhsPad = (ThroughHoleLandPatternPad)rhs;
        rhsInt = rhsPad.getHoleWidthInNanoMeters();
      }
      else
        rhsInt = 0;

      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else if (_comparingAttribute.equals(LandPatternPadComparatorEnum.HOLE_LENGTH)) //Siew Yeng - XCR-3318
    {
//      Assert.expect(lhs instanceof ThroughHoleLandPatternPad, "Cannot compare hole diameter for non throughhole pads");
//      Assert.expect(rhs instanceof ThroughHoleLandPatternPad, "Cannot compare hole diameter for non throughhole pads");
      // since a surface mount pad does not have a hole, sort it as a 0 size hole
      if (lhs instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad lhsPad = (ThroughHoleLandPatternPad)lhs;
        lhsInt = lhsPad.getHoleLengthInNanoMeters();
      }
      else
        lhsInt = 0;
      if (rhs instanceof ThroughHoleLandPatternPad)
      {
        ThroughHoleLandPatternPad rhsPad = (ThroughHoleLandPatternPad)rhs;
        rhsInt = rhsPad.getHoleLengthInNanoMeters();
      }
      else
        rhsInt = 0;

      if (lhsInt > rhsInt)
        if (_ascending)
          return 1;
        else
          return -1;
      else if (lhsInt == rhsInt)
        return 0;
      else
      if (_ascending)
        return -1;
      else
        return 1;
    }
    else
    {
      Assert.expect(false, "Please add to this method your new land pattern pad attribute to sort on.");
      return 0;
    }
  }
}
