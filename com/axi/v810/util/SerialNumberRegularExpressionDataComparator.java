package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Cheah Lee Herng
 */
public class SerialNumberRegularExpressionDataComparator implements Comparator<SerialNumberRegularExpressionData> 
{
    private AlphaNumericComparator _alphaComparator;
    private SerialNumberRegularExpressionDataComparatorEnum _comparingAttribute;
    private boolean _ascending;
    
    /**
     * @author Cheah Lee Herng
    */
    public SerialNumberRegularExpressionDataComparator(boolean ascending, SerialNumberRegularExpressionDataComparatorEnum comparingAttribute)
    {
        Assert.expect(comparingAttribute != null);
        _ascending = ascending;
        _comparingAttribute = comparingAttribute;
        _alphaComparator = new AlphaNumericComparator(_ascending);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int compare(SerialNumberRegularExpressionData lhs, SerialNumberRegularExpressionData rhs) 
    {
        Assert.expect(lhs != null);
        Assert.expect(rhs != null);
        
        String lhsString;
        String rhsString;
        
        if (_comparingAttribute.equals(SerialNumberRegularExpressionDataComparatorEnum.PATTERN_TYPE))
        {
          lhsString = lhs.getInterpretedAsRegularExpressionStringValue();
          rhsString = rhs.getInterpretedAsRegularExpressionStringValue();
          return _alphaComparator.compare(lhsString, rhsString);
        }
        else if (_comparingAttribute.equals(SerialNumberRegularExpressionDataComparatorEnum.PATTERN))
        {
          lhsString = lhs.getRegularExpression();
          rhsString = rhs.getRegularExpression();
          return _alphaComparator.compare(lhsString, rhsString);  
        }
        else if (_comparingAttribute.equals(SerialNumberRegularExpressionDataComparatorEnum.PROJECT_NAME))
        {
          lhsString = lhs.getProjectName();
          rhsString = rhs.getProjectName();
          return _alphaComparator.compare(lhsString, rhsString);  
        }
        else
        {
          Assert.expect(false, "Please add your new serial number regular expression attribute to sort on to this method");
          return 0;
        }
    }
    
}
