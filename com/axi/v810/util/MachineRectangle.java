package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * This class defines a rectangle in machine space.  The origin is the "stage home" position.
 * The x-axis grows right and the y-axis grows up.
 * The unit is nanometers.
 *
 * @author Matt Wharton
 */
public class MachineRectangle extends IntRectangle implements Serializable
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public MachineRectangle(int bottomLeftX, int bottomLeftY, int width, int height)
  {
    super(bottomLeftX, bottomLeftY, width, height);
  }

  /**
   * @author George A. David
   */
  public MachineRectangle(java.awt.Shape shape)
  {
    super(shape);
  }

  /**
   * Copy constructor.
   *
   * @author Matt Wharton
   */
  public MachineRectangle(MachineRectangle rhs)
  {
    super(rhs);
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializeRectangle(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializeRectangle(os);
  }
}
