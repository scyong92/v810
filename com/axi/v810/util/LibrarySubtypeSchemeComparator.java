package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.util.Assert;

public class LibrarySubtypeSchemeComparator implements Comparator<LibrarySubtypeScheme>
{
  /**
   * Compares its two arguments for order.
   *
   * @param lhLibrarySubtypeScheme LibrarySubtypeScheme
   * @param rhLibrarySubtypeScheme LibrarySubtypeScheme
   * @return -1 if the first LibrarySubtypeScheme percentage is less than the second,
   * 0 if the two LibrarySubtypeScheme percentage are equal,
   * 1 if the first LibrarySubtypeScheme percentage is greater than the second
   */
  public int compare(LibrarySubtypeScheme lhLibrarySubtypeScheme, LibrarySubtypeScheme rhLibrarySubtypeScheme)
  {
    Assert.expect(lhLibrarySubtypeScheme != null);
    Assert.expect(rhLibrarySubtypeScheme != null);

    double lhDouble = lhLibrarySubtypeScheme.getMatchPercentage();
    double rhDouble = rhLibrarySubtypeScheme.getMatchPercentage();

    if (lhDouble < rhDouble)
    {
      return 1;
    }
    else if (lhDouble > rhDouble)
    {
      return -1;
    }
    else
    {
      return 0;
    }
  }
}
