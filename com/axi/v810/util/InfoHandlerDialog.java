package com.axi.v810.util;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.gui.*;
import com.axi.v810.images.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import com.axi.v810.util.*;
import java.io.*;
import java.util.*;
import java.util.List;
import javax.swing.event.*;
import javax.swing.tree.*;

import ekit.*;
import java.net.*;


/**
 * @author bee-hoon.goh
 * @author Swee Yee Wong
 */
public class InfoHandlerDialog extends JFrame implements Observer
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private JPanel _infoTreePanel = new JPanel();
  private JPanel _infoDetailsPanel = new JPanel();
  private JPanel _infoEnumCodePanel = new JPanel();
  private JPanel _infoEnumComboPanel = new JPanel();
  private JPanel _infoEnumSearchPanel = new JPanel();
  private JPanel _okInnerPanel = new JPanel();
  
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private BorderLayout _infoTreePanelBorderLayout = new BorderLayout();
  private BorderLayout _infoDetailsPanelBorderLayout = new BorderLayout();
  private BorderLayout _infoEnumCodePanelBorderLayout = new BorderLayout();
  private FlowLayout _infoEnumComboPanelFlowLayout = new FlowLayout();
  private FlowLayout _infoEnumSearchPanelFlowLayout = new FlowLayout();
  private FlowLayout _okPanelFlowLayout = new FlowLayout();
  private GridLayout _okInnerGridLayout = new GridLayout();
  
  private JComboBox _infoEnumCodeComboBox = new JComboBox();
  private JLabel _infoEnumCodeLabel = new JLabel();
  private JLabel _infoEnumSearchLabel = new JLabel();
  private JLabel _selectedLabel = new JLabel();
  private JTextField _infoEnumSearchTextField = new JTextField();
  private JButton _editContentButton = new JButton();
  private JButton _okButton = new JButton();
  private static JEditorPane _editor = new JEditorPane();
  private static JScrollPane _pane = null;
  private JFileChooser _fileChooser = null;
  private JSplitPane _splitPane = null;
  
  private JTree _tree;
  private DefaultTreeModel _model;
  private DefaultMutableTreeNode _root;
  
  private JMenuBar _menuBar;
  private JMenu _edit;
  private JMenuItem _insertTag;
  private JMenuItem _deleteTag;
  private JMenuItem _exportHtml;
  private JMenuItem _importHtml;
  private JMenu _settings;
  private JMenuItem _databaseDirSet;
  private JMenuItem _validateMediaFile;
  private JMenuItem _importDatabase;
  private JMenu _language;
  private JMenuItem _english;
  private JMenuItem _chinese;
  
  JFrame _frame = null;
  
  private static InfoHandlerPanel _infoHandlerPanel;
  private List<String> _infoTagFullList = new ArrayList<String>();
  private List<String> _comboBoxFullList = new ArrayList<String>();
  private List<String> _searchList = new ArrayList<String>();
  
  private final String TEMP_HTML_FILE = "temp.html";
  
  static  InsertNewInfoTagDialog _insertDlg;
  static  DeleteInfoTagDialog _deleteDlg;
  static  ValidateMediaFileDialog _validateFileDlg;
  static  ValidateMissingTagDialog _validateMissingTagDlg;
  static  ImportDatabaseDialog _importDatabaseDlg;
  
  private static boolean _isSearching = false;
  private static boolean _isFullDisplayMode = false;
  private boolean _isDatabaseValid = false;
  
  private static String _ENGLISH = "en";
  private static String _CHINESE = "zh_CN";
  
  private static DatabaseObservable _databaseObservable;

  /**
   * @author bee-hoon.goh
   * @author Swee Yee Wong
   */
  InfoHandlerDialog (InfoHandlerPanel infoHandlerPanel)
  {
    _frame = new JFrame();
    _infoHandlerPanel = infoHandlerPanel;
    _databaseObservable = DatabaseObservable.getInstance();
    InfoHandlerDatabaseManager.getInstance().openConnection();
    _isDatabaseValid = InfoHandlerDatabaseManager.getInstance().getDatabaseConnection();
    if(_isDatabaseValid == true)
    {
      double databaseVersion = InfoHandlerDatabaseManager.getInstance().getDatabaseVersion();
      setTitle(StringLocalizer.infoHandlerKeyToString("INFO_HANDLER_GUI_NAME_KEY") + " " + databaseVersion);
    }
    else
    {
      SwingUtils.invokeLater(new Runnable()
      {

        public void run()
        {
          MessageDialog.showErrorDialog(null,
                    StringLocalizer.infoHandlerKeyToString("DATABASE_NOT_FOUND_DIALOG_MESSAGE_KEY"),
                    StringLocalizer.infoHandlerKeyToString("DATABASE_NOT_FOUND_DIALOG_TITLE_KEY"),
                    true);
        }
      });
      selectNewDatabaseDir();
    }
    
    try
    {
      jbInit();
      pack();
      InfoHandlerPanel.getInstance().setDialogInitStatus(true);
    }
    catch(Exception ex)
    {
      System.out.println("Failed to create Info Handler Frame");
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void addTree()
  {
    _root = getTreeRoot();
    _model = new DefaultTreeModel(_root);
    _tree = new JTree(_model);

    addTreeNode();
  }
  
  private void addTreeNode()
  {
    if (_isDatabaseValid)
    {

      for (String tagName : _infoTagFullList)
      {
        insertNewNode(_root, tagName, tagName);
      }

      _tree.setShowsRootHandles(true);

      _tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener()
      {
        public void valueChanged(TreeSelectionEvent e)
        {
          if (_isDatabaseValid)
          {
            DefaultMutableTreeNode SelectedNode = (DefaultMutableTreeNode) _tree.getLastSelectedPathComponent();
            if (SelectedNode != null)
            {
              if (InfoHandlerDatabaseManager.getInstance().isTagExists(SelectedNode.getUserObject().toString()) == true)
              {
                addInfoTagToComboBox(SelectedNode.getUserObject().toString());
              }
              _selectedLabel.setText(SelectedNode.getUserObject().toString());
            }
          }
        }
      });
    }
  }
  
  private DefaultMutableTreeNode getTreeRoot()
  {
    DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Database");

    return rootNode;
  }
  
  private int insertNewNode(DefaultMutableTreeNode rootNode, String message, String fullName)
  {
    String nodeName;
    int index = message.indexOf('_');
    if (index == -1)
    {
      nodeName = message;
      for (int i = 0; i < _tree.getModel().getChildCount(rootNode); i++)
      {
        DefaultMutableTreeNode child = (DefaultMutableTreeNode) _tree.getModel().getChild(rootNode, i);
        if (child.getUserObject().toString().equals(nodeName))
        {
          return 0;
        }
      }
      _model.insertNodeInto(new DefaultMutableTreeNode(fullName), rootNode, rootNode.getChildCount());
      return 1;
    }
    else
    {
      nodeName = message.substring(0, index);
      String Nextmessage = message.substring(index + 1);
      for (int i = 0; i < _tree.getModel().getChildCount(rootNode); i++)
      {
        DefaultMutableTreeNode child = (DefaultMutableTreeNode) _tree.getModel().getChild(rootNode, i);
        if (child.getUserObject().toString().equals(nodeName))
        {
          return insertNewNode(child, Nextmessage, fullName);
        }
      }
      DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(nodeName);
      _model.insertNodeInto(newNode, rootNode, rootNode.getChildCount());
      return insertNewNode(newNode, Nextmessage, fullName);
    }
  }
  
  private int deleteNode(DefaultMutableTreeNode rootNode, DefaultMutableTreeNode deletingNode, String message, String fullName)
  {
    String nodeName;
    DefaultMutableTreeNode singleChildNode = deletingNode;
    if (_tree.getModel().getChildCount(rootNode) < 2 && deletingNode == null)
    {
      singleChildNode = rootNode;
    }
    else if (_tree.getModel().getChildCount(rootNode) > 1)
    {
      singleChildNode = null;
    }

    int index = message.indexOf('_');
    if (index == -1)
    {
      for (int i = 0; i < _tree.getModel().getChildCount(rootNode); i++)
      {
        DefaultMutableTreeNode child = (DefaultMutableTreeNode) _tree.getModel().getChild(rootNode, i);
        if (child.getUserObject().toString().equals(fullName))
        {
          if (singleChildNode != null)
            _model.removeNodeFromParent(singleChildNode);
          else
            _model.removeNodeFromParent(child);
          return 1;
        }
      }
      return 0;
    }
    else
    {
      nodeName = message.substring(0, index);
      String Nextmessage = message.substring(index + 1);
      for (int i = 0; i < _tree.getModel().getChildCount(rootNode); i++)
      {
        DefaultMutableTreeNode child = (DefaultMutableTreeNode) _tree.getModel().getChild(rootNode, i);
        if (child.getUserObject().toString().equals(nodeName))
        {
          return deleteNode(child, singleChildNode, Nextmessage, fullName);
        }
      }
      return 0;
    }
  }
  
  private void deleteAllTreeNode()
  {
    while(_tree.getModel().getChildCount(_root) > 0)
    {
      DefaultMutableTreeNode child = (DefaultMutableTreeNode) _tree.getModel().getChild(_root, 0);
      _model.removeNodeFromParent(child);
    }
  }
  
  /**
   * @author bee-hoon.goh
   * @author Swee Yee Wong
   */
  private void jbInit() throws Exception
  {
    _databaseObservable.addObserver(this);
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _infoTreePanel.setLayout(_infoTreePanelBorderLayout);
    _infoDetailsPanel.setLayout(_infoDetailsPanelBorderLayout);
    _infoEnumCodePanel.setLayout(_infoEnumCodePanelBorderLayout);
    _infoEnumComboPanel.setLayout(_infoEnumComboPanelFlowLayout);
    _infoEnumSearchPanel.setLayout(_infoEnumSearchPanelFlowLayout);  
    _okCancelPanel.setLayout(_okPanelFlowLayout);
    _okCancelPanel.setPreferredSize(new Dimension(850, 50));
    _okPanelFlowLayout.setAlignment(FlowLayout.RIGHT);
    _okPanelFlowLayout.setHgap(20);
    _okPanelFlowLayout.setVgap(10);
    
    _editor.setContentType("text/html");
    _editor.setEditable(false);
    _pane = new JScrollPane();
    _pane.getViewport().add(_editor, null);
    _pane.setPreferredSize(new Dimension(850, 700));
    _infoEnumCodeLabel.setText(StringLocalizer.infoHandlerKeyToString("GUI_INFO_CODE_KEY"));

    _infoEnumSearchLabel.setText(StringLocalizer.infoHandlerKeyToString("GUI_SEARCH_LABEL_KEY"));
    _infoEnumSearchTextField.setPreferredSize(new Dimension(180, 27));
    _infoEnumSearchTextField.addCaretListener(new CaretListener()
    {
      public void caretUpdate(CaretEvent ce)
      {
        searchInfoEnum(ce);

      }
    });
    
    //swee yee wong
    _editContentButton.setText(StringLocalizer.infoHandlerKeyToString("GUI_EDIT_CONTENT_BUTTON_KEY"));
    _editContentButton.setEnabled(false);
    _editContentButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        editContentButton_actionPerformed(e);
      }
    });
    
    _okButton.setMnemonic(StringLocalizer.infoHandlerKeyToString("GUI_INFO_OK_BUTTON_KEY").charAt(0));
    _okButton.setText(StringLocalizer.infoHandlerKeyToString("GUI_INFO_OK_BUTTON_KEY"));
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    
    _infoEnumCodeComboBox.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        infoEnumCodeComboBox_actionPerformed(e);
      }
    });
    
    _okInnerPanel.setLayout(_okInnerGridLayout);
    _okInnerGridLayout.setHgap(20);
    _okInnerGridLayout.setVgap(0);

    setJMenuBar();
    addFullListToComboBox();
    addTree();

    getContentPane().add(_mainPanel);
    _infoTreePanel.add(_selectedLabel, BorderLayout.SOUTH);
    _infoTreePanel.add(_tree);
    _infoTreePanel.add(new JScrollPane(_tree));
    _infoDetailsPanel.add(_infoEnumCodePanel, BorderLayout.NORTH);
    _infoEnumCodePanel.add(_infoEnumComboPanel, BorderLayout.WEST);
    _infoEnumComboPanel.add(_infoEnumCodeLabel, null);
    _infoEnumComboPanel.add(_infoEnumCodeComboBox, null);
    _infoEnumCodePanel.add(_infoEnumSearchPanel, BorderLayout.EAST);
    _infoEnumSearchPanel.add(_infoEnumSearchLabel, null);
    _infoEnumSearchPanel.add(_infoEnumSearchTextField, null);
    _infoDetailsPanel.add(_pane, BorderLayout.CENTER);
    _infoDetailsPanel.add(_okCancelPanel, BorderLayout.SOUTH);
    _okCancelPanel.add(_okInnerPanel);
    _okInnerPanel.add(_editContentButton);
    _okInnerPanel.add(_okButton);
    
    _splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, _infoTreePanel, _infoDetailsPanel);
    _splitPane.setContinuousLayout(true);
    _splitPane.setResizeWeight(0.2);
    
    _mainPanel.add(_splitPane, BorderLayout.CENTER);
    
  }

  private void setJMenuBar()
  {
    _menuBar = new JMenuBar();
    _edit = new JMenu(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_EDIT_LABEL_KEY"));
    _edit.setMnemonic(KeyEvent.VK_E);
    
    _insertTag = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_INSERT_TAG_LABEL_KEY"));
    _insertTag.setMnemonic(KeyEvent.VK_I);
    _insertTag.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_INSERT_TAG_TOOLTIP_TEXT_KEY"));
    _insertTag.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_I, ActionEvent.ALT_MASK));
    _insertTag.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        insertTag_actionPerformed(event);
      }
    });
    
    _deleteTag = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_DELETE_TAG_LABEL_KEY"));
    _deleteTag.setMnemonic(KeyEvent.VK_D);
    _deleteTag.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_DELETE_TAG_TOOLTIP_TEXT_KEY"));
    _deleteTag.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_D, ActionEvent.ALT_MASK));
    _deleteTag.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        deleteTag_actionPerformed(event);
      }
    });

    _exportHtml = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_EXPORT_HTML_LABEL_KEY"));
    _exportHtml.setMnemonic(KeyEvent.VK_E);
    _exportHtml.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_EXPORT_HTML_TOOLTIP_TEXT_KEY"));
    _exportHtml.setAccelerator(KeyStroke.getKeyStroke(
      KeyEvent.VK_E, ActionEvent.ALT_MASK));
    _exportHtml.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        exportHtml_actionPerformed(event);
      }
    });
    
    _importHtml = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_IMPORT_HTML_LABEL_KEY"));
    _importHtml.setMnemonic(KeyEvent.VK_P);
    _importHtml.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_IMPORT_HTML_TOOLTIP_TEXT_KEY"));
    _importHtml.setAccelerator(KeyStroke.getKeyStroke(
      KeyEvent.VK_P, ActionEvent.ALT_MASK));
    _importHtml.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        importHtml_actionPerformed(event);
      }
    });
    
    _settings = new JMenu(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_SETTING_LABEL_KEY"));
    _settings.setMnemonic(KeyEvent.VK_S);

    _databaseDirSet = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_CHANGE_DIR_LABEL_KEY"));
    _databaseDirSet.setMnemonic(KeyEvent.VK_C);
    _databaseDirSet.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_CHANGE_DIR_TOOLTIP_TEXT_KEY"));
    _databaseDirSet.setAccelerator(KeyStroke.getKeyStroke(
      KeyEvent.VK_C, ActionEvent.ALT_MASK));
    _databaseDirSet.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        selectDatabase_actionPerformed(event);
      }
    });
      
    _validateMediaFile = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_VALIDATE_FILE_LABEL_KEY"));
    _validateMediaFile.setMnemonic(KeyEvent.VK_M);
    _validateMediaFile.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_VALIDATE_FILE_TOOLTIP_TEXT_KEY"));
    _validateMediaFile.setAccelerator(KeyStroke.getKeyStroke(
      KeyEvent.VK_M, ActionEvent.ALT_MASK));
    _validateMediaFile.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        validateMediaFile_actionPerformed(event);
      }
    });
    
    
    _importDatabase = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_IMPORT_DATABASE_LABEL_KEY"));
    _importDatabase.setMnemonic(KeyEvent.VK_B);
    _importDatabase.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_IMPORT_DATABASE_TOOLTIP_TEXT_KEY"));
    _importDatabase.setAccelerator(KeyStroke.getKeyStroke(
      KeyEvent.VK_B, ActionEvent.ALT_MASK));
    _importDatabase.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        importDatabase_actionPerformed(event);
      }
    });
    
     
    _language = new JMenu(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_LANGUAGE_LABEL_KEY"));
    _language.setMnemonic(KeyEvent.VK_L);
    _language.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_LANGUAGE_TOOLTIP_TEXT_KEY"));
    
    _english = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_LANGUAGE_ENGLISH_LABEL_KEY"));
    _english.setMnemonic(KeyEvent.VK_N);
    _english.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_LANGUAGE_ENGLISH_TOOLTIP_TEXT_KEY"));
    _english.setAccelerator(KeyStroke.getKeyStroke(
      KeyEvent.VK_N, ActionEvent.ALT_MASK));
    _english.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        english_actionPerformed(event);
      }
    });
    
    _chinese = new JMenuItem(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_LANGUAGE_CHINESE_LABEL_KEY"));
    _chinese.setMnemonic(KeyEvent.VK_H);
    _chinese.setToolTipText(StringLocalizer.infoHandlerKeyToString("GUI_MENU_BAR_LANGUAGE_CHINESE_TOOLTIP_TEXT_KEY"));
    _chinese.setAccelerator(KeyStroke.getKeyStroke(
      KeyEvent.VK_H, ActionEvent.ALT_MASK));
    _chinese.addActionListener(new ActionListener()
    {
      @Override
      public void actionPerformed(ActionEvent event)
      {
        chinese_actionPerformed(event);
      }
    });
    
    _language.add(_english);
    _language.add(_chinese);
      
    _edit.add(_insertTag);
    _edit.add(_deleteTag);
    _edit.add(_exportHtml);
    _edit.add(_importHtml);
    _edit.add(_importDatabase);
    _edit.add(_databaseDirSet);
    
    _settings.add(_validateMediaFile);
    _settings.add(_language);
    
    _menuBar.add(_edit);
    _menuBar.add(_settings);

    setJMenuBar(_menuBar);
  }
  
  /**
   * @author swee-yee.wong
   */
  private void searchInfoEnum(CaretEvent ce)
  {
    String search = _infoEnumSearchTextField.getText();

    if (search.length() > 0)
    {
      if (_isSearching == false)
      {
        for (String tagName : _comboBoxFullList)
        {
          _searchList.add(tagName);
        }
        _isSearching = true;
      }
      clearComboList();

      for (String tagName : _searchList)
      {
        if (tagName.contains(search))
        {
          _infoEnumCodeComboBox.addItem(tagName);
          _comboBoxFullList.add(tagName);
          _infoEnumCodeComboBox.setSelectedItem(tagName);
        }
      }
    }
    else
    {
      resetSearchList();
    }
  }

  /**
   * @author bee-hoon.goh
   */
  private void okButton_actionPerformed(ActionEvent e)
  {
    if(InfoHandlerConfig.getInstance().isUsingLanguageEqualLocale() == false)
    {
      InfoHandlerDatabaseManager.getInstance().closeConnection();
      InfoHandlerConfig.getInstance().resetLanguage();
      InfoHandlerDatabaseManager.getInstance().openConnection();
      populateData();
      if (_isDatabaseValid == true)
      {
        refreshHtmlContent();
      }
    }
//    InfoHandlerDatabaseManager.getInstance().closeConnection();
    _infoHandlerPanel.setInfoHandlerVisible(false);
    _infoEnumSearchTextField.setText("");
    this.setVisible(false);
  }
  
  /**
   * @author swee-yee.wong
   */
  private void editContentButton_actionPerformed(ActionEvent e)
  {
    if (_isDatabaseValid)
    {
      final String fullPath = InfoHandlerConfig.getInstance().getInfoHandlerTempDir() + TEMP_HTML_FILE;
      try
      {
        InfoHandlerFileUtil.getInstance().writeToFile(fullPath, _editor.getText());

        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            setHtmlEditor();
            loadHtmlContentFromFile(fullPath);
            saveContent();
          }
        });
      }
      catch (CouldNotCreateFileException ex)
      {
        MessageDialog.showErrorDialog(null,
          StringLocalizer.infoHandlerKeyToString("ERR_FAILED_TO_WRITE_FILE_MESSAGE_KEY") + fullPath,
          StringLocalizer.infoHandlerKeyToString("ERR_FAILED_TO_WRITE_FILE_DIALOG_TITLE_KEY"),
          true);
      }
    }
  }

  /**
   * @author swee-yee.wong
   */
  private void insertTag_actionPerformed(ActionEvent e)
  {
    if (_isDatabaseValid)
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          createInsertTagDialog();
        }
      });
      setFullDisplayMode(true);
    }
  }

  private void createInsertTagDialog()
  {
    _insertDlg = new InsertNewInfoTagDialog(this,
      StringLocalizer.infoHandlerKeyToString("INSERT_NEW_TAG_GUI_NAME_KEY"),
      true, InfoHandlerDatabaseManager.getInstance());
    SwingUtils.centerOnComponent(_insertDlg, this);
    _insertDlg.setVisible(true);
  }

  /**
   * @author swee-yee.wong
   */
  private void selectDatabase_actionPerformed(ActionEvent e)
  {
    selectNewDatabaseDir();
  }

  /**
   * @author swee-yee.wong
   */
  private void deleteTag_actionPerformed(ActionEvent e)
  {
    if (_isDatabaseValid)
    {
      SwingUtils.invokeLater(new Runnable()
      {

        public void run()
        {
          createDeleteTagDialog();
          setFullDisplayMode(true);
        }
      });
    }
  }
  
  /**
   * @author swee-yee.wong
   */
  private void exportHtml_actionPerformed(ActionEvent e)
  {
    if (_isDatabaseValid)
    {
      String fullPath = InfoHandlerConfig.getInstance().getInfoHandlerTempDir() + _infoEnumCodeComboBox.getSelectedItem().toString() + ".html";
      try
      {
        InfoHandlerFileUtil.getInstance().writeToFile(fullPath, _editor.getText());
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            MessageDialog.showInformationDialog(null,
              StringLocalizer.infoHandlerKeyToString("DATABASE_HTML_EXPORT_MESSAGE_KEY") + InfoHandlerConfig.getInstance().getInfoHandlerTempDir(),
              StringLocalizer.infoHandlerKeyToString("DATABASE_HTML_EXPORT_TITLE_KEY"),
              true);
          }
        });
        _selectedLabel.setText("Content is saved in temp folder");
      }
      catch (CouldNotCreateFileException ex)
      {
        MessageDialog.showErrorDialog(null,
          StringLocalizer.infoHandlerKeyToString("ERR_FAILED_TO_WRITE_FILE_MESSAGE_KEY") + InfoHandlerConfig.getInstance().getInfoHandlerTempDir(),
          StringLocalizer.infoHandlerKeyToString("ERR_FAILED_TO_WRITE_FILE_DIALOG_TITLE_KEY"),
          true);
      }
    }
  }
  
  /**
   * @author swee-yee.wong
   */
  private void importHtml_actionPerformed(ActionEvent e)
  {
    if (_isDatabaseValid)
    {
      _fileChooser = new JFileChooser();
      _fileChooser.setCurrentDirectory(new File(InfoHandlerConfig.getInfoHandlerDir()));
      _fileChooser.setDialogTitle(StringLocalizer.infoHandlerKeyToString("IMPORT_HTML_FILE_CHOOSER_GUI_NAME_KEY"));

      if (_fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
      {
        loadHtmlContentFromFile(_fileChooser.getSelectedFile().toString());
        saveContent();
        _selectedLabel.setText("Current page is replaced with selected html file");
      }
    }
  }
    
  private void createDeleteTagDialog()
  {
    _deleteDlg = new DeleteInfoTagDialog(this,
      StringLocalizer.infoHandlerKeyToString("DELETE_TAG_GUI_NAME_KEY"),
      true, InfoHandlerDatabaseManager.getInstance(),
      _infoEnumCodeComboBox.getSelectedItem().toString());
    SwingUtils.centerOnComponent(_deleteDlg, this);
    _deleteDlg.setVisible(true);
  }

  private void validateMediaFile_actionPerformed(ActionEvent e)
  {
    boolean isMissingFiles = validateMediaFile();
    if (isMissingFiles == false)
    {
      SwingUtils.invokeLater(new Runnable()
      {
        public void run()
        {
          MessageDialog.showInformationDialog(null,
            StringLocalizer.infoHandlerKeyToString("VALIDATE_MEDIA_FILES_NO_MISSING_MESSAGE_KEY") + InfoHandlerConfig.getInstance().getInfoHandlerMediaDir(),
            StringLocalizer.infoHandlerKeyToString("VALIDATE_MEDIA_FILES_NO_MISSING_TITLE_KEY"),
            true);
        }
      });
    }
  }
  
  private void importDatabase_actionPerformed(ActionEvent e)
  {
    _fileChooser = new JFileChooser();
    _fileChooser.setCurrentDirectory(new File(InfoHandlerConfig.getInfoHandlerDir()));
    _fileChooser.setDialogTitle(StringLocalizer.infoHandlerKeyToString("IMPORT_DATABASE_FILE_CHOOSER_GUI_NAME_KEY"));
    _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    //
    // disable the "All files" option.
    //
    _fileChooser.setAcceptAllFileFilterUsed(false);
    //    
    if (_fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      InfoHandlerDatabaseManager.getInstance2().setLibraryPath(_fileChooser.getSelectedFile().toString());
      InfoHandlerDatabaseManager.getInstance2().openConnection();
      if (InfoHandlerDatabaseManager.getInstance2().getDatabaseConnection() == false)
      {
        _selectedLabel.setText(StringLocalizer.infoHandlerKeyToString("DATABASE_NOT_FOUND_MESSAGE_KEY"));
      }
      else
      {
        final ArrayList<ImportDatabaseDiffPackage> importList = InfoHandlerDatabaseManager.getInstance2().getImportDatabaseDiffPackage();

        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            createImportDatabaseDialog(importList);
            setFullDisplayMode(true);
            if (_isDatabaseValid == true)
            {
              refreshHtmlContent();
            }
          }
        });
      }
    }
  }
  
  private void createImportDatabaseDialog(ArrayList<ImportDatabaseDiffPackage> importList)
  {
    _importDatabaseDlg = new ImportDatabaseDialog(this,
      StringLocalizer.infoHandlerKeyToString("IMPORT_DATABASE_GUI_NAME_KEY"),
      true, importList);
    SwingUtils.centerOnComponent(_importDatabaseDlg, this);
    _importDatabaseDlg.setVisible(true);
  }
    
  private void english_actionPerformed(ActionEvent e)
  {
    if (InfoHandlerConfig.getInstance().getUsingDatabaseLanguage().equals(_ENGLISH) == false)
    {
      String currentTag = null;
      List<String> currentComboBoxList = new ArrayList<String>();
      if (_infoEnumCodeComboBox.getItemCount() > 0)
      {
        currentTag = _infoEnumCodeComboBox.getSelectedItem().toString();
        for (String tagName : _comboBoxFullList)
        {
          currentComboBoxList.add(tagName);
        }
      }
      InfoHandlerConfig.getInstance().setDatabaseLanguage(_ENGLISH);
      InfoHandlerDatabaseManager.getInstance().closeConnection();
      InfoHandlerConfig.getInstance().refreshConfig();
      InfoHandlerDatabaseManager.getInstance().openConnection();
      populateData();
      if (_isFullDisplayMode)
      {
        if (_isDatabaseValid == false)
        {
          clearComboList();
          addInfoListToComboBox(currentComboBoxList);
        }
        if (currentTag != null && _comboBoxFullList.contains(currentTag))
        {
          _infoEnumCodeComboBox.setSelectedItem(currentTag);
        }
      }
      else
      {
        if (currentTag != null)
        {
          clearComboList();
          addInfoListToComboBox(currentComboBoxList);
          _infoEnumCodeComboBox.setSelectedItem(currentTag);
        }
      }
      if (_isDatabaseValid == true)
      {
        refreshHtmlContent();
      }
    }
  }
  
  private void chinese_actionPerformed(ActionEvent e)
  {
    if (InfoHandlerConfig.getInstance().getUsingDatabaseLanguage().equals(_CHINESE)== false)
    {
      String currentTag = null;
      List<String> currentComboBoxList = new ArrayList<String>();
      if (_infoEnumCodeComboBox.getItemCount() > 0)
      {
        currentTag = _infoEnumCodeComboBox.getSelectedItem().toString();
        for (String tagName : _comboBoxFullList)
        {
          currentComboBoxList.add(tagName);
        }
      }
      InfoHandlerConfig.getInstance().setDatabaseLanguage(_CHINESE);
      InfoHandlerDatabaseManager.getInstance().closeConnection();
      InfoHandlerConfig.getInstance().refreshConfig();
      InfoHandlerDatabaseManager.getInstance().openConnection();
      populateData();
      if (_isFullDisplayMode)
      {
        if (_isDatabaseValid == false)
        {
          clearComboList();
          addInfoListToComboBox(currentComboBoxList);
        }
        if (currentTag != null && _comboBoxFullList.contains(currentTag))
        {
          _infoEnumCodeComboBox.setSelectedItem(currentTag);
        }
      }
      else
      {
        if (currentTag != null)
        {
          clearComboList();
          addInfoListToComboBox(currentComboBoxList);
          _infoEnumCodeComboBox.setSelectedItem(currentTag);
        }
      }
      if (_isDatabaseValid == true)
      {
        refreshHtmlContent();
      }
    }
  }
  
  
  /**
   * @author swee-yee.wong
   */
  private void infoEnumCodeComboBox_actionPerformed(ActionEvent e)
  {
    if (_infoEnumCodeComboBox.getSelectedItem() != null && _isDatabaseValid == true)
    {
      _editor.setText(InfoHandlerDatabaseManager.getInstance().getTagHtml(_infoEnumCodeComboBox.getSelectedItem().toString()));
    }
    else
    {
      _editor.setText("");
    }
  }
  
  
  private void saveContent()
  {
    if(_isDatabaseValid == true)
    {
      InfoHandlerDatabaseManager.getInstance().updateTagHtmlContent(_infoEnumCodeComboBox.getSelectedItem().toString(), _editor.getText());
      InfoHandlerDatabaseManager.getInstance().extractAndUpdateTagMediaList(_infoEnumCodeComboBox.getSelectedItem().toString(), _editor.getText());
      InfoHandlerDatabaseManager.getInstance().renameDatabase();
      _selectedLabel.setText(_infoEnumCodeComboBox.getSelectedItem().toString() + " is saved in database");
    }
  }
  
  private void selectNewDatabaseDir()
  {
    String currentTag = null;
    List<String> currentComboBoxList = new ArrayList<String>();
    if (_infoEnumCodeComboBox.getItemCount() > 0)
    {
      currentTag = _infoEnumCodeComboBox.getSelectedItem().toString();
      for (String tagName : _comboBoxFullList)
      {
        currentComboBoxList.add(tagName);
      }
    }
    _fileChooser = new JFileChooser();
    _fileChooser.setCurrentDirectory(new File(InfoHandlerConfig.getInfoHandlerDir()));
    _fileChooser.setDialogTitle(StringLocalizer.infoHandlerKeyToString("CHANGE_DIR_FILE_CHOOSER_GUI_NAME_KEY"));
    _fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    //
    // disable the "All files" option.
    //
    _fileChooser.setAcceptAllFileFilterUsed(false);
    //    
    if (_fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
    {
      InfoHandlerDatabaseManager.getInstance().closeConnection();
      String newPath = _fileChooser.getSelectedFile().toString();
      InfoHandlerConfig.getInstance().saveDatabaseDirToConfig(InfoHandlerFileUtil.changeFullPathToRelativePath(newPath));
      InfoHandlerConfig.getInstance().refreshConfig();
      InfoHandlerDatabaseManager.getInstance().openConnection();
      populateData();
      if (_isFullDisplayMode)
      {
        if (_isDatabaseValid == false)
        {
          clearComboList();
          addInfoListToComboBox(currentComboBoxList);
        }
        if (currentTag != null && _comboBoxFullList.contains(currentTag))
        {
          _infoEnumCodeComboBox.setSelectedItem(currentTag);
        }
      }
      else
      {
        if (currentTag != null)
        {
          clearComboList();
          addInfoListToComboBox(currentComboBoxList);
          _infoEnumCodeComboBox.setSelectedItem(currentTag);
        }
      }
    }
    else
    {
      populateData();
      _selectedLabel.setText("No Selection");
    }
    
  }
  
  private void setHtmlEditor()
  {
    String sLang = null;
    String sCtry = null;
    
    String locale = Localization.getLocale().toString();
    int index = locale.indexOf("_");
    
    if(index > -1)
    {
      sLang = locale.substring(0, index);
      sCtry = locale.substring(index + 1);
    }
    
    String sDocument = InfoHandlerConfig.getInstance().getInfoHandlerTempDir() + TEMP_HTML_FILE;
		String sStyleSheet = null;
		String sRawDocument = null;
		URL urlStyleSheet = null;
		boolean includeToolBar = true;
		boolean multibar = true;
		boolean includeViewSource = false;
		boolean includeMenuIcons = true;
		boolean modeExclusive = true;
		boolean base64 = false;
		boolean debugOn = false;
		boolean spellCheck = false;
		boolean enterBreak = false;
    boolean infoHandlerModeStatus = true;
    String imageLibraryDirectory = InfoHandlerConfig.getInstance().getImagePath();

		Ekit ekit = new Ekit(this, sDocument, sStyleSheet, sRawDocument, urlStyleSheet, includeToolBar, includeViewSource, includeMenuIcons, modeExclusive, sLang, sCtry, base64, debugOn, spellCheck, multibar, enterBreak, infoHandlerModeStatus, imageLibraryDirectory);
    
  }
  
  private void loadHtmlContentFromFile(String fullPath)
  {
    String editedHtml = InfoHandlerFileUtil.getInstance().readFromFile(fullPath);
    if (editedHtml.isEmpty() == false)
    {
      //this function is used to compensate image format error from ekit which missing "file:" in front of image inserted
      editedHtml = InfoHandlerDatabaseManager.getInstance().processImageFormatInHtml(editedHtml);
      _editor.setText(editedHtml);
    }
  }
  
  private void refreshHtmlContent()
  {
    if(_isDatabaseValid == true && _infoEnumCodeComboBox.getSelectedItem() != null)
    {
      _editor.setText(InfoHandlerDatabaseManager.getInstance().getTagHtml(_infoEnumCodeComboBox.getSelectedItem().toString()));
    }
  }
  
  public void validateInfoTagDialog()
  {
    if (_isDatabaseValid)
    {
      final List<String> finalMissingTagList = InfoHandlerDatabaseManager.getInstance().validateTagList();
      if (finalMissingTagList.isEmpty() == false)
      {
        SwingUtils.invokeLater(new Runnable()
        {

          public void run()
          {
            createValidateInfoTagDialog(finalMissingTagList);
          }
        });
      }
    }
  }
  
  private void createValidateInfoTagDialog(List<String> missingTagList)
  {
    _validateMissingTagDlg = new ValidateMissingTagDialog(this,
      StringLocalizer.infoHandlerKeyToString("VALIDATE_TAG_GUI_NAME_KEY"),
      true, missingTagList);
    SwingUtils.centerOnComponent(_validateMissingTagDlg, this);
    _validateMissingTagDlg.setVisible(true);
  }

  /**
   * @author swee-yee.wong
   */
  public boolean validateMediaFile()
  {
    boolean isMissingFiles = false;
    if(_isDatabaseValid)
    {
      final List<String> finalMissingMediaList = InfoHandlerDatabaseManager.getInstance().getMissingMediaList();

      if (finalMissingMediaList.isEmpty() == false)
      {
        isMissingFiles = true;
        SwingUtils.invokeLater(new Runnable()
        {
          public void run()
          {
            createValidateMediaFileDialog(finalMissingMediaList);
          }
        });
        _selectedLabel.setText(StringLocalizer.infoHandlerKeyToString("MISSING_FILES_MESSAGE_KEY"));
      }
      else
      {
        _selectedLabel.setText(StringLocalizer.infoHandlerKeyToString("NO_MISSING_FILES_MESSAGE_KEY"));
      }
    }
    return isMissingFiles;
  }
  
  private void createValidateMediaFileDialog(List<String> missingMediaList)
  {
    _validateFileDlg = new ValidateMediaFileDialog(this,
      StringLocalizer.infoHandlerKeyToString("VALIDATE_MEDIA_FILES_GUI_NAME_KEY"),
      true, missingMediaList);
    SwingUtils.centerOnComponent(_validateFileDlg, this);
    _validateFileDlg.setVisible(true);
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void clearComboList()
  {
    _editor.setText("");
    _infoEnumCodeComboBox.removeAllItems();
    _comboBoxFullList.removeAll(_comboBoxFullList);
  }
  
  /**
   * @author swee-yee.wong
   */
  public void addFullListToComboBox()
  {
    clearComboList();
    _editor.setText("");
    if(_isDatabaseValid)
    {
      _infoTagFullList = InfoHandlerDatabaseManager.getInstance().getDatabaseFullList();

      for (String tagName : _infoTagFullList)
      {
        _infoEnumCodeComboBox.addItem(tagName);
        _comboBoxFullList.add(tagName);
      }
    }
  }
  
  /**
   * @author swee-yee.wong
   */
  public void addInfoListToComboBox(List<String> infoList)
  {
    clearComboList();
    for (String tagName : infoList)
    {
      _infoEnumCodeComboBox.addItem(tagName);
      _comboBoxFullList.add(tagName);
      _infoEnumCodeComboBox.setSelectedItem(tagName);
    }
  }
  
  /**
   * @author swee-yee.wong
   */
  public void addInfoTagToComboBox(String tag)
  {
    boolean exists = false;
    for (int index = 0; index < _infoEnumCodeComboBox.getItemCount() && !exists; index++)
    {
      if (tag.equals(_infoEnumCodeComboBox.getItemAt(index)))
      {
        exists = true;
      }
    }
    if (!exists)
    {
      _infoEnumCodeComboBox.addItem(tag);
      _comboBoxFullList.add(tag);
    }
    _infoEnumCodeComboBox.setSelectedItem(tag);
  }
  
  public void clearSearchTextField()
  {
    _infoEnumSearchTextField.setText("");
  }
  
  public void resetSearchList()
  {
    if (_isSearching == true)
    {
      _isSearching = false;
      addInfoListToComboBox(_searchList);
      _searchList.removeAll(_searchList);
    }
  }
  
  
  public void setFullDisplayMode(boolean fullDisplayMode)
  {
    _isFullDisplayMode = fullDisplayMode;
    _editContentButton.setVisible(InfoHandlerConfig.getInstance().isEditButtonVisible(fullDisplayMode));
    _edit.setEnabled(InfoHandlerConfig.getInstance().isDialogEditable(fullDisplayMode));
    _editContentButton.setEnabled(InfoHandlerConfig.getInstance().isDialogEditable(fullDisplayMode));
    if(InfoHandlerConfig.getInstance().isTreeVisible(fullDisplayMode))
    {
      _splitPane.setDividerLocation(0.2);
    }
    else
    {
      _splitPane.setDividerLocation(0.0);
    }
    
    _infoEnumSearchTextField.setEditable(true);
    _infoEnumCodeComboBox.setEnabled(true);
    _tree.setEnabled(true);
    _okButton.setEnabled(true);
    _editor.setEditable(false);
  }
  
  public void removeObservers()
  {
    _databaseObservable.deleteObserver(this);
  }
  
  public synchronized void update(final Observable observable, final Object object)
  {
    SwingUtils.invokeLater(new Runnable()
    {
      public void run()
      {
        if (observable instanceof DatabaseObservable)
        {
          DatabaseChangeEvent databaseChangeEvent = (DatabaseChangeEvent)object;
          DatabaseChangeTypeEnum databaseChangeTypeEnum = databaseChangeEvent.getDatabaseChangeTypeEnum();
          if (databaseChangeTypeEnum == DatabaseChangeTypeEnum.INSERT)
          {
            String tagName = databaseChangeEvent.getTagName();
            addInfoTagToComboBox(tagName);
            insertNewNode(_root, tagName, tagName);
            _selectedLabel.setText(StringLocalizer.infoHandlerKeyToString("TAG_INSERTED_MESSAGE_KEY"));
          }
          else if (databaseChangeTypeEnum == DatabaseChangeTypeEnum.DELETE)
          {
            String tagName = databaseChangeEvent.getTagName();
            _infoEnumCodeComboBox.removeItem(tagName);
            _comboBoxFullList.remove(tagName);
            deleteNode(_root, null, tagName, tagName);
            _selectedLabel.setText(StringLocalizer.infoHandlerKeyToString("TAG_DELETED_MESSAGE_KEY"));
          }
          else if(databaseChangeTypeEnum == DatabaseChangeTypeEnum.UPDATE_VERSION)
          {
            double databaseVersion = databaseChangeEvent.getVersion();
            setTitle(StringLocalizer.infoHandlerKeyToString("INFO_HANDLER_GUI_NAME_KEY") + " " + databaseVersion);
          }
        }
      }
    });
  }
  
  private void unpopulateData()
  {
    _infoTagFullList.removeAll(_infoTagFullList);
    _comboBoxFullList.removeAll(_comboBoxFullList);
    _searchList.removeAll(_searchList);
  }
  
  private void populateData()
  {
    _isDatabaseValid = InfoHandlerDatabaseManager.getInstance().getDatabaseConnection();
    if (_isDatabaseValid == false)
    {
      clearComboList();
      if (_tree != null)
      {
        deleteAllTreeNode();
      }
      setTitle(StringLocalizer.infoHandlerKeyToString("DATABASE_NOT_FOUND_MESSAGE_KEY"));
      _selectedLabel.setText(StringLocalizer.infoHandlerKeyToString("DATABASE_NOT_FOUND_MESSAGE_KEY"));
    }

    else
    {
      unpopulateData();
      double databaseVersion = InfoHandlerDatabaseManager.getInstance().getDatabaseVersion();
      setTitle(StringLocalizer.infoHandlerKeyToString("INFO_HANDLER_GUI_NAME_KEY") + " " + databaseVersion);
      _infoTagFullList = InfoHandlerDatabaseManager.getInstance().getDatabaseFullList();
      InfoHandlerDatabaseManager.getInstance().unpopulateVersion();

      if (_tree != null)
      {
        deleteAllTreeNode();
        addFullListToComboBox();
        addTreeNode();
        validateMediaFile();
        validateInfoTagDialog();
      }
    }
  }
  
  /**
   * @author bee-hoon.goh
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      if (InfoHandlerConfig.getInstance().isUsingLanguageEqualLocale() == false)
      {
        InfoHandlerDatabaseManager.getInstance().closeConnection();
        InfoHandlerConfig.getInstance().resetLanguage();
        InfoHandlerDatabaseManager.getInstance().openConnection();
        populateData();
        if (_isDatabaseValid == true)
        {
          refreshHtmlContent();
        }
      }
//      InfoHandlerDatabaseManager.getInstance().closeConnection();
      _infoHandlerPanel.setInfoHandlerVisible(false);
      _infoEnumSearchTextField.setText("");
      this.setVisible(false);
    }
  }
  
  public void setWarningIcon()
  {
    this.setIconImage(Image5DX.getImage(Image5DX.CD_FAIL));
  }
  
  public void setDefaultIcon()
  {
    this.setIconImage(Image5DX.getImage(Image5DX.INFO_HANDLER_FRAME_ICON_NAME));
  }
  
}
