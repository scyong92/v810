package com.axi.v810.util;

import com.axi.util.*;
import com.axi.v810.gui.psp.FringePatternDisplayModeEnum;
import com.axi.v810.hardware.*;
import java.awt.event.*;

import javax.media.opengl.*;
import javax.media.opengl.awt.*;

/**
 * This is Fringe Pattern Java OpenGL class. It responsible to draw Fringe Pattern
 * or white light.
 * 
 * @author Cheah Lee Herng
 */
public class FringePattern implements GLEventListener, KeyListener
{
    public static double DEFAULT_GLOBAL_INTENSITY = 1.0;
    public static int DEFAULT_PERIOD = 20;
    public static int DEFAULT_XXL_PERIOD = 12;
    
    private static double _c;
    private static int _offset;
    private int _shift;    
    private int _period;
    private static double _globalIntensity;
    
    private GLProfile _glp;
    private GLCapabilities _cap;
    private static GLCanvas _canvas;
    private FringePatternDisplayModeEnum _displayMode;
    
    private static boolean _isFinishDisplay = false;
    private FringePatternObservable _fringePatternObservable = FringePatternObservable.getInstance();
    
    /**
     * @author Cheah Lee Herng 
     */
    public FringePattern(int offset, int period)
    {
        GLProfile.initSingleton(true);
        
        _glp = GLProfile.getDefault();
        _cap = new GLCapabilities(_glp);
        _canvas = new GLCanvas(_cap);
        _isFinishDisplay = false;
        
        _c = 1;
        _offset = offset;
        _shift = 0;
        _period = period;
        _globalIntensity = DEFAULT_GLOBAL_INTENSITY;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public FringePattern(GLProfile glp, GLCapabilities cap, GLCanvas canvas, int offset, int period)
    {
        _glp = glp;
        _cap = cap;
        _canvas = canvas;
        _isFinishDisplay = false;
        
        _c = 1;
        _offset = offset;
        _shift = 0;
        _period = period;
        _globalIntensity = DEFAULT_GLOBAL_INTENSITY;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public FringePattern(int offset, int period, double c, double globalIntensity)
    {
        _glp = GLProfile.getDefault();
        _cap = new GLCapabilities(_glp);
        _canvas = new GLCanvas(_cap);
        _isFinishDisplay = false;
        
        _c = c;
        _offset = offset;
        _shift = 0;
        _period = period;
        _globalIntensity = globalIntensity;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public double calcCol(double angleInRadian)
    {
        return ((Math.sin(angleInRadian) + 1) / 2);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public static double getIntensityRatio()
    {
        return _c;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void setIntensityRatio(double c)
    {
        _c = c;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public static double getGlobalIntensity()
    {
        return _globalIntensity;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void setGlobalIntensity(double globalIntensity)
    {
        _globalIntensity = globalIntensity;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public GLCanvas getGLCanvas()
    {
        return _canvas;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public int getShift()
    {
        return _shift;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void setShift(int shift)
    {
        _shift = shift;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static int getOffset()
    {
        return _offset;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void setOffset(int offset)
    {
        _offset = offset;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void setDisplayMode(FringePatternDisplayModeEnum fringePatternDisplayMode)
    {
        Assert.expect(fringePatternDisplayMode != null);
        _displayMode = fringePatternDisplayMode;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public FringePatternDisplayModeEnum getDisplayMode()
    {
        Assert.expect(_displayMode != null);
        return _displayMode;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void calculateShiftBasedOnDisplayMode()
    {
        Assert.expect(_displayMode != null);
        
        if (_displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_ONE)
            _shift = _offset;
        else if (_displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_TWO)
            _shift = _offset + (_period / 4);
        else if (_displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_THREE)
            _shift = _offset + (_period / 2);
        else if (_displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_FOUR)
            _shift = _offset + ((_period / 4) * 3);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void init(GLAutoDrawable gLDrawable) 
    {
        Assert.expect(_displayMode != null);
        
        // Reset display flag
        _isFinishDisplay = false;
        
        // We need to re-calculate the shift value so that we can display different fringe pattern
        calculateShiftBasedOnDisplayMode();
        
        GL2 gl = gLDrawable.getGL().getGL2();
        gl.glShadeModel(GL2.GL_SMOOTH);// Enable Smooth Shading
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);// Black Background

        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(0, 640, 0, 480, 0, 1);
        gl.glDisable(GL.GL_DEPTH_TEST);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        // Displacement trick for exact pixelization
        gl.glTranslatef(0.375f, 0.375f, 0.0f);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

        //gLDrawable.addKeyListener(this);
        _canvas.addKeyListener(this);
    }

    /**
     * @author Cheah Lee Herng 
     */
    public void dispose(GLAutoDrawable glad) 
    {
        // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void setFinishDisplay(boolean finishDisplay)
    {
        _isFinishDisplay = finishDisplay;
    }

    public void display(GLAutoDrawable gLDrawable) 
    {
        Assert.expect(_displayMode != null);
        
        // We need to re-calculate the shift value so that we can display different fringe pattern
        calculateShiftBasedOnDisplayMode();
        
        GL2 gl = gLDrawable.getGL().getGL2();
        gl.glClear(GL.GL_COLOR_BUFFER_BIT);
        
        double R;
        double G;
        double B;
        double m = (1.0 - _c) / 479.0;
        double ratio;


        gl.glPushMatrix();
            if (_displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_ONE ||
                _displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_TWO ||
                _displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_THREE ||
                _displayMode == FringePatternDisplayModeEnum.FRINGE_PATTERN_SHIFT_FOUR)
            {
                gl.glBegin(GL2.GL_QUAD_STRIP);
                    R = G = B = calcCol((_period - _shift) * ((3.142 * 2) / _period));
                    ratio = (m * 479) + _c;
                    gl.glColor3f((float)(R * ratio * _globalIntensity), (float)(G * ratio * _globalIntensity), (float)(B * ratio * _globalIntensity));
                    gl.glVertex2f(639, 479);
                    gl.glVertex2f(0, 479);
                    for (int count = 1; count < _shift; count++)
                    {
                        R = G = B = calcCol((_period - _shift + count) * ((3.142 * 2) / _period));
                        ratio = (m * (479 - count)) + _c;
                        gl.glColor3f((float)(R * ratio * _globalIntensity), (float)(G * ratio * _globalIntensity), (float)(B * ratio * _globalIntensity));
                        gl.glVertex2f(639, 479 - count);
                        gl.glVertex2f(0, 479 - count);
                    }
                    for (int count = _shift; count < 480; count++)
                    {
                        R = G = B = calcCol((count - _shift) * ((3.142 * 2) / _period));
                        ratio = (m * (479 - count)) + _c;
                        gl.glColor3f((float)(R * ratio * _globalIntensity), (float)(G * ratio * _globalIntensity), (float)(B * ratio * _globalIntensity));
                        gl.glVertex2f(639, 479 - count);
                        gl.glVertex2f(0, 479 - count);
                    }
                gl.glEnd();
            }
            else if (_displayMode == FringePatternDisplayModeEnum.WHITE_LIGHT)
            {
                gl.glBegin(GL2.GL_QUADS);
                    gl.glColor3f((float)_globalIntensity, (float)_globalIntensity, (float)_globalIntensity);
                    gl.glVertex2f(639, 479);
                    gl.glVertex2f(0, 479);
                    gl.glColor3f((float)_c * (float)_globalIntensity, (float)_c * (float)_globalIntensity, (float)_c * (float)_globalIntensity);
                    gl.glVertex2f(0, 0);
                    gl.glVertex2f(639, 0);

                gl.glEnd();
            }            
        gl.glPopMatrix();

        if (_canvas != null)
        {
            _canvas.repaint();
            
            if (_canvas.isShowing())
            {
                if (_isFinishDisplay == false)
                {
                    _fringePatternObservable.sendEvent(new FringePatternInfo(OpticalCameraIdEnum.OPTICAL_CAMERA_1, FringePatternEnum.FINISH_DISPLAY_FRINGE_PATTERN));
                    _isFinishDisplay = true;
                }
            }
        }
    }

    /**
     * @author Cheah Lee Herng 
     */
    public void reshape(GLAutoDrawable gLDrawable, int i, int i1, int width, int height) 
    {
        GL2 gl = gLDrawable.getGL().getGL2();
        if (height <= 0) // avoid a divide by zero error!
            height = 1;

        final float h = (float)width / (float)height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        //glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glOrtho(0, 640, 0, 480, 0, 1);
        gl.glMatrixMode (GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslatef(0.375f, 0.375f, 0.0f);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    }

    /**
     * @author Cheah Lee Herng 
     */
    public void keyTyped(KeyEvent e) 
    {
        // Do nothing
    }

    /**
     * @author Cheah Lee Herng 
     */
    public void keyPressed(KeyEvent e) 
    {
//        if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
//            System.exit(0);
//        if (e.getKeyChar() == '+')
//        {
//            if (_c + 0.05 <= 1.0)
//                _c += 0.05;
//        }
//        if (e.getKeyChar() == '-')
//        {
//            if (_c - 0.05 >= 0.0)
//                _c -= 0.05;
//        }
//        if (e.getKeyChar() == '2')
//            _displayMode = FringePatternDisplayModeEnum.WHITE_LIGHT;
//        if (e.getKeyChar() == '3')
//        {
//            if (_globalIntensity - 0.05 > 0.0)
//                _globalIntensity-=0.05;
//        }
//        if (e.getKeyChar() == '4')
//        {
//            if (_globalIntensity + 0.05 < 1.0)
//                _globalIntensity+=0.05;
//        }
//
//        _canvas.repaint();
    }

    /**
     * @author Cheah Lee Herng
     */
    public void keyReleased(KeyEvent e) 
    {
        // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void increaseIntensityRatio()
    {   
        Assert.expect(_canvas != null);
        
        if (_c + 0.05 <= 1.0)
            _c += 0.05;
        
        _canvas.repaint();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void decreaseIntensityRatio()
    {
        Assert.expect(_canvas != null);
        
        if (_c - 0.05 >= 0.0)
            _c -= 0.05;
        
        _canvas.repaint();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void increaseGlobalIntensity()
    {
        Assert.expect(_canvas != null);
        
        if (_globalIntensity + 0.05 < 1.0)
            _globalIntensity+=0.05;
        
        _canvas.repaint();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static void decreaseGlobalIntensity()
    {
        Assert.expect(_canvas != null);
        
        if (_globalIntensity - 0.05 > 0.0)
            _globalIntensity-=0.05;
        
        _canvas.repaint();
    }
}
