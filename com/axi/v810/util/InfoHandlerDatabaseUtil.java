/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.util;

import java.sql.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.database.*;
import java.text.*;
import java.util.*;

/**
 * @author swee-yee.wong
 */
public class InfoHandlerDatabaseUtil
{

  private AbstractDatabase _database = AbstractDatabase.getInstance();
  private Connection _connection;
  private static final DatabaseTypeEnum PACKAGE_LIBRARY_DATABASE_TYPE_ENUM = DatabaseTypeEnum.PACKAGE_LIBRARY_DATABASE;
  public List<String> _infoTagFullList = new ArrayList<String>();
  private boolean _isConnectionOpen = false;
  private static String _versionTag = "version";
  private String _createInfoHandlerTable = "CREATE TABLE InfoHandler (ID INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY, PARENT_ID INTEGER NOT NULL, TAG_NAME varchar(255) NOT NULL, HTML_S LONG VARCHAR, MEDIA LONG VARCHAR, DATE_S VARCHAR(255), VERSION DOUBLE)";
  
  
  /**
   * @author Swee Yee Wong
   */
  public void openDatabaseConnection(String libraryPath) throws DatastoreException
  {

    try
    {
      // Get JDBC connection
      String jdbcUrl = _database.getJdbcUrl(libraryPath, false);
      _connection = RelationalDatabaseUtil.openConnection(jdbcUrl,
        _database.getDatabaseUserName(),
        _database.getDatabasePassword(),
        PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
      _isConnectionOpen = true;
    }
    catch (DatastoreException e)
    {
      System.out.println("Failed to open database connection.");
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void closeDatabaseConnection() throws DatastoreException
  {
    try
    {
      // Close connection
      RelationalDatabaseUtil.closeConnection(_connection, PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
      _connection = null;
      _isConnectionOpen = false;
    }
    catch (DatastoreException e)
    {
      System.out.println("Failed to close database connection.");
    }
    finally
    {
      //_database.shutdownDatabase();
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void createInfoHandlerDatabase(String libraryPath) throws DatastoreException
  {
    if (_isConnectionOpen)
    {
      try
      {
        closeDatabaseConnection();
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to close database driver.");
      }
    }
    // Startup database
    _database.startupDatabase();
    
    try
    {
      // Get JDBC connection
      String jdbcUrl = _database.getJdbcUrl(libraryPath, true);
      _connection = RelationalDatabaseUtil.openConnection(jdbcUrl,
        _database.getDatabaseUserName(),
        _database.getDatabasePassword(),
        PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);

      // Create statement object
      Statement statement = RelationalDatabaseUtil.createStatement(_connection);

      // Create RefDataType table
      RelationalDatabaseUtil.executeUpdate(statement, _createInfoHandlerTable);

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(statement);

      // Close connection
      RelationalDatabaseUtil.closeConnection(_connection, PACKAGE_LIBRARY_DATABASE_TYPE_ENUM);
    }
    finally
    {
      _database.shutdownDatabase();
      if (_isConnectionOpen == false)
      {
        try
        {
          openDatabaseConnection(libraryPath);
        }
        catch (DatastoreException e)
        {
          System.out.println("Failed to open database driver.");
        }
      }
    }
  }

  public boolean insertInfoHandlerDatabase(String libraryPath, int tagParentID, String tagName, String htmlContent, String mediaList, double newVersion) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }
    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        // Close statement
        RelationalDatabaseUtil.closeStatement(st1);
        return false;
      }
      else
      {
        String currentDateTime = getCurrentDateTime();
        // Create RefDataType table
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "INSERT into InfoHandler (PARENT_ID,TAG_NAME,HTML_S, MEDIA,DATE_S,VERSION) VALUES (?,?,?,?,?,?)");
        st.setInt(1, tagParentID);
        st.setString(2, tagName);
        st.setString(3, htmlContent);
        st.setString(4, mediaList);
        st.setString(5, currentDateTime);
        st.setDouble(6, newVersion);
        int rstu2 = st.executeUpdate();
        updateTagDateTimeDatabase(libraryPath, "version", currentDateTime);
        InfoHandlerConfig.getInstance().saveDatabaseDateToConfig(currentDateTime);
        // Commit changes
        RelationalDatabaseUtil.commit(_connection);

        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
      return true;
    }

    catch (SQLException err)
    {
      System.out.println(err.getMessage());
      return false;
    }

  }

  public boolean deleteInfoHandlerDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        // Create RefDataType table
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "DELETE from InfoHandler where TAG_NAME = ?");
        st.setString(1, tagName);
        int rstu2 = st.executeUpdate();
        // Commit changes
        RelationalDatabaseUtil.commit(_connection);

        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }
      else
      {
        //info msg
        System.out.println(tagName + " is not found in database.");
        // Close statement
        RelationalDatabaseUtil.closeStatement(st1);
        return false;
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
      return true;
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
      return false;
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public boolean isTagExistsDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    boolean tagExists = false;
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {

      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);
      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        tagExists = true;
      }
      else
      {
        tagExists = false;
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);
      return tagExists;
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
      return tagExists;
    }
  }

  public void printInfoHandlerDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {

      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);
      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        int id1 = rst.getInt(1);
        int id2 = rst.getInt(2); 
        String s1 = rst.getString(3);
        String s2 = rst.getString(4);
        String s3 = rst.getString(5);
        String s4 = rst.getString(6);
        Double date1 = rst.getDouble(7);
        //Sets Records in TextFields.
        //String p = s1 + " " + s2;
        System.out.println(id1);
        System.out.println(id2);
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
        System.out.println(date1.toString());
      }
      else
      {
        System.out.println(tagName + " is not found in database.");
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);

    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public String getTagHtmlDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    String htmlContent = null;
    
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);

      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        htmlContent = rst.getString(4);
      }
      else
      {
        System.out.println(tagName + " is not found in database.");
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);

    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
    return htmlContent;
  }

  /**
   * @author Swee Yee Wong
   */
  public String getTagMediaListDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    String mediaList = null;
    
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);

      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        mediaList = rst.getString(5);
      }
      else
      {
        System.out.println(tagName + " is not found in database.");
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);

    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
    return mediaList;
  }

  /**
   * @author Swee Yee Wong
   */
  public String getTagDateTimeDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    String dateTime = null;
    
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);

      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        dateTime = rst.getString(6);
      }
      else
      {
        System.out.println(tagName + " is not found in database.");
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
    return dateTime;
  }

  /**
   * @author Swee Yee Wong
   */
  public double getTagVersionDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    double databaseVersion = 0;
    
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }
    
    try
    {
      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);

      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        databaseVersion = rst.getDouble(7);
      }
      else
      {
        System.out.println(tagName + " is not found in database.");
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
    return databaseVersion;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public int getTagIDDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    int tagID = 0;
    
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }
    
    try
    {
      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);

      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        tagID = rst.getInt(1);
      }
      else
      {
        System.out.println(tagName + " is not found in database.");
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
    return tagID;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public int getTagParentIDDatabase(String libraryPath, String tagName) throws DatastoreException
  {
    int parentID = 0;
    
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }
    
    try
    {
      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st.setString(1, tagName);

      ResultSet rst = st.executeQuery();

      if (rst.next())
      {
        parentID = rst.getInt(2);
      }
      else
      {
        System.out.println(tagName + " is not found in database.");
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
    return parentID;
  }

  /**
   * @author Swee Yee Wong
   */
  public void updateTagDatabase(String libraryPath, int parentID, String tagName, String htmlContent, String mediaList, double newVersion) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        String currentDateTime = getCurrentDateTime();
        //PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "INSERT into InfoHandler (TAG_NAME,HTML_S,DATE_S,VERSION) VALUES ('version','" + _defaultHtml + "', '9/7/2014 08:44:00', 1.0)");
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "UPDATE InfoHandler set PARENT_ID=?, HTML_S=? , MEDIA=?, DATE_S=?, VERSION=? where TAG_NAME=?");
        st.setInt(1, parentID);
        st.setString(2, htmlContent);
        st.setString(3, mediaList);
        st.setString(4, currentDateTime);
        st.setDouble(5, newVersion);
        st.setString(6, tagName);
        int rstu2 = st.executeUpdate();
        // Commit changes
        RelationalDatabaseUtil.commit(_connection);
        updateTagDateTimeDatabase(libraryPath, "version", currentDateTime);
        InfoHandlerConfig.getInstance().saveDatabaseDateToConfig(currentDateTime);
        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }
      else
      {
        //info msg
        System.out.println(tagName + " is not found in database.");
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
  }
  
  

  /**
   * @author Swee Yee Wong
   */
  public void updateTagHtmlContentDatabase(String libraryPath, String tagName, String htmlContent, double newVersion) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        String currentDateTime = getCurrentDateTime();
        // Create statement object
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "UPDATE InfoHandler set HTML_S=?, DATE_S=?, VERSION=? where TAG_NAME=?");

        st.setString(1, htmlContent);
        st.setString(2, currentDateTime);
        st.setDouble(3, newVersion);
        st.setString(4, tagName);

        int rstu2 = st.executeUpdate();

        // Commit changes
        RelationalDatabaseUtil.commit(_connection);
        updateTagDateTimeDatabase(libraryPath, "version", currentDateTime);
        InfoHandlerConfig.getInstance().saveDatabaseDateToConfig(currentDateTime);
        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }
      else
      {
        //info msg
        System.out.println(tagName + " is not found in database.");
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void updateTagParentIDDatabase(String libraryPath, String tagName, int parentID, double newVersion) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        String currentDateTime = getCurrentDateTime();
        // Create statement object
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "UPDATE InfoHandler set PARENT_ID=?, DATE_S=?, VERSION=? where TAG_NAME=?");

        st.setInt(1, parentID);
        st.setString(2, currentDateTime);
        st.setDouble(3, newVersion);
        st.setString(4, tagName);

        int rstu2 = st.executeUpdate();

        // Commit changes
        RelationalDatabaseUtil.commit(_connection);
        updateTagDateTimeDatabase(libraryPath, "version", currentDateTime);
        InfoHandlerConfig.getInstance().saveDatabaseDateToConfig(currentDateTime);
        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }
      else
      {
        //info msg
        System.out.println(tagName + " is not found in database.");
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void updateTagMediaListDatabase(String libraryPath, String tagName, String mediaList, double newVersion) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        // Create statement object
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "UPDATE InfoHandler set MEDIA=?, DATE_S=?, VERSION=? where TAG_NAME=?");
        String currentDateTime = getCurrentDateTime();
        st.setString(1, mediaList);
        st.setString(2, currentDateTime);
        st.setDouble(3, newVersion);
        st.setString(4, tagName);

        int rstu2 = st.executeUpdate();

        // Commit changes
        RelationalDatabaseUtil.commit(_connection);
        updateTagDateTimeDatabase(libraryPath, "version", currentDateTime);
        InfoHandlerConfig.getInstance().saveDatabaseDateToConfig(currentDateTime);
        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }
      else
      {
        //info msg
        System.out.println(tagName + " is not found in database.");
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public void updateTagVersionDatabase(String libraryPath, String tagName, double newVersion) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        String currentDateTime = getCurrentDateTime();
        // Create statement object
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "UPDATE InfoHandler set DATE_S=?, VERSION=? where TAG_NAME=?");

        st.setString(1, currentDateTime);
        st.setDouble(2, newVersion);
        st.setString(3, tagName);

        int rstu2 = st.executeUpdate();

        // Commit changes
        RelationalDatabaseUtil.commit(_connection);
        updateTagDateTimeDatabase(libraryPath, "version", currentDateTime);
        InfoHandlerConfig.getInstance().saveDatabaseDateToConfig(currentDateTime);
        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }
      else
      {
        //info msg
        System.out.println(tagName + " is not found in database.");
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void updateTagDateTimeDatabase(String libraryPath, String tagName, String dateTime) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      // Create statement object
      PreparedStatement st1 = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler where TAG_NAME=?");
      st1.setString(1, tagName);

      ResultSet rst = st1.executeQuery();

      if (rst.next())
      {
        // Create statement object
        PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "UPDATE InfoHandler set DATE_S=? where TAG_NAME=?");

        st.setString(1, dateTime);
        st.setString(2, tagName);

        int rstu2 = st.executeUpdate();

        // Commit changes
        RelationalDatabaseUtil.commit(_connection);
        // Close statement
        RelationalDatabaseUtil.closeStatement(st);
      }
      else
      {
        //info msg
        System.out.println(tagName + " is not found in database.");
      }

      // Close statement
      RelationalDatabaseUtil.closeStatement(st1);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }
  }

  /**
   * @author Swee Yee Wong
   */
  public List<String> getInfoTagFullListDatabase(String libraryPath) throws DatastoreException
  {
    if (_isConnectionOpen == false || _connection == null)
    {
      try
      {
        openDatabaseConnection(libraryPath);
      }
      catch (DatastoreException e)
      {
        System.out.println("Failed to open database driver.");
      }
    }

    try
    {
      _infoTagFullList.removeAll(_infoTagFullList);
      // Create statement object
      PreparedStatement st = RelationalDatabaseUtil.createPreparedStatement(_connection, "select * from InfoHandler");

      ResultSet rst = st.executeQuery();

      while (rst.next())
      {
        String tag = rst.getString(3);
        if (tag.equals(_versionTag) == false)
        {
          _infoTagFullList.add(tag);
        }
      }

      // Commit changes
      RelationalDatabaseUtil.commit(_connection);

      // Close statement
      RelationalDatabaseUtil.closeStatement(st);
    }
    catch (SQLException err)
    {
      System.out.println(err.getMessage());
    }

    return _infoTagFullList;
  }

  /**
   * @author Swee Yee Wong
   */
  public String getCurrentDateTime()
  {
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    //get current date time with Date()
    java.util.Date date = new java.util.Date();
    return dateFormat.format(date);
  }
  
  public boolean isDatabaseConnected()
  {
    return _isConnectionOpen;
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void startupDriver() throws DatastoreException
  {
    // Startup database
    _database.startupDatabase();
  }
  
  /**
   * @author Swee Yee Wong
   */
  public void shutdownDriver() throws DatastoreException
  {
    // Startup database
    _database.shutdownDatabase();
  }

}
