package com.axi.v810.util;

import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.commons.net.tftp.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * The purpose of this class is to provide support for Trivial File Transfer Protocl (TFTP) and
 * provide localized exception handling.
 *
 * This product includes software developed by the Apache Software Foundation (http://www.apache.org/).
 *
 * Copyright 2001-2005 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Greg Esparza
 */
public class TftpClientAxi
{
  private TFTPClient _tftpClient;
  private String _serverIpAddress;
  private List<String> _exceptionParameters;
  private TftpClientAxiCommunicationModeEnum _communicationMode;

  /**
   * @param serverIpAddress is the address of the remote server
   * @param communicationMode defines the communication mode such as ascii, binary, etc
   * @author Greg Esparza
   */
  public TftpClientAxi(String serverIpAddress, TftpClientAxiCommunicationModeEnum communicationMode)
  {
    Assert.expect(serverIpAddress != null);
    Assert.expect(serverIpAddress.length() > 0);
    Assert.expect(communicationMode != null);

    _serverIpAddress = serverIpAddress;
    _communicationMode = communicationMode;
    _exceptionParameters = new ArrayList<String>();
    _tftpClient = new TFTPClient();
  }

  /**
   * @param timeoutInMiliseconds
   * @author Wei Chin
   */
  public void setDefaultTimeout(int timeoutInMiliseconds)
  {
    _tftpClient.setDefaultTimeout(timeoutInMiliseconds);
  }

  /**
   * Open on the first available port on the local host.
   *
   * Note: the client of this class must call "close()" when they before they are
   *       done referencing this class.  Also, it is recommended that they "close()"
   *       after each send or receive transaction.
   *
   * @author Greg Esparza
   */
  public void open() throws XrayTesterException
  {
    try
    {
      if (_tftpClient.isOpen() == false)
        _tftpClient.open();
    }
    catch (SocketException se)
    {
      _exceptionParameters.add(_serverIpAddress);
      _exceptionParameters.add(se.getMessage());
      CommunicationHardwareException che = new CommunicationHardwareException("TFTP_CLIENT_FAILED_TO_OPEN_KEY", _exceptionParameters);
      che.initCause(se);
      throw che;
    }
  }

  /**
   * @author Greg Esparza
   */
  public void close()
  {
    if (_tftpClient.isOpen())
      _tftpClient.close();
  }

  /**
   * Send a file from the local mahcine to the remote machine
   *
   * @param localFileNameFullPath is the file name and location where to get the file to send.
   * @param remoteFileNameFullPath is the file name and location on the remote machine where the file is sent.
   * @author Greg Esparza
   */
  public void sendFile(String localFileNameFullPath, String remoteFileNameFullPath) throws XrayTesterException
  {
    Assert.expect(localFileNameFullPath != null);
    Assert.expect(remoteFileNameFullPath != null);

    FileInputStream localFIS = null;
    try 
    {
      localFIS = new FileInputStream(localFileNameFullPath);
      try
      {     
        _tftpClient.sendFile(remoteFileNameFullPath, _communicationMode.getCommunicationMode(), localFIS, InetAddress.getByName(_serverIpAddress));
      }
      catch (UnknownHostException uhe)
      {
        _exceptionParameters.clear();
        _exceptionParameters.add(_serverIpAddress);
        CommunicationHardwareException che = new CommunicationHardwareException("TFTP_CLIENT_DETECTED_UNKNOWN_HOST_KEY", _exceptionParameters);
        che.initCause(uhe);
        throw che;
      }
      catch (IOException ioe)
      {
        _exceptionParameters.clear();
        _exceptionParameters.add(_serverIpAddress);
        _exceptionParameters.add(localFileNameFullPath);
        _exceptionParameters.add(remoteFileNameFullPath);
        _exceptionParameters.add(ioe.getMessage());
        CommunicationHardwareException che = new CommunicationHardwareException("TFTP_CLIENT_FAILED_TO_SEND_FILE_KEY", _exceptionParameters);
        che.initCause(ioe);
        throw che;
      }
    } 
    catch (FileNotFoundException fnfe) 
    {
      DatastoreException ex = new CannotOpenFileDatastoreException(localFileNameFullPath);
      ex.initCause(fnfe);
      throw ex;
    }
    finally
    {
      if(localFIS != null)
      {
        try 
        {
          localFIS.close();
        } 
        catch (IOException ex) 
        {
          System.out.println("Error closing file \" " + localFileNameFullPath + " \" : " + ex.getMessage());
        }
        finally
        {
          localFIS = null;
        }
      }
    }
  }

  /**
   * Receive a file from the remote machine to the local machine
   *
   * @param localFileNameFullPath is the file name and location where to put the file that is received.
   * @param remoteFileNameFullPath is the file name and location on the remote machine where to get the file.
   * @author Greg Esparza
   */
  public void receiveFile(String localFileNameFullPath, String remoteFileNameFullPath) throws XrayTesterException
  {
    Assert.expect(localFileNameFullPath != null);
    Assert.expect(remoteFileNameFullPath != null);

    try
    {
      _tftpClient.receiveFile(remoteFileNameFullPath, _communicationMode.getCommunicationMode(), new FileOutputStream(localFileNameFullPath),InetAddress.getByName(_serverIpAddress));
    }
    catch (UnknownHostException uhe)
    {
      _exceptionParameters.clear();
      _exceptionParameters.add(_serverIpAddress);
      CommunicationHardwareException che = new CommunicationHardwareException("TFTP_CLIENT_DETECTED_UNKNOWN_HOST_KEY", _exceptionParameters);
      che.initCause(uhe);
      throw che;
    }
    catch (IOException ioe)
    {
      _exceptionParameters.clear();
      _exceptionParameters.add(_serverIpAddress);
      _exceptionParameters.add(localFileNameFullPath);
      _exceptionParameters.add(remoteFileNameFullPath);
      _exceptionParameters.add(ioe.getMessage());
      CommunicationHardwareException che = new CommunicationHardwareException("TFTP_CLIENT_FAILED_TO_RECEIVE_FILE_KEY", _exceptionParameters);
      che.initCause(ioe);
      throw che;
    }
  }
}
