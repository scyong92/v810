package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * This class should be used to compare many of the properties of the Image Set Data object. In order to use this class
 * just set the appropriate property to indicate which property you would like to compare. If you are comparing the
 * selected state of the image set data object you will need to also specify a map from image set data objects to its
 * selected state
 *
 * @author Erica Wheatcroft
 */
public class ImageSetComparator implements Comparator<ImageSetData>
{
  // property indicating if the comparator will compare the items in ascending order
  private boolean _ascending = true;

  // property indicating if we should compare by image set name
  private boolean _compareImageSetName = false;

  // property indicating if we should compare by image set project name
  private boolean _compareImageSetProjectName = false;

  // property indicating if we should compare by image set version
  private boolean _compareImageSetVersion = false;

  // property indicating if we should compare by image set date
  private boolean _compareImageSetDate = false;

  // property indicating if we should compare by the image set property
  private boolean _comapreImageSetPopulated = false;

  // property indicating if we should compare by the image set type
  private boolean _compareImageSetType = false;

  // property indicating if we should compare by the image set description
  private boolean _compareImageSetDescription = false;

  // property indicating if we should compare by the image set compatibility percentage.
  private boolean _compareImageSetPercentCompatable = false;

  // property indicating if we should compare by the image set selected property
  // in order to compare the selected state the imageSetToSelectedStateMap must
  // be set as well.
  private boolean _compareImageSetSelectedState = false;
  private Map<ImageSetData,Boolean> _imageSetToSelectedStateMap = null;

  /**
   * @author Erica Wheatcroft
   */
  public ImageSetComparator()
  {

  }

  /**
   * @author Erica Wheatcroft
   */
  public void setAscending(boolean compareInAscendingOrder)
  {
    _ascending = compareInAscendingOrder;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetNames()
  {
    clearAllFlags();
    _compareImageSetName = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetProjectNames()
  {
    clearAllFlags();
    _compareImageSetProjectName = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetVersions()
  {
    clearAllFlags();
    _compareImageSetVersion = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetDates()
  {
    clearAllFlags();
    _compareImageSetDate = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetPopulated()
  {
    clearAllFlags();
    _comapreImageSetPopulated = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetTypes()
  {
    clearAllFlags();
    _compareImageSetType = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetDescriptions()
  {
    clearAllFlags();
    _compareImageSetDescription = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetCompatablePercents()
  {
    clearAllFlags();
    _compareImageSetPercentCompatable = true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void compareImageSetSelectedStates(Map<ImageSetData, Boolean> imageSetToSelectedStatemap)
  {
    Assert.expect(imageSetToSelectedStatemap != null, "map is null");
    clearAllFlags();
    _compareImageSetSelectedState = true;
    _imageSetToSelectedStateMap = imageSetToSelectedStatemap;
  }

  /**
   * @author Erica Wheatcroft
   */
  private void clearAllFlags()
  {
    _compareImageSetName = false;
    _compareImageSetProjectName = false;
    _compareImageSetVersion = false;
    _compareImageSetDate = false;
    _comapreImageSetPopulated = false;
    _compareImageSetType = false;
    _compareImageSetDescription = false;
    _compareImageSetSelectedState = false;
    _compareImageSetPercentCompatable = false;
  }

  /**
   * Compares its two arguments for order.
   *
   * @param lhs the first object to be compared.
   * @param rhs the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
   *   than the second.
   *
   * @author Erica Wheatcroft
   */
  public int compare(ImageSetData lhs, ImageSetData rhs)
  {
    Assert.expect(lhs != null, "lhs image set data is null");
    Assert.expect(rhs != null, "rhs image set data is null");
    Assert.expect(_compareImageSetName ||  _compareImageSetProjectName || _compareImageSetVersion ||
                  _compareImageSetDate || _comapreImageSetPopulated ||
                  _compareImageSetType || _compareImageSetDescription ||
                  _compareImageSetSelectedState || _compareImageSetPercentCompatable, "one property must be set");

    if(_compareImageSetName)
      return compareImageSetName(lhs, rhs);
    else if(_compareImageSetProjectName)
      return compareImageSetProjectName(lhs, rhs);
    else if(_compareImageSetVersion)
      return compareImageSetVersion(lhs, rhs);
    else if(_compareImageSetDate)
      return compareImageSetDate(lhs, rhs);
    else if (_comapreImageSetPopulated)
      return compareImageSetPopulated(lhs, rhs);
    else if(_compareImageSetType)
      return compareImageSetTypes(lhs, rhs);
    else if(_compareImageSetDescription)
      return compareImageSetDescription(lhs,rhs);
    else if(_compareImageSetSelectedState)
      return compareImageSetSelectedState(lhs, rhs);
    else if(_compareImageSetPercentCompatable)
      return compareImageSetCompatablePercents(lhs, rhs);

    Assert.expect(false ," atleast one property must be set.");
    return 0;
  }

  /**
   * This method will compare the image set compatable percentage of the lhs and rhs objects.
   * @author Erica Wheatcroft
   */
  private int compareImageSetCompatablePercents(ImageSetData lhs, ImageSetData rhs)
  {
    Double lhPercent = lhs.getPercentOfCompatibleImages();
    Double rhPercent = rhs.getPercentOfCompatibleImages();

    if(_ascending)
      return lhPercent.compareTo(rhPercent);
    else
      return rhPercent.compareTo(lhPercent);
  }

  /**
   * this method will compare the image set name of the lhs and rhs objects
   * @author Erica Wheatcroft
   */
  private int compareImageSetName(ImageSetData lhs, ImageSetData rhs)
  {
    if (_ascending)
      return lhs.getSystemDescription().compareTo(rhs.getSystemDescription());
    else
      return rhs.getSystemDescription().compareTo(lhs.getSystemDescription());
  }

  /**
   * this method will compare the image set name of the lhs and rhs objects
   * @author Andy Mechtenberg
   */
  private int compareImageSetProjectName(ImageSetData lhs, ImageSetData rhs)
  {
    if (_ascending)
      return lhs.getProjectName().compareToIgnoreCase(rhs.getProjectName());
    else
      return rhs.getProjectName().compareToIgnoreCase(lhs.getProjectName());
  }

  /**
   * this method will compare the image set version of the lhs and rhs objects
   * @author Erica Wheatcroft
   */
  private int compareImageSetVersion(ImageSetData lhs, ImageSetData rhs)
  {
    Integer lhVersion = lhs.getTestProgramVersionNumber();
    Integer rhVersion = rhs.getTestProgramVersionNumber();
    if (_ascending)
      return lhVersion.compareTo(rhVersion);
    else
      return rhVersion.compareTo(lhVersion);
  }

  /**
   * This method will compare the image set creation date of the lhs and rhs objects
   * @author Erica Wheatcroft
   */
  private int compareImageSetDate(ImageSetData lhs, ImageSetData rhs)
  {
    Long lhLong = lhs.getDateInMils();
    Long rhLong = rhs.getDateInMils();

    if (_ascending)
      return lhLong.compareTo(rhLong);
    else
      return rhLong.compareTo(lhLong);
  }

  /**
   * This method will compare the image set populated property of the lhs and rhs objects
   * @author Erica Wheatcroft
   */
  private int compareImageSetPopulated(ImageSetData lhs, ImageSetData rhs)
  {
    Boolean lsBoardPopulated = lhs.isBoardPopulated();
    Boolean rhBoardPopulated = rhs.isBoardPopulated();

    if (_ascending)
      return lsBoardPopulated.compareTo(rhBoardPopulated);
    else
      return rhBoardPopulated.compareTo(lsBoardPopulated);
  }

  /**
   * This method will compare the image set types of the lhs and rhs objects
   * @author Erica Wheatcroft
   */
  private int compareImageSetTypes(ImageSetData lhs, ImageSetData rhs)
  {
    Integer lhTypeId = lhs.getImageSetTypeEnum().getId();
    Integer rhTypeId = rhs.getImageSetTypeEnum().getId();

    if (_ascending)
      return lhTypeId.compareTo(rhTypeId);
    else
      return rhTypeId.compareTo(lhTypeId);
  }

  /**
   * This method will compare the image set user defined description of the lhs
   * and rhs objects
   * @author Erica Wheatcroft
   */
  private int compareImageSetDescription(ImageSetData lhs, ImageSetData rhs)
  {
    String lhString = lhs.getUserDescription();
    String rhString = rhs.getUserDescription();

    if (_ascending)
      return lhString.compareTo(rhString);
    else
      return rhString.compareTo(lhString);
  }

  /**
   * This method will compare the selected state of the lhs and rhs objects
   * @author Erica Wheatcroft
   */
  private int compareImageSetSelectedState(ImageSetData lhs, ImageSetData rhs)
  {
    Assert.expect(_imageSetToSelectedStateMap != null, "image set to selected state map is null");

    //lets get the selected state of the lhs object.
    Assert.expect(_imageSetToSelectedStateMap.containsKey(lhs), "the map does not contain the Lhs");
    Assert.expect(_imageSetToSelectedStateMap.containsKey(rhs), "the map does not contain the rhs");

    Boolean lhSelectedState = _imageSetToSelectedStateMap.get(lhs);
    Boolean rhSelectedState = _imageSetToSelectedStateMap.get(rhs);

    if (_ascending)
      return lhSelectedState.compareTo(rhSelectedState);
    else
      return rhSelectedState.compareTo(lhSelectedState);

  }
}
