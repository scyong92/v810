package com.axi.v810.util;

import com.axi.util.*;

/**
 * The HashUtil class applies a hash function to a string such as a panel name
 * and returns a hashed directory name.  The original code for the methods
 * found in this class came from the Pascal file common.pas/src/hashunit.pas.<p>
 *
 * Copyright:    Copyright (c) 2000 Agilent Technologies, Inc.<p>
 * Company:      Agilent Technologies, Inc.<p>
 * @author Steve Anonson
 */
public class HashUtil
{
  /**
   * @author Steve Anonson
   */
  private HashUtil()
  {
    // do nothing
  }

  /**
   * This method creates a string representation of the given number in the
   * given base.  The base should not be larger than 36 since there are only
   * 36 letters and digits.
   * @author Steve Anonson
  */
  private static String baseX( long inum, int base )
  {
    Assert.expect(base >= 0);
    Assert.expect(base <= 36);

    StringBuffer carray = new StringBuffer("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    StringBuffer tstring = new StringBuffer();
    long curval = inum;
    int remainder = 0;
    int len = 0;

    do
    {
      remainder = (int)(curval % base);
      curval = curval / base;
      len++;
      tstring.append( carray.charAt(remainder) );
    } while ( curval != 0 );

    tstring.reverse();
    return tstring.toString();
  }

  /**
   * @author Steve Anonson
   */
  private static long myHash( String self, long p )
  {
    Assert.expect(self != null);

    long z = 0;
    String mySelf = self.toUpperCase();
    for( int i = 0; i < mySelf.length(); i++)
      z = ((int)mySelf.charAt(i) + (256 * z)) % p;
    return z;
  }

  /**
   * This is the public method used to convert a string into a hashed string.
   *
   * @param   self - String to be converted.
   * @return  String containing the hashed result.
   * @exception IllegalArgumentException if passed a null String.
   * @author Steve Anonson
   */
  public static String hashName( String self )
  {
    Assert.expect(self != null);

    // this duplicates the same functionality found in the legacy 5dx code
    String hashName = baseX( myHash( self, 99991 ), 36 ) +
                      baseX( myHash( self,   997 ), 36 ) +
                      baseX( myHash( self,   947 ), 36 );
    return hashName;
  }
}
