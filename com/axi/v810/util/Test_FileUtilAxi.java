package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * Tests the FileUtil5dx class
 * @author George A. David
 */
public class Test_FileUtilAxi extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_FileUtilAxi());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testReadBytes(os);
  }

  /**
   * @author George A. David
   */
  private void testReadBytes(PrintWriter os)
  {
    Assert.expect(os != null);

    String filePath = "data" + File.separator + "Test_FileUtil5dx" + File.separator + "xrayCameraFirmware.ace";
    byte[] bytes = null;
    try
    {
      bytes = FileUtilAxi.readBytes(filePath);
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false);
      ex.printStackTrace(os);
    }

    filePath = UnitTest.stripOffViewName(filePath);
    os.println("The following is the binary contents of the file " + filePath);
    os.println("The length of the file is " + bytes.length + " bytes.");
    for(int i = 0; i < bytes.length; ++i)
    {
      os.println("bytes[" + i + "] = " + bytes[i]);
    }
  }
}
