package com.axi.v810.util;

import com.axi.v810.hardware.*;
import com.axi.util.*;

/**
 * @author Roy Williams
 */
public class IPaddressException extends HardwareException
{
  /**
   * @author Roy Williams
   */
  public IPaddressException()
  {
    super(new LocalizedString("HW_NO_PRIVATE_IP_ADDRESS_KEY", new Object[]{}));
  }

}
