package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * This class starts up a DOS cmd executable and connects to its standard input, output and error
 * streams to give it commands and interact with it.
 *
 * Creating an instance of this class will create a cmd.exe process.  Call sendCommand() to send
 * commands to it.  Call exit() to kill the process.  If the process sends output to standard
 * out the DOSOutputInt.standardOutputLine(String line) method will be called with the line as
 * the input parameters.  If the process sends output to standard error the DOSOutputInt.standardErrorLine(String line)
 * method will be called.  It is up to you to implement this interface how you want.  It is recommended that
 * any standard error messages be converted into Java exceptions.  Note that these two calls will happen
 * asynchonously.  They are on their own threads.  Make sure your code can handle the multi-threaded
 * nature of these calls!  An alternative design is to have the sendCommand() call return back
 * the standard out and standard error lines as lists.  This design does not work if you are
 * calling an executable that will keep running for a long period of time and/or that wants input
 * sent to it at the same time that it is responding with messages.
 *
 * To have a programs standard input, standard output and standard error interact with this Java program
 * simply run the program using the method sendCommand("yourCommand");  If you want to start up
 * a program and do not need to interact with it from Java you can use Runtime.exec() or you can use this
 * class with a sendCommand("start yourExecutable");  Either of these approaches will let the user
 * interact with your program through the DOS shell.  In the first approach all interaction will happen
 * through Java instead of through the DOS window.
 *
 * You can have Pascal interact in two different ways with this class.  If the following lines
 * are the first things in your Pascal programs then the Java input, output and error streams will
 * get all information from the Pascal program.  The DOS shell will not get any output in this mode.  It
 * will sit there blank.  You can call the hideWindow() method to hide the DOS window entirely in this
 * mode.
 *    assign(input, '');
 *    reset(input);
 *    assign(output, '');
 *    rewrite(output);
 *  If the Pascal program does not have these lines then its ouput and input will go through the DOS
 *  window and Java will not see any of this data.  It is not possible to have Java interact with the
 *  DOS standard input and output and have that output display in the DOS window.  You have to choose
 *  one mode or the other.
 *
 * If the Pascal or any other executable needs to get input from standard in it must print to standard output
 * the exact same string as the _PROMPT variable in this class is set to, then it can do a read on standard in.  For
 * example:
 *    writeln('Waiting for some input now');
 *    writeln('MSLOPDOSREADY'); // THIS LINE MUST BE HERE BEFORE THE FLUSH AND THE READLN
 *    flush(Output);
 *    readln(input, inputLine);
 *    writeln('got the input:' + inputLine);
 *    flush(Output);
 *
 * COMMON PROBLEMS WITH OUR PASCAL CODE
 * - many of our pascal programs require that you cd into their directory before they are run
 * - our pascal programs will not run from a directory with more than 8 characters in it
 * - make sure you call prepareToRun16BitApps() before running one of the 5dx pascal executables.
 *
 * @author Bill Darbie
 */
public class DosShell
{
  // this needs be a string that will NEVER come out from normal output of any executable run
  private static final String _PROMPT = "MSLOPDOSREADY";
  private static final String _COMMAND_OK = "MSLOPDOS_CMD_OK";
  private static final String _COMMAND_FAILED = "MSLOPDOS_CMD_FAILED";
  private static final String _COMMAND_END = "&& echo " + _COMMAND_OK + " || echo " + _COMMAND_FAILED;

  private static int _instanceNumber = 0;
  private static String _startCommand;

  private BufferedReader _is;
  private BufferedReader _es;
  private PrintWriter _os;

  private ReadDosInputStreamThread _inputThread;
  private ReadDosErrorStreamThread _errorThread;

  private Process _cmdProcess;

  private DosOutputInt _dosOutputInt;

  // _exitReceived and _notified must be volatile to guarantee that two different
  // threads will both see the most recent value
  private volatile boolean _exitReceived = false;
  private volatile boolean _readyForNextCommand = false;

  private List<Boolean> _passed = Collections.synchronizedList(new LinkedList<Boolean>());

  /**
   * Create a dos cmd.exe process
   * @param dosOutputInt is the interface for standard error and standard output messages will go.
   *        If it is null then those messages will be ignored.
   *
   * @author Bill Darbie
   */
  public DosShell(DosOutputInt dosOutputInt) throws IOException
  {
    // it is OK for dosOutputInt to be null
    _dosOutputInt = dosOutputInt;
    ++_instanceNumber;
    startDosCommandShell();
  }

  /**
   * Fire up a DOS command shell.
   *
   * @author Bill Darbie
   */
  private void startDosCommandShell() throws IOException
  {
    _startCommand = "cmd.exe";
    _cmdProcess = Runtime.getRuntime().exec(_startCommand + " /a");

    InputStream is = _cmdProcess.getInputStream();
    InputStream es = _cmdProcess.getErrorStream();
    InputStreamReader isr = new InputStreamReader(is);
    InputStreamReader esr = new InputStreamReader(es);
    _is = new BufferedReader(isr);
    _es = new BufferedReader(esr);

    OutputStream os = _cmdProcess.getOutputStream();
    _os = new PrintWriter(os);

    _inputThread = new ReadDosInputStreamThread(this,
                                                _is,
                                                _PROMPT,
                                                _COMMAND_END,
                                                _COMMAND_OK,
                                                _COMMAND_FAILED,
                                                _dosOutputInt);
    _inputThread.setDaemon(true);
    _inputThread.start();

    _errorThread = new ReadDosErrorStreamThread(_es, _dosOutputInt);
    _errorThread.setDaemon(true);
    _errorThread.start();

    // set the dos prompt to something we can easily recognize
    _os.println("prompt " + _PROMPT);
    _os.flush();

    hideWindow();
  }

  /**
   * Cd to the directory specified.  This method will automatically
   * switch to the correct directory letter first (if one is given).
   * Then it will cd to the directory.
   *
   * @return true if the command is successful
   * @author Bill Darbie
   */
  public synchronized boolean cd(String directory)
  {
    Assert.expect(directory != null);

    String dir = "";
    // replace / with \\
    for(int i = 0; i < directory.length(); ++i)
    {
      char ch = directory.charAt(i);
      if (ch == '/')
        dir = dir + "\\";
      else
        dir = dir + ch;
    }

    return sendCommand("cd /d " + dir);
  }

  /**
   * Send dos an exit command.
   * @author Bill Darbie
   */
  public synchronized void exit()
  {
    // send a command to tell Dos to exit
    sendCommand("exit");

    // wait for the exitReceived() method to be called before doing anything else
    while (_exitReceived == false)
    {
      try
      {
        wait(200);
      }
      catch(InterruptedException e)
      {
        // do nothing
      }
    }
    _exitReceived = false;

    try
    {
      _cmdProcess.waitFor();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }

    disconnectFromDos();

    // get the exitValue - a saw a description of a bug saying that if you do not call this things may hang
    // so I am calling it just in case
    _cmdProcess.exitValue();
    // destory the dos shell
    _cmdProcess.destroy();
  }

  /**
   * Stop the input and error threads and close the streams.
   * @author Bill Darbie
   */
  private void disconnectFromDos()
  {
    // now stop the threads so we can cleanly close the streams
    _inputThread.stopThread();
    _errorThread.stopThread();

    // close all the streams
    _os.close();
    try
    {
      _is.close();
    }
    catch(IOException ioe)
    {
      // do nothing
    }
    try
    {
      _es.close();
    }
    catch(IOException ioe)
    {
      // do nothing
    }
  }

  /**
   * The input stream thread will call this method when a dos command has finished.
   * The sendCommand must wait until this is called before it sends the next command
   * or Dos will not behave properly
   *
   * @author Bill Darbie
   */
  synchronized void readyForNextCommand(boolean passed)
  {
    // the first time this method is called the passed parameter should
    // not be used since it is not valid
    _passed.add(new Boolean(passed));

    _readyForNextCommand = true;
    notifyAll();
  }

  /**
   * The ReadDOSInputStreamThread will call this once it sees that an exit command was sent
   * to DOS.
   */
  synchronized void exitReceived()
  {
    _exitReceived = true;
    notifyAll();
  }

  /**
   * Send a command to the Dos shell.
   *
   * @return true if the command was successful (meaning that the command returned a 0 to the OS)
   * @author Bill Darbie
   */
  public synchronized boolean sendCommand(String command)
  {
    Assert.expect(command != null);

    _os.println(command + _COMMAND_END);
    _os.flush();
    try
    {
      while ((_readyForNextCommand == false) && (_exitReceived == false))
      {
        // only wait if a notify has not already come through - otherwise we will deadlock here
        wait(200);
        // remember that the readyForNextCommand() call that calls the notifyAll() method will
        // set _readyForNextCommand to true, so at this point in the code _readyForNextCommand is true
      }
      _readyForNextCommand = false;
    }
    catch(InterruptedException ie)
    {
      // do nothing
    }

    // if we were told to exit the DosSHell entirely, then return right away
    if (_exitReceived)
      return true;

    Assert.expect(_passed != null);
    Assert.expect(_passed.size() > 0);
    Boolean passed = (Boolean)_passed.remove(0);

    return passed.booleanValue();
  }

  /**
   * Hide the DOS shell window completely.
   * @author Bill Darbie
   */
  public void hideWindow()
  {
    if (UnitTest.unitTesting() == false)
    {
      Runtime runtime = Runtime.getRuntime();
      String title = _startCommand + "." + Integer.toString(_instanceNumber);
      sendCommand("title " + title);
      try
      {
        Process process = runtime.exec("hideWindow " + title);
        process.waitFor();  // let the process finish -- it should be fast!
      }
      catch(IOException e)
      {
        // do nothing
      }
      catch (InterruptedException ie)  // needed by the waitFor()
      {
        // do nothing
      }
    }
  }

  /**
   * Make the dos window visible.
   * @author Bill Darbie
   */
  public void showWindow()
  {
    Runtime runtime = Runtime.getRuntime();
    String title = _startCommand + "." + _instanceNumber;
    sendCommand("title " + title);
    try
    {
      Process process = runtime.exec("showWindow " + title);
      process.waitFor();  // let the process finish -- it should be fast!
    }
    catch(IOException e)
    {
      // do nothing
    }
    catch (InterruptedException ie)  // needed by the waitFor()
    {
      // do nothing
    }
  }
}
