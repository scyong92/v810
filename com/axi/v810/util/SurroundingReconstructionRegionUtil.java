package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;


/**
 * Given a list of renderes it will determine the list of renderers that fall in a rectangle
 * @author George A. David
 */
public class SurroundingReconstructionRegionUtil<K extends ReconstructionRegion>
{
  private Map<Double, Set<K>> _xCoordToReconstructionRegionSetMap = new TreeMap<Double, Set<K>>();
  private Map<Double, Set<K>> _yCoordToReconstructionRegionSetMap = new TreeMap<Double, Set<K>>();

  private List<Double> _xCoords;
  private List<Double> _yCoords;

  private Set<K> _topReconstructionRegions = new HashSet<K>();
  private Set<K> _bottomReconstructionRegions = new HashSet<K>();
  private boolean _useBoundsForDeterminingVicinity = false;
  private double _largestWidthInNanos = -1;
  private double _largestLengthInNanos = -1;

  /**
   * @author George A. David
   */
  public void setUseBoundsForDeterminingVicinity(boolean useBoundsForDeterminigVicinity)
  {
    _useBoundsForDeterminingVicinity = useBoundsForDeterminigVicinity;
  }

  /**
   * @author George A. David
   */
  public void setReconstructionRegions(Collection<K> reconstructionRegions)
  {
    Assert.expect(reconstructionRegions != null);

    _largestWidthInNanos = -1;
    _largestLengthInNanos = -1;
    _xCoordToReconstructionRegionSetMap.clear();
    _yCoordToReconstructionRegionSetMap.clear();
    _topReconstructionRegions.clear();
    _bottomReconstructionRegions.clear();

    for(K region : reconstructionRegions)
      addReconstructionRegion(region);

    _xCoords = new ArrayList<Double>(_xCoordToReconstructionRegionSetMap.keySet());
    _yCoords = new ArrayList<Double>(_yCoordToReconstructionRegionSetMap.keySet());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void clearReconstructionRegions()
  {
    _largestWidthInNanos = -1;
    _largestLengthInNanos = -1;
    _xCoordToReconstructionRegionSetMap.clear();
    _yCoordToReconstructionRegionSetMap.clear();
    _topReconstructionRegions.clear();
    _bottomReconstructionRegions.clear();
  }

  /**
   * @author George A. David
   */
  private void addReconstructionRegion(K reconstructionRegion)
  {
    PanelRectangle rect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    if(_useBoundsForDeterminingVicinity)
    {
      addReconstructionRegion(reconstructionRegion, rect.getMinX(), _xCoordToReconstructionRegionSetMap);
      addReconstructionRegion(reconstructionRegion, rect.getMaxX(), _xCoordToReconstructionRegionSetMap);
      addReconstructionRegion(reconstructionRegion, rect.getMinY(), _yCoordToReconstructionRegionSetMap);
      addReconstructionRegion(reconstructionRegion, rect.getMaxY(), _yCoordToReconstructionRegionSetMap);
      _largestWidthInNanos = Math.max(_largestWidthInNanos, rect.getWidth());
      _largestLengthInNanos = Math.max(_largestLengthInNanos, rect.getHeight());
    }
    else
    {
      addReconstructionRegion(reconstructionRegion, rect.getCenterX(), _xCoordToReconstructionRegionSetMap);
      addReconstructionRegion(reconstructionRegion, rect.getCenterY(), _yCoordToReconstructionRegionSetMap);
    }

    if(reconstructionRegion.isTopSide())
      _topReconstructionRegions.add(reconstructionRegion);
    else
      _bottomReconstructionRegions.add(reconstructionRegion);
  }

  /**
   * @author George A. David
   */
  private void addReconstructionRegion(K reconstructionRegion, double coordValue,  Map<Double, Set<K>> coordToReconstructionRegionSetMap)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(coordToReconstructionRegionSetMap != null);

    if(coordToReconstructionRegionSetMap.containsKey(coordValue))
    {
      coordToReconstructionRegionSetMap.get(coordValue).add(reconstructionRegion);
    }
    else
    {
      Set<K> reconstructionRegionSet = new HashSet<K>();
      reconstructionRegionSet.add(reconstructionRegion);
      coordToReconstructionRegionSetMap.put(coordValue, reconstructionRegionSet);
    }
  }

  /**
   * @author George A. David
   */
  public Set<K> getIntersectingReconstructionRegionsInRectangle(PanelRectangle rectangle, boolean isTopSide)
  {
    Set<K> reconstructionRegions = getIntersectingReconstructionRegionsInRectangle(rectangle);
    if(isTopSide)
      reconstructionRegions.retainAll(_topReconstructionRegions);
    else
      reconstructionRegions.retainAll(_bottomReconstructionRegions);

    return reconstructionRegions;
  }

  /**
   * @author George A. David
   */
  public Set<K> getIntersectingReconstructionRegionsInRectangle(PanelRectangle rectangle)
  {
    Set<K> reconstructionRegions = getIntersectingReconstructionRegionsBetweenValues(rectangle.getMinX(),
                                                                         rectangle.getMaxX(),
                                                                         _xCoords,
                                                                         _xCoordToReconstructionRegionSetMap,
                                                                         _largestWidthInNanos);
    reconstructionRegions.retainAll(getIntersectingReconstructionRegionsBetweenValues(rectangle.getMinY(),
                                                                          rectangle.getMaxY(),
                                                                          _yCoords,
                                                                          _yCoordToReconstructionRegionSetMap,
                                                                          _largestLengthInNanos));
    return reconstructionRegions;
  }

  /**
   * @author George A. David
   */
  private Set<K> getIntersectingReconstructionRegionsBetweenValues(double minValue,
                                                                   double maxValue,
                                                                   List<Double> coords,
                                                                   Map<Double, Set<K>> coordToReconstructionRegionSetMap,
                                                                   double largestDimension)
  {
    Assert.expect(coordToReconstructionRegionSetMap != null);
    Assert.expect(maxValue >= minValue);


    Set<K> reconstructionRegions = new HashSet<K>();

    int beginIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, minValue);
    int endIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, maxValue);

    if(beginIndex < 0)
      beginIndex = 0;

    if (endIndex > coords.size() - 1)
      endIndex = coords.size() - 1;

    for(int i = beginIndex; i <= endIndex; ++i)
    {
      double coord = coords.get(i);
      reconstructionRegions.addAll(coordToReconstructionRegionSetMap.get(coord));
    }

    if(beginIndex > 0 && endIndex < coords.size() - 1)
    {
      // ok, now because we are using the extents, we may have a
      // reconstruciton whose extents are beyond this rectangle, but we still want
      // to report it. so let's check for any regions that cross this intersection.

      // we don't want to always start with 0 index, this slows us down too much
      // let's be smart about this, we know the max size of a reconstruction region, so
      // let's use that to help us limit the search.
      double smallestStartingCoordinate = maxValue - largestDimension;

      int smallestIndex = ListUtil.findIndexOfValueGreaterThanOrEqualTo(coords, smallestStartingCoordinate);

      Set<K> beginRegions = new HashSet<K>();
      for (int i = smallestIndex; i < beginIndex; ++i)
      {
        double coord = coords.get(i);
        beginRegions.addAll(coordToReconstructionRegionSetMap.get(coord));
      }

      double largestEndingCoordinate = minValue + largestDimension;
      int biggestIndex = ListUtil.findIndexOfValueLessThanOrEqualTo(coords, largestEndingCoordinate);
      Set<K> endRegions = new HashSet<K>();
      for (int i = endIndex + 1; i <= biggestIndex; ++i)
      {
        double coord = coords.get(i);
        endRegions.addAll(coordToReconstructionRegionSetMap.get(coord));
      }
      beginRegions.retainAll(endRegions);
      reconstructionRegions.addAll(beginRegions);
    }

    return reconstructionRegions;
  }

  /**
   * @author George A. David
   */
  private Set<K> getSurroundingReconstructionRegions(double coordValue, List<Double> coords,  Map<Double, Set<K>> coordToReconstructionRegionSetMap)
  {
    Assert.expect(coordToReconstructionRegionSetMap != null);

    Set<K> reconstructionRegions = new HashSet<K>();

    Pair<Integer, Integer> pair = ListUtil.findIndiciesOfSurroundingValues(coords, coordValue);
    int previousIndex = pair.getFirst();
    int nextIndex = pair.getSecond();

    for(int i = previousIndex; i <= nextIndex; ++i)
      reconstructionRegions.addAll(coordToReconstructionRegionSetMap.get(coords.get(i)));

    return reconstructionRegions;
  }

  /**
   * @author George A. David
   */
  public Collection<K> getSurroundingReconstructionRegions(K reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    PanelCoordinate coord = reconstructionRegion.getCenterCoordinateRelativeToPanelInNanoMeters();
    Set<K> regions = getSurroundingReconstructionRegions(coord.getX(), _xCoords, _xCoordToReconstructionRegionSetMap);
    regions.retainAll(getSurroundingReconstructionRegions(coord.getY(), _yCoords, _yCoordToReconstructionRegionSetMap));

    return regions;
  }

  /**
   * @author George A. David
   */
  public Collection<K> getSurroundingReconstructionRegions(K reconstructionRegion, boolean isTopSide)
  {
    Assert.expect(reconstructionRegion != null);

    PanelCoordinate coord = reconstructionRegion.getCenterCoordinateRelativeToPanelInNanoMeters();
    Set<K> regions = getSurroundingReconstructionRegions(coord.getX(), _xCoords, _xCoordToReconstructionRegionSetMap);
    regions.retainAll(getSurroundingReconstructionRegions(coord.getY(), _yCoords, _yCoordToReconstructionRegionSetMap));
    if(isTopSide)
      regions.retainAll(_topReconstructionRegions);
    else
      regions.retainAll(_bottomReconstructionRegions);

    return regions;
  }

  /**
   * @author George A. David
   */
  public Collection<K> getReconstructionRegionsAtXcoordinate(int xCoordinate)
  {
    Set<K> regions = new HashSet<K>();
    regions.addAll(_xCoordToReconstructionRegionSetMap.get(xCoordinate));

    return regions;
  }

  /**
   * @author George A. David
   */
  public Collection<K> getReconstructionRegionsAtYcoordinate(int yCoordinate)
  {
    Set<K> regions = new HashSet<K>();
    regions.addAll(_yCoordToReconstructionRegionSetMap.get(yCoordinate));

    return regions;
  }

  /**
   * @author George A. David
   */
  public void printDebugInfo()
  {
//    System.out.println("x map");
//    for(Integer integer : _xCoordToPadSetMap.keySet())
//    {
//      System.out.println("xCoord: " + integer);
//      System.out.print("Pads: ");
//      for(K pad : _xCoordToPadSetMap.get(integer))
//      {
//        System.out.print(pad.getName() + " ");
//      }
//      System.out.println();
//    }
//
//    System.out.println("y map");
//    for(Integer integer : _yCoordToPadSetMap.keySet())
//    {
//      System.out.println("yCoord: " + integer);
//      System.out.print("Pads: ");
//      for(K pad : _yCoordToPadSetMap.get(integer))
//      {
//        System.out.print(pad.getName() + " ");
//      }
//      System.out.println();
//    }
  }
}
