package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * Represents a coordineate in board space.  The origin is the lower left corner of the board.
 * The x axis grows to the right and the y axis grows up.
 *
 * @author Bill Darbie
 */
public class BoardCoordinate extends IntCoordinate implements Serializable
{
  /**
   * @author Matt Wharton
   */
  public BoardCoordinate()
  {
    super();
  }

  /**
   * @author Matt Wharton
   */
  public BoardCoordinate(BoardCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * @author Laura Cormos
   */
  public BoardCoordinate(IntCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * @param x the x value of this coordinate.
   * @param y the y value of this coordinate.
   * @author Matt Wharton
   */
  public BoardCoordinate(int x, int y)
  {
    super(x, y);
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }
}
