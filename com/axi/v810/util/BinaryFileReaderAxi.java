package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
/**
 * This class is a wrapper class for the com.axi.util.BinaryFileReader class. It converts all the non x6k exceptions
 * thrown by the BinaryFileReader class into DatastoreExceptions
 *
 * @author Ronald Lim
 */
public class BinaryFileReaderAxi
{

  /**
   * @author Ronald Lim
   */

  public byte[] readFileInByte(String filename ) throws DatastoreException
  {
    byte[] outputBytes = null;

    try
    {
      BinaryFileReader fileReader = new BinaryFileReader();
      outputBytes = fileReader.readFileInByte(filename);
    }
    catch(FileNotFoundException fnfe)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filename);
      dex.initCause(fnfe);
      throw dex;
    }
    catch(IOException ioe)
    {
       DatastoreException dex = new CannotReadDatastoreException(filename);
       dex.initCause(ioe);
       throw dex;
    }

    return outputBytes;
  }
}
