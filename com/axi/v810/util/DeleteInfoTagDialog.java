/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author swee-yee.wong
 */
public class DeleteInfoTagDialog extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _deleteTagPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private BorderLayout _deleteTagPanelBorderLayout = new BorderLayout();
  private FlowLayout _okPanelFlowLayout = new FlowLayout();
  private JLabel _deleteTagLabel = new JLabel();
  private JTextField _deleteTagTextField = new JTextField();
  private JButton _okButton = new JButton();
  private JButton _cancelButton = new JButton();
  private static InfoHandlerDatabaseManager _infoHandlerDatabaseManager;
  
  private String _defaultTagName;
  
  DeleteInfoTagDialog (JFrame frame, String title, boolean modal, InfoHandlerDatabaseManager infoHandlerDatabaseManager, String defaultTagName)
  {
    super(frame, title, modal);
    
    _infoHandlerDatabaseManager = infoHandlerDatabaseManager;
    _defaultTagName = defaultTagName;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      //info msg
      System.out.println("Failed to open Delete Info Tag Dialog.");
    }
  }
  
  private void jbInit() throws Exception
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _mainPanel.setPreferredSize(new Dimension(300, 100));
    _deleteTagPanel.setLayout(_deleteTagPanelBorderLayout);
    _okCancelPanel.setLayout(_okPanelFlowLayout);
    _okPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _deleteTagLabel.setPreferredSize(new Dimension(185, 20));
    _deleteTagLabel.setText(StringLocalizer.infoHandlerKeyToString("DELETE_TAG_MESSAGE_KEY")); 
    _deleteTagTextField.setPreferredSize(new Dimension (250,30));
    _deleteTagTextField.setText(_defaultTagName);
    
    //swee yee wong
    _okButton.setText(StringLocalizer.infoHandlerKeyToString("GUI_INFO_OK_BUTTON_KEY"));
    _okButton.setEnabled(true);
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    
        //swee yee wong
    _cancelButton.setText(StringLocalizer.infoHandlerKeyToString("GUI_INFO_CANCEL_BUTTON_KEY"));
    _cancelButton.setEnabled(true);
    _cancelButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    
    getContentPane().add(_mainPanel);
    getRootPane().setDefaultButton(_okButton);
    _mainPanel.add(_deleteTagPanel, BorderLayout.CENTER);
    _deleteTagPanel.add(_deleteTagLabel, BorderLayout.NORTH);
    _deleteTagPanel.add(_deleteTagTextField, BorderLayout.CENTER);
    _mainPanel.add(_okCancelPanel, BorderLayout.SOUTH);
    _okCancelPanel.add(_okButton);
    _okCancelPanel.add(_cancelButton);
  }
  
  private void okButton_actionPerformed(ActionEvent e)
  {
    if(_deleteTagTextField.getText().isEmpty() == false)
    {
      _infoHandlerDatabaseManager.deleteInfoHandler(_deleteTagTextField.getText());
      unpopulate();
      dispose();
    }
  }
  
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    unpopulate();
    dispose();
  }
  
  /**
   * @author swee-yee.wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      unpopulate();
      dispose();
    }
  }

  private void unpopulate()
  {
    _mainPanel = null;
    _deleteTagPanel = null;
    _okCancelPanel = null;
    _mainPanelBorderLayout = null;
    _deleteTagPanelBorderLayout = null;
    _okPanelFlowLayout = null;
    _deleteTagLabel = null;
    _deleteTagTextField = null;
    _okButton = null;
    _cancelButton = null;
    _infoHandlerDatabaseManager = null;
    _defaultTagName = null;
  }
}
