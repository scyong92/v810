package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;


/**
 * @author Andy Mechtenberg
 */
public class Test_AlgorithmSettingDisplayOrderComparator extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmSettingDisplayOrderComparator());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // component sort
    Project project = null;
    String panelName = "Test_panel_1";
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    try
    {
      //delete the panel file if it exists, we first want to test loading ndf files.
      String projectFullPath = FileName.getProjectSerializedFullPath(panelName);
      if (FileUtil.exists(projectFullPath))
      {
        try
        {
          FileUtilAxi.delete(FileName.getProjectSerializedFullPath(panelName));
        }
        catch (DatastoreException dex)
        {
          Expect.expect(false);
        }
      }

      project = Project.importProjectFromNdfs(panelName, warnings);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    Expect.expect(project != null);
    Expect.expect(warnings.size() == 0);

    Algorithm algorithm = InspectionFamily.getAlgorithm(InspectionFamilyEnum.GULLWING, AlgorithmEnum.SHORT);
    List<AlgorithmSetting> settings = algorithm.getAlgorithmSettings(JointTypeEnum.GULLWING);
    Collections.sort(settings, new AlgorithmSettingDisplayOrderComparator());

    AlgorithmSetting setting = settings.get(0);
    Expect.expect(setting.getName().equalsIgnoreCase("minimum short thickness"));
    setting = settings.get(1);
    Expect.expect(setting.getName().equalsIgnoreCase("Region Inner Edge Location"));
    setting = settings.get(2);
    Expect.expect(setting.getName().equalsIgnoreCase("Region Outer Edge Location"));
  }

}
