package com.axi.v810.util;

import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.commons.net.bsd.*;

import com.axi.util.*;

/**
 * The purpose of this class is to provide support for Remote Execution (REXEC) of commands and
 * provide localized exception handling.
 *
 * This product includes software developed by the Apache Software Foundation (http://www.apache.org/).
 *
 * Copyright 2001-2005 The Apache Software Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Greg Esparza
 */
public class RexecClientAxi
{
  private static final boolean _USE_SEPARATE_ERROR_STREAM = true;

  private String _command;
  private String _userName;
  private String _password;
  private String _serverIpAddress;
  private RExecClient _rExecClient;
  private InputStream _errorStream;
  private InputStream _inputStream;
  private List<String> _exceptionParameters;

  /**
   * @author Greg Esparza
   */
  public RexecClientAxi(String serverIpAddress, String userName, String password)
  {
    Assert.expect(serverIpAddress != null);
    Assert.expect(userName != null);
    Assert.expect(password != null);

    _userName = userName;
    _password = password;
    _serverIpAddress = serverIpAddress;
    _rExecClient = new RExecClient();
    _errorStream = null;
    _inputStream = null;
    _exceptionParameters = new ArrayList<String>();
  }

  /**
   * @author Greg Esparza
   */
  public void sendMessage(String command) throws XrayTesterException
  {
    try
    {
      connect();
      rexec(command);
      waitForRexecCommandToComplete();
      disconnect();
    }
    catch (XrayTesterException mainXte)
    {
      try
      {
        disconnect();
      }
      catch (XrayTesterException xte)
      {
        xte.printStackTrace();
      }

      throw mainXte;
    }
  }

  /**
   * @author Greg Esparza
   */
  public String sendMessageAndReceiveReply(String command) throws XrayTesterException
  {
    Assert.expect(command != null);

    String reply = "";

    try
    {
      connect();
      rexec(command);
      reply = getInputStreamResult();
      verifyReply(reply);
      disconnect();
    }
    catch (XrayTesterException mainXte)
    {
      try
      {
        disconnect();
      }
      catch (XrayTesterException xte)
      {
        xte.printStackTrace();
      }

      throw mainXte;
    }

    return reply;
  }

  /**
   * @author Greg Esparza
   */
  private void setCommonExceptionParameters()
  {
    _exceptionParameters.clear();
    _exceptionParameters.add(_serverIpAddress);
  }

  /**
   * @author Greg Esparza
   */
  private void connect() throws XrayTesterException
  {
    try
    {
      _rExecClient.connect(InetAddress.getByName(_serverIpAddress));
    }
    catch (UnknownHostException uhe)
    {
      setCommonExceptionParameters();
      CommunicationHardwareException che = new CommunicationHardwareException("REXEC_CLIENT_DETECTED_UNKNOWN_HOST_KEY", _exceptionParameters);
      che.initCause(uhe);
      throw che;
    }
    catch (IOException ioe)
    {
      setCommonExceptionParameters();
      _exceptionParameters.add(ioe.getMessage());
      CommunicationHardwareException che = new CommunicationHardwareException("REXEC_CLIENT_FAILED_TO_CONNECT_TO_SERVER_KEY", _exceptionParameters);
      che.initCause(ioe);
      throw che;
    }
  }

  /**
   * @author Greg Esparza
   */
  private void disconnect() throws XrayTesterException
  {
    try
    {
      if (_rExecClient.isConnected())
        _rExecClient.disconnect();
    }
    catch (IOException ioe)
    {
      setCommonExceptionParameters();
      _exceptionParameters.add(ioe.getMessage());
      CommunicationHardwareException che = new CommunicationHardwareException("REXEC_CLIENT_FAILED_TO_DISCONNECT_FROM_SERVER_KEY", _exceptionParameters);
      che.initCause(ioe);
      throw che;
    }
  }

  /**
   * Note: the remote machine will have an "xinet" or "inet" service running.  This service will respond to this
   *       rexec request and will spawn an rexec daemon.  However, the "inet" service will only allow (N) requests per minute.
   *       If the user of this RexecClientAxi class is running many "sendMessage" or "sendMessageAndReceiveReply"
   *       commands, then the "inet" service on the remote machine might detect a maximum of (N) requests and will stop servicing
   *       any new requests.  For example, one unfortunate behavior of the "inet" service is that it will detect the (N+1)
   *       request and stop servicing. This will cause this client to hang because the "inet" service fails
   *       creating an error stream.  The "inet" service denies the (N+1) request but it never resumes after one minute
   *       has expired so this RexecClientAxi class can either resume the command or error with timeout.
   *
   *       One solution is increase the "inet" service (N) requests per minute value or modify the user of this class
   *       to send more than one command in a single "sendMessage" or "sendMessageAndReceiveReply". For example, the
   *       user of this class could have a command like the following:
   *
   *       From : sendMessage("cp <file1> <file2>")
   *              sendMessage("chmod a+w <file2>")
   *
   *       To : sendMessage("cp <file1> <file2> && chmod a+w <file2>");
   *
   * @author Greg Esparza
   */
  private void rexec(String command) throws XrayTesterException
  {
    Assert.expect(command != null);
    Assert.expect(command.length() > 0);

    try
    {
      _command = command;
      _rExecClient.rexec(_userName, _password, command, _USE_SEPARATE_ERROR_STREAM);
    }
    catch (IOException ioe)
    {
      setCommonExceptionParameters();
      _exceptionParameters.add(command);
      _exceptionParameters.add(ioe.getMessage());
      CommunicationHardwareException che = new CommunicationHardwareException("REXEC_CLIENT_FAILED_EXECUTING_COMMAND_KEY", _exceptionParameters);
      che.initCause(ioe);
      throw che;
    }
  }

  /**
   * @author Greg Esparza
   */
  private String getInputStreamResult() throws XrayTesterException
  {
    String message = "";
    _inputStream = _rExecClient.getInputStream();
    InputStreamReader inputStreamReader = new InputStreamReader(_inputStream);
    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

    try
    {
      int lineCount = 1;
      String line = new String();
      while (line != null)
      {
        line = bufferedReader.readLine();
        if (line != null)
        {
          if (lineCount == 1)
            message += line;
          else
            message += "\n" + line;

          ++lineCount;
        }
      }
    }
    catch (IOException ioe)
    {
      setCommonExceptionParameters();
      _exceptionParameters.add(ioe.getMessage());
      CommunicationHardwareException che = new CommunicationHardwareException("REXEC_CLIENT_FAILED_TO_GET_SERVER_INPUT_STREAM_RESULT_MESSAGE_KEY", _exceptionParameters);
      che.initCause(ioe);
      throw che;
    }
    finally
    {
      try
      {
        bufferedReader.close();
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }

    checkForServerErrors();

    return message;
  }

  /**
   * @author Greg Esparza
   */
  private void waitForRexecCommandToComplete() throws XrayTesterException
  {
    // We need to read from the input stream at least once to ensure that we reach the end of file signal.
    // This signal indicates that the command completed.
    getInputStreamResult();
  }

  /**
   * @author Greg Esparza
   */
  private void verifyReply(String reply) throws XrayTesterException
  {
    Assert.expect(reply != null);

    if (reply.length() == 0)
    {
      setCommonExceptionParameters();
      throw new CommunicationHardwareException("REXEC_CLIENT_FAILED_TO_DETECT_COMMAND_RESULT_KEY", _exceptionParameters);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void checkForServerErrors() throws XrayTesterException
  {
    String message = getServerErrorMessage();
    if (message.length() > 0)
    {
      setCommonExceptionParameters();
      _exceptionParameters.add(_command);
      _exceptionParameters.add(message);
      throw new CommunicationHardwareException("REXEC_CLIENT_FAILED_EXECUTING_COMMAND_KEY", _exceptionParameters);
    }
  }

  /**
   * @author Greg Esparza
   */
  private String getServerErrorMessage() throws XrayTesterException
  {
    String message = "";
    _errorStream = _rExecClient.getErrorStream();
    InputStreamReader inputStreamReader = new InputStreamReader(_errorStream);
    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

    try
    {
      int lineCount = 1;
      String line = new String();
      while (line != null)
      {
        line = bufferedReader.readLine();
        if (line != null)
        {
          if (lineCount == 1)
            message += line;
          else
            message += "\n" + line;

          ++lineCount;
        }
      }
    }
    catch (IOException ioe)
    {
      setCommonExceptionParameters();
      _exceptionParameters.add(ioe.getMessage());
      CommunicationHardwareException che = new CommunicationHardwareException("REXEC_CLIENT_FAILED_TO_GET_SERVER_ERROR_MESSAGE_KEY", _exceptionParameters);
      che.initCause(ioe);
      throw che;
    }
    finally
    {
      try
      {
        bufferedReader.close();
      }
      catch (IOException ioe)
      {
        // do nothing
      }
    }

    return message;
  }
}
