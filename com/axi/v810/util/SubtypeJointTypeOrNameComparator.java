package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This class will be used to compare CustomAlgorithmFamily objects alphanumerically by their names
 * @author George A. David
 */

public class SubtypeJointTypeOrNameComparator implements Comparator<Subtype>
{
  private boolean _ascending;
  private boolean _sortOnJointType;

  /**
   * @author George A. David
   */
  public SubtypeJointTypeOrNameComparator(boolean sortOnJointType, boolean ascending)
  {
    _sortOnJointType = sortOnJointType;
    _ascending = ascending;
  }

  /**
   * @author George A. David
   */
  public int compare(Subtype lhs, Subtype rhs)
  {
    // check to make sure that both objects are not null
    Assert.expect(lhs != null, "SubtypeJointTypeOrNameComparator.compare() - left hand side object is null");
    Assert.expect(rhs != null, "SubtypeJointTypeOrNameComparator.compare() - right hand side object is null");

    String lhString;
    String rhString;
    if (_sortOnJointType)
    {
      lhString = StringLocalizer.keyToString(lhs.getJointTypeEnum().getName());
      rhString = StringLocalizer.keyToString(rhs.getJointTypeEnum().getName());
    }
    else // sort on subtype name
    {
      lhString = lhs.getShortName();
      rhString = rhs.getShortName();
    }

    if (_ascending)
      return lhString.compareTo(rhString);
    else
      return rhString.compareTo(lhString);
  }
}
