package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;


/**
 * This class defines a rectangle in with respect to the system fiducial.  The system fiducial
 * is dependent on the scan direction.  Left Feature 1 on the fixed rail is the system fiducial
 * if the x scan direction is right to left.  Right Feature 1 on the fixed rail is the system
 * fiducial if the x scan direction is left to right.  The x-axis grows right and the y-axis grows up.
 * The unit is nanometers.
 *
 * @author Dave Ferguson
 */
public class SystemFiducialRectangle extends IntRectangle implements Serializable
{
  /**
   * Constructor.
   *
   * @author Dave Ferguson
   */
  public SystemFiducialRectangle(int bottomLeftX, int bottomLeftY, int width, int height)
  {
    super(bottomLeftX, bottomLeftY, width, height);
  }

  /**
   * Copy constructor.
   *
   * @author Dave Ferguson
   */
  public SystemFiducialRectangle(StageRectangle rhs)
  {
    super(rhs);
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializeRectangle(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializeRectangle(os);
  }
}
