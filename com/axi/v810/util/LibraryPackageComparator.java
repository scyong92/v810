package com.axi.v810.util;

import java.util.*;
import com.axi.v810.business.packageLibrary.*;
import com.axi.util.Assert;

public class LibraryPackageComparator implements Comparator<LibraryPackage>
{
  /**
   * Compares its two arguments for order.
   *
   * @param lhsPackage the first LibraryPackage to be compared.
   * @param rhsPackage the second LibraryPackage to be compared.
   * @return int a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater
   *   than the second.
   * @todo Implement this java.util.Comparator method
   * @author Tan Hock Zoon
   */
  public int compare(LibraryPackage lhsPackage, LibraryPackage rhsPackage)
  {
    Assert.expect(lhsPackage != null);
    Assert.expect(rhsPackage != null);

    double lhDouble = lhsPackage.getSelectedLibrarySubtypeScheme().getMatchPercentage();
    double rhDouble = rhsPackage.getSelectedLibrarySubtypeScheme().getMatchPercentage();

    if (lhDouble < rhDouble)
    {
      return 1;
    }
    else if (lhDouble > rhDouble)
    {
      return -1;
    }
    else
    {
      return 0;
    }

  }
}
