package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;

/**
 * This class enumerates descirption of devices
 * connected via a USB connection
 * @author George A. David
 */
public class UsbDeviceDescriptionEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  public static UsbDeviceDescriptionEnum XRAY_SOURCE = new UsbDeviceDescriptionEnum(++_index);
  public static UsbDeviceDescriptionEnum INTERLOCK_MONITOR = new UsbDeviceDescriptionEnum(++_index);
  private static Map<UsbDeviceDescriptionEnum, String> _usbDeviceDescriptionEnumToDescriptionMap = new HashMap<UsbDeviceDescriptionEnum, String>();

  // initialize the map
  static
  {
    Assert.expect(_usbDeviceDescriptionEnumToDescriptionMap.put(XRAY_SOURCE, "Xray Source") == null);
    Assert.expect(_usbDeviceDescriptionEnumToDescriptionMap.put(INTERLOCK_MONITOR, "Interlock Monitor") == null);
  }

  /**
   * @author George A. David
   */
  private UsbDeviceDescriptionEnum(int id)
  {
    super(id);
  }

  /**
   * @author George A. David
   */
  public String getDescription()
  {
    String description = _usbDeviceDescriptionEnumToDescriptionMap.get(this);
    Assert.expect(description != null);

    return description;
  }
}
