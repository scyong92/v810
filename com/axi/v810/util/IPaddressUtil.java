package com.axi.v810.util;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Roy Williams
 */
public class IPaddressUtil
{
  static private boolean _isSimulationOn = false;
  static private InetAddress _remoteLoopbackAddress;

  static
  {
    Config config = Config.getInstance();
    _isSimulationOn = config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    try
    {
      _remoteLoopbackAddress = InetAddress.getByName("localhost");
    }
    catch (UnknownHostException ex)
    {
      Assert.expect(false, "Remote loopback address not defined");
    }
  }

  /**
   * Returns the InetAddress of the host computer on the switch side of the network.
   * There are 3 network cards in the computer.  2 are dedicated to network
   * traffic on the switch side of the network with IRP's and cameras.
   *
   * @author Roy Williams
   */
  public static InetAddress getHostInetAddressOnPrivateNetwork() throws IPaddressException
  {
    InetAddress controllerInetAddress = null;
    if (_isSimulationOn == false)
    {
      controllerInetAddress = getHostInetAddressOnPrivateNetwork(false);
    }
    else
    {
      try
      {
        controllerInetAddress = InetAddress.getLocalHost();
      }
      catch (UnknownHostException e)
      {
        Assert.expect(false, "Cannot get inet address of localhost");
      }
    }
    return controllerInetAddress;
  }

  /**
   * @return the first (or last) NIC card (in your computer) with the first three
   * octets containing "192.168.128"
   * @author Rex Shang
   */
  public static InetAddress getHostInetAddressOnPrivateNetwork(boolean useFirstInList) throws IPaddressException
  {
    List<InetAddress> privateAddresses = getHostInetAddressesOnPrivateNetwork();
    // There has to be at least one address in the list.  Otherwise, it will throw exception.
    Assert.expect(privateAddresses.size() > 0);

    InetAddress inetAddress = null;
    if (useFirstInList)
      inetAddress = privateAddresses.get(0);
    else
      inetAddress = privateAddresses.get(privateAddresses.size() - 1);

    return inetAddress;
  }

  /**
   * @return a list of InetAddress with the first three octets containing "192.168.128".
   * @author Rex Shang
   */
  public static List<InetAddress> getHostInetAddressesOnPrivateNetwork() throws IPaddressException
  {
    IOException ioException = null;
    List<InetAddress> inetAddress = new ArrayList<InetAddress>();
    byte addrPrefix[] = {(byte)192, (byte)168, (byte)128};

    try
    {
      String hostName = InetAddress.getLocalHost().getHostName();
      InetAddress[] addresses = InetAddress.getAllByName(hostName);

      for (InetAddress address : addresses)
      {
          // CR1021 fix by LeeHerng - Add a checking here to make sure
          // we only process IP address version 4
          if (address instanceof Inet4Address)
          {
            byte[] addrBytes = address.getAddress();
            Assert.expect(addrBytes.length == 4);
            if ((addrBytes[0] == addrPrefix[0])
                && (addrBytes[1] == addrPrefix[1])
                && (addrBytes[2] == addrPrefix[2])
                && inetAddress.contains(address) == false)
            {
              inetAddress.add(address);
            }
          }
      }

// RMS: the following code seems to be a better implementation.  But we often
// get no address out of it.
      // The next call throws only if an "I/O error" occurs (no interfaces is not an I/0 error)
//      List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
//      for (NetworkInterface networkInterface : networkInterfaces)
//      {
//        List<InetAddress> addresses = Collections.list(networkInterface.getInetAddresses());
//        for (InetAddress address : addresses)
//        {
//          byte[] addrBytes = address.getAddress();
//          Assert.expect(addrBytes.length == 4);
//          if ((addrBytes[0] == addrPrefix[0])
//              && (addrBytes[1] == addrPrefix[1])
//              && (addrBytes[2] == addrPrefix[2])
//              && inetAddress.contains(address) == false)
//          {
//            inetAddress.add(address);
//          }
//        }
//      }

      // Sort the list so the addresses are always in consistent order.
      Collections.sort(inetAddress, new Comparator<InetAddress>() {
        public int compare(InetAddress lhs, InetAddress rhs)
        {
          return lhs.getHostAddress().compareTo(rhs.getHostAddress());
        }
      });
    }
    catch (IOException ioe)
    {
      ioException = ioe;
    }
    finally
    {
      if (inetAddress.size() == 0)
      {
        IPaddressException ipAddressException = new IPaddressException();
        if (ioException != null)
          ipAddressException.initCause(ioException);
        throw ipAddressException;
      }
    }

    return inetAddress;
  }

  /**
   * @author Roy Williams
   */
  static public InetAddress getRemoteLoopbackAddress()
  {
    return _remoteLoopbackAddress;
  }
}
