package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * This comparator will sort an InspectionFamilyEnum object based  on it's name.
 *
 * @todo fix this to compare InspectionFamilies instead since the Enums no longer
 * have a name with them
 *
 * @author Andy Mechtenberg
 */
public class JointTypeEnumComparator implements Comparator<JointTypeEnum>
{
  private boolean _ascending;

  /**
   * @author Andy Mechtenberg
   */
  public JointTypeEnumComparator(boolean ascending)
  {
    _ascending = ascending;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int compare(JointTypeEnum lhJointType, JointTypeEnum rhJointType)
  {
    Assert.expect(lhJointType != null);
    Assert.expect(rhJointType != null);

    String lhName = lhJointType.toString();
    String rhName = rhJointType.toString();

    if (_ascending)
      return lhName.compareToIgnoreCase(rhName);
    else
      return rhName.compareToIgnoreCase(lhName);
  }
}
