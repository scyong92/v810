package com.axi.v810.util;

import java.io.*;


/**
 * Title:
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author Kristin Casterton
 * @version 1.0
 */

public class HTMLWriter
{
  private FileWriter _htmlFile;
  private PrintWriter _printWriter;
  String _html;

  /**
   * @author Kristin Casterton
   */
  public HTMLWriter(HTMLDocument document, String fileLocation) throws IOException
  {
    _html = new String();
    _html = document.getHTML();
    // this may throw an exception
    _htmlFile = new FileWriter(fileLocation, false);
    _printWriter = new PrintWriter(_htmlFile, true);
    _printWriter.flush();
  }

  /**
   * @author Kristin Casterton
   */
  public void write()
  {
    _printWriter.flush();
    _printWriter.println(_html);
    _printWriter.close();
  }
}