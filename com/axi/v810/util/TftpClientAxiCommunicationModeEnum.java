package com.axi.v810.util;

import org.apache.commons.net.tftp.*;

/**
 * @author Greg Esparza
 */
public class TftpClientAxiCommunicationModeEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  public static TftpClientAxiCommunicationModeEnum TFTP_ASCII_MODE = new TftpClientAxiCommunicationModeEnum(++_index, TFTP.ASCII_MODE);
  public static TftpClientAxiCommunicationModeEnum TFTP_BINARY_MODE = new TftpClientAxiCommunicationModeEnum(++_index, TFTP.BINARY_MODE);
  public static TftpClientAxiCommunicationModeEnum TFTP_OCTET_MODE = new TftpClientAxiCommunicationModeEnum(++_index, TFTP.OCTET_MODE);
  public static TftpClientAxiCommunicationModeEnum TFTP_NETASCII_MODE = new TftpClientAxiCommunicationModeEnum(++_index, TFTP.NETASCII_MODE);
  public static TftpClientAxiCommunicationModeEnum TFTP_IMAGE_MODE = new TftpClientAxiCommunicationModeEnum(++_index, TFTP.IMAGE_MODE);

  // Default to bianary mode, it will get a new value in constructor.
  private int _communicationMode = TFTP.BINARY_MODE;

  /**
   * @author Greg Esparza
   */
  public TftpClientAxiCommunicationModeEnum(int id, int communicationMode)
  {
    super(id);

    _communicationMode = communicationMode;
  }

  /**
   * Make this method package so it can only be used by TftpClientAxi to get
   * the mode.
   * @author Rex Shang
   */
  int getCommunicationMode()
  {
    return _communicationMode;
  }
}

