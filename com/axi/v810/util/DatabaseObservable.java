/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import java.util.*;
/**
 *
 * @author swee-yee.wong
 */
public class DatabaseObservable extends Observable
{
  private static DatabaseObservable _instance;

  /**
   * @author George A. David
   */
  private DatabaseObservable()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public static synchronized DatabaseObservable getInstance()
  {
    if (_instance == null)
      _instance = new DatabaseObservable();

    return _instance;
  }


  public void sendDatabaseChangeEvent(DatabaseChangeEvent databaseChangeEvent)
  {
    if(databaseChangeEvent != null)
    {
      setChanged();
      notifyObservers(databaseChangeEvent);
    }
    else
    {
      System.out.println("Failed to update database event.");
    }
  }
}