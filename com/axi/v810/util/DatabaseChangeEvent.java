/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.util;

/**
 *
 * @author swee-yee.wong
 */
public class DatabaseChangeEvent
{

  private String _tagName;
  private DatabaseChangeTypeEnum _databaseChangeTypeEnum;
  private double _version;

  /**
   * @author Bill Darbie
   */
  public DatabaseChangeEvent(DatabaseChangeTypeEnum databaseChangeTypeEnum, Object object)
  {
    if(databaseChangeTypeEnum != null && object != null)
    {
      if (databaseChangeTypeEnum == DatabaseChangeTypeEnum.INSERT || databaseChangeTypeEnum == DatabaseChangeTypeEnum.DELETE)
      {
        _tagName = (String)object;
      }
      else if(databaseChangeTypeEnum == DatabaseChangeTypeEnum.UPDATE_VERSION)
      {
        _version = (double)object;
      }
      _databaseChangeTypeEnum = databaseChangeTypeEnum;
    }
    
  }

  /**
   * @author Swee Yee Wong
   */
  public String getTagName()
  {
    return _tagName;
  }

  /**
   * @author Swee Yee Wong
   */
  public DatabaseChangeTypeEnum getDatabaseChangeTypeEnum()
  {
    return _databaseChangeTypeEnum;
  }
  
  public double getVersion()
  {
    return _version;
  }
  
}
