package com.axi.v810.util;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;

/**
 * Test DirectoryLoggerAxi exception throwing.  All other functionality tested
 * in regression test for FileLogger.
 *
 * @author John Dutton
 */
public class Test_DirectoryLoggerAxi extends UnitTest
{
  /**
   * @author John Dutton
   */
  static public void main(String[] args)
  {
    UnitTest.execute(new Test_DirectoryLoggerAxi());
  }

  /**
   * @author John Dutton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // See that DirLoggerAxi log and logIfTimedOut throw only a DatastoreException.  If it throws
    // something else, a compile time error will occur.
    try
    {
      String logFilesDirectory = getTestDataDir();
      String logFileNamePrefix = "DirLogTest";
      String logFileNameExtension = ".log";
      int maxNosOfLogFiles = 4;
      int elapsedTimeThresholdSeconds = 0;

      DirectoryLoggerAxi directoryLogger =
          new DirectoryLoggerAxi(logFilesDirectory, logFileNamePrefix,
                                 logFileNameExtension, maxNosOfLogFiles);

      if (logFileNamePrefix.equals("somejunk"))
      {
        // Above condition is always false so calls won't occur, necessitating file system cleanup.
        directoryLogger.log("foobar");
        directoryLogger.logIfTimedOut("foobar", elapsedTimeThresholdSeconds);
      }
    }
    catch (DatastoreException ioe)
    {
      // do nothing
    }
  }


}

