package com.axi.v810.util;

/**
 *
 * @author chin-seong.kee
 */
public class SubtypeCompratorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static SubtypeCompratorEnum JOINT_TYPE = new SubtypeCompratorEnum(++_index);
  public static SubtypeCompratorEnum SUBTYPE = new SubtypeCompratorEnum(++_index);
  public static SubtypeCompratorEnum ARTIFACT_COMPENSATION = new SubtypeCompratorEnum(++_index);
  public static SubtypeCompratorEnum SIGNAL_COMPENSATION = new SubtypeCompratorEnum(++_index);
  public static SubtypeCompratorEnum USER_GAIN = new SubtypeCompratorEnum(++_index);
  public static SubtypeCompratorEnum MAGNIFICATION_TYPE = new SubtypeCompratorEnum(++_index);
  public static SubtypeCompratorEnum STAGE_SPEED = new SubtypeCompratorEnum(++_index);
  public static SubtypeCompratorEnum DYNAMIC_RANGE_OPTIMIZATION_LEVEL = new SubtypeCompratorEnum(++_index);

  /**
   * @author George Booth
   */
  private SubtypeCompratorEnum(int id)
  {
    super(id);
  }
}