package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * Test class for ComponentFamilyOrSubtypeComparator
 *
 * @author Andy Mechtenberg
 */

public class Test_ComponentTypeComparator extends UnitTest
{

  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute( new Test_ComponentTypeComparator() );
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test( BufferedReader is, PrintWriter os )
  {
    // component sort
    Project project = null;
    String panelName = "Test_panel_1";
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    try
    {
      project = Project.importProjectFromNdfs(panelName, warnings);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    Expect.expect(project != null);
    Expect.expect(warnings.size() == 0);

    List<BoardType> boardTypes = project.getPanel().getBoardTypes();
    Expect.expect(boardTypes.size() == 1);
    BoardType boardType = (BoardType)boardTypes.get(0);

    List<ComponentType> components = boardType.getComponentTypes();
    // sort ascending, based on Family Name
    Collections.sort(components, new ComponentTypeComparator(true, ComponentTypeComparatorEnum.JOINT_TYPE) );
    ComponentType testC = (ComponentType)components.get(0);

/** @todo wpd */
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.CONNECTOR));  // Gullwing
//    testC = (ComponentType)components.get(1);
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.GULLWING));  // pth
//    testC = (ComponentType)components.get(2);
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.GULLWING));  // pth
//    testC = (ComponentType)components.get(3);
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.RES));  // res
//
//    // sort descending based on family name
//    Collections.sort(components, new ComponentJointTypeOrSubtypeComparator(false, true));
//    testC = (ComponentType)components.get(0);
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.RES));
//    testC = (ComponentType)components.get(1);
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.GULLWING));
//    testC = (ComponentType)components.get(2);
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.GULLWING));
//    testC = (ComponentType)components.get(3);
//    Expect.expect(testC.getCompPackage().getJointTypeEnum().equals(JointTypeEnum.CONNECTOR));


    // sort ascending, based on Subtype Name
    Collections.sort(components, new ComponentTypeComparator(true, ComponentTypeComparatorEnum.SUBTYPE));
    testC = (ComponentType)components.get(0);
//    Pad pad = (Pad)testC.getPads().get(0);
// wpd virgo - ADD THIS BACK put this back in for virgo once the subtypes algorithm is put in
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("1206_R<A>"));
//    testC = (Component)components.get(1);
//    pad = (Pad)testC.getPads().get(0);
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("26PINCONN_J<B>"));
//    testC = (Component)components.get(2);
//    pad = (Pad)testC.getPads().get(0);
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("LED_LED_RA_D<A>"));
//    testC = (Component)components.get(3);
//    pad = (Pad)testC.getPads().get(0);
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("SO16_U<A>"));
//
//    // sort descending, based on Subtype Name
//    Collections.sort(components, new ComponentFamilyOrSubtypeComparator(false, false) );
//    testC = (Component)components.get(0);
//    pad = (Pad)testC.getPads().get(0);
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("SO16_U<A>"));
//    testC = (Component)components.get(1);
//    pad = (Pad)testC.getPads().get(0);
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("LED_LED_RA_D<A>"));
//    testC = (Component)components.get(2);
//    pad = (Pad)testC.getPads().get(0);
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("26PINCONN_J<B>"));
//    testC = (Component)components.get(3);
//    pad = (Pad)testC.getPads().get(0);
//    Expect.expect(pad.getCustomAlgorithmFamily().getPartialCustomName().equalsIgnoreCase("1206_R<A>"));
  }
}
