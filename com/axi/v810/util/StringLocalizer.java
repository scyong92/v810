package com.axi.v810.util;

import java.text.*;
import java.util.*;

import com.axi.util.*;

/**
* This class handles string message localizalization.  The constructor finds the local strings file
* and the keyToString will use that file to change any string that ends with _KEY to local language string
* representation.  If the _KEY does not exist in the localization file, then the _KEY string is returned intact.
*
* @author Andy Mechtenberg
*/
public class StringLocalizer
{
  private static ResourceBundle _bundle = null;
  private static ResourceBundle _docsBundle = null;
  private static ResourceBundle _infoHandlerBundle = null;

  private static MessageFormat _formatter = null;
  private static final String _LOCALIZATION_PROPERTIES = "com.axi.v810.properties.Localization";
  private static final String _DOCS_PROPERTIES = "com.axi.v810.properties.Docs";
  private static final String _INFO_HANDLER_LOCALIZATION_PROPERTIES = "com.axi.v810.properties.InfoHandlerLocalization";

 /*
  * @author Andy Mechtenberg
  */
  static
  {
    _bundle = ResourceHandler.getPropertyBundle(_LOCALIZATION_PROPERTIES);
    _docsBundle = ResourceHandler.getPropertyBundle(_DOCS_PROPERTIES);
    _infoHandlerBundle = ResourceHandler.getPropertyBundle(_INFO_HANDLER_LOCALIZATION_PROPERTIES);
    _formatter = new MessageFormat("");
  }

  /**
  * This is called to translate a _KEY string to the local language version of that string
  *
  * @param key is a string ending in _KEY
  * @return A localized string matching the _KEY key string.
  * @author Andy Mechtenberg
  */
  public synchronized static String keyToString(String key)
  {
    Assert.expect(key != null);

    String retString = key;

    if (key.endsWith("_KEY"))
    {
      try
      {
        retString = _bundle.getString(key);
      }
      catch(MissingResourceException e1)
      {
        try
        {
          retString = _docsBundle.getString(key);
        }
        catch(MissingResourceException e3)
        {
          // do nothing - just show the key
        }
      }
    }

    return retString;
  }

  /**
  * Given a localizedString return the corresponding message with all message
  * arguments filled in as needed.
  *
  * @param localizedString is the localized string to be converted into a message
  * @author Bill Darbie
  */
  public synchronized static String keyToString(LocalizedString localizedString)
  {
    Assert.expect(localizedString != null);

    String key = localizedString.getMessageKey();
    String message = keyToString(key);

    if ((message != null) && (message.equals(key) == false))
    {
      Object[] args = localizedString.getArguments();
      if ((args != null) && (args.length > 0))
      {
        _formatter.applyPattern(message);
        message = _formatter.format(args);
      }
    }

    return message;
  }

  /**
  * This is called to change the localized file away from the default of Locale.US
  *
  * @author Andy Mechtenberg
  * @param newLocale A new Locale specifying local language file to be used.
  */
  public synchronized static void setLocale(Locale newLocale)
  {
    Assert.expect(newLocale != null);

    Localization.setLocale(newLocale);
    _bundle = ResourceHandler.getPropertyBundle(_LOCALIZATION_PROPERTIES);
    _docsBundle = ResourceHandler.getPropertyBundle(_DOCS_PROPERTIES);
    _infoHandlerBundle = ResourceHandler.getPropertyBundle(_INFO_HANDLER_LOCALIZATION_PROPERTIES);
    _formatter.setLocale(newLocale);
  }
  
  public synchronized static String infoHandlerKeyToString(String key)
  {
    Assert.expect(key != null);

    String retString = key;

    if (key.endsWith("_KEY"))
    {
      try
      {
        retString = _infoHandlerBundle.getString(key);
      }
      catch(MissingResourceException e1)
      {
        try
        {
          retString = _docsBundle.getString(key);
        }
        catch(MissingResourceException e3)
        {
          // do nothing - just show the key
        }
      }
    }

    return retString;
  }
}
