/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import java.util.*;
import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * @author swee-yee.wong
 */
public class ImportDatabaseLayoutTableModel extends DefaultSortTableModel
{
  private List<ImportDatabaseDiffPackage> _packages = new ArrayList<ImportDatabaseDiffPackage>();

  private static final String _SELECT_COLUMN_HEADING = StringLocalizer.infoHandlerKeyToString("IMPORT_DATABASE_TABLE_COLUMN_SELECT_LABEL_KEY");
  private static final String _TAG_NAME_COLUMN_HEADING = StringLocalizer.infoHandlerKeyToString("IMPORT_DATABASE_TABLE_COLUMN_TAG_NAME_LABEL_KEY");
  private static final String _TAG_VERSION_COLUMN_HEADING = StringLocalizer.infoHandlerKeyToString("IMPORT_DATABASE_TABLE_COLUMN_MODIFIED_VERSION_LABEL_KEY");
  private static final String _LAST_MODIFIED_DATE_COLUMN_HEADING = StringLocalizer.infoHandlerKeyToString("IMPORT_DATABASE_TABLE_COLUMN_LAST_MODIFIED_DATE_LABEL_KEY");

  private final String[] _tableColumns =
      {_SELECT_COLUMN_HEADING,
      _TAG_NAME_COLUMN_HEADING,
      _TAG_VERSION_COLUMN_HEADING,
      _LAST_MODIFIED_DATE_COLUMN_HEADING};

  private List<ImportDatabaseDiffPackage> _selectedPackages = null;
  
  
  ImportDatabaseLayoutTableModel(List<ImportDatabaseDiffPackage> selectedPackages)
  {
    _selectedPackages = selectedPackages;
    for (ImportDatabaseDiffPackage importDatabaseDiffPackage : _selectedPackages)
    {
      _packages.add(importDatabaseDiffPackage);
    }
    fireTableDataChanged();
  }

  
  ImportDatabaseDiffPackage getDatabasePackagesAt(int row)
  {
    if (_packages != null && row >= 0 && row < _packages.size())
    {
      return (ImportDatabaseDiffPackage) _packages.get(row);
    }
    else
    {
      System.out.println("Imported database is empty or row selected is out of range.");
      return null;
    }
  }


  int getRowForDatabaseDiffPackage(ImportDatabaseDiffPackage importDatabaseDiffPackage)
  {
    if (_packages != null && importDatabaseDiffPackage != null)
    {
      return _packages.indexOf(importDatabaseDiffPackage);
    }
    else
    {
      System.out.println("Imported database is empty.");
      return 0;
    }
  }

  void clear()
  {
    _packages.clear();
    _selectedPackages.clear();
    fireTableDataChanged();
  }


  public int getColumnCount()
  {
    Assert.expect(_tableColumns != null);
    return _tableColumns.length;
  }

  public String getColumnName(int columnIndex)
  {
    Assert.expect(_tableColumns != null);
    Assert.expect(columnIndex >= 0 && columnIndex < getColumnCount());
    return (String)_tableColumns[columnIndex];
  }


  public int getRowCount()
  {
    if (_packages == null)
    {
      return 0;
    }
    else
    {
      return _packages.size();
    }
  }

  public boolean isCellEditable(int rowIndex, int columnIndex)
  {
    if (_packages == null || _packages.size() == 0)
    {
      return false;
    }

    switch (columnIndex)
    {
      case ImportDatabaseLayoutTable._SELECT_INDEX:
      {
        return true;
      }
    }
    return false;
  }


  @Override
  public void setValueAt(Object object, int rowIndex, int columnIndex)
  {
    Assert.expect(object != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    ImportDatabaseDiffPackage importDatabaseDiffPackage = _packages.get(rowIndex);

    switch (columnIndex)
    {
      case ImportDatabaseLayoutTable._SELECT_INDEX:
      {
        boolean selected = (Boolean)object;
        if (selected)
        {
          if (_selectedPackages.contains(importDatabaseDiffPackage) == false)
          {
            _selectedPackages.add(importDatabaseDiffPackage);
          }
        }
        else
        {
          if (_selectedPackages.contains(importDatabaseDiffPackage) == true)
          {
            _selectedPackages.remove(importDatabaseDiffPackage);
          }
        }
        fireTableCellUpdated(rowIndex, columnIndex);
        //GuiObservable.getInstance().stateChanged(this, SelectionEventEnum.EXPORT_LIBRARY_SELECTION);
        break;
      }
      default:
        Assert.expect(false,"Edit this column is not implemented");
    }
  }


  @Override
  public Object getValueAt(int rowIndex, int columnIndex)
  {
    Assert.expect(_packages != null);
    Assert.expect(rowIndex >= 0);
    Assert.expect(columnIndex >= 0);

    Object value = null;

    ImportDatabaseDiffPackage importDatabaseDiffPackage = _packages.get(rowIndex);
    //get the appropriate data item that corresponds to the column
    switch (columnIndex)
    {
      case ImportDatabaseLayoutTable._SELECT_INDEX:
      {
        value = new Boolean(_selectedPackages.contains(importDatabaseDiffPackage));
        break;
      }
      case ImportDatabaseLayoutTable._TAG_NAME_INDEX:
      {
        value = importDatabaseDiffPackage.getTagName();
        break;
      }
      case ImportDatabaseLayoutTable._TAG_VERSION_INDEX:
      {
        value = importDatabaseDiffPackage.getImportVersion();
        break;
      }
      case ImportDatabaseLayoutTable._LAST_MODIFIED_DATE_INDEX:
      {
        value = importDatabaseDiffPackage.getImportDateModified();
        break;
      }
      default:
      {
        Assert.expect(false);
      }
    }
    return value;
  }


  public List <ImportDatabaseDiffPackage> getSelectedPackages()
  {
    Assert.expect(_selectedPackages != null);
    return new ArrayList<ImportDatabaseDiffPackage>(_selectedPackages);
  }


  public void selectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(true), i, ImportDatabaseLayoutTable._SELECT_INDEX);
    }
  }


  public void unselectAllPackages()
  {
    for (int i = 0; i < getRowCount(); i++)
    {
      setValueAt(new Boolean(false), i, ImportDatabaseLayoutTable._SELECT_INDEX);
    }
  }

}
