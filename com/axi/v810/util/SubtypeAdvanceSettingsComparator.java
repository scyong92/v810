package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 *
 * @author chin-seong.kee
 */
public class SubtypeAdvanceSettingsComparator implements Comparator<Subtype>
{
  private SubtypeCompratorEnum _comparingAttribute;
  private boolean _ascending;

  /**
   * @author Chin Seong
   */
  public SubtypeAdvanceSettingsComparator(boolean ascending, SubtypeCompratorEnum comparingAttribute)
  {
    Assert.expect(comparingAttribute != null);
    _ascending = ascending;
    _comparingAttribute = comparingAttribute;
  }

  /**
   * @author Chin Seong
   */
  public int compare(Subtype lhs, Subtype rhs)
  {
    Assert.expect(lhs != null);
    Assert.expect(rhs != null);

    String lhsString;
    String rhsString;
    Double lhsDouble;
    Double rhsDouble;

    if (_comparingAttribute.equals(SubtypeCompratorEnum.JOINT_TYPE))
    {
      lhsString = lhs.getJointTypeEnum().getName();
      rhsString = rhs.getJointTypeEnum().getName();
      if(_ascending)
        return lhsString.compareToIgnoreCase(rhsString);
      else
        return rhsString.compareToIgnoreCase(lhsString);
    }
    else if (_comparingAttribute.equals(SubtypeCompratorEnum.SUBTYPE))
    {
      // sort order for subtypes is:
      //   Untestable
      //   No Test
      //   Partial Test
      //   No Load
      //   Mixed
      //   other subtypes sorted alphanumerically
      // is no longer compare the list on top as subtype Advanced settings only applicable for subtype
      // which is not under no test, no load nor untestable.

      lhsString = "";
      rhsString = "";
      
      lhsString = lhs.getShortName().toString();
      rhsString = rhs.getShortName().toString();

      if (_ascending)
      {
         return lhsString.compareTo(rhsString);
      }
      else
      {
         return rhsString.compareTo(lhsString);
      }
    }
    else if (_comparingAttribute.equals(SubtypeCompratorEnum.ARTIFACT_COMPENSATION))
    {
      lhsString = lhs.getSubtypeAdvanceSettings().getArtifactCompensationState().toString();
      rhsString = rhs.getSubtypeAdvanceSettings().getArtifactCompensationState().toString();

      if (_ascending)
        return lhsString.compareTo(rhsString);
      else
        return rhsString.compareTo(lhsString);

    }
    else if (_comparingAttribute.equals(SubtypeCompratorEnum.SIGNAL_COMPENSATION))
    {
      lhsString = lhs.getSubtypeAdvanceSettings().getSignalCompensation().toString();
      rhsString = rhs.getSubtypeAdvanceSettings().getSignalCompensation().toString();

      if (_ascending)
        return lhsString.compareTo(rhsString);
      else
        return rhsString.compareTo(lhsString);

    }
    else if (_comparingAttribute.equals(SubtypeCompratorEnum.USER_GAIN))
    {
      lhsString = lhs.getSubtypeAdvanceSettings().getUserGain().toString();
      rhsString = rhs.getSubtypeAdvanceSettings().getUserGain().toString();
      if (_ascending)
      {
        return lhsString.compareTo(rhsString);
      }
      else
      {
        return rhsString.compareTo(lhsString);
      }
    }
    else if (_comparingAttribute.equals(SubtypeCompratorEnum.STAGE_SPEED))
    {
      if (lhs.getSubtypeAdvanceSettings().getStageSpeedList().size() == rhs.getSubtypeAdvanceSettings().getStageSpeedList().size())
      {
        String lhsSpeed = lhs.getSubtypeAdvanceSettings().getLowestStageSpeedSetting().toString();
        String rhsSpeed = rhs.getSubtypeAdvanceSettings().getLowestStageSpeedSetting().toString();
        if (_ascending)
        {
          return lhsSpeed.compareTo(rhsSpeed);
        }
        else
        {
          return rhsSpeed.compareTo(lhsSpeed);
        }
      }
      else
      {
        lhsString = lhs.getSubtypeAdvanceSettings().getStageSpeedList().size() + "";
        rhsString = rhs.getSubtypeAdvanceSettings().getStageSpeedList().size() + "";
        if (_ascending)
        {
          return lhsString.compareTo(rhsString);
        }
        else
        {
          return rhsString.compareTo(lhsString);
        }
      }
    }
    else if (_comparingAttribute.equals(SubtypeCompratorEnum.MAGNIFICATION_TYPE))
    {
      lhsString = lhs.getSubtypeAdvanceSettings().getMagnificationType().toString();
      rhsString = rhs.getSubtypeAdvanceSettings().getMagnificationType().toString();
      if (_ascending)
      {
        return rhsString.compareTo(lhsString);
      }
      else
      {
        return lhsString.compareTo(rhsString);
      }
    }
    else if (_comparingAttribute.equals(SubtypeCompratorEnum.DYNAMIC_RANGE_OPTIMIZATION_LEVEL))
    {
      //Siew Yeng - XCR-3145 - Failed to sort DRO value
      lhsDouble = lhs.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel().toDouble();
      rhsDouble = rhs.getSubtypeAdvanceSettings().getDynamicRangeOptimizationLevel().toDouble();
      
      if (_ascending)
      {
        return lhsDouble.compareTo(rhsDouble);
      }
      else
      {
        return rhsDouble.compareTo(lhsDouble);
      }
    }
    else
    {
      Assert.expect(false, "Please add your new component attribute to sort on to this method");
      return 0;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setSubtypeComparatorEnum(SubtypeCompratorEnum subtypeComparatorEnum)
  {
    Assert.expect(subtypeComparatorEnum != null);
    _comparingAttribute = subtypeComparatorEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setAscending(boolean ascending)
  {
    _ascending = ascending;
  }
}
