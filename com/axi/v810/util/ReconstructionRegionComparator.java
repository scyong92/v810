package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.gui.testDev.*;

/**
 * <p>Title: ComponentTypeComparator</p>
 *
 * <p>Description: Provide sorting for ComponentType table entries</p>
 *
 * <p>Copyright: Copyright (c) 2006</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */

public class ReconstructionRegionComparator implements Comparator<FocusReconstructionRegionWrapper>
{
  private AlphaNumericComparator _alphaComparator;
  private ReconstructionRegionComparatorEnum _comparingAttribute;
  private boolean _ascending;

  /**
   * @author George Booth
   */
  public ReconstructionRegionComparator(boolean ascending, ReconstructionRegionComparatorEnum comparingAttribute)
  {
    Assert.expect(comparingAttribute != null);
    _ascending = ascending;
    _comparingAttribute = comparingAttribute;
    _alphaComparator = new AlphaNumericComparator(_ascending);
  }

  /**
   * @author George Booth
   */
  public int compare(FocusReconstructionRegionWrapper lhs, FocusReconstructionRegionWrapper rhs)
  {
    String lhsString;
    String rhsString;

    if (_comparingAttribute.equals(ReconstructionRegionComparatorEnum.DESCRIPTION))
    {
      lhsString = lhs.toString();
      rhsString = rhs.toString();

      return _alphaComparator.compare(lhsString, rhsString);
    }
    else if (_comparingAttribute.equals(ReconstructionRegionComparatorEnum.FOCUS_METHOD))
    {
      return 0;
    }
    else if (_comparingAttribute.equals(ReconstructionRegionComparatorEnum.RETEST_STATUS))
    {
      Boolean lhsBool = new Boolean(lhs.isUsingRetest());
      Boolean rhsBool = new Boolean(rhs.isUsingRetest());
      if (_ascending)
        return lhsBool.compareTo(rhsBool);
      else
        return rhsBool.compareTo(lhsBool);
    }
    else if (_comparingAttribute.equals(ReconstructionRegionComparatorEnum.NOTEST_STATUS))
    {
      return 0;
    }
    else
    {
      Assert.expect(false, "Unexpected sort criteria: " + _comparingAttribute);
      return 0;
    }
  }
}
