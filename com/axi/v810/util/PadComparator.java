package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * @author Bill Darbie
 */
public class PadComparator implements Comparator<Pad>, Serializable
{
  private AlphaNumericComparator _alphaComparator;

  /**
   * @author Bill Darbie
   */
  public PadComparator()
  {
    _alphaComparator = new AlphaNumericComparator();
  }

  /**
   * @author Bill Darbie
   */
  public int compare(Pad lhs, Pad rhs)
  {
    String lhsName = lhs.getBoardAndComponentAndPadName();
    String rhsName = rhs.getBoardAndComponentAndPadName();

    return _alphaComparator.compare(lhsName, rhsName);
  }
}
