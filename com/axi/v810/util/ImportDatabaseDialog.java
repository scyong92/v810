/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import javax.swing.event.*;
/**
 *
 * @author swee-yee.wong
 */
public class ImportDatabaseDialog  extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _topPanel = new JPanel();
  private JPanel _infoPanel = new JPanel();
  private JPanel _editorPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private static JEditorPane _editor = new JEditorPane();
  private static JEditorPane _importEditor = new JEditorPane();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private BorderLayout _topPanelBorderLayout = new BorderLayout();
  private BorderLayout _infoPanelBorderLayout = new BorderLayout();
  private GridBagLayout _editorPanelGridBagLayout = new GridBagLayout();
  private FlowLayout _okPanelFlowLayout = new FlowLayout();
  private JLabel _contentLabel = new JLabel();
  private JButton _okButton = new JButton();
  private JScrollPane _tablePanelLayoutScroll = null;
  private static JScrollPane _pane;
  private static JScrollPane _importPane;
  private JSplitPane _splitPane = null;
  private JCheckBox _selectAll;
  private List<ImportDatabaseDiffPackage> _selectedPackages = null;
  
  private ImportDatabaseLayoutTable _importDatabaseLayoutTable = null;
  private ImportDatabaseLayoutTableModel _importDatabaseLayoutTableModel = null;


  
  ImportDatabaseDialog (JFrame frame, String title, boolean modal, List<ImportDatabaseDiffPackage> selectedPackages)
  {
    super(frame, title, modal);
    
    _selectedPackages = selectedPackages;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      //info msg
      System.out.println("Failed to open Import Database Dialog.");
    }
  }
  
  private void jbInit() throws Exception
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _topPanel.setLayout(_topPanelBorderLayout);
    _infoPanel.setLayout(_infoPanelBorderLayout);
    _editorPanel.setLayout(_editorPanelGridBagLayout);
    _okCancelPanel.setLayout(_okPanelFlowLayout);
    _okPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _contentLabel.setPreferredSize(new Dimension(600, 20));
    _contentLabel.setText(StringLocalizer.infoHandlerKeyToString("IMPORT_DATABASE_MESSAGE_KEY"));
    
    _selectAll = new JCheckBox(StringLocalizer.infoHandlerKeyToString("GUI_SELECT_ALL_CHECKBOX_LABEL_KEY"));
    _selectAll.addItemListener(new CheckBoxListener());

    
    _tablePanelLayoutScroll = new JScrollPane();
    
    _importDatabaseLayoutTableModel = new ImportDatabaseLayoutTableModel(_selectedPackages);
    _importDatabaseLayoutTable = new ImportDatabaseLayoutTable(_importDatabaseLayoutTableModel);
    
    _importDatabaseLayoutTable.setModel(_importDatabaseLayoutTableModel);
    _importDatabaseLayoutTable.setEditorsAndRenderers();
    _importDatabaseLayoutTable.setPreferredColumnWidths();
    
    _importDatabaseLayoutTableModel.unselectAllPackages();

    _tablePanelLayoutScroll.getViewport().add(_importDatabaseLayoutTable, null);
    _tablePanelLayoutScroll.getViewport().setOpaque(false);
    _tablePanelLayoutScroll.setPreferredSize(new Dimension(1600, 200));
    
    ListSelectionModel rowSM = _importDatabaseLayoutTable.getSelectionModel();
    rowSM.addListSelectionListener(new ListSelectionListener()
    {
      public void valueChanged(ListSelectionEvent e)
      {

        ListSelectionModel lsm = (ListSelectionModel)e.getSource();

        if (lsm.getValueIsAdjusting() == false)
        {
          // we want to select the pad(s) in the graphics window corresponding to the newly selected table row(s)
          int selectedRows = _importDatabaseLayoutTable.getSelectedRow();
          if (selectedRows >= 0)
          {
            ImportDatabaseDiffPackage importDatabaseDiffPackage = _importDatabaseLayoutTableModel.getDatabasePackagesAt(selectedRows);
            _editor.setText(InfoHandlerDatabaseManager.getInstance().getTagHtml(importDatabaseDiffPackage.getTagName()));
            _importEditor.setText(InfoHandlerDatabaseManager.getInstance2().getTagHtml(importDatabaseDiffPackage.getTagName()));
          }
          revalidate();
          repaint();
        }
      }
    });
    
    _editor.setContentType("text/html");
    _editor.setEditable(false);
    _pane = new JScrollPane(_editor);
    _pane.setPreferredSize(new Dimension(800, 600));
    
    _importEditor.setContentType("text/html");
    _importEditor.setEditable(false);
    _importPane = new JScrollPane(_importEditor);
    _importPane.setPreferredSize(new Dimension(800, 600));
    
    //swee yee wong
    _okButton.setText(StringLocalizer.infoHandlerKeyToString("GUI_INFO_OK_BUTTON_KEY"));
    _okButton.setEnabled(true);
    _okButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        okButton_actionPerformed(e);
      }
    });
    
    getContentPane().add(_mainPanel);
    getRootPane().setDefaultButton(_okButton);
    _mainPanel.add(_topPanel, BorderLayout.NORTH);
    _topPanel.add(_contentLabel, BorderLayout.NORTH);
    _topPanel.add(_selectAll, BorderLayout.SOUTH);
    _infoPanel.add(_tablePanelLayoutScroll, BorderLayout.CENTER);
    GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 0.5;
    c.weighty = 1.0;
    c.gridx = 0;
    c.gridy = 0;
    _editorPanel.add(_pane, c);
    c.fill = GridBagConstraints.BOTH;
    c.weightx = 0.5;
    c.weighty = 1.0;
    c.gridx = 1;
    c.gridy = 0;
    _editorPanel.add(_importPane, c);
    
    _splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, _infoPanel, _editorPanel);
    _splitPane.setContinuousLayout(true);
    _splitPane.setResizeWeight(0.3);
    
    _mainPanel.add(_splitPane, BorderLayout.CENTER);
    _mainPanel.add(_okCancelPanel, BorderLayout.SOUTH);
    _okCancelPanel.add(_okButton);
  }
  
  private void okButton_actionPerformed(ActionEvent e)
  {
    //InfoHandlerDatabaseManager.getInstance().insertInfoHandler(_newTagList);
    List<ImportDatabaseDiffPackage> selectedPackages = _importDatabaseLayoutTableModel.getSelectedPackages();
    if (selectedPackages.isEmpty() == false)
    {
      for (ImportDatabaseDiffPackage importPackage : selectedPackages)
      {
        InfoHandlerDatabaseManager.getInstance2().startupDatabaseDriver();
        String htmlContent = InfoHandlerDatabaseManager.getInstance2().getTagHtml(importPackage.getTagName());

        boolean exists = InfoHandlerDatabaseManager.getInstance().isTagExists(importPackage.getTagName());
        if (exists == false)
        {
          InfoHandlerDatabaseManager.getInstance().insertInfoHandler(importPackage.getTagName());
        }
        InfoHandlerDatabaseManager.getInstance().updateTagHtmlContent(importPackage.getTagName(), htmlContent);
        InfoHandlerDatabaseManager.getInstance().extractAndUpdateTagMediaList(importPackage.getTagName(), htmlContent);
      }
    }
    unpopulate();
    
    InfoHandlerDatabaseManager.getInstance2().closeConnection();
    InfoHandlerDatabaseManager.getInstance().renameDatabase();
    dispose();
  }
  
  /**
   * @author swee-yee.wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      unpopulate();
      InfoHandlerDatabaseManager.getInstance2().closeConnection();
      dispose();
    }
  }

  private class CheckBoxListener implements ItemListener
  {
    public void itemStateChanged(ItemEvent e)
    {
      if (e.getSource() == _selectAll)
      {
        if (e.getStateChange() == ItemEvent.SELECTED)
        {
          _importDatabaseLayoutTableModel.selectAllPackages();
        }
        else
        {
          _importDatabaseLayoutTableModel.unselectAllPackages();
        }
      }
    }
  }

  private void unpopulate()
  {
    _selectedPackages = null;
  }
}
