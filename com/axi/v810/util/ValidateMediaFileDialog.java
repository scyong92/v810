/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.axi.v810.util;

import com.axi.v810.util.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import javax.swing.*;
import java.util.*;


/**
 *
 * @author swee-yee.wong
 */
public class ValidateMediaFileDialog  extends JDialog
{
  private JPanel _mainPanel = new JPanel();
  private JPanel _centerPanel = new JPanel();
  private JPanel _contentPanel = new JPanel();
  private JPanel _okCancelPanel = new JPanel();
  private BorderLayout _mainPanelBorderLayout = new BorderLayout();
  private BorderLayout _contentPanelBorderLayout = new BorderLayout();
  private BorderLayout _centerPanelBorderLayout = new BorderLayout();
  private FlowLayout _okPanelFlowLayout = new FlowLayout();
  private JLabel _contentLabel = new JLabel();
  private JTextArea _contentTextArea = new JTextArea();
  private JButton _closeButton = new JButton();
  private static JScrollPane _pane;
  private List<String> _fullMediaList = new ArrayList<String>();


  
  ValidateMediaFileDialog (JFrame frame, String title, boolean modal, List<String> fullMediaList)
  {
    super(frame, title, modal);
    
    _fullMediaList = fullMediaList;
    try
    {
      jbInit();
      pack();
    }
    catch(Exception ex)
    {
      //info msg
      System.out.println("Failed to open Validate Media File Dialog.");
    }
  }
  
  private void jbInit() throws Exception
  {
    _mainPanel.setLayout(_mainPanelBorderLayout);
    _mainPanel.setPreferredSize(new Dimension(550, 300));
    _centerPanel.setLayout(_centerPanelBorderLayout);
    _contentPanel.setLayout(_contentPanelBorderLayout);
    _okCancelPanel.setLayout(_okPanelFlowLayout);
    _okPanelFlowLayout.setAlignment(FlowLayout.CENTER);
    _contentLabel.setPreferredSize(new Dimension(420, 40));
    _contentLabel.setText(StringLocalizer.infoHandlerKeyToString("VALIDATE_MEDIA_FILES_MESSAGE_KEY"));
    _pane = new JScrollPane(_contentTextArea);
    _pane.setPreferredSize(new Dimension(540, 210));
    if (_fullMediaList.size() != 0)
    {
      for (String pictureFile : _fullMediaList)
      {
        _contentTextArea.append(pictureFile);
        _contentTextArea.append("\n");
      }
    }
    else
      _contentTextArea.setText("No missing file");
    _contentTextArea.setEnabled(true);
    _contentTextArea.setEditable(false);
    //swee yee wong
    _closeButton.setText(StringLocalizer.infoHandlerKeyToString("GUI_CLOSE_BUTTON_KEY"));
    _closeButton.setEnabled(true);
    _closeButton.addActionListener(new java.awt.event.ActionListener()
    {
      public void actionPerformed(ActionEvent e)
      {
        cancelButton_actionPerformed(e);
      }
    });
    
    getContentPane().add(_mainPanel);
    getRootPane().setDefaultButton(_closeButton);
    _mainPanel.add(_centerPanel, BorderLayout.CENTER);
    _centerPanel.add(_contentPanel, BorderLayout.CENTER);
    _contentPanel.add(_contentLabel, BorderLayout.NORTH);
    _contentPanel.add(_pane, BorderLayout.CENTER);
    _centerPanel.add(_okCancelPanel, BorderLayout.SOUTH);
    _okCancelPanel.add(_closeButton);
  }
  
  private void cancelButton_actionPerformed(ActionEvent e)
  {
    unpopulate();
    dispose();
  }
  
  /**
   * @author swee-yee.wong
   */
  protected void processWindowEvent(WindowEvent e)
  {
    super.processWindowEvent(e);
    int eventId = e.getID();
    if (eventId == WindowEvent.WINDOW_CLOSING)
    {
      unpopulate();
      dispose();
    }
  }
  
  private void unpopulate()
  {
    _mainPanel = null;
    _centerPanel = null;
    _contentPanel = null;
    _okCancelPanel = null;
    _mainPanelBorderLayout = null;
    _contentPanelBorderLayout = null;
    _centerPanelBorderLayout = null;
    _okPanelFlowLayout = null;
    _contentLabel = null;
    _contentTextArea = null;
    _closeButton = null;
    _pane = null;
    _fullMediaList = null;
  }
}

