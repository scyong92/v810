package com.axi.v810.util;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.FileName;
import com.axi.v810.datastore.DatastoreException;

/**
 * Test class for Test_CompPackageNameOrFamilyComparator
 * @author Andy Mechtenberg
 */
public class Test_CompPackageComparator extends UnitTest
{
  /**
   * @author Andy Mechtenberg
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CompPackageComparator());
  }

  /**
   * @author Andy Mechtenberg
   */
  public void test( BufferedReader is, PrintWriter os )
  {
    // component sort
    Project project = null;
    String panelName = "Test_panel_1";
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    try
    {
      //delete the panel file if it exists, we first want to test loading ndf files.
      String projectFullPath = FileName.getProjectSerializedFullPath(panelName);
      if (FileUtil.exists(projectFullPath))
      {
        try
        {
          FileUtilAxi.delete(FileName.getProjectSerializedFullPath(panelName));
        }
        catch (DatastoreException dex)
        {
          Expect.expect(false);
        }
      }

      project = Project.importProjectFromNdfs(panelName, warnings);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    Expect.expect(project != null);
    Expect.expect(warnings.size() == 0);

    List<CompPackage> packages = project.getPanel().getCompPackages();

    // sort ascending, based on Package Name
    Collections.sort(packages, new CompPackageComparator(true, CompPackageComparatorEnum.LONG_NAME) );
    CompPackage testC = (CompPackage)packages.get(0);
    Expect.expect(testC.getLongName().equalsIgnoreCase("1206_Resistor"));
    testC = (CompPackage)packages.get(1);
    Expect.expect(testC.getLongName().equalsIgnoreCase("26PINCONN_PTH"));
    testC = (CompPackage)packages.get(2);
    Expect.expect(testC.getLongName().equalsIgnoreCase("LED_LED_RA_PTH"));
    testC = (CompPackage)packages.get(3);
    Expect.expect(testC.getLongName().equalsIgnoreCase("SO16_Gullwing"));

    // sort descending based on package name
    Collections.sort(packages, new CompPackageComparator(false, CompPackageComparatorEnum.LONG_NAME) );
    testC = (CompPackage)packages.get(0);
    Expect.expect(testC.getLongName().equalsIgnoreCase("SO16_Gullwing"));
    testC = (CompPackage)packages.get(1);
    Expect.expect(testC.getLongName().equalsIgnoreCase("LED_LED_RA_PTH"));
    testC = (CompPackage)packages.get(2);
    Expect.expect(testC.getLongName().equalsIgnoreCase("26PINCONN_PTH"));
    testC = (CompPackage)packages.get(3);
    Expect.expect(testC.getLongName().equalsIgnoreCase("1206_Resistor"));

    // sort ascending, based on Package Name alphanumerically
    Collections.sort(packages, new CompPackageComparator(true, CompPackageComparatorEnum.LONG_NAME_ALPHANUMERICALLY) );
    testC = (CompPackage)packages.get(0);
    Expect.expect(testC.getLongName().equalsIgnoreCase("26PINCONN_PTH"));
    testC = (CompPackage)packages.get(1);
    Expect.expect(testC.getLongName().equalsIgnoreCase("1206_Resistor"));
    testC = (CompPackage)packages.get(2);
    Expect.expect(testC.getLongName().equalsIgnoreCase("LED_LED_RA_PTH"));
    testC = (CompPackage)packages.get(3);
    Expect.expect(testC.getLongName().equalsIgnoreCase("SO16_Gullwing"));

    // sort descending based on package name alphanumerically
    Collections.sort(packages, new CompPackageComparator(false, CompPackageComparatorEnum.LONG_NAME_ALPHANUMERICALLY) );
    testC = (CompPackage)packages.get(0);
    Expect.expect(testC.getLongName().equalsIgnoreCase("SO16_Gullwing"));
    testC = (CompPackage)packages.get(1);
    Expect.expect(testC.getLongName().equalsIgnoreCase("LED_LED_RA_PTH"));
    testC = (CompPackage)packages.get(2);
    Expect.expect(testC.getLongName().equalsIgnoreCase("1206_Resistor"));
    testC = (CompPackage)packages.get(3);
    Expect.expect(testC.getLongName().equalsIgnoreCase("26PINCONN_PTH"));

    // sort ascending, based on Family Name
    Collections.sort(packages, new CompPackageComparator(true, CompPackageComparatorEnum.JOINT_TYPE));
    testC = (CompPackage)packages.get(0);

    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.GULLWING));
    testC = (CompPackage)packages.get(1);
    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE));
    testC = (CompPackage)packages.get(2);
    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE));
    testC = (CompPackage)packages.get(3);
    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.RESISTOR));

    // sort descending, based on Family Name
    Collections.sort(packages, new CompPackageComparator(false, CompPackageComparatorEnum.JOINT_TYPE) );
    testC = (CompPackage)packages.get(0);
    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.RESISTOR));
    testC = (CompPackage)packages.get(1);
    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE));
    testC = (CompPackage)packages.get(2);
    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE));
    testC = (CompPackage)packages.get(3);
    Expect.expect(testC.getJointTypeEnum().equals(JointTypeEnum.GULLWING));
  }
}
