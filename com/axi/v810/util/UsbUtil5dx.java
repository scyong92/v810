package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;

/**
 * This is wrapper class for the com.axi.util.UsbUtil class.
 * It converts all the exceptions to HardwareExceptions.
 * @author George A. David
 */
public class UsbUtil5dx
{
  private static UsbUtil5dx _instance;

  private UsbUtil _usbUtil;

  /**
   * @author George A. David
   */
  public static synchronized UsbUtil5dx getInstance(UsbDriverTypeEnum usbDriverType)
  {
    Assert.expect(usbDriverType != null);
    Assert.expect(usbDriverType.equals(UsbDriverTypeEnum.FTDI));

    // note that once more UsbDriverTypeEnums are available we will need to add a Map<UsbDriverTypeEnum, UsbUtil5dx>
    if (_instance == null)
    {
      _instance = new UsbUtil5dx(usbDriverType);
    }

    return _instance;
  }

  /**
   * @author George A. David
   */
  private UsbUtil5dx(UsbDriverTypeEnum usbDriverType)
  {
    _usbUtil = UsbUtil.getInstance(usbDriverType);
  }

  /**
   * @author George A. David
   */
  public void sendMessage(UsbDeviceDescriptionEnum description, String message) throws CommunicationHardwareException
  {
    Assert.expect(description != null);
    Assert.expect(message != null);
    Assert.expect(_usbUtil != null);

    try
    {
      _usbUtil.sendMessage(description.getDescription(), message);
    }
    catch (CommunicationException ex)
    {
      CommunicationHardwareException che = new CommunicationHardwareException(ex.getKey(), ex.getParameters());
      che.initCause(ex);
      throw che;
    }
  }

  /**
   * @author George A. David
   */
  public List<String> getReplies(UsbDeviceDescriptionEnum description) throws CommunicationHardwareException
  {
    Assert.expect(description != null);
    Assert.expect(_usbUtil != null);

    List<String> replies = null;

    try
    {
      replies = _usbUtil.getReplies(description.getDescription());
    }
    catch (CommunicationException ex)
    {
      CommunicationHardwareException che = new CommunicationHardwareException(ex.getKey(), ex.getParameters());
      che.initCause(ex);
      throw che;
    }

    Assert.expect(replies != null);
    return replies;
  }

  /**
   * @author George A. David
   */
  public List<String> sendMessageAndGetReplies(UsbDeviceDescriptionEnum description, String message) throws CommunicationHardwareException
  {
    Assert.expect(description != null);
    Assert.expect(message != null);
    Assert.expect(_usbUtil != null);

    List<String> replies = null;
    try
    {
      replies = _usbUtil.sendMessageAndGetReplies(description.getDescription(), message);
    }
    catch (CommunicationException ex)
    {
      CommunicationHardwareException che = new CommunicationHardwareException(ex.getKey(), ex.getParameters());
      che.initCause(ex);
      throw che;
    }

    Assert.expect(replies != null);
    return replies;
  }

  /**
   * @author George A. David
   */
  public void connect(UsbDeviceDescriptionEnum description) throws CommunicationHardwareException
  {
    Assert.expect(description != null);
    Assert.expect(_usbUtil != null);

    try
    {
      _usbUtil.connect(description.getDescription());
    }
    catch (CommunicationException ex)
    {
      CommunicationHardwareException che = new CommunicationHardwareException(ex.getKey(), ex.getParameters());
      che.initCause(ex);
      throw che;
    }
  }

  /**
   * @author George A. David
   */
  public void disconnect(UsbDeviceDescriptionEnum description) throws CommunicationHardwareException
  {
    Assert.expect(description != null);
    Assert.expect(_usbUtil != null);

    try
    {
      _usbUtil.disconnect(description.getDescription());
    }
    catch (CommunicationException ex)
    {
      CommunicationHardwareException che = new CommunicationHardwareException(ex.getKey(), ex.getParameters());
      che.initCause(ex);
      throw che;
    }
  }
}
