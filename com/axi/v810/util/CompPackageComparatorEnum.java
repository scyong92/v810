package com.axi.v810.util;

/**
 * @author Andy Mechtenberg
 */
public class CompPackageComparatorEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static CompPackageComparatorEnum LONG_NAME = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum LONG_NAME_ALPHANUMERICALLY = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum JOINT_TYPE = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum JOINT_HEIGHT = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum IS_IMPORTED = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum LIBRARY_PATH = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum NUMBER_OF_PADS = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum IS_MODIFIED = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum HAS_MODIFIED_SUBTYPE = new CompPackageComparatorEnum(++_index);
  public static CompPackageComparatorEnum OVERWRITE = new CompPackageComparatorEnum(++_index);

  /**
   * @author Andy Mechtenberg
   */
  private CompPackageComparatorEnum(int id)
  {
    super(id);
  }
}
