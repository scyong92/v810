package com.axi.v810.util;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This class is a wrapper class for the com.axi.util.DirectoryLogger class.
 * It converts all the non AXI exceptions thrown by the DirectoryLogger class into
 * DatastoreExceptions.
 *
 */
public class DirectoryLoggerAxi
{
  private DirectoryLogger _directoryLogger;

  public DirectoryLoggerAxi(String logFilesDirectory,
                            String logFileNamePrefix,
                            String logFileNameExtension,
                            int maxNosOfLogFiles)
  {
    _directoryLogger = new DirectoryLogger(logFilesDirectory, logFileNamePrefix,
                                           logFileNameExtension, maxNosOfLogFiles);
  }

  /**
   * @author John Dutton
   */
  public void log(String logString) throws DatastoreException
  {
    try
    {
      _directoryLogger.log(logString);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;

    }
    catch (CouldNotDeleteFileException cndfe)
    {
      CannotDeleteFileDatastoreException cannotDeleteFileDatastoreException =
          new CannotDeleteFileDatastoreException(cndfe.getFileName());
      cannotDeleteFileDatastoreException.initCause(cndfe);
      throw cannotDeleteFileDatastoreException;
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex =
          new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * @author John Dutton
   */
  public boolean logIfTimedOut(String logString, long elapsedTimeThresholdMillis) throws DatastoreException
  {
    try
    {
      return _directoryLogger.logIfTimedOut(logString, elapsedTimeThresholdMillis);
    }
    catch (CouldNotCreateFileException cncfe)
    {
      CannotCreateFileDatastoreException cannotCreateFileDatastoreException =
          new CannotCreateFileDatastoreException(cncfe.getFileName());
      cannotCreateFileDatastoreException.initCause(cncfe);
      throw cannotCreateFileDatastoreException;

    }
    catch (CouldNotDeleteFileException cndfe)
    {
      CannotDeleteFileDatastoreException cannotDeleteFileDatastoreException =
          new CannotDeleteFileDatastoreException(cndfe.getFileName());
      cannotDeleteFileDatastoreException.initCause(cndfe);
      throw cannotDeleteFileDatastoreException;
    }
    catch (FileFoundWhereDirectoryWasExpectedException ex)
    {
      FileFoundWhereDirectoryWasExpectedDatastoreException dex =
          new FileFoundWhereDirectoryWasExpectedDatastoreException(ex.getDirectoryName());
      dex.initCause(ex);
      throw dex;
    }

  }

  /**
   * @author John Dutton
   */
  public void setAdditionalHeaderInfo(String additionalHeaderInfo)
  {
    _directoryLogger.setAdditionalHeaderInfo(additionalHeaderInfo);
  }

  /**
   * @author John Dutton
   */
  public boolean isLoggingNeeded(long elapsedTimeThresholdMillis)
  {
    return _directoryLogger.isLoggingNeeded(elapsedTimeThresholdMillis);
  }





}
