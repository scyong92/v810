package com.axi.v810.util;

import com.axi.util.*;
import java.io.*;

/**
 * Tests the UsbDeviceDescriptionEnum class
 * @author George A. David
 */
public class Test_UsbDeviceDescriptionEnum extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_UsbDeviceDescriptionEnum());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    UsbDeviceDescriptionEnum deviceDescription = UsbDeviceDescriptionEnum.XRAY_SOURCE;
    Expect.expect(deviceDescription == UsbDeviceDescriptionEnum.XRAY_SOURCE);
    Expect.expect(deviceDescription.getDescription().equals("Xray Source"));
  }
}