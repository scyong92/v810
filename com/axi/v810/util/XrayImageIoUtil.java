package com.axi.v810.util;

import java.awt.image.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;

/**
 *
 * Wrapper class for com.axi.util.images.ImageIoUtil.
 * Converts exceptions to Xray exceptions with localization.
 * @author Kay Lannen
 */
public class XrayImageIoUtil
{
  /**
   * Creates the directory and its parent directories if they do not already exist.
   * @param pathName the directory path to be created

   * @author Kay Lannen
   * @author Matt Wharton
   */
  static public void createImageDirectoryIfNeeded(String pathName) throws DatastoreException
  {
    Assert.expect(pathName != null);

    try
    {
      ImageIoUtil.createImageDirectoryIfNeeded(pathName);
    }

    catch (FileFoundWhereDirectoryWasExpectedException exc)
    {
      DatastoreException dex = new FileFoundWhereDirectoryWasExpectedDatastoreException(exc.getDirectoryName());
      dex.initCause(exc);
      throw dex;
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }
  }


  /**
   * Saves a buffered image to disk as a Jpeg image with embedded text metadata.
   * @param buffImg buffered image to be saved
   * @param filePath filename of the image
   * @param metadata metadata string to be stored within the image header

   * @author Kay Lannen
   */
  static public void saveJpegImage(java.awt.image.BufferedImage buffImg,
                                   String filePath,
                                   String metadata) throws DatastoreException
  {
    Assert.expect(buffImg != null);
    Assert.expect(filePath != null);
    Assert.expect(metadata != null);

    try
    {
      ImageIoUtil.saveJpegImage(buffImg, filePath, metadata);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }

  }

  /**
   * @author Patrick Lacz
   */
  static public void saveImage(Image image, String filePath) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(filePath != null);

    try
    {
      ImageIoUtil.saveImage(image, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }
  }


  /**
   * @author Patrick Lacz
   */
  static public void saveJpegImage(Image image, String filePath) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(filePath != null);

    try
    {
      ImageIoUtil.saveJpegImage(image, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }
  }

  /**
   * @author Patrick Lacz
   */
  static public void savePngImage(Image image, String filePath) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(filePath != null);

    try
    {
      ImageIoUtil.savePngImage(image, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }
  }

  /**
   * @author George A. David
   */
  static public void saveJpegImage(java.awt.image.BufferedImage buffImg,
                                   String filePath) throws DatastoreException
  {
    Assert.expect(buffImg != null);
    Assert.expect(filePath != null);

    try
    {
      ImageIoUtil.saveJpegImage(buffImg, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }

  }

  /**
   * Saves a buffered image to disk as a PNG (Portable Network Graphics) image
   * @param buffImg buffered image to be saved
   * @param filePath filename of the image

   * @author Kay Lannen
   * @author Matt Wharton
   */
  static public void savePngImage(java.awt.image.BufferedImage buffImg,
                                  String filePath) throws DatastoreException
  {
    Assert.expect(buffImg != null);
    Assert.expect(filePath != null);

    try
    {
      // Make sure the directory exists before trying to write the file.
      String dir = FileUtil.getAbsoluteParent(filePath);
      FileUtil.createDirectory(dir);

      ImageIoUtil.savePngImage(buffImg, filePath);
    }
    catch (CouldNotCreateFileException ex)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }

  }

  /**
   * Loads a Jpeg image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * If only the BufferedImage is needed, use loadJpegBufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public Image loadJpegImage(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);

    try
    {
      return ImageIoUtil.loadJpegImage(filePath);
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Loads a Jpeg image from the specified file.
   * If the ImageDescription is needed, use loadJpegImage instead.
   * @param filePath the file name of the JPEG image
   * @return BufferedImage the loaded image

   * @author Kay Lannen
   */
  static public java.awt.image.BufferedImage loadJpegBufferedImage(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);

    try
    {
      return ImageIoUtil.loadJpegBufferedImage(filePath);
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Loads a Png image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * If only the BufferedImage is needed, use loadPngBufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public Image loadPngImage(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);

    try
    {
      return ImageIoUtil.loadPngImage(filePath);
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Loads a Png image from the specified file.
   * If the ImageDescription is needed, use loadJpegImage instead.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public java.awt.image.BufferedImage loadPngBufferedImage(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);

    try
    {
      return ImageIoUtil.loadPngBufferedImage(filePath);
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * Loads an image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * This calls the appropriate load routine based on the file extension.
   * If only the BufferedImage is needed, use loadPngBufferedImage or loadJpegBufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Kay Lannen
   */
  static public Image loadImage(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);

    try
    {
      return ImageIoUtil.loadImage(filePath);
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * FOR DEBUGGING USE ONLY!!!
   * Loads a full-precision image from the specified file.
   * The Image includes an empty ImageDescription and BufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image
   *
   * @author Kay Lannen
   * @author John Heumann

   */
  static public Image loadFullPrecisionImage(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);

    try
    {
      return ImageIoUtil.loadFullPrecisionImage(filePath);
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }

  /**
   * FOR DEBUGGING USE ONLY!!!
   * @author Patrick Lacz
   * @author John Heumann
   */
  static public void saveFullPrecisionImage(Image image, String filePath) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(filePath != null);

    try
    {
      ImageIoUtil.saveFullPrecisionImage(image, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }
  }

  /**
   * FOR DEBUGGING USE ONLY!!!
   * Saves a buffered image to disk as a full-precision image
   * @param buffImg buffered image to be saved
   * @param filePath filename of the image
   *
   * @author Kay Lannen
   * @author Matt Wharton
   * @author John Heumann
   */
  static public void saveFullPrecisionImage(java.awt.image.BufferedImage buffImg,
                                  String filePath) throws DatastoreException
  {
    Assert.expect(buffImg != null);
    Assert.expect(filePath != null);

    try
    {
      // Make sure the directory exists before trying to write the file.
      String dir = FileUtil.getAbsoluteParent(filePath);
      FileUtil.createDirectory(dir);

      ImageIoUtil.saveFullPrecisionImage(buffImg, filePath);
    }
    catch (CouldNotCreateFileException ex)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }
  
  /**
   * @author Wei Chin
   */
  static public void saveTiffImage(Image image, String filePath) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(filePath != null);

    try
    {
      ImageIoUtil.saveTiffImage(image, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }
  }

  /**
   * Loads a Tiff image from the specified file.
   * The Image includes an ImageDescription and BufferedImage.
   * If only the BufferedImage is needed, use loadPngBufferedImage.
   * @param filePath the file name of the JPEG image
   * @return Image the loaded image

   * @author Wei Chin
   */
  static public Image loadTiffImage(String filePath) throws DatastoreException
  {
    Assert.expect(filePath != null);

    try
    {
      return ImageIoUtil.loadTiffImage(filePath);
    }
    catch (CouldNotReadFileException ex)
    {
      DatastoreException dex = new CannotReadDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
    catch (FileDoesNotExistException ex)
    {
      DatastoreException dex = new FileNotFoundDatastoreException(filePath);
      dex.initCause(ex);
      throw dex;
    }
  }
  
  /**
   * @author sham
   */
  static public void saveTiffImage(BufferedImage image, String filePath) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(filePath != null);

    try
    {
      ImageIoUtil.saveTiffImage(image, filePath);
    }
    catch (CouldNotCreateFileException exc)
    {
      DatastoreException dex = new CannotCreateFileDatastoreException(exc.getFileName());
      dex.initCause(exc);
      throw dex;
    }
  }
}
