package com.axi.v810.util;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * This comparator will sort a PadType object on either the family used to inpsect it
 * or the subtype assigned to it.
 * @author Andy Mechtenberg
 */
public class PadJointTypeOrSubtypeComparator implements Comparator<PadType>
{
  private boolean _ascending;
  private boolean _sortOnJointType;
  private boolean _sortOnArtifactCompensation;
  private boolean _sortOnIntegrationLevel;
  private boolean _sortOnShadedByComponent;
  private boolean _sortOnStatus;

  // for subtype compares, these are the types of values
  private static final int _UNTESTABLE = 0;
  private static final int _NO_TEST = 1;
  private static final int _SUBTYPE = 2;
  
  // for status compares, these are the types of values
  private final String _noTest = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOTEST_KEY");
  private final String _noLoad = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_NOLOAD_KEY");
  private final String _untestable = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_UNTESTABLE_KEY");
  private final String _testable = StringLocalizer.keyToString("MMGUI_SUBTYPE_CHANGE_TESTABLE_KEY");

  /**
   * @author Andy Mechtenberg
   * @todo EEW we really need to clean this up. THis should have an enum instead of all these booleans.
   */
  public PadJointTypeOrSubtypeComparator(boolean ascending,
                                         boolean sortOnJointType,
                                         boolean sortOnArtifactCompensation,
                                         boolean sortOnIntegrationLevel,
                                         boolean sortOnShadedByComponent,
                                         boolean sortOnStatus)
  {
    _ascending = ascending;
    _sortOnJointType = sortOnJointType;
    _sortOnArtifactCompensation = sortOnArtifactCompensation;
    _sortOnIntegrationLevel = sortOnIntegrationLevel;
    _sortOnShadedByComponent = sortOnShadedByComponent;
    _sortOnStatus = sortOnStatus;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int compare(PadType lhPad, PadType rhPad)
  {
    Assert.expect(lhPad != null);
    Assert.expect(rhPad != null);

    String lhName = "";
    String rhName = "";

    if (_sortOnJointType)
    {
      lhName = lhPad.getSubtype().getJointTypeEnum().getName();
      rhName = rhPad.getSubtype().getJointTypeEnum().getName();
    }
    else if (_sortOnJointType == false && _sortOnArtifactCompensation == false && _sortOnIntegrationLevel == false && _sortOnShadedByComponent == false && _sortOnStatus == false)
    {
      lhName = lhPad.getSubtype().getShortName();
      PadTypeSettings lhPadSettings = lhPad.getPadTypeSettings();

      rhName = rhPad.getSubtype().getShortName();
      PadTypeSettings rhPadSettings = rhPad.getPadTypeSettings();

      int lhState = _SUBTYPE;
      int rhState = _SUBTYPE;

      // sort on subtype.  This can be one of 2 different types for pads:
      // 1.  Untestable
      // 2.  No Test
      // 3.  An actual subtype name
      // The behavior we want is for the ascending order to be as listed above.  Group together the No Tests, sort the subtype names alphabetically etc.

      if (lhPadSettings.isTestable() == false)
        lhState = _UNTESTABLE;
      else if(lhPadSettings.isInspected() == false)
        lhState = _NO_TEST;
      else
      {
        lhState = _SUBTYPE;
        lhName = lhPad.getSubtype().getShortName();
      }

      if (rhPadSettings.isTestable() == false)
        rhState = _UNTESTABLE;
      else if(rhPadSettings.isInspected() == false)
        rhState = _NO_TEST;
      else
      {
        rhState = _SUBTYPE;
        rhName = rhPad.getSubtype().getShortName();;
      }

      if(_ascending)
      {
        switch(lhState)
        {
          case _UNTESTABLE:
          {
            if(rhState == _UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _NO_TEST:
          {
            if (rhState == _UNTESTABLE)
              return 1;
            if(rhState == _NO_TEST)
              return 0;
            else if(rhState == _SUBTYPE)
              return -1;
            break;
          }
          case _SUBTYPE:
          {
            if(rhState == _UNTESTABLE || rhState == _NO_TEST)
              return 1;
            else if(rhState == _SUBTYPE)
              return lhName.compareTo(rhName);
            break;
          }
          default:
            Assert.expect(false);
        }
      }
      else // descending
      {
        switch(rhState)
        {
          case _UNTESTABLE:
          {
            if (lhState == _UNTESTABLE)
              return 0;
            else
              return -1;
          }
          case _NO_TEST:
          {
            if (lhState == _UNTESTABLE)
              return 1;
            if(lhState == _NO_TEST)
              return 0;
            else
              return -1;
          }
          case _SUBTYPE:
          {
            if(lhState == _UNTESTABLE || lhState == _NO_TEST)
              return 1;
            else if(lhState == _SUBTYPE)
              return rhName.compareTo(lhName);
            break;
          }
          default:
            Assert.expect(false);
        }
      }
      Assert.expect(false);
      return 0;
    }
    else if (_sortOnJointType == false && _sortOnArtifactCompensation && _sortOnIntegrationLevel == false && _sortOnShadedByComponent == false && _sortOnStatus == false)
    {
      lhName = lhPad.getPadTypeSettings().getEffectiveArtifactCompensationState().toString();
      rhName = rhPad.getPadTypeSettings().getEffectiveArtifactCompensationState().toString();
    }
    else if (_sortOnJointType == false && _sortOnIntegrationLevel && _sortOnShadedByComponent == false && _sortOnArtifactCompensation == false && _sortOnStatus == false)
    {
      lhName = lhPad.getPadTypeSettings().getSignalCompensation().toString();
      rhName = rhPad.getPadTypeSettings().getSignalCompensation().toString();
    }
    else if (_sortOnJointType == false && _sortOnIntegrationLevel == false && _sortOnShadedByComponent && _sortOnArtifactCompensation == false && _sortOnStatus == false)
    {
      if (lhPad.hasComponentTypesThatCauseInterferencePatterns() == false)
        lhName = "N/A";
      else
      {
        List<ComponentType> lhCompnentTypes = lhPad.getComponentTypesThatCauseInterferencePatterns();
        Assert.expect(lhCompnentTypes.size() != 0);

        for (ComponentType componentType : lhCompnentTypes)
        {
          lhName += componentType.getComponents().get(0).getReferenceDesignator();
          lhName += ", ";
        }
      }

      if (rhPad.hasComponentTypesThatCauseInterferencePatterns() == false)
        lhName = "N/A";
      else
      {
        List<ComponentType> rhCompnentTypes = rhPad.getComponentTypesThatCauseInterferencePatterns();
        Assert.expect(rhCompnentTypes.size() != 0);

        for (ComponentType componentType : rhCompnentTypes)
        {
          rhName += componentType.getComponents().get(0).getReferenceDesignator();
          rhName += ", ";
        }
      }
    }
    //Kok Chun, Tan - sort on the column == 4 (Status) 
    //XCR-2212
    else if (_sortOnJointType == false && _sortOnArtifactCompensation == false && _sortOnIntegrationLevel == false && _sortOnShadedByComponent == false && _sortOnStatus == true)
    {
      String lhState = getStatusName(lhPad);
      String rhState = getStatusName(rhPad);

      if (_ascending)
      {
        return lhState.compareTo(rhState);
      }
      else //descending
      {
        return rhState.compareTo(lhState);
      }
    }
    else
      Assert.expect(false);


    if (_ascending)
      return lhName.compareToIgnoreCase(rhName);
    else
      return rhName.compareToIgnoreCase(lhName);
  }
  
  /**
   * XCR-2212
   * @author Kok Chun, Tan
   */
  public String getStatusName(PadType Pad)
  {
    if (Pad.getPadTypeSettings().isTestable() == false)
    {
      return _untestable;
    }
    else if (Pad.getComponentType().isLoaded() == false)
    {
      return _noLoad;
    }
    else if (Pad.getPadTypeSettings().isInspected() == false)
    {
      return _noTest;
    }
    else
    {
      return _testable;
    }
  }
}