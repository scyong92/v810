package com.axi.v810.util;

import java.io.*;

import com.axi.util.*;

/**
 * Represents a coordinate with respect to the system fiducial.  The system fiducial
 * is dependent on the scan direction.  Left Feature 1 on the fixed rail is the system fiducial
 * if the x scan direction is right to left.  Right Feature 1 on the fixed rail is the system
 * fiducial if the x scan direction is left to right.  The x-axis grows right and the y-axis grows up.
 * The unit is nanometers.
 *
 * @author Dave Ferguson
 */
public class SystemFiducialCoordinate extends IntCoordinate implements Serializable
{
  /**
   * Constructor.
   *
   * @author Dave Ferguson
   */
  public SystemFiducialCoordinate()
  {
    super();
  }

  /**
   * Copy constructor.
   *
   * @author Dave Ferguson
   */
  public SystemFiducialCoordinate(SystemFiducialCoordinate rhs)
  {
    super(rhs);
  }

  /**
   * Constructor.
   *
   * @param x the x value of this coordinate.
   * @param y the y value of this coordinate.
   * @author Dave Ferguson
   */
  public SystemFiducialCoordinate(int x, int y)
  {
    super(x, y);
  }

  /**
   * @author Peter Esbensen
   */
  public SystemFiducialCoordinate(IntCoordinate intCoordinate)
  {
    super(intCoordinate);
  }

  /**
   * @author Matt Wharton
   */
  private void readObject(ObjectInputStream is) throws IOException, ClassNotFoundException
  {
    is.defaultReadObject();
    deserializePoint(is);
  }

  /**
   * @author Matt Wharton
   */
  private void writeObject(ObjectOutputStream os) throws IOException
  {
    os.defaultWriteObject();
    serializePoint(os);
  }
}
