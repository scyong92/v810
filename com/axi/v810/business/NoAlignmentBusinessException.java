package com.axi.v810.business;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * This excception is thrown if there is no alignment data for a panel or board
 *
 * @author Andy Mechtenberg
 */
public class NoAlignmentBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public NoAlignmentBusinessException()
  {
    super(new LocalizedString("BUS_NO_ALIGNMENT_EXCEPTION_KEY", null));
  }
}
