package com.axi.v810.business;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import java.io.*;
import revertrecipe.*;

/**
 *
 * This Class is to used to call the external jar revertrecipe to do the whole
 * convert process;
 *
 * @author weng-jian.eoh
 */
public class RecipeConverter
{

  private static RecipeConverter _recipeConverter = null;
  
  private String _recipePath;
  private String _originalRevision;
  private String _convertRevision;
  private boolean _isConvertRecipe = false;

  /**
   *
   * @author weng-jian.eoh
   */
  private RecipeConverter() 
  {
    //do nothing
  }

  /**
   *
   * @author weng-jian.eoh
   */
  public static synchronized RecipeConverter getInstance()
  {
    if (_recipeConverter == null)
    {
      _recipeConverter = new RecipeConverter();
    }
    return _recipeConverter;
  }

  /**
   * @author weng-jian.eoh
   */
  public String getOriginalRevision()
  {
    return _originalRevision;
  }

  /**
   * @author weng-jian.eoh
   */
  public String getConvertRevision()
  {
    return _convertRevision;
  }

  /**
   * @author weng-jian.eoh
   */
  public String getRecipe()
  {
    return _recipePath;
  }

  /**
   * @author weng-jian.eoh
   */
  public void convert(String recipe) throws XrayTesterException, CouldNotDeleteFileException
  {
    Assert.expect(recipe != null);

    _recipePath = recipe;

    convert();
  }
  
  public void setConvertRevision(String convertRevision)
  {
     _convertRevision = convertRevision;
  }
  
  public void setOrigninalRevision(String originalRevision)
  {
    _originalRevision = originalRevision;
  }

  /**
   * @author weng-jian.eoh
   * 
   */
  public void setConvertRecipe(boolean convertRecipe)
  {
    _isConvertRecipe = convertRecipe;
  }

  public boolean isConvertRecipe()
  {
    return _isConvertRecipe;
  }

  /**
   * @author weng-jian.eoh
   */
  private void convert() throws XrayTesterException, CouldNotDeleteFileException
  {
    boolean revertJarFileExist = FileUtilAxi.exists(FileName.getRevertRecipeFileFullPath());;
    if (revertJarFileExist == false)
    {
      FileUtilAxi.delete(_recipePath);
      throw new FileNotFoundDatastoreException(FileName.getRevertRecipeFileFullPath());
    }
    
    RecipeVersionConverter converter= RecipeVersionConverter.getAnInstance(_convertRevision);
    try
    {
      converter.revertRecipeToOtherRevision(_recipePath, _originalRevision, _convertRevision);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FileUtilAxi.delete(_recipePath);
      CannotRevertRecipeRevisionBusinessException e = new CannotRevertRecipeRevisionBusinessException(_recipePath, _convertRevision);
      e.initCause(ex);
      throw e;
    }
  }

  /**
   * @author weng-jian.eoh
   */
  public void reset()
  {
    _isConvertRecipe = false;
    _recipePath = null;
    _originalRevision = null;
    _convertRevision = null;
  }
}
