package com.axi.v810.business.virtualLive;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;


/**
 * @author Scott Richardson
 */
public class VirtualLiveSurroundingSliceUtil<K extends ReconstructionRegion>
{
  private Map<Double, Set<K>> _xCoordToReconstructionRegionSetMap = new TreeMap<Double, Set<K>>();
  private Map<Double, Set<K>> _yCoordToReconstructionRegionSetMap = new TreeMap<Double, Set<K>>();

  private Set<K> _reconstructionRegions = new HashSet<K>();

  /**
   * @author Scott Richardson
   */
  public VirtualLiveSurroundingSliceUtil()
  {
    // do nothing
  }

  /**
   * @author George A. David
   * @author Scott Richardson
   */
  public void setReconstructionRegions(Collection<K> reconstructionRegions)
  {
    Assert.expect(reconstructionRegions != null);

    _xCoordToReconstructionRegionSetMap.clear();
    _yCoordToReconstructionRegionSetMap.clear();
    _reconstructionRegions.clear();

    for(K region : reconstructionRegions)
      addReconstructionRegion(region);
  }

  /**
   * @author George A. David
   * @author Scott Richardson
   */
  private void addReconstructionRegion(K reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    PanelRectangle rect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    addReconstructionRegion(reconstructionRegion, rect.getCenterX(), _xCoordToReconstructionRegionSetMap);
    addReconstructionRegion(reconstructionRegion, rect.getCenterY(), _yCoordToReconstructionRegionSetMap);

    _reconstructionRegions.add(reconstructionRegion);
  }

  /**
   * @author George A. David
   * @author Scott Richardson
   */
  private void addReconstructionRegion(K reconstructionRegion, double coordValue,  Map<Double, Set<K>> coordToReconstructionRegionSetMap)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(coordToReconstructionRegionSetMap != null);

    if(coordToReconstructionRegionSetMap.containsKey(coordValue))
    {
      coordToReconstructionRegionSetMap.get(coordValue).add(reconstructionRegion);
    }
    else
    {
      Set<K> reconstructionRegionSet = new HashSet<K>();
      reconstructionRegionSet.add(reconstructionRegion);
      coordToReconstructionRegionSetMap.put(coordValue, reconstructionRegionSet);
    }
  }


  /**
   * @author George A. David
   * @author Scott Richardson
   */
  public Set<K> getReconstructionRegionsAtXcoordinate(double xCoordinate)
  {
    Set<K> regions = new HashSet<K>();
    regions.addAll(_xCoordToReconstructionRegionSetMap.get(xCoordinate));

    return regions;
  }

  /**
   * @author George A. David
   * @author Scott Richardson
   */
  public Set<K> getReconstructionRegionsAtYcoordinate(double yCoordinate)
  {
    Set<K> regions = new HashSet<K>();
    regions.addAll(_yCoordToReconstructionRegionSetMap.get(yCoordinate));

    return regions;
  }

  /**
   * @author Scott Richardson
   */
  public Set<K> getReconstructionRegionsAtReconstructionRegion(K reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    PanelRectangle rect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();

    Set<K> reconstructionRegions = getReconstructionRegionsAtXcoordinate(rect.getCenterX());
    reconstructionRegions.retainAll(getReconstructionRegionsAtYcoordinate(rect.getCenterY()));

    return reconstructionRegions;
  }

  /**
   * Get the slices of interest from the reconstruction regions
   * @author Scott Richardson
   */
  public static Set<Slice> getSlicesAtPlaneOfInterestForRegions(Set<VirtualLiveReconstructionColumn> regions, int zFocusHeightInNanoMeters)
  {
    Assert.expect(regions != null);

    Set<Slice> slicesForRegions = new HashSet<Slice>();
    // for each region, find which slice we want to render, based on the Z focus height
    for (VirtualLiveReconstructionColumn region : regions)
    {
      Assert.expect(region.getFocusGroups().size() == 1);

      Slice desiredSlice = null;
      for (Slice slice : region.getFocusGroups().get(0).getSlices())
      {
        if (slice.getFocusInstruction().getZHeightInNanoMeters() == zFocusHeightInNanoMeters)
        {
          desiredSlice = slice;
          break;
        }
      }
      // If no slice exists at the Z focus height, then create one
      if (desiredSlice == null)
      {
        // create the slice
        desiredSlice = region.insertSliceAtZHeight(zFocusHeightInNanoMeters);
      }

      slicesForRegions.add(desiredSlice);
    }

    return slicesForRegions;
  }
}
