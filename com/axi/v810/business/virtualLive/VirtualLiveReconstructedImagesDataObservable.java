package com.axi.v810.business.virtualLive;

import java.util.*;

/**
 *
 * @author khang-shian.sham
 */
public class VirtualLiveReconstructedImagesDataObservable extends Observable
{
  private static VirtualLiveReconstructedImagesDataObservable _reconstructedImagesDataObservable;

  VirtualLiveReconstructedImagesDataObservable()
  {
    //do nothing
  }

  /**
   * @author khang-shian.sham
   */
  public static VirtualLiveReconstructedImagesDataObservable getInstance()
  {
    if (_reconstructedImagesDataObservable == null)
    {
      _reconstructedImagesDataObservable = new VirtualLiveReconstructedImagesDataObservable();
    }
    return _reconstructedImagesDataObservable;
  }

  /**
   * @author khang-shian.sham
   */
  public void setObservable()
  {
    setChanged();
    notifyObservers();
  }
}


