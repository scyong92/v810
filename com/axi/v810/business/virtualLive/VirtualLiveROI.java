package com.axi.v810.business.virtualLive;

import java.util.*;

/**
 *
 * @author wei-chin.chong
 */
public class VirtualLiveROI extends Observable
{
  private int _centerXInNanoMeter = 0;
  private int _centerYInNanoMeter = 0;
  private int _widthInNanoMeter = 0;
  private int _heightInNanoMeter = 0;
  private int _enlargeUncertaintyInNanoMeter = 0;

  /**
   * @param centerXInNanoMeter
   * @param centerYInNanoMeter
   * @param widthInNanoMeter
   * @param heightInNanoMeter 
   * @author Wei Chin
   */
  VirtualLiveROI(int centerXInNanoMeter, int centerYInNanoMeter,  int widthInNanoMeter, int heightInNanoMeter)
  {
    _centerXInNanoMeter = centerXInNanoMeter;
    _centerYInNanoMeter = centerYInNanoMeter;
    _widthInNanoMeter = widthInNanoMeter;
    _heightInNanoMeter = heightInNanoMeter;
  }
  
  /**
   * @return the _centerXInNanoMeter
   * @author Wei Chin
   */
  public int getCenterXInNanoMeter()
  {
    return _centerXInNanoMeter;
  }

  /**
   * @param centerXInNanoMeter the _centerXInNanoMeter to set
   * @author Wei Chin
   */
  public void setCenterXInNanoMeter(int centerXInNanoMeter)
  {
    _centerXInNanoMeter = centerXInNanoMeter;
  }

  /**
   * @return the _centerYInNanoMeter
   * @author Wei Chin
   */
  public int getCenterYInNanoMeter()
  {
    return _centerYInNanoMeter;
  }

  /**
   * @param centerYInNanoMeter the _centerYInNanoMeter to set
   * @author Wei Chin
   */
  public void setCenterYInNanoMeter(int centerYInNanoMeter)
  {
    _centerYInNanoMeter = centerYInNanoMeter;
  }

  /**
   * @return the _widthInNanoMeter
   * @author Wei Chin
   */
  public int getWidthInNanoMeter()
  {
    return _widthInNanoMeter;
  }

  /**
   * @param widthInNanoMeter the _widthInNanoMeter to set
   * @author Wei Chin
   */
  public void setWidthInNanoMeter(int widthInNanoMeter)
  {
    _widthInNanoMeter = widthInNanoMeter;
  }

  /**
   * @return the _heightInNanoMeter
   * @author Wei Chin
   */
  public int getHeightInNanoMeter()
  {
    return _heightInNanoMeter;
  }

  /**
   * @param heightInNanoMeter the _heightInNanoMeter to set
   * @author Wei Chin
   */
  public void setHeightInNanoMeter(int heightInNanoMeter)
  {
    _heightInNanoMeter = heightInNanoMeter;
  }
  
  /**
   * @param centerXInNanoMeter
   * @param centerYInNanoMeter
   * @param widthInNanoMeter
   * @param heightInNanoMeter 
   * @author Wei Chin
   */
  public void setROI(int centerXInNanoMeter, int centerYInNanoMeter,  int widthInNanoMeter, int heightInNanoMeter)
  {
    _centerXInNanoMeter = centerXInNanoMeter;
    _centerYInNanoMeter = centerYInNanoMeter;
    _widthInNanoMeter = widthInNanoMeter;
    _heightInNanoMeter = heightInNanoMeter;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public boolean isValidShape()
  {
    return _widthInNanoMeter > 0 && _heightInNanoMeter > 0;
  }
  
  public void clearValidShape()
  {
    _widthInNanoMeter = 0 ;
    _heightInNanoMeter = 0 ;
  }
  /**
   * @return 
   * @author Wei Chin
   */
  public java.awt.Shape getShape()
  {
    if(isValidShape() == false)
      return null;
    
    java.awt.Shape shape = new java.awt.Rectangle.Double(getCenterXInNanoMeter() - getWidthInNanoMeter() / 2 - getEnlargeUncertaintyInNanoMeter(),
              getCenterYInNanoMeter() - getHeightInNanoMeter() / 2 - getEnlargeUncertaintyInNanoMeter(),
              getWidthInNanoMeter() + 2 * getEnlargeUncertaintyInNanoMeter(),
              getHeightInNanoMeter() + 2 * getEnlargeUncertaintyInNanoMeter());
    return shape;
  }

  /**
   * @return the _enlargeUncertaintyInNanoMeter
   * @author Wei Chin
   */
  public int getEnlargeUncertaintyInNanoMeter()
  {
    return _enlargeUncertaintyInNanoMeter;
  }

  /**
   * @param enlargeUncertaintyInNanoMeter the _enlargeUncertaintyInNanoMeter to set
   * @author Wei Chin
   */
  public void setEnlargeUncertaintyInNanoMeter(int enlargeUncertaintyInNanoMeter)
  {
    _enlargeUncertaintyInNanoMeter = enlargeUncertaintyInNanoMeter;
  }
}
