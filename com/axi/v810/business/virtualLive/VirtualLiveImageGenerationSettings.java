package com.axi.v810.business.virtualLive;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author khang-shian.sham
 */
public class VirtualLiveImageGenerationSettings extends Observable
{
  private int _enabled = -1;
  
  private java.awt.Shape _panelShape;
  
  private Map<String, VirtualLiveROI> _refDesToVLROIMap = new HashMap<String, VirtualLiveROI>();
  
  private VirtualLiveROI _virtualLiveROI = new VirtualLiveROI(0,0,0,0);
  
  private int _zStepSizeInNanoMeter = MathUtil.convertMilsToNanoMetersInteger(2);
  private int _maxZStepSizeInNanoMeter = MathUtil.convertMilsToNanoMetersInteger(6);
  private int _minZStepSizeInNanoMeter = MathUtil.convertMilsToNanoMetersInteger(1);
  //XCR-3279: XXL Virtual Live Initial value of (From & To) should be different with STD system
  private static int _minZHeightInNanoMeter;
  private static int _maxZHeightInNanoMeter;
  
  //XCR-3279: XXL Virtual Live Initial value of (From & To) should be different with STD system
  private static final int THROUGHPUT_DEFAULT_MIN_Z_HEIGHT_IN_MILS = -130;
  private static final int THROUGHPUT_DEFAULT_MAX_Z_HEIGHT_IN_MILS = 70;
  private static final int XXL_DEFAULT_MIN_Z_HEIGHT_IN_MILS = 80;
  private static final int XXL_DEFAULT_MAX_Z_HEIGHT_IN_MILS = 280;
  private static final int S2EX_DEFAULT_MIN_Z_HEIGHT_IN_MILS = 10;
  private static final int S2EX_DEFAULT_MAX_Z_HEIGHT_IN_MILS = 210;
  private static final int OLP_DEFAULT_MIN_Z_HEIGHT_IN_MILS = -100;
  private static final int OLP_DEFAULT_MAX_Z_HEIGHT_IN_MILS = 100;
  private static int _defaultMinZHeightInMils;
  private static int _defaultMaxZHeightInMils;
  private static int _centerZHeightInNanometers;
  
  private static VirtualLiveImageGenerationSettings _virtualLiveImageGenerationData;
  private int _integrationLevel = 1;
  private int _userGain = 1;
  private StageSpeedEnum _stageSpeed = StageSpeedEnum.ONE;
  private String _componentRefDes = StringLocalizer.keyToString("VP_NO_COMPONENT_KEY");
  private int _bypassAlignment = 0;

  private String _magnificationKey = "0_LOW";
  private boolean _isNeedToSaveStitchImage = false;
  
  private List<String> _componentRefDesList = new ArrayList<String>();
  private Map<String, java.awt.Shape> _componentShapeMap = new HashMap<String, java.awt.Shape>();
  
  private boolean _isMultipleComponentSelectOn = false;

  private final String LOW_MAGNIFICATION_STRING = "0_LOW";
  private final double SERIES_1_MAX_IMAGE_REGION_WIDTH  = MathUtil.convertMilsToNanoMeters(1000);
  private final double SERIES_1_MAX_IMAGE_REGION_HEIGHT = MathUtil.convertMilsToNanoMeters(1000);  
  private final double SERIES_2_LOW_MAG_MAX_IMAGE_REGION_WIDTH  = MathUtil.convertMilsToNanoMeters(3000);
  private final double SERIES_2_LOW_MAG_MAX_IMAGE_REGION_HEIGHT = MathUtil.convertMilsToNanoMeters(3000);
  private final double SERIES_2_HIGH_MAG_MAX_IMAGE_REGION_WIDTH  = MathUtil.convertMilsToNanoMeters(1500);
  private final double SERIES_2_HIGH_MAG_MAX_IMAGE_REGION_HEIGHT = MathUtil.convertMilsToNanoMeters(1500);
  //XCR-2612
  private boolean _isSelectedVirtualLiveRoiInvalid = false;
  /*
   * Kee Chin Seong - This to get ComponentRefDes, So have to store the board name too
   */
  private String _boardName = "";
  
  /*
   * chin-seong, kee
   */
  private double _xrayTubeVoltageValue = 0, _xrayTubeCurrentValue = 0;

  /**
   * @author hee-jihn.chuah - XCR-3279: XXL Virtual Live Initial value of (From & To) should be different with STD system
   */
  static
  {
    if (XrayTester.isHardwareAvailable())
    {
      SystemTypeEnum systemType = XrayTester.getSystemType();
      
      if (systemType.equals(SystemTypeEnum.THROUGHPUT))
      {
        _defaultMinZHeightInMils = THROUGHPUT_DEFAULT_MIN_Z_HEIGHT_IN_MILS;
        _defaultMaxZHeightInMils = THROUGHPUT_DEFAULT_MAX_Z_HEIGHT_IN_MILS;
      }
      else if (systemType.equals(SystemTypeEnum.XXL))
      {
        _defaultMinZHeightInMils = XXL_DEFAULT_MIN_Z_HEIGHT_IN_MILS;
        _defaultMaxZHeightInMils = XXL_DEFAULT_MAX_Z_HEIGHT_IN_MILS;
      }
      else if (systemType.equals(SystemTypeEnum.S2EX))
      {
        _defaultMinZHeightInMils = S2EX_DEFAULT_MIN_Z_HEIGHT_IN_MILS;
        _defaultMaxZHeightInMils = S2EX_DEFAULT_MAX_Z_HEIGHT_IN_MILS;
      }
      else
        Assert.expect(false, "Unknown system type: " + systemType.getName());    
    }
    else
    {
      _defaultMinZHeightInMils = OLP_DEFAULT_MIN_Z_HEIGHT_IN_MILS;
      _defaultMaxZHeightInMils = OLP_DEFAULT_MAX_Z_HEIGHT_IN_MILS;
    }
    
    _minZHeightInNanoMeter = MathUtil.convertMilsToNanoMetersInteger(_defaultMinZHeightInMils);
    _maxZHeightInNanoMeter = MathUtil.convertMilsToNanoMetersInteger(_defaultMaxZHeightInMils);
  }
  
  /**
   * @author khang-shian.sham
   */
  VirtualLiveImageGenerationSettings()
  {
    //do nothing
  }

  /**
   * @author khang-shian.sham
   */
  public static VirtualLiveImageGenerationSettings getInstance()
  {
    if (_virtualLiveImageGenerationData == null)
    {
      _virtualLiveImageGenerationData = new VirtualLiveImageGenerationSettings();
    }

    return _virtualLiveImageGenerationData;
  }

  /**
   * @author khang-shian.sham
   */
  public void setVirtualLiveImageGenerationData(java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    disable();
    getVirtualLiveROI().setROI((int) shape.getBounds2D().getCenterX(), 
                          (int) shape.getBounds2D().getCenterY(), 
                          (int) shape.getBounds2D().getWidth(), 
                          (int) shape.getBounds2D().getHeight());
    
    //XCR-2612 Check whether the X is out of the inspectable width or not 
    int panelWidth = Project.getCurrentlyLoadedProject().getPanel().getWidthAfterAllRotationsInNanoMeters();
    int _minXAllowableRegionInNanometer = ( panelWidth- ProgramGeneration.getInstance().getMaxPanelInspectableWidthInNanoMeters()) / 2;
    int _maxXAllowableRegionInNanometer =  panelWidth - _minXAllowableRegionInNanometer;
    if ( shape.getBounds().getMinX() < _minXAllowableRegionInNanometer || shape.getBounds().getMaxX() > _maxXAllowableRegionInNanometer)
    {
      _isSelectedVirtualLiveRoiInvalid = true;
    }
    else
    {
      _isSelectedVirtualLiveRoiInvalid = false;
    } 
    enable();
    setObservable();
    
    _isMultipleComponentSelectOn = false;
  }
  
  /*
   * @author Kee Chin Seong
   * 
   */
  public void addVirtualLiveImageGenerationData(String refDes, java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    _refDesToVLROIMap.put(refDes, new VirtualLiveROI((int) shape.getBounds2D().getCenterX(), (int) shape.getBounds2D().getCenterY(), (int) shape.getBounds2D().getWidth(), (int) shape.getBounds2D().getHeight() ));
    
    _isMultipleComponentSelectOn = true;
  }
  
  /**
   * @author khang-shian.sham
   */
  public void setPanelShape(java.awt.Shape shape)
  {
    Assert.expect(shape != null);

    _panelShape = shape;
  }

  /**
   * @author khang-shian.sham
   */
  public java.awt.Shape getPanelShape()
  {
    Assert.expect(_panelShape!=null);
    
    return _panelShape;
  }

  /**
   * @author khang-shian.sham
   */
  public void setCenterXInNanoMeter(int x)
  {
    Assert.expect(x >= 0);
    
    getVirtualLiveROI().setCenterXInNanoMeter(x);
  }

  /**
   * @author khang-shian.sham
   */
  public void setCenterYInNanoMeter(int y)
  {
    Assert.expect(y >= 0);

    getVirtualLiveROI().setCenterYInNanoMeter(y);
  }

  /**
   * @author khang-shian.sham
   */
  public void setWidthInNanoMeter(int w)
  {
    getVirtualLiveROI().setWidthInNanoMeter(w);
  }

  /**
   * @author khang-shian.sham
   */
  public void setHeightInNanoMeter(int h)
  {
    Assert.expect(h >= 0);

    getVirtualLiveROI().setHeightInNanoMeter(h);
  }

  /**
   * @author khang-shian.sham
   */
  public void setZStepSizeInNanoMeter(int zSize)
  {
    if (zSize > _maxZStepSizeInNanoMeter)
    {
      zSize = _maxZStepSizeInNanoMeter;
    }
    else if (zSize < _minZStepSizeInNanoMeter)
    {
      zSize = _minZStepSizeInNanoMeter;
    }
    _zStepSizeInNanoMeter = zSize;
    setObservable();
  }

  /**
   * @author khang-shian.sham
   */
  public void setMinZHeightInNanoMeter(int minZHeightInNanoMeter)
  {
    //double minSliceHeight =  XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers();
    //double maxSliceHeight =  XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers();
    double minSliceHeight =  XrayTester.getMinimumAllowableSliceHeightFromNominalSliceInNanometers();
    double maxSliceHeight =  XrayTester.getMaximumAllowableSliceHeightFromNominalSliceInNanometers();
    
    /*
    if(minZHeightInNanoMeter < XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers())
    {
      _minZHeightInNanoMeter = (int)XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers();
    }
    */
    if((minZHeightInNanoMeter < minSliceHeight) && ((XrayTester.isXXLBased() || XrayTester.isS2EXEnabled()) == false))
    {
      _minZHeightInNanoMeter = (int)minSliceHeight;
    }
    else if(minZHeightInNanoMeter > maxSliceHeight)
    {
      _minZHeightInNanoMeter = (int)maxSliceHeight;
    }
    else
    {
      _minZHeightInNanoMeter = minZHeightInNanoMeter;
    }
    setObservable();
  }

  /**
   * @author khang-shian.sham
   */
  public void setMaxZHeightInNanoMeter(int maxZHeightInNanoMeter)
  {
    //double minSliceHeight =  XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers();
    //double maxSliceHeight =  XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers();
    double minSliceHeight =  XrayTester.getMinimumAllowableSliceHeightFromNominalSliceInNanometers();
    double maxSliceHeight =  XrayTester.getMaximumAllowableSliceHeightFromNominalSliceInNanometers();
    
    if(maxZHeightInNanoMeter > maxSliceHeight)
    {
      _maxZHeightInNanoMeter = (int)maxSliceHeight;
    }
    else if(maxZHeightInNanoMeter < minSliceHeight)
    {
      _maxZHeightInNanoMeter = (int)minSliceHeight;
    }
    else
    {
      _maxZHeightInNanoMeter = maxZHeightInNanoMeter;
    }
    setObservable();
  }

  /**
   * @author khang-shian.sham
   */
  public void setEnlargeUncertaintyInNanoMeter(int enlargeUncertaintyInNanoMeter)
  {
    Assert.expect(enlargeUncertaintyInNanoMeter >= 0);

    getVirtualLiveROI().setEnlargeUncertaintyInNanoMeter (enlargeUncertaintyInNanoMeter);
  }

  /**
   * @author khang-shian.sham
   */
  public void setIntegrationLevel(int integrationLevel)
  {
    Assert.expect(integrationLevel > 0);

    _integrationLevel = integrationLevel;
    setObservable();
  }

  /**
   * @author khang-shian.sham
   */
  public void setUserGain(int userGain)
  {
    Assert.expect(userGain > 0);

    _userGain = userGain;
  }
  
  /**
   * @author sheng chuan
   */
  public void setStageSpeed(StageSpeedEnum stageSpeed)
  {
    Assert.expect(stageSpeed != null);

    _stageSpeed = stageSpeed;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setBoardName(String boardName)
  {
    if(_boardName != boardName)      
    {
      _boardName = boardName;
      setObservable();
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public String getBoardName()
  {
    return _boardName;
  }
  
  /**
   * @author Kee Chin Seong
   */
  public void addComponentRefDes(String componentRefDes, boolean isMultiBoardPanel)
  {
    Assert.expect(_componentRefDesList != null);

    if (isMultiBoardPanel)
    {
      String refDesWithBoardName = componentRefDes + "#" + _boardName;
      if(_componentRefDesList.contains(refDesWithBoardName) == false)
        _componentRefDesList.add(refDesWithBoardName);
    }
    else
    {
      if(_componentRefDesList.contains(componentRefDes) == false)
         _componentRefDesList.add(componentRefDes);
    }
  }
  
  /*
   * @author Kee Chin Seong
   */
  public List<String> getComponentList()
  {
    Assert.expect(_componentRefDesList != null);
    
    return _componentRefDesList;
  }
  
   /*
   * @author Jack Hwee
   */
  public void clearComponentList()
  {
    Assert.expect(_componentRefDesList != null);
    _componentRefDesList.clear();
  }
  
  public boolean hasVirtualLiveROI()
  {
    if (_virtualLiveROI == null)
      return false;
    else
      return true;
  }
  
  public void clearVirtualLiveROI()
  {
    Assert.expect(_virtualLiveROI != null);
     _virtualLiveROI.clearValidShape();  
  }
  /*
   * @author Kee Chin Seong
   */
  public void addComponentShape(String componentRefDes, java.awt.Shape shape)
  {
    Assert.expect(_componentShapeMap != null);
    
    _componentShapeMap.put(_boardName, shape);
  }
  
  /**
   * @author khang-shian.sham
   */
  public void setComponentRefDes(String componentRefDes)
  {
    _componentRefDes = componentRefDes;
    setObservable();
  }

  /**
   * @author khang-shian.sham
   */
  public void setBypassAlignment(int bypassAlignment)
  {
    Assert.expect(bypassAlignment >= 0);

    _bypassAlignment = bypassAlignment;
  }
  
  /*
   * @author chin-seong, kee
   */
  public void setXrayTubeVoltage(double value)
  {
    Assert.expect(value > 0);
    
    _xrayTubeVoltageValue = value;
  }
  
    /*
   * @author chin-seong, kee
   */
  public void setXrayTubeCurrent(double value)
  {
    Assert.expect(value > 0);
    
    _xrayTubeCurrentValue = value;
  }

    /*
   * @author chin-seong, kee
   */
  public double getXrayTubeVoltage()
  {
    return _xrayTubeVoltageValue;
  }
  
    /*
   * @author chin-seong, kee
   */
  public double getXrayTubeCurrent()
  {
    return _xrayTubeCurrentValue;
  }
  
  /*
   * @author Chin-Seong, Kee
   */
  public void setIsNeedSaveStitchImage(boolean isNeedToSaveStitchImage)
  {
    _isNeedToSaveStitchImage = isNeedToSaveStitchImage;
  }
  
   /*
   * @author Chin-Seong, Kee
   */
  public boolean isNeedSaveStitchImage()
  {
    return _isNeedToSaveStitchImage;
  }

  /**
   * @author khang-shian.sham
   */
  public int getCenterXInNanoMeter()
  {
    return getVirtualLiveROI().getCenterXInNanoMeter();
  }

  /**
   * @author khang-shian.sham
   */
  public int getCenterYInNanoMeter()
  {
    return getVirtualLiveROI().getCenterYInNanoMeter();
  }

  /**
   * @author khang-shian.sham
   */
  public int getWidthInNanoMeter()
  {
    return getVirtualLiveROI().getWidthInNanoMeter();
  }

  /**
   * @author khang-shian.sham
   */
  public int getHeightInNanoMeter()
  {
    return getVirtualLiveROI().getHeightInNanoMeter();
  }

  /**
   * @author khang-shian.sham
   */
  public int getZStepSizeInNanoMeter()
  {
    return _zStepSizeInNanoMeter;
  }

  /**
   * @author khang-shian.sham
   */
  public int getMinZHeightInNanoMeter()
  {
    return _minZHeightInNanoMeter;
  }

  /**
   * @author khang-shian.sham
   */
  public int getMaxZHeightInNanoMeter()
  {
    return _maxZHeightInNanoMeter;
  }

  /**
   * @author khang-shian.sham
   */
  public int getEnlargeUncertaintyInNanoMeter()
  {
    return getVirtualLiveROI().getEnlargeUncertaintyInNanoMeter();
  }

  /**
   * @author khang-shian.sham
   */
  public int getIntegrationLevel()
  {
    return _integrationLevel;
  }

  /**
   * @author khang-shian.sham
   */
  public int getUserGain()
  {
    return _userGain;
  }

  /**
   * @author ksheng chuan
   */
  public StageSpeedEnum getStageSpeed()
  {
    return _stageSpeed;
  }

  
  /**
   * @author khang-shian.sham
   */
  public String getComponentRefDes()
  {
    return _componentRefDes;
  }

  /**
   * @author khang-shian.sham
   */
  public int getBypassAlignment()
  {
    return _bypassAlignment;
  }

//  /**
//   * @author khang-shian.sham
//   */
//  public double getMaxImageRegionWidth()
//  {
//    return _maxImageRegionWidth;
//  }
//
//  /**
//   * @author khang-shian.sham
//   */
//  public double getMaxImageRegionHeight()
//  {
//    return _maxImageRegionHeight;
//  }
  
  /*
   * @modified Wei Chin
   */
  public java.awt.Shape getShapeFitWithinPanel(String refDes)
  {
    java.awt.Shape shape = null;
    
    VirtualLiveROI currentVirtualLiveROI = _refDesToVLROIMap.remove(refDes);
    shape = currentVirtualLiveROI.getShape();
    
    if(shape != null)
      shape = changeShapeFitToPanelSizeIfNeeded(shape);

    return shape;
  }
  
  /**
   * @author khang-shian.sham
   */
  public java.awt.Shape getShapeFitWithinPanel()
  {
    java.awt.Shape shape = null;

    shape = getVirtualLiveROI().getShape();
    
    if(shape != null)
      shape = changeShapeFitToPanelSizeIfNeeded(shape);
    
    return shape;
  }

  /**
   * @author khang-shian.sham
   * @modified Wei Chin
   */
  private java.awt.Shape changeShapeFitToPanelSizeIfNeeded(java.awt.Shape shape)
  {
    Assert.expect(shape != null);
    // reset invalid Min X and Max X or Min Y and Max Y
    if ( (shape.getBounds2D().getMinX() > getPanelShape().getBounds2D().getMaxX()) || 
         (shape.getBounds2D().getMinY() > getPanelShape().getBounds2D().getMaxY()) || 
         (shape.getBounds2D().getMaxX() < 0) || 
         (shape.getBounds2D().getMaxY() < 0))
    {
//      shape.getBounds2D().setRect(0, 0, 0, 0);
      // Swee-Yee.Wong - Reshape the region to default value
      //XCR 2612 - Reset the starting origin to upper right corner
      double x = Math.max(VirtualLiveImageGenerationSettings.getInstance().getCenterXInNanoMeter()-(VirtualLiveImageGenerationSettings.getInstance().getWidthInNanoMeter()/2), 0);
      double y = Math.max(VirtualLiveImageGenerationSettings.getInstance().getCenterYInNanoMeter()-(VirtualLiveImageGenerationSettings.getInstance().getHeightInNanoMeter()/2), 0);
      shape = new Rectangle2D.Double(x, y,VirtualLiveImageGenerationSettings.getInstance().getWidthInNanoMeter(),VirtualLiveImageGenerationSettings.getInstance().getHeightInNanoMeter());
      return shape;
    }
    
    // Trim if X is negative 
    if (shape.getBounds2D().getMinX() < 0)
    {
//      shape.getBounds2D().setRect(0, shape.getBounds2D().getMinY(), shape.getBounds2D().getWidth() - (0 - shape.getBounds2D().getMinX()), shape.getBounds2D().getHeight());
      shape = new Rectangle2D.Double(0, shape.getBounds2D().getMinY(), shape.getBounds2D().getWidth() - (0 - shape.getBounds2D().getMinX()), shape.getBounds2D().getHeight());
    }
    // Trim if Y is negative
    if (shape.getBounds2D().getMinY() < 0)
    {
//      shape.getBounds2D().setRect(shape.getBounds2D().getMinX(), 0, shape.getBounds2D().getWidth(), shape.getBounds2D().getHeight() - (0 - shape.getBounds2D().getMinY()));
      shape = new Rectangle2D.Double(shape.getBounds2D().getMinX(), 0, shape.getBounds2D().getWidth(), shape.getBounds2D().getHeight() - (0 - shape.getBounds2D().getMinY())); 
    }
    // Trim when X is greater than panel maximum width
    if (shape.getBounds2D().getMaxX() > getPanelShape().getBounds2D().getMaxX())
    {
      shape = new Rectangle2D.Double(shape.getBounds2D().getMinX(),
        shape.getBounds2D().getMinY(),
        shape.getBounds2D().getWidth() - (shape.getBounds2D().getMaxX() - getPanelShape().getBounds2D().getMaxX()),
        shape.getBounds2D().getHeight());
    }
    // Trim when Y is greater than panel maximum height
    if (shape.getBounds2D().getMaxY() > getPanelShape().getBounds2D().getMaxY())
    {
      shape = new Rectangle2D.Double(shape.getBounds2D().getMinX(),
        shape.getBounds2D().getMinY(),
        shape.getBounds2D().getWidth(),
        shape.getBounds2D().getHeight() - (shape.getBounds2D().getMaxY() - getPanelShape().getBounds2D().getMaxY()));
    }
    setEnlargeUncertaintyInNanoMeter(0);

//    if(shapeIsUpdate)
//    {
//      // send event trigger
//      setObservable();
//    }
    return shape;
  }
  
  /**
   * @author sham 
   */
  public void setMagnificationKey(String magnificationKey)
  {
    Assert.expect(magnificationKey != null);
    
    _magnificationKey = magnificationKey;
  }
  
  /**
   * @author sham 
   */
  public String getMagnificationKey()
  {
    Assert.expect(_magnificationKey != null);
    
    return  _magnificationKey;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean isMultipleSelectComponentOn()
  {
    return _isMultipleComponentSelectOn;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void setMultipleSelectComponent(boolean isMultipleComponentSelectOn)
  {
    _isMultipleComponentSelectOn = isMultipleComponentSelectOn;
  }
  
    /**
   * @author khang-shian.sham
   */
  public void setObservable()
  {
    java.awt.Shape shape = getShapeFitWithinPanel();
    if(isEnabled() && shape != null)
    {
      setChanged();
      notifyObservers(shape);
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public void setObservable(java.awt.Shape shape)
  {
    Assert.expect(shape != null);
    
    if(isEnabled())
    {
      setChanged();
      notifyObservers(shape);
    }
  }
  
    /**
   * @author Bill Darbie
   */
  public void enable()
  {
    --_enabled;
    Assert.expect(_enabled >= 0);
  }

  /**
   * @author Bill Darbie
   */
  public void disable()
  {
    if (_enabled == -1)
      _enabled = 0;
    ++_enabled;
  }

  /**
   * @author Bill Darbie
   */
  public boolean isEnabled()
  {
    if (_enabled == 0)
      return true;

    return false;
  }
  
  /**
   * @author Bill Darbie
   */
  public void enableAfterProjectLoad()
  {
    _enabled = 0;
  }

  /**
   * @return the _virtualLiveROI
   */
  public VirtualLiveROI getVirtualLiveROI()
  {
    return _virtualLiveROI;
  }

  /**
   * @author Swee-Yee.Wong
   */
  public double getLowMagMaxImageRegionHeight()
  {
    if (Config.is64bitIrp())
      return SERIES_2_LOW_MAG_MAX_IMAGE_REGION_HEIGHT;
    else
      return SERIES_1_MAX_IMAGE_REGION_HEIGHT;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public double getHighMagMaxImageRegionHeight()
  {
    if (Config.is64bitIrp())
      return SERIES_2_HIGH_MAG_MAX_IMAGE_REGION_HEIGHT;
    else
      return SERIES_1_MAX_IMAGE_REGION_HEIGHT;
  }
  
    /**
   * @author Swee-Yee.Wong
   */
  public double getLowMagMaxImageRegionWidth()
  {
    if (Config.is64bitIrp())
      return SERIES_2_LOW_MAG_MAX_IMAGE_REGION_WIDTH;
    else
      return SERIES_1_MAX_IMAGE_REGION_WIDTH;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public double getHighMagMaxImageRegionWidth()
  {
    if (Config.is64bitIrp())
      return SERIES_2_HIGH_MAG_MAX_IMAGE_REGION_WIDTH;
    else
      return SERIES_1_MAX_IMAGE_REGION_WIDTH;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  public double getMaxImageRegionHeight()
  {
    if(getMagnificationKey().equals(LOW_MAGNIFICATION_STRING))
      return getLowMagMaxImageRegionHeight();
    else
      return getHighMagMaxImageRegionHeight();
  }

  /**
   * @author Swee-Yee.Wong
   */
  public double getMaxImageRegionWidth()
  {
    if(getMagnificationKey().equals(LOW_MAGNIFICATION_STRING))
      return getLowMagMaxImageRegionWidth();
    else
      return getHighMagMaxImageRegionWidth();
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3279: XXL Virtual Live Initial value of (From & To) should be different with STD system
   */
  public int getDefaultMinZHeightInMils()
  {
    return _defaultMinZHeightInMils;
  }
  
  /**
   * @author hee-jihn.chuah - XCR-3279: XXL Virtual Live Initial value of (From & To) should be different with STD system
   */
    public int getDefaultMaxZHeightInMils()
  {
    return _defaultMaxZHeightInMils;
  }
  
   /**
   * @author yi-fong.ooi - XCR 3731: No VL images generated - Offline
   */  
  public int getDefaultCenterZHeightInNanometers()
  {
    SystemTypeEnum systemType = XrayTester.getSystemType();

      if (systemType.equals(SystemTypeEnum.THROUGHPUT))
      {
        _centerZHeightInNanometers = -762000;  //-30 mils
      }
      else if (systemType.equals(SystemTypeEnum.XXL))
      {
        _centerZHeightInNanometers = 4572000; // 180 mils
      }
      else if (systemType.equals(SystemTypeEnum.S2EX))
      {
        _centerZHeightInNanometers = 2794000; // 110 mils
      }
      return _centerZHeightInNanometers;
  }
 
  public boolean isSelectedVirtualLiveRoiInvalid()
  {
    return _isSelectedVirtualLiveRoiInvalid;
  }
}
