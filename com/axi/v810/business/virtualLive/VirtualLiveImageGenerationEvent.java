package com.axi.v810.business.virtualLive;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * @author khang-shian.sham
 */
public class VirtualLiveImageGenerationEvent
{
  private VirtualLiveImageGenerationEventEnum _virtualLiveImageGenerationEventEnum;
  private static XrayTesterException _xrayTesterException;
  // this is used for the begin cases. It tells the observers how many events to expect.
  int _numEventsToExpect = -1;

  /**
   * @author George A. David
   */
  public VirtualLiveImageGenerationEvent(VirtualLiveImageGenerationEventEnum virtualLiveImageGenerationEventEnum)
  {
    Assert.expect(virtualLiveImageGenerationEventEnum != null);

    _virtualLiveImageGenerationEventEnum = virtualLiveImageGenerationEventEnum;
  }

  /**
   * @author George A. David
   */
  public void setVirtualLiveImageGenerationEventEnum(VirtualLiveImageGenerationEventEnum virtualLiveImageGenerationEventEnum)
  {
    Assert.expect(virtualLiveImageGenerationEventEnum != null);

    _virtualLiveImageGenerationEventEnum = virtualLiveImageGenerationEventEnum;

  }

  /**
   * @author George A. David
   */
  public VirtualLiveImageGenerationEventEnum getVirtualLiveImageGenerationEventEnum()
  {
    Assert.expect(_virtualLiveImageGenerationEventEnum != null);

    return _virtualLiveImageGenerationEventEnum;
  }

  /**
   * @author George A. David
   */
  public void setNumEventsToExpect(int numEvents)
  {
    Assert.expect(_virtualLiveImageGenerationEventEnum.equals(VirtualLiveImageGenerationEventEnum.BEGIN_ACQUIRE_VIRTUALLIVE_IMAGES));
    Assert.expect(numEvents > 0);

    _numEventsToExpect = numEvents;
  }

  /**
   * @author George A. David
   */
  public int getNumEventsToExpect()
  {
    Assert.expect(_virtualLiveImageGenerationEventEnum.equals(VirtualLiveImageGenerationEventEnum.BEGIN_ACQUIRE_VIRTUALLIVE_IMAGES));
    Assert.expect(_numEventsToExpect > 0);


    return _numEventsToExpect;
  }

  public void setXrayTesterException(XrayTesterException xrayTesterException)
  {
    _xrayTesterException = xrayTesterException;
  }

  public XrayTesterException getXrayTesterException()
  {
    return _xrayTesterException;
  }
}
