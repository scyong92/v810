package com.axi.v810.business.virtualLive;

import java.util.*;

import com.axi.v810.business.testProgram.*;

/**
 * A ReconstructionLayer is a set of reconstructionColumns that tile the panel. Used to define reconstructionWorkers.
 * @author Scott Richardson
 */
public class ReconstructionLayer<K extends ReconstructionRegion>
{
  private Set<K> _reconstructionRegionSet = new HashSet<K>();

  /**
   * @author Scott Richardson
   */
  public ReconstructionLayer(Set<K> reconstructionRegionSet)
  {
    _reconstructionRegionSet = reconstructionRegionSet;
  }

  /**
   * @author Scott Richardson
   */
  public Set<K> getSet()
  {
    return _reconstructionRegionSet;
  }
}
