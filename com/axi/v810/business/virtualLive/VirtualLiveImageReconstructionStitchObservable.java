package com.axi.v810.business.virtualLive;

import java.util.*;

/**
 *
 * @author khang-shian.sham
 */
public class VirtualLiveImageReconstructionStitchObservable extends Observable
{
  private static VirtualLiveImageReconstructionStitchObservable _virtualLiveImageReconstructionStichObservable;

  VirtualLiveImageReconstructionStitchObservable()
  {
    //do nothing
  }

  /**
   * @author khang-shian.sham
   */
  public static VirtualLiveImageReconstructionStitchObservable getInstance()
  {
    if (_virtualLiveImageReconstructionStichObservable == null)
    {
      _virtualLiveImageReconstructionStichObservable = new VirtualLiveImageReconstructionStitchObservable();
    }
    return _virtualLiveImageReconstructionStichObservable;
  }

  /**
   * @author khang-shian.sham
   */
  public void setObservable(int zHeightInNanometers)
  {
    setChanged();
    notifyObservers(zHeightInNanometers);
  }
}