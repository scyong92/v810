package com.axi.v810.business.virtualLive;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.*;


/** A VirtualLiveReconstructionColumn is an analog of a reconstructionRegion with a monotonically increasing
 * number of slices. The slices of a VirtualLiveReconstructionColumn are used as an index into the
 * VirtualLiveImageProducer's reconstructedImages map (the slice, uniquely identified by an x, y, and z
 * location, keys to the corresponding image). Initially, a reconstructionColumn is a clone of a
 * reconstructionRegion from the virtualLiveTestProgram at a specific x, y, with a specific width and height.
 * As the user moves in Z, slices for the visible reconstructionRegions are inserted into the
 * VirtualLiveReconstructionColumn via the <code>insertSliceAtZHeight</code> method.
 * @author Scott Richardson
 */
public class VirtualLiveReconstructionColumn extends ReconstructionRegion
{
  private Map<Integer, Slice> _zFocusHeightToSliceMap = new HashMap<Integer, Slice>();

  /**
   * Create a deep copy of the ReconstructionRegion
   * @author Scott Richardson
   */
  public VirtualLiveReconstructionColumn(ReconstructionRegion region)
  {
    super();

    Assert.expect(region != null);

    setReconstructionRegionTypeEnum(region.getReconstructionRegionTypeEnum());
    setTopSide(region.isTopSide());
    setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(region.getRegionRectangleRelativeToPanelInNanoMeters()));
    addFocusGroup(VirtualLiveProgramGeneration.getInstance().createFocusGroupForVirtualLiveRegion(new PanelRectangle(
        region.getFocusGroups().get(0).getFocusSearchParameters().getFocusRegion().getRectangleRelativeToPanelInNanometers()), this));
    setTestSubProgram(region.getTestSubProgram());
    setFocusPriority(FocusPriorityEnum.FIRST);

    // Rename the slices as virtual slices (as far as I am concerned, slices are identified by their x, y, and z
    // position, not their slice name). Also populate the zHeightToSliceMap so that slice lookup is easy.
    for(Slice slice : getFocusGroups().get(0).getSlices())
    {
      slice.setSliceName(SliceNameEnum.VIRTUAL_LIVE_VIRTUAL_SLICE);
      _zFocusHeightToSliceMap.put(slice.getFocusInstruction().getZHeightInNanoMeters(), slice);
    }
  }

  /**
   * Insert a slice at the specified Z Height into the reconstructionRegion.
   * @author Scott Richardson
   */
  public Slice insertSliceAtZHeight(int zFocusHeightInNanoMeters)
  {
    Slice slice = new Slice();

    FocusInstruction focusInstruction = new FocusInstruction();
    focusInstruction.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
    focusInstruction.setZHeightInNanoMeters(zFocusHeightInNanoMeters);
    focusInstruction.setZOffsetInNanoMeters(0);
    slice = new Slice();
    slice.setSliceName(SliceNameEnum.VIRTUAL_LIVE_VIRTUAL_SLICE);
    slice.setSliceType(SliceTypeEnum.RECONSTRUCTION);
    slice.setFocusInstruction(focusInstruction);
    slice.setAssociatedReconstructionRegion(this);
    slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);

    getFocusGroups().get(0).addSlice(slice);

    return slice;
  }

  /**
   * @author Scott Richardson
   */  public boolean containsSliceAtZHeight(int zFocusHeightInNanoMeters)
  {
    boolean containsSlice = false;
    if(_zFocusHeightToSliceMap.containsKey(zFocusHeightInNanoMeters))
    {
      containsSlice = true;
    }
    return containsSlice;
  }
}
