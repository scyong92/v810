package com.axi.v810.business.virtualLive;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class VirtualLiveImageGenerationProgressObservable extends Observable
{
  private static VirtualLiveImageGenerationProgressObservable _instance;

  /**
   * @author Cheah Lee Herng
   */
  public static synchronized VirtualLiveImageGenerationProgressObservable getInstance()
  {
    if (_instance == null)
    {
      _instance = new VirtualLiveImageGenerationProgressObservable();
    }
    return _instance;
  }

  /**
   * @author George A. David
   */
  public void stateChanged(VirtualLiveImageGenerationEvent virtualLiveImageGenerationEvent)
  {
    Assert.expect(virtualLiveImageGenerationEvent != null);

    setChanged();
    notifyObservers(virtualLiveImageGenerationEvent);
  }

  /**
   * @author George A. David
   */
  public void stateChanged(VirtualLiveImageGenerationEvent virtualLiveImageGenerationEvent, XrayTesterException xrayTesterException)
  {
    Assert.expect(virtualLiveImageGenerationEvent != null);

    virtualLiveImageGenerationEvent.setXrayTesterException(xrayTesterException);
    setChanged();
    notifyObservers(virtualLiveImageGenerationEvent);
  }
}
