package com.axi.v810.business.virtualLive;

import com.axi.util.*;

/**
 *
 * @author khang-shian.sham
 */
public class VirtualLiveImageGenerationEventEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  public static VirtualLiveImageGenerationEventEnum BEGIN_ACQUIRE_VIRTUALLIVE_IMAGES = new VirtualLiveImageGenerationEventEnum(++_index);
  public static VirtualLiveImageGenerationEventEnum END_ACQUIRE_VIRTUALLIVE_IMAGES = new VirtualLiveImageGenerationEventEnum(++_index);
  public static VirtualLiveImageGenerationEventEnum VIRTUALLIVE_IMAGE_ACQUIRED = new VirtualLiveImageGenerationEventEnum(++_index);
  public static VirtualLiveImageGenerationEventEnum ALIGNMENT_FAIL = new VirtualLiveImageGenerationEventEnum(++_index);
  public static VirtualLiveImageGenerationEventEnum XRAYEXCEPTION_WHEN_GENERATE_VIRTUALLIVE_IMAGES = new VirtualLiveImageGenerationEventEnum(++_index);

  private VirtualLiveImageGenerationEventEnum(int id)
  {
    super(id);
  }
}
