package com.axi.v810.business.virtualLive;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for managing the memory used to store images.  The system
 * will only keeping a certain amount of image data in memory at any one time.  Once this is exceeded,
 * we will need to start swapping images out to disk--this class handles the mechanics of that process.
 *
 * @author Matt Wharton
 * @author Scott Richardson
 */
public class VirtualLiveImageMemoryManager<K>
{
  // Upper memory limit (in bytes) for images.  
  private static final long _IMAGE_MEMORY_MULTIPLIER_FOR_SERIES_1 = 2;
  private static final long _IMAGE_MEMORY_MULTIPLIER_FOR_SERIES_2 = 5;
  private static final long _SINGLE_MEMORY_CHUNCK_IN_BYTES = 1024;  

  // Temporary image cache location.
  private static final String _TEMP_IMAGE_CACHE_DIR = Directory.getTempImageCacheDir();

  private long _memoryUsed = -1L;
  private Map<K, java.awt.image.BufferedImage> _imageNameToBufferedImageInMemoryMap = new LinkedHashMap<K, java.awt.image.BufferedImage>();
  private Map<K, Long> _imageNameToBufferedImageMemoryOnDiskMap = new LinkedHashMap<K,Long>();
  private Map<K, Integer> _imageNameToOnDiskImageZHeightMap = new LinkedHashMap<K, Integer>();
  private Map<K, Integer> _imageNameToZheightMap = new LinkedHashMap<K, Integer>();
  private Map<Integer, java.util.List<Pair<String, Pair<Double,Double>>>> _zHeightToImageNameWithXYPositionListMap =
          new HashMap<Integer, java.util.List<Pair<String, Pair<Double,Double>>>>();

  private int _imageInsertionCounter = 0;
  private static final int _GARBAGE_COLLECTION_FREQUENCY = 50;
  private int MILS_TO_NANOMETER = 25400;
  
  private static long _maximumAvailableMemory = -1L;
  
  static
  {
    if (Config.is64bitIrp() == false)
    {
      // Total allocated memory for Series I is 2GB
      _maximumAvailableMemory = _IMAGE_MEMORY_MULTIPLIER_FOR_SERIES_1 * _SINGLE_MEMORY_CHUNCK_IN_BYTES * _SINGLE_MEMORY_CHUNCK_IN_BYTES * _SINGLE_MEMORY_CHUNCK_IN_BYTES;
    }
    else
    {
      // Total allocated memory for Series I is 5GB
      _maximumAvailableMemory = _IMAGE_MEMORY_MULTIPLIER_FOR_SERIES_2 * _SINGLE_MEMORY_CHUNCK_IN_BYTES * _SINGLE_MEMORY_CHUNCK_IN_BYTES * _SINGLE_MEMORY_CHUNCK_IN_BYTES;
    }
  }

  /**
   * @author Matt Wharton
   */
  public VirtualLiveImageMemoryManager()
  {
    // do nothing, don't initialize as there is the datastoreException
  }

  /**
   * @author Matt Wharton
   * @author Scott Richardson
   */
  public synchronized void initialize() throws DatastoreException
  {
    Assert.expect(_imageNameToBufferedImageInMemoryMap != null);
    Assert.expect(_imageNameToBufferedImageMemoryOnDiskMap != null);

    // Clear all maps and reset the memory used counter.
    _imageNameToBufferedImageInMemoryMap.clear();
    _imageNameToBufferedImageMemoryOnDiskMap.clear();
    _memoryUsed = 0L;
    _imageInsertionCounter = 0;

    // Make sure we have a clean image cache directory.
    if (FileUtilAxi.exists(_TEMP_IMAGE_CACHE_DIR))
    {
      FileUtilAxi.deleteDirectoryContents(_TEMP_IMAGE_CACHE_DIR);
    }
    else
    {
      FileUtilAxi.mkdirs(_TEMP_IMAGE_CACHE_DIR);
    }
  }

  /**
   * @author Matt Wharton
   * @author Scott Richardson
   */
  public synchronized java.awt.image.BufferedImage getImage(K imageName) throws DatastoreException
  {
    Assert.expect(imageName != null);
    Assert.expect(_imageNameToBufferedImageInMemoryMap != null);
    Assert.expect(_imageNameToBufferedImageMemoryOnDiskMap != null);
    Assert.expect((_memoryUsed >= 0) && (_memoryUsed <= _maximumAvailableMemory));

    // Check to see if we have such an image in memory.
    java.awt.image.BufferedImage image = _imageNameToBufferedImageInMemoryMap.get(imageName);
    if (image == null)
    {
      // Images are not in memory, check to see if they are on disk.
      if (_imageNameToBufferedImageMemoryOnDiskMap.containsKey(imageName))
      {
        long availableMemory = _maximumAvailableMemory - _memoryUsed;
        long memoryNeededForImages = _imageNameToBufferedImageMemoryOnDiskMap.get(imageName);

        // Is there enough memory available to swap the images back in from disk?
        if (memoryNeededForImages > availableMemory)
        {
          // Swap entries in memory to disk until there is enough memory to bring in the requested images back
          // into memory from disk.
          Iterator<K> keyIt = _imageNameToBufferedImageInMemoryMap.keySet().iterator();
          while (keyIt.hasNext())
          {
            K swappedOutKey = keyIt.next();
            java.awt.image.BufferedImage swappedOutImages = _imageNameToBufferedImageInMemoryMap.get(swappedOutKey);

            // Remove the entry from the "in memory" map.
            keyIt.remove();

            // Add the entry to the "on disk" map.
            long memoryFreed = memorySize(swappedOutImages);

            // Save the image to disk.
            cacheImageToDisk(swappedOutKey, swappedOutImages, memoryFreed);

            // Update the memory used.
            _memoryUsed -= memoryFreed;
            availableMemory = _maximumAvailableMemory - _memoryUsed;

            // Check to see if we have enough available memory now.
            if (memoryNeededForImages <= availableMemory)
            {
              break;
            }
          }
        }

        // At this point, there had better be enough space in memory for the images--otherwise we are in serious trouble.
        Assert.expect(memoryNeededForImages <= availableMemory, "Insufficient memory to load images from disk!");

        // Load the images from disk.
        image = loadCachedImageFromDisk(imageName);

        // Add an entry to the "in memory" map.
        Assert.expect(image != null);
        _imageNameToBufferedImageInMemoryMap.put(imageName, image);

        // Remove the entry from the "on disk" map.
        _imageNameToBufferedImageMemoryOnDiskMap.remove(imageName);

        // Update the memory used.
        _memoryUsed += memoryNeededForImages;
        availableMemory = _maximumAvailableMemory - _memoryUsed;
      }
      else
      {
        // If we got here, it means the requested image is neither in memory nor on disk.  Bad times!
        Assert.expect(false, "Requested image is neither in memory nor on disk!");
      }
    }

    return image;
  }

  /**
   * Adds the specified Images to the memory manager.  Caches to disk if there isn't enough memory.
   *
   * @author Matt Wharton
   * @author Scott Richardson
   */
  public synchronized void putImage(int zHeightInNanometers, K imageName, java.awt.image.BufferedImage image) throws DatastoreException
  {
    Assert.expect(imageName != null);
    Assert.expect(image != null);
    Assert.expect(_imageNameToBufferedImageInMemoryMap != null);
    Assert.expect(_imageNameToBufferedImageMemoryOnDiskMap != null);
    Assert.expect(_zHeightToImageNameWithXYPositionListMap != null);
    //Assert.expect((_memoryUsed >= 0) && (_memoryUsed <= _MAX_AVAILABLE_IMAGE_MEMORY));

    String[] strArray = null;
    Double x, y;
    Pair <String, Pair<Double,Double>> imageNameWithXYPosition=new Pair<String,Pair<Double, Double>>();
    
    // Run garbage collection periodically to try and keep the memory footprint under control.
    ++_imageInsertionCounter;
    if ((_imageInsertionCounter % _GARBAGE_COLLECTION_FREQUENCY) == 0)
   {
      MemoryUtil.garbageCollect();
    }

    // Check to see if we have space in memory for the images.
    long availableMemory = _maximumAvailableMemory - _memoryUsed;
    long memoryNeededForImages = memorySize(image);
    if (memoryNeededForImages <= availableMemory)
    {
      // We have enough space in memory.

      // Add an entry to the "in memory" map.
      _imageNameToBufferedImageInMemoryMap.put(imageName, image);

      // Update the memory used.
      _memoryUsed += memoryNeededForImages;
    }
    else
    {
      // Save the image to disk.
      cacheImageToDisk(imageName, image, memoryNeededForImages);
      _imageNameToBufferedImageMemoryOnDiskMap.put(imageName,memoryNeededForImages);
      _imageNameToOnDiskImageZHeightMap.put(imageName,zHeightInNanometers);
    }
    _imageNameToZheightMap.put(imageName, zHeightInNanometers);
    strArray = ((String)imageName).split("_");
    if(((String)imageName).matches(".*high.*"))
    {
      x = (double)(Math.round(Integer.parseInt(strArray[2]) * MILS_TO_NANOMETER / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel()));
      y = (double)(Math.round(Integer.parseInt(strArray[3]) * MILS_TO_NANOMETER / MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel()));
    } 
    else
    {
      //Khaw Chek Hau - XCR2837 : Assert When Viewing Result in Fine Tuning/virtual live
      Assert.expect(strArray[1].matches("^[+-]?\\d+$"),
                    strArray[1] + " is not integer, The imageset name is: " + (String)imageName);
      x = (double)(Math.round(Integer.parseInt(strArray[1]) * MILS_TO_NANOMETER / MagnificationEnum.NOMINAL.getNanoMetersPerPixel()));
      y = (double)(Math.round(Integer.parseInt(strArray[2]) * MILS_TO_NANOMETER / MagnificationEnum.NOMINAL.getNanoMetersPerPixel()));
    }
    imageNameWithXYPosition.setPair((String)imageName, new Pair<Double, Double>(x,y));

    if(_zHeightToImageNameWithXYPositionListMap.get(zHeightInNanometers)== null)
    {
      java.util.List<Pair<String,Pair<Double, Double>>> imageNameWithXYPositionList = new java.util.LinkedList<Pair<String,Pair<Double,Double>>>();
      imageNameWithXYPositionList.add(imageNameWithXYPosition);
      _zHeightToImageNameWithXYPositionListMap.put(zHeightInNanometers,imageNameWithXYPositionList);
    }
    else
    {
      _zHeightToImageNameWithXYPositionListMap.get(zHeightInNanometers).add(imageNameWithXYPosition);
    }
    if(_zHeightToImageNameWithXYPositionListMap.get(zHeightInNanometers).size() == VirtualLiveManager.getInstance().getTotalNumberOfRegionsForEachSlice())
    {
      VirtualLiveImageReconstructionStitchObservable.getInstance().setObservable(zHeightInNanometers);
    }
  }

  /**
   * @author Matt Wharton
   * @author Scott Richardson
   */
  protected synchronized void freeImages(K key)
  {
    Assert.expect(key != null);
    Assert.expect(_imageNameToBufferedImageInMemoryMap != null);
    Assert.expect(_imageNameToBufferedImageMemoryOnDiskMap != null);
    Assert.expect((_memoryUsed >= 0) && (_memoryUsed <= _maximumAvailableMemory));

    // Remove the entry from the "in memory" map if needed.

    // Is the entry in the "in memory" map?
    if (_imageNameToBufferedImageInMemoryMap.containsKey(key))
    {
      // Remove the image from the "in memory" map.
      java.awt.image.BufferedImage image =_imageNameToBufferedImageInMemoryMap.remove(key);

      // If we removed an image from memory, update the "memory used" counter to reflect this.
      long memoryFreed = memorySize(image);
      _memoryUsed -= memoryFreed;
    }
    // Is the entry in the "on disk" map?
    else if (_imageNameToBufferedImageMemoryOnDiskMap.containsKey(key))
    {
      // Remove the entry from the "on disk" map.
      _imageNameToBufferedImageMemoryOnDiskMap.remove(key);
    }
    else
    {
      Assert.expect(false, "Requested image is neither on memory nor on disk!");
    }
  }

  /**
   * @author Matt Wharton
   * @author Scott Richardson
   */
  private void cacheImageToDisk(K key, java.awt.image.BufferedImage image, long memoryFreed) throws DatastoreException
  {
    Assert.expect(key != null);
    Assert.expect(image != null);

    if (UnitTest.unitTesting() == false)
    {
      System.out.println("Caching images to disk");
    }

    List<String> cachedImageFullPaths = new LinkedList<String>();
    // Save the Image object.
    String cachedImageFile = key.hashCode() + "_" + ".bi";
    String cachedImageFullPath = _TEMP_IMAGE_CACHE_DIR + File.separator + cachedImageFile;

    // Save the image if it's not already saved.
    if (FileUtilAxi.exists(cachedImageFullPath) == false)
    {
      XrayImageIoUtil.savePngImage(image, cachedImageFullPath);
    }
    else
    {
      // @todo sgr print some form of warning?
    }
    _imageNameToBufferedImageMemoryOnDiskMap.put(key,memoryFreed);
  }

  /**
   * @author Matt Wharton
   * @author Scott Richardson
   */
  private java.awt.image.BufferedImage loadCachedImageFromDisk(K key) throws DatastoreException
  {
    Assert.expect(key != null);

    if (UnitTest.unitTesting() == false)
    {
      System.out.println("Loading cached images from disk");
    }

    // assume that the files exist, i.e. they are not being tampered with
    java.awt.image.BufferedImage image = null;
    // the file can be referenced via its key's hash code.
    String cachedImageFile = key.hashCode() + "_" + ".bi";
    String cachedImageFullPath = _TEMP_IMAGE_CACHE_DIR + File.separator + cachedImageFile;

    image = XrayImageIoUtil.loadPngBufferedImage(cachedImageFullPath);

    Assert.expect(image != null);
    return image;
  }

  /**
   * @author Scott Richardson
   */
  public int size()
  {
    return _imageNameToBufferedImageInMemoryMap.size() + _imageNameToBufferedImageMemoryOnDiskMap.size();
  }

  /**
   * @author Scott Richardson
   */
  public boolean containsKey(K key)
  {
    Assert.expect(key != null);

    if (_imageNameToBufferedImageInMemoryMap.containsKey(key) || _imageNameToBufferedImageMemoryOnDiskMap.containsKey(key))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @author Matt Wharton
   */
  public synchronized float getFractionOfMemoryUsed()
  {
    Assert.expect((_memoryUsed >= 0) && (_memoryUsed <= _maximumAvailableMemory));

    float fractionOfMemoryUsed = (float)_memoryUsed / (float)_maximumAvailableMemory;

    return fractionOfMemoryUsed;
  }

  /**
   * @author Matt Wharton
   */
  public synchronized long getMaximumAvailableImageMemoryInBytes()
  {
    return _maximumAvailableMemory;
  }

  /**
   * @author Matt Wharton
   */
  public synchronized long getMemoryUsed()
  {
    Assert.expect(_memoryUsed >= 0L);

    return _memoryUsed;
  }

  /**
   * NOTE: This method should NOT be called from production code--it's just for unit tests.
   *
   * @author Matt Wharton
   */
  protected synchronized void setMemoryUsed(long memoryUsed)
  {
    Assert.expect(memoryUsed >= 0L);

    _memoryUsed = memoryUsed;
  }

  /**
   * @author Roy Williams
   * @author Matt Whraton
   * @author Scott Richardson
   * @todo sgr make this measure more accurate
   */
  private long memorySize(java.awt.image.BufferedImage image)
  {
    Assert.expect(image != null);

    long sum = image.getWidth() * image.getHeight() * java.awt.image.BufferedImage.TYPE_INT_RGB;

    return sum;
  }

  /**
   * @author sham
   */
  public void clear()
  {
    Assert.expect(_imageNameToBufferedImageInMemoryMap != null);
    Assert.expect(_imageNameToBufferedImageMemoryOnDiskMap != null);
    Assert.expect(_zHeightToImageNameWithXYPositionListMap != null);
    Assert.expect(_imageNameToZheightMap != null);
    Assert.expect(_imageNameToOnDiskImageZHeightMap != null);
    
    _imageNameToBufferedImageInMemoryMap.clear();
    _imageNameToBufferedImageMemoryOnDiskMap.clear();
    _zHeightToImageNameWithXYPositionListMap.clear();
    _imageNameToZheightMap.clear();
    _imageNameToOnDiskImageZHeightMap.clear();
    _memoryUsed = -1L;
    
  }

  /**
   * @author sham
   */
  public int getZHeightInNanometersByImageName(String imageName)
  {
    Assert.expect(imageName != null);

    return _imageNameToZheightMap.get(imageName);
  }

  /**
   * @author sham
   */
  public java.util.List<Pair<String, Pair<Double,Double>>> getImageNameWithXYPositionList(int zHeightInNanometers) throws DatastoreException
  {
    Assert.expect(_zHeightToImageNameWithXYPositionListMap != null);

    return _zHeightToImageNameWithXYPositionListMap.get(zHeightInNanometers);
  }
}
