package com.axi.v810.business.virtualLive;

import java.awt.image.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Instruct the IAE to acquire or reacquire reconstructed images. The IRPs and cameras are designed to listen
 * to only one instance of a controling thread form java, hence, it is necessary that this be a singleton.
 * SEE xRayTest.business.ImageGeneration for reference.
 * @todo sgr Implement Runnable and design this so that it can do an atomic unit (essentially a request
 * reacquisition(...) and the handleVirtualLiveIncomingImages(...)) of re-reconstruction work (as does
 * ImageReciever.java). Perhaps move acquireVirtualLiveImages(...) into VLImageReconstructionManager.
 * @author Scott Richardson
 */
public class VirtualLiveImageGeneration
{
  private static VirtualLiveImageGeneration _instance;
  private ImageManager _imageManager = ImageManager.getInstance();
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();
  private boolean _isGeneratingImages = false;
  private WorkerThread _fetchIncomingImagesWorkerThread = new WorkerThread("virtual live images generation");
  private com.axi.util.TimerUtil getTimer = new com.axi.util.TimerUtil();
  private VirtualLiveImageGenerationProgressObservable _virtualLiveImageGenerationObservable = VirtualLiveImageGenerationProgressObservable.getInstance();
  private Alignment _alignment;
  private boolean _aborted = false;
  private String _imageSetName;
  private Map<String, BufferedImage> _imageNameToBufferedImageMap=new HashMap<String, BufferedImage>();

  /**
   * @author Scott Richardson
   */
  public static synchronized VirtualLiveImageGeneration getInstance()
  {
    if (_instance == null)
    {
      _instance = new VirtualLiveImageGeneration();
    }

    return _instance;
  }

  /**
   * @author Scott Richardson
   */
  private VirtualLiveImageGeneration()
  {
//    FileUtilAxi.delete(Directory.getXrayVirtualLiveImagesDir(Pr));
  }

  /**
   * @author Scott Richardson
   */
  public void acquireVirtualLiveImages(final TestProgram testProgram, final TestExecutionTimer testExecutionTimer)
    throws XrayTesterException
  {
    // NOTE: this thread does not die until all the reconstructionRegions are marked complete

    Assert.expect(testProgram != null);
    Assert.expect(testExecutionTimer != null);
    _isGeneratingImages = true;
    _aborted = false;

    _imageAcquisitionEngine.initialize();
    VirtualLiveImageGenerationEvent virtualLiveImageGenerationEvent = new VirtualLiveImageGenerationEvent(VirtualLiveImageGenerationEventEnum.BEGIN_ACQUIRE_VIRTUALLIVE_IMAGES);

    if (VirtualLiveManager.getInstance().isVirtualLiveWithAlignment())
    {
      virtualLiveImageGenerationEvent.setNumEventsToExpect(testProgram.getFilteredInspectionRegions().size());
    }
    else
    {
      virtualLiveImageGenerationEvent.setNumEventsToExpect(testProgram.getVerificationRegions().size());
    }
    _virtualLiveImageGenerationObservable.stateChanged(virtualLiveImageGenerationEvent);
    try
    {
      handleIncomingVirtualLiveImagesWorkerThread(testProgram);
    }
    finally
    {
      //
    }
    if (_aborted)
    {
      return;
    }


    try
    {
      if (VirtualLiveManager.getInstance().isVirtualLiveWithAlignment())
      {
        // We know the necessary projections are loaded on the IRP after the requisite reconstructed images are
        // returned, at which point, reacquisition can take place and the UI becomes interactive.
        // initialize alignment
        if (_alignment == null)
        {
          _alignment = Alignment.getInstance();
        }

        _alignment.initializeAlignmentProcess(testProgram);
        _imageAcquisitionEngine.acquireImageSetImages(testProgram, false, testExecutionTimer);
      }
      else
      {
        _imageAcquisitionEngine.acquireVerificationImages(testProgram, testExecutionTimer);
      }
      if (_aborted)
      {
        return;
      }
    }
    finally
    {
      _isGeneratingImages = false;
    }


  }

  /**
   * This method encapsulates a reacquisition request AND the cooresponding images fetch into an atomic unit of work.
   * @author Scott Richardson
   */
  public void reacquireImages(final ReconstructionRegion reconstructionRegion)
    throws XrayTesterException
  {
    Assert.expect(reconstructionRegion != null);

    // non-blocking message
    /** @todo sgr This call should really be made on the hardwareWorkerThread. However, in the current implementation,
     * the ImageAcquisitionEngine.acquireVerificationImages(...) method blocks the hardwareWorkerThread
     * until all the reconstructionRegions are marked complete, at which time, the IRP disposes of the
     * corresponding projections. This obviously breaks VirtualLive mode (re-reconstruction is not posible
     * without the requisit projections). For now, make this call on any another thread. The long term solution may
     * be to create a new API in ImageAcquisitionEngine:acquireVirtualLiveImages(...) which is allowed to
     * <code>return</code> after the projections are loaded into memroy without marking the regions complete,
     * thereby allowing control of the hardwareWorkerThread to be relinquished to the IAE.reacquireRequests(...). */
    if (_imageAcquisitionEngine.isAbortInProgress() == false)
    {
      _imageAcquisitionEngine.reacquireImages(reconstructionRegion);
    }
    else
    {
      return;
    } // start the thread to get and store images when available
    handleIncomingVirtualLiveImagesWorkerThread(1);
  }

  /**
   * Get and store the incomming images
   * @author Scott Richardson
   */
  public void handleIncomingVirtualLiveImagesWorkerThread(final int expectedNumberOfReconstructedImages)
  {
    Assert.expect(expectedNumberOfReconstructedImages > 0);

    // start the thread to get and store images when available
    _fetchIncomingImagesWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        try
        {
          VirtualLiveImageProducer virtualLiveImageProducer = VirtualLiveImageProducer.getInstance();
          for (int i = 0; i < expectedNumberOfReconstructedImages; i++)
          {
            BooleanRef abortedDuringGet = new BooleanRef(false);
            // This thread blocks untill the Image Acquisition Engine gets an image
            ReconstructedImages reconstructedImages = null;
            try
            {
              reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(abortedDuringGet);
            }
            catch (XrayTesterException ex)
            {
              abort();
              /** @todo sgr Because this thread is non-blocking, I have no one to report the exception to; so, for now do this. */
              _hardwareObservable.notifyObserversOfException(ex);

              return;
            }

            if (abortedDuringGet.getValue() || reconstructedImages == null)
            {
              return;
            }

            ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
            Assert.expect(reconstructionRegion.getFocusGroups().size() == 1);

            for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
            {
              for (Slice slice : reconstructionRegion.getFocusGroups().get(0).getSlices())
              {
                if (slice.getFocusInstruction().getZHeightInNanoMeters() == reconstructedSlice.getHeightInNanometers())
                {
                  // store the images in a virtualLiveImageMap
                  try
                  {
                    /** @todo sgr this should proabably pass the reconstructedImage to the back up to the
                     * VLImageReconstructionManager.putReconstructedImage(...) method, which then passes it along
                     * to the VLImageProducer. */
                    // slice name indexed by <top>_<xCoordinate>_<yCoordinate>_<width>_<height>_<sliceHeight>
                    String name = reconstructionRegion.getName() + "_" + slice.getFocusInstruction().getZHeightInNanoMeters();
                    String imageName;
                    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
                    {
                      imageName = FileName.getVirtualLiveTiffImageName(name);
                      virtualLiveImageProducer.putReconstructedImage(slice, reconstructedSlice.getHeightInNanometers(), imageName, reconstructedSlice.getImage());
                    }
                    else
                    {
                      imageName = FileName.getVirtualLiveImageName(name);
                      virtualLiveImageProducer.putReconstructedImage(slice, reconstructedSlice.getHeightInNanometers(), imageName, reconstructedSlice.getImage());
                    }
                  }
                  catch (DatastoreException ex)
                  {
                    abort();
                    /** @todo sgr Because this thread is non-blocking, I have no one to report the exception to; so, for now do this. */
                    _hardwareObservable.notifyObserversOfException(ex);

                    return;
                  }
                  break;
                }
              }
            }

            reconstructedImages.decrementReferenceCount();

            // Added by LeeHerng - Make sure we mark the reconstruction region as complete
            // so that IRP can release the image.
            if (_imageAcquisitionEngine.isAbortInProgress() == false)
            {
              _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
            }
            _imageAcquisitionEngine.freeReconstructedImages(reconstructionRegion);
          }
          
          // XCR1576 by Lee Herng - Unable to run Virtual Live for multiple times
          MemoryUtil.garbageCollect();
        }
        catch (XrayTesterException ex)
        {
          abort();
        }
        catch (Throwable ex)
        {
          Assert.logException(ex);
        }
      }
    });
  }

  public void handleIncomingVirtualLiveImagesWorkerThread(final TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    // start the thread to get and store images when available
    _fetchIncomingImagesWorkerThread.invokeLater(new Runnable()
    {
      public void run()
      {
        BooleanLock waitForAlignmentToCompleteLock = null;
        if (VirtualLiveManager.getInstance().isVirtualLiveWithAlignment())
        {
          waitForAlignmentToCompleteLock = new BooleanLock(false);
        }

        try
        {
          if (TestExecution.getInstance().didUserAbort())
          {
            _aborted = true;
            VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
            return;
          }

          VirtualLiveImageProducer virtualLiveImageProducer = VirtualLiveImageProducer.getInstance();
//          getTimer.start();

          for (TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
          {
            //int numRegions = testSubProgram.getVerificationRegions().size();
            int numRegions = 0;
            if (VirtualLiveManager.getInstance().isVirtualLiveWithAlignment())
            {
              numRegions = testSubProgram.getFilteredInspectionRegions().size();
            }
            else
            {
              numRegions = testSubProgram.getVerificationRegions().size();
            }

            if (VirtualLiveManager.getInstance().isVirtualLiveWithAlignment())
            {
              processIncomingAlignmentImages(testSubProgram, waitForAlignmentToCompleteLock, true);
              try
              {
                waitForAlignmentToCompleteLock.waitUntilTrue();
              }
              catch (InterruptedException ex1)
              {
                // do nothing
              }
            }
            if (_aborted)
            {
              return;
            }

            for (int i = 0; i < numRegions; i++)
            {
              BooleanRef abortedDuringGet = new BooleanRef(false);
              // This thread blocks untill the Image Acquisition Engine gets an image
              ReconstructedImages reconstructedImages = null;

              try
              {
                reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(abortedDuringGet);
              }
              catch (XrayTesterException ex)
              {
                abort();
                /** @todo sgr Because this thread is non-blocking, I have no one to report the exception to; so, for now do this. */
                _hardwareObservable.notifyObserversOfException(ex);

                return;
              }

              if (abortedDuringGet.getValue() || reconstructedImages == null || TestExecution.getInstance().didUserAbort())
              {
                if (reconstructedImages != null)
                {
                  reconstructedImages.decrementReferenceCount();
                  reconstructedImages = null;
                }

                _aborted = true;
                abort();
                VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
                return;
              }

              ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
              Assert.expect(reconstructionRegion.getFocusGroups().size() == 1);

              for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
              {
                for (Slice slice : reconstructionRegion.getFocusGroups().get(0).getSlices())
                {
                  // Jack Hwee - handle virtual live multi-gain image generation
                  if (slice.getFocusInstruction().getFocusMethod().equals(FocusMethodEnum.SHARPEST) == false)
                  {
                    if (slice.getFocusInstruction().getZHeightInNanoMeters() == reconstructedSlice.getHeightInNanometers())
                    {
                      // store the images in a virtualLiveImageMap
                      try
                      {
                        /**
                         * @todo sgr this should proabably pass the
                         * reconstructedImage to the back up to the
                         * VLImageReconstructionManager.putReconstructedImage(...)
                         * method, which then passes it along to the
                         * VLImageProducer.
                         */
                        // slice name indexed by <top>_<xCoordinate>_<yCoordinate>_<width>_<height>_<sliceHeight>
                        String name = reconstructionRegion.getName() + "_" + slice.getFocusInstruction().getZHeightInNanoMeters();
                        String imageName;
                        if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
                        {
                          imageName = FileName.getVirtualLiveTiffImageName(name);
                          virtualLiveImageProducer.putReconstructedImage(slice, reconstructedSlice.getHeightInNanometers(), imageName, reconstructedSlice.getImage());
                        }
                        else
                        {
                          imageName = FileName.getVirtualLiveImageName(name);
                          virtualLiveImageProducer.putReconstructedImage(slice, reconstructedSlice.getHeightInNanometers(), imageName, reconstructedSlice.getImage());
                        }
                      }
                      catch (DatastoreException ex)
                      {
                        abort();
                        /**
                         * @todo sgr Because this thread is non-blocking, I have
                         * no one to report the exception to; so, for now do
                         * this.
                         */
                        _hardwareObservable.notifyObserversOfException(ex);

                        return;
                      }

                      break;
                    }
                  }
                }
              }
              reconstructedImages.decrementReferenceCount();

              // save the reconstructed images to disk
//              try
//              {
//                saveReconstructedImagesToDisk(reconstructedImages, false);
//              }
//              catch (DatastoreException ex)
//              {
//                abort();
//                //                _exception = ex;
//
//                return;
//              } 
              // Added by LeeHerng - Make sure we mark the reconstruction region as complete
              // so that IRP can release the image.
              if (_imageAcquisitionEngine.isAbortInProgress() == false)
              {
                _imageAcquisitionEngine.reconstructionRegionComplete(reconstructionRegion, false);
              }
              _imageAcquisitionEngine.freeReconstructedImages(reconstructionRegion);

              VirtualLiveImageGenerationEvent virtualLiveImageGenerationEvent = new VirtualLiveImageGenerationEvent(VirtualLiveImageGenerationEventEnum.VIRTUALLIVE_IMAGE_ACQUIRED);
              _virtualLiveImageGenerationObservable.stateChanged(virtualLiveImageGenerationEvent);
            }
          }
        }
        catch (XrayTesterException ex)
        {
          // abort();
        }
        catch (Throwable ex)
        {
          Assert.logException(ex);
        }
        finally
        {
          VirtualLiveImageGenerationEvent virtualLiveImageGenerationEvent = new VirtualLiveImageGenerationEvent(VirtualLiveImageGenerationEventEnum.END_ACQUIRE_VIRTUALLIVE_IMAGES);
          _virtualLiveImageGenerationObservable.stateChanged(virtualLiveImageGenerationEvent);
        }
      }
    });
  }

  /**
   * @author George A. David
   * @author Scott Richardson
   */
  public void markReconstructionRegionsComplete(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);

    // Mark the reconstruction regions complete
    for (ReconstructionRegion region : testProgram.getVerificationRegions())
    {
      try
      {
        if (_imageAcquisitionEngine.isAbortInProgress() == false)
        {
          _imageAcquisitionEngine.reconstructionRegionComplete(region, false);
        }
        _imageAcquisitionEngine.freeReconstructedImages(region);
      }
      catch (XrayTesterException ex)
      {
        abort();
        _imageAcquisitionEngine.abort();
      }
    }
  }

  public String getImageSetName()
  {
    return _imageSetName;
  }

  public void setImageSetName(String imageSetName)
  {
    Assert.expect(imageSetName != null);

    _imageSetName = imageSetName;
  }

  /**
   * @author Scott Richardson
   */
  private synchronized void saveReconstructedImagesToDisk(ReconstructedImages reconstructedImages, boolean enhanceImageContrast)
    throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);

    String projectName = Project.getCurrentlyLoadedProject().getName();

    // save the reconstructed images to disk
    Assert.expect(projectName != null, "initilize the project");
    _imageManager.saveVirtualLiveImages(projectName, reconstructedImages, enhanceImageContrast);
    System.gc();
  }

  /**
   * @author George A. David
   */
  public void abort()// throws XrayTesterException
  {
    Assert.expect(_imageAcquisitionEngine != null);

    if (_isGeneratingImages)
    {
      try
      {
        _imageAcquisitionEngine.abort();
      }
      catch (XrayTesterException xex)
      {
        /** @todo gad proto code */
        Assert.logException(xex);
      }
    }
  }

  /**
   * debug funtion. try to reacquire reocnstructedImages for the testProgram's reconstruction regions
   * @author Scott Richardson
   */
  private void reacquireReconstructionRegionsDebug(TestProgram testProgram) throws XrayTesterException
  {
    Assert.expect(testProgram != null);
    for (ReconstructionRegion region : testProgram.getVerificationRegions())
    {
      for (int i = 0; i
        < testProgram.getNumberOfVerificationRegions(); i++)
      {
        reacquireImages(region);
      }
    }
  }

  private void handleIncomingAlignmentImages(TestSubProgram subProgram, boolean sendAlignmentInfoToIrps) throws XrayTesterException
  {
    int numRegions = subProgram.getAlignmentRegions().size();

    if (subProgram.isSubProgramPerformAlignment())
    {
      for (int i = 0; i
              < numRegions;
              ++i)
      {
        BooleanRef abortedDuringGet = new BooleanRef(false);
        ReconstructedImages reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(abortedDuringGet);
        if (abortedDuringGet.getValue())
        {
          abort();
          return;
        }

        try
        {         
          //Khaw Chek Hau - XCR2285: 2D Alignment on v810
          if (subProgram.getTestProgram().getProject().getPanel().getPanelSettings().isUsing2DAlignmentMethod())
            _alignment.alignOn2DImage(reconstructedImages, sendAlignmentInfoToIrps); 
          else
            _alignment.alignOnImage(reconstructedImages, sendAlignmentInfoToIrps); 
        }
        catch (XrayTesterException ex)
        {
          if (ex != null)
          {
            //Jack Hwee  
            VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
            //VirtualLiveImageGenerationProgressObservable.getInstance().stateChanged(new VirtualLiveImageGenerationEvent(VirtualLiveImageGenerationEventEnum.XRAYEXCEPTION_WHEN_GENERATE_VIRTUALLIVE_IMAGES), ex);
          }
          throw ex;
        }
        finally
        {
          if (abortedDuringGet.getValue() == false)
          {
            reconstructedImages.decrementReferenceCount();
          }
        }
      }
    }
    else
    {
      // Jack Hwee - always get the first testSubProgram AggregateRuntimeAlignmentTransform
     _imageAcquisitionEngine.applyAlignmentResult(subProgram.getTestProgram().getTestSubProgramById(subProgram.getSubProgramRefferenceIdForAlignment()).getAggregateRuntimeAlignmentTransform(), subProgram.getId());
    }
  }

  /**
   * @param subProgram
   * @param waitForAlignmentToCompleteLock
   * @param sendAlignmentInfoToIrps
   *
   * @author Cheah Lee Herng
   */
  private void processIncomingAlignmentImages(final TestSubProgram subProgram, final BooleanLock waitForAlignmentToCompleteLock, final boolean sendAlignmentInfoToIrps) throws XrayTesterException
  {
    Assert.expect(subProgram != null);

    try
    {
      handleIncomingAlignmentImages(subProgram, sendAlignmentInfoToIrps);
    }
    catch (XrayTesterException ex)
    {
      _aborted = true;
      abort();
      VirtualLiveImageGenerationSettings.getInstance().setMultipleSelectComponent(false);
      VirtualLiveImageGenerationProgressObservable.getInstance().stateChanged(new VirtualLiveImageGenerationEvent(VirtualLiveImageGenerationEventEnum.ALIGNMENT_FAIL), ex);

      throw ex;
    }
    catch (Throwable ex)
    {
      Assert.logException(ex);
    }
    finally
    {
      waitForAlignmentToCompleteLock.setValue(true);
    }
  }

  /**
   * @author sham
   */
  public void setImageNameToBufferedImageMap(String imageName, BufferedImage bufferedImage)
  {
    Assert.expect(imageName != null);
    Assert.expect(bufferedImage != null);

    _imageNameToBufferedImageMap.put(imageName,bufferedImage);
  }

    /**
   * @author sham
   */
  public Map<String, BufferedImage> getImageNameToBufferedImageMap()
  {
    Assert.expect(_imageNameToBufferedImageMap != null);

    return _imageNameToBufferedImageMap;
  }

  /**
   * @author sham
   */
  public void clear()
  {
    _imageNameToBufferedImageMap.clear();
  }
}
