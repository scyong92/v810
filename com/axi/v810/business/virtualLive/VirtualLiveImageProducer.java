package com.axi.v810.business.virtualLive;

import com.axi.util.*;
import com.axi.util.image.Image;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;


/**
 * VirtualLiveImageProducer maintains a map of ReconstructedImages keyed by a Slice object.
 * @author Scott Richardson
 */
public class VirtualLiveImageProducer
{
  private static VirtualLiveImageProducer _virtualLiveImageProducer;

  private Project _project;
  private ImageManager _imageManager = ImageManager.getInstance();

  private VirtualLiveImageReconstructionManager _virtualLiveImageReconstructionManager = new VirtualLiveImageReconstructionManager();
  // A 1:1 correspondance exists between the Slices and the ReconstructedImages.
  // It is preferable to use a Map of buffered images here, as opposed to Images or a ReconstructedImages, because
  // the getBufferedImage() call is very time consuming.
  private VirtualLiveImageMemoryManager<String> _virtualLiveMemoryManagedImageMap = new VirtualLiveImageMemoryManager<String>();

  /**
   * Get, and if necessary re-reconstruct, the slices of interest
   * @author Scott Richardson
   */
  private VirtualLiveImageProducer(Project project)
  {
    Assert.expect(project != null);

    _project = project;
  }

  /**
   * @author Scott Richardson
   */
  public static synchronized VirtualLiveImageProducer getInstance()
  {
    if (_virtualLiveImageProducer == null)
    {
      _virtualLiveImageProducer = new VirtualLiveImageProducer(Project.getCurrentlyLoadedProject());
    }

    return _virtualLiveImageProducer;
  }

  /**
   * Load images from memory, disk, or, if necessary, the Testhead.
   * If the image must be fetched from the testhead, then return a NULL image, which will then prompt
   * the VirtualLiveImageRenderer to display an alternate image, and make a note that the desired image
   * is yet availiable.
   * @author Scott Richardson
   * @author sham
   */
  public java.awt.image.BufferedImage getImage(String imageName) throws XrayTesterException
  {
    java.awt.image.BufferedImage image = null;
      try
      {
        image = getImageFromMemroyOrDisk(imageName);
      }
      catch (DatastoreException ex)
      {
        // If something goes wrong with the import, then return a null image. This will prompt the
        // VirtualLiveImageRenderer to display an alternate image.
        image = null;
      }

    return image;
  }

  /**
   * @author Scott Richardson
   */
  public java.awt.image.BufferedImage getImageFromMemroyOrDisk(String imageName) throws DatastoreException
  {
    Assert.expect(imageName != null);

    java.awt.image.BufferedImage image = null;

    // load image from memory managed map
    if (_virtualLiveMemoryManagedImageMap.containsKey(imageName))
    {
      image = _virtualLiveMemoryManagedImageMap.getImage(imageName);
    }
    // try to load image from disk; if it exists, then add it to the memory managed map
    else
    {
      image = _imageManager.loadVirtualLiveImage(_project.getName(), imageName);
      _virtualLiveMemoryManagedImageMap.putImage(_virtualLiveMemoryManagedImageMap.getZHeightInNanometersByImageName(imageName), imageName, image);
    }

    return image;
  }

  /**
   * @author Scott Richardson
   * @author sham
   */
  public void putReconstructedImage(Slice slice, int zHeightInNanometersString, String imageName, Image reconstructedImage) throws DatastoreException
  {
    Assert.expect(slice != null);
    Assert.expect(imageName != null);
    Assert.expect(reconstructedImage != null);

    //will only put 8 bit image because graphic engine cannot support
//    if (Config.getInstance().getIntValue(SoftwareConfigEnum.USE_IMAGE_TYPE_FORMAT) == SoftwareConfigEnum.getTiffImageType())
//    {
//      _virtualLiveMemoryManagedImageMap.putImage(zHeightInNanometersString, imageName, reconstructedImage.get32BitBufferedImage());
//    }
//    else
//    {
      _virtualLiveMemoryManagedImageMap.putImage(zHeightInNanometersString, imageName, reconstructedImage.getBufferedImage());      
//    }
    _virtualLiveImageReconstructionManager.removeReconstructionWorker(slice);
  }

  /**
   * @author sham
   */
  public VirtualLiveImageMemoryManager<String> getVirtualLiveMemoryManagedImageMap()
  {
    return _virtualLiveMemoryManagedImageMap;
  }
}
