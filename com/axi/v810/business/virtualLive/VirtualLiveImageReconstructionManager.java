package com.axi.v810.business.virtualLive;

import java.util.*;
import java.util.concurrent.locks.*;
import java.util.concurrent.locks.Lock;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Maintains a queue of re-reconstruction requests (slices).
 * Use the SurroundingReconstructionRegionUtil to identify those reconstructionRegions in the TestProgam
 * which are capable of re-reconstructing the request - i.e. those regions that are at the same x and y
 * coordinates with the same width and height as the requested slice. IF a reconstructionRegion is not
 * already being used for another re-reconstruction request, then the re-reconstruction may proceed.
 * ELSE, leave this request in the queue and try the next request. IF no requests can be processed, then
 * block until a reconstruction worker is free.
 * @author Scott Richardson
 */
public class VirtualLiveImageReconstructionManager
{
  // A VirtualLiveReconstructionColumn is mapped to a set of ReconstructionRegions which are available for
  // re-reconstruction requests.
  private Map<VirtualLiveReconstructionColumn, Set<ReconstructionRegion>> _reconstructionColumnToReconstructionWorkerMap =
      new HashMap<VirtualLiveReconstructionColumn, Set<ReconstructionRegion>>();
  private Set<ReconstructionRegion> _busyReconstructionWorkers = new HashSet<ReconstructionRegion>();
  private BooleanLock _waitForRegionWorkerToBeAvailableLock = new BooleanLock();

  // The reconstruction region worker queue is monitored by this lock
  private LinkedList<Slice> _reReconstructionRequestsQueue = new LinkedList<Slice>();
  private Lock _reReconstructionRequestQueueLock = new ReentrantLock();

  private WorkerThread _requestReconstructedImageWorkerThread  = new WorkerThread("virtual live re-reconstruction thread");

  private HardwareObservable _hardwareObservable = HardwareObservable.getInstance();

  /**
   * @author Scott Richardson
   */
  public VirtualLiveImageReconstructionManager()
  {
    _reconstructionColumnToReconstructionWorkerMap = VirtualLiveProgramGeneration.getInstance().getReconstructionWorkers();
  }

  public void removeReconstructionWorker(Slice slice)
  {
    _busyReconstructionWorkers.remove(slice.getAssociatedReconstructionRegion());
    _waitForRegionWorkerToBeAvailableLock.setValue(true);
  }

  /**
   * @author Scott Richardson
   */
  public void requestReconstructedImageWorkerThread() throws XrayTesterException
  {
    Assert.expect(_reReconstructionRequestsQueue != null);

    _waitForRegionWorkerToBeAvailableLock.setValue(false);
    _reReconstructionRequestQueueLock.lock();
    try
    {
      _requestReconstructedImageWorkerThread.invokeLater(new Runnable()
      {
        public void run()
        {
          boolean executedRequest = false;

          LinkedList<Slice> reReconstructionRequestsWithoutReconstructionWorkersQueue = new LinkedList<Slice>();

          // execute exactly one re-reconstruction request
          while (executedRequest == false)
          {
            // true iif all the requests in the _reReconstructionRequestsQueue are moved to the
            // reReconstructionRequestsWithoutReconstructionWorkerQueue; that is, when no reconstruction
            // workers are availiable, no re-reconstruction requests can be executed.
            if (_reReconstructionRequestsQueue.peek() == null)
            {
              _waitForRegionWorkerToBeAvailableLock.setValue(false);
              try
              {
                // region workers become availiable when a reconstructedImage is returned from the IRP, at which point
                // the reconstructionRegion is moved out of the busyReconstructionWorkers set
                _waitForRegionWorkerToBeAvailableLock.waitUntilTrue();
              }
              catch (Exception ex)
              {
                Assert.logException(ex);
              }

              _reReconstructionRequestsQueue = reReconstructionRequestsWithoutReconstructionWorkersQueue;
            }

            Assert.expect(_reReconstructionRequestsQueue != null);
            Slice sliceOfInterest = _reReconstructionRequestsQueue.poll();
            Set<ReconstructionRegion>
                reconstructionWorkers = _reconstructionColumnToReconstructionWorkerMap.get(sliceOfInterest.getAssociatedReconstructionRegion());

            // TRUE if the re-reconstruction request can be fulfilled by a reconstruction region worker
            if (_busyReconstructionWorkers.containsAll(reconstructionWorkers) == false)
            {
              ReconstructionRegion reconstructionRegion = reconstructionWorkers.iterator().next();
              _busyReconstructionWorkers.add(reconstructionRegion);

              // this cast is necessary, as to genericize Slice.java would be difficult
              VirtualLiveProgramGeneration.getInstance().reSpecifyReconstructionRegionSliceHeights((VirtualLiveReconstructionColumn)
                  sliceOfInterest.getAssociatedReconstructionRegion(), reconstructionRegion,
                  sliceOfInterest.getFocusInstruction().getZHeightInNanoMeters());

              for (Slice slice : reconstructionRegion.getFocusGroups().get(0).getSlices())
              {
                // message the IRP to modify the focus instruction z height of the slice
                try
                {
                  FocusInstruction focusInstruction = slice.getFocusInstruction();
                  byte focusMethodId = ReconstructionTypeEnum.map(focusInstruction.getFocusMethod());
                  ImageAcquisitionEngine.getInstance().modifyFocusInstructionZHeight(reconstructionRegion.
                      getTestSubProgram().getId(),
                      reconstructionRegion, slice.getSliceName().getId(),
                      focusInstruction.getZHeightInNanoMeters(),
                      focusInstruction.getPercentValue(),
                      focusInstruction.getZHeightInNanoMeters(),
                      focusMethodId);
                }
                catch (XrayTesterException ex1)
                {
                  /** @todo sgr Because this thread is non-blocking, I have no one to report the exception to; so, for now do this. */
                  _hardwareObservable.notifyObserversOfException(ex1);
                  return;
                }
              }

              // submit the slice to the reReconstruction queue
              try
              {
                VirtualLiveImageGeneration.getInstance().reacquireImages(reconstructionRegion);
              }
              catch (XrayTesterException ex2)
              {
                /** @todo sgr Because this thread is non-blocking, I have no one to report the exception to; so, for now do this. */
                _hardwareObservable.notifyObserversOfException(ex2);
                return;
              }

              executedRequest = true;
            }
            else
            {
              reReconstructionRequestsWithoutReconstructionWorkersQueue.addFirst(sliceOfInterest);
            }
          }
          // prepend all the reconstruction requests that could not be processed to the front of the list so
          // that they can get first dibs on the reconstruction workers
          for (Slice reReconstructionRequestWithoutReconstructionWorkersQueue : reReconstructionRequestsWithoutReconstructionWorkersQueue)
          {
            _reReconstructionRequestsQueue.addFirst(reReconstructionRequestWithoutReconstructionWorkersQueue);
          }
        }
      });
    }
    finally
    {
      _reReconstructionRequestQueueLock.unlock();
    }
  }

  /**
   * @author Scott Richardson
   */
  public void add(Slice slice) throws XrayTesterException
  {
    _reReconstructionRequestQueueLock.lock();
    try
    {
      _reReconstructionRequestsQueue.add(slice);
    }
    finally
    {
      _reReconstructionRequestQueueLock.unlock();
    }

    requestReconstructedImageWorkerThread();
  }
}
