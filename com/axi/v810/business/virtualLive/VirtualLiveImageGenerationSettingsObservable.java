package com.axi.v810.business.virtualLive;

import com.axi.util.*;
import java.util.*;

/**
 *
 * @author khang-shian.sham
 */
public class VirtualLiveImageGenerationSettingsObservable extends Observable
{
  private static VirtualLiveImageGenerationSettingsObservable _virtualLiveImageGenerationDataObservable;

  VirtualLiveImageGenerationSettingsObservable()
  {
    //do nothing
  }

  /**
   * @author khang-shian.sham
   */
  public static VirtualLiveImageGenerationSettingsObservable getInstance()
  {
    if (_virtualLiveImageGenerationDataObservable == null)
    {
      _virtualLiveImageGenerationDataObservable = new VirtualLiveImageGenerationSettingsObservable();
    }
    return _virtualLiveImageGenerationDataObservable;
  }

  /**
   * @author khang-shian.sham
   */
  public void setObservable()
  {
    java.awt.Shape shape = VirtualLiveImageGenerationSettings.getInstance().getShapeFitWithinPanel();
    if(shape != null)
    {
      setChanged();
      notifyObservers(shape);
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public void setObservable(java.awt.Shape shape)
  {
    Assert.expect(shape != null);  
    setChanged();
    notifyObservers(shape);
  }
}
