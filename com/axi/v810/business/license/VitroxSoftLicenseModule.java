package com.axi.v810.business.license;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import java.util.*;

/**
 *
 * @author wei-chin.chong
 */
public class VitroxSoftLicenseModule extends AbstractVitroxLicenseModule
{
  // vitrox License
  static private final int PRODUCT_ID = 8;
  static private final String _MACHINE_KEY = "MACHINE";
  static private final String _TDW_KEY = "TDW";
  static private final String _DEVELOPER_KEY = "DEVELOPER";
  static private final String _VIRTUAL_LIVE_KEY = "VIRTUAL_LIVE";
  static private final String _PSP_KEY = "PSP";
  static private final String _VM_KEY = "VM";
  static private final String _LC_KEY = "LC"; // Chnee Khang Wah, 2011-11-30, Low Cost AXI
  static private final String _XXL_KEY = "XXL"; 
  static private final String _S2EX_KEY = "S2EX";
  static private final String _THROUGHPUT_KEY = "THROUGHPUT";

  static private final String TRUE_VALUE = "TRUE";
  static private final String FALSE_VALUE = "FALSE";
  
  static private ViTroxLicenseUtil _vitroxLicenseUtil = null;
  static private VitroxSoftLicenseModule _instance = null;
  
  /**
   * @throws VitroxLicenseException 
   * @author Wei Chin
   */
  VitroxSoftLicenseModule() throws VitroxLicenseException
  {
    _vitroxLicenseUtil = ViTroxLicenseUtil.getInstance();
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static AbstractVitroxLicenseModule getInstance() throws BusinessException
  {
    try
    {
      if(_instance == null)
        _instance = new VitroxSoftLicenseModule();
    }
    catch(VitroxLicenseException exception)
    {
      throw new LicenseGeneralBusinessException(exception);
    }    
    return _instance;
  }

  /**
   * Check for the License Expired, Host, and Version
   *
   * @author Wei Chin, Chong
   */
  public void checkThatValidLicenseExists() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return;
    }

    try
    {
      String customString = _vitroxLicenseUtil.checkLicenseStatus(PRODUCT_ID, _SOFTWARE_VERSION_NUMBER);
      StringTokenizer token = new StringTokenizer(customString, ViTroxLicenseUtil._SEPERATOR);

      while(token.hasMoreTokens())
      {
        String currentToken = token.nextToken();
        boolean currentfeature = false;
        if( currentToken.contains(TRUE_VALUE) )
        {
          currentfeature = true;
        }
        else if(currentToken.contains(FALSE_VALUE))
        {
          currentfeature = false;
        }
        if(currentToken.startsWith(_MACHINE_KEY))
        {
          _enabledV810 = currentfeature;
        }
        else if(currentToken.startsWith(_TDW_KEY))
        {
          _enabledTdw = currentfeature;
        }
        else if(currentToken.startsWith(_DEVELOPER_KEY))
        {
          _enabledDiagnostics = currentfeature;
          if(_enabledDeveloperSystem == null)
            _enabledDeveloperSystem = new Boolean(currentfeature);
        }
        else if(currentToken.startsWith(_VIRTUAL_LIVE_KEY))
        {
          _enabledVirtualLive = currentfeature;
        }
        else if(currentToken.startsWith(_PSP_KEY))
        {
          _enabledPSP = currentfeature;
        }
        else if(currentToken.startsWith(_VM_KEY))
        {
          _enabledVM = currentfeature;
        }
        else if(currentToken.startsWith(_LC_KEY)) // Chnee Khang Wah, 2011-11-30, Low Cost AXI
        {
          _enabledLowCost = currentfeature;
        }
        else if(currentToken.startsWith(_XXL_KEY))
        {
          _enabledXXL = currentfeature;
        }
        else if(currentToken.startsWith(_S2EX_KEY))
        {
          _enabledS2EX = currentfeature;
        }
        else if (currentToken.startsWith(_THROUGHPUT_KEY))
        {
          _enabledThroughput = currentfeature;
        }
      }
      if (_enabledV810 != null && _enabledV810 == false && 
          _enabledTdw != null && _enabledTdw == true)
      {
        // XCR-3589 Combo STD and XXL software GUI
        if (((_enabledThroughput != null && _enabledThroughput) ||
             (_enabledS2EX != null && _enabledS2EX) ||
             (_enabledXXL != null && _enabledXXL)) == false)
        {
          throw new VitroxLicenseException("LICENSE_INVALID_EXCEPTION_KEY", "");
        }
      }
    }
    catch(VitroxLicenseException vle)
    {
      //Khaw Chek Hau - XCR2737: Assert when there is invalid license in XXL machine
      for (SystemTypeEnum systemTypeEnum : SystemTypeEnum.getAllSystemTypes())
      {
        if (FileUtil.exists(Directory.getSystemTypeMotionDriveDir(systemTypeEnum)))
        {
          if (systemTypeEnum == SystemTypeEnum.XXL)
            _enabledXXL = true;  
          else if (systemTypeEnum == SystemTypeEnum.S2EX)
            _enabledS2EX = true;  
        }
      }
      BusinessException be = new LicenseInvalidBusinessException(vle.getLocalizedString());
      throw be;
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isV810() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledV810 == null)
      _enabledV810 = true;
    
    return _enabledV810;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isTdw() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledTdw == null)
      _enabledTdw = false;
    
    return _enabledTdw;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean areDiagnosticsEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    return _enabledDiagnostics;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isEarlyPrototypeHardwareEnabled() throws BusinessException
  {
    return false;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isVirtualLiveEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledVirtualLive == null)
      _enabledVirtualLive = false;
    return _enabledVirtualLive;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isVariableMagnificationEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledVM == null)
      _enabledVM = false;
    return _enabledVM;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isPSPEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledPSP == null)
      _enabledPSP = false;
    
    return _enabledPSP;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isXXLEnabled()
  {
    if (_bypassLicense == true)
    {
      return false;
    }
    
    if(_enabledXXL == null)
      _enabledXXL = false;
    
    return _enabledXXL;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public boolean isS2EXEnabled()
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledS2EX == null)
      _enabledS2EX = false;
    
    return _enabledS2EX;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isThroughputEnabled()
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if (_enabledThroughput == null)
    {
      _enabledThroughput = false;
    }
    
    return _enabledThroughput;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isDeveloperSystemEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledDeveloperSystem == null)
      checkThatValidLicenseExists();
    if(_enabledDeveloperSystem == null)
      _enabledDeveloperSystem = false;
    
    return _enabledDeveloperSystem.booleanValue();
  }
  
  /**
   * @author Wei Chin
   */
  public void releaseLicense()
  {
    // do nothing
  }
}
