package com.axi.v810.business.license;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.datastore.*;

/**
 * This class provides a basic test harness for the Sentinel licensing for the
 * v810.
 * @author Reid Hayhow
 * @author Wei Chin, Chong
 */
public class Test_LicenseManager extends UnitTest
{
  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    //Trivial test class to ensure that licenses can be obtained and work
    
    try
    {
      //Do a high level check for a valid license, should pass
      LicenseManager.checkThatValidLicenseExists();

      //This should always be set/available in regression test builds
      Expect.expect(LicenseManager.isV810());

      //Check TDW license
      Expect.expect(LicenseManager.isTdw());

      //Check diagnostics
      Expect.expect(LicenseManager.areDiagnosticsEnabled());

      //Check early proto hardware feature
      // Manually add the feature to the license file before testing
      // Expect.expect(lm.isEarlyPrototypeHardwareEnabled());

      // Check developer system license
      Expect.expect(LicenseManager.isDeveloperSystemEnabled());
    }
    //Any license failures will generate a Business Exception
    catch (BusinessException ex)
    {

      //If it is an invalid license, dump the contents for a postmortem
      if (ex instanceof LicenseInvalidBusinessException)
      {
        //Dump a preamble with directory info
//        os.print("An invalid license was found, printing contents of " +
//                 Directory.getLicenseDir() +
//                 File.separator +
//                 "standard.lic");
////        try
//        {
//          //Open the license file
//          BufferedReader in = new BufferedReader(new FileReader(Directory.getLicenseDir() + File.separator + "standard.lic"));
//          String str;
//
//          //Print each line to a string
//          while ((str = in.readLine()) != null)
//          {
//            //Dump the string into our PrintWriter
//            os.println(str);
//          }
//          //Close the file/reader
//          in.close();
//        }
//        //If it wasn't there, we want to know that as well
//        catch (IOException e)
//        {
//          os.print("Didn't find the standard.lic at " + Directory.getLicenseDir());
//        }
      }

      //dump the stack if we caught a Business Exception
      ex.printStackTrace(os);

    }
    catch(Exception ex)
    {
      ex.printStackTrace(os);
    }
  }

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_LicenseManager());
  }
}
