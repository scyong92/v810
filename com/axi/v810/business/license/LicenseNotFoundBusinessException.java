package com.axi.v810.business.license;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * This exception is thrown if the pay per use license is not found
 *
 * @author Erica Wheatcroft
 */
public class LicenseNotFoundBusinessException extends BusinessException
{
  /**
   * @author Erica Wheatcroft
   */
  public LicenseNotFoundBusinessException(String licenseDir, String hostId)
  {
    super(new LocalizedString("BUS_SOFTWARE_LICENSE_NOT_FOUND_KEY", new Object[]{licenseDir, hostId}));
    Assert.expect(licenseDir != null);
  }

  /**
   * @author Wei Chin, Chong
   */
  public LicenseNotFoundBusinessException()
  {
    super(new LocalizedString("BUS_SOFTWARE_LICENSE_NOT_FOUND_KEY", null));
  }
}