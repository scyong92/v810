package com.axi.v810.business.license;

import com.axi.util.*;
import com.axi.v810.business.*;
import java.util.*;

/**
 *
 * @author wei-chin.chong
 */
public class SentinelEMSLicenseModule extends AbstractVitroxLicenseModule
{
    // sentinel EMS License
  static private final int SEMS_VERSION_STRING = 0x0001;
  static private final int SEMS_MAC_STRING = 0x0006;
  static private final int SEMS_MACHINE_BOOLEAN = 0x0020;
  static private final int SEMS_TDW_BOOLEAN = 0x0021;
  static private final int SEMS_VIRTUAL_LIVE_BOOLEAN = 0x0022;
  static private final int SEMS_PSP_BOOLEAN =  0x0023;
  static private final int SEMS_VM_BOOLEAN  =  0x0024;
  static private final int SEMS_DEVELOPER_BOOLEAN = 0x0025;
  static private final int SEMS_DIAGNOSTICS_BOOLEAN = 0x0026;
  static private final int SEMS_XXL_BOOLEAN =  0x0027; 
  static private final int SEMS_S2EX_BOOLEAN  = 0x0028; 
  static private final int SEMS_MINI_BOOLEAN = 0x0029;
  static private final int SEMS_THROUGHPUT_BOOLEAN = 0x0030;
  static private final int SEMS_MAC_STRING_LENGTH = 20;
  static private final int SEMS_VERSION_STRING_LENGTH = 5;
  
  static private SentinelEMSLicenseModule _instance = null;
  static private SentinelEMS _sentinelEms = null;
  
  static private final String DEMO = "DEMO";
  /**
   * @throws DongleInitialiseException 
   * @author Wei Chin
   */
  SentinelEMSLicenseModule() throws DongleInitialiseException
  {
    _sentinelEms = SentinelEMS.getInstance();
    init();
  }
  
  
  /**
   * @throws DongleInitialiseException 
   * @author Wei Chin
   */
  private void init()
  {
    // do nothing
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static AbstractVitroxLicenseModule getInstance() throws BusinessException
  {
    try
    {
      if(_instance == null)
        _instance = new SentinelEMSLicenseModule();
    }
    catch(DongleInitialiseException exception)
    {
      throw new LicenseGeneralBusinessException(exception);
    }    
    return _instance;
  }
  /**
   * Check for the License Expired, Host, and Version
   *
   * @author Wei Chin, Chong
   */
  public void checkThatValidLicenseExists() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return;
    }
    try
    {
      if(_sentinelEms.hadLogin() == false)
        _sentinelEms.login();

      boolean hasLicenseExpiration = hasLicenseExpiration();
      boolean licenseNotExpired = true;
      if(hasLicenseExpiration)
        licenseNotExpired = isLicenseNotExpired();
      boolean validHostId = isMacAddressCorrect();
      boolean demoHostId = isDemoAddress();
      boolean validVersion = isValidVersion(_SOFTWARE_VERSION_NUMBER);
      
      if (isTdw() && isV810() == false)
      {
        if ((isS2EXEnabled()||isXXLEnabled()||isThroughputEnabled()) == false)
        {
          LocalizedString errorMessage = new LocalizedString("LICENSE_INVALID_EXCEPTION_KEY",null);
          BusinessException be = new LicenseInvalidBusinessException(errorMessage);
          throw be;
        }
      }
      
      if(demoHostId && licenseNotExpired)
        return;

      if (licenseNotExpired == false)
      {
        BusinessException be = new LicenseInvalidBusinessException(getExpiredDate(),
                               Integer.toString(getExecutionCounts()));
        throw be;
      }

      if (validHostId == false || validVersion == false)
      {
        BusinessException be = new LicenseInvalidBusinessException(NetworkUtil.getInstance().getMACAddresses().toString());
        throw be;
      }
    }
    catch(DongleInitialiseException | DongleReadWriteException die)
    {
      throw new LicenseGeneralBusinessException(die);
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isV810() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledV810 == null)
    {
      try
      {
        byte[] data = new byte[1];

        _enabledV810 = _sentinelEms.readBooleanValue(SEMS_MACHINE_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        throw new LicenseGeneralBusinessException(badFormat);
      }
      catch(DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledV810;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isTdw() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledTdw == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledTdw = _sentinelEms.readBooleanValue(SEMS_TDW_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        throw new LicenseGeneralBusinessException(badFormat);
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledTdw;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean areDiagnosticsEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }

    if(_enabledDiagnostics == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledDiagnostics = _sentinelEms.readBooleanValue(SEMS_DIAGNOSTICS_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        throw new LicenseGeneralBusinessException(badFormat);
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledDiagnostics;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isEarlyPrototypeHardwareEnabled() throws BusinessException
  {
    return false;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isVirtualLiveEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledVirtualLive == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledVirtualLive = _sentinelEms.readBooleanValue(SEMS_VIRTUAL_LIVE_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        throw new LicenseGeneralBusinessException(badFormat);
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledVirtualLive;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isVariableMagnificationEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledVM == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledVM = _sentinelEms.readBooleanValue(SEMS_VM_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        throw new LicenseGeneralBusinessException(badFormat);
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledVM;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isPSPEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledPSP == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledPSP = _sentinelEms.readBooleanValue(SEMS_PSP_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        throw new LicenseGeneralBusinessException(badFormat);
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledPSP;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isXXLEnabled()
  {
    if (_bypassLicense == true)
    {
      return false;
    }
    if(_enabledXXL == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledXXL = _sentinelEms.readBooleanValue(SEMS_XXL_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        _enabledXXL = false;
      }
      catch (DongleReadWriteException drwe)
      {
        _enabledXXL = false;
      }
    }
    return _enabledXXL;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public boolean isS2EXEnabled()
  {
    if (_bypassLicense == true)
    {
      return false;
    }
    if(_enabledS2EX == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledS2EX = _sentinelEms.readBooleanValue(SEMS_S2EX_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        _enabledS2EX = false;
      }
      catch (DongleReadWriteException drwe)
      {
        _enabledS2EX = false;
      }
    }
    return _enabledS2EX;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isThroughputEnabled()
  {
    if (_bypassLicense == true)
    {
      return false;
    }
    if(_enabledThroughput == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledThroughput = _sentinelEms.readBooleanValue(SEMS_THROUGHPUT_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        _enabledThroughput = false;
      }
      catch (DongleReadWriteException drwe)
      {
        _enabledThroughput = false;
      }
    }
    return _enabledThroughput;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isDeveloperSystemEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    // Do a lazy initialization of the Boolean value for this feature
    if(_enabledDeveloperSystem == null)
    {
      try
      {
        byte [] data = new byte[1];

        _enabledDeveloperSystem = _sentinelEms.readBooleanValue(SEMS_DEVELOPER_BOOLEAN, data);
      }
      catch(BadFormatException badFormat)
      {
        throw new LicenseGeneralBusinessException(badFormat);
      }
      catch (DongleReadWriteException drwe)
      {
       throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledDeveloperSystem.booleanValue();
  }

  /**
   * This method validates the license with MAC address of the system which the license is located.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean isMacAddressCorrect() throws DongleReadWriteException
  {
    boolean key = false;

    byte[] data = new byte[SEMS_MAC_STRING_LENGTH];

    String macStringValue = _sentinelEms.readValue(SEMS_MAC_STRING, data);
    java.util.List <String> macAddresses = NetworkUtil.getInstance().getMACAddresses();

    for(String macAddress : macAddresses)
    {
      if(macStringValue.toString().equals(macAddress))
      {
        key = true;
        break;
      }
    }
    return key;
  }

  /**
   * This method validates the DEMO MAC address of the system.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean isDemoAddress() throws DongleReadWriteException
  {
    if(_bypassLicense == true)
    {
        return false;
    }
    boolean key = false;
    byte[] data = new byte[SEMS_MAC_STRING_LENGTH];

    String macStringValue = _sentinelEms.readValue(SEMS_MAC_STRING, data);
    if(macStringValue.toString().equalsIgnoreCase(DEMO))
      return true;
    return key;
  }

  /**
   * This method validates the license with version of the software.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean isValidVersion(String version) throws DongleReadWriteException
  {
    boolean key = false;

    byte[] data = new byte[SEMS_VERSION_STRING_LENGTH];

    String versionStringValue = _sentinelEms.readValue(SEMS_VERSION_STRING, data);
    if(versionStringValue.equals(version))
      return true;
    return key;
  }

  /**
   * This method validates the license with Expired Date.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean isLicenseNotExpired() throws DongleReadWriteException
  {    
    boolean LicenseNotExpired = true;
    
    String dateString = _sentinelEms.getExpirationDate();
    Date expirateDate = new Date(Long.parseLong(dateString) * 1000);
    Date currentDate = new Date();
    if(expirateDate.compareTo(currentDate) > 0)
    { 
      LicenseNotExpired = true;
    }
    else
    {
      LicenseNotExpired = false;
    }
    return LicenseNotExpired;
  }

  /**
   * This method validates the license with Expired Date.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean hasLicenseExpiration() throws DongleReadWriteException
  {
    boolean hasLicenseExpiration = false;
    hasLicenseExpiration = _sentinelEms.hasExpirationDate();    
    return hasLicenseExpiration;
  }

  /**
   * @return Expired Date
   *
   * @author Wei Chin, Chong
   */
  static private synchronized String getExpiredDate() throws DongleReadWriteException
  {
    String dateString = "";
    dateString = _sentinelEms.getExpirationDate();
    Date expirationDate = new Date(Long.parseLong(dateString));
    dateString = expirationDate.getDate() + "/" +  expirationDate.getMonth() + "/" + expirationDate.getYear();
    return dateString;
  }

  /**
   * @return Execution Count(s)
   *
   * @author Wei Chin, Chong
   */
  static private synchronized int getExecutionCounts() throws DongleReadWriteException
  {
    // not supported
    return 0;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void releaseLicense()
  {
    _sentinelEms.logout();
  }
}
