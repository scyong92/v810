package com.axi.v810.business.license;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * This exception is thrown if the pay per use license is not found
 *
 * @author Wei Chin, Chong
 */
public class LicenseGeneralBusinessException extends BusinessException
{
  /**
   * @author Wei Chin
   */
  public LicenseGeneralBusinessException(LocalizedException die)
  {
    super(die.getLocalizedString());
    Assert.expect(die != null);
  }
//  
//  /**
//   * @author Erica Wheatcroft
//   */
//  public LicenseGeneralBusinessException(DongleInitialiseException die)
//  {
//    super(die.getLocalizedString());
//    Assert.expect(die != null);
//  }
//
//  /**
//   * @author Wei Chin, Chong
//   */
//  public LicenseGeneralBusinessException(DongleReadWriteException drwe)
//  {
//    super(drwe.getLocalizedString());
//    Assert.expect(drwe != null);
//  }
//
//  /**
//   * @author Wei Chin, Chong
//   */
//  public LicenseGeneralBusinessException(VitroxLicenseException vle)
//  {
//    super(vle.getLocalizedString());
//    Assert.expect(vle != null);
//  }
}