package com.axi.v810.business.license;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.util.*;
/**
 *
 * @author wei-chin.chong
 */
public abstract class AbstractVitroxLicenseModule
{  
  static
  {
    double versionNumber = VersionUtil.getMajorVersion(Version.getVersionNumber());       

    // When we verify the license against the version number, we only care for the major release version number.
    // For example, if the software's version is 8.1, LicenseManager will treat this software to be 8.0
    // Therefore we need to strip off the minor release version number from the version number that we get from
    // Version.getVersionNumber().
    //double minorVersionNumber = versionNumber - (int)versionNumber;

    //versionNumber = versionNumber - minorVersionNumber;
    _SOFTWARE_VERSION_NUMBER = Double.toString(versionNumber);
  }
  protected static String _SOFTWARE_VERSION_NUMBER;
  
   // The set of licensed features
  protected Boolean _enabledV810 = null;
  protected Boolean _enabledTdw = null;
  protected Boolean _enabledDiagnostics = null;;
  protected boolean _enabledEarlyPrototypeHardware = false;
  protected Boolean _enabledDeveloperSystem = null;
  protected Boolean _enabledVirtualLive = null;
  protected Boolean _enabledPSP = null;
  protected Boolean _enabledVM = null;
  protected Boolean _enabledLowCost; //Chnee Khang Wah, 2011-11-30, Low Cost AXI
  protected Boolean _enabledXXL = null; 
  protected Boolean _enabledS2EX = null; // Chnee Khang Wah, 2014-07-07, Series 3
  protected Boolean _enabledThroughput = null;
  
  static protected boolean _bypassLicense = true;
  
//  public abstract AbstractVitroxLicenseModule getInstance() throws BusinessException ;
  
  public abstract void checkThatValidLicenseExists() throws BusinessException;  
  public abstract boolean isV810() throws BusinessException;
  public abstract boolean isTdw() throws BusinessException;
  public abstract boolean areDiagnosticsEnabled() throws BusinessException;
  public abstract boolean isEarlyPrototypeHardwareEnabled() throws BusinessException;
  public abstract boolean isVirtualLiveEnabled() throws BusinessException;
  public abstract boolean isVariableMagnificationEnabled() throws BusinessException;
  public abstract boolean isPSPEnabled() throws BusinessException;
  public abstract boolean isXXLEnabled();
  public abstract boolean isS2EXEnabled();
  public abstract boolean isThroughputEnabled();
  public abstract boolean isDeveloperSystemEnabled() throws BusinessException;
  
  public abstract void releaseLicense();
}
