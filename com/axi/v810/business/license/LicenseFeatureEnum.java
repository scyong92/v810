package com.axi.v810.business.license;

import com.axi.util.*;

/**
 * This class defines all the feature names that are used in the license file.
 *
 * @author Eugene Kim-Leighton
 */
public class LicenseFeatureEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  public final static LicenseFeatureEnum V810 = new LicenseFeatureEnum (++_index, "V810_MACHINE_VITROX");
  public final static LicenseFeatureEnum TDW = new LicenseFeatureEnum(++_index, "V810_TDW_VITROX");
  public final static LicenseFeatureEnum DIAGNOSTICS = new LicenseFeatureEnum(++_index, "V810_DIAGNOSTICS");
  public final static LicenseFeatureEnum EARLY_PROTOTYPE_HARDWARE = new LicenseFeatureEnum(++_index, "EARLY_PROTOTYPE");
  public final static LicenseFeatureEnum DEVELOPER_SYSTEM = new LicenseFeatureEnum(++_index, "DEVELOPER_SYSTEM_V810");

  private String _featureName;

  /**
   * @author Eugene Kim-Leighton
   */
  private LicenseFeatureEnum(int index, String featureName)
  {
    super(index);
    Assert.expect(featureName != null && featureName.equals("") == false);
    _featureName = featureName.intern();
  }

  /**
   * @author Eugene Kim-Leighton
   */
  public String getName()
  {
    return _featureName;
  }
}
