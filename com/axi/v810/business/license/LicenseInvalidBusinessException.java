package com.axi.v810.business.license;

import com.axi.v810.business.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This exception will be thrown if the software license is invalid.
 *
 * @author Wei Chin, Chong
 */
public class LicenseInvalidBusinessException extends BusinessException
{
  /**
   * @param dateString
   * @param countString
   *
   * @author Wei Chin, Chong
   */
  public LicenseInvalidBusinessException(String dateString, String countString)
  {
    super(new LocalizedString("BUS_SOFTWARE_LICENSE_EXPIRED_KEY", new Object[]{dateString,countString}));
    Assert.expect(dateString != null);
    Assert.expect(countString != null);
  }

  /**
   * @param HostId
   *
   * @author Wei Chin, Chong
   */
  public LicenseInvalidBusinessException(String HostId)
  {
    super(new LocalizedString("BUS_SOFTWARE_LICENSE_INVALID_KEY", new Object[]{HostId, Version.getVersion()}));
    Assert.expect(HostId != null);
  }

  /**
   * @param localizedString
   *
   * @author Wei Chin, Chong
   */
  public LicenseInvalidBusinessException(LocalizedString localizedString)
  {
    super(localizedString);
    Assert.expect(localizedString != null);
  }
}
