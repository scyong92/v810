package com.axi.v810.business.license;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * This class uses the WorkerThread class and wraps its invokeAndWait(RunnableWithExceptions)
 * call with one that only throws LicenseException instead of throwing Exception.  It
 * also uses getInstance() to make sure that all the code is using the same
 * thread.
 *
 *  @author wei-chin.chong
 */
public class LicenseWorkerThread
{
  private static LicenseWorkerThread _instance;
  private static WorkerThread _workerThread = new WorkerThread("LicenseWorkerThread");
  //XCR3648: Software crash when eject XXL dongle license while loading V810
  private XrayTesterException _licenseException = null;
  
  /**
   * @author Wei Chin
   */
  public static synchronized LicenseWorkerThread getInstance()
  {
    if (_instance == null)
      _instance = new LicenseWorkerThread();

    return _instance;
  }
  

  /**
   * @return true if the thread calling this method is this classes worker thread
   * @author Wei Chin
   */
  public static synchronized boolean isLicenseWorkerThread()
  {
    if (Thread.currentThread() == _workerThread.getThread())
    {
      return true;
    }

    return false;
  }

  /**
   * @author Bill Darbie
   */
  private LicenseWorkerThread()
  {
    // do not allow the constructor to be used
  }

  /**
   * @author Wei Chin
   */
  public void invokeAndWait(RunnableWithExceptions runnable) throws XrayTesterException
  {
    try
    {
      if(this == _instance)
      {
        _workerThread.invokeAndWait(runnable);
      }
    }
    catch(XrayTesterException ex)
    {
      throw ex;
    }
    catch (RuntimeException ex)
    {
      throw ex;
    }
    catch(Exception ex)
    {
      Assert.logException(ex);
    }
  }

  /**
   * @author Wei Chin
   */
  public void invokeLater(Runnable runnable)
  {
    try
    {
      if (this == _instance)
      {
        _workerThread.invokeLater(runnable);
      }
    }
    catch(Throwable ex)
    {
      Assert.logException(ex);
    }
  } 
  
  /**
   * @author Wei Chin
   */
  public void stopLicenseWorkerThread()
  {
    try
    {
      _workerThread.clearPendingTasks();
      _workerThread.getThread().interrupt();
    }
    catch(Throwable ex)
    {
      Assert.logException(ex);
    }
  }
  
  /**
   * XCR3648: Software crash when eject XXL dongle license while loading V810
   * @author Hee-Jihn.Chuah
   */
  public void setLicenseException(XrayTesterException xte) 
  {
    Assert.expect(xte != null);
    _licenseException = xte;
  }
  
  public XrayTesterException getLicenseException()
  {
    Assert.expect(_licenseException != null);
    return _licenseException;
  }
  
  public boolean hasLicenseException()
  {
    if (_licenseException == null)
      return false;
    else
      return true;
  }
}