/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business.license;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 *
 * @author wei-chin.chong
 */
public class SentinelUltraProLicenseModule extends AbstractVitroxLicenseModule
{
  // sentinel License
  static private final long SP_V810_FULLLICENSE = 0x6000C;
  static private final long SP_V810_FULLLICENSE_EXECUTION_CONTROL = 0x8000B;
//  private final long SP_V810_FULLLICENSE_EXPIRATION_DATE = 0x90009;
//  private final long SP_V810_FULLLICENSE_TRIAL_PERIOD = 0x90008;
  static private final long SP_VERSION_STRING = 0x3030010;
  static private final long SP_MAC_STRING = 0x11030012;
  static private final long SP_MACHINE_BOOLEAN = 0x4001B;
  static private final long SP_TDW_BOOLEAN = 0x14001B;
  static private final long SP_DIAGNOSTICS_BOOLEAN = 0x24001B;
  static private final long SP_DEVELOPER_BOOLEAN = 0x34001B;
  static private final long SP_EARLYPROTO_BOOLEAN = 0x44001B;
  static private final long SP_VIRTUAL_LIVE_BOOLEAN = 0x54001B;

  static private final long SP_VM_BOOLEAN  =  0x64001B;
  static private final long SP_PSP_BOOLEAN =  0x74001B;
  static private final long SP_LC_BOOLEAN  =  0x84001B; // Chnee Khang Wah, 2011-11-30
  static private final long SP_XXL_BOOLEAN =  0x94001B; 
  static private final long SP_S2EX_BOOLEAN  = 0xA4001B; // Chnee Khang Wah, 2014-07-07
  
  static private final int MAX_STRING_LENGTH = 50;
  static private final int WRITE_PWD = 0x7110;
  
  static private final String DEMO = "DEMO";
  
  static SentinelUltraProLicenseModule _instance = null;
  static private SentinelUltraPro _sentinelUltraPro;
  
  static boolean _haventDecrement = true;
  
  /**
   * @throws DongleInitialiseException 
   * @author Wei Chin
   */
  SentinelUltraProLicenseModule() throws DongleInitialiseException
  {
    _sentinelUltraPro = SentinelUltraPro.getInstance();
    init();
  }
  
  
  /**
   * @throws DongleInitialiseException 
   * @author Wei Chin
   */
  private void init()
  {
    //  nothing
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static AbstractVitroxLicenseModule getInstance() throws BusinessException
  {
    try
    {
      if(_instance == null)
        _instance = new SentinelUltraProLicenseModule();
    }
    catch(DongleInitialiseException exception)
    {
      throw new LicenseGeneralBusinessException(exception);
    }    
    return _instance;
  }
  
  /**
   * @throws BusinessException 
   * 
   * @author Wei Chin
   */
  @Override
  public void checkThatValidLicenseExists() throws BusinessException
  {
    try
    {
      boolean hasLicenseExpiration = hasLicenseExpiration();
      boolean licenseNotExpired = true;
      if (hasLicenseExpiration)
      {
        licenseNotExpired = isLicenseNotExpired();
      }
      boolean validHostId = isMacAddressCorrect();
      boolean demoHostId = isDemoAddress();
      boolean validVersion = isValidVersion(_SOFTWARE_VERSION_NUMBER);

      if (demoHostId && licenseNotExpired)
      {
        return;
      }

      if (licenseNotExpired == false)
      {
        BusinessException be = new LicenseInvalidBusinessException(getExpiredDate(), Integer.toString(getExecutionCounts()));
        throw be;
      }

      if (validHostId == false || validVersion == false)
      {
        BusinessException be = new LicenseInvalidBusinessException(NetworkUtil.getInstance().getMACAddresses().toString());
        throw be;
      }
    }
    catch (DongleReadWriteException die)
    {
      throw new LicenseGeneralBusinessException(die);
    }
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public boolean isV810() throws BusinessException
  {
//    checkThatValidLicenseExists();

    if (_bypassLicense == true)
    {
      return true;
    }

    if(_enabledV810 == null)
    {
      try
      {
       long [] readValue_boolean = {0,0,0,0,0,0,0,0};

      _sentinelUltraPro.readValue(SP_MACHINE_BOOLEAN, readValue_boolean);

      if(readValue_boolean[0] == 1)
        _enabledV810 = true;
      else 
        _enabledV810 = false;

      }
      catch(DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledV810;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isTdw() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledTdw == null)
    {
      try
      {
       long [] readValue_boolean = {0,0,0,0,0,0,0,0};

       _sentinelUltraPro.readValue(SP_TDW_BOOLEAN,readValue_boolean);

       if(readValue_boolean[0] == 1)
        _enabledTdw = true;
       else
        _enabledTdw = false;     
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    
      
    return _enabledTdw;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean areDiagnosticsEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }

    if(_enabledDiagnostics == null)
    {
      try
      {

        long [] readValue_boolean = {0,0,0,0,0,0,0,0};

        _sentinelUltraPro.readValue(SP_DIAGNOSTICS_BOOLEAN, readValue_boolean);

        if(readValue_boolean[0] == 1)
          _enabledDiagnostics = true;
        else
          _enabledDiagnostics = false;
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledDiagnostics;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isEarlyPrototypeHardwareEnabled() throws BusinessException
  {
    return false;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isVirtualLiveEnabled() throws BusinessException
  {
//    checkThatValidLicenseExists();
    
    if (_bypassLicense == true)
    {
      return true;
    }
    
    if(_enabledVirtualLive == null)
    {
      try
      {
        long [] readValue_boolean = {0,0,0,0,0,0,0,0};

        _sentinelUltraPro.readValue( SP_VIRTUAL_LIVE_BOOLEAN, readValue_boolean);

        if(readValue_boolean[0] == 1)
          _enabledVirtualLive = true;
        else
          _enabledVirtualLive = false;
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledVirtualLive;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isVariableMagnificationEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledVM == null)
    {
      try
      {
        long [] readValue_boolean = {0,0,0,0,0,0,0,0};

        _sentinelUltraPro.readValue( SP_VM_BOOLEAN, readValue_boolean);

        if(readValue_boolean[0] == 1)
          _enabledVM = true;
        else
          _enabledVM = false;
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledVM;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isPSPEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    if(_enabledPSP == null)
    {
      try
      {
        long [] readValue_boolean =  {0,0,0,0,0,0,0,0};

        _sentinelUltraPro.readValue( SP_PSP_BOOLEAN, readValue_boolean);

        if(readValue_boolean[0] == 1)
          _enabledPSP = true;
        else
          _enabledPSP = false;
      }
      catch (DongleReadWriteException drwe)
      {
        throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledPSP;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isXXLEnabled()
  {
    if (_bypassLicense == true)
    {
      return false;
    }
    if(_enabledXXL == null)
    {
      try
      {
        long [] readValue_boolean = {0,0,0,0,0,0,0,0};

        _sentinelUltraPro.readValue( SP_XXL_BOOLEAN, readValue_boolean);

        if(readValue_boolean[0] == 1)
          _enabledXXL = true;
        else
          _enabledXXL = false;
      }
      catch (DongleReadWriteException drwe)
      {
        _enabledXXL = false;
      }
    }
    return _enabledXXL;
  }
  
  /**
   * @author Chnee Khang Wah
   */
  public boolean isS2EXEnabled()
  {
    if (_bypassLicense == true)
    {
      return false;
    }
    if(_enabledS2EX == null)
    {
      try
      {
        long [] readValue_boolean = {0,0,0,0,0,0,0,0};

        _sentinelUltraPro.readValue( SP_S2EX_BOOLEAN, readValue_boolean);

        if(readValue_boolean[0] == 1)
          _enabledS2EX = true;
        else
          _enabledS2EX = false;
      }
      catch (DongleReadWriteException drwe)
      {
        _enabledS2EX = false;
      }
    }
    return _enabledS2EX;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @return 
   */
  public boolean isThroughputEnabled()
  {
    if (isS2EXEnabled() || isXXLEnabled())
    {
      return false;
    }
    
    return true;
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public boolean isDeveloperSystemEnabled() throws BusinessException
  {
    if (_bypassLicense == true)
    {
      return true;
    }
    
    // Do a lazy initialization of the Boolean value for this feature
    if(_enabledDeveloperSystem == null)
    {
      try
      {
        // Check to see if the feature is enabled and convert any exceptions to the correct Business exception
        long [] readValue_boolean =  {0,0,0,0,0,0,0,0};

        _sentinelUltraPro.readValue( SP_DEVELOPER_BOOLEAN, readValue_boolean);

        if(readValue_boolean[0] == 1)
          _enabledDeveloperSystem = new Boolean(true);
        else
          _enabledDeveloperSystem = new Boolean(false);
      }
      catch (DongleReadWriteException drwe)
      {
       throw new LicenseGeneralBusinessException(drwe);
      }
    }
    return _enabledDeveloperSystem.booleanValue();
  }

  /**
   * This method validates the license with MAC address of the system which the license is located.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  private synchronized boolean isMacAddressCorrect() throws DongleReadWriteException
  {
    boolean key = false;
    StringBuffer macStringValue = new StringBuffer(MAX_STRING_LENGTH);
    java.util.List <String> macAddresses = NetworkUtil.getInstance().getMACAddresses();

    _sentinelUltraPro.readString(SP_MAC_STRING, macStringValue, MAX_STRING_LENGTH);

    for(String macAddress : macAddresses)
    {
      if(macStringValue.toString().equals(macAddress))
      {
        key = true;
        break;
      }
    }
    return key;
  }

  /**
   * This method validates the DEMO MAC address of the system.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean isDemoAddress() throws DongleReadWriteException
  {
    if(_bypassLicense == true)
    {
        return false;
    }
    boolean key = false;
    StringBuffer macStringValue = new StringBuffer(MAX_STRING_LENGTH);

    _sentinelUltraPro.readString(SP_MAC_STRING, macStringValue, MAX_STRING_LENGTH);

    if(macStringValue.toString().equalsIgnoreCase(DEMO))
      return true;
    return key;
  }

  /**
   * This method validates the license with version of the software.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean isValidVersion(String version) throws DongleReadWriteException
  {
    boolean key = false;

    StringBuffer versionStringValue = new StringBuffer(MAX_STRING_LENGTH);

    _sentinelUltraPro.readString( SP_VERSION_STRING, versionStringValue, MAX_STRING_LENGTH);

    if ( versionStringValue.toString().equals(version) )
      key = true;
    return key;
  }
  
/**
   * This method validates the license with Expired Date.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean isLicenseNotExpired() throws DongleReadWriteException
  {    
    boolean LicenseNotExpired = true;
    
    JDateInfo dateInfo = new JDateInfo();
    long [] readCounter_lease = {0};
    int[] daysLeft_lease = {0};

    /* You can call the SFNTsntlReadValue API function to read the value of the number of executions included within
    a Full License element.It requires the Toolkit Cell Address corresponding to that control. Refer to the design header file
    generated for this design. */
    _sentinelUltraPro.readValue(SP_V810_FULLLICENSE_EXECUTION_CONTROL, readCounter_lease);

    if(_haventDecrement && readCounter_lease[0] > 1)
    {
      _sentinelUltraPro.decrementCounter(SP_V810_FULLLICENSE_EXECUTION_CONTROL, WRITE_PWD, 1);
      _haventDecrement = false;
    }

    /* Call the SFNTsntlReadLease API function to read the date up to which the lease is valid.
    This API will also calculate the number of days remaining, from the current date, for the lease to expire. */
    _sentinelUltraPro.readLease(SP_V810_FULLLICENSE, dateInfo, daysLeft_lease);

    if ( daysLeft_lease[0] == 0 &&
        ( dateInfo.days == 0 && dateInfo.month == 0 && dateInfo.year == 0) == false)
    {
      LicenseNotExpired = false;
    }

    if(readCounter_lease[0] == 1)
    {
      LicenseNotExpired = false;
    }

    return LicenseNotExpired;
  }
  
    /**
   * This method validates the license with Expired Date.
   *
   * @return
   *
   * @author Wei Chin, Chong
   */
  static private synchronized boolean hasLicenseExpiration() throws DongleReadWriteException
  {
    boolean hasLicenseExpiration = false;
    
    JDateInfo dateInfo = new JDateInfo();
    long [] readCounter_lease = {0};
    int[] daysLeft_lease = {0};

    /* You can call the SFNTsntlReadValue API function to read the value of the number of executions included within
    a Full License element.It requires the Toolkit Cell Address corresponding to that control. Refer to the design header file
    generated for this design. */
    _sentinelUltraPro.readValue(SP_V810_FULLLICENSE_EXECUTION_CONTROL, readCounter_lease);

    /* Call the SFNTsntlReadLease API function to read the date up to which the lease is valid.
    This API will also calculate the number of days remaining, from the current date, for the lease to expire. */
    _sentinelUltraPro.readLease(SP_V810_FULLLICENSE, dateInfo, daysLeft_lease);

    if (daysLeft_lease[0] >= 1)
    {
      return true;
    }

    if(readCounter_lease[0] >= 1)
    {
      return true;
    }
    
    return hasLicenseExpiration;
  }

  /**
   * @return Expired Date
   *
   * @author Wei Chin, Chong
   */
  static private synchronized String getExpiredDate() throws DongleReadWriteException
  {
    String dateString = "";
    int [] daysLeft_lease = {0};
    JDateInfo dateInfo = new JDateInfo();

     /* Call the SFNTsntlReadLease API function to read the date up to which the lease is valid.
    This API will also calculate the number of days remaining, from the current date, for the lease to expire. */
    _sentinelUltraPro.readLease(SP_V810_FULLLICENSE, dateInfo, daysLeft_lease);

    if(dateInfo.days == 0 && dateInfo.month == 0 && dateInfo.year == 0 )
      return "Unlimited";

    if(dateInfo.days < 10)
      dateString = "0" + dateInfo.days + "/";
    else
      dateString = dateInfo.days + "/";

    if(dateInfo.month < 10)
      dateString = dateString + "0" + dateInfo.month + "/";
    else
      dateString = dateString + dateInfo.month + "/";

    dateString = dateString + dateInfo.year;
    return dateString;
  }

  /**
   * @return Execution Count(s)
   *
   * @author Wei Chin, Chong
   */
  static private synchronized int getExecutionCounts() throws DongleReadWriteException
  {
    long [] readCounter_lease = {0};

    _sentinelUltraPro.readValue(SP_V810_FULLLICENSE_EXECUTION_CONTROL, readCounter_lease);

    return (int)readCounter_lease[0] - 1;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void releaseLicense()
  {
    _sentinelUltraPro.SFNTsntlCleanup();
  }
}
