package com.axi.v810.business.license;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import java.util.*;

/**
 * @author Eugene Kim-Leighton
 * @author Wei Chin, Chong
 */
public class LicenseManager
{
  private static LicenseManager _instance = null;
  private static AbstractVitroxLicenseModule _vitroxLicenseInstance = null;  
  
  /**
   * @author Bill Darbie
   */
  static
  {
    try
    {
      initialize();
      checkThatValidLicenseExists();
    }
    catch (BusinessException ex)
    {
      //Ngie Xing
      javax.swing.JOptionPane.showMessageDialog(null,
        ex.getMessage(),
        StringLocalizer.keyToString("MMGUI_GUI_NAME_KEY"),
        javax.swing.JOptionPane.ERROR_MESSAGE);
      com.axi.v810.gui.mainMenu.MainMenuGui.getInstance().exitV810(false);
    }
  }

  /**
   * This method gets the one and only instance of LicenseManager object
   *
   * @author Eugene Kim-Leighton
   */
  public synchronized static LicenseManager getInstance() throws BusinessException
  {
    if (_instance == null)
      _instance = new LicenseManager();

    return _instance;
  }

  /**
   * @author Wei Chin, Chong
   */
  private LicenseManager() throws BusinessException
  {
    initialize();
  }
  
  /**
   * @author Wei Chin
   */
  static synchronized public void initialize() throws BusinessException
  {
    XrayTesterException.disableExceptionLogging();
    // old dongle
    try
    {
      _vitroxLicenseInstance = SentinelUltraProLicenseModule.getInstance();
    }
    catch(BusinessException be1)
    {
      // new dongle
      try
      {
        _vitroxLicenseInstance = SentinelEMSLicenseModule.getInstance();
      }
      catch(BusinessException be2)
      {
        XrayTesterException.enableExceptionLogging();
        // soft license
        _vitroxLicenseInstance = VitroxSoftLicenseModule.getInstance();
      }
    }
    finally
    {
      XrayTesterException.enableExceptionLogging();
    }
  }

  /**
   * Check for the License Expired, Host, and Version
   *
   * @author Wei Chin, Chong
   */
  static synchronized public void checkThatValidLicenseExists() throws BusinessException
  {
    Assert.expect(_vitroxLicenseInstance != null);
    
    _vitroxLicenseInstance.checkThatValidLicenseExists();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isV810() throws BusinessException
  {
    Assert.expect(_vitroxLicenseInstance != null);
    
    return _vitroxLicenseInstance.isV810();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isTdw() throws BusinessException
  {
    Assert.expect(_vitroxLicenseInstance != null);
    
    return _vitroxLicenseInstance.isTdw();
  }
  
  /**
   * @author Wei Chin, Chong
   */
  static public boolean areDiagnosticsEnabled() throws BusinessException
  {
    Assert.expect(_vitroxLicenseInstance != null);
    
    return _vitroxLicenseInstance.areDiagnosticsEnabled();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isEarlyPrototypeHardwareEnabled() throws BusinessException
  { 
    Assert.expect(_vitroxLicenseInstance != null);
    return _vitroxLicenseInstance.isEarlyPrototypeHardwareEnabled();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isVirtualLiveEnabled() throws BusinessException
  {
    Assert.expect(_vitroxLicenseInstance != null);
    
    return _vitroxLicenseInstance.isVirtualLiveEnabled();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isVariableMagnificationEnabled() throws BusinessException
  {
    if (XrayActuator.isInstalled()==false)
    {
      return false;
    }
    Assert.expect(_vitroxLicenseInstance != null);
    
    return _vitroxLicenseInstance.isVariableMagnificationEnabled();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isPSPEnabled() throws BusinessException
  {
    Assert.expect(_vitroxLicenseInstance != null);
    
    return _vitroxLicenseInstance.isPSPEnabled();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isXXLEnabled()
  {
    Assert.expect(_vitroxLicenseInstance != null);
    
    return _vitroxLicenseInstance.isXXLEnabled();
  }
  
  /**
   * @author Chnee Khang Wah
   */
  static public boolean isS2EXEnabled()
  { 
    Assert.expect(_vitroxLicenseInstance != null);
    return _vitroxLicenseInstance.isS2EXEnabled();
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @return 
   */
  static public boolean isThroughputEnabled()
  {
    Assert.expect(_vitroxLicenseInstance != null);
    return _vitroxLicenseInstance.isThroughputEnabled();
  }

  /**
   * @author Wei Chin, Chong
   */
  static public boolean isDeveloperSystemEnabled() throws BusinessException
  {
    Assert.expect(_vitroxLicenseInstance != null);
    return _vitroxLicenseInstance.isDeveloperSystemEnabled();
  }
  
  /**
   * @author Wei Chin
   */
  static public void releaseLicense()
  {
    Assert.expect(_vitroxLicenseInstance != null);
    _vitroxLicenseInstance.releaseLicense();
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @return
   * @throws BusinessException 
   */
  static public boolean isOfflineProgramming() throws BusinessException
  {
    return isV810() == false && isTdw();
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   */
  public List<String> getSupportedSystemType()
  {
    List<String> supportedSystemType = new ArrayList<String>();

    if(isXXLEnabled())
    {
      supportedSystemType.add(SystemTypeEnum.XXL.getName());
    }
    if (isS2EXEnabled())
    {
      supportedSystemType.add(SystemTypeEnum.S2EX.getName());
    }
    if (isThroughputEnabled())
    {
      supportedSystemType.add(SystemTypeEnum.STANDARD.getName().toUpperCase());
    }
    
    return supportedSystemType;
  }
  
  /**
   * XCR-3589 Combo STD and XXL software GUI
   * @author weng-jian.eoh
   * @param systemType
   * @return 
   */
  static public boolean checkSupportedSystemTypeForOfflineProgramming(SystemTypeEnum systemType)
  {
    boolean systemTypeIsValid = false;
    
    if (systemType.equals(SystemTypeEnum.THROUGHPUT) && isThroughputEnabled())
    {
      systemTypeIsValid = true;
    }
    else if (systemType.equals(SystemTypeEnum.XXL) && isXXLEnabled())
    {
      systemTypeIsValid = true;
    }
    else if (systemType.equals(SystemTypeEnum.S2EX) && isS2EXEnabled())
    {
      systemTypeIsValid = true;
    }
        
    return systemTypeIsValid;
  }
}
