package com.axi.v810.business;

import java.io.*;
import com.axi.util.*;
import com.axi.v810.util.*;

/**
* This is a unit test for SerialCommunicationsProtocolHardwareException.
*
* @author Jeff Ryer
*/
public class Test_MultiplePanelMatchesBusinessException extends UnitTest
{

  /**
  * @author Jeff Ryer
  */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_MultiplePanelMatchesBusinessException());
  }

  /**
  * @author Jeff Ryer
  */
  public void test(BufferedReader is, PrintWriter os)
  {
    MultiplePanelMatchesBusinessException ex = new MultiplePanelMatchesBusinessException("firstPanelName","secondPanelName");
    String message = ex.getLocalizedMessage();
    os.println(message);
  }
}



