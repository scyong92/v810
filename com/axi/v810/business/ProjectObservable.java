package com.axi.v810.business;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;

/**
 * This class allows Observers to be updated each time something in the datastore layer changes state.
 * Every set method in the datastore layer should call DatastoreObservable.stateChanged(this).
 * If the set method calls other datastore set methods then you should do the following:
 * <pre>
 *
 *  void function()
 *  {
 *     // put any asserts for parameters here
 *
 *     // turn off all update calls from DatastoreObservable so other methods that this method
 *     // calls do not cause update() calls
 *     _datastoreObservable.setEnabled(false);
 *     try
 *     {
 *       // call other set methods here
 *     }
 *     finally
 *     {
 *       // re-enable DatastoreObservable updates - must be in the finally so it is
 *       // guaranteed to be called
 *       _datastoreObservable.setEnabled(true);
 *     }
 *     // now send the update() about this method being called
 *     _datastoreObservable.stateChanged(this, SomeEventEnum.SOME_EVENT);
 *   }
 *
 *
 * </pre>
 *
 * @author Bill Darbie
 */
public class ProjectObservable extends Observable
{
  private static ProjectObservable _instance = null;

  private ProjectState _projectState;
  private int _numDisables = 0;

  /**
   * Do not allow the constructor to be called.
   * @author Bill Darbie
   */
  private ProjectObservable()
  {
    // do nothing
  }

  /**
  * @return an instance of this object.
  * @author Bill Darbie
  */
  public static synchronized ProjectObservable getInstance()
  {
    if (_instance == null)
      _instance = new ProjectObservable();

    return _instance;
  }

  /**
   * @author Bill Darbie
   */
  public void setProjectState(ProjectState projectState)
  {
    // null is allowed for projectState
    _projectState = projectState;
  }

  /**
   * Set enable to true to cause datastore events to trigger update() calls.
   * When enable is set to false no updates will be made.  Note that you must
   * pair each setEnabled(false) with a setEnabled(true) because this method
   * keeps track of nested calls in order to work properly.
   *
   * @author Bill Darbie
   */
  public void setEnabled(boolean enable)
  {
    if (enable)
      --_numDisables;
    else
      ++_numDisables;
  }

  /**
   * Each datastore object should call this method when ever it does something that changes the
   * state of the data.  Any Observers of this class will be notified that the datastore state
   * has changed.
   *
   * @author Bill Darbie
   */
  public void stateChanged(Object source, Object oldValue, Object newValue, ProjectChangeEventEnum projectChangeEventEnum)
  {
    Assert.expect(source != null);
    // oldValue can be null
    // newValue can be null
    Assert.expect(projectChangeEventEnum != null);

    Assert.expect(_numDisables >= 0);

    ProjectChangeEvent projChangeEvent = new ProjectChangeEvent(source, oldValue, newValue, projectChangeEventEnum);
    // give the state machine all state changes
    if (_projectState != null)
      _projectState.stateChange(projChangeEvent);

    if (isEnabled())
    {
      // send up only the initiating state change call to observers
      setChanged();
      notifyObservers(projChangeEvent);
    }
  }

  /**
   * @author Bill Darbie
   */
  public void sendProjectChangeEvent(ProjectChangeEvent projChangeEvent)
  {
    Assert.expect(projChangeEvent != null);

    setChanged();
    notifyObservers(projChangeEvent);
  }

  /**
   * @author George A. David
   */
  public void sendProjectEvent(ProjectEventEnum projectEvent)
  {
    Assert.expect(projectEvent != null);
    setChanged();
    notifyObservers(projectEvent);
  }

  /**
   * @author Bill Darbie
   */
  public boolean isEnabled()
  {
    if (_numDisables == 0)
      return true;

    return false;
  }
}


