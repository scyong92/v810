package com.axi.v810.business;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.Directory;

/**
 * <p>Title: ImageSetData</p>
 *
 * <p>Description: This class will be responsible for all image run data that needs to be sent to the Test Execution
 * Engine for acquiring images.</p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author Erica Wheatcroft
 */
public class ImageSetData implements Comparable
{

  private String _projectName;
  private int _testProgramVersionNumber;
  private String _imageSetName;
  // by default we will assume that the board is populated
  private boolean _populatedBoard = true;
  // by default we will genererate images for no test and no load devices
  private boolean _generateImagesForNoTestAndNoLoadDevices = true;
  // this is user specified
  private String _imageSetUserDescription = null;
  // this is system generated
  private String _imageSetSystemDescription = "";
  // this is the board instance the user has selected.
  private Board _selectedBoardInstance = null;
  // if the user has selected to perform an image collection against a board type (or all selected board instances)
  private BoardType _selectedBoardType = null;
  // this object will either be a board, component, pad, jointype, subtype, componentType, or a padType
  private Object _componentToAcquireImagesFor = null;
  // this will define what type of image run we are doing
  private ImageSetTypeEnum _imageSetTypeEnum = null;
  // this will define what kind of image collection is to be done: precision (using surface model and full production mode) or fine tuning (limited)
  private ImageSetCollectionTypeEnum _imageSetCollectionTypeEnum = ImageSetCollectionTypeEnum.FINE_TUNING;
  // this will indicate if we are using a board type or board instance
  private boolean _collectImagesOnAllBoardInstances = false;
  // this will indicate if we are going to collect images for the entire panel. This option is only available
  // with joint type image set collections or subtype image sets.
  private boolean _collectImagesOnEntirePanel = false;
  // this will indicate if the image run collection for subtype or jointtype should be at a panel level
  private boolean _collectImagesForPanel = false;
  // this will indicate if the user wishes to collect lightest images.
  private boolean _collectLightestImages = false;
  // this will indicate if the user wishes to collect focus confirmation images.
  private boolean _collectFocusConfirmationImages = false;
  // this will indicate how many images were saved with this image set.
  private int _numberOfImagesSaved = 0;
  // the date the image set was saved to disk in mils
  private long _dateInMils = 0;
  // the machine serial number of the machine that was used to create this image
  private String _machineSerialNumber = null;
  private boolean _isOnlineTestDevelopment = false;
  private double _percentOfCompatibleImages = -1;
  private boolean _saveGoodImages = false;
  private String _highMagnification = null;
  private String _lowMagnification = null;
  // indicate if the ImageSet has 2.5D image enabled
  private boolean _isGenerateMultiAngleImagesEnabled = false;

  private boolean _isImageSetFullyLoaded = false;
  
  //variant management
  private String _selectedVariationName;

  /**
   * @author Erica Wheatcroft
   */
  public ImageSetData()
  {
    // do nothing
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setGenerateImagesForNoTestAndNoLoadDevices(boolean generateImagesForNoTestNoLoadDevices)
  {
    _generateImagesForNoTestAndNoLoadDevices = generateImagesForNoTestNoLoadDevices;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean generateImagesForNoTestAndNoLoadDevices()
  {
    return _generateImagesForNoTestAndNoLoadDevices;
  }

  /**
   * @return True if the board is populated with components false otherwise
   * @author Erica Wheatcroft
   */
  public boolean isBoardPopulated()
  {
    return _populatedBoard;
  }

  /**
   * @return boolean True if the user wants to collection image sets for the entire panel.
   * @author Erica Wheatcroft
   */
  public boolean collectImagesOnEntirePanel()
  {
    return _collectImagesForPanel;
  }

  /**
   * @param boardPopulated boolean indicating if a board is populated with components or not.
   * @author Erica Wheatcroft
   */
  public void setBoardPopulated(boolean boardPopulated)
  {
    _populatedBoard = boardPopulated;
  }

  /**
   * @return String user defined description of the image run
   * @author Erica Wheatcroft
   */
  public String getUserDescription()
  {
    Assert.expect(_imageSetUserDescription != null, "Image run description is not set");
    return _imageSetUserDescription;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setUserDescription(String imageSetUserDescription)
  {
    Assert.expect(imageSetUserDescription != null, "Image Run description to set is null");
    _imageSetUserDescription = imageSetUserDescription;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setCollectLightestImages(boolean collectLightestImages)
  {
    _collectLightestImages = collectLightestImages;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean collectLightestImages()
  {
    return _collectLightestImages;
  }

  /**
   * @author Matt Wharton
   */
  public void setCollectFocusConfirmationImages(boolean collectFocusConfirmationImages)
  {
    _collectFocusConfirmationImages = collectFocusConfirmationImages;
  }

  /**
   * @author Matt Wharton
   */
  public boolean collectFocusConfirmationImages()
  {
    return _collectFocusConfirmationImages;
  }

  /**
   * @return String system defined description of the image run
   * @author Erica Wheatcroft
   */
  public String getSystemDescription()
  {
    Assert.expect(_imageSetSystemDescription != null, "Image set description is not set");
    return _imageSetSystemDescription;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setSystemDescription(String imageSetSystemDescription)
  {
    Assert.expect(imageSetSystemDescription != null, "Image Set description to set is null");
    _imageSetSystemDescription = imageSetSystemDescription;
  }

  /**
   * This method will set the board instance that a subtype or joint type image run will need if the user
   * selects to acquire images for a particular board instance.
   * @author Erica Wheatcroft
   */
  public void setBoardInstanceToAcquireImagesAgainst(Board board)
  {
    Assert.expect(board != null, "Board instance is null");
    _selectedBoardInstance = board;
  }

  /**
   * This method will return the board instance that a subtype or joint type image run will need if the user
   * selects to acquire images for a particular board instance.
   * @author Erica Wheatcroft
   */
  public Board getBoardInstanceToAcquireImagesAgainst()
  {
    Assert.expect(_selectedBoardInstance != null, "Board instance is null");
    return _selectedBoardInstance;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean hasBoardInstance()
  {
    if (_selectedBoardInstance == null)
    {
      return false;
    }

    return true;
  }

  /**
   * This method will set the board type that a component, pad, subtype, or joint type image run will need if the user
   * selects to acquire images for a ALL board instances.
   * @author Erica Wheatcroft
   */
  public void setBoardTypeToAcquireImagesAgainst(BoardType boardType)
  {
    Assert.expect(boardType != null, "Board type is null");
    _selectedBoardType = boardType;
  }

  /**
   * This method will return the board type that a component, pad, subtype, or joint type image run will need if the user
   * selects to acquire images for a ALL board instances.
   * @author Erica Wheatcroft
   */
  public BoardType getBoardTypeToAcquireImagesAgainst()
  {
    Assert.expect(_selectedBoardType != null, "Board type is null");
    return _selectedBoardType;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean hasBoardType()
  {
    if (_selectedBoardType == null)
    {
      return false;
    }

    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setBoardToAcquireImagesFor(Board board)
  {
    Assert.expect(board != null, "board is null");
    _componentToAcquireImagesFor = board;
  }

  /**
   * @author Erica Wheatcroft
   */
  public Board getBoardToAcquireImagesFor()
  {
    Assert.expect(_componentToAcquireImagesFor != null, "Component to acquire images for is null");
    Assert.expect(_componentToAcquireImagesFor instanceof Board, "Not an instance of a board");

    return (Board) _componentToAcquireImagesFor;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setComponentToAcquireImagesFor(Component component)
  {
    Assert.expect(component != null, "component is null");
    _componentToAcquireImagesFor = component;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setComponentTypeToAcquireImagesFor(ComponentType componentType)
  {
    Assert.expect(componentType != null, "component type is null");
    _componentToAcquireImagesFor = componentType;
  }

  /**
   * @author Erica Wheatcroft
   */
  public Component getComponentToAcquireImagesFor()
  {
    Assert.expect(_componentToAcquireImagesFor != null, "Component to acquire images for is null");
    Assert.expect(_componentToAcquireImagesFor instanceof Component, "Not an instance of a Component");

    return (Component) _componentToAcquireImagesFor;
  }

  /**
   * @author Erica Wheatcroft
   */
  public ComponentType getComponentTypeToAcquireImagesFor()
  {
    Assert.expect(_selectedBoardType != null, "In order to get a component type board type must be set");
    Assert.expect(_componentToAcquireImagesFor instanceof ComponentType, "Not an instance of a Component Type");

    return (ComponentType) _componentToAcquireImagesFor;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setPadToAcquireImagesFor(Pad pad)
  {
    Assert.expect(pad != null, "pad is null");
    _componentToAcquireImagesFor = pad;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setPadTypeToAcquireImagesFor(PadType padType)
  {
    Assert.expect(padType != null, "padType is null");
    _componentToAcquireImagesFor = padType;
  }

  /**
   * @author Erica Wheatcroft
   */
  public Pad getPadToAcquireImagesFor()
  {
    Assert.expect(_componentToAcquireImagesFor != null, "Pad to acquire images for is null");
    Assert.expect(_componentToAcquireImagesFor instanceof Pad, "Not an instance of a Pad");

    return (Pad) _componentToAcquireImagesFor;
  }

  /**
   * @author Erica Wheatcroft
   */
  public PadType getPadTypeToAcquireImagesFor()
  {
    Assert.expect(_selectedBoardType != null, "In order to get a pad type board type must be set");
    Assert.expect(_componentToAcquireImagesFor instanceof PadType, "Not an instance of a Pad Type");

    return (PadType) _componentToAcquireImagesFor;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setJointTypeEnumToAcquireImagesFor(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null, "joint type enum is null");
    _componentToAcquireImagesFor = jointTypeEnum;
  }

  /**
   * @author Erica Wheatcroft
   */
  public JointTypeEnum getJointTypeEnumToAcquireImagesFor()
  {
    Assert.expect(_componentToAcquireImagesFor != null, "JointTypeEnum to acquire images for is null");
    Assert.expect(_componentToAcquireImagesFor instanceof JointTypeEnum, "Not an instance of a JointTypeEnum");

    return (JointTypeEnum) _componentToAcquireImagesFor;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setSubtypeToAcquireImagesFor(Subtype subtype)
  {
    Assert.expect(subtype != null, "subtype is null");
    _componentToAcquireImagesFor = subtype;
  }

  /**
   * @author Erica Wheatcroft
   */
  public Subtype getSubtypeToAcquireImagesFor()
  {
    Assert.expect(_componentToAcquireImagesFor != null, "v to acquire images for is null");
    Assert.expect(_componentToAcquireImagesFor instanceof Subtype, "Not an instance of a Subtype");

    return (Subtype) _componentToAcquireImagesFor;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setImageSetTypeEnum(ImageSetTypeEnum imageSetTypeEnum)
  {
    Assert.expect(imageSetTypeEnum != null, "ImageSetTypeEnum is null");
    _imageSetTypeEnum = imageSetTypeEnum;
  }

  /**
   * @author Erica Wheatcroft
   */
  public ImageSetTypeEnum getImageSetTypeEnum()
  {
    Assert.expect(_imageSetTypeEnum != null, "ImageSetTypeEnum is null");
    return _imageSetTypeEnum;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean hasImageSetTypeEnum()
  {
    if (_imageSetTypeEnum != null)
    {
      return true;
    }

    return false;
  }

  /**
   * @author George A. David
   */
  public void setProjectName(String projectName)
  {
    Assert.expect(projectName != null);

    _projectName = projectName;
  }

  /**
   * @author George A. David
   */
  public String getProjectName()
  {
    Assert.expect(_projectName != null);

    return _projectName;
  }

  /**
   * @author George A. David
   */
  public void setTestProgramVersionNumber(int testProgramVersionNumber)
  {
    Assert.expect(testProgramVersionNumber > 0);

    _testProgramVersionNumber = testProgramVersionNumber;
  }

  /**
   * @author George A. David
   */
  public int getTestProgramVersionNumber()
  {
    Assert.expect(_testProgramVersionNumber > 0);

    return _testProgramVersionNumber;
  }

  /**
   * @author George A. David
   */
  public void setImageSetName(String imageSetName)
  {
    Assert.expect(imageSetName != null);

    _imageSetName = imageSetName;
  }

  /**
   * @author George A. David
   */
  public String getImageSetName()
  {
    Assert.expect(_imageSetName != null);

    return _imageSetName;
  }

  /**
   * @author Patrick Lacz
   */
  public String getDir()
  {
    if (isOnlineTestDevelopment())
    {
      return Directory.getOnlineXrayImagesDir(getImageSetName());
    }
    else
    {
      return Directory.getXrayInspectionImagesDir(getProjectName(), getImageSetName());
    }
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean collectImagesOnAllBoardInstances()
  {
    return _collectImagesOnAllBoardInstances;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setCollectImagesOnAllBoardInstances(boolean collectImagesOnAllBoardInstances)
  {
    _collectImagesOnAllBoardInstances = collectImagesOnAllBoardInstances;
  }

  /**
   * @author Erica Wheatcroft
   */
  public boolean collectImagesForPanel()
  {
    return _collectImagesForPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setCollectImagesForPanel(boolean collectImagesForPanel)
  {
    _collectImagesForPanel = collectImagesForPanel;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setNumberOfImagesSaved(int numberOfImagesSaved)
  {
    Assert.expect(numberOfImagesSaved >= 0, "number is less than zero");
    _numberOfImagesSaved = numberOfImagesSaved;
  }

  /**
   * @author Erica Wheatcroft
   */
  public int getNumberOfImagesSaved()
  {
    return _numberOfImagesSaved;
  }

  /**
   * This is a fast call to return an approx number.
   * The estimate assumes an average of 8600 bytes per image
   * @author Andy Mechtenberg
   */
  public long getEstimatedSizeInBytes(boolean singleImage)
  {
    if (singleImage)
    {
      return 8600;
    }
    int numImages = getNumberOfImagesSaved();
    long sizeOfSelectedImageSetsInBytes = 0;
    if (numImages < 1000)
    {
      // use slow mode here as it's fast enought
      long slowSize = FileUtil.getSizeOfDirectoryContentsInBytes(getDir());
      sizeOfSelectedImageSetsInBytes = slowSize;
    }
    else
    {
      long fastSize = numImages * 8600; // 8600 is an average size in bytes of a single inspection image
      sizeOfSelectedImageSetsInBytes = fastSize;
    }
    return sizeOfSelectedImageSetsInBytes;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setDateInMils(long dateInMils)
  {
    _dateInMils = dateInMils;
  }

  /**
   * @author Erica Wheatcroft
   */
  public long getDateInMils()
  {
    return _dateInMils;
  }

  /**
   * @author Erica Wheatcroft
   */
  public void setMachineSerialNumber(String machineSerialNumber)
  {
    Assert.expect(machineSerialNumber != null, "machine serial number is null");
    _machineSerialNumber = machineSerialNumber;
  }

  /**
   * @author Erica Wheatcroft
   */
  public String getMachineSerialNumber()
  {
    Assert.expect(_machineSerialNumber != null, "machine serial number is not set");
    return _machineSerialNumber;
  }

  /**
   * @author George A. David
   */
  public void setIsOnlineTestDevelopment(boolean isOnlineTestDevelopment)
  {
    _isOnlineTestDevelopment = isOnlineTestDevelopment;
  }

  /**
   * @author George A. David
   */
  public boolean isOnlineTestDevelopment()
  {
    return _isOnlineTestDevelopment;
  }

  /**
   * @author George A. David
   */
  public double getPercentOfCompatibleImages()
  {
    Assert.expect(_percentOfCompatibleImages >= 0);

    return _percentOfCompatibleImages;
  }

  /**
   * @author George A. David
   */
  public void setPercentOfCompatibleImages(double percentOfCompatibleImages)
  {
    Assert.expect(percentOfCompatibleImages >= 0);

    _percentOfCompatibleImages = percentOfCompatibleImages;
  }

  /**
   * @author Laura Cormos
   */
  public void setImageSetCollectionTypeEnum(ImageSetCollectionTypeEnum collectionTypeEnum)
  {
    Assert.expect(collectionTypeEnum != null);

    _imageSetCollectionTypeEnum = collectionTypeEnum;
  }

  /**
   * @author Laura Cormos
   */
  public ImageSetCollectionTypeEnum getImageSetCollectionTypeEnum()
  {
    Assert.expect(_imageSetCollectionTypeEnum != null);

    return _imageSetCollectionTypeEnum;
  }

  /**
   * @author Laura Cormos
   */
  public boolean isCollectionTypeEnumSet()
  {
    if (_imageSetCollectionTypeEnum != null)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * @return true if both objects are equal
   * @author Erica Wheatcroft
   */
  public boolean equals(Object object)
  {
    if ((object instanceof ImageSetData) == false)
    {
      return false;
    }

    if (this == object)
    {
      return true;
    }

    ImageSetData imageSetData = (ImageSetData) object;

    if (_imageSetName.equalsIgnoreCase(imageSetData.getImageSetName()) == false)
    {
      return false;
    }

    if (_imageSetTypeEnum.equals(imageSetData.getImageSetTypeEnum()) == false)
    {
      return false;
    }

    if (_projectName.equalsIgnoreCase(imageSetData.getProjectName()) == false)
    {
      return false;
    }

    if ((_testProgramVersionNumber == imageSetData.getTestProgramVersionNumber()) == false)
    {
      return false;
    }

    if ((_populatedBoard == imageSetData.isBoardPopulated()) == false)
    {
      return false;
    }

    // if we got to this point then they are equal
    return true;
  }

  /**
   * @author Erica Wheatcroft
   */
  public int hashCode()
  {
    // the algorithm was taken from effective java item #8.

    int result = 17;
    result = 37 * result + _imageSetName.hashCode();
    result = 37 * result + _imageSetTypeEnum.hashCode();
    result = 37 * result + _projectName.hashCode();

    long longValue = Double.doubleToLongBits(_testProgramVersionNumber);
    result = 37 * result + (int) (longValue ^ (longValue >>> 32));

    if (_populatedBoard)
    {
      result = 37 * result + 0;
    }
    else
    {
      result = 37 * result + 1;
    }

    return result;
  }

  /**
   * @author Peter Esbensen
   */
  public int compareTo(Object object) throws ClassCastException
  {
    if (!(object instanceof ImageSetData))
    {
      throw new ClassCastException("Wrong class used in ImageSetData.compareTo()");
    }

    if (this == object)
    {
      return 0;
    }

    ImageSetData imageSetData = (ImageSetData) object;

    if (_imageSetName.equalsIgnoreCase(imageSetData.getImageSetName()) == false)
    {
      return _imageSetName.compareTo(imageSetData.getImageSetName());
    }

    if (_imageSetTypeEnum.equals(imageSetData.getImageSetTypeEnum()) == false)
    {
      return _imageSetTypeEnum.compareTo(imageSetData.getImageSetTypeEnum());
    }

    if (_projectName.equalsIgnoreCase(imageSetData.getProjectName()) == false)
    {
      return _projectName.compareTo(imageSetData.getProjectName());
    }

    if ((_testProgramVersionNumber == imageSetData.getTestProgramVersionNumber()) == false)
    {
      if (_testProgramVersionNumber < imageSetData.getTestProgramVersionNumber())
      {
        return -1;
      }
      else
      {
        return 1;
      }
    }

    if ((_populatedBoard == imageSetData.isBoardPopulated()) == false)
    {
      return -1;
    }

    return 1;
  }

  /**
   * @author khang-shian.sham
   */
  public void setSaveGoodImages(boolean bool)
  {
    _saveGoodImages = bool;
  }

  /**
   * @author khang-shian.sham
   */
  public boolean isSaveGoodImages()
  {
    return _saveGoodImages;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setGenerateMultiAngleImages(boolean isGenerateMultiAngleImagesEnabled)
  {
    _isGenerateMultiAngleImagesEnabled = isGenerateMultiAngleImagesEnabled;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public boolean isGenerateMultiAngleImagesEnabled()
  {
    return _isGenerateMultiAngleImagesEnabled;
  }
 
  /**
   * @author Ooi Yi Fong 
   */
  public void setHighMagnification(String highMagnification)
  {
    Assert.expect(highMagnification != null);
    _highMagnification = highMagnification;
  }

 /**
   * @author Ooi Yi Fong 
   */
  public void setLowMagnification(String lowMagnification)
  {
    Assert.expect(lowMagnification != null);
    _lowMagnification = lowMagnification;
    
  }

 /**
   * @author Ooi Yi Fong 
   */
  public String getHighMagnification()
  {
    return _highMagnification;
  }
  
 /**
   * @author Ooi Yi Fong 
   */
  public boolean hasHighMagnification()
  {
    if (_highMagnification == null)
      return false; 
    else
      return true;
  }

 /**
   * @author Ooi Yi Fong 
   */
  public String getLowMagnification()
  {
    return _lowMagnification;
  }
  
 /**
   * @author Ooi Yi Fong 
   */
  public boolean hasLowMagnification()
  {
    if (_lowMagnification == null)
      return false;
    else
      return true;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void setVariationName(String name)
  {
    Assert.expect(name != null);
    _selectedVariationName = name;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public String getVariationName()
  {
    return _selectedVariationName;
  }

  /**
   * sheng chuan
   * @param status 
   */
  public void setImageSetDataLoadStatus(boolean status)
  {
    _isImageSetFullyLoaded = status;
  }

  /**
   * sheng chuan
   * @param status 
   */
  public boolean isImageSetDataFullyLoaded()
  {
    return _isImageSetFullyLoaded;
  }
}        
