package com.axi.v810.business;

import java.awt.image.*;
import java.io.*;
import java.util.*;

import javax.imageio.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * <p>Title: Test_ImageManager</p>
 * <p>Description: Regression test for ImageManager class</p>
 * <p>Copyright: Copyright (c) 2004</p>
 * <p>Company:  Agilent Technogies</p>
 * @author Kay Lannen
 * @version 1.0
 */
public class Test_ImageManager extends UnitTest
{
  private ImageManager _imageManager = ImageManager.getInstance();
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
//    ImageIoUtil.getImageWriter("null", "PNG");
//    ImageIoUtil.getImageWriter("null", "JPG");
//    debugTest();
    UnitTest.execute(new Test_ImageManager());
  }

  /**
   * @author George A. David
   */
  private static Project loadProject(String projectName)
  {
//    System.out.println(FileName.getLegacyPanelNdfFullPath(projectName));
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;
    try
    {
      project = Project.importProjectFromNdfs(projectName, warnings);
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
      Assert.expect(false);
    }

    return project;
  }

  /**
   * @author George A. David
   */
  private static void debugTest()
  {
    TimerUtil timer = new TimerUtil();
    int count = 0;
    String tempTestDir = Directory.getTempDir() + File.separator + "imageManagerDebugTest";
    try
    {
      if(FileUtilAxi.exists(tempTestDir) == false)
        FileUtilAxi.mkdirs(tempTestDir);
      String imagesDir = "d:/genesis/temp/inspectionImages/2006-07-18_09-34-04";
      File file = new File(imagesDir);
      File[] files = file.listFiles();

      for (File imageFile : files)
      {
        if (imageFile.getAbsolutePath().endsWith(".png"))
        {
          BufferedImage buffImg = XrayImageIoUtil.loadPngBufferedImage(imageFile.getAbsolutePath());
//          Image image = new Image(buffImg);
          timer.start();
          XrayImageIoUtil.savePngImage(buffImg, tempTestDir + File.separator + count + "_" + imageFile.getName());
//          XrayImageIoUtil.saveJpegImage(buffImg, tempTestDir + File.separator + count + "_" + imageFile.getName() + ".jpg");
//          ImageIO.write(image, "png", new File(tempTestDir + File.separator + imageFile.getName() + "_" + count));
//          Image.createCopy(new Image(image));
//          Assert.expect(image.getByteBuffer().isDirect());
//          Image.savePng(image.getByteBuffer(), image.getWidth(), image.getHeight(), tempTestDir + File.separator + count + "_" + imageFile.getName());
          timer.stop();
          ++count;
        }
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        if (FileUtilAxi.exists(tempTestDir))
        {
          FileUtilAxi.delete(tempTestDir);
        }
      }
      catch (DatastoreException ex1)
      {
        ex1.printStackTrace();
      }

      System.out.println("Time to save "+ count + " png images: " + timer.getElapsedTimeInMillis());
    }
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String imagesDir = Directory.getXrayImagesDir("0027parts");
    String backupImagesDir = imagesDir + ".backup";
    try
    {
      if(FileUtilAxi.exists(imagesDir))
         FileUtilAxi.rename(imagesDir, backupImagesDir);
      Project project = loadProject("0027parts");

      ImageSetData imageSetData = new ImageSetData();
      imageSetData.setImageSetName("myProjectImages");
      imageSetData.setProjectName(project.getName());

      testSaveAndLoadImages(project, imageSetData);
      testImageRemoval(imageSetData);

      testImageSetCompatibility();

      // test image compatibility
//      imageSetData = new ImageSetData();
//      imageSetData.setTestProgramVersionNumber(1);
//      imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
//      imageSetData.setMachineSerialNumber("serial");
//      imageSetData.setUserDescription("my description");
//      imageSetData.setProjectName(project.getName());
//      imageSetData.setImageSetName("Test_ImageManager");
//      ImageSetInfoWriter infoWriter = new ImageSetInfoWriter();
//      infoWriter.write(imageSetData);
//      _imageManager.saveInspectionImageInfo(imageSetData, project.getTestProgram());
//
//      List<ImageSetData> compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
//      Assert.expect(compatibleImageSets.size() == 1);
//      ImageSetData compatibleImageSetData = compatibleImageSets.get(0);
//      Expect.expect(compatibleImageSetData.getProjectName().equalsIgnoreCase(imageSetData.getProjectName()));
//      Expect.expect(compatibleImageSetData.getImageSetName().equalsIgnoreCase(imageSetData.getImageSetName()));
//
//      // delete a component, and confirm that this
//      // image set is no longer compatible, it
//      // will have too many images.
//      Board board = project.getPanel().getBoards().get(0);
//      board.removeComponent(board.getComponents().get(0));
//      compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
//      Expect.expect(compatibleImageSets.isEmpty());
//
//      // ok, now save the inspection image names with the delete component
//      _imageManager.saveInspectionImageInfo(imageSetData, project.getTestProgram());
//
//      // reload the project so the component we
//      // deleted is back, and confirm that the
//      // image names are compatible, because
//      // all the images in the image set are
//      // used, we are just missing some for
//      // the component we deleted
//      project = loadProject("0027parts");
//      compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
//      Assert.expect(compatibleImageSets.size() == 1);
//      compatibleImageSetData = compatibleImageSets.get(0);
//      Expect.expect(compatibleImageSetData.getProjectName().equalsIgnoreCase(imageSetData.getProjectName()));
//      Expect.expect(compatibleImageSetData.getImageSetName().equalsIgnoreCase(imageSetData.getImageSetName()));
//
//      _imageManager.deleteImages(imageSetData);
//
//      // ok, now try a big project
//      project = loadProject("41k_joints");
//      imageSetData.setProjectName(project.getName());
//      infoWriter.write(imageSetData);
//      TestProgram testProgram = project.getTestProgram();
//      _imageManager.saveInspectionImageInfo(imageSetData, testProgram);
//      compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
//      Assert.expect(compatibleImageSets.size() == 1);
//      compatibleImageSetData = compatibleImageSets.get(0);
//      Expect.expect(compatibleImageSetData.getProjectName().equalsIgnoreCase(imageSetData.getProjectName()));
//      Expect.expect(compatibleImageSetData.getImageSetName().equalsIgnoreCase(imageSetData.getImageSetName()));
//      _imageManager.deleteImages(imageSetData);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      Expect.expect( false, "Exception:" + ex.getMessage() );
    }
    finally
    {
      try
      {
        if(FileUtilAxi.exists(imagesDir))
          FileUtilAxi.delete(imagesDir);
        if(FileUtilAxi.exists(backupImagesDir))
          FileUtilAxi.rename(backupImagesDir, imagesDir);
      }
      catch (DatastoreException ex1)
      {
        ex1.printStackTrace();
      }
    }
  }

  /**
   * @author George A. David
   */
  private void testSaveAndLoadImages(Project project, ImageSetData imageSetData)
  {
    // Create a test image.
   BufferedImage buffImg = Test_ImageIoUtil.getGradientImage();

   // Create a panel rectangle describing an image.
   int imageWidth = 800000;
   int imageHeight = 4096000;

   // Save some inspection and panel tile images.
    try
    {
      TestProgram program = project.getTestProgram();
      program.addFilter(project.getPanel().getPads().get(0));

      FileUtilAxi.createDirectory(imageSetData.getDir());
      for(ReconstructionRegion region : program.getFilteredInspectionRegions())
      {
        for(Slice slice : region.getReconstructionSlices())
          _imageManager.saveInspectionImageToBeDeprecated(imageSetData, buffImg, region.getName(), slice.getSliceName().getId(), region.getRegionRectangleRelativeToPanelInNanoMeters());
      }

      // Now load the saved images.
      for(ReconstructionRegion region : program.getFilteredInspectionRegions())
      {
        testLoadInspectionImagesForRegion(imageSetData, region, buffImg, region.getReconstructionSlices().size());
      }

      ReconstructionRegion verificationRegion = program.getVerificationRegions().get(0);
      FileUtilAxi.createDirectory(Directory.getXrayVerificationImagesDir(project.getName()));
      _imageManager.saveVerificationImage(project.getName(), verificationRegion, buffImg);
      testLoadPanelTileImage(imageSetData, verificationRegion, buffImg);
    }
    catch (Exception exc)
    {
      exc.printStackTrace();
      Expect.expect( false, exc.getMessage() );
    }
  }

  /**
   * @author George A. David
   */
  private void testLoadInspectionImagesForRegion(ImageSetData imageSetData,
                                                 ReconstructionRegion region,
                                                 BufferedImage buffImgExpect,
                                                 int numImagesExpected )
  {
    try
    {
      ReconstructedImages reconImagesResult = _imageManager.loadInspectionImages(imageSetData, region );
      Expect.expect( reconImagesResult.getNumberOfSlices() == numImagesExpected );

      for (ReconstructedSlice reconImage : reconImagesResult.getReconstructedSlices())
      {
        Image imgBuffResult = reconImage.getImage();
        BufferedImage buffImgResult = imgBuffResult.getBufferedImage();
        Expect.expect( Test_ImageIoUtil.compareBufferedImages( buffImgExpect, buffImgResult ));
      }

      reconImagesResult.decrementReferenceCount();
    }
    catch (DatastoreException ex)
    {
      Expect.expect( false, "DatastoreException:" + ex.getMessage() );
    }
  }

  /**
   * @author George A. David
   */
  private void testLoadPanelTileImage(ImageSetData imageSetData,
                                      ReconstructionRegion reconstructionRegion,
                                      BufferedImage buffImgExpect )
  {
    try
    {
      BufferedImage buffImgResult = _imageManager.loadVerificationImage(imageSetData.getProjectName(), reconstructionRegion);
      // The images will not match because of Jpeg lossy compression.  That's okay,
      // we just want to see if the saved image will load without an error.
      Expect.expect( Test_ImageIoUtil.compareBufferedImages( buffImgExpect, buffImgResult ) == false );
    }
    catch (DatastoreException ex)
    {
      Expect.expect( false, "DatastoreException:" + ex.getMessage() );
    }
  }

  /**
   * @author George A. David
   */
  private void testImageRemoval(ImageSetData imageSetData) throws DatastoreException
  {
    _imageManager.deleteImages(imageSetData);
  }

  /**
   * @author George A. David
   */
  private void testImageSetCompatibility() throws DatastoreException
  {
    SystemTypeEnum currentSystemType = XrayTester.getSystemType();
    boolean isHardwareAvailable = XrayTester.isHardwareAvailable();
    Config config = Config.getInstance();
    config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false);
    try
    {
      Project project = loadProject("0027parts");
      TestProgram program = project.getTestProgram();
      program.addFilter(project.getPanel().getPads().get(0));

      // test image compatibility
      ImageSetData imageSetData = new ImageSetData();
      imageSetData.setTestProgramVersionNumber(1);
      imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
      imageSetData.setMachineSerialNumber("serial");
      imageSetData.setUserDescription("my description");
      imageSetData.setProjectName(project.getName());
      imageSetData.setImageSetName("Test_ImageManager");
      ImageSetInfoWriter infoWriter = new ImageSetInfoWriter();
      infoWriter.write(imageSetData);
      _imageManager.saveInspectionImageInfo(imageSetData, project.getTestProgram());

      List<ImageSetData> compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
      Assert.expect(compatibleImageSets.size() == 1);
      ImageSetData compatibleImageSetData = compatibleImageSets.get(0);
      Expect.expect(compatibleImageSetData.getProjectName().equalsIgnoreCase(imageSetData.getProjectName()));
      Expect.expect(compatibleImageSetData.getImageSetName().equalsIgnoreCase(imageSetData.getImageSetName()));

      // delete a component, and confirm that this
      // image set is no longer compatible, it
      // will have too many images.
      Board board = project.getPanel().getBoards().get(0);
      board.removeComponent(board.getComponents().get(0));
      compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
      Expect.expect(compatibleImageSets.isEmpty());

      // ok, now save the inspection image names with the delete component
      _imageManager.saveInspectionImageInfo(imageSetData, project.getTestProgram());

      // reload the project so the component we
      // deleted is back, and confirm that the
      // image names are compatible, because
      // all the images in the image set are
      // used, we are just missing some for
      // the component we deleted
      project = loadProject("0027parts");
      compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
      Assert.expect(compatibleImageSets.size() == 1);
      compatibleImageSetData = compatibleImageSets.get(0);
      Expect.expect(compatibleImageSetData.getProjectName().equalsIgnoreCase(imageSetData.getProjectName()));
      Expect.expect(compatibleImageSetData.getImageSetName().equalsIgnoreCase(imageSetData.getImageSetName()));

      // change the system type of the project and confirm that the images are no longer compatible
      SystemTypeEnum otherType = null;
      for (SystemTypeEnum systemType : SystemTypeEnum.getAllSystemTypes())
      {
        if (systemType.equals(currentSystemType) == false)
        {
          otherType = systemType;
          break;
        }
      }
      project.setSystemType(otherType);
      XrayTester.changeSystemTypeIfNecessary(otherType);
      compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
      Expect.expect(compatibleImageSets.isEmpty());
      XrayTester.changeSystemTypeIfNecessary(currentSystemType);

      _imageManager.deleteImages(imageSetData);

      // ok, now try a big project
      project = loadProject("41k_joints");
      imageSetData.setProjectName(project.getName());
      infoWriter.write(imageSetData);
      TestProgram testProgram = project.getTestProgram();
      _imageManager.saveInspectionImageInfo(imageSetData, testProgram);
      compatibleImageSets = _imageManager.getCompatibleImageSetData(project);
      Assert.expect(compatibleImageSets.size() == 1);
      compatibleImageSetData = compatibleImageSets.get(0);
      Expect.expect(compatibleImageSetData.getProjectName().equalsIgnoreCase(imageSetData.getProjectName()));
      Expect.expect(compatibleImageSetData.getImageSetName().equalsIgnoreCase(imageSetData.getImageSetName()));
      _imageManager.deleteImages(imageSetData);
    }
    finally
    {
      // reset the original value
      XrayTester.changeSystemTypeIfNecessary(currentSystemType);
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, isHardwareAvailable);
    }
  }
}
