package com.axi.v810.business.resultsProcessing;

import java.io.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is responsible for copying sets of files - specifically result images.
 *
 * @author Patrick Lacz
 */
public class MultipleFileTarget extends Target
{
  private ResultFileTypeEnum _resultFileTypeEnum = null;
  private List<String> _destinationPathList = null;

  /**
   * Choose the correct source directory and the file-name matching pattern.
   *
   * @author Patrick Lacz
   */
  public static Pair<String, String> getSourceFilePathAndNamePattern(
      ResultFileTypeEnum resultFileTypeEnum,
      String projectName,
      long inspectionStartTimeInMillis)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(projectName != null);

    String sourcePath = Directory.getInspectionResultsDir(projectName, inspectionStartTimeInMillis);
    String sourceFileNamePattern = "";

    if (resultFileTypeEnum.equals(ResultFileTypeEnum.DEFECT_IMAGE_FILES))
    {
      sourceFileNamePattern = "^.*\\.jpg$";
    }
    else
    {
      Assert.expect(false, "Unexpected Result File Type");
    }

    return new Pair<String, String>(sourcePath, sourceFileNamePattern);
  }

  /**
   * @author Patrick Lacz
   */
  public static List<String> getResultFilesForEnum(ResultFileTypeEnum resultFileTypeEnum, String projectName,
      long inspectionStartTimeInMillis)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(projectName != null);
    List<String> listofFiles = new ArrayList<String>();

    Pair<String, String> imagesPathAndPattern =
        MultipleFileTarget.getSourceFilePathAndNamePattern(resultFileTypeEnum, projectName, inspectionStartTimeInMillis);

    // The path should be absolute, and the pattern a regular expression like '.*\.jpg'
    String imagePath = imagesPathAndPattern.getFirst();
    String imageNamePattern = imagesPathAndPattern.getSecond();

    Pattern fileNamePattern = Pattern.compile(imageNamePattern, Pattern.CASE_INSENSITIVE);

    // gather all the file names.
    Collection<String> filesInInspectionResultDirectory = FileUtilAxi.listAllFilesFullPathInDirectory(imagePath);
    for (String sourceFileNameWithFullPath : filesInInspectionResultDirectory)
    {
      String fileName = FileUtil.getNameWithoutPath(sourceFileNameWithFullPath);
      Matcher matcher = fileNamePattern.matcher(fileName);
      if (matcher.matches())
      {
        listofFiles.add(sourceFileNameWithFullPath);
      }
    }
    return listofFiles;
  }

  /**
   * @author Patrick Lacz
   */
  public void initialize(ResultFileTypeEnum fileTypeEnum, List<String> destinationList)
  {
    Assert.expect(fileTypeEnum != null);
    Assert.expect(destinationList != null);
    Assert.expect(fileTypeEnum.getTargetImplementationClass().equals(this.getClass()));

    _resultFileTypeEnum = fileTypeEnum;
    _destinationPathList = new ArrayList<String>(destinationList);
  }

  /**
   * @author Patrick Lacz
   */
  public ResultFileTypeEnum getResultFileTypeEnum()
  {
    Assert.expect(_resultFileTypeEnum != null);
    return _resultFileTypeEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public List<DatastoreException> performCopy(InspectionInformation inspectionInformation)
  {
    Assert.expect(inspectionInformation != null);
    Assert.expect(_resultFileTypeEnum != null);
    Assert.expect(_destinationPathList != null);

    List<DatastoreException> listOfDatastoreExceptions = new LinkedList<DatastoreException>();

    if (_resultFileTypeEnum.equals(ResultFileTypeEnum.INTERNAL_RESULTS_IMAGE_SET_FILES))
    {
      String imageSetFullPath = inspectionInformation.getProductionTuningImageSetFullPath();
      for (String destinationFullPath : _destinationPathList)
      {
//        System.out.println("Copy from: " + imageSetFullPath);
//        System.out.println("Copy to  : " + destinationFullPath);

        try
        {
          FileUtilAxi.createDirectory(destinationFullPath);
          FileUtilAxi.copyDirectory(imageSetFullPath, destinationFullPath);
          
          if (FileUtilAxi.exists(imageSetFullPath))
            FileUtilAxi.delete(imageSetFullPath);
        }
        catch (DatastoreException de)
        {
          listOfDatastoreExceptions.add(de);
          ResultsProcessor.getInstance().addFailedOperation(this, inspectionInformation, de);
        }
      }
      return listOfDatastoreExceptions;
    }


    // do symbolic replacement on the target paths.
    List<String> absoluteDestinationPaths = new ArrayList<String>(_destinationPathList.size());
    for (String rawDestinationPath : _destinationPathList)
    {
      absoluteDestinationPaths.add(Rule.replaceSymbolsInDestinationPath(rawDestinationPath, inspectionInformation));
    }

    String projectName = inspectionInformation.getProjectName();

    Collection<String>
        filesInInspectionDirectory = getResultFilesForEnum(_resultFileTypeEnum, projectName, inspectionInformation.getInspectionStartTime());


    // Copy each of the files as a 'single copy'
    for (String imageFileNameWithPath : filesInInspectionDirectory)
    {
      String fileName = FileUtil.getNameWithoutPath(imageFileNameWithPath);
      for (String finalDestinationPath : absoluteDestinationPaths)
      {
        finalDestinationPath = FileUtil.concatFileNameToPath(finalDestinationPath, fileName);

        try
        {
          performSingleCopy(imageFileNameWithPath, finalDestinationPath);
        }
        catch (DatastoreException de)
        {
          listOfDatastoreExceptions.add(de);
          ResultsProcessor.getInstance().addFailedOperation(this, inspectionInformation, de);
        }
      }
    }
    return listOfDatastoreExceptions;
  }

  /**
   * @author Patrick Lacz
   */
  public List<String> getDestinationList()
  {
    Assert.expect(_destinationPathList != null);
    return _destinationPathList;
  }

  /**
   * @author Patrick Lacz
   */
  public void setDestinationList(List<String> newListOfDestinations)
  {
    Assert.expect(newListOfDestinations != null);
    _destinationPathList = new ArrayList<String>(newListOfDestinations);
  }
}

