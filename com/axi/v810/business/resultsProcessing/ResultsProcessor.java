package com.axi.v810.business.resultsProcessing;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;
import java.util.concurrent.locks.*;
import java.util.concurrent.locks.Lock;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Patrick Lacz
 */
public class ResultsProcessor
{
  // The results processor is a singleton. Instanteate it by calling loadResultsProcessorFromConfigurationFile().
  // use hasInstance() to wrap getInstance() calls.
  private static ResultsProcessor _instance = null;

  // The order of the list determines the priority of the rules
  // Earlier in the list, the higher the priority.
  private List<Rule> _rules = new LinkedList<Rule>();

  private String _repairToolDirectory = "";
  private Rule _repairToolRule = new Rule();
  private boolean _repairToolRuleIsEnabled = false;

  private ProductionTuneManager _productionTuneManager = ProductionTuneManager.getInstance();

  // we keep track of the copies that have failed and attempt to redo them.
  private Map<Pair<Target, InspectionInformation>, List<DatastoreException>> _failedOperationToExceptionMap =
      new HashMap<Pair<Target, InspectionInformation>, List<DatastoreException>>();

  // create a map that has the result directories sorted from earliest to latest.
  // if this queue's size grows larger than the number of results we want to keep, we start popping
  // directories and deleting them.
  private PriorityQueue<Pair<Long, String>> _localResultsDeletionQueue = null;

  // create a map that has the result directories sorted from earliest to latest.
  // if this queue's size grows larger than the number of image results we want to keep, we start popping
  // and deleting the images from these directories (but not the other results)
  private PriorityQueue<Pair<Long, String>> _localRepairFilesDeletionQueue = null;

  // a thread-safe queue used to communicate test completions to the results processor.
  // A "test result" is a program and a time stamp
  private AbstractQueue<InspectionInformation> _testResultsToBeProcessed = null;

  private static WorkerThread _workerThread = new WorkerThread("Results Processor");

  private AtomicBoolean _retryCopyFailuresFlag = new AtomicBoolean(false);
  private AtomicBoolean _clearCopyFailuresFlag = new AtomicBoolean(false);

  private Lock _workWaitingLock = new ReentrantLock();
  private java.util.concurrent.locks.Condition _workWaitingForProcessingThread = _workWaitingLock.newCondition();

  private static ResultsProcessingObservable _resultsProcessingObservable = ResultsProcessingObservable.getInstance();
  private static Config _config = Config.getInstance();

  private boolean _resultsProcessingEnabled;
  private boolean _deletionPolicyEnabled;
  private int _maximumNumberOfRepairResultsToKeep;
  private int _maximumNumberOfResultsToKeep;

  private static boolean _resultIsProcessDone = true;

  /**
   * Upon construction, figure out what result directories are still on the system. Add them to the to-be-deleted queue.
   * @author Patrick Lacz
   */
  private ResultsProcessor()
  {
    _testResultsToBeProcessed = new ConcurrentLinkedQueue<InspectionInformation>();

    _resultsProcessingEnabled = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_RESULTS_PROCESSING);
    _deletionPolicyEnabled = _config.getBooleanValue(SoftwareConfigEnum.AUTOMATICALLY_DELETE_RESULTS);
    _maximumNumberOfRepairResultsToKeep = _config.getIntValue(SoftwareConfigEnum.KEEP_N_MOST_RECENT_REPAIR_FILE_RESULTS);
    _maximumNumberOfResultsToKeep = _config.getIntValue(SoftwareConfigEnum.KEEP_N_MOST_RECENT_RESULT_FILES);

    // start a thread that monitors for results to process.
    final ResultsProcessor thisResultsProcessor = this;
    _workerThread.invokeLater(new Runnable(){
      public void run()
      {
        thisResultsProcessor.waitForResultsToProcess();
      }
    });
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized static ResultsProcessor getInstance()
  {
    if (_instance == null)
      _instance = new ResultsProcessor();
    return _instance;
  }

  /**
   * Intended for unit testing from a blank slate.
   * @author Patrick Lacz
   */
  static synchronized ResultsProcessor createEmptyResultsProcessor()
  {
    _instance = new ResultsProcessor();
    return _instance;
  }

  /**
   * @author Patrick Lacz
   */
  public void loadFromConfigurationFile() throws DatastoreException
  {
    ResultsProcessorReader.getInstance().readResultsProcessorConfigFile();
  }

  /**
   * @author Patrick Lacz
   */
  public void saveToConfigurationFile() throws DatastoreException
  {
    ResultsProcessorWriter.writeConfigurationFile();
  }

  /**
   * @author Patrick Lacz
   */
  private void initializeDeletionQueues()
  {
    Assert.expect(_localResultsDeletionQueue == null);
    Assert.expect(_localRepairFilesDeletionQueue == null);

    _localResultsDeletionQueue = new PriorityQueue<Pair<Long,String>>(20, new PairComparator<Long, String>());
    _localRepairFilesDeletionQueue = new PriorityQueue<Pair<Long,String>>(20, new PairComparator<Long, String>());

    String resultsDirectory = Directory.getResultsDir();
    if (FileUtilAxi.exists(resultsDirectory) == false)
      return;

    Collection<String>
        namesOfProjectsWithResults = FileUtilAxi.listAllSubDirectoriesInDirectory(Directory.getResultsDir());

    // iterate over all the projects, then all the result directories within that project
    for (String projectName : namesOfProjectsWithResults)
    {
      List<String> inspectionResultDirectoryList = Directory.getInspectionResultsDirs(projectName);
      for (String inspectionResultDirectory : inspectionResultDirectoryList)
      {
        long timeInMillis = Directory.getTimeStamp(inspectionResultDirectory);

        _localResultsDeletionQueue.add(new Pair<Long, String>(timeInMillis, inspectionResultDirectory));

        // check for defect images
        /** @todo PWL : Really this should be looking for xml files, not just jpgs */
        if (inspectionDirectoryContainsRepairToolData(projectName, timeInMillis))
        {
          _localRepairFilesDeletionQueue.add(new Pair<Long, String>(timeInMillis, inspectionResultDirectory));
        }
      }
    }
  }

  /**
   * This method looks in the result directory for any of the repair tool files.
   * At the moment, that means any jpg, xml, or xsd files.
   * @author Patrick Lacz
   */
  private boolean inspectionDirectoryContainsRepairToolData(String projectName, long inspectionStartTimeInMillis)
  {
    Assert.expect(projectName != null);

    String fileNameRegularExpression = "^.*\\.(jpg|xml|xsd)$";
    Pattern fileNamePattern = Pattern.compile(fileNameRegularExpression, Pattern.CASE_INSENSITIVE);

    String sourcePath = Directory.getInspectionResultsDir(projectName, inspectionStartTimeInMillis);

    // gather all the file names.
    Collection<String> filesInInspectionResultDirectory = FileUtilAxi.listAllFilesFullPathInDirectory(sourcePath);
    for (String sourceFileNameWithFullPath : filesInInspectionResultDirectory)
    {
      String fileName = FileUtil.getNameWithoutPath(sourceFileNameWithFullPath);
      Matcher matcher = fileNamePattern.matcher(fileName);
      if (matcher.matches())
      {
        return true;
      }

    }
    return false;
  }

  /**
   * @author Patrick Lacz
   */
  public static Target createTarget(ResultFileTypeEnum resultFileTypeEnum, List<String> destinationList)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(destinationList != null);

    Target newTarget = ResultFileTypeEnum.createTarget(resultFileTypeEnum, destinationList);

    Assert.expect(newTarget != null);
    return newTarget;
  }

  /**
   * @author Patrick Lacz
   */
  public static Condition createRegularExpressionCondition(ConditionKeyEnum conditionKeyEnum, List<String> patternStringList)
      throws InvalidRegularExpressionDatastoreException
  {
    Assert.expect(conditionKeyEnum != null);
    Assert.expect(patternStringList != null);

    String combinedRegEx = "";

    for (String regEx : patternStringList)
    {
      if (combinedRegEx.length() > 0)
        combinedRegEx = combinedRegEx + "|";
      combinedRegEx = combinedRegEx + regEx;
    }

    // use a non-capturing group
    combinedRegEx = "^(?:" + combinedRegEx + ")$";

    Pattern compiledPattern = null;
    try
    {
      compiledPattern = Pattern.compile(combinedRegEx, Pattern.CASE_INSENSITIVE);
    }
    catch (PatternSyntaxException pse)
    {
      InvalidRegularExpressionDatastoreException ex =  new InvalidRegularExpressionDatastoreException(pse);
      ex.initCause(pse);
    }

    Condition newCondition = ConditionKeyEnum.createConditionInstance(
      conditionKeyEnum,
      patternStringList,
      compiledPattern,
      Condition.REGEX_TYPE_IDENTIFIER);

    Assert.expect(newCondition != null);
    return newCondition;
  }

  /**
   * Creates a condition that matches one of the given pattern strings.
   * @author Patrick Lacz
   */
  public static Condition createSimpleExpressionCondition(ConditionKeyEnum conditionKeyEnum, List<String> patternStringList)
  {
    Assert.expect(conditionKeyEnum != null);
    Assert.expect(patternStringList != null);
    Assert.expect(patternStringList.isEmpty() == false);

    String regExStringOfAllPatterns = new String("");
    for (String patternString : patternStringList)
    {
      String regExString = RegularExpressionUtilAxi.createRegularExpressionFromSimpleExpression(patternString);
      if (regExStringOfAllPatterns.length() > 0)
        regExStringOfAllPatterns = regExStringOfAllPatterns + "|";
      regExStringOfAllPatterns = regExStringOfAllPatterns + regExString;
    }

    // use a non-capturing group
    // there may be an issue if the user attempts to use capturing group recognition due to this concatenation.
    regExStringOfAllPatterns = "^(?:" + regExStringOfAllPatterns + ")$";
    Pattern compiledPattern;
    try
    {
      compiledPattern = Pattern.compile(regExStringOfAllPatterns, Pattern.CASE_INSENSITIVE);
    }
    catch (PatternSyntaxException pse)
    {
      Assert.logException(pse);
      Assert.expect(false);
      return null; // can never reach this code, but satisfies JBuilder about variable initialization
    }

    // Create the Condition from the class that the enum defines to be its implementation class
    Condition newCondition = ConditionKeyEnum.createConditionInstance(
        conditionKeyEnum,
        patternStringList,
        compiledPattern,
        Condition.SIMPLE_TYPE_IDENTIFIER);

    Assert.expect(newCondition != null);
    return newCondition;
  }

  /**
   * @throws InvalidRegularExpressionDatastoreException When a regular expression is not formatted correctly.
   * @throws InvalidRegularExpressionDatastoreException When a simple expression is not formatted correctly.
   * @author Patrick Lacz
   */
  public static Condition createCondition(ConditionKeyEnum conditionKeyEnum, String typeName, List<String> patternStringList) throws DatastoreException
  {
    Assert.expect(conditionKeyEnum != null);
    Assert.expect(typeName != null);
    Assert.expect(patternStringList != null);

    if (typeName.equals(Condition.REGEX_TYPE_IDENTIFIER))
      return createRegularExpressionCondition(conditionKeyEnum, patternStringList);
    if (typeName.equals(Condition.SIMPLE_TYPE_IDENTIFIER))
      return createSimpleExpressionCondition(conditionKeyEnum, patternStringList);
    Assert.expect(false, "Invalid condition type name.");
    return null;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void clear()
  {
    _failedOperationToExceptionMap.clear();
    _repairToolDirectory = "";
    _repairToolRule = null;
    _repairToolRuleIsEnabled = false;
    _rules.clear();
  }


  /**
   * Test Execution should use this method when finished with an inspection result.
   *
   * The information from the test result is added to a queue which is watched by a thread executing waitForResultsToProcess()
   *
   * @author Patrick Lacz
   */
  public void queueInspectionResultForProcessing(TestProgram testProgram,
                                                 long startInspectionTime,
                                                 String panelSerialNumber,
                                                 Map<String, String> boardNameToSerialNumberMap,
                                                 boolean containsRepairToolData,
                                                 String productionTuneImageSetFullPath)
  {
    Assert.expect(testProgram != null);
    Assert.expect(_testResultsToBeProcessed != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);
    Assert.expect(productionTuneImageSetFullPath != null);

    InspectionInformation inspectionInformation = new InspectionInformation(testProgram, startInspectionTime, panelSerialNumber, boardNameToSerialNumberMap);
    // the to-be-processed queue is thread-safe
    // the worker thread is monitoring it and will grab results from it.

    inspectionInformation.setContainsRepairToolData(containsRepairToolData);
    inspectionInformation.setProductionTuningImageSetFullPath(productionTuneImageSetFullPath);

    _workWaitingLock.lock();
    try
    {
      _testResultsToBeProcessed.offer(inspectionInformation);
      _workWaitingForProcessingThread.signal();
    }
    finally
    {
      _workWaitingLock.unlock();
    }
  }

  /**
   * @author Patrick Lacz
   */
  public void userWantsToRetryFailedCopies()
  {
    Assert.expect(_retryCopyFailuresFlag != null);

    _workWaitingLock.lock();
    try
    {
      _retryCopyFailuresFlag.set(true);
      _workWaitingForProcessingThread.signal();
    }
    finally
    {
      _workWaitingLock.unlock();
    }
  }

  /**
   * @author Patrick Lacz
   */
  public void userWantsToIgnoreFailedCopies()
  {
    Assert.expect(_clearCopyFailuresFlag != null);

    _workWaitingLock.lock();
    try
    {
      _clearCopyFailuresFlag.set(true);
      _workWaitingForProcessingThread.signal();
    }
    finally
    {
      _workWaitingLock.unlock();
    }
  }

  /**
   * This is the method that the Results Processor WorkerThread lives in.
   *
   * If we put the gui signals (_retryCopyFailuresFlag and _ignoreCopyFailuresFlag) and _testResultsToBeProcessed
   *   on some common condition, we don't need to wait(333), we can wait on a condition. Much more efficient. Or, we could
   *   move to a task-executor thread model (rather than a polling thread).
   *
   * @author Patrick Lacz
   */
  private void waitForResultsToProcess()
  {
    Assert.expect(_testResultsToBeProcessed != null);

    boolean loopForever = true;
    do
    {
      _workWaitingLock.lock();
      try
      {
        _workWaitingForProcessingThread.await(333, TimeUnit.MILLISECONDS);
      }
      catch (InterruptedException ex1)
      {
        // do nothing, just keep going
      }
      finally
      {
        _workWaitingLock.unlock();
      }

      synchronized (this)
      {
        _resultIsProcessDone = false;
        // check for gui commands first: retry or ignore all

        // retry
        if (_retryCopyFailuresFlag.getAndSet(false) == true)
        {
          // retryFailedCopies may make a notifyObservers call if it has failures re-trying the copies
          retryFailedOperations();
        }

        // ignore
        if (_clearCopyFailuresFlag.getAndSet(false) == true)
        {
          _failedOperationToExceptionMap.clear();
        }

        InspectionInformation resultToProcess = _testResultsToBeProcessed.poll();

        if (resultToProcess != null)
        {
          // inspection result
          if ((_resultsProcessingEnabled) || (resultToProcess.isProductionTuningEnabled()))
          {
            processInspectionResult(resultToProcess);
          }

          addResultToDeletionQueues(resultToProcess);

          try
          {
            enforceDeletionPolicy();
          }
          catch (DatastoreException ex)
          {
            // Send a message to the Gui observable with the list of messages.
            _resultsProcessingObservable.notifyWithNewFailures(Collections.singletonList(ex));
          }
        }
        _resultIsProcessDone = true;
      }
    } while (loopForever);
  }

  /**
   * @author Patrick Lacz
   */
  private void addResultToDeletionQueues(InspectionInformation resultToProcess)
  {
    // add the project to the deletion queues.
    String projectName = resultToProcess.getProjectName();
    String resultsDirectory = Directory.getInspectionResultsDir(projectName, resultToProcess.getInspectionStartTime());

    if (_localResultsDeletionQueue != null && _localRepairFilesDeletionQueue != null)
    {
      // add our new directory to the deletion queue.
      synchronized (_localResultsDeletionQueue)
      {
        _localResultsDeletionQueue.add(new Pair<Long, String>(resultToProcess.getInspectionStartTime(), resultsDirectory));
      }

      if (resultToProcess.getContainsRepairToolData())
      {
        synchronized (_localRepairFilesDeletionQueue)
        {
          _localRepairFilesDeletionQueue.add(new Pair<Long, String>(resultToProcess.getInspectionStartTime(), resultsDirectory));
        }
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  private synchronized void processInspectionResult(InspectionInformation resultToProcess)
  {
    Assert.expect(resultToProcess != null);
    Assert.expect(_rules != null);

    if (resultToProcess.getContainsRepairToolData() == false)
      return;

    String projectName = resultToProcess.getProjectName();
    String resultsDirectory = Directory.getInspectionResultsDir(projectName, resultToProcess.getInspectionStartTime());

    // Execute the rule for the repair tool. This is kept separate from the rest of the rules since the user
    // interacts with it in significantly different ways.
    // We also want to do it first, since it is probably the most time-sensitive.
    if (_repairToolRule != null && _repairToolRuleIsEnabled)
    {
      _repairToolRule.executeActions(resultsDirectory, resultToProcess);
    }

    // use the production Tune Manager to create a rule for this project, and execute it's copy actions
    Target resultsTarget = null;
    try
    {
      if (resultToProcess.isProductionTuningEnabled())
      {
        // if it was enabled, then we have data to copy.
        String resultsDestination = _productionTuneManager.getResultsDestination(projectName);
        String imageDestination = _productionTuneManager.getImageDestination(projectName);

        // now we should fix the two destinations.
        // The Results needs the <projectName>/<resultsDir> added to it
        resultsDestination = resultsDestination + File.separator + Directory.getInspectionResultsBaseDir(projectName, resultToProcess.getInspectionStartTime());
        List<String> pathList = new ArrayList<String>();
        pathList.add(resultsDestination + File.separator);
        resultsTarget = createTarget(ResultFileTypeEnum.INTERNAL_RESULTS_BINARY_FILE, pathList);
        Rule productionTuneRule = new Rule();
        productionTuneRule.setName(StringLocalizer.keyToString("CFGUI_RESPROC_PRODUCTION_TUNE_TAB_TITLE_KEY"));
        productionTuneRule.addTarget(resultsTarget);

        // now, the image set
        String imageSetFullPath = resultToProcess.getProductionTuningImageSetFullPath();
        String imageSetName = imageSetFullPath.substring(imageSetFullPath.lastIndexOf(File.separator)+1, imageSetFullPath.length());
        String destinationFullPath = Directory.getTargetXrayInspectionImagesDir(imageDestination, projectName, imageSetName);
        pathList.clear();
        pathList.add(destinationFullPath);
        Target imagesTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.INTERNAL_RESULTS_IMAGE_SET_FILES, pathList);
        productionTuneRule.addTarget(imagesTarget);

        productionTuneRule.executeActions(resultsDirectory, resultToProcess);
      }
    }
    catch (DatastoreException ex)
    {
      // Send a message to the Gui observable with the list of messages.
      _resultsProcessingObservable.notifyWithNewFailures(Collections.singletonList(ex));
    }

    // Execute all the user-defined rules that match the conditions of this inspection
    for (Rule rule : _rules)
    {
      if (rule.passesConditions(resultToProcess))
      {
        rule.executeActions(resultsDirectory, resultToProcess);
      }
    }

    // any and all errors have been added to this _failedOperationToExeceptionMap via addFailedOperation()
    if (_failedOperationToExceptionMap.isEmpty() == false)
    {
      _resultsProcessingObservable.notifyWithFailureMap(_failedOperationToExceptionMap);
    }
  }

  private Pattern _repairFileNamePattern = Pattern.compile("^.*\\.(jpg|xml|xsd)$", Pattern.CASE_INSENSITIVE);

  /**
   * Performs deletions of old inspection results.
   *
   * @author Patrick Lacz
   */
  private void enforceDeletionPolicy() throws DatastoreException
  {
    // proceed only if we're supposed to
    if (getUsesAutomaticResultDeletion() == false)
      return;

    if (_localResultsDeletionQueue == null)
    {
      initializeDeletionQueues();
    }

    Assert.expect(_repairFileNamePattern != null);
    Assert.expect(_localResultsDeletionQueue != null);
    Assert.expect(_localRepairFilesDeletionQueue != null);

    int numberOfResultsToKeep = getMaximumNumberOfResultsToKeep();
    int numberOfRepairResultsToKeep = getMaximumNumberOfRepairResultsToKeep();

    try
    {
      enforceDeletionOfResultDirectories(numberOfResultsToKeep);
      enforceDeletionOfRepairResults(numberOfRepairResultsToKeep);    
    }
    catch (DatastoreException de)
    {
      System.out.println("ResultsProcessor: enforceDeletionPolicy fail");
      System.out.println(de.getLocalizedMessage());
      
      // try again.. there might just be some strange concurrency issue.
      try
      {
        Thread.sleep(200);
      }
      catch (InterruptedException e)
      {
        // do nothing, we just wanted to wait a little bit for the filesystem to clear its locks.
      }
      // If the files are lock by other process, don't delete it. Keep the queue for next run.
      enforceDeletionOfResultDirectories(numberOfResultsToKeep);
      enforceDeletionOfRepairResults(numberOfRepairResultsToKeep);      
    }
  }
  
  /**
   * XCR-3328 Allow False Call Triggering to Remove indictment.xml
   * @param projectName Target recipe name that results
   * @param startInspectionTime Target result inspection time for deletion
   * @author Cheah Lee Herng
   */
  public void enforceFalseCallMonitoringDeletionPolicy(String projectName, long startInspectionTime)
  {
    Assert.expect(projectName != null);
    
    try
    {
      enforceDeletionOfResultsDirectories(projectName, startInspectionTime);
    }
    catch(DatastoreException de)
    {
      // Deletion error during results directory. We will wait a little while
      // before re-try again.
      try
      {
        Thread.sleep(200);
      }
      catch (InterruptedException e)
      {
        // do nothing, we just wanted to wait a little bit for the filesystem to clear its locks.
      }
      
      try
      {
        enforceDeletionOfResultsDirectories(projectName, startInspectionTime);
      }
      catch(DatastoreException dex)
      {
        // We really have problem delete results directories. Leave it for now.
      }
    }
  }
  
  /**
   * XCR-3328 Allow False Call Triggering to Remove indictment.xml
   * @author Cheah Lee Herng
   */
  private void enforceDeletionOfResultsDirectories(String projectName, long startInspectionTime) throws DatastoreException
  {
    Assert.expect(projectName != null);
    
    // Initialize results directory
    String resultsDirectory = Directory.getInspectionResultsDir(projectName, startInspectionTime);    
    
    _workWaitingLock.lock();
    try
    {
      synchronized (_localResultsDeletionQueue)
      {
        // Initialize empty placeholder
        Pair<Long, String> resultsToDelete = null;
        
        for(Pair<Long, String> item : _localResultsDeletionQueue)
        {
          if (item.getFirst() == startInspectionTime && item.getSecond().equals(resultsDirectory))
          {
            FileUtilAxi.deleteFileOrDirectoryByExternalProcessIfExists(resultsDirectory);
            
            // Flag to indicate we foudn the results
            resultsToDelete = item;
            break;
          }
        }
        
        if (resultsToDelete != null)
          _localResultsDeletionQueue.remove(resultsToDelete);
      }
      
      synchronized (_localRepairFilesDeletionQueue)
      {
        // Initialize empty placeholder
        Pair<Long, String> repairFilesToDelete = null;
        
        for(Pair<Long, String> item : _localRepairFilesDeletionQueue)
        {
          if (item.getFirst() == startInspectionTime && item.getSecond().equals(resultsDirectory))
          {
            // Flag to indicate we foudn the repair results
            repairFilesToDelete = item;
            break;
          }
        }
        
        if (repairFilesToDelete != null)
          _localRepairFilesDeletionQueue.remove(repairFilesToDelete);
      }
      
      _workWaitingForProcessingThread.signal();
    }
    finally
    {
      _workWaitingLock.unlock();
    }
  }

  /**
   * delete the oldest result directories
   * @author Patrick Lacz
   */
  private void enforceDeletionOfResultDirectories(int numberOfResultsToKeep) throws DatastoreException
  {
    while (_localResultsDeletionQueue.size() > numberOfResultsToKeep)
    {
      // synchronize around the deletion
      synchronized (_localResultsDeletionQueue)
      {
        // just peek - we don't want to delete the entry if there's some datastore failure
        Pair<Long, String> resultTimeAndDirectory = _localResultsDeletionQueue.peek();
        String resultDirectoryToDelete = resultTimeAndDirectory.getSecond();
        FileUtilAxi.deleteFileOrDirectoryByExternalProcessIfExists(resultDirectoryToDelete);
        // if we reach here, we have sucessfully deleted the results. remove the pair from the queue.
        _localResultsDeletionQueue.remove();
      }
    }
  }

  /**
   * delete the oldest repair data (presumably a more restrictive number than the whole directory)
   * @author Patrick Lacz
   */
  private void enforceDeletionOfRepairResults(int numberOfRepairResultsToKeep) throws DatastoreException
  {
    while (_localRepairFilesDeletionQueue.size() > numberOfRepairResultsToKeep)
    {
      // synchronize around the deletion
      synchronized (_localRepairFilesDeletionQueue)
      {
        // just peek - we don't want to delete the entry if there's some datastore failure
        Pair<Long, String> resultTimeAndDirectory = _localRepairFilesDeletionQueue.peek();
        String resultDirectoryWithRepairDataToDelete = resultTimeAndDirectory.getSecond();

        if (FileUtilAxi.exists(resultDirectoryWithRepairDataToDelete))
        {
          Collection<String> filesInInspectionResultDirectory = FileUtilAxi.listAllFilesFullPathInDirectory(resultDirectoryWithRepairDataToDelete);
          for (String sourceFileNameWithFullPath : filesInInspectionResultDirectory)
          {
            String fileName = FileUtil.getNameWithoutPath(sourceFileNameWithFullPath);
            Matcher matcher = _repairFileNamePattern.matcher(fileName);
            if (matcher.matches())
            {
              String fileNameWithPath = FileUtil.concatFileNameToPath(resultDirectoryWithRepairDataToDelete, fileName);
              if (FileUtilAxi.exists(fileNameWithPath))
                FileUtilAxi.delete(fileNameWithPath);
            }
          }
        }
        // if we reach here, we have sucessfully deleted the results. remove the pair from the queue.
        _localRepairFilesDeletionQueue.remove();
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  public boolean getUsesAutomaticResultDeletion()
  {
    return _deletionPolicyEnabled;
  }

  /**
   * @author Patrick Lacz
   */
  public int getMaximumNumberOfRepairResultsToKeep()
  {
    Assert.expect(_maximumNumberOfRepairResultsToKeep >= 0);
    return _maximumNumberOfRepairResultsToKeep;
  }

  /**
   * @author Patrick Lacz
   */
  public int getMaximumNumberOfResultsToKeep()
  {
    Assert.expect(_maximumNumberOfResultsToKeep >= 0);
    return _maximumNumberOfResultsToKeep;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean getGenerateAndProcessTestDevelopmentResultsForUnitTests()
  {
    return _config.getBooleanValue(SoftwareConfigEnum.GENERATE_AND_PROCESS_TEST_DEVELOPMENT_RESULTS);
  }

  /**
   * @author Patrick Lacz
   */
  public boolean isResultsProcessingEnabled()
  {
    return _resultsProcessingEnabled;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void setUsesAutomaticResultDeletion(boolean flag) throws DatastoreException
  {
    _deletionPolicyEnabled = flag;
    _config.setValue(SoftwareConfigEnum.AUTOMATICALLY_DELETE_RESULTS, flag);
    if (_localRepairFilesDeletionQueue != null)
    {
      _localRepairFilesDeletionQueue.clear();
      _localRepairFilesDeletionQueue = null;
    }
    if (_localResultsDeletionQueue != null)
    {
      _localResultsDeletionQueue.clear();
      _localResultsDeletionQueue = null;
    }
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void setMaximumNumberOfRepairResultsToKeep(int newMax) throws DatastoreException
  {
    Assert.expect(newMax >= 0);
    _maximumNumberOfRepairResultsToKeep = newMax;
    _config.setValue(SoftwareConfigEnum.KEEP_N_MOST_RECENT_REPAIR_FILE_RESULTS, newMax);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void setMaximumNumberOfResultsToKeep(int newMax) throws DatastoreException
  {
    Assert.expect(newMax >= 0);
    _maximumNumberOfResultsToKeep = newMax;
    _config.setValue(SoftwareConfigEnum.KEEP_N_MOST_RECENT_RESULT_FILES, newMax);
  }

  /**
   * @author Patrick Lacz
   */
  public void setIsResultsProcessingEnabled(boolean flag) throws DatastoreException
  {
    _resultsProcessingEnabled = flag;
     _config.setValue(SoftwareConfigEnum.ENABLE_RESULTS_PROCESSING, flag);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void setTestResultsProcessingEnabled(boolean enableTestMode) throws DatastoreException
  {
    _config.setValue(SoftwareConfigEnum.GENERATE_AND_PROCESS_TEST_DEVELOPMENT_RESULTS, enableTestMode);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void appendRule(Rule newRule)
  {
    Assert.expect(_rules != null);
    Assert.expect(newRule != null);

    _rules.add(newRule);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void insertRule(Rule newRule, int insertBeforeIndex)
  {
    Assert.expect(_rules != null);
    Assert.expect(newRule != null);
    if (insertBeforeIndex < 0)
      insertBeforeIndex = 0;
    if (insertBeforeIndex > _rules.size())
      insertBeforeIndex = _rules.size();

    _rules.add(insertBeforeIndex, newRule);
  }

  /**
   * Initializes the rule list with a deep copy of the provided rule list.
   * @author Patrick Lacz
   */
  public synchronized void setRuleList(List<Rule> inputRuleList)
  {
    Assert.expect(inputRuleList != null);
    Assert.expect(_rules != null);

    _rules.clear();
    for (Rule rule : inputRuleList)
      _rules.add(rule.createCopy(rule.getName()));
  }

  /**
   * Returns an unmodifiable list of all the rules.
   * @author Patrick Lacz
   */
  public synchronized List<Rule> getRuleList()
  {
    Assert.expect(_rules != null);
    return Collections.unmodifiableList(_rules);
  }

  /**
   * Makes a deep copy of the rule list.
   *
   * @author Patrick Lacz
   */
  public synchronized List<Rule> getCopyOfRuleList()
  {
    Assert.expect(_rules != null);
    List<Rule> newList = new ArrayList<Rule>(_rules.size());

    for ( Rule rule : _rules )
      newList.add(rule.createCopy(rule.getName()));

    return newList;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void removeRule(int ruleIndex)
  {
    Assert.expect(_rules != null);
    Assert.expect(ruleIndex >= 0 && ruleIndex < _rules.size());

    _rules.remove(ruleIndex);
  }

  /**
   * Package private. Only the subclasses of Target should use this method.
   * @author Patrick Lacz
   */
  void addFailedOperation(Target target, InspectionInformation inspectionInformation, DatastoreException ex)
  {
    Assert.expect(target != null);
    Assert.expect(inspectionInformation != null);
    Assert.expect(_failedOperationToExceptionMap != null);

    Pair<Target, InspectionInformation> operationKey = new Pair<Target, InspectionInformation>(target, inspectionInformation);

    if (_failedOperationToExceptionMap.containsKey(operationKey) == false)
    {
      _failedOperationToExceptionMap.put(operationKey, new LinkedList<DatastoreException>());
    }
    _failedOperationToExceptionMap.get(operationKey).add(ex);
  }

  /**
   * Attempt to copy all the previous failures
   * @author Patrick Lacz
   */
  private void retryFailedOperations()
  {
    Assert.expect(_failedOperationToExceptionMap != null);

    Map<Pair<Target, InspectionInformation>, List<DatastoreException>> oldFailureToExceptionMap = _failedOperationToExceptionMap;
    _failedOperationToExceptionMap = new HashMap<Pair<Target, InspectionInformation>, List<DatastoreException>>();

    List<DatastoreException> listOfAllFailuresDuringRetry = new LinkedList<DatastoreException>();

    for (Pair<Target, InspectionInformation> targetInspectionPair : oldFailureToExceptionMap.keySet())
    {
      Target target = targetInspectionPair.getFirst();
      InspectionInformation inspectionInformation = targetInspectionPair.getSecond();

      listOfAllFailuresDuringRetry.addAll(target.performCopy(inspectionInformation));
    }

    if (_failedOperationToExceptionMap.isEmpty() == false)
    {
      _resultsProcessingObservable.notifyWithFailureMap(_failedOperationToExceptionMap);
    }

    try
    {
      enforceDeletionPolicy();
    }
    catch (DatastoreException ex)
    {
      // Send a message to the Gui observable with the list of messages.
      _resultsProcessingObservable.notifyWithNewFailures(Collections.singletonList(ex));
    }
  }

  /**
   * @author Patrick Lacz
   *
   * This is not in software.config because of the way the user interacts with it in the ResultsProcessing Configuration Gui,
   * and perhaps more importantly, so that environment variables can be used in the path. Paths in software.config, I have been told,
   * can not allow environment variable expansions.
   */
  public synchronized void setRepairToolDirectory(String path)
  {
    Assert.expect(path != null);
    _repairToolDirectory = path;
    if (path.length() == 0)
      _repairToolRule = null;
    _repairToolRule = createRuleForRepairTool(path);
    ConfigObservable.getInstance().stateChanged(this);
  }

  /**
   * @author Patrick Lacz
   */
  public boolean getIsRepairToolRuleEnabled()
  {
    return _repairToolRuleIsEnabled;
  }

  /**
   * @author Patrick Lacz
   */
  public void setIsRepairToolRuleEnabled(boolean isEnabled)
  {
    _repairToolRuleIsEnabled = isEnabled;
    ConfigObservable.getInstance().stateChanged(_repairToolRule);
  }

  /**
   * @author Patrick Lacz
   */
  public String getRepairToolDirectory()
  {
    Assert.expect(_repairToolDirectory != null);
    return _repairToolDirectory;
  }

  /**
   * @author Patrick Lacz
   */
  public Rule createRuleForRepairTool(String repairToolDirectory)
  {
    Assert.expect(repairToolDirectory != null);

    Rule repairToolRule = new Rule();
    repairToolRule.setName(StringLocalizer.keyToString("TESTEXEC_MRT_RULE_NAME_KEY"));

    List<String> pathList = new ArrayList<String>();
    pathList.add(repairToolDirectory + File.separator);
    Target cadTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.XML_CAD_FILE, pathList);

    pathList.clear();
    String inspectionResultsTargetDirectory = FileUtil.concatFileNameToPath(repairToolDirectory,
        "inspectionRun_$(YYYY)-$(MM)-$(DD)_$(hh)-$(mm)-$(ss)-$(ms)") + File.separator;
    pathList.add(inspectionResultsTargetDirectory);
    Target imagesTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.DEFECT_IMAGE_FILES, pathList);

    pathList.clear();
    pathList.add(FileUtil.concatFileNameToPath(repairToolDirectory, "inspectionRun_$(YYYY)-$(MM)-$(DD)_$(hh)-$(mm)-$(ss)-$(ms).result.xml"));
    Target indictmentsTarget = ResultsProcessor.createTarget(ResultFileTypeEnum.XML_INDICTMENTS_FILE_FOR_MRT, new ArrayList<String>(pathList));

    repairToolRule.addTarget(imagesTarget);
    repairToolRule.addTarget(cadTarget);
    repairToolRule.addTarget(indictmentsTarget);

    return repairToolRule;
  }

  /**
   * @return
   * @author Wei Chin
   */
  public static boolean isResultProcessDone()
  {
    return _resultIsProcessDone;
  }
}
