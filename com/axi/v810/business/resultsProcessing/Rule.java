package com.axi.v810.business.resultsProcessing;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Patrick Lacz
 */
public class Rule
{
  // The regular expression that matches symbols in destination patterns.
  // either $WORD, $(WORD), or %WORD%
  // windows allows dashes in environment variables, so we allow them in the $() and %% notations
  private static Pattern _replacementSymbolPattern = Pattern.compile("(?:\\$(\\w+)|\\$\\(([\\w-]+)\\)|%([\\w-]+)%)");

  // A rule may have a name; it has no effect on operation.
  private String _name = "Unnamed Rule";
  private boolean _isEnabled = true;
  private Map<ConditionKeyEnum, Condition> _inputToConditionMap = new HashMap<ConditionKeyEnum, Condition>();

  // This needs to be a tree map so that the transfer order of the result file types is enforced.
  // in-particular, the indictments.xml file must be the final file transferred to the MTSS Agent directory.
  private Map<ResultFileTypeEnum, Target> _targetMap = new TreeMap<ResultFileTypeEnum,Target>();

  /**
   * @author Patrick Lacz
   */
  public Rule()
  {
    // do nothing
  }

  /**
   * @author Patrick Lacz
   */
  public Rule createCopy(String newName)
  {
    Assert.expect(_inputToConditionMap != null);

    Rule newRule = new Rule();

    // copy the name and comment (todo: change the name?)
    newRule.setName(newName);

    // copy the enabled state
    newRule.setIsEnabled(_isEnabled);

    // copy the conditions
    for (Condition condition : _inputToConditionMap.values())
    {
      Condition newCondition = condition.createCopy();
      newRule.addCondition(newCondition);
    }

    // copy the targets
    for (Target target : _targetMap.values())
    {
      Target newTarget = ResultsProcessor.createTarget(target.getResultFileTypeEnum(), target.getDestinationList());
      newRule.addTarget(newTarget);
    }
    return newRule;
  }

  /**
   * Returns if the given data passes all the conditions for this rule.
   * If no conditions exist for this rule, all conditions match.
   *
   * @return True if all the conditions pass.
   * @author Patrick Lacz
   */
  public boolean passesConditions(InspectionInformation inspectionInformation)
  {
    Assert.expect(inspectionInformation != null);
    Assert.expect(_inputToConditionMap != null);

    if (_isEnabled == false)
      return false;

    for (Condition keyCondition : _inputToConditionMap.values())
    {
      if (keyCondition.passes(inspectionInformation) == false)
        return false;
    }

    return true;
  }

  /**
   * @author Patrick Lacz
   */
  public void executeActions(String resultsDirectory, InspectionInformation inspectionInformation)
  {
    Assert.expect(_targetMap != null);
    Assert.expect(inspectionInformation != null);
    Assert.expect(resultsDirectory != null);

    for (Target target : _targetMap.values())
    {
      target.performCopy(inspectionInformation);
    }
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasConditionForKey(ConditionKeyEnum key)
  {
    Assert.expect(_inputToConditionMap != null);
    Assert.expect(key != null);

    return _inputToConditionMap.containsKey(key);
  }

  /**
   * @author Patrick Lacz
   */
  public Condition getConditionForKey(ConditionKeyEnum key)
  {
    Assert.expect(_inputToConditionMap != null);
    Assert.expect(key != null);

    Condition condition = _inputToConditionMap.get(key);
    Assert.expect(condition != null);

    return condition;
  }

  /**
   * @author Patrick Lacz
   */
  public void addCondition(Condition condition)
  {
    Assert.expect(condition != null);
    Assert.expect(_inputToConditionMap != null);

    ConditionKeyEnum conditionKeyEnum = condition.getConditionKeyEnum();
    Assert.expect(hasConditionForKey(conditionKeyEnum) == false);
    _inputToConditionMap.put(conditionKeyEnum, condition);
  }

  /**
   * @author Patrick Lacz
   */
  public void removeConditionForKey(ConditionKeyEnum conditionKeyEnum)
  {
    Assert.expect(conditionKeyEnum != null);
    Assert.expect(_inputToConditionMap != null);

    if (hasConditionForKey(conditionKeyEnum) == false)
      return;

    _inputToConditionMap.remove(conditionKeyEnum);
  }

  /**
   * @author Laura Cormos
   */
  public void removeAllConditions()
  {
    Assert.expect(_inputToConditionMap != null);

    _inputToConditionMap.clear();
  }

  /**
   * @author Patrick Lacz
   */
  public String getName()
  {
    Assert.expect(_name != null);
    return _name;
  }

  /**
   * @author Patrick Lacz
   */
  public void setName(String newName)
  {
    Assert.expect(newName != null);
    _name = newName;
    ConfigObservable.getInstance().stateChanged(this);
  }

  /**
   * @author Patrick Lacz
   */
  public boolean getIsEnabled()
  {
    return _isEnabled;
  }

  /**
   * @author Patrick Lacz
   */
  public void setIsEnabled(boolean isEnabled)
  {
    _isEnabled = isEnabled;
    ConfigObservable.getInstance().stateChanged(this);
  }
  /**
   * @author Patrick Lacz
   */
  public Collection<Condition> getConditions()
  {
    Assert.expect(_inputToConditionMap != null);

    return _inputToConditionMap.values();
  }

  /**
   * Replaces any existing target with the same ResultFileType.
   * @author Patrick Lacz
   */
  public void addTarget(Target target)
  {
    Assert.expect(target != null);
    Assert.expect(_targetMap != null);

    ResultFileTypeEnum resultFileTypeEnum = target.getResultFileTypeEnum();

    _targetMap.put(resultFileTypeEnum, target);
    target.setParentRule(this);
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasTargetForResultFileType(ResultFileTypeEnum resultFileTypeEnum)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(_targetMap != null);

    return _targetMap.containsKey(resultFileTypeEnum);
  }

  /**
   * @author Patrick Lacz
   */
  public Target getTargetForResultFileType(ResultFileTypeEnum resultFileTypeEnum)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(_targetMap != null);

    Target target = _targetMap.get(resultFileTypeEnum);
    Assert.expect(target != null);
    return target;
  }

  /**
   * @author Patrick Lacz
   */
  public void removeTargetForResultFileType(ResultFileTypeEnum resultFileTypeEnum)
  {
    Assert.expect(_targetMap != null);
    Assert.expect(resultFileTypeEnum != null);

    _targetMap.remove(resultFileTypeEnum);
  }


  /**
   * @author Patrick Lacz
   */
  public Collection<Target> getTargets()
  {
    Assert.expect(_targetMap != null);

    return _targetMap.values();
  }

  /**
   * Get a list of all the directories used in the targets of this rule. This is used by the gui to
   * provide a convient collection of paths to work with.
   *
   * @author Patrick Lacz
   */
  public Collection<String> getDirectoriesUsedInTargets()
  {
    Assert.expect(_targetMap != null);

    Set<String> directoriesUsed = new TreeSet<String>();

    for (Target target : _targetMap.values())
    {
      directoriesUsed.addAll(target.getDestinationList());
    }

    return directoriesUsed;
  }

  /**
   * This method is package-private so that it can be throughly unit tested.
   * @author Patrick Lacz
   */
  static String replaceSymbolsInDestinationPath(String destinationPath, InspectionInformation inspectionInformation)
  {
    Assert.expect(destinationPath != null);
    Assert.expect(inspectionInformation != null);
    Assert.expect(_replacementSymbolPattern != null);

    // iterate over all the substrings that match our substitution pattern
    Matcher replacementSymbolMatcher = _replacementSymbolPattern.matcher(destinationPath);
    String newPathString = new String("");
    int copiedUpToIndex = 0;
    while (replacementSymbolMatcher.find())
    {
      int startIndex = replacementSymbolMatcher.start();

      newPathString += destinationPath.substring(copiedUpToIndex, startIndex);

      Assert.expect(replacementSymbolMatcher.groupCount() == 3);

      // start with the symbol from the $WORD pattern
      String matchedSymbol = replacementSymbolMatcher.group(1);

      // if that didn't work, use the $(WORD) pattern
      if (matchedSymbol == null)
        matchedSymbol = replacementSymbolMatcher.group(2);

      // finally, try the %WORD% pattern
      if (matchedSymbol == null)
        matchedSymbol = replacementSymbolMatcher.group(3);

      // replace the symbol with the data it represents
      String replacementString = getReplacementString(matchedSymbol, inspectionInformation);
      newPathString += replacementString;

      copiedUpToIndex = replacementSymbolMatcher.end();
    }

    if (copiedUpToIndex < destinationPath.length())
      newPathString += destinationPath.substring(copiedUpToIndex);

    // remove double slashes.
    String finalPathString = replaceSlashesWithSingleSlash(newPathString);
    return finalPathString;
  }

  private static Pattern _multislashPattern = Pattern.compile("[\\\\/][\\\\/]+");
  /**
   * @author Patrick Lacz
   */
  private static String replaceSlashesWithSingleSlash(String pathString)
  {
    Assert.expect(pathString != null);
    String pathStringWithoutMultipleSlashes = "";
    int copiedUpToIndex = 0;
    Matcher multislashMatcher = _multislashPattern.matcher(pathString);

    // we don't just do a replace all because we don't want to replace a string that starts with "\\"
    // eg. \\networkmachine\sharename\subdir will be supported.
    /** @todo PWL Is "\\\\\machinename\..." a problem? */
    while (multislashMatcher.find())
    {
      int startIndex = multislashMatcher.start();
      if (startIndex == 0)
        continue;

      pathStringWithoutMultipleSlashes += pathString.substring(copiedUpToIndex, startIndex);
      pathStringWithoutMultipleSlashes += File.separator;
      copiedUpToIndex = multislashMatcher.end();
    }
    if (copiedUpToIndex < pathString.length())
      pathStringWithoutMultipleSlashes += pathString.substring(copiedUpToIndex);

    return pathStringWithoutMultipleSlashes;
  }

  private static Pattern _illegalCharactersString = Pattern.compile("[\\/:*\"<>|?]");
  /**
   * @author Patrick Lacz
   */
  private static String replaceIllegalCharactersForFileSystem(String pathString)
  {
    Assert.expect(pathString != null);

    Matcher illegalCharacterMatcher = _illegalCharactersString.matcher(pathString);
    String returnString = illegalCharacterMatcher.replaceAll("_");
    return returnString;
  }

  private static GregorianCalendar _calendar = new GregorianCalendar();
  private static Pattern _boardSerialNumberPattern = Pattern.compile("^board_(\\d+)_serial_number$", Pattern.CASE_INSENSITIVE);
  private static Pattern _boardNamePattern = Pattern.compile("^board_(\\d+)_name$", Pattern.CASE_INSENSITIVE);

  /**
   * supported symbols:
   * date/time of inspection
   * $DD   Day, eg. 02 or 14
   * $MM   Month, eg. 01 or 12
   * $YY   Year, eg. 06
   * $YYYY Year, eg. 2006
   * $hh   Hour, eg 02 or 20
   * $mm   Minute, eg 05 or 54
   * $ss   Second, eg 02 or 42
   *
   * $TIMESTAMP Is a combination of the above: $(YYYY)-$(MM)-$(DD)_$(hh)-$(mm)-$(ss)-$(ms)
   * $PROJECT_NAME  Name of the project
   * $PANEL_NAME
   * $SERIAL_NUMBER either the panel serial number or the first board serial number
   * $CAD_CHECKSUM the cad file checksum
   * $SETTINGS_CHECKSUM the settings file checksum
   * $PANEL_SERIAL_NUMBER
   * $BOARD_n_NAME
   * $BOARD_n_SERIAL_NUMBER
   *
   * Environment variables.
   *
   * @author Patrick Lacz
   */
  static String getReplacementString(String pattern, InspectionInformation inspectionInformation)
  {
    Assert.expect(pattern != null);
    Assert.expect(inspectionInformation != null);
    Assert.expect(_calendar != null);
    Assert.expect(_boardSerialNumberPattern != null);
    Assert.expect(_boardNamePattern != null);

    String replacementString = "";
    Project project = inspectionInformation.getTestProgram().getProject();

    NumberFormat limitTwoDigits = NumberFormat.getNumberInstance(Locale.US);
    limitTwoDigits.setMinimumIntegerDigits(2);
    NumberFormat limitThreeDigits = NumberFormat.getNumberInstance(Locale.US);
    limitThreeDigits.setMinimumIntegerDigits(3);

    _calendar.setTimeInMillis(inspectionInformation.getInspectionStartTime());

    if (pattern.equals("YYYY"))
      replacementString = Integer.toString(_calendar.get( Calendar.YEAR ));
    else if (pattern.equals("YY"))
    {
      replacementString = Integer.toString(_calendar.get( Calendar.YEAR ));
      replacementString = replacementString.substring(replacementString.length() - 2);
      Assert.expect(replacementString.length() == 2);
    }
    else if (pattern.equals("MM"))
      replacementString = limitTwoDigits.format(_calendar.get( Calendar.MONTH ) + 1);
    else if (pattern.equals("DD"))
      replacementString = limitTwoDigits.format(_calendar.get( Calendar.DAY_OF_MONTH ));
    else if (pattern.equals("hh"))
      replacementString = limitTwoDigits.format(_calendar.get( Calendar.HOUR_OF_DAY ));
    else if (pattern.equals("mm"))
      replacementString = limitTwoDigits.format(_calendar.get( Calendar.MINUTE ));
    else if (pattern.equals("ss"))
      replacementString = limitTwoDigits.format( _calendar.get( Calendar.SECOND ));
    else if (pattern.equals("ms"))
      replacementString = limitThreeDigits.format(_calendar.get(Calendar.MILLISECOND));
    else if (pattern.equalsIgnoreCase("TIMESTAMP"))
    {
      replacementString = Integer.toString(_calendar.get( Calendar.YEAR )) + "-" +
                          limitTwoDigits.format(_calendar.get( Calendar.MONTH ) + 1) + "-" +
                          limitTwoDigits.format(_calendar.get( Calendar.DAY_OF_MONTH )) + "_" +
                          limitTwoDigits.format(_calendar.get( Calendar.HOUR_OF_DAY )) + "-" +
                          limitTwoDigits.format(_calendar.get( Calendar.MINUTE )) + "-" +
                          limitTwoDigits.format( _calendar.get( Calendar.SECOND )) + "-" +
                          limitThreeDigits.format(_calendar.get(Calendar.MILLISECOND));
    }

    else if (pattern.equalsIgnoreCase("PROJECT_NAME"))
      replacementString = replaceIllegalCharactersForFileSystem(project.getName());
    else if (pattern.equalsIgnoreCase("PANEL_SERIAL_NUMBER"))
      replacementString = replaceIllegalCharactersForFileSystem(inspectionInformation.getPanelSerialNumber());
    else if (pattern.equalsIgnoreCase("CAD_CHECKSUM"))
      replacementString = Long.toString(project.getCadXmlChecksum());
    else if (pattern.equalsIgnoreCase("SETTINGS_CHECKSUM"))
      replacementString = Long.toString(project.getSettingsXmlChecksum());
    else if (pattern.equalsIgnoreCase("SERIAL_NUMBER"))
    {
      replacementString = "";
      // look for the first valid serial number
      replacementString = inspectionInformation.getPanelSerialNumber();
      // @todo ? get the matching serial number?
      if (replacementString.length() == 0)
      {
        Map<String, String> boardNameToSerialNumberMap = inspectionInformation.getBoardNameToBoardSerialNumberMap();
        // get the first serial number
        for (Board board : project.getPanel().getBoards())
        {
          replacementString = boardNameToSerialNumberMap.get(board.getName());
          if (replacementString != null && replacementString.length() > 0)
            break;
        }
        if (replacementString == null)
          replacementString = "";
        replacementString = replaceIllegalCharactersForFileSystem(replacementString);
      }
    }
    else
    {
      // BOARD_n_SERIAL_NUMBER
      Matcher boardSerialNumberMatcher = _boardSerialNumberPattern.matcher(pattern);
      Assert.expect(boardSerialNumberMatcher.groupCount() == 1);
      if (boardSerialNumberMatcher.matches())
      {
        int whichBoard  = Integer.parseInt(boardSerialNumberMatcher.group(1));
        List<Board> boardList = project.getPanel().getBoards();
        if (whichBoard >= 1 && whichBoard <= boardList.size()) // 1-based index
        {
          String boardName = boardList.get(whichBoard-1).getName();
          if (inspectionInformation.hasBoardSerialNumberForBoardName(boardName))
            replacementString = inspectionInformation.getBoardSerialNumberFromBoardName(boardName);
          else
            replacementString = "";
        }
        replacementString = replaceIllegalCharactersForFileSystem(replacementString);
      }
      // BOARD_n_NAME
      Matcher boardNameMatcher = _boardNamePattern.matcher(pattern);
      Assert.expect(boardNameMatcher.groupCount() == 1);
      if (boardNameMatcher.matches())
      {
        int whichBoard = Integer.parseInt(boardNameMatcher.group(1));
        List<Board> boardList = project.getPanel().getBoards();
        if (whichBoard >= 1 && whichBoard <= boardList.size()) // 1-based index
          replacementString = boardList.get(whichBoard-1).getName();
      }
      replacementString = replaceIllegalCharactersForFileSystem(replacementString);
    }
    if (replacementString.length() == 0)
    {
      // still no match, try looking at environment variables
      String environmentVariableValue = System.getenv(pattern);
      if (environmentVariableValue != null)
        replacementString = environmentVariableValue;
    }

    return replacementString;
  }
}
