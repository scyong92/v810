package com.axi.v810.business.resultsProcessing;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;

/**
 * @author Patrick Lacz
 */
public class Test_Condition extends UnitTest
{

  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Condition());
  }

  /**
   * @author Patrick Lacz
   */
  public Test_Condition()
  {
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testFailures();
    testSinglePattern();
    testMultiplePatterns();
  }

  /**
   * @author Patrick Lacz
   */
  private void testMultiplePatterns()
  {
    /** @todo wpd - Patrick */
//    List<String> multipleSimplePatternList = new ArrayList<String>();
//
//    multipleSimplePatternList.add("a?b,a*bb");
//    Condition testSimpleCondition = null;
//    try
//    {p
//      testSimpleCondition = ResultsProcessor.createCondition(ConditionKeyEnum.PANEL_NAME,
//          Condition.SIMPLE_TYPE_IDENTIFIER, multipleSimplePatternList);
//    }
//    catch (DatastoreException ex)
//    {
//      Expect.expect(false, ex.getLocalizedMessage());
//    }
//
//    Project project = new Project(false);
//    Panel testPanel = new Panel();
//    testPanel.setName("some_random_name");
//    project.setPanel(testPanel);
//
//    TestProgram testProgram = new TestProgram();
//    testProgram.setProject(project);
//
//    InspectionInformation inspectionInformation = new InspectionInformation(testProgram, 100, "", new TreeMap<String, String>());
//
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("a");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("ab");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("axb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("axxb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("axbb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("axxxbb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("axxxcb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("bxxxcb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName(",abb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("abb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//
//
//    multipleSimplePatternList.clear();
//    multipleSimplePatternList.add("xxx,a?b");
//    multipleSimplePatternList.add("a*bb");
//    try
//    {
//      testSimpleCondition = ResultsProcessor.createCondition(ConditionKeyEnum.PANEL_NAME,
//          Condition.SIMPLE_TYPE_IDENTIFIER, multipleSimplePatternList);
//    }
//    catch (DatastoreException ex1)
//    {
//      Expect.expect(false, ex1.getLocalizedMessage());
//    }
//
//    testPanel.setName("some_random_name");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("a");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("ab");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("axb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("axxb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("axbb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("aXBb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("axxxbb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("AXXXBB");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("axxxcb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("bxxxcb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName(",abb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == false);
//    testPanel.setName("abb");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//    testPanel.setName("xXx");
//    Expect.expect(testSimpleCondition.passes(inspectionInformation) == true);
//
//    List<String> multipleRegExPatternList = new ArrayList<String>();
//    multipleRegExPatternList.add("123x*456[zxy][^e]");
//    multipleRegExPatternList.add("123x*456yester(day|year)");
//    Condition testRegExCondition = null;
//    try
//    {
//      testRegExCondition = ResultsProcessor.createCondition(ConditionKeyEnum.PANEL_NAME,
//          Condition.REGEX_TYPE_IDENTIFIER, multipleRegExPatternList);
//    }
//    catch (DatastoreException ex2)
//    {
//      Expect.expect(false, ex2.getLocalizedMessage());
//    }
//
//    testPanel.setName("some_random_name");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == false);
//    testPanel.setName("123456z");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == false);
//    testPanel.setName("123456zf");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == true);
//    testPanel.setName("123456z1extratext");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == false);
//    testPanel.setName("123xx456x");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == false);
//    testPanel.setName("123xx456xesterday");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == false);
//    testPanel.setName("123xx456yesterday");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == true);
//    testPanel.setName("123456yesteryear");
//    Expect.expect(testRegExCondition.passes(inspectionInformation) == true);

  }

  /**
   * @author Patrick Lacz
   */
  private void testSinglePattern()
  {
    List<String> patternList = new ArrayList<String>();
    patternList.add("a*");
    Condition testSimpleCondition = null;
    Condition testRegExCondition = null;
    /** @todo wpd - Patrick */
//    try
//    {
//      testSimpleCondition = ResultsProcessor.createCondition(ConditionKeyEnum.PANEL_NAME,
//          Condition.SIMPLE_TYPE_IDENTIFIER, patternList);
//      testRegExCondition = ResultsProcessor.createCondition(ConditionKeyEnum.PANEL_NAME,
//          Condition.REGEX_TYPE_IDENTIFIER, patternList);
//    }
//    catch (DatastoreException ex)
//    {
//      Expect.expect(false, ex.getLocalizedMessage());
//    }
//
//    Expect.expect(Condition.SIMPLE_TYPE_IDENTIFIER.equals(testSimpleCondition.getExpressionType()));
//    Expect.expect(Condition.REGEX_TYPE_IDENTIFIER.equals(testRegExCondition.getExpressionType()));
//
//    Expect.expect(testSimpleCondition.getConditionKeyEnum().equals(ConditionKeyEnum.PANEL_NAME));
//    Expect.expect(testRegExCondition.getConditionKeyEnum().equals(ConditionKeyEnum.PANEL_NAME));
//
//    Expect.expect(testSimpleCondition.getDefintionExpressionList().size() == 1);
//    Expect.expect(testRegExCondition.getDefintionExpressionList().size() == 1);
//
//    Expect.expect(testSimpleCondition.isDefinedUsingRegularExpression() == false);
//    Expect.expect(testSimpleCondition.isDefinedUsingSimpleExpression() == true);
//    Expect.expect(testRegExCondition.isDefinedUsingRegularExpression() == true);
//    Expect.expect(testRegExCondition.isDefinedUsingSimpleExpression() == false);
//
//    Project project = new Project(false);
//    Panel testPanel = new Panel();
//    testPanel.setName("some_random_name");
//    project.setPanel(testPanel);
//
//    TestProgram testProgram = new TestProgram();
//    testProgram.setProject(project);
//
//    InspectionInformation inspectionInformation = new InspectionInformation(testProgram, 100, "", new TreeMap<String, String>());
//
//    boolean result;
//    result = testSimpleCondition.passes(inspectionInformation);
//    Expect.expect(result == false);
//    result = testRegExCondition.passes(inspectionInformation);
//    Expect.expect(result == false);
//
//    project.getPanel().setName("asdf");
//    result = testSimpleCondition.passes(inspectionInformation);
//    Expect.expect(result == true);
//    result = testRegExCondition.passes(inspectionInformation);
//    Expect.expect(result == false);
//
//    project.getPanel().setName("a");
//    result = testSimpleCondition.passes(inspectionInformation);
//    Expect.expect(result == true);
//    result = testRegExCondition.passes(inspectionInformation);
//    Expect.expect(result == true);
//
//    project.getPanel().setName("aaaaaa");
//    result = testSimpleCondition.passes(inspectionInformation);
//    Expect.expect(result == true);
//    result = testRegExCondition.passes(inspectionInformation);
//    Expect.expect(result == true);
//
//    project.getPanel().setName("AaaAaa");
//    result = testSimpleCondition.passes(inspectionInformation);
//    Expect.expect(result == true);
//    result = testRegExCondition.passes(inspectionInformation);
//    Expect.expect(result == true); // case insensitive
  }

  /**
   * @author Patrick Lacz
   */
  private void testFailures()
  {
    Condition condition = new Condition();

    // condtion must have 'initialized' called before going further
    try
    {
      condition.createCopy();
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing, expected
    }

    try
    {
      condition.getDefintionExpressionList();
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // do nothing, expected
    }

    /** @todo wpd - Patrick */
//    List<String> patternList = new ArrayList<String>();
//    try
//    {
//      ResultsProcessor.createCondition(ConditionKeyEnum.PANEL_NAME, Condition.SIMPLE_TYPE_IDENTIFIER,
//                                       patternList);
//      Expect.expect(false);
//    }
//    catch (DatastoreException ex)
//    {
//      // not expected, complain
//      Expect.expect(false, ex.getLocalizedMessage());
//    }
//    catch (AssertException ae)
//    {
//      // expected, do nothing
//    }
  }


  /**
   * @author Patrick Lacz
   */
  protected Project getProject(String projectName)
  {
    Assert.expect(projectName != null);

    Project project = null;
    try
    {
      BooleanRef abortedDuringLoad = new BooleanRef();
      project = Project.load(projectName, abortedDuringLoad);
      Assert.expect(abortedDuringLoad.getValue() == false);
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false);
    }

    return project;
  }

}
