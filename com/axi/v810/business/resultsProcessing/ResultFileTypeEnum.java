package com.axi.v810.business.resultsProcessing;

import com.axi.util.*;
import java.util.List;

/**
 * The order of the enums in this file is important; this is the order in which the files will be transferred.
 * MTSS has the requirement that the xml indictments file is the final file that is placed into the target directory.
 *
 * @author Patrick Lacz
 */
public class ResultFileTypeEnum extends com.axi.util.Enum
{
  private static int _index = 0;

  // ./results/<proj_name>/inspectionRun_<timestamp>/*.jpg
  public static ResultFileTypeEnum DEFECT_IMAGE_FILES = new ResultFileTypeEnum(_index++, MultipleFileTarget.class);

  // for internal use only; the zip file tyes will not include this binary.
  // ./results/<proj_name>/inspectionRun_<timestamp>/<proj_name>.results
  public static ResultFileTypeEnum INTERNAL_RESULTS_BINARY_FILE = new ResultFileTypeEnum(_index++, SingleFileTarget.class);

  // for internal use only;
  // ./temp/onlineXrayImages/<timestamp>/
  public static ResultFileTypeEnum INTERNAL_RESULTS_IMAGE_SET_FILES = new ResultFileTypeEnum(_index++, MultipleFileTarget.class);

  // ./results/<proj_name>/cad.<latest_checksum>.xml
  public static ResultFileTypeEnum XML_CAD_FILE = new ResultFileTypeEnum(_index++, XmlFileTarget.class);

  // ./results/<proj_name>/setting.<latest_checksum>.xml
  public static ResultFileTypeEnum XML_SETTINGS_FILE = new ResultFileTypeEnum(_index++, XmlFileTarget.class);

  // ./results/<proj_name>/inspectionRun_<timestamp>/<proj_name>.indictments.xml
  public static ResultFileTypeEnum XML_INDICTMENTS_FILE = new ResultFileTypeEnum(_index++, XmlFileTarget.class);

  // THE XML INDICTMENTS FILE MUST BE THE FINAL TYPE - IT MUST BE COPIED LAST FOR MTSS TO BE HAPPY
  // MTSS requires a 'special' xsd file (temporarily, hopefully)
  public static ResultFileTypeEnum XML_INDICTMENTS_FILE_FOR_MRT = new ResultFileTypeEnum(_index++, XmlFileTarget.class);

  // Janan - XCR-3836 Measurement XML able to transfer to other location
  public static ResultFileTypeEnum XML_MEASUREMENTS_FILE = new ResultFileTypeEnum(_index++, XmlFileTarget.class);
  
  private Class<? extends Target> _targetImplementationClass = null;

  /**
   * @author Patrick Lacz
   */
  public ResultFileTypeEnum(int index, Class<? extends Target> implementationClass)
  {
    super(index);
    Assert.expect(implementationClass != null);
    _targetImplementationClass = implementationClass;
  }

  /**
   * @author Patrick Lacz
   */
  public Class<? extends Target> getTargetImplementationClass()
  {
    Assert.expect(_targetImplementationClass != null);
    return _targetImplementationClass;
  }

  /**
   * @author Patrick Lacz
   */
  public static Target createTarget(ResultFileTypeEnum resultFileTypeEnum, List<String> destinationList)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(destinationList != null);

    Target newTarget = null;
    try
    {
      if (resultFileTypeEnum.getTargetImplementationClass().equals(SingleFileTarget.class))
      {
        newTarget = new SingleFileTarget();
        newTarget.initialize(resultFileTypeEnum, destinationList);
      }
      else if (resultFileTypeEnum.getTargetImplementationClass().equals(MultipleFileTarget.class))
      {
        newTarget = new MultipleFileTarget();
        newTarget.initialize(resultFileTypeEnum, destinationList);
      }
      else if (resultFileTypeEnum.getTargetImplementationClass().equals(XmlFileTarget.class))
      {
        newTarget = new XmlFileTarget();
        newTarget.initialize(resultFileTypeEnum, destinationList);
      }

      else
      {
        Assert.expect(false);
      }
    }
    catch (Exception ex)
    {
      Assert.expect(false);
    }
    return newTarget;
  }

}
