package com.axi.v810.business.resultsProcessing;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * This enumeration represents the possible fields that the Results Processor Conditions may key off of (match against)
 * in order to determine if the rule applies or not.
 *
 * @author Patrick Lacz
 */
public class ConditionKeyEnum extends com.axi.util.Enum
{
  private static int _index = -1;
  private static Map<String, ConditionKeyEnum> _nameToConditionKeyEnumMap = new HashMap<String, ConditionKeyEnum>();

  public static final ConditionKeyEnum PROJECT_NAME = new ConditionKeyEnum(++_index, Condition.class,
      StringLocalizer.keyToString("CFGUI_PROJECT_NAME_CONDITION_KEY"));
  public static final ConditionKeyEnum USER_NAME = new ConditionKeyEnum(++_index, Condition.class,
      StringLocalizer.keyToString("CFGUI_USER_NAME_CONDITION_KEY"));
  public static final ConditionKeyEnum USER_ACCOUNT_TYPE = new ConditionKeyEnum(++_index, Condition.class,
      StringLocalizer.keyToString("CFGUI_USER_ACCOUNT_TYPE_CONDITION_KEY"));

  public static final ConditionKeyEnum ANY_SERIAL_NUMBER = new ConditionKeyEnum(++_index, SerialNumberCondition.class,
      StringLocalizer.keyToString("CFGUI_SERIAL_NUMBER_CONDITION_KEY"));
  // the following are not in the requirements, but were easy to add
  public static final ConditionKeyEnum PROGRAMMER_NAME = new ConditionKeyEnum(++_index, Condition.class,
      StringLocalizer.keyToString("CFGUI_PROGAMMER_NAME_CONDITION_KEY"));
  public static final ConditionKeyEnum TARGET_CUSTOMER = new ConditionKeyEnum(++_index, Condition.class,
      StringLocalizer.keyToString("CFGUI_CUSTOMER_NAME_CONDITION_KEY"));

  private Class<? extends Condition> _implementationClass = null;
  private String _name;

  /**
   * @author Patrick Lacz
   */
  private ConditionKeyEnum(int id,Class<? extends Condition> implementationClass, String name)
  {
    super(id);
    Assert.expect(name != null);
    Assert.expect(implementationClass != null);

    _implementationClass = implementationClass;
    _name = name;
    ConditionKeyEnum conditionKeyEnum = _nameToConditionKeyEnumMap.put(name, this);
    Assert.expect(conditionKeyEnum == null);
  }

  /**
   * Get all the Enums.
   *
   * @author Laura Cormos
   */
  public static Collection<ConditionKeyEnum> getAllEnums()
  {
    Assert.expect(_nameToConditionKeyEnumMap != null);
    return _nameToConditionKeyEnumMap.values();
  }

  /**
   * @author Laura Cormos
   */
  public String toString()
  {
    Assert.expect(_name != null);
    return _name;
  }

  /**
   * @author Patrick Lacz
   */
  public Class<? extends Condition> getImplementationClass()
  {
    Assert.expect(_implementationClass != null);
    return _implementationClass;
  }

  /**
   * @author Patrick Lacz
   */
  public static Condition createConditionInstance(ConditionKeyEnum conditionKeyEnum, List<String> patternStringList, Pattern compiledRegExPattern, String expressionTypeName)
  {
    Assert.expect(conditionKeyEnum != null);
    Class<? extends Condition> implementationClass = conditionKeyEnum.getImplementationClass();

    Condition newCondition = null;
    if (implementationClass.equals(Condition.class))
    {
      newCondition = new Condition();
      newCondition.initialize(conditionKeyEnum, patternStringList, compiledRegExPattern, expressionTypeName);
    }
    else if (implementationClass.equals(SerialNumberCondition.class))
    {
      newCondition = new SerialNumberCondition();
      newCondition.initialize(conditionKeyEnum, patternStringList, compiledRegExPattern, expressionTypeName);
    }
    Assert.expect(newCondition != null);
    return newCondition;
  }
}
