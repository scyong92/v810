package com.axi.v810.business.resultsProcessing;

import java.util.*;
import java.io.File;

import com.axi.util.*;
import com.axi.v810.business.testProgram.TestProgram;
import com.axi.v810.business.panelSettings.Project;
import com.axi.v810.datastore.FileName;
import com.axi.v810.datastore.Directory;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Patrick Lacz
 */
public class XmlFileTarget extends Target
{
  ResultFileTypeEnum _resultFileTypeEnum = null;
  List<String> _destinationPathList = null;

  /**
   * @author Patrick Lacz
   */
  public List<String> getDestinationList()
  {
    Assert.expect(_destinationPathList != null);
    return _destinationPathList;
  }

  /**
   * @author Patrick Lacz
   */
  public ResultFileTypeEnum getResultFileTypeEnum()
  {
    Assert.expect(_resultFileTypeEnum != null);
    return _resultFileTypeEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void initialize(ResultFileTypeEnum resultFileTypeEnum, List<String> destinationList)
  {
    Assert.expect(destinationList != null);
    Assert.expect(resultFileTypeEnum.getTargetImplementationClass().equals(this.getClass()));

    _resultFileTypeEnum = resultFileTypeEnum;
    _destinationPathList = new ArrayList<String>(destinationList);
  }

  /**
   * @author Patrick Lacz
   */
  public List<DatastoreException> performCopy(InspectionInformation inspectionInformation)
  {
    Assert.expect(inspectionInformation != null);
    Assert.expect(_resultFileTypeEnum != null);
    Assert.expect(_destinationPathList != null);

    Pair<String, String> xmlSourcePathAndFile = getXMLSourceFilePathAndName(_resultFileTypeEnum,
                                                                      inspectionInformation.getTestProgram(),
                                                                      inspectionInformation.getInspectionStartTime());
    String xsdSourceFileAndPath = XmlFileTarget.getSchemaSourceFileWithFullPath(_resultFileTypeEnum,
                                                                      inspectionInformation.getTestProgram());

    String xmlSourcePath = xmlSourcePathAndFile.getFirst();
    String xmlSourceFileName = xmlSourcePathAndFile.getSecond();

    String xmlSourceFileFullPath = xmlSourcePath + File.separator + xmlSourceFileName;
    List<DatastoreException> listOfDatastoreExceptions = new LinkedList<DatastoreException>();

    for (String rawDestinationPath : _destinationPathList)
    {
      if (Config.getInstance().getBooleanValue(SoftwareConfigEnum.ENABLE_CREATE_TIME_STAMP_FOLDER_FOR_FILE_TRANSFER) == true)
      {
        // Added by Jack Hwee - Create result XML folder for specification
        rawDestinationPath = rawDestinationPath + "inspectionRun_" + Directory.getDirName(inspectionInformation.getInspectionStartTime()) + File.separator;
      }
        
      String xmlFinalDestinationPath = Rule.replaceSymbolsInDestinationPath(rawDestinationPath, inspectionInformation);
      String xmlFinalDestinationPathWithoutFilename = xmlFinalDestinationPath;
      if (xmlFinalDestinationPath.endsWith(File.separator))
      {
        xmlFinalDestinationPath = FileUtil.concatFileNameToPath(xmlFinalDestinationPath, xmlSourceFileName);
      }
      else
      {
        xmlFinalDestinationPathWithoutFilename = FileUtilAxi.getPathWithoutFileName(xmlFinalDestinationPath);
      }

      String xsdFileName = FileUtilAxi.getNameWithoutPath(xsdSourceFileAndPath);

      if (_resultFileTypeEnum.equals(ResultFileTypeEnum.XML_INDICTMENTS_FILE_FOR_MRT))
      {
        xsdFileName = FileName.getInspectionIndictmentsXmlSchemaFileName();
      }
      String xsdFinalDestinationPath = FileUtil.concatFileNameToPath(xmlFinalDestinationPathWithoutFilename, xsdFileName);

      // copy the XML file
      try
      {
        performSingleCopy(xmlSourceFileFullPath, xmlFinalDestinationPath);
      }
      catch (DatastoreException de1)
      {
        listOfDatastoreExceptions.add(de1);
        ResultsProcessor.getInstance().addFailedOperation(this, inspectionInformation, de1);
      }

      // copy the XSD file
      try
      {
        performSingleCopy(xsdSourceFileAndPath, xsdFinalDestinationPath);
      }
      catch (DatastoreException de2)
      {
        listOfDatastoreExceptions.add(de2);
        ResultsProcessor.getInstance().addFailedOperation(this, inspectionInformation, de2);
      }
    }

    return listOfDatastoreExceptions;
  }

  /**
   * Return the file name and location for the given file type.
   * Only manages types that this class handles.
   *
   * @author Patrick Lacz
   */
  public static Pair<String, String> getXMLSourceFilePathAndName(
      ResultFileTypeEnum resultFileTypeEnum,
      TestProgram testProgram,
      long inspectionStartTimeInMillis)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(testProgram != null);

    Project project = testProgram.getProject();
    String projectName = project.getName();

    String sourcePath = Directory.getInspectionResultsDir(projectName, inspectionStartTimeInMillis);
    String sourceFileName = "";

    // choose the correct file name based on the 'file type'
    if (resultFileTypeEnum.equals(ResultFileTypeEnum.XML_CAD_FILE))
    {
      sourcePath = Directory.getResultsDir(projectName);
      sourceFileName = FileName.getCadFileName(project.getCadXmlChecksum());
    }
    else if (resultFileTypeEnum.equals(ResultFileTypeEnum.XML_SETTINGS_FILE))
    {
      sourcePath = Directory.getResultsDir(projectName);
      sourceFileName = FileName.getSettingsFileName(project.getSettingsXmlChecksum());
    }
    else if (resultFileTypeEnum.equals(ResultFileTypeEnum.XML_INDICTMENTS_FILE) ||
             resultFileTypeEnum.equals(ResultFileTypeEnum.XML_INDICTMENTS_FILE_FOR_MRT))
    {
      sourceFileName = FileName.getXMLResultsFileName(projectName);
    }
    else if(resultFileTypeEnum.equals(ResultFileTypeEnum.XML_MEASUREMENTS_FILE))
    {
      sourceFileName = FileName.getXMLMeasurementsFileName(projectName);
    }
    else
    {
      Assert.expect(false, "Unexpected Result File Type");
    }

    return new Pair<String,String>(sourcePath, sourceFileName);
  }

  /**
   * @author Patrick Lacz
   */
  public void setDestinationList(List<String> newListOfDestinations)
  {
    Assert.expect(newListOfDestinations != null);
    _destinationPathList = new ArrayList<String>(newListOfDestinations);
  }

  /**
   * @author Patrick Lacz
   */
  public static String getSchemaSourceFileWithFullPath(
      ResultFileTypeEnum resultFileTypeEnum,
      TestProgram testProgram)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(testProgram != null);

    Project project = testProgram.getProject();
    String projectName = project.getName();

    String sourceFileName = "";

    // choose the correct file name based on the 'file type'
    if (resultFileTypeEnum.equals(ResultFileTypeEnum.XML_CAD_FILE))
    {
      sourceFileName = FileName.getCadXmlSchemaFileNameFullPath();
    }
    else if (resultFileTypeEnum.equals(ResultFileTypeEnum.XML_SETTINGS_FILE))
    {
      sourceFileName = FileName.getSettingsXmlSchemaFileNameFullPath();
    }
    else if (resultFileTypeEnum.equals(ResultFileTypeEnum.XML_INDICTMENTS_FILE))
    {
      sourceFileName = FileName.getResultsXmlSchemaFileNameFullPath();
    }
    else if (resultFileTypeEnum.equals(ResultFileTypeEnum.XML_INDICTMENTS_FILE_FOR_MRT))
    {
      sourceFileName = FileName.getResultsXmlSchemaForMRTOnlyFileNameFullPath();
    }
    else if(resultFileTypeEnum.equals(ResultFileTypeEnum.XML_MEASUREMENTS_FILE))
    {
      sourceFileName = FileName.getResultsXmlSchemaFileNameFullPath();
    }
    else
    {
      Assert.expect(false, "Unexpected Result File Type");
    }

    return sourceFileName;
  }
}
