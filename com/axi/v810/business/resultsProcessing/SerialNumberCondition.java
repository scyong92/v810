package com.axi.v810.business.resultsProcessing;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Patrick Lacz
 */
public class SerialNumberCondition extends Condition
{
  /**
   * For the Serial Number, we don't know if the system is using panel or board serial numbers.
   * So, first check against panel serial numbers; if that doesn't match, test each of the board
   * serial numbers.
   *
   * The results files are not in board-level granularity -- if ANY of the board serial numbers match,
   * the entire results file is copied.
   *
   * @author Patrick Lacz
   */
  public boolean passes(InspectionInformation inspectionInformation)
  {
    Assert.expect(inspectionInformation != null);
    Assert.expect(_pattern != null);

    boolean passesCondition = false;

    // first, try the panel serial number
    String panelSerialNumber = inspectionInformation.getPanelSerialNumber();
    Matcher matcher = _pattern.matcher(panelSerialNumber);
    passesCondition = matcher.matches();

    if (passesCondition == false)
    {
      // continue by looking at the board serial numbers
      for (Board board : inspectionInformation.getTestProgram().getProject().getPanel().getBoards())
      {
        String boardName = board.getName();
        if (inspectionInformation.hasBoardSerialNumberForBoardName(boardName))
        {
          String boardSerialNumber = inspectionInformation.getBoardSerialNumberFromBoardName(boardName);

          matcher = _pattern.matcher(boardSerialNumber);
          passesCondition = matcher.matches();
          if (passesCondition)
            break;
        }
      }
    }

    return passesCondition;
  }
}
