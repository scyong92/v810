package com.axi.v810.business.resultsProcessing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;

/**
 * non-mutable information specific to a single inspection.
 *
 * @author Patrick Lacz
 */
public class InspectionInformation
{
  private TestProgram _testProgram = null;
  private long _inspectionStartTime = -1;
  private String _panelSerialNumber = null;
  private Map<String, String> _boardNameToSerialNumberMap = null;

  private boolean _containsRepairToolData = false;

  private String _productionTuningImageSetFullPath = "";

  /**
   * @author Patrick Lacz
   */
  public InspectionInformation(TestProgram testProgram, long inspectionStartTimeInMS, String panelSerialNumber, Map<String, String> boardNameToSerialNumberMap)
  {
    Assert.expect(testProgram != null);
    Assert.expect(panelSerialNumber != null);
    Assert.expect(boardNameToSerialNumberMap != null);
    Assert.expect(inspectionStartTimeInMS > 0);

    _testProgram = testProgram;
    _inspectionStartTime = inspectionStartTimeInMS;
    _panelSerialNumber = panelSerialNumber;
    _boardNameToSerialNumberMap = boardNameToSerialNumberMap;
  }

  /**
   * @author Patrick Lacz
   */
  public TestProgram getTestProgram()
  {
    Assert.expect(_testProgram != null);
    return _testProgram;
  }

  /**
   * @author Patrick Lacz
   */
  public String getProjectName()
  {
    Assert.expect(_testProgram != null);
    return _testProgram.getProject().getName();
  }

  /**
   * @author Patrick Lacz
   */
  public long getInspectionStartTime()
  {
    Assert.expect(_inspectionStartTime != -1);
    return _inspectionStartTime;
  }

  /**
   * @author Patrick Lacz
   */
  public String getPanelSerialNumber()
  {
    Assert.expect(_panelSerialNumber != null);
    return _panelSerialNumber;
  }

  /**
   * @author Patrick Lacz
   */
  public void setContainsRepairToolData(boolean flag)
  {
    _containsRepairToolData = flag;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean getContainsRepairToolData()
  {
    return _containsRepairToolData;
  }

  /**
   * @author Andy Mechtenberg
   */
  public void setProductionTuningImageSetFullPath(String fullPath)
  {
    Assert.expect(fullPath != null);
    _productionTuningImageSetFullPath = fullPath;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getProductionTuningImageSetFullPath()
  {
    return _productionTuningImageSetFullPath;
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean isProductionTuningEnabled()
  {
    if (_productionTuningImageSetFullPath.length() == 0)
      return false;
    else
      return true;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasBoardSerialNumberForBoardName(String boardName)
  {
    Assert.expect(_boardNameToSerialNumberMap != null);
    return _boardNameToSerialNumberMap.containsKey(boardName);
  }

  /**
   * @author Patrick Lacz
   */
  public String getBoardSerialNumberFromBoardName(String boardName)
  {
    Assert.expect(_boardNameToSerialNumberMap != null);
    Assert.expect(_boardNameToSerialNumberMap.containsKey(boardName));

    return _boardNameToSerialNumberMap.get(boardName);
  }

  /**
   * @author Patrick Lacz
   */
  public Map<String, String> getBoardNameToBoardSerialNumberMap()
  {
    Assert.expect(_boardNameToSerialNumberMap != null);
    return _boardNameToSerialNumberMap;
  }
}
