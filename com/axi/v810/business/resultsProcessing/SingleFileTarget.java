package com.axi.v810.business.resultsProcessing;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;

/**
 * @author Patrick Lacz
 */
public class SingleFileTarget extends Target
{
  ResultFileTypeEnum _resultFileTypeEnum = null;
  List<String> _destinationPathList = null;

  /**
   * Return the file name and location for the given file type.
   * Only manages types that this class handles.
   *
   * @author Patrick Lacz
   */
  public static Pair<String, String> getSourceFilePathAndName(
      ResultFileTypeEnum resultFileTypeEnum,
      TestProgram testProgram,
      long inspectionStartTimeInMillis)
  {
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(testProgram != null);

    Project project = testProgram.getProject();
    String projectName = project.getName();

    String sourcePath = Directory.getInspectionResultsDir(projectName, inspectionStartTimeInMillis);
    String sourceFileName = "";

    // choose the correct file name based on the 'file type'
    if (resultFileTypeEnum.equals(ResultFileTypeEnum.INTERNAL_RESULTS_BINARY_FILE))
    {
      // Our internal results file is included here out of a sense of completeness and consideration that
      // it may be useful internally. The customer should never depend on this functionality.
      sourceFileName = FileName.getResultsFileName(projectName);
    }
    else
    {
      Assert.expect(false, "Unexpected Result File Type");
    }

    return new Pair<String,String>(sourcePath, sourceFileName);
  }

  /**
   * @author Patrick Lacz
   */
  public void initialize(ResultFileTypeEnum fileTypeEnum, List<String> destinationList)
  {
    Assert.expect(fileTypeEnum != null);
    Assert.expect(destinationList != null);
    Assert.expect(fileTypeEnum.getTargetImplementationClass().equals(this.getClass()));

    _resultFileTypeEnum = fileTypeEnum;
    _destinationPathList = new ArrayList<String>(destinationList);
  }

  /**
   * @author Patrick Lacz
   */
  public ResultFileTypeEnum getResultFileTypeEnum()
  {
    Assert.expect(_resultFileTypeEnum != null);
    return _resultFileTypeEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public List<DatastoreException> performCopy(InspectionInformation inspectionInformation)
  {
    Assert.expect(inspectionInformation != null);
    Assert.expect(_resultFileTypeEnum != null);
    Assert.expect(_destinationPathList != null);

    Pair<String, String> sourcePathAndFile = getSourceFilePathAndName(_resultFileTypeEnum,
                                                                      inspectionInformation.getTestProgram(),
                                                                      inspectionInformation.getInspectionStartTime());

    String sourcePath = sourcePathAndFile.getFirst();
    String sourceFileName = sourcePathAndFile.getSecond();

    String sourceFileFullPath = sourcePath + File.separator + sourceFileName;

    List<DatastoreException> listOfDatastoreExceptions = new LinkedList<DatastoreException>();

    for (String rawDestinationPath : _destinationPathList)
    {
      String finalDestinationPath = Rule.replaceSymbolsInDestinationPath(rawDestinationPath, inspectionInformation);
      if (finalDestinationPath.endsWith(File.separator))
      {
        finalDestinationPath = FileUtil.concatFileNameToPath(finalDestinationPath, sourceFileName);
      }

      try
      {
        performSingleCopy(sourceFileFullPath, finalDestinationPath);
      }
      catch (DatastoreException de)
      {
        listOfDatastoreExceptions.add(de);
        ResultsProcessor.getInstance().addFailedOperation(this, inspectionInformation, de);
      }
    }

    return listOfDatastoreExceptions;
  }

  /**
   * @author Patrick Lacz
   */
  public List<String> getDestinationList()
  {
    Assert.expect(_destinationPathList != null);
    return _destinationPathList;
  }


  /**
   * @author Patrick Lacz
   */
  public void setDestinationList(List<String> newListOfDestinations)
  {
    Assert.expect(newListOfDestinations != null);
    _destinationPathList = new ArrayList<String>(newListOfDestinations);
  }

}
