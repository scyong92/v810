package com.axi.v810.business.resultsProcessing;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.resultsProcessing.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Patrick Lacz
 */
public class Test_Target extends UnitTest
{
  Map<ResultFileTypeEnum, String> _resultFileTypeToSourceFileNameWithPathMap = new HashMap<ResultFileTypeEnum,String>(4);

  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_Target());
  }

  /**
   * @author Patrick Lacz
   */
  public Test_Target()
  {
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    String projectName = "test_project";
    long timeStamp = 86400000; // January 1st, 1970
    createDummyFiles(projectName, 86400000, 110, 101);


    String temporaryDirectory = null;
    try
    {
      temporaryDirectory = FileUtilAxi.createTempDir("C:\\Temp");
    }
    catch (DatastoreException ex)
    {
      Assert.expect(false, ex.getLocalizedMessage());
    }

    try {
      testTarget(temporaryDirectory, projectName, 86400000, 110, 101);
    }
    finally
    {
      cleanUpFiles(projectName, temporaryDirectory);
    }
  }

  /**
   * @author Patrick Lacz
   */
  private void testTarget(String temporaryDirectory, String projectName, long fakeTimeStamp, long cadCheckSum, long settingsCheckSum)
  {
    Assert.expect(projectName != null);
    Assert.expect(temporaryDirectory != null);

    Project fakeProject = new Project(false);
    fakeProject.setName(projectName);
    fakeProject.setCadXmlChecksum(cadCheckSum);
    fakeProject.setSettingsXmlChecksum(settingsCheckSum);

    TestProgram fakeTestProgram = new TestProgram();
    fakeTestProgram.setProject(fakeProject);

    String fakePanelSerialNumber = "";
    Map<String, String> fakeBoardSerialNumberMap = new TreeMap<String,String>();

    InspectionInformation fakeInspectionInformation = new InspectionInformation(fakeTestProgram, fakeTimeStamp, fakePanelSerialNumber, fakeBoardSerialNumberMap);

    List<String> targetDirectoryList = new ArrayList<String>();

    // Cad file
    testXmlFileTarget(fakeTestProgram, fakeTimeStamp,
                          ResultFileTypeEnum.XML_CAD_FILE,
                          temporaryDirectory);

    // Settings file
    testXmlFileTarget(fakeTestProgram, fakeTimeStamp,
                          ResultFileTypeEnum.XML_SETTINGS_FILE,
                          temporaryDirectory + File.separator);

    // Results file
    testXmlFileTarget(fakeTestProgram, fakeTimeStamp,
                          ResultFileTypeEnum.XML_INDICTMENTS_FILE,
                          temporaryDirectory);


    // Defect Images
    targetDirectoryList.clear();
    String temporaryResultsDirectory = temporaryDirectory + File.separator;
    targetDirectoryList.add(temporaryResultsDirectory);

    Target target = ResultsProcessor.createTarget(ResultFileTypeEnum.DEFECT_IMAGE_FILES, targetDirectoryList);
    boolean copyWasSuccessful = target.performCopy(fakeInspectionInformation).isEmpty();

    Expect.expect(copyWasSuccessful);
    // sixteen regions of images, three xml files, three xsd files.
    Expect.expect(FileUtilAxi.listAllFilesInDirectory(temporaryResultsDirectory).size() == 16*3+3+3);

    // Test the pattern replacements
    testXmlFileTargetWithPattern(fakeTestProgram, fakeTimeStamp,
                                    ResultFileTypeEnum.XML_CAD_FILE,
                                    temporaryDirectory + File.separator + "$(Project_Name)_$YYYY-$MM-$(DD)_$CAD_CHECKSUM $SeTtInGs_ChEcksUm" + File.separator,
                                    temporaryDirectory + File.separator + projectName + "_1970-01-02_110 101" + File.separator);
  }

  /**
   * @author Patrick Lacz
   */
  private void testXmlFileTarget(TestProgram fakeTestProgram, long fakeTimeStamp, ResultFileTypeEnum resultFileTypeEnum, String targetFilePath)
  {
    Assert.expect(fakeTestProgram != null);
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(targetFilePath != null);

    List<String> targetDirectoryList = new ArrayList<String>();
    targetDirectoryList.add(targetFilePath + File.separator);

    String fakePanelSerialNumber = "";
    Map<String, String> fakeBoardSerialNumberMap = new TreeMap<String,String>();

    InspectionInformation fakeInspectionInformation = new InspectionInformation(fakeTestProgram, fakeTimeStamp, fakePanelSerialNumber, fakeBoardSerialNumberMap);

    Target target = ResultsProcessor.createTarget(resultFileTypeEnum, targetDirectoryList);
    boolean copyWasSuccessful = target.performCopy(fakeInspectionInformation).isEmpty();
    Expect.expect(copyWasSuccessful);

    Pair<String, String> sourcePathAndFileName = XmlFileTarget.getXMLSourceFilePathAndName(resultFileTypeEnum, fakeTestProgram, fakeTimeStamp);

    String targetFullPath = FileUtil.concatFileNameToPath(targetFilePath, sourcePathAndFileName.getSecond());

    expectFilesAreIdentical(_resultFileTypeToSourceFileNameWithPathMap.get(resultFileTypeEnum), targetFullPath);
  }
  /**
   * @author Patrick Lacz
   */
  private void testXmlFileTargetWithPattern(TestProgram fakeTestProgram,
                                               long fakeTimeStamp,
                                               ResultFileTypeEnum resultFileTypeEnum,
                                               String targetFilePath,
                                               String expectedTargetPath)
  {
    Assert.expect(fakeTestProgram != null);
    Assert.expect(resultFileTypeEnum != null);
    Assert.expect(targetFilePath != null);
    Assert.expect(expectedTargetPath != null);

    String fakePanelSerialNumber = "";
    Map<String, String> fakeBoardSerialNumberMap = new TreeMap<String,String>();

    InspectionInformation fakeInspectionInformation = new InspectionInformation(fakeTestProgram, fakeTimeStamp, fakePanelSerialNumber, fakeBoardSerialNumberMap);

    List<String> targetDirectoryList = new ArrayList<String>();
    targetDirectoryList.add(targetFilePath);

    Target target = ResultsProcessor.createTarget(resultFileTypeEnum, targetDirectoryList);
    boolean copyWasSuccessful = target.performCopy(fakeInspectionInformation).isEmpty();

    Expect.expect(copyWasSuccessful);

    Pair<String, String> sourcePathAndFileName = XmlFileTarget.getXMLSourceFilePathAndName(resultFileTypeEnum, fakeTestProgram, fakeTimeStamp);
    String targetFullPath = FileUtil.concatFileNameToPath(expectedTargetPath, sourcePathAndFileName.getSecond());

    expectFilesAreIdentical(_resultFileTypeToSourceFileNameWithPathMap.get(resultFileTypeEnum), targetFullPath);
  }

  /**
   * @author Patrick Lacz
   */
  private void expectFilesAreIdentical(String file1, String file2)
  {
    Expect.expect(FileUtilAxi.exists(file1));
    Expect.expect(FileUtilAxi.exists(file2));
    try
    {
      Expect.expect(FileUtil.areFilesIdentical(file1, file2));
    }
    catch (CouldNotReadFileException ex1)
    {
      Expect.expect(false, ex1.getLocalizedMessage());
    }
    catch (FileDoesNotExistException ex1)
    {
      Expect.expect(false, ex1.getLocalizedMessage());
    }
  }

  /**
   * @author Patrick Lacz
   * @author Kee Chin Seong - Adding the Width Height to make the image name unique
   */
  private void createDummyFiles(String projectName, long fakeTimeStamp, long cadCheckSum, long settingsCheckSum)
  {
    Assert.expect(projectName != null);

    String inspectionResultDirectory = Directory.getInspectionResultsDir(projectName, fakeTimeStamp);
    try
    {
      FileUtilAxi.mkdirs(inspectionResultDirectory);
    }
    catch (DatastoreException ex)
    {
      Assert.expect(false, ex.getMessage());
    }

    String cadXmlFileName = FileName.getCadFileNameInResultsDirFullPath(projectName, cadCheckSum);
    File cadXmlFile = new File(cadXmlFileName);
    try
    {
      cadXmlFile.createNewFile();
    }
    catch (IOException ex1)
    {
      Assert.expect(false, ex1.getMessage());
    }
    _resultFileTypeToSourceFileNameWithPathMap.put(ResultFileTypeEnum.XML_CAD_FILE, cadXmlFileName);

    String settingsXmlFileName = FileName.getSettingsFileNameInResultsDirFullPath(projectName, settingsCheckSum);
    File settingsXmlFile = new File(settingsXmlFileName);
    try
    {
      settingsXmlFile.createNewFile();
    }
    catch (IOException ex1)
    {
      Assert.expect(false, ex1.getMessage());
    }
    _resultFileTypeToSourceFileNameWithPathMap.put(ResultFileTypeEnum.XML_SETTINGS_FILE, settingsXmlFileName);

    String resultsXmlFileName = FileName.getXMLResultsFileFullPath(projectName, fakeTimeStamp);
    File resultsXmlFile = new File(resultsXmlFileName);
    try
    {
      resultsXmlFile.createNewFile();
    }
    catch (IOException ex1)
    {
      Assert.expect(false, ex1.getMessage());
    }
    _resultFileTypeToSourceFileNameWithPathMap.put(ResultFileTypeEnum.XML_INDICTMENTS_FILE, resultsXmlFileName);

    // Create a bunch of image, 3x3x3 of them, to be precise. (27)
    for (int x = 0 ; x <= 300 ; x += 100)
    {
      for (int y = 0 ; y <= 300 ; y += 100)
      {
        for (int slice = 0 ; slice < 3 ; ++slice)
        {
          String imageFileName = FileName.getResultImageName(true, x, y, 12, 12, slice);
          File imageFile = new File(inspectionResultDirectory + File.separator + imageFileName);
          try
          {
            imageFile.createNewFile();
          }
          catch (IOException ex1)
          {
            Assert.expect(false, ex1.getMessage());
          }
        }
      }
    }
  }

  /**
   * @author Patrick Lacz
   */
  private void cleanUpFiles(String projectName, String temporaryDirectoryPath)
  {
    Assert.expect(projectName != null);
    Assert.expect(temporaryDirectoryPath != null);

    try
    {
      FileUtilAxi.delete(temporaryDirectoryPath);
    }
    catch (DatastoreException ex)
    {
      Expect.expect(false, "Unable to clean up temporary directory: " + temporaryDirectoryPath);
    }

    String projectResultsPath = Directory.getResultsDir(projectName);
    try
    {
      FileUtilAxi.delete(projectResultsPath);
    }
    catch (DatastoreException ex1)
    {
      Expect.expect(false, "Unable to clean up temporary results directory: " + projectResultsPath);
    }

  }
}
