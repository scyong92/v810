package com.axi.v810.business.resultsProcessing;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This is really an interface class with some common routines used by all the sub-classes.
 * @author Patrick Lacz
 */
public abstract class Target
{

  /**
   * @author Patrick Lacz
   */
  public abstract void initialize(ResultFileTypeEnum resultFileTypeEnum, List<String> destinationList);

  /**
   * @return List of errors encountered during copy.
   * @author Patrick Lacz
   */
  public abstract List<DatastoreException> performCopy(InspectionInformation inspectionInformation);

  /**
   * @author Patrick Lacz
   */
  public abstract ResultFileTypeEnum getResultFileTypeEnum();

  /**
   * @author Patrick Lacz
   */
  public abstract List<String> getDestinationList();

  /**
   * @author Patrick Lacz
   */
  public abstract void setDestinationList(List<String> newListOfDestinations);

  private Rule _parentRule = null;

  /**
   * @author Patrick Lacz
   */
  public boolean hasParentRule()
  {
    return _parentRule != null;
  }

  /**
   * Always check hasParentRule() before using this method.
   * @author Patrick Lacz
   */
  public Rule getParentRule()
  {
    Assert.expect(_parentRule != null);
    return _parentRule;
  }

  /**
   * @author Patrick Lacz
   */
  public void setParentRule(Rule newParentRule)
  {
    _parentRule = newParentRule;
  }

  /**
   * @author Patrick Lacz
   */
  protected void performSingleCopy(
      String sourceFileWithPath,
      String destinationFileWithPath) throws DatastoreException
  {
    Assert.expect(sourceFileWithPath != null);
    Assert.expect(destinationFileWithPath != null);

    String pathWithoutFileName = FileUtil.getPathWithoutFileName(destinationFileWithPath);
    if (FileUtilAxi.exists(pathWithoutFileName) == false)
    {
      FileUtilAxi.mkdirs(pathWithoutFileName);
    }
    
    // XCR-3323 Some result files are not found when run test
    if (FileUtilAxi.exists(sourceFileWithPath) == false)
      return;

    if (FileUtilAxi.exists(destinationFileWithPath))
    {
      // do some comparisons. If we're pretty certain it is the same file, don't copy.
      File sourceFile = new File(sourceFileWithPath);
      File destinationFile = new File(destinationFileWithPath);
      if (sourceFile.canRead() && destinationFile.canRead())
      {
        if (sourceFile.length() == destinationFile.length() &&
            sourceFile.lastModified() <= destinationFile.lastModified())
          return;
      }
    }

    FileUtilAxi.copy(sourceFileWithPath, destinationFileWithPath);
  }

}
