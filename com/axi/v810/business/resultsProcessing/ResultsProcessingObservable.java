package com.axi.v810.business.resultsProcessing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;

/**
 * This observable is sent messages from the Results Processor.
 * Currently the only 'message' is a list of DatastoreExceptions that have occurred.
 * It is expected that when this message is sent, the gui will pop up a non-blocking gui that provides the localized messages
 * to the user and gives them a button to retry these failures (retryFailedCopies).
 *
 * @author Patrick Lacz
 */
public class ResultsProcessingObservable extends Observable
{
  private static ResultsProcessingObservable _instance;

  /**
   * @author Patrick Lacz
   */
  public static synchronized ResultsProcessingObservable getInstance()
  {
    if (_instance == null)
      _instance = new ResultsProcessingObservable();
    return _instance;
  }

  /**
   * @author Patrick Lacz
   */
  private ResultsProcessingObservable()
  {
    // do nothing
  }

  /**
   * @author Patrick Lacz
   */
  public void notifyWithNewFailures(List<DatastoreException> listOfDatastoreExceptions)
  {
    Assert.expect(listOfDatastoreExceptions != null);

    setChanged();
    notifyObservers(listOfDatastoreExceptions);
  }

  /**
   * @author Patrick Lacz
   */
  public void notifyWithFailureMap(Map<Pair<Target, InspectionInformation>, List<DatastoreException>> failureMap)
  {
    Assert.expect(failureMap != null);

    setChanged();
    notifyObservers(failureMap);
  }

}
