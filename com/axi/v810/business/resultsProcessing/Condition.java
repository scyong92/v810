package com.axi.v810.business.resultsProcessing;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.business.userAccounts.*;
import com.axi.v810.datastore.*;

/**
 * A class that encapsulates evaluating a condition for using a particular rule in the results processor.
 *
 * @author Patrick Lacz
 */
public class Condition
{
  // indicates the expression is a regular expression
  public static final String REGEX_TYPE_IDENTIFIER = "regex";

  // indicates the pattern was defined using 'simple' expression (aka globs) which allow ? and * to
  // represent any one non-whitespace character and any number of non-whitespace characters respectively.
  public static final String SIMPLE_TYPE_IDENTIFIER = "simple";

  // the type name; must match one of the strings above.
  private String _typeName = null;

  // These are the original input patterns
  private List<String> _expressionList = null;

  // This is the object that performs the matching.
  // Protected so that inheiriting classes may use it as well. (eg. SerialNumberCondition)
  // The guts of this class may in the future be extracted into a separate subclass.
  protected Pattern _pattern = null;

  // The ConditionKey that this Condition has been specified to use.
  private ConditionKeyEnum _conditionKeyEnum = null;

  /**
   * @author Patrick Lacz
   */
  public void initialize(ConditionKeyEnum conditionKeyEnum,
                      List<String> patternStringList,
                      Pattern compiledRegExPattern,
                      String expressionTypeName)
  {
    Assert.expect(conditionKeyEnum != null);
    Assert.expect(patternStringList != null);
    Assert.expect(patternStringList.isEmpty() == false);
    Assert.expect(expressionTypeName != null);
    Assert.expect(compiledRegExPattern != null);
    Assert.expect(conditionKeyEnum.getImplementationClass().equals(this.getClass()));

    _conditionKeyEnum = conditionKeyEnum;
    _expressionList = new ArrayList<String>(patternStringList);
    _pattern = compiledRegExPattern;
    _typeName = expressionTypeName;
  }

  /**
   * @author Patrick Lacz
   */
  public Condition createCopy()
  {
    Assert.expect(_conditionKeyEnum != null);
    Assert.expect(_typeName != null);
    Assert.expect(_expressionList != null);

    try
    {
      return ResultsProcessor.createCondition(_conditionKeyEnum, _typeName, _expressionList);
    }
    catch (DatastoreException ex)
    {
      Assert.logException(ex);
      return null;
    }

  }

  /**
   * @author Patrick Lacz
   */
  public boolean passes(InspectionInformation inspectionInformation)
  {
    Assert.expect(inspectionInformation != null);
    Assert.expect(_pattern != null);
    Assert.expect(_conditionKeyEnum != null);

    String valueToTest = null;
    if (_conditionKeyEnum.equals(ConditionKeyEnum.PROJECT_NAME))
      valueToTest = inspectionInformation.getProjectName();
    else if (_conditionKeyEnum.equals(ConditionKeyEnum.USER_NAME))
      valueToTest = UserAccountsManager.getCurrentUserLoginName();
    else if (_conditionKeyEnum.equals(ConditionKeyEnum.USER_ACCOUNT_TYPE))
      valueToTest = UserAccountsManager.getCurrentUserType().getName();
    else if (_conditionKeyEnum.equals(ConditionKeyEnum.TARGET_CUSTOMER))
      valueToTest = inspectionInformation.getTestProgram().getProject().getTargetCustomerName();
    else if (_conditionKeyEnum.equals(ConditionKeyEnum.PROGRAMMER_NAME))
      valueToTest = inspectionInformation.getTestProgram().getProject().getProgrammerName();

    Assert.expect(valueToTest != null, "Unsupported Condition Key");

    Matcher matcher = _pattern.matcher(valueToTest);
    boolean passesCondition = matcher.matches();

    return passesCondition;
  }

  /**
   * @author Patrick Lacz
   */
  public String getExpressionType()
  {
    Assert.expect(_typeName != null);
    return _typeName;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean isDefinedUsingRegularExpression()
  {
    Assert.expect(_typeName != null);
    return _typeName.equals(REGEX_TYPE_IDENTIFIER);
  }

  /**
   * @author Patrick Lacz
   */
  public boolean isDefinedUsingSimpleExpression()
  {
    Assert.expect(_typeName != null);
    return _typeName.equals(SIMPLE_TYPE_IDENTIFIER);
  }

  /**
   * Returns the unmodified string used to define this condition.
   * @author Patrick Lacz
   */
  public List<String> getDefintionExpressionList()
  {
    Assert.expect(_expressionList != null);
    return _expressionList;
  }

  /**
   * @author Patrick Lacz
   */
  public ConditionKeyEnum getConditionKeyEnum()
  {
    Assert.expect(_conditionKeyEnum != null);
    return _conditionKeyEnum;
  }
}
