package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

public class Test_MultiDimensionProfileDiagnosticInfo extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public Test_MultiDimensionProfileDiagnosticInfo()
  {
    // Do nothing.
  }

  /**
   * Tests the 'happy path'.
   *
   * @author Matt Wharton
   * @author Peter Esbensen
   */
  private void testHappyPath()
  {
    testConstructorAndAccessorMethods();
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  private void testConstructorAndAccessorMethods()
  {
    // Test out the constructor and relevant getters.
    ProfileTypeEnum profileType = ProfileTypeEnum.SOLDER_PROFILE;
    int numberOfDimensions = 2;
    List<LocalizedString> axisDescriptions = new ArrayList<LocalizedString>();
    axisDescriptions.add(new LocalizedString("axis_key_1", null));
    axisDescriptions.add(new LocalizedString("axis_key_2", null));
    List<Pair<Float, Float>> axisExtents = new ArrayList<Pair<Float,Float>>();
    axisExtents.add(new Pair<Float, Float>(-10f, 10f));
    axisExtents.add(new Pair<Float, Float>(-20f, 20f));
    MeasurementUnitsEnum profileUnits = MeasurementUnitsEnum.MILLIMETERS;
    MultiDimensionProfileDiagnosticInfo profileDiagInfo = new MultiDimensionProfileDiagnosticInfo("padName",
                                                                                                  profileType,
                                                                                                  numberOfDimensions,
                                                                                                  axisDescriptions,
                                                                                                  axisExtents,
                                                                                                  profileUnits);

    Expect.expect(profileDiagInfo.getProfileType() == profileType, "Profile type doesn't match!");
    Expect.expect(profileDiagInfo.getNumberOfDimensions() == numberOfDimensions, "Number of dimensions doesn't match!");
    Expect.expect(profileDiagInfo.getAxisDescriptions() == axisDescriptions, "Axis descriptions don't match!");
    Expect.expect(profileDiagInfo.getAxisExtents() == axisExtents, "Axis extents don't match!");
    Expect.expect(profileDiagInfo.getProfileUnits() == profileUnits, "Profile units don't match!");

    // Test that adding data points works properly.
    MeasurementRegionEnum measurementRegionEnum1 = MeasurementRegionEnum.PAD_REGION;
    List<Float> dataPoint1 = Arrays.asList(new Float[]{ 1f, 2f });
    profileDiagInfo.addDataPoint(measurementRegionEnum1, dataPoint1);
    MeasurementRegionEnum measurementRegionEnum2 = MeasurementRegionEnum.PAD_REGION;
    List<Float> dataPoint2 = Arrays.asList(new Float[]{ 3f, 4f });
    profileDiagInfo.addDataPoint(measurementRegionEnum2, dataPoint2);

    List<Pair<MeasurementRegionEnum, List<Float>>> data = profileDiagInfo.getData();
    Expect.expect(data.get(0).getFirst() == measurementRegionEnum1, "Measurement region enum doesn't match!");
    Expect.expect(data.get(0).getSecond() == dataPoint1, "Data point doesn't match!");
    Expect.expect(data.get(1).getFirst() == measurementRegionEnum2, "Measurement region enum doesn't match!");
    Expect.expect(data.get(1).getSecond() == dataPoint2, "Data point doesn't match!");

    // Test that data clearing works properly.
    profileDiagInfo.clearData();
    Expect.expect(profileDiagInfo.getData().isEmpty(), "Data did not clear!");
  }

  /**
   * Tests that the asserts are working properly.
   */
  private void testAsserts()
  {
    final ProfileTypeEnum profileType = ProfileTypeEnum.SOLDER_PROFILE;
    final int numberOfDimensions = 2;
    final List<LocalizedString> axisDescriptions = new ArrayList<LocalizedString>();
    axisDescriptions.add(new LocalizedString("axis_key_1", null));
    axisDescriptions.add(new LocalizedString("axis_key_2", null));
    final List<Pair<Float, Float>> axisExtents = new ArrayList<Pair<Float,Float>>();
    axisExtents.add(new Pair<Float, Float>(-10f, 10f));
    axisExtents.add(new Pair<Float, Float>(-20f, 20f));
    final MeasurementUnitsEnum profileUnits = MeasurementUnitsEnum.MILLIMETERS;

    // Verify that the constructor asserts when passed null parameters.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MultiDimensionProfileDiagnosticInfo("padName",
                                                null,
                                                numberOfDimensions,
                                                axisDescriptions,
                                                axisExtents,
                                                profileUnits);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MultiDimensionProfileDiagnosticInfo("padName",
                                                profileType,
                                                numberOfDimensions,
                                                null,
                                                axisExtents,
                                                profileUnits);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MultiDimensionProfileDiagnosticInfo("padName",
                                                profileType,
                                                numberOfDimensions,
                                                axisDescriptions,
                                                null,
                                                profileUnits);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MultiDimensionProfileDiagnosticInfo("padName",
                                                profileType,
                                                numberOfDimensions,
                                                axisDescriptions,
                                                axisExtents,
                                                null);
      }
    });

    // Verify that the constructor assets when passed an invalid number of dimensions.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MultiDimensionProfileDiagnosticInfo("padName",
                                                profileType,
                                                -1,
                                                axisDescriptions,
                                                axisExtents,
                                                profileUnits);
      }
    });

    // Verify that the constructor asserts when passed axis descriptions with the wrong number of dimensions.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MultiDimensionProfileDiagnosticInfo("padName",
                                                profileType,
                                                numberOfDimensions,
                                                Arrays.asList(new LocalizedString[]{new LocalizedString("bogus", null)}),
                                                axisExtents,
                                                profileUnits);
      }
    });

    // Verify that the constructor asserts when passed axis extents with the wrong number of dimensions.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MultiDimensionProfileDiagnosticInfo("padName",
                                                profileType,
                                                numberOfDimensions,
                                                axisDescriptions,
                                                new ArrayList<Pair<Float, Float>>(),
                                                profileUnits);
      }
    });

    final MultiDimensionProfileDiagnosticInfo profileDiagInfo = new MultiDimensionProfileDiagnosticInfo("padName",
                                                                                                        profileType,
                                                                                                        numberOfDimensions,
                                                                                                        axisDescriptions,
                                                                                                        axisExtents,
                                                                                                        profileUnits);
    final List<Float> dataPoint1 = Arrays.asList(new Float[]{1f, 1f});

    // Verify that calling addDataPoint() with null params causes asserts.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        profileDiagInfo.addDataPoint(null, dataPoint1);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        profileDiagInfo.addDataPoint(MeasurementRegionEnum.PAD_REGION, null);
      }
    });

    // Verify that calling addDataPoint() with a tuple of incorrect dimensions causes an assert.
    final List<Float> dataPointWithIncorrectDimensions = Arrays.asList(new Float[]{1f, 2f, 3f});
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        profileDiagInfo.addDataPoint(MeasurementRegionEnum.PAD_REGION, dataPointWithIncorrectDimensions);
      }
    });
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // Test the 'happy path'.
    testHappyPath();

    // Test the asserts.
    testAsserts();
  }

  /**
   * Test fixture entry point.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_MultiDimensionProfileDiagnosticInfo());
  }
}
