package com.axi.v810.business.imageAnalysis;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.internalTools.*;
import com.axi.v810.util.*;

/**
 * This class extends UnitTest to include useful shared utilities for algorithm
 * unit tests.
 *
 * @author Peter Esbensen
 */
public abstract class AlgorithmUnitTest extends UnitTest
{

  protected final boolean _LEARN_SUBTYPES = true;
  protected final boolean _DONT_LEARN_SUBTYPES = false;
  protected final boolean _LEARN_SHORT = true;
  protected final boolean _DONT_LEARN_TEMPLATE = false;//ShengChuan - Clear Tombstone
  protected final boolean _DONT_LEARN_SHORT = false;
  protected final boolean _LEARN_EXPECTED_IMAGES = true;
  protected final boolean _DONT_LEARN_EXPECTED_IMAGES = false;

  protected final boolean POPULATED_BOARD = true;
  protected final boolean UNPOPULATED_BOARD = false;

  protected final boolean CLEAR_PREVIOUS_IMAGE_RUNS = true;
  protected final boolean DONT_CLEAR_PREVIOUS_IMAGE_RUNS = false;

  protected final String ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE = "normalUnitTestImageSet";
  protected long _currentInspectionStartTime = -1;
  private ImageManager _imageManager;
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

  /**
   * @author Peter Esbensen
   */
  public AlgorithmUnitTest()
  {
    _imageManager = ImageManager.getInstance();
  }

  /**
   * This method deletes subtype and shorts learned data.  This will reset all learned setting to their defaults, and
   * it will also removed stored data that's used for learning.
   * @author Sunit Bhalla
   */
  protected void deleteLearnedData(Subtype subtype) throws XrayTesterException
  {
    Assert.expect(subtype != null);

    Panel panel = subtype.getPanel();
    Project project = panel.getProject();

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter();
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear Tombstone
    
    algorithmSubtypeLearning.open();
    algorithmShortsLearning.open();
    algorithmExpectedImageLearning.open();
    algorithmBrokenPinLearning.open();
    algorithmExpectedImageTemplateLearning.open();//ShengChuan - Clear Tombstone

    subtype.deleteLearnedAlgorithmSettingsData();
    subtype.setLearnedSettingsToDefaults();
    subtype.deleteLearnedJointSpecificData();
    subtype.deleteLearnedExpectedImageData();
    subtype.deleteLearnedExpectedImageTemplateData();//ShengChuan - Clear Tombstone

    algorithmSubtypeLearning.close();
    algorithmShortsLearning.close();
    algorithmExpectedImageLearning.close();
    algorithmBrokenPinLearning.close();
    algorithmExpectedImageTemplateLearning.close();//ShengChuan - Clear Tombstone
  }

  /**
   * This method deletes all subtype and shorts learned data.
   * @author Sunit Bhalla
   */
  public void deleteAllLearnedData(Project project) throws DatastoreException
  {
    AlgorithmSubtypesLearningReaderWriter algorithmLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    algorithmLearning.open();
    List<Subtype> subtypes = project.getPanel().getSubtypes();

    for (Subtype subtype : subtypes)
    {
      subtype.deleteLearnedAlgorithmSettingsData();
      subtype.deleteLearnedJointSpecificData();
    }
    algorithmLearning.close();
  }

  /**
   * @author Matt Wharton
   */
  protected ImageSetData getNormalImageSet(Project project)
  {
    Assert.expect(project != null);

    List<ImageSetData> imageSetDataList = null;
    try
    {
      imageSetDataList = _imageManager.getCompatibleImageSetData(project);
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }

    ImageSetData imageSetDataToUse = null;
    for (ImageSetData imageSetData : imageSetDataList)
    {
      if (imageSetData.getImageSetName().equals(ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE))
      {
        imageSetDataToUse = imageSetData;
        break;
      }
    }
    Assert.expect(imageSetDataToUse != null);

    return imageSetDataToUse;
  }

  /**
   * @author Matt Wharton
   */
  protected ImageSetData createAndSelectBlackImageSet(Project project, Pad pad, String imageSetDescription)
  {
    Assert.expect(project != null);
    Assert.expect(pad != null);
    Assert.expect(imageSetDescription != null);

    // Create a new image set record and add it to the project.
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project, pad, imageSetDescription, POPULATED_BOARD);

    // Generate the image set.
    generateBlackImageSet(project, imageSetData);

    Assert.expect(imageSetData != null);
    return imageSetData;
  }

  /**
   * @author Matt Wharton
   */
  protected ImageSetData createAndSelectWhiteImageSet(Project project, Pad pad, String imageSetDescription)
  {
    Assert.expect(project != null);
    Assert.expect(pad != null);
    Assert.expect(imageSetDescription != null);

    // Create a new image set record and add it to the project.
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project, pad, imageSetDescription, POPULATED_BOARD);

    // Generate the image set.
    generateWhiteImageSet(project, imageSetData);

    Assert.expect(imageSetData != null);
    return imageSetData;
  }

  /**
   * @author Matt Wharton
   */
  protected ImageSetData createAndSelectPseudoRandomImageSet(Project project, Pad pad, String imageSetDescription)
  {
    Assert.expect(project != null);
    Assert.expect(pad != null);
    Assert.expect(imageSetDescription != null);

    // Create a new image set record and add it to the project.
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project, pad, imageSetDescription, POPULATED_BOARD);

    // Generate the image set.
    generateRandomImageSet(project, imageSetData);

    Assert.expect(imageSetData != null);
    return imageSetData;
  }

  /**
   * Run a subtype inspection.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Peter Esbensen
   */
  protected void runPadUnitTestInspection(Project project, Pad pad, List<ImageSetData> imageSetDataList)
  {
    Assert.expect(project != null);
    Assert.expect(pad != null);
    Assert.expect(imageSetDataList != null);

    TestProgram testProgram = project.getTestProgram();

    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      TestExecution testExecutionEngine = TestExecution.getInstance();
      InspectionEngine inspectionEngine = InspectionEngine.getInstance();
      String description = new String("unit test inspection");
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      panelInspectionSettings.setInspectedSubtypes(pad);

      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images
      inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(pad);
      testExecutionEngine.inspectPanelForTestDevelopmentUsingImageSetData(testProgram, imageSetDataList, description);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }


  /**
   * Run a subtype inspection.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Peter Esbensen
   */
  protected void runSubtypeUnitTestInspection(Project project,
                                              Subtype subtype,
                                              List<ImageSetData> imageSetDataList) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);

    TestProgram testProgram = project.getTestProgram();

    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      TestExecution testExecutionEngine = TestExecution.getInstance();
      InspectionEngine inspectionEngine = InspectionEngine.getInstance();
      String description = new String("unit test inspection");
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      panelInspectionSettings.setInspectedSubtypes(subtype);

      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images
      inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(subtype);
      testExecutionEngine.inspectPanelForTestDevelopmentUsingImageSetData(testProgram, imageSetDataList, description);
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }


  /**
   * Run a component inspection.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Peter Esbensen
   */
  protected void runComponentUnitTestInspection(Project project,
                                                Component component)
  {
    Assert.expect(project != null);
    Assert.expect(component != null);

    TestProgram testProgram = project.getTestProgram();

    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      InspectionEngine testExecutionEngine = InspectionEngine.getInstance();
      String description = new String("unit test inspection");
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      panelInspectionSettings.setInspectedSubtypes(component);

      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images
      testExecutionEngine.setPanelInspectionSettings(panelInspectionSettings);
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(component);
      testExecutionEngine.inspectForTestDevelopment(testProgram, new TestExecutionTimer(), description);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * Run a component inspection.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Patrick Lacz
   */
  protected void runPanelUnitTestInspection(Project project, List<ImageSetData> selectedImageSets)
  {
    Assert.expect(project != null);
    Assert.expect(selectedImageSets != null);

    TestProgram testProgram = project.getTestProgram();

    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      TestExecution testExecutionEngine = TestExecution.getInstance();
      InspectionEngine inspectionEngine = InspectionEngine.getInstance();
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      panelInspectionSettings.setInspectedSubtypes(project.getPanel());

      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false);  // go to offline mode to run off saved images

      inspectionEngine.setPanelInspectionSettings(panelInspectionSettings);
      testExecutionEngine.inspectPanelForTestDevelopmentUsingImageSetData(testProgram, selectedImageSets, "runDescription");

      _currentInspectionStartTime = inspectionEngine.getInspectionStartTimeInMillis();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void deleteSerializedProject(String projectName)
  {
    Assert.expect(projectName != null);
    try
    {
      String serializedProjectPath = FileName.getProjectSerializedFullPath(projectName);
      if (FileUtil.exists(serializedProjectPath))
      {
        FileUtil.delete(serializedProjectPath);
      }
    }
    catch (CouldNotDeleteFileException ex)
    {
      ex.printStackTrace();
      Expect.expect(false);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void addAlignmentGroupsToProject(Project project)
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    panel.getPanelSettings().setRightManualAlignmentTransform(AffineTransform.getScaleInstance(1.0, 1.0));

    List<AlignmentGroup> alignmentGroups = project.getPanel().getPanelSettings().getAlignmentGroupsForShortPanel();
    AlignmentGroup alignmentGroup1 = alignmentGroups.get(0);
    AlignmentGroup alignmentGroup2 = alignmentGroups.get(1);
    AlignmentGroup alignmentGroup3 = alignmentGroups.get(2);
    List<Pad> pads = project.getPanel().getPads();
    alignmentGroup1.addPad(pads.get(0));
    alignmentGroup2.addPad(pads.get(1));
    alignmentGroup3.addPad(pads.get(2));
  }

  /**
   * @author Peter Esbensen
   */
  protected Project getProject(String projectName)
  {
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    Project project = null;

    deleteSerializedProject(projectName);

    try
    {
      project = Project.importProjectFromNdfs(projectName, warnings);
      TestProgram testProgram = project.getTestProgram();
      for(TestSubProgram testSubProgram : testProgram.getFilteredTestSubPrograms())
      {
        AffineTransform transform = testSubProgram.getDefaultAggregateAlignmentTransform();
        project.getPanel().getPanelSettings().setRightManualAlignmentTransform(transform);
      }
    }
    catch (XrayTesterException xte)
    {
      xte.printStackTrace();
    }
    Expect.expect(project != null);

//    addAlignmentGroupsToProject(project);

    return project;
  }

  /**
   * Used in Exposed Pad regression test
   *
   * @author George Booth
   */
    protected Project loadProject(String projectName)
    {
      Project project = null;

      deleteSerializedProject(projectName);

      try
      {
        BooleanRef abortedDuringLoad = new BooleanRef();
        project = Project.load(projectName, abortedDuringLoad);
        Assert.expect(abortedDuringLoad.getValue() == false);
      }
      catch (XrayTesterException xte)
      {
        xte.printStackTrace();
      }
      Expect.expect(project != null);

      return project;
    }

  /**
   * Learns a subtype.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Sunit Bhalla
   */
  protected void learnSubtypeUnitTest(Project project,
                                      Subtype subtype,
                                      List<ImageSetData> imageSetDataList,
                                      boolean learnSubtypes,
                                      boolean learnShort)
  {
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT, _DONT_LEARN_TEMPLATE);
  }
    
  /**
   * Learns a subtype.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Sunit Bhalla
   * @author ShengChuan - Clear Tombstone - Overload for template image use.
   * @param project
   */
  protected void learnSubtypeUnitTest(Project project,
                                      Subtype subtype,
                                      List<ImageSetData> imageSetDataList,
                                      boolean learnSubtypes,
                                      boolean learnShort,
                                      boolean learnTemplate)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);

    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      StatisticalTuningEngine learningEngine = StatisticalTuningEngine.getInstance();
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      InspectionEngine.getInstance().setPanelInspectionSettings(panelInspectionSettings);

      //ShengChuan - Clear tombstone
      learningEngine.learnSubtype(project, subtype, imageSetDataList, learnSubtypes, learnShort, learnTemplate, false);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * Learns a subtype.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Sunit Bhalla
   * @author ShengChuan - Clear Tombstone - add learnTemplate
   */
  protected void learnSubtypeUnitTest(Project project,
                                      Subtype subtype,
                                      List<ImageSetData> imageSetDataList,
                                      boolean learnSubtypes,
                                      boolean learnShort,
                                      boolean learnExpectedImages,
                                      boolean learnTemplate)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);

    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      StatisticalTuningEngine learningEngine = StatisticalTuningEngine.getInstance();
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      InspectionEngine.getInstance().setPanelInspectionSettings(panelInspectionSettings);

      learningEngine.learnSubtype(project, subtype, imageSetDataList, learnSubtypes, learnShort, learnTemplate, learnExpectedImages);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * Learns a joint type.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Sunit Bhalla
   * @author ShengChuan - Clear Tombstone - add learnTemplate
   */
  protected void learnJointTypeUnitTest(Project project,
                                        JointTypeEnum jointType,
                                        List<ImageSetData> imageSetDataList,
                                        boolean learnSubtypes,
                                        boolean learnShort,
                                        boolean learnTemplate)
  {
    Assert.expect(project != null);
    Assert.expect(jointType!= null);
    Assert.expect(imageSetDataList != null);

    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      StatisticalTuningEngine learningEngine = StatisticalTuningEngine.getInstance();
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      InspectionEngine.getInstance().setPanelInspectionSettings(panelInspectionSettings);

      learningEngine.learnJointType(project, jointType, imageSetDataList, learnSubtypes, learnShort, learnTemplate);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }


  /**
   * Learns a panel.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Sunit Bhalla
   */
  protected void learnPanelUnitTest(Project project,
                                    List<ImageSetData> imageSetDataList,
                                    boolean learnSubtypes,
                                    boolean learnShort)
  {
    learnPanelUnitTest(project, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT, _DONT_LEARN_TEMPLATE);
  }

  /**
   * Learns a panel.  Be sure to have an ImageSet selected before
   * calling this.
   *
   * @author Sunit Bhalla
   * @author ShengChuan - Clear Tombstone - Overload function for learn template image use.
   */
  protected void learnPanelUnitTest(Project project,
                                    List<ImageSetData> imageSetDataList,
                                    boolean learnSubtypes,
                                    boolean learnShort,
                                    boolean learnTemplate)
  {
    Assert.expect(project != null);


    Config config = Config.getInstance();
    boolean savedOnlineWorkstationSetting = (Boolean)config.getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);

    try
    {
      StatisticalTuningEngine learningEngine = StatisticalTuningEngine.getInstance();
      config.setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images
      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      InspectionEngine.getInstance().setPanelInspectionSettings(panelInspectionSettings);

      learningEngine.learnPanel(project, imageSetDataList, learnSubtypes, learnShort, learnTemplate);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }
  }

  /**
   * @author Sunit Bhalla
   * @author Patrick Lacz
   */
  protected void verifySettingIsNotAtDefault(Subtype subtype, AlgorithmSettingEnum setting)
  {
    Assert.expect(subtype != null);
    Assert.expect(setting != null);

    Object algorithmSettingValue = subtype.getAlgorithmSettingValue(setting);
    if (algorithmSettingValue instanceof Number)
    {
      float value = ((Number)algorithmSettingValue).floatValue();
      float defaultSettingValue = ((Number)subtype.getAlgorithmSettingDefaultValue(setting)).floatValue();
      String expectString = "Setting: " + setting.getName() + " Default: " + defaultSettingValue + " Value: " + value;
      Expect.expect((MathUtil.fuzzyEquals(value, defaultSettingValue) == false), expectString);
    }
    else
      Expect.expect(false, "unknown class of algorithm setting value");
  }

  /**
   * @author Sunit Bhalla
   */
  protected void verifyAllSettingsAreNotAtDefault(Subtype subtype, List<AlgorithmSettingEnum> settings)
  {
    Assert.expect(subtype != null);
    Assert.expect(settings != null);

    for (AlgorithmSettingEnum setting : settings)
    {
      verifySettingIsNotAtDefault(subtype, setting);
    }
  }


  /**
   * @author Sunit Bhalla
   * @author Patrick Lacz
   */
  protected void verifySettingIsAtDefault(Subtype subtype, AlgorithmSettingEnum setting)
  {
    Assert.expect(subtype != null);
    Assert.expect(setting != null);

    Object algorithmSettingValue = subtype.getAlgorithmSettingValue(setting);
    if (algorithmSettingValue instanceof Number)
    {
      float value = ((Number)algorithmSettingValue).floatValue();
      float defaultSettingValue = ((Number)subtype.getAlgorithmSettingDefaultValue(setting)).floatValue();
      String expectString = "Setting: " + setting.getName() + " Default: " + defaultSettingValue + " Value: " + value;
      Expect.expect(MathUtil.fuzzyEquals(value, defaultSettingValue), expectString);
    }
    else
      Expect.expect(false, "unknown class of algorithm setting value");
  }

  /**
   * @author Sunit Bhalla
   */
  protected void verifyAllSettingsAreAtDefault(Subtype subtype, List<AlgorithmSettingEnum> settings)
  {
    Assert.expect(subtype != null);
    Assert.expect(settings != null);

    for (AlgorithmSettingEnum setting : settings)
    {
      verifySettingIsAtDefault(subtype, setting);
    }
  }

  /**
   * Add an image set for a subtype to the test program.  Note that this does not generate images; call
   * generatePseduoImageSet() after a call to this method if images need to be generated.
   * @author Sunit Bhalla
   */
  protected ImageSetData createImageSetAndSetProgramFilters(Project project,
                                                            Pad pad,
                                                            String imageSetLocation,
                                                            boolean populatedBoard)
  {
    Assert.expect(project != null);
    Assert.expect(pad != null);

    TestProgram testProgram = project.getTestProgram();

    ImageSetData imageSetData = new ImageSetData();
    imageSetData.setBoardPopulated(populatedBoard);
    imageSetData.setPadToAcquireImagesFor(pad);
    imageSetData.setImageSetName(imageSetLocation);
    imageSetData.setProjectName(project.getName());
    imageSetData.setTestProgramVersionNumber(project.getTestProgramVersion());
    imageSetData.setUserDescription("A description");
    imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.SUBTYPE);
    imageSetData.setMachineSerialNumber("123");

    testProgram.clearFilters();
      testProgram.addFilter(true);
    testProgram.addFilter(imageSetData.getPadToAcquireImagesFor());

    return imageSetData;
  }


  /**
   * Add an image set for a subtype to the test program.  Note that this does not generate images; call
   * generatePseduoImageSet() after a call to this method if images need to be generated.
   * @author Sunit Bhalla
   */
  protected ImageSetData createImageSetAndSetProgramFilters(Project project,
                                                            Subtype subtype,
                                                            String imageSetLocation,
                                                            boolean populatedBoard)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    TestProgram testProgram = project.getTestProgram();

    ImageSetData imageSetData = new ImageSetData();
    imageSetData.setBoardPopulated(populatedBoard);
    imageSetData.setSubtypeToAcquireImagesFor(subtype);
    imageSetData.setImageSetName(imageSetLocation);
    imageSetData.setProjectName(project.getName());
    imageSetData.setTestProgramVersionNumber(project.getTestProgramVersion());
    imageSetData.setUserDescription("A description");
    imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.SUBTYPE);
    imageSetData.setMachineSerialNumber("123");

    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(imageSetData.getSubtypeToAcquireImagesFor());

    return imageSetData;
  }

  /**
   * Add an image set to the test program.  Note that this does not generate images; call
   * generatePseduoImageSet() after a call to this method if images need to be generated.
   * @author Sunit Bhalla
   */
  protected ImageSetData createImageSetAndSetProgramFilters(Project project,
                                                            String imageSetLocation,
                                                            boolean populatedBoard)
  {
    Assert.expect(project != null);

    TestProgram testProgram = project.getTestProgram();

    ImageSetData imageSetData = new ImageSetData();
    imageSetData.setBoardPopulated(populatedBoard);
    imageSetData.setCollectImagesOnAllBoardInstances(true);
    imageSetData.setImageSetName(imageSetLocation);
    imageSetData.setProjectName(project.getName());
    imageSetData.setTestProgramVersionNumber(project.getTestProgramVersion());
    imageSetData.setUserDescription("A description");
    imageSetData.setImageSetTypeEnum(ImageSetTypeEnum.PANEL);
    imageSetData.setMachineSerialNumber("123");

    // Make sure no filters set so all images from this board can be used.
    testProgram.clearFilters();
    testProgram.addFilter(true);

    return imageSetData;
  }


  /**
   * Generates pseudo image data for imageSetData
   * @author Sunit Bhalla
   */
  protected void generatePseudoImageSet(Project project, ImageSetData imageSetData)
  {
    Assert.expect(project != null);
    Assert.expect(imageSetData != null);
    final boolean NOT_VERBOSE_MODE = false;
    try
    {
      final ImageSetGenerator generator = ImageSetGenerator.getInstance();
      generator.generatePseudoImageSet(project, imageSetData, NOT_VERBOSE_MODE);
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * Generates image data with all white images
   * @author Sunit Bhalla
   */
  protected void generateWhiteImageSet(Project project, ImageSetData imageSetData)
  {
    Assert.expect(project != null);
    Assert.expect(imageSetData != null);
    final boolean NOT_VERBOSE_MODE = false;
    try
    {
      ImageSetGenerator generator = ImageSetGenerator.getInstance();
      generator.setCreateWhiteImages(true);
      generator.generatePseudoImageSet(project, imageSetData, NOT_VERBOSE_MODE);
      generator.setCreateWhiteImages(false);
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * Generates image data with all black images
   * @author Sunit Bhalla
   */
  protected void generateBlackImageSet(Project project, ImageSetData imageSetData)
  {
    Assert.expect(project != null);
    Assert.expect(imageSetData != null);
    final boolean NOT_VERBOSE_MODE = false;
    try
    {
      ImageSetGenerator generator = ImageSetGenerator.getInstance();
      generator.setCreateBlackImages(true);
      generator.generatePseudoImageSet(project, imageSetData, NOT_VERBOSE_MODE);
      generator.setCreateBlackImages(false);
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * Generates image data with random shapes
   *
   * @author Peter Esbensen
   */
  protected void generateRandomImageSet(Project project, ImageSetData imageSetData)
  {
    Assert.expect(project != null);
    Assert.expect(imageSetData != null);
    final boolean NOT_VERBOSE_MODE = false;
    try
    {
      ImageSetGenerator generator = ImageSetGenerator.getInstance();
      generator.setCreateRandomImages(true);
      generator.generatePseudoImageSet(project, imageSetData, NOT_VERBOSE_MODE);
      generator.setCreateRandomImages(false);
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * Generates image data with random shapes
   *
   * @author Peter Esbensen
   */
  protected void generateRegularImageSet(Project project, ImageSetData imageSetData)
  {
    Assert.expect(project != null);
    Assert.expect(imageSetData != null);
    final boolean NOT_VERBOSE_MODE = false;
    try
    {
      ImageSetGenerator generator = ImageSetGenerator.getInstance();
      generator.generatePseudoImageSet(project, imageSetData, NOT_VERBOSE_MODE);
    }
    catch (DatastoreException dex)
    {
      dex.printStackTrace();
      Assert.expect(false);
    }
  }

  /**
   * @author Sunit Bhalla
   */
  protected int getNumberOfMeasurements(Subtype subtype, SliceNameEnum slice, MeasurementEnum measurementEnum) throws
      DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(measurementEnum != null);

    int returnValue = 0;

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = subtype.getPanel().getProject().getAlgorithmSubtypeLearningReaderWriter();
    algorithmSubtypeLearning.open();

    float[] databaseData = subtype.getLearnedJointMeasurementValues(slice, measurementEnum, true, true);
    returnValue = databaseData.length;

    algorithmSubtypeLearning.close();
    return returnValue;
  }

  /**
   * This test checks the error conditions of learning.
   * @author Sunit Bhalla
   */
  public void testLearnSubtypesAsserts(Project project) throws DatastoreException
  {
    Subtype subtype = new Subtype();
    TestProgram testProgram = project.getTestProgram();
    ManagedOfflineImageSet typicalBoardImages = new ManagedOfflineImageSet(testProgram, new ArrayList<ImageSetData>());
    ManagedOfflineImageSet unloadedBoardImages = new ManagedOfflineImageSet(testProgram, new ArrayList<ImageSetData>());

    Algorithm measureAlgorithm = InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);

    try
    {
      measureAlgorithm.learnAlgorithmSettings(null, typicalBoardImages, unloadedBoardImages);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      // Do nothing--assertException is expected to occur.
    }

    try
    {
      measureAlgorithm.learnAlgorithmSettings(subtype, null, unloadedBoardImages);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      // Do nothing--assertException is expected to occur.
    }

    try
    {
      measureAlgorithm.learnAlgorithmSettings(subtype, typicalBoardImages, null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      // Do nothing--assertException is expected to occur.
    }
  }

  /**
   * This tests checks that learning isn't done if only unloaded boards are used to learn and if no data exist
   * in the learning database.
   * @author Sunit Bhalla
   */
  protected void testLearningOnlyUnloadedBoards(Project project,
                                                Subtype subtype,
                                                List<AlgorithmSettingEnum> learnedSettings) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(learnedSettings != null);

    Panel panel = project.getPanel();

    deleteLearnedData(subtype);

    // Create an image run that has only unloaded boards for the subtype
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "testUnpopulatedImageSet",
                                                                   UNPOPULATED_BOARD);
    generatePseudoImageSet(project, imageSetData);
    imageSetDataList.add(imageSetData);
    //ShengChuan - Clear tombstone
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT, _LEARN_EXPECTED_IMAGES, _DONT_LEARN_TEMPLATE);

    // Check that no subtypes were learned
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == 0);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == 0);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithExpectedImageLearned() == 0);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithImageTemplateLearned() == 0);

    deleteLearnedData(subtype);

    testLearnSubtypesAsserts(project);
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  protected void checkLearnedValues(Subtype subtype,
                                    Map<AlgorithmSettingEnum, Float> algorithmSettingToExpectedValueMap)
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmSettingToExpectedValueMap != null);

    for (Map.Entry<AlgorithmSettingEnum, Float> settingToExpectedValue : algorithmSettingToExpectedValueMap.entrySet())
    {
      float expectedValue = settingToExpectedValue.getValue();
      AlgorithmSettingEnum algorithmSettingEnum = settingToExpectedValue.getKey();
      Object algorithmSettingValue = subtype.getAlgorithmSettingValue(algorithmSettingEnum);
      Expect.expect(algorithmSettingValue != null);

      float learnedValue = Float.NaN;
      if (algorithmSettingValue instanceof Number)
        learnedValue = ((Number)algorithmSettingValue).floatValue();
      else
        Expect.expect(false, "unknown class for algorithm setting");

      String expectString = "Setting: " + algorithmSettingEnum.getName() + " Expected: " + expectedValue + " Actual: " + learnedValue;
      float tolerance = 0.01f * expectedValue;
      Expect.expect(MathUtil.fuzzyEquals(learnedValue, expectedValue, tolerance), expectString);
    }
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  protected void checkLearnedValuesAsPercentOfExpected(Subtype subtype,
      Map<AlgorithmSettingEnum, Float> algorithmSettingToExpectedValueMap,
      float percentTolerance)
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmSettingToExpectedValueMap != null);
    Assert.expect(percentTolerance > 0.0f);

    for (Map.Entry<AlgorithmSettingEnum, Float> settingToExpectedValue : algorithmSettingToExpectedValueMap.entrySet())
    {
      float expectedValue = settingToExpectedValue.getValue();
      AlgorithmSettingEnum algorithmSettingEnum = settingToExpectedValue.getKey();
      Object algorithmSettingValue = subtype.getAlgorithmSettingValue(algorithmSettingEnum);
      Expect.expect(algorithmSettingValue != null);

      float learnedValue = Float.NaN;
      if (algorithmSettingValue instanceof Number)
        learnedValue = ((Number)algorithmSettingValue).floatValue();
      else
        Expect.expect(false, "unknown class for algorithm setting");

      String expectString = "Setting: " + algorithmSettingEnum.getName() + " Expected: " + expectedValue + " Actual: " + learnedValue;
      float tolerance = 0.01f * percentTolerance * expectedValue;
      Expect.expect(MathUtil.fuzzyEquals(learnedValue, expectedValue, tolerance), expectString);
    }
  }

}
