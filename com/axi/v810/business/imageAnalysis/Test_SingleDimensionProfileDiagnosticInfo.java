package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

public class Test_SingleDimensionProfileDiagnosticInfo extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  private Test_SingleDimensionProfileDiagnosticInfo()
  {
    // Do nothing...
  }

  /**
   * Tests the 'happy path'.
   *
   * @author Matt Wharton
   */
  private void testHappyPath()
  {
    testConstructorAndAccessorMethods();
  }

  /**
   * @author Matt Wharton
   */
  private void testConstructorAndAccessorMethods()
  {
    // Test out the constructor and relevant getters.
    ProfileTypeEnum profileType = ProfileTypeEnum.SOLDER_PROFILE;
    LocalizedString axisDescription = new LocalizedString("axis_key_1", null);
    Pair<Float, Float> axisExtents = new Pair<Float,Float>(-10f, 10f);
    MeasurementUnitsEnum profileUnits = MeasurementUnitsEnum.MILLIMETERS;
    SingleDimensionProfileDiagnosticInfo profileDiagInfo = new SingleDimensionProfileDiagnosticInfo("padName",
                                                                                                    profileType,
                                                                                                    axisDescription,
                                                                                                    axisExtents.getFirst(),
                                                                                                    axisExtents.getSecond(),
                                                                                                    profileUnits);

    Expect.expect(profileDiagInfo.getProfileType() == profileType, "Profile type doesn't match!");
    Expect.expect(profileDiagInfo.getNumberOfDimensions() == 1, "Number of dimensions is not 1!");
    Expect.expect(profileDiagInfo.getAxisDescriptions().get(0) == axisDescription, "Axis descriptions don't match!");
    Expect.expect(profileDiagInfo.getAxisExtents().get(0).equals(axisExtents), "Axis extents don't match!");
    Expect.expect(profileDiagInfo.getProfileUnits() == profileUnits, "Profile units don't match!");

    // Test that adding data points works properly.
    MeasurementRegionEnum measurementRegionEnum1 = MeasurementRegionEnum.PAD_REGION;
    float dataPoint1 = 1f;
    profileDiagInfo.addDataPoint(measurementRegionEnum1, dataPoint1);
    MeasurementRegionEnum measurementRegionEnum2 = MeasurementRegionEnum.PAD_REGION;
    float dataPoint2 = 2f;
    profileDiagInfo.addDataPoint(measurementRegionEnum2, dataPoint2);

    List<Pair<MeasurementRegionEnum, Float>> data = profileDiagInfo.getData();
    Expect.expect(data.get(0).getFirst() == measurementRegionEnum1, "Measurement region enum doesn't match!");
    Expect.expect(data.get(0).getSecond() == dataPoint1, "Data point doesn't match!");
    Expect.expect(data.get(1).getFirst() == measurementRegionEnum2, "Measurement region enum doesn't match!");
    Expect.expect(data.get(1).getSecond() == dataPoint2, "Data point doesn't match!");

    // Test that data clearing works properly.
    profileDiagInfo.clearData();
    Expect.expect(profileDiagInfo.getData().isEmpty(), "Data did not clear!");
  }

  /**
   * Tests that the asserts are working properly.
   */
  private void testAsserts()
  {
    final ProfileTypeEnum profileType = ProfileTypeEnum.SOLDER_PROFILE;
    final LocalizedString axisDescription = new LocalizedString("axis_key_1", null);
    final Pair<Float, Float> axisExtents = new Pair<Float,Float>(-10f, 10f);
    final MeasurementUnitsEnum profileUnits = MeasurementUnitsEnum.MILLIMETERS;

    // Verify that the constructor asserts when passed null parameters.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new SingleDimensionProfileDiagnosticInfo("padName",
                                                 null,
                                                 axisDescription,
                                                 axisExtents.getFirst(),
                                                 axisExtents.getSecond(),
                                                 profileUnits);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new SingleDimensionProfileDiagnosticInfo("padName",
                                                 profileType,
                                                 null,
                                                 axisExtents.getFirst(),
                                                 axisExtents.getSecond(),
                                                 profileUnits);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new SingleDimensionProfileDiagnosticInfo("padName",
                                                 profileType,
                                                 null,
                                                 axisExtents.getFirst(),
                                                 axisExtents.getSecond(),
                                                 null);
      }
    });

    final SingleDimensionProfileDiagnosticInfo profileDiagInfo = new SingleDimensionProfileDiagnosticInfo("padName",
                                                                                                          profileType,
                                                                                                          axisDescription,
                                                                                                          axisExtents.getFirst(),
                                                                                                          axisExtents.getSecond(),
                                                                                                          profileUnits);
    final float dataPoint1 = 1f;

    // Verify that calling addDataPoint() with null params causes asserts.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        profileDiagInfo.addDataPoint(null, dataPoint1);
      }
    });
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // Test the 'happy path'.
    testHappyPath();

    // Test the asserts.
    testAsserts();
  }

  /**
   * Test fixture entry point.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_SingleDimensionProfileDiagnosticInfo());
  }
}
