package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;
import com.axi.util.image.*;

/**
 * Represents a measurement region that is passed up as diagnostic info.
 * In practice the measurement region is really just a java.awt.Shape that is tied to
 * a MeasurementRegionEnum.
 *
 * @author Matt Wharton
 */
public class MeasurementRegionDiagnosticInfo extends DiagnosticInfo
{
  private java.awt.Shape _diagnosticRegion = null;
  private MeasurementRegionEnum _measurementRegionEnum = null;
  private String _label = null;

  /**
   * Constructor.
   *
   * @param diagnosticRegion The Shape which represents the region.
   * @param measurementRegionEnum Indicates what type of region this is.
   * @author Matt Wharton
   */
  public MeasurementRegionDiagnosticInfo(java.awt.Shape diagnosticRegion, MeasurementRegionEnum measurementRegionEnum)
  {
    Assert.expect(diagnosticRegion != null);
    Assert.expect(measurementRegionEnum != null);
    _diagnosticRegion = diagnosticRegion;
    _measurementRegionEnum = measurementRegionEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public MeasurementRegionDiagnosticInfo(RegionOfInterest regionOfInterest, MeasurementRegionEnum measurementRegionEnum)
  {
    Assert.expect(regionOfInterest != null);
    Assert.expect(measurementRegionEnum != null);

    _diagnosticRegion = regionOfInterest.getShape();
    _measurementRegionEnum = measurementRegionEnum;
  }


  /**
   * Gets the associated measurement region Shape.
   *
   * @return The shape representing the measurement region.
   * @author Matt Wharton
   */
  public java.awt.Shape getDiagnosticRegion()
  {
    Assert.expect(_diagnosticRegion != null);
    return _diagnosticRegion;
  }

  /**
   * Sets the associated measurement region Shape.
   *
   * @param diagnosticRegion The associated Shape for the measurement region.
   * @author Matt Wharton
   */
  void setDiagnosticRegion(java.awt.Shape diagnosticRegion)
  {
    Assert.expect(diagnosticRegion != null);
    _diagnosticRegion = diagnosticRegion;
  }

  /**
   * Gets the associated MeasurementRegionEnum.
   *
   * @return The associated MeasurementRegionEnum.
   * @author Matt Wharton
   */
  public MeasurementRegionEnum getMeasurementRegionEnum()
  {
    Assert.expect(_measurementRegionEnum != null);
    return _measurementRegionEnum;
  }

  /**
   * Sets the associated MeasurementRegionEnum.
   *
   * @param measurementRegionEnum The associated MeasurementRegionEnum.
   * @author Matt Wharton
   */
  public void setMeasurementRegionEnum(MeasurementRegionEnum measurementRegionEnum)
  {
    Assert.expect(measurementRegionEnum != null);
    _measurementRegionEnum = measurementRegionEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void setLabel(String text)
  {
    Assert.expect(text != null);
    _label = text;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasLabel()
  {
    return (_label != null);
  }

  /**
   * @author Patrick Lacz
   */
  public String getLabel()
  {
    Assert.expect(_label != null);
    return _label;
  }
}
