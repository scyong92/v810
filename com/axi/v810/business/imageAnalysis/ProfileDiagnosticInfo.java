package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Matt Wharton
 */
public abstract class ProfileDiagnosticInfo extends DiagnosticInfo
{
  protected int _numberOfDimensions = -1;

  private String _name;
  private ProfileTypeEnum _profileTypeEnum;
  private List<LocalizedString> _axisDescriptions;
  private List<Pair<Float, Float>> _axisExtents;
  private MeasurementUnitsEnum _profileUnits;

  /**
   * @author Peter Esbensen
   */
  public String getNameOfJointOrComponent()
  {
    Assert.expect(_name != null);
    return _name;
  }

  /**
   * @author Matt Wharton
   */
  protected ProfileDiagnosticInfo(String name,
                                  ProfileTypeEnum profileTypeEnum,
                                  int numberOfDimensions,
                                  List<LocalizedString> axisDescriptions,
                                  List<Pair<Float, Float>> axisExtents,
                                  MeasurementUnitsEnum profileUnits)
  {
    Assert.expect(name != null, "Profile must have a name");
    Assert.expect(profileTypeEnum != null);
    Assert.expect(numberOfDimensions > 0, "Number of dimensions must be positive!");
    Assert.expect(axisDescriptions != null);
    Assert.expect(axisExtents != null);
    Assert.expect(axisDescriptions.size() == numberOfDimensions, "Number of axis descriptions does NOT match number of dimensions");
    Assert.expect(axisExtents.size() == numberOfDimensions, "Number of axis extents does NOT match number of dimensions");
    Assert.expect(axisDescriptions.contains(null) == false, "Axis descriptions list cannot contain nulls!");
    Assert.expect(axisExtents.contains(null) == false, "Axis extents list cannot contain nulls!");
    Assert.expect(profileUnits != null, "Profile units cannot be null!");

    _name = name;
    _profileTypeEnum = profileTypeEnum;
    _numberOfDimensions = numberOfDimensions;
    _axisDescriptions = axisDescriptions;
    _axisExtents = axisExtents;
    _profileUnits = profileUnits;
  }

  /**
   * @author Matt Wharton
   */
  public int getNumberOfDimensions()
  {
    Assert.expect(_numberOfDimensions != -1);

    return _numberOfDimensions;
  }

  /**
   * @author Matt Wharton
   */
  public ProfileTypeEnum getProfileType()
  {
    Assert.expect(_profileTypeEnum != null);

    return _profileTypeEnum;
  }

  /**
   * @author Matt Wharton
   */
  public List<LocalizedString> getAxisDescriptions()
  {
    Assert.expect(_axisDescriptions != null);

    return _axisDescriptions;
  }

  /**
   * @author Matt Wharton
   */
  public List<Pair<Float, Float>> getAxisExtents()
  {
    Assert.expect(_axisExtents != null);

    return _axisExtents;
  }

  /**
   * @author Matt Wharton
   */
  public MeasurementUnitsEnum getProfileUnits()
  {
    Assert.expect(_profileUnits != null);

    return _profileUnits;
  }

  /**
   * Creates a one dimensional ProfileDiagnosticInfo object out of the given data.
   *
   * @param dataMin is the suggested minimum value that should be used when visualizing the data
   * @param dataMax is the suggested maximum value that should be used when visualizing the data
   * @param measurementRegionEnumForAllData is the MeasurementRegionEnum to use for all data.  If you need different Enums for different parts of the profile, you can't use this method.
   *
   * @author Sunit Bhalla
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public static SingleDimensionProfileDiagnosticInfo createSingleDimensionProfileDiagnosticInfo(
      String nameOfJointOrComponent,
      ProfileTypeEnum profileTypeEnum,
      LocalizedString axisDescription,
      float dataMin,
      float dataMax,
      float[] data,
      MeasurementRegionEnum measurementRegionEnumForAllData,
      MeasurementUnitsEnum profileUnits)
  {
    Assert.expect(nameOfJointOrComponent != null);
    Assert.expect(profileTypeEnum != null);
    Assert.expect(axisDescription != null);
    Assert.expect(dataMin <= dataMax);
    Assert.expect(data != null);
    Assert.expect(data.length > 0);
    Assert.expect(measurementRegionEnumForAllData != null);
    Assert.expect(profileUnits != null);

    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new HashMap<Pair<Integer, Integer>, MeasurementRegionEnum>();
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, data.length),
                                                  measurementRegionEnumForAllData);

    return createSingleDimensionProfileDiagnosticInfo(nameOfJointOrComponent,
                                                      profileTypeEnum,
                                                      axisDescription,
                                                      dataMin,
                                                      dataMax,
                                                      data,
                                                      profileSubrangeToMeasurementRegionEnumMap,
                                                      profileUnits);
  }

  /**
   * Creates a one dimensional ProfileDiagnosticInfo object out of the given data.
   *
   * @param dataMin is the suggested minimum value that should be used when visualizing the data.
   * @param dataMax is the suggested maximum value that should be used when visualizing the data.
   * @param measurementRegionEnums is an array of MeasurementRegionEnum that describes each data point in the data array

   * @author Peter Esbensen
   */
  public static SingleDimensionProfileDiagnosticInfo createSingleDimensionProfileDiagnosticInfo(
      String nameOfJointOrComponent,
      ProfileTypeEnum profileTypeEnum,
      LocalizedString axisDescription,
      float dataMin,
      float dataMax,
      float[] data,
      MeasurementRegionEnum[] measurementRegionEnums,
      MeasurementUnitsEnum profileUnits)
  {
    Assert.expect(profileTypeEnum != null);
    Assert.expect(axisDescription != null);
    Assert.expect(dataMin <= dataMax);
    Assert.expect(data != null);
    Assert.expect(data.length > 0);
    Assert.expect(measurementRegionEnums != null);
    Assert.expect(profileUnits != null);

    SingleDimensionProfileDiagnosticInfo profileDiagnosticInfo = new SingleDimensionProfileDiagnosticInfo(
        nameOfJointOrComponent,
        profileTypeEnum,
        axisDescription,
        dataMin,
        dataMax,
        profileUnits);

    for (int i = 0; i < data.length; ++i)
    {
      Assert.expect(measurementRegionEnums[i] != null);
      profileDiagnosticInfo.addDataPoint(measurementRegionEnums[i], data[i]);
    }

    return profileDiagnosticInfo;
  }


  /**
   * Creates a one dimensional ProfileDiagnosticInfo object out of the given data.
   *
   * @param dataMin is the suggested minimum value that should be used when visualizing the data.
   * @param dataMax is the suggested maximum value that should be used when visualizing the data.
   * @param profileSubrangeToMeasurementRegionEnumMap is a map of array index subranges in [start, end) form to
   * the applicable MeasurementRegionEnum.

   * @author Matt Wharton
   */
  public static SingleDimensionProfileDiagnosticInfo createSingleDimensionProfileDiagnosticInfo(
      String nameOfJointOrComponent,
      ProfileTypeEnum profileTypeEnum,
      LocalizedString axisDescription,
      float dataMin,
      float dataMax,
      float[] data,
      Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap,
      MeasurementUnitsEnum profileUnits)
  {
    Assert.expect(profileTypeEnum != null);
    Assert.expect(axisDescription != null);
    Assert.expect(dataMin <= dataMax);
    Assert.expect(data != null);
    Assert.expect(data.length > 0);
    Assert.expect(profileUnits != null);

    // Keep track of which profile bins are assigned to a MeasurementRegionEnum.
    MeasurementRegionEnum[] measurementRegionEnumBins = new MeasurementRegionEnum[data.length];

    for (Map.Entry<Pair<Integer, Integer>, MeasurementRegionEnum> mapEntry : profileSubrangeToMeasurementRegionEnumMap.entrySet())
    {
      Pair<Integer, Integer> subrange = mapEntry.getKey();
      int startIndex = subrange.getFirst();
      int endIndex = subrange.getSecond();
      MeasurementRegionEnum measurementRegionEnum = mapEntry.getValue();
      Arrays.fill(measurementRegionEnumBins, startIndex, endIndex, measurementRegionEnum);
    }

    // Now, let's create the ProfileDiagnosticInfo and populate it.
    SingleDimensionProfileDiagnosticInfo profileDiagnosticInfo = new SingleDimensionProfileDiagnosticInfo(
        nameOfJointOrComponent,
        profileTypeEnum,
        axisDescription,
        dataMin,
        dataMax,
        profileUnits);

    for (int i = 0; i < data.length; ++i)
    {
      Assert.expect(measurementRegionEnumBins[i] != null);
      profileDiagnosticInfo.addDataPoint(measurementRegionEnumBins[i], data[i]);
    }

    return profileDiagnosticInfo;
  }

  /**
   * Clears out the raw data contained in this ProfileDiagnosticInfo.
   *
   * @author Matt Wharton
   */
  abstract void clearData();
}
