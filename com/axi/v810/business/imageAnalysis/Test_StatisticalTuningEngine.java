package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.gridArray.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.util.*;
import com.axi.v810.business.testExec.InspectionEngine;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.datastore.config.SoftwareConfigEnum;
import com.axi.v810.business.testExec.PanelInspectionSettings;

/**
 * @author Sunit Bhalla
 */
public class Test_StatisticalTuningEngine extends AlgorithmUnitTest
{
  private static final String _BGA_SUBTYPE_1 = "bg00001_CollapsibleBGA";
  private static final String _BGA_SUBTYPE_2 = "bg00002_CollapsibleBGA";
  private static final String _COMPONENT_IN_BGA_SUBTYPE_1 = "U27";

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_StatisticalTuningEngine());
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {

    boolean printDebugInformation = false;

    try
    {

      TimerUtil wholeTestTimer = new TimerUtil();
      wholeTestTimer.start();

      TimerUtil timer = new TimerUtil();

      // get the project
      timer.start();
      Project project =  getProject("FAMILIES_ALL_RLV");
      timer.stop();
      if (printDebugInformation)
        System.out.println("The project created: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      // delete all learned data
      timer.start();
      deleteAllLearnedData(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("deleteOldDatabaseData done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      // test learn subtype and delete learned data
      timer.start();
      testLearnSubtypeAndDeleteLearnedData(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testLearnSubtype done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      // test relearn subtype
      timer.start();
      testRelearnSubtype(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testLearnSubtype done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      // test relearn joint type
      timer.start();
      testRelearnJointType(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testLearnSubtype done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      // test relearn panel
      timer.start();
      testRelearnPanel(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testLearnPanel done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      wholeTestTimer.stop();

      if (printDebugInformation)
        System.out.println("Time for test (min): " + wholeTestTimer.getElapsedTimeInMillis() / 60000.0);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }

  }

  /**
   * @author Peter Esbensen
   */
  private void setPanelInspectionSettings()
  {
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.clearAllInspectionSettings();
    InspectionEngine.getInstance().setPanelInspectionSettings(panelInspectionSettings);
  }

  /**
   * This test verifies that relearning can be done on a subtype.  This is not a complete test but it checks the typical
   * flow.

   * @author Peter Esbensen
   */
  protected void testRelearnSubtype(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    // Check that no joint type learning exists for the subtype
    Assert.expect(subtype.isJointSpecificDataLearned() == false);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);

    boolean doSubtypeLearning = false;
    boolean doShortLearning = true;

    // learn the subtype
    learnSubtypeUnitTest(project, subtype, imageSetDataList, doSubtypeLearning, doShortLearning);

    // Check to make sure that shortlearning exists now
    Expect.expect(subtype.isJointSpecificDataLearned() == true);

    // There should be one instance of short learning per joint at this point
    int numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 1 );

    // learn the subtype again
    learnSubtypeUnitTest(project, subtype, imageSetDataList, doSubtypeLearning, doShortLearning);

    // Now there should be two sets of learning in there
    numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 2 );

    // Now "relearn" the subtype
    boolean savedOnlineWorkstationSetting = (Boolean)Config.getInstance().getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);
    try
    {
      Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images

      setPanelInspectionSettings();

      StatisticalTuningEngine.getInstance().relearnJointSpecificDataForSubtype(project, subtype, null, imageSetDataList, false);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }

    // make sure we just have one learning run in there now
    numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 1 );

    // clean up this test by deleting the learned data
    deleteLearnedData(subtype);
  }

  /**
   * Figure out how many instances of short learning profiles exist.  This method depends on knowledge of the Short
   * Learned Data, so if that ever changes in the future, we'll have to update this method accordingly.
   *
   * @author Peter Esbensen
   */
  private int getNumberOfShortLearningRuns(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    Project project = subtype.getPanel().getProject();

    // Make sure learning still exists
    Expect.expect(subtype.isJointSpecificDataLearned() == true);

    // There should only be one set of learning though
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    StatisticalTuningEngine.getInstance().openConnectionToShortsLearningDatabase(project.getTestProgram(), algorithmShortsLearning);

    // Get the first pad in the subtype
    Pad pad = subtype.getPads().get(0);
    Assert.expect(pad != null);
    PadSettings padSettings = pad.getPadSettings();

    // We'll check for short learning at the pad slice
    SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;

    // Now get the number of short learning profiles
    Assert.expect(padSettings.hasShortProfileLearning(sliceNameEnum));
    ShortProfileLearning shortProfileLearning = padSettings.getShortProfileLearning(sliceNameEnum);
    int numberOfShortLearnings = shortProfileLearning.getNumberOfThicknessProfilesLearned();

    algorithmShortsLearning.close();

    return numberOfShortLearnings;
  }

  /**
   * This test verifies that relearning is done properly a joint type.

   * @author Peter Esbensen
   * @author ShengChuan - Clear Tombstone
   */
  protected void testRelearnJointType(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    // Check that no joint type learning exists for the subtype
    Assert.expect(subtype.isJointSpecificDataLearned() == false);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);

    boolean doSubtypeLearning = false;
    boolean doShortLearning = true;
    boolean doTemplateLearning = false;//ShengChuan - Clear Tombstone

    // learn the joint type
    JointTypeEnum jointTypeEnum = JointTypeEnum.COLLAPSABLE_BGA;
    //ShengChuan - Clear Tombstone
    learnJointTypeUnitTest(project, jointTypeEnum, imageSetDataList, doSubtypeLearning, doShortLearning, doTemplateLearning);

    // Check to make sure that short learning exists now
    int numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 1 );

    // learn the joint type again
    //ShengChuan - Clear Tombstone
    learnJointTypeUnitTest(project, jointTypeEnum, imageSetDataList, doSubtypeLearning, doShortLearning, doTemplateLearning);

    // Now there should be two sets of short learning
    numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 2 );

    // Now "relearn" the joint type
    boolean savedOnlineWorkstationSetting = (Boolean)Config.getInstance().getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);
    try
    {
      Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images

      setPanelInspectionSettings();

      StatisticalTuningEngine.getInstance().relearnJointSpecificDataForJointType(project, jointTypeEnum, null, imageSetDataList, false);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }

    // Now there should be one set of learning
    numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 1 );

    // clean up this test by deleting the learned data
    deleteLearnedData(subtype);
  }

  /**
   * This test verifies that relearning is done properly on a panel.
   * @author Peter Esbensen
   */
  protected void testRelearnPanel(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    // Check that no joint type learning exists for the subtype
    Assert.expect(subtype.isJointSpecificDataLearned() == false);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);

    boolean doSubtypeLearning = false;
    boolean doShortLearning = true;

    // learn the panel
    learnPanelUnitTest(project, imageSetDataList, doSubtypeLearning, doShortLearning);

    // Check to make sure that short learning exists now
    int numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 1 );

    // learn the panel again
    learnPanelUnitTest(project, imageSetDataList, doSubtypeLearning, doShortLearning);

    // Now there should be two sets of short learning
    numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 2 );

    // Now "relearn" the panel
    boolean savedOnlineWorkstationSetting = (Boolean)Config.getInstance().getValue(SoftwareConfigEnum.ONLINE_WORKSTATION);
    try
    {
      Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, false); // go to offline mode to run off saved images

      setPanelInspectionSettings();

      StatisticalTuningEngine.getInstance().relearnJointSpecificDataForPanel(project, imageSetDataList, false);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    finally
    {
      try
      {
        Config.getInstance().setValue(SoftwareConfigEnum.ONLINE_WORKSTATION, savedOnlineWorkstationSetting); // reset to saved state
      }
      catch (DatastoreException dex)
      {
        dex.printStackTrace();
      }
    }

    // Now there should be one set of learning
    numberOfShortLearningRuns = getNumberOfShortLearningRuns(subtype);
    Expect.expect( numberOfShortLearningRuns == 1 );

    // clean up this test by deleting the learned data
    deleteLearnedData(subtype);
  }


  /**
   * @author Sunit Bhalla
   * This test verifies that learning can be done on a subtype.  This is not a complete test (that is in
   * Test_GridArrayLearning), but it checks the typical flow of subtype learning.
   */
  protected void testLearnSubtypeAndDeleteLearnedData(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    // Check that the settings are set to the default values
    verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // Check that settings changed
    verifySettingIsNotAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    verifySettingIsNotAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

    // Delete subtype data; make sure values are back to their defaults
    AlgorithmSubtypesLearningReaderWriter algorithmSubtypesLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    algorithmSubtypesLearning.open();
    subtype.deleteLearnedAlgorithmSettingsData();
    subtype.setLearnedSettingsToDefaults();
    algorithmSubtypesLearning.close();

    verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

    int numberOfMidballDiametersInDatabase = GridArrayMeasurementAlgorithm.numberOfMeasurements(subtype,
                                                                                                SliceNameEnum.MIDBALL,
                                                                                                MeasurementEnum.GRID_ARRAY_DIAMETER);
    Assert.expect(numberOfMidballDiametersInDatabase == 0);

    deleteLearnedData(subtype);
  }


  /**
   * @author Sunit Bhalla
   * Tests learning of one joint type (GridArray).
   */
  protected void testLearnJointType(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    StatisticalTuningEngine learningEngine = StatisticalTuningEngine.getInstance();
    Panel panel = project.getPanel();

    JointTypeEnum jointType = JointTypeEnum.COLLAPSABLE_BGA;

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);
    //ShengChuan - Clear Tombstone
    learnJointTypeUnitTest(project, JointTypeEnum.COLLAPSABLE_BGA, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT, _DONT_LEARN_TEMPLATE);

    // For each subtype in jointType, make sure that nominal diameter has been changed.
    for (Subtype subtype : panel.getInspectedSubtypes(jointType))
    {
      if (subtype.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA))
      {
        verifySettingIsNotAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
      }
    }

    deleteAllLearnedData(project);
  }

  /**
   * @author Sunit Bhalla
   * This tests learning on an entire panel.
   */
  protected void testLearnPanel(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    learnPanelUnitTest(project, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // For each subtype in BGA, make sure that nominal diameter has been changed.
    for (Subtype subtype : panel.getInspectedSubtypes(JointTypeEnum.COLLAPSABLE_BGA))
    {
      // If these subtypes were learned, the nominal diameter should different from the default.
      if (subtype.getJointTypeEnum().equals(JointTypeEnum.COLLAPSABLE_BGA))
      {
        verifySettingIsNotAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
      }
    }

    // Check that no subtypes are unlearned; check list is empty
    int totalSubtypes = panel.getNumberOfInspectedSubtypes();
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == totalSubtypes);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == totalSubtypes);

    int sizeOfUnlearnedSubtypesList = panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned().size();
    Expect.expect(sizeOfUnlearnedSubtypesList == 0);

    int sizeOfUnlearnedShortList = panel.getInspectedSubtypesWithoutShortLearned().size();
    Expect.expect(sizeOfUnlearnedShortList == 0);

    deleteAllLearnedData(project);

  }

  /**
   * @author Sunit Bhalla
   * This tests checks that learning isn't done if only unloaded boards are used to learn, and if no data exist
   * in the learning database.
   */
  protected void testOnlyUnloadedBoards(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
    // Create image run and learn.  Image run has only unloaded boards.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "Unloaded_bg00001_U",
                                                                   UNPOPULATED_BOARD);
    imageSetDataList.add(imageSetData);

    generatePseudoImageSet(project, imageSetData);


    // createNormalImageSet(project, subtype, CREATE_UNPOPULATED_BOARD);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);


    // Check that no subtypes were learned
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == 0);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == 0);

    // Check that values are at their defaults
    verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

    deleteLearnedData(subtype);
  }


  /**
   * @author Sunit Bhalla
   */
  protected int getNumberOfMeasurements(Subtype subtype, SliceNameEnum slice, MeasurementEnum measurementEnum) throws
      DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(measurementEnum != null);

    int returnValue = 0;

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = subtype.getPanel().getProject().getAlgorithmSubtypeLearningReaderWriter();
    algorithmSubtypeLearning.open();

    float[] databaseData = subtype.getLearnedJointMeasurementValues(slice, measurementEnum, true, true);
    returnValue = databaseData.length;

    algorithmSubtypeLearning.close();
    return returnValue;
  }


  protected void testIncrementalLearning(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();
    float diameter;

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    diameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    int numberOfDatapoints = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);


    // Learn the same subtype again
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // Check learned setting values
    float diameterAfterSecondLearning = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    int numberOfDatapoints2 = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    Expect.expect(MathUtil.fuzzyEquals(diameter, diameterAfterSecondLearning, 0.1));
    Expect.expect((2 * numberOfDatapoints) == numberOfDatapoints2);

    deleteLearnedData(subtype);
  }


  /**
   * @author Sunit Bhalla
   * This test checks the information used in the status window of the learning screen.
   */
  protected void testStatusWindowInformation(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    int totalSubtypes = panel.getNumberOfInspectedSubtypes();

    // Check initial status of board: nothing learned
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == 0);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == 0);
    int sizeOfUnlearnedSubtypesList = panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned().size();
    Expect.expect(sizeOfUnlearnedSubtypesList == totalSubtypes);
    int sizeOfUnlearnedShortsList = panel.getInspectedSubtypesWithoutShortLearned().size();
    Expect.expect(sizeOfUnlearnedShortsList == totalSubtypes);

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    // Check that lists of unlearned subtypes contains the subtype
    Expect.expect(panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned().contains(subtype));
    Expect.expect(panel.getInspectedSubtypesWithoutShortLearned().contains(subtype));

    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // Check number of learned subtypes
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == 1);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == 1);

    // Check lists of unlearned subtypes
    Expect.expect(panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned().contains(subtype) == false);
    Expect.expect(panel.getInspectedSubtypesWithoutShortLearned().contains(subtype) == false);

    // Learn second setting (only subtype; don't learn short)
    Subtype subtype2 = panel.getSubtype(_BGA_SUBTYPE_2);
    imageSetData = createImageSetAndSetProgramFilters(project,
                                                      ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                      POPULATED_BOARD);
    imageSetDataList.clear();
    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype2, imageSetDataList, _LEARN_SUBTYPES, _DONT_LEARN_SHORT);

    // Check number of learned subtypes
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == 2);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == 1);

    // Check lists of unlearned subtypes
    Expect.expect(panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned().contains(subtype2) == false);
    Expect.expect(panel.getInspectedSubtypesWithoutShortLearned().contains(subtype2) == true);

    // Delete subtype data for first subtype; verify lists and counts
    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    algorithmSubtypeLearning.open();
    subtype.deleteLearnedAlgorithmSettingsData();
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == 1);
    Expect.expect(panel.getInspectedSubtypesWithoutAllAlgorithmsExceptShortLearned().contains(subtype) == true);

    subtype.deleteLearnedJointSpecificData();
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == 0);
    Expect.expect(panel.getInspectedSubtypesWithoutShortLearned().contains(subtype) == true);
    algorithmSubtypeLearning.close();

    deleteLearnedData(subtype);
    deleteLearnedData(subtype2);
  }


  /**
    * @author Sunit Bhalla
    * This test learns a subtype on two images runs
    */
   protected void testMultipleImageRuns(Project project) throws XrayTesterException
   {
     Assert.expect(project != null);

     Panel panel = project.getPanel();
     float thickness;

     Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

     // Create two image runs and learn
     List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
     ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                    subtype,
                                                                    "StatisticalTuningWhiteImageSet",
                                                                    POPULATED_BOARD);
     imageSetDataList.add(imageSetData);
     generateWhiteImageSet(project, imageSetData);
     imageSetData = createImageSetAndSetProgramFilters(project,
                                                       subtype,
                                                       "statisticalTuningBlackImageSet",
                                                       POPULATED_BOARD);
     imageSetDataList.add(imageSetData);
     generateBlackImageSet(project, imageSetData);

     learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

     // Check learned setting values
     thickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

     // Expected value is zero (both image sets should have thickness of zero since in both cases the background and foreground gray levels are the same)
     Expect.expect(MathUtil.fuzzyEquals(thickness, 0.381, 0.001));

     deleteLearnedData(subtype);

   }


   /**
    * @author Sunit Bhalla
    * This method tries to learn subtype when the selected image set contains only data for another subtype.
    * Nothing should be learned.  This test case was created to test a CR.
    */
   protected void testNonApplicableImageRun(Project project) throws XrayTesterException
   {
     Assert.expect(project != null);

     Panel panel = project.getPanel();

     Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
     Subtype subtype2 = panel.getSubtype(_BGA_SUBTYPE_2);

     // Create image run for subtype2

     // addNormalImageSetToTestProgram(project, CREATE_POPULATED_BOARD);
     List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
     ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                    subtype2,
                                                                    "NonApplicableImageRun",
                                                                    POPULATED_BOARD);
     imageSetDataList.add(imageSetData);
     generatePseudoImageSet(project, imageSetData);

     // Make sure that the testProgram filter is only looking for subtype (not subtype2)
     project.getTestProgram().clearFilters();
     project.getTestProgram().addFilter(true);
     project.getTestProgram().addFilter(subtype);

     // Try to learn subtype
     learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _DONT_LEARN_SHORT);

     // Check that settings have not changed
     verifySettingIsAtDefault(subtype2, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
     verifySettingIsAtDefault(subtype2, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);
     verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
     verifySettingIsAtDefault(subtype, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

     // Now try to learn the whole panel to make sure that nothing asserts.
     learnPanelUnitTest(project, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

     deleteLearnedData(subtype);

   }

  /**
   * @author Sunit Bhalla
   * This test checks the error conditions of learnSubtype().
   */
  public void testLearnSubtypesAsserts(Project project)
  {
    Assert.expect(project != null);

    Subtype subtype = project.getPanel().getSubtype(_BGA_SUBTYPE_1);

    StatisticalTuningEngine engine = StatisticalTuningEngine.getInstance();

    boolean learnAllAlgorithmsExceptShort = false;
    boolean learnShort = false;
    boolean learnExpectedImage = false;

    // Test assert when testProgram is null
    try
    {
      try
      {
        //ShengChuan - Clear Tombstone
        engine.learnSubtype(null, subtype, null, new ArrayList<ImageSetData>(), learnAllAlgorithmsExceptShort, false, learnShort, false, learnExpectedImage, false, false);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        // Do nothing--assertException is expected to occur.
      }

      // Test assert when subtype is null
      try
      {
        //ShengChuan - Clear Tombstone
        engine.learnSubtype(project, null, null, new ArrayList<ImageSetData>(), learnAllAlgorithmsExceptShort, false, learnShort, false, learnExpectedImage, false, false);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        // Do nothing--assertException is expected to occur.
      }

      // Test assert when typicalImageSets and unloadedImageSets are both empty.
      try
      {
        //ShengChuan - Clear Tombstone
        engine.learnSubtype(project, subtype, null, new ArrayList<ImageSetData>(), learnAllAlgorithmsExceptShort, false, learnShort, false, learnExpectedImage, false, false);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        // Do nothing--assertException is expected to occur.
      }
    }
    catch (XrayTesterException ex)
    {
      Expect.expect(false, "Unexpected XrayTesterException during test" + ex.getMessage());
    }
  }


}
