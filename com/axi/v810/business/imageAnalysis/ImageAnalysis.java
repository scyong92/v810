package com.axi.v810.business.imageAnalysis;

import java.util.*;
import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * Provides functionality for extracting measurements from an image and classify joints
 * as either good or defective.
 *
 * @author Peter Esbensen
 * @author Matt Wharton
 */
public class ImageAnalysis
{
  public static final int MAX_INTERPAD_DISTANCE_IN_NANOMETERS_FOR_ALGORITHMS = MathUtil.convertMilsToNanoMetersInteger(30);
  private static Map<Algorithm, AtomicLong> _algorithmToTimingInMSMap = new HashMap<Algorithm, AtomicLong>();
  private static Map<Algorithm, AtomicLong> _algorithmToNumberOfPinsProcessedMap = new HashMap<Algorithm, AtomicLong>();
  private static Config _config = Config.getInstance();
  private static final boolean _USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION = _config.getBooleanValue(SoftwareConfigEnum.USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION);
  private static final boolean _LOG_FOCUS_CONFIRMATION_DATA = _config.getBooleanValue(SoftwareConfigEnum.LOG_FOCUS_CONFIRMATION_DATA);
  private boolean _doSurfaceModeling = true;  // we will turn this off during focus confirmation sequences

  // Handle to the inspection engine.
  private static InspectionEngine _inspectionEngine = InspectionEngine.getInstance();

  static
  {
    for (InspectionFamily inspectionFamily : InspectionFamily.getInspectionFamilies())
    {
      for (Algorithm algorithm : inspectionFamily.getAlgorithms())
      {
        _algorithmToNumberOfPinsProcessedMap.put(algorithm, new AtomicLong(0));
        _algorithmToTimingInMSMap.put(algorithm, new AtomicLong(0));
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  public ImageAnalysis()
  {
    // Do nothing ...
  }

  /**
   * @author George A. David
   */
  public static PanelRectangle getMaximumAreaNeededByAlgorithmsAroundRegion(PanelRectangle panelRectangle, int algorithmLimitedInterPadDistanceInNanos)
  {
    Assert.expect(panelRectangle != null);
    Assert.expect(algorithmLimitedInterPadDistanceInNanos >= 0);

    // some pads have an interpad distance of 0. this can cause problems for the algorithms.
    // if this is the case, set the ipd to be about 30 mils.
    if(algorithmLimitedInterPadDistanceInNanos == 0)
      algorithmLimitedInterPadDistanceInNanos = MAX_INTERPAD_DISTANCE_IN_NANOMETERS_FOR_ALGORITHMS;

    int longestPadDimensionInNanos = Math.max(panelRectangle.getHeight(), panelRectangle.getWidth());

    float regionOfInterestDimensionInNanos = longestPadDimensionInNanos + algorithmLimitedInterPadDistanceInNanos * 2.0f;

    int bottomLeftX = panelRectangle.getCenterX() - Math.round(regionOfInterestDimensionInNanos * 0.5f);
    int bottomLeftY = panelRectangle.getCenterY() - Math.round(regionOfInterestDimensionInNanos * 0.5f);

    return (new PanelRectangle(bottomLeftX,
                               bottomLeftY,
                               (int)regionOfInterestDimensionInNanos,
                               (int)regionOfInterestDimensionInNanos));
  }

  /**
   * @author Peter Esbensen
   */
  public void setDoSurfaceModeling(boolean doSurfaceModeling)
  {
    _doSurfaceModeling = doSurfaceModeling;
  }

  /**
   * Get the region that describes the area on the board that the algorithms
   * need in order to inspect the joint.
   *
   * @return an PanelRectangle in nanometers
   * @author Peter Esbensen
   */
  static public PanelRectangle getMaximumAreaNeededByAlgorithmsAroundPad(Pad pad)
  {
    Assert.expect(pad != null);
    if(pad.getSubtype().getInspectionFamilyEnum().equals(InspectionFamilyEnum.CALIBRATION))
    {
      PanelRectangle rect = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
      rect.setRect(rect.getCenterX() - FocusRegion.getMaxFocusRegionSizeInNanoMeters() / 2.0,
                   rect.getCenterY() - FocusRegion.getMaxFocusRegionSizeInNanoMeters() / 2.0,
                   FocusRegion.getMaxFocusRegionSizeInNanoMeters(),
                   FocusRegion.getMaxFocusRegionSizeInNanoMeters());
      return rect;
    }

    PanelRectangle panelRectangle = pad.getPanelRectangleWithLowerLeftOriginInNanoMeters();
//    int interPadDistanceInNanos = pad.getInterPadDistanceInNanoMeters();
    int interPadDistanceInNanos = pad.getAlgorithmLimitedInterPadDistanceInNanometers();

    return getMaximumAreaNeededByAlgorithmsAroundRegion(panelRectangle, interPadDistanceInNanos);
  }

  /**
   * Get the region that describes the area on the board that the algorithms
   * need in order to inspect the joint.
   *
   * @return an PanelRectangle in nanometers
   * @author Peter Esbensen
   */
  static public PanelRectangle getMaximumAreaNeededByAlgorithmsAroundLandPatternPad(LandPatternPad landPatternPad)
  {
    Assert.expect(landPatternPad != null);

    PanelRectangle panelRectangle = new PanelRectangle(landPatternPad.getShapeInNanoMeters());
    int interPadDistanceInNanos = landPatternPad.getAlgorithmLimitedInterPadDistanceInNanoMeters();

    return getMaximumAreaNeededByAlgorithmsAroundRegion(panelRectangle, interPadDistanceInNanos);
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public void classifyJoints(ReconstructedImages reconstructedImages) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(_inspectionEngine != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    Map<Subtype, List<JointInspectionData>> subtypeToInspectableJointListMap = reconstructionRegion.getSubtypeToInspectableJointListMap();

    for (Map.Entry<Subtype, List<JointInspectionData>> mapEntry : subtypeToInspectableJointListMap.entrySet())
    {
      Subtype subtype = mapEntry.getKey();
      JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

      ImageAcquisitionEngine.changeMagnification(subtype.getSubtypeAdvanceSettings().getMagnificationType());
      
      if (_inspectionEngine.isSubtypeInspected(subtype))
      {
        // Run an initial pass of the algorithms.
        for (Algorithm algorithm : subtype.getEnabledAlgorithms())
        {
          // Make sure the algorithm isn't temporarily disabled (via the tuner or some such).
          if (_inspectionEngine.isAlgorithmDisabled(jointTypeEnum, algorithm) == false)
          {
            _algorithmToNumberOfPinsProcessedMap.get(algorithm).addAndGet(mapEntry.getValue().size());
            TimerUtil timerUtil = new TimerUtil();
            timerUtil.start();
            algorithm.classifyJoints(reconstructedImages, mapEntry.getValue());
            timerUtil.stop();

            _algorithmToTimingInMSMap.get(algorithm).addAndGet(timerUtil.getElapsedTimeInMillis());
          }
        }
        //Siew Yeng - XCR-3859 - Run fine tuning do not have to update surface model
        if(_inspectionEngine.isProductionMode())
          updateSurfaceModel(mapEntry.getValue(), reconstructedImages, false);
      }
    }
  }

  /**
   * Now that we are done doing classifyJoints(), let's add the region to the z height estimator model and figure out
   * the estimated z height error.  This will be useful during the focus confirmation work.
   *
   * @author Peter Esbensen
   * @author Siew Yeng - XCR-3781 - add isFocusConfirmation parameter for debugging purpose
   */
  public void updateSurfaceModel(List<JointInspectionData> jointInspectionDataList,
      ReconstructedImages reconstructedImages,
      boolean isFocusConfirmation)
  {
    Assert.expect(jointInspectionDataList != null);
    Assert.expect(reconstructedImages != null);
    
    // Added by Khang Wah, 2013-08-29, RealTime PSH
    if(Config.isRealTimePshEnabled() && reconstructedImages.getReconstructionRegion().getTestSubProgram().isSufficientMemoryForRealTimePsh() &&
       ImagingChainProgramGenerator.reconstructionRegionRequiresGlobalSurfaceModel(reconstructedImages.getReconstructionRegion()))
      return;

    if (_doSurfaceModeling && _USE_SURFACE_MODEL_DURING_FOCUS_CONFIRMATION)
    {
      TimerUtil timerUtil = new TimerUtil();
      ZHeightEstimator zHeightEstimator = ZHeightEstimator.getInstance();

      timerUtil.start();
      IntegerRef padZHeight = new IntegerRef();
      
      //Siew Yeng - XCR-3781 - addDataPoint according to PshSettings
      PshSettings pshSettings = reconstructedImages.getReconstructionRegion().getTestSubProgram().getTestProgram().getProject().getPanel().getPshSettings();
      zHeightEstimator.addDataPoint(jointInspectionDataList, reconstructedImages, padZHeight, pshSettings, isFocusConfirmation);

      timerUtil.stop();

      if (_LOG_FOCUS_CONFIRMATION_DATA)
      {
         XYLocationEstimator.getInstance().addDataPoint(jointInspectionDataList, reconstructedImages, padZHeight.getValue());

         /*
        // log bad focus
        BooleanRef isTopSide = new BooleanRef(false);
        int padSliceZHeight = zHeightEstimator.getPadSliceZHeight(jointInspectionDataList.get(0), reconstructedImages, isTopSide);
        ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
        BooleanRef estimateIsValid = new BooleanRef(false);
        SystemFiducialRectangle systemFiducialRectangle = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(
            reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters(),
            reconstructionRegion.getManualAlignmentMatrix());

        int estimatedZHeight = zHeightEstimator.getBestEstimatedZHeight(systemFiducialRectangle,
            reconstructionRegion.isTopSide(),
            estimateIsValid);

        if (estimateIsValid.getValue() == false)
        {
          estimatedZHeight = zHeightEstimator.getReasonablyEstimatedZHeight(systemFiducialRectangle,
                                                                            reconstructionRegion.isTopSide(),
                                                                            estimateIsValid);
        }

        if (estimateIsValid.getValue() == true)
        {
          int estimatedZHeightError = estimatedZHeight - padSliceZHeight;

          int failed = 0; // 0 = passed, 1 = failed
          for (JointInspectionData jointInspectionData : jointInspectionDataList)
          {
            if (jointInspectionData.getJointInspectionResult().passed() == false)
            {
              failed = 1;
              break;
            }
            if (jointInspectionData.getComponentInspectionData().getComponentInspectionResult().passed() == false)
            {
              failed = 1;
              break;
            }
          }

          JointInspectionData jointInspectionData = jointInspectionDataList.get(0);
          if (Math.abs(estimatedZHeightError) > (25400 * 10))
            System.out.println("badFocus padZ estZ error failed " + jointInspectionData.getSubtype().getShortName() + " " +
                               jointInspectionData.getFullyQualifiedPadName() + " " + padSliceZHeight + "  " +
                               estimatedZHeight + " " + estimatedZHeightError + " " + failed);
        }
        // done logging bad focus
*/
      }

    }
  }

  /**
   * Re-classifies the specified subtype with the given reconstructed images.
   *
   * @author Matt Wharton
   */
  public void reclassifyJoints(ReconstructedImages reconstructedImages,
                               Subtype subtypeToReclassify) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(subtypeToReclassify != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    Map<Subtype, List<JointInspectionData>> subtypeToInspectableJointsMap =
        reconstructionRegion.getSubtypeToInspectableJointListMap();

    // Re-run classification on the specified subtype.
    JointTypeEnum jointTypeEnum = subtypeToReclassify.getJointTypeEnum();
    List<JointInspectionData> jointsToReclassify = subtypeToInspectableJointsMap.get(subtypeToReclassify);

    for (Algorithm algorithm : subtypeToReclassify.getEnabledAlgorithms())
    {
      // Make sure the algorithm isn't temporarily disabled (via the tuner or some such).
      // Also check to make sure the given algorithm is an acceptable algorithm to retest (e.g. Short is not).
      if (_inspectionEngine.isAlgorithmDisabled(jointTypeEnum, algorithm) == false)
      {
        _algorithmToNumberOfPinsProcessedMap.get(algorithm).addAndGet(jointsToReclassify.size());
        TimerUtil timerUtil = new TimerUtil();
        timerUtil.start();
        algorithm.classifyJoints(reconstructedImages, jointsToReclassify);
        timerUtil.stop();

        _algorithmToTimingInMSMap.get(algorithm).addAndGet(timerUtil.getElapsedTimeInMillis());
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyMeasurementGroup(MeasurementGroup measurementGroup,
                                       Map<Pad, ReconstructionRegion> padToImageMap) throws XrayTesterException
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(padToImageMap != null);
    Assert.expect(_inspectionEngine != null);

    Subtype subtype = measurementGroup.getSubtype();

    // Make sure the subtype is actually being inspected.
    if (_inspectionEngine.isSubtypeInspected(subtype))
    {
      for (Algorithm algorithm : subtype.getEnabledAlgorithms())
      {
        // Make sure the algorithm isn't temporarily disabled (via the tuner or some such).
        if (_inspectionEngine.isAlgorithmDisabled(subtype.getJointTypeEnum(), algorithm) == false)
        {
          algorithm.classifyMeasurementGroup(measurementGroup, padToImageMap);
        }
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public void classifyMeasurementGroups(List<MeasurementGroup> measurementGroups,
                                       Map<Pad, ReconstructionRegion> padToImageMap) throws XrayTesterException
  {
    Assert.expect(measurementGroups != null);
    Assert.expect(padToImageMap != null);
    Assert.expect(_inspectionEngine != null);

    List<MeasurementGroup> excludedMeasurementGroup = new ArrayList();
    for(MeasurementGroup measurementGroup : measurementGroups)
    {
      // Make sure the subtype is actually being inspected.
      if((_inspectionEngine.isSubtypeInspected(measurementGroup.getSubtype()) == false) ||
         (measurementGroup.getSubtype().isAlgorithmEnabled(AlgorithmEnum.VOIDING) == false) ||
          _inspectionEngine.isAlgorithmDisabled(measurementGroup.getSubtype().getJointTypeEnum(), 
                                                InspectionFamily.getAlgorithm(InspectionFamilyEnum.LARGE_PAD, AlgorithmEnum.VOIDING)))
      {
        excludedMeasurementGroup.add(measurementGroup);
      }
    }
    if(excludedMeasurementGroup.isEmpty() == false)
      measurementGroups.removeAll(excludedMeasurementGroup);

    //Siew Yeng - XCR-3561 - System crash when voiding algorithm is disabled for single pad mixed subtype
    if(measurementGroups.isEmpty() == false)
    {
      Algorithm algorithm = InspectionFamily.getAlgorithm(InspectionFamilyEnum.LARGE_PAD, AlgorithmEnum.VOIDING);
      algorithm.classifyMeasurementGroups(measurementGroups, padToImageMap);
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void printTimingStats()
  {
    //Lim, Lay Ngor - XCR1743 Benchmark
    //modify from false to true
    printTimingStats(true);
  }

  /**
   * @author Peter Esbensen
   */
  static void printTimingStats(boolean alwaysPrintStats)
  {
    Assert.expect(_algorithmToNumberOfPinsProcessedMap != null);
    Assert.expect(_algorithmToTimingInMSMap != null);

    Set<String> messagesToPrint = new TreeSet<String>();

    if ((UnitTest.unitTesting() == false) || alwaysPrintStats)
    {
      Map<InspectionFamily, Integer> inspectionFamilyToPinsProcessedMap = new HashMap<InspectionFamily,Integer>();
      Map<InspectionFamily, Long> inspectionFamilyToTimeElapsedMap = new HashMap<InspectionFamily,Long>();

      for (Map.Entry<Algorithm, AtomicLong> mapEntry : _algorithmToTimingInMSMap.entrySet())
      {
        long elapsedTimeInMS = mapEntry.getValue().longValue();
        Algorithm algorithm = mapEntry.getKey();

        int pinsProcessed = _algorithmToNumberOfPinsProcessedMap.get(algorithm).intValue();
        float overallJointsPerSecond = (float)pinsProcessed / (elapsedTimeInMS / 1000.f);

        if (pinsProcessed > 0)
        {
          // add the stats to the summary stats for this family
          InspectionFamily inspectionFamily = algorithm.getInspectionFamily();
          if (inspectionFamilyToPinsProcessedMap.containsKey(inspectionFamily) == false)
          {
            Assert.expect(inspectionFamilyToTimeElapsedMap.containsKey(inspectionFamily) == false);
            inspectionFamilyToPinsProcessedMap.put(inspectionFamily, pinsProcessed);
            inspectionFamilyToTimeElapsedMap.put(inspectionFamily, 0L);
          }
          inspectionFamilyToTimeElapsedMap.put(inspectionFamily,
                                               inspectionFamilyToTimeElapsedMap.get(inspectionFamily) + elapsedTimeInMS);

          // print out the stats for this algorithm
          messagesToPrint.add( new String(algorithm.getInspectionFamily().getName() + " " +
                             algorithm.getName() + " (" + pinsProcessed +
                             " joints processed over " + elapsedTimeInMS + " ms): " + overallJointsPerSecond +
                             "  joints per second for all subtypes inspected)"));
        }
      }
      for (String line : messagesToPrint)
        System.out.println(line);
      messagesToPrint.clear();

      // print out the summary stats for this family
      for (Map.Entry<InspectionFamily, Integer> mapEntry : inspectionFamilyToPinsProcessedMap.entrySet())
      {
        InspectionFamily inspectionFamily = mapEntry.getKey();
        int pinsProcessed = mapEntry.getValue();
        long elapsedTime = inspectionFamilyToTimeElapsedMap.get(inspectionFamily);
        float overallJointsPerSecond = (float)pinsProcessed / (elapsedTime / 1000.f);
        messagesToPrint.add( new String(inspectionFamily.getName() + " COMBINED " +
                           " (" + pinsProcessed +
                           " joints processed over " + elapsedTime + " ms): " + overallJointsPerSecond +
                           "  joints per second for all subtypes inspected)"));

      }
      for (String line : messagesToPrint)
        System.out.println(line);
      messagesToPrint.clear();
      Date date = new Date();
      System.out.println("Date: " + date);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static void resetTimers()
  {
    for (AtomicLong pinCount : _algorithmToNumberOfPinsProcessedMap.values())
    {
      pinCount.set(0);
    }
    for (AtomicLong timeInMS : _algorithmToTimingInMSMap.values())
    {
      timeInMS.set(0);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static boolean isSpecialTwoPinComponent(JointTypeEnum jointTypeEnum)
  {
    if ( jointTypeEnum.equals(JointTypeEnum.CAPACITOR) ||
         jointTypeEnum.equals(JointTypeEnum.RESISTOR) ||
         jointTypeEnum.equals(JointTypeEnum.POLARIZED_CAP)
         // Wei Chin (Tall Cap)
//         || jointTypeEnum.equals(JointTypeEnum.TALL_CAPACITOR)
         )
      return true;
    return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static boolean requiresThroughHoleLandPatternPad(JointTypeEnum jointTypeEnum)
  {
    if (jointTypeEnum.equals(JointTypeEnum.PRESSFIT) ||
        jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE) ||
        jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) //Siew Yeng - XCR-3318 - Oval PTH
      return true;
    return false;
  }


  /**
   * Returns the ReconstructedImages for the specified inspection region and image type.
   *
   * @author Matt Wharton
   */
  public static ReconstructedImages getImagesForRegion(ReconstructionRegion inspectionRegion,
                                                       ImageTypeEnum imageTypeEnum) throws DatastoreException
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(imageTypeEnum != null);

    return _inspectionEngine.getImagesForRegion(inspectionRegion, imageTypeEnum);
  }

  /**
   * Acquires lightest images for the specified reconstruction region.  Blocks until the images are available.
   * Returns immediately if image acquisition is paused or if an abort occurs.
   *
   * @author Matt Wharton
   */
  public static ReconstructedImages getLightestImagesForRegion(ReconstructionRegion inspectionRegion,
                                                               BooleanRef lightestImagesAvailable) throws XrayTesterException
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(lightestImagesAvailable != null);

    return _inspectionEngine.getLightestImagesForRegion(inspectionRegion, lightestImagesAvailable);
  }

  /**
   * @return true if diagnostics are enabled for the specified joint type and algorithm; false otherwise.
   *
   * @author Matt Wharton
   */
  public static boolean areDiagnosticsEnabled(JointTypeEnum jointTypeEnum, Algorithm algorithm)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(_inspectionEngine != null);

    return _inspectionEngine.areDiagnosticsEnabled(jointTypeEnum, algorithm);
  }

}
