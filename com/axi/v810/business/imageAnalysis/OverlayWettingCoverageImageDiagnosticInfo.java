package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.util.*;

/**
 * A diagnostic message that requests an image overlay on the inspection image.
 *
 * @author Jack Hwee
 */
public class OverlayWettingCoverageImageDiagnosticInfo extends DiagnosticInfo
{

  private Image _overlayImage = null;
  private RegionOfInterest _regionOfInterestInInspectionImage = null;
  private String _diagnosticLegendLabel = StringLocalizer.keyToString("VOIDING_DIAGNOSTIC_KEY");

  /**
   * @author Patrick Lacz
   */
  public OverlayWettingCoverageImageDiagnosticInfo(Image overlayImage, RegionOfInterest roi)
  {
    Assert.expect(overlayImage != null);
    Assert.expect(roi != null);

    _overlayImage = overlayImage;
    _overlayImage.incrementReferenceCount();
    _regionOfInterestInInspectionImage = roi;
  }

  /**
   * @author George Booth
   */
  public OverlayWettingCoverageImageDiagnosticInfo(Image overlayImage, RegionOfInterest roi, String label)
  {
    Assert.expect(overlayImage != null);
    Assert.expect(roi != null);
    Assert.expect(label != null);

    _overlayImage = overlayImage;
    _overlayImage.incrementReferenceCount();
    _regionOfInterestInInspectionImage = roi;
    _diagnosticLegendLabel = label;
  }

  /**
   * @author Patrick Lacz
   */
  public Image getOverlayImage()
  {
    Assert.expect(_overlayImage != null);

    return _overlayImage;
  }

  /**
   * @author Patrick Lacz
   */
  public RegionOfInterest getRegionOfInterestInInspectionImage()
  {
    Assert.expect(_regionOfInterestInInspectionImage != null);

    return _regionOfInterestInInspectionImage;
  }

  /**
   * @author George Booth
   */
  public String getLabel()
  {
    Assert.expect(_diagnosticLegendLabel != null);

    return _diagnosticLegendLabel;
  }

  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    _overlayImage.incrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    _overlayImage.decrementReferenceCount();
  }

}
