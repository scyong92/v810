package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Peter Esbensen
 */
class Test_AlgorithmInspectionOrderComparator extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmInspectionOrderComparator());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Algorithm gridArrayMeasurementAlgorithm = InspectionFamily.getAlgorithm(
      InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
    Algorithm gridArrayOpenAlgorithm = InspectionFamily.getAlgorithm(
      InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.OPEN);

    List<Algorithm> algorithms = new ArrayList<Algorithm>();

    algorithms.add(gridArrayMeasurementAlgorithm);
    algorithms.add(gridArrayOpenAlgorithm);

    Collections.sort(algorithms, new AlgorithmInspectionOrderComparator());
    Expect.expect(algorithms.get(0).getAlgorithmEnum().equals(AlgorithmEnum.MEASUREMENT));

    algorithms = new ArrayList<Algorithm>();

    algorithms.add(gridArrayOpenAlgorithm);
    algorithms.add(gridArrayMeasurementAlgorithm);

    Collections.sort(algorithms, new AlgorithmInspectionOrderComparator());
    Expect.expect(algorithms.get(0).getAlgorithmEnum().equals(AlgorithmEnum.MEASUREMENT));

    algorithms = new ArrayList<Algorithm>();

    algorithms.add(gridArrayOpenAlgorithm);
    algorithms.add(gridArrayMeasurementAlgorithm);
    algorithms.add(gridArrayMeasurementAlgorithm);

    Collections.sort(algorithms, new AlgorithmInspectionOrderComparator());
    Expect.expect(algorithms.get(0).getAlgorithmEnum().equals(AlgorithmEnum.MEASUREMENT));
    Expect.expect(algorithms.get(1).getAlgorithmEnum().equals(AlgorithmEnum.MEASUREMENT));
  }
}
