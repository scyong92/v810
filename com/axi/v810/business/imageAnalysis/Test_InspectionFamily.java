package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author not attributable
 * @version 1.0
 */
class Test_InspectionFamily extends UnitTest
{

  /**
   * Constructor.
   *
   * @author Peter Esbensen
   */
  public Test_InspectionFamily()
  {
    super();
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   *
   * @author Peter Esbensen
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // try calling methods with a few parameters to make sure they are basically working

    final Set<AlgorithmEnum> algorithmEnums = InspectionFamily.getAlgorithmEnumsSharedByAllInspectionFamilies();
    Expect.expect(algorithmEnums.contains(AlgorithmEnum.MEASUREMENT));

    Expect.expect( InspectionFamily.isSpecialTwoPinDevice(InspectionFamilyEnum.CHIP));

    Expect.expect( InspectionFamily.getInspectionFamily( JointTypeEnum.JLEAD ).getInspectionFamilyEnum().equals(InspectionFamilyEnum.GULLWING) );
  }


  /**
   * Entry point for test fixture.
   *
   * @author Peter Esbensen
   */
  public static void main(final String[] args)
  {
    UnitTest.execute(new Test_InspectionFamily());
  }
}
