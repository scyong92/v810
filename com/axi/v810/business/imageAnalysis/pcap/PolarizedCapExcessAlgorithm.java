package com.axi.v810.business.imageAnalysis.pcap;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Lim, Seng Yew
 */
class PolarizedCapExcessAlgorithm extends Algorithm
{
  /**
   * @author Lim, Seng Yew
   */
  public PolarizedCapExcessAlgorithm(InspectionFamily polarizedCapInspectionFamily)
  {
    super(AlgorithmEnum.EXCESS, InspectionFamilyEnum.POLARIZED_CAP);

    Assert.expect(polarizedCapInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

   AlgorithmSetting maximumFilletThickness = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS,
        displayOrder++,
        150.0f, // default
        0.0f, // min
        500.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_PCAP_EXCESS_(PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_EXCESS_(PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_PCAP_EXCESS_(PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      maximumFilletThickness,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      maximumFilletThickness,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(maximumFilletThickness);

    addMeasurementEnums();
  }

  /**
   * @author Lim, Seng Yew
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL);
  }

  /**
   * @author Lim, Seng Yew
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ComponentInspectionData componentInspectionData :
         AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
      JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();

      checkFilletThicknesses(padOneJointInspectionData, padTwoJointInspectionData, reconstructedImages, reconstructionRegion);
    }
  }

  /**
   * @author Lim, Seng Yew
   */
  private void checkFilletThicknesses(JointInspectionData padOneJointInspectionData,
                                      JointInspectionData padTwoJointInspectionData,
                                      ReconstructedImages reconstructedImages,
                                      ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructionRegion != null);

    boolean bothPassed = true;

    SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, padOneJointInspectionData.getSubtype(), this);

    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              padOneJointInspectionData,
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              true);

    Subtype subtype = padOneJointInspectionData.getSubtype();

    JointMeasurement filletOneThicknessPercentOfNominalMeasurement = getPadOneFilletThicknessAsPercentOfNominal(padOneJointInspectionData,
        sliceNameEnum, subtype);

    float maximumFilletThicknessThreshold = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS);

    JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();

    if (filletOneThicknessPercentOfNominalMeasurement.getValue() > maximumFilletThicknessThreshold)
    {
      JointIndictment jointIndictment = new JointIndictment(IndictmentEnum.EXCESS,
          this, sliceNameEnum);
      jointIndictment.addFailingMeasurement(filletOneThicknessPercentOfNominalMeasurement);
      padOneJointInspectionResult.addIndictment(jointIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      filletOneThicknessPercentOfNominalMeasurement,
                                                      maximumFilletThicknessThreshold);
      bothPassed = false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      filletOneThicknessPercentOfNominalMeasurement,
                                                      maximumFilletThicknessThreshold);
    }



    JointMeasurement filletTwoThicknessPercentOfNominalMeasurement = getPadTwoFilletThicknessAsPercentOfNominal(
        padTwoJointInspectionData,
        sliceNameEnum, subtype);

    JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();

    if (filletTwoThicknessPercentOfNominalMeasurement.getValue() > maximumFilletThicknessThreshold)
    {
      JointIndictment jointIndictment = new JointIndictment(IndictmentEnum.EXCESS,
          this, sliceNameEnum);
      jointIndictment.addFailingMeasurement(filletTwoThicknessPercentOfNominalMeasurement);
      padTwoJointInspectionResult.addIndictment(jointIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padTwoJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      filletTwoThicknessPercentOfNominalMeasurement,
                                                      maximumFilletThicknessThreshold);
      bothPassed = false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      padTwoJointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      filletTwoThicknessPercentOfNominalMeasurement,
                                                      maximumFilletThicknessThreshold);
    }

    AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                padOneJointInspectionData.getComponentInspectionData(),
                                                                reconstructionRegion,
                                                                sliceNameEnum,
                                                                bothPassed);
  }

  /**
   * @author Lim, Seng Yew
   */
  private JointMeasurement getPadOneFilletThicknessAsPercentOfNominal(JointInspectionData padOneJointInspectionData,
      SliceNameEnum sliceNameEnum,
      Subtype subtype)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    JointMeasurement filletThicknessMeasurement = PolarizedCapMeasurementAlgorithm.getPadOneUpperFilletThicknessInMillimeters(
        padOneJointInspectionData, sliceNameEnum);

    float nominalFilletThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS);

    float filletThicknessPercentOfNominal = 100.0f * filletThicknessMeasurement.getValue() / nominalFilletThickness;
    JointMeasurement filletThicknessPercentOfNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL,
        MeasurementUnitsEnum.PERCENT, padOneJointInspectionData.getPad(),
        sliceNameEnum, filletThicknessPercentOfNominal, true);

    padOneJointInspectionData.getJointInspectionResult().addMeasurement(filletThicknessPercentOfNominalMeasurement);
    return filletThicknessPercentOfNominalMeasurement;
  }

  /**
   * @author Lim, Seng Yew
   */
  private JointMeasurement getPadTwoFilletThicknessAsPercentOfNominal(JointInspectionData padTwoJointInspectionData,
      SliceNameEnum sliceNameEnum,
      Subtype subtype)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    JointMeasurement filletThicknessMeasurement = PolarizedCapMeasurementAlgorithm.getPadTwoUpperFilletThicknessInMillimeters(
        padTwoJointInspectionData, sliceNameEnum);

    float nominalFilletThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS);

    float filletThicknessPercentOfNominal = 100.0f * filletThicknessMeasurement.getValue() / nominalFilletThickness;
    JointMeasurement filletThicknessPercentOfNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL,
        MeasurementUnitsEnum.PERCENT, padTwoJointInspectionData.getPad(),
        sliceNameEnum, filletThicknessPercentOfNominal, true);

    padTwoJointInspectionData.getJointInspectionResult().addMeasurement(filletThicknessPercentOfNominalMeasurement);
    return filletThicknessPercentOfNominalMeasurement;
  }

}
