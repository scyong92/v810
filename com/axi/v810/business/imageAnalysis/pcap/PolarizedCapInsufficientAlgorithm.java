package com.axi.v810.business.imageAnalysis.pcap;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Peter Esbensen
 */
class PolarizedCapInsufficientAlgorithm extends Algorithm
{
  /**
   * @author Peter Esbensen
   */
  public PolarizedCapInsufficientAlgorithm(InspectionFamily polarizedCapInspectionFamily)
  {
    super(AlgorithmEnum.INSUFFICIENT, InspectionFamilyEnum.POLARIZED_CAP);

    Assert.expect(polarizedCapInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

   AlgorithmSetting minimumFilletThickness = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_PCAP_INSUFFICIENT_(PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_INSUFFICIENT_(PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_PCAP_INSUFFICIENT_(PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      minimumFilletThickness,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      minimumFilletThickness,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minimumFilletThickness);

    addMeasurementEnums();
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL);
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ComponentInspectionData componentInspectionData :
         AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
      JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();

      checkFilletThicknesses(padOneJointInspectionData, padTwoJointInspectionData, reconstructedImages, reconstructionRegion);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void checkFilletThicknesses(JointInspectionData padOneJointInspectionData,
                                      JointInspectionData padTwoJointInspectionData,
                                      ReconstructedImages reconstructedImages,
                                      ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructionRegion != null);

    boolean bothPassed = true;

    SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, padOneJointInspectionData.getSubtype(), this);

    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              padOneJointInspectionData,
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              true);

    Subtype subtype = padOneJointInspectionData.getSubtype();

    JointMeasurement filletOneThicknessPercentOfNominalMeasurement = getPadOneFilletThicknessAsPercentOfNominal(padOneJointInspectionData,
        sliceNameEnum, subtype);

    float minimumFilletThicknessThreshold = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS);

    JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();

    if (filletOneThicknessPercentOfNominalMeasurement.getValue() < minimumFilletThicknessThreshold)
    {
      JointIndictment jointIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT,
          this, sliceNameEnum);
      jointIndictment.addFailingMeasurement(filletOneThicknessPercentOfNominalMeasurement);
      padOneJointInspectionResult.addIndictment(jointIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      filletOneThicknessPercentOfNominalMeasurement,
                                                      minimumFilletThicknessThreshold);
      bothPassed = false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      filletOneThicknessPercentOfNominalMeasurement,
                                                      minimumFilletThicknessThreshold);
    }



    JointMeasurement filletTwoThicknessPercentOfNominalMeasurement = getPadTwoFilletThicknessAsPercentOfNominal(
        padTwoJointInspectionData,
        sliceNameEnum, subtype);

    JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();

    if (filletTwoThicknessPercentOfNominalMeasurement.getValue() < minimumFilletThicknessThreshold)
    {
      JointIndictment jointIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT,
          this, sliceNameEnum);
      jointIndictment.addFailingMeasurement(filletTwoThicknessPercentOfNominalMeasurement);
      padTwoJointInspectionResult.addIndictment(jointIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padTwoJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      filletTwoThicknessPercentOfNominalMeasurement,
                                                      minimumFilletThicknessThreshold);
      bothPassed = false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      padTwoJointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      filletTwoThicknessPercentOfNominalMeasurement,
                                                      minimumFilletThicknessThreshold);
    }

    AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                padOneJointInspectionData.getComponentInspectionData(),
                                                                reconstructionRegion,
                                                                sliceNameEnum,
                                                                bothPassed);
  }

  /**
   * @author Peter Esbensen
   */
  private JointMeasurement getPadOneFilletThicknessAsPercentOfNominal(JointInspectionData padOneJointInspectionData,
      SliceNameEnum sliceNameEnum,
      Subtype subtype)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    JointMeasurement filletThicknessMeasurement = PolarizedCapMeasurementAlgorithm.getPadOneUpperFilletThicknessInMillimeters(
        padOneJointInspectionData, sliceNameEnum);

    float nominalFilletThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS);

    float filletThicknessPercentOfNominal = 100.0f * filletThicknessMeasurement.getValue() / nominalFilletThickness;
    JointMeasurement filletThicknessPercentOfNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL,
        MeasurementUnitsEnum.PERCENT, padOneJointInspectionData.getPad(),
        sliceNameEnum, filletThicknessPercentOfNominal, true);

    padOneJointInspectionData.getJointInspectionResult().addMeasurement(filletThicknessPercentOfNominalMeasurement);
    return filletThicknessPercentOfNominalMeasurement;
  }

  /**
   * @author Peter Esbensen
   */
  private JointMeasurement getPadTwoFilletThicknessAsPercentOfNominal(JointInspectionData padTwoJointInspectionData,
      SliceNameEnum sliceNameEnum,
      Subtype subtype)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    JointMeasurement filletThicknessMeasurement = PolarizedCapMeasurementAlgorithm.getPadTwoUpperFilletThicknessInMillimeters(
        padTwoJointInspectionData, sliceNameEnum);

    float nominalFilletThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS);

    float filletThicknessPercentOfNominal = 100.0f * filletThicknessMeasurement.getValue() / nominalFilletThickness;
    JointMeasurement filletThicknessPercentOfNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL,
        MeasurementUnitsEnum.PERCENT, padTwoJointInspectionData.getPad(),
        sliceNameEnum, filletThicknessPercentOfNominal, true);

    padTwoJointInspectionData.getJointInspectionResult().addMeasurement(filletThicknessPercentOfNominalMeasurement);
    return filletThicknessPercentOfNominalMeasurement;
  }

}
