package com.axi.v810.business.imageAnalysis.pcap;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Peter Esbensen
 */
class PolarizedCapOpenAlgorithm extends Algorithm
{
  /**
   * @author Peter Esbensen
   */
  public PolarizedCapOpenAlgorithm(InspectionFamily polarizedCapInspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.POLARIZED_CAP);

    Assert.expect(polarizedCapInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    addMeasurementEnums();

    AlgorithmSetting padOneOpenSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_OPEN_PAD_ONE_MINIMUM_OPEN_SIGNAL,
        displayOrder++,
        0.0254f, // default
        -0.6350f, // min
        0.635f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_OPEN_(PCAP_OPEN_PAD_ONE_OPEN_SIGNAL)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_OPEN_(PCAP_OPEN_PAD_ONE_OPEN_SIGNAL)_KEY", // detailed desc
        "IMG_DESC_PCAP_OPEN_(PCAP_OPEN_PAD_ONE_OPEN_SIGNAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      padOneOpenSignal,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_OPEN_PAD_ONE_OPEN_SIGNAL);
    addAlgorithmSetting(padOneOpenSignal);

    AlgorithmSetting clearOpenSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_OPEN_PAD_TWO_MINIMUM_OPEN_SIGNAL,
        displayOrder++,
        0.0254f, // default
        -0.6350f, // min
        0.6350f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_OPEN_(PCAP_OPEN_PAD_TWO_OPEN_SIGNAL)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_OPEN_(PCAP_OPEN_PAD_TWO_OPEN_SIGNAL)_KEY", // detailed desc
        "IMG_DESC_PCAP_OPEN_(PCAP_OPEN_PAD_TWO_OPEN_SIGNAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      clearOpenSignal,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_OPEN_PAD_TWO_OPEN_SIGNAL);
    addAlgorithmSetting(clearOpenSignal);

    AlgorithmSetting minimumSlugThickness = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_OPEN_MINIMUM_SLUG_THICKNESS,
        displayOrder++,
        0.127f, // default
        0.0f, // min
        2.54f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_OPEN_(PCAP_OPEN_MINIMUM_SLUG_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_OPEN_(PCAP_OPEN_MINIMUM_SLUG_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_PCAP_OPEN_(PCAP_OPEN_MINIMUM_SLUG_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      minimumSlugThickness,
                                                                      SliceNameEnum.PCAP_SLUG,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS);
    addAlgorithmSetting(minimumSlugThickness);

  }

  /**
  * @author Peter Esbensen
  */
 private void addMeasurementEnums()
 {
   _jointMeasurementEnums.add(MeasurementEnum.PCAP_OPEN_PAD_ONE_OPEN_SIGNAL);
   _jointMeasurementEnums.add(MeasurementEnum.PCAP_OPEN_PAD_TWO_OPEN_SIGNAL);
 }


  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ComponentInspectionData componentInspectionData :
         AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
      JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();

      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, padOneJointInspectionData.getSubtype(), this);

      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                padOneJointInspectionData,
                                                reconstructionRegion,
                                                reconstructedSlice,
                                                false);

      boolean jointPassed = checkOpenSignals(padOneJointInspectionData, padTwoJointInspectionData,
                                             reconstructedSlice, reconstructionRegion);

      // now switch to the slug slice to check slug thickness
      sliceNameEnum = SliceNameEnum.PCAP_SLUG;
      reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, padOneJointInspectionData.getSubtype(), this);

      jointPassed &= checkSlugThickness(componentInspectionData, reconstructedSlice, reconstructionRegion);


      AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                  componentInspectionData,
                                                                  reconstructionRegion,
                                                                  sliceNameEnum,
                                                                  jointPassed);
    }
  }

  /*
   * @author Peter Esbensen
   */
  private boolean checkOpenSignals(JointInspectionData padOneJointInspectionData,
                                   JointInspectionData padTwoJointInspectionData,
                                   ReconstructedSlice reconstructedSlice,
                                   ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    boolean bothPassed = true;

    Subtype subtype = padOneJointInspectionData.getSubtype();

    JointMeasurement padOneLowerFilletThicknessInMillimetersMeasurement = PolarizedCapMeasurementAlgorithm.getPadOneLowerFilletThicknessInMillimeters(
        padOneJointInspectionData, reconstructedSlice.getSliceNameEnum());
    JointMeasurement padOneUpperFilletThicknessInMillimetersMeasurement = PolarizedCapMeasurementAlgorithm.getPadOneUpperFilletThicknessInMillimeters(
        padOneJointInspectionData, reconstructedSlice.getSliceNameEnum());
    float padOneOpenSignal = padOneUpperFilletThicknessInMillimetersMeasurement.getValue() - padOneLowerFilletThicknessInMillimetersMeasurement.getValue();

    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    JointMeasurement padOneOpenSignalMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_OPEN_PAD_ONE_OPEN_SIGNAL,
        MeasurementUnitsEnum.MILLIMETERS, padOneJointInspectionData.getPad(),
        sliceNameEnum, padOneOpenSignal);

    JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();
    padOneJointInspectionResult.addMeasurement(padOneOpenSignalMeasurement);

    float padOneMinimumClearOpenSignalThresholdInMillimeters = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.PCAP_OPEN_PAD_ONE_MINIMUM_OPEN_SIGNAL);

    boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();

    if (padOneOpenSignal < padOneMinimumClearOpenSignalThresholdInMillimeters)
    {
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
          this, sliceNameEnum);
      openIndictment.addFailingMeasurement(padOneOpenSignalMeasurement);
      openIndictment.addRelatedMeasurement(padOneUpperFilletThicknessInMillimetersMeasurement);
      openIndictment.addRelatedMeasurement(padOneLowerFilletThicknessInMillimetersMeasurement);
      padOneJointInspectionResult.addIndictment(openIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      padOneOpenSignalMeasurement,
                                                      padOneMinimumClearOpenSignalThresholdInMillimeters);
      bothPassed = false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      padOneOpenSignalMeasurement,
                                                      padOneMinimumClearOpenSignalThresholdInMillimeters);
    }

    JointMeasurement padTwoLowerFilletThicknessInMillimetersMeasurement = PolarizedCapMeasurementAlgorithm.getPadTwoLowerFilletThicknessInMillimeters(
        padTwoJointInspectionData, reconstructedSlice.getSliceNameEnum());
    JointMeasurement padTwoUpperFilletThicknessInMillimetersMeasurement = PolarizedCapMeasurementAlgorithm.getPadTwoUpperFilletThicknessInMillimeters(
        padTwoJointInspectionData, reconstructedSlice.getSliceNameEnum());
    float padTwoOpenSignal = padTwoUpperFilletThicknessInMillimetersMeasurement.getValue() - padTwoLowerFilletThicknessInMillimetersMeasurement.getValue();

    JointMeasurement padTwoOpenSignalMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_OPEN_PAD_TWO_OPEN_SIGNAL,
        MeasurementUnitsEnum.MILLIMETERS, padTwoJointInspectionData.getPad(),
        sliceNameEnum, padTwoOpenSignal);

    JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();
    padTwoJointInspectionResult.addMeasurement(padTwoOpenSignalMeasurement);

    float padTwoMinimumClearOpenSignalThreshold = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.PCAP_OPEN_PAD_TWO_MINIMUM_OPEN_SIGNAL);

    if (padTwoOpenSignal < padTwoMinimumClearOpenSignalThreshold)
    {
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
          this, sliceNameEnum);
      openIndictment.addFailingMeasurement(padTwoOpenSignalMeasurement);
      openIndictment.addRelatedMeasurement(padTwoUpperFilletThicknessInMillimetersMeasurement);
      openIndictment.addRelatedMeasurement(padTwoLowerFilletThicknessInMillimetersMeasurement);
      padTwoJointInspectionResult.addIndictment(openIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padTwoJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      padTwoOpenSignalMeasurement,
                                                      padTwoMinimumClearOpenSignalThreshold);
      bothPassed = false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      padTwoJointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      padTwoOpenSignalMeasurement,
                                                      padTwoMinimumClearOpenSignalThreshold);
    }

    return bothPassed;
  }

  /*
   * @author Peter Esbensen
   */
  private boolean checkSlugThickness(ComponentInspectionData componentInspectionData,
                                     ReconstructedSlice reconstructedSlice,
                                     ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();

    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    
    ComponentMeasurement slugThicknessInMillimetersMeasurement  = null;
    JointMeasurement jointSlugThicknessInMillimetersMeasurement = null;
    float slugThicknessInMillimetersFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      slugThicknessInMillimetersMeasurement = PolarizedCapMeasurementAlgorithm.getSlugThicknessInMillimetersMeasurement(componentInspectionData, sliceNameEnum);
      slugThicknessInMillimetersFloatValue = slugThicknessInMillimetersMeasurement.getValue();
    }
    else
    {
      jointSlugThicknessInMillimetersMeasurement = PolarizedCapMeasurementAlgorithm.getSlugThicknessInMillimetersMeasurement(componentInspectionData.getPadOneJointInspectionData(), 
              sliceNameEnum);
      slugThicknessInMillimetersFloatValue = jointSlugThicknessInMillimetersMeasurement.getValue();
    }    

    ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();

    float minimumSlugThicknessThresholdInMillimeters = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.PCAP_OPEN_MINIMUM_SLUG_THICKNESS);

    boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();
    float minimumSlugThicknessThresholdInUsersUnits = minimumSlugThicknessThresholdInMillimeters;
    if (usingMetricUnits == false)
    {
      // convert to Mils
      minimumSlugThicknessThresholdInUsersUnits = MathUtil.convertMillimetersToMils(minimumSlugThicknessThresholdInMillimeters);
    }

    boolean passed = false;

    if (slugThicknessInMillimetersFloatValue < minimumSlugThicknessThresholdInMillimeters)
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (slugThicknessInMillimetersMeasurement != null)
        {
          ComponentIndictment openIndictment = new ComponentIndictment(IndictmentEnum.OPEN,
              this, sliceNameEnum, subtype, subtype.getJointTypeEnum());
          openIndictment.addFailingMeasurement(slugThicknessInMillimetersMeasurement);
          componentInspectionResult.addIndictment(openIndictment);
          AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                              componentInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              slugThicknessInMillimetersMeasurement,
                                                              minimumSlugThicknessThresholdInMillimeters);
        }
      }
      else
      {
        if (jointSlugThicknessInMillimetersMeasurement != null)
        {
          JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
              this, sliceNameEnum);
          openIndictment.addFailingMeasurement(jointSlugThicknessInMillimetersMeasurement);
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(openIndictment);
          AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                          componentInspectionData.getPadOneJointInspectionData(),
                                                          reconstructionRegion,
                                                          sliceNameEnum,
                                                          jointSlugThicknessInMillimetersMeasurement,
                                                          minimumSlugThicknessThresholdInMillimeters);
        }
      }            
      passed = false;
    }
    else
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (slugThicknessInMillimetersMeasurement != null)
        {
          AlgorithmUtil.postPassingComponentTextualDiagnostic(this,
                                                          componentInspectionData,
                                                          sliceNameEnum,
                                                          reconstructionRegion,
                                                          slugThicknessInMillimetersMeasurement,
                                                          minimumSlugThicknessThresholdInMillimeters);
        }
      }
      else
      {
        if (jointSlugThicknessInMillimetersMeasurement != null)
        {
          AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadOneJointInspectionData(),
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      jointSlugThicknessInMillimetersMeasurement,
                                                      minimumSlugThicknessThresholdInMillimeters);
        }
      }            
      passed = true;
    }
    return passed;
  }



}
