package com.axi.v810.business.imageAnalysis.pcap;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;

/**
 * The Misalignment algorithm checks opaque capacitors to see if they have been
 * shifted to far across their pads and checks polarized capacitors to make sure
 * they haven't been loaded backwards (reversed polarity).
 *
 * @author Peter Esbensen
 */
public class PolarizedCapMisalignmentAlgorithm extends Algorithm
{
  /**
   * @author Peter Esbensen
   */
   private final static String _ASYMMETRY_METHOD = "Asymmetry Method";
   private final static String _SLUG_EDGE_METHOD = "Slug Edge Method";
   private final static ArrayList<String> PCAP_MISALIGNMENT_CHOICES = new ArrayList<String>(Arrays.asList(_ASYMMETRY_METHOD,_SLUG_EDGE_METHOD));
   
  public PolarizedCapMisalignmentAlgorithm(InspectionFamily polarizedCapInspectionFamily)
  {
    super(AlgorithmEnum.MISALIGNMENT, InspectionFamilyEnum.POLARIZED_CAP);

    Assert.expect(polarizedCapInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

     AlgorithmSetting misalignmentMethodOptions = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS, // setting enum
        displayOrder++, // display order,
        _ASYMMETRY_METHOD,
        PCAP_MISALIGNMENT_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS)_KEY", // detailed description URL Key
        "IMG_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(misalignmentMethodOptions);
    
    AlgorithmSetting minimumPolaritySignal = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL,
        displayOrder++,
        0.0f, // default
        -100.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL)_KEY", // detailed desc
        "IMG_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      minimumPolaritySignal,
                                                                      SliceNameEnum.PCAP_SLUG,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL);
    addAlgorithmSetting(minimumPolaritySignal);
  
    AlgorithmSetting slugEdgeDetectionThreshold = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_THRESHOLD, // setting enum
        displayOrder++, // display order,
        15f, // default
        0f, // min
        50f, // max
        MeasurementUnitsEnum.MILS,
        "HTML_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_THRESHOLD)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_THRESHOLD)_KEY", // detailed description URL Key
        "IMG_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_THRESHOLD)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      slugEdgeDetectionThreshold,
                                                                      SliceNameEnum.PCAP_SLUG,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_THRESHOLD);
    addAlgorithmSetting(slugEdgeDetectionThreshold);
    
    AlgorithmSetting slugEdgeDetectionLengthAcross = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ACROSS, // setting enum
        displayOrder++, // display order,
        50.0f,  // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS,
        "HTML_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_LENGTH_ACROSS)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_LENGTH_ACROSS)_KEY", // detailed description URL Key
        "IMG_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_LENGTH_ACROSS)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      slugEdgeDetectionLengthAcross,
                                                                      SliceNameEnum.PCAP_SLUG,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_REGION_LENGTH_ACROSS);
    addAlgorithmSetting(slugEdgeDetectionLengthAcross);
    
    AlgorithmSetting slugEdgeDetectionLengthAlong = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ALONG, // setting enum
        displayOrder++, // display order,
        50.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_LENGTH_ALONG)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_LENGTH_ALONG)_KEY", // detailed description URL Key
        "IMG_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_LENGTH_ALONG)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      slugEdgeDetectionLengthAlong,
                                                                      SliceNameEnum.PCAP_SLUG,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_REGION_LENGTH_ALONG);
    addAlgorithmSetting(slugEdgeDetectionLengthAlong);
    
    AlgorithmSetting slugEdgeDetectionPosition = new AlgorithmSetting(
        AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_POSITION, // setting enum
        displayOrder++, // display order,
        0.5f, // default value
        -0.2f, // minimum value
        1.2f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_POSITION)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_POSITION)_KEY", // detailed description URL Key
        "IMG_DESC_PCAP_MISALIGNMENT_(PCAP_MISALIGNMENT_SLUG_EDGE_DETECTION_POSITION)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      slugEdgeDetectionPosition,
                                                                      SliceNameEnum.PCAP_SLUG,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_SLUG_EDGE_REGION_POSITION);
    addAlgorithmSetting(slugEdgeDetectionPosition);
    
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ComponentInspectionData componentInspectionData :
         AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();

      boolean jointPassed = true;

      jointPassed &= checkPolarity(componentInspectionData, reconstructedImages, reconstructionRegion);
      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
      AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                  padOneJointInspectionData.getComponentInspectionData(),
                                                                  reconstructionRegion,
                                                                  sliceNameEnum,
                                                                  jointPassed);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private boolean checkPolarity(ComponentInspectionData componentInspectionData,
                             ReconstructedImages reconstructedImages,
                             ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructionRegion != null);

    SliceNameEnum sliceNameEnum = SliceNameEnum.PCAP_SLUG;
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, componentInspectionData.getPadOneJointInspectionData().getSubtype(), this);

    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              componentInspectionData.getPadOneJointInspectionData(),
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              false);

    ComponentMeasurement asymmetryMeasurement   = null;
    JointMeasurement jointAsymmetryMeasurement  = null;
    float asymmetryMeasurementFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      asymmetryMeasurement = PolarizedCapMeasurementAlgorithm.getPolaritySignalMeasurement(
        componentInspectionData, sliceNameEnum);
      asymmetryMeasurementFloatValue = asymmetryMeasurement.getValue();
    }
    else
    {
      jointAsymmetryMeasurement = PolarizedCapMeasurementAlgorithm.getPolaritySignalMeasurement(
        componentInspectionData.getPadOneJointInspectionData(), sliceNameEnum);
      asymmetryMeasurementFloatValue = jointAsymmetryMeasurement.getValue();
    }

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();

    float minimumPolaritySignalThreshold = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL);

    if (asymmetryMeasurementFloatValue < minimumPolaritySignalThreshold)
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (asymmetryMeasurement != null)
        {
          ComponentIndictment componentIndictment = new ComponentIndictment(IndictmentEnum.MISALIGNMENT,
                                                                            this,
                                                                            sliceNameEnum,
                                                                            subtype,
                                                                            subtype.getJointTypeEnum());
          componentIndictment.addFailingMeasurement(asymmetryMeasurement);
          componentInspectionData.getComponentInspectionResult().addIndictment(componentIndictment);
          AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                              componentInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              asymmetryMeasurement,
                                                              minimumPolaritySignalThreshold);
        }
      }
      else
      {
        if (jointAsymmetryMeasurement != null)
        {
          JointIndictment componentIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);
          componentIndictment.addFailingMeasurement(jointAsymmetryMeasurement);
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(componentIndictment);
          AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                          componentInspectionData.getPadOneJointInspectionData(),
                                                          reconstructionRegion,
                                                          sliceNameEnum,
                                                          jointAsymmetryMeasurement,
                                                          minimumPolaritySignalThreshold);
        }
      }            
      return false;
    }

    if (Config.isComponentLevelClassificationEnabled())
    {
      if (asymmetryMeasurement != null)
      {
        AlgorithmUtil.postPassingComponentTextualDiagnostic(this,
                                                        componentInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        asymmetryMeasurement,
                                                        minimumPolaritySignalThreshold);
      }
    }
    else
    {
      if (jointAsymmetryMeasurement != null)
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadOneJointInspectionData(),
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      jointAsymmetryMeasurement,
                                                      minimumPolaritySignalThreshold);
      }
    }    
    return true;
  }
  
  /**
   * @author Goh Bee Hoon
   */
  static public boolean isAsymmetryMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String misalignmentChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS);

    if (misalignmentChoice.equals(_ASYMMETRY_METHOD))
    {
      return true;
    }

    return false;
  }
  
  /**
   * @author Goh Bee Hoon
   */
  static public boolean isSlugEdgeDetectionMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String misalignmentChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS);

    if (misalignmentChoice.equals(_SLUG_EDGE_METHOD))
    {
      return true;
    }

    return false;
  }
}
