package com.axi.v810.business.imageAnalysis.pcap;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
public class PolarizedCapMeasurementAlgorithm extends Algorithm
{
  static private final int _PROFILE_SMOOTHING_STEP_SIZE = 3;
  static private final float _SCALING_FACTOR_FOR_DISPLAYING_PROFILES = 4.0f;
  private final static String _ASYMMETRY_METHOD = "Asymmetry Method";
  private final static String _SLUG_DETECTION_METHOD = "Slug Edge Method";
//  private String _name;
  private TimerUtil _timerUtil = new TimerUtil();
  //private static final String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";
  
  /**
   * @author Peter Esbensen
   */
  public PolarizedCapMeasurementAlgorithm(InspectionFamily polarizedCapInspectionFamily)
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.POLARIZED_CAP);

    Assert.expect(polarizedCapInspectionFamily != null);
    Assert.expect(_learnedAlgorithmSettingEnums != null);
    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();
    addMeasurementEnums();

    AlgorithmSetting nominalPadOneThickness = new AlgorithmSetting(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4.0f), // default
        MathUtil.convertMilsToMillimeters(0.10f), // min
        MathUtil.convertMilsToMillimeters(25.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      nominalPadOneThickness,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_FILLET_ONE_THICKNESS);
    addAlgorithmSetting(nominalPadOneThickness);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS);

    AlgorithmSetting nominalPadTwoThickness = new AlgorithmSetting(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4.0f), // default
        MathUtil.convertMilsToMillimeters(0.10f), // min
        MathUtil.convertMilsToMillimeters(25.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    polarizedCapInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.POLARIZED_CAP,
                                                                      nominalPadTwoThickness,
                                                                      SliceNameEnum.PAD,
                                                                      MeasurementEnum.PCAP_MEASUREMENT_FILLET_TWO_THICKNESS);
    addAlgorithmSetting(nominalPadTwoThickness);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS);

    AlgorithmSetting lowerFilletOffset = new AlgorithmSetting(AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET,
        displayOrder++,
        20.0f, // default
        -100.0f, // min
        500.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_LOWER_FILLET_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_LOWER_FILLET_OFFSET)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_LOWER_FILLET_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(lowerFilletOffset);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET);

    AlgorithmSetting openSignalSearchLength = new AlgorithmSetting(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH,
        displayOrder++,
        0.0f, // default
        0.0f, // min
        500.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(openSignalSearchLength);

    AlgorithmSetting upperFilletOffset = new AlgorithmSetting(AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET,
        displayOrder++,
        0.0f, // default
        -100.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_UPPER_FILLET_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_UPPER_FILLET_OFFSET)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_UPPER_FILLET_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(upperFilletOffset);
    //Ngie Xing, XCR-2190, Some settings are not being reset to their default values
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET);

    AlgorithmSetting filletEdgePercent = new AlgorithmSetting(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_FILLET_EDGE_PERCENT,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        300.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG,
        "HTML_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_FILLET_EDGE_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_FILLET_EDGE_PERCENT)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_FILLET_EDGE_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(filletEdgePercent);
    //Ngie Xing, XCR-2190, Some settings are not being reset to their default values
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.PCAP_MEASUREMENT_FILLET_EDGE_PERCENT);

    AlgorithmSetting backgroundRegionLocation = new AlgorithmSetting(AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT,
        displayOrder++,
        50f, // default
        0f, // min
        200f, // max
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(backgroundRegionLocation);
    //Ngie Xing, XCR-2190, Some settings are not being reset to their default values
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT);
    
    //Siew Yeng - XCR-3094
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList("False", "True"));
    AlgorithmSetting enableSaveAllJointImageWhenFailComponent = new AlgorithmSetting(
        AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS,
        displayOrder++,
        "False",
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // description URL key
        "HTML_DETAILED_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // desailed description URL key
        "IMG_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(enableSaveAllJointImageWhenFailComponent);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(40.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_PCAP_SLUG_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(40.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_MEASUREMENT_(USER_DEFINED_PCAP_SLUG_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(USER_DEFINED_PCAP_SLUG_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(USER_DEFINED_PCAP_SLUG_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);

    // Added by Lee Herng (4 Mar 2011)
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -200.0f), // minimum value
        MathUtil.convertMilsToMillimeters(200.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PCAP_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed desc
        "IMG_DESC_PCAP_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_PCAP_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_PCAP_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is displayed right before psh in the Slice Setup Tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_PCAP_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PCAP_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PCAP_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));

    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_FILLET_ONE_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_FILLET_TWO_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_PAD_ONE_LOWER_FILLET_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_PAD_TWO_LOWER_FILLET_THICKNESS);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());

    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());    
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      _componentMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS);
      _componentMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL);
    }
    else
    {
      _jointMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS);
      _jointMeasurementEnums.add(MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL);
    }
  }

  /**
   * Locate the upper and lower fillets and measure their thicknesses
   *
   * @author Peter Esbensen
   */
  private void collectStandardMeasurements(float[] componentThicknessProfile,
                                           RegionOfInterest componentRegion,
                                           JointInspectionData padOneJointInspectionData,
                                           JointInspectionData padTwoJointInspectionData,
                                           float[] componentDerivativeProfile,
                                           float pad1FilletIndex,
                                           float pad2FilletIndex,
                                           SliceNameEnum sliceNameEnum,
                                           ReconstructedSlice reconstructedSlice,
                                           ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(componentRegion != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(componentDerivativeProfile != null);
    Assert.expect(pad1FilletIndex >= 0);
    Assert.expect(pad2FilletIndex >= 0);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    FloatRef filletOneThicknessInMilsRef = new FloatRef();
    FloatRef filletTwoThicknessInMilsRef  = new FloatRef();
    FloatRef filletOneLowerRegionThicknessInMilsRef = new FloatRef();
    FloatRef filletTwoLowerRegionThicknessInMilsRef = new FloatRef();
    IntegerRef padOneUpperFilletIndexRef = new IntegerRef();
    IntegerRef padOneLowerFilletIndexRef = new IntegerRef();
    IntegerRef padTwoUpperFilletIndexRef = new IntegerRef();
    IntegerRef padTwoLowerFilletIndexRef = new IntegerRef();

    Subtype subtype = padOneJointInspectionData.getSubtype();
    float upperFilletOffsetAsFractionOfPadLength = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET) * 0.01f;
    float lowerFilletOffsetAsFractionOfPadLength = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET) * 0.01f;
    float openSignalSearchLengthAsFractionOfPadLength = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH) * 0.01f;

    getFilletThicknesses(componentThicknessProfile, padOneJointInspectionData,
                                     padTwoJointInspectionData, pad1FilletIndex, pad2FilletIndex,
                                     filletOneThicknessInMilsRef,
                                     filletTwoThicknessInMilsRef, filletOneLowerRegionThicknessInMilsRef,
                                     filletTwoLowerRegionThicknessInMilsRef, padOneUpperFilletIndexRef,
                                     padOneLowerFilletIndexRef, padTwoUpperFilletIndexRef,
                                     padTwoLowerFilletIndexRef, upperFilletOffsetAsFractionOfPadLength,
                                     lowerFilletOffsetAsFractionOfPadLength,
                                     openSignalSearchLengthAsFractionOfPadLength);

    int padOneUpperFilletIndex = padOneUpperFilletIndexRef.getValue();
    int padOneLowerFilletIndex = padOneLowerFilletIndexRef.getValue();
    int padTwoUpperFilletIndex = padTwoUpperFilletIndexRef.getValue();
    int padTwoLowerFilletIndex = padTwoLowerFilletIndexRef.getValue();

    int padOneLeftSideIndex = componentThicknessProfile.length - 1 -
                              padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                              getLengthAlong();
    int padTwoRightSideIndex = padTwoJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                               getLengthAlong();

    storeAndPostClearPolarizedCapMeasurements(padOneJointInspectionData, padTwoJointInspectionData,
                                              reconstructionRegion, sliceNameEnum, filletOneThicknessInMilsRef.getValue(),
                                              filletTwoThicknessInMilsRef.getValue(),
                                              filletOneLowerRegionThicknessInMilsRef.getValue(),
                                              filletTwoLowerRegionThicknessInMilsRef.getValue());

    postClearPolarizedCapFilletProfileDiagnostics(componentThicknessProfile, (int)pad1FilletIndex,
                                              (int)pad2FilletIndex, padOneUpperFilletIndex,
                                              padOneLowerFilletIndex, padTwoUpperFilletIndex,
                                              padTwoLowerFilletIndex, padTwoRightSideIndex,
                                              padOneLeftSideIndex, reconstructionRegion,
                                              sliceNameEnum, padOneJointInspectionData);
  }

  /**
   * @author Peter Esbensen
   */
  private int findBestOpenSignalIndex(float[] componentThicknessProfile,
                                      int lowerFilletOffsetRelativeToUpperFilletInPixels,
                                      int centerOfSearchIndex,
                                      int searchWidth)
  {
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(centerOfSearchIndex >= 0);
    Assert.expect(centerOfSearchIndex < componentThicknessProfile.length);
    Assert.expect(searchWidth >= 0);

    int startIndex = Math.max(0, centerOfSearchIndex - searchWidth / 2);
    int endIndex = Math.min(componentThicknessProfile.length - 1, centerOfSearchIndex + searchWidth / 2);

    float bestOpenSignal = Float.MIN_VALUE;
    int bestOpenSignalIndex = startIndex;
    for (int i = startIndex; i < endIndex; ++i)
    {
      if (((i + lowerFilletOffsetRelativeToUpperFilletInPixels) >= componentThicknessProfile.length ) ||
          ((i + lowerFilletOffsetRelativeToUpperFilletInPixels) < 0))
        break; // don't search if we run out of room on the profile.
      float openSignal = componentThicknessProfile[i] - componentThicknessProfile[ i + lowerFilletOffsetRelativeToUpperFilletInPixels ];
      if (openSignal > bestOpenSignal)
      {
        bestOpenSignal = openSignal;
        bestOpenSignalIndex = i;
      }
    }
    return bestOpenSignalIndex;
  }

  /**
   * Measure the Upper and Lower fillet thicknesses.  Their locations are controlled by the upperFilletOffsetAsFractionOfPadLength
   * and lowerFilletOffsetAsFractionOfPadLength parameters.  Note that both of these parameters are relative to the
   * fillet edges, which are located at pad1FilletIndex and pad2FilletIndex.  The offsets move the locations inwards
   * towards the body of the component if they are positive and away from the body if they are negative.  If the user
   * has specified a search length, then those fillet locations will be shifted in unison until they find the best
   * open signal within that specified search area.
   *
   * @author Peter Esbensen
   */
  private void getFilletThicknesses(float[] componentThicknessProfile,
      JointInspectionData padOneJointInspectionData,
      JointInspectionData padTwoJointInspectionData,
      float pad1FilletIndex,
      float pad2FilletIndex,
      FloatRef filletOneThicknessInMilsRef,
      FloatRef filletTwoThicknessInMilsRef,
      FloatRef filletOneLowerRegionThicknessInMilsRef,
      FloatRef filletTwoLowerRegionThicknessInMilsRef,
      IntegerRef padOneUpperFilletIndexRef,
      IntegerRef padOneLowerFilletIndexRef,
      IntegerRef padTwoUpperFilletIndexRef,
      IntegerRef padTwoLowerFilletIndexRef,
      float upperFilletOffsetAsFractionOfPadLength,
      float lowerFilletOffsetAsFractionOfPadLength,
      float openSignalSearchLengthAsFractionOfPadLength)
  {
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(filletOneThicknessInMilsRef != null);
    Assert.expect(filletTwoThicknessInMilsRef != null);
    Assert.expect(filletOneLowerRegionThicknessInMilsRef != null);
    Assert.expect(filletTwoLowerRegionThicknessInMilsRef != null);
    Assert.expect(padOneUpperFilletIndexRef != null);
    Assert.expect(padOneLowerFilletIndexRef != null);
    Assert.expect(padTwoUpperFilletIndexRef != null);
    Assert.expect(padTwoLowerFilletIndexRef != null);

    int padOneLengthAlong = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                            getLengthAlong();

    int upperFilletOffsetInPixels = Math.round(upperFilletOffsetAsFractionOfPadLength * padOneLengthAlong);

    int lowerFilletOffsetRelativeToUpperFilletInPixels = Math.round((lowerFilletOffsetAsFractionOfPadLength * padOneLengthAlong) -
                                                                    upperFilletOffsetInPixels);

    int centerOfSearchIndex = forceIndexOntoPadOne(padOneJointInspectionData, componentThicknessProfile,
                                                   Math.round(pad1FilletIndex - upperFilletOffsetInPixels));
    int searchWidth = Math.round(openSignalSearchLengthAsFractionOfPadLength * padOneLengthAlong);
    int padOneBestOpenSignalLocationIndex = findBestOpenSignalIndex(componentThicknessProfile,
        -1 * lowerFilletOffsetRelativeToUpperFilletInPixels, centerOfSearchIndex, searchWidth);

    centerOfSearchIndex = forceIndexOntoPadTwo(padTwoJointInspectionData, componentThicknessProfile,
                                               Math.round(pad2FilletIndex + upperFilletOffsetAsFractionOfPadLength * padOneLengthAlong));
    int padTwoBestOpenSignalLocationIndex = findBestOpenSignalIndex(componentThicknessProfile,
        lowerFilletOffsetRelativeToUpperFilletInPixels, centerOfSearchIndex, searchWidth);

    // store the indices and measurements into the Reference objects to return to the calling function
    padOneUpperFilletIndexRef.setValue(forceIndexOntoPadOne(padOneJointInspectionData, componentThicknessProfile, padOneBestOpenSignalLocationIndex));
    filletOneThicknessInMilsRef.setValue(componentThicknessProfile[padOneBestOpenSignalLocationIndex]);

    padOneLowerFilletIndexRef.setValue(forceIndexOntoPadOne(padOneJointInspectionData, componentThicknessProfile, padOneBestOpenSignalLocationIndex - lowerFilletOffsetRelativeToUpperFilletInPixels));
    filletOneLowerRegionThicknessInMilsRef.setValue(componentThicknessProfile[padOneLowerFilletIndexRef.getValue()]);

    padTwoUpperFilletIndexRef.setValue(forceIndexOntoPadTwo(padTwoJointInspectionData, componentThicknessProfile,  padTwoBestOpenSignalLocationIndex));
    filletTwoThicknessInMilsRef.setValue(componentThicknessProfile[padTwoBestOpenSignalLocationIndex]);

    padTwoLowerFilletIndexRef.setValue(forceIndexOntoPadTwo(padTwoJointInspectionData, componentThicknessProfile,  padTwoBestOpenSignalLocationIndex + lowerFilletOffsetRelativeToUpperFilletInPixels));
    filletTwoLowerRegionThicknessInMilsRef.setValue(componentThicknessProfile[padTwoLowerFilletIndexRef.getValue()]);
  }

  /**
   * @author Peter Esbensen
   */
  private int forceIndexOntoPadOne(JointInspectionData padOneJointInspectionData, float[] profile, int index)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(profile != null);

    return Math.max(getPadOneLeftSideIndex(profile, padOneJointInspectionData),
                    Math.min(getPadOneRightSideIndex(profile), index));
  }

  /**
   * @author Peter Esbensen
   */
  private int forceIndexOntoPadTwo(JointInspectionData padTwoJointInspectionData, float[] profile, int index)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(profile != null);

    return Math.max(getPadTwoLeftSideIndex(), Math.min(getPadTwoRightSideIndex(padTwoJointInspectionData), index));
  }

  /**
   * Unlike Chip, we do not include a profile extension on the outsides of the pads, so these methods are slightly
   * different from the Chip versions of them.
   *
   * @author Peter Esbensen
   */
  public static int getPadOneRightSideIndex(float[] componentProfile)
  {
    Assert.expect(componentProfile != null);

    return componentProfile.length - 1;
  }

  /**
   * Unlike Chip, we do not include a profile extension on the outsides of the pads, so these methods are slightly
   * different from the Chip versions of them.
   *
   * @author Peter Esbensen
   */
  public static int getPadOneLeftSideIndex(float[] componentProfile,
                                           JointInspectionData padOneJointInspectionData)
  {
    Assert.expect(componentProfile != null);
    Assert.expect(padOneJointInspectionData != null);

    int padOneLength = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getLengthAlong();
    int padOneLeftSideIndex = componentProfile.length - 1 - padOneLength;
    return padOneLeftSideIndex;
  }

  /**
   * Unlike Chip, we do not include a profile extension on the outsides of the pads, so these methods are slightly
   * different from the Chip versions of them.
   *
   * @author Peter Esbensen
   */
  public static int getPadTwoLeftSideIndex()
  {
    return 0;
  }

  /**
   * Unlike Chip, we do not include a profile extension on the outsides of the pads, so these methods are slightly
   * different from the Chip versions of them.
   *
   * @author Peter Esbensen
   */
  public static int getPadTwoRightSideIndex(JointInspectionData padTwoJointInspectionData)
  {
    Assert.expect(padTwoJointInspectionData != null);
    int padTwoLength = padTwoJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getLengthAlong();
    return padTwoLength - 1;
  }

  /**
   * @author Peter Esbensen
   */
  private void storeAndPostClearPolarizedCapMeasurements(JointInspectionData padOneJointInspectionData,
      JointInspectionData padTwoJointInspectionData,
      ReconstructionRegion reconstructionRegion,
      SliceNameEnum sliceNameEnum,
      float filletOneThicknessInMils,
      float filletTwoThicknessInMils,
      float filletOneLowerRegionThicknessInMils,
      float filletTwoLowerRegionThicknessInMils)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(filletOneThicknessInMils >= 0.0f);
    Assert.expect(filletTwoThicknessInMils >= 0.0f);
    Assert.expect(filletOneLowerRegionThicknessInMils >= 0.0f);
    Assert.expect(filletTwoLowerRegionThicknessInMils >= 0.0f);

    JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();
    JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();
    Pad padOne = padOneJointInspectionData.getPad();
    Pad padTwo = padTwoJointInspectionData.getPad();

    // store pad one upper fillet thickness
    JointMeasurement padOneFilletThicknessMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_MEASUREMENT_FILLET_ONE_THICKNESS,
        MeasurementUnitsEnum.MILLIMETERS,
        padOne,
        sliceNameEnum,
        MathUtil.convertMilsToMillimeters(filletOneThicknessInMils));
    padOneJointInspectionResult.addMeasurement(padOneFilletThicknessMeasurement);

    // store pad one lower fillet thickness
    JointMeasurement padOneLowerFilletThicknessMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_MEASUREMENT_PAD_ONE_LOWER_FILLET_THICKNESS,
        MeasurementUnitsEnum.MILLIMETERS,
        padOne,
        sliceNameEnum,
        MathUtil.convertMilsToMillimeters(filletOneLowerRegionThicknessInMils));
    padOneJointInspectionResult.addMeasurement(padOneLowerFilletThicknessMeasurement);

    // store pad two upper fillet thickness
    JointMeasurement padTwoFilletThicknessMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_MEASUREMENT_FILLET_TWO_THICKNESS,
        MeasurementUnitsEnum.MILLIMETERS,
        padTwo,
        sliceNameEnum,
        MathUtil.convertMilsToMillimeters(filletTwoThicknessInMils));
    padTwoJointInspectionResult.addMeasurement(padTwoFilletThicknessMeasurement);



    // store pad two lower fillet thickness
    JointMeasurement padTwoLowerFilletThicknessMeasurement = new JointMeasurement(this,
        MeasurementEnum.PCAP_MEASUREMENT_PAD_TWO_LOWER_FILLET_THICKNESS,
        MeasurementUnitsEnum.MILLIMETERS,
        padTwo,
        sliceNameEnum,
        MathUtil.convertMilsToMillimeters(filletTwoLowerRegionThicknessInMils));
    padTwoJointInspectionResult.addMeasurement(padTwoLowerFilletThicknessMeasurement);
    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructionRegion,
                                                    padOneJointInspectionData,
                                                    padOneFilletThicknessMeasurement,
                                                    padOneLowerFilletThicknessMeasurement,
                                                    padTwoFilletThicknessMeasurement,
                                                    padTwoLowerFilletThicknessMeasurement);
  }

  /**
   * @author Peter Esbensen
   */
  private void postClearPolarizedCapFilletProfileDiagnostics(float[] componentBodyThicknessProfile,
      int padOneFilletEdgeIndex,
      int padTwoFilletEdgeIndex,
      int padOneFilletIndex,
      int padOneLowerFilletIndex,
      int padTwoFilletIndex,
      int padTwoLowerFilletIndex,
      int padTwoRightSideIndex,
      int padOneLeftSideIndex,
      ReconstructionRegion reconstructionRegion,
      SliceNameEnum sliceNameEnum,
      JointInspectionData padOneJointInspectionData)
  {
    Assert.expect(componentBodyThicknessProfile != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoFilletIndex >= 0);
    Assert.expect(padTwoRightSideIndex >= padTwoFilletIndex);
    Assert.expect(padOneLeftSideIndex >= padTwoRightSideIndex);
    Assert.expect(padOneFilletEdgeIndex >= padOneLeftSideIndex);
    Assert.expect(padTwoFilletEdgeIndex <= padTwoRightSideIndex);

    if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getJointTypeEnum(), this))
    {
      int lengthOfProfile = componentBodyThicknessProfile.length;

      MeasurementRegionEnum[] backgroundMeasurementRegionEnums = new MeasurementRegionEnum[lengthOfProfile];
      RegionOfInterest componentRegion = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

      // first set everything to background
      Arrays.fill(backgroundMeasurementRegionEnums, MeasurementRegionEnum.BACKGROUND_REGION);

      // now fill in the pad areas
      Arrays.fill(backgroundMeasurementRegionEnums, 0, padTwoRightSideIndex, MeasurementRegionEnum.PAD_REGION);
      int padOneLength = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getLengthAlong();
      Arrays.fill(backgroundMeasurementRegionEnums, lengthOfProfile - padOneLength, lengthOfProfile,
                  MeasurementRegionEnum.PAD_REGION);

      MeasurementRegionEnumArrayManager arrayManager = new MeasurementRegionEnumArrayManager();
      arrayManager.setBackgroundArray(backgroundMeasurementRegionEnums);

      // highlight the pad one upper fillet region
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneFilletIndex,
                                                                      MeasurementRegionEnum.UPPER_FILLET_REGION);
      RegionOfInterest padOneUpperFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padOneFilletIndex);
      MeasurementRegionDiagnosticInfo padOneUpperFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneUpperFilletRegion,
                                                                                                                  MeasurementRegionEnum.UPPER_FILLET_REGION);

      // highlight the pad one lower fillet region
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneLowerFilletIndex,
                                                                      MeasurementRegionEnum.LOWER_FILLET_REGION);
      RegionOfInterest padOneLowerFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padOneLowerFilletIndex);
      MeasurementRegionDiagnosticInfo padOneLowerFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneLowerFilletRegion,
                                                                                                                  MeasurementRegionEnum.LOWER_FILLET_REGION);

      // highlight the pad two upper fillet region
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoFilletIndex,
                                                                      MeasurementRegionEnum.UPPER_FILLET_REGION);
      RegionOfInterest padTwoUpperFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padTwoFilletIndex);
      MeasurementRegionDiagnosticInfo padTwoUpperFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoUpperFilletRegion,
                                                                                                                  MeasurementRegionEnum.UPPER_FILLET_REGION);


      // highlight the pad two lower fillet region
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoLowerFilletIndex,
                                                                      MeasurementRegionEnum.LOWER_FILLET_REGION);
      RegionOfInterest padTwoLowerFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padTwoLowerFilletIndex);
      MeasurementRegionDiagnosticInfo padTwoLowerFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoLowerFilletRegion,
                                                                                                                  MeasurementRegionEnum.LOWER_FILLET_REGION);


      // highlight the pad one fillet edge
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneFilletEdgeIndex,
                                                                      MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

      // highlight the pad two fillet edge
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoFilletEdgeIndex,
                                                                      MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

      // the componentRegion is longer than the actual component.  Shrink it down to the located edges
      RegionOfInterest modifiedComponentRegion = AlgorithmUtil.getComponentRegionOfInterestBasedOnFilletEdges(componentRegion,
                                                                                                              padOneFilletEdgeIndex,
                                                                                                              padTwoFilletEdgeIndex);
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(modifiedComponentRegion,
                                                                                                          MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
      componentRegionDiagnosticInfo.setLabel(padOneJointInspectionData.getComponent().getReferenceDesignator());

      // use the nominal pad thickness to determine how to scale the profile
      Subtype subtype = padOneJointInspectionData.getSubtype();
      float nominalFilletThicknessInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS);
      float suggestedMaxVerticalScaleInMils = MathUtil.convertMillimetersToMils(nominalFilletThicknessInMillimeters * _SCALING_FACTOR_FOR_DISPLAYING_PROFILES);

      LocalizedString padThicknessProfileLabelLocalizedString =
          new LocalizedString("ALGIAG_PCAP_MEASUREMENT_CLEAR_PCAP_FILLET_REGIONS_PROFILE_KEY", null);
      ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padThicknessProfileLabelLocalizedString,
                                                                           0.0f,
                                                                           suggestedMaxVerticalScaleInMils,
                                                                           componentBodyThicknessProfile,
                                                                           arrayManager.getFinalArray(),
                                                                           MeasurementUnitsEnum.MILS);
      _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum,
                                   padOneJointInspectionData.getSubtype(),
                                   this, false, true, labeledPadThicknessProfileDiagInfo,
                                   padOneUpperFilletRegionDiagnosticInfo,
                                   padOneLowerFilletRegionDiagnosticInfo,
                                   padTwoUpperFilletRegionDiagnosticInfo,
                                   padTwoLowerFilletRegionDiagnosticInfo,
                                   componentRegionDiagnosticInfo);
    }
  }
  
   /**
   * @author Peter Esbensen
   */
  private void postClearPolarizedCapSlugProfileDiagnostics(float[] componentProfile,
      int padOneSlugEdgeIndex,
      int padTwoSlugEdgeIndex,
      ReconstructionRegion reconstructionRegion,
      SliceNameEnum sliceNameEnum,
      JointInspectionData padOneJointInspectionData)
  {
    Assert.expect(componentProfile != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padOneSlugEdgeIndex >= padTwoSlugEdgeIndex);

    if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getJointTypeEnum(), this))
    {
      int lengthOfProfile = componentProfile.length;

      MeasurementRegionEnum[] backgroundMeasurementRegionEnums = new MeasurementRegionEnum[lengthOfProfile];
      RegionOfInterest componentRegion = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

      // first set everything to background
      Arrays.fill(backgroundMeasurementRegionEnums, MeasurementRegionEnum.BACKGROUND_REGION);

      // now fill in the pad areas
      Arrays.fill(backgroundMeasurementRegionEnums, 0, lengthOfProfile, MeasurementRegionEnum.PAD_REGION);
      int padOneLength = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getLengthAlong();
      Arrays.fill(backgroundMeasurementRegionEnums, lengthOfProfile - padOneLength, lengthOfProfile,
                  MeasurementRegionEnum.PAD_REGION);

      MeasurementRegionEnumArrayManager arrayManager = new MeasurementRegionEnumArrayManager();
      arrayManager.setBackgroundArray(backgroundMeasurementRegionEnums);
      
      // highlight the pad one fillet edge
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneSlugEdgeIndex,
                                                                      MeasurementRegionEnum.SLUG_EDGE_REGION);

      // highlight the pad two fillet edge
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoSlugEdgeIndex,
                                                                      MeasurementRegionEnum.SLUG_EDGE_REGION);
      
      // the componentRegion is longer than the actual component.  Shrink it down to the located edges
      RegionOfInterest modifiedComponentRegion = AlgorithmUtil.getComponentRegionOfInterestBasedOnFilletEdges(componentRegion,
                                                                                                              padOneSlugEdgeIndex,
                                                                                                              padTwoSlugEdgeIndex);
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(modifiedComponentRegion,
                                                                                                          MeasurementRegionEnum.SLUG_EDGE_REGION);
      LocalizedString componentSlugLabelLocalizedString = new LocalizedString("ALGIAG_PCAP_MEASUREMENT_SLUG_REGION_KEY", null);
      String name = StringLocalizer.keyToString(componentSlugLabelLocalizedString);
      componentRegionDiagnosticInfo.setLabel(name);
      
      LocalizedString padThicknessProfileLabelLocalizedString =
          new LocalizedString("ALGIAG_PCAP_MEASUREMENT_COMPONENT_THICKNESS_PROFILE_KEY", null);
      ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.BACKGROUND_PROFILE,
                                                                           padThicknessProfileLabelLocalizedString,
                                                                           0.0f,
                                                                           ArrayUtil.max(componentProfile),
                                                                           componentProfile,
                                                                           arrayManager.getFinalArray(),
                                                                           MeasurementUnitsEnum.NONE);
      _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum,
                                   padOneJointInspectionData.getSubtype(),
                                   this, false, true, labeledPadThicknessProfileDiagInfo,
                                   componentRegionDiagnosticInfo);
    }
  }

  /**
   * @author Goh Bee Hoon
   */
  private void postClearPolarizedCapSlugRLHSProfileDiagnostics(float[] componentProfile,
      RegionOfInterest modifiedComponentRegionOfInterest,
      ReconstructionRegion reconstructionRegion,
      SliceNameEnum sliceNameEnum,
      JointInspectionData padOneJointInspectionData,
      int padNumber)
  {
    Assert.expect(componentProfile != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(padOneJointInspectionData != null);

    if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getJointTypeEnum(), this))
    {
      int lengthOfProfile = componentProfile.length;

      MeasurementRegionEnum[] backgroundMeasurementRegionEnums = new MeasurementRegionEnum[lengthOfProfile];

      // first set everything to background
      Arrays.fill(backgroundMeasurementRegionEnums, MeasurementRegionEnum.BACKGROUND_REGION);

      // now fill in the pad areas
      Arrays.fill(backgroundMeasurementRegionEnums, 0, lengthOfProfile, MeasurementRegionEnum.PAD_REGION);

      MeasurementRegionEnumArrayManager arrayManager = new MeasurementRegionEnumArrayManager();
      arrayManager.setBackgroundArray(backgroundMeasurementRegionEnums);
      
      // the componentRegion is longer than the actual component.  Shrink it down to the located edges
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(modifiedComponentRegionOfInterest,
                                                                                                          MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
      LocalizedString componentSlugLabelLocalizedString = new LocalizedString("MMGUI_SETUP_PAD_KEY", null);
      String name = StringLocalizer.keyToString(componentSlugLabelLocalizedString); 
      componentRegionDiagnosticInfo.setLabel(name + padNumber);
      
      LocalizedString padThicknessProfileLabelLocalizedString =
          new LocalizedString("ALGIAG_PCAP_MEASUREMENT_ADJUSTED_GRAY_LEVEL_PROFILE_KEY", null);
      ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getBoardNameAndReferenceDesignator() + " " + name + padNumber,
                                                                           ProfileTypeEnum.BACKGROUND_PROFILE,
                                                                           padThicknessProfileLabelLocalizedString,
                                                                           0.0f,
                                                                           ArrayUtil.max(componentProfile),
                                                                           componentProfile,
                                                                           arrayManager.getFinalArray(),
                                                                           MeasurementUnitsEnum.NONE);
      _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum,
                                   padOneJointInspectionData.getSubtype(),
                                   this, false, true, labeledPadThicknessProfileDiagInfo,
                                   componentRegionDiagnosticInfo);
    }
  }
  
  /**
   * @author Peter Esbensen
   */
  private RegionOfInterest runLocator(ReconstructedImages reconstructedImages,
                          RegionOfInterest componentRegionOfInterest,
                          ComponentInspectionData componentInspectionData)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(componentRegionOfInterest != null);

    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    Subtype subtype = padOneJointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = subtype.getInspectionFamily().getSliceNameEnumForLocator(subtype);
    Locator.locateRectangularComponent(reconstructedImages, sliceNameEnum,
                                       componentInspectionData, this, true);
    
    // Vincent Tan - XCR-3252 Assert during Run Custom Diagnostic
    // Copy the locator xy center from pad slice to pcap slug slice 
    // due to the need to visualize it during custom diagnostic
    float xCenter = componentInspectionData.getComponentInspectionResult().getComponentMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_X_LOCATION).getValue();
    float yCenter = componentInspectionData.getComponentInspectionResult().getComponentMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_Y_LOCATION).getValue();
    
    componentInspectionData.getComponentInspectionResult().addMeasurement(new ComponentMeasurement(
      this,
      subtype,
      MeasurementEnum.LOCATOR_X_LOCATION,
      MeasurementUnitsEnum.PIXELS,
      componentInspectionData.getComponent(),
      SliceNameEnum.PCAP_SLUG,
      xCenter
    ));
    
    componentInspectionData.getComponentInspectionResult().addMeasurement(new ComponentMeasurement(
      this,
      subtype,
      MeasurementEnum.LOCATOR_Y_LOCATION,
      MeasurementUnitsEnum.PIXELS,
      componentInspectionData.getComponent(),
      SliceNameEnum.PCAP_SLUG,
      yCenter
    ));

    return Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData, sliceNameEnum, true);
  }




  /**
   * @author Peter Esbensen
   */
  private float[] getComponentThicknessProfile(ReconstructedSlice reconstructedSlice,
                                               ReconstructionRegion reconstructionRegion,
                                               RegionOfInterest componentRegionOfInterest,
                                               JointInspectionData padOneJointInspectionData) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(padOneJointInspectionData != null);

    // get a profile over the component
    float[] componentProfile = getComponentProfile(reconstructionRegion,
        reconstructedSlice,
        componentRegionOfInterest);

    // get an average of the two background profiles
    Subtype subtype = padOneJointInspectionData.getSubtype();
    
    float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f * (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT);
    float[] backgroundProfile = AlgorithmUtil.getComponentBasedBackgroundProfile(reconstructionRegion, reconstructedSlice,
        componentRegionOfInterest, padOneJointInspectionData, backgroundRegionShiftAsFractionOfInterPadDistance, this, true);

    // compute component thickness profile
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityThreshold = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float[] componentThicknessProfile = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMils(componentProfile,
                                                                                                      backgroundProfile,
                                                                                                      thicknessTable,
                                                                                                      backgroundSensitivityThreshold);

    return componentThicknessProfile;
  }

  /**
   * @author Peter Esbensen
   */
  private void locateEdges(JointInspectionData padOneJointInspectionData,
                           JointInspectionData padTwoJointInspectionData,
                           float[] componentBodyThicknessProfile,
                           FloatRef padOneFilletEdgeIndex,
                           FloatRef padTwoFilletEdgeIndex)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentBodyThicknessProfile != null);
    Assert.expect(padOneFilletEdgeIndex != null);
    Assert.expect(padTwoFilletEdgeIndex != null);

    Subtype subtype = padOneJointInspectionData.getSubtype();

    int padTwoLengthAlong = padTwoJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                            getLengthAlong();
    int padOneLengthAlong = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                            getLengthAlong();

    float padOneMedianThicknessInMils = getPadOneMedianThicknessInMils(componentBodyThicknessProfile, padOneJointInspectionData);
    float padTwoMedianThicknessInMils = getPadTwoMedianThicknessInMils(componentBodyThicknessProfile, padTwoJointInspectionData);
    float minimumOfTheTwoMedianThicknessesInMils = Math.min(padOneMedianThicknessInMils, padTwoMedianThicknessInMils);

    float edgeThicknessInMils = Math.max(0.0f, (2f * minimumOfTheTwoMedianThicknessesInMils) *
                          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_FILLET_EDGE_PERCENT) * 0.01f);

    padTwoFilletEdgeIndex.setValue(ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(componentBodyThicknessProfile,
        0, padTwoLengthAlong, edgeThicknessInMils));
    padOneFilletEdgeIndex.setValue(ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(componentBodyThicknessProfile,
        componentBodyThicknessProfile.length - 1,
        componentBodyThicknessProfile.length - 1 - padOneLengthAlong, edgeThicknessInMils));
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    _timerUtil.reset();
    _timerUtil.start();
    
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
      JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
      Subtype subtype = padOneJointInspectionData.getSubtype();

      // get the component region
      RegionOfInterest componentRegionOfInterest = componentInspectionData.getOrthogonalComponentRegionOfInterest();

      SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

      //Siew Yeng - XCR-2683 - add enhanced image
      if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
      {
        ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, sliceNameEnum);
      }
      
      // run locator
      componentRegionOfInterest = runLocator(reconstructedImages, componentRegionOfInterest,
                 padOneJointInspectionData.getComponentInspectionData());

      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                padOneJointInspectionData,
                                                reconstructionRegion,
                                                reconstructedSlice,
                                                false);

      // compute component thickness profile
      float[] componentThicknessProfile = getComponentThicknessProfile(reconstructedSlice,
          reconstructionRegion, componentRegionOfInterest, padOneJointInspectionData);

      // get a derivative profile
      float[] componentDerivativeProfile = ProfileUtil.createDerivativeProfile(componentThicknessProfile,
          _PROFILE_SMOOTHING_STEP_SIZE);

      // find the pad fillet locations
      FloatRef pad1FilletIndexRef = new FloatRef();
      FloatRef pad2FilletIndexRef = new FloatRef();
      locateEdges(padOneJointInspectionData, padTwoJointInspectionData,
                  componentThicknessProfile, pad1FilletIndexRef, pad2FilletIndexRef);

      float pad1FilletIndex = pad1FilletIndexRef.getValue();
      float pad2FilletIndex = pad2FilletIndexRef.getValue();

      collectStandardMeasurements(componentThicknessProfile, componentRegionOfInterest, padOneJointInspectionData,
                                  padTwoJointInspectionData, componentDerivativeProfile,
                                  pad1FilletIndex, pad2FilletIndex, sliceNameEnum,
                                  reconstructedSlice, reconstructionRegion);
        
      measureAsymmetryAndSlugThickness(componentInspectionData, componentRegionOfInterest,
                                        reconstructedImages,
                                        Math.round(pad1FilletIndex),
                                        Math.round(pad2FilletIndex),
                                        subtype);
      
      if (PolarizedCapMisalignmentAlgorithm.isSlugEdgeDetectionMethod(subtype))
      {
         measureSlugGrayValue(componentInspectionData, componentRegionOfInterest,
                            reconstructedImages,
                            Math.round(pad1FilletIndex),
                            Math.round(pad2FilletIndex),
                            subtype);
      }
    }
    _timerUtil.stop();
    //System.out.println("Polarized Capacitor Misalignment Classify Joint: " + _timerUtil.getElapsedTimeInMillis());
  }

  /**
   * The gray value profile is measured at slug edge on both side.
   * The final profile is generated from the subtraction of LHS (pad 2) gray value profile and RHS (pad 1) gray value profile.
   * 
   * @author Goh Bee Hoon
   */
  private void measureSlugGrayValue(ComponentInspectionData componentInspectionData,
                                    RegionOfInterest componentRegionOfInterest,
                                    ReconstructedImages reconstructedImages,
                                    int filletOneEdgeIndex,
                                    int filletTwoEdgeIndex,
                                    Subtype subtype) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(filletOneEdgeIndex >= 0);
    Assert.expect(filletTwoEdgeIndex >= 0);
    Assert.expect(filletOneEdgeIndex >= filletTwoEdgeIndex);
    Assert.expect(subtype != null);
    Assert.expect(subtype.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP));

    SliceNameEnum sliceNameEnum = SliceNameEnum.PCAP_SLUG;
    ReconstructedSlice slugReconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    
    //Siew Yeng - XCR-2683 - add enhanced image
    if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
    {
      ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, sliceNameEnum);
    }
    
    // get component gray value profile
    float[] componentProfile = getComponentProfile(reconstructionRegion, slugReconstructedSlice, componentRegionOfInterest);
    
    // get a profile over the component    
    float[] componentThicknessProfile = getComponentThicknessProfile(slugReconstructedSlice, reconstructionRegion, 
                                                                     componentRegionOfInterest, padOneJointInspectionData);
    
    // get the threshold value to search thru the component thickness profile
    float thresholdValue = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_THRESHOLD);
    float padOneSlugEdgeIndex = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(componentThicknessProfile, 0, componentThicknessProfile.length/2, thresholdValue);
    float padTwoSlugEdgeIndex = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(componentThicknessProfile, componentThicknessProfile.length-1, componentThicknessProfile.length/2, thresholdValue);
       
    // get component pad ROI
    RegionOfInterest padOneRegion = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padTwoRegion = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
   
    // set lhsROI as pad 2 region, rhsROI as pad 1 region
    RegionOfInterest lhsROI = new RegionOfInterest(padTwoRegion);
    RegionOfInterest rhsROI = new RegionOfInterest(padOneRegion);
    double lhsMinROICoordinate = lhsROI.getMinCoordinateAlong();
    double rhsMinROICoordinate = rhsROI.getMinCoordinateAlong();

    // set LHSROI and RHSROI with specified lengthAlong and lengthAcross
    float lengthAcrossValue = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ACROSS);
    float lengthAlongValue = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ALONG);
    float positionValue = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MISALIGNMENT_SLUG_EDGE_REGION_POSITION);
    lhsROI.setLengthAlong(Math.round(lengthAlongValue));
    rhsROI.setLengthAlong(Math.round(lengthAlongValue));
    lhsROI.setLengthAcrossWithAdjustableCenter(Math.round(lengthAcrossValue), positionValue);
    rhsROI.setLengthAcrossWithAdjustableCenter(Math.round(lengthAcrossValue), positionValue);
    
    // move lhsROI and rhsROI to the edge of the slug 
    if (lhsMinROICoordinate > rhsMinROICoordinate) // pad 1 on right, pad 2 on left / pad 1 at above, pad 2 at bottom
    { 
      lhsROI.setMinCoordinateAlong((int)rhsMinROICoordinate + (componentProfile.length - Math.round(padOneSlugEdgeIndex)));
      rhsROI.setMinCoordinateAlong((int)rhsMinROICoordinate + (componentProfile.length - Math.round(padTwoSlugEdgeIndex)) - rhsROI.getLengthAlong());
    } 
    else if (lhsMinROICoordinate < rhsMinROICoordinate) // pad 2 on right, pad 1 on left / pad 2 at above, pad 1 at bottom
    {
      rhsROI.setMinCoordinateAlong((int)lhsMinROICoordinate + Math.round(padTwoSlugEdgeIndex));
      lhsROI.setMinCoordinateAlong((int)lhsMinROICoordinate + Math.round(padOneSlugEdgeIndex) - lhsROI.getLengthAlong());
    }
    
    // set the orientation degree in order to get a vertical profile 
    lhsROI.setOrientationInDegrees(lhsROI.getOrientationInDegrees() + 90);
    rhsROI.setOrientationInDegrees(rhsROI.getOrientationInDegrees() - 90);
    
    float[] componentProfileLHS = getComponentProfile(reconstructionRegion, slugReconstructedSlice, lhsROI);
    float[] componentProfileRHS = getComponentProfile(reconstructionRegion, slugReconstructedSlice, rhsROI);
    
    // make the lhs profile and rhs profile to have the same max value (profile leveling)
    float maxValueProfileLHS = ArrayUtil.max(componentProfileLHS);
    float maxValueProfileRHS = ArrayUtil.max(componentProfileRHS);
    for (int ind = 0; ind < componentProfileLHS.length; ind++)
    {
      componentProfileLHS[ind] = componentProfileLHS[ind] + maxValueProfileRHS - maxValueProfileLHS;
    }
    
    // subtract both lhs profile and rhs profile to get the diff profile
    float[] endProfile = ArrayUtil.subtractArrays(componentProfileLHS, componentProfileRHS);
    // calculate polarity value
    float newPolarityValue = ArrayUtil.sum(endProfile);
    newPolarityValue = newPolarityValue / endProfile.length;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      // store and post the polarity signal - Component level
      ComponentMeasurement polaritySignalMeasurement = new ComponentMeasurement(this, subtype,
                                                                                MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL,
                                                                                MeasurementUnitsEnum.NONE, componentInspectionData.getComponent(),
                                                                                sliceNameEnum, newPolarityValue);
      componentInspectionData.getComponentInspectionResult().addMeasurement(polaritySignalMeasurement);
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      componentInspectionData,
                                                      polaritySignalMeasurement);
    }
    else
    {
      // store and post the polarity signal - Joint level
      JointMeasurement polaritySignalMeasurement = new JointMeasurement(this, 
                                                                        MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL,
                                                                        MeasurementUnitsEnum.NONE, padOneJointInspectionData.getPad(),
                                                                        sliceNameEnum, newPolarityValue);
      padOneJointInspectionData.getJointInspectionResult().addMeasurement(polaritySignalMeasurement);
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      polaritySignalMeasurement);
    }
    
    // post component thickness profile (according to horizontal/ vertical orientation)
    if (componentRegionOfInterest.getOrientationInDegrees()== 90 || componentRegionOfInterest.getOrientationInDegrees()== 270)
    {
      postClearPolarizedCapSlugProfileDiagnostics(componentThicknessProfile, componentProfile.length - Math.round(padOneSlugEdgeIndex),
                                                 componentProfile.length - Math.round(padTwoSlugEdgeIndex), reconstructionRegion,
                                                 sliceNameEnum, padOneJointInspectionData);
    } 
    else if (componentRegionOfInterest.getOrientationInDegrees()== 0 || componentRegionOfInterest.getOrientationInDegrees()== 180)
    {
      postClearPolarizedCapSlugProfileDiagnostics(componentThicknessProfile, Math.round(padTwoSlugEdgeIndex),
                                                 Math.round(padOneSlugEdgeIndex), reconstructionRegion,
                                                 sliceNameEnum, padOneJointInspectionData);
    }  
    
    // post lhs and rhs gray value profile (according to pad 1/ pad 2 position in image)
    if (componentRegionOfInterest.getOrientationInDegrees()== 0 || componentRegionOfInterest.getOrientationInDegrees()== 270)
    {
      postClearPolarizedCapSlugRLHSProfileDiagnostics(componentProfileRHS, rhsROI, reconstructionRegion,
                                                     sliceNameEnum, padTwoJointInspectionData, 1);  
      postClearPolarizedCapSlugRLHSProfileDiagnostics(componentProfileLHS, lhsROI, reconstructionRegion,
                                                     sliceNameEnum, padOneJointInspectionData, 2);
    } 
    else if (componentRegionOfInterest.getOrientationInDegrees()== 90 || componentRegionOfInterest.getOrientationInDegrees()== 180)
    {
      postClearPolarizedCapSlugRLHSProfileDiagnostics(componentProfileLHS, lhsROI, reconstructionRegion,
                                                     sliceNameEnum, padOneJointInspectionData, 2);
      postClearPolarizedCapSlugRLHSProfileDiagnostics(componentProfileRHS, rhsROI, reconstructionRegion,
                                                     sliceNameEnum, padTwoJointInspectionData, 1);
    }   
  }
  
  /**
   * The asymmetry is measured in the slug slice profile between the two fillets.  It is the average difference
   * in thickness between the left and right sides of that region (reflected about the center . . . so it is a measure
   * of center symmetry).
   *
   * The thickness is defined as the average thickness in that same region.
   *
   * @author Peter Esbensen
   */
  private void measureAsymmetryAndSlugThickness(ComponentInspectionData componentInspectionData,
                                                RegionOfInterest componentRegionOfInterest,
                                                ReconstructedImages reconstructedImages,
                                                int filletOneEdgeIndex,
                                                int filletTwoEdgeIndex,
                                                Subtype subtype) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(filletOneEdgeIndex >= 0);
    Assert.expect(filletTwoEdgeIndex >= 0);
    Assert.expect(filletOneEdgeIndex >= filletTwoEdgeIndex);
    Assert.expect(subtype != null);
    Assert.expect(subtype.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP));

    SliceNameEnum sliceNameEnum = SliceNameEnum.PCAP_SLUG;
    ReconstructedSlice slugReconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();

    AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_PCAP_MEASUREMENT_STARTING_TO_MEASURE_POLARITY_KEY", null),
        reconstructionRegion, sliceNameEnum, componentInspectionData.getPadOneJointInspectionData(), this);
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    float[] componentThicknessProfile = getComponentThicknessProfile(slugReconstructedSlice,
          reconstructionRegion, componentRegionOfInterest, padOneJointInspectionData);

    int distanceBetweenFillets = filletOneEdgeIndex - filletTwoEdgeIndex;
    int halfDistance = distanceBetweenFillets / 2;
    float asymmetryScore = 0.0f;
    float thicknessSum = 0f;
    boolean isAsymmetryMethod = PolarizedCapMisalignmentAlgorithm.isAsymmetryMethod(subtype);
    for (int i = 0; i <= halfDistance; ++i)
    {
      int leftSideBin = filletTwoEdgeIndex + i;
      int rightSideBin = filletOneEdgeIndex - i;
      float leftSideBinThickness = componentThicknessProfile[ leftSideBin ];
      float rightSideBinThickness = componentThicknessProfile[ rightSideBin ];
      if (isAsymmetryMethod)
        asymmetryScore += ( leftSideBinThickness - rightSideBinThickness );
      thicknessSum += ( leftSideBinThickness + rightSideBinThickness );
    }
    asymmetryScore /= halfDistance;
    float averageThicknessInMils = thicknessSum / (halfDistance * 2);
    float averageThicknessInMillimeters = MathUtil.convertMilsToMillimeters(averageThicknessInMils);

    if (isAsymmetryMethod)
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        // store and post the polarity signal - Component level
        ComponentMeasurement polaritySignalMeasurement = new ComponentMeasurement(this, subtype,
                                                                                  MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL,
                                                                                  MeasurementUnitsEnum.NONE, componentInspectionData.getComponent(),
                                                                                  sliceNameEnum, asymmetryScore);
        componentInspectionData.getComponentInspectionResult().addMeasurement(polaritySignalMeasurement);
        AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                        reconstructionRegion,
                                                        componentInspectionData,
                                                        polaritySignalMeasurement);
      }
      else
      {
        // store and post the polarity signal - Joint level
        JointMeasurement polaritySignalMeasurement = new JointMeasurement(this,
                                                                          MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL,
                                                                          MeasurementUnitsEnum.NONE, padOneJointInspectionData.getPad(),
                                                                          sliceNameEnum, asymmetryScore);
        padOneJointInspectionData.getJointInspectionResult().addMeasurement(polaritySignalMeasurement);
        AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                        reconstructionRegion,
                                                        padOneJointInspectionData,
                                                        polaritySignalMeasurement);
      }
    }
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      // store and post the slug thickness - Component level
      ComponentMeasurement slugThicknessInMillimetersMeasurement = new ComponentMeasurement(this, subtype,
                                                                                            MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS,
                                                                                            MeasurementUnitsEnum.MILLIMETERS, componentInspectionData.getComponent(),
                                                                                            sliceNameEnum, averageThicknessInMillimeters);
      componentInspectionData.getComponentInspectionResult().addMeasurement(slugThicknessInMillimetersMeasurement);
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      componentInspectionData,
                                                      slugThicknessInMillimetersMeasurement);
    }
    else
    {
      // store and post the slug thickness - Joint level
      JointMeasurement jointSlugThicknessInMillimetersMeasurement = new JointMeasurement(this,
                                                                                            MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS,
                                                                                            MeasurementUnitsEnum.MILLIMETERS, padOneJointInspectionData.getPad(),
                                                                                            sliceNameEnum, averageThicknessInMillimeters);
      padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointSlugThicknessInMillimetersMeasurement);
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      jointSlugThicknessInMillimetersMeasurement);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private float[] getComponentProfile(ReconstructionRegion reconstructionRegion,
                                      ReconstructedSlice reconstructedSlice,
                                      RegionOfInterest componentRegionOfInterest)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);

    Image image = reconstructedSlice.getOrthogonalImage();
    RegionOfInterest componentRegionTruncatedToImage = new RegionOfInterest(componentRegionOfInterest);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, componentRegionTruncatedToImage);

    float[] componentProfile = ImageFeatureExtraction.profile(image, componentRegionTruncatedToImage);

    return componentProfile;
  }

  /**
   * @author Peter Esbensen
   */
  private float[] getBackgroundProfile(ReconstructionRegion reconstructionRegion,
                                       ReconstructedSlice reconstructedSlice,
                                       RegionOfInterest componentRegionOfInterest,
                                       JointInspectionData padOneJointInspectionData)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(padOneJointInspectionData != null);

    final int BACKGROUND_REGION_WIDTH = 3;

    Image image = reconstructedSlice.getOrthogonalImage();

    int interPadDistanceInPixels = padOneJointInspectionData.getInterPadDistanceInPixels();

    RegionOfInterest leftSideBackgroundRegion = new RegionOfInterest(componentRegionOfInterest);
    RegionOfInterest rightSideBackgroundRegion = new RegionOfInterest(componentRegionOfInterest);

    leftSideBackgroundRegion.setLengthAcross(BACKGROUND_REGION_WIDTH);
    rightSideBackgroundRegion.setLengthAcross(BACKGROUND_REGION_WIDTH);

    int backgroundShift = Math.round((componentRegionOfInterest.getLengthAcross() * 0.5f) +
                                     (interPadDistanceInPixels * 0.5f));
    leftSideBackgroundRegion.translateAlongAcross(0, -1 * backgroundShift);
    rightSideBackgroundRegion.translateAlongAcross(0, backgroundShift);

    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, leftSideBackgroundRegion);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, rightSideBackgroundRegion);

    float[] leftSideBackgroundProfile = ImageFeatureExtraction.profile(image, leftSideBackgroundRegion);
    float[] rightSideBackgroundProfile = ImageFeatureExtraction.profile(image, rightSideBackgroundRegion);

    float[] backgroundProfile = ProfileUtil.addProfiles(leftSideBackgroundProfile, rightSideBackgroundProfile);
    ProfileUtil.multiplyProfileByConstant(backgroundProfile, 0.5f);

    return backgroundProfile;
  }

  /**
   * @author Peter Esbensen plagiarizing Sunit Bhalla
   */
  private void convertInspectionMeasurementsToLearningLists(ComponentInspectionData componentInspectionData,
                                                            List<Float> padOneThicknessInMillimetersMeasurements,
                                                            List<Float> padTwoThicknessInMillimetersMeasurements)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(padOneThicknessInMillimetersMeasurements != null);
    Assert.expect(padTwoThicknessInMillimetersMeasurements != null);


    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    SliceNameEnum sliceNameEnum = SliceNameEnum.PAD;

    JointMeasurement padOneJointMeasurement = getPadOneUpperFilletThicknessInMillimeters(padOneJointInspectionData, sliceNameEnum);
    JointMeasurement padTwoJointMeasurement = getPadTwoUpperFilletThicknessInMillimeters(padTwoJointInspectionData, sliceNameEnum);

    padOneThicknessInMillimetersMeasurements.add(padOneJointMeasurement.getValue());
    padTwoThicknessInMillimetersMeasurements.add(padTwoJointMeasurement.getValue());
  }

  /**
   * @author Peter Esbensen
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    if (typicalBoardImages.size() == 0)
      return;

    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    ReconstructedImages reconstructedImages;
    List<Float> padOneThicknessInMillimetersMeasurements = new LinkedList<Float>();
    List<Float> padTwoThicknessInMillimetersMeasurements = new LinkedList<Float>();

    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      classifyJoints(reconstructedImages, jointInspectionDataList);

      Collection<ComponentInspectionData> componentInspectionDataList = AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList); 
      for (ComponentInspectionData componentInspectionData : componentInspectionDataList)
      {
        convertInspectionMeasurementsToLearningLists(componentInspectionData,
                                                     padOneThicknessInMillimetersMeasurements,
                                                     padTwoThicknessInMillimetersMeasurements);
        componentInspectionData.getComponentInspectionResult().clearMeasurements();
        componentInspectionData.clearComponentInspectionResult();
      }
      for (JointInspectionData jointInspectionData : jointInspectionDataList)
      {
        jointInspectionData.getJointInspectionResult().clearMeasurements();
        jointInspectionData.clearJointInspectionResult();
      }

      imagesIterator.finishedWithCurrentRegion();
      
      // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
      if (componentInspectionDataList != null)
      {
        componentInspectionDataList.clear();
        componentInspectionDataList = null;
      }

      if (jointInspectionDataList != null)
      {
        jointInspectionDataList.clear();
        jointInspectionDataList = null;
      }
    }

    if (padOneThicknessInMillimetersMeasurements.size() > 0)
    {
      float expectedPadOneThicknessInMillimeters = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(padOneThicknessInMillimetersMeasurements));
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS, expectedPadOneThicknessInMillimeters);
    }
    if (padTwoThicknessInMillimetersMeasurements.size() > 0)
    {
      float expectedPadTwoThicknessMillimeters = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(padTwoThicknessInMillimetersMeasurements));
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS, expectedPadTwoThicknessMillimeters);
    }

    // Added by Seng Yew on 22-Apr-2011
    // Need to be careful when call this. Have to make sure JointMeasurement & ComponentMeasurement instances created before this reset are not used anywhere.
    // JointInspectionResult::addMeasurement(...) & ComponentInspectionResult::addMeasurement(...) checks for duplicate _id when add.
    // After call this function, any new JointMeasurement & ComponentMeasurement instances created will start with zero again, and cause duplicate _id and cause crashes.
    subtype.getPanel().getProject().getTestProgram().startNewInspectionRun(); // make sure we clear out the measurements for the next run (WRONG USAGE PREVIOUSLY)
  }

  /**
   * @author Peter Esbensen
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    if (typicalBoardImages.size() == 0)
      return;

    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);

    // get all the profiles and put them into this map thing
    Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> typicalPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap =
        getPadOneJointInspectionDataToComponentProfileMap(subtype, typicalBoardImages);

    Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap =
        getPadOneJointInspectionDataToComponentProfileMap(subtype, unloadedBoardImages);

    // figure out best Locator settings
//    learnLocatorSettings(subtype, typicalPadOneJointInspectionDataAndLearnedDataSet);

    // figure out best background locations
    float bestBackgroundLocation = AlgorithmUtil.learnChipBackgroundRegionLocations(subtype, typicalBoardImages, this);
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because too complicated 
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT,
                            bestBackgroundLocation);

    // learn the nominal pad thicknesses
    boolean populatedBoard = true;

    // learn fillet edges
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier inside
    learnFilletEdges(typicalPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
                     unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
                     subtype);

    // learn the best offset for the lower fillet region
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because not so appplicable
    learnFilletOffsets(typicalPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
                                    unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
                                    subtype);

    // learn the upper and lower fillet thickness so the open signals can be learned by the open algorithm
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Partially exclude inside
    learnUpperAndLowerFilletThicknesses(typicalPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap, subtype,
                                        populatedBoard);
  }

  /**
   * @author Peter Esbensen
   */
  private void learnUpperAndLowerFilletThicknesses(
      Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
      Subtype subtype,
      boolean populatedBoard) throws DatastoreException
  {
    Assert.expect(padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap != null);
    Assert.expect(subtype != null);

    SliceNameEnum sliceNameEnum = null;

    Iterator<Map.Entry<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData>> mapIt = padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap.entrySet().iterator();

    while (mapIt.hasNext())
    {
      Map.Entry<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> mapEntry = mapIt.next();

      JointInspectionData padOneJointInspectionData = mapEntry.getKey().getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
          getPadTwoJointInspectionDataOnTwoPinDevices();

      float[] componentBodyProfile = null;
      PolarizedCapMeasurementLearnedData chipMeasurementLearnedData = mapEntry.getValue();

      sliceNameEnum = SliceNameEnum.PAD;
      componentBodyProfile = chipMeasurementLearnedData.getComponentBodyThicknessProfile(sliceNameEnum);

      FloatRef padOneFilletEdgeIndexRef = new FloatRef();
      FloatRef padTwoFilletEdgeIndexRef = new FloatRef();

      locateEdges(padOneJointInspectionData, padTwoJointInspectionData,
                  componentBodyProfile, padOneFilletEdgeIndexRef, padTwoFilletEdgeIndexRef);

      FloatRef filletOneThicknessInMilsRef = new FloatRef();
      FloatRef filletTwoThicknessInMilsRef = new FloatRef();
      FloatRef filletOneLowerRegionThicknessInMilsRef = new FloatRef();
      FloatRef filletTwoLowerRegionThicknessInMilsRef = new FloatRef();
      IntegerRef padOneUpperFilletIndexRef = new IntegerRef();
      IntegerRef padOneLowerFilletIndexRef = new IntegerRef();
      IntegerRef padTwoUpperFilletIndexRef = new IntegerRef();
      IntegerRef padTwoLowerFilletIndexRef = new IntegerRef();

      float upperFilletOffsetAsFractionOfPadLength = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET) * 0.01f;
      float lowerFilletOffsetAsFractionOfPadLength = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET) * 0.01f;
      float openSignalSearchLengthAsFractionOfPadLength = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH) * 0.01f;

      getFilletThicknesses(componentBodyProfile,
                                       padOneJointInspectionData,
                                       padTwoJointInspectionData,
                                       padOneFilletEdgeIndexRef.getValue(),
                                       padTwoFilletEdgeIndexRef.getValue(),
                                       filletOneThicknessInMilsRef,
                                       filletTwoThicknessInMilsRef,
                                       filletOneLowerRegionThicknessInMilsRef,
                                       filletTwoLowerRegionThicknessInMilsRef,
                                       padOneUpperFilletIndexRef,
                                       padOneLowerFilletIndexRef,
                                       padTwoUpperFilletIndexRef,
                                       padTwoLowerFilletIndexRef,
                                       upperFilletOffsetAsFractionOfPadLength,
                                       lowerFilletOffsetAsFractionOfPadLength,
                                       openSignalSearchLengthAsFractionOfPadLength);

      Pad padOne = padOneJointInspectionData.getPad();
      Pad padTwo = padTwoJointInspectionData.getPad();
      boolean goodJoint = true;
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because too complicated, unless exclude componentBodyProfile
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOne,
          sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_FILLET_ONE_THICKNESS,
          filletOneThicknessInMilsRef.getValue(), populatedBoard, goodJoint));
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOne,
          sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_PAD_ONE_LOWER_FILLET_THICKNESS,
          filletOneLowerRegionThicknessInMilsRef.getValue(), populatedBoard, goodJoint));
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padTwo,
          sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_FILLET_TWO_THICKNESS,
          filletTwoThicknessInMilsRef.getValue(), populatedBoard, goodJoint));
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padTwo,
          sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_PAD_TWO_LOWER_FILLET_THICKNESS,
          filletTwoLowerRegionThicknessInMilsRef.getValue(), populatedBoard, goodJoint));
    }
    // get back all measurements from database and set nominals
    // pad one
    float[] upperFilletThicknessesInMils = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
        MeasurementEnum.PCAP_MEASUREMENT_FILLET_ONE_THICKNESS,
        true, // populatedBoard,
        true); //goodJoint);

    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    float expectedPadThicknessInMillimeters = MathUtil.convertMilsToMillimeters(
      AlgorithmUtil.medianWithExcludeOutlierDecision(upperFilletThicknessesInMils, false));
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS,
        expectedPadThicknessInMillimeters);

    // pad two
    upperFilletThicknessesInMils = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
        MeasurementEnum.PCAP_MEASUREMENT_FILLET_TWO_THICKNESS,
        true, // populatedBoard,
        true); //goodJoint);

    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    expectedPadThicknessInMillimeters = MathUtil.convertMilsToMillimeters(
      AlgorithmUtil.medianWithExcludeOutlierDecision(upperFilletThicknessesInMils, false));
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS,
        expectedPadThicknessInMillimeters);
  }

  /**
   * @author Peter Esbensen
   */
  private void locateEdgesAndGetOpenSignals(JointInspectionData padOneJointInspectionData,
      JointInspectionData padTwoJointInspectionData,
      float[] componentBodyThicknessProfile,
      float upperFilletOffsetAsFractionOfPad,
      float lowerFilletOffsetAsFractionOfPad,
      List<Float> openSignalList)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentBodyThicknessProfile != null);

    FloatRef padOneFilletEdgeIndex = new FloatRef();
    FloatRef padTwoFilletEdgeIndex = new FloatRef();

    locateEdges(padOneJointInspectionData, padTwoJointInspectionData, componentBodyThicknessProfile,
                padOneFilletEdgeIndex, padTwoFilletEdgeIndex);

    FloatRef filletOneThicknessInMilsRef = new FloatRef();
    FloatRef filletTwoThicknessInMilsRef = new FloatRef();
    FloatRef filletOneLowerRegionThicknessInMilsRef = new FloatRef();
    FloatRef filletTwoLowerRegionThicknessInMilsRef = new FloatRef();
    IntegerRef padOneUpperFilletIndexRef = new IntegerRef();
    IntegerRef padOneLowerFilletIndexRef = new IntegerRef();
    IntegerRef padTwoUpperFilletIndexRef = new IntegerRef();
    IntegerRef padTwoLowerFilletIndexRef = new IntegerRef();

    getFilletThicknesses(componentBodyThicknessProfile, padOneJointInspectionData,
                                          padTwoJointInspectionData, padOneFilletEdgeIndex.getValue(),
                                          padTwoFilletEdgeIndex.getValue(),
                                          filletOneThicknessInMilsRef, filletTwoThicknessInMilsRef,
                                          filletOneLowerRegionThicknessInMilsRef,
                                          filletTwoLowerRegionThicknessInMilsRef, padOneUpperFilletIndexRef,
                                          padOneLowerFilletIndexRef, padTwoUpperFilletIndexRef,
                                          padTwoLowerFilletIndexRef, upperFilletOffsetAsFractionOfPad, lowerFilletOffsetAsFractionOfPad, 0.0f);

    float padOneOpenSignal = filletOneThicknessInMilsRef.getValue() - filletOneLowerRegionThicknessInMilsRef.getValue();
    float padTwoOpenSignal = filletTwoThicknessInMilsRef.getValue() - filletTwoLowerRegionThicknessInMilsRef.getValue();

    openSignalList.add(padOneOpenSignal);
    openSignalList.add(padTwoOpenSignal);

  }

  /**
   * Estimate the open signal separation between typical joints and bad joints.  If the user has provided an unloaded
   * panel then use that for the bad joint open signals.  Otherwise, simulate them.
   *
   * @author Peter Esbensen
   */
  private float estimateOpenSignalSeparation(
      Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
      Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
      Subtype subtype, float upperFilletOffsetAsFractionOfPad, float lowerFilletOffsetAsFractionOfPad)
  {
    Assert.expect(padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap != null);
    Assert.expect(unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap != null);
    Assert.expect(subtype != null);

    List<Float> typicalOpenSignals = new LinkedList<Float>();
    for (Map.Entry<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> jointInspectionDataAndLearnedData : padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap.entrySet())
    {
      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getKey().getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().getPadTwoJointInspectionDataOnTwoPinDevices();
      float[] componentBodyThicknessProfile = jointInspectionDataAndLearnedData.getValue().getComponentBodyThicknessProfile(SliceNameEnum.PAD);

      locateEdgesAndGetOpenSignals(padOneJointInspectionData, padTwoJointInspectionData,
                                   componentBodyThicknessProfile, upperFilletOffsetAsFractionOfPad,
                                   lowerFilletOffsetAsFractionOfPad, typicalOpenSignals);
    }

    List<Float> unloadedOpenSignals = new LinkedList<Float>();

    if (unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap.size() > 1)
    {
      for (Map.Entry<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> jointInspectionDataAndLearnedData :
           unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap.entrySet())
      {
        JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getKey().getFirst();
        JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
            getPadTwoJointInspectionDataOnTwoPinDevices();
        float[] componentBodyThicknessProfile = jointInspectionDataAndLearnedData.getValue().
                                                getComponentBodyThicknessProfile(SliceNameEnum.PAD);

        locateEdgesAndGetOpenSignals(padOneJointInspectionData, padTwoJointInspectionData,
                                     componentBodyThicknessProfile, upperFilletOffsetAsFractionOfPad,
                                     lowerFilletOffsetAsFractionOfPad, unloadedOpenSignals);
      }
    }
    else // create fake open joints
    {
      for (Map.Entry<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> jointInspectionDataAndLearnedData :
           padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap.entrySet())
      {
        JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getKey().getFirst();
        JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
            getPadTwoJointInspectionDataOnTwoPinDevices();
        float[] componentBodyThicknessProfile = createFakeMissingComponentProfileInMils(padOneJointInspectionData,
            jointInspectionDataAndLearnedData.getValue().getComponentBodyThicknessProfile(SliceNameEnum.PAD));

        locateEdgesAndGetOpenSignals(padOneJointInspectionData, padTwoJointInspectionData,
                                     componentBodyThicknessProfile, upperFilletOffsetAsFractionOfPad,
                                     lowerFilletOffsetAsFractionOfPad, unloadedOpenSignals);
      }
    }

    float[] typicalOpenSignalsArray = ArrayUtil.convertFloatListToFloatArray(typicalOpenSignals);
    float[] unloadedOpenSignalsArray = ArrayUtil.convertFloatListToFloatArray(unloadedOpenSignals);

    float[] quartileRangesForTypicalOpenSignals = StatisticsUtil.quartileRanges(typicalOpenSignalsArray);
    float[] quartileRangesForUnloadedJointOpenSignals = StatisticsUtil.quartileRanges(unloadedOpenSignalsArray);
    float expectedTypicalOpenSignal = quartileRangesForTypicalOpenSignals[2];
    float spreadOfTypicalOpenSignal = quartileRangesForTypicalOpenSignals[3] - quartileRangesForTypicalOpenSignals[1];
    float expectedUnloadedJointOpenSignal = quartileRangesForUnloadedJointOpenSignals[2];

    float spreadOfUnloadedJointOpenSignal = quartileRangesForUnloadedJointOpenSignals[3] - quartileRangesForUnloadedJointOpenSignals[1];
    if (unloadedOpenSignalsArray.length < 10)
      spreadOfUnloadedJointOpenSignal = 0.0f; // not enough data, we'll just use zero spread

    float separation = (expectedTypicalOpenSignal - expectedUnloadedJointOpenSignal) /
                       (spreadOfTypicalOpenSignal + spreadOfUnloadedJointOpenSignal);

    // this can be used for debugging the search for the best parameters
    //    float tmp = separation;
    //    if (filletLocationsAreValid.getValue() == false)
    //      tmp = 0.0f;
    //    System.out.println("location offset good expected spread bad expected spread separation " + locationAsFractionOfPad + " " + offsetAsFractionOfPad
    //                       + " " + expectedTypicalOpenSignal + " " + spreadOfTypicalOpenSignal + " " + expectedUnloadedJointOpenSignal +
    //                       " " + spreadOfUnloadedJointOpenSignal + " " + separation);

    return separation;

  }

  /**
   * Create an estimate of what a profile of an unloaded part would look like.
   *
   * @author Peter Esbensen
   */
  private float[] createFakeMissingComponentProfileInMils(JointInspectionData padOneJointInspectionData,
                                                          float[] goodJointProfile)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padOneJointInspectionData != null);

    float[] fakeMissingComponentProfile = new float[ goodJointProfile.length ];

    int padOneLengthInPixels = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                               getLengthAlong();

    // get pad one profile
    float[] padOneProfile = new float[ padOneLengthInPixels ];
    for (int i = 0; i < padOneLengthInPixels; ++i)
    {
      padOneProfile[i] = goodJointProfile[ goodJointProfile.length - 1 - padOneLengthInPixels + i ];
    }
    float[] fakePadOneProfile = AlgorithmUtil.createFakeOpenJointProfileInMils(padOneProfile, padOneJointInspectionData.getSubtype());

    // use the fake pad one to estimate pad two (the normal pad two estimate is bad because the slug messes up our solder volume estimate)
    for (int i = 0; i < padOneLengthInPixels; ++i)
    {
      fakeMissingComponentProfile[i] = fakePadOneProfile[i];
    }

    // now just copy in the pad one estimate
    for (int i = 0; i < padOneLengthInPixels; ++i)
    {
      fakeMissingComponentProfile[goodJointProfile.length - 1 - padOneLengthInPixels + i] = fakePadOneProfile[i];
    }

    return fakeMissingComponentProfile;
  }

  /**
   * Find the open signal location and offset that gives the best separation between
   * good and bad.
   *
   * @author Peter Esbensen
   */
  private void learnFilletEdges(
      Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData>
      padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
      Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData>
      unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
      Subtype subtype)
  {
    Assert.expect(padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap != null);
    Assert.expect(unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap != null);
    Assert.expect(subtype != null);

    float edgeFractionArray[] = new float[padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap.size()];
    int i = 0;

    // consider the steepest downward slope in the pad one area to be our pad fillet
    for (Map.Entry<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> jointInspectionDataAndLearnedData : padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap.entrySet())
    {
      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getKey().getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().getPadTwoJointInspectionDataOnTwoPinDevices();
      int padOneLengthInPixels = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                               getLengthAlong();
      float[] componentBodyThicknessProfile = jointInspectionDataAndLearnedData.getValue().getComponentBodyThicknessProfile(SliceNameEnum.PAD);
      float[] componentDerivativeProfile = jointInspectionDataAndLearnedData.getValue().getComponentDerivativeProfile(SliceNameEnum.PAD);

      float padOneMedianThicknessInMiils = getPadOneMedianThicknessInMils(componentBodyThicknessProfile, padOneJointInspectionData);
      float padTwoMedianThicknessInMils = getPadTwoMedianThicknessInMils(componentBodyThicknessProfile, padTwoJointInspectionData);
      float minimumOfTheTwoMedianThicknessesInMils = Math.min(padOneMedianThicknessInMiils, padTwoMedianThicknessInMils);

      int sharpestDownwardSlopeIndex = ArrayUtil.minIndex(componentDerivativeProfile,
                                                          componentDerivativeProfile.length - 1 - padOneLengthInPixels,
                                                          componentDerivativeProfile.length);

      edgeFractionArray[i++] = componentBodyThicknessProfile[ sharpestDownwardSlopeIndex ] /
                               (2f * minimumOfTheTwoMedianThicknessesInMils);
    }
    final float PERCENTILE_FOR_LEARNING_EDGE = 0.25f;
    float bestEdgeFraction = AlgorithmUtil.percentileWithExcludeOutlierDecision(edgeFractionArray, PERCENTILE_FOR_LEARNING_EDGE, false);

    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_FILLET_EDGE_PERCENT,
                            100.f * bestEdgeFraction);
  }

  /**
   * @author Peter Esbensen
   */
  private float getPadOneMedianThicknessInMils(float[] componentThicknessProfileInMils,
                                               JointInspectionData padOneJointInspectionData)
  {
    Assert.expect(componentThicknessProfileInMils != null);
    Assert.expect(padOneJointInspectionData != null);

    int endIndex = componentThicknessProfileInMils.length;
    int padOneLength = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getLengthAlong();
    int firstIndex = componentThicknessProfileInMils.length - Math.max(1, padOneLength);

    float medianThickness = StatisticsUtil.median(componentThicknessProfileInMils, firstIndex, endIndex);
    return medianThickness;
  }

  /**
   * @author Peter Esbensen
   */
  private float getPadTwoMedianThicknessInMils(float[] componentThicknessProfileInMils,
                                               JointInspectionData padTwoJointInspectionData)
  {
    Assert.expect(componentThicknessProfileInMils != null);
    Assert.expect(padTwoJointInspectionData != null);

    int padTwoLength = padTwoJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getLengthAlong();
    int endIndex = padTwoLength;
    int firstIndex = 0;

    float medianThickness = StatisticsUtil.median(componentThicknessProfileInMils, firstIndex, endIndex);
    return medianThickness;
  }

  /**
   * Find the open signal location and offset that gives the best separation between
   * good and bad.  Basically, we do a brute force search to see which combination of upper and lower fillet offsets
   * give the best separation.  Note that, if the user has specified a search distance, that is used as well.
   *
   * @author Peter Esbensen
   */
  private void learnFilletOffsets(
      Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
      Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
      Subtype subtype)
  {
    Assert.expect(padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap != null);
    Assert.expect(unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap != null);
    Assert.expect(subtype != null);

    float bestOpenSignalSeparation = -99999f;
    float bestUpperFilletOffsetAsFractionOfPad = 0.0f;
    float bestLowerFilletOffsetAsFractionOfPad = 0.0f;

    final float minUpperFilletOffsetAsFractionOfPad = 0.0f;  // we know this shouldn't be to the outside (negative) of the fillet edge
    final float maxUpperFilletOffsetAsFractionOfPad = 0.5f;
    final float upperFilletSearchIncrementAsFractionOfPad = 0.025f;
    final float minLowerFilletOffsetAsFractionOfPad = 0.0f;  // again, this should be outside of the fillet edge
    final float maxLowerOffsetAsFractionOfPad = 1.0f;
    final float lowerFilletOffsetSearchIncrementAsFractionOfPad = 0.025f;

    // iterate over the various offsets to see which give us the best separation
    for (float upperFilletOffsetAsFractionOfPad = minUpperFilletOffsetAsFractionOfPad;
         upperFilletOffsetAsFractionOfPad < maxUpperFilletOffsetAsFractionOfPad;
         upperFilletOffsetAsFractionOfPad += upperFilletSearchIncrementAsFractionOfPad)
    {
      for (float lowerFilletOffsetAsFractionOfPad = minLowerFilletOffsetAsFractionOfPad;
           lowerFilletOffsetAsFractionOfPad < maxLowerOffsetAsFractionOfPad;
           lowerFilletOffsetAsFractionOfPad += lowerFilletOffsetSearchIncrementAsFractionOfPad)
      {
        if (MathUtil.fuzzyEquals(lowerFilletOffsetAsFractionOfPad, upperFilletOffsetAsFractionOfPad, 0.01) == false) // if they are in the same place it's a meaningless open signal, so don't bother with it
        {
          if (lowerFilletOffsetAsFractionOfPad > upperFilletOffsetAsFractionOfPad) // Kathy has requested that the lower fillet always be placed to the inside of the upper fillet for ease-of-training, even if that doesn't give you the best separation(!)

          {
            float estimatedOpenSignalSeparation = estimateOpenSignalSeparation(padOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
                                                                               unloadedPadOneJointInspectionDataToPolarizedCapMeasurementLearnedDataMap,
                                                                               subtype,
                                                                               upperFilletOffsetAsFractionOfPad,
                                                                               lowerFilletOffsetAsFractionOfPad);

            if (estimatedOpenSignalSeparation > bestOpenSignalSeparation)
            {
              bestOpenSignalSeparation = estimatedOpenSignalSeparation;
              bestUpperFilletOffsetAsFractionOfPad = upperFilletOffsetAsFractionOfPad;
              bestLowerFilletOffsetAsFractionOfPad = lowerFilletOffsetAsFractionOfPad;
            }
          }
        }
      }
    }

    // set the algo settings that were the winners
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_LOWER_FILLET_OFFSET,
                            100.f * bestLowerFilletOffsetAsFractionOfPad);

    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PCAP_MEASUREMENT_UPPER_FILLET_OFFSET,
        100.0f * bestUpperFilletOffsetAsFractionOfPad);
  }

  /**
   * For pcaps, we can't make the simple assumption that there will be a single
   * peak for pad two or that the steepest slope over pad two will be the fillet
   * edge because the slug often appears over the pad as well.  The slug confuses
   * the appearance of pad two.
   * So we'll do a hybrid approach, where we assume the steepest slope on pad ONE
   * indicates the fillet edge (since it should be "uncorrupted" by the slug.
   * Then, to find the pad two fillet edge, we will search inwards from the outside
   * for a fillet thickness that is most similar to what was found for pad two.
   *
   * @author Peter Esbensen
   */
  float estimatePolarizedCapacitorBodyLength(JointInspectionData padOneJointInspectionData,
                                             JointInspectionData padTwoJointInspectionData,
                                             float[] componentBodyProfile,
                                             float[] componentDerivativeProfile)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentBodyProfile != null);
    Assert.expect(componentDerivativeProfile != null);

    // find the pad one peak
    int padOneLengthInPixels = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                               getLengthAlong();
    int padOneLeftSideIndex = componentBodyProfile.length - padOneLengthInPixels;
    int padOneRightSideIndex = componentBodyProfile.length;
    float indexOfPadOneFilletPeak = ArrayUtil.maxIndex(componentBodyProfile,
        padOneLeftSideIndex,
        padOneRightSideIndex);

    // find the pad one fillet edge - this is the steepest falling slope to the right of the pad one peak
    int padOneFilletPeakIndex = Math.round(indexOfPadOneFilletPeak);
    float indexOfPadOneFilletEdge = ArrayUtil.minIndex(componentDerivativeProfile,
        padOneFilletPeakIndex,
        componentDerivativeProfile.length);
    float thicknessAtPadOneFilletEdge = componentBodyProfile[Math.round(indexOfPadOneFilletEdge)];

    // find the pad two fillet edge - this the leftmost point on the profile that has the same thickness as the pad one fillet edge
    float indexOfPadTwoFilletEdge = ProfileUtil.findSubpixelEdgeLocationBasedOnExceedingThreshold(
      componentBodyProfile, thicknessAtPadOneFilletEdge);
    int padTwoLengthInPixels = padTwoJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                               getLengthAlong();
    indexOfPadTwoFilletEdge = Math.min(indexOfPadTwoFilletEdge, padTwoLengthInPixels);

    // compute the body length
    Assert.expect(indexOfPadOneFilletEdge >= indexOfPadTwoFilletEdge);
    return (indexOfPadOneFilletEdge - indexOfPadTwoFilletEdge);
  }


  /**
   * Resistors have clearly defined inner edges on each pad.  So simply learn
   * the distance between those two sharp edges by assuming they are where
   * the slopes are steepest in those inside edge areas.
   *
   * @author Peter Esbensen
   */
  float estimateClearBodyLength(JointInspectionData padOneJointInspectionData,
                               float[] componentBodyProfile,
                               float[] componentDerivativeProfile)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(componentBodyProfile != null);
    Assert.expect(componentDerivativeProfile != null);

    // find the pad one peak - we will look to the left of this for the inside edge
    int padOneLengthInPixels = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                               getLengthAlong();
    int padOneLeftSideIndex = componentBodyProfile.length - padOneLengthInPixels;
    int padOneRightSideIndex = componentBodyProfile.length;
    float indexOfPadOneFilletPeak = ArrayUtil.maxIndex(componentBodyProfile,
        padOneLeftSideIndex,
        padOneRightSideIndex);

    // find the pad two peak - we will look to the right of this for the inside edge
    int padTwoLengthInPixels = padOneJointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().
                               getLengthAlong();
    int padTwoLeftSideIndex = 0;
    int padTwoRightSideIndex = padTwoLengthInPixels;
    float indexOfPadTwoFilletPeak = ArrayUtil.maxIndex(componentBodyProfile,
        padTwoLeftSideIndex,
        padTwoRightSideIndex);

    // find the pad one fillet edge - this is the steepest rising slope
    int maxSearchIndexOfPadOneFilletEdge = Math.min(componentBodyProfile.length, Math.round(indexOfPadOneFilletPeak));
    int minSearchIndexOfPadOneFilletEdge = padOneLeftSideIndex + _PROFILE_SMOOTHING_STEP_SIZE;
    float indexOfPadOneFilletEdge = ArrayUtil.maxIndex(componentDerivativeProfile,
        minSearchIndexOfPadOneFilletEdge, // start after the pad edge
        Math.max(minSearchIndexOfPadOneFilletEdge, maxSearchIndexOfPadOneFilletEdge));

    // find the pad two fillet edge - this is the steepest falling slope
    int padTwoFilletPeakIndex = Math.round(indexOfPadTwoFilletPeak);
    float indexOfPadTwoFilletEdge = ArrayUtil.minIndex(componentDerivativeProfile,
        padTwoFilletPeakIndex,
        Math.max(padTwoFilletPeakIndex, padTwoRightSideIndex - _PROFILE_SMOOTHING_STEP_SIZE)); // stop before we find the pad edge

    // compute the body length
    Assert.expect(indexOfPadOneFilletEdge >= indexOfPadTwoFilletEdge);
    return (indexOfPadOneFilletEdge - indexOfPadTwoFilletEdge);
  }

  /**
   * @author Peter Esbensen
   */
  float estimateOpaqueBodyLength(float[] componentDerivativeProfile)
  {
    Assert.expect(componentDerivativeProfile != null);

    // find the distance between the edges as defined by steepest slope
    float indexOfPadOneFilletEdge = ArrayUtil.minIndex(componentDerivativeProfile);
    float indexOfPadTwoFilletEdge = ArrayUtil.maxIndex(componentDerivativeProfile);
    if (indexOfPadTwoFilletEdge < indexOfPadOneFilletEdge)
    {
      return (indexOfPadOneFilletEdge - indexOfPadTwoFilletEdge);
    }
    else
      return -1.0f;
  }


  /**
   * @author Peter Esbensen
   */
  private Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> getPadOneJointInspectionDataToComponentProfileMap(
      Subtype subtype,
      ManagedOfflineImageSet typicalBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);


    Map<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData> padOneJointInspectionDataToComponentProfileMap =
        new HashMap<Pair<JointInspectionData, Integer>, PolarizedCapMeasurementLearnedData>();

    // iterate through all joints in images and collect profiles
    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    ReconstructedImages reconstructedImages;
    int myOwnImageIndex = 0;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList))
      {
        JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();

        PolarizedCapMeasurementLearnedData chipMeasurementLearnedData = new PolarizedCapMeasurementLearnedData();

        for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
        {
          SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

          if ((subtype.getJointTypeEnum().equals(JointTypeEnum.RESISTOR)) &&
              (sliceNameEnum.equals(SliceNameEnum.OPAQUE_CHIP_PAD) || sliceNameEnum.equals(SliceNameEnum.PCAP_SLUG)))
          {
            // ignore this slice, it doesn't apply to Resistors
          }
          else if ((subtype.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR)) &&
              sliceNameEnum.equals(SliceNameEnum.PCAP_SLUG))
          {
            // ignore this slice, it doesn't apply to Capacitors
          }
          else
          {
            // get the component region
            RegionOfInterest componentRegionOfInterest = componentInspectionData.getOrthogonalComponentRegionOfInterest();
            // run locator
            componentRegionOfInterest = runLocator(reconstructedImages, componentRegionOfInterest,
                                                   padOneJointInspectionData.getComponentInspectionData());

            // get a profile over the component
            float[] componentProfile = getComponentProfile(reconstructionRegion,
                reconstructedSlice,
                componentRegionOfInterest);

            // get an average of the two background profiles
            float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f * (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT);
            float[] backgroundProfile = AlgorithmUtil.getComponentBasedBackgroundProfile(reconstructionRegion, reconstructedSlice,
                componentRegionOfInterest, padOneJointInspectionData, backgroundRegionShiftAsFractionOfInterPadDistance, this, true);

            SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
            int backgroundSensitivityThreshold = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
            // compute component thickness profile
            float[] componentThicknessProfile = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMils(
                componentProfile,
                backgroundProfile,
                thicknessTable,
                backgroundSensitivityThreshold);

            float[] componentDerivativeProfile = ProfileUtil.createDerivativeProfile(componentThicknessProfile,
                _PROFILE_SMOOTHING_STEP_SIZE);

            chipMeasurementLearnedData.setComponentBodyThicknessProfile(sliceNameEnum, componentThicknessProfile);
            chipMeasurementLearnedData.setComponentDerivativeProfile(sliceNameEnum, componentDerivativeProfile);
          }
        }
        Assert.expect(padOneJointInspectionData != null);
        padOneJointInspectionDataToComponentProfileMap.put(new Pair<JointInspectionData, Integer>(padOneJointInspectionData, myOwnImageIndex), chipMeasurementLearnedData);
      }
      ++myOwnImageIndex;
      imagesIterator.finishedWithCurrentRegion();
    }
    return padOneJointInspectionDataToComponentProfileMap;
  }

  /**
   * @author Patrick Lacz
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Locator.setLearnedSettingsToDefaults(subtype);

    for (AlgorithmSettingEnum algorithmSettingEnum : getLearnedAlgorithmSettingEnums())
    {
      subtype.setSettingToDefaultValue(algorithmSettingEnum);
    }
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getPadOneLowerFilletThicknessInMillimeters(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_PAD_ONE_LOWER_FILLET_THICKNESS);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getPadOneUpperFilletThicknessInMillimeters(JointInspectionData jointInspectionData,
                                             SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_FILLET_ONE_THICKNESS);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getPadTwoLowerFilletThicknessInMillimeters(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_PAD_TWO_LOWER_FILLET_THICKNESS);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getPadTwoUpperFilletThicknessInMillimeters(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_FILLET_TWO_THICKNESS);
  }


  /**
   * @author Peter Esbensen
   */
  static ComponentMeasurement getPolaritySignalMeasurement(ComponentInspectionData componentInspectionData,
                                                           SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  static JointMeasurement getPolaritySignalMeasurement(JointInspectionData jointInspectionData,
                                                           SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_POLARITY_SIGNAL);
  }

  /**
   * @author Peter Esbensen
   */
  static ComponentMeasurement getSlugThicknessInMillimetersMeasurement(ComponentInspectionData componentInspectionData,
                                                          SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS);
  }

  /**
   * @author Cheah Lee Herng 
   */
  static JointMeasurement getSlugThicknessInMillimetersMeasurement(JointInspectionData jointInspectionData,
                                                          SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.PCAP_MEASUREMENT_SLUG_THICKNESS);
  }

  /**
   * Holds all the information that is learned for a given PolarizedCap component.
   * This is a nested class that only exists inside of PolarizedCapMeasurementAlgorithm.
   *
   * @author Peter Esbensen
   */
  class PolarizedCapMeasurementLearnedData
  {
    Map<SliceNameEnum, float[]> _sliceNameEnumToComponentBodyThicknessProfile =
        new HashMap<SliceNameEnum, float[]>();
    Map<SliceNameEnum, float[]> _sliceNameEnumToComponentDerivativeProfile =
        new HashMap<SliceNameEnum, float[]>();

    /**
     * @author Peter Esbensen
     */
    PolarizedCapMeasurementLearnedData()
    {
      // do nothing
    }

    /**
     * @author Peter Esbensen
     */
    void setComponentBodyThicknessProfile(SliceNameEnum sliceNameEnum,
                                          float[] componentBodyThicknessProfile)
    {
      Assert.expect(sliceNameEnum != null);
      Assert.expect(componentBodyThicknessProfile != null);
      float[] previousEntry = _sliceNameEnumToComponentBodyThicknessProfile.put(sliceNameEnum,
          componentBodyThicknessProfile);
      Assert.expect(previousEntry == null);
    }

    /**
     * @author Peter Esbensen
     */
    float[] getComponentBodyThicknessProfile(SliceNameEnum sliceNameEnum)
    {
      Assert.expect(sliceNameEnum != null);
      Assert.expect(_sliceNameEnumToComponentBodyThicknessProfile != null);
      return _sliceNameEnumToComponentBodyThicknessProfile.get(sliceNameEnum);
    }

    /**
     * @author Peter Esbensen
     */
    void setComponentDerivativeProfile(SliceNameEnum sliceNameEnum,
                                          float[] componentDerivativeProfile)
    {
      Assert.expect(sliceNameEnum != null);
      Assert.expect(componentDerivativeProfile != null);
      float[] previousEntry = _sliceNameEnumToComponentDerivativeProfile.put(sliceNameEnum,
          componentDerivativeProfile);
      Assert.expect(previousEntry == null);
    }

    /**
     * @author Peter Esbensen
     */
    float[] getComponentDerivativeProfile(SliceNameEnum sliceNameEnum)
    {
      Assert.expect(sliceNameEnum != null);
      Assert.expect(_sliceNameEnumToComponentDerivativeProfile != null);
      return _sliceNameEnumToComponentDerivativeProfile.get(sliceNameEnum);
    }
  }
}
