package com.axi.v810.business.imageAnalysis.pcap;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for resistors, capacitors (including polarized caps).
 *
 * @author Peter Esbensen
 */
public class PolarizedCapInspectionFamily extends InspectionFamily
{
  /**
   * @author Peter Esbensen
   */
  public PolarizedCapInspectionFamily()
  {
    super(InspectionFamilyEnum.POLARIZED_CAP);
    Algorithm chipMeasurementAlgorithm = new PolarizedCapMeasurementAlgorithm(this);
    addAlgorithm(chipMeasurementAlgorithm);
    Algorithm chipShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.POLARIZED_CAP);
    addAlgorithm(chipShortAlgorithm);
    Algorithm chipOpenAlgorithm = new PolarizedCapOpenAlgorithm(this);
    addAlgorithm(chipOpenAlgorithm);
    Algorithm chipMisalignmentAlgorithm = new PolarizedCapMisalignmentAlgorithm(this);
    addAlgorithm(chipMisalignmentAlgorithm);
    Algorithm chipInsufficientAlgorithm = new PolarizedCapInsufficientAlgorithm(this);
    addAlgorithm(chipInsufficientAlgorithm);
    Algorithm chipExcessAlgorithm = new PolarizedCapExcessAlgorithm(this);
    addAlgorithm(chipExcessAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Collection<SliceNameEnum> inspectedSlices = new LinkedList<SliceNameEnum>();
    inspectedSlices.add(SliceNameEnum.PAD);
    inspectedSlices.add(SliceNameEnum.PCAP_SLUG);

    return inspectedSlices;
  }

  /**
   * Returns the appropriate default SliceNameEnum for the Pad slice on the PolarizedCap inspection family given the joint
   * type of the specified JointInspectionData.
   *
   * @author Matt Wharton
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return SliceNameEnum.PAD;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return true;
  }

}
