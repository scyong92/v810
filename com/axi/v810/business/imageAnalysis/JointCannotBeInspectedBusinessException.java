package com.axi.v810.business.imageAnalysis;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * @author Peter Esbensen
 */
public class JointCannotBeInspectedBusinessException extends BusinessException
{
  /**
   * @author Peter Esbensen
   */
  public JointCannotBeInspectedBusinessException(String message)
  {
    super(new LocalizedString(message, null));
    Assert.expect(message != null);
  }
}
