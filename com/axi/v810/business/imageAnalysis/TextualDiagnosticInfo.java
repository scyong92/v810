package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;

/**
 * Represents the textual diagnostic information from the algorithms that is made available to the UI.
 *
 * @author Matt Wharton
 */
public class TextualDiagnosticInfo extends DiagnosticInfo
{
  private LocalizedString _diagnosticText = null;

  /**
   * Constructor.
   *
   * @param diagnosticText A LocalizedString which represents the diagnostic text message.
   * @author Matt Wharton
   */
  public TextualDiagnosticInfo(LocalizedString diagnosticText)
  {
    Assert.expect(diagnosticText != null);

    _diagnosticText = diagnosticText;
  }

  /**
   * Constructor
   *
   * @param localizedStringKey A string containing the localization key to be looked up.
   * @param keyInputs May be null, an array of inputs to the localization key ({0}, {1} dereferenced).
   * @author Patrick Lacz
   */
  public TextualDiagnosticInfo(String localizedStringKey, Object... keyInputs)
  {
    Assert.expect(localizedStringKey != null);
    _diagnosticText = new LocalizedString(localizedStringKey, keyInputs);
  }

  /**
   * Gets the diagnostic text.
   *
   * @return A LocalizedString representing the diagnostic text.
   * @author Matt Wharton
   */
  public LocalizedString getDiagnosticText()
  {
    Assert.expect(_diagnosticText != null);

    return _diagnosticText;
  }

  /**
   * Sets the diagnostic text.
   *
   * @param diagnosticText A LocalizedString representing the diagnostic text.
   * @author Matt Wharton
   */
  void setDiagnosticText(LocalizedString diagnosticText)
  {
    Assert.expect(diagnosticText != null);

    _diagnosticText = diagnosticText;
  }
}
