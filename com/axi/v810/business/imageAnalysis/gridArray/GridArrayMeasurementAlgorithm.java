package com.axi.v810.business.imageAnalysis.gridArray;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.StringLocalizer;

/**
 * Algorithm for measuring PBGA, CBGA, and CCGA joints.  Pretty much any sort
 * of round grid array device.
 *
 * @author Peter Esbensen
 */
public class GridArrayMeasurementAlgorithm extends Algorithm
{
  private final static String _PERCENT_OF_MAX = "Percent of Max";
  private final static String _MAX_SLOPE = "Max Slope";
  private final static String _ON = "On";
  private final static String _OFF = "Off";
  private final static String _TRUE = "True";
  private final static String _FALSE = "False";

  private final static String _MIN_NUMBER_OF_USER_DEFINED_SLICE = "0";
  private final static String _MAX_NUMBER_OF_USER_DEFINED_SLICE = "10";// Siew Yeng - XCR-2814 - reduce from 20 to 10
  
//  private final static ArrayList<String> _GRID_ARRAY_NUMBER_OF_SLICE = new ArrayList<String>(Arrays.asList(_MIN_NUMBER_OF_USER_DEFINED_SLICE, "1", "2", "3", "4", "5", "6", "7", "8", "9",_MAX_NUMBER_OF_USER_DEFINED_SLICE));

  final int _MAX_NEW_DATAPOINTS_FROM_LEARNING = 500;
  
  //private static final String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";

  /**
   * @author Bill Darbie
   */
  public GridArrayMeasurementAlgorithm(InspectionFamilyEnum inspectionFamilyEnum, InspectionFamily gridArrayInspectionFamily)
  {
    super(AlgorithmEnum.MEASUREMENT, inspectionFamilyEnum);

    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting>
        locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
    {
      addAlgorithmSetting(algSetting);
    }

    displayOrder += locatorAlgorithmSettings.size();
    
    AlgorithmSetting enableJointBasedEdgeDetection = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_ENABLE_JOINT_BASED_EDGE_DETECTION,
      displayOrder++,
      _FALSE, // default
      new ArrayList<>(Arrays.asList(_TRUE, _FALSE)),
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(ENABLE_JOINT_BASED_EDGE_DETECTION)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(ENABLE_JOINT_BASED_EDGE_DETECTION)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(ENABLE_JOINT_BASED_EDGE_DETECTION)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(enableJointBasedEdgeDetection);
    
    AlgorithmSetting edgeDetectionPercentPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PAD,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(edgeDetectionPercentPad);
    
    AlgorithmSetting edgeDetectionPercentLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_LOWERPAD,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentLowerPad.filter(JointTypeEnum.CGA);
    edgeDetectionPercentLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    edgeDetectionPercentLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(edgeDetectionPercentLowerPad);

    AlgorithmSetting edgeDetectionPercentMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_MIDBALL,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentMidball.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentMidball);

    AlgorithmSetting edgeDetectionPercentPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PACKAGE,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentPackage.filter(JointTypeEnum.CGA);
    edgeDetectionPercentPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(edgeDetectionPercentPackage);

    AlgorithmSetting edgeDetectionPercentUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    edgeDetectionPercentUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice1.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice1);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    edgeDetectionPercentUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice2.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice2);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    edgeDetectionPercentUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice3.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice3);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    edgeDetectionPercentUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice4.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice4);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    edgeDetectionPercentUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice5.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice5);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    edgeDetectionPercentUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice6.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice6);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    edgeDetectionPercentUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice7.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice7);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    edgeDetectionPercentUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice8.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice8);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    edgeDetectionPercentUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice9.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice9);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    edgeDetectionPercentUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice10.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice10);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    edgeDetectionPercentUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice11.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice11);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    edgeDetectionPercentUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice12.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice12);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    edgeDetectionPercentUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice13.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice13);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    edgeDetectionPercentUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice14.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice14);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    edgeDetectionPercentUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice15.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice15);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    edgeDetectionPercentUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice16.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice16);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    edgeDetectionPercentUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice17.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice17);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    edgeDetectionPercentUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice18.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice18);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    edgeDetectionPercentUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice19.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice19);
    
    AlgorithmSetting edgeDetectionPercentUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20,
      displayOrder++,
      80.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    edgeDetectionPercentUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    edgeDetectionPercentUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    edgeDetectionPercentUserDefinedSlice20.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(edgeDetectionPercentUserDefinedSlice20);

    AlgorithmSetting profileSearchDistance = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_PROFILE_SEARCH_DISTANCE,
      displayOrder++,
      100f, // default
      0.0f, // min
      100f, // max
      MeasurementUnitsEnum.PERCENT_OF_PITCH,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(PROFILE_SEARCH_DISTANCE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(PROFILE_SEARCH_DISTANCE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(PROFILE_SEARCH_DISTANCE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(profileSearchDistance);

    ArrayList<String>
        allowableEdgeDetectionTechniques = new ArrayList<String>(Arrays.asList(_PERCENT_OF_MAX, _MAX_SLOPE));
    AlgorithmSetting edgeDetectionTechnique = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE,
      displayOrder++,
      _PERCENT_OF_MAX,
      allowableEdgeDetectionTechniques,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(GRID_ARRAY_EDGE_DETECTION_TECHNIQUE)_KEY",
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(GRID_ARRAY_EDGE_DETECTION_TECHNIQUE)_KEY",
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(GRID_ARRAY_EDGE_DETECTION_TECHNIQUE)_KEY",
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(edgeDetectionTechnique);

    AlgorithmSetting nominalDiameterPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   nominalDiameterPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   nominalDiameterPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);

    nominalDiameterPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(nominalDiameterPad);

    AlgorithmSetting nominalThicknessPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   nominalThicknessPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   nominalThicknessPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessPad.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessPad);
    
    AlgorithmSetting nominalDiameterLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterLowerPad,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);

    nominalDiameterLowerPad.filter(JointTypeEnum.CGA);
    nominalDiameterLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalDiameterLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalDiameterLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(nominalDiameterLowerPad);
    
    AlgorithmSetting nominalThicknessLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessLowerPad,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessLowerPad.filter(JointTypeEnum.CGA);
    nominalThicknessLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(nominalThicknessLowerPad);

    AlgorithmSetting nominalDiameterMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterMidball.filter(JointTypeEnum.CGA);
    nominalDiameterMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterMidball);

    AlgorithmSetting nominalThicknessMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessMidball.filter(JointTypeEnum.CGA);
    nominalThicknessMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalThicknessMidball);

    AlgorithmSetting nominalDiameterPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   nominalDiameterPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterPackage.filter(JointTypeEnum.CGA);
    nominalDiameterPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(nominalDiameterPackage);

    AlgorithmSetting nominalThicknessPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   nominalThicknessPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessPackage.filter(JointTypeEnum.CGA);
    nominalThicknessPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessPackage.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessPackage.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessPackage);

    AlgorithmSetting nominalDiameterUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice1.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice1);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice1.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice1);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice2.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice2);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice2.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice2);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice3.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice3);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice3.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice3);

    AlgorithmSetting nominalDiameterUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice4.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice4);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice4.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice4.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice4.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice4.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice4);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice5.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice5);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice5.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice5.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice5.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice5.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice5);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice6.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice6);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice6.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice6.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice6.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice6.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice6);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice7.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice7);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice7.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice7.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice7.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice7.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice7);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice8.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice8);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice8.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice8.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice8.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice8.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice8);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice9.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice9);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice9.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice9.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice9.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice9.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice9);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalDiameterUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice10.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice10);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    nominalThicknessUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice10.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice10.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice10.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice10.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice10);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice11.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice11);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice11.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice11.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice11.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice11.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice11);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice12.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice12);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice12.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice12.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice12.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice12.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice12);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice13.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice13);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice13.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice13.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice13.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice13.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice13);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice14.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice14);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice14.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice14.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice14.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice14.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice14);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice15.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice15);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice15.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice15.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice15.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice15.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice15);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice16.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice16);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice16.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice16.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice16.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice16.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice16);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice17.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice17);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice17.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice17.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice17.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice17.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice17);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice18.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice18);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice18.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice18.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice18.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice18.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice18);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice19.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice19);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice19.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice19.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice19.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice19.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice19);
    
    AlgorithmSetting nominalDiameterUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(30.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalDiameterUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalDiameterUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER);
    nominalDiameterUserDefinedSlice20.filter(JointTypeEnum.CGA);
    nominalDiameterUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(nominalDiameterUserDefinedSlice20);
    
    AlgorithmSetting nominalThicknessUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(15.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(200.0f), // max /
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(NOMINAL_THICKNESS_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    nominalThicknessUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   nominalThicknessUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   nominalThicknessUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   nominalThicknessUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS);
    nominalThicknessUserDefinedSlice20.filter(JointTypeEnum.CGA);
    nominalThicknessUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice20.filter(JointTypeEnum.COLLAPSABLE_BGA);
    nominalThicknessUserDefinedSlice20.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    nominalThicknessUserDefinedSlice20.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(nominalThicknessUserDefinedSlice20);
    
    AlgorithmSetting backgroundRegionInnerEdgeLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_INNER_EDGE_LOCATION,
      displayOrder++,
      25.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionInnerEdgeLocation);

    AlgorithmSetting backgroundRegionOuterEdgeLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_OUTER_EDGE_LOCATION,
      displayOrder++,
      75.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionOuterEdgeLocation);

    ArrayList<String>
        useFixedEccentricityAxisChoices = new ArrayList<String>(Arrays.asList(_ON, _OFF));
    AlgorithmSetting useFixedEccentricityAxis = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_USE_FIXED_ECCENTRICITY_AXIS,
      displayOrder++,
      _OFF,
      useFixedEccentricityAxisChoices,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USE_FIXED_ECCENTRICITY_AXIS)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USE_FIXED_ECCENTRICITY_AXIS)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USE_FIXED_ECCENTRICITY_AXIS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    // hide this setting for every joint type that doesn't need it
    for (JointTypeEnum jointTypeEnum : JointTypeEnum.getAllJointTypeEnums())
    {
      if (GridArrayOpenAlgorithm.eccentricityIsTested(jointTypeEnum) == false)
        useFixedEccentricityAxis.filter(jointTypeEnum);
    }
    addAlgorithmSetting(useFixedEccentricityAxis);


    AlgorithmSetting fixedEccentricityAxis = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_FIXED_ECCENTRICITY_AXIS,
      displayOrder++,
      45f, // default
      -360f, // min
      360f, // max
      MeasurementUnitsEnum.DEGREES,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(FIXED_ECCENTRICITY_AXIS)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(FIXED_ECCENTRICITY_AXIS)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(FIXED_ECCENTRICITY_AXIS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    // hide this setting for every joint type that doesn't need it
    for (JointTypeEnum jointTypeEnum : JointTypeEnum.getAllJointTypeEnums())
    {
      if (GridArrayOpenAlgorithm.eccentricityIsTested(jointTypeEnum) == false)
        fixedEccentricityAxis.filter(jointTypeEnum);
    }
    addAlgorithmSetting(fixedEccentricityAxis);

    // added by wei chin 14 Jul
    // Contrast Enhancement
    ArrayList<String> allowableContrastEnhancementValues =
      new ArrayList<String>(Arrays.asList(
        "On",
        "Off"));
    AlgorithmSetting contrastEnhancementSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CONTRAST_ENHANCEMENT,
      displayOrder++,
      "Off",
      allowableContrastEnhancementValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(CONTRAST_ENHANCEMENT)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(CONTRAST_ENHANCEMENT)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(CONTRAST_ENHANCEMENT)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(contrastEnhancementSetting);

    //Siew Yeng - XCR-3094
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList("False", "True"));
    AlgorithmSetting enableSaveAllJointImageWhenFailComponent = new AlgorithmSetting(
        AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS,
        displayOrder++,
        "False",
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // description URL key
        "HTML_DETAILED_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // desailed description URL key
        "IMG_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(enableSaveAllJointImageWhenFailComponent);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting padSliceOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    padSliceOffset.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(padSliceOffset);

    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting additionalPadSliceOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(-5.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    additionalPadSliceOffset.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    additionalPadSliceOffset.filter(JointTypeEnum.CGA);
    additionalPadSliceOffset.filter(JointTypeEnum.COLLAPSABLE_BGA);
    additionalPadSliceOffset.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(additionalPadSliceOffset);


    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting midballSliceOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_MIDBALL_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_MIDBALL_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_MIDBALL_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_MIDBALL_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    midballSliceOffset.filter(JointTypeEnum.CGA);
    midballSliceOffset.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(midballSliceOffset);

    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting packageSliceOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_PACKAGE_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_PACKAGE_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_PACKAGE_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_PACKAGE_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    packageSliceOffset.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    packageSliceOffset.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(packageSliceOffset);
	
	//Swee Yee Wong
    ArrayList<String> defineOffsetForPadAndPackageDropList = new ArrayList<String>(Arrays.asList(_ON, _OFF));
    AlgorithmSetting defineOffsetForPadAndPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE,
      displayOrder++,
      _OFF,
      defineOffsetForPadAndPackageDropList,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(DEFINE_OFFSET_FOR_PAD_AND_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(DEFINE_OFFSET_FOR_PAD_AND_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(DEFINE_OFFSET_FOR_PAD_AND_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    defineOffsetForPadAndPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    defineOffsetForPadAndPackage.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(defineOffsetForPadAndPackage);
    
	//Swee Yee Wong
    // Note:  This sliceheight setting is used to provide constant pad and package distance for each joint in same component.
    AlgorithmSetting midballToEdgeOffset = new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(0.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT)_KEY", // image file
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    midballToEdgeOffset.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    midballToEdgeOffset.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(midballToEdgeOffset);

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);

    // Added by Lee Herng (4 Mar 2011)
    AlgorithmSetting autoFocusMidBallOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -200.0f), // minimum value
        MathUtil.convertMilsToMillimeters(200.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(autoFocusMidBallOffset);

    ArrayList<String> gridArrayNumberOfSlices = new ArrayList();//(Arrays.asList(_MIN_NUMBER_OF_USER_DEFINED_SLICE, "1", "2", "3", "4", "5", "6", "7", "8", "9",_MAX_NUMBER_OF_USER_DEFINED_SLICE));
    for(int slice = Integer.parseInt(_MIN_NUMBER_OF_USER_DEFINED_SLICE) ; slice <= Integer.parseInt(_MAX_NUMBER_OF_USER_DEFINED_SLICE) ; slice++ )
    {
      gridArrayNumberOfSlices.add(""+slice);
    }
    // Added by Lee herng on 5-Apr-2012
    AlgorithmSetting userDefineNumOfSlice = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE, // setting enum
        displayOrder++, // display order,
        _MIN_NUMBER_OF_USER_DEFINED_SLICE,
        gridArrayNumberOfSlices,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_MEASUREMENT_(NUMBER_OF_USER_DEFINED_SLICE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(NUMBER_OF_USER_DEFINED_SLICE)_KEY", // detailed description URL Key
        "IMG_DESC_GRIDARRAY_MEASUREMENT_(NUMBER_OF_USER_DEFINED_SLICE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(userDefineNumOfSlice);
    
    AlgorithmSetting userDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_1)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_1)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_1)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice1);
    
    AlgorithmSetting userDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_2)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_2)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_2)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice2);
    
    AlgorithmSetting userDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_3)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_3)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_3)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice3);
    
    AlgorithmSetting userDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_4, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_4)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_4)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_4)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice4);
    
    AlgorithmSetting userDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_5, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_5)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_5)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_5)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice5);
    
    AlgorithmSetting userDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_6, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_6)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_6)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_6)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice6);
    
    AlgorithmSetting userDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_7, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_7)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_7)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_7)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice7);
    
    AlgorithmSetting userDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_8, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_8)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_8)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_8)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice8);
    
    AlgorithmSetting userDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_9, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_9)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_9)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_9)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice9);
    
    AlgorithmSetting userDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_10, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_10)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_10)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_10)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice10);
    
    AlgorithmSetting userDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_11, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_11)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_11)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_11)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice11);
    
    AlgorithmSetting userDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_12, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_12)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_12)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_12)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice12);
    
    AlgorithmSetting userDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_13, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_13)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_13)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_13)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice13);
    
    AlgorithmSetting userDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_14, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_14)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_14)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_14)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice14);
    
    AlgorithmSetting userDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_15, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_15)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_15)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_15)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice15);
    
    AlgorithmSetting userDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_16, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_16)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_16)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_16)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice16);
    
    AlgorithmSetting userDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_17, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_17)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_17)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_17)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice17);
    
    AlgorithmSetting userDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_18, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_18)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_18)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_18)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice18);
    
    AlgorithmSetting userDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_19, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_19)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_19)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_19)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice19);
    
    AlgorithmSetting userDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_USER_DEFINED_SLICE_20, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_20)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_20)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_SLICE_20)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice20);

    addMeasurementEnums();
    
    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    AlgorithmSetting userDefinedWaveLetLevel = new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is right before psh in the Slice Setup tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(userDefinedWaveLetLevel);
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    AlgorithmSetting predictiveSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab - CR31430
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(predictiveSliceHeight);

    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_DIAMETER);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());
  }


  /**
   * @author Peter Esbensen
   */
  private void computeJointOffsetMeasurements(int locatedX,
                                              int locatedY,
                                              JointInspectionData jointInspectionData,
                                              boolean successfullyMeasuredJoint,
                                              RegionOfInterest measuredRegion,
                                              SliceNameEnum sliceNameEnum,
                                              List<Float> xOffsets,
                                              List<Float> yOffsets,
                                              final float MILLIMETERS_PER_PIXEL)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(xOffsets != null);
    Assert.expect(yOffsets != null);
    Assert.expect(sliceNameEnum != null);
    // Assert.expect(measuredRegion != null);  this is expected to be null if measurement is not valid

    if (GridArrayMisalignmentAlgorithm.shouldMisalignmentBeTested(jointInspectionData.getJointTypeEnum(),
                                                                  sliceNameEnum))
    {
      RegionOfInterest cadPadRegion = jointInspectionData.getOrthogonalRegionOfInterestInPixels();
      int cadX = cadPadRegion.getCenterX();
      int cadY = cadPadRegion.getCenterY();

      int offsetFromCadX = locatedX - cadX;
      int offsetFromCadY = locatedY - cadY;

      if (successfullyMeasuredJoint)
      {
        offsetFromCadX = measuredRegion.getCenterX() - cadPadRegion.getCenterX();
        offsetFromCadY = measuredRegion.getCenterY() - cadPadRegion.getCenterY();
      }

      float offsetFromCadXInMM = offsetFromCadX * MILLIMETERS_PER_PIXEL;
      float offsetFromCadYInMM = offsetFromCadY * MILLIMETERS_PER_PIXEL;

      float offsetFromCADInMM = (float)Math.sqrt(offsetFromCadXInMM * offsetFromCadXInMM +
                                                 offsetFromCadYInMM * offsetFromCadYInMM);

      Pad pad = jointInspectionData.getPad();
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD,
                                                                MeasurementUnitsEnum.MILLIMETERS,
                                                                pad,
                                                                sliceNameEnum,
                                                                offsetFromCADInMM,
                                                                successfullyMeasuredJoint));
      jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD,
                                                                MeasurementUnitsEnum.MILLIMETERS,
                                                                pad,
                                                                sliceNameEnum,
                                                                offsetFromCadXInMM,
                                                                successfullyMeasuredJoint));
      jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD,
                                                                MeasurementUnitsEnum.MILLIMETERS,
                                                                pad,
                                                                sliceNameEnum,
                                                                offsetFromCadYInMM,
                                                                successfullyMeasuredJoint));
      xOffsets.add((float)offsetFromCadXInMM);
      yOffsets.add((float)offsetFromCadYInMM);
    }
  }
  
    /**
   * @author Peter Esbensen
   * @author Seng-Yew Lim - Display "Measured Joint" first because it has lower enum index, and always covered by "Eccentricity Region" which has higher enum index.
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

	final float MILLIMETERS_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
	
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    SliceNameEnum sliceForLocator = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    Locator.locateJoints(reconstructedImages, sliceForLocator, jointInspectionDataObjects, this, true);

    List<SliceNameEnum> slicesInOrderOfInspection = getInspectionSliceOrder(subtype);
    
    boolean isBGAInspectionRegionSetTo1X1 = subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1();
    if (isBGAInspectionRegionSetTo1X1 == false || isJointBasedEdgeDetectionEnabled(subtype) == false)
    {
      for (SliceNameEnum sliceNameEnum : slicesInOrderOfInspection)
      {
        ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
        Image image = reconstructedSlice.getOrthogonalImage();

        //Siew Yeng - XCR-2683 - add enhanced image
        if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
        {
          ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, sliceNameEnum);
        }
      
        // added by wei chin 14-Jul
        // Enhance the contrast if needed
        if (isContrastEnhancementEnabled(subtype))
        {
          AlgorithmUtil.enhanceContrastByEqualizingHistogram(image);
        }

        // draw the slice image
        AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

        AlgorithmSettingEnum nominalDiameterAlgorithmSetting = getNominalDiameterAlgorithmSetting(sliceNameEnum);
        float nominalDiameterInMillimeters = (Float) subtype.getAlgorithmSettingValue(nominalDiameterAlgorithmSetting);
        float nominalDiameterInPixels = nominalDiameterInMillimeters / MILLIMETERS_PER_PIXEL;

        AlgorithmSettingEnum nominalThicknessAlgorithmSetting = getNominalThicknessAlgorithmSetting(sliceNameEnum);
        float nominalThicknessInMillimeters = (Float) subtype.getAlgorithmSettingValue(nominalThicknessAlgorithmSetting);
        float nominalThicknessInMils = MathUtil.convertMillimetersToMils(nominalThicknessInMillimeters);

        List<Float> validDiameterMeasurementsInMils = new ArrayList<Float>();
        List<Float> xOffsetMeasurementsInMM = new ArrayList<Float>();
        List<Float> yOffsetMeasurementsInMM = new ArrayList<Float>();

        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                    jointInspectionData,
                                                    reconstructionRegion,
                                                    reconstructedSlice,
                                                    false);

          RegionOfInterest locatedRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
          ImageCoordinate locatedCenter = new ImageCoordinate(locatedRegion.getCenterX(), locatedRegion.getCenterY());

          BooleanRef foundJointInImage = new BooleanRef(true);

          int pitchInPixels = jointInspectionData.getPitchInPixels();

          FloatRef diameterInMilsReference = new FloatRef(0.0f);

          String edgeDetectionTechnique = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
              GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE);
          boolean useMaxSlopeTechnique = edgeDetectionTechnique.contains(_MAX_SLOPE);
          float edgeDetectionFraction = getEdgeDetectionPercentageAlgorithmSettingValue(subtype, sliceNameEnum) * 0.01f;

          float profileSearchDistanceMultiplier =
              (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_PROFILE_SEARCH_DISTANCE) /
              100.f;

          float profileSearchDistanceInPixels = pitchInPixels * profileSearchDistanceMultiplier;

          // it doesn't make sense to let profileSearchDistance < nominalDiameterInPixels
          profileSearchDistanceInPixels = Math.max(profileSearchDistanceInPixels, nominalDiameterInPixels);

          // LeeHerng - Temporary remark the following printout. It caused slow inspection when DeveloperDebugMode is turn on.
  //        if (Config.isDeveloperDebugModeOn())
  //        {
  //            System.out.println("jointInspectionData: ");
  //            System.out.println("RefDes: " + jointInspectionData.getComponent().getReferenceDesignator());
  //            System.out.println("Pad: " + jointInspectionData.getPad().getName());
  //        }
          // search for the joint
          RegionOfInterest measuredRegion = AlgorithmUtil.measureDiameter(image,
                                                                          jointInspectionData,
                                                                          locatedRegion,
                                                                          nominalDiameterInPixels,
                                                                          getNominalThicknessForDiagnosticProfileInMils(subtype),
                                                                          subtype,
                                                                          reconstructedImages.getReconstructionRegion(),
                                                                          sliceNameEnum,
                                                                          foundJointInImage,
                                                                          profileSearchDistanceInPixels,
                                                                          diameterInMilsReference,
                                                                          useMaxSlopeTechnique,
                                                                          edgeDetectionFraction,
                                                                          this);

          // use default values for thickness and eccentricity in case we can't measure them (because we couldn't find the joint)
          boolean measurementIsValid = false;
          float thicknessInMils = 0.0f;
          float eccentricity = 0.0f;

          boolean thicknessShouldBeMeasured = GridArrayOpenAlgorithm.shouldThicknessBeTested(subtype.getJointTypeEnum(), sliceNameEnum);

          JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
          boolean eccentricityShouldBeMeasured = GridArrayOpenAlgorithm.shouldMinimumEccentricityBeTested(jointTypeEnum, sliceNameEnum) ||
                                                 GridArrayOpenAlgorithm.shouldMaximumEccentricityBeTested(jointTypeEnum, sliceNameEnum);

          if (foundJointInImage.getValue() == true)
          {
            measurementIsValid = true;
            if (thicknessShouldBeMeasured)
            {
              thicknessInMils = measureThickness(image, reconstructedSlice, measuredRegion, jointInspectionData, reconstructionRegion, MILLIMETERS_PER_PIXEL);
            }
            validDiameterMeasurementsInMils.add((float)diameterInMilsReference.getValue());

            // Display "Measured Joint" first because it has lower enum index, and always covered by "Eccentricity Region" which has higher enum index.
            // By doing this, users will see the "Measured Joint" more clearly when stepping through.
            // Moved from end of this function to here by Seng-Yew Lim
            _diagnostics.postDiagnostics(reconstructionRegion,
                                         sliceNameEnum,
                                         jointInspectionData,
                                         this,
                                         false,
                                         new MeasurementRegionDiagnosticInfo(measuredRegion,
                                                                             MeasurementRegionEnum.MEASURED_JOINT_BOUNDARY));

            // Measure the joint eccentricity
            if (eccentricityShouldBeMeasured)
            {
              eccentricity = measureEccentricity(reconstructedSlice, reconstructedImages, jointInspectionData, measuredRegion,
                                                 subtype, diameterInMilsReference.getValue(), MILLIMETERS_PER_PIXEL);
            }

          }
          else
          {
            postMessageThatNoJointWasFound(this, jointInspectionData, reconstructionRegion, reconstructedSlice);
          }

          JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
          Pad pad = jointInspectionData.getPad();
          JointMeasurement diameterMeasurement = new JointMeasurement(this,
                                                                      MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                      MeasurementUnitsEnum.MILLIMETERS,
                                                                      pad,
                                                                      sliceNameEnum,
                                                                      (float)MathUtil.convertMilsToMillimeters(diameterInMilsReference.getValue()),
                                                                      measurementIsValid);
          jointInspectionResult.addMeasurement(diameterMeasurement);

          if (GridArrayOpenAlgorithm.shouldMinimumDiameterBeTested(subtype.getJointTypeEnum(), sliceNameEnum))
          {
            float diameterPercentOfNominal = (100.0f * MathUtil.convertMilsToMillimeters(diameterInMilsReference.getValue()) /
                                              nominalDiameterInMillimeters);
            jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                      MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL,
                                                                      MeasurementUnitsEnum.PERCENT,
                                                                      pad,
                                                                      sliceNameEnum,
                                                                      (float)diameterPercentOfNominal,
                                                                      measurementIsValid));
          }

          JointMeasurement thicknessMeasurement = null;
          if (thicknessShouldBeMeasured)
          {
            thicknessMeasurement = new JointMeasurement(this,
                                                        MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                        MeasurementUnitsEnum.MILLIMETERS,
                                                        pad,
                                                        sliceNameEnum,
                                                        (float)MathUtil.convertMilsToMillimeters(thicknessInMils),
                                                        measurementIsValid);
            jointInspectionResult.addMeasurement(thicknessMeasurement);
            jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                     MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL,
                                                                     MeasurementUnitsEnum.PERCENT,
                                                                     pad,
                                                                     sliceNameEnum,
                                                                     (float)(100.0f * thicknessInMils / nominalThicknessInMils),
                                                                     measurementIsValid));
         }

         if (eccentricityShouldBeMeasured)
          {
            JointMeasurement eccentricityMeasurement = new JointMeasurement(this,
                                                                            MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY,
                                                                            MeasurementUnitsEnum.NONE,
                                                                            pad,
                                                                            reconstructedSlice.getSliceNameEnum(),
                                                                            (float)eccentricity);
            jointInspectionResult.addMeasurement(eccentricityMeasurement);

            // post the measurement data for the user to see
            AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, jointInspectionData, eccentricityMeasurement);
          }



          computeJointOffsetMeasurements(locatedCenter.getX(),
                                         locatedCenter.getY(),
                                         jointInspectionData,
                                         measurementIsValid,
                                         measuredRegion,
                                         sliceNameEnum,
                                         xOffsetMeasurementsInMM,
                                         yOffsetMeasurementsInMM,
					 MILLIMETERS_PER_PIXEL);

          if (measurementIsValid)
          {

            // Tell the user what our measurements were.
            if (thicknessShouldBeMeasured)
            {
              AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                              reconstructionRegion,
                                                              jointInspectionData,
                                                              diameterMeasurement,
                                                              thicknessMeasurement);
            }
            else
            {
              AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                              reconstructionRegion,
                                                              jointInspectionData,
                                                              diameterMeasurement);
            }
          }
        }
        //Siew Yeng - XCR-2762
        if(isBGAInspectionRegionSetTo1X1 == false)
        {
          GridArrayOpenAlgorithm gridArrayOpenAlgorithm = (GridArrayOpenAlgorithm)InspectionFamily.getAlgorithm(this.getInspectionFamilyEnum(), AlgorithmEnum.OPEN);
          gridArrayOpenAlgorithm.computeRegionOutlierMeasurementsForDiameter(jointInspectionDataObjects, 
                                                                              ArrayUtil.convertFloatListToFloatArray(validDiameterMeasurementsInMils),
                                                                              sliceNameEnum,
                                                                              false);
          GridArrayMisalignmentAlgorithm gridArrayMisalignmentAlgorithm = (GridArrayMisalignmentAlgorithm)InspectionFamily.getAlgorithm(this.getInspectionFamilyEnum(), AlgorithmEnum.MISALIGNMENT);
          gridArrayMisalignmentAlgorithm.computeRegionOutlierMeasurementsForLocation(jointInspectionDataObjects,
                                                                                    ArrayUtil.convertFloatListToFloatArray(xOffsetMeasurementsInMM),
                                                                                    ArrayUtil.convertFloatListToFloatArray(yOffsetMeasurementsInMM),
                                                                                    sliceNameEnum,
                                                                                    false);
//          computeRegionOutlierMeasurementsForDiameterAndLocation(jointInspectionDataObjects,
//            ArrayUtil.convertFloatListToFloatArray(validDiameterMeasurementsInMils),
//            ArrayUtil.convertFloatListToFloatArray(xOffsetMeasurementsInMM),
//            ArrayUtil.convertFloatListToFloatArray(yOffsetMeasurementsInMM),
//            sliceNameEnum);
        }
      }
    }
    else
    {
      Map<SliceNameEnum, List<Float>> sliceNameEnumToDiameterArrayMap = new HashMap<SliceNameEnum, List<Float>>();
      Map<SliceNameEnum, List<Float>> sliceNameEnumToxOffsetArrayMap = new HashMap<SliceNameEnum, List<Float>>();
      Map<SliceNameEnum, List<Float>> sliceNameEnumToyOffsetArrayMap = new HashMap<SliceNameEnum, List<Float>>();
      for (SliceNameEnum sliceNameEnum : slicesInOrderOfInspection)
      {
        List<Float> validDiameterMeasurementsInMils = new ArrayList<Float>();
        List<Float> xOffsetMeasurementsInMM = new ArrayList<Float>();
        List<Float> yOffsetMeasurementsInMM = new ArrayList<Float>();
        sliceNameEnumToDiameterArrayMap.put(sliceNameEnum, validDiameterMeasurementsInMils);
        sliceNameEnumToxOffsetArrayMap.put(sliceNameEnum, xOffsetMeasurementsInMM);
        sliceNameEnumToyOffsetArrayMap.put(sliceNameEnum, yOffsetMeasurementsInMM);
      }
      // The edge detection gray level for non-midball slices when joint based thresholding is used
      float[] edgeDetectionGrayLevel = new float[4];
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        for (SliceNameEnum sliceNameEnum : slicesInOrderOfInspection)
        {
          List<Float> validDiameterMeasurementsInMils = sliceNameEnumToDiameterArrayMap.get(sliceNameEnum);
          List<Float> xOffsetMeasurementsInMM = sliceNameEnumToxOffsetArrayMap.get(sliceNameEnum);
          List<Float> yOffsetMeasurementsInMM = sliceNameEnumToyOffsetArrayMap.get(sliceNameEnum);
          
          ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
          Image image = reconstructedSlice.getOrthogonalImage();

          // RJG: Reset the edge detection gray level when joint based thresholding is used, it will be set from the
          // midball slice
          if (sliceNameEnum.getName().equals("Midball"))
          {
            for (int i = 0; i < edgeDetectionGrayLevel.length; i++)
            {
              edgeDetectionGrayLevel[i] = -1.0F;
            }
          }

          // added by wei chin 14-Jul
          // Enhance the contrast if needed
          if (isContrastEnhancementEnabled(subtype))
          {
            AlgorithmUtil.enhanceContrastByEqualizingHistogram(image);
          }

          // draw the slice image
          AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

          AlgorithmSettingEnum nominalDiameterAlgorithmSetting = getNominalDiameterAlgorithmSetting(sliceNameEnum);
          float nominalDiameterInMillimeters = (Float) subtype.getAlgorithmSettingValue(nominalDiameterAlgorithmSetting);
          float nominalDiameterInPixels = nominalDiameterInMillimeters / MILLIMETERS_PER_PIXEL;

          AlgorithmSettingEnum nominalThicknessAlgorithmSetting = getNominalThicknessAlgorithmSetting(sliceNameEnum);
          float nominalThicknessInMillimeters = (Float) subtype.getAlgorithmSettingValue(nominalThicknessAlgorithmSetting);
          float nominalThicknessInMils = MathUtil.convertMillimetersToMils(nominalThicknessInMillimeters);

            AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                      jointInspectionData,
                                                      reconstructionRegion,
                                                      reconstructedSlice,
                                                      false);

            RegionOfInterest locatedRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
            ImageCoordinate locatedCenter = new ImageCoordinate(locatedRegion.getCenterX(), locatedRegion.getCenterY());

            BooleanRef foundJointInImage = new BooleanRef(true);

            int pitchInPixels = jointInspectionData.getPitchInPixels();

            FloatRef diameterInMilsReference = new FloatRef(0.0f);

            String edgeDetectionTechnique = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE);
            boolean useMaxSlopeTechnique = edgeDetectionTechnique.contains(_MAX_SLOPE);
            float edgeDetectionFraction = getEdgeDetectionPercentageAlgorithmSettingValue(subtype, sliceNameEnum) * 0.01f;

            float profileSearchDistanceMultiplier =
                (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_PROFILE_SEARCH_DISTANCE) /
                100.f;

            float profileSearchDistanceInPixels = pitchInPixels * profileSearchDistanceMultiplier;

            // it doesn't make sense to let profileSearchDistance < nominalDiameterInPixels
            profileSearchDistanceInPixels = Math.max(profileSearchDistanceInPixels, nominalDiameterInPixels);

            // LeeHerng - Temporary remark the following printout. It caused slow inspection when DeveloperDebugMode is turn on.
    //        if (Config.isDeveloperDebugModeOn())
    //        {
    //            System.out.println("jointInspectionData: ");
    //            System.out.println("RefDes: " + jointInspectionData.getComponent().getReferenceDesignator());
    //            System.out.println("Pad: " + jointInspectionData.getPad().getName());
    //        }
            // search for the joint
            RegionOfInterest measuredRegion = AlgorithmUtil.measureDiameterWithJointBasedThreshold(image,
                                                                            jointInspectionData,
                                                                            locatedRegion,
                                                                            nominalDiameterInPixels,
                                                                            getNominalThicknessForDiagnosticProfileInMils(subtype),
                                                                            subtype,
                                                                            reconstructedImages.getReconstructionRegion(),
                                                                            sliceNameEnum,
                                                                            foundJointInImage,
                                                                            profileSearchDistanceInPixels,
                                                                            diameterInMilsReference,
                                                                            useMaxSlopeTechnique,
                                                                            edgeDetectionFraction,
                                                                            edgeDetectionGrayLevel,
                                                                            this);

            // use default values for thickness and eccentricity in case we can't measure them (because we couldn't find the joint)
            boolean measurementIsValid = false;
            float thicknessInMils = 0.0f;
            float eccentricity = 0.0f;

            boolean thicknessShouldBeMeasured = GridArrayOpenAlgorithm.shouldThicknessBeTested(subtype.getJointTypeEnum(), sliceNameEnum);

            JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
            boolean eccentricityShouldBeMeasured = GridArrayOpenAlgorithm.shouldMinimumEccentricityBeTested(jointTypeEnum, sliceNameEnum) ||
                                                   GridArrayOpenAlgorithm.shouldMaximumEccentricityBeTested(jointTypeEnum, sliceNameEnum);

            if (foundJointInImage.getValue() == true)
            {
              measurementIsValid = true;
              if (thicknessShouldBeMeasured)
              {
                thicknessInMils = measureThickness(image, reconstructedSlice, measuredRegion, jointInspectionData, reconstructionRegion, MILLIMETERS_PER_PIXEL);
              }
              validDiameterMeasurementsInMils.add((float)diameterInMilsReference.getValue());

              // Display "Measured Joint" first because it has lower enum index, and always covered by "Eccentricity Region" which has higher enum index.
              // By doing this, users will see the "Measured Joint" more clearly when stepping through.
              // Moved from end of this function to here by Seng-Yew Lim
              _diagnostics.postDiagnostics(reconstructionRegion,
                                           sliceNameEnum,
                                           jointInspectionData,
                                           this,
                                           false,
                                           new MeasurementRegionDiagnosticInfo(measuredRegion,
                                                                               MeasurementRegionEnum.MEASURED_JOINT_BOUNDARY));

              // Measure the joint eccentricity
              if (eccentricityShouldBeMeasured)
              {
                eccentricity = measureEccentricity(reconstructedSlice, reconstructedImages, jointInspectionData, measuredRegion,
                                                   subtype, diameterInMilsReference.getValue(), MILLIMETERS_PER_PIXEL);
              }

            }
            else
            {
              postMessageThatNoJointWasFound(this, jointInspectionData, reconstructionRegion, reconstructedSlice);
            }

            JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
            Pad pad = jointInspectionData.getPad();
            JointMeasurement diameterMeasurement = new JointMeasurement(this,
                                                                        MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                        MeasurementUnitsEnum.MILLIMETERS,
                                                                        pad,
                                                                        sliceNameEnum,
                                                                        (float)MathUtil.convertMilsToMillimeters(diameterInMilsReference.getValue()),
                                                                        measurementIsValid);
            jointInspectionResult.addMeasurement(diameterMeasurement);

            if (GridArrayOpenAlgorithm.shouldMinimumDiameterBeTested(subtype.getJointTypeEnum(), sliceNameEnum))
            {
              float diameterPercentOfNominal = (100.0f * MathUtil.convertMilsToMillimeters(diameterInMilsReference.getValue()) /
                                                nominalDiameterInMillimeters);
              jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                        MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL,
                                                                        MeasurementUnitsEnum.PERCENT,
                                                                        pad,
                                                                        sliceNameEnum,
                                                                        (float)diameterPercentOfNominal,
                                                                        measurementIsValid));
            }

            JointMeasurement thicknessMeasurement = null;
            if (thicknessShouldBeMeasured)
            {
              thicknessMeasurement = new JointMeasurement(this,
                                                          MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                          MeasurementUnitsEnum.MILLIMETERS,
                                                          pad,
                                                          sliceNameEnum,
                                                          (float)MathUtil.convertMilsToMillimeters(thicknessInMils),
                                                          measurementIsValid);
              jointInspectionResult.addMeasurement(thicknessMeasurement);
              jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                       MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL,
                                                                       MeasurementUnitsEnum.PERCENT,
                                                                       pad,
                                                                       sliceNameEnum,
                                                                       (float)(100.0f * thicknessInMils / nominalThicknessInMils),
                                                                       measurementIsValid));
           }

           if (eccentricityShouldBeMeasured)
            {
              JointMeasurement eccentricityMeasurement = new JointMeasurement(this,
                                                                              MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY,
                                                                              MeasurementUnitsEnum.NONE,
                                                                              pad,
                                                                              reconstructedSlice.getSliceNameEnum(),
                                                                              (float)eccentricity);
              jointInspectionResult.addMeasurement(eccentricityMeasurement);

              // post the measurement data for the user to see
              AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, jointInspectionData, eccentricityMeasurement);
            }

           computeJointOffsetMeasurements(locatedCenter.getX(),
                                           locatedCenter.getY(),
                                           jointInspectionData,
                                           measurementIsValid,
                                           measuredRegion,
                                           sliceNameEnum,
                                           xOffsetMeasurementsInMM,
                                           yOffsetMeasurementsInMM,
                                           MILLIMETERS_PER_PIXEL);

            if (measurementIsValid)
            {

              // Tell the user what our measurements were.
              if (thicknessShouldBeMeasured)
              {
                AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                                reconstructionRegion,
                                                                jointInspectionData,
                                                                diameterMeasurement,
                                                                thicknessMeasurement);
              }
              else
              {
                AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                                reconstructionRegion,
                                                                jointInspectionData,
                                                                diameterMeasurement);
              }
            }
        }
      }
    }
  }

  /**
   * @author Sunit Bhalla
   */
  private float getEdgeDetectionPercentageAlgorithmSettingValue(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    AlgorithmSettingEnum edgeDetectionPercentageSetting = null;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PAD;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_LOWERPAD;  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_MIDBALL;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PACKAGE;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      edgeDetectionPercentageSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20;
    }
    else
    {
      Assert.expect(false);
    }

    return ((Float)subtype.getAlgorithmSettingValue(edgeDetectionPercentageSetting));

  }

  /**
   * @param subtype
   * @return List<SliceNameEnum>
   * @author Lam
   */
  public List<SliceNameEnum> getInspectionSliceOrder(Subtype subtype)
  {
    Assert.expect(subtype != null);

    InspectionFamily thisFamily= getInspectionFamily();
    List<SliceNameEnum> slicesInOrderOfInspection = (List<SliceNameEnum>)thisFamily.getOrderedInspectionSlices(subtype);
    // DO NOT ADD new slices here. Add slices in InspectionFamily.
    // Remove slices  not required for Algorithm here
    if ( subtype.getJointTypeEnum().equals(JointTypeEnum.CGA) )
    {
      int highshortindex = slicesInOrderOfInspection.indexOf(SliceNameEnum.HIGH_SHORT);
      if ( highshortindex >= 0 )
      {
        slicesInOrderOfInspection.remove(highshortindex);
      }
    }
    
    return slicesInOrderOfInspection ;
  }

   /**
   * @param sliceNameEnum SliceNameEnum
   * @return AlgorithmSettingEnum
   * @author Sunit Bhalla
   */
  private AlgorithmSettingEnum getNominalDiameterAlgorithmSetting(SliceNameEnum sliceNameEnum)
  {
    AlgorithmSettingEnum algorithmSetting = null;

    if (sliceNameEnum.equals(SliceNameEnum.PAD) ||sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) )
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19; 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20; 
    }
    else
    {
      Assert.expect(false);
    }

    Assert.expect(algorithmSetting != null);
    return algorithmSetting;
  }


  /**
   * @author Sunit Bhalla
   */
  private AlgorithmSettingEnum getNominalThicknessAlgorithmSetting(SliceNameEnum sliceNameEnum)
  {
    AlgorithmSettingEnum algorithmSetting = null;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD;  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      algorithmSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20;
    }
    else
    {
      Assert.expect(false);
    }

    Assert.expect(algorithmSetting != null);
    return algorithmSetting;
  }


  /**
   * @author Sunit Bhalla
   * This method returns the nominal thickness value to use in the thickness profiles of the diagnostics.  We want
   * the maximum value of the Y axis to be 150% of the nominal thickness value.  However, we don't always have nominal
   * thickness values for all slices.  For instance, for collapsible BGA's, we only compute the nominal thickness
   * for the midball slice.  So, this thickness is used in computing the maximum Y value for all collapsible BGA slices.
   */
  private float getNominalThicknessForDiagnosticProfileInMils(Subtype subtype)
  {
    Assert.expect(subtype != null);

    JointTypeEnum jointType = subtype.getJointTypeEnum();

    if ((jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointType.equals(JointTypeEnum.CGA) == false) &&
        (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    float returnValueInMM = 0.0f;

    if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      returnValueInMM = getNominalThicknessAlgorithmSettingValue(subtype, SliceNameEnum.MIDBALL);
    }
    if (jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      returnValueInMM = getNominalThicknessAlgorithmSettingValue(subtype, SliceNameEnum.PAD);
    }
    if (jointType.equals(JointTypeEnum.CGA))
    {
      returnValueInMM = getNominalThicknessAlgorithmSettingValue(subtype, SliceNameEnum.PAD);
    }
    if (jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      returnValueInMM = getNominalThicknessAlgorithmSettingValue(subtype, SliceNameEnum.MIDBALL);
    }
    if (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      returnValueInMM = getNominalThicknessAlgorithmSettingValue(subtype, SliceNameEnum.MIDBALL);
    }

    return MathUtil.convertMillimetersToMils(returnValueInMM);
  }

  /**
   * @author Peter Esbensen
   */
  void postMessageThatNoJointWasFound(Algorithm algorithm,
                                      JointInspectionData jointInspectionData,
                                      ReconstructionRegion reconstructionRegion,
                                      ReconstructedSlice reconstructedSlice)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reconstructedSlice != null);

    _diagnostics.postDiagnostics(reconstructionRegion,
                                 reconstructedSlice.getSliceNameEnum(),
                                 jointInspectionData,
                                 this,
                                 false,
                                 new TextualDiagnosticInfo("ALGDIAG_GRIDARRAY_MEASUREMENT_COULD_NOT_LOCATE_JOINT_KEY",
                                                           new Object[]
                                                           {}));
  }

  /**
   * @author Peter Esbensen
   * Siew Yeng - commented this function (not using anymore)
   * This function is separate into two functions below: 
   * - computeRegionOutlierMeasurementsForDiameter(GridArrayOpenAlgorithm) & 
   * - computeRegionOutlierMeasurementsForLocation (GridArrayMisalignmentAlgorithm)
   *//*
  private void computeRegionOutlierMeasurementsForDiameterAndLocation(Collection<JointInspectionData>
                                                                      jointInspectionDataObjects,
                                                                      float[] validDiameters,
                                                                      float[] xOffsetsInMM,
                                                                      float[] yOffsetsInMM,
                                                                      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(validDiameters != null);
    Assert.expect(xOffsetsInMM != null);
    Assert.expect(yOffsetsInMM != null);
    Assert.expect(xOffsetsInMM.length == yOffsetsInMM.length);
    Assert.expect(sliceNameEnum != null);

    float expectedDiameter = 0.0f;
    float expectedXOffsetInMM = 0.0f;
    float expectedYOffsetInMM = 0.0f;

    if (validDiameters.length > 0)
    {
      expectedDiameter = StatisticsUtil.median(validDiameters);
    }

    if (xOffsetsInMM.length > 0)
      expectedXOffsetInMM = StatisticsUtil.median(xOffsetsInMM);

    if (yOffsetsInMM.length > 0)
      expectedYOffsetInMM = StatisticsUtil.median(yOffsetsInMM);

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      float diameter = MathUtil.convertMillimetersToMils(jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER).getValue());

      float regionOutlierPercent = 0.0f;

      if (validDiameters.length > 0)
      {
        regionOutlierPercent = 100.0f * diameter / expectedDiameter;
      }

      if (GridArrayOpenAlgorithm.shouldMinimumRegionOutlierBeTested(jointInspectionData.getJointTypeEnum(),
                                                                    sliceNameEnum))
      {

        jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                  MeasurementEnum.GRID_ARRAY_REGION_OUTLIER,
                                                                  MeasurementUnitsEnum.PERCENT,
                                                                  jointInspectionData.getPad(),
                                                                  sliceNameEnum,
                                                                regionOutlierPercent));
      }

      if (GridArrayMisalignmentAlgorithm.shouldMisalignmentBeTested(jointInspectionData.getJointTypeEnum(), sliceNameEnum))
      {
        float jointOffsetXInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                           MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD).getValue();
        float jointOffsetYInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                           MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD).getValue();

        float differenceFromExpectedXInMM = jointOffsetXInMM - expectedXOffsetInMM;
        float differenceFromExpectedYInMM = jointOffsetYInMM - expectedYOffsetInMM;

        float differenceFromAverageOffsetInMM = (float)Math.sqrt(differenceFromExpectedXInMM * differenceFromExpectedXInMM +
                                                                 differenceFromExpectedYInMM * differenceFromExpectedYInMM);

        jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                  MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION,
                                                                  MeasurementUnitsEnum.MILLIMETERS,
                                                                  jointInspectionData.getPad(),
                                                                  sliceNameEnum,
                                                                  differenceFromAverageOffsetInMM));
      }
    }
  }*/

  /**
   * @author Peter Esbensen
   */
  private float measureThickness(Image image,
                                 ReconstructedSlice reconstructedSlice,
                                 RegionOfInterest jointRegion,
                                 JointInspectionData jointInspectionData,
                                 ReconstructionRegion reconstructionRegion,
								 final float MILLIMETERS_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(jointRegion != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);


    float medianGrayLevelWithinJoint = Statistics.circularMedian(image, jointRegion);

    float backgroundGrayLevelEstimate = measureBackgroundGraylevel(image,
                                                                   reconstructedSlice,
                                                                   jointRegion,
                                                                   jointInspectionData,
                                                                   reconstructionRegion,
																   MILLIMETERS_PER_PIXEL);

    // if the background is darker than the foreground, then just return zero mils thickness
    if (backgroundGrayLevelEstimate < medianGrayLevelWithinJoint)
    {
      return 0.0f;
    }
    if (backgroundGrayLevelEstimate > 255)
    {
      return 255.0f;
    }

    SolderThickness solderThickness = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());

    float deltaGray = Math.max(0.0f, backgroundGrayLevelEstimate - medianGrayLevelWithinJoint);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    return solderThickness.getThicknessInMils(backgroundGrayLevelEstimate, deltaGray, backgroundSensitivityOffset);
  }

  /**
   * @author Peter Esbensen
   */
  private float measureBackgroundGraylevel(Image image,
                                           ReconstructedSlice reconstructedSlice,
                                           RegionOfInterest jointRegion,
                                           JointInspectionData jointInspectionData,
                                           ReconstructionRegion reconstructionRegion,
										   final float MILLIMETERS_PER_PIXEL)
  {
    Assert.expect(image != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(jointRegion != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);

    int nanometersPerPixel = (int)MathUtil.convertMillimetersToNanometers(MILLIMETERS_PER_PIXEL);
    RegionOfInterest innerRegion = new RegionOfInterest(jointRegion);
    innerRegion.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);
    Subtype subtype = jointInspectionData.getSubtype();
    float currentNominalDiameterInMillimeters = getNominalDiameterSettingInMillimeters(reconstructedSlice.
        getSliceNameEnum(), subtype);
    int nominalDiameterInPixels = (int)MathUtil.convertMillimetersToNanometers(currentNominalDiameterInMillimeters) /
                                  nanometersPerPixel;
    int algorithmLimitedInterpadDistanceInPixels = jointInspectionData.getPad().getLandPatternPad().getAlgorithmLimitedInterPadDistanceInNanoMeters() / nanometersPerPixel;

    float innerEdgeAsFractionOfInterPadDistance =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_INNER_EDGE_LOCATION) / 100.0f ;
    float outerEdgeAsFractionOfInterPadDistance =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_OUTER_EDGE_LOCATION) / 100.0f;

    if ((innerEdgeAsFractionOfInterPadDistance >= outerEdgeAsFractionOfInterPadDistance) ||
        (algorithmLimitedInterpadDistanceInPixels < 1))
    {
      // Raise a warning to the user that thickness measurement will be invalid.
      LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
      "ALGDIAG_GRIDARRAY_BACKGROUND_OUTER_EDGE_LOCATION_LESS_THEN_INNER_EDGE_LOCATION_WARNING_KEY",
      new Object[] { subtype.getShortName() });
      AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);

      return 0.0f;
    }
    else
    {
      int innerRegionSizeInPixels =
          nominalDiameterInPixels + (int)Math.ceil(innerEdgeAsFractionOfInterPadDistance * algorithmLimitedInterpadDistanceInPixels) * 2;
      int outerRegionSizeInPixels =
          nominalDiameterInPixels + (int)Math.ceil(outerEdgeAsFractionOfInterPadDistance * algorithmLimitedInterpadDistanceInPixels) * 2;

      innerRegion.setWidthKeepingSameCenter(innerRegionSizeInPixels);
      innerRegion.setHeightKeepingSameCenter(innerRegionSizeInPixels);

      RegionOfInterest outerRegion = new RegionOfInterest(innerRegion);
      outerRegion.setWidthKeepingSameCenter(outerRegionSizeInPixels);
      outerRegion.setHeightKeepingSameCenter(outerRegionSizeInPixels);

      AlgorithmUtil.checkRegionBoundaries(outerRegion, image, reconstructionRegion, "background measurement boundary");

      outerRegion = RegionOfInterest.createRegionFromIntersection(outerRegion, image);
      Assert.expect(outerRegion.getWidth() >= 3);
      Assert.expect(outerRegion.getHeight() >= 3);

      innerRegion = RegionOfInterest.createRegionFromIntersection(innerRegion, outerRegion);
      // This portion is to cater for inner regions.
      if ( innerRegion.getWidth() == outerRegion.getWidth() )
      {
        innerRegion.setRect(innerRegion.getMinX() + 1, innerRegion.getMinY(),
                innerRegion.getWidth() - 2,
                innerRegion.getHeight());
      }
      if ( innerRegion.getHeight() == outerRegion.getHeight() )
      {
        innerRegion.setRect(innerRegion.getMinX(), innerRegion.getMinY() + 1,
                innerRegion.getWidth(),
                innerRegion.getHeight() - 2);
      }

      postBackgroundDiagnosticRegions(image,
                                      reconstructionRegion,
                                      reconstructedSlice.getSliceNameEnum(),
                                      jointInspectionData,
                                      innerRegion,
                                      outerRegion);

      return Statistics.medianAroundRegion(image, outerRegion, innerRegion);
    }
  }

  private float getNominalDiameterSettingInMillimeters(SliceNameEnum sliceNameEnum,
      Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float nominalDiameter = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PAD)||sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20) )
    {
      nominalDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
          GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20);
    }
    else
    {
      Assert.expect(false, "illegal slice in getNominalDiameterSetting");
    }
    return nominalDiameter;
  }

  /**
   * @author Peter Esbensen
   */
  private void postBackgroundDiagnosticRegions(Image image,
                                               ReconstructionRegion reconstructionRegion,
                                               SliceNameEnum sliceNameEnum,
                                               JointInspectionData jointInspectionData,
                                               RegionOfInterest innerRegion,
                                               RegionOfInterest outerRegion)
  {
    Assert.expect(innerRegion != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(image != null);
    Assert.expect(outerRegion != null);

    MeasurementRegionDiagnosticInfo innerRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(
        innerRegion, MeasurementRegionEnum.BACKGROUND_REGION);
    MeasurementRegionDiagnosticInfo outerRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(
        outerRegion, MeasurementRegionEnum.BACKGROUND_REGION);

    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 this, false,
                                 innerRegionDiagnosticInfo, outerRegionDiagnosticInfo);
  }

  /**
   * @author Patrick Lacz
   */
  static float getNominalDiameterAlgorithmSettingValue(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    AlgorithmSettingEnum nominalDiameterSetting = null;
    if (sliceNameEnum.equals(SliceNameEnum.PAD) ||sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      nominalDiameterSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      nominalDiameterSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      nominalDiameterSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE;
    }
    else
    {
      Assert.expect(false);
    }

    return ((Number)subtype.getAlgorithmSettingValue(nominalDiameterSetting)).floatValue();
  }

  /**
   * @author Sunit Bhalla
   */
  static float getNominalThicknessAlgorithmSettingValue(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    AlgorithmSettingEnum nominalThicknessSetting = null;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      nominalThicknessSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      nominalThicknessSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD;  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      nominalThicknessSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL;
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      nominalThicknessSetting = AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE;
    }
    else
    {
      Assert.expect(false);
    }

    return ((Number)subtype.getAlgorithmSettingValue(nominalThicknessSetting)).floatValue();
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getDiameterMeasurement(JointInspectionData jointInspectionData,
                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum,
        MeasurementEnum.GRID_ARRAY_DIAMETER);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getDiameterAsPercentOfNominalMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getThicknessMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_THICKNESS);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getThicknessAsPercentOfNominalMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getDiameterRegionOutlierMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getOffsetFromCADMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getOffsetFromExpectedMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
  }


  /**
   * @author Sunit Bhalla
   * Unloaded boards are not used for GridArray learning.  So, learning should only occur if we have new typical boards.
   */

  protected static boolean algorithmCanBeLearned(Subtype subtype,
                                                 ManagedOfflineImageSet typicalBoardImages,
                                                 ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    if (typicalBoardImages.size() == 0)
      return false;
    else
      return true;
  }


  /**
   * @author Sunit Bhalla
   * This method returns the size of the pad.  While this isn't necessarily a great guess for the ball diameter, it's
   * the best that we can do.
   */
  private float generateInitialGuessForNominalDiameterInMMFromCAD(Subtype subtype,
                                                                  ManagedOfflineImageSet typicalBoardImages,
                                                                  AlgorithmSettingEnum algorithmSetting,
																  final float MILLIMETERS_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(algorithmSetting != null);

    int numberOfDataPoints = 0;
    float totalOfDataPoints = 0.0f;

    for (ReconstructionRegion reconstructionRegion : typicalBoardImages.getReconstructionRegions())
    {
      List<JointInspectionData> jointInspectionDataObjects = reconstructionRegion.getInspectableJointInspectionDataList(subtype);
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        Pad pad = jointInspectionData.getPad();
        int padSizeInNM = (pad.getWidthAfterAllRotationsInNanoMeters() + pad.getLengthAfterAllRotationsInNanoMeters()) / 2;
        int padSizeInPixels = MathUtil.convertNanoMetersToPixelsUsingRound(padSizeInNM,
                                                                           MathUtil.convertMillimetersToNanometers(MILLIMETERS_PER_PIXEL));
        totalOfDataPoints += padSizeInPixels;
        numberOfDataPoints++;
      }
    }

    float diameterInPixels = (Float)subtype.getAlgorithmSettingDefaultValue(algorithmSetting);

    if (numberOfDataPoints > 0)
    {
      diameterInPixels = totalOfDataPoints / numberOfDataPoints;
    }

    float diameterInMM = diameterInPixels * MILLIMETERS_PER_PIXEL;

    return diameterInMM;
  }
  
  /**
   * @author Swee Yee Wong
   * This method returns the size of the pad.  While this isn't necessarily a great guess for the ball diameter, it's
   * the best that we can do.
   */
  public float generateInitialGuessForMidballDiameterInMMFromCAD(Subtype subtype,
                                                                  AlgorithmSettingEnum algorithmSetting) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithmSetting != null);

    int numberOfDataPoints = 0;
    float totalOfDataPoints = 0.0f;
    final float MILLIMETERS_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    List<Pad> Pads = subtype.getPads();
    for (Pad pad : Pads)
    {
      int padSizeInNM = (pad.getWidthAfterAllRotationsInNanoMeters() + pad.getLengthAfterAllRotationsInNanoMeters()) / 2;
      int padSizeInPixels = MathUtil.convertNanoMetersToPixelsUsingRound(padSizeInNM,
        MathUtil.convertMillimetersToNanometers(MILLIMETERS_PER_PIXEL));
      totalOfDataPoints += padSizeInPixels;
      numberOfDataPoints++;
    }

    float diameterInPixels = (Float)subtype.getAlgorithmSettingDefaultValue(algorithmSetting);

    if (numberOfDataPoints > 0)
    {
      diameterInPixels = totalOfDataPoints / numberOfDataPoints;
    }

    float diameterInMM = diameterInPixels * MILLIMETERS_PER_PIXEL;

    return diameterInMM;
  }

  /**
   * @author Sunit Bhalla
   * This method returns sample rate to use when learning.  For example, if the sample rate is 4, we use every forth image.
   */
  private int determineSampleRate(Subtype subtype, ManagedOfflineImageSet typicalBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    int sampleRate = 1;
    int totalJoints = 0;

    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    ReconstructionRegion reconstructionRegion = null;

    while ((reconstructionRegion = imagesIterator.getNextRegionWithoutLoadingImages()) != null)
    {
      int joints = reconstructionRegion.getInspectableJointInspectionDataList(subtype).size();
      totalJoints += joints;
      imagesIterator.finishedWithCurrentRegion();
    }


    if (totalJoints > _MAX_NEW_DATAPOINTS_FROM_LEARNING)
    {
      float sampleRateFloat = (float)totalJoints / (float)_MAX_NEW_DATAPOINTS_FROM_LEARNING;
      sampleRate = Math.round(sampleRateFloat);
    }

    return sampleRate;
  }
  
  /**
   * @author Sunit Bhalla
   * This method returns the lists of diameter and thickness measurements for the subtype.
   */
  private void measureDiameterAndThicknessForLearning(Subtype subtype,
                                                      ManagedOfflineImageSet typicalBoardImages,
                                                      List<LearnedJointMeasurement> padDiameterMeasurements,
                                                      List<LearnedJointMeasurement> padThicknessMeasurements,
                                                      List<LearnedJointMeasurement> padRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> midballDiameterMeasurements,
                                                      List<LearnedJointMeasurement> midballThicknessMeasurements,
                                                      List<LearnedJointMeasurement> midballRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> packageDiameterMeasurements,
                                                      List<LearnedJointMeasurement> packageThicknessMeasurements,
                                                      List<LearnedJointMeasurement> packageRegionOutlierMeasurements) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    Assert.expect(padDiameterMeasurements != null);
    Assert.expect(padThicknessMeasurements != null);
    Assert.expect(padRegionOutlierMeasurements != null);

    Assert.expect(midballDiameterMeasurements != null);
    Assert.expect(midballThicknessMeasurements != null);
    Assert.expect(midballRegionOutlierMeasurements != null);

    Assert.expect(packageDiameterMeasurements != null);
    Assert.expect(packageThicknessMeasurements != null);
    Assert.expect(packageRegionOutlierMeasurements != null);

    int sampleRate = determineSampleRate(subtype, typicalBoardImages);

    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    imagesIterator.setSamplingFrequency(sampleRate);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData>
          jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      // Execute the normal GridArrayMeasurementAlgorithm classification routine to get the measurements.  Note that
      // neasurements are taken only on slices that are inspected for this joint type (via the call to
      // slicesInOrderOfInspection() in classifyJoints).
      classifyJoints(reconstructedImages, jointInspectionDataList);

      Collection<SliceNameEnum> inspectedSlices = reconstructionRegion.getInspectedSliceNames(subtype);

      for (JointInspectionData jointInspectionData : jointInspectionDataList)
      {
        if (inspectedSlices.contains(SliceNameEnum.PAD))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PAD,
                                                       padDiameterMeasurements,
                                                       padThicknessMeasurements,
                                                       padRegionOutlierMeasurements);
        }

        if (inspectedSlices.contains(SliceNameEnum.MIDBALL))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.MIDBALL,
                                                       midballDiameterMeasurements,
                                                       midballThicknessMeasurements,
                                                       midballRegionOutlierMeasurements);
        }
        if (inspectedSlices.contains(SliceNameEnum.PACKAGE))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PACKAGE,
                                                       packageDiameterMeasurements,
                                                       packageThicknessMeasurements,
                                                       packageRegionOutlierMeasurements);
        }
        jointInspectionData.getJointInspectionResult().clearMeasurements();
      }

      // Added by Seng Yew on 22-Apr-2011
      // Need to be careful when call startNewInspectionRun() above.
      // Have to make sure JointMeasurement & ComponentMeasurement instances created before this reset are not used anywhere.
      // JointInspectionResult::addMeasurement(...) & ComponentInspectionResult::addMeasurement(...) checks for duplicate _id when add.
      // After call this function, any new JointMeasurement & ComponentMeasurement instances created will start with zero again, and cause duplicate _id and cause crashes.
      subtype.getPanel().getProject().getTestProgram().startNewInspectionRun(); // Cause all previous test results to be reset (WRONG USAGE PREVIOUSLY)
      for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList))
      {
        componentInspectionData.getComponentInspectionResult().clearMeasurements();
      }

      imagesIterator.finishedWithCurrentRegion();
    }
  }
/*
   *                                                       List<LearnedJointMeasurement> packageRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> userDefinedSlice1DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice1RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2RegionOutlierMeasurementsInMillimeters
   */
  /**
   * @author Sunit Bhalla
   * This method returns the lists of diameter and thickness measurements for the subtype.
   */
  private void measureDiameterAndThicknessForLearning(Subtype subtype,
                                                      ManagedOfflineImageSet typicalBoardImages,
                                                      List<LearnedJointMeasurement> padDiameterMeasurements,
                                                      List<LearnedJointMeasurement> padThicknessMeasurements,
                                                      List<LearnedJointMeasurement> padRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> lowerpadDiameterMeasurements,
                                                      List<LearnedJointMeasurement> lowerpadThicknessMeasurements,
                                                      List<LearnedJointMeasurement> lowerpadRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> midballDiameterMeasurements,
                                                      List<LearnedJointMeasurement> midballThicknessMeasurements,
                                                      List<LearnedJointMeasurement> midballRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> packageDiameterMeasurements,
                                                      List<LearnedJointMeasurement> packageThicknessMeasurements,
                                                      List<LearnedJointMeasurement> packageRegionOutlierMeasurements) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    Assert.expect(padDiameterMeasurements != null);
    Assert.expect(padThicknessMeasurements != null);
    Assert.expect(padRegionOutlierMeasurements != null);
    
    Assert.expect(lowerpadDiameterMeasurements != null);
    Assert.expect(lowerpadThicknessMeasurements != null);
    Assert.expect(lowerpadRegionOutlierMeasurements != null);

    Assert.expect(midballDiameterMeasurements != null);
    Assert.expect(midballThicknessMeasurements != null);
    Assert.expect(midballRegionOutlierMeasurements != null);

    Assert.expect(packageDiameterMeasurements != null);
    Assert.expect(packageThicknessMeasurements != null);
    Assert.expect(packageRegionOutlierMeasurements != null);

    int sampleRate = determineSampleRate(subtype, typicalBoardImages);

    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    imagesIterator.setSamplingFrequency(sampleRate);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData>
          jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      // Execute the normal GridArrayMeasurementAlgorithm classification routine to get the measurements.  Note that
      // neasurements are taken only on slices that are inspected for this joint type (via the call to
      // slicesInOrderOfInspection() in classifyJoints).
      classifyJoints(reconstructedImages, jointInspectionDataList);

      Collection<SliceNameEnum> inspectedSlices = reconstructionRegion.getInspectedSliceNames(subtype);

      for (JointInspectionData jointInspectionData : jointInspectionDataList)
      {
        if (inspectedSlices.contains(SliceNameEnum.PAD))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PAD,
                                                       padDiameterMeasurements,
                                                       padThicknessMeasurements,
                                                       padRegionOutlierMeasurements);
        }
        
        if (inspectedSlices.contains(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
        {
           convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                        SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                        lowerpadDiameterMeasurements,
                                                        lowerpadThicknessMeasurements,
                                                        lowerpadRegionOutlierMeasurements); 
        }

        if (inspectedSlices.contains(SliceNameEnum.MIDBALL))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.MIDBALL,
                                                       midballDiameterMeasurements,
                                                       midballThicknessMeasurements,
                                                       midballRegionOutlierMeasurements);
        }
        if (inspectedSlices.contains(SliceNameEnum.PACKAGE))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PACKAGE,
                                                       packageDiameterMeasurements,
                                                       packageThicknessMeasurements,
                                                       packageRegionOutlierMeasurements);
        }
        jointInspectionData.getJointInspectionResult().clearMeasurements();
      }

      // Added by Seng Yew on 22-Apr-2011
      // Need to be careful when call startNewInspectionRun() above.
      // Have to make sure JointMeasurement & ComponentMeasurement instances created before this reset are not used anywhere.
      // JointInspectionResult::addMeasurement(...) & ComponentInspectionResult::addMeasurement(...) checks for duplicate _id when add.
      // After call this function, any new JointMeasurement & ComponentMeasurement instances created will start with zero again, and cause duplicate _id and cause crashes.
      subtype.getPanel().getProject().getTestProgram().startNewInspectionRun(); // Cause all previous test results to be reset (WRONG USAGE PREVIOUSLY)
      for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList))
      {
        componentInspectionData.getComponentInspectionResult().clearMeasurements();
      }

      imagesIterator.finishedWithCurrentRegion();
    }
  }

   private void measureDiameterAndThicknessForLearning(Subtype subtype,
                                                      ManagedOfflineImageSet typicalBoardImages,
                                                      List<LearnedJointMeasurement> padDiameterMeasurements,
                                                      List<LearnedJointMeasurement> padThicknessMeasurements,
                                                      List<LearnedJointMeasurement> padRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> midballDiameterMeasurements,
                                                      List<LearnedJointMeasurement> midballThicknessMeasurements,
                                                      List<LearnedJointMeasurement> midballRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> packageDiameterMeasurements,
                                                      List<LearnedJointMeasurement> packageThicknessMeasurements,
                                                      List<LearnedJointMeasurement> packageRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> userDefinedSlice1DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice1RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice3DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice3ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice3RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice4DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice4ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice4RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice5DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice5ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice5RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice6DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice6ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice6RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice7DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice7ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice7RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice8DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice8RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice9DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice9ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice9RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice10DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice10ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice10RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice11DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice11ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice11RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice12DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice12ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice12RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice13DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice13ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice13RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice14DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice14ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice14RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice15DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice15ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice15RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice16DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice16ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice16RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice17DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice17ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice17RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice18DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice18ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice18RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice19DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice19ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice19RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice20DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice20ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice20RegionOutlierMeasurementsInMillimeters
                                                      ) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    Assert.expect(padDiameterMeasurements != null);
    Assert.expect(padThicknessMeasurements != null);
    Assert.expect(padRegionOutlierMeasurements != null);

    Assert.expect(midballDiameterMeasurements != null);
    Assert.expect(midballThicknessMeasurements != null);
    Assert.expect(midballRegionOutlierMeasurements != null);

    Assert.expect(packageDiameterMeasurements != null);
    Assert.expect(packageThicknessMeasurements != null);
    Assert.expect(packageRegionOutlierMeasurements != null);
    
    Assert.expect(userDefinedSlice1DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice1ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice1RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice2DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice2ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice2RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice3DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice3ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice3RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice4DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice4ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice4RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice5DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice5ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice5RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice6DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice6ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice6RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice7DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice7ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice7RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice8DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice8ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice8RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice9DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice9ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice9RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice10DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice10ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice10RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice11DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice11ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice11RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice12DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice12ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice12RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice13DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice13ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice13RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice14DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice14ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice14RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice15DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice15ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice15RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice16DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice16ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice16RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice17DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice17ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice17RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice18DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice18ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice18RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice19DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice19ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice19RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice20DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice20ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice20RegionOutlierMeasurementsInMillimeters != null);

    int sampleRate = determineSampleRate(subtype, typicalBoardImages);

    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    imagesIterator.setSamplingFrequency(sampleRate);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData>
          jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      // Execute the normal GridArrayMeasurementAlgorithm classification routine to get the measurements.  Note that
      // neasurements are taken only on slices that are inspected for this joint type (via the call to
      // slicesInOrderOfInspection() in classifyJoints).
      classifyJoints(reconstructedImages, jointInspectionDataList);

      Collection<SliceNameEnum> inspectedSlices = reconstructionRegion.getInspectedSliceNames(subtype);

      for (JointInspectionData jointInspectionData : jointInspectionDataList)
      {
        if (inspectedSlices.contains(SliceNameEnum.PAD))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PAD,
                                                       padDiameterMeasurements,
                                                       padThicknessMeasurements,
                                                       padRegionOutlierMeasurements);
        }

        if (inspectedSlices.contains(SliceNameEnum.MIDBALL))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.MIDBALL,
                                                       midballDiameterMeasurements,
                                                       midballThicknessMeasurements,
                                                       midballRegionOutlierMeasurements);
        }
        if (inspectedSlices.contains(SliceNameEnum.PACKAGE))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PACKAGE,
                                                       packageDiameterMeasurements,
                                                       packageThicknessMeasurements,
                                                       packageRegionOutlierMeasurements);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                       userDefinedSlice1DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice1RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                       userDefinedSlice2DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice2RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                       userDefinedSlice3DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice3ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice3RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                       userDefinedSlice4DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice4ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice4RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                       userDefinedSlice5DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice5ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice5RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                       userDefinedSlice6DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice6ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice6RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                       userDefinedSlice7DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice7ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice7RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                       userDefinedSlice8DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice8RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                       userDefinedSlice8DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice8RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                       userDefinedSlice9DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice9ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice9RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                       userDefinedSlice10DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice10ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice10RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                       userDefinedSlice11DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice11ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice11RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                       userDefinedSlice12DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice12ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice12RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                       userDefinedSlice13DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice13ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice13RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                       userDefinedSlice14DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice14ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice14RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                       userDefinedSlice15DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice15ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice15RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                       userDefinedSlice16DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice16ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice16RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                       userDefinedSlice17DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice17ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice17RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                       userDefinedSlice18DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice18ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice18RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                       userDefinedSlice19DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice19ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice19RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                       userDefinedSlice20DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice20ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice20RegionOutlierMeasurementsInMillimeters);
        }
        jointInspectionData.getJointInspectionResult().clearMeasurements();
      }

      // Added by Seng Yew on 22-Apr-2011
      // Need to be careful when call startNewInspectionRun() above.
      // Have to make sure JointMeasurement & ComponentMeasurement instances created before this reset are not used anywhere.
      // JointInspectionResult::addMeasurement(...) & ComponentInspectionResult::addMeasurement(...) checks for duplicate _id when add.
      // After call this function, any new JointMeasurement & ComponentMeasurement instances created will start with zero again, and cause duplicate _id and cause crashes.
      subtype.getPanel().getProject().getTestProgram().startNewInspectionRun(); // Cause all previous test results to be reset (WRONG USAGE PREVIOUSLY)
      for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList))
      {
        componentInspectionData.getComponentInspectionResult().clearMeasurements();
      }

      imagesIterator.finishedWithCurrentRegion();
    }
  }

  /**
   * @author Sunit Bhalla
   * This method returns the lists of diameter and thickness measurements for the subtype.
   */
  private void measureDiameterAndThicknessForLearning(Subtype subtype,
                                                      ManagedOfflineImageSet typicalBoardImages,
                                                      List<LearnedJointMeasurement> padDiameterMeasurements,
                                                      List<LearnedJointMeasurement> padThicknessMeasurements,
                                                      List<LearnedJointMeasurement> padRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> lowerpadDiameterMeasurements,
                                                      List<LearnedJointMeasurement> lowerpadThicknessMeasurements,
                                                      List<LearnedJointMeasurement> lowerpadRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> midballDiameterMeasurements,
                                                      List<LearnedJointMeasurement> midballThicknessMeasurements,
                                                      List<LearnedJointMeasurement> midballRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> packageDiameterMeasurements,
                                                      List<LearnedJointMeasurement> packageThicknessMeasurements, 
                                                      List<LearnedJointMeasurement> packageRegionOutlierMeasurements,
                                                      List<LearnedJointMeasurement> userDefinedSlice1DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice1RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice2RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice3DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice3ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice3RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice4DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice4ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice4RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice5DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice5ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice5RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice6DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice6ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice6RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice7DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice7ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice7RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice8DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice8RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice9DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice9ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice9RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice10DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice10ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice10RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice11DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice11ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice11RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice12DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice12ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice12RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice13DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice13ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice13RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice14DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice14ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice14RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice15DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice15ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice15RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice16DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice16ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice16RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice17DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice17ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice17RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice18DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice18ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice18RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice19DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice19ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice19RegionOutlierMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice20DiameterMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice20ThicknessMeasurementsInMillimeters,
                                                      List<LearnedJointMeasurement> userDefinedSlice20RegionOutlierMeasurementsInMillimeters
                                                      ) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    Assert.expect(padDiameterMeasurements != null);
    Assert.expect(padThicknessMeasurements != null);
    Assert.expect(padRegionOutlierMeasurements != null);
    
    Assert.expect(lowerpadDiameterMeasurements != null);
    Assert.expect(lowerpadThicknessMeasurements != null);
    Assert.expect(lowerpadRegionOutlierMeasurements != null);

    Assert.expect(midballDiameterMeasurements != null);
    Assert.expect(midballThicknessMeasurements != null);
    Assert.expect(midballRegionOutlierMeasurements != null);

    Assert.expect(packageDiameterMeasurements != null);
    Assert.expect(packageThicknessMeasurements != null);
    Assert.expect(packageRegionOutlierMeasurements != null);
    
    Assert.expect(userDefinedSlice1DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice1ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice1RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice2DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice2ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice2RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice3DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice3ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice3RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice4DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice4ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice4RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice5DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice5ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice5RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice6DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice6ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice6RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice7DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice7ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice7RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice8DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice8ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice8RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice9DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice9ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice9RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice10DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice10ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice10RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice11DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice11ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice11RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice12DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice12ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice12RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice13DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice13ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice13RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice14DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice14ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice14RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice15DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice15ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice15RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice16DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice16ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice16RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice17DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice17ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice17RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice18DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice18ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice18RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice19DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice19ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice19RegionOutlierMeasurementsInMillimeters != null);
    
    Assert.expect(userDefinedSlice20DiameterMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice20ThicknessMeasurementsInMillimeters != null);
    Assert.expect(userDefinedSlice20RegionOutlierMeasurementsInMillimeters != null);

    int sampleRate = determineSampleRate(subtype, typicalBoardImages);

    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    imagesIterator.setSamplingFrequency(sampleRate);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData>
          jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      // Execute the normal GridArrayMeasurementAlgorithm classification routine to get the measurements.  Note that
      // neasurements are taken only on slices that are inspected for this joint type (via the call to
      // slicesInOrderOfInspection() in classifyJoints).
      classifyJoints(reconstructedImages, jointInspectionDataList);

      Collection<SliceNameEnum> inspectedSlices = reconstructionRegion.getInspectedSliceNames(subtype);

      for (JointInspectionData jointInspectionData : jointInspectionDataList)
      {
        if (inspectedSlices.contains(SliceNameEnum.PAD))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PAD,
                                                       padDiameterMeasurements,
                                                       padThicknessMeasurements,
                                                       padRegionOutlierMeasurements);
        }
        
        if (inspectedSlices.contains(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
        {
           convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                        SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                        lowerpadDiameterMeasurements,
                                                        lowerpadThicknessMeasurements,
                                                        lowerpadRegionOutlierMeasurements); 
        }

        if (inspectedSlices.contains(SliceNameEnum.MIDBALL))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.MIDBALL,
                                                       midballDiameterMeasurements,
                                                       midballThicknessMeasurements,
                                                       midballRegionOutlierMeasurements);
        }
        if (inspectedSlices.contains(SliceNameEnum.PACKAGE))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.PACKAGE,
                                                       packageDiameterMeasurements,
                                                       packageThicknessMeasurements,
                                                       packageRegionOutlierMeasurements);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                       userDefinedSlice1DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice1RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                       userDefinedSlice2DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice2RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                       userDefinedSlice3DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice3ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice3RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                       userDefinedSlice4DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice4ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice4RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                       userDefinedSlice5DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice5ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice5RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                       userDefinedSlice6DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice6ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice6RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                       userDefinedSlice7DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice7ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice7RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                       userDefinedSlice8DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice8RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                       userDefinedSlice8DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice8RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                       userDefinedSlice9DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice9ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice9RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                       userDefinedSlice10DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice10ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice10RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                       userDefinedSlice11DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice11ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice11RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                       userDefinedSlice12DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice12ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice12RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                       userDefinedSlice13DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice13ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice13RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                       userDefinedSlice14DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice14ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice14RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                       userDefinedSlice15DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice15ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice15RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                       userDefinedSlice16DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice16ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice16RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                       userDefinedSlice17DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice17ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice17RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                       userDefinedSlice18DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice18ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice18RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                       userDefinedSlice19DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice19ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice19RegionOutlierMeasurementsInMillimeters);
        }
        if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        {
          convertInspectionMeasurementsToLearningLists(jointInspectionData,
                                                       SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                       userDefinedSlice20DiameterMeasurementsInMillimeters,
                                                       userDefinedSlice20ThicknessMeasurementsInMillimeters,
                                                       userDefinedSlice20RegionOutlierMeasurementsInMillimeters);
        }
        jointInspectionData.getJointInspectionResult().clearMeasurements();
        jointInspectionData.clearJointInspectionResult();
      }

      // Added by Seng Yew on 22-Apr-2011
      // Need to be careful when call startNewInspectionRun() above.
      // Have to make sure JointMeasurement & ComponentMeasurement instances created before this reset are not used anywhere.
      // JointInspectionResult::addMeasurement(...) & ComponentInspectionResult::addMeasurement(...) checks for duplicate _id when add.
      // After call this function, any new JointMeasurement & ComponentMeasurement instances created will start with zero again, and cause duplicate _id and cause crashes.
      subtype.getPanel().getProject().getTestProgram().startNewInspectionRun(); // Cause all previous test results to be reset (WRONG USAGE PREVIOUSLY)
      Collection<ComponentInspectionData> componentInspectionDataList = AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList);
      for (ComponentInspectionData componentInspectionData : componentInspectionDataList)
      {
        componentInspectionData.getComponentInspectionResult().clearMeasurements();
        componentInspectionData.clearComponentInspectionResult();
      }

      imagesIterator.finishedWithCurrentRegion();
      
      // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
      if (componentInspectionDataList != null)
      {          
        componentInspectionDataList.clear();
        componentInspectionDataList = null;
      }

      if (jointInspectionDataList != null)
      {          
        jointInspectionDataList.clear();
        jointInspectionDataList = null;
      }
    }
  } 

  /**
   * @author Sunit "Power" Bhalla
   * @author Patrick Lacz
   */
  private void convertInspectionMeasurementsToLearningLists(JointInspectionData jointInspectionData,
                                                            SliceNameEnum sliceNameEnum,
                                                            List<LearnedJointMeasurement> diameterMeasurements,
                                                            List<LearnedJointMeasurement> thicknessMeasurements,
                                                            List<LearnedJointMeasurement> regionOutlierMeasurements)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(diameterMeasurements != null);
    Assert.expect(thicknessMeasurements != null);
    Assert.expect(regionOutlierMeasurements != null);

    JointMeasurement measurement;

    measurement = GridArrayMeasurementAlgorithm.getDiameterMeasurement(jointInspectionData, sliceNameEnum);
    if (measurement.isMeasurementValid())
    {
      diameterMeasurements.add(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                           sliceNameEnum,
                                                           MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                           measurement.getValue(),
                                                           true,
                                                           true));
    }

    if (GridArrayOpenAlgorithm.shouldThicknessBeTested(jointInspectionData.getJointTypeEnum(), sliceNameEnum))
    {
      measurement = GridArrayMeasurementAlgorithm.getThicknessMeasurement(jointInspectionData, sliceNameEnum);
      if (measurement.isMeasurementValid())
      {
        thicknessMeasurements.add(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                              sliceNameEnum,
                                                              MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                              measurement.getValue(),
                                                              true,
                                                              true));
      }
    }

    if(jointInspectionData.getSubtype().getPanel().getProject().isBGAInspectionRegionSetTo1X1() == false)
    {
      if (GridArrayOpenAlgorithm.shouldMinimumRegionOutlierBeTested(jointInspectionData.getJointTypeEnum(), sliceNameEnum))
      {
        measurement = GridArrayMeasurementAlgorithm.getDiameterRegionOutlierMeasurement(jointInspectionData,
                                                                                        sliceNameEnum);
        if (measurement.isMeasurementValid())
        {
          regionOutlierMeasurements.add(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                    sliceNameEnum,
                                                                    MeasurementEnum.GRID_ARRAY_REGION_OUTLIER,
                                                                    measurement.getValue(),
                                                                    true,
                                                                    true));
        }
      }
    }
  }

  /**
   * @author Sunit Bhalla
   * This method returns the new nominal of the measurements.  The stored measurements in the database are
   * retrieved, and then outlier detection is run.
   */
  private float computeNewNominalSettingFromMeasurements(Subtype subtype,
                                                         SliceNameEnum slice,
                                                         MeasurementEnum measurementEnum,
                                                         List<LearnedJointMeasurement> newMeasurements,
                                                         float existingMeasurementsInDatabase[],
                                                         float settingDefault) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(measurementEnum != null);
    Assert.expect(newMeasurements != null);
    Assert.expect(existingMeasurementsInDatabase != null);

    if (newMeasurements.size() + existingMeasurementsInDatabase.length == 0)
    {
      // If no measurements exist, return the default as the new nominal setting
      return settingDefault;
    }

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
    {
      System.out.println("Retrieving " + existingMeasurementsInDatabase.length + " items from the database. ");
    }

    float[] newMeasurementData = LearnedJointMeasurement.convertLearnedJointMeasurementListToFloatArray(newMeasurements);

    float[] allMeasurements = new float[existingMeasurementsInDatabase.length + newMeasurementData.length];
    System.arraycopy(existingMeasurementsInDatabase,
                     0,
                     allMeasurements,
                     0,
                     existingMeasurementsInDatabase.length);
    System.arraycopy(newMeasurementData,
                     0,
                     allMeasurements,
                     existingMeasurementsInDatabase.length,
                     newMeasurementData.length);

    newMeasurementData = null;
    existingMeasurementsInDatabase = null;

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
    {
      System.out.println("Now analyzing " + allMeasurements.length + " items. ");
    }

    //Ngie Xing - XCR-2027 Exclude Outlier
    float median = AlgorithmUtil.medianWithExcludeOutlierDecision(allMeasurements, true);
    return median;
  }

  /**
   * @author Sunit Bhalla
   * This method verifies that learning is done on enough data.  If not, a limited data warning is displayed to the
   * user.
   */
  private void checkForLimitedData(Subtype subtype) throws DatastoreException
  {
    final int MINIMUM_DATAPOINTS_NEEDED = 100;
    Assert.expect(subtype != null);

    SliceNameEnum sliceNameForThisSubtype = getInspectionSliceOrder(subtype).get(0);
    int numberOfMeasurements = numberOfMeasurements(subtype, sliceNameForThisSubtype, MeasurementEnum.GRID_ARRAY_DIAMETER);
    if (numberOfMeasurements < MINIMUM_DATAPOINTS_NEEDED)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfMeasurements);
    }
  }

  /**
   * @author Sunit Bhalla
   * This method verifies that learning is done on enough data.  If not, a limited data warning is displayed to the
   * user.
   */
 public static int numberOfMeasurements(Subtype subtype,
                                        SliceNameEnum slice,
                                        MeasurementEnum measurementEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(measurementEnum != null);

    float[] existingMeasurementsInDatabase = subtype.getLearnedJointMeasurementValues(slice,
                                                                                      measurementEnum,
                                                                                      true,
                                                                                      true);
    return existingMeasurementsInDatabase.length;
  }

  /**
   * @author Sunit Bhalla
   */
  private void initializeNominalDiameters(Subtype subtype, ManagedOfflineImageSet typicalBoardImages, final float MILLIMETERS_PER_PIXEL) throws
      DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    List<SliceNameEnum> slices = new ArrayList<SliceNameEnum>();
    slices.add(SliceNameEnum.PAD);
    slices.add(SliceNameEnum.MIDBALL);
    slices.add(SliceNameEnum.PACKAGE);

    for (SliceNameEnum slice : slices)
    {
      AlgorithmSettingEnum algorithmSetting = getNominalDiameterAlgorithmSetting(slice);

      if (subtype.isAlgorithmSettingValueEqualToDefault(algorithmSetting))
      {
        float initialDiameterEstimateInMM = generateInitialGuessForNominalDiameterInMMFromCAD(subtype,
                                                                                              typicalBoardImages,
                                                                                              algorithmSetting,
																							  MILLIMETERS_PER_PIXEL);
        //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, algorithmSetting, initialDiameterEstimateInMM);
      }
    }
  }

  /**
   * @author Sunit Bhalla
   */
  private int convertFloatMMToIntegerPixels(float value)
  {
    Assert.expect(value >= 0.0);

    return MathUtil.convertMillimetersToPixelsUsingCeil(value, MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel());
  }


  /**
   * This method returns true if all nominal diameters have converged to within one pixel.
   * @author Sunit Bhalla
   */

  private boolean haveAllDiametersConverged(float previousPadDiameterEstimateInMM,
                                            float currentPadDiameterEstimateInMM,
                                            float newPadDiameterEstimateInMM,

                                            float previousMidballDiameterEstimateInMM,
                                            float currentMidballDiameterEstimateInMM,
                                            float newMidballDiameterEstimateInMM,

                                            float previousPackageDiameterEstimateInMM,
                                            float currentPackageDiameterEstimateInMM,
                                            float newPackageDiameterEstimateInMM)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
    {
      System.out.print(" Pad converging: " +
                       convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM));
      System.out.print(" Midball converging: " +
                       convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM));
      System.out.println(" Package converging: " +
                         convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM));
    }

    // Slice is converged if the new guess equals the current guess OR if the new guess equals the previous guess (which
    // means that the diameters are toggling between two numbers.

    boolean padDiameterConverged = ((convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM)) ||
                                    (convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM)));

    boolean midballDiameterConverged = ((convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM)));


    boolean packageDiameterConverged = ((convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM)));

    return (padDiameterConverged && midballDiameterConverged && packageDiameterConverged);
  }
  
  /**
   * This method returns true if all nominal diameters have converged to within one pixel.
   * @author Sunit Bhalla
   */
   private boolean haveAllDiametersConverged(float previousPadDiameterEstimateInMM,
                                            float currentPadDiameterEstimateInMM,
                                            float newPadDiameterEstimateInMM,

                                            float previousMidballDiameterEstimateInMM,
                                            float currentMidballDiameterEstimateInMM,
                                            float newMidballDiameterEstimateInMM,

                                            float previousPackageDiameterEstimateInMM,
                                            float currentPackageDiameterEstimateInMM,
                                            float newPackageDiameterEstimateInMM,
                                            
                                            float previousGridArrayUserDefinedSlice1EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice1DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice2EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice2DiameterInMilimeters)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
    {
      System.out.print(" Pad converging: " +
                       convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM));
      System.out.print(" Midball converging: " +
                       convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM));
      System.out.println(" Package converging: " +
                         convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM));
      System.out.println(" User Define Slice 1 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice1EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice1DiameterInMilimeters));
      System.out.println(" User Define Slice 2 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice2EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice2DiameterInMilimeters));
    }

    // Slice is converged if the new guess equals the current guess OR if the new guess equals the previous guess (which
    // means that the diameters are toggling between two numbers.

    boolean padDiameterConverged = ((convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM)) ||
                                    (convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM)));

    boolean midballDiameterConverged = ((convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM)));


    boolean packageDiameterConverged = ((convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM)));
    
    boolean userDefineSlice1Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice1DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice1EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice1DiameterInMilimeters)));
    
    boolean userDefineSlice2Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice2DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice2EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice2DiameterInMilimeters)));

    return (padDiameterConverged && midballDiameterConverged && packageDiameterConverged && userDefineSlice1Coveraged && userDefineSlice2Coveraged);
  }
   
   /**
   * This method returns true if all nominal diameters have converged to within one pixel.
   * @author Sunit Bhalla
   */
  private boolean haveAllDiametersConverged(float previousPadDiameterEstimateInMM,
                                            float currentPadDiameterEstimateInMM,
                                            float newPadDiameterEstimateInMM,
                                            
                                            float previousLowerPadDiameterEstimateInMM,
                                            float currentLowerPadDiameterEstimateInMM,
                                            float newLowerPadDiameterEstimateInMM,

                                            float previousMidballDiameterEstimateInMM,
                                            float currentMidballDiameterEstimateInMM,
                                            float newMidballDiameterEstimateInMM,

                                            float previousPackageDiameterEstimateInMM,
                                            float currentPackageDiameterEstimateInMM,
                                            float newPackageDiameterEstimateInMM, 
                                            
                                            float previousGridArrayUserDefinedSlice1EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice1DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice2EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice2DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice3EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice3DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice3DiameterInMilimeters, 
                                            
                                            float previousGridArrayUserDefinedSlice4EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice4DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice4DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice5EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice5DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice5DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice6EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice6DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice6DiameterInMilimeters, 
                                            
                                            float previousGridArrayUserDefinedSlice7EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice7DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice7DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice8EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice8DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice8DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice9EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice9DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice9DiameterInMilimeters, 
                                            
                                            float previousGridArrayUserDefinedSlice10EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice10DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice10DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice11EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice11DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice11DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice12EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice12DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice12DiameterInMilimeters, 
                                            
                                            float previousGridArrayUserDefinedSlice13EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice13DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice13DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice14EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice14DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice14DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice15EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice15DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice15DiameterInMilimeters, 
                                            
                                            float previousGridArrayUserDefinedSlice16EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice16DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice16DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice17EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice17DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice17DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice18EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice18DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice18DiameterInMilimeters, 
                                            
                                            float previousGridArrayUserDefinedSlice19EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice19DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice19DiameterInMilimeters,
                                            
                                            float previousGridArrayUserDefinedSlice20EstimateInMillimeters,
                                            float currentGridArrayUserDefinedSlice20DiameterEstimateInMillimeters,
                                            float newGridArrayUserDefinedSlice20DiameterInMilimeters)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
    {
      System.out.print(" Pad converging: " +
                       convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM));
      System.out.print(" Lower Pad converging: " +
                       convertFloatMMToIntegerPixels(previousLowerPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentLowerPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newLowerPadDiameterEstimateInMM));
      System.out.print(" Midball converging: " +
                       convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM));
      System.out.println(" Package converging: " +
                         convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM));
      System.out.println(" User Define Slice 1 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice1EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice1DiameterInMilimeters));
      System.out.println(" User Define Slice 2 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice2EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice2DiameterInMilimeters));
      System.out.println(" User Define Slice 3 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice3EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice3DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice3DiameterInMilimeters));
      System.out.println(" User Define Slice 4 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice4EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice4DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice4DiameterInMilimeters));
      System.out.println(" User Define Slice 5 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice5EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice5DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice5DiameterInMilimeters));
      System.out.println(" User Define Slice 6 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice6EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice6DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice6DiameterInMilimeters));
      System.out.println(" User Define Slice 7 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice7EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice7DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice7DiameterInMilimeters));
      System.out.println(" User Define Slice 8 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice8EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice8DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice8DiameterInMilimeters));
      System.out.println(" User Define Slice 9 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice9EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice9DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice9DiameterInMilimeters));
      System.out.println(" User Define Slice 10 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice10EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice10DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice10DiameterInMilimeters));
      System.out.println(" User Define Slice 11 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice11EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice11DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice11DiameterInMilimeters));
      System.out.println(" User Define Slice 12 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice12EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice12DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice12DiameterInMilimeters));
      System.out.println(" User Define Slice 13 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice13EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice13DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice13DiameterInMilimeters));
      System.out.println(" User Define Slice 14 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice14EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice14DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice14DiameterInMilimeters));
      System.out.println(" User Define Slice 15 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice15EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice15DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice15DiameterInMilimeters));
      System.out.println(" User Define Slice 16 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice16EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice16DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice16DiameterInMilimeters));
      System.out.println(" User Define Slice 17 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice17EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice17DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice17DiameterInMilimeters));
      System.out.println(" User Define Slice 18 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice18EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice18DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice18DiameterInMilimeters));
      System.out.println(" User Define Slice 19 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice19EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice19DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice19DiameterInMilimeters));
      System.out.println(" User Define Slice 20 converging: " +
                         convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice20EstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice20DiameterEstimateInMillimeters) + " " +
                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice20DiameterInMilimeters));
    }

    // Slice is converged if the new guess equals the current guess OR if the new guess equals the previous guess (which
    // means that the diameters are toggling between two numbers.

    boolean padDiameterConverged = ((convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM)) ||
                                    (convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM)));
    
    boolean lowerpadDiameterConverged = ((convertFloatMMToIntegerPixels(newLowerPadDiameterEstimateInMM) ==
                                          convertFloatMMToIntegerPixels(currentLowerPadDiameterEstimateInMM)) ||
                                         (convertFloatMMToIntegerPixels(previousLowerPadDiameterEstimateInMM) ==
                                          convertFloatMMToIntegerPixels(newLowerPadDiameterEstimateInMM)));

    boolean midballDiameterConverged = ((convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM)));


    boolean packageDiameterConverged = ((convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM)));
    
    boolean userDefineSlice1Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice1DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice1EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice1DiameterInMilimeters)));
    
    boolean userDefineSlice2Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice2DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice2EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice2DiameterInMilimeters)));
    
    boolean userDefineSlice3Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice3DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice3DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice3EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice3DiameterInMilimeters)));
    
    boolean userDefineSlice4Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice4DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice4DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice4EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice4DiameterInMilimeters)));
    
    boolean userDefineSlice5Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice5DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice5DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice5EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice5DiameterInMilimeters)));
    
    boolean userDefineSlice6Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice6DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice6DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice6EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice6DiameterInMilimeters)));
    
    boolean userDefineSlice7Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice7DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice7DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice7EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice7DiameterInMilimeters)));
    
    boolean userDefineSlice8Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice8DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice8DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice8EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice8DiameterInMilimeters)));
    
    boolean userDefineSlice9Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice9DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice9DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice9EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice9DiameterInMilimeters)));
    
    boolean userDefineSlice10Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice10DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice10DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice10EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice10DiameterInMilimeters)));
    
    boolean userDefineSlice11Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice11DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice11DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice11EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice11DiameterInMilimeters)));
    
    boolean userDefineSlice12Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice12DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice12DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice12EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice12DiameterInMilimeters)));
    
    boolean userDefineSlice13Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice13DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice13DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice13EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice13DiameterInMilimeters)));
    
    boolean userDefineSlice14Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice14DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice14DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice14EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice14DiameterInMilimeters)));
    
    boolean userDefineSlice15Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice15DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice15DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice15EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice15DiameterInMilimeters)));
    
    boolean userDefineSlice16Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice16DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice16DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice16EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice16DiameterInMilimeters)));
    
    boolean userDefineSlice17Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice17DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice17DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice17EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice17DiameterInMilimeters)));
    
    boolean userDefineSlice18Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice18DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice18DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice18EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice18DiameterInMilimeters)));
    
    boolean userDefineSlice19Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice19DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice19DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice19EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice19DiameterInMilimeters)));
    
    boolean userDefineSlice20Coveraged = ((convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice20DiameterInMilimeters) ==
                                         convertFloatMMToIntegerPixels(currentGridArrayUserDefinedSlice20DiameterEstimateInMillimeters)) ||
                                        (convertFloatMMToIntegerPixels(previousGridArrayUserDefinedSlice20EstimateInMillimeters) ==
                                         convertFloatMMToIntegerPixels(newGridArrayUserDefinedSlice20DiameterInMilimeters)));

    return (padDiameterConverged && lowerpadDiameterConverged && midballDiameterConverged && packageDiameterConverged && 
            userDefineSlice1Coveraged && userDefineSlice2Coveraged && userDefineSlice3Coveraged && userDefineSlice4Coveraged && 
            userDefineSlice5Coveraged && userDefineSlice6Coveraged && userDefineSlice7Coveraged && userDefineSlice8Coveraged && 
            userDefineSlice9Coveraged && userDefineSlice10Coveraged && userDefineSlice11Coveraged && userDefineSlice12Coveraged && 
            userDefineSlice13Coveraged && userDefineSlice14Coveraged && userDefineSlice15Coveraged && userDefineSlice16Coveraged && 
            userDefineSlice17Coveraged && userDefineSlice18Coveraged && userDefineSlice19Coveraged && userDefineSlice20Coveraged);
  }
  
  /**
   * This method returns true if all nominal diameters have converged to within one pixel.
   * @author Sunit Bhalla
   */
  private boolean haveAllDiametersConverged(float previousPadDiameterEstimateInMM,
                                            float currentPadDiameterEstimateInMM,
                                            float newPadDiameterEstimateInMM,
                                            
                                            float previousLowerPadDiameterEstimateInMM,
                                            float currentLowerPadDiameterEstimateInMM,
                                            float newLowerPadDiameterEstimateInMM,

                                            float previousMidballDiameterEstimateInMM,
                                            float currentMidballDiameterEstimateInMM,
                                            float newMidballDiameterEstimateInMM,

                                            float previousPackageDiameterEstimateInMM,
                                            float currentPackageDiameterEstimateInMM,
                                            float newPackageDiameterEstimateInMM)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
    {
      System.out.print(" Pad converging: " +
                       convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM));
      System.out.print(" Lower Pad converging: " +
                       convertFloatMMToIntegerPixels(previousLowerPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentLowerPadDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newLowerPadDiameterEstimateInMM));
      System.out.print(" Midball converging: " +
                       convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM) + " " +
                       convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM));
      System.out.println(" Package converging: " +
                         convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM) + " " +
                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM));
    }

    // Slice is converged if the new guess equals the current guess OR if the new guess equals the previous guess (which
    // means that the diameters are toggling between two numbers.

    boolean padDiameterConverged = ((convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(currentPadDiameterEstimateInMM)) ||
                                    (convertFloatMMToIntegerPixels(previousPadDiameterEstimateInMM) ==
                                     convertFloatMMToIntegerPixels(newPadDiameterEstimateInMM)));
    
    boolean lowerpadDiameterConverged = ((convertFloatMMToIntegerPixels(newLowerPadDiameterEstimateInMM) ==
                                          convertFloatMMToIntegerPixels(currentLowerPadDiameterEstimateInMM)) ||
                                         (convertFloatMMToIntegerPixels(previousLowerPadDiameterEstimateInMM) ==
                                          convertFloatMMToIntegerPixels(newLowerPadDiameterEstimateInMM)));

    boolean midballDiameterConverged = ((convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentMidballDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousMidballDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newMidballDiameterEstimateInMM)));


    boolean packageDiameterConverged = ((convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(currentPackageDiameterEstimateInMM)) ||
                                        (convertFloatMMToIntegerPixels(previousPackageDiameterEstimateInMM) ==
                                         convertFloatMMToIntegerPixels(newPackageDiameterEstimateInMM)));

    return (padDiameterConverged && lowerpadDiameterConverged && midballDiameterConverged && packageDiameterConverged);
  }



  /**
   * @author Sunit Bhalla
   * This method learns the locator and nominal settings for a subtype
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    boolean canBeLearned = GridArrayMeasurementAlgorithm.algorithmCanBeLearned(subtype,
                                                                               typicalBoardImages,
                                                                               unloadedBoardImages);
    if (canBeLearned == false)
    {
      return;
    }
    else
    {
      //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude inside
      learnLocatorSearchWindowSettings(subtype, typicalBoardImages, unloadedBoardImages);
      //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
      learnBackgroundRegionSettings(subtype);
      //Ngie Xing - XCR-2027 Exclude Outlier, Exclude inside
      learnNominals(subtype, typicalBoardImages, unloadedBoardImages, true);
    }

  }

  /**
   * @author Sunit Bhalla
   * This method learns background region settings for a subtype.  These settings are only learned for the
   * Variable Height BGA connector joint type.
   */
  public void learnBackgroundRegionSettings(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    if (subtype.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      final float INNER_BACKGROUND_REGION_FOR_VARIABLE_HEIGHT_BGA_CONNECTOR = 10.0f;
      final float OUTER_BACKGROUND_REGION_FOR_VARIABLE_HEIGHT_BGA_CONNECTOR = 40.0f;
      //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_INNER_EDGE_LOCATION,
                              INNER_BACKGROUND_REGION_FOR_VARIABLE_HEIGHT_BGA_CONNECTOR);
      //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_OUTER_EDGE_LOCATION,
                              OUTER_BACKGROUND_REGION_FOR_VARIABLE_HEIGHT_BGA_CONNECTOR);
    }
  }


  /**
   * @author Sunit Bhalla
   * This method learns Locator Search window across and along settings for a subtype.
   */
  public void learnLocatorSearchWindowSettings(Subtype subtype,
                                   ManagedOfflineImageSet typicalBoardImages,
                                   ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);
    float searchWindowAcross = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS);
    //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
    if (searchWindowAcross > 165.f)
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS, 165.f);
    float searchWindowAlong = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG);
    //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
    if (searchWindowAlong > 165.f)
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG, 165.f);
  }

    /**
     * @author Sunit Bhalla
     * This method learns and stores the Locator effective width and heigth settings for a subtype.
     */
    public void learnLocatorEffectiveWidthAndLengthSettings(Subtype subtype,
                                                            ManagedOfflineImageSet typicalBoardImages,
                                                            ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
    {
      Assert.expect(subtype != null);
      Assert.expect(typicalBoardImages != null);
      Assert.expect(unloadedBoardImages != null);

      // write out effective width/height for the locator code
      if (typicalBoardImages.size() > 0)
      {
      ReconstructionRegion reconstructionRegion = typicalBoardImages.getReconstructionRegions().iterator().next();

      float nominalDiameterInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                                   GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);

      JointInspectionData firstJointInspectionData = reconstructionRegion.getInspectableJointInspectionDataList(subtype).get(0);

      float cadWidthInMillimeters = MathUtil.convertNanometersToMillimeters((float)firstJointInspectionData.getPad().getWidthAfterAllRotationsInNanoMeters());
      float cadLengthInMillimeters = MathUtil.convertNanometersToMillimeters(firstJointInspectionData.getPad().getLengthAfterAllRotationsInNanoMeters());
      //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH, 100.f * nominalDiameterInMillimeters / cadWidthInMillimeters);
      //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH, 100.f * nominalDiameterInMillimeters / cadLengthInMillimeters);
    }
  }


  /**
   * @author Sunit Bhalla
   * This method updates the nominals.
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    boolean canBeLearned = GridArrayMeasurementAlgorithm.algorithmCanBeLearned(subtype,
                                                                               typicalBoardImages,
                                                                               unloadedBoardImages);
    if (canBeLearned == false)
    {
      return;
    }
    else
    {
      // Delete old database for this subtype because the data may be inconsistent with the about-to-be-learned measurements
      subtype.deleteLearnedMeasurementData();

      // Reset nominals to their initial setting.  This makes the method derministic.  If this isn't done,
      // the initial guess would not be the same, and the iteration process would be different.
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);
      
      if (hasLowerPadSlice(subtype))
      {
          subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD);
          subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD);
      }

      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);

      int numberOfUserDefinedSlice = getNumberOfUserDefinedSlice(subtype);
      if(numberOfUserDefinedSlice >= 1)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1);
      }
      if(numberOfUserDefinedSlice >= 2)
      {        
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2);
      }
      if(numberOfUserDefinedSlice >= 3)
      {        
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3);
      }
      if(numberOfUserDefinedSlice >= 4)
      {        
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4);
      }
      if(numberOfUserDefinedSlice >= 5)
      {        
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5);
      }  
      if(numberOfUserDefinedSlice >= 6)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6);
      }     
      if(numberOfUserDefinedSlice >= 7)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7);
      }
      if(numberOfUserDefinedSlice >= 8)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8);
      }
      if(numberOfUserDefinedSlice >= 9)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9);
      }
      if(numberOfUserDefinedSlice >= 10)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10);
      }
      if(numberOfUserDefinedSlice >= 11)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11);
      }
      if(numberOfUserDefinedSlice >= 12)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12);
      }
      if(numberOfUserDefinedSlice >= 13)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13);
      }
      if(numberOfUserDefinedSlice >= 14)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14);
      }
      if(numberOfUserDefinedSlice >= 15)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15);
      }
      if(numberOfUserDefinedSlice >= 16)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16);
      }
      if(numberOfUserDefinedSlice >= 17)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17);
      }
      if(numberOfUserDefinedSlice >= 18)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18);
      }
      if(numberOfUserDefinedSlice >= 19)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19);
      }
      if(numberOfUserDefinedSlice >= 20)
      {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20);
      }
      
      learnNominals(subtype, typicalBoardImages, unloadedBoardImages, false);
    }
  }


  /**
   * @author Sunit Bhalla
   * This method learns the nominal diameters and thicknesses for a subtype.
   *
   * We store measurements for region outlier, although these values are not used until the GridArrayOpen learning.
   */
  public void learnNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages,
                             boolean updateLocatorEffectiveWidthAndLengthSettings) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    List<LearnedJointMeasurement> padDiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> padThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> padRegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> lowerpadDiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> lowerpadThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> lowerpadRegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);

    List<LearnedJointMeasurement> midballDiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> midballThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> midballRegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);

    List<LearnedJointMeasurement> packageDiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> packageThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> packageRegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);

    List<LearnedJointMeasurement> userDefinedSlice1DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice1ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice1RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice2DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice2ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice2RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice3DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice3ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice3RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);

    List<LearnedJointMeasurement> userDefinedSlice4DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice4ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice4RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice5DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice5ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice5RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice6DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice6ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice6RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice7DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice7ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice7RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice8DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice8ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice8RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice9DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice9ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice9RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice10DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice10ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice10RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice11DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice11ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice11RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice12DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice12ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice12RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice13DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice13ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice13RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice14DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice14ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice14RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice15DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice15ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice15RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice16DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice16ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice16RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice17DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice17ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice17RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice18DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice18ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice18RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice19DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice19ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice19RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    List<LearnedJointMeasurement> userDefinedSlice20DiameterMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice20ThicknessMeasurementsInMillimeters = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    List<LearnedJointMeasurement> userDefinedSlice20RegionOutlierMeasurements = new ArrayList<LearnedJointMeasurement>(_MAX_NEW_DATAPOINTS_FROM_LEARNING);
    
    float padDiameterValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float padThicknessValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    
    float lowerpadDiameterValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float lowerpadThicknessValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);

    float midballDiameterValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float midballThicknessValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);

    float packageDiameterValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float packageThicknessValuesInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);

    float gridArrayUserDefinedSlice1DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice2DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice3DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice4DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice5DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice6DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice7DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice8DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice9DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice10DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice11DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice12DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice13DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice14DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice15DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice16DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice17DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice18DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice19DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    float gridArrayUserDefinedSlice20DiameterInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20, MeasurementEnum.GRID_ARRAY_DIAMETER, true, true);
    
    float gridArrayUserDefinedSlice1ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice2ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice3ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice4ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice5ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice6ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice7ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice8ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice9ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice10ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice11ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice12ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice13ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice14ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice15ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice16ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice17ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice18ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice19ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    float gridArrayUserDefinedSlice20ThicknessInDatabase[] = subtype.getLearnedJointMeasurementValues(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20, MeasurementEnum.GRID_ARRAY_THICKNESS, true, true);
    
    float defaultPadDiameterEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    float defaultPadThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);
    
    float defaultLowerPadDiameterEstimateInMillimeters = 0.0f;
    float defaultLowerPadThicknessEstimateInMillimeters = 0.0f;
    float currentLowerPadDiameterEstimateInMillimeters = 0.0f;
    float newLowerPadDiameterEstimateInMillimeters = 0.0f;
    float newLowerPadThicknessEstimateInMillimeters = 0.0f;
    float newGridArrayUserDefinedSlice1DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice1ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice2DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice2ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice3DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice3ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice4DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice4ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice5DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice5ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice6DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice6ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice7DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice7ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice8DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice8ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice9DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice9ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice10DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice10ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice11DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice11ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice12DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice12ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice13DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice13ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice14DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice14ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice15DiameterInMilimeters = 0.0f; 
    float newGridArrayUserDefinedSlice15ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice16DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice16ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice17DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice17ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice18DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice18ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice19DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice19ThicknessInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice20DiameterInMilimeters = 0.0f;
    float newGridArrayUserDefinedSlice20ThicknessInMilimeters = 0.0f;
    
    float defaultUserDefinedSlice1DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice1ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice2DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice2ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice3DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice3ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice4DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice4ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice5DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice5ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice6DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice6ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice7DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice7ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice8DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice8ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice9DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice9ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice10DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice10ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice11DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice11ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice12DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice12ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice13DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice13ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice14DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice14ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice15DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice15ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice16DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice16ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice17DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice17ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice18DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice18ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice19DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice19ThicknessEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice20DiameterEstimateInMillimeters = 0.0f;
    float defaultUserDefinedSlice20ThicknessEstimateInMillimeters = 0.0f;
    
    float currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice3DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice4DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice5DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice6DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice7DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice8DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice9DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice10DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice11DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice12DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice13DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice14DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice15DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice16DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice17DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice18DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice19DiameterEstimateInMillimeters = 0.0f;
    float currentGridArrayUserDefinedSlice20DiameterEstimateInMillimeters = 0.0f;
    
    boolean hasLowerPadSlice = hasLowerPadSlice(subtype);
    
    if (hasLowerPadSlice)
    {
        defaultLowerPadDiameterEstimateInMillimeters =
            (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD);
        defaultLowerPadThicknessEstimateInMillimeters =
            (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD);
        
        currentLowerPadDiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD);
    }

    float defaultMidballDiameterEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    float defaultMidballThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);


    float defaultPackageDiameterEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    float defaultPackageThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);

    int numberOfUserDefinedSlice = getNumberOfUserDefinedSlice(subtype);
    if(numberOfUserDefinedSlice >= 1)
    {
      defaultUserDefinedSlice1DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1);
      defaultUserDefinedSlice1ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1);
    }
    if(numberOfUserDefinedSlice >= 2)
    {
      defaultUserDefinedSlice2DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2);
      defaultUserDefinedSlice2ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2);
    }
    if(numberOfUserDefinedSlice >= 3)
    {     
      defaultUserDefinedSlice3DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3);
      defaultUserDefinedSlice3ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3);
    }
    if(numberOfUserDefinedSlice >= 4)
    {
      defaultUserDefinedSlice4DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4);
      defaultUserDefinedSlice4ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4);
    }
    if(numberOfUserDefinedSlice >= 5)
    {
      defaultUserDefinedSlice5DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5);
      defaultUserDefinedSlice5ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5);
    }
    if(numberOfUserDefinedSlice >= 6)
    {
      defaultUserDefinedSlice6DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6);
      defaultUserDefinedSlice6ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6);
    }
    if(numberOfUserDefinedSlice >= 7)
    {
      defaultUserDefinedSlice7DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7);
      defaultUserDefinedSlice7ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7);
    }
    if(numberOfUserDefinedSlice >= 8)
    {
      defaultUserDefinedSlice8DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8);
      defaultUserDefinedSlice8ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8);
    }
    if(numberOfUserDefinedSlice >= 9)
    {
      defaultUserDefinedSlice9DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9);
      defaultUserDefinedSlice9ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9);
    }
    if(numberOfUserDefinedSlice >= 10)
    {
      defaultUserDefinedSlice10DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10);
      defaultUserDefinedSlice10ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10);
    }
    if(numberOfUserDefinedSlice >= 11)
    {
      defaultUserDefinedSlice11DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11);
      defaultUserDefinedSlice11ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11);
    }
    if(numberOfUserDefinedSlice >= 12)
    {
      defaultUserDefinedSlice12DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12);
      defaultUserDefinedSlice12ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12);
    }
    if(numberOfUserDefinedSlice >= 13)
    {
      defaultUserDefinedSlice13DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13);
      defaultUserDefinedSlice13ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13);
    }
    if(numberOfUserDefinedSlice >= 14)
    {
      defaultUserDefinedSlice14DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14);
      defaultUserDefinedSlice14ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14);
    }
    if(numberOfUserDefinedSlice >= 15)
    {
      defaultUserDefinedSlice15DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15);
      defaultUserDefinedSlice15ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15);
    }
    if(numberOfUserDefinedSlice >= 16)
    {
      defaultUserDefinedSlice16DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16);
      defaultUserDefinedSlice16ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16);
    }
    if(numberOfUserDefinedSlice >= 17)
    {
      defaultUserDefinedSlice17DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17);
      defaultUserDefinedSlice17ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17);
    }
    if(numberOfUserDefinedSlice >= 18)
    {
      defaultUserDefinedSlice18DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18);
      defaultUserDefinedSlice18ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18);
    }
    if(numberOfUserDefinedSlice >= 19)
    {
      defaultUserDefinedSlice19DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19);
      defaultUserDefinedSlice19ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19);
    }
    if(numberOfUserDefinedSlice >= 20)
    {
      defaultUserDefinedSlice20DiameterEstimateInMillimeters = 
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20);
      defaultUserDefinedSlice20ThicknessEstimateInMillimeters =
        (Float)subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20);
    }
    
    float previousPadDiameterEstimateInMillimeters = 0.0f;
    float previousLowerPadDiameterEstimateInMillimeters = 0.0f;
    float previousMidballDiameterEstimateInMillimeters = 0.0f;
    float previousPackageDiameterEstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice1EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice2EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice3EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice4EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice5EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice6EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice7EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice8EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice9EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice10EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice11EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice12EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice13EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice14EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice15EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice16EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice17EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice18EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice19EstimateInMillimeters = 0.0f;
    float previousGridArrayUserDefinedSlice20EstimateInMillimeters = 0.0f;

	final float MILLIMETERS_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
	
    // If the nominal diameters are still set to their defaults, use the CAD to create an estimate for the diameter.
    //Ngie Xing - XCR-2027 Exclude Outlier, No exclude inside
    initializeNominalDiameters(subtype, typicalBoardImages, MILLIMETERS_PER_PIXEL);

    // Set Locator Effective Width and Length settings based on new diameter guess if we're not only updating nominals
    if (updateLocatorEffectiveWidthAndLengthSettings)
      //Ngie Xing - XCR-2027 - Exclude Outlier, No exclude inside
      learnLocatorEffectiveWidthAndLengthSettings(subtype, typicalBoardImages, unloadedBoardImages);

    // Iterate until change is within DIAMETER_MEASUREMENT_PRECISION of if the number of iterations is
    // greater than MAX_NUMBER_ITERATIONS

    final int MAX_NUMBER_ITERATIONS = 10;
    int numberOfIterations = 0;
    boolean haveConvergedOnSettings = false;
    while (numberOfIterations < MAX_NUMBER_ITERATIONS && haveConvergedOnSettings == false)
    {
      // Clear out measurement lists
      padDiameterMeasurementsInMillimeters.clear();
      padThicknessMeasurementsInMillimeters.clear();
      padRegionOutlierMeasurements.clear();
      
      if (hasLowerPadSlice)
      {
        lowerpadDiameterMeasurementsInMillimeters.clear();
        lowerpadThicknessMeasurementsInMillimeters.clear();
        lowerpadRegionOutlierMeasurements.clear();
      }
      
      midballDiameterMeasurementsInMillimeters.clear();
      midballThicknessMeasurementsInMillimeters.clear();
      midballRegionOutlierMeasurements.clear();
      packageDiameterMeasurementsInMillimeters.clear();
      packageThicknessMeasurementsInMillimeters.clear();
      packageRegionOutlierMeasurements.clear();

     if(numberOfUserDefinedSlice >= 1)
     {
        userDefinedSlice1DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice1ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice1RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1);
     }
     if(numberOfUserDefinedSlice >= 2)
     {
        userDefinedSlice2DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice2ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice2RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2);
     }
     if(numberOfUserDefinedSlice >= 3)
     {
        userDefinedSlice3DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice3ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice3RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice3DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3);
     }
     if(numberOfUserDefinedSlice >= 4)
     {
        userDefinedSlice4DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice4ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice4RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice4DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4);
     }
     if(numberOfUserDefinedSlice >= 5)
     {
        userDefinedSlice5DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice5ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice5RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice5DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5);
     }
     if(numberOfUserDefinedSlice >= 6)
     {
        userDefinedSlice6DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice6ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice6RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice6DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6);
     }
     if(numberOfUserDefinedSlice >= 7)
     {
        userDefinedSlice7DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice7ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice7RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice7DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7);
     }
     if(numberOfUserDefinedSlice >= 8)
     {
        userDefinedSlice8DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice8ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice8RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice8DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8);
     }
     if(numberOfUserDefinedSlice >= 9)
     {
        userDefinedSlice9DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice9ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice9RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice9DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9);
     }
     if(numberOfUserDefinedSlice >= 10)
     {
        userDefinedSlice10DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice10ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice10RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice10DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10);
     }
     if(numberOfUserDefinedSlice >= 11)
     {
        userDefinedSlice11DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice11ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice11RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice11DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11);
     }
     if(numberOfUserDefinedSlice >= 12)
     {
        userDefinedSlice12DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice12ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice12RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice12DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12);
     }
     if(numberOfUserDefinedSlice >= 13)
     {
        userDefinedSlice13DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice13ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice13RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice13DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13);
     }
     if(numberOfUserDefinedSlice >= 14)
     {
        userDefinedSlice14DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice14ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice14RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice14DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14); 
     }
     if(numberOfUserDefinedSlice >= 15)
     {  
        userDefinedSlice15DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice15ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice15RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice15DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15); 
     }
     if(numberOfUserDefinedSlice >= 16)
     {
        userDefinedSlice16DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice16ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice16RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice16DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16); 
     }
     if(numberOfUserDefinedSlice >= 17)
     {
        userDefinedSlice17DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice17ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice17RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice17DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17);
     }
     if(numberOfUserDefinedSlice >= 18)
     {
        userDefinedSlice18DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice18ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice18RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice18DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18);
     }
     if(numberOfUserDefinedSlice >= 19)
     {
        userDefinedSlice19DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice19ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice19RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice19DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19);
     }
     if(numberOfUserDefinedSlice >= 20)
     {
        userDefinedSlice20DiameterMeasurementsInMillimeters.clear();
        userDefinedSlice20ThicknessMeasurementsInMillimeters.clear();
        userDefinedSlice20RegionOutlierMeasurements.clear();
        
        currentGridArrayUserDefinedSlice20DiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20);
     }

      float currentPadDiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
      float currentMidballDiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
      float currentPackageDiameterEstimateInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);

      if (hasLowerPadSlice)
      {
        if(numberOfUserDefinedSlice > 0)
           measureDiameterAndThicknessForLearning(subtype,
                                               typicalBoardImages,
                                               padDiameterMeasurementsInMillimeters,
                                               padThicknessMeasurementsInMillimeters,
                                               padRegionOutlierMeasurements,
                                               lowerpadDiameterMeasurementsInMillimeters,
                                               lowerpadThicknessMeasurementsInMillimeters,
                                               lowerpadRegionOutlierMeasurements,
                                               midballDiameterMeasurementsInMillimeters,
                                               midballThicknessMeasurementsInMillimeters,
                                               midballRegionOutlierMeasurements,
                                               packageDiameterMeasurementsInMillimeters,
                                               packageThicknessMeasurementsInMillimeters,
                                               packageRegionOutlierMeasurements,
                                               userDefinedSlice1DiameterMeasurementsInMillimeters,
                                               userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice1RegionOutlierMeasurements,
                                               userDefinedSlice2DiameterMeasurementsInMillimeters,
                                               userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice2RegionOutlierMeasurements,
                                               userDefinedSlice3DiameterMeasurementsInMillimeters,
                                               userDefinedSlice3ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice3RegionOutlierMeasurements,
                                               userDefinedSlice4DiameterMeasurementsInMillimeters,
                                               userDefinedSlice4ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice4RegionOutlierMeasurements,
                                               userDefinedSlice5DiameterMeasurementsInMillimeters,
                                               userDefinedSlice5ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice5RegionOutlierMeasurements,
                                               userDefinedSlice6DiameterMeasurementsInMillimeters,
                                               userDefinedSlice6ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice6RegionOutlierMeasurements,
                                               userDefinedSlice7DiameterMeasurementsInMillimeters,
                                               userDefinedSlice7ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice7RegionOutlierMeasurements,
                                               userDefinedSlice8DiameterMeasurementsInMillimeters,
                                               userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice8RegionOutlierMeasurements,
                                               userDefinedSlice9DiameterMeasurementsInMillimeters,
                                               userDefinedSlice9ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice9RegionOutlierMeasurements,
                                               userDefinedSlice10DiameterMeasurementsInMillimeters,
                                               userDefinedSlice10ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice10RegionOutlierMeasurements,
                                               userDefinedSlice11DiameterMeasurementsInMillimeters,
                                               userDefinedSlice11ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice11RegionOutlierMeasurements,
                                               userDefinedSlice12DiameterMeasurementsInMillimeters,
                                               userDefinedSlice12ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice12RegionOutlierMeasurements,
                                               userDefinedSlice13DiameterMeasurementsInMillimeters,
                                               userDefinedSlice13ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice13RegionOutlierMeasurements,
                                               userDefinedSlice14DiameterMeasurementsInMillimeters,
                                               userDefinedSlice14ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice14RegionOutlierMeasurements,
                                               userDefinedSlice15DiameterMeasurementsInMillimeters,
                                               userDefinedSlice15ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice15RegionOutlierMeasurements,
                                               userDefinedSlice16DiameterMeasurementsInMillimeters,
                                               userDefinedSlice16ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice16RegionOutlierMeasurements,
                                               userDefinedSlice17DiameterMeasurementsInMillimeters,
                                               userDefinedSlice17ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice17RegionOutlierMeasurements,
                                               userDefinedSlice18DiameterMeasurementsInMillimeters,
                                               userDefinedSlice18ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice18RegionOutlierMeasurements,
                                               userDefinedSlice19DiameterMeasurementsInMillimeters,
                                               userDefinedSlice19ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice19RegionOutlierMeasurements,
                                               userDefinedSlice20DiameterMeasurementsInMillimeters,
                                               userDefinedSlice20ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice20RegionOutlierMeasurements); 
        else
         measureDiameterAndThicknessForLearning(subtype,
                                             typicalBoardImages,
                                             padDiameterMeasurementsInMillimeters,
                                             padThicknessMeasurementsInMillimeters,
                                             padRegionOutlierMeasurements,
                                             lowerpadDiameterMeasurementsInMillimeters,
                                             lowerpadThicknessMeasurementsInMillimeters,
                                             lowerpadRegionOutlierMeasurements,
                                             midballDiameterMeasurementsInMillimeters,
                                             midballThicknessMeasurementsInMillimeters,
                                             midballRegionOutlierMeasurements,
                                             packageDiameterMeasurementsInMillimeters,
                                             packageThicknessMeasurementsInMillimeters,
                                             packageRegionOutlierMeasurements); 
      }
      else
      {
        if(numberOfUserDefinedSlice > 0)
          measureDiameterAndThicknessForLearning(subtype,
                                             typicalBoardImages,
                                             padDiameterMeasurementsInMillimeters,
                                             padThicknessMeasurementsInMillimeters,
                                             padRegionOutlierMeasurements,
                                             midballDiameterMeasurementsInMillimeters,
                                             midballThicknessMeasurementsInMillimeters,
                                             midballRegionOutlierMeasurements,
                                             packageDiameterMeasurementsInMillimeters,
                                             packageThicknessMeasurementsInMillimeters,
                                             packageRegionOutlierMeasurements,
                                             userDefinedSlice1DiameterMeasurementsInMillimeters,
                                             userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                             userDefinedSlice1RegionOutlierMeasurements,
                                             userDefinedSlice2DiameterMeasurementsInMillimeters,
                                             userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                             userDefinedSlice2RegionOutlierMeasurements,
                                             userDefinedSlice3DiameterMeasurementsInMillimeters,
                                             userDefinedSlice3ThicknessMeasurementsInMillimeters,
                                             userDefinedSlice3RegionOutlierMeasurements,
                                             userDefinedSlice4DiameterMeasurementsInMillimeters,
                                               userDefinedSlice4ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice4RegionOutlierMeasurements,
                                               userDefinedSlice5DiameterMeasurementsInMillimeters,
                                               userDefinedSlice5ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice5RegionOutlierMeasurements,
                                               userDefinedSlice6DiameterMeasurementsInMillimeters,
                                               userDefinedSlice6ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice6RegionOutlierMeasurements,
                                               userDefinedSlice7DiameterMeasurementsInMillimeters,
                                               userDefinedSlice7ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice7RegionOutlierMeasurements,
                                               userDefinedSlice8DiameterMeasurementsInMillimeters,
                                               userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice8RegionOutlierMeasurements,
                                               userDefinedSlice9DiameterMeasurementsInMillimeters,
                                               userDefinedSlice9ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice9RegionOutlierMeasurements,
                                               userDefinedSlice10DiameterMeasurementsInMillimeters,
                                               userDefinedSlice10ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice10RegionOutlierMeasurements,
                                               userDefinedSlice11DiameterMeasurementsInMillimeters,
                                               userDefinedSlice11ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice11RegionOutlierMeasurements,
                                               userDefinedSlice12DiameterMeasurementsInMillimeters,
                                               userDefinedSlice12ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice12RegionOutlierMeasurements,
                                               userDefinedSlice13DiameterMeasurementsInMillimeters,
                                               userDefinedSlice13ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice13RegionOutlierMeasurements,
                                               userDefinedSlice14DiameterMeasurementsInMillimeters,
                                               userDefinedSlice14ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice14RegionOutlierMeasurements,
                                               userDefinedSlice15DiameterMeasurementsInMillimeters,
                                               userDefinedSlice15ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice15RegionOutlierMeasurements,
                                               userDefinedSlice16DiameterMeasurementsInMillimeters,
                                               userDefinedSlice16ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice16RegionOutlierMeasurements,
                                               userDefinedSlice17DiameterMeasurementsInMillimeters,
                                               userDefinedSlice17ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice17RegionOutlierMeasurements,
                                               userDefinedSlice18DiameterMeasurementsInMillimeters,
                                               userDefinedSlice18ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice18RegionOutlierMeasurements,
                                               userDefinedSlice19DiameterMeasurementsInMillimeters,
                                               userDefinedSlice19ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice19RegionOutlierMeasurements,
                                               userDefinedSlice20DiameterMeasurementsInMillimeters,
                                               userDefinedSlice20ThicknessMeasurementsInMillimeters,
                                               userDefinedSlice20RegionOutlierMeasurements);
        else
          measureDiameterAndThicknessForLearning(subtype,
                                             typicalBoardImages,
                                             padDiameterMeasurementsInMillimeters,
                                             padThicknessMeasurementsInMillimeters,
                                             padRegionOutlierMeasurements,
                                             midballDiameterMeasurementsInMillimeters,
                                             midballThicknessMeasurementsInMillimeters,
                                             midballRegionOutlierMeasurements,
                                             packageDiameterMeasurementsInMillimeters,
                                             packageThicknessMeasurementsInMillimeters,
                                             packageRegionOutlierMeasurements);
      }

      float newPadDiameterEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                           SliceNameEnum.PAD,
                                                                                           MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                           padDiameterMeasurementsInMillimeters,
                                                                                           padDiameterValuesInDatabase,
                                                                                           defaultPadDiameterEstimateInMillimeters);

      float newPadThicknessEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                            SliceNameEnum.PAD,
                                                                                            MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                            padThicknessMeasurementsInMillimeters,
                                                                                            padThicknessValuesInDatabase,
                                                                                            defaultPadThicknessEstimateInMillimeters);
      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
      {
        System.out.println("Pad current guess: " + currentPadDiameterEstimateInMillimeters +
                           " New Guess: " + newPadDiameterEstimateInMillimeters);
      }
      
      // Handle lower pad slice
      if (hasLowerPadSlice)
      {
          newLowerPadDiameterEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                              SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                                              MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                              lowerpadDiameterMeasurementsInMillimeters,
                                                                                              lowerpadDiameterValuesInDatabase,
                                                                                              defaultLowerPadDiameterEstimateInMillimeters);
          
          newLowerPadThicknessEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                                               MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                               lowerpadThicknessMeasurementsInMillimeters,
                                                                                               lowerpadThicknessValuesInDatabase,
                                                                                               defaultLowerPadThicknessEstimateInMillimeters);
          
          //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
          if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
          {
            System.out.println("Lower Pad current guess: " + currentLowerPadDiameterEstimateInMillimeters +
                               " New Guess: " + newLowerPadDiameterEstimateInMillimeters);
          }
      }

      float newMidballDiameterEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.MIDBALL,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               midballDiameterMeasurementsInMillimeters,
                                                                                               midballDiameterValuesInDatabase,
                                                                                               defaultMidballDiameterEstimateInMillimeters);
      float newMidballThicknessEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.MIDBALL,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                midballThicknessMeasurementsInMillimeters,
                                                                                                midballThicknessValuesInDatabase,
                                                                                                defaultMidballThicknessEstimateInMillimeters);
      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
      {
        System.out.println("Midball current guess: " + currentMidballDiameterEstimateInMillimeters +
                           " New Guess: " + newMidballDiameterEstimateInMillimeters);
      }

      float newPackageDiameterEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.PACKAGE,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               packageDiameterMeasurementsInMillimeters,
                                                                                               packageDiameterValuesInDatabase,
                                                                                               defaultPackageDiameterEstimateInMillimeters);
      float newPackageThicknessEstimateInMillimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.PACKAGE,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                packageThicknessMeasurementsInMillimeters,
                                                                                                packageThicknessValuesInDatabase,
                                                                                                defaultPackageThicknessEstimateInMillimeters);
      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
      {
        System.out.println("Package current guess: " + currentPackageDiameterEstimateInMillimeters +
                           " New Guess: " + newPackageDiameterEstimateInMillimeters);
      }

      if(numberOfUserDefinedSlice >= 1)
      {
        newGridArrayUserDefinedSlice1DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice1DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice1DiameterInDatabase,
                                                                                               defaultUserDefinedSlice1DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice1ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice1ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice1ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice1ThicknessEstimateInMillimeters);
      }
      if(numberOfUserDefinedSlice >= 2)
      {
        newGridArrayUserDefinedSlice2DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice2DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice2DiameterInDatabase,
                                                                                               defaultUserDefinedSlice2DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice2ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice2ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice2ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice2ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 3)
      {
        newGridArrayUserDefinedSlice3DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice3DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice3DiameterInDatabase,
                                                                                               defaultUserDefinedSlice3DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice3ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice3ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice3ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice3ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 4)
      {
        newGridArrayUserDefinedSlice4DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice4DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice4DiameterInDatabase,
                                                                                               defaultUserDefinedSlice4DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice4ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice4ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice4ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice4ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 5)
      {
        newGridArrayUserDefinedSlice5DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice5DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice5DiameterInDatabase,
                                                                                               defaultUserDefinedSlice5DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice5ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice5ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice5ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice5ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 6)
      {
        newGridArrayUserDefinedSlice6DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice6DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice6DiameterInDatabase,
                                                                                               defaultUserDefinedSlice6DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice6ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice6ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice6ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice6ThicknessEstimateInMillimeters);
      }
      if(numberOfUserDefinedSlice >= 7)
      {
        newGridArrayUserDefinedSlice7DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice7DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice7DiameterInDatabase,
                                                                                               defaultUserDefinedSlice7DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice7ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice7ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice7ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice7ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 8)
      {
        newGridArrayUserDefinedSlice8DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice8DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice8DiameterInDatabase,
                                                                                               defaultUserDefinedSlice8DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice8ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice8ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice8ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice8ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 9)
      {
        newGridArrayUserDefinedSlice9DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice9DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice9DiameterInDatabase,
                                                                                               defaultUserDefinedSlice9DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice9ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice9ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice9ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice9ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 10)
      {
        newGridArrayUserDefinedSlice10DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice10DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice10DiameterInDatabase,
                                                                                               defaultUserDefinedSlice10DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice10ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice10ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice10ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice10ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 11)
      {
        newGridArrayUserDefinedSlice11DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice11DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice11DiameterInDatabase,
                                                                                               defaultUserDefinedSlice11DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice11ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice11ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice11ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice11ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 12)
      {
        newGridArrayUserDefinedSlice12DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice12DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice12DiameterInDatabase,
                                                                                               defaultUserDefinedSlice12DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice12ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice12ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice12ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice12ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 13)
      {
        newGridArrayUserDefinedSlice13DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice13DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice13DiameterInDatabase,
                                                                                               defaultUserDefinedSlice13DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice13ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice13ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice13ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice13ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 14)
      {
        newGridArrayUserDefinedSlice14DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice14DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice14DiameterInDatabase,
                                                                                               defaultUserDefinedSlice14DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice14ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice14ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice14ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice14ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 15)
      {
        newGridArrayUserDefinedSlice15DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice15DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice15DiameterInDatabase,
                                                                                               defaultUserDefinedSlice15DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice15ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice15ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice15ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice15ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 16)
      {
        newGridArrayUserDefinedSlice16DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice16DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice16DiameterInDatabase,
                                                                                               defaultUserDefinedSlice16DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice16ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice16ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice16ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice16ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 17)
      {
        newGridArrayUserDefinedSlice17DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice17DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice17DiameterInDatabase,
                                                                                               defaultUserDefinedSlice17DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice17ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice17ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice17ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice17ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 18)
      {
        newGridArrayUserDefinedSlice18DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice18DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice18DiameterInDatabase,
                                                                                               defaultUserDefinedSlice18DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice18ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice18ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice18ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice18ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 19)
      {
        newGridArrayUserDefinedSlice19DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice19DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice19DiameterInDatabase,
                                                                                               defaultUserDefinedSlice19DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice19ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice19ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice19ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice19ThicknessEstimateInMillimeters);
      }
      if (numberOfUserDefinedSlice >= 20)
      {
        newGridArrayUserDefinedSlice20DiameterInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                               SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                                               MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                                                               userDefinedSlice20DiameterMeasurementsInMillimeters,
                                                                                               gridArrayUserDefinedSlice20DiameterInDatabase,
                                                                                               defaultUserDefinedSlice20DiameterEstimateInMillimeters);
        newGridArrayUserDefinedSlice20ThicknessInMilimeters = computeNewNominalSettingFromMeasurements(subtype,
                                                                                                SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                                                MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                                                                userDefinedSlice20ThicknessMeasurementsInMillimeters,
                                                                                                gridArrayUserDefinedSlice20ThicknessInDatabase,
                                                                                                defaultUserDefinedSlice20ThicknessEstimateInMillimeters);
      }

      // Set nominal diameters setting to be the new nominal estimate
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD,
                              newPadDiameterEstimateInMillimeters);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD,
                              newPadThicknessEstimateInMillimeters);
      
      if (hasLowerPadSlice)
      {
          subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD,
                              newLowerPadDiameterEstimateInMillimeters);
          subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD,
                                  newLowerPadThicknessEstimateInMillimeters);
      }

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL,
                              newMidballDiameterEstimateInMillimeters);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL,
                              newMidballThicknessEstimateInMillimeters);

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE,
                              newPackageDiameterEstimateInMillimeters);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE,
                              newPackageThicknessEstimateInMillimeters);

      if(numberOfUserDefinedSlice >= 1)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1,
                                newGridArrayUserDefinedSlice1DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1,
                                newGridArrayUserDefinedSlice1ThicknessInMilimeters);
      }      
      if(numberOfUserDefinedSlice >= 2)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2,
                                newGridArrayUserDefinedSlice2DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2,
                                newGridArrayUserDefinedSlice2ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 3)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3,
                                newGridArrayUserDefinedSlice3DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3,
                                newGridArrayUserDefinedSlice3ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 4)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4,
                                newGridArrayUserDefinedSlice4DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4,
                                newGridArrayUserDefinedSlice4ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 5)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5,
                                newGridArrayUserDefinedSlice5DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5,
                                newGridArrayUserDefinedSlice5ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 6)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6,
                                newGridArrayUserDefinedSlice6DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6,
                                newGridArrayUserDefinedSlice6ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 7)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7,
                                newGridArrayUserDefinedSlice7DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7,
                                newGridArrayUserDefinedSlice7ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 8)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8,
                                newGridArrayUserDefinedSlice8DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8,
                                newGridArrayUserDefinedSlice8ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 9)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9,
                                newGridArrayUserDefinedSlice9DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9,
                                newGridArrayUserDefinedSlice9ThicknessInMilimeters);
      } 
      if(numberOfUserDefinedSlice >= 10)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10,
                                newGridArrayUserDefinedSlice10DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10,
                                newGridArrayUserDefinedSlice10ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 11)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11,
                                newGridArrayUserDefinedSlice11DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11,
                                newGridArrayUserDefinedSlice11ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 12)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12,
                                newGridArrayUserDefinedSlice12DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12,
                                newGridArrayUserDefinedSlice12ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 13)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13,
                                newGridArrayUserDefinedSlice13DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13,
                                newGridArrayUserDefinedSlice13ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 14)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14,
                                newGridArrayUserDefinedSlice14DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14,
                                newGridArrayUserDefinedSlice14ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 15)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15,
                                newGridArrayUserDefinedSlice15DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15,
                                newGridArrayUserDefinedSlice15ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 16)
      { 
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16,
                                newGridArrayUserDefinedSlice16DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16,
                                newGridArrayUserDefinedSlice16ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 17)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17,
                                newGridArrayUserDefinedSlice17DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17,
                                newGridArrayUserDefinedSlice17ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 18)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18,
                                newGridArrayUserDefinedSlice18DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18,
                                newGridArrayUserDefinedSlice18ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 19)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19,
                                newGridArrayUserDefinedSlice19DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19,
                                newGridArrayUserDefinedSlice19ThicknessInMilimeters);
      }
      if(numberOfUserDefinedSlice >= 20)
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20,
                                newGridArrayUserDefinedSlice20DiameterInMilimeters);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20,
                                newGridArrayUserDefinedSlice20ThicknessInMilimeters);
      }

      // Set Locator Effective Width and Length settings based on new diameter guess (but only if we're not updating only nominals).
      if (updateLocatorEffectiveWidthAndLengthSettings)
        learnLocatorEffectiveWidthAndLengthSettings(subtype, typicalBoardImages, unloadedBoardImages);

      numberOfIterations++;

      // Stop iterating if all diameters have converged
      if (hasLowerPadSlice)
      {
        if(numberOfUserDefinedSlice > 0)
          haveConvergedOnSettings = haveAllDiametersConverged(previousPadDiameterEstimateInMillimeters,
                                                          currentPadDiameterEstimateInMillimeters,
                                                          newPadDiameterEstimateInMillimeters,
                                                          previousLowerPadDiameterEstimateInMillimeters,
                                                          currentLowerPadDiameterEstimateInMillimeters,
                                                          newLowerPadDiameterEstimateInMillimeters,
                                                          previousMidballDiameterEstimateInMillimeters,
                                                          currentMidballDiameterEstimateInMillimeters,
                                                          newMidballDiameterEstimateInMillimeters,
                                                          previousPackageDiameterEstimateInMillimeters,
                                                          currentPackageDiameterEstimateInMillimeters,
                                                          newPackageDiameterEstimateInMillimeters,
                                                          previousGridArrayUserDefinedSlice1EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice1ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice2EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice2ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice3EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice3DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice3ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice4EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice4DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice4ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice5EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice5DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice5ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice6EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice6DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice6ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice7EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice7DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice7ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice8EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice8DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice8ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice9EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice9DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice9ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice10EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice10DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice10ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice11EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice11DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice11ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice12EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice12DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice12ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice13EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice13DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice13ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice14EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice14DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice14ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice15EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice15DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice15ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice16EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice16DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice16ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice17EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice17DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice17ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice18EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice18DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice18ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice19EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice19DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice19ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice20EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice20DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice20ThicknessInMilimeters);
        else
          haveConvergedOnSettings = haveAllDiametersConverged(previousPadDiameterEstimateInMillimeters,
                                                          currentPadDiameterEstimateInMillimeters,
                                                          newPadDiameterEstimateInMillimeters,
                                                          previousLowerPadDiameterEstimateInMillimeters,
                                                          currentLowerPadDiameterEstimateInMillimeters,
                                                          newLowerPadDiameterEstimateInMillimeters,
                                                          previousMidballDiameterEstimateInMillimeters,
                                                          currentMidballDiameterEstimateInMillimeters,
                                                          newMidballDiameterEstimateInMillimeters,
                                                          previousPackageDiameterEstimateInMillimeters,
                                                          currentPackageDiameterEstimateInMillimeters,
                                                          newPackageDiameterEstimateInMillimeters);
          
          previousLowerPadDiameterEstimateInMillimeters = currentLowerPadDiameterEstimateInMillimeters;
      }
      else
      {          
        if(numberOfUserDefinedSlice > 0)
          haveConvergedOnSettings = haveAllDiametersConverged(previousPadDiameterEstimateInMillimeters,
                                                          currentPadDiameterEstimateInMillimeters,
                                                          newPadDiameterEstimateInMillimeters,
                                                          previousLowerPadDiameterEstimateInMillimeters,
                                                          currentLowerPadDiameterEstimateInMillimeters,
                                                          newLowerPadDiameterEstimateInMillimeters,
                                                          previousMidballDiameterEstimateInMillimeters,
                                                          currentMidballDiameterEstimateInMillimeters,
                                                          newMidballDiameterEstimateInMillimeters,
                                                          previousPackageDiameterEstimateInMillimeters,
                                                          currentPackageDiameterEstimateInMillimeters,
                                                          newPackageDiameterEstimateInMillimeters,
                                                          previousGridArrayUserDefinedSlice1EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice1ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice2EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice2ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice3EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice3DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice3ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice4EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice4DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice4ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice5EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice5DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice5ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice6EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice6DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice6ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice7EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice7DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice7ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice8EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice8DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice8ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice9EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice9DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice9ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice10EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice10DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice10ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice11EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice11DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice11ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice12EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice12DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice12ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice13EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice13DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice13ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice14EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice14DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice14ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice15EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice15DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice15ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice16EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice16DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice16ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice17EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice17DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice17ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice18EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice18DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice18ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice19EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice19DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice19ThicknessInMilimeters,
                                                          previousGridArrayUserDefinedSlice20EstimateInMillimeters,
                                                          currentGridArrayUserDefinedSlice20DiameterEstimateInMillimeters,
                                                          newGridArrayUserDefinedSlice20ThicknessInMilimeters);
      
        else
          haveConvergedOnSettings = haveAllDiametersConverged(previousPadDiameterEstimateInMillimeters,
                                                          currentPadDiameterEstimateInMillimeters,
                                                          newPadDiameterEstimateInMillimeters,
                                                          previousMidballDiameterEstimateInMillimeters,
                                                          currentMidballDiameterEstimateInMillimeters,
                                                          newMidballDiameterEstimateInMillimeters,
                                                          previousPackageDiameterEstimateInMillimeters,
                                                          currentPackageDiameterEstimateInMillimeters,
                                                          newPackageDiameterEstimateInMillimeters);
      }
      

      // These will be used on the next iteration (used to check if the value is toggling between two numbers)
      previousPadDiameterEstimateInMillimeters = currentPadDiameterEstimateInMillimeters;
      previousMidballDiameterEstimateInMillimeters = currentMidballDiameterEstimateInMillimeters;
      previousPackageDiameterEstimateInMillimeters = currentPackageDiameterEstimateInMillimeters;
      previousGridArrayUserDefinedSlice1EstimateInMillimeters = currentGridArrayUserDefinedSlice1DiameterEstimateInMillimeters;
      previousGridArrayUserDefinedSlice2EstimateInMillimeters = currentGridArrayUserDefinedSlice2DiameterEstimateInMillimeters;
    }

    subtype.addLearnedJointMeasurementData(padDiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(padThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(padRegionOutlierMeasurements);
    
    if (hasLowerPadSlice)
    {
       subtype.addLearnedJointMeasurementData(lowerpadDiameterMeasurementsInMillimeters);
       subtype.addLearnedJointMeasurementData(lowerpadThicknessMeasurementsInMillimeters);
       subtype.addLearnedJointMeasurementData(lowerpadRegionOutlierMeasurements); 
    }

    subtype.addLearnedJointMeasurementData(midballDiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(midballThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(midballRegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(packageDiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(packageThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(packageRegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice1DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice1ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice1RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice2DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice2ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice2RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice3DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice3ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice3RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice4DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice4ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice4RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice5DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice5ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice5RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice6DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice6ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice6RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice7DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice7ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice7RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice8DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice8ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice8RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice9DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice9ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice9RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice10DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice10ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice10RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice11DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice11ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice11RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice12DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice12ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice12RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice13DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice13ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice13RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice14DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice14ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice14RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice15DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice15ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice15RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice16DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice16ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice16RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice17DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice17ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice17RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice18DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice18ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice18RegionOutlierMeasurements);

    subtype.addLearnedJointMeasurementData(userDefinedSlice19DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice19ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice19RegionOutlierMeasurements);
    
    subtype.addLearnedJointMeasurementData(userDefinedSlice20DiameterMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice20ThicknessMeasurementsInMillimeters);
    subtype.addLearnedJointMeasurementData(userDefinedSlice20RegionOutlierMeasurements);

    checkForLimitedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   * This method deletes all learned data and sets all learned settings back to their defaults.
   */

  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Locator.setLearnedSettingsToDefaults(subtype);

    // These are locator settings, but the Grid Array learning routine sets them.
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);

    if (hasLowerPadSlice(subtype))
    {
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD);
        subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD);
    }
    
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);
  }

  /**
   * @author Patrick Lacz
   * @author Peter Esbensen
   */
  private float measureEccentricity(ReconstructedSlice reconstructedSlice,
                                    ReconstructedImages reconstructedImages,
                                    JointInspectionData jointInspectionData,
                                    RegionOfInterest jointRoi,
                                    Subtype subtype,
                                    float diameterInMils,
									final float MILLIMETERS_PER_PIXEL)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointRoi != null);
    Assert.expect(subtype != null);

    Image sliceImage = reconstructedSlice.getOrthogonalImage();

    // Set up the joint region so that in includes the entire measured region.
    // It would be nice here to have the x vs. y diameter measurements.. maybe sometime in the future.

    // convert to pixels
    float diameterInPixels = MathUtil.convertMilsToMillimeters(diameterInMils) / MILLIMETERS_PER_PIXEL;
    final float ECCENTRICITY_ROI_EXPANSION_FACTOR = 1.3f;
    int intDiameterInPixels = (int)Math.round(Math.ceil(diameterInPixels) * ECCENTRICITY_ROI_EXPANSION_FACTOR);

    // modify a copy of the joint ROI (do not modify the original one otherwise it will mess up code in classifyJoints())
    RegionOfInterest eccentricityRoi = new RegionOfInterest(jointRoi);
    eccentricityRoi.setWidthKeepingSameCenter(intDiameterInPixels);
    eccentricityRoi.setHeightKeepingSameCenter(intDiameterInPixels);
    eccentricityRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    // truncate to fit in image
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(sliceImage, eccentricityRoi);

    // post the eccentricity measurement region diagnostics
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 this,
                                 false,
                                 new MeasurementRegionDiagnosticInfo(eccentricityRoi,
                                                                     MeasurementRegionEnum.ECCENTRICITY_REGION));

    Image jointImageCopy = Image.createCopy(sliceImage, eccentricityRoi);

    // analyze the histogram to automatically determine a good binarization threshold
    int numHistogramBins = 256;
    int[] histogram = Threshold.histogram(sliceImage, eccentricityRoi, numHistogramBins);
    float threshold = Threshold.getAutoThreshold(histogram);

    Threshold.threshold(jointImageCopy, threshold, 255f, threshold, 0f);

    // post the thresholded image
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
    {
      postThresholdedEccentricityDiags(reconstructionRegion,
                                       sliceNameEnum,
                                       jointInspectionData,
                                       jointImageCopy,
                                       eccentricityRoi);
    }


    Image eccentricityImage = jointImageCopy;

    String useFixedEccentricityAxisSettingString = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_USE_FIXED_ECCENTRICITY_AXIS);
    boolean useFixedEccentricityAxis = useFixedEccentricityAxisSettingString.contains(_ON);

    int userSpecifiedEccentricityAxis = Math.round(((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_FIXED_ECCENTRICITY_AXIS)));
    if (useFixedEccentricityAxis)
    {
      // compute the eccentricity along the user-specified axis.  Do this by rotating the image by the user-specified
      // amount first and then just computing the ratio of the second moments along each cardinal axis

      // now rotate the region to so that the specified axis runs along the x axis

      AffineTransform rotation = AffineTransform.getRotateInstance( Math.toRadians(userSpecifiedEccentricityAxis) );
      Image rotatedImage = Transform.applyAffineTransform(rotation, jointImageCopy, 0.0f);

      // the rotated image will probably be bigger since it is sized to include the post-rotated region, so let's fix that
      // figure out a crop region
      /** @todo PE pull this resizing into shared routine */
      RegionOfInterest desiredROI = RegionOfInterest.createRegionFromImage(jointImageCopy);
      int increaseInSizeX = rotatedImage.getWidth() - jointImageCopy.getWidth();
      Assert.expect(increaseInSizeX >= 0);
      int increaseInSizeY = rotatedImage.getHeight() - jointImageCopy.getHeight();
      Assert.expect(increaseInSizeY >= 0);
      desiredROI.translateXY(increaseInSizeX / 2, increaseInSizeY / 2);
      // now we can crop the rotated image back to the original size
      Image croppedRotatedImage = Image.createCopy(rotatedImage, desiredROI);
      rotatedImage.decrementReferenceCount();

      eccentricityImage = croppedRotatedImage;

      // display the major axis in the diags
      /** @todo PE modify overlay diagnostics to use different colors */
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
      {
        postMajorAxisDiags(jointImageCopy,
                           userSpecifiedEccentricityAxis,
                           reconstructionRegion,
                           sliceNameEnum,
                           jointInspectionData,
                           eccentricityRoi);
      }

      jointImageCopy.decrementReferenceCount();
    }

    // compute the moments
    Map<IntCoordinate, Double> mapOfMoments = Statistics.computeNormalizedCentralMoments(eccentricityImage, RegionOfInterest.createRegionFromImage(eccentricityImage), 2);
    double u20 = mapOfMoments.get(new IntCoordinate(2, 0));
    double u11 = mapOfMoments.get(new IntCoordinate(1, 1));
    double u02 = mapOfMoments.get(new IntCoordinate(0, 2));

    // notify the eccentricityImage that we are done with it
    eccentricityImage.decrementReferenceCount();

    double eccentricity = 0.0;
    if (useFixedEccentricityAxis)
    {
      // the eccentricity image should already be rotated so that the user specified orientation has been rotated to match
      // the cardinal axes

      eccentricity = u20 / u02;
    }
    else
    {
      // compute eccentricity in a rotation-invariant manner

      // the value under the sqrt is always positive, so we don't worry about imaginary roots
      double sqrtTerm = Math.sqrt((u20 - u02) * (u20 - u02) + 4.0 * u11 * u11);
      double denominator = u20 + u02 - sqrtTerm;
      if (denominator < 0.000001)
        return 0.f;

      //double eccentricity = (u20 + u02 + sqrtTerm) / (u20 + u02 - sqrtTerm);
      // an alternative equation for eccentricity that seems to work with a more understandable range
      // double eccentricity = sqrtTerm / (u20 + u02);
      eccentricity = (u20 + u02 + sqrtTerm) / (u20 + u02 - sqrtTerm);
    }

    return (float)eccentricity;
  }

  /**
   * @author Peter Esbensen
   */
  private void postThresholdedEccentricityDiags(ReconstructionRegion reconstructionRegion,
                                                SliceNameEnum sliceNameEnum,
                                                JointInspectionData jointInspectionData,
                                                Image thresholdedImage,
                                                RegionOfInterest eccentricityRoi)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(thresholdedImage != null);
    Assert.expect(eccentricityRoi != null);

    // erode the thresholded image
    Image erodedThresholdedImage = Filter.erode(thresholdedImage);

    // get the difference between the original and the eroded image -- that's the outline that we'll display
    Image perimetersOfThresholdedImage = Arithmetic.subtractImages(thresholdedImage, erodedThresholdedImage);

    erodedThresholdedImage.decrementReferenceCount();

    // some values will be less than zero --- just force them to zero
    Threshold.clamp(perimetersOfThresholdedImage, 0.0f, 255f);

    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 this,
                                 false,
                                 new OverlayImageDiagnosticInfo(perimetersOfThresholdedImage,
                                                                eccentricityRoi,
                                                                StringLocalizer.keyToString("ALGDIAG_GRIDARRAY_ECCENTRICITY_PERIMETER_KEY")));

    perimetersOfThresholdedImage.decrementReferenceCount();
  }

  /**
   * @author Peter Esbensen
   */
  private void postMajorAxisDiags(Image jointImage,
                                  int userSpecifiedEccentricityAxis,
                                  ReconstructionRegion reconstructionRegion,
                                  SliceNameEnum sliceNameEnum,
                                  JointInspectionData jointInspectionData,
                                  RegionOfInterest jointRoi)
  {
    Assert.expect(jointImage != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    // display the major axis in the diags
    Image majorAxisImage = jointImage;
    // set all pixels to zero except a line along x axis
    Paint.fillImage(majorAxisImage, 0.0f);
    RegionOfInterest majorAxisLine = RegionOfInterest.createRegionFromImage(jointImage);
    majorAxisLine.setLengthAcross(2); /** @todo fix this */
    Paint.fillRegionOfInterest(majorAxisImage, majorAxisLine, 255f);
    AffineTransform rotation = AffineTransform.getRotateInstance( -1 * Math.toRadians(userSpecifiedEccentricityAxis));
    Image rotatedMajorAxisImage = Transform.applyAffineTransform(rotation, jointImage, 0.0f);
    // the rotated image will probably be bigger since it is sized to include the post-rotated region, so let's fix that
    // figure out a crop region
    /** @todo PE pull this resizing into shared utility */
    RegionOfInterest desiredROI = RegionOfInterest.createRegionFromImage(jointImage);
    int increaseInSizeX = rotatedMajorAxisImage.getWidth() - jointImage.getWidth();
    Assert.expect(increaseInSizeX >= 0);
    int increaseInSizeY = rotatedMajorAxisImage.getHeight() - jointImage.getHeight();
    Assert.expect(increaseInSizeY >= 0);
    desiredROI.translateXY(increaseInSizeX / 2, increaseInSizeY / 2);
    // now we can crop the rotated image back to the original size
    Image croppedRotatedMajorAxisImage = Image.createCopy(rotatedMajorAxisImage, desiredROI);
    rotatedMajorAxisImage.decrementReferenceCount();

    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 this,
                                 false,
                                 new OverlayImageDiagnosticInfo(croppedRotatedMajorAxisImage,
                                                                jointRoi,
                                                                StringLocalizer.keyToString("ALGDIAG_GRIDARRAY_ECCENTRICITY_MAJOR_AXIS_KEY")));

    croppedRotatedMajorAxisImage.decrementReferenceCount();
  }

  /**
   * @author Peter Esbensen
   */
  static JointMeasurement getEccentricity(JointInspectionData jointInspectionData,
                                          SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
  }

  /**
   * "On", Off"
   * @author George Booth
   */
  public static boolean isContrastEnhancementEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String contrastEnhancementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CONTRAST_ENHANCEMENT);

    if (contrastEnhancementSetting.equals("On"))
    {
      return true;
    }
    else if (contrastEnhancementSetting.equals("Off"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Contrast Enhancement setting: " + contrastEnhancementSetting);
      return false;
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean hasLowerPadSlice(Subtype subtype)
  {
      Assert.expect(subtype != null);
      
      if (subtype.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
          return true;
      else
          return false;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private static int getNumberOfUserDefinedSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE);

    return Integer.parseInt(numberOfSlice);
  }
  
  /**
   * @author Swee Yee Wong
   * This method will learn and update the algorithm setting if necessary during program generation
   */
  public void learnAndUpdateAlgorithmSettingIfNecessaryDuringProgramGeneration(Subtype subtype,
                                                                  AlgorithmSettingEnum algSettingEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(algSettingEnum != null);

    if(algSettingEnum.equals(AlgorithmSettingEnum.USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT))
    {
      float initialGuessForMidballDiameter = 0.0f;
      try
      {
        initialGuessForMidballDiameter = generateInitialGuessForMidballDiameterInMMFromCAD(subtype, algSettingEnum);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, algSettingEnum, initialGuessForMidballDiameter/2);
      }
      catch(DatastoreException e)
      {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * @author Siew yeng
   */
  public static boolean isJointBasedEdgeDetectionEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String isJointBasedEdgeDetectionEnabled = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_ENABLE_JOINT_BASED_EDGE_DETECTION);
    
    return isJointBasedEdgeDetectionEnabled.equals(_TRUE);
  }
  
  /**
   * @author Siew Yeng
   */
  public static boolean hasDiameterMeasurement(JointInspectionData jointInspectionData,
                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum,
        MeasurementEnum.GRID_ARRAY_DIAMETER);
  }
}
