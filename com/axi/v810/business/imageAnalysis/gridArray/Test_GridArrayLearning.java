package com.axi.v810.business.imageAnalysis.gridArray;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.util.*;

/**
 * @author Sunit Bhalla
 */
public class Test_GridArrayLearning extends AlgorithmUnitTest implements Observer
{

  private final String _BGA_SUBTYPE_1 = "bg00001_CollapsibleBGA";
  private final String REFDES_TO_TEST = "U27";
  private final String PAD_TO_TEST = "230";

  private final float _DIAMETER_OF_BGAS_MIDBALL_IN_MILS = 0.3958f;
  private final float _DIAMETER_OF_BGAS_PAD_AND_PACKAGE_IN_MILS = 0.3738f;

  private final float _DIAMETER_OF_BGAS_FOR_INCREMENTAL_LEARNING_IN_MILS = 0.3958f;
  private final float _FUZZY_THRESHOLD_FOR_DIAMETER_IN_MILS = 0.001f;

  private final float _THICKNESS_OF_BGAS_MIDBALL_IN_MILS = 0.1068f;
  private final float _THICKNESS_OF_BGAS_PAD_AND_PACKAGE_IN_MILS = 0.109f;
  private final float _FUZZY_THRESHOLD_FOR_THICKNESS_IN_MILS = 0.002f;

  private List<AlgorithmSettingEnum> _collapsibleBGALearnedSettings = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _nonCollapsibleBGALearnedSettings = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _CSPLearnedSettings = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _CGALearnedSettings = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _NexLevLearnedSettings = new ArrayList<AlgorithmSettingEnum>();

  private List<AlgorithmSettingEnum> _collapsibleBGASettingsNotLearned = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _nonCollapsibleBGASettingsNotLearned = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _CSPSettingsNotLearned = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _CGASettingsNotLearned = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _NexLevSettingsNotLearned = new ArrayList<AlgorithmSettingEnum>();

  private List<AlgorithmSettingEnum> _learnedCollapsibleBGADiameterSettings = new ArrayList<AlgorithmSettingEnum>();
  private List<AlgorithmSettingEnum> _learnedCollapsibleBGAThicknessSettings = new ArrayList<AlgorithmSettingEnum>();

  /**
   * @author Sunit Bhalla
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_GridArrayLearning());
  }

  /**
   * @author Sunit Bhalla
   */
  public void test(BufferedReader is, PrintWriter os)
  {

    TimerUtil wholeTestTimer = new TimerUtil();
    wholeTestTimer.start();

    TimerUtil timer = new TimerUtil();

    boolean printDebugInformation = false;

    setCollapsibleBGANominalSettingsLists();
    setGridArrayLearnedSettings();

    try
    {

      timer.start();
      Project project =  getProject("FAMILIES_ALL_RLV");
      timer.stop();
      if (printDebugInformation)
        System.out.println("The project created: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      deleteAllLearnedData(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("deleteOldDatabaseData done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testLearnSubtype(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testLearnSubtype done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testCollapsibleBGA();
      timer.stop();
      if (printDebugInformation)
        System.out.println("testCollapsibleBGA done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testCGA();
      timer.stop();
      if (printDebugInformation)
        System.out.println("testCGA done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testCSP();
      timer.stop();
      if (printDebugInformation)
        System.out.println("testCSP done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testNonCollapsibleBGA();
      timer.stop();
      if (printDebugInformation)
        System.out.println("testNonCollapsibleBGA done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testNexLev();
      timer.stop();
      if (printDebugInformation)
        System.out.println("testNexLev done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      wholeTestTimer.stop();

      if (printDebugInformation)
        System.out.println("Time for test (min): " + wholeTestTimer.getElapsedTimeInMillis() / 60000.0);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }


  /**
   * @author Sunit Bhalla
   */

  protected void setCollapsibleBGANominalSettingsLists()
  {
    _learnedCollapsibleBGADiameterSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    _learnedCollapsibleBGADiameterSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    _learnedCollapsibleBGADiameterSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);

    _learnedCollapsibleBGAThicknessSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);
  }

  /**
   * @author Sunit Bhalla
   * This defines which settings should be learned and not learned.
   */
  protected void setGridArrayLearnedSettings()
  {
    setCollapsibleBGALearnedSettings();
    setNonCollapsbileBGALearnedSettings();
    setCGALearnedSettings();
    setCSPLearnedSettings();
    setNexLevLearnedSettings();
  }

  /**
   * @author Sunit Bhalla
   * This sets up the lists of which settings are (and are not) learned for Collapsible BGA's
   */
  protected void setCollapsibleBGALearnedSettings()
  {
    // Measurement learned Settings
    _collapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);
    _collapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    _collapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);
    _collapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);

    // Open learned settings
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);

    // Misalignment settings
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED);
    _collapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION);

    // Insufficient settings
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE);
    _collapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL);
  }


  /**
   * @author Sunit Bhalla
   * This sets up the lists of which settings are (and are not) learned for CSP joints
   */
  protected void setCSPLearnedSettings()
  {
    // Measurement learned Settings
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);
    _CSPLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    _CSPLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);

    // Open learned settings
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL);
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE);
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    _CSPLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL);
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);

    // Misalignment settings
    _CSPSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED);
    _CSPLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION);
  }

  /**
   * @author Sunit Bhalla
   * This sets up the lists of which settings are (and are not) learned for CSP joints
   */
  protected void setNexLevLearnedSettings()
  {
    // Measurement learned Settings
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    _NexLevSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    _NexLevSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);

    // Open learned settings
    _NexLevSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    _NexLevSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL);
    _NexLevSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE);
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL);
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);

    // Misalignment settings
    _NexLevSettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED);
    _NexLevLearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION);
  }

  /**
   * @author Sunit Bhalla
   * This sets up the lists of which settings are (and are not) learned for non-collapsible
   */
  protected void setNonCollapsbileBGALearnedSettings()
  {
    // Measurement learned Settings
    _nonCollapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    _nonCollapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);
    _nonCollapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    _nonCollapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);
    _nonCollapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    _nonCollapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);

    // Open learned settings
    _nonCollapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    _nonCollapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL);
    _nonCollapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE);
    _nonCollapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    _nonCollapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL);
    _nonCollapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);

    // Misalignment settings
    _nonCollapsibleBGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED);
    _nonCollapsibleBGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION);
  }


  /**
   * @author Sunit Bhalla
   * This sets up the lists of which settings are (and are not) learned for CGA joints
   */
  protected void setCGALearnedSettings()
  {
    // Measurement learned Settings
    _CGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
    _CGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE);

    // Open learned settings
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE);
    _CGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL);
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);

    // Misalignment settings
    _CGASettingsNotLearned.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED);
    _CGALearnedSettings.add(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION);
  }

  /**
   * @author Sunit Bhalla
   * This test verifies that the subtype can be learned.
   */
  protected void testLearnSubtype(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();
    float diameter;
    float thickness;

    String expectString = null;

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    // Check that the settings are set to the default values
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGALearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGASettingsNotLearned);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    verifyAllSettingsAreNotAtDefault(subtype, _collapsibleBGALearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGASettingsNotLearned);

    int numberOfDatapoints = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    expectString = "Actual: " + numberOfDatapoints;

    // There are 324 joints in the subtype, but numberOfDatapoints could be less than this if the maximum datapoints
    // used in learning less than 324.
    Expect.expect((numberOfDatapoints == 324), expectString);

    // Check learned setting values
    for (AlgorithmSettingEnum diameterSetting : _learnedCollapsibleBGADiameterSettings)
    {
      diameter = (Float)subtype.getAlgorithmSettingValue(diameterSetting);
      expectString = "Actual: " + diameter;
      if (diameterSetting.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL))
      {
        Expect.expect(MathUtil.fuzzyEquals(diameter, _DIAMETER_OF_BGAS_MIDBALL_IN_MILS, _FUZZY_THRESHOLD_FOR_DIAMETER_IN_MILS),
                      expectString);
      }
      else if (diameterSetting.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE) ||
               (diameterSetting.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD)))
      {
        Expect.expect(MathUtil.fuzzyEquals(diameter, _DIAMETER_OF_BGAS_PAD_AND_PACKAGE_IN_MILS,
                                           _FUZZY_THRESHOLD_FOR_DIAMETER_IN_MILS),
                      expectString);
      }
      else
      {
        Expect.expect(false, "Algorithm Setting not supported");
      }
    }

    float openMinimumDiameter =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    expectString = "Actual: " + openMinimumDiameter;
    Expect.expect(MathUtil.fuzzyEquals(openMinimumDiameter, 70.0, 0.1), expectString);

    openMinimumDiameter =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);
    expectString = "Actual: " + openMinimumDiameter;
    Expect.expect(MathUtil.fuzzyEquals(openMinimumDiameter, 70.0, 0.1), expectString);

    float openMaximumDifference =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    expectString = "Actual: " + openMaximumDifference;
    Expect.expect(MathUtil.fuzzyEquals(openMaximumDifference, 90.0, 0.1), expectString);

    for (AlgorithmSettingEnum thicknessSetting : _learnedCollapsibleBGAThicknessSettings)
    {
      thickness = (Float)subtype.getAlgorithmSettingValue(thicknessSetting);
      expectString = "Actual: " + thickness;
      if (thicknessSetting.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL))
      {
        Expect.expect(MathUtil.fuzzyEquals(thickness, _THICKNESS_OF_BGAS_MIDBALL_IN_MILS, _FUZZY_THRESHOLD_FOR_THICKNESS_IN_MILS),
                      expectString);
      }
      else if (thicknessSetting.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE) ||
               (thicknessSetting.equals(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD)))
      {
        Expect.expect(MathUtil.fuzzyEquals(thickness, _THICKNESS_OF_BGAS_PAD_AND_PACKAGE_IN_MILS,
                                           _FUZZY_THRESHOLD_FOR_THICKNESS_IN_MILS), expectString);
      }
      else
      {
        Expect.expect(false, "Algorithm Setting not supported");
      }
    }

    // Delete subtype data; make sure values are back to their defaults
    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    algorithmSubtypeLearning.open();
    subtype.deleteLearnedAlgorithmSettingsData();
    algorithmSubtypeLearning.close();

    subtype.setLearnedSettingsToDefaults();

    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGALearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGASettingsNotLearned);

    verifyGridArrayDatabaseIsEmpty(subtype);

    deleteLearnedData(subtype);
  }


  /**
   * @author Sunit Bhalla
   * This test verifies that learned data can be deleted.
   */
  protected void testDeleteLearnedData(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();
    float diameter;
    float thickness;

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    // Check that the settings are set to the default values
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGALearnedSettings);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);

    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    verifyAllSettingsAreNotAtDefault(subtype, _collapsibleBGALearnedSettings);

    // Check learned setting values
    diameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    Expect.expect(MathUtil.fuzzyEquals(diameter, _DIAMETER_OF_BGAS_MIDBALL_IN_MILS, _FUZZY_THRESHOLD_FOR_DIAMETER_IN_MILS));
    thickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL);

    String expectString = "Actual: " + thickness;

    Expect.expect(MathUtil.fuzzyEquals(thickness,  _THICKNESS_OF_BGAS_MIDBALL_IN_MILS, _FUZZY_THRESHOLD_FOR_THICKNESS_IN_MILS), expectString);

    // Delete subtype data; make sure values are back to their defaults
    AlgorithmSubtypesLearningReaderWriter algorithmLearning = subtype.getPanel().getProject().getAlgorithmSubtypeLearningReaderWriter();
    algorithmLearning.open();
    subtype.deleteLearnedAlgorithmSettingsData();
    algorithmLearning.close();

    subtype.setLearnedSettingsToDefaults();

    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGALearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGASettingsNotLearned);

    int numberOfMeasurements = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    Expect.expect (numberOfMeasurements == 0);

    verifyGridArrayDatabaseIsEmpty(subtype);

    deleteLearnedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   */
  protected void verifyGridArrayDatabaseIsEmpty(Subtype subtype) throws XrayTesterException
  {
    Assert.expect(subtype != null);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = subtype.getPanel().getProject().getAlgorithmSubtypeLearningReaderWriter();
    algorithmSubtypeLearning.open();

    SliceNameEnum slice = SliceNameEnum.PAD;
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_REGION_OUTLIER,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL,
                                                           true,
                                                           true).length == 0);

    slice = SliceNameEnum.MIDBALL;
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_REGION_OUTLIER,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL,
                                                           true,
                                                           true).length == 0);

    slice = SliceNameEnum.PACKAGE;
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_DIAMETER,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_THICKNESS,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_REGION_OUTLIER,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL,
                                                           true,
                                                           true).length == 0);

    algorithmSubtypeLearning.close();
  }


  /**
   * @author Sunit Bhalla
   */
  protected void testIncrementalLearning(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();
    float diameter;

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // Check learned setting values
    diameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    int numberOfDatapoints = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    Expect.expect(MathUtil.fuzzyEquals(diameter, _DIAMETER_OF_BGAS_MIDBALL_IN_MILS, _FUZZY_THRESHOLD_FOR_DIAMETER_IN_MILS));

    // Learn the same subtype again
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // Check learned setting values
    diameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    int numberOfDatapoints2 = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    Expect.expect(MathUtil.fuzzyEquals(diameter, _DIAMETER_OF_BGAS_FOR_INCREMENTAL_LEARNING_IN_MILS, _FUZZY_THRESHOLD_FOR_DIAMETER_IN_MILS), "Actual : " + diameter );
    Expect.expect((2 * numberOfDatapoints) == numberOfDatapoints2);

    deleteLearnedData(subtype);
    verifyGridArrayDatabaseIsEmpty(subtype);
  }


  /**
   * @author Sunit Bhalla
   * This test learns a subtype on two images runs
   */
  protected void testMultipleImageRuns(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "StatisticalTuningWhiteImageSet",
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generateWhiteImageSet(project, imageSetData);

    imageSetData = createImageSetAndSetProgramFilters(project,
                                                      subtype,
                                                      "statisticalTuningBlackImageSet",
                                                      POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generateBlackImageSet(project, imageSetData);

    // Joints won't be found if "% of max" is used.
    subtype.setTunedValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE, "Max Slope");

    // This test just makes sure that running on two image sets doesn't crash.
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    deleteLearnedData(subtype);

  }

  /**
   * @author Sunit Bhalla
   * This test checks to make sure that running learning on black images runs acceptably.
   */
  protected void testBlackImages(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "statisticalTuningBlackImageSet",
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generateBlackImageSet(project, imageSetData);

     // Joints won't be found if "% of max" is used.
    subtype.setTunedValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE, "Max Slope");

    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    float diameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    String expectString = "Actual: " + diameter;
    Expect.expect(MathUtil.fuzzyEquals(diameter,  MathUtil.convertMilsToMillimeters(49.0),  MathUtil.convertMilsToMillimeters(0.5)),expectString);

    deleteLearnedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   * This test checks to make sure that running learning on white images runs acceptably.
   */
  protected void testWhiteImages(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "statisticalTuningWhiteImageSet",
                                                                   POPULATED_BOARD);
    generateWhiteImageSet(project, imageSetData);
    imageSetDataList.add(imageSetData);

    // Joints won't be found if "% of max" is used.
    subtype.setTunedValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE, "Max Slope");

    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    float diameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
    double expectedDiameterInMM = MathUtil.convertMilsToMillimeters(49.0);
    String expectString = "Expected: " + expectedDiameterInMM + " Actual: " + diameter;
    Expect.expect(MathUtil.fuzzyEquals(diameter,  expectedDiameterInMM,  MathUtil.convertMilsToMillimeters(0.5)),expectString);

    deleteLearnedData(subtype);
  }


  /**
   * @author Sunit Bhalla
   * This tests checks that learning isn't done if only unloaded boards are used to learn and if no data exist
   * in the learning database.
   */
  protected void testOnlyUnloadedBoards(Project project) throws XrayTesterException
  {
    Assert.expect(project != null);

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
    // Create an image run that has only unloaded boards for the subtype
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "TestGridArrayUnpopulatedImageSet",
                                                                   UNPOPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generatePseudoImageSet(project, imageSetData);

    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // Check that no subtypes were learned
    Expect.expect(panel.getNumberOfInspectedSubtypesWithAllAlgorithmsExceptShortLearned() == 0);
    Expect.expect(panel.getNumberOfInspectedSubtypesWithShortLearned() == 0);

    // Check that values are at their defaults
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGALearnedSettings);

    deleteLearnedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   * This tests checks that learning is done if the database has data and unloaded boards are passed in.
   */
  protected void testUnloadedBoardsWithTypicalDataInDatabase(Project project) throws XrayTesterException
  {

    Assert.expect(project != null);

    // Note: GridArray does not use unloaded boards to set settings.  So, this test doesn't really do much.  However,
    // I wanted to keep it here so we remember to make this test for other Inspection Familes.

    Panel panel = project.getPanel();

    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // Check that values are at their defaults
    verifyAllSettingsAreNotAtDefault(subtype, _collapsibleBGALearnedSettings);

    int numberOfDatapoints = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    String expectString = "Actual: " + numberOfDatapoints;
    Expect.expect((numberOfDatapoints == 324), expectString);

    // Create an image run that has only unloaded boards for the subtype
    imageSetData = createImageSetAndSetProgramFilters(project,
                                                      subtype,
                                                      "TestGridArrayUnpopulatedImageSet",
                                                      UNPOPULATED_BOARD);
    generatePseudoImageSet(project, imageSetData);

    imageSetDataList.clear();
    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // If GridArray supported learning with unloaded boards (assuming there's typical data in the database), I would
    // do a check to make sure that the unloaded data was saved (and settings were impacted).  For now, I'll just
    // check the number of datapoints to make sure that nothing was really added.
    numberOfDatapoints = getNumberOfMeasurements(subtype, SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_DIAMETER);
    expectString = "Actual: " + numberOfDatapoints;
    Expect.expect((numberOfDatapoints == 324), expectString);

    deleteLearnedData(subtype);
  }


  /**
    * @author Sunit Bhalla
    * This test checks to make sure that learning on limited data generates a warning
    */
   protected void testLimitedDataWarning(Project project) throws XrayTesterException
   {
     Assert.expect(project != null);

     InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
     inspectionEventObservable.addObserver(this);

     Panel panel = project.getPanel();
     Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

     Pad pad = project.getPanel().getBoards().get(0).getComponent(REFDES_TO_TEST).getPad(PAD_TO_TEST);

     // Create image run and learn
     List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
     ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                    pad,
                                                                    "singleGridArrayPadImageSet",
                                                                    POPULATED_BOARD);
     imageSetDataList.add(imageSetData);

     generateRegularImageSet(project, imageSetData);
     deleteLearnedData(subtype);

     // Should generate limited data warning
     learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

     inspectionEventObservable.deleteObserver(this);
     deleteLearnedData(subtype);
   }

   /**
    * @author Sunit Bhalla
    */
   protected void testCGA() throws XrayTesterException
   {
     Project project =  getProject("FAMILIES_ALL_RLV");
     Panel panel = project.getPanel();

     // Change joint type for a package to a new joint type
     ComponentType componentType = panel.getComponentType("boardType1", "C1");
     CompPackage compPackage = componentType.getCompPackage();
     compPackage.setJointTypeEnum(JointTypeEnum.CGA);

     // Doing this will regenerate the test program.
     project.getTestProgram();

     Subtype subtype = panel.getSubtype("sm00137_CGA");

     // Create image run and learn
     List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
     ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                    subtype,
                                                                    "CGAImageSet",
                                                                    POPULATED_BOARD);
     imageSetDataList.add(imageSetData);
     generatePseudoImageSet(project, imageSetData);

     learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

     verifyAllSettingsAreNotAtDefault(subtype, _CGALearnedSettings);
     verifyAllSettingsAreAtDefault(subtype, _CGASettingsNotLearned);

     deleteLearnedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   */
  protected void testCollapsibleBGA() throws XrayTesterException
  {
    Project project =  getProject("FAMILIES_ALL_RLV");
    Panel panel = project.getPanel();
    Subtype subtype = panel.getSubtype(_BGA_SUBTYPE_1);

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    verifyAllSettingsAreNotAtDefault(subtype, _collapsibleBGALearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _collapsibleBGASettingsNotLearned);

    deleteLearnedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   */
  protected void testNonCollapsibleBGA() throws XrayTesterException
  {
    Project project =  getProject("FAMILIES_ALL_RLV");
    Panel panel = project.getPanel();

    // Change joint type for a package to a new joint type
    ComponentType componentType = panel.getComponentType("boardType1", "C1");
    CompPackage compPackage = componentType.getCompPackage();
    compPackage.setJointTypeEnum(JointTypeEnum.NON_COLLAPSABLE_BGA);

    // Doing this will regenerate the test program.
    project.getTestProgram();

    Subtype subtype = panel.getSubtype("sm00137_NonCollapsibleBGA");

    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "NonCollapsibleBGAImageSet",
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generatePseudoImageSet(project, imageSetData);

    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    verifyAllSettingsAreNotAtDefault(subtype, _nonCollapsibleBGALearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _nonCollapsibleBGASettingsNotLearned);

    deleteLearnedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   */
  protected void testCSP() throws XrayTesterException
  {
    Project project =  getProject("FAMILIES_ALL_RLV");
    Panel panel = project.getPanel();

    // Change joint type for a package to a new joint type
    ComponentType componentType = panel.getComponentType("boardType1", "C1");
    CompPackage compPackage = componentType.getCompPackage();
    compPackage.setJointTypeEnum(JointTypeEnum.CHIP_SCALE_PACKAGE);

    // Doing this will regenerate the test program.
    project.getTestProgram();

    Subtype subtype = panel.getSubtype("sm00137_CSP");

    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "CSPImageSet",
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generatePseudoImageSet(project, imageSetData);

    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    verifyAllSettingsAreNotAtDefault(subtype, _CSPLearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _CSPSettingsNotLearned);

    deleteLearnedData(subtype);
  }

  /**
   * @author Sunit Bhalla
   */
  protected void testNexLev() throws XrayTesterException
  {
    Project project =  getProject("FAMILIES_ALL_RLV");
    Panel panel = project.getPanel();

    // Change joint type for a package to a new joint type
    ComponentType componentType = panel.getComponentType("boardType1", "C1");
    CompPackage compPackage = componentType.getCompPackage();
    compPackage.setJointTypeEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);

    // Doing this will regenerate the test program.
    project.getTestProgram();

    Subtype subtype = panel.getSubtype("sm00137_VariableHeightBGAConnector");

    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "NexLevImageSet",
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generatePseudoImageSet(project, imageSetData);

    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    verifyAllSettingsAreNotAtDefault(subtype, _NexLevLearnedSettings);
    verifyAllSettingsAreAtDefault(subtype, _NexLevSettingsNotLearned);

    deleteLearnedData(subtype);
  }

  public void update(Observable observable, Object object)
  {
    if (observable instanceof InspectionEventObservable)
    {
      InspectionEvent inspectionEvent = (InspectionEvent)object;
      if (inspectionEvent instanceof MessageInspectionEvent)
      {
        MessageInspectionEvent event = (MessageInspectionEvent)inspectionEvent;
        System.out.println(event.getMessage());
      }

    }
  }
}
