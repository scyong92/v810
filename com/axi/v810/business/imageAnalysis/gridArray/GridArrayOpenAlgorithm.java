package com.axi.v810.business.imageAnalysis.gridArray;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;

/**
 * Algorithm for detecting opens on PBGA, CBGA, and CCGA joints.  There are a few
 * different features that we're looking for.
 * <p>
 * First, on collapsible BGAs (PBGAs),
 * we are primarily looking for any diameter that is too big.  A diameter at the
 * midball is a good indication that the joint is detatched at either the pad
 * or the package slice.  Why don't we check the pad or package slices for diameters
 * that are too small?  While this is the most intuitive thing to do, the shading
 * from the rest of the joint makes it very difficult to detect how much solder
 * there is at those interface slices.  On the other hand, we are pretty good at
 * accurately measuring the diameter at the midball so that's what we rely on:
 * if there is a gap at pad or package, there will be a side-effect of the midball
 * being too big.  So we detect opens on PBGAs through this kind of indirect test.
 * <p>
 * Why do we also test the pad and package for being too big?  When joint are *really*
 * grossly open then the midball may appear normal size but the interface slice
 * opposite from the open will be very large.
 * <p>
 * For non-collapsible BGAs (CBGAs) and column grid arrays (CCGAs) we cannot
 * take advantage of the fact that the midball will get bigger in the event of
 * an open;  the non-collapsible balls and columns do not change shape in any
 * case.  So we are forced to check the interface slices to see if it looks
 * like there is enough solder there.  This is difficult due to the aforementioned
 * shading problems.
 * <p>
 * In addition to checking the diameter of a joint, we also check the "thickness",
 * or how dark the joint appears.  This not a very reliable measurement so we
 * only try to catch totally missing balls or columns.  Why don't we use diameter
 * to catch these?  Because in those cases we are likely to lock onto the
 * bare pad outline and it's possible that diameter may appear to be ok.  But
 * a gross check of thickness should catch this problem.
 *
 * Region Outlier was not used in ASAP.  Minimum region outlier was found to be useful
 * in testing NextLev components.  Maximum region outlier is not supported in Genesis.
 * <p>
 * <h3>Eccentricity</h3>
 * One other alternate method for detecting opens is the eccentricity measurement.
 * Eccentricity measures how oblong the joint is.  This allows the customer to
 * use a design-for-test technique in which the pads are given an oblong shape
 * so that a good soldered joint will take on an oblong appearance while an open
 * joint will remain circular.
 * <p>
 * In the 5dx, the flattening measurement was based on a computed contour and comparing
 * longest part of that contour to the size perpendicular to that.
 * For Genesis, we compute a metric that will give teardrop shapes an even higher
 * score : moment-based eccentricity. The function is : <p>
 * <pre>
 *             sqrt((u20 - u02)^2 + 4*u11^2)
 *       ecc = -----------------------------
 *                     u20 + u02
 * </pre><p>
 * where uMN is the (M+N)th order normalized central moment.
 * This formula is the most reliable I (Patrick Lacz) have found giving a convient range
 * of values. The more ubiquitous: <p>
 * <pre>
 *             (u20 - u02)^2 + 4*u11^2
 *       ecc = -----------------------
 *                        u00
 * </pre><p>
 * produces very small numbers with ranges that fluxtuate more dramatically in less
 * predictable ways. (And thus would be more sensitive to variations in thickness)
 *
 * @author Peter Esbensen
 */
public class GridArrayOpenAlgorithm extends Algorithm
{
  /**
   * @author Peter Esbensen
   */
  public GridArrayOpenAlgorithm(InspectionFamily gridArrayInspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.GRID_ARRAY);

    int displayOrder = 1;
    int currentVersion = 1;

    // This threshold compares the diameter against the nominal diameter at the pad slice
    AlgorithmSetting minimumDiameterPadSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minimumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minimumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);

    minimumDiameterPadSlice.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minimumDiameterPadSlice);
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the nominal diameter at the pad slice
    AlgorithmSetting maximumDiameterPadSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PAD,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maximumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maximumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterPadSlice,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);

    maximumDiameterPadSlice.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumDiameterPadSlice);
    
    // Added by Lee Herng 15 Nov 2011 - This threshold compares the diameter against the nominal diameter at the lower pad slice
    AlgorithmSetting minimumDiameterLowerPadSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_LOWERPAD,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                  minimumDiameterLowerPadSlice,
                                                                  SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                  MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL); //lam

    minimumDiameterLowerPadSlice.filter(JointTypeEnum.CGA);
    minimumDiameterLowerPadSlice.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumDiameterLowerPadSlice.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minimumDiameterLowerPadSlice.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minimumDiameterLowerPadSlice);
    
    // Added by LeeHerng 15 Nov 2011 - This threshold compares the diameter against the nominal diameter at the lower pad slice
    AlgorithmSetting maximumDiameterLowerPadSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_LOWERPAD,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                  maximumDiameterLowerPadSlice,
                                                                  SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                  MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL); //lam

    maximumDiameterLowerPadSlice.filter(JointTypeEnum.CGA);
    maximumDiameterLowerPadSlice.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumDiameterLowerPadSlice.filter(JointTypeEnum.COLLAPSABLE_BGA);
    maximumDiameterLowerPadSlice.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumDiameterLowerPadSlice);

    // This threshold compares the diameter against the nominal diameter at the midball slice
    AlgorithmSetting minimumDiameterMidballSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterMidballSlice,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterMidballSlice,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterMidballSlice,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterMidballSlice.filter(JointTypeEnum.CGA);
    minimumDiameterMidballSlice.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterMidballSlice);
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the nominal diameter at the midball slice
    AlgorithmSetting maximumDiameterMidballSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_MIDBALL,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterMidballSlice,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterMidballSlice,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterMidballSlice,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterMidballSlice.filter(JointTypeEnum.CGA);
    maximumDiameterMidballSlice.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterMidballSlice);

    // This threshold compares the diameter against the nominal diameter at the package slice
    AlgorithmSetting minimumDiameterPackageSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minimumDiameterPackageSlice,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterPackageSlice,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);

    minimumDiameterPackageSlice.filter(JointTypeEnum.CGA);
    minimumDiameterPackageSlice.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minimumDiameterPackageSlice.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minimumDiameterPackageSlice);
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the nominal diameter at the package slice
    AlgorithmSetting maximumDiameterPackageSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PACKAGE,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maximumDiameterPackageSlice,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterPackageSlice,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);

    maximumDiameterPackageSlice.filter(JointTypeEnum.CGA);
    maximumDiameterPackageSlice.filter(JointTypeEnum.COLLAPSABLE_BGA);
    maximumDiameterPackageSlice.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumDiameterPackageSlice);
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 1
    AlgorithmSetting minimumDiameterUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_1,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice1.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice1);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 1
    AlgorithmSetting maximumDiameterUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice1.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice1);
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 2
    AlgorithmSetting minimumDiameterUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_2,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice2.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice2);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 2
    AlgorithmSetting maximumDiameterUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice2.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice2);                
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 3
    AlgorithmSetting minimumDiameterUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_3,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice3.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice3);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 3
    AlgorithmSetting maximumDiameterUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice3.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice3);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 4
    AlgorithmSetting minimumDiameterUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_4,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice4.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice4);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 4
    AlgorithmSetting maximumDiameterUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice4.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice4);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 5
    AlgorithmSetting minimumDiameterUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_5,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice5.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice5);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 5
    AlgorithmSetting maximumDiameterUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice5.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice5);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 6
    AlgorithmSetting minimumDiameterUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_6,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice6.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice6);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 3
    AlgorithmSetting maximumDiameterUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice6.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice6);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 7
    AlgorithmSetting minimumDiameterUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_7,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice7.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice7);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 7
    AlgorithmSetting maximumDiameterUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice7.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice7);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 8
    AlgorithmSetting minimumDiameterUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_8,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice8.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice8);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 8
    AlgorithmSetting maximumDiameterUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice8.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice8);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 9
    AlgorithmSetting minimumDiameterUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_9,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice9.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice9);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 9
    AlgorithmSetting maximumDiameterUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice9.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice9);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 10
    AlgorithmSetting minimumDiameterUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_10,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumDiameterUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice10.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice10);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 10
    AlgorithmSetting maximumDiameterUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumDiameterUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice10.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice10);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 11
    AlgorithmSetting minimumDiameterUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_11,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice11.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice11);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 11
    AlgorithmSetting maximumDiameterUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice11.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice11);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 12
    AlgorithmSetting minimumDiameterUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_12,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice12.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice12);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 12
    AlgorithmSetting maximumDiameterUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice12.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice12);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 13
    AlgorithmSetting minimumDiameterUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_13,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice13.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice13);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 13
    AlgorithmSetting maximumDiameterUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice13.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice13);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 14
    AlgorithmSetting minimumDiameterUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_14,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice14.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice14);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 14
    AlgorithmSetting maximumDiameterUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice14.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice14);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 15
    AlgorithmSetting minimumDiameterUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_15,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice15.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice15);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 15
    AlgorithmSetting maximumDiameterUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice15.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice15);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 16
    AlgorithmSetting minimumDiameterUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_16,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice16.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice16);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 16
    AlgorithmSetting maximumDiameterUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice16.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice16);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 17
    AlgorithmSetting minimumDiameterUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_17,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice17.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice17);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 17
    AlgorithmSetting maximumDiameterUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice17.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice17);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 18
    AlgorithmSetting minimumDiameterUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_18,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice18.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice18);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 18
    AlgorithmSetting maximumDiameterUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice18.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice18);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 19
    AlgorithmSetting minimumDiameterUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_19,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice19.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice19);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 19
    AlgorithmSetting maximumDiameterUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice19.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice19);        
    
    // This threshold compares the diameter against the nominal diameter at the user-defined slice 20
    AlgorithmSetting minimumDiameterUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_20,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumDiameterUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumDiameterUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumDiameterUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumDiameterUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minimumDiameterUserDefinedSlice20.filter(JointTypeEnum.CGA);
    minimumDiameterUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumDiameterUserDefinedSlice20);
    
    // Added by LeeHerng 7 April 2011 - This threshold compares the diameter against the nominal diameter at the user-defined slice 20
    AlgorithmSetting maximumDiameterUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumDiameterUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumDiameterUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumDiameterUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumDiameterUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    maximumDiameterUserDefinedSlice20.filter(JointTypeEnum.CGA);
    maximumDiameterUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumDiameterUserDefinedSlice20);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the pad slice
    AlgorithmSetting minimumNeighborOutlierPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PAD,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minimumNeighborOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);

    minimumNeighborOutlierPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minimumNeighborOutlierPad);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the pad slice
    AlgorithmSetting maximumNeighborOutlierPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PAD,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierPad.filter(JointTypeEnum.CGA);
    maximumNeighborOutlierPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    maximumNeighborOutlierPad.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(maximumNeighborOutlierPad);
    
    // Added by Lee Herng 15 Nov 2011 - This threshold compares the diameter against the diameters of the joint's nearest neighbors at the pad slice
    AlgorithmSetting minimumNeighborOutlierLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                  minimumNeighborOutlierLowerPad,
                                                                  SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                  MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE); // lam


    minimumNeighborOutlierLowerPad.filter(JointTypeEnum.CGA);
    minimumNeighborOutlierLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minimumNeighborOutlierLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minimumNeighborOutlierLowerPad);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the pad slice
    AlgorithmSetting maximumNeighborOutlierLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierLowerPad,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierLowerPad.filter(JointTypeEnum.CGA);
    maximumNeighborOutlierLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    maximumNeighborOutlierLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumNeighborOutlierLowerPad);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the midball slice
    AlgorithmSetting minimumNeighborOutlierMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_MIDBALL,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierMidball.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierMidball);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the midball slice
    AlgorithmSetting maximumNeighborOutlierMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierMidball.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierMidball);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the package slice
    AlgorithmSetting minimumNeighborOutlierPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PACKAGE,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierPackage.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minimumNeighborOutlierPackage.filter(JointTypeEnum.CGA);
    minimumNeighborOutlierPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    minimumNeighborOutlierPackage.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(minimumNeighborOutlierPackage);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the package slice
    AlgorithmSetting maximumNeighborOutlierPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierPackage.filter(JointTypeEnum.CGA);
    maximumNeighborOutlierPackage.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumNeighborOutlierPackage);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 1
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice1.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice1);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 1
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice1.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice1);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 2
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice2.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice2);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 2
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice2.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice2);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 3
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice3.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice3);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 3
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice3.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice3);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 4
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice4.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice4);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 4
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice4.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice4);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 5
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice5.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice5);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 5
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice5.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice5);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 6
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice6.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice6);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 6
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice6.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice6);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 7
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice7.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice7);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 7
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice7.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice7);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 8
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice8.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice8);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 8
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice8.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice8);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 9
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice9.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice9);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 9
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice9.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice9);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 10
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice10.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice10);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 10
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice10.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice10);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 11
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice11.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice11);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 11
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice11.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice11);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 12
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice12.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice12);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 12
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice12.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice12);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 13
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice13.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice13);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 13
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice13.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice13);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 14
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice14.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice14);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 14
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice14.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice14);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 15
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice15.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice15);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 15
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice15.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice15);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 16
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice16.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice16);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 16
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice16.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice16);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 17
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice17.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice17);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 17
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice17.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice17);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 18
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice18.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice18);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 18
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice18.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice18);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 19
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice19.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice19);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 19
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice19.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice19);
    
    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 20
    AlgorithmSetting minimumNeighborOutlierUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20,
      displayOrder++,
      -3.5f, // default
      -100.0f, // min
      0.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumNeighborOutlierUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumNeighborOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumNeighborOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumNeighborOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    minimumNeighborOutlierUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumNeighborOutlierUserDefinedSlice20.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(minimumNeighborOutlierUserDefinedSlice20);

    // This threshold compares the diameter against the diameters of the joint's nearest neighbors at the user-defined slice 20
    AlgorithmSetting maximumNeighborOutlierUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20,
      displayOrder++,
      3.5f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumNeighborOutlierUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumNeighborOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumNeighborOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumNeighborOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    maximumNeighborOutlierUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumNeighborOutlierUserDefinedSlice20.filter(JointTypeEnum.CGA);
    addAlgorithmSetting(maximumNeighborOutlierUserDefinedSlice20);

    // This threshold compares the tbickness against the nominal thickness at the pad slice
    AlgorithmSetting minimumThickness = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_THICKNESS,
      displayOrder++,
      50.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_THICKNESS)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minimumThickness,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumThickness,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumThickness,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minimumThickness,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minimumThickness,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumThickness,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minimumThickness);


    AlgorithmSetting minimumEccentricityPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_PAD,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minimumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minimumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);

    minimumEccentricityPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minimumEccentricityPad);

    AlgorithmSetting maximumEccentricityPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_PAD,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maximumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maximumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);

    maximumEccentricityPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumEccentricityPad);
    
    // Added by Lee Herng 15 Nov 2011
    AlgorithmSetting minimumEccentricityLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_LOWERPAD,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityLowerPad,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY); //lam

    minimumEccentricityLowerPad.filter(JointTypeEnum.CGA);
    minimumEccentricityLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumEccentricityLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minimumEccentricityLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minimumEccentricityLowerPad);
    
    AlgorithmSetting maximumEccentricityLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_LOWERPAD,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityLowerPad,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY); // lam

    maximumEccentricityLowerPad.filter(JointTypeEnum.CGA);
    maximumEccentricityLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maximumEccentricityLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    maximumEccentricityLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumEccentricityLowerPad);

    // the smallest allowable 'eccentricty' score
    AlgorithmSetting minimumEccentricityMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_MIDBALL,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityMidball.filter(JointTypeEnum.CGA);
    minimumEccentricityMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumEccentricityMidball.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minimumEccentricityMidball.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(minimumEccentricityMidball);


    // the largest allowable 'eccentricity' score
    AlgorithmSetting maximumEccentricityMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_MIDBALL,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityMidball.filter(JointTypeEnum.CGA);
    maximumEccentricityMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityMidball);
    
    // Added by Lee Herng 7 April 2012
    AlgorithmSetting minimumEccentricityUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice1.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice1);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice1.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice1);
    
    // Added by Lee Herng 7 April 2012
    AlgorithmSetting minimumEccentricityUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice2.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice2);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice2.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice2);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice3.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice3);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice3.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice3);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice4.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice4);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice4.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice4);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice5.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice5);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice5.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice5);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice6.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice6);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice6.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice6);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice7.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice7);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice7.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice7);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice8.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice8);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice8.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice8);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice9.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice9);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice9.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice9);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    minimumEccentricityUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice10.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice10);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    maximumEccentricityUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice10.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice10);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice11.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice11);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice11.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice11);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice12.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice12);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice12.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice12);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice13.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice13);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice13.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice13);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice14.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice14);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice14.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice14);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice15.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice15);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice15.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice15);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice16.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice16);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice16.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice16);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice17.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice17);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice17.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice17);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice18.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice18);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice18.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice18);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice19.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice19);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice19.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice19);
    
    AlgorithmSetting minimumEccentricityUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20,
      displayOrder++,
      -20.0f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumEccentricityUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumEccentricityUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumEccentricityUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumEccentricityUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    minimumEccentricityUserDefinedSlice20.filter(JointTypeEnum.CGA);
    minimumEccentricityUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumEccentricityUserDefinedSlice20);
    
    AlgorithmSetting maximumEccentricityUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20,
      displayOrder++,
      1.8f, // default
      -20.0f, // min
      20.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumEccentricityUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumEccentricityUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumEccentricityUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumEccentricityUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    maximumEccentricityUserDefinedSlice20.filter(JointTypeEnum.CGA);
    maximumEccentricityUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumEccentricityUserDefinedSlice20);

    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the pad slice
    AlgorithmSetting minRegionOutlierPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    
    minRegionOutlierPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minRegionOutlierPad);
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the pad slice
    AlgorithmSetting maxRegionOutlierPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PAD,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);

    maxRegionOutlierPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maxRegionOutlierPad);
    
    // Added by Lee Herng 15 Nov 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the lower pad slice
    AlgorithmSetting minRegionOutlierLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_LOWERPAD,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierLowerPad,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER); //lam

    minRegionOutlierLowerPad.filter(JointTypeEnum.CGA);
    minRegionOutlierLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minRegionOutlierLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minRegionOutlierLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minRegionOutlierLowerPad);
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the pad slice
    AlgorithmSetting maxRegionOutlierLowerPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_LOWERPAD,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_LOWERPAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_LOWERPAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_LOWERPAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierLowerPad,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER); //lam

    maxRegionOutlierLowerPad.filter(JointTypeEnum.CGA);
    maxRegionOutlierLowerPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maxRegionOutlierLowerPad.filter(JointTypeEnum.COLLAPSABLE_BGA);
    maxRegionOutlierLowerPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maxRegionOutlierLowerPad);

    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the midball slice
    AlgorithmSetting minRegionOutlierMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    minRegionOutlierMidball.filter(JointTypeEnum.COLLAPSABLE_BGA);
    minRegionOutlierMidball.filter(JointTypeEnum.CGA);
    minRegionOutlierMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minRegionOutlierMidball.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(minRegionOutlierMidball);
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the midball slice
    AlgorithmSetting maxRegionOutlierMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_MIDBALL,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    maxRegionOutlierMidball.filter(JointTypeEnum.COLLAPSABLE_BGA);
    maxRegionOutlierMidball.filter(JointTypeEnum.CGA);
    maxRegionOutlierMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    maxRegionOutlierMidball.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(maxRegionOutlierMidball);

    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the package slice
    AlgorithmSetting minRegionOutlierPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    minRegionOutlierPackage.filter(JointTypeEnum.CGA);
    minRegionOutlierPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(minRegionOutlierPackage);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the package slice
    AlgorithmSetting maxRegionOutlierPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PACKAGE,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    maxRegionOutlierPackage.filter(JointTypeEnum.CGA);
    maxRegionOutlierPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maxRegionOutlierPackage);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 1
    AlgorithmSetting minRegionOutlierUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice1);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 1
    AlgorithmSetting maxRegionOutlierUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice1);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 2
    AlgorithmSetting minRegionOutlierUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice2);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 2
    AlgorithmSetting maxRegionOutlierUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice2);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 3
    AlgorithmSetting minRegionOutlierUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice3);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 3
    AlgorithmSetting maxRegionOutlierUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice3);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 4
    AlgorithmSetting minRegionOutlierUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice4);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 4
    AlgorithmSetting maxRegionOutlierUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice4);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 5
    AlgorithmSetting minRegionOutlierUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice5);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 5
    AlgorithmSetting maxRegionOutlierUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice5);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 6
    AlgorithmSetting minRegionOutlierUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice6);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 6
    AlgorithmSetting maxRegionOutlierUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice6);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 7
    AlgorithmSetting minRegionOutlierUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice7);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 7
    AlgorithmSetting maxRegionOutlierUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice7);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 8
    AlgorithmSetting minRegionOutlierUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice8);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 8
    AlgorithmSetting maxRegionOutlierUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice8);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 9
    AlgorithmSetting minRegionOutlierUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice9);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 9
    AlgorithmSetting maxRegionOutlierUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice9);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 10
    AlgorithmSetting minRegionOutlierUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minRegionOutlierUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice10);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 10
    AlgorithmSetting maxRegionOutlierUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maxRegionOutlierUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice10);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 11
    AlgorithmSetting minRegionOutlierUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice11);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 11
    AlgorithmSetting maxRegionOutlierUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice11);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 12
    AlgorithmSetting minRegionOutlierUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice12);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 12
    AlgorithmSetting maxRegionOutlierUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice12);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 13
    AlgorithmSetting minRegionOutlierUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice13);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 13
    AlgorithmSetting maxRegionOutlierUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice13);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 14
    AlgorithmSetting minRegionOutlierUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice14);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 14
    AlgorithmSetting maxRegionOutlierUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice14);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 15
    AlgorithmSetting minRegionOutlierUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice15);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 15
    AlgorithmSetting maxRegionOutlierUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice15);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 16
    AlgorithmSetting minRegionOutlierUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice16);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 16
    AlgorithmSetting maxRegionOutlierUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice16);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 17
    AlgorithmSetting minRegionOutlierUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice17);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 17
    AlgorithmSetting maxRegionOutlierUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice17);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 18
    AlgorithmSetting minRegionOutlierUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice18);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 18
    AlgorithmSetting maxRegionOutlierUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice18);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 19
    AlgorithmSetting minRegionOutlierUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice19);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 19
    AlgorithmSetting maxRegionOutlierUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice19);
    
    // This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 20
    AlgorithmSetting minRegionOutlierUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minRegionOutlierUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   minRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   minRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(minRegionOutlierUserDefinedSlice20);    
    
    // Added by LeeHerng 15 June 2011 - This threshold compares the diameter against the diameter of all joints in the same ReconstructionRegion at the user-defined slice 20
    AlgorithmSetting maxRegionOutlierUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20,
      displayOrder++,
      120.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maxRegionOutlierUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maxRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maxRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maxRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maxRegionOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);
    addAlgorithmSetting(maxRegionOutlierUserDefinedSlice20);

    AlgorithmSetting voidAreaLimitForNeighborOutlier = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER,
      displayOrder++,
      10.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_OPEN_(VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(voidAreaLimitForNeighborOutlier);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);//lam
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   voidAreaLimitForNeighborOutlier,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);

    // This threshold checks the percent of joints falling below the marginal diameter threshold
    AlgorithmSetting maxPercentOfJointsWithInsufficientDiameter = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER,
        displayOrder++,
        100.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_OPEN_(GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_OPEN_(GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    maxPercentOfJointsWithInsufficientDiameter.filter(JointTypeEnum.CGA);
    maxPercentOfJointsWithInsufficientDiameter.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maxPercentOfJointsWithInsufficientDiameter);

    // This sets the threshold for marginal diameters (insufficients).  This will only call a defect in combination with the GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER threshold
    AlgorithmSetting minMarginalDiameter = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL,
        displayOrder++,
        95.0f, // default
        0.0f, // min
        300.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_OPEN_(GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_OPEN_(GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minMarginalDiameter,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minMarginalDiameter,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minMarginalDiameter,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL);
    minMarginalDiameter.filter(JointTypeEnum.CGA);
    minMarginalDiameter.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minMarginalDiameter);
    
    AlgorithmSetting minimumHipOutlierPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_PAD,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierPad.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    minimumHipOutlierPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    minimumHipOutlierPad.filter(JointTypeEnum.CGA);
    minimumHipOutlierPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierPad);

    // This threshold compares the HIP outlier score of a joint at pad slice
    AlgorithmSetting maximumHipOutlierPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_PAD,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierPad.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    maximumHipOutlierPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    maximumHipOutlierPad.filter(JointTypeEnum.CGA);
    maximumHipOutlierPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierPad);                
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice1.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice1);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 1
    AlgorithmSetting maximumHipOutlierUserDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice1.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice1,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice1.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice1.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice1);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice2.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice2);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 2
    AlgorithmSetting maximumHipOutlierUserDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice2.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice2,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice2.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice2.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice2);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice3.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice3);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 3
    AlgorithmSetting maximumHipOutlierUserDefinedSlice3 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice3.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice3,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice3.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice3.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice3);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice4.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice4);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 4
    AlgorithmSetting maximumHipOutlierUserDefinedSlice4 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice4.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice4,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice4.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice4.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice4);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice5.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice5);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 5
    AlgorithmSetting maximumHipOutlierUserDefinedSlice5 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice5.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice5,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice5.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice5.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice5);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice6.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice6);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 6
    AlgorithmSetting maximumHipOutlierUserDefinedSlice6 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice6.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice6,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice6.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice6.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice6);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice7.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice7);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 7
    AlgorithmSetting maximumHipOutlierUserDefinedSlice7 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice7.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice7,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice7.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice7.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice7);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice8.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice8);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 8
    AlgorithmSetting maximumHipOutlierUserDefinedSlice8 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice8.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice8,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice8.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice8.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice8);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice9.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice9);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 9
    AlgorithmSetting maximumHipOutlierUserDefinedSlice9 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice9.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice9,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice9.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice9.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice9);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    minimumHipOutlierUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice10.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice10);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 10
    AlgorithmSetting maximumHipOutlierUserDefinedSlice10 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    maximumHipOutlierUserDefinedSlice10.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice10,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice10.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice10.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice10);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice11.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice11);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 11
    AlgorithmSetting maximumHipOutlierUserDefinedSlice11 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice11.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice11,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice11.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice11.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice11);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice12.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice12);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 12
    AlgorithmSetting maximumHipOutlierUserDefinedSlice12 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice12.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice12,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice12.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice12.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice12);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice13.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice13);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 13
    AlgorithmSetting maximumHipOutlierUserDefinedSlice13 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice13.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice13,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice13.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice13.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice13);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice14.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice14);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 14
    AlgorithmSetting maximumHipOutlierUserDefinedSlice14 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice14.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice14,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice14.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice14.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice14);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice15.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice15);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 15
    AlgorithmSetting maximumHipOutlierUserDefinedSlice15 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice15.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice15,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice15.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice15.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice15);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice16.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice16);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 16
    AlgorithmSetting maximumHipOutlierUserDefinedSlice16 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice16.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice16,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice16.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice16.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice16);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice17.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice17);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 17
    AlgorithmSetting maximumHipOutlierUserDefinedSlice17 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice17.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice17,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice17.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice17.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice17);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice18.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice18);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 18
    AlgorithmSetting maximumHipOutlierUserDefinedSlice18 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice18.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice18,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice18.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice18.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice18);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice19.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice19);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 19
    AlgorithmSetting maximumHipOutlierUserDefinedSlice19 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice19.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice19,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice19.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice19.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice19);        
    
    AlgorithmSetting minimumHipOutlierUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20,
      displayOrder++,
      -100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    minimumHipOutlierUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumHipOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   minimumHipOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   minimumHipOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    minimumHipOutlierUserDefinedSlice20.filter(JointTypeEnum.CGA);
    minimumHipOutlierUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(minimumHipOutlierUserDefinedSlice20);
    
    // This threshold compares the HIP outlier score of a joint at user defined slice 20
    AlgorithmSetting maximumHipOutlierUserDefinedSlice20 = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20,
      displayOrder++,
      100.0f, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
      "HTML_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_OPEN_(MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    maximumHipOutlierUserDefinedSlice20.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumHipOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumHipOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumHipOutlierUserDefinedSlice20,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    maximumHipOutlierUserDefinedSlice20.filter(JointTypeEnum.CGA);
    maximumHipOutlierUserDefinedSlice20.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(maximumHipOutlierUserDefinedSlice20);
    
    ArrayList<String> multiPassInspectionOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_OPEN_MULTIPASS_INSPECTION,
      displayOrder++,
      "Off",
      multiPassInspectionOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_OPEN_(MULTIPASS_INSPECTION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_OPEN_(MULTIPASS_INSPECTION)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_OPEN_(MULTIPASS_INSPECTION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion));

    addMeasurementEnums();
  }


  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_OPEN_ECCENTRICITY);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_HIP_VALUE);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_REGION_OUTLIER);

    if (Config.isComponentLevelClassificationEnabled())
      _componentMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_PERCENT_OF_JOINTS_WITH_INSUFFICIENT_DIAMETER);
    else
      _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_PERCENT_OF_JOINTS_WITH_INSUFFICIENT_DIAMETER);
  }


  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);
    Assert.expect(GridArrayInspectionFamily.hasAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT));

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    BooleanRef jointPassed = new BooleanRef(true);
    // lam changes
    GridArrayMeasurementAlgorithm measurementAlgo = (GridArrayMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
    for (SliceNameEnum sliceNameEnum : measurementAlgo.getInspectionSliceOrder(subtype))
    {
      ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, reconstructionRegion, slice, false);

        jointPassed.setValue(true);

        checkDiameterVersusNominal(slice, jointInspectionData, reconstructionRegion, subtype, jointPassed);

        checkThickness(slice, jointInspectionData, reconstructionRegion, subtype, jointPassed);

        if(subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1() == false)
        {
          checkRegionOutlier(sliceNameEnum, jointInspectionData, reconstructionRegion, jointTypeEnum, subtype, jointPassed);
        }

        if (shouldMinimumEccentricityBeTested(jointTypeEnum, sliceNameEnum))
        {
          checkMinimumEccentricity(slice, reconstructedImages, jointInspectionData, jointPassed);
        }

        if (shouldMaximumEccentricityBeTested(jointTypeEnum, sliceNameEnum))
        {
          checkMaximumEccentricity(slice, reconstructedImages, jointInspectionData, jointPassed);
        }

        // Show the pass/fail diagnostic
        AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                                jointInspectionData,
                                                                reconstructedImages.getReconstructionRegion(),
                                                                sliceNameEnum,
                                                                jointPassed.getValue());
      }
    }
  }


  /**
   * Compare the diameter of the joint-under-test to the nominal diameter.  If
   * it is too small, indict the joint as being open.  We only run this at the
   * interface slices (pad and package slices) because that's where we are worried
   * about there not being enough solder.
   *
   * @author Peter Esbensen
   */
  private void checkDiameterVersusNominal(ReconstructedSlice reconstructedSlice,
                                          JointInspectionData jointInspectionData,
                                          ReconstructionRegion reconstructionRegion,
                                          Subtype subtype,
                                          BooleanRef jointPassed)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(subtype != null);

    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();

    if (shouldMinimumDiameterBeTested(jointTypeEnum, sliceNameEnum))
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      JointMeasurement diameterAsPercentOfNominalMeasurement =
      GridArrayMeasurementAlgorithm.getDiameterAsPercentOfNominalMeasurement(jointInspectionData,
                                                                             sliceNameEnum);

      float diameterAsPercentOfNominal = diameterAsPercentOfNominalMeasurement.getValue();

      float minimumDiameterThreshold = getMinimumDiameterThreshold(sliceNameEnum, subtype);
      float maximumDiameterThreshold = getMaximumDiameterThreshold(sliceNameEnum, subtype);

      if (diameterAsPercentOfNominal < minimumDiameterThreshold)
      {
        JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
        openIndictment.addFailingMeasurement(diameterAsPercentOfNominalMeasurement);
        JointMeasurement diameterMeasurement = GridArrayMeasurementAlgorithm.getDiameterMeasurement(jointInspectionData,
                                                                                                    sliceNameEnum);
        openIndictment.addRelatedMeasurement(diameterMeasurement);
        jointInspectionResult.addIndictment(openIndictment);
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        diameterAsPercentOfNominalMeasurement,
                                                        minimumDiameterThreshold);
        jointPassed.setValue(false);
      }
      else if (diameterAsPercentOfNominal > maximumDiameterThreshold)
      {
        JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
        openIndictment.addFailingMeasurement(diameterAsPercentOfNominalMeasurement);
        JointMeasurement diameterMeasurement = GridArrayMeasurementAlgorithm.getDiameterMeasurement(jointInspectionData,
                                                                                                    sliceNameEnum);
        openIndictment.addRelatedMeasurement(diameterMeasurement);
        jointInspectionResult.addIndictment(openIndictment);
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        diameterAsPercentOfNominalMeasurement,
                                                        maximumDiameterThreshold);
        jointPassed.setValue(false);
      }
      else
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        diameterAsPercentOfNominalMeasurement,
                                                        minimumDiameterThreshold);
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private float getMinimumDiameterThreshold(SliceNameEnum sliceNameEnum,
                                            Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float minimumDiameterThreshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                         GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_LOWERPAD);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_1);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_2);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_3);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_4);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_5);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_6);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_7);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_8);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_9);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_10);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_11);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_12);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_13);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_14);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_15);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_16);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_17);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_18);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_19);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      minimumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_20);
    }
    else
    {
      Assert.expect(false);
    }
    return minimumDiameterThreshold;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private float getMaximumDiameterThreshold(SliceNameEnum sliceNameEnum,
                                            Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    
    float maximumDiameterThreshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                         GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_LOWERPAD);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PACKAGE);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      maximumDiameterThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20);
    }
    else
    {
      Assert.expect(false);
    }
    return maximumDiameterThreshold;
  }

  /**
   * Compare the thickness of the joint-under-test to the nominal thickness.
   *
   * @author Peter Esbensen
   */
  private void checkThickness(ReconstructedSlice reconstructedSlice,
                              JointInspectionData jointInspectionData,
                              ReconstructionRegion reconstructionRegion,
                              Subtype subtype,
                              BooleanRef jointPassed)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(jointPassed != null);

    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    if (shouldThicknessBeTested(jointTypeEnum, sliceNameEnum))
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      JointMeasurement thicknessMeasurement = GridArrayMeasurementAlgorithm.getThicknessMeasurement(jointInspectionData,
                                                                                                    sliceNameEnum);

      JointMeasurement thicknessAsPercentOfNominalMeasurement =
      GridArrayMeasurementAlgorithm.getThicknessAsPercentOfNominalMeasurement(jointInspectionData, sliceNameEnum);

      float thicknessAsPercentOfNominal = thicknessAsPercentOfNominalMeasurement.getValue();

      float minimumThicknessSetting =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_THICKNESS);

      if (thicknessAsPercentOfNominal < minimumThicknessSetting)
      {
        JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                             this,
                                                             sliceNameEnum);
        openIndictment.addFailingMeasurement(thicknessAsPercentOfNominalMeasurement);
        openIndictment.addRelatedMeasurement(thicknessMeasurement);
        jointInspectionResult.addIndictment(openIndictment);
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        thicknessAsPercentOfNominalMeasurement,
                                                        minimumThicknessSetting);
        jointPassed.setValue(false);
      }
      else
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        thicknessAsPercentOfNominalMeasurement,
                                                        minimumThicknessSetting);
      }
    }
  }

  /**
   * Compare the diameter of the joint-under-test to the median diameter of all
   * the joints in the same ReconstructionRegion.
   *
   * @author Cheah Lee Herng
   * @author Peter Esbensen
   * @author Siew Yeng - change parameters from reconstructedSlice to sliceNameEnum
   */
  private void checkRegionOutlier(SliceNameEnum sliceNameEnum,
                                  JointInspectionData jointInspectionData,
                                  ReconstructionRegion reconstructionRegion,
                                  JointTypeEnum jointTypeEnum,
                                  Subtype subtype,
                                  BooleanRef jointPassed)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(jointTypeEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(jointPassed != null);

//    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    if (shouldMinimumRegionOutlierBeTested(jointTypeEnum, sliceNameEnum))
    {
      JointMeasurement regionOutlierMeasurement =
      GridArrayMeasurementAlgorithm.getDiameterRegionOutlierMeasurement(jointInspectionData, sliceNameEnum);

      float regionOutlierMeasurementValue = regionOutlierMeasurement.getValue();

      float minimumRegionOutlierThreshold = getMinimumRegionOutlierThreshold(sliceNameEnum, subtype);
      float maximumRegionOutlierThreshold = getMaximumRegionOutlierThreshold(sliceNameEnum, subtype);

      if (regionOutlierMeasurementValue < minimumRegionOutlierThreshold)
      {
        JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
        openIndictment.addFailingMeasurement(regionOutlierMeasurement);
        jointInspectionResult.addIndictment(openIndictment);
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        regionOutlierMeasurement,
                                                        minimumRegionOutlierThreshold);
        jointPassed.setValue(false);
      }
      else if (regionOutlierMeasurementValue > maximumRegionOutlierThreshold)
      {
        JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
        openIndictment.addFailingMeasurement(regionOutlierMeasurement);
        jointInspectionResult.addIndictment(openIndictment);
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        regionOutlierMeasurement,
                                                        maximumRegionOutlierThreshold);
        jointPassed.setValue(false);  
      }
      else
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        regionOutlierMeasurement,
                                                        minimumRegionOutlierThreshold);
      }
    }
  }

  /*
   * @author Sunit Bhalla
   * Returns true if the minimum diameter should be tested.  This is determined by the joint type.
   */
  static boolean shouldMinimumDiameterBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||
          sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
      {
        return true;
      }
    }
    if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||
          sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD)  ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {  //lam
      if (sliceNameEnum.equals(SliceNameEnum.PAD) || sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) ||
          sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
           return true;
    }
    return false;
  }

  /*
   * @author Sunit Bhalla
   * Returns true if the minimum region outlier should be tested.  This is determined by the joint type.
   */
  static boolean shouldMinimumRegionOutlierBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) || 
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) || 
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {  //lam
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||  
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) || 
          sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    return false;
  }


  /*
   * @author Peter Esbensen
   * Returns true if the minimum neighbor outlier should be tested.  This is determined by the joint type.
   */
  static boolean shouldPercentOfJointsWithInsuffDiameterBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
        return true;
    }

    return false;
  }

  /*
   * @author Sunit Bhalla
   * Returns true if the minimum neighbor outlier should be tested.  This is determined by the joint type.
   */
  static boolean shouldMinimumNeighborOutlierBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||
          sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) || 
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||
          sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) || 
          sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }

    return false;
  }


  /*
   * @author Sunit Bhalla
   * Returns true if the maximum neighbor outlier should be tested.  This is determined by the joint type.
   */
  static boolean shouldMaximumNeighborOutlierBeTested(JointTypeEnum jointTypeEnum,
                                               SliceNameEnum sliceNameEnum,
                                               boolean voidingFailed)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    // If voiding failed, don't run open outlier
    if (voidingFailed)
      return false;

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) || 
          sliceNameEnum.equals(SliceNameEnum.MIDBALL) || 
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) || 
          sliceNameEnum.equals(SliceNameEnum.PACKAGE) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }

    return false;
  }


  /*
   * @author Sunit Bhalla
   * Returns true if the thickness should be tested.  This is determined by the joint type.
   */
  static boolean shouldThicknessBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
        jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      // Only test thickness at the midball
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
        return true;
    }
    else if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
             jointTypeEnum.equals(JointTypeEnum.CGA) ||
             jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      return true; // For these joint types, test thickness on all tested slices
    }

    return false;
  }

  /*
   * Returns true if eccentricity should be tested.  This is determined by the joint type and slice.

   * @author Peter Esbensen
   */
  static boolean shouldMaximumEccentricityBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    else if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
             jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    else if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
             jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) || 
          sliceNameEnum.equals(SliceNameEnum.PAD) || 
          sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) || 
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) || 
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20)) //lam
        return true;
    }

    return false;
  }

  /**
   * @author Peter Esbensen
   */
  static boolean eccentricityIsTested(JointTypeEnum jointTypeEnum)
  {
    if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
        jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
        jointTypeEnum.equals(JointTypeEnum.CGA) ||
        jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
        jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      return true;
    }
    return false;
  }

  /*
   * @author Sunit Bhalla
   *
   * @todo PE do we want to add max eccentricity at midball to catch weird looking joints?
   *
   * Returns true if eccentricity should be tested.  This is determined by the joint type and slice.
   */
  static boolean shouldMinimumEccentricityBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if (eccentricityIsTested(jointTypeEnum) == false)
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
        return true;
    }
    else if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
             jointTypeEnum.equals(JointTypeEnum.CGA) ||
             jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
             jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) || 
          sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20)) // lam
        return true;
    }

    return false;
  }


  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  private float getMinimumRegionOutlierThreshold(SliceNameEnum sliceNameEnum, Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float minimumRegionOutlier = 0.0f;

    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
       minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_LOWERPAD); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      minimumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20); 
    }
    else
    {
      Assert.expect(false, "illegal slice in gridArray");
    }
    return minimumRegionOutlier;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private float getMaximumRegionOutlierThreshold(SliceNameEnum sliceNameEnum, Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float maximumRegionOutlier = 0.0f;

    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
        maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
        maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_LOWERPAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
        maximumRegionOutlier =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
        maximumRegionOutlier =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PACKAGE);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19); 
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      maximumRegionOutlier =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20); 
    }
    else
    {
      Assert.expect(false, "illegal slice in gridArray");
    }
    return maximumRegionOutlier;
  }

  /**
   * Compute the percentage of joints with insufficient diameters
   *
   * @param jointInspectionDataObjects contains all the jointInspectionDataObjects in the measurementGroup
   *
   * @author Peter Esbensen
   */
  private float computePercentOfJointsWithInsufficientDiameters(Collection<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);

    int numberOfJointsWithSufficientDiameter = 0;
    int numberOfJointsWithInsufficientDiameter = 0;

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointMeasurement diameterMeasurement = GridArrayMeasurementAlgorithm.getDiameterMeasurement(jointInspectionData,
                                                                                                  sliceNameEnum);
      if (diameterMeasurement.isMeasurementValid())
      {
        float diameter = diameterMeasurement.getValue();
        Subtype subtype = jointInspectionData.getSubtype();
        float nominalDiameter = GridArrayMeasurementAlgorithm.getNominalDiameterAlgorithmSettingValue(subtype, sliceNameEnum);

        float diameterAsPercentOfNominal = 100f * diameter / nominalDiameter;

        float minimumMarginalDiameterThreshold =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL);

        if (diameterAsPercentOfNominal < minimumMarginalDiameterThreshold)
          ++numberOfJointsWithInsufficientDiameter;
        else
          ++numberOfJointsWithSufficientDiameter;
      }
      else
      {
        // count invalid measurements as being insufficient
        ++numberOfJointsWithInsufficientDiameter;
      }
    }

    int totalNumberOfJoints = numberOfJointsWithSufficientDiameter + numberOfJointsWithInsufficientDiameter;
    if (totalNumberOfJoints > 0)
      return 100f * (float)numberOfJointsWithInsufficientDiameter / (float)totalNumberOfJoints;

    // We must not have had enough diameters to get a valid measurement.  Just return zero.
    return 0f;
  }


  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */

  private void computeDiameterDifferencesVersusNeighbors(Collection<JointInspectionData> jointInspectionDataObjects,
                                                         SliceNameEnum sliceNameEnum,
                                                         List<Float> diameterDifferencesVersusNeighbors,
                                                         Map<SliceNameEnum, List<JointInspectionData>> sliceNameEnumToHipJointOutlierScoreFailuresMap)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(diameterDifferencesVersusNeighbors != null);
    Assert.expect(sliceNameEnumToHipJointOutlierScoreFailuresMap != null);

    final int MINIMUM_NUMBER_OF_NEIGHBORS_NEEDED = 3;

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // We only retest joint which is not flag as Hip Outlier Score failure
      if (sliceNameEnumToHipJointOutlierScoreFailuresMap.containsKey(sliceNameEnum) && 
          sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).contains(jointInspectionData))
      {
        continue;
      }
      
      Collection<JointInspectionData> neighboringJoints = jointInspectionData.getNeighboringJointsForOutlierTest();

      float diameterDifferenceVersusNeighborsInMM = 0.0f;

      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      List<Float> neighborDiameters = new ArrayList<Float>();

      JointMeasurement diameterMeasurement = GridArrayMeasurementAlgorithm.getDiameterMeasurement(jointInspectionData,
                                                                                                  sliceNameEnum);

      if (diameterMeasurement.isMeasurementValid() && (neighboringJoints.size() >  0))
      {
        float diameter = diameterMeasurement.getValue();
        
        for (JointInspectionData neighborJoint : neighboringJoints)
        {
          if (neighborJoint.getPad().isInspected() && 
              (sliceNameEnumToHipJointOutlierScoreFailuresMap.containsKey(sliceNameEnum) == false ||
               sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).contains(neighborJoint) == false))
          {
            //Siew Yeng - XCR-3210 - skip this neighbour joint if does not have measurement - might happened for mixed subtype(diff number of user-defined slice)
            if(GridArrayMeasurementAlgorithm.hasDiameterMeasurement(neighborJoint, sliceNameEnum))
            {
              JointMeasurement neighborDiameterMeasurement =
                      GridArrayMeasurementAlgorithm.getDiameterMeasurement(neighborJoint, sliceNameEnum);
              if (neighborDiameterMeasurement.isMeasurementValid())
              {
                neighborDiameters.add(neighborDiameterMeasurement.getValue());
              }
            }
          }
        }

        float[] neighborDiametersArray = ArrayUtil.convertFloatListToFloatArray(neighborDiameters);

        if (neighborDiameters.isEmpty() == false)
        {
          // Can't call median if no neighbor diameters exist.  Measurement will be marked as invalid a few lines down.
          float median = StatisticsUtil.median(neighborDiametersArray);
          diameterDifferenceVersusNeighborsInMM = diameter - median;
        }
      }

      boolean diameterDifferenceVersusNeighborsIsValid = true;

      // Mark as invalid if we don't have enough neighbors or if this joint was not found
      if ((diameterMeasurement.isMeasurementValid() == false) || (neighborDiameters.size() < MINIMUM_NUMBER_OF_NEIGHBORS_NEEDED))
        diameterDifferenceVersusNeighborsIsValid = false;

      if (diameterDifferenceVersusNeighborsIsValid)
      {
        diameterDifferencesVersusNeighbors.add(diameterDifferenceVersusNeighborsInMM);
      }
      
      if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS))
      {
          JointMeasurement diameterDifferenceMeasurement = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS);
          diameterDifferenceMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.MILLIMETERS);
          diameterDifferenceMeasurement.setSliceNameEnum(sliceNameEnum);
          diameterDifferenceMeasurement.setValue(diameterDifferenceVersusNeighborsInMM);
          diameterDifferenceMeasurement.setMeasurementValid(diameterDifferenceVersusNeighborsIsValid);
      }
      else
      {
          JointMeasurement diameterDifferenceMeasurement = new JointMeasurement(this,
                                                                            MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS,
                                                                            MeasurementUnitsEnum.MILLIMETERS,
                                                                            jointInspectionData.getPad(),
                                                                            sliceNameEnum,
                                                                            diameterDifferenceVersusNeighborsInMM,
                                                                            diameterDifferenceVersusNeighborsIsValid);
          jointInspectionResult.addMeasurement(diameterDifferenceMeasurement);
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void checkAllJointsForNeighborOutlier(Collection<JointInspectionData> jointInspectionDataObjects,
                                                float interquartileRange,
                                                SliceNameEnum sliceNameEnum,
                                                Map<Pad, ReconstructionRegion> padToRegionMap,
                                                Map<SliceNameEnum, List<JointInspectionData>> sliceNameEnumToHipJointOutlierScoreFailuresMap,
                                                boolean isMultiplassInspectionEnableForSubtype) throws DatastoreException
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(interquartileRange >= 0.0);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(padToRegionMap != null);
    Assert.expect(sliceNameEnumToHipJointOutlierScoreFailuresMap != null);

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // We only retest joint which is not flag as Hip Outlier Score failure
      if (sliceNameEnumToHipJointOutlierScoreFailuresMap.containsKey(sliceNameEnum) && 
          sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).contains(jointInspectionData))
      {
        continue;
      }
        
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      JointMeasurement diameterDifferenceFromNeighbors = jointInspectionResult.getJointMeasurement(sliceNameEnum,
          MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS);

      if (diameterDifferenceFromNeighbors.isMeasurementValid() == false)
      {
        // If the joint was not found, don't run open outlier.
        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getSubtype().getJointTypeEnum(), this))
          raiseOpenOutlierNotRunWarning(jointInspectionData, sliceNameEnum);
      }
      else
      {
        JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
        if (shouldMaximumNeighborOutlierBeTested(jointTypeEnum, sliceNameEnum, false) ||
            shouldMinimumNeighborOutlierBeTested(jointTypeEnum, sliceNameEnum))
        {
          float diameterDifferenceVersusNeighbors = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                              MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS).getValue();
          float outlierScore = 0.0f;

          final float IQR_PRECISION = 0.0005f;
          if (MathUtil.fuzzyEquals(interquartileRange, 0.0f, IQR_PRECISION))
          {
            // With fake images, there is no variability between the BGA balls, so set outlierScore to 0.
            outlierScore = 0.0f;
          }
          else
            outlierScore = diameterDifferenceVersusNeighbors / interquartileRange;

          JointMeasurement neighborOutlierMeasurement = null;
          if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE))
          {
              neighborOutlierMeasurement = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              neighborOutlierMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.INTER_QUARTILE_RANGE);
              neighborOutlierMeasurement.setSliceNameEnum(sliceNameEnum);
              neighborOutlierMeasurement.setValue(outlierScore);
          }
          else
          {
              neighborOutlierMeasurement = new JointMeasurement(this,
                                                                MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE,
                                                                MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
                                                                jointInspectionData.getPad(),
                                                                sliceNameEnum,
                                                                outlierScore);
              jointInspectionResult.addMeasurement(neighborOutlierMeasurement);
          }
          Assert.expect(neighborOutlierMeasurement != null);
          
          // Added by Seng Yew to create HIP measurement value
          if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE))
          {
            boolean isDumpHIPValueCSV = true;//Config.getInstance().getBooleanValue(SoftwareConfigEnum.DUMP_HIP_VALUE_CSV);
            if (isDumpHIPValueCSV)
            {
              JointMeasurement neighborOutlierMeasurementForPad = jointInspectionResult.getJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForMidball = jointInspectionResult.getJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForPackage = jointInspectionResult.getJointMeasurement(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              float packageValue = neighborOutlierMeasurementForPackage.getValue();
              float midballValue = neighborOutlierMeasurementForMidball.getValue();
              float padValue = neighborOutlierMeasurementForPad.getValue();
              float HIPValue = (midballValue*0.5F) + (packageValue*2.0F) - padValue;
              
              if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
              {
                  JointMeasurement HIPMeasurement = jointInspectionResult.getJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
                  HIPMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.NONE);
                  HIPMeasurement.setSliceNameEnum(SliceNameEnum.PAD);
                  HIPMeasurement.setValue(HIPValue);
              }
              else
              {
                  JointMeasurement HIPMeasurement = new JointMeasurement(this,
                                                                     MeasurementEnum.GRID_ARRAY_HIP_VALUE,
                                                                     MeasurementUnitsEnum.NONE,
                                                                     jointInspectionData.getPad(),
                                                                     SliceNameEnum.PAD,
                                                                     HIPValue);
                  jointInspectionResult.addMeasurement(HIPMeasurement);
              }
            }
          }
          
          // Added by Lee Herng to create HIP measurement value for User-Defined Slice 1
          if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE))
          {    
            boolean isDumpHIPValueCSV = true;//Config.getInstance().getBooleanValue(SoftwareConfigEnum.DUMP_HIP_VALUE_CSV);
            if (isDumpHIPValueCSV)
            {
              JointMeasurement neighborOutlierMeasurementForUserDefinedSlice1 = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForMidball = jointInspectionResult.getJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForPackage = jointInspectionResult.getJointMeasurement(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              float packageValue = neighborOutlierMeasurementForPackage.getValue();
              float midballValue = neighborOutlierMeasurementForMidball.getValue();
              float userDefinedSlice1Value = neighborOutlierMeasurementForUserDefinedSlice1.getValue();
              float HIPValue = (midballValue*0.5F) + (packageValue*2.0F) - userDefinedSlice1Value;
              
              if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
              {
                 JointMeasurement HIPMeasurement = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
                 HIPMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.NONE);
                 HIPMeasurement.setSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
                 HIPMeasurement.setValue(HIPValue); 
              }
              else
              {
                 JointMeasurement HIPMeasurement = new JointMeasurement(this,
                                                                        MeasurementEnum.GRID_ARRAY_HIP_VALUE,
                                                                        MeasurementUnitsEnum.NONE,
                                                                        jointInspectionData.getPad(),
                                                                        SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                        HIPValue);
                 jointInspectionResult.addMeasurement(HIPMeasurement); 
              }
            }
          }
          
          // Added by Lee Herng to create HIP measurement value for User-Defined Slice 2
          if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE))
          {
            boolean isDumpHIPValueCSV = true;//Config.getInstance().getBooleanValue(SoftwareConfigEnum.DUMP_HIP_VALUE_CSV);
            if (isDumpHIPValueCSV)
            {
              JointMeasurement neighborOutlierMeasurementForUserDefinedSlice1 = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForMidball = jointInspectionResult.getJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForUserDefinedSlice2 = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              float userDefinedSlice2Value = neighborOutlierMeasurementForUserDefinedSlice2.getValue();
              float midballValue = neighborOutlierMeasurementForMidball.getValue();
              float userDefinedSlice1Value = neighborOutlierMeasurementForUserDefinedSlice1.getValue();
              float HIPValue = (midballValue*0.5F) + (userDefinedSlice2Value*2.0F) - userDefinedSlice1Value;
              
              if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
              {
                 JointMeasurement HIPMeasurement = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
                 HIPMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.NONE);
                 HIPMeasurement.setSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
                 HIPMeasurement.setValue(HIPValue); 
              }
              else
              {
                 JointMeasurement HIPMeasurement = new JointMeasurement(this,
                                                                        MeasurementEnum.GRID_ARRAY_HIP_VALUE,
                                                                        MeasurementUnitsEnum.NONE,
                                                                        jointInspectionData.getPad(),
                                                                        SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                        HIPValue);
                 jointInspectionResult.addMeasurement(HIPMeasurement); 
              }
            }
          }
          
          // Added by Lee Herng to create HIP measurement value for User-Defined Slice 3
          if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
              jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE))
          {
            boolean isDumpHIPValueCSV = true;//Config.getInstance().getBooleanValue(SoftwareConfigEnum.DUMP_HIP_VALUE_CSV);
            if (isDumpHIPValueCSV)
            {
              JointMeasurement neighborOutlierMeasurementForUserDefinedSlice1 = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForMidball = jointInspectionResult.getJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              JointMeasurement neighborOutlierMeasurementForUserDefinedSlice3 = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              float userDefinedSlice3Value = neighborOutlierMeasurementForUserDefinedSlice3.getValue();
              float midballValue = neighborOutlierMeasurementForMidball.getValue();
              float userDefinedSlice1Value = neighborOutlierMeasurementForUserDefinedSlice1.getValue();
              float HIPValue = (midballValue*0.5F) + (userDefinedSlice3Value*2.0F) - userDefinedSlice1Value;
              
              if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
              {
                 JointMeasurement HIPMeasurement = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
                 HIPMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.NONE);
                 HIPMeasurement.setSliceNameEnum(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
                 HIPMeasurement.setValue(HIPValue); 
              }
              else
              {
                 JointMeasurement HIPMeasurement = new JointMeasurement(this,
                                                                        MeasurementEnum.GRID_ARRAY_HIP_VALUE,
                                                                        MeasurementUnitsEnum.NONE,
                                                                        jointInspectionData.getPad(),
                                                                        SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                        HIPValue);
                 jointInspectionResult.addMeasurement(HIPMeasurement); 
              }
            }
          }
          
          if(sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
             sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
          {
            if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
                jointInspectionResult.hasJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
                jointInspectionResult.hasJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE) &&
                jointInspectionResult.hasJointMeasurement(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE))
            {
              JointMeasurement neighborOutlierMeasurementReplacePad;
              JointMeasurement neighborOutlierMeasurementReplacePackage;
              AlgorithmSettingEnum algoSettingEnum = UserDefinedSliceheights.getAlgoSettingsEnum(sliceNameEnum);
              float offsetFromMidball = (Float)jointInspectionData.getSubtype().getAlgorithmSettingValue(algoSettingEnum);
              if(offsetFromMidball <= 0)
              {
                neighborOutlierMeasurementReplacePad = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
                neighborOutlierMeasurementReplacePackage = jointInspectionResult.getJointMeasurement(SliceNameEnum.PACKAGE, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              }
              else
              {
                neighborOutlierMeasurementReplacePad = jointInspectionResult.getJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
                neighborOutlierMeasurementReplacePackage = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);
              }

              boolean isDumpHIPValueCSV = true;//Config.getInstance().getBooleanValue(SoftwareConfigEnum.DUMP_HIP_VALUE_CSV);
              if (isDumpHIPValueCSV)
              {
                JointMeasurement neighborOutlierMeasurementForMidball = jointInspectionResult.getJointMeasurement(SliceNameEnum.MIDBALL, MeasurementEnum.GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE);

                float replacedPackageValue = neighborOutlierMeasurementReplacePackage.getValue();
                float midballValue = neighborOutlierMeasurementForMidball.getValue();
                float replacedPadValue = neighborOutlierMeasurementReplacePad.getValue();
                float HIPValue = (midballValue*0.5F) + (replacedPackageValue*2.0F) - replacedPadValue;

                if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
                {
                   JointMeasurement HIPMeasurement = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
                   HIPMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.NONE);
                   HIPMeasurement.setSliceNameEnum(sliceNameEnum);
                   HIPMeasurement.setValue(HIPValue); 
                }
                else
                {
                   JointMeasurement HIPMeasurement = new JointMeasurement(this,
                                                                          MeasurementEnum.GRID_ARRAY_HIP_VALUE,
                                                                          MeasurementUnitsEnum.NONE,
                                                                          jointInspectionData.getPad(),
                                                                          sliceNameEnum,
                                                                          HIPValue);
                   jointInspectionResult.addMeasurement(HIPMeasurement); 
                }
              }
            }
          }
          ReconstructionRegion reconstructionRegion = jointInspectionData.getInspectionRegion();
          checkNeighborOutlier(sliceNameEnum,
                               jointInspectionData,
                               reconstructionRegion,
                               neighborOutlierMeasurement);
        }
      }
    }

    // Added by Seng Yew to calculate IQR for HIP measurement value, so that we can do thresholding.
    boolean needToProcessHip = false;
    boolean needToProcessHipForUserDefinedSlice1 = false;
    boolean needToProcessHipForUserDefinedSlice2 = false;
    boolean needToProcessHipForUserDefinedSlice3 = false;
    boolean needToProcessHipForUserDefinedSlice = false;
    
    List<Float> hipValueList = new ArrayList<Float>();
    List<Float> userDefinedSlice1HipValueList = new ArrayList<Float>();
    List<Float> userDefinedSlice2HipValueList = new ArrayList<Float>();
    List<Float> userDefinedSlice3HipValueList = new ArrayList<Float>();
    List<Float> userDefinedSliceHipValueList = new ArrayList<Float>();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // We only retest joint which is not flag as Hip Outlier Score failure
      if (sliceNameEnumToHipJointOutlierScoreFailuresMap.containsKey(sliceNameEnum) && 
          sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).contains(jointInspectionData))
      {
        continue;
      }
        
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
      {
        JointMeasurement hipValue = jointInspectionResult.getJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
        hipValueList.add(hipValue.getValue());
        
        // Added by Lee Herng 26 July 2013 - In bare-board case, we may have only 1 slice that has HIP Value measurement.
        // IQR needs at least 2 HIP value in order to process.
        if (needToProcessHip == false && hipValueList.size() >= 2)  
          needToProcessHip = true;
      }
      
//      if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
//      {
//        JointMeasurement hipValue = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
//        userDefinedSlice1HipValueList.add(hipValue.getValue());
//        
//        // Added by Lee Herng 26 July 2013 - In bare-board case, we may have only 1 slice that has HIP Value measurement.
//        // IQR needs at least 2 HIP value in order to process.
//        if (needToProcessHipForUserDefinedSlice1 == false && userDefinedSlice1HipValueList.size() >= 2)
//          needToProcessHipForUserDefinedSlice1 = true;  
//      }
//      
//      if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
//      {
//        JointMeasurement hipValue = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
//        userDefinedSlice2HipValueList.add(hipValue.getValue());
//        
//        // Added by Lee Herng 26 July 2013 - In bare-board case, we may have only 1 slice that has HIP Value measurement.
//        // IQR needs at least 2 HIP value in order to process.
//        if (needToProcessHipForUserDefinedSlice2 == false && userDefinedSlice2HipValueList.size() >= 2)
//          needToProcessHipForUserDefinedSlice2 = true;  
//      }
//      
//      if (jointInspectionResult.hasJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
//      {
//        JointMeasurement hipValue = jointInspectionResult.getJointMeasurement(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
//        userDefinedSlice3HipValueList.add(hipValue.getValue());
//
//        // Added by Lee Herng 26 July 2013 - In bare-board case, we may have only 1 slice that has HIP Value measurement.
//        // IQR needs at least 2 HIP value in order to process.
//        if (needToProcessHipForUserDefinedSlice3 == false && userDefinedSlice3HipValueList.size() >= 2)
//          needToProcessHipForUserDefinedSlice3 = true;  
//      }
      
      if(sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19) ||
          sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
      {
        
        if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
        {
          JointMeasurement hipValue = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
          userDefinedSliceHipValueList.add(hipValue.getValue());

          // Added by Lee Herng 26 July 2013 - In bare-board case, we may have only 1 slice that has HIP Value measurement.
          // IQR needs at least 2 HIP value in order to process.
          if (needToProcessHipForUserDefinedSlice == false && userDefinedSliceHipValueList.size() >= 2)
            needToProcessHipForUserDefinedSlice = true;  
        }
      }
    }
    
    if (needToProcessHip)
    {
        checkHipOutlierScore(jointInspectionDataObjects,
                             hipValueList,
                             SliceNameEnum.PAD,
                             sliceNameEnumToHipJointOutlierScoreFailuresMap,
                             isMultiplassInspectionEnableForSubtype);
    }
    
//    if (needToProcessHipForUserDefinedSlice1)
//    {
//        checkHipOutlierScore(jointInspectionDataObjects,
//                             userDefinedSlice1HipValueList,
//                             SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
//                             jointForHipOutlierScoreFailure,
//                             isMultiplassInspectionEnableForSubtype);
//    }
//    
//    if (needToProcessHipForUserDefinedSlice2)
//    {
//        checkHipOutlierScore(jointInspectionDataObjects,
//                             userDefinedSlice2HipValueList,
//                             SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
//                             jointForHipOutlierScoreFailure,
//                             isMultiplassInspectionEnableForSubtype);
//    }
//    
//    if (needToProcessHipForUserDefinedSlice3)
//    {     
//      checkHipOutlierScore(jointInspectionDataObjects,
//                            userDefinedSlice3HipValueList,
//                            SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
//                            jointForHipOutlierScoreFailure,
//                            isMultiplassInspectionEnableForSubtype);
//    }
    
    if (needToProcessHipForUserDefinedSlice)
    {     
      checkHipOutlierScore(jointInspectionDataObjects,
                            userDefinedSliceHipValueList,
                            sliceNameEnum,
                            sliceNameEnumToHipJointOutlierScoreFailuresMap,
                            isMultiplassInspectionEnableForSubtype);
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private boolean isJointFailForHipOutlierScore(SliceNameEnum sliceNameEnum,
                                                JointInspectionData jointInspectionData)
  {
    Assert.expect(sliceNameEnum != null); 
    Assert.expect(jointInspectionData != null);
    
    boolean isJointFailForHipOutlierScore;
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    
    if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE))
    {
        JointMeasurement jointMeasurement = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
        JointIndictment jointIndictment = jointInspectionResult.getFailingIndictment(IndictmentEnum.OPEN, this, sliceNameEnum, jointMeasurement);
        if (jointIndictment == null)
            isJointFailForHipOutlierScore = false;
        else
            isJointFailForHipOutlierScore = true;
    }
    else
        isJointFailForHipOutlierScore = false;
    
    return isJointFailForHipOutlierScore;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void checkHipOutlierScore(Collection<JointInspectionData> jointInspectionDataObjects,
                                    List<Float> hipValueList,
                                    SliceNameEnum sliceNameEnum,
                                    Map<SliceNameEnum, List<JointInspectionData>> sliceNameEnumToHipJointOutlierScoreFailuresMap,
                                    boolean isMultiplassInspectionEnableForSubtype)
  {
      Assert.expect(jointInspectionDataObjects != null);
      Assert.expect(hipValueList != null);
      Assert.expect(sliceNameEnum != null);
      Assert.expect(sliceNameEnumToHipJointOutlierScoreFailuresMap != null);
      
      float[] hipValueArray = ArrayUtil.convertFloatListToFloatArray(hipValueList);
      float[] quartileRanges = StatisticsUtil.quartileRanges(hipValueArray);
      float interquartileRangeForHIP = quartileRanges[3] - quartileRanges[1];
      
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // We only retest joint which is not flag as Hip Outlier Score failure
        if (sliceNameEnumToHipJointOutlierScoreFailuresMap.containsKey(sliceNameEnum) && 
            sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).contains(jointInspectionData))
        {
          continue;
        }
          
        if (isJointFailForHipOutlierScore(sliceNameEnum, jointInspectionData))
          continue;
        
        JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
        if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_VALUE))
        {
          JointMeasurement hipValue = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_VALUE);
          if (hipValue.isMeasurementValid())
          {
            float hipOutlierScore = hipValue.getValue() / interquartileRangeForHIP;
            
            JointMeasurement hipOutlierMeasurement = null;
            if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE))
            {
                hipOutlierMeasurement = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE);
                hipOutlierMeasurement.setMeasurementUnitsEnum(MeasurementUnitsEnum.INTER_QUARTILE_RANGE);
                hipOutlierMeasurement.setSliceNameEnum(sliceNameEnum);
                hipOutlierMeasurement.setValue(hipOutlierScore);
            }
            else
            {
                hipOutlierMeasurement = new JointMeasurement(this,
                                                             MeasurementEnum.GRID_ARRAY_HIP_OUTLIER_SCORE,
                                                             MeasurementUnitsEnum.INTER_QUARTILE_RANGE,
                                                             jointInspectionData.getPad(),
                                                             sliceNameEnum,
                                                             hipOutlierScore);
                jointInspectionResult.addMeasurement(hipOutlierMeasurement);
            }
            Assert.expect(hipOutlierMeasurement != null);
            
            Subtype subtype = jointInspectionData.getSubtype();
            ReconstructionRegion reconstructionRegion = jointInspectionData.getInspectionRegion();
            float maxHipOutlierScore = getMaximumHipOutlierThreshold(sliceNameEnum, subtype);
            
            if (hipOutlierScore > maxHipOutlierScore)
            {
              JointIndictment openIndictment = jointInspectionResult.getFailingIndictment(IndictmentEnum.OPEN, this, sliceNameEnum, hipOutlierMeasurement);
              if (openIndictment == null)
              {
                  openIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                       this,
                                                       sliceNameEnum);
                  openIndictment.addFailingMeasurement(hipOutlierMeasurement);
                  jointInspectionResult.addIndictment(openIndictment);
              }
              AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                              jointInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              hipOutlierMeasurement,
                                                              hipOutlierScore);
              
              if (sliceNameEnumToHipJointOutlierScoreFailuresMap.containsKey(sliceNameEnum) == false)
                sliceNameEnumToHipJointOutlierScoreFailuresMap.put(sliceNameEnum, new ArrayList<JointInspectionData>());
              
              if (sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).contains(jointInspectionData) == false &&
                  isMultiplassInspectionEnableForSubtype)
              {
                sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).add(jointInspectionData);
              }
            }
            else
            {
              AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                              jointInspectionData,
                                                              sliceNameEnum,
                                                              reconstructionRegion,
                                                              hipOutlierMeasurement,
                                                              hipOutlierScore);
            }
            
            //Siew Yeng
            float minHipOutlierScore = getMinimumHipOutlierThreshold(sliceNameEnum, subtype);
            
            if (hipOutlierScore < minHipOutlierScore)
            {
              JointIndictment openIndictment = jointInspectionResult.getFailingIndictment(IndictmentEnum.OPEN, this, sliceNameEnum, hipOutlierMeasurement);
              if (openIndictment == null)
              {
                  openIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                       this,
                                                       sliceNameEnum);
                  openIndictment.addFailingMeasurement(hipOutlierMeasurement);
                  jointInspectionResult.addIndictment(openIndictment);
              }
              AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                              jointInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              hipOutlierMeasurement,
                                                              hipOutlierScore);
              
              if (sliceNameEnumToHipJointOutlierScoreFailuresMap.containsKey(sliceNameEnum) == false)
                sliceNameEnumToHipJointOutlierScoreFailuresMap.put(sliceNameEnum, new ArrayList<JointInspectionData>());
              
              if (sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).contains(jointInspectionData) == false &&
                  isMultiplassInspectionEnableForSubtype)
              {
                sliceNameEnumToHipJointOutlierScoreFailuresMap.get(sliceNameEnum).add(jointInspectionData);
              }
            }
            else
            {
              AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                              jointInspectionData,
                                                              sliceNameEnum,
                                                              reconstructionRegion,
                                                              hipOutlierMeasurement,
                                                              hipOutlierScore);
            }
          } 
        }
      }
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private float getMaximumHipOutlierThreshold(SliceNameEnum sliceNameEnum,
                                              Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
     
    float maximumHipOutlierThreshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
        maximumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20);
    }
    else
    {
        Assert.expect(false);
    }
    return maximumHipOutlierThreshold;
  }
  
  /**
   * @author Siew Yeng
   */
  private float getMinimumHipOutlierThreshold(SliceNameEnum sliceNameEnum,
                                              Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
     
    float minimumHipOutlierThreshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
        minimumHipOutlierThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20);
    }
    else
    {
        Assert.expect(false);
    }
    return minimumHipOutlierThreshold;
  }

  /**
   * @author Peter Esbensen
   */
  private boolean checkMaximumEccentricity(ReconstructedSlice reconstructedSlice,
                                           ReconstructedImages reconstructedImages,
                                           JointInspectionData jointInspectionData,
                                           BooleanRef jointPassed)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    Subtype subtype = jointInspectionData.getSubtype();

    float maxEccentricityThreshold = getMaximumEccentricityThreshold(sliceNameEnum, subtype);

    JointMeasurement eccentricityMeasurement = GridArrayMeasurementAlgorithm.getEccentricity(jointInspectionData, sliceNameEnum);

    float eccentricityValue = eccentricityMeasurement.getValue();

    if (eccentricityValue > maxEccentricityThreshold)
    {
      // The joint fails - it is not circular enough
      JointIndictment indictment = new JointIndictment(IndictmentEnum.OPEN, this, reconstructedSlice.getSliceNameEnum());
      indictment.addFailingMeasurement(eccentricityMeasurement);
      jointInspectionData.getJointInspectionResult().addIndictment(indictment);
      jointPassed.setValue(false);

      // post the failure diagnostic
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceNameEnum,
                                                      eccentricityMeasurement,
                                                      maxEccentricityThreshold);

      return false;
    }

    // The joint passes - it is circular enough.
    // ??? Should we have a nominal / upper threshold to detect if something looks too weird? (ie, shorts)

    // post the passing diagnostic
    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    eccentricityMeasurement,
                                                    maxEccentricityThreshold);
    return true;
  }


  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  private boolean checkMinimumEccentricity(ReconstructedSlice reconstructedSlice,
                                           ReconstructedImages reconstructedImages,
                                           JointInspectionData jointInspectionData,
                                           BooleanRef jointPassed)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    Subtype subtype = jointInspectionData.getSubtype();

    float minEccentricityThreshold = getMinimumEccentricityThreshold(sliceNameEnum, subtype);

    JointMeasurement eccentricityMeasurement = GridArrayMeasurementAlgorithm.getEccentricity(jointInspectionData, sliceNameEnum);

    float eccentricityValue = eccentricityMeasurement.getValue();

    if (eccentricityValue < minEccentricityThreshold)
    {
      // The joint fails - it is too circular.
      JointIndictment indictment = new JointIndictment(IndictmentEnum.OPEN, this, reconstructedSlice.getSliceNameEnum());
      indictment.addFailingMeasurement(eccentricityMeasurement);
      jointInspectionData.getJointInspectionResult().addIndictment(indictment);
      jointPassed.setValue(false);

      // post the failure diagnostic
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceNameEnum,
                                                      eccentricityMeasurement,
                                                      minEccentricityThreshold);

      return false;
    }

    // The joint passes - it is eccentric (non-circular) enough.
    // ??? Should we have a nominal / upper threshold to detect if something looks too weird? (ie, shorts)

    // post the passing diagnostic
    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    eccentricityMeasurement,
                                                    minEccentricityThreshold);
    return true;
  }


  /**
   * @author Patrick Lacz
   */
  private float measureEccentricity(ReconstructedSlice reconstructedSlice,
                                    ReconstructedImages reconstructedImages,
                                    JointInspectionData jointInspectionData,
                                    RegionOfInterest jointRoi,
                                    float diameterInMils)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointRoi != null);

    Image sliceImage = reconstructedSlice.getOrthogonalImage();

    // Set up the joint region so that in includes the entire measured region.
    // It would be nice here to have the x vs. y diameter measurements.. maybe sometime in the future.
    //    Subtype subtype = jointInspectionData.getSubtype();
    //    RegionOfInterest jointRoi = new RegionOfInterest(
    //        Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData,
    //        subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype)));

    // convert to pixels
    float diameterInPixels = diameterInMils / AlgorithmUtil.getMilsPerPixel();
    int intDiameterInPixels = (int)Math.round(Math.ceil(diameterInPixels)) + 6;

    // modify the region
    jointRoi.setWidthKeepingSameCenter(intDiameterInPixels);
    jointRoi.setHeightKeepingSameCenter(intDiameterInPixels);
    jointRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    // this magic number is based on the expectation that a circle or oval is expected to take up pi/4 of
    // its bounding rectangle in area.  Choose a pixel that should be about the median of the areas around
    // the actual joint.
    float percentileMeasurements[] = Statistics.getPercentiles(sliceImage, jointRoi, (float)(1.0 - 0.5*(1.0 - Math.PI/4.0)), 0.5f);

    Assert.expect(percentileMeasurements.length == 2);
    float backgroundMeasurement = percentileMeasurements[0];
    float medianMeasurement = percentileMeasurements[1];

    // We're going to rescale some values down to zero, and half of them to 1.0.
    // This should remove bias from asymmetric varying thickness inside the ball, as well as minimize most of the pixels
    // outside of the joint area.
    // Our formula for eccentricity expects a binary image.

    // we want an image where the shape we wish to measure is 1.f and the background 0.f.
    // We allow for pixels with values between 0 and 1.
    // with our eccentricty formula (over u20 + u02), we don't strictly need this to be a binary image, but we do want
    // it clamped in this way, so we'll just keep it as it is.
    Image jointImageCopy = Image.createCopy(sliceImage, jointRoi);
    Arithmetic.addConstantInPlace(jointImageCopy, -backgroundMeasurement);
    Arithmetic.multiplyByConstantInPlace(jointImageCopy, -1.f/(backgroundMeasurement-medianMeasurement));
    Threshold.clamp(jointImageCopy, 0.f, 1.f);

    // compute the moments
    Map<IntCoordinate, Double> mapOfMoments = Statistics.computeNormalizedCentralMoments(jointImageCopy, RegionOfInterest.createRegionFromImage(jointImageCopy), 2);

    // notify the jointImageCopy that we are done with it
    jointImageCopy.decrementReferenceCount();

    double u20 = mapOfMoments.get(new IntCoordinate(2, 0));
    double u11 = mapOfMoments.get(new IntCoordinate(1, 1));
    double u02 = mapOfMoments.get(new IntCoordinate(0, 2));

    // the value under the sqrt is always positive, so we don't worry about imaginary roots
    double sqrtTerm = Math.sqrt((u20 - u02) * (u20 - u02) + 4.0 * u11 * u11);
    double denominator = u20 + u02 - sqrtTerm;
    if (denominator < 0.000001)
      return 0.f;

    //double eccentricity = (u20 + u02 + sqrtTerm) / (u20 + u02 - sqrtTerm);
    // an alternative equation for eccentricity that seems to work with a more understandable range
    double eccentricity = sqrtTerm / (u20 + u02);
    return (float)eccentricity;
  }

  /**
   * @param jointInspectionDataObjects should be all the jointInspectionDataObjects in the measurementGroup
   *
   * @author Peter Esbensen
   */
  private void processPercentOfJointsWithInsufficientDiameters(MeasurementGroup measurementGroup,
                                                               List<JointInspectionData> jointInspectionDataObjects,
                                                               Map<Pad, ReconstructionRegion> padToRegionMap)  throws DatastoreException
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(padToRegionMap != null);

    List<SliceNameEnum> slicesToTestForPercentInsuffDiameter = determineSlicesForPercentOfJointsWithInsufficientDiameter(measurementGroup);

    for (SliceNameEnum sliceNameEnum : slicesToTestForPercentInsuffDiameter)
    {
      float percentOfJointsWithInsufficientDiameter = computePercentOfJointsWithInsufficientDiameters(jointInspectionDataObjects,
                                                                                                      sliceNameEnum);

      ComponentInspectionData componentInspectionData = measurementGroup.getComponentInspectionData();

      ComponentMeasurement percentOfJointsWithInsufficientDiameterMeasurement   = null;
      JointMeasurement jointPercentOfJointsWithInsufficientDiameterMeasurement  = null;
      float percentOfJointsWithInsufficientDiameterFloatValue = 0.0f;
      
      if (Config.isComponentLevelClassificationEnabled())
      {
        percentOfJointsWithInsufficientDiameterMeasurement = new ComponentMeasurement(this,
                                                                                      measurementGroup.getSubtype(),
                                                                                      MeasurementEnum.GRID_ARRAY_PERCENT_OF_JOINTS_WITH_INSUFFICIENT_DIAMETER,
                                                                                      MeasurementUnitsEnum.PERCENT,
                                                                                      componentInspectionData.getComponent(),
                                                                                      sliceNameEnum,
                                                                                      percentOfJointsWithInsufficientDiameter);
        componentInspectionData.getComponentInspectionResult().addMeasurement(percentOfJointsWithInsufficientDiameterMeasurement);
        
        percentOfJointsWithInsufficientDiameterFloatValue = percentOfJointsWithInsufficientDiameterMeasurement.getValue();
      }
      else
      {
        jointPercentOfJointsWithInsufficientDiameterMeasurement = new JointMeasurement(this, 
                                                                                       MeasurementEnum.GRID_ARRAY_PERCENT_OF_JOINTS_WITH_INSUFFICIENT_DIAMETER,
                                                                                       MeasurementUnitsEnum.PERCENT,
                                                                                       componentInspectionData.getPadOneJointInspectionData().getPad(),
                                                                                       sliceNameEnum,
                                                                                       percentOfJointsWithInsufficientDiameter);
        componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addMeasurement(jointPercentOfJointsWithInsufficientDiameterMeasurement);
        
        percentOfJointsWithInsufficientDiameterFloatValue = jointPercentOfJointsWithInsufficientDiameterMeasurement.getValue();
      }

      // get the maximum allowed percent of joints with marginal diameters
      Subtype subtype = measurementGroup.getSubtype();
      float maximumPercentOfJointsWithMarginalDiameters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER);

      // if we've exceeded the threshold, indict the component
      if (percentOfJointsWithInsufficientDiameter > maximumPercentOfJointsWithMarginalDiameters)
      {
        if (Config.isComponentLevelClassificationEnabled())
        {
          if (percentOfJointsWithInsufficientDiameterMeasurement != null)
          {
            ComponentIndictment openIndictment = new ComponentIndictment(IndictmentEnum.OPEN,
                                                                         this,
                                                                         sliceNameEnum,
                                                                         subtype,
                                                                         subtype.getJointTypeEnum());

            openIndictment.addFailingMeasurement(percentOfJointsWithInsufficientDiameterMeasurement);
            componentInspectionData.getComponentInspectionResult().addIndictment(openIndictment);
          }
        }
        else
        {
          if (jointPercentOfJointsWithInsufficientDiameterMeasurement != null)
          {
            JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                 this,
                                                                 sliceNameEnum);
            openIndictment.addFailingMeasurement(jointPercentOfJointsWithInsufficientDiameterMeasurement);
            componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(openIndictment);
          }
        }                
      }
    }
  }

  /**
   * @param jointInspectionDataObjects should be all the jointInspectionDataObjects in the measurementGroup
   *
   * @author Peter Esbensen
   */
  private void processDiameterDifferenceVersusNeighbors(MeasurementGroup measurementGroup,
                                                        List<JointInspectionData> jointInspectionDataObjects,
                                                        Map<Pad, ReconstructionRegion> padToRegionMap)  throws DatastoreException
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(padToRegionMap != null);

    List<Float> diameterDifferencesVersusNeighbors = new ArrayList<Float>();
    List<SliceNameEnum> slicesToTestForOpenOutlier = determineSlicesForOutlierTest(measurementGroup);
    Map<SliceNameEnum, List<JointInspectionData>> sliceNameEnumToHipJointOutlierScoreFailuresMap = new HashMap<SliceNameEnum, List<JointInspectionData>>();
    Map<SliceNameEnum, Pair<BooleanRef, IntegerRef>> sliceNameEnumToRetestHipJointFailurePairMap = new HashMap<SliceNameEnum, Pair<BooleanRef, IntegerRef>>();
    boolean firstPass = true;
    boolean retestJoint = false;
    boolean isMultiplassInspectionEnableForSubtype = false;
    
    try
    {
        // Subtype can have either single-pass or multi-pass inspection.
        Subtype subtype = jointInspectionDataObjects.iterator().next().getSubtype();
        if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MULTIPASS_INSPECTION))
        {
            Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MULTIPASS_INSPECTION);
            Assert.expect(value instanceof String);
            if (value.equals("Off"))
                isMultiplassInspectionEnableForSubtype = false;
            else if (value.equals("On"))
                isMultiplassInspectionEnableForSubtype = true;
            else
              Assert.expect(false, "Unexpected Multipass Inspection value.");
        }
        
        if (isMultiplassInspectionEnableForSubtype)
        {
            do
            {
                retestJoint = false;

                for (SliceNameEnum sliceNameEnum : slicesToTestForOpenOutlier)
                {
                    diameterDifferencesVersusNeighbors.clear();
                    computeDiameterDifferencesVersusNeighbors(measurementGroup.getInspectableJointInspectionDataObjects(),
                                                              sliceNameEnum,
                                                              diameterDifferencesVersusNeighbors,
                                                              sliceNameEnumToHipJointOutlierScoreFailuresMap);

                    int numberOfDiameterDifferences = diameterDifferencesVersusNeighbors.size();
                    if (numberOfDiameterDifferences > 1)
                    {
                        float[] diameterArray = new float[numberOfDiameterDifferences];
                        for (int i = 0; i < numberOfDiameterDifferences; ++i)
                        {
                          diameterArray[i] = diameterDifferencesVersusNeighbors.get(i);
                        }
                        float interquartileRange = 0.0f;

                        // We can only do the IQR statistics if we have at least two data points.
                        if (diameterArray.length > 1)
                        {
                          float[] quartileRanges = StatisticsUtil.quartileRanges(diameterArray);
                          interquartileRange = quartileRanges[3] - quartileRanges[1];
                        }
                        else
                        {
                          // If there are not enough datapoints, set IQR to 0 so that no open outlier calls will be made.
                          interquartileRange = 0.0f;
                        }

                        checkAllJointsForNeighborOutlier(jointInspectionDataObjects, 
                                                         interquartileRange, 
                                                         sliceNameEnum, 
                                                         padToRegionMap, 
                                                         sliceNameEnumToHipJointOutlierScoreFailuresMap,
                                                         isMultiplassInspectionEnableForSubtype);
                    }
                }

                // XCR-3562 Missing HiP Outlier Measurement for BGA User-Defined Slice When Turn on Multipass
                // Determine if we need to re-test joints that fail HiP
                if (firstPass)
                {
                  firstPass = false;
                  
                  for(Map.Entry<SliceNameEnum, List<JointInspectionData>> entry : sliceNameEnumToHipJointOutlierScoreFailuresMap.entrySet())
                  {
                    SliceNameEnum sliceNameEnum = entry.getKey();
                    List<JointInspectionData> failHipJointInspectionDataList = entry.getValue();
                    
                    if (sliceNameEnumToRetestHipJointFailurePairMap.containsKey(sliceNameEnum) == false)
                      sliceNameEnumToRetestHipJointFailurePairMap.put(sliceNameEnum, new Pair<BooleanRef, IntegerRef>(new BooleanRef(), new IntegerRef()));
                    
                    Pair<BooleanRef,IntegerRef> retestHipJointPair = sliceNameEnumToRetestHipJointFailurePairMap.get(sliceNameEnum);
                    if (failHipJointInspectionDataList.isEmpty() == false)
                    {
                      retestHipJointPair.getFirst().setValue(true);
                      retestHipJointPair.getSecond().setValue(failHipJointInspectionDataList.size());
                    }
                    else
                    {
                      retestHipJointPair.getFirst().setValue(false);
                      retestHipJointPair.getSecond().setValue(0);
                    }
                  }
                  
                  for(Map.Entry<SliceNameEnum, Pair<BooleanRef, IntegerRef>> entry : sliceNameEnumToRetestHipJointFailurePairMap.entrySet())
                  {
                    Pair<BooleanRef, IntegerRef> retestHipJointPair = entry.getValue();
                    if (retestHipJointPair.getFirst().getValue() == true)
                    {
                      retestJoint = true;
                      break;
                    }
                  }
                }
                else
                {
                  for(Map.Entry<SliceNameEnum, List<JointInspectionData>> entry : sliceNameEnumToHipJointOutlierScoreFailuresMap.entrySet())
                  {
                    SliceNameEnum sliceNameEnum = entry.getKey();
                    List<JointInspectionData> failHipJointInspectionDataList = entry.getValue();
                    
                    if (sliceNameEnumToRetestHipJointFailurePairMap.containsKey(sliceNameEnum) == false)
                      sliceNameEnumToRetestHipJointFailurePairMap.put(sliceNameEnum, new Pair<BooleanRef, IntegerRef>());
                    
                    Pair<BooleanRef,IntegerRef> retestHipJointPair = sliceNameEnumToRetestHipJointFailurePairMap.get(sliceNameEnum);
                    if (failHipJointInspectionDataList.isEmpty() == false && 
                        failHipJointInspectionDataList.size() > retestHipJointPair.getSecond().getValue())
                    {
                      retestHipJointPair.getFirst().setValue(true);
                      retestHipJointPair.getSecond().setValue(failHipJointInspectionDataList.size());
                    }
                    else
                    {
                      retestHipJointPair.getFirst().setValue(false);
                    }
                  }
                  
                  for(Map.Entry<SliceNameEnum, Pair<BooleanRef, IntegerRef>> entry : sliceNameEnumToRetestHipJointFailurePairMap.entrySet())
                  {
                    Pair<BooleanRef, IntegerRef> retestHipJointPair = entry.getValue();
                    if (retestHipJointPair.getFirst().getValue() == true)
                    {
                      retestJoint = true;
                      break;
                    }
                  }
                }
            } while (retestJoint == true);
        }
        else
        {
            for (SliceNameEnum sliceNameEnum : slicesToTestForOpenOutlier)
            {               
              diameterDifferencesVersusNeighbors.clear();
              computeDiameterDifferencesVersusNeighbors(measurementGroup.getInspectableJointInspectionDataObjects(),
                                                        sliceNameEnum,
                                                        diameterDifferencesVersusNeighbors,
                                                        sliceNameEnumToHipJointOutlierScoreFailuresMap);

              int numberOfDiameterDifferences = diameterDifferencesVersusNeighbors.size();
              if (numberOfDiameterDifferences > 1)
              {
                float[] diameterArray = new float[numberOfDiameterDifferences];
                for (int i = 0; i < numberOfDiameterDifferences; ++i)
                {
                  diameterArray[i] = diameterDifferencesVersusNeighbors.get(i);
                }
                float interquartileRange = 0.0f;

                // We can only do the IQR statistics if we have at least two data points.
                if (diameterArray.length > 1)
                {
                  float[] quartileRanges = StatisticsUtil.quartileRanges(diameterArray);
                  interquartileRange = quartileRanges[3] - quartileRanges[1];
                }
                else
                {
                  // If there are not enough datapoints, set IQR to 0 so that no open outlier calls will be made.
                  interquartileRange = 0.0f;
                }

                checkAllJointsForNeighborOutlier(jointInspectionDataObjects, 
                                                 interquartileRange, 
                                                 sliceNameEnum, 
                                                 padToRegionMap, 
                                                 sliceNameEnumToHipJointOutlierScoreFailuresMap,
                                                 isMultiplassInspectionEnableForSubtype);
              }
            }
        }
    }
    finally
    {
        if (diameterDifferencesVersusNeighbors != null)
        {
            diameterDifferencesVersusNeighbors.clear();
            diameterDifferencesVersusNeighbors = null;
        }
        
        if (slicesToTestForOpenOutlier != null)
        {
            slicesToTestForOpenOutlier.clear();
            slicesToTestForOpenOutlier = null;
        }
        
        if (sliceNameEnumToHipJointOutlierScoreFailuresMap != null)
        {
            sliceNameEnumToHipJointOutlierScoreFailuresMap.clear();
            sliceNameEnumToHipJointOutlierScoreFailuresMap = null;
        }
        
        if (sliceNameEnumToRetestHipJointFailurePairMap != null)
        {
          sliceNameEnumToRetestHipJointFailurePairMap.clear();
          sliceNameEnumToRetestHipJointFailurePairMap = null;
        }
    }
  }

  /**
   * Compute the Neighbor Outlier measurement and check all joints in the MeasurementGroup
   * to make sure the outlier score isn't too big.
   *
   * The measurement group should be all pins on the component.
   *
   * @author Peter Esbensen
   */
  protected void classifyMeasurementGroup(MeasurementGroup measurementGroup,
                                          Map<Pad, ReconstructionRegion> padToRegionMap) throws DatastoreException
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(padToRegionMap != null);

    //    System.out.println("Measurement group: ");
    //    System.out.println("  " + measurementGroup.getPads().toString());

    List<JointInspectionData> jointInspectionDataObjects = new ArrayList<JointInspectionData>();
    jointInspectionDataObjects.addAll(measurementGroup.getInspectableJointInspectionDataObjects());

    // do diameterDiffVersusNeighbors stuff
    processDiameterDifferenceVersusNeighbors(measurementGroup, jointInspectionDataObjects, padToRegionMap);

    // do marginal diameter stuff
    processPercentOfJointsWithInsufficientDiameters(measurementGroup, jointInspectionDataObjects, padToRegionMap);
    
    if(measurementGroup.getSubtype().getPanel().getProject().isBGAInspectionRegionSetTo1X1())
    {
      // do diameter region outlier stuff
      processRegionOutlierMeasurementsForDiameter(measurementGroup, jointInspectionDataObjects);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private List<SliceNameEnum> determineSlicesForPercentOfJointsWithInsufficientDiameter(MeasurementGroup measurementGroup)
  {
    Assert.expect(measurementGroup != null);

    List<SliceNameEnum> slicesToTestPercentInsuffDiameter = new ArrayList<SliceNameEnum>();
    JointTypeEnum jointTypeEnum = measurementGroup.getSubtype().getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      slicesToTestPercentInsuffDiameter.add(SliceNameEnum.MIDBALL);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      // not tested
    }
    else if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      // not tested
    }
    else if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      slicesToTestPercentInsuffDiameter.add(SliceNameEnum.MIDBALL);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      slicesToTestPercentInsuffDiameter.add(SliceNameEnum.MIDBALL);
    }
    else
      Assert.expect(false, "GridArray inspection family does not support this joint type!");

    return slicesToTestPercentInsuffDiameter;
  }


  /**
   * @author Sunit Bhalla
   */
  private List<SliceNameEnum> determineSlicesForOutlierTest(MeasurementGroup measurementGroup)
  {
    Assert.expect(measurementGroup != null);

    List<SliceNameEnum> slicesToTestOpenOutlier = new ArrayList<SliceNameEnum>();
    JointTypeEnum jointTypeEnum = measurementGroup.getSubtype().getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      slicesToTestOpenOutlier.add(SliceNameEnum.MIDBALL);
      slicesToTestOpenOutlier.add(SliceNameEnum.PAD);
      slicesToTestOpenOutlier.add(SliceNameEnum.PACKAGE);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      slicesToTestOpenOutlier.add(SliceNameEnum.PAD);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      slicesToTestOpenOutlier.add(SliceNameEnum.PAD);
      slicesToTestOpenOutlier.add(SliceNameEnum.PACKAGE);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      slicesToTestOpenOutlier.add(SliceNameEnum.MIDBALL);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      slicesToTestOpenOutlier.add(SliceNameEnum.MIDBALL);
      slicesToTestOpenOutlier.add(SliceNameEnum.PAD);
      slicesToTestOpenOutlier.add(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL); // lam
      slicesToTestOpenOutlier.add(SliceNameEnum.PACKAGE);
    }
    else
      Assert.expect(false, "GridArray inspection family does not support this joint type!");
    
    int totalUserDefinedSlice = getNumberOfUserDefinedSlice(measurementGroup.getSubtype());
    if (totalUserDefinedSlice >= 1)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    }
    if (totalUserDefinedSlice >= 2)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    }
    if (totalUserDefinedSlice >= 3)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    }
    if (totalUserDefinedSlice >= 4)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    }
    if (totalUserDefinedSlice >= 5)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    }
    if (totalUserDefinedSlice >= 6)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    }
    if (totalUserDefinedSlice >= 7)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    }
    if (totalUserDefinedSlice >= 8)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    }
    if (totalUserDefinedSlice >= 9)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    }
    if (totalUserDefinedSlice >= 10)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    }
    if (totalUserDefinedSlice >= 11)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    }
    if (totalUserDefinedSlice >= 12)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    }
    if (totalUserDefinedSlice >= 13)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    }
    if (totalUserDefinedSlice >= 14)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    }
    if (totalUserDefinedSlice >= 15)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    }
    if (totalUserDefinedSlice >= 16)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    }
    if (totalUserDefinedSlice >= 17)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    }
    if (totalUserDefinedSlice >= 18)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    }
    if (totalUserDefinedSlice >= 19)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    }
    if (totalUserDefinedSlice >= 20)
    {
       slicesToTestOpenOutlier.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    }

    return slicesToTestOpenOutlier;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  private void checkNeighborOutlier(SliceNameEnum sliceNameEnum,
                                    JointInspectionData jointInspectionData,
                                    ReconstructionRegion reconstructionRegion,
                                    JointMeasurement neighborOutlierMeasurement)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(neighborOutlierMeasurement != null);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    Subtype subtype = jointInspectionData.getSubtype();
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

    boolean voidingFailed = false;
    float voidingPercentage = 0.0f;

    if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT))
    {
      JointMeasurement voidingPercentageMeasurement = jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
      voidingPercentage = (float)voidingPercentageMeasurement.getValue();
    }

    float voidingLimitForNeighborOutlier =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER);

    if (voidingPercentage > voidingLimitForNeighborOutlier)
    {
      voidingFailed = true;
      if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getSubtype().getJointTypeEnum(), this))
        raiseOpenOutlierNotRunWarning(jointInspectionData, sliceNameEnum);
    }

    float outlierScore = neighborOutlierMeasurement.getValue();

    if (shouldMaximumNeighborOutlierBeTested(jointTypeEnum, sliceNameEnum, voidingFailed))
    {
      float maximumOpenOutlier = getMaximumNeighborOutlierThreshold(sliceNameEnum, subtype);

      if (outlierScore > maximumOpenOutlier)
      {
        JointIndictment openIndictment = jointInspectionResult.getFailingIndictment(IndictmentEnum.OPEN, this, sliceNameEnum, neighborOutlierMeasurement);
        if (openIndictment == null)
        {
            openIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                 this,
                                                 sliceNameEnum);
            openIndictment.addFailingMeasurement(neighborOutlierMeasurement);
            jointInspectionResult.addIndictment(openIndictment);
        }
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        neighborOutlierMeasurement,
                                                        maximumOpenOutlier);
      }
      else
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        neighborOutlierMeasurement,
                                                        maximumOpenOutlier);
      }
    }

    // For now, we still run minimum neighbor outlier even if the neighbors are voided.  The median of the neighbors is
    // used to create the open outlier score, so we can tolerate some neighbor voiding without creating false calls.  And,
    // of course, we'd like to avoid escapes.
    if (shouldMinimumNeighborOutlierBeTested(jointTypeEnum, sliceNameEnum))
    {
      float minimumOpenOutlier = getMinimumNeighborOutlierThreshold(sliceNameEnum, subtype);

      if (outlierScore < minimumOpenOutlier)
      {
        JointIndictment openIndictment = jointInspectionResult.getFailingIndictment(IndictmentEnum.OPEN, this, sliceNameEnum, neighborOutlierMeasurement);
        if (openIndictment == null)
        {
            openIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                             this,
                                             sliceNameEnum);
            openIndictment.addFailingMeasurement(neighborOutlierMeasurement);
            jointInspectionResult.addIndictment(openIndictment);
        }
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        neighborOutlierMeasurement,
                                                        minimumOpenOutlier);
      }
      else
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        neighborOutlierMeasurement,
                                                        minimumOpenOutlier);
      }
    }
  }

  /**
   * Raises an algorithm warning that open oulier is not being run.
   *
   * @author Sunit Bhalla
   */
  private void raiseOpenOutlierNotRunWarning(JointInspectionData jointInspectionData,
                                             SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    LocalizedString warningText = new LocalizedString("ALGDIAG_GRIDARRAY_OPEN_NEIGHBOR_OUTLIER_NOT_RUN_WARNING_KEY",
                                                      new Object[] { jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName() });
    AlgorithmUtil.raiseAlgorithmWarning(warningText);
  }

  /**
   * @author Peter Esbensen
   */
  private float getMaximumNeighborOutlierThreshold(SliceNameEnum sliceNameEnum,
                                                   Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float threshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20);  
    }
    else
    {
      Assert.expect(false, "illegal slice in gridArray open");
    }
    return threshold;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  private float getMinimumNeighborOutlierThreshold(SliceNameEnum sliceNameEnum,
                                                   Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float threshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PACKAGE);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20);  
    }
    else
    {
      Assert.expect(false, "illegal slice in gridArray open");
    }
    return threshold;
  }


  /**
   * @author Peter Esbensen plagiarizing Sunit
   */
  private float getMaximumEccentricityThreshold(SliceNameEnum sliceNameEnum, Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float threshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_LOWERPAD);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20);
    }
    else
    {
      Assert.expect(false, "illegal slice in gridArray open");
    }
    return threshold;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  private float getMinimumEccentricityThreshold(SliceNameEnum sliceNameEnum, Subtype subtype)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    float threshold = 0.0f;
    if (sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_PAD);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_LOWERPAD);  
    }
    else if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_MIDBALL);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19);
    }
    else if (sliceNameEnum.equals(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20);
    }
    else
    {
      Assert.expect(false, "illegal slice in gridArray open");
    }
    return threshold;
  }


  /**
   * @author Sunit Bhalla
   */
  private void learnMinimumRegionOutlierSetting(Subtype subtype,
                                                SliceNameEnum slice,
                                                AlgorithmSettingEnum algorithmSetting,
                                                MeasurementEnum measurementEnum,
                                                float minimumLearnedValueForSetting,
                                                float maximumLearnedValueForSetting) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(algorithmSetting != null);
    Assert.expect(measurementEnum != null);
    Assert.expect((slice == SliceNameEnum.PAD) || (slice == SliceNameEnum.BGA_LOWERPAD_ADDITIONAL)
                  ||(slice == SliceNameEnum.PACKAGE) || (slice == SliceNameEnum.MIDBALL)); // lam

    final float NUMBER_OF_IQRS_TO_USE = 4.0f;

    float[] measurements = subtype.getLearnedJointMeasurementValues(slice,
                                                                    measurementEnum,
                                                                    true,
                                                                    true);

    if (measurements.length == 0)
      return;

    // We can only do the IQR statistics if we have at least two data points.
    if (measurements.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRanges(measurements);
      float medianValue = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];
      float learnedValue = medianValue - (NUMBER_OF_IQRS_TO_USE * interQuartileRange);

      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
      {
        System.out.println("Minimum region outlier Total Samples: " + measurements.length +
                           " Median: " + medianValue + "IQR: " + interQuartileRange +
                           " learned value (before min/max testing): " + learnedValue);
      }

      // Make sure learned value is in the legal range
      learnedValue = Math.max(learnedValue, minimumLearnedValueForSetting);
      learnedValue = Math.min(learnedValue, maximumLearnedValueForSetting);

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, algorithmSetting, learnedValue);
    }
  }

  /**
   * @author Sunit Bhalla
   */
  private void learnMinimumDiameterSetting(Subtype subtype,
                                           SliceNameEnum slice,
                                           AlgorithmSettingEnum minimumDiameterSettingToSet,
                                           float minimumLearnedValueForSetting,
                                           float maximumLearnedValueForSetting) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(minimumDiameterSettingToSet != null);
    Assert.expect((slice == SliceNameEnum.PAD) || (slice == SliceNameEnum.BGA_LOWERPAD_ADDITIONAL) //lam
                  ||(slice == SliceNameEnum.PACKAGE) || (slice == SliceNameEnum.MIDBALL));

    final float NUMBER_OF_IQRS_TO_USE = 4.0f;

    // For each measurement stored in the database, divide by current nominal (threshold) to get the diameter as a
    // percent of the nominal.

    float[] measurementsInMM = subtype.getLearnedJointMeasurementValues(slice,
        MeasurementEnum.GRID_ARRAY_DIAMETER,
        true,
        true);

    if (measurementsInMM.length == 0)
      return;

    float nominalDiameterSettingInMM =
        GridArrayMeasurementAlgorithm.getNominalDiameterAlgorithmSettingValue(subtype, slice);

    // For each measurement stored in the database, divide by current nominal (threshold) to get the diameter as a
    // percent of the nominal.
    for (int i = 0; i < measurementsInMM.length; i++)
      measurementsInMM[i] = 100.0f * measurementsInMM[i] / nominalDiameterSettingInMM;

    // We can only do the IQR statistics if we have at least two data points.
    if (measurementsInMM.length > 1)
    {
      //Ngie Xing - XCR-2027 - Exclude Outlier
      float[] quartileRanges = AlgorithmUtil.quartileRangesWithExcludeOutlierDecision(measurementsInMM, false);
      float medianValue = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];
      float learnedValue = medianValue - (NUMBER_OF_IQRS_TO_USE * interQuartileRange);

      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.LEARNING_DEBUG))
      {
        System.out.println("Minimum diameter Total Samples: " + measurementsInMM.length +
                           " Median: " + medianValue + " IQR: " + interQuartileRange +
                           " learned value (before min/max testing): " + learnedValue);
      }


      // Make sure learned value is in the legal range
      learnedValue = Math.max(learnedValue, minimumLearnedValueForSetting);
      learnedValue = Math.min(learnedValue, maximumLearnedValueForSetting);

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, minimumDiameterSettingToSet, learnedValue);
    }
  }

  /**
   * @author Sunit Bhalla
   * @author Patrick Lacz
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {

    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // If we don't have any typical images (either passed in or in the database), we can't learn
    if (GridArrayMeasurementAlgorithm.algorithmCanBeLearned(subtype,
                                                            typicalBoardImages,
                                                            unloadedBoardImages) == false)
      return;


    final float MINIMUM_LEARNED_VALUE_FOR_MINIMUM_REGION_OUTLIER = 50.0f;
    final float MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_REGION_OUTLIER = 90.0f;

    final float MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING = 50.0f;
    final float MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING = 90.0f;

    JointTypeEnum jointType = subtype.getJointTypeEnum();
    if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
    }
    else if (jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.PAD,
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);
      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.PACKAGE,
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);

    }
    else if (jointType.equals(JointTypeEnum.CGA))
    {
      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.PAD,
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);

    }
    else if (jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.MIDBALL,
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);
    }
    else if (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.PAD,
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);
      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,  // lam
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);

      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.MIDBALL,
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);
      learnMinimumDiameterSetting(subtype,
                                  SliceNameEnum.PACKAGE,
                                  AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE,
                                  MINIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING,
                                  MAXIMUM_LEARNED_VALUE_FOR_MINIMUM_DIAMETER_SETTING);
    }
    else
      Assert.expect(false, "GridArray inspection family does not support this joint type!");

 }




  /**
   * @author Sunit Bhalla
   * This method sets all learned settings back to their defaults.
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private static int getNumberOfUserDefinedSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE);

    return Integer.parseInt(numberOfSlice);
  }
  
  /**
   * @author Peter Esbensen
   */
  protected void computeRegionOutlierMeasurementsForDiameter(Collection<JointInspectionData> jointInspectionDataObjects,
                                                           float[] validDiameters,
                                                           SliceNameEnum sliceNameEnum,
                                                           boolean isBGAInspectionRegionSetTo1X1)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(validDiameters != null);
    Assert.expect(sliceNameEnum != null);

    float expectedDiameter = 0.0f;

    if (validDiameters.length > 0)
    {
      expectedDiameter = StatisticsUtil.median(validDiameters);
    }

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      float diameter = MathUtil.convertMillimetersToMils(jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                                   MeasurementEnum.GRID_ARRAY_DIAMETER).getValue());

      float regionOutlierPercent = 0.0f;

      if (validDiameters.length > 0)
      {
        regionOutlierPercent = 100.0f * diameter / expectedDiameter;
      }

      if (shouldMinimumRegionOutlierBeTested(jointInspectionData.getJointTypeEnum(),
                                                                    sliceNameEnum))
      {

        jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                  MeasurementEnum.GRID_ARRAY_REGION_OUTLIER,
                                                                  MeasurementUnitsEnum.PERCENT,
                                                                  jointInspectionData.getPad(),
                                                                  sliceNameEnum,
                                                                regionOutlierPercent));
        
        if(isBGAInspectionRegionSetTo1X1)
          checkRegionOutlier(sliceNameEnum, 
                          jointInspectionData, 
                          jointInspectionData.getInspectionRegion(), 
                          jointInspectionData.getJointTypeEnum(), jointInspectionData.getSubtype(), new BooleanRef());
      }
    }
  }
  
  /**
   * @author Siew Yeng 
   */
  private void processRegionOutlierMeasurementsForDiameter(MeasurementGroup measurementGroup,
                                                           List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(jointInspectionDataObjects != null);

    List<SliceNameEnum> slicesToTestForOpenOutlier = determineSlicesForOutlierTest(measurementGroup);
    List<Float> validDiameters = new ArrayList<Float>();
    
    for(SliceNameEnum sliceNameEnum : slicesToTestForOpenOutlier)
    {
      validDiameters.clear();
      
      for(JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        //Siew Yeng - XCR-3210 - skip this if does not have diameter measurement - might happened on mixed subtype(diff number of user-defined slice)
        if(GridArrayMeasurementAlgorithm.hasDiameterMeasurement(jointInspectionData, sliceNameEnum))
        {
          JointMeasurement diameterMeasurement = GridArrayMeasurementAlgorithm.getDiameterMeasurement(jointInspectionData, sliceNameEnum);
          float diameter = MathUtil.convertMillimetersToMils(diameterMeasurement.getValue());
          if(diameterMeasurement.isMeasurementValid())
            validDiameters.add(diameter);
        }
      }
      
      computeRegionOutlierMeasurementsForDiameter(jointInspectionDataObjects,
                                                  ArrayUtil.convertFloatListToFloatArray(validDiameters),
                                                  sliceNameEnum,
                                                  true);
    }   
    
  }
}
