package com.axi.v810.business.imageAnalysis.gridArray;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Tests Grid Array joints for insufficient.
 *
 * @author Lim, Seng Yew - requested during Flex Doumen eval 2 by Xiao Jun
 */
public class GridArrayInsufficientAlgorithm extends Algorithm
{
  /**
   * @author Lim, Seng Yew
   */
  public GridArrayInsufficientAlgorithm(InspectionFamily gridArrayInspectionFamily)
  {
    super(AlgorithmEnum.INSUFFICIENT, InspectionFamilyEnum.GRID_ARRAY);

    Assert.expect(gridArrayInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    // Check to see if the joint is offset more than the median offset of other joints in the same region
    AlgorithmSetting minimumThicknessPercentPad = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      200.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumThicknessPercentPad,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    minimumThicknessPercentPad.filter(JointTypeEnum.CGA);
    minimumThicknessPercentPad.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumThicknessPercentPad.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    minimumThicknessPercentPad.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(minimumThicknessPercentPad);

    // Check to see if the joint is offset more than the median offset of other joints in the same region
    AlgorithmSetting minimumThicknessPercentPackage = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      200.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumThicknessPercentPackage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    minimumThicknessPercentPackage.filter(JointTypeEnum.CGA);
    minimumThicknessPercentPackage.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumThicknessPercentPackage.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    minimumThicknessPercentPackage.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(minimumThicknessPercentPackage);

    // Check to see if the joint is offset more than the median offset of other joints in the same region
    AlgorithmSetting minimumThicknessPercentMidball = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL,
      displayOrder++,
      70.0f, // default
      0.0f, // min
      200.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_INSUFFICIENT_(MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   minimumThicknessPercentMidball,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL);
    minimumThicknessPercentMidball.filter(JointTypeEnum.CGA);
    minimumThicknessPercentMidball.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    minimumThicknessPercentMidball.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    minimumThicknessPercentMidball.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(minimumThicknessPercentMidball);
  }

  /**
   * @author Lim, Seng Yew
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(GridArrayInspectionFamily.hasAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT));
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    JointTypeEnum jointTypeEnum = jointInspectionDataObjects.get(0).getJointTypeEnum();
    // the slices to use are defined in GridArray Measurement
    GridArrayMeasurementAlgorithm measurementAlgo = (GridArrayMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
    for (SliceNameEnum sliceNameEnum : measurementAlgo.getInspectionSliceOrder(subtype))

    {
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
      if (shouldInsufficientBeTested(jointTypeEnum, reconstructedSlice.getSliceNameEnum()))
      {
        ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
        AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                    jointInspectionData,
                                                    reconstructionRegion,
                                                    reconstructedSlice,
                                                    false);

          BooleanRef jointPassed = new BooleanRef(true);
          BooleanRef bypass = new BooleanRef(false);

          checkMinimumPercentageOfNominalThickness(jointInspectionData, reconstructedSlice, reconstructionRegion, jointPassed, bypass);
          if (bypass.getValue()==true)
            continue;

          // Show the pass/fail diagnostic
          AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                                  jointInspectionData,
                                                                  reconstructedImages.getReconstructionRegion(),
                                                                  reconstructedSlice.getSliceNameEnum(),
                                                                  jointPassed.getValue());
        }
      }
    }
  }

  /**
   * Check if there is minimum percentage of nominal thickness.
   *
   * @author Lim, Seng Yew
   */
  private void checkMinimumPercentageOfNominalThickness(JointInspectionData jointInspectionData,
                                              ReconstructedSlice reconstructedSlice,
                                              ReconstructionRegion reconstructionRegion,
                                              BooleanRef jointPassed,
                                              BooleanRef bypass)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = jointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    if (jointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL) == false)
    {
      bypass.setValue(true);
      return;
    }
    JointMeasurement percentOfNominalThicknessJointMeasurement = GridArrayMeasurementAlgorithm.
                                                         getThicknessAsPercentOfNominalMeasurement(jointInspectionData, reconstructedSlice.getSliceNameEnum());
    float percentOfNominalThickness = percentOfNominalThicknessJointMeasurement.getValue();

    float minimumPercentOfNominalThickness;
    if (sliceNameEnum == SliceNameEnum.PAD)
      minimumPercentOfNominalThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                                           GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD);
    else if (sliceNameEnum == SliceNameEnum.PACKAGE)
      minimumPercentOfNominalThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                                           GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE);
    else if (sliceNameEnum == SliceNameEnum.MIDBALL)
      minimumPercentOfNominalThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                                           GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL);
    else
    {
      bypass.setValue(true);
      return;
    }

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    if (percentOfNominalThickness < minimumPercentOfNominalThickness)
    {
      JointIndictment insufficientIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT,
                                                                 this,
                                                                 sliceNameEnum);
      insufficientIndictment.addFailingMeasurement(percentOfNominalThicknessJointMeasurement);
      jointInspectionResult.addIndictment(insufficientIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      percentOfNominalThicknessJointMeasurement,
                                                      minimumPercentOfNominalThickness);
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      percentOfNominalThicknessJointMeasurement,
                                                      minimumPercentOfNominalThickness);
    }
  }

  /*
   * @author Lim, Seng Yew
   * Returns true if insufficient should be tested.  This is determined by the joint type & slice enum.
   */
  public static boolean shouldInsufficientBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
      return true;
    return false;
  }
}
