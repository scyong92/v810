package com.axi.v810.business.imageAnalysis.gridArray;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Tests Grid Array joints for misalignment.  We check for two kinds of misalignment:
 * First, we check to make sure that the ball is not offset when compared against
 * its neighbors in the same region.  Second, we check to make sure that
 * the ball is not too offset from the CAD position.
 *
 * To compare a joint's offset against the others in the region, we are using
 * offsets of each joint from their CAD location.  We compute a median offset
 * for all joints and then compare the current joint against that median.  So
 * if most joints are down and to the right of their CAD locations, but then we
 * test a joint that is up and to the left of its CAD location, this should
 * detect that the offset is much different for that joint and should indict the
 * joint as being misaligned.
 *
 *
 * @author Peter Esbensen
 * @author Sunit Bhalla
 */
public class GridArrayMisalignmentAlgorithm extends Algorithm
{
  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  public GridArrayMisalignmentAlgorithm(InspectionFamily gridArrayInspectionFamily)
  {
    super(AlgorithmEnum.MISALIGNMENT, InspectionFamilyEnum.GRID_ARRAY);

    Assert.expect(gridArrayInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    // Check to see if the joint is offset more than the median offset of other joints in the same region
    AlgorithmSetting maximumOffsetFromRegionAverage = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(6.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(50.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_AVERAGE)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_AVERAGE)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_AVERAGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maximumOffsetFromRegionAverage,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumOffsetFromRegionAverage,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumOffsetFromRegionAverage,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maximumOffsetFromRegionAverage,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumOffsetFromRegionAverage,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumOffsetFromRegionAverage,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);

    addAlgorithmSetting(maximumOffsetFromRegionAverage);

    // Check to see if the joint is offset from CAD
    // Checking that component isn't shifted by a pitch
    AlgorithmSetting maximumOffsetFromCadPosition = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(20.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(50.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_CAD_POSITION)_KEY", // desc
        "HTML_DETAILED_DESC_GRIDARRAY_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_CAD_POSITION)_KEY", // detailed desc
        "IMG_DESC_GRIDARRAY_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_CAD_POSITION)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   maximumOffsetFromCadPosition,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   maximumOffsetFromCadPosition,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   maximumOffsetFromCadPosition,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   maximumOffsetFromCadPosition,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumOffsetFromCadPosition,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   maximumOffsetFromCadPosition,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_CAD);

    addAlgorithmSetting(maximumOffsetFromCadPosition);
    
    //Siew Yeng - XCR-2651
    addMeasurementEnums();
  }
  
  /**
   * @author Siew Yeng
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD);
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD);
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(GridArrayInspectionFamily.hasAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT));
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    JointTypeEnum jointTypeEnum = jointInspectionDataObjects.get(0).getJointTypeEnum();
    // the slices to use are defined in GridArray Measurement
    GridArrayMeasurementAlgorithm measurementAlgo = (GridArrayMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
    for (SliceNameEnum sliceNameEnum : measurementAlgo.getInspectionSliceOrder(subtype))

    {
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
      if (shouldMisalignmentBeTested(jointTypeEnum, reconstructedSlice.getSliceNameEnum()))
      {
        ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
        AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                    jointInspectionData,
                                                    reconstructionRegion,
                                                    reconstructedSlice,
                                                    false);

          BooleanRef jointPassed = new BooleanRef(true);

          //Siew Yeng - XCR-2651
          if(subtype.getPanel().getProject().isBGAInspectionRegionSetTo1X1() == false)
          {
            checkMaximumOffsetFromExpected(jointInspectionData, sliceNameEnum, reconstructionRegion, jointPassed);
          }
          
          checkMaximumOffsetFromCad(jointInspectionData, reconstructedSlice, reconstructionRegion, jointPassed);

          // Show the pass/fail diagnostic
          AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                                  jointInspectionData,
                                                                  reconstructedImages.getReconstructionRegion(),
                                                                  reconstructedSlice.getSliceNameEnum(),
                                                                  jointPassed.getValue());
        }
      }
    }
  }
  
  /**
   * Compute the region offset and check all joints in the MeasurementGroup
   * to make sure the outlier score isn't too big.
   * The measurement group should be all pins on the component.
   *
   * @author Siew Yeng
   */
  protected void classifyMeasurementGroup(MeasurementGroup measurementGroup,
                                          Map<Pad, ReconstructionRegion> padToRegionMap) throws DatastoreException
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(padToRegionMap != null);

    List<JointInspectionData> jointInspectionDataObjects = new ArrayList<JointInspectionData>();
    jointInspectionDataObjects.addAll(measurementGroup.getInspectableJointInspectionDataObjects());
    
    if(measurementGroup.getSubtype().getPanel().getProject().isBGAInspectionRegionSetTo1X1())
    {
      processRegionOutlierMeasurementsForLocation(measurementGroup, jointInspectionDataObjects);
    }
  }

  /**
   * Compare the given joint's location against the median location (relative to CAD)
   * of all the other joints in the region.
   *
   * @author Peter Esbensen
   * @author Siew Yeng - change parameter reconstructed slice to sliceNameEnum
   */
  private void checkMaximumOffsetFromExpected(JointInspectionData jointInspectionData,
                                              SliceNameEnum sliceNameEnum,
                                              ReconstructionRegion reconstructionRegion,
                                              BooleanRef jointPassed)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = jointInspectionData.getSubtype();

    JointMeasurement offsetFromExpectedMeasurementInMM = GridArrayMeasurementAlgorithm.
                                                         getOffsetFromExpectedMeasurement(jointInspectionData, sliceNameEnum);
    float offsetFromExpectedInMM = offsetFromExpectedMeasurementInMM.getValue();

    float maximumOffsetFromExpectedThresholdInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                                           GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    if (offsetFromExpectedInMM > maximumOffsetFromExpectedThresholdInMM)
    {
      JointIndictment misalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT,
                                                                 this,
                                                                 sliceNameEnum);
      misalignedIndictment.addFailingMeasurement(offsetFromExpectedMeasurementInMM);
      jointInspectionResult.addIndictment(misalignedIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      offsetFromExpectedMeasurementInMM,
                                                      maximumOffsetFromExpectedThresholdInMM);
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      offsetFromExpectedMeasurementInMM,
                                                      maximumOffsetFromExpectedThresholdInMM);
    }
  }

  /**
   * Check the given joint's location versus the CAD location.
   *
   * @author Peter Esbensen
   */
  private void checkMaximumOffsetFromCad(JointInspectionData jointInspectionData,
                                         ReconstructedSlice reconstructedSlice,
                                         ReconstructionRegion reconstructionRegion,
                                         BooleanRef jointPassed)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = jointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    JointMeasurement offsetFromCadMeasurementInMM =
      GridArrayMeasurementAlgorithm.getOffsetFromCADMeasurement(jointInspectionData, reconstructedSlice.getSliceNameEnum());
    float offsetFromCadInMM = offsetFromCadMeasurementInMM.getValue();

    float maximumOffsetFromCadThresholdInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
                                                                                      GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    if (offsetFromCadInMM > maximumOffsetFromCadThresholdInMM)
    {
      JointIndictment misalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT,
                                                                 this,
                                                                 sliceNameEnum);
      misalignedIndictment.addFailingMeasurement(offsetFromCadMeasurementInMM);
      jointInspectionResult.addIndictment(misalignedIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      offsetFromCadMeasurementInMM,
                                                      maximumOffsetFromCadThresholdInMM);
      jointPassed.setValue(false);

    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      offsetFromCadMeasurementInMM,
                                                      maximumOffsetFromCadThresholdInMM);
    }
  }

  /*
   * @author Sunit Bhalla
   * Returns true if the misalignment should be tested.  This is determined by the joint type.
   */
  public static boolean shouldMisalignmentBeTested(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(sliceNameEnum != null);

    if ((jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CGA) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) == false) &&
        (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) == false))
    {
      Assert.expect(false, "Joint type not supported");
    }

    if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PACKAGE))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.MIDBALL))
        return true;
    }
    if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      if (sliceNameEnum.equals(SliceNameEnum.PAD) ||sliceNameEnum.equals(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
        return true;
    }
    return false;
  }

  /**
   * @author Sunit Bhalla
   * This method learns Maximum Offset from CAD setting to be 1/2 of the pitch.
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    if (GridArrayMeasurementAlgorithm.algorithmCanBeLearned(subtype,
                                                            typicalBoardImages,
                                                            unloadedBoardImages))
    {
      ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
      ReconstructedImages reconstructedImages = reconstructedImages = imagesIterator.getNext();
      if (reconstructedImages != null)
      {
        ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
        List<JointInspectionData> jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);
        if (jointInspectionDataList.isEmpty() == false)
        {
          JointInspectionData jointInspectionData = jointInspectionDataList.get(0);
          int pitchInPixels = jointInspectionData.getPitchInPixels();

          final float MILLIMETERS_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
          float pitchInMM = pitchInPixels * MILLIMETERS_PER_PIXEL;

          subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION, 0.5f * pitchInMM);
        }
      }
    }
  }

  /**
   * @author Sunit Bhalla
   * This method sets all learned settings back to their defaults.
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION);
  }
  
  /**
   * @author Siew Yeng 
   */
  private void processRegionOutlierMeasurementsForLocation(MeasurementGroup measurementGroup,
                                                           List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(jointInspectionDataObjects != null);

    List<Float> xOffsetMeasurements = new ArrayList<Float>();
    List<Float> yOffsetMeasurements = new ArrayList<Float>();
    
    GridArrayMeasurementAlgorithm measurementAlgo = (GridArrayMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
    for (SliceNameEnum sliceNameEnum : measurementAlgo.getInspectionSliceOrder(measurementGroup.getSubtype()))
    {
      if(shouldMisalignmentBeTested(measurementGroup.getSubtype().getJointTypeEnum(), sliceNameEnum))
      {
        xOffsetMeasurements.clear();
        yOffsetMeasurements.clear();

        for(JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
          JointMeasurement xOffsetMeasurementInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                           MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD);
          if(xOffsetMeasurementInMM.isMeasurementValid())
            xOffsetMeasurements.add(xOffsetMeasurementInMM.getValue());

          JointMeasurement yOffsetMeasurementInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                           MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD);
          if(yOffsetMeasurementInMM.isMeasurementValid())
            yOffsetMeasurements.add(yOffsetMeasurementInMM.getValue());
        }

        computeRegionOutlierMeasurementsForLocation(jointInspectionDataObjects,
                                                    ArrayUtil.convertFloatListToFloatArray(xOffsetMeasurements),
                                                    ArrayUtil.convertFloatListToFloatArray(yOffsetMeasurements),
                                                    sliceNameEnum,
                                                    true);
      }
    }   
    
  }
  
  /**
   * @author Siew Yeng
   */
  protected void computeRegionOutlierMeasurementsForLocation(Collection<JointInspectionData> jointInspectionDataObjects,
                                                          float[] xOffsetsInMM,
                                                          float[] yOffsetsInMM,
                                                          SliceNameEnum sliceNameEnum,
                                                          boolean isBGAInspectionRegionSetTo1X1)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(xOffsetsInMM != null);
    Assert.expect(yOffsetsInMM != null);
    Assert.expect(xOffsetsInMM.length == yOffsetsInMM.length);
    Assert.expect(sliceNameEnum != null);

    float expectedXOffsetInMM = 0.0f;
    float expectedYOffsetInMM = 0.0f;

    if (xOffsetsInMM.length > 0)
      expectedXOffsetInMM = StatisticsUtil.median(xOffsetsInMM);

    if (yOffsetsInMM.length > 0)
      expectedYOffsetInMM = StatisticsUtil.median(yOffsetsInMM);

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      if (GridArrayMisalignmentAlgorithm.shouldMisalignmentBeTested(jointInspectionData.getJointTypeEnum(), sliceNameEnum))
      {
        float jointOffsetXInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                           MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD).getValue();
        float jointOffsetYInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                           MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD).getValue();

        float differenceFromExpectedXInMM = jointOffsetXInMM - expectedXOffsetInMM;
        float differenceFromExpectedYInMM = jointOffsetYInMM - expectedYOffsetInMM;

        float differenceFromAverageOffsetInMM = (float)Math.sqrt(differenceFromExpectedXInMM * differenceFromExpectedXInMM +
                                                                 differenceFromExpectedYInMM * differenceFromExpectedYInMM);

        jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                  MeasurementEnum.GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION,
                                                                  MeasurementUnitsEnum.MILLIMETERS,
                                                                  jointInspectionData.getPad(),
                                                                  sliceNameEnum,
                                                                  differenceFromAverageOffsetInMM));
        
        if(isBGAInspectionRegionSetTo1X1)
          checkMaximumOffsetFromExpected(jointInspectionData, sliceNameEnum, jointInspectionData.getInspectionRegion(), new BooleanRef());
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  protected static float computeRegionOutlierMeasurementsForLocationForEachJoint(JointInspectionResult jointInspectionResult,
                                                                                float expectedXOffsetInMM,
                                                                                float expectedYOffsetInMM,
                                                                                SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);

    float jointOffsetXInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                       MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD).getValue();
    float jointOffsetYInMM = jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                       MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD).getValue();

    float differenceFromExpectedXInMM = jointOffsetXInMM - expectedXOffsetInMM;
    float differenceFromExpectedYInMM = jointOffsetYInMM - expectedYOffsetInMM;

    float differenceFromAverageOffsetInMM = (float)Math.sqrt(differenceFromExpectedXInMM * differenceFromExpectedXInMM +
                                                             differenceFromExpectedYInMM * differenceFromExpectedYInMM);

    return differenceFromAverageOffsetInMM;
  }
}
