package com.axi.v810.business.imageAnalysis.gridArray;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Sunit Bhalla
 */
public class Test_GridArrayLearningFull extends AlgorithmUnitTest
{
  private static final String _BGA_SUBTYPE_1 = "bg00001_CollapsableBGA";

  /**
   * @author Sunit Bhalla
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_GridArrayLearningFull());
  }

  /**
   * @author Sunit Bhalla
   */
  public void test(BufferedReader is, PrintWriter os)
  {

    Test_GridArrayLearning testGridArrayLearning = new Test_GridArrayLearning();

    TimerUtil wholeTestTimer = new TimerUtil();
    wholeTestTimer.start();

    TimerUtil timer = new TimerUtil();

    boolean runFullTests = true;

    boolean printDebugInformation = false;

    testGridArrayLearning.setCollapsibleBGANominalSettingsLists();
    testGridArrayLearning.setGridArrayLearnedSettings();

    try
    {

      timer.start();
      Project project =  getProject("FAMILIES_ALL_RLV");
      timer.stop();
      if (printDebugInformation)
        System.out.println("The project created: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testGridArrayLearning.deleteAllLearnedData(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("deleteOldDatabaseData done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testGridArrayLearning.testLearnSubtype(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testLearnSubtype done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testDeleteLearnedData(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testDeleteLearnedData done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testIncrementalLearning(project);;
      timer.stop();
      if (printDebugInformation)
        System.out.println("testIncrementalLearning done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testMultipleImageRuns(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testMultipleImageRuns done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testGridArrayLearning.testLimitedDataWarning(project);      timer.stop();
      if (printDebugInformation)
        System.out.println("testLimitedDataWarning done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testBlackImages(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testBlackImages done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testWhiteImages(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testWhiteImages done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testOnlyUnloadedBoards(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("unloadedBoards done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testUnloadedBoardsWithTypicalDataInDatabase(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testUnloadedBoardsWithDataInDatabase done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testGridArrayLearning.testLearnSubtypesAsserts(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testAsserts done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      wholeTestTimer.stop();

      if (printDebugInformation)
        System.out.println("Time for test (min): " + wholeTestTimer.getElapsedTimeInMillis() / 60000.0);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
