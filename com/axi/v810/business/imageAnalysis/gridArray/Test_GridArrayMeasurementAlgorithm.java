package com.axi.v810.business.imageAnalysis.gridArray;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.XrayTesterException;

/**
 * @author Peter Esbensen
 */
class Test_GridArrayMeasurementAlgorithm extends AlgorithmUnitTest
{
  private final String BGA_SUBTYPE_1 = "bg00001_CollapsibleBGA";
  private final String REFDES_TO_TEST = "U34";
  private final String PAD_TO_TEST = "1";

  /**
   * @author Peter Esbensen
   */
  public Test_GridArrayMeasurementAlgorithm()
  {
    // do nothing
  }

  public void testJointType(Panel panel,
                            Project project,
                            JointTypeEnum jointTypeEnum,
                            String jointTypeString,
                            String imageSetName) throws XrayTesterException
  {
    Assert.expect(panel != null);
    Assert.expect(project != null);
    Assert.expect(jointTypeEnum != null);
    Assert.expect(jointTypeString != null);
    Assert.expect(imageSetName != null);

    // Change joint type for a package to a new joint type
    ComponentType componentType = panel.getComponentType("boardType1", "U27");
    CompPackage compPackage = componentType.getCompPackage();
    compPackage.setJointTypeEnum(jointTypeEnum);

    // Doing this will regenerate the test program.
    project.getTestProgram();

    String fullSubtypeName = "bg00001_" + jointTypeString;
    Subtype subtype = panel.getSubtype(fullSubtypeName);

    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   imageSetName,
                                                                   POPULATED_BOARD);
    imageSetDataList.add(imageSetData);
    generatePseudoImageSet(project, imageSetData);
    runSubtypeUnitTestInspection(project, subtype, imageSetDataList);
  }

  /**
   * Main test method.
   *
   * @author Peter Esbensen
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      // Right now, these tests don't really verify any test results, they
      // just make sure that things don't crash.
      Project project = getProject("FAMILIES_ALL_RLV");
      Panel panel = project.getPanel();
      Subtype subtype = panel.getSubtype(BGA_SUBTYPE_1);

      Pad pad = project.getPanel().getBoards().get(0).getComponent(REFDES_TO_TEST).getPad(PAD_TO_TEST);

      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = getNormalImageSet(project);
      imageSetDataList.add(imageSetData);
      learnSubtypeUnitTest(project, subtype, imageSetDataList, true, true);
      runSubtypeUnitTestInspection(project, subtype, imageSetDataList);

      testJointType(panel, project, JointTypeEnum.CGA, "CGA", "CGATestImageSet");
      testJointType(panel, project, JointTypeEnum.COLLAPSABLE_BGA, "CollapsibleBGA", "CollapsbaleBGATestImageSet");
      testJointType(panel, project, JointTypeEnum.NON_COLLAPSABLE_BGA, "NonCollapsibleBGA", "NonCollapsibleTestImageSet");
      testJointType(panel, project, JointTypeEnum.CHIP_SCALE_PACKAGE, "CSP", "CSPTestImageSet");
      testJointType(panel, project, JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR, "VariableHeightBGAConnector", "NexLevTestImageSet");

      imageSetData = createAndSelectBlackImageSet(project, pad, "testGridArrayBlackImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      imageSetData = createAndSelectWhiteImageSet(project, pad, "testGridArrayWhiteImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      imageSetData = createAndSelectPseudoRandomImageSet(project, pad, "testGridArrayPseudoRandomImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_GridArrayMeasurementAlgorithm());
  }
}
