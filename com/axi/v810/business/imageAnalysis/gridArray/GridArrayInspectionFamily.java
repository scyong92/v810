package com.axi.v810.business.imageAnalysis.gridArray;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for grid array joints such as BGAs and CGAs.
 *
 * @author Peter Esbensen
 */
public class GridArrayInspectionFamily extends InspectionFamily
{
  /**
   * @author Peter Esbensen
   */
  private final static String _ON = "On";
  private final static String _OFF = "Off";

  public GridArrayInspectionFamily()
  {
    super(InspectionFamilyEnum.GRID_ARRAY);

    Algorithm gridArrayMeasurementAlgorithm = new GridArrayMeasurementAlgorithm(InspectionFamilyEnum.GRID_ARRAY, this);
    addAlgorithm(gridArrayMeasurementAlgorithm);

    Algorithm gridArrayShortAlgorithm = new CircularShortAlgorithm(InspectionFamilyEnum.GRID_ARRAY);
    addAlgorithm(gridArrayShortAlgorithm);

    Algorithm gridArrayOpenAlgorithm = new GridArrayOpenAlgorithm(this);
    addAlgorithm(gridArrayOpenAlgorithm);

    Algorithm gridArrayMisalignmentAlgorithm = new GridArrayMisalignmentAlgorithm(this);
    addAlgorithm(gridArrayMisalignmentAlgorithm);

    Algorithm gridArrayVoidingAlgorithm = new GridArrayVoidingAlgorithm(this);
    addAlgorithm(gridArrayVoidingAlgorithm);
//
//    Algorithm gridArrayFloodVoidingAlgorithm = new FloodVoidingAlgorithm(InspectionFamilyEnum.GRID_ARRAY);
//    addAlgorithm(gridArrayFloodVoidingAlgorithm);

    Algorithm gridArrayInsufficientAlgorithm = new GridArrayInsufficientAlgorithm(this);
    addAlgorithm(gridArrayInsufficientAlgorithm);
  }

  /** @todo
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Collection<SliceNameEnum> inspectedSlices = new LinkedList<SliceNameEnum>();
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      inspectedSlices.add(SliceNameEnum.PAD);
      inspectedSlices.add(SliceNameEnum.PACKAGE);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      inspectedSlices.add(SliceNameEnum.PAD);
      inspectedSlices.add(SliceNameEnum.HIGH_SHORT);
    }
    else if(jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      inspectedSlices.add(SliceNameEnum.MIDBALL);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      inspectedSlices.add(SliceNameEnum.MIDBALL);
      inspectedSlices.add(SliceNameEnum.PAD);
      inspectedSlices.add(SliceNameEnum.PACKAGE);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      inspectedSlices.add(SliceNameEnum.MIDBALL);
      inspectedSlices.add(SliceNameEnum.PAD);
      inspectedSlices.add(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL);
      inspectedSlices.add(SliceNameEnum.PACKAGE);
    }
    else
    {
      Assert.expect(false, "Unexpected joint type: " + jointTypeEnum);
    }
    
    // Since UDS is an offset from midball, it makes sense to add UDS 1 and UDS 2
    // on those joint type that has midball slice.
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
        jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
        jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
        jointTypeEnum.equals(JointTypeEnum.CGA))
    {
      int numberOfUserDefinedSlice = Algorithm.getNumberOfSlice(subtype);
      if (numberOfUserDefinedSlice >= 1)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
      }
      if (numberOfUserDefinedSlice >= 2)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
      }
      if (numberOfUserDefinedSlice >= 3)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
      }
      if (numberOfUserDefinedSlice >= 4)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
      }
      if (numberOfUserDefinedSlice >= 5)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
      }
      if (numberOfUserDefinedSlice >= 6)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
      }
      if (numberOfUserDefinedSlice >= 7)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
      }
      if (numberOfUserDefinedSlice >= 8)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
      }
      if (numberOfUserDefinedSlice >= 9)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
      }
      if (numberOfUserDefinedSlice >= 10)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
      }
      if (numberOfUserDefinedSlice >= 11)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
      }
      if (numberOfUserDefinedSlice >= 12)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
      }
      if (numberOfUserDefinedSlice >= 13)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
      }
      if (numberOfUserDefinedSlice >= 14)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
      }
      if (numberOfUserDefinedSlice >= 15)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
      }
      if (numberOfUserDefinedSlice >= 16)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
      }
      if (numberOfUserDefinedSlice >= 17)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
      }
      if (numberOfUserDefinedSlice >= 18)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
      }
      if (numberOfUserDefinedSlice >= 19)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
      }
      if (numberOfUserDefinedSlice >= 20)
      {
        inspectedSlices.add(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
      }
    }

    return inspectedSlices;
  }

  /**
   * @return the default SliceNameEnum for the pad slice of the GridArray inspection family.
   * @author Matt Wharton
   * @author Sunit Bhalla
   *
   * This should really be called getLocatorInspectionSlice().
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    SliceNameEnum sliceForLocator = null;
    JointTypeEnum jointType = subtype.getJointTypeEnum();
    if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      sliceForLocator = SliceNameEnum.MIDBALL;
    }
    else if (jointType.equals(JointTypeEnum.CGA) ||
             jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
             jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      sliceForLocator = SliceNameEnum.PAD;
    }
    else if (jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      sliceForLocator = SliceNameEnum.MIDBALL;
    }
    else
      Assert.expect(false, "GridArray inspection family does not support this joint type!");

    return sliceForLocator;
  }

  /**
   * @return the slices that the short algorithm should be run on for this inspection family and subtype.  \
   * For Grid Array, this should always be the Pad slice.
   * @author Patrick Lacz
   * Due to customer request, added check all slices for short(optional setting in measurement tab).
   * @author Lam Wai Beng
   */
  public Collection<SliceNameEnum> getShortInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    JointTypeEnum jointType = subtype.getJointTypeEnum();

    Collection<SliceNameEnum> shortInspectionSlices = new LinkedList<SliceNameEnum>();

    // inspect short on all slices for BGA ?
    boolean useAllSlices=false;
    String checkAllSlices = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CHECK_ALL_SLICES_FOR_SHORT);
    if ( checkAllSlices.equals(_ON) ) useAllSlices = true;

    if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA))
    {
      shortInspectionSlices.add(SliceNameEnum.MIDBALL);
      shortInspectionSlices.add(SliceNameEnum.PAD);
      if ( useAllSlices ) //lam
        shortInspectionSlices.add(SliceNameEnum.PACKAGE);

    }
    else if (jointType.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      shortInspectionSlices.add(SliceNameEnum.PAD);
      if ( useAllSlices ) //lam
        shortInspectionSlices.add(SliceNameEnum.PACKAGE);
    }
    else if (jointType.equals(JointTypeEnum.CGA))
    {
      shortInspectionSlices.add(SliceNameEnum.PAD);
      shortInspectionSlices.add(SliceNameEnum.HIGH_SHORT);
    }
    else if (jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
    {
      shortInspectionSlices.add(SliceNameEnum.PAD);
      if ( useAllSlices ) { // lam
        shortInspectionSlices.add(SliceNameEnum.MIDBALL);
        shortInspectionSlices.add(SliceNameEnum.PACKAGE);
      }
     }
    else if (jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      shortInspectionSlices.add(SliceNameEnum.MIDBALL);
    }
    else
      Assert.expect(false, "GridArray inspection family does not support this joint type!");


    return shortInspectionSlices;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return true;
  }

}
