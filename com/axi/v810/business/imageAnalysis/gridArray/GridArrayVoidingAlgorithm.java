package com.axi.v810.business.imageAnalysis.gridArray;

import java.util.*;
import ij.ImagePlus;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * A void-detection algorithm based heavily upon the 5DX 8.3 (bga2voiding2) algorithm.
 * <p>
 * The algorithm works generally by computing an expected image of the region of interest
 * by looking at other joints in the inspection region. If, based on this expected image,
 * a large enough number of pixels are too light (showing less solder), the joint either is
 * indicted directly or counted towards a component level indictment.
 * <p>
 * <h3>The Inspected Region</h3><p>
 * The region that is examined for voiding begins with a circle at the measured location and diameter.
 * The <code>GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD/PACKAGE/MIDBALL</code> algorithm setting allows the user
 * to shrink this region. It is anticipated that this setting will be useful if diameter measurement
 * is inaccurate or more likely, this algorithm does not perform as well at the edges where our estimate
 * is less likely to be correct.
 * <p>
 * For consistency between joints, all images of the joints are converted to a standard size.
 * That size is based on the <code>GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER</code> setting for the slice multiplied
 * by the percent-of-diameter setting mentioned above. There is a hidden threshold, <code>GRID_ARRAY_VOIDING_IMAGE_SIZE_SCALING</code>,
 * which is intended to experiment with decreasing or increasing the resolution of this image. It is set to 1.0,
 * so the number of pixels in the standard size is the number we expect to see based on the nominal diameter and percent we are
 * testing of that diameter.
 * <p>
 * <h3>The Expected Image</h3><p>
 * To build the expected image, we make a median image of the 5 joints with the closest mean graylevel to the
 * inspected joint's graylevel. The way the algorithm is structured, all the median images are created in one step
 * before joints are examined individually. For example, if we have nine joints of different mean gray levels, they are sorted, then
 * five median images are produced, which are then used as the expected images.
 * <p>
 * <pre>
 *  A1 184                B1 172 \
 *  A2 200                A1 184  \
 *  A3 202                C1 186   --------->   Expected 1, used for B1, A1
 *  B1 172  ---------->   B2 192  /  median     Expected 2, used for C1
 *  B2 192   sort by      B3 195 <  of five     Expected 3, used for B2
 *  B3 195  graylevels    C3 197  \  images     Expected 4, used for B3, C3
 *  C1 186                A2 200   --------->   Expected 5, used for A2, C2, A3
 *  C2 200                C2 200  /
 *  C3 197                A3 204 /
 * </pre>
 * <p>
 * Notice that the joints that use the expected image is not a direct mapping. The joints are chosen
 * with the closest mean graylevel then the median image of those joints is used as the expected image.
 * <p>
 * Also notice that the joint being tested is one of the images used in the median. This was not the
 * case in the 5dx algorithm,  and it remains to be seen if the bias this causes is significant.
 * <p>
 * <h3>Voiding Pixels</h3><p>
 * Rather than doing a strict graylevel or percent-of threshold, a pixel is labeled as 'voiding' if the
 * thickness between the observed and expected graylevels is too high. The thickness is computed using
 * the solder table using the observed image as the 'background' and the expected image as the 'foreground'.
 * <p>
 * The user specifies a threshold of this thickness in the
 * <code>GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD/PACKAGE/MIDBALL</code> setting. The units are presumably
 * mils, however it is not labelled as such because the solder table is unreliable. Pixels registering a missing
 * thickness greater than this value are labeled as voiding.
 * <p>
 * Noise reduction may be used at this point. The <code>GRID_ARRAY_VOIDING_NOISE_REDUCTION</code> setting
 * determines how many cycles of opening (erode, dialate) are performed. Each cycle is performed with a 3x3
 * kernel,  so large features should stay about the same size while features that are only a couple pixels
 * wide will be removed. In general the area begins to look more 'squarish' after each level of opening.
 * <p>
 * We may consider changing the first level of noise reduction to a blur filter, then use erode-dialate cycles
 * for settings of 2+. Setting noise reduction to zero should always remove all noise reduction.
 * <p>
 * <h3>Indictments</h3>
 * Two percent-of-area thresholds are defined for each slice. The
 * <code>GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD/PACKAGE/MIDBALL</code> setting is generally the higher
 * of the two. If the number of pixels labeled as 'voiding' divided by the area of the measured area of the
 * joint exceeds this amount, the joint is indicted as Voiding.
 * <p>
 * The other threshold will classify the joint as "marginally" voiding. It does not lead directly to an indictment.
 * If the percent of area labeled as 'voiding' exceeds the <code>GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD/PACKAGE/MIDBALL</code>
 * threshold, it is counted towards a percent-of-failing-joints in the component metric. If that percent (joints failing the
 * lower threshold) exceeds <code>GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PAD/PACKAGE/MIDBALL</code>, the
 * component is indicted as Voiding. This indictment indicates that there may be a flaw in the process causing
 * higher than expected voiding.
 * <p>
 * There is also a volume-of-void threshold for each slice (GRID_ARRAY_VOIDING_FAIL_JOINT_VOLUME_PAD/PACKAGE/MIDBALL).
 * The volume of the void is the pixel area (in mm) multiplied by the sum of the missing thickness measurements for
 * the voiding pixels. This measurement appears to have better separation than the area-based measurement.
 * If either threshold fails, the joint is indicted. For simplicity, this threshold has not been added for marginal voids.
 * <p>

 * <h3>Learning</h3>
 * None of the algorithm settings specifically for voiding are learnt at this time. The Nominal Diameter for the
 * Measurement algorithm should be learned and influences this algorithm.
 *
 * @author Patrick Lacz
 */
public class GridArrayVoidingAlgorithm extends Algorithm
{

  // The number of images to use for the median-of-images
  private final int _NUMBER_OF_IMAGES_TO_SAMPLE = 5;
  private final int _MINIMUM_NUMBER_OF_IMAGES_TO_SAMPLE = 3;

  private final int _ALGORITHM_VERSION = 1;

  private final float _VOID_PIXEL_INTENSITY = 200.f;
  private final float _MEDIAN_VOID_PIXEL_INTENSITY = 175.f;   //55.f

  private final String _PERFORMANCE_MODE_FAST = "Fast";
  private final String _PERFORMANCE_MODE_ACCURATE = "Accurate";

   private final static String _THICKNESS_METHOD = "Thickness Method";
   private final static String _MEDIAN_METHOD = "Median Method";
   private final static String _FLOODFILL_METHOD = "FloodFill Method";
   private final static ArrayList<String> GRID_ARRAY_VOIDING_CHOICES = new ArrayList<String>(Arrays.asList(_THICKNESS_METHOD,_MEDIAN_METHOD,_FLOODFILL_METHOD));
   
   //Siew Yeng
   public static final boolean _GRID_ARRAY_VOIDING_INSPECTION_TIME_STAMP = false;
   private TimerUtil _gridArrayVoidingInspectionTime = null;
 
  /**
   * @author Patrick Lacz
   */
  public GridArrayVoidingAlgorithm(InspectionFamily gridArrayInspectionFamily)
  {
    super(AlgorithmEnum.VOIDING, InspectionFamilyEnum.GRID_ARRAY);

    Assert.expect(gridArrayInspectionFamily != null);

    //Siew Yeng
    if(_GRID_ARRAY_VOIDING_INSPECTION_TIME_STAMP)
    {
      _gridArrayVoidingInspectionTime = new TimerUtil(); 
      _gridArrayVoidingInspectionTime.reset();
    }
    
    int displayOrder = 0;


     //Jack
     AlgorithmSetting voidDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD, // setting enum
        displayOrder++, // display order,
        _THICKNESS_METHOD,
        GRID_ARRAY_VOIDING_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_VOIDING_(VOIDING_METHOD)_KEY", // description URL Key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(VOIDING_METHOD)_KEY", // detailed description URL Key
        "IMG_DESC_GRIDARRAY_VOIDING_(VOIDING_METHOD)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(voidDetection);
    
      AlgorithmSetting voidFloodFillSensitivityThreshold = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD,
        displayOrder++,
        2, // default value
        1, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_THRESHOLD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_THRESHOLD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_THRESHOLD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(voidFloodFillSensitivityThreshold);

    AlgorithmSetting voidBorderThreshold = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD,
        displayOrder++,
        2.8f, // default value
        2.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_VOIDING_(VOIDING_BORDER_THRESHOLD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(VOIDING_BORDER_THRESHOLD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(VOIDING_BORDER_THRESHOLD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(voidBorderThreshold);

    AlgorithmSetting percentOfDiameterPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_PAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    percentOfDiameterPadSetting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterPadSetting);
    
    AlgorithmSetting percentOfDiameterLowerPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_LOWERPAD,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_LOWERPAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_LOWERPAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_LOWERPAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterLowerPadSetting.filter(JointTypeEnum.CGA);
    percentOfDiameterLowerPadSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterLowerPadSetting.filter(JointTypeEnum.COLLAPSABLE_BGA);
    percentOfDiameterLowerPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(percentOfDiameterLowerPadSetting);
   
    AlgorithmSetting percentOfDiameterMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_MIDBALL)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_MIDBALL)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_MIDBALL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterMidballSetting.filter(JointTypeEnum.CGA);
    percentOfDiameterMidballSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterMidballSetting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterMidballSetting);

    AlgorithmSetting percentOfDiameterPackageSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_PACKAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_PACKAGE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_PACKAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterPackageSetting.filter(JointTypeEnum.CGA);
    percentOfDiameterPackageSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    percentOfDiameterPackageSetting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterPackageSetting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice1Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice1Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    percentOfDiameterUserDefinedSlice1Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice1Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice1Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice1Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice2Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice2Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    percentOfDiameterUserDefinedSlice2Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice2Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice2Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice2Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice3Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice3Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    percentOfDiameterUserDefinedSlice3Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice3Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice3Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice3Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice4Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice4Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    percentOfDiameterUserDefinedSlice4Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice4Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice4Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice4Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice5Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice5Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    percentOfDiameterUserDefinedSlice5Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice5Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice5Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice5Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice6Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice6Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    percentOfDiameterUserDefinedSlice6Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice6Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice6Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice6Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice7Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice7Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    percentOfDiameterUserDefinedSlice7Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice7Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice7Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice7Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice8Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice8Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    percentOfDiameterUserDefinedSlice8Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice8Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice8Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice8Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice9Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice9Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    percentOfDiameterUserDefinedSlice9Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice9Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice9Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice9Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice10Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice10Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    percentOfDiameterUserDefinedSlice10Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice10Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice10Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice10Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice11Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice11Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    percentOfDiameterUserDefinedSlice11Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice11Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice11Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice11Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice12Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice12Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    percentOfDiameterUserDefinedSlice12Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice12Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice12Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice12Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice13Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice13Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    percentOfDiameterUserDefinedSlice13Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice13Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice13Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice13Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice14Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice14Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    percentOfDiameterUserDefinedSlice14Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice14Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice14Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice14Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice15Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice15Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    percentOfDiameterUserDefinedSlice15Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice15Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice15Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice15Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice16Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice16Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    percentOfDiameterUserDefinedSlice16Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice16Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice16Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice16Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice17Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice17Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    percentOfDiameterUserDefinedSlice17Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice17Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice17Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice17Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice18Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice18Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    percentOfDiameterUserDefinedSlice18Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice18Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice18Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice18Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice19Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice19Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    percentOfDiameterUserDefinedSlice19Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice19Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice19Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice19Setting);
    
    AlgorithmSetting percentOfDiameterUserDefinedSlice20Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20,
        displayOrder++,
        90.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    percentOfDiameterUserDefinedSlice20Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    percentOfDiameterUserDefinedSlice20Setting.filter(JointTypeEnum.CGA);
    percentOfDiameterUserDefinedSlice20Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    percentOfDiameterUserDefinedSlice20Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(percentOfDiameterUserDefinedSlice20Setting);

    AlgorithmSetting thicknessThresholdPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_PAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(thicknessThresholdPadSetting);
    
    AlgorithmSetting thicknessThresholdLowerPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_LOWERPAD,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_LOWERPAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_LOWERPAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_LOWERPAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdLowerPadSetting.filter(JointTypeEnum.CGA);
    thicknessThresholdLowerPadSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    thicknessThresholdLowerPadSetting.filter(JointTypeEnum.COLLAPSABLE_BGA);
    thicknessThresholdLowerPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(thicknessThresholdLowerPadSetting);

    AlgorithmSetting thicknessThresholdMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_MIDBALL,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_MIDBALL)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_MIDBALL)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_MIDBALL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdMidballSetting.filter(JointTypeEnum.CGA);
    thicknessThresholdMidballSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdMidballSetting);

    AlgorithmSetting thicknessThresholdPackageSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PACKAGE,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_PACKAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_PACKAGE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_PACKAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdPackageSetting.filter(JointTypeEnum.CGA);
    thicknessThresholdPackageSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(thicknessThresholdPackageSetting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice1Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice1Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    thicknessThresholdUserDefinedSlice1Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice1Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice1Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice2Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice2Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    thicknessThresholdUserDefinedSlice2Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice2Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice2Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice3Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice3Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    thicknessThresholdUserDefinedSlice3Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice3Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice3Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice4Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice4Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    thicknessThresholdUserDefinedSlice4Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice4Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice4Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice5Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice5Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    thicknessThresholdUserDefinedSlice5Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice5Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice5Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice6Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice6Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    thicknessThresholdUserDefinedSlice6Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice6Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice6Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice7Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice7Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    thicknessThresholdUserDefinedSlice7Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice7Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice7Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice8Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice8Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    thicknessThresholdUserDefinedSlice8Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice8Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice8Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice9Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice9Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    thicknessThresholdUserDefinedSlice9Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice9Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice9Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice10Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice10Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    thicknessThresholdUserDefinedSlice10Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice10Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice10Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice11Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice11Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    thicknessThresholdUserDefinedSlice11Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice11Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice11Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice12Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice12Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    thicknessThresholdUserDefinedSlice12Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice12Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice12Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice13Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice13Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    thicknessThresholdUserDefinedSlice13Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice13Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice13Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice14Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice14Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    thicknessThresholdUserDefinedSlice14Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice14Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice14Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice15Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice15Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    thicknessThresholdUserDefinedSlice15Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice15Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice15Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice16Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice16Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    thicknessThresholdUserDefinedSlice16Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice16Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice16Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice17Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice17Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    thicknessThresholdUserDefinedSlice17Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice17Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice17Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice18Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice18Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    thicknessThresholdUserDefinedSlice18Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice18Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice18Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice19Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice19Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    thicknessThresholdUserDefinedSlice19Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice19Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice19Setting);
    
    AlgorithmSetting thicknessThresholdUserDefinedSlice20Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    thicknessThresholdUserDefinedSlice20Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    thicknessThresholdUserDefinedSlice20Setting.filter(JointTypeEnum.CGA);
    thicknessThresholdUserDefinedSlice20Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(thicknessThresholdUserDefinedSlice20Setting);

    // Joint Voiding Area Threshold
    // If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as
    // being voided. This value is usually larger than the component-level voiding.
    AlgorithmSetting failJointPercentPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);

     // Individual Joint Voiding Area Threshold
    // If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as
    // being voided. This value is usually larger than the component-level voiding.
    AlgorithmSetting failIndividualJointPercentPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD,
        displayOrder++,
        40.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   failJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);

    failJointPercentPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failJointPercentPadSetting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   failIndividualJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);

    failIndividualJointPercentPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failIndividualJointPercentPadSetting);
    
    //Siew Yeng - XCR-3515
    AlgorithmSetting failIndividualJointDiameterPercentPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PAD,
        displayOrder++,
        100.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   failIndividualJointDiameterPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointDiameterPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failIndividualJointDiameterPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointDiameterPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);

    failIndividualJointDiameterPercentPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failIndividualJointDiameterPercentPadSetting);
    
    // Added by Lee Herng 15 Nov 2011 - Joint Voiding Area Threshold
    // If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as
    // being voided. This value is usually larger than the component-level voiding.
    AlgorithmSetting failJointPercentLowerPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_LOWERPAD,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_LOWERPAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_LOWERPAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_LOWERPAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);

     // Added by Lee Herng 15 Nov 2011 - Individual Joint Voiding Area Threshold
    // If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as
    // being voided. This value is usually larger than the component-level voiding.
    AlgorithmSetting failIndividualJointPercentLowerPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_LOWERPAD,
        displayOrder++,
        40.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_LOWERPAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_LOWERPAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_LOWERPAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentLowerPadSetting,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);

    failJointPercentLowerPadSetting.filter(JointTypeEnum.CGA);
    failJointPercentLowerPadSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failJointPercentLowerPadSetting.filter(JointTypeEnum.COLLAPSABLE_BGA);
    failJointPercentLowerPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failJointPercentLowerPadSetting);

    //Jack Hwee-individual void
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentLowerPadSetting,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);

    failIndividualJointPercentLowerPadSetting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentLowerPadSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failIndividualJointPercentLowerPadSetting.filter(JointTypeEnum.COLLAPSABLE_BGA);
    failIndividualJointPercentLowerPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failIndividualJointPercentLowerPadSetting);

    AlgorithmSetting failJointPercentMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL,
        displayOrder++,
        30.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_MIDBALL)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_MIDBALL)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_MIDBALL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);

    //Jack Hwee-individual void
     AlgorithmSetting failIndividualJointPercentMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_MIDBALL)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_MIDBALL)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_MIDBALL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentMidballSetting.filter(JointTypeEnum.CGA);
    failJointPercentMidballSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentMidballSetting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentMidballSetting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentMidballSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentMidballSetting);
    
    //Siew Yeng - XCR-3515
    AlgorithmSetting failIndividualJointDiameterPercentMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_MIDBALL,
        displayOrder++,
        100.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_MIDBALL)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_MIDBALL)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_MIDBALL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointDiameterPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointDiameterPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointDiameterPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    failIndividualJointDiameterPercentMidballSetting.filter(JointTypeEnum.CGA);
    failIndividualJointDiameterPercentMidballSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointDiameterPercentMidballSetting);
    
    AlgorithmSetting failJointPercentPackageSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_PACKAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_PACKAGE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_PACKAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);

  //Jack Hwee-individual void
     AlgorithmSetting failIndividualJointPercentPackageSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE,
        displayOrder++,
        40.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PACKAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PACKAGE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PACKAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failJointPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentPackageSetting.filter(JointTypeEnum.CGA);
    failJointPercentPackageSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failJointPercentPackageSetting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentPackageSetting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentPackageSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failIndividualJointPercentPackageSetting);
    
    //Siew Yeng - XCR-3515
    AlgorithmSetting failIndividualJointDiameterPercentPackageSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PACKAGE,
        displayOrder++,
        100.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_PACKAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_PACKAGE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_DIAMETER_PERCENT_PACKAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointDiameterPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failIndividualJointDiameterPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointDiameterPercentPackageSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT);
    failIndividualJointDiameterPercentPackageSetting.filter(JointTypeEnum.CGA);
    failIndividualJointDiameterPercentPackageSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failIndividualJointDiameterPercentPackageSetting);
    
    // Added by Lee Herng - Grid Array User Defined Slice 1
    AlgorithmSetting failJointPercentUserDefinedSlice1Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice1Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice1Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice1Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice1Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice1Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice1Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice1Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice1Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice1Setting);
    
    // Added by Lee Herng - Grid Array User Defined Slice 2
    AlgorithmSetting failJointPercentUserDefinedSlice2Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice2Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice2Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice2Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice2Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice2Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice2Setting);

    //Jack Hwee-individual void
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice2Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice2Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice2Setting);
    
    // Added by Lee Herng - Grid Array User Defined Slice 3
    AlgorithmSetting failJointPercentUserDefinedSlice3Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice3Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice3Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice3Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice3Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice3Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice3Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice3Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice3Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice3Setting);
    
    //swee-yee.wong
    AlgorithmSetting failJointPercentUserDefinedSlice4Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice4Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice4Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice4Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice4Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice4Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice4Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice4Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice4Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice4Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice5Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice5Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice5Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice5Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice5Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice5Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice5Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice5Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice5Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice5Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice6Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice6Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice6Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice6Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice6Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice6Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice6Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice6Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice6Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice6Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice7Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice7Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice7Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice7Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice7Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice7Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice7Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice7Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice7Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice7Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice8Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice8Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice8Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice8Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice8Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice8Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice8Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice8Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice8Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice8Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice9Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice9Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice9Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice9Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice9Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice9Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice9Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice9Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice9Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice9Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice10Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice10Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice10Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice10Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice10Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice10Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice10Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice10Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice10Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice10Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice11Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice11Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice11Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice11Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice11Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice11Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice11Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice11Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice11Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice11Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice12Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice12Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice12Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice12Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice12Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice12Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice12Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice12Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice12Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice12Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice13Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice13Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice13Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice13Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice13Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice13Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice13Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice13Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice13Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice13Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice14Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice14Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice14Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice14Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice14Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice14Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice14Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice14Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice14Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice14Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice15Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice15Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice15Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice15Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice15Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice15Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice15Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice15Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice15Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice15Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice16Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice16Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice16Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice16Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice16Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice16Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice16Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice16Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice16Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice16Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice17Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice17Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice17Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice17Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice17Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice17Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice17Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice17Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice17Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice17Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice18Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice18Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice18Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice18Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice18Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice18Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice18Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice18Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice18Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice18Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice19Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice19Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice19Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice19Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice19Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice19Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice19Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice19Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice19Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice19Setting);
    
    AlgorithmSetting failJointPercentUserDefinedSlice20Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_JOINT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failJointPercentUserDefinedSlice20Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);

    AlgorithmSetting failIndividualJointPercentUserDefinedSlice20Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failIndividualJointPercentUserDefinedSlice20Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);

    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failJointPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failJointPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failJointPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    failJointPercentUserDefinedSlice20Setting.filter(JointTypeEnum.CGA);
    failJointPercentUserDefinedSlice20Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failJointPercentUserDefinedSlice20Setting);

    //Jack Hwee-individual void
     gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failIndividualJointPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failIndividualJointPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failIndividualJointPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);
    failIndividualJointPercentUserDefinedSlice20Setting.filter(JointTypeEnum.CGA);
    failIndividualJointPercentUserDefinedSlice20Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    addAlgorithmSetting(failIndividualJointPercentUserDefinedSlice20Setting);

    // Component Voiding Threshold
    // The Percent-of-area to count the joint towards the component-fail threshold. Generally this
    // percentage is lower than the Joint Voiding Threshold.
    AlgorithmSetting countToComponentFailPercentPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(countToComponentFailPercentPadSetting);
    
    // Added by Lee Herng 15 Nov 2011 - Component Voiding Threshold
    // The Percent-of-area to count the joint towards the component-fail threshold. Generally this
    // percentage is lower than the Joint Voiding Threshold.
    AlgorithmSetting countToComponentFailPercentLowerPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_LOWERPAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_LOWERPAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_LOWERPAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentLowerPadSetting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentLowerPadSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentLowerPadSetting.filter(JointTypeEnum.COLLAPSABLE_BGA);
    countToComponentFailPercentLowerPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(countToComponentFailPercentLowerPadSetting);

    AlgorithmSetting countToComponentFailPercentMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_MIDBALL)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_MIDBALL)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_MIDBALL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentMidballSetting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentMidballSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentMidballSetting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentMidballSetting);

    AlgorithmSetting countToComponentFailPercentPackageSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_PACKAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_PACKAGE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_PACKAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentPackageSetting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentPackageSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    countToComponentFailPercentPackageSetting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentPackageSetting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice1Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_1)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_1)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_1)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice1Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    countToComponentFailPercentUserDefinedSlice1Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice1Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice1Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice1Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice2Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_2)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_2)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_2)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice2Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    countToComponentFailPercentUserDefinedSlice2Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice2Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice2Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice2Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice3Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_3)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_3)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_3)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice3Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    countToComponentFailPercentUserDefinedSlice3Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice3Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice3Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice3Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice4Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_4)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_4)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_4)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice4Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    countToComponentFailPercentUserDefinedSlice4Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice4Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice4Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice4Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice5Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_5)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_5)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_5)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice5Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    countToComponentFailPercentUserDefinedSlice5Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice5Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice5Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice5Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice6Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_6)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_6)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_6)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice6Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    countToComponentFailPercentUserDefinedSlice6Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice6Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice6Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice6Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice7Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_7)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_7)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_7)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice7Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    countToComponentFailPercentUserDefinedSlice7Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice7Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice7Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice7Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice8Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_8)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_8)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_8)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice8Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    countToComponentFailPercentUserDefinedSlice8Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice8Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice8Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice8Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice9Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_9)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_9)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_9)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice9Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    countToComponentFailPercentUserDefinedSlice9Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice9Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice9Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice9Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice10Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_10)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_10)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_10)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice10Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    countToComponentFailPercentUserDefinedSlice10Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice10Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice10Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice10Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice11Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_11)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_11)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_11)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice11Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    countToComponentFailPercentUserDefinedSlice11Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice11Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice11Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice11Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice12Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_12)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_12)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_12)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice12Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    countToComponentFailPercentUserDefinedSlice12Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice12Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice12Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice12Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice13Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_13)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_13)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_13)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice13Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    countToComponentFailPercentUserDefinedSlice13Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice13Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice13Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice13Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice14Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_14)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_14)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_14)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice14Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    countToComponentFailPercentUserDefinedSlice14Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice14Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice14Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice14Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice15Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_15)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_15)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_15)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice15Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    countToComponentFailPercentUserDefinedSlice15Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice15Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice15Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice15Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice16Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_16)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_16)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_16)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice16Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    countToComponentFailPercentUserDefinedSlice16Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice16Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice16Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice16Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice17Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_17)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_17)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_17)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice17Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    countToComponentFailPercentUserDefinedSlice17Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice17Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice17Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice17Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice18Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_18)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_18)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_18)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice18Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    countToComponentFailPercentUserDefinedSlice18Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice18Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice18Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice18Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice19Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_19)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_19)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_19)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice19Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    countToComponentFailPercentUserDefinedSlice19Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice19Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice19Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice19Setting);
    
    AlgorithmSetting countToComponentFailPercentUserDefinedSlice20Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20,
        displayOrder++,
        10.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_20)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_20)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(COUNT_TO_COMPONENT_FAIL_PERCENT_USER_DEFINED_SLICE_20)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    countToComponentFailPercentUserDefinedSlice20Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    countToComponentFailPercentUserDefinedSlice20Setting.filter(JointTypeEnum.CGA);
    countToComponentFailPercentUserDefinedSlice20Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    countToComponentFailPercentUserDefinedSlice20Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(countToComponentFailPercentUserDefinedSlice20Setting);

    // Fail Component Percent
    // If >= this percent of joints of the component are classified with the process-level voiding threshold (above),
    // the component should be indicted as voided.
    AlgorithmSetting failComponentPercentPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PAD,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CGA,
                                                                   failComponentPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failComponentPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failComponentPercentPadSetting,
                                                                   SliceNameEnum.PAD,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);

    failComponentPercentPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failComponentPercentPadSetting);
    
    AlgorithmSetting failComponentPercentLowerPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_LOWERPAD,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_LOWERPAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_LOWERPAD)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_LOWERPAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR,
                                                                   failComponentPercentLowerPadSetting,
                                                                   SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);

    failComponentPercentLowerPadSetting.filter(JointTypeEnum.CGA);
    failComponentPercentLowerPadSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentLowerPadSetting.filter(JointTypeEnum.COLLAPSABLE_BGA);
    failComponentPercentLowerPadSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(failComponentPercentLowerPadSetting);

    AlgorithmSetting failComponentPercentMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_MIDBALL,
        displayOrder++,
        30.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_MIDBALL)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_MIDBALL)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_MIDBALL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentMidballSetting,
                                                                   SliceNameEnum.MIDBALL,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentMidballSetting.filter(JointTypeEnum.CGA);
    failComponentPercentMidballSetting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentMidballSetting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentMidballSetting);
    
    AlgorithmSetting maximumNumberOfPinFailedMidballVoid = new AlgorithmSetting(
        AlgorithmSettingEnum.MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        1000.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_VOIDING_(MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    maximumNumberOfPinFailedMidballVoid.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    addAlgorithmSetting(maximumNumberOfPinFailedMidballVoid);
    
    AlgorithmSetting failComponentPercentPackageMidballSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PACKAGE,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_PACKAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_PACKAGE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_PACKAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentPackageMidballSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.NON_COLLAPSABLE_BGA,
                                                                   failComponentPercentPackageMidballSetting,
                                                                   SliceNameEnum.PACKAGE,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentPackageMidballSetting.filter(JointTypeEnum.CGA);
    failComponentPercentPackageMidballSetting.filter(JointTypeEnum.CHIP_SCALE_PACKAGE);
    failComponentPercentPackageMidballSetting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentPackageMidballSetting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice1Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice1Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice1Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice1Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice1Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice1Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice1Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice2Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice2Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice2Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice2Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice2Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice2Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice2Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice3Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice3Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice3Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice3Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice3Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice3Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice3Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice4Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice4Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice4Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice4Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice4Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice4Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice4Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice5Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice5Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice5Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice5Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice5Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice5Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice5Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice6Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice6Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice6Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice6Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice6Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice6Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice6Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice7Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice7Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice7Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice7Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice7Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice7Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice7Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice8Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice8Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice8Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice8Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice8Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice8Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice8Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice9Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice9Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice9Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice9Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice9Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice9Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice9Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice10Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice10Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice10Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice10Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice10Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice10Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice10Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice11Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice11Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice11Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice11Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice11Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice11Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice11Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice12Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice12Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice12Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice12Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice12Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice12Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice12Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice13Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice13Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice13Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice13Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice13Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice13Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice13Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice14Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice14Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice14Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice14Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice14Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice14Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice14Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice15Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice15Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice15Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice15Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice15Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice15Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice15Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice16Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice16Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice16Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice16Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice16Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice16Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice16Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice17Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice17Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice17Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice17Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice17Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice17Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice17Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice18Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice18Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice18Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice18Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice18Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice18Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice18Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice19Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice19Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice19Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice19Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice19Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice19Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice19Setting);
    
    AlgorithmSetting failComponentPercentUserDefinedSlice20Setting = new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20,
        displayOrder++,
        20.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    failComponentPercentUserDefinedSlice20Setting.setSpecificSlice(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CHIP_SCALE_PACKAGE,
                                                                   failComponentPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    gridArrayInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.COLLAPSABLE_BGA,
                                                                   failComponentPercentUserDefinedSlice20Setting,
                                                                   SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                                                   MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    failComponentPercentUserDefinedSlice20Setting.filter(JointTypeEnum.CGA);
    failComponentPercentUserDefinedSlice20Setting.filter(JointTypeEnum.NON_COLLAPSABLE_BGA);
    failComponentPercentUserDefinedSlice20Setting.filter(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR);
    addAlgorithmSetting(failComponentPercentUserDefinedSlice20Setting);

    // Noise Reduction
    // How much to try to compensate for noise.
    // 0 = no noise reduction
    // 1 = apply blur
    // 2+ = apply blur & NR-1 cycles of opening (dilate/erode cycles) to the void pixels image.
    // The opening will remove smaller features from the image and generally make the regions appear
    // more blocky than without NR.
    //  I expect that this value will normally be 0 to 2.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION,
        displayOrder++,
        2, // default value
        0, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));

    // 0 means use original algo which is, if number of joints is less than 5 then set as 3, otherwise set as 5.
    // 1 means bypass median filter. Initial study shows that it worsen the GRnR value.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NUMBER_OF_IMAGES_TO_SAMPLE,
        displayOrder++,
        0, // default value
        0, // minimum value
        10, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_VOIDING_(NUMBER_OF_IMAGES_TO_SAMPLE)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(NUMBER_OF_IMAGES_TO_SAMPLE)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(NUMBER_OF_IMAGES_TO_SAMPLE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));

    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveDiagnoticVoidImageAlgorithmSetting(displayOrder, _ALGORITHM_VERSION));
    
    addMeasurementEnums();

  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);

    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT);  
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT); //Siew Yeng - XCR-3515
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      _componentMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_COMPONENT_MIDBALL_VOIDING);
      _componentMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    }
    else
    {
      _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_COMPONENT_MIDBALL_VOIDING);
      _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    }
  }


   /**
   * @author Jack Hwee
   */
  static public boolean isThicknessMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD);

    if (voidingChoice.equals(_THICKNESS_METHOD))
    {
      return true;
    }

    return false;
  }

   /**
   * @author Jack Hwee
   */
  static public boolean isMedianMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD);

    if (voidingChoice.equals(_MEDIAN_METHOD))
    {
      return true;
    }

    return false;
  }

  /**
   * @author Patrick Lacz
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(GridArrayInspectionFamily.hasAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT));

    //Siew Yeng
    if(_GRID_ARRAY_VOIDING_INSPECTION_TIME_STAMP)
    {
      _gridArrayVoidingInspectionTime.reset();
      _gridArrayVoidingInspectionTime.start();
    }
    
    if (jointInspectionDataObjects.size() < _MINIMUM_NUMBER_OF_IMAGES_TO_SAMPLE)
    {
      LocalizedString notEnoughJointsLocalizedString = new LocalizedString("ALGDIAG_GRIDARRAY_VOIDING_NOT_ENOUGH_JOINTS_TO_INSPECT_KEY",
                                                       new Object[] { _MINIMUM_NUMBER_OF_IMAGES_TO_SAMPLE } );
      AlgorithmUtil.raiseAlgorithmWarning(notEnoughJointsLocalizedString);
      return;
    }

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    
    final float MILLIMETERS_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtypeOfJoints);
    // get the slice to inspect from GridArrayMeasurement
    GridArrayMeasurementAlgorithm measurementAlgo = (GridArrayMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.GRID_ARRAY, AlgorithmEnum.MEASUREMENT);
    Collection<SliceNameEnum> inspectedSlices = measurementAlgo.getInspectionSliceOrder(subtypeOfJoints);

    int noiseReduction = (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION);

   if(isThicknessMethod(subtypeOfJoints))
  {
    // For some grid array types, there is no mid-ball slice.
    if (inspectedSlices.contains(SliceNameEnum.MIDBALL))
    {
      classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.MIDBALL,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_MIDBALL)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL);
    }

    // For some grid array types, there is no pad slice.
    if (inspectedSlices.contains(SliceNameEnum.PAD))
    {
      classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.PAD,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL);
    }
    
    // Added by Lee Herng 15 Nov 2011 - For some grid array types, there is no lower pad slice.
    if (inspectedSlices.contains(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_LOWERPAD),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_LOWERPAD)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }

    // For some grid array types, there is no package slice.
    if (inspectedSlices.contains(SliceNameEnum.PACKAGE))
    {
      classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.PACKAGE,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PACKAGE)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL);
    }
    
    // Added by Lee Herng 8 April 2012 - Handle user-defined slice
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
       classifySlice(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20),
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20)),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20),
                    noiseReduction,
                    MILLIMETERS_PER_PIXEL); 
    }
   }

   else if(isMedianMethod(subtypeOfJoints))
   {
    // For some grid array types, there is no mid-ball slice.
    if (inspectedSlices.contains(SliceNameEnum.MIDBALL))
    {
      classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.MIDBALL,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }

    // For some grid array types, there is no pad slice.
    if (inspectedSlices.contains(SliceNameEnum.PAD))
    {
      classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.PAD,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    
    // Added by Lee Herng 15 Nov 2011 - For some grid array types, there is no lower pad slice.
    if (inspectedSlices.contains(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }

    // For some grid array types, there is no package slice.
    if (inspectedSlices.contains(SliceNameEnum.PACKAGE))
    {
      classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.PACKAGE,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }

	// Added by Lee Herng 8 April 2012 - Handle user-defined slice
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }

	if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
       classifySliceMedianMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }
   }
   
    else if(isFloodFillMethod(subtypeOfJoints))
   {
    // For some grid array types, there is no mid-ball slice.
    if (inspectedSlices.contains(SliceNameEnum.MIDBALL))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.MIDBALL,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }

    // For some grid array types, there is no pad slice.
    if (inspectedSlices.contains(SliceNameEnum.PAD))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.PAD,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    
    // Added by Lee Herng 15 Nov 2011 - For some grid array types, there is no lower pad slice.
    if (inspectedSlices.contains(SliceNameEnum.BGA_LOWERPAD_ADDITIONAL))
    {
       classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_LOWERPAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL); 
    }

    // For some grid array types, there is no package slice.
    if (inspectedSlices.contains(SliceNameEnum.PACKAGE))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.PACKAGE,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
    // For some grid array types, there is no user defined slice.
    if (inspectedSlices.contains(SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20))
    {
      classifySliceFloodFillMethod(reconstructedImages, jointInspectionDataObjects,
                    SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                    MathUtil.convertMillimetersToMils((Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20)),
                    0.01f * (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20),
                    noiseReduction,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_BORDER_THRESHOLD),
                    (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD),
                    MILLIMETERS_PER_PIXEL);
    }
   }
    //Siew Yeng
    if(_GRID_ARRAY_VOIDING_INSPECTION_TIME_STAMP)
    {
      _gridArrayVoidingInspectionTime.stop();
      
      long totalGridArrayVOidingInspectionTime = _gridArrayVoidingInspectionTime.getElapsedTimeInMillis();
      System.out.println("Total GridArray Voiding Inspection time(ms): " + totalGridArrayVOidingInspectionTime);
    }
  }


   /**
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Patrick Lacz
   */
  private void classifySlice(
      ReconstructedImages reconstructedImages,
      List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float nominalDiameterInMils,
      float fractionOfDiameter,
      float voidThicknessThresholdInMils,
      float voidPercentAreaFailThreshold,
      float individualVoidPercentAreaFailThreshold,
      float voidPercentAreaMarginalFailThreshold,
      int noiseReduction,
      final float MILLIMETERS_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    // the standard image size for each joint is going to be nominalDiameterInPixels x nominalDiameterInPixels
    int standardizedDiameterInPixels = (int)Math.round(Math.ceil((nominalDiameterInMils * fractionOfDiameter) / MathUtil.convertMillimetersToMils(MILLIMETERS_PER_PIXEL)));

    if (standardizedDiameterInPixels < 5)
    {
      LocalizedString warningText = new LocalizedString("ALGDIAG_GRIDARRAY_VOIDING_STANDARD_DIAMETER_TOO_SMALL_WARNING_KEY",
                                                        new Object[] { sliceNameEnum.getName() });
      AlgorithmUtil.raiseAlgorithmWarning(warningText);
      return;
    }

    // Precompute a circular mask to get rid of the pixels in the corners that we aren't interested in.
    Image circularMaskImage = new Image(standardizedDiameterInPixels, standardizedDiameterInPixels);
    Paint.fillImage(circularMaskImage, 0.f);

     float numberOfPixelsTested = (float)(Paint.fillCircle(circularMaskImage, RegionOfInterest.createRegionFromImage(circularMaskImage),
        new DoubleCoordinate((float)standardizedDiameterInPixels / 2.0f, (float)standardizedDiameterInPixels / 2.0f),
        standardizedDiameterInPixels / 2.0f, 1.0f));

    // determine settings from noise reduction.
    // noise reduction of 1 = blur the image, 2 and above : apply opening afterwards.
    boolean applyBlurToJointImages = noiseReduction >= 1;
    int numberOfTimesToOpenVoidPixelImage = Math.max(0, noiseReduction - 1);

    // Create standard-sized images of each joint.
    Map<Integer, Pair<Image, RegionOfInterest> > jointIndexToImageMap =
        buildJointImageMap(jointInspectionDataObjects,
                           slice,
                           fractionOfDiameter,
                           standardizedDiameterInPixels,
                           applyBlurToJointImages,
                           MILLIMETERS_PER_PIXEL);

    if (jointIndexToImageMap.size() < _MINIMUM_NUMBER_OF_IMAGES_TO_SAMPLE)
    {
      LocalizedString warningText = new LocalizedString("ALGDIAG_GRIDARRAY_VOIDING_NOT_ENOUGH_JOINTS_IN_REGION_WARNING_KEY",
                                                        new Object[]{sliceNameEnum.getName()});
      AlgorithmUtil.raiseAlgorithmWarning(warningText);
      circularMaskImage.decrementReferenceCount();
      for (Pair<Image, RegionOfInterest> pair : jointIndexToImageMap.values())
      {
        pair.getFirst().decrementReferenceCount();
      }
      return;
    }

    // Measure the mean graylevel of each joint and build a sorted table.
    // This table will be used so that a joint is compared to joints with similar graylevels.
    ArrayList<Pair<Float, Integer>> meanGraylevelAndJointIndexList =
        buildGraylevelTable(jointInspectionDataObjects, jointIndexToImageMap, sliceNameEnum);

    float meanGraylevelArray[] = new float[meanGraylevelAndJointIndexList.size()];
    for (Pair<Float, Integer> pair : meanGraylevelAndJointIndexList)
    {
      meanGraylevelArray[pair.getSecond()] = pair.getFirst();
    }

    // we may have fewer than _NUMBER_OF_IMAGES_TO_SAMPLE. In that case, median 3 images instead of 5.
    Assert.expect(jointInspectionDataObjects.size() >= 3); // we check this above, but just to be sure that doesn't change..
    int numberOfImagesToMedian = _NUMBER_OF_IMAGES_TO_SAMPLE;
    if (jointIndexToImageMap.size() < _NUMBER_OF_IMAGES_TO_SAMPLE)
      numberOfImagesToMedian = _MINIMUM_NUMBER_OF_IMAGES_TO_SAMPLE;

    int numberOfImagesToSample = (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NUMBER_OF_IMAGES_TO_SAMPLE);
    boolean useOriginalMethod = true;
    if (numberOfImagesToSample != 0)
    {
      if (jointIndexToImageMap.size() >= numberOfImagesToSample)
        numberOfImagesToMedian = numberOfImagesToSample;
      else
        numberOfImagesToMedian = jointIndexToImageMap.size();
      if (numberOfImagesToMedian%2 == 0)
        numberOfImagesToMedian -= 1;
      useOriginalMethod = false;
    }
    // create an image where there are (numberOfImagesToMedian) images stacked on top of each other.
    Image aggregateMedianImage = createAggregateMedianImage(jointIndexToImageMap, meanGraylevelAndJointIndexList, numberOfImagesToMedian, useOriginalMethod);

    Image sampleImage = jointIndexToImageMap.values().iterator().next().getFirst();
    int jointImageWidth = sampleImage.getWidth();
    int jointImageHeight = sampleImage.getHeight();
    // we don't increment / decrement the reference count because it's still in the map.
    makeAggregateMedianImageConvex(aggregateMedianImage, jointImageWidth, jointImageHeight);

    int jointIndex = 0;
    
    // Precompute a circular mask to get rid of the pixels in the corners that we aren't interested in.
    Image combineDiagnosticVoidImage = null;
    
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      Pair<Image, RegionOfInterest> jointImageRegionPair = jointIndexToImageMap.get(jointIndex);
      if (jointImageRegionPair == null)
        continue;

      Image jointImage = jointImageRegionPair.getFirst();
      jointImage.incrementReferenceCount();

      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);

      JointInspectionResult jointResult = joint.getJointInspectionResult();
    
      // figure out which of the median images we have created best matches the graylevel for this joint
    float jointGraylevel = meanGraylevelArray[jointIndex];
          //jointResult.getJointMeasurement(sliceName, MeasurementEnum.GRID_ARRAY_VOIDING_GRAYLEVEL).getValue();
      int medianImageToUse = findIndexOfMedianForGraylevel(jointGraylevel, meanGraylevelAndJointIndexList);

      // pixelwise, perform thickness table lookups. (bg=src, fg=expected), gives positive
      // values if the src is brighter than the expected. Returned thickness is the amount of
      // solder we are NOT seeing (eg. voiding)
      SolderThickness thicnessTable = AlgorithmUtil.getSolderThickness(subtype);
      Image voidPixelsImage = AlgorithmUtil.createThicknessImage(
          aggregateMedianImage,
          new RegionOfInterest(0, jointImage.getHeight()*medianImageToUse, jointImage.getWidth(), jointImage.getHeight(), 0, RegionShapeEnum.RECTANGULAR),
          jointImage,
          RegionOfInterest.createRegionFromImage(jointImage),
          thicnessTable);
      jointImage.decrementReferenceCount();

      // mask (remove the corners)
      RegionOfInterest voidPixelsImageRoi = RegionOfInterest.createRegionFromImage(voidPixelsImage);
      Image maskedVoidPixelsImage = Arithmetic.multiplyImages(voidPixelsImage, voidPixelsImageRoi,
          circularMaskImage, RegionOfInterest.createRegionFromImage(circularMaskImage));
      voidPixelsImage.decrementReferenceCount();

      // threshold this thickness table with the thickness threshold.
      // Pixels with a value of 1.0 came out with a missing thickness greater than the threshold -- these are potential voids.
      Threshold.threshold(maskedVoidPixelsImage, (float)voidThicknessThresholdInMils, 0.0f, (float)voidThicknessThresholdInMils, _VOID_PIXEL_INTENSITY);

      // Noise Reduction
      if (numberOfTimesToOpenVoidPixelImage > 0)
      {
        Image openedVoidPixelsImage = Filter.opening(maskedVoidPixelsImage, RegionOfInterest.createRegionFromImage(maskedVoidPixelsImage), numberOfTimesToOpenVoidPixelImage);
        maskedVoidPixelsImage.decrementReferenceCount();
        maskedVoidPixelsImage = openedVoidPixelsImage;
      }
     
      // count the number of "void" pixels (== 1.f, but we may as well have a pretty loose bound)
      int numberOfVoidPixels = Threshold.countPixelsInRange(maskedVoidPixelsImage, _VOID_PIXEL_INTENSITY-0.5f, _VOID_PIXEL_INTENSITY+0.5f);

      // Store the percent voiding
      float voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;
      
      //Jack Hwee-individual void
//       int numberOfIndividualVoidPixels = Threshold.individualVoid(maskedVoidPixelsImage,individualVoidPercentAreaFailThreshold, numberOfPixelsTested);
      //Siew Yeng - XCR-3515- Void Diameter
      float[] largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(maskedVoidPixelsImage);
      
      //create image with largest void area only
      Image largestVoidImage = Image.createFloatImageFromArray(maskedVoidPixelsImage.getWidth(), maskedVoidPixelsImage.getHeight(), largestVoidImageArray);
      
      //Jack Hwee-individual void
      int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 255 - 0.5f, 255.f);
      float individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTested;
      
      boolean shouldTestVoidDiameter = shouldIndividualVoidPercentDiameterBeTested(sliceNameEnum);
      float individualVoidDiameterPercent = 0.f;
      if(shouldTestVoidDiameter && numberOfIndividualVoidPixels > 0)
      {
        individualVoidDiameterPercent = calculateIndividualVoidDiameterPercent(largestVoidImageArray, 
                                                                                largestVoidImage.getWidth(),
                                                                                largestVoidImage.getHeight(),
                                                                                subtype, sliceNameEnum,
                                                                                MILLIMETERS_PER_PIXEL);
      }
      
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);
      jointResult.addMeasurement(voidPercentMeasurement);

      //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

       jointResult.addMeasurement(individualVoidPercentMeasurement);

      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailedAreaTest = voidPercent > voidPercentAreaFailThreshold;
      boolean jointFailedMarginalTest = voidPercent > voidPercentAreaMarginalFailThreshold;
      boolean individualJointFailedAreaTest = (individualVoidPercent > individualVoidPercentAreaFailThreshold);
      
      if (jointFailedAreaTest)
      {
        JointIndictment voidingIndictment = new JointIndictment(IndictmentEnum.VOIDING, this, sliceNameEnum);
        if (jointFailedAreaTest == true)
          voidingIndictment.addFailingMeasurement(voidPercentMeasurement);
        else
          voidingIndictment.addRelatedMeasurement(voidPercentMeasurement);

        jointResult.addIndictment(voidingIndictment);
      }

    //Jack Hwee-individual void
      if (individualJointFailedAreaTest)
      {
        JointIndictment individualVoidingIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING, this, sliceNameEnum);
        if (individualJointFailedAreaTest == true)
          individualVoidingIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        else
          individualVoidingIndictment.addRelatedMeasurement(individualVoidPercentMeasurement);

        jointResult.addIndictment(individualVoidingIndictment);
      }
      
      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      if (jointFailedAreaTest || individualJointFailedAreaTest)
      {
        if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype))
        {
          combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, 
                                                                            combineDiagnosticVoidImage, 
                                                                            maskedVoidPixelsImage, 
                                                                            jointImageRegionPair.getSecond(), 
                                                                            sliceNameEnum);
        }
      }
      
      postClassificationDiagnostics(sliceNameEnum,
                                    voidPercentAreaFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    maskedVoidPixelsImage,
                                    voidPercentMeasurement,
                                    jointFailedAreaTest,
                                    jointFailedMarginalTest);
      
      postClassificationDiagnostics(sliceNameEnum,
                                    individualVoidPercentAreaFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    maskedVoidPixelsImage,
                                    individualVoidPercentMeasurement,
                                    individualJointFailedAreaTest,
                                    jointFailedMarginalTest);
      
      //Siew Yeng - XCR-3515 - Void Diameter Threshold for BGA algorithm
      if (shouldTestVoidDiameter)
      {
        JointMeasurement individualVoidDiameterPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_DIAMETER_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidDiameterPercent);
        
        jointResult.addMeasurement(individualVoidDiameterPercentMeasurement);
        
        float individualVoidPercentDiameterFailThreshold = getIndividualVoidPercentDiameterFailThreshold(subtype, sliceNameEnum);
        boolean individualJointFailedDiameterTest = (individualVoidDiameterPercent > individualVoidPercentDiameterFailThreshold);
      
        if(individualJointFailedDiameterTest)
        {
          JointIndictment individualVoidingDiameterIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING, this, sliceNameEnum);
          if (individualJointFailedDiameterTest == true)
            individualVoidingDiameterIndictment.addFailingMeasurement(individualVoidDiameterPercentMeasurement);
          else
            individualVoidingDiameterIndictment.addRelatedMeasurement(individualVoidDiameterPercentMeasurement);

          jointResult.addIndictment(individualVoidingDiameterIndictment);
        }
        
        postClassificationDiagnostics(sliceNameEnum,
                                    individualVoidPercentDiameterFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    maskedVoidPixelsImage,
                                    individualVoidDiameterPercentMeasurement,
                                    individualJointFailedDiameterTest,
                                    jointFailedMarginalTest);
      }

      // the void pixels image may be sent as a diagnostic
      maskedVoidPixelsImage.decrementReferenceCount();
      largestVoidImage.decrementReferenceCount();
      largestVoidImageArray = null;

      ++jointIndex;
    }
    //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
    if(combineDiagnosticVoidImage != null)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
      combineDiagnosticVoidImage.decrementReferenceCount();
    }

    aggregateMedianImage.decrementReferenceCount();
    circularMaskImage.decrementReferenceCount();
    
    for (Pair<Image, RegionOfInterest> value : jointIndexToImageMap.values())
    {
      value.getFirst().decrementReferenceCount();
    }

  }


  /**
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Jack Hwee
   */
  private void classifySliceMedianMethod(
      ReconstructedImages reconstructedImages,
      List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float nominalDiameterInMils,
      float fractionOfDiameter,
      float voidPercentAreaFailThreshold,
      float individualVoidPercentAreaFailThreshold,
      float voidPercentAreaMarginalFailThreshold,
      int noiseReduction,
      float voidBorderThreshold,
      final float MILLIMETERS_PER_PIXEL)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    // the standard image size for each joint is going to be nominalDiameterInPixels x nominalDiameterInPixels
    int standardizedDiameterInPixels = (int)Math.round(Math.ceil((nominalDiameterInMils * fractionOfDiameter) / MathUtil.convertMillimetersToMils(MILLIMETERS_PER_PIXEL)));

    if (standardizedDiameterInPixels < 5)
    {
      LocalizedString warningText = new LocalizedString("ALGDIAG_GRIDARRAY_VOIDING_STANDARD_DIAMETER_TOO_SMALL_WARNING_KEY",
                                                        new Object[] { sliceNameEnum.getName() });
      AlgorithmUtil.raiseAlgorithmWarning(warningText);
      return;
    }

    // Precompute a circular mask to get rid of the pixels in the corners that we aren't interested in.
    Image circularMaskImage = new Image(standardizedDiameterInPixels, standardizedDiameterInPixels);
    Paint.fillImage(circularMaskImage, 0.f);

    // create image which opposite with circularMaskImage, where to mask only the corners of the joint image
    Image cornerMaskImage = new Image(standardizedDiameterInPixels, standardizedDiameterInPixels);
    Paint.fillImage(cornerMaskImage, 1.f);

    Paint.fillCircle(cornerMaskImage, RegionOfInterest.createRegionFromImage(cornerMaskImage),
        new DoubleCoordinate((float)standardizedDiameterInPixels / 2.0f, (float)standardizedDiameterInPixels / 2.0f),
        standardizedDiameterInPixels / 2.0f, 0.0f);


    // determine settings from noise reduction.
    // noise reduction of 1 = blur the image, 2 and above : apply opening afterwards.
    boolean applyBlurToJointImages = noiseReduction >= 1;
    int numberOfTimesToOpenVoidPixelImage = Math.max(0, noiseReduction - 1);

    // Create standard-sized images of each joint.
    Map<Integer, Pair<Image, RegionOfInterest> > jointIndexToImageMap =
        buildJointImageMap(jointInspectionDataObjects,
                           slice,
                           fractionOfDiameter,
                           standardizedDiameterInPixels,
                           applyBlurToJointImages,
                           MILLIMETERS_PER_PIXEL);


    Image combineDiagnosticVoidImage = null;
    int jointIndex = 0;
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      Pair<Image, RegionOfInterest> jointImageRegionPair = jointIndexToImageMap.get(jointIndex);
      if (jointImageRegionPair == null)
        continue;

      Image jointImage = jointImageRegionPair.getFirst();
      jointImage.incrementReferenceCount();

      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);

      JointInspectionResult jointResult = joint.getJointInspectionResult();

 /*   if (joint.getComponent().toString().equals("U3000") && joint.getPad().toString().equals("A3"))
         {
       String path =  "C:\\" + "image3\\" + sliceNameEnum.toString() + "_" + joint.getComponent().toString() + "_" + joint.getPad().toString() + "cornerMaskImage" + ".png";

       ImageIoUtil.nativeSavePng(cornerMaskImage.getNativeDefinition(),path, "");
       System.out.print('\n');

          } */
     
      //Get the "true" region of the joint by thresholding the joint image by auto-level by imageJ
     Image maskedjointImage = new Image(jointImage.getWidth(), jointImage.getHeight());
     Transform.copyImageIntoImage(jointImage, maskedjointImage);
     int numHistogramBins = 256;   
     int[] histogram = Threshold.histogram(jointImage, RegionOfInterest.createRegionFromImage(jointImage), numHistogramBins);
     int level = Threshold.getAutoThreshold(histogram);
     
     Threshold.threshold(maskedjointImage, (float)0.0f, 0.0f, level + 1, 80.0f);   //_VOID_PIXEL_INTENSITY
     
     //Get the corners of the image
     Threshold.threshold(cornerMaskImage, (float)0.0f, 0.0f, 0.0f, _VOID_PIXEL_INTENSITY);

      //Get the total of joint region, gray value more than _VOID_PIXEL_INTENSITY not consider as joint
     // int numberOfPixelsTested = Threshold.countPixelsInRange(maskedjointImage, 0.0f, _VOID_PIXEL_INTENSITY- 0.5f);
       float numberOfPixelsTested = (float)(Paint.fillCircle(circularMaskImage, RegionOfInterest.createRegionFromImage(circularMaskImage),
        new DoubleCoordinate((float)standardizedDiameterInPixels / 2.0f, (float)standardizedDiameterInPixels / 2.0f),
        standardizedDiameterInPixels / 2.8f, 1.0f));

    //  int fullJointTolerance = 10;
        
    //  int fullJointThreshold = Math.round((numberOfPixelsTested / 2) - fullJointTolerance);

    //  int numberOfCornersPixels = Threshold.countPixelsInRange(maskedjointImage, _VOID_PIXEL_INTENSITY - 0.5f, _VOID_PIXEL_INTENSITY + 0.5f);

      //Get the median of joint image, then threshold it with median to get visible void image
      float median = Statistics.median(jointImage);
      Threshold.threshold(jointImage, (float)0.0f, 0.0f, median, 80.0f);   //_VOID_PIXEL_INTENSITY
      
    // To get void image included with area of whole joint(where the borders around diameter of joint are consider void), we create maskedVoidPixelsImage
      RegionOfInterest jointImageRoi = RegionOfInterest.createRegionFromImage(jointImage);
      Image maskedVoidPixelsImage = Arithmetic.subtractImages(jointImage, jointImageRoi,
         cornerMaskImage, RegionOfInterest.createRegionFromImage(cornerMaskImage));

      // To get rid of the void pixels around borders of joint, we create newMaskedVoidPixelsImage
      int x = maskedVoidPixelsImage.getCenterCoordinate().getX();
      int y = maskedVoidPixelsImage.getCenterCoordinate().getY();

      Paint.fillCircle(maskedjointImage, RegionOfInterest.createRegionFromImage(maskedjointImage),
        new DoubleCoordinate(x, y),
        standardizedDiameterInPixels / voidBorderThreshold, 255.0f);  // changing the value voidBorderThreshold will affect the borders area

      Image newMaskedVoidPixelsImage = Arithmetic.subtractImages(maskedjointImage, RegionOfInterest.createRegionFromImage(maskedjointImage),
        jointImage, RegionOfInterest.createRegionFromImage(jointImage));

      Threshold.threshold(newMaskedVoidPixelsImage, (float)175.0f, 0.0f, 175.0f, 0.0f);

      float voidPercent = 0;
      float individualVoidPercent = 0;
      float individualVoidDiameterPercent = 0.f;
      int numberOfVoidPixels = 0;

      boolean shouldTestVoidDiameter = shouldIndividualVoidPercentDiameterBeTested(sliceNameEnum);
      
      if ((numberOfPixelsTested > 0))
      {
        //Count the individual void pixels
//        int numberOfIndividualVoidPixels = Threshold.individualVoid(newMaskedVoidPixelsImage, individualVoidPercentAreaFailThreshold, numberOfPixelsTested);
        //Siew Yeng - XCR-3515- Void Diameter
        float[] largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(newMaskedVoidPixelsImage);
      
        //create image with largest void area only
        Image largestVoidImage = Image.createFloatImageFromArray(newMaskedVoidPixelsImage.getWidth(), newMaskedVoidPixelsImage.getHeight(), largestVoidImageArray);

        //Jack Hwee-individual void
        int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 255 - 0.5f, 255.f);
        
        if (numberOfIndividualVoidPixels < numberOfPixelsTested)
        {
          individualVoidPercent = 100.f * (float) numberOfIndividualVoidPixels / numberOfPixelsTested;

          //Count the void pixels
          numberOfVoidPixels = Threshold.countPixelsInRange(newMaskedVoidPixelsImage, _MEDIAN_VOID_PIXEL_INTENSITY - 0.5f, _MEDIAN_VOID_PIXEL_INTENSITY + 0.5f);

          voidPercent = 100.f * (float) numberOfVoidPixels / numberOfPixelsTested;

          //make sure individual void percent is not greater than void percent
          if (individualVoidPercent > voidPercent)
          {
            individualVoidPercent = voidPercent;
          }
          
          if(shouldTestVoidDiameter && numberOfIndividualVoidPixels > 0)
          {
            individualVoidDiameterPercent = calculateIndividualVoidDiameterPercent(largestVoidImageArray, 
                                                                                    largestVoidImage.getWidth(),
                                                                                    largestVoidImage.getHeight(),
                                                                                    subtype, sliceNameEnum,
                                                                                    MILLIMETERS_PER_PIXEL);
          }
        }
        largestVoidImage.decrementReferenceCount();
        largestVoidImageArray = null;
      }
     
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);
      jointResult.addMeasurement(voidPercentMeasurement);

      //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

      jointResult.addMeasurement(individualVoidPercentMeasurement);
      
      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailedAreaTest = voidPercent > voidPercentAreaFailThreshold;
      boolean jointFailedMarginalTest = voidPercent > voidPercentAreaMarginalFailThreshold;
      boolean individualJointFailedAreaTest = (individualVoidPercent > individualVoidPercentAreaFailThreshold);
      
      if (jointFailedAreaTest)
      {
        JointIndictment voidingIndictment = new JointIndictment(IndictmentEnum.VOIDING, this, sliceNameEnum);
        if (jointFailedAreaTest == true)
          voidingIndictment.addFailingMeasurement(voidPercentMeasurement);
        else
          voidingIndictment.addRelatedMeasurement(voidPercentMeasurement);

        jointResult.addIndictment(voidingIndictment);
      }
      
    //Jack Hwee-individual void
      if (individualJointFailedAreaTest)
      {
        JointIndictment individualVoidingIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING, this, sliceNameEnum);
        if (individualJointFailedAreaTest == true)
          individualVoidingIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        else
          individualVoidingIndictment.addRelatedMeasurement(individualVoidPercentMeasurement);

        jointResult.addIndictment(individualVoidingIndictment);
      }
      
      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      if (jointFailedAreaTest || individualJointFailedAreaTest)
      {
        if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype))
        {
          combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, 
                                                                            combineDiagnosticVoidImage, 
                                                                            newMaskedVoidPixelsImage, 
                                                                            jointImageRegionPair.getSecond(), 
                                                                            sliceNameEnum);
        }
      }

      postClassificationDiagnostics(sliceNameEnum,
                                    voidPercentAreaFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    newMaskedVoidPixelsImage,
                                    voidPercentMeasurement,
                                    jointFailedAreaTest,
                                    jointFailedMarginalTest);
      
       postClassificationDiagnostics(sliceNameEnum,
                                    individualVoidPercentAreaFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    newMaskedVoidPixelsImage,
                                    individualVoidPercentMeasurement,
                                    individualJointFailedAreaTest,
                                    jointFailedMarginalTest);
      
      //Siew Yeng - XCR-3515- Void Diameter
      if (shouldTestVoidDiameter)
      {
        JointMeasurement individualVoidDiameterPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_DIAMETER_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidDiameterPercent);
        
        jointResult.addMeasurement(individualVoidDiameterPercentMeasurement);
      
        float individualVoidPercentDiameterFailThreshold = getIndividualVoidPercentDiameterFailThreshold(subtype, sliceNameEnum);
        boolean individualJointFailedDiameterTest = (individualVoidDiameterPercent > individualVoidPercentDiameterFailThreshold);
      
        if(individualJointFailedDiameterTest)
        {
          JointIndictment individualVoidingDiameterIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING, this, sliceNameEnum);
          if (individualJointFailedDiameterTest == true)
            individualVoidingDiameterIndictment.addFailingMeasurement(individualVoidDiameterPercentMeasurement);
          else
            individualVoidingDiameterIndictment.addRelatedMeasurement(individualVoidDiameterPercentMeasurement);

          jointResult.addIndictment(individualVoidingDiameterIndictment);
        }
        postClassificationDiagnostics(sliceNameEnum,
                                    individualVoidPercentDiameterFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    newMaskedVoidPixelsImage,
                                    individualVoidDiameterPercentMeasurement,
                                    individualJointFailedDiameterTest,
                                    jointFailedMarginalTest);
      }

      // the void pixels image may be sent as a diagnostic
      maskedVoidPixelsImage.decrementReferenceCount();
     // totalGaussVoidImage.decrementReferenceCount();
      jointImage.decrementReferenceCount();
      newMaskedVoidPixelsImage.decrementReferenceCount();
      maskedjointImage.decrementReferenceCount();
    
      ++jointIndex;
    }
    //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
    if(combineDiagnosticVoidImage != null)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
      combineDiagnosticVoidImage.decrementReferenceCount();
    }

    cornerMaskImage.decrementReferenceCount();
    circularMaskImage.decrementReferenceCount();
    
    for (Pair<Image, RegionOfInterest> value : jointIndexToImageMap.values())
    {
      value.getFirst().decrementReferenceCount();
    }
    
  }
  
   /**
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Jack Hwee
   */
  private void classifySliceFloodFillMethod(
      ReconstructedImages reconstructedImages,
      List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float nominalDiameterInMils,
      float fractionOfDiameter,
      float voidPercentAreaFailThreshold,
      float individualVoidPercentAreaFailThreshold,
      float voidPercentAreaMarginalFailThreshold,
      int noiseReduction,
      float voidBorderThreshold,
      int floodFillSensitivity,
      final float MILLIMETERS_PER_PIXEL)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    // the standard image size for each joint is going to be nominalDiameterInPixels x nominalDiameterInPixels
    int standardizedDiameterInPixels = (int)Math.round(Math.ceil((nominalDiameterInMils * fractionOfDiameter) / MathUtil.convertMillimetersToMils(MILLIMETERS_PER_PIXEL)));

    if (standardizedDiameterInPixels < 5)
    {
      LocalizedString warningText = new LocalizedString("ALGDIAG_GRIDARRAY_VOIDING_STANDARD_DIAMETER_TOO_SMALL_WARNING_KEY",
                                                        new Object[] { sliceNameEnum.getName() });
      AlgorithmUtil.raiseAlgorithmWarning(warningText);
      return;
    }

    // Precompute a circular mask to get rid of the pixels in the corners that we aren't interested in.
    Image circularMaskImage = new Image(standardizedDiameterInPixels, standardizedDiameterInPixels);
    Paint.fillImage(circularMaskImage, 0.f);

    // create image which opposite with circularMaskImage, where to mask only the corners of the joint image
    Image cornerMaskImage = new Image(standardizedDiameterInPixels, standardizedDiameterInPixels);
    Paint.fillImage(cornerMaskImage, 1.f);

    Paint.fillCircle(cornerMaskImage, RegionOfInterest.createRegionFromImage(cornerMaskImage),
        new DoubleCoordinate((float)standardizedDiameterInPixels / 2.0f, (float)standardizedDiameterInPixels / 2.0f),
        standardizedDiameterInPixels / 2.0f, 0.0f);

    // determine settings from noise reduction.
    // noise reduction of 1 = blur the image, 2 and above : apply opening afterwards.
    boolean applyBlurToJointImages = noiseReduction >= 1;
    int numberOfTimesToOpenVoidPixelImage = Math.max(0, noiseReduction - 1);

    // Create standard-sized images of each joint.
    Map<Integer, Pair<Image, RegionOfInterest> > jointIndexToImageMap =
        buildJointImageMap(jointInspectionDataObjects,
                           slice,
                           fractionOfDiameter,
                           standardizedDiameterInPixels,
                           applyBlurToJointImages,
                           MILLIMETERS_PER_PIXEL);


    int jointIndex = 0;
    Image combineDiagnosticVoidImage = null;
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      Pair<Image, RegionOfInterest> jointImageRegionPair = jointIndexToImageMap.get(jointIndex);
      if (jointImageRegionPair == null)
        continue;

      Image jointImage = jointImageRegionPair.getFirst();
      jointImage.incrementReferenceCount();
    
      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);

      JointInspectionResult jointResult = joint.getJointInspectionResult();

//    if (joint.getComponent().toString().equals("U32") && joint.getPad().toString().equals("268"))
//    {
//       String path =  "C:\\" + "image3\\" + sliceNameEnum.toString() + "_" + joint.getComponent().toString() + "_" + joint.getPad().toString() + "jointImage2" + ".png";
//
//       ImageIoUtil.nativeSavePng(jointImage2.getNativeDefinition(),path, "");
//       System.out.print('\n');
//
//     } 
      
      ImagePlus gaussianBlurBuffImg = null;
    
      gaussianBlurBuffImg = new ImagePlus("", jointImage.getBufferedImage());
      
      GaussianBlur gaussianBlur = new GaussianBlur();
      
      if (gaussianBlurBuffImg != null)
             gaussianBlur.blurGaussian(gaussianBlurBuffImg.getProcessor(), (double)10.0, (double)10.0, 0.01);
      
      gaussianBlurBuffImg.lockSilently();
     
      Image filterGaussImage = Image.createFloatImageFromBufferedImage(gaussianBlurBuffImg.getBufferedImage());
  
      gaussianBlurBuffImg.close();
      gaussianBlurBuffImg.flush();      
      
      Image totalGaussVoidImage = null;
  
      totalGaussVoidImage = Arithmetic.subtractImages(filterGaussImage, jointImage);
      
      filterGaussImage.decrementReferenceCount();
  
      int numHistogramBins = 256;   
      int[] histogram = Threshold.histogram(totalGaussVoidImage, RegionOfInterest.createRegionFromImage(totalGaussVoidImage), numHistogramBins);
      int level = Threshold.getAutoThreshold(histogram);
      
      if (level > 4)
      {
        level = 2;
      }
              
      float floodFactor = 0;
      if (floodFillSensitivity == 1) floodFactor = 2.5f;
      if (floodFillSensitivity == 2) floodFactor = 1.5f;
      if (floodFillSensitivity == 3) floodFactor = 0.5f;
      if (floodFillSensitivity == 4) floodFactor = -1.5f;
      if (floodFillSensitivity == 5) floodFactor = -2.5f;
    
      Threshold.threshold(totalGaussVoidImage, (float) level - floodFactor, 255.0f, (float)level - floodFactor + 0.5f, 0.0f); 
      
      Image maskedjointImage = new Image(jointImage.getWidth(), jointImage.getHeight());

      // To get rid of the void pixels around borders of joint, we create newMaskedVoidPixelsImage
      int x = maskedjointImage.getCenterCoordinate().getX();
      int y = maskedjointImage.getCenterCoordinate().getY();

      Paint.fillImage(maskedjointImage, 11.f);
      Paint.fillCircle(maskedjointImage, RegionOfInterest.createRegionFromImage(maskedjointImage),
        new DoubleCoordinate(x, y),
        standardizedDiameterInPixels / voidBorderThreshold, 255.0f);  // changing the value voidBorderThreshold will affect the borders area
      
      Image newMaskedVoidPixelsImage = Arithmetic.subtractImages(maskedjointImage, RegionOfInterest.createRegionFromImage(maskedjointImage),
        totalGaussVoidImage, RegionOfInterest.createRegionFromImage(totalGaussVoidImage));
      
      float voidPercent = 0;
      float individualVoidPercent = 0;
      float individualVoidDiameterPercent = 0.f;
      int numberOfVoidPixels = 0;

      float numberOfPixelsTested = (float)(Paint.fillCircle(circularMaskImage, RegionOfInterest.createRegionFromImage(circularMaskImage),
        new DoubleCoordinate((float)standardizedDiameterInPixels / 2.0f, (float)standardizedDiameterInPixels / 2.0f),
        standardizedDiameterInPixels / 2.0f, 1.0f));
       
//      float numberOfPixelsTested = (float)(Paint.fillCircle(circularMaskImage, RegionOfInterest.createRegionFromImage(circularMaskImage),
//        new DoubleCoordinate((float)standardizedDiameterInPixels / 2.0f, (float)standardizedDiameterInPixels / 2.0f),
//        standardizedDiameterInPixels / 2.8f, 1.0f)); 
    
      Threshold.threshold(newMaskedVoidPixelsImage, (float)0.5f, 255.0f, (float)5.0f, 0.0f);
      
      Arithmetic.andImages(maskedjointImage, RegionOfInterest.createRegionFromImage(maskedjointImage),
                 newMaskedVoidPixelsImage, RegionOfInterest.createRegionFromImage(newMaskedVoidPixelsImage), newMaskedVoidPixelsImage, 
                 RegionOfInterest.createRegionFromImage(newMaskedVoidPixelsImage));
       
      Threshold.threshold(newMaskedVoidPixelsImage, (float)200.0f, 0.0f, 200.0f, 175.0f);
    
      boolean shouldTestVoidDiameter = shouldIndividualVoidPercentDiameterBeTested(sliceNameEnum);
      
      if ((numberOfPixelsTested > 0))
      {
        //Count the individual void pixels
//        int numberOfIndividualVoidPixels = Threshold.individualVoid(newMaskedVoidPixelsImage, individualVoidPercentAreaFailThreshold, numberOfPixelsTested);
        //Siew Yeng - XCR-3515- Void Diameter
        float[] largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(newMaskedVoidPixelsImage);
      
        //create image with largest void area only
        Image largestVoidImage = Image.createFloatImageFromArray(newMaskedVoidPixelsImage.getWidth(), newMaskedVoidPixelsImage.getHeight(), largestVoidImageArray);

        //Jack Hwee-individual void
        int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 255 - 0.5f, 255.f);
        
        if (numberOfIndividualVoidPixels < numberOfPixelsTested)
        {
          individualVoidPercent = 100.f * (float) numberOfIndividualVoidPixels / numberOfPixelsTested;

          //Count the void pixels
          numberOfVoidPixels = Threshold.countPixelsInRange(newMaskedVoidPixelsImage, _MEDIAN_VOID_PIXEL_INTENSITY - 0.5f, _MEDIAN_VOID_PIXEL_INTENSITY + 0.5f);

          voidPercent = 100.f * (float) numberOfVoidPixels / numberOfPixelsTested;

          //make sure individual void percent is not greater than void percent
          if (individualVoidPercent > voidPercent)
          {
            individualVoidPercent = voidPercent;
          }

          if(shouldTestVoidDiameter && numberOfIndividualVoidPixels > 0)
          {
            individualVoidDiameterPercent = calculateIndividualVoidDiameterPercent(largestVoidImageArray, 
                                                                                    largestVoidImage.getWidth(),
                                                                                    largestVoidImage.getHeight(),
                                                                                    subtype, sliceNameEnum,
                                                                                    MILLIMETERS_PER_PIXEL);
          }
        }
        largestVoidImage.decrementReferenceCount();
        largestVoidImageArray = null;
      }
      
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);
      jointResult.addMeasurement(voidPercentMeasurement);

      //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

      jointResult.addMeasurement(individualVoidPercentMeasurement);
      
      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailedAreaTest = voidPercent > voidPercentAreaFailThreshold;
      boolean jointFailedMarginalTest = voidPercent > voidPercentAreaMarginalFailThreshold;
      boolean individualJointFailedAreaTest = (individualVoidPercent > individualVoidPercentAreaFailThreshold);
      
      if (jointFailedAreaTest)
      {
        JointIndictment voidingIndictment = new JointIndictment(IndictmentEnum.VOIDING, this, sliceNameEnum);
        if (jointFailedAreaTest == true)
          voidingIndictment.addFailingMeasurement(voidPercentMeasurement);
        else
          voidingIndictment.addRelatedMeasurement(voidPercentMeasurement);

        jointResult.addIndictment(voidingIndictment);
      }
      
    //Jack Hwee-individual void
      if (individualJointFailedAreaTest)
      {
        JointIndictment individualVoidingIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING, this, sliceNameEnum);
        if (individualJointFailedAreaTest == true)
          individualVoidingIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        else
          individualVoidingIndictment.addRelatedMeasurement(individualVoidPercentMeasurement);

        jointResult.addIndictment(individualVoidingIndictment);
      }
      
      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      if (jointFailedAreaTest || individualJointFailedAreaTest)
      {
        if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype))
        {
          combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, 
                                                                            combineDiagnosticVoidImage, 
                                                                            newMaskedVoidPixelsImage, 
                                                                            jointImageRegionPair.getSecond(), 
                                                                            sliceNameEnum);
        }
      }

      postClassificationDiagnostics(sliceNameEnum,
                                    voidPercentAreaFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    newMaskedVoidPixelsImage,
                                    voidPercentMeasurement,
                                    jointFailedAreaTest,
                                    jointFailedMarginalTest);
      
      postClassificationDiagnostics(sliceNameEnum,
                                    individualVoidPercentAreaFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    newMaskedVoidPixelsImage,
                                    individualVoidPercentMeasurement,
                                    individualJointFailedAreaTest,
                                    jointFailedMarginalTest);
      
      //Siew Yeng - XCR-3515- Void Diameter
      if (shouldTestVoidDiameter)
      {
        JointMeasurement individualVoidDiameterPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_DIAMETER_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidDiameterPercent);

        jointResult.addMeasurement(individualVoidDiameterPercentMeasurement);
      
        float individualVoidPercentDiameterFailThreshold = getIndividualVoidPercentDiameterFailThreshold(subtype, sliceNameEnum);
        boolean individualJointFailedDiameterTest = (individualVoidDiameterPercent > individualVoidPercentDiameterFailThreshold);
      
        if(individualJointFailedDiameterTest)
        {
          JointIndictment individualVoidingDiameterIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING, this, sliceNameEnum);
          if (individualJointFailedDiameterTest == true)
            individualVoidingDiameterIndictment.addFailingMeasurement(individualVoidDiameterPercentMeasurement);
          else
            individualVoidingDiameterIndictment.addRelatedMeasurement(individualVoidDiameterPercentMeasurement);

          jointResult.addIndictment(individualVoidingDiameterIndictment);
        }
        postClassificationDiagnostics(sliceNameEnum,
                                    individualVoidPercentDiameterFailThreshold,
                                    reconstructionRegion,
                                    joint,
                                    jointImageRegionPair,
                                    newMaskedVoidPixelsImage,
                                    individualVoidDiameterPercentMeasurement,
                                    individualJointFailedDiameterTest,
                                    jointFailedMarginalTest);
      }

      // the void pixels image may be sent as a diagnostic
      totalGaussVoidImage.decrementReferenceCount();
      jointImage.decrementReferenceCount();
      newMaskedVoidPixelsImage.decrementReferenceCount();
      maskedjointImage.decrementReferenceCount();
     
      gaussianBlurBuffImg.unlock();
      ++jointIndex;
    }
    //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
    if(combineDiagnosticVoidImage != null)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
      combineDiagnosticVoidImage.decrementReferenceCount();
    }

    cornerMaskImage.decrementReferenceCount();
    circularMaskImage.decrementReferenceCount();

    for (Pair<Image, RegionOfInterest> value : jointIndexToImageMap.values())
    {
      value.getFirst().decrementReferenceCount();
    }

  }

  /**
   * I've extracted this method to encapsulate displaying all of the diagnostic messages at the end of classification.
   *
   * @author Patrick Lacz
   */
  private void postClassificationDiagnostics(SliceNameEnum sliceName,
                                             float voidPercentAreaFailThreshold,
                                             ReconstructionRegion reconstructionRegion,
                                             JointInspectionData joint,
                                             Pair<Image, RegionOfInterest> jointImageRegionPair,
                                             Image maskedVoidPixelsImage,
                                             JointMeasurement voidPercentMeasurement,
                                             boolean jointFailedAreaTest,
                                             boolean jointFailedMarginalAreaTest)
  {
    Assert.expect(sliceName != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(joint != null);
    Assert.expect(jointImageRegionPair != null);
    Assert.expect(maskedVoidPixelsImage != null);
    Assert.expect(voidPercentMeasurement != null);


    MeasurementRegionEnum classificationRegionEnum = MeasurementRegionEnum.PASSING_JOINT_REGION;

    if (jointFailedMarginalAreaTest == true)
      classificationRegionEnum = MeasurementRegionEnum.MARGINAL_VOID_REGION;
    if (jointFailedAreaTest == true)
      classificationRegionEnum = MeasurementRegionEnum.FAILING_JOINT_REGION;

    if (jointFailedAreaTest == true)
    {
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      joint,
                                                      reconstructionRegion,
                                                      sliceName,
                                                      voidPercentMeasurement,
                                                      voidPercentAreaFailThreshold);
    }
    else
    {
      AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, voidPercentMeasurement);
    }

    if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
    {
      //Siew Yeng - XCR-2566 - Display Voiding Percentage Value on Diagnostic Image
      OverlayImageDiagnosticInfo overlayImageDiagnosticInfo = new OverlayImageDiagnosticInfo(maskedVoidPixelsImage, jointImageRegionPair.getSecond());
      if(voidPercentMeasurement.getMeasurementEnum().equals(MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT))
      {
        String measurementString = MeasurementUnitsEnum.formatNumberIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), voidPercentMeasurement.getValue());
        overlayImageDiagnosticInfo.setMeasurementLabel(MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), measurementString));
      }
      
      _diagnostics.postDiagnostics(reconstructionRegion, sliceName, joint, this, false,
          overlayImageDiagnosticInfo,
          new MeasurementRegionDiagnosticInfo(jointImageRegionPair.getSecond(),
               classificationRegionEnum));
    }
  }

  /**
   * Creates an 'aggregate' image containing the medians of every group of _NUMBER_OF_IMAGES_TO_SAMPLE images in the meanGraylevelAndJointIndexList.
   * An 'aggregate' image here is short cut to a 3-dimensional image. Conceptually every row contains one image, or all the images are stacked vertically.
   * We use the Image.setWidthAndHeight method to change between the two representations.
   *
   * In this case we know what the width of the image in the row should be,
   * so we can reconstruct the pixel x,y by x+y*knownWidth. (done in computeThicknessImage)
   *
   * Perhaps in the future we will have an object that represents 3d images.
   *
   * @author Patrick Lacz
   */
  private Image createAggregateMedianImage(Map<Integer, Pair<Image, RegionOfInterest> > jointIndexToImageMap,
      ArrayList<Pair<Float, Integer>> meanGraylevelAndJointIndexList, int numberOfImagesToMedian, boolean useOriginalMethod)
  {
    Assert.expect(jointIndexToImageMap != null);
    Assert.expect(meanGraylevelAndJointIndexList != null);

    int numberOfJoints = jointIndexToImageMap.size();

    Image sampleImage = jointIndexToImageMap.values().iterator().next().getFirst();
    int width = sampleImage.getWidth();
    int height = sampleImage.getHeight();
    int sizeOfSingleJointImage = width * height;
    RegionOfInterest jointRoi = RegionOfInterest.createRegionFromImage(sampleImage);
    RegionOfInterest targetRoi = new RegionOfInterest(jointRoi);

    Image aggregateImage = new Image(width , height * (numberOfJoints + numberOfImagesToMedian));
    for (Pair<Float, Integer> graylevelAndJoint : meanGraylevelAndJointIndexList)
    {
      Image jointImage = jointIndexToImageMap.get(graylevelAndJoint.getSecond()).getFirst();
      Assert.expect(jointImage.getWidth() * jointImage.getHeight() == sizeOfSingleJointImage);

      Transform.copyImageIntoImage(jointImage, jointRoi, aggregateImage, targetRoi);
      targetRoi.translateXY(0, jointRoi.getHeight());
    }
    if (useOriginalMethod == false)
    {
      // This is to fix bug where last few expected images are not doing median calculation
      // At the same time, we do not want to change existing behaviour in older recipes.
      // added by Seng Yew on 1-Dec-2010
      int iCount = 0;
      for (Pair<Float, Integer> graylevelAndJoint : meanGraylevelAndJointIndexList)
      {
        Image jointImage = jointIndexToImageMap.get(graylevelAndJoint.getSecond()).getFirst();
        Assert.expect(jointImage.getWidth() * jointImage.getHeight() == sizeOfSingleJointImage);

        Transform.copyImageIntoImage(jointImage, jointRoi, aggregateImage, targetRoi);
        targetRoi.translateXY(0, jointRoi.getHeight());
        iCount += 1;
        if (iCount >= numberOfImagesToMedian)
          break;
      }
    }

    // convert from the stacked representation to the one image per row representation
    // ie, from width x (height * N) to (width*height) x N
    int originalStride = aggregateImage.getStride();
    Assert.expect(sampleImage.getStride() == originalStride);
    aggregateImage.setWidthAndHeight((originalStride/4)*height, (numberOfJoints+numberOfImagesToMedian), originalStride*height);

    RegionOfInterest regionOfInterestToApplyMedian = RegionOfInterest.createRegionFromImage(aggregateImage);
    regionOfInterestToApplyMedian.setHeightKeepingSameMinY((numberOfJoints+numberOfImagesToMedian)-(numberOfImagesToMedian-1));

    Image aggregateMedianImage = Filter.median(aggregateImage, regionOfInterestToApplyMedian, 1, numberOfImagesToMedian);
    aggregateImage.decrementReferenceCount();

    // convert from (width*height) x N to width x (height * N)
    // (from the one image per row to stacked representation)
    aggregateMedianImage.setWidthAndHeight(width, height * aggregateMedianImage.getHeight(), originalStride);

    return aggregateMedianImage;
  }

  private final float _FLOOD_FILL_INTENSITY = 300.f; // larger than the image values
  private final float _VISITED_PIXEL_INTENSITY = _FLOOD_FILL_INTENSITY + 1.f; // larger than _FLOOD_FILL_INTENSITY

  /**
   * One problem with our approach is that our expected images - which are median images of joints in the same region -
   * is that we could have a void in our expected image. This step fills in any of these voids so that there are none of these
   * concave regions.
   *
   * I'm not certain 'convex' is the most accurate term to describe the shape, but it should convey the idea of this approach.
   *
   * @author Patrick Lacz
   */
  private void makeAggregateMedianImageConvex(Image aggregateMedianImage, int widthOfJointImage, int heightOfJointImage)
  {
    Assert.expect(aggregateMedianImage != null);
    Assert.expect(widthOfJointImage > 0);
    Assert.expect(heightOfJointImage > 0);

    int numberOfImages = aggregateMedianImage.getHeight() / heightOfJointImage;
    if (numberOfImages == 0)
      return;

    RegionOfInterest subImageRoi = new RegionOfInterest(0, 0, widthOfJointImage, heightOfJointImage, 0, RegionShapeEnum.RECTANGULAR);

    // the flood fill image has a one pixel border around the entire image. This is used so that the flood fill algorithm
    // visits all the pixels around the edge of the image.
    Image floodFillImage = new Image(widthOfJointImage + 2, heightOfJointImage + 2);
    Paint.fillImage(floodFillImage, _FLOOD_FILL_INTENSITY);

    RegionOfInterest floodFillRoi = new RegionOfInterest(subImageRoi);
    floodFillRoi.setMinXY(1, 1);

    for (int i = 0 ; i < numberOfImages ; ++i)
    {
      subImageRoi.translateXY(0, heightOfJointImage * i);

      Transform.copyImageIntoImage(aggregateMedianImage, subImageRoi, floodFillImage, floodFillRoi);

      // allow decreasing gradients from the edge, but not increasing (or just barely - the .25 should cover up the bumps)
      Paint.floodFillGradient(floodFillImage,
                              RegionOfInterest.createRegionFromImage(floodFillImage),
                              0, 0,
                              _FLOOD_FILL_INTENSITY,
                              2.f * _FLOOD_FILL_INTENSITY, 0.25f);

      // identify the pixels still within the normal 0-255 range. These are the concave regions.
      // inside of each region, find the maximum of the perimeter. We're going to fill the region with that value.
      Pair<ImageCoordinate, Float> searchResult = Threshold.searchForPixelInRange(floodFillImage, floodFillRoi, 0.0f, _FLOOD_FILL_INTENSITY - 0.01f);
      while (searchResult != null)
      {
        ImageCoordinate searchResultLocation = searchResult.getFirst();
        float pixelIntensity = searchResult.getSecond();
        identifyAndFillConcaveRegion(floodFillImage,
                                     floodFillRoi,
                                     searchResultLocation.getX(), searchResultLocation.getY(),
                                     pixelIntensity,
                                     aggregateMedianImage, subImageRoi);
        searchResult = Threshold.searchForNextPixelInRange(floodFillImage, floodFillRoi, 0.0f, _FLOOD_FILL_INTENSITY - 0.01f, searchResultLocation);
      }

      subImageRoi.setMinXY(0, 0);
    }

    floodFillImage.decrementReferenceCount();
  }

  /**
   * Starting with the identified pixel - which is required to be inside a concave region, find the edges of this region.
   * Take the minimum pixel value along the edge and fill the region with that value.
   *
   * So, techinically filling won't be fully convex -- there will be a lip around each region (at a minimum).
   * Future research could eliminate this or fit a surface over the hole (see the copious hole-filling research)
   *
   * @author Patrick Lacz
   */
  private void identifyAndFillConcaveRegion(Image floodFillImage,
                                            RegionOfInterest floodFillRoi,
                                            int x, int y, float pixelIntensity,
                                            Image aggregateMedianImage, RegionOfInterest subImageRoi)
  {
    Assert.expect(floodFillImage != null);
    Assert.expect(floodFillRoi != null);
    Assert.expect(aggregateMedianImage != null);
    Assert.expect(subImageRoi != null);

    List<ImageCoordinate> pixelsInRegion = new ArrayList<ImageCoordinate>();
    float minimumEdgeIntensity = searchForMinimumEdgeIntensity(floodFillImage, x, y, pixelsInRegion);

    Assert.expect(minimumEdgeIntensity != Float.MAX_VALUE);

    for (ImageCoordinate coordinate : pixelsInRegion)
    {
      aggregateMedianImage.setPixelValue(subImageRoi.getMinX() + coordinate.getX(), subImageRoi.getMinY() + coordinate.getY(), minimumEdgeIntensity);
    }
  }

  /**
   * This routine tries to find the pixels around the edge of a contiguous region.
   * The minimum of this set of pixels will be returned.
   *
   * Change recursive method to while loop. - Lim, Seng-Yew
   *
   * @author Patrick Lacz
   */
  private float searchForMinimumEdgeIntensity(Image floodFillImage,
                                              int x, int y,
                                              List<ImageCoordinate> pixelsInRegion)
  {
    Assert.expect(floodFillImage != null);
    Assert.expect(x >= 0);
    Assert.expect(y >= 0);
    Assert.expect(pixelsInRegion != null);

    float minimum = Float.MAX_VALUE;
    float currentPixelIntensity = floodFillImage.getPixelValue(x, y);
    int xCurrent, yCurrent;
    xCurrent = x;
    yCurrent = y;

    Stack<Pair<ImageCoordinate, Float>> pixelStack = new Stack<Pair<ImageCoordinate, Float>>();
    Pair<ImageCoordinate, Float> currentPair;
    pixelStack.push(new Pair<ImageCoordinate, Float>(new ImageCoordinate(xCurrent, yCurrent),currentPixelIntensity));
    while (pixelStack.size() != 0)
    {
      currentPair = pixelStack.pop();
      ImageCoordinate currentPixelLocation = currentPair.getFirst();
      xCurrent = currentPixelLocation.getX();
      yCurrent = currentPixelLocation.getY();
      currentPixelIntensity = currentPair.getSecond();
      if (currentPixelIntensity == _FLOOD_FILL_INTENSITY || currentPixelIntensity == _VISITED_PIXEL_INTENSITY)
        continue;

      floodFillImage.setPixelValue(xCurrent, yCurrent, _VISITED_PIXEL_INTENSITY); // we don't want to visit this pixel again.
      float northIntensity = floodFillImage.getPixelValue(xCurrent, yCurrent - 1);
      float southIntensity = floodFillImage.getPixelValue(xCurrent, yCurrent + 1);
      float westIntensity = floodFillImage.getPixelValue(xCurrent - 1, yCurrent);
      float eastIntensity = floodFillImage.getPixelValue(xCurrent + 1, yCurrent);

      if (northIntensity == _FLOOD_FILL_INTENSITY || southIntensity == _FLOOD_FILL_INTENSITY ||
          westIntensity == _FLOOD_FILL_INTENSITY || eastIntensity == _FLOOD_FILL_INTENSITY)
      {
        // we are a border pixel, we can override the minimum-so-far
        if (currentPixelIntensity < minimum)
          minimum = currentPixelIntensity;
      }

      pixelsInRegion.add(new ImageCoordinate(xCurrent, yCurrent));

      // Reverse the original north, south, west, east order
      // to fully replicate the original recursive function behaviour.
      if (eastIntensity < _FLOOD_FILL_INTENSITY)
        pixelStack.push(new Pair<ImageCoordinate, Float>(new ImageCoordinate(xCurrent+1, yCurrent),eastIntensity));
      if (westIntensity < _FLOOD_FILL_INTENSITY)
        pixelStack.push(new Pair<ImageCoordinate, Float>(new ImageCoordinate(xCurrent-1, yCurrent),westIntensity));
      if (southIntensity < _FLOOD_FILL_INTENSITY)
        pixelStack.push(new Pair<ImageCoordinate, Float>(new ImageCoordinate(xCurrent, yCurrent+1),southIntensity));
      if (northIntensity < _FLOOD_FILL_INTENSITY)
        pixelStack.push(new Pair<ImageCoordinate, Float>(new ImageCoordinate(xCurrent, yCurrent-1),northIntensity));
    }
    return minimum;
  }

  /**
   * Create a map from a joint index to an image of that joint
   * @author Patrick Lacz
   */
  private Map<Integer, Pair<Image, RegionOfInterest> > buildJointImageMap(List<JointInspectionData> jointInspectionDataObjects,
      ReconstructedSlice slice, float fractionOfDiameter, int standardizedDiameterInPixels,  boolean applyBlur, final float MILLIMETERS_PER_PIXEL)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(slice != null);

    SliceNameEnum sliceName = slice.getSliceNameEnum();
    Image sliceImage = slice.getOrthogonalImage();

    // For the first iteration through the joints, extract the joint's image normalized to the nominal size.
    Map<Integer, Pair<Image, RegionOfInterest>>
        jointIndexToImageMap = new HashMap<Integer, Pair<Image, RegionOfInterest>>(jointInspectionDataObjects.size());

    List<DiagnosticInfo> padRegionDiagnosticInfoList = new ArrayList<DiagnosticInfo>(jointInspectionDataObjects.size());

    int jointIndex = 0;
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      RegionOfInterest locatedRegionOfInterest = Locator.getRegionOfInterestAtMeasuredLocation(joint);

      JointMeasurement diameterMeasurement = GridArrayMeasurementAlgorithm.getDiameterMeasurement(joint, sliceName);
      float measuredDiameterInMM = diameterMeasurement.getValue();
      float measuredDiameterInPixels = measuredDiameterInMM / MILLIMETERS_PER_PIXEL;

      int thumbnailDiameter = (int)Math.ceil(fractionOfDiameter * measuredDiameterInPixels);
      if (thumbnailDiameter < 5 || diameterMeasurement.isMeasurementValid() == false)
      {
        LocalizedString warningText = new LocalizedString("ALGDIAG_GRIDARRAY_VOIDING_MISSING_JOINT_WARNING_KEY",
                                                          new Object[]{joint.getFullyQualifiedPadName(), sliceName.getName()});
        AlgorithmUtil.raiseAlgorithmWarning(warningText);
        continue;
      }

      locatedRegionOfInterest.setWidthKeepingSameCenter(thumbnailDiameter);
      locatedRegionOfInterest.setHeightKeepingSameCenter(thumbnailDiameter);

      if (locatedRegionOfInterest.fitsWithinImage(sliceImage) == false)
      {
        // joint extending over image boundaries.
        continue;
      }

      padRegionDiagnosticInfoList.add(new MeasurementRegionDiagnosticInfo(locatedRegionOfInterest, MeasurementRegionEnum.PAD_REGION));

      Image jointImage = Image.createCopy(sliceImage, locatedRegionOfInterest);

      Image standardizedJointImage = Transform.resizeImage(jointImage, standardizedDiameterInPixels,
          standardizedDiameterInPixels);

      jointImage.decrementReferenceCount();

      if (applyBlur)
      {
        Image blurredStandardizedJointImage = Filter.convolveLowpass(standardizedJointImage);
        standardizedJointImage.decrementReferenceCount();
        standardizedJointImage = blurredStandardizedJointImage;
      }
      standardizedJointImage.incrementReferenceCount(); // the map's reference
      jointIndexToImageMap.put(jointIndex, new Pair<Image, RegionOfInterest>(standardizedJointImage, locatedRegionOfInterest));

      standardizedJointImage.decrementReferenceCount(); // this routine is finished with the image
      ++jointIndex;
    }

    JointInspectionData firstJoint = jointInspectionDataObjects.iterator().next();

    _diagnostics.postDiagnostics(firstJoint.getInspectionRegion(), sliceName, firstJoint.getSubtype(), this, false, false,
                                 padRegionDiagnosticInfoList.toArray(new DiagnosticInfo[padRegionDiagnosticInfoList.size()]));

    return jointIndexToImageMap;
  }

  /**
   * Builds a sorted table of graylevels mapping to joint indicies.
   *
   * @author Patrick Lacz
   */
  private ArrayList< Pair< Float, Integer > > buildGraylevelTable(
      List<JointInspectionData> jointInspectionDataObjects,
      Map<Integer, Pair<Image, RegionOfInterest> > jointIndexToImageMap,
      SliceNameEnum sliceName)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointIndexToImageMap != null);
    Assert.expect(sliceName != null);

    ArrayList<Pair<Float, Integer>> meanGraylevelAndJointIndexList =
      new ArrayList<Pair<Float, Integer>>(jointInspectionDataObjects.size());

    int jointIndex = 0;
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      // Get the image of the joint we are interested in.
      Pair<Image, RegionOfInterest> jointImageRegionPair = jointIndexToImageMap.get(jointIndex);
      if (jointImageRegionPair == null)
        continue;
      Image jointImage = jointImageRegionPair.getFirst();

      // compute the average graylevel (could use median, etc)
      float meanGraylevel = Statistics.mean(jointImage);

      // add the measurement to an array that we can later use to sort.
      meanGraylevelAndJointIndexList.add(new Pair<Float, Integer>(meanGraylevel, jointIndex));

      ++jointIndex;
    }

    // Sort the list by graylevel.
    Collections.sort(meanGraylevelAndJointIndexList, new PairComparator<Float, Integer>());

    return meanGraylevelAndJointIndexList;
  }

  /**
   * Determines which median image to use for the given graylevel.
   * We try to use the median image that was computed using the images closest to the target graylevel.
   * We do this by starting with the closest graylevel to the input graylevel. Then either add the
   * entry above or below it in the list, based on which has a lower difference from the original
   * threshold.
   *
   * Once we have enough images (or there is not enough space), we stop and return the first index in
   * our set. Since our set is contiguous with a known size, this uniquely identifies which median image
   * to use.
   *
   * @author Patrick Lacz
   */
  private int findIndexOfMedianForGraylevel(float graylevel, ArrayList< Pair<Float, Integer> > graylevelList)
  {
    Assert.expect(graylevelList != null);
    Assert.expect(graylevelList.isEmpty() == false);

    if (graylevelList.size() <= _NUMBER_OF_IMAGES_TO_SAMPLE)
      return 0;

    int numberOfImagesAdded = 0; // finished when == _NUMBER_OF_IMAGES_TO_SAMPLE
    float upperGraylevel; // highest graylevel in the set of images
    float lowerGraylevel; // lowest graylevel in the set of images

    // find the starting point - the closest match (should be ==)
    int matchIndex = 0;
    float matchDifference = Float.POSITIVE_INFINITY;

    // this assumes that the graylevelList has been sorted, and that integer access to the list is efficient (ArrayList)
    // find the index of the entry with the graylevel closest to the target graylevel
    int listSize = graylevelList.size();
    while (matchIndex < listSize)
    {
       Pair<Float, Integer> valueInList = graylevelList.get(matchIndex);
       float currentDifference = Math.abs(graylevel - valueInList.getFirst());
       if (currentDifference > matchDifference)
       {
         break;
       }
       matchDifference = currentDifference;
       ++matchIndex;
    }
    // back up one, this loop ends with matchIndex one too high
    --matchIndex;

    numberOfImagesAdded = 1; // the closest entry is the first one added.
    upperGraylevel = lowerGraylevel = graylevelList.get(matchIndex).getFirst();

    // The images we are using are [matchIndex, matchIndex+numberOfImagesAdded)
    // So if we add the image below matchIndex to the set, both matchIndex and numberOfImagesAdded increase,
    // otherwise only numberOfImagesAdded increases.
    while (numberOfImagesAdded < _NUMBER_OF_IMAGES_TO_SAMPLE)
    {
      float lowerDifference = Float.POSITIVE_INFINITY;
      float upperDifference = Float.POSITIVE_INFINITY;
      if (matchIndex > 0)
        lowerDifference = lowerGraylevel - graylevelList.get(matchIndex-1).getFirst();
      if (matchIndex + numberOfImagesAdded < listSize)
        upperDifference = graylevelList.get(matchIndex+numberOfImagesAdded).getFirst() - upperGraylevel;

      if (lowerDifference < upperDifference)
        --matchIndex; // lower the match index
      ++numberOfImagesAdded; // every pass we add one entry
    }

    return matchIndex;
  }

  /**
   * @author Patrick Lacz
   */
  protected void classifyMeasurementGroup(MeasurementGroup measurementGroup,
                                          Map<Pad, ReconstructionRegion> padToRegionMap)
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(padToRegionMap != null);

    Subtype subtype = measurementGroup.getSubtype();
    classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.MIDBALL,
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL)).floatValue(),
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_MIDBALL)).floatValue());
    classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.PAD,
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD)).floatValue(),
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PAD)).floatValue());
    classifyNumberVoidingMeasurementGroupSlice(measurementGroup, SliceNameEnum.MIDBALL,
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL)).floatValue(),
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_MIDBALL)).floatValue());
    
    if (hasLowerPadSlice(subtype))
    {
        classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.BGA_LOWERPAD_ADDITIONAL,
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD)).floatValue(),
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_LOWERPAD)).floatValue());
    }    
    
    classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.PACKAGE,
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE)).floatValue(),
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PACKAGE)).floatValue());
    
    // Added by Lee Herng 8 April 2012 - Handle user-defined slice
    int totalUserDefinedSlice = getNumberOfUserDefinedSlice(subtype);
    if (totalUserDefinedSlice == 1)
    {
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1)).floatValue());
    }
    else if (totalUserDefinedSlice == 2)
    {
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2)).floatValue());
    }
  }
  
  /*
   * @Author Kee Chin Seong
   */
  private void classifyNumberVoidingMeasurementGroupSlice(MeasurementGroup measurementGroup,
                                                    SliceNameEnum sliceName,
                                                    float processVoidingThreshold,
                                                    float percentOfJointsThreshold)
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(sliceName != null);

    Subtype subtype = measurementGroup.getSubtype();

    // compute the number of failing joints
    int numberOfFailing = 0;
   
    //Kee Chin Seong - Component Midball Voiding
    float maximumNumberPinFailedMidBallVoid = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID); 
    for (JointInspectionResult jointResult : measurementGroup.getJointMeasurements())
    {
      if (jointResult.hasJointMeasurement(sliceName, MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT))
      {
        float percentFailingPadSlice = jointResult.getJointMeasurement(sliceName, MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT).getValue();
        
        //percent of failing over the threshold
        if (percentFailingPadSlice >= processVoidingThreshold)
          ++numberOfFailing;
      }
    }

    if(numberOfFailing == 0)
      return;

    if(maximumNumberPinFailedMidBallVoid != 0)
    {
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_1,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_2,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_3,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_4,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_5,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_6,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_7,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_8,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_9,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_10,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_11,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_12,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_13,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_14,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_15,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_16,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_17,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_18,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_19,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19)).floatValue());
      
      classifyMeasurementGroupSlice(measurementGroup, SliceNameEnum.GRID_ARRAY_USER_DEFINED_SLICE_20,
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20)).floatValue(),
                                ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20)).floatValue());
      
      if(numberOfFailing > maximumNumberPinFailedMidBallVoid)
      {
            ComponentInspectionData componentInspectionData = measurementGroup.getComponentInspectionData();

            if (Config.isComponentLevelClassificationEnabled())
            {
              ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
              ComponentMeasurement percentFailingPadSliceMeasurement = new ComponentMeasurement(
                this,
                measurementGroup.getSubtype(),
                MeasurementEnum.GRID_ARRAY_COMPONENT_MIDBALL_VOIDING,
                MeasurementUnitsEnum.NONE,
                componentInspectionData.getComponent(),
                sliceName,
                numberOfFailing);
              componentInspectionResult.addMeasurement(percentFailingPadSliceMeasurement);

              ComponentIndictment voidingIndictment = new ComponentIndictment(IndictmentEnum.COMPONENT_VOIDING,
                                                                                this,
                                                                                sliceName,
                                                                                subtype,
                                                                                subtype.getJointTypeEnum());
              voidingIndictment.addFailingMeasurement(percentFailingPadSliceMeasurement);
              componentInspectionResult.addIndictment(voidingIndictment);
            }
            else
            {
              JointMeasurement jointPercentFailingPadSliceMeasurement = new JointMeasurement(
                    this,
                    MeasurementEnum.GRID_ARRAY_COMPONENT_MIDBALL_VOIDING,
                    MeasurementUnitsEnum.NONE,
                    componentInspectionData.getPadOneJointInspectionData().getPad(),
                    sliceName,
                    numberOfFailing);
              componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addMeasurement(jointPercentFailingPadSliceMeasurement);
              
              JointIndictment voidingIndictment = new JointIndictment(IndictmentEnum.COMPONENT_VOIDING, this, sliceName);
              voidingIndictment.addFailingMeasurement(jointPercentFailingPadSliceMeasurement);
              componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(voidingIndictment);
            }
       } 
    }
  }

  /**
   * For the measurement group, we examine the number of joints failing a (presumably) lower threshold than the
   * joint indictments. The idea is that the client would like to know if there is quite a bit of voiding going
   * on in the process, even if on each joint it isn't as severe as their indictment threshold.
   *
   * @author Patrick Lacz
   */
  private void classifyMeasurementGroupSlice(MeasurementGroup measurementGroup,
                                             SliceNameEnum sliceName,
                                             float processVoidingThreshold,
                                             float percentOfJointsThreshold)
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(sliceName != null);

    Subtype subtype = measurementGroup.getSubtype();

    // compute the number of failing joints
    int numberOfFailing = 0;
    int totalNumber = 0;
    for (JointInspectionResult jointResult : measurementGroup.getJointMeasurements())
    {
      if (jointResult.hasJointMeasurement(sliceName, MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT))
      {
        float percentFailingPadSlice = jointResult.getJointMeasurement(sliceName, MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT).getValue();
        if (percentFailingPadSlice >= processVoidingThreshold)
          ++numberOfFailing;
        ++totalNumber;
      }
    }

    if(totalNumber == 0)
      return;

    float percentFailing = 100.f * (float)numberOfFailing / totalNumber;
    ComponentInspectionData componentInspectionData = measurementGroup.getComponentInspectionData();

    ComponentMeasurement percentFailingPadSliceMeasurement  = null;
    JointMeasurement jointPercentFailingPadSliceMeasurement = null;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      percentFailingPadSliceMeasurement = new ComponentMeasurement(
        this,
        measurementGroup.getSubtype(),
        MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT,
        MeasurementUnitsEnum.PERCENT,
        componentInspectionData.getComponent(),
        sliceName,
        percentFailing);
      componentInspectionData.getComponentInspectionResult().addMeasurement(percentFailingPadSliceMeasurement);
    }
    else
    {
      jointPercentFailingPadSliceMeasurement = new JointMeasurement(
        this,
        MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT,
        MeasurementUnitsEnum.PERCENT,
        componentInspectionData.getPadOneJointInspectionData().getPad(),
        sliceName,
        percentFailing);
      componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addMeasurement(jointPercentFailingPadSliceMeasurement);
    }

    // compare with the threshold
    if (percentFailing >= percentOfJointsThreshold)
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (percentFailingPadSliceMeasurement != null)
        {
          ComponentIndictment voidingIndictment = new ComponentIndictment(IndictmentEnum.COMPONENT_VOIDING,
                                                                          this,
                                                                          sliceName,
                                                                          subtype,
                                                                          subtype.getJointTypeEnum());
          voidingIndictment.addFailingMeasurement(percentFailingPadSliceMeasurement);
          componentInspectionData.getComponentInspectionResult().addIndictment(voidingIndictment);
        }
      }
      else
      {
        if (jointPercentFailingPadSliceMeasurement != null)
        {
          JointIndictment voidingIndictment = new JointIndictment(IndictmentEnum.COMPONENT_VOIDING, this, sliceName);
          voidingIndictment.addFailingMeasurement(jointPercentFailingPadSliceMeasurement);          
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(voidingIndictment);          
        }
      }
    }
  }

  /**
   * @author Sunit Bhalla
   * Turn voiding off by default for CGA, non-collapsible BGA's and CSP subtypes
   */
  public boolean algorithmIsEnabledByDefault(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if ((subtype.getJointTypeEnum().equals(JointTypeEnum.CGA)) ||
        (subtype.getJointTypeEnum().equals(JointTypeEnum.NON_COLLAPSABLE_BGA)) ||
        (subtype.getJointTypeEnum().equals(JointTypeEnum.CHIP_SCALE_PACKAGE)))
      return false;
    else
      return true;
  }

  /**
   * @author Cheah Lee Herng 
   */
  private boolean hasLowerPadSlice(Subtype subtype)
  {
      Assert.expect(subtype != null);
      
      if (subtype.getJointTypeEnum().equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR))
          return true;
      else
          return false;
  }
  
   /**
   * @author Jack Hwee
   */
  static public boolean isFloodFillMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_METHOD);

    if (voidingChoice.equals(_FLOODFILL_METHOD))
    {
      return true;
    }

    return false;
  }

  /**
   * @author Cheah Lee Herng 
   */
  private static int getNumberOfUserDefinedSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE);

    return Integer.parseInt(numberOfSlice);
  }
  
  /**
   * @author Siew Yeng
   */
  private float getIndividualVoidPercentDiameterFailThreshold(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    float threshold = 100.f;
    if(sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PAD);
    }
    else if(sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_MIDBALL);
    }
    else if(sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PACKAGE);
    }
    
    return threshold;
  }
  
  /**
   * @author Siew Yeng
   */
  private float getVoidingPercentOfDiameterToTestThreshold(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    float threshold = 100.f;
    if(sliceNameEnum.equals(SliceNameEnum.PAD))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD);
    }
    else if(sliceNameEnum.equals(SliceNameEnum.MIDBALL))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL);
    }
    else if(sliceNameEnum.equals(SliceNameEnum.PACKAGE))
    {
      threshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE);
    }
    
    return threshold;
  }
  
  /**
   * @author Siew Yeng
   */
  static boolean shouldIndividualVoidPercentDiameterBeTested(SliceNameEnum sliceNameEnum)
  {
    if(sliceNameEnum.equals(SliceNameEnum.MIDBALL) || 
       sliceNameEnum.equals(SliceNameEnum.PAD) ||
       sliceNameEnum.equals(SliceNameEnum.PACKAGE))
      return true;
    
    return false;
  }
  
  /**
   * @author Siew Yeng 
   */
  private float calculateIndividualVoidDiameterPercent(float[] largestVoidImageArray, 
                                                        int imageWidth, 
                                                        int imageHeight, 
                                                        Subtype subtype, 
                                                        SliceNameEnum sliceNameEnum, 
                                                        float MILLIMETERS_PER_PIXEL)
  {
    int minX = imageWidth;
    int minY = imageHeight;
    int maxX = 0;
    int maxY = 0;
    for (int i = 0; i < imageHeight; i++)
    {
      for (int j = 0; j < imageWidth; j++)
      {
        if(largestVoidImageArray[i*imageWidth + j] == 255)
        {
          if(i < minY)
            minY = i;
          if(i > maxY)
            maxY = i;

          if(j < minX)
            minX = j;
          if(j > maxX)
            maxX = j;
        }
      }
    }
    
    double voidDiameterInPixels = Math.max((maxX - minX) + 1, (maxY - minY) + 1);
    float nominalDiameterInMils = MathUtil.convertMillimetersToMils(GridArrayMeasurementAlgorithm.getNominalDiameterAlgorithmSettingValue(subtype, sliceNameEnum));
    float fractionOfDiameterMidball = 0.01f * getVoidingPercentOfDiameterToTestThreshold(subtype, sliceNameEnum);
    int standardizedDiameterMidballInPixels = (int)Math.round(Math.ceil((nominalDiameterInMils * fractionOfDiameterMidball) / MathUtil.convertMillimetersToMils(MILLIMETERS_PER_PIXEL)));

    float individualVoidDiameterPercent = 100.f * (float)voidDiameterInPixels / standardizedDiameterMidballInPixels;
    
    return individualVoidDiameterPercent;
  }
}
