package com.axi.v810.business.imageAnalysis;

import java.util.*;
import java.io.*;
import java.math.*;
import java.text.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.chip.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.license.*;

/**
 * Some common algorithm utility methods.  These methods are shared among the algorithms, but are
 * specific to the xray codebase; as such, they don't belong in the com.axi.util.image package.
 *
 * @author Matt Wharton
 */
public class AlgorithmUtil
{
  //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
  public enum MinMaxThresholdChekingModeEnum
  {
    CHECK_MINIMUM_THRESHOLD_ONLY,
    CHECK_MAXIMUM_THRESHOLD_ONLY,
    CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD
  }
  //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

  private static Map<MagnificationEnum, Float> _magnificationEnumToMilsPerPixelMap =
      new HashMap<MagnificationEnum, Float>();
  private static Map<MagnificationEnum, Float> _magnificationEnumToMillimetersPerPixelMap =
      new HashMap<MagnificationEnum, Float>();

  private static AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();
  private static boolean _isIntelligentLearning = Config.isIntelligentLearning();
  private static boolean _isDeveloperDebugModeOn = Config.isDeveloperDebugModeOn();

  /**
   * Constructor.
   *
   * All methods are static, so shouldn't ever need to instantiate.
   *
   * @author Matt Wharton
   */
  private AlgorithmUtil()
  {
    // Do nothing...
  }

  /**
   * Create a profile of what a joint would look like if it was totally open.
   *
   * @author Peter Esbensen
   */
  static public float[] createFakeOpenJointProfileInMils(float[] goodJointProfileInMils, Subtype subtype)
  {
    Assert.expect(goodJointProfileInMils != null);

    int lengthOfProfile = goodJointProfileInMils.length;
    float milsPerPixel = MathUtil.convertNanoMetersToMils(getNanometersPerPixel(subtype));

    // convert the profile to "pixel units" so that both the x and the f(x) axes are in the same units
    float[] goodJointProfileInPixels = new float[ lengthOfProfile ];
    for (int i = 0; i < lengthOfProfile ; ++i)
    {
      goodJointProfileInPixels[i] = goodJointProfileInMils[i] / milsPerPixel;
    }

    // get the solder volume of the good joint
    float goodJointSolderVolume = ArrayUtil.sum(goodJointProfileInPixels);

    // estimate the bad joint as half of an ellipse, keeping the volume the same
    double majorAxisLength = lengthOfProfile;

    // the "semiminor axis" is half of the minor axis, the "semimajor axis" is half the major axis
    double semiMajorAxisLength = majorAxisLength * 0.5;

    // derive minor axis by solving AreaOfHalfEllipse = (PI * major * minor) / 2
    double semiMinorAxisLength = 2.0 * goodJointSolderVolume / ( Math.PI * semiMajorAxisLength);

    // now create the fake profile
    float[] fakeOpenJointProfile = new float[lengthOfProfile];
    int midpoint = lengthOfProfile / 2;
    for (int i = 0; i < lengthOfProfile; ++i)
    {
      // use quadratic formula to solve ellipse equation for y
      double a = 1.0 / (semiMinorAxisLength * semiMinorAxisLength);
      double b = 0.0;
      double c = -1.0 + ( (double)(i - midpoint) * (double)(i - midpoint) ) / (semiMajorAxisLength * semiMajorAxisLength);
      double y = ( -1 * b + Math.sqrt( b * b - 4.0 * a * c ) ) / ( 2.0 * a );
      if ((Double.isNaN(y) == false) && (Double.isInfinite(y) == false))
        fakeOpenJointProfile[i] = (float)y * milsPerPixel;
      else
        fakeOpenJointProfile[i] = 0f;  // this can happen sometimes
    }
    return fakeOpenJointProfile;
  }


  /**
   * Converts the specified gray level profile to a thickness profile using the specified percentile
   * of the gray level profile as the background.
   * Negative delta gray values are capped at zero.
   * The returned thickness profile is in mils.
   *
   * @author Matt Wharton
   */
  public static float[] convertGrayLevelProfileToThicknessProfileInMils(float[] grayLevelProfile,
                                                                        float backgroundPercentile,
                                                                        SolderThickness thicknessTable,
                                                                        int backgroundSensitivityOffset)
  {
    Assert.expect(grayLevelProfile != null);
    Assert.expect((backgroundPercentile >= 0f) && (backgroundPercentile <= 1f));

    // Make a background estimate for this profile using the specified percentile.
    float backgroundGrey = StatisticsUtil.percentile(grayLevelProfile, backgroundPercentile);
    backgroundGrey = Math.min(backgroundGrey, 255);
    backgroundGrey = Math.max(backgroundGrey, 0);    
    // Create a new array to hold the thickness profile.
    float[] thicknessProfile = new float[grayLevelProfile.length];

    // Convert each gray level profile value to a thickness value.
    for (int i = 0; i < thicknessProfile.length; ++i)
    {
      float absoluteGreyLevel = grayLevelProfile[i];
      float deltaGreyLevel = backgroundGrey - absoluteGreyLevel;
      deltaGreyLevel = Math.max(deltaGreyLevel, 0);
      thicknessProfile[i] = thicknessTable.getThicknessInMils(backgroundGrey, deltaGreyLevel, backgroundSensitivityOffset);
    }

    return thicknessProfile;
  }

  /**
   * Converts the specified gray level profile to a thickness profile using the specified percentile
   * of the gray level profile as the background.
   * Negative delta gray values are capped at zero.
   * The returned thickness profile is in millimeters.
   *
   * @author Matt Wharton
   */
  public static float[] convertGrayLevelProfileToThicknessProfileInMillimeters(float[] grayLevelProfile,
                                                                               float backgroundPercentile,
                                                                               SolderThickness thicknessTable,
                                                                               int backgroundSensitivityOffset)
  {
    Assert.expect(grayLevelProfile != null);
    Assert.expect(thicknessTable != null);
    Assert.expect((backgroundPercentile >= 0f) && (backgroundPercentile <= 1f));

    // Make a background estimate for this profile using the specified percentile.
    float backgroundGrey = StatisticsUtil.percentile(grayLevelProfile, backgroundPercentile);

    // Create a new array to hold the thickness profile.
    float[] thicknessProfile = new float[grayLevelProfile.length];

    // Convert each gray level profile value to a thickness value.
    for (int i = 0; i < thicknessProfile.length; ++i)
    {
      float absoluteGreyLevel = grayLevelProfile[i];
      float deltaGreyLevel = backgroundGrey - absoluteGreyLevel;
      deltaGreyLevel = Math.max(deltaGreyLevel, 0);
      backgroundGrey = Math.min(backgroundGrey, 255);
      backgroundGrey = Math.max(backgroundGrey, 0);
      thicknessProfile[i] = thicknessTable.getThicknessInMillimeters(backgroundGrey, deltaGreyLevel, backgroundSensitivityOffset);
    }

    return thicknessProfile;
  }

  /**
   * Creates a thickness profile given the specified foreground and background gray level profiles.
   * Negative delta gray values are capped at zero.
   * The returned thickness profile is in mils.
   *
   * @author Matt Wharton
   */
  public static float[] convertGrayLevelProfileToThicknessProfileInMils(float[] foregroundGrayLevelProfile,
                                                                        float[] backgroundGrayLevelProfile,
                                                                        SolderThickness thicknessTable,
                                                                        int backgroundSensitivityOffset)
  {
    Assert.expect(foregroundGrayLevelProfile != null);
    Assert.expect(backgroundGrayLevelProfile != null);
    Assert.expect(foregroundGrayLevelProfile.length == backgroundGrayLevelProfile.length);
    Assert.expect(thicknessTable != null);

    // Create a new array to hold the thickness profile.
    float[] thicknessProfile = new float[foregroundGrayLevelProfile.length];

    // Convert each gray level profile value to a thickness value.
    for (int i = 0; i < thicknessProfile.length; ++i)
    {
      float backgroundGrayValue = backgroundGrayLevelProfile[i];
      float foregroundGrayValue = foregroundGrayLevelProfile[i];
      float deltaGrayLevel = backgroundGrayValue - foregroundGrayValue;
      deltaGrayLevel = Math.max(deltaGrayLevel, 0);
      backgroundGrayValue = Math.min(backgroundGrayValue, 255);
      backgroundGrayValue = Math.max(backgroundGrayValue, 0);
      thicknessProfile[i] = thicknessTable.getThicknessInMils(backgroundGrayValue, deltaGrayLevel, backgroundSensitivityOffset);
      // wei chin fixes (ensure there are not negative thickness.)
      thicknessProfile[i] = Math.max(thicknessProfile[i], 0.0f);
      //Siew Yeng - XCR-2591
      if(Float.isNaN(thicknessProfile[i]))
        thicknessProfile[i] = 0.0f;
    }

    return thicknessProfile;
  }

  /**
   * Creates a thickness profile given the specified foreground and background gray level profiles.
   * Negative delta gray values are capped at zero.
   * The returned thickness profile is in millimeters.
   *
   * @author Matt Wharton
   */
  public static float[] convertGrayLevelProfileToThicknessProfileInMillimeters(float[] foregroundGrayLevelProfile,
                                                                               float[] backgroundGrayLevelProfile,
                                                                               SolderThickness thicknessTable,
                                                                               int backgroundSensitivityOffset)
  {
    Assert.expect(foregroundGrayLevelProfile != null);
    Assert.expect(backgroundGrayLevelProfile != null);
    Assert.expect(foregroundGrayLevelProfile.length == backgroundGrayLevelProfile.length);
    Assert.expect(thicknessTable != null);

    // Create a new array to hold the thickness profile.
    float[] thicknessProfile = new float[foregroundGrayLevelProfile.length];

    // Convert each gray level profile value to a thickness value.
    for (int i = 0; i < thicknessProfile.length; ++i)
    {
      float backgroundGrayValue = backgroundGrayLevelProfile[i];
      float foregroundGrayValue = foregroundGrayLevelProfile[i];
      float deltaGrayLevel = backgroundGrayValue - foregroundGrayValue;
      deltaGrayLevel = Math.max(deltaGrayLevel, 0);
      backgroundGrayValue = Math.min(backgroundGrayValue, 255);
      backgroundGrayValue = Math.max(backgroundGrayValue, 0);
      thicknessProfile[i] = thicknessTable.getThicknessInMillimeters(backgroundGrayValue, deltaGrayLevel, backgroundSensitivityOffset);
      // wei chin fixes (ensure there are not negative thickness.)
      thicknessProfile[i] = Math.max(thicknessProfile[i], 0.0f);
      //Siew Yeng - XCR-2591
      if(Float.isNaN(thicknessProfile[i]))
        thicknessProfile[i] = 0.0f;
    }

    return thicknessProfile;
  }

  /**
   * Converts the specified grey level profile to a delta gray profile.
   *
   * @author Matt Wharton
   */
  public static float[] convertThicknessProfileToGrayLevelProfile(float[] thicknessProfile, float backgroundGray, SolderThickness thicknessTable)
  {
    Assert.expect(thicknessProfile != null);
    Assert.expect((backgroundGray >= 0f) && (backgroundGray <= 255f));

    // Create a new array to hold the grey level profile.
    float[] grayLevelProfile = new float[thicknessProfile.length];

    for (int i = 0; i < grayLevelProfile.length; ++i)
    {
      grayLevelProfile[i] = thicknessTable.getDeltaGrayLevel(backgroundGray, thicknessProfile[i]);
    }

    return grayLevelProfile;
  }
  
  /**
   * Converts the getCorrectedGreyLevelForDebug
   *
   * @author Kee Chin Seong
   */
  public static float getCorrectedGreyLevelForDebug(Image image, float backgroundGreylevel, float foregroundGreylevel, SolderThickness thicknessTable, int backgroundSensitivityOffset)
  {
    Assert.expect(thicknessTable != null);
    
    boolean reverse = false;
    if (foregroundGreylevel > backgroundGreylevel)
    {
      reverse = true;
      float temp = backgroundGreylevel;
      backgroundGreylevel = foregroundGreylevel;
      foregroundGreylevel = temp;
    }
    
    backgroundGreylevel = Math.min(backgroundGreylevel, 255);
    backgroundGreylevel = Math.max(backgroundGreylevel, 0);
    float thickness = thicknessTable.getThicknessInMils(backgroundGreylevel, backgroundGreylevel-foregroundGreylevel, backgroundSensitivityOffset);
    
    if(thickness <= 0.0)
    {
        System.out.println("DEBUG MODE : AlgorithmUtil : CorrectedGreyLevel => " + thickness);
        try
        {
           ImageIoUtil.saveImage(image, "C:\\debugAlgorithmCrash.png");
        }
        catch (CouldNotCreateFileException ex) 
        {
           CouldNotCreateFileException dex = new CouldNotCreateFileException("C:\\debugAlgorithmCrash.png");
           dex.initCause(ex);
           dex.printStackTrace();
        }
    }
    
    float correctedGreylevel = 255.f - thicknessTable.getDeltaGrayLevel(255.f, thickness);

    if (reverse)
      correctedGreylevel = 255.f + (255.f-correctedGreylevel);

    return correctedGreylevel;

  }

  /**
   * Returns the 'corrected' gray level, which is defined to be the graylevel that would produce
   * the same thickness if the background were 255.  Thus a thickness of 0 will produce a corrected
   * greylevel of 255.
   *
   * Unlike most methods, this one allows the foreground to be brighter than the background.
   * In this case it may return values greater than 255.
   *
   * @author Patrick Lacz
   */
  public static float getCorrectedGreylevel(float backgroundGreylevel, float foregroundGreylevel, SolderThickness thicknessTable, int backgroundSensitivityOffset)
  {
    Assert.expect(thicknessTable != null);
    
    boolean reverse = false;
    if (foregroundGreylevel > backgroundGreylevel)
    {
      reverse = true;
      float temp = backgroundGreylevel;
      backgroundGreylevel = foregroundGreylevel;
      foregroundGreylevel = temp;
    }
    
    backgroundGreylevel = Math.min(backgroundGreylevel, 255);
    backgroundGreylevel = Math.max(backgroundGreylevel, 0);
    // Kok Chun, Tan - XCR-3340 - greylevel must between 0 to 255.
    foregroundGreylevel = Math.min(foregroundGreylevel, 255);
    foregroundGreylevel = Math.max(foregroundGreylevel, 0);
    float thickness = thicknessTable.getThicknessInMils(backgroundGreylevel, backgroundGreylevel-foregroundGreylevel, backgroundSensitivityOffset);
    float correctedGreylevel = 255.f - thicknessTable.getDeltaGrayLevel(255.f, thickness);

    if (reverse)
      correctedGreylevel = 255.f + (255.f-correctedGreylevel);

    return correctedGreylevel;
  }

  /**
   * Create an image containing the thickness for that pixel given a constant background level.
   * @author Patrick Lacz
   */
  public static Image createThicknessImage( Image foregroundImage,
                                            RegionOfInterest foregroundRoi,
                                            Image backgroundImage,
                                            RegionOfInterest backgroundRoi,
                                            SolderThickness thicknessTable)
  {
    Assert.expect(foregroundImage != null);
    Assert.expect(foregroundRoi != null);
    Assert.expect(foregroundRoi.fitsWithinImage(foregroundImage));
    Assert.expect(backgroundImage != null);
    Assert.expect(backgroundRoi != null);
    Assert.expect(backgroundRoi.fitsWithinImage(backgroundImage));
    Assert.expect(foregroundRoi.getWidth() == backgroundRoi.getWidth());
    Assert.expect(foregroundRoi.getHeight() == backgroundRoi.getHeight());
    Assert.expect(thicknessTable != null);

    Image deltaGrayImage = Arithmetic.subtractImages(backgroundImage, backgroundRoi, foregroundImage, foregroundRoi);
    Threshold.clamp(deltaGrayImage, 0.0f, 255.f);

    Image thicknessTableImage = thicknessTable.getThicknessTableAsImage();

    Image thicknessImage = new Image(foregroundRoi.getWidth(), foregroundRoi.getHeight());

    // Use deltaGray as the x coordinate and backgroundImage as the y coordinate for every pixel,
    // look up into thicknessTableImage the value (with interpolation)
    Transform.remapIntoImage(thicknessTableImage, RegionOfInterest.createRegionFromImage(thicknessTableImage),
        deltaGrayImage, RegionOfInterest.createRegionFromImage(deltaGrayImage), backgroundImage, backgroundRoi,
        thicknessImage, RegionOfInterest.createRegionFromImage(thicknessImage), 1);

    deltaGrayImage.decrementReferenceCount();

    return thicknessImage;
  }

  /**
   * @author Peter Esbensen
   */
  public static boolean regionCanFitInImage(RegionOfInterest roi, Image image)
  {
    Assert.expect(roi != null);
    Assert.expect(image != null);

    int roiWidth = roi.getWidth();
    int roiHeight = roi.getHeight();

    int imageWidth = image.getWidth();
    int imageHeight = image.getHeight();

    if ((roiWidth <= imageWidth) && (roiHeight <= imageHeight))
      return true;

    return false;
  }

  /**
   * Checks to see if the specified ROI fits within the specified image.  Posts a warning if not.
   *
   * @return true if the RegionOfInterest fits within the image; false otherwise.
   *
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public static boolean checkRegionBoundaries(RegionOfInterest roi, Image image, ReconstructionRegion inspectionRegion, String regionName)
  {
    Assert.expect(roi != null);
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(regionName != null);

    boolean regionFitsWithinImage = roi.fitsWithinImage(image);

    if (regionFitsWithinImage == false)
    {
      LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
          "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[] {inspectionRegion});
      raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);
    }

    return regionFitsWithinImage;
  }

  /**
   * Returns the number of mils per pixel at the specified MagnicationEnum.
   *
   * @author Matt Wharton
   */
  public static float getMilsPerPixel(MagnificationEnum magnificationEnum)
  {
    Assert.expect(magnificationEnum != null);

    // Only calculate the value if we haven't already calculated it before.
    Float milsPerPixel = _magnificationEnumToMilsPerPixelMap.get(magnificationEnum);
    if (milsPerPixel == null)
    {
      final float NANOS_PER_PIXEL = magnificationEnum.getNanoMetersPerPixel();
      milsPerPixel = (float)MathUtil.convertNanoMetersToMils(NANOS_PER_PIXEL);

      // Add the calculated value to the map.
      _magnificationEnumToMilsPerPixelMap.put(magnificationEnum, milsPerPixel);
    }

    return milsPerPixel;
  }

  /**
   * Returns the number of mils per pixel.  Assumes the nominal magnification level.
   *
   * @author Matt Wharton
   */
  public static float getMilsPerPixel()
  {
    return getMilsPerPixel(MagnificationEnum.getCurrentNorminal());
  }

  /**
   * Returns the number of mils per pixel.  Assumes the nominal magnification level.
   *
   * @author Matt Wharton
   */
  public static float getMilsPerPixel(Subtype subtype)
  {
    Assert.expect(subtype != null);
    int resizeFactor = 1;
    if (ImageProcessingAlgorithm.useResize(subtype))
    {
      resizeFactor = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
    }
    Assert.expect(resizeFactor > 0);
    return getMilsPerPixel(MagnificationEnum.getCurrentNorminal()) / resizeFactor;
  }  
  
  /**
   * Returns the number of millimeters per pixel.  Assumes the nominal magnification level.
   *
   * @author Peter Esbensen
   */
  public static float getMillimetersPerPixel()
  {
    return getMillimetersPerPixel(MagnificationEnum.getCurrentNorminal());
  }
  
  /**
   * Returns the number of millimeters per pixel.  Assumes the nominal magnification level.
   *
   * @author Wei Chin
   */
  public static float getMillimetersPerPixel(Subtype subtype)
  {
    Assert.expect(subtype != null);
    int resizeFactor = getResizeFactor(subtype);;
    Assert.expect(resizeFactor > 0);
    return getMillimetersPerPixel(MagnificationEnum.getCurrentNorminal()) / resizeFactor;
  }
  

  /**
   * Returns the number of millimeters per pixel at the specified MagnicationEnum.
   *
   * @author Matt Wharton
   */
  public static float getMillimetersPerPixel(MagnificationEnum magnificationEnum)
  {
    Assert.expect(magnificationEnum != null);

    // Only calculate the value if we haven't already calculated it before.
    Float millimetersPerPixel = _magnificationEnumToMillimetersPerPixelMap.get(magnificationEnum);
    if (millimetersPerPixel == null)
    {
      final float NANOS_PER_PIXEL = magnificationEnum.getNanoMetersPerPixel();
      millimetersPerPixel = (float)MathUtil.convertNanometersToMillimeters(NANOS_PER_PIXEL);

      // Add the calculated value to the map.
      _magnificationEnumToMillimetersPerPixelMap.put(magnificationEnum, millimetersPerPixel);
    }

    return millimetersPerPixel;
  }
  
/**
   * Returns the number of millimeters per pixel at the specified MagnicationEnum.
   *
   * @author Wei Chin
   */
  public static float getNanometersPerPixel(Subtype subtype)
  {
    Assert.expect(subtype != null);
    int resizeFactor = getResizeFactor(subtype);;
    Assert.expect(resizeFactor > 0);
    return MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel() / resizeFactor;
  }

  /**
   * @author Patrick Lacz
   */
  public static float measureCorrectedGraylevelFromPercentile(Image image,
      JointInspectionData jointInspectionData,
      RegionOfInterest regionToMeasureForeground,
      RegionOfInterest regionToMeasureBackground,
      float percentileForBackgroundSample,
      SliceNameEnum sliceNameEnum,
      ReconstructionRegion reconstructionRegion,
      Algorithm algorithm,
      boolean showDiagnostics) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(regionToMeasureForeground != null);
    Assert.expect(regionToMeasureBackground != null);
    Assert.expect(regionToMeasureForeground.fitsWithinImage(image));
    Assert.expect(regionToMeasureBackground.fitsWithinImage(image));
    Assert.expect(sliceNameEnum != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(algorithm != null);
    Assert.expect(percentileForBackgroundSample <= 1.0f && percentileForBackgroundSample >= 0.f);

    float averageGraylevelWithinHole = Statistics.mean(image, regionToMeasureForeground);

    // the normal use case of this method was to pass in the joint region for both the foreground and the background
    // and this method would figure out a certain amount to add to the region for the background and sample from that area.
    // that use case has changed somewhat, and perhaps too much is being done in this method now.

    RegionOfInterest outerRegion = new RegionOfInterest(regionToMeasureBackground);
    float interPadDistanceInNM = jointInspectionData.getPad().getAlgorithmLimitedInterPadDistanceInNanometers();
    float interPadDistanceInPixels = interPadDistanceInNM / getNanometersPerPixel(jointInspectionData.getSubtype());

    // we want to be sure not to be stepping over IPD, so make certain that the corners of the
    // region are at the IPD.
    int outerRegionWidth = (int)Math.floor(2 * 0.6 * interPadDistanceInPixels * Math.cos(Math.PI / 4));
    outerRegion.setWidthKeepingSameCenter(outerRegionWidth+regionToMeasureForeground.getWidth());
    outerRegion.setHeightKeepingSameCenter(outerRegionWidth+regionToMeasureForeground.getHeight());
    outerRegion.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    /** optimize PWL: Change this to sample ONLY from the region outside of the area passed in, rather than including that area in the
     * sample. The percentages passed in will need to be modified accordingly. */
    float lightPixelGraylevel = Statistics.getPercentile(image, RegionOfInterest.createRegionFromIntersection(outerRegion, image), percentileForBackgroundSample);

    if (showDiagnostics)
    {
      AlgorithmDiagnostics.getInstance().postDiagnostics(reconstructionRegion, sliceNameEnum, jointInspectionData, algorithm, false,
          new MeasurementRegionDiagnosticInfo(outerRegion, MeasurementRegionEnum.BACKGROUND_REGION),
          new MeasurementRegionDiagnosticInfo(regionToMeasureForeground, MeasurementRegionEnum.PAD_REGION));
    }
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    return getCorrectedGreyLevelForDebug(image, lightPixelGraylevel, (float)averageGraylevelWithinHole, thicknessTable, backroundSensitivityOffset);
  }

  /**
   * @author Patrick Lacz
   */
  public static RegionOfInterest getRegionOfInterestOfThroughholeBarrel(JointInspectionData joint, boolean ableToPerformResize)
  {
    Assert.expect(joint != null);
    RegionOfInterest barrelRegion = joint.getOrthogonalRegionOfInterestInPixels(ableToPerformResize);
    LandPatternPad landPatternPad = joint.getPad().getLandPatternPad();

    Assert.expect(landPatternPad.isThroughHolePad() == true);

    ThroughHoleLandPatternPad throughHolePad = (ThroughHoleLandPatternPad)landPatternPad;
    
    //Siew Yeng - XCR-3318 - Oval PTH
    // int holeDiameterInNanometers = throughHolePad.getHoleDiameterInNanoMeters();
    int holeWidthInNanometers = throughHolePad.getHoleWidthInNanoMeters();
    int holeLengthInNanometers = throughHolePad.getHoleLengthInNanoMeters();
    
    if(joint.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE) )
    {
      if(barrelRegion.getWidth() > barrelRegion.getHeight())
      {
        holeWidthInNanometers = Math.max(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters());
        holeLengthInNanometers = Math.min(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters());
      }
      else
      {
        holeWidthInNanometers = Math.min(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters());
        holeLengthInNanometers = Math.max(throughHolePad.getHoleWidthInNanoMeters(), throughHolePad.getHoleLengthInNanoMeters());
      }
    }

    // Compute the expected location of the hole.
    // This code is similar to ReconstructedImages.getRegionOfInterestForJoint
    ComponentCoordinate holeOffsetFromComponent = throughHolePad.getHoleCoordinateInNanoMeters();
    ComponentCoordinate padOffsetFromComponent = joint.getPad().getCoordinateInNanoMeters();

    IntCoordinate offsetFromPadRegionInNanometers = new IntCoordinate(
        holeOffsetFromComponent.getX() - padOffsetFromComponent.getX(),
        holeOffsetFromComponent.getY() - padOffsetFromComponent.getY());

    // Map the ROI from panel coordinates to inspection region relative image coordinates.
    final double pixelsPerNanometer = getNanoMeterPerPixelWithResizeConsideration(joint.getInspectionRegion().getTestSubProgram(), joint.getSubtype(), ableToPerformResize);
    
    offsetFromPadRegionInNanometers.scale(pixelsPerNanometer);

    barrelRegion.translateXY((int)(offsetFromPadRegionInNanometers.getX() * pixelsPerNanometer),
                             (int)(offsetFromPadRegionInNanometers.getY() * pixelsPerNanometer));
    // set up the hole region
    barrelRegion.setWidthKeepingSameCenter((int)(holeWidthInNanometers * pixelsPerNanometer));
    barrelRegion.setHeightKeepingSameCenter((int)(holeLengthInNanometers * pixelsPerNanometer));

    barrelRegion.setRegionShapeEnum(RegionShapeEnum.OBROUND);
    return barrelRegion;
  }


  /**
   * Raises an algorithm warning
   * @author Matt Wharton
   * @author Patrick Lacz
   * @author Sunit Bhalla
   */
  public static void raiseAlgorithmWarning(LocalizedString warningText)
  {
    Assert.expect(warningText != null);

    InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
    Assert.expect(_inspectionEventObservable != null);

    InspectionEvent inspectionEvent = new AlgorithmMessageInspectionEvent(warningText);
    _inspectionEventObservable.sendEventInfo(inspectionEvent);
 }

  /**
   * Raises an initial tuning warning for a given subtype
   *
   * @author Sunit Bhalla
   */
  public static void raiseInitialTuningLimitedDataWarning(Subtype subtype, int numberOfMeasurements)
  {
    Assert.expect(subtype != null);
    LocalizedString warningText = new LocalizedString("ALGDIAG_LEARNING_LIMITED_DATA_KEY",
                                                      new Object[] { subtype.getJointTypeEnum().getName(),
                                                                     subtype.getShortName(),
                                                                     subtype.getPads().size() });
    raiseAlgorithmWarning(warningText);
  }

  /**
   * Raises an warning that the subtype can't be learned becuase the selected image set(s) don't contain any
   * images of the subtype.
   *
   * @author Sunit Bhalla
   */
  public static void raiseNoImagesForInitialTuningWarning(Subtype subtype)
  {
    Assert.expect(subtype != null);
    LocalizedString warningText = new LocalizedString("ALGDIAG_NO_IMAGES_FOR_INITIAL_TUNING_KEY",
                                                      new Object[] { subtype.getShortName()});
    raiseAlgorithmWarning(warningText);
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  public static RegionOfInterest measureDiameter(Image image,
                                                 JointInspectionData jointInspectionData,
                                                 RegionOfInterest expectedJointRegion,
                                                 float expectedDiameterInPixels,
                                                 float expectedThicknessInMils,
                                                 Subtype subtype,
                                                 ReconstructionRegion reconstructionRegion,
                                                 SliceNameEnum sliceNameEnum,
                                                 BooleanRef foundJointInImage,
                                                 float profileSearchDistanceInPixels,
                                                 FloatRef measuredDiameterInMils,
                                                 boolean useMaxSlopeMethod,
                                                 float percentOfMax,
                                                 Algorithm algorithm) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(expectedDiameterInPixels >= 0.0);
    Assert.expect(expectedThicknessInMils >= 0.0);
    Assert.expect(jointInspectionData != null);
    Assert.expect(expectedJointRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(profileSearchDistanceInPixels >= expectedDiameterInPixels);
    Assert.expect(measuredDiameterInMils != null);
    Assert.expect(algorithm != null);

    int halfProfileSearchDistanceInPixels = (int)Math.round(profileSearchDistanceInPixels * 0.5);

    int numberOfPixelsProfileSearchExtendsBeyondLeftOfImage = -1 * Math.min(0,
                                                                            (expectedJointRegion.getCenterX() -
                                                                             halfProfileSearchDistanceInPixels));
    int numberOfPixelsProfileSearchExtendsBeyondRightOfImage = Math.max(0,
                                                                        ((expectedJointRegion.getCenterX() +
                                                                          halfProfileSearchDistanceInPixels) - (image.getWidth() - 1)));
    int pixelsToRemoveSoThatHorizontalProfileFitsOnScreen = 2 * Math.max(numberOfPixelsProfileSearchExtendsBeyondLeftOfImage,
                                                               numberOfPixelsProfileSearchExtendsBeyondRightOfImage);

    int horizontalProfileSearchDistanceInPixels = Math.max(0, Math.round(profileSearchDistanceInPixels) - pixelsToRemoveSoThatHorizontalProfileFitsOnScreen);

    int expectedDiameterInteger = Math.round((float)expectedDiameterInPixels);

    if ((horizontalProfileSearchDistanceInPixels == 0) || (expectedDiameterInteger == 0))
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    // LEFT-TO-RIGHT
    RegionOfInterest horizontalProfileRegion = new RegionOfInterest(expectedJointRegion);
    horizontalProfileRegion.setWidthKeepingSameCenter(Math.round((float)horizontalProfileSearchDistanceInPixels));
    horizontalProfileRegion.setHeightKeepingSameCenter(expectedDiameterInteger);
    horizontalProfileRegion.setOrientationInDegrees(0);

    RegionOfInterest forwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      horizontalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(forwardTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    // Note: We want to use greyLevel readings for "% of max" or "max slope", but we want to display a thickness profile
    // to the user.  (From Peter: greylevel comparisons work better than thickness.)
    float[] forwardCircleProfile = ImageFeatureExtraction.circleProfile(image, forwardTruncatedRegion);
    float[] forwardProfile = ProfileUtil.getSmoothedProfile(forwardCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(forwardProfile);


    // RIGHT-TO-LEFT
    horizontalProfileRegion.setOrientationInDegrees(180);
    RegionOfInterest reverseTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      horizontalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(reverseTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    float[] reverseCircleProfile = ImageFeatureExtraction.circleProfile(image, reverseTruncatedRegion);
    float[] reverseProfile = ProfileUtil.getSmoothedProfile(reverseCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(reverseProfile);

    int numberOfPixelsProfileSearchExtendsBeyondTopOfImage = -1 * Math.min(0,
        (expectedJointRegion.getCenterY() - halfProfileSearchDistanceInPixels));
    int numberOfPixelsProfileSearchExtendsBeyondBottomOfImage = Math.max(0,
        ((expectedJointRegion.getCenterY() + halfProfileSearchDistanceInPixels) - (image.getHeight() - 1)));
    int pixelsToRemoveSoThatVerticalProfileFitsOnScreen = 2 * Math.max(numberOfPixelsProfileSearchExtendsBeyondTopOfImage,
        numberOfPixelsProfileSearchExtendsBeyondBottomOfImage);

    int verticalProfileSearchDistanceInPixels = Math.max(0, Math.round(profileSearchDistanceInPixels) - pixelsToRemoveSoThatVerticalProfileFitsOnScreen);

    if ((verticalProfileSearchDistanceInPixels == 0) || (expectedDiameterInteger == 0))
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    // DOWNWARD
    RegionOfInterest verticalProfileRegion = new RegionOfInterest(expectedJointRegion);
    verticalProfileRegion.setWidthKeepingSameCenter(expectedDiameterInteger);
    verticalProfileRegion.setHeightKeepingSameCenter(Math.round((float)verticalProfileSearchDistanceInPixels));

    verticalProfileRegion.setOrientationInDegrees(90);
    RegionOfInterest downwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      verticalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(downwardTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    float[] downwardCircleProfile = ImageFeatureExtraction.circleProfile(image, downwardTruncatedRegion);
    float[] downwardProfile = ProfileUtil.getSmoothedProfile(downwardCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(downwardProfile);

    // UPWARDS
    verticalProfileRegion.setOrientationInDegrees(270);
    RegionOfInterest upwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      verticalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(upwardTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    float[] upwardCircleProfile = ImageFeatureExtraction.circleProfile(image, upwardTruncatedRegion);
    float[] upwardProfile = ProfileUtil.getSmoothedProfile(upwardCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(upwardProfile);

    if (useMaxSlopeMethod)
    {
      forwardProfile = ProfileUtil.createDerivativeProfile(forwardProfile, 2);
      reverseProfile = ProfileUtil.createDerivativeProfile(reverseProfile, 2);
      downwardProfile = ProfileUtil.createDerivativeProfile(downwardProfile, 2);
      upwardProfile = ProfileUtil.createDerivativeProfile(upwardProfile, 2);
    }

    RegionOfInterest measuredJointBoundary = findJointBoundaries(forwardTruncatedRegion,
                                                                 reverseTruncatedRegion,
                                                                 downwardTruncatedRegion,
                                                                 upwardTruncatedRegion,
                                                                 forwardProfile,
                                                                 reverseProfile,
                                                                 downwardProfile,
                                                                 upwardProfile,
                                                                 foundJointInImage,
                                                                 measuredDiameterInMils,
                                                                 subtype,
                                                                 useMaxSlopeMethod,
                                                                 percentOfMax);

    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), algorithm) && foundJointInImage.getValue())
    {
      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                forwardTruncatedRegion,
                                forwardCircleProfile,
                                measuredJointBoundary.getMinX() - forwardTruncatedRegion.getMinX(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_FORWARD_PROFILE_LABEL_KEY", null));

      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                reverseTruncatedRegion,
                                reverseCircleProfile,
                                reverseTruncatedRegion.getMaxX() - measuredJointBoundary.getMaxX(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_REVERSE_PROFILE_LABEL_KEY", null));

      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                downwardTruncatedRegion,
                                downwardCircleProfile,
                                measuredJointBoundary.getMinY() - downwardTruncatedRegion.getMinY(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_DOWNWARD_PROFILE_LABEL_KEY", null));

      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                upwardTruncatedRegion,
                                upwardCircleProfile,
                                upwardTruncatedRegion.getMaxY() - measuredJointBoundary.getMaxY(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_UPWARD_PROFILE_LABEL_KEY", null));
    }
    return measuredJointBoundary;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  public static RegionOfInterest measureDiameterWithJointBasedThreshold(Image image,
                                                 JointInspectionData jointInspectionData,
                                                 RegionOfInterest expectedJointRegion,
                                                 float expectedDiameterInPixels,
                                                 float expectedThicknessInMils,
                                                 Subtype subtype,
                                                 ReconstructionRegion reconstructionRegion,
                                                 SliceNameEnum sliceNameEnum,
                                                 BooleanRef foundJointInImage,
                                                 float profileSearchDistanceInPixels,
                                                 FloatRef measuredDiameterInMils,
                                                 boolean useMaxSlopeMethod,
                                                 float percentOfMax,
                                                 float[] edgeDetectionThresholds,
                                                 Algorithm algorithm) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(expectedDiameterInPixels >= 0.0);
    Assert.expect(expectedThicknessInMils >= 0.0);
    Assert.expect(jointInspectionData != null);
    Assert.expect(expectedJointRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(profileSearchDistanceInPixels >= expectedDiameterInPixels);
    Assert.expect(measuredDiameterInMils != null);
    Assert.expect(algorithm != null);

    int halfProfileSearchDistanceInPixels = (int)Math.round(profileSearchDistanceInPixels * 0.5);

    int numberOfPixelsProfileSearchExtendsBeyondLeftOfImage = -1 * Math.min(0,
                                                                            (expectedJointRegion.getCenterX() -
                                                                             halfProfileSearchDistanceInPixels));
    int numberOfPixelsProfileSearchExtendsBeyondRightOfImage = Math.max(0,
                                                                        ((expectedJointRegion.getCenterX() +
                                                                          halfProfileSearchDistanceInPixels) - (image.getWidth() - 1)));
    int pixelsToRemoveSoThatHorizontalProfileFitsOnScreen = 2 * Math.max(numberOfPixelsProfileSearchExtendsBeyondLeftOfImage,
                                                               numberOfPixelsProfileSearchExtendsBeyondRightOfImage);

    int horizontalProfileSearchDistanceInPixels = Math.max(0, Math.round(profileSearchDistanceInPixels) - pixelsToRemoveSoThatHorizontalProfileFitsOnScreen);

    int expectedDiameterInteger = Math.round((float)expectedDiameterInPixels);

    if ((horizontalProfileSearchDistanceInPixels == 0) || (expectedDiameterInteger == 0))
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    // LEFT-TO-RIGHT
    RegionOfInterest horizontalProfileRegion = new RegionOfInterest(expectedJointRegion);
    horizontalProfileRegion.setWidthKeepingSameCenter(Math.round((float)horizontalProfileSearchDistanceInPixels));
    horizontalProfileRegion.setHeightKeepingSameCenter(expectedDiameterInteger);
    horizontalProfileRegion.setOrientationInDegrees(0);

    RegionOfInterest forwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      horizontalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(forwardTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    // Note: We want to use greyLevel readings for "% of max" or "max slope", but we want to display a thickness profile
    // to the user.  (From Peter: greylevel comparisons work better than thickness.)
    float[] forwardCircleProfile = ImageFeatureExtraction.circleProfile(image, forwardTruncatedRegion);
    float[] forwardProfile = ProfileUtil.getSmoothedProfile(forwardCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(forwardProfile);


    // RIGHT-TO-LEFT
    horizontalProfileRegion.setOrientationInDegrees(180);
    RegionOfInterest reverseTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      horizontalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(reverseTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    float[] reverseCircleProfile = ImageFeatureExtraction.circleProfile(image, reverseTruncatedRegion);
    float[] reverseProfile = ProfileUtil.getSmoothedProfile(reverseCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(reverseProfile);

    int numberOfPixelsProfileSearchExtendsBeyondTopOfImage = -1 * Math.min(0,
        (expectedJointRegion.getCenterY() - halfProfileSearchDistanceInPixels));
    int numberOfPixelsProfileSearchExtendsBeyondBottomOfImage = Math.max(0,
        ((expectedJointRegion.getCenterY() + halfProfileSearchDistanceInPixels) - (image.getHeight() - 1)));
    int pixelsToRemoveSoThatVerticalProfileFitsOnScreen = 2 * Math.max(numberOfPixelsProfileSearchExtendsBeyondTopOfImage,
        numberOfPixelsProfileSearchExtendsBeyondBottomOfImage);

    int verticalProfileSearchDistanceInPixels = Math.max(0, Math.round(profileSearchDistanceInPixels) - pixelsToRemoveSoThatVerticalProfileFitsOnScreen);

    if ((verticalProfileSearchDistanceInPixels == 0) || (expectedDiameterInteger == 0))
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    // DOWNWARD
    RegionOfInterest verticalProfileRegion = new RegionOfInterest(expectedJointRegion);
    verticalProfileRegion.setWidthKeepingSameCenter(expectedDiameterInteger);
    verticalProfileRegion.setHeightKeepingSameCenter(Math.round((float)verticalProfileSearchDistanceInPixels));

    verticalProfileRegion.setOrientationInDegrees(90);
    RegionOfInterest downwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      verticalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(downwardTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    float[] downwardCircleProfile = ImageFeatureExtraction.circleProfile(image, downwardTruncatedRegion);
    float[] downwardProfile = ProfileUtil.getSmoothedProfile(downwardCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(downwardProfile);

    // UPWARDS
    verticalProfileRegion.setOrientationInDegrees(270);
    RegionOfInterest upwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(
      verticalProfileRegion, image);

    if (ImageFeatureExtraction.regionIsValidForCircleProfile(upwardTruncatedRegion) == false)
    {
      foundJointInImage.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
      return null;
    }

    float[] upwardCircleProfile = ImageFeatureExtraction.circleProfile(image, upwardTruncatedRegion);
    float[] upwardProfile = ProfileUtil.getSmoothedProfile(upwardCircleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(upwardProfile);

    if (useMaxSlopeMethod)
    {
      forwardProfile = ProfileUtil.createDerivativeProfile(forwardProfile, 2);
      reverseProfile = ProfileUtil.createDerivativeProfile(reverseProfile, 2);
      downwardProfile = ProfileUtil.createDerivativeProfile(downwardProfile, 2);
      upwardProfile = ProfileUtil.createDerivativeProfile(upwardProfile, 2);
    }
    
    RegionOfInterest measuredJointBoundary = null;
    
    measuredJointBoundary = findJointBoundariesUsingGrayLevel(forwardTruncatedRegion,
      reverseTruncatedRegion,
      downwardTruncatedRegion,
      upwardTruncatedRegion,
      forwardProfile,
      reverseProfile,
      downwardProfile,
      upwardProfile,
      foundJointInImage,
      measuredDiameterInMils,
      subtype,
      percentOfMax,
      edgeDetectionThresholds);
//    System.out.printf("RRID: %d  joint %s  slice: %s  percentOfMax: %f  ltor threshold %f  rtol threshold %f  ttob threshold %f  btot threshold %f  measured diameter: %f\n",
//      reconstructionRegion.getRegionId(),
//      jointInspectionData.getFullyQualifiedPadName(),
//      sliceNameEnum.getName(),
//      percentOfMax,
//      edgeDetectionThresholds[0],
//      edgeDetectionThresholds[1],
//      edgeDetectionThresholds[2],
//      edgeDetectionThresholds[3],
//      measuredDiameterInMils.getValue());
    // RJG
//    log_array(forwardCircleProfile, "left_to_right_raw", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[0]);
//    log_array(forwardProfile, "left_to_right_smoothed", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[0]);
//    log_array(forwardProfile, "left_to_right_smoothed_br", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[0]);
//
//    log_array(reverseCircleProfile, "right_to_left_raw", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[1]);
//    log_array(reverseProfile, "right_to_left_smoothed", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[1]);
//    log_array(reverseProfile, "right_to_left_smoothed_br", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[1]);
//
//    log_array(downwardCircleProfile, "top_to_bottom_raw", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[2]);
//    log_array(downwardProfile, "top_to_bottom_smoothed", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[2]);
//    log_array(downwardProfile, "top_to_bottom_smoothed_br", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[2]);
//
//    log_array(upwardCircleProfile, "bottom_to_top_raw", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[3]);
//    log_array(upwardProfile, "bottom_to_top_smoothed", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[3]);
//    log_array(upwardProfile, "bottom_to_top_smoothed_br", reconstructionRegion.getRegionId(), sliceNameEnum.getName(), jointInspectionData.getFullyQualifiedPadName(), edgeDetectionThresholds[3]);
 
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), algorithm) && foundJointInImage.getValue())
    {
      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                forwardTruncatedRegion,
                                forwardCircleProfile,
                                measuredJointBoundary.getMinX() - forwardTruncatedRegion.getMinX(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_FORWARD_PROFILE_LABEL_KEY", null));

      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                reverseTruncatedRegion,
                                reverseCircleProfile,
                                reverseTruncatedRegion.getMaxX() - measuredJointBoundary.getMaxX(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_REVERSE_PROFILE_LABEL_KEY", null));

      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                downwardTruncatedRegion,
                                downwardCircleProfile,
                                measuredJointBoundary.getMinY() - downwardTruncatedRegion.getMinY(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_DOWNWARD_PROFILE_LABEL_KEY", null));

      postDeltaGreylevelProfile(image,
                                jointInspectionData,
                                reconstructionRegion,
                                sliceNameEnum,
                                algorithm,
                                upwardTruncatedRegion,
                                upwardCircleProfile,
                                upwardTruncatedRegion.getMaxY() - measuredJointBoundary.getMaxY(),
                                expectedThicknessInMils,
                                new LocalizedString("ALGDIAG_GRIDARRAY_UPWARD_PROFILE_LABEL_KEY", null));
    }
    return measuredJointBoundary;
  }


  private static void log_array(float[] forwardProfile, String array_name, int rrid, String sliceName, String padName,
                                float threshold)
  {
    boolean disable_logging = false;
    if (disable_logging) return;

    String lead = String.format("%s: %d, %s, %s, %f,", array_name, rrid, padName, sliceName, threshold);
    StringBuilder string_builder = new StringBuilder(lead);
    for (float value : forwardProfile)
    {
      string_builder.append(String.format("%f,", value));
    }
    System.out.println(string_builder.toString());
  }


  /**
   * @author Patrick Lacz
   */
  private static void postThicknessProfileFromGraylevelProfile(Image image,
                                                               JointInspectionData jointInspectionData,
                                                               ReconstructionRegion reconstructionRegion,
                                                               SliceNameEnum sliceNameEnum,
                                                               Algorithm algorithm,
                                                               RegionOfInterest truncatedRegion,
                                                               float[] profile,
                                                               int edgeIndex,
                                                               float nominalThickness,
                                                               LocalizedString profileName,
                                                               int backgroundSensitivityOffset) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(truncatedRegion != null);
    Assert.expect(profile != null);
    Assert.expect(nominalThickness >= 0.0f);
    Assert.expect(profileName != null);

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    float[] forwardBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(profile);
    float[] forwardThicknessProfile = convertGrayLevelProfileToThicknessProfileInMils(profile, forwardBackgroundProfile, thicknessTable, backgroundSensitivityOffset);
    forwardThicknessProfile = ProfileUtil.getSmoothedProfile(forwardThicknessProfile, 3);

    MeasurementRegionEnum[] forwardMeasurementRegionEnums = new MeasurementRegionEnum[forwardThicknessProfile.length];
    Arrays.fill(forwardMeasurementRegionEnums, MeasurementRegionEnum.BACKGROUND_REGION);
    if (edgeIndex >= 0 && edgeIndex < forwardMeasurementRegionEnums.length)
      forwardMeasurementRegionEnums[edgeIndex] =  MeasurementRegionEnum.GRID_ARRAY_EDGE_REGION;

    postDiagnosticsForProfileWithRegion(image,
                                        reconstructionRegion,
                                        sliceNameEnum,
                                        jointInspectionData,
                                        profileName,
                                        truncatedRegion,
                                        forwardThicknessProfile,
                                        MeasurementUnitsEnum.MILS,
                                        forwardMeasurementRegionEnums,
                                        (float)Math.ceil(nominalThickness * 1.5f),
                                        algorithm);
  }

  /**
   * @author Sunit Bhalla
   *
   */
  private static void postDeltaGreylevelProfile(Image image,
                                                JointInspectionData jointInspectionData,
                                                ReconstructionRegion reconstructionRegion,
                                                SliceNameEnum sliceNameEnum,
                                                Algorithm algorithm,
                                                RegionOfInterest truncatedRegion,
                                                float[] circleProfile,
                                                int edgeIndex,
                                                float nominalThickness,
                                                LocalizedString profileName) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(truncatedRegion != null);
    Assert.expect(circleProfile != null);
    Assert.expect(profileName != null);
    Assert.expect(nominalThickness >= 0);

    float[] profile = ProfileUtil.getSmoothedProfile(circleProfile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(profile);
    float[] deltaGreyProfile = new float[circleProfile.length];
    for (int i = 0; i < circleProfile.length; i++)
      deltaGreyProfile[i] = 255.0f - profile[i];

    MeasurementRegionEnum[] forwardMeasurementRegionEnums = new MeasurementRegionEnum[circleProfile.length];
    Arrays.fill(forwardMeasurementRegionEnums, MeasurementRegionEnum.BACKGROUND_REGION);
    if (edgeIndex >= 0 && edgeIndex < forwardMeasurementRegionEnums.length)
      forwardMeasurementRegionEnums[edgeIndex] =  MeasurementRegionEnum.GRID_ARRAY_EDGE_REGION;

    // Compute maximum value
    float maxOfProfile = ArrayUtil.max(circleProfile);
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    float deltaGreyValueForNominalThickness = thicknessTable.getDeltaGrayLevel(maxOfProfile, nominalThickness);
    float maximumYAxisValueForChart = 1.5f * deltaGreyValueForNominalThickness;

    maximumYAxisValueForChart = MathUtil.roundToPlaces(maximumYAxisValueForChart, 0);

    postDiagnosticsForProfileWithRegion(image,
                                        reconstructionRegion,
                                        sliceNameEnum,
                                        jointInspectionData,
                                        profileName,
                                        truncatedRegion,
                                        deltaGreyProfile,
                                        MeasurementUnitsEnum.GRAYLEVEL,
                                        forwardMeasurementRegionEnums,
                                        maximumYAxisValueForChart,
                                        algorithm);
  }


  /**
   * @author Peter Esbensen
   */
  static private float[] getSmoothedProfileWithBackgroundTrendRemoved(Image image,
                                                                      Subtype subtype,
                                                                      RegionOfInterest profileRegion,
                                                                      boolean useMaxSlopeTechnique)
  {
    Assert.expect(image != null);
    Assert.expect(subtype != null);
    Assert.expect(profileRegion != null);

    float[] profile = ImageFeatureExtraction.circleProfile(image, profileRegion);

    float[] smoothedProfile = ProfileUtil.getSmoothedProfile(profile, 3);
    ProfileUtil.removeBackgroundTrendFromProfile(smoothedProfile);

    if (useMaxSlopeTechnique)
    {
      return ProfileUtil.createDerivativeProfile(smoothedProfile, 2);
    }
    return smoothedProfile;
  }


  /**
   * @author Sunit Bhalla
   */
  static private float[] getSmoothedThicknessProfileWithBackgroundTrendRemoved(Image image,
                                                                               Subtype subtype,
                                                                               RegionOfInterest profileRegion) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(subtype != null);
    Assert.expect(profileRegion != null);

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityThreshold = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float[] profile = ImageFeatureExtraction.circleProfile(image, profileRegion);
    float[] backgroundProfile = ProfileUtil.getBackgroundTrendProfile(profile);
    float[] thicknessProfile = convertGrayLevelProfileToThicknessProfileInMils(profile, backgroundProfile, thicknessTable, backgroundSensitivityThreshold);

    float[] smoothedThicknessProfile = ProfileUtil.getSmoothedProfile(thicknessProfile, 3);

    return smoothedThicknessProfile;
  }

  /**
   * @author Peter Esbensen
   */
  static public void postDiagnosticsForProfileWithRegion(Image image,
                                                         ReconstructionRegion reconstructionRegion,
                                                         SliceNameEnum sliceNameEnum,
                                                         JointInspectionData jointInspectionData,
                                                         LocalizedString profileName,
                                                         RegionOfInterest measurementRegion,
                                                         float[] profile,
                                                         MeasurementUnitsEnum profileUnits,
                                                         Algorithm algorithm)
  {
    Assert.expect(profileName != null);
    Assert.expect(profile != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(profileUnits != null);
    Assert.expect(algorithm != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getSubtype().getJointTypeEnum(), algorithm))
    {
      ProfileDiagnosticInfo profileDiagnosticInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getComponentAndPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           profileName,
                                                                           ArrayUtil.min(profile),
                                                                           ArrayUtil.max(profile),
                                                                           profile,
                                                                           MeasurementRegionEnum.PAD_REGION,
                                                                           profileUnits);

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   algorithm,
                                   false,
                                   new MeasurementRegionDiagnosticInfo(measurementRegion, MeasurementRegionEnum.PAD_REGION),
                                   profileDiagnosticInfo);
    }
  }

  /**
   * @author Sunit Bhalla
   * This allows MeasurementRegionEnums to be used so edges can be drawn where the edge of the ball is found.
   */
  static public void postDiagnosticsForProfileWithRegion(Image image,
                                                         ReconstructionRegion reconstructionRegion,
                                                         SliceNameEnum sliceNameEnum,
                                                         JointInspectionData jointInspectionData,
                                                         LocalizedString profileName,
                                                         RegionOfInterest measurementRegion,
                                                         float[] profile,
                                                         MeasurementUnitsEnum profileUnits,
                                                         MeasurementRegionEnum[] measurementRegionEnums,
                                                         float maximumValueForYAxis,
                                                         Algorithm algorithm)
  {
    Assert.expect(profileName != null);
    Assert.expect(profile != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(algorithm != null);
    Assert.expect(profileUnits != null);
    Assert.expect(maximumValueForYAxis >= 0.0);
    Assert.expect(measurementRegionEnums != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getSubtype().getJointTypeEnum(), algorithm))
    {
      ProfileDiagnosticInfo profileDiagnosticInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           profileName,
                                                                           0.0f,
                                                                           maximumValueForYAxis,
                                                                           profile,
                                                                           measurementRegionEnums,
                                                                           profileUnits);

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   algorithm,
                                   false,
                                   new MeasurementRegionDiagnosticInfo(measurementRegion,
                                                                       MeasurementRegionEnum.GRID_ARRAY_EDGE_SEARCH_REGION),
                                   profileDiagnosticInfo);
    }
  }


  /**
   * @author Patrick Lacz
   */
  public static void postStartOfSliceDiagnostics(ReconstructedImages reconstructedImages,
                                                 SliceNameEnum sliceNameEnum,
                                                 Subtype subtype,
                                                 Algorithm algorithm)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(algorithm != null);

    postStartOfSliceDiagnostics(reconstructedImages,
                                sliceNameEnum,
                                subtype,
                                algorithm,
                                new LinkedList<DiagnosticInfo>());
  }


  /**
 * @author Patrick Lacz
 */
public static void postStartOfSliceDiagnostics(ReconstructedImages reconstructedImages,
                                               SliceNameEnum sliceNameEnum,
                                               Subtype subtype,
                                               Algorithm algorithm,
                                               List<DiagnosticInfo> diagnosticsToDisplay)
{
  Assert.expect(reconstructedImages != null);
  Assert.expect(sliceNameEnum != null);
  Assert.expect(subtype != null);
  Assert.expect(algorithm != null);
  Assert.expect(diagnosticsToDisplay != null);

  if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), algorithm))
  {
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    diagnosticsToDisplay.add(0, new InspectionImageDiagnosticInfo(reconstructedImages, sliceNameEnum, algorithm));

//    if (Config.isDeveloperDebugModeOn())
//    {
//      String zHeightString = MathUtil.convertNanoMetersToMils(slice.getHeightInNanometers()) + " mils";
//      if (Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric())
//        zHeightString = MathUtil.convertNanometersToMillimeters(slice.getHeightInNanometers()) + " mm";
//      diagnosticsToDisplay.add(1, new TextualDiagnosticInfo("Z Height of " + slice.getSliceNameEnum().getName() + ": " + zHeightString + "\n"));
//    }

    AlgorithmDiagnostics.getInstance().postDiagnostics(reconstructionRegion, slice.getSliceNameEnum(), subtype, algorithm, true, false,
                                                       diagnosticsToDisplay.toArray(new DiagnosticInfo[diagnosticsToDisplay.size()]));
  }
}


  /**
   * @author Peter Esbensen
   */
  static public void postDiagnosticsForProfile(Image image,
                                               ReconstructionRegion reconstructionRegion,
                                               SliceNameEnum sliceNameEnum,
                                               JointInspectionData jointInspectionData,
                                               LocalizedString profileName,
                                               float[] profile,
                                               MeasurementUnitsEnum profileUnits,
                                               Algorithm algorithm,
                                               float suggestedMinValueForVerticalScale,
                                               float suggestedMaxValueForVerticalScale)
  {
    Assert.expect(profileName != null);
    Assert.expect(profile != null);
    Assert.expect(profileUnits != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(algorithm != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getSubtype().getJointTypeEnum(), algorithm))
    {
      ProfileDiagnosticInfo profileDiagnosticInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getComponentAndPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           profileName,
                                                                           suggestedMinValueForVerticalScale,
                                                                           suggestedMaxValueForVerticalScale,
                                                                           profile,
                                                                           MeasurementRegionEnum.PAD_REGION,
                                                                           profileUnits);

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   algorithm,
                                   false,
                                   profileDiagnosticInfo);
    }
  }

  /**
   * Notify the user that inspection of a new joint and a new slice has begun.
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static void postStartOfJointDiagnostics(Algorithm algorithm,
                                                 JointInspectionData jointInspectionData,
                                                 ReconstructionRegion reconstructionRegion,
                                                 ReconstructedSlice reconstructedSlice,
                                                 boolean invalidatePreviousGraphics)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(reconstructedSlice != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), algorithm))
    {
      _diagnostics.postDiagnostics(reconstructionRegion,
                                   reconstructedSlice.getSliceNameEnum(),
                                   jointInspectionData,
                                   algorithm,
                                   invalidatePreviousGraphics,
                                   new TextualDiagnosticInfo("ALGDIAG_BEGIN_NEW_JOINT_SLICE_DIAGNOSTIC_KEY",
                                                             new Object[]
                                                             {algorithm.getName(),
                                                             reconstructedSlice.getSliceNameEnum().getName(),
                                                             jointInspectionData.getComponent().getReferenceDesignator(),
                                                             jointInspectionData.getPad().getName()}));
    }
  }

  /**
   * Notify the user that inspection of a new joint which need all slices has begun.
   * 
   * @author Lim, Lay Ngor PIPA
   */
  public static void postStartOfJointDiagnostics(Algorithm algorithm,
                                                 JointInspectionData jointInspectionData,
                                                 ReconstructionRegion reconstructionRegion,
                                                 boolean invalidatePreviousGraphics)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), algorithm))
    {
      _diagnostics.postDiagnostics(reconstructionRegion,
                                   null,
                                   jointInspectionData,
                                   algorithm,
                                   invalidatePreviousGraphics,
                                   new TextualDiagnosticInfo("ALGDIAG_BEGIN_NEW_JOINT_SLICE_DIAGNOSTIC_KEY",
                                                             new Object[]
                                                             {algorithm.getName(),
                                                             "All",
                                                             jointInspectionData.getComponent().getReferenceDesignator(),
                                                             jointInspectionData.getPad().getName()}));
    }
  }

  /**
   * Display textual information to the test developer showing that a joint has passed a threshold comparison.
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static void postPassingJointTextualDiagnostic(Algorithm algorithm,
                                                       JointInspectionData jointInspectionData,
                                                       SliceNameEnum sliceNameEnum,
                                                       ReconstructionRegion reconstructionRegion,
                                                       JointMeasurement jointMeasurement,
                                                       float thresholdValue)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), algorithm))
    {
      Pair<JointMeasurement, Float> jointMeasurementAndThresholdValueInProperUnits =
          convertMeasurementAndThresholdValueToDesiredUnits(jointMeasurement, thresholdValue);
      JointMeasurement jointMeasurementInProperUnits = jointMeasurementAndThresholdValueInProperUnits.getFirst();
      float thresholdValueInProperUnits = jointMeasurementAndThresholdValueInProperUnits.getSecond();

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   algorithm,
                                   false,
                                   new TextualDiagnosticInfo("ALGDIAG_JOINT_PASSED_DIAGNOSTIC_KEY",
                                                             new Object[]
                                                             {sliceNameEnum.getName(),
                                                             jointInspectionData.getPad().getName(),
                                                             jointMeasurementInProperUnits.getMeasurementName(),
                                                             jointMeasurementInProperUnits.getValue(),
                                                             thresholdValueInProperUnits}));
    }
  }
  /**
   * Display textual information to the test developer showing that a component has passed a threshold comparison.
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static void postPassingComponentTextualDiagnostic(Algorithm algorithm,
                                                           ComponentInspectionData componentInspectionData,
                                                           SliceNameEnum sliceNameEnum,
                                                           ReconstructionRegion reconstructionRegion,
                                                           ComponentMeasurement componentMeasurement,
                                                           float thresholdValue)
  {
    Assert.expect(algorithm != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    if (ImageAnalysis.areDiagnosticsEnabled(componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum(),
                                            algorithm))
    {
      Pair<ComponentMeasurement, Float> componentMeasurementAndThresholdValueInProperUnits =
          convertMeasurementAndThresholdValueToDesiredUnits(componentMeasurement, thresholdValue);
      ComponentMeasurement componentMeasurementInProperUnits = componentMeasurementAndThresholdValueInProperUnits.getFirst();
      float thresholdValueInProperUnits = componentMeasurementAndThresholdValueInProperUnits.getSecond();

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   componentInspectionData.getPadOneJointInspectionData(),
                                   algorithm,
                                   false,
                                   new TextualDiagnosticInfo("ALGDIAG_COMPONENT_PASSED_DIAGNOSTIC_KEY",
                                                             new Object[]
                                                             {sliceNameEnum.getName(),
                                                             componentInspectionData.getComponent().getReferenceDesignator(),
                                                             componentMeasurementInProperUnits.getMeasurementName(),
                                                             componentMeasurementInProperUnits.getValue(),
                                                             thresholdValueInProperUnits}));
    }
  }

  /**
   * Display textual information to the test developer showing data pertaining to a indictment.
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static void postFailingComponentTextualDiagnostic(Algorithm algorithm,
                                                           ComponentInspectionData componentInspectionData,
                                                           ReconstructionRegion reconstructionRegion,
                                                           SliceNameEnum sliceNameEnum,
                                                           ComponentMeasurement componentMeasurement,
                                                           float thresholdValue)
  {
    Assert.expect(algorithm != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    if (ImageAnalysis.areDiagnosticsEnabled(componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum(),
                                            algorithm))
    {
      Pair<ComponentMeasurement, Float> componentMeasurementAndThresholdValueInProperUnits =
          convertMeasurementAndThresholdValueToDesiredUnits(componentMeasurement, thresholdValue);
      ComponentMeasurement componentMeasurementInProperUnits = componentMeasurementAndThresholdValueInProperUnits.getFirst();
      float thresholdValueInProperUnits = componentMeasurementAndThresholdValueInProperUnits.getSecond();

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   componentInspectionData.getPadOneJointInspectionData(),
                                   algorithm,
                                   false,
                                   new TextualDiagnosticInfo("ALGDIAG_COMPONENT_FAILED_DIAGNOSTIC_KEY",
                                                             new Object[]
                                                             {sliceNameEnum.getName(),
                                                             componentInspectionData.getComponent().getReferenceDesignator(),
                                                             componentMeasurementInProperUnits.getMeasurementName(),
                                                             componentMeasurementInProperUnits.getValue(),
                                                             thresholdValueInProperUnits}));
    }
  }

  /**
   * Display a red or green region around the component to indicate if component
   * has passed or failed.
   *
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public static void postComponentPassingOrFailingRegionDiagnostic(Algorithm algorithm,
                                                                   ComponentInspectionData componentInspectionData,
                                                                   ReconstructionRegion reconstructionRegion,
                                                                   SliceNameEnum sliceNameEnum,
                                                                   boolean jointPassed)
  {
    Assert.expect(algorithm != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    if (ImageAnalysis.areDiagnosticsEnabled(componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum(),
                                            algorithm))
    {
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = null;
      boolean ableToPerformResize = isAlgorithmAbleToPerformResize(algorithm);
      
      //Lim, Lay Ngor add sliceNameEnum so that can use camera 0 slice instead of barrel only! - Broken Pin
      RegionOfInterest locatedRoi = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData, sliceNameEnum, ableToPerformResize);
      if (jointPassed == true)
      {
        componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(locatedRoi,
                                                                            MeasurementRegionEnum.PASSING_JOINT_REGION);
      }
      else
      {
        componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(locatedRoi,
                                                                            MeasurementRegionEnum.FAILING_JOINT_REGION);
      }
      componentRegionDiagnosticInfo.setLabel(componentInspectionData.getComponent().getReferenceDesignator());

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   componentInspectionData.getPadOneJointInspectionData(),
                                   algorithm,
                                   false,
                                   componentRegionDiagnosticInfo);
    }
  }


  /**
   * Display a red or green region around the joint to indicate if the region has been indicted.
   *
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static void postJointPassingOrFailingRegionDiagnostic(Algorithm algorithm,
                                                               JointInspectionData jointInspectionData,
                                                               ReconstructionRegion reconstructionRegion,
                                                               SliceNameEnum sliceNameEnum,
                                                               boolean jointPassed)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), algorithm))
    {
      MeasurementRegionDiagnosticInfo jointRegionDiagnosticInfo = null;
      boolean ableToPerformResize = isAlgorithmAbleToPerformResize(algorithm);
      
      RegionOfInterest locatedRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData, sliceNameEnum, ableToPerformResize);
      if (jointPassed == true)
      {
        jointRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(locatedRoi, MeasurementRegionEnum.PASSING_JOINT_REGION);
      }
      else
      {
        jointRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(locatedRoi, MeasurementRegionEnum.FAILING_JOINT_REGION);
      }
      jointRegionDiagnosticInfo.setLabel(jointInspectionData.getPad().getName());

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   algorithm,
                                   false,
                                   jointRegionDiagnosticInfo);
    }
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public static void postFailingJointTextualDiagnostic(Algorithm algorithm,
                                                       JointInspectionData jointInspectionData,
                                                       ReconstructionRegion reconstructionRegion,
                                                       SliceNameEnum sliceNameEnum,
                                                       JointMeasurement jointMeasurement,
                                                       float thresholdValue)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), algorithm))
    {
      Pair<JointMeasurement, Float> jointMeasurementAndThresholdValueInProperUnits =
          convertMeasurementAndThresholdValueToDesiredUnits(jointMeasurement, thresholdValue);
      JointMeasurement jointMeasurementInProperUnits = jointMeasurementAndThresholdValueInProperUnits.getFirst();
      float thresholdValueInProperUnits = jointMeasurementAndThresholdValueInProperUnits.getSecond();

      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   algorithm,
                                   false,
                                   new TextualDiagnosticInfo("ALGDIAG_JOINT_FAILED_DIAGNOSTIC_KEY",
                                                             new Object[]
                                                             {sliceNameEnum.getName(),
                                                             jointInspectionData.getPad().getName(),
                                                             jointMeasurementInProperUnits.getMeasurementName(),
                                                             jointMeasurementInProperUnits.getValue(),
                                                             thresholdValueInProperUnits}));
    }
  }

  /**
   * Use this method to report the result of a joint measurement to the gui.
   * @author Patrick Lacz
   */
  public static void postMeasurementTextualDiagnostics(Algorithm algorithm,
                                                       ReconstructionRegion reconstructionRegion,
                                                       JointInspectionData jointInspectionData,
                                                       JointMeasurement ...measurementArray)
  {
    Assert.expect(algorithm != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(measurementArray != null);

    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), algorithm))
    {
      DiagnosticInfo diagnosticInfoArray[] = new DiagnosticInfo[measurementArray.length];
      SliceNameEnum sliceNameEnum = null;

      for (int i = 0; i < measurementArray.length; ++i)
      {
        JointMeasurement measurement = measurementArray[i];

        measurement = convertMeasurementToDesiredUnits(measurement);

        diagnosticInfoArray[i] = new TextualDiagnosticInfo("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",
                                                           new Object[]
                                                           {
                                                           measurement.getMeasurementName(),
                                                           measurement.getValue(),
                                                           measurement.getMeasurementUnitsEnum().getName()});
        sliceNameEnum = measurement.getSliceNameEnum();
      }

      _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, jointInspectionData, algorithm, false, diagnosticInfoArray);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static void postTextualDiagnostics(LocalizedString localizedString,
                                            ReconstructionRegion reconstructionRegion,
                                            SliceNameEnum sliceNameEnum,
                                            JointInspectionData jointInspectionData,
                                            Algorithm algorithm)
  {
    Assert.expect(localizedString != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(algorithm != null);

    DiagnosticInfo diagnosticInfoArray[] = new DiagnosticInfo[1];
    diagnosticInfoArray[0] = new TextualDiagnosticInfo(localizedString);
    _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, jointInspectionData, algorithm, false, diagnosticInfoArray);
  }

  /**
   * Use this method to report the result of a component measurement to the gui.
   * @author Patrick Lacz
   */
  public static void postMeasurementTextualDiagnostics(Algorithm algorithm,
                                                       ReconstructionRegion reconstructionRegion,
                                                       ComponentInspectionData componentInspectionData,
                                                       ComponentMeasurement ...measurementArray)

  {
    Assert.expect(algorithm != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(measurementArray != null);

    if (ImageAnalysis.areDiagnosticsEnabled(componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum(),
                                            algorithm))
    {
      DiagnosticInfo diagnosticInfoArray[] = new DiagnosticInfo[measurementArray.length];
      SliceNameEnum sliceNameEnum = null;

      boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();

      for (int i = 0; i < measurementArray.length; ++i)
      {
        ComponentMeasurement measurement = measurementArray[i];

        if (usingMetricUnits == false && measurement.getMeasurementUnitsEnum().equals(MeasurementUnitsEnum.MILLIMETERS))
        {
          // convert to Mils
          ComponentMeasurement measurementInMils = new ComponentMeasurement(measurement.getAlgorithm(),
                                                                            measurement.getSubtype(),
                                                                            measurement.getMeasurementEnum(),
                                                                            MeasurementUnitsEnum.MILS,
                                                                            measurement.getComponent(),
                                                                            measurement.getSliceNameEnum(),
                                                                            MathUtil.convertMillimetersToMils(measurement.getValue()),
                                                                            measurement.isMeasurementValid());
          measurement = measurementInMils;
        }

        diagnosticInfoArray[i] = new TextualDiagnosticInfo("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",
                                                           new Object[]
                                                           {
                                                           measurement.getMeasurementName(),
                                                           measurement.getValue(),
                                                           measurement.getMeasurementUnitsEnum().getName()
        });
        sliceNameEnum = measurement.getSliceNameEnum();
      }

      Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();

      _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, subtype, algorithm, false, false, diagnosticInfoArray);
    }
  }

  /**
   * @author Patrick Lacz
   * @author Peter Esbensen
   */
  private static ComponentMeasurement convertMeasurementToDesiredUnits(ComponentMeasurement componentMeasurement)
  {
    Assert.expect(componentMeasurement != null);

    boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();

    if (usingMetricUnits == false && componentMeasurement.getMeasurementUnitsEnum().equals(MeasurementUnitsEnum.MILLIMETERS))
    {
      // convert to Mils
      ComponentMeasurement measurementInMils = new ComponentMeasurement(componentMeasurement.getAlgorithm(),
                                                                        componentMeasurement.getSubtype(),
                                                                        componentMeasurement.getMeasurementEnum(),
                                                                        MeasurementUnitsEnum.MILS,
                                                                        componentMeasurement.getComponent(),
                                                                        componentMeasurement.getSliceNameEnum(),
                                                                        MathUtil.convertMillimetersToMils(componentMeasurement.getValue()),
                                                                        componentMeasurement.isMeasurementValid());
      return measurementInMils;
    }
    else
      return componentMeasurement;
  }

  /**
   * @author Matt Wharton
   */
  private static Pair<ComponentMeasurement, Float> convertMeasurementAndThresholdValueToDesiredUnits(ComponentMeasurement componentMeasurement,
                                                                                                     float thresholdValue)
  {
    Assert.expect(componentMeasurement != null);

    boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();
    if ((usingMetricUnits == false) && componentMeasurement.getMeasurementUnitsEnum().equals(MeasurementUnitsEnum.MILLIMETERS))
    {
      // convert to Mils
      ComponentMeasurement measurementInMils = new ComponentMeasurement(componentMeasurement.getAlgorithm(),
                                                                        componentMeasurement.getSubtype(),
                                                                        componentMeasurement.getMeasurementEnum(),
                                                                        MeasurementUnitsEnum.MILS,
                                                                        componentMeasurement.getComponent(),
                                                                        componentMeasurement.getSliceNameEnum(),
                                                                        MathUtil.convertMillimetersToMils(componentMeasurement.getValue()),
                                                                        componentMeasurement.isMeasurementValid());
      float thresholdValueInMils = MathUtil.convertMillimetersToMils(thresholdValue);

      return new Pair<ComponentMeasurement, Float>(measurementInMils, thresholdValueInMils);
    }
    else
    {
      return new Pair<ComponentMeasurement, Float>(componentMeasurement, thresholdValue);
    }
  }

  /**
   * @author Patrick Lacz
   * @author Peter Esbensen
   */
  private static JointMeasurement convertMeasurementToDesiredUnits(JointMeasurement jointMeasurement)
  {
    Assert.expect(jointMeasurement != null);

    boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();

    if (usingMetricUnits == false && jointMeasurement.getMeasurementUnitsEnum().equals(MeasurementUnitsEnum.MILLIMETERS))
    {
      // convert to Mils
      JointMeasurement measurementInMils = new JointMeasurement(jointMeasurement.getAlgorithm(),
                                                                jointMeasurement.getMeasurementEnum(),
                                                                MeasurementUnitsEnum.MILS,
                                                                jointMeasurement.getPad(),
                                                                jointMeasurement.getSliceNameEnum(),
                                                                MathUtil.convertMillimetersToMils(jointMeasurement.getValue()),
                                                                jointMeasurement.isMeasurementValid());
      return measurementInMils;
    }
    else
      return jointMeasurement;
  }

  /**
   * @author Matt Wharton
   */
  private static Pair<JointMeasurement, Float> convertMeasurementAndThresholdValueToDesiredUnits(JointMeasurement jointMeasurement,
                                                                                                 float thresholdValue)
  {
    Assert.expect(jointMeasurement != null);

    boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();
    if ((usingMetricUnits == false) && jointMeasurement.getMeasurementUnitsEnum().equals(MeasurementUnitsEnum.MILLIMETERS))
    {
      // Convert the JointMeasurement and threshold value to mils.
      JointMeasurement measurementInMils = new JointMeasurement(jointMeasurement.getAlgorithm(),
                                                                jointMeasurement.getMeasurementEnum(),
                                                                MeasurementUnitsEnum.MILS,
                                                                jointMeasurement.getPad(),
                                                                jointMeasurement.getSliceNameEnum(),
                                                                MathUtil.convertMillimetersToMils(jointMeasurement.getValue()),
                                                                jointMeasurement.isMeasurementValid());
      float thresholdValueInMils = MathUtil.convertMillimetersToMils(thresholdValue);

      return new Pair<JointMeasurement, Float>(measurementInMils, thresholdValueInMils);
    }
    else
    {
      return new Pair<JointMeasurement, Float>(jointMeasurement, thresholdValue);
    }
  }

  /**
   * @author Rick Gaudette
   */
  private static RegionOfInterest
  findJointBoundariesUsingGrayLevel(RegionOfInterest forwardTruncatedProfileRegion,
                                    RegionOfInterest reverseTruncatedProfileRegion,
                                    RegionOfInterest downwardsTruncatedProfileRegion,
                                    RegionOfInterest upwardsTruncatedProfileRegion,
                                    float[] forwardProfile,
                                    float[] reverseProfile,
                                    float[] downwardProfile,
                                    float[] upwardProfile,
                                    BooleanRef foundJoint,
                                    FloatRef measuredDiameterInMils,
                                    Subtype subtype,
                                    float percentOfMax,
                                    float[] edgeDetectionThresholds)
  {
    Assert.expect(forwardProfile != null);
    Assert.expect(reverseProfile != null);
    Assert.expect(downwardProfile != null);
    Assert.expect(upwardProfile != null);
    Assert.expect(foundJoint != null);
    Assert.expect(forwardTruncatedProfileRegion != null);
    Assert.expect(downwardsTruncatedProfileRegion != null);
    Assert.expect(reverseTruncatedProfileRegion != null);
    Assert.expect(upwardsTruncatedProfileRegion != null);
    Assert.expect(forwardProfile.length > 0);
    Assert.expect(reverseProfile.length > 0);
    Assert.expect(downwardProfile.length > 0);
    Assert.expect(upwardProfile.length > 0);
    Assert.expect(subtype != null);
    Assert.expect(measuredDiameterInMils != null);


    float forwardEdgeInXPixelCoordinates = 0;
    float reverseEdgeInXPixelCoordinates = 0;
    float downwardEdgeInYPixelCoordinates = 0;
    float upwardEdgeInYPixelCoordinates = 0;

    // If any of the edge detection thresholds are less zero (which is a sentinel value for not set), compute them
    // the current profile.  This should happen for the midball slice only.
    if (edgeDetectionThresholds[0] < 0.0F)
    {
      edgeDetectionThresholds[0] = ProfileUtil.findMinMaxRelativeGrayLevel(forwardProfile, percentOfMax);
    }
    if (edgeDetectionThresholds[1] < 0.0F)
    {
      edgeDetectionThresholds[1] = ProfileUtil.findMinMaxRelativeGrayLevel(reverseProfile, percentOfMax);
    }
    if (edgeDetectionThresholds[2] < 0.0F)
    {
      edgeDetectionThresholds[2] = ProfileUtil.findMinMaxRelativeGrayLevel(downwardProfile, percentOfMax);
    }
    if (edgeDetectionThresholds[3] < 0.0F)
    {
      edgeDetectionThresholds[3] = ProfileUtil.findMinMaxRelativeGrayLevel(upwardProfile, percentOfMax);
    }

    forwardEdgeInXPixelCoordinates = forwardTruncatedProfileRegion.getMinX() +
        ProfileUtil.findSubpixelEdgeLocationBasedOnGrayLevel(forwardProfile, edgeDetectionThresholds[0]);
    reverseEdgeInXPixelCoordinates = reverseTruncatedProfileRegion.getMaxX() -
        ProfileUtil.findSubpixelEdgeLocationBasedOnGrayLevel(reverseProfile, edgeDetectionThresholds[1]);
    downwardEdgeInYPixelCoordinates = downwardsTruncatedProfileRegion.getMinY() +
        ProfileUtil.findSubpixelEdgeLocationBasedOnGrayLevel(downwardProfile, edgeDetectionThresholds[2]);
    upwardEdgeInYPixelCoordinates = upwardsTruncatedProfileRegion.getMaxY() -
        ProfileUtil.findSubpixelEdgeLocationBasedOnGrayLevel(upwardProfile, edgeDetectionThresholds[3]);

    RegionOfInterest jointBoundaryRegion = null;

    int roundedForwardEdgeInXPixelCoordinates = Math.round(forwardEdgeInXPixelCoordinates);
    int roundedDownwardEdgeInYPixelCoordinates = Math.round(downwardEdgeInYPixelCoordinates);
    int roundedReverseEdgeInXPixelCoordinates = Math.round(reverseEdgeInXPixelCoordinates);
    int roundedUpwardEdgeInYPixelCoordinates = Math.round(upwardEdgeInYPixelCoordinates);

    if ((roundedForwardEdgeInXPixelCoordinates < roundedReverseEdgeInXPixelCoordinates) &&
        (roundedDownwardEdgeInYPixelCoordinates < roundedUpwardEdgeInYPixelCoordinates))
    {
      foundJoint.setValue(true);
      float horizontalDiameter = reverseEdgeInXPixelCoordinates - forwardEdgeInXPixelCoordinates;
      float verticalDiameter = upwardEdgeInYPixelCoordinates - downwardEdgeInYPixelCoordinates;
      float diameterInPixels = (horizontalDiameter + verticalDiameter) * 0.5f;
      float diameterInMils = MathUtil.convertNanoMetersToMils(diameterInPixels *
                                                                  getNanometersPerPixel(subtype));
      measuredDiameterInMils.setValue(diameterInMils);

      jointBoundaryRegion = RegionOfInterest.createRegionFromRegionBorder(
          roundedForwardEdgeInXPixelCoordinates,
          roundedDownwardEdgeInYPixelCoordinates,
          roundedReverseEdgeInXPixelCoordinates,
          roundedUpwardEdgeInYPixelCoordinates,
          0, RegionShapeEnum.OBROUND);
    }
    else
    {
      jointBoundaryRegion = null;
      foundJoint.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
    }

    return jointBoundaryRegion;
  }

  /**
   * @author Peter Esbensen
   */
  private static RegionOfInterest findJointBoundaries(RegionOfInterest forwardTruncatedProfileRegion,
                                                      RegionOfInterest reverseTruncatedProfileRegion,
                                                      RegionOfInterest downwardsTruncatedProfileRegion,
                                                      RegionOfInterest upwardsTruncatedProfileRegion,
                                                      float[] forwardProfile,
                                                      float[] reverseProfile,
                                                      float[] downwardProfile,
                                                      float[] upwardProfile,
                                                      BooleanRef foundJoint,
                                                      FloatRef measuredDiameterInMils,
                                                      Subtype subtype,
                                                      boolean useMaxSlopeMethod,
                                                      float percentOfMax)
  {
    Assert.expect(forwardProfile != null);
    Assert.expect(reverseProfile != null);
    Assert.expect(downwardProfile != null);
    Assert.expect(upwardProfile != null);
    Assert.expect(foundJoint != null);
    Assert.expect(forwardTruncatedProfileRegion != null);
    Assert.expect(downwardsTruncatedProfileRegion != null);
    Assert.expect(reverseTruncatedProfileRegion != null);
    Assert.expect(upwardsTruncatedProfileRegion != null);
    Assert.expect(forwardProfile.length > 0);
    Assert.expect(reverseProfile.length > 0);
    Assert.expect(downwardProfile.length > 0);
    Assert.expect(upwardProfile.length > 0);
    Assert.expect(subtype != null);
    Assert.expect(measuredDiameterInMils != null);


    float forwardEdgeInXPixelCoordinates = 0;
    float reverseEdgeInXPixelCoordinates = 0;
    float downwardEdgeInYPixelCoordinates = 0;
    float upwardEdgeInYPixelCoordinates = 0;

    if (useMaxSlopeMethod == false)
    {
      forwardEdgeInXPixelCoordinates = forwardTruncatedProfileRegion.getMinX() +
                                       ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(forwardProfile,
                                                                                                  (float)percentOfMax);
      reverseEdgeInXPixelCoordinates = reverseTruncatedProfileRegion.getMaxX() -
                                       ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(reverseProfile,
                                                                                                  (float)percentOfMax);
      downwardEdgeInYPixelCoordinates = downwardsTruncatedProfileRegion.getMinY() +
                                        ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(downwardProfile,
                                                                                                   (float)percentOfMax);
      upwardEdgeInYPixelCoordinates = upwardsTruncatedProfileRegion.getMaxY() -
                                      ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(upwardProfile,
                                                                                                 (float)percentOfMax);
    }
    else
    {
      forwardEdgeInXPixelCoordinates = forwardTruncatedProfileRegion.getMinX() +
                                       ArrayUtil.interpolatedMinIndex(forwardProfile);
      reverseEdgeInXPixelCoordinates = reverseTruncatedProfileRegion.getMaxX() -
                                       ArrayUtil.interpolatedMinIndex(reverseProfile);
      downwardEdgeInYPixelCoordinates = downwardsTruncatedProfileRegion.getMinY() +
                                        ArrayUtil.interpolatedMinIndex(downwardProfile);
      upwardEdgeInYPixelCoordinates = upwardsTruncatedProfileRegion.getMaxY() -
                                      ArrayUtil.interpolatedMinIndex(upwardProfile);
    }

    RegionOfInterest jointBoundaryRegion = null;

    int roundedForwardEdgeInXPixelCoordinates = Math.round(forwardEdgeInXPixelCoordinates);
    int roundedDownwardEdgeInYPixelCoordinates = Math.round(downwardEdgeInYPixelCoordinates);
    int roundedReverseEdgeInXPixelCoordinates = Math.round(reverseEdgeInXPixelCoordinates);
    int roundedUpwardEdgeInYPixelCoordinates = Math.round(upwardEdgeInYPixelCoordinates);

    if ((roundedForwardEdgeInXPixelCoordinates < roundedReverseEdgeInXPixelCoordinates) &&
        (roundedDownwardEdgeInYPixelCoordinates < roundedUpwardEdgeInYPixelCoordinates))
    {
      foundJoint.setValue(true);
      float horizontalDiameter = reverseEdgeInXPixelCoordinates - forwardEdgeInXPixelCoordinates;
      float verticalDiameter = upwardEdgeInYPixelCoordinates - downwardEdgeInYPixelCoordinates;
      float diameterInPixels = (horizontalDiameter + verticalDiameter) * 0.5f;
      float diameterInMils = MathUtil.convertNanoMetersToMils(diameterInPixels *
          getNanometersPerPixel(subtype));
      measuredDiameterInMils.setValue(diameterInMils);

      jointBoundaryRegion = RegionOfInterest.createRegionFromRegionBorder(
                            roundedForwardEdgeInXPixelCoordinates,
                            roundedDownwardEdgeInYPixelCoordinates,
                            roundedReverseEdgeInXPixelCoordinates,
                            roundedUpwardEdgeInYPixelCoordinates,
                            0, RegionShapeEnum.OBROUND);
    }
    else
    {
      jointBoundaryRegion = null;
      foundJoint.setValue(false);
      measuredDiameterInMils.setValue(0.0f);
    }

    return jointBoundaryRegion;
  }

  /**
   * @author Peter Esbensen
   */
  static public Collection<ComponentInspectionData> getComponentInspectionDataCollection(
      Collection<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Set<ComponentInspectionData> componentInspectionDataSet = new HashSet<ComponentInspectionData>();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      componentInspectionDataSet.add(jointInspectionData.getComponentInspectionData());
    }
    return componentInspectionDataSet;
  }

  /**
   * Sums up the absolute values of all the values in the specified profile and then converts that to a mil area value.
   *
   * @author Matt Wharton
   */
  public static float measureAreaUnderProfileCurveInMils(float[] profile, MagnificationEnum magnificationEnum)
  {
    Assert.expect(profile != null);
    Assert.expect(magnificationEnum != null);

    // Sum up the profile values (absolute values).
    float sum = 0f;
    for (float value : profile)
    {
      sum += Math.abs(value);
    }

    // Take the sum and multiply it by the mils per pixel.
    final float MILS_PER_PIXEL = getMilsPerPixel(magnificationEnum);
    float area = sum * MILS_PER_PIXEL;

    return area;
  }

  /**
   * Sums up the absolute values of all the values in the specified profile and then converts that to a mil area value.
   * Assumes the nominal magnification level.
   *
   * @author Matt Wharton
   */
  public static float measureAreaUnderProfileCurveInMils(float[] profile)
  {
    Assert.expect(profile != null);

    return measureAreaUnderProfileCurveInMils(profile, MagnificationEnum.getCurrentNorminal());
  }

  /**
   * Sums up the absolute values of all the values in the specified profile and then converts that to a mm area value.
   *
   * @author Matt Wharton
   */
  public static float measureAreaUnderProfileCurveInMillimeters(float[] profile, MagnificationEnum magnificationEnum)
  {
    Assert.expect(profile != null);
    Assert.expect(magnificationEnum != null);

    // Sum up the profile values (absolute values).
    float sum = 0f;
    for (float value : profile)
    {
      sum += Math.abs(value);
    }

    // Take the sum and multiply it by the mils per pixel.
    final float MILLIMETERS_PER_PIXEL = getMillimetersPerPixel(magnificationEnum);
    float area = sum * MILLIMETERS_PER_PIXEL;

    return area;
  }

  /**
   * Sums up the absolute values of all the values in the specified profile and then converts that to a mm area value.
   * Assumes the nominal magnification level.
   *
   * @author Matt Wharton
   */
  public static float measureAreaUnderProfileCurveInMillimeters(float[] profile)
  {
    Assert.expect(profile != null);

    return measureAreaUnderProfileCurveInMillimeters(profile, MagnificationEnum.getCurrentNorminal());
  }

  /**
   * @author Peter Esbensen
   */
  public static void shiftRegionAcrossIntoImageIfNecessary(RegionOfInterest regionOfInterest, Image image)
  {
    Assert.expect(regionOfInterest != null);
    Assert.expect(image != null);

    int minCoordinateAcross = (int)regionOfInterest.getMinCoordinateAcross();
    int maxCoordinateAcross = (int)regionOfInterest.getMaxCoordinateAcross();

    RegionOfInterest imageRoiOrientedSameAsJoint = RegionOfInterest.createRegionFromImage(image);
    imageRoiOrientedSameAsJoint.setOrientationInDegrees(regionOfInterest.getOrientationInDegrees());
    int imageDistanceAcross = imageRoiOrientedSameAsJoint.getLengthAcross();

    int distanceOverMinEdge = -minCoordinateAcross;
    int distanceOverMaxEdge = maxCoordinateAcross - (imageDistanceAcross - 1);

    int shiftDistance = 0;
    if (distanceOverMinEdge > 0)
      shiftDistance = distanceOverMinEdge;
    else if (distanceOverMaxEdge > 0)
      shiftDistance = -distanceOverMaxEdge;

    if ((regionOfInterest.getOrientationInDegrees() == 270) || (regionOfInterest.getOrientationInDegrees() == 180))
      shiftDistance *= -1;

    Assert.expect(!(distanceOverMaxEdge > 0 && distanceOverMinEdge > 0) );

    regionOfInterest.translateAlongAcross(0, shiftDistance);
  }

  /**
   * If needed, shifts the specified ROI into the bounds of the specified image.
   * NOTE: This method asserts the the ROI is small enough to be fully contained in the image.
   *
   * @todo Peter figure out if this is being called too much - george may need to fix program generation
   *
   * @author Matt Wharton
   */
  public static void shiftRegionIntoImageIfNecessary(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);

    // If the ROI already fits within the image, then we're done.
    if (roi.fitsWithinImage(image))
    {
      return;
    }

    // Make sure that the ROI isn't too big to fit in the image at all (even if it's shifted).
    RegionOfInterest imageRoi = RegionOfInterest.createRegionFromImage(image);
    int imageWidth = imageRoi.getWidth();
    int imageHeight = imageRoi.getHeight();
    int roiWidth = roi.getWidth();
    int roiHeight = roi.getHeight();
    Assert.expect(roiWidth <= imageWidth);
    Assert.expect(roiHeight <= imageHeight);

    int roiMinX = roi.getMinX();
    int roiMinY = roi.getMinY();
    int roiMaxX = roi.getMaxX();
    int roiMaxY = roi.getMaxY();
    int imageMinX = imageRoi.getMinX();
    int imageMinY = imageRoi.getMinY();
    int imageMaxX = imageRoi.getMaxX();
    int imageMaxY = imageRoi.getMaxY();

    // Figure out if we're off the image in X.
    int xShiftDistance = 0;
    if (roiMinX < imageMinX)
    {
      xShiftDistance = imageMinX - roiMinX;
    }
    else if (roiMaxX > imageMaxX)
    {
      xShiftDistance = imageMaxX - roiMaxX;
    }

    // Figure out if we're off the image in Y.
    int yShiftDistance = 0;
    if (roiMinY < imageMinY)
    {
      yShiftDistance = imageMinY - roiMinY;
    }
    else if (roiMaxY > imageMaxY)
    {
      yShiftDistance = imageMaxY - roiMaxY;
    }

    // Shift the ROI back onto the image.
    roi.translateXY(xShiftDistance, yShiftDistance);
  }

  /**
   * If needed, shifts the specified ROI into the bounds of the specified image.
   * If the ROI is too big to fit in the image, it is truncated to the image bounds.
   *
   * @author Matt Wharton
   */
  public static void shiftRegionIntoImageAndTruncateIfNecessary(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);

    // Make sure the ROI isn't bigger than the image.
    roi.setWidthKeepingSameMinX(Math.min(roi.getWidth(), image.getWidth()));
    roi.setHeightKeepingSameMinY(Math.min(roi.getHeight(), image.getHeight()));

    // Shift the region into the image.
    shiftRegionIntoImageIfNecessary(image, roi);
  }

  /**
   * Takes a derivative profile and scales the values so that the independent variable (x-axis)
   * uses the specified unit and magnification.  Our default derivative profiles are always
   * [change in y value in whatever units] / 1 pixel.  This routine changes the 1 pixel denominator
   * to however many mils or mm.  The resulting derivative profile's units then are always
   * [units of profile]/[specified independent variable units].
   *
   * @author Matt Wharton
   */
  public static float[] createUnitizedDerivativeProfile(float[] profile,
                                                        int stepSize,
                                                        MagnificationEnum magnificationEnum,
                                                        MeasurementUnitsEnum unitsOfIndependentVariable)
  {
    Assert.expect(profile != null);
    Assert.expect(stepSize > 0);
    Assert.expect(magnificationEnum != null);
    Assert.expect(unitsOfIndependentVariable != null);

    float pixelsPerIndependentVariableUnit = 0f;
    if (unitsOfIndependentVariable.equals(MeasurementUnitsEnum.MILLIMETERS))
    {
      pixelsPerIndependentVariableUnit = 1f / getMillimetersPerPixel(magnificationEnum);
    }
    else if (unitsOfIndependentVariable.equals(MeasurementUnitsEnum.MILS))
    {
      pixelsPerIndependentVariableUnit = 1f / getMilsPerPixel(magnificationEnum);
    }
    else
    {
      Assert.expect(false, "Unsupported independent variable unit: " + unitsOfIndependentVariable.getName());
    }

    float[] unitizedDerivativeProfile = ProfileUtil.createDerivativeProfile(profile, stepSize);
    float profileScalingFactor = pixelsPerIndependentVariableUnit / (float)stepSize;
    ProfileUtil.multiplyProfileByConstant(unitizedDerivativeProfile, profileScalingFactor);

    return unitizedDerivativeProfile;
  }

  /**
   * Creates a new profile by determining the curvature at each point in the passed profiles.
   *
   * Curvature is defined as:
   *                        f''(x)             f''(x) is the second derivative
   *     curvature(x) = -------------------
   *                    (1 + f'(x)^2)^(3/2)    f'(x) is the first derivatve
   *
   * @author George Booth
   */
  public static float[] createCurvatureProfile(float[] smoothedFirstDerivativeProfile, float[] smoothedSecondDerivativeProfile)
  {
    Assert.expect(smoothedFirstDerivativeProfile != null);
    Assert.expect(smoothedFirstDerivativeProfile.length > 0);
    Assert.expect(smoothedSecondDerivativeProfile != null);
    Assert.expect(smoothedSecondDerivativeProfile.length > 0);
    Assert.expect(smoothedFirstDerivativeProfile.length == smoothedSecondDerivativeProfile.length);

    float[] curvatureProfile = new float[smoothedFirstDerivativeProfile.length];
    for (int i = 0; i < smoothedFirstDerivativeProfile.length; i++)
    {
      double firstDeriv = smoothedFirstDerivativeProfile[i];
      double firstDerivSqr = firstDeriv * firstDeriv;
      double secondDeriv = smoothedSecondDerivativeProfile[i];
      double curvature = secondDeriv / Math.pow(1 + firstDerivSqr, 1.5);
      // scale curvature since it is a small number
      curvatureProfile[i] = (float)(curvature * 100.0);
    }
    return curvatureProfile;
  }

  /**
   * Returns the profile value that corresponds to a certain percentage (actually fraction) of the profile.
   * Does not do any interpolation, even if the percent lies between two pixel values.
   *
   * This was ported from 5DX FET/HSSOP code written by Patrick Lacz
   *
   * @author George Booth
   */
   public static float findPercentile(float[] profile,
                                int startSearch,
                                int endSearch,
                                float percentile)
   {
     Assert.expect(profile != null);
     Assert.expect(profile.length > 0);
     Assert.expect(startSearch < endSearch);
     Assert.expect(percentile >= 0.f);
     Assert.expect(percentile <= 1.f);

     int profileLength = endSearch - startSearch + 1;
     List<Float> profileCopy = new ArrayList<Float>();
     for (int i = startSearch; i < endSearch; i++)
     {
       profileCopy.add(profile[i]);
     }

     Collections.sort(profileCopy);

     int indexToFind = (int)((float)profileLength * percentile);
     float returnValue = profileCopy.get(indexToFind);

     return returnValue;
   }

  /**
   * @author Peter Esbensen
   */
  public static void truncateRegionSymmetricallyIntoImageIfNecessary(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);

    // If the ROI already fits within the image, then we're done.
    if (roi.fitsWithinImage(image))
    {
      return;
    }

    RegionOfInterest imageRoi = RegionOfInterest.createRegionFromImage(image);

    int roiMinX = roi.getMinX();
    int roiMinY = roi.getMinY();
    int roiMaxX = roi.getMaxX();
    int roiMaxY = roi.getMaxY();
    int imageMinX = imageRoi.getMinX();
    int imageMinY = imageRoi.getMinY();
    int imageMaxX = imageRoi.getMaxX();
    int imageMaxY = imageRoi.getMaxY();

    // Figure out if we're off the image in X.
    int xOverlap = 0;
    if (roiMinX < imageMinX)
    {
      xOverlap = imageMinX - roiMinX;
    }
    if (roiMaxX > imageMaxX)
    {
      int tempXOverlap = roiMaxX - imageMaxX;
      if (tempXOverlap > xOverlap)
        xOverlap = tempXOverlap;
    }

    // Figure out if we're off the image in Y.
    int yOverlap = 0;
    if (roiMinY < imageMinY)
    {
      yOverlap = imageMinY - roiMinY;
    }
    if (roiMaxY > imageMaxY)
    {
      int tempYOverlap = roiMaxY - imageMaxY;
      if (tempYOverlap > yOverlap)
        yOverlap = tempYOverlap;
    }

    // Shift the ROI back onto the image.
    int newWidth = roi.getWidth() - (2 * xOverlap);
    int newHeight = roi.getHeight() - (2 * yOverlap);
    roi.setHeightKeepingSameCenter(newHeight);
    roi.setWidthKeepingSameCenter(newWidth);

    // if the region lay completely off the image or if it needed a very large shift, it still may not be fully contained in the image
    // force the sucker to fit
    shiftRegionIntoImageAndTruncateIfNecessary(image, roi);
  }

  /**
   * Get a background profile for a component like Resistor, Capacitor, or Pcap
   *
   * @author Peter Esbensen
   */
  public static float[] getComponentBasedBackgroundProfile(ReconstructionRegion reconstructionRegion,
                                                           ReconstructedSlice reconstructedSlice,
                                                           RegionOfInterest componentRegionOfInterest,
                                                           JointInspectionData padOneJointInspectionData,
                                                           float backgroundRegionShiftAsFractionOfInterPadDistance,
                                                           Algorithm callerAlgorithm,
                                                           boolean postDiagnostics)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(callerAlgorithm != null);

    final int BACKGROUND_REGION_WIDTH = 3;

    Image image = reconstructedSlice.getOrthogonalImage();

    int interPadDistanceInPixels = padOneJointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    RegionOfInterest leftSideBackgroundRegion = new RegionOfInterest(componentRegionOfInterest);
    RegionOfInterest rightSideBackgroundRegion = new RegionOfInterest(componentRegionOfInterest);

    leftSideBackgroundRegion.setLengthAcross(BACKGROUND_REGION_WIDTH);
    rightSideBackgroundRegion.setLengthAcross(BACKGROUND_REGION_WIDTH);

    int backgroundShift = Math.round((componentRegionOfInterest.getLengthAcross() * 0.5f) +
                                     (interPadDistanceInPixels * backgroundRegionShiftAsFractionOfInterPadDistance));
    leftSideBackgroundRegion.translateAlongAcross(0, -1 * backgroundShift);
    rightSideBackgroundRegion.translateAlongAcross(0, backgroundShift);

    // shift region across if necessary
    shiftRegionAcrossIntoImageIfNecessary(leftSideBackgroundRegion, image);
    shiftRegionAcrossIntoImageIfNecessary(rightSideBackgroundRegion, image);
    truncateRegionSymmetricallyIntoImageIfNecessary(image, leftSideBackgroundRegion);
    truncateRegionSymmetricallyIntoImageIfNecessary(image, rightSideBackgroundRegion);

    float[] leftSideBackgroundProfile = ImageFeatureExtraction.profile(image, leftSideBackgroundRegion);
    float[] rightSideBackgroundProfile = ImageFeatureExtraction.profile(image, rightSideBackgroundRegion);

    float[] backgroundProfile = ProfileUtil.addProfiles(leftSideBackgroundProfile, rightSideBackgroundProfile);
    ProfileUtil.multiplyProfileByConstant(backgroundProfile, 0.5f);
    if (postDiagnostics)
    {
      if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getJointTypeEnum(), callerAlgorithm))
      {
        MeasurementRegionDiagnosticInfo leftBackgroundDiagnosticRegion =
            new MeasurementRegionDiagnosticInfo(leftSideBackgroundRegion,
                                                MeasurementRegionEnum.BACKGROUND_REGION);
        MeasurementRegionDiagnosticInfo rightBackgroundDiagnosticRegion =
            new MeasurementRegionDiagnosticInfo(rightSideBackgroundRegion,
                                                MeasurementRegionEnum.BACKGROUND_REGION);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     reconstructedSlice.getSliceNameEnum(),
                                     padOneJointInspectionData,
                                     callerAlgorithm,
                                     false,
                                     leftBackgroundDiagnosticRegion,
                                     rightBackgroundDiagnosticRegion);
      }
    }

    return backgroundProfile;
  }

  /**
   * Learn the best background region location for a component like Resistor, Capacitor, or Pcap
   *
   * @return best location as fraction of interpad distance
   *
   * @author Peter Esbensen
   */
  public static float learnChipBackgroundRegionLocations(Subtype subtype,
                                                         ManagedOfflineImageSet typicalBoardImages,
                                                         Algorithm callerAlgorithm) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(callerAlgorithm != null);

    final int NUMBER_OF_SAMPLES_REQUIRED = 100;

    final float MINIMUM_BACKGROUND_LOCATION = 20.0f; // if it is much closer, it can sometime overlap the component when locator doesn't lock on properly to a shifted component
    final float MAXIMUM_BACKGROUND_LOCATION = 100.0f;
    final float SEARCH_RESOLUTION = 5.0f;

    float bestBackgroundLocationScore = Float.NEGATIVE_INFINITY;
    float bestBackgroundLocation = 50.f;

    int numberOfImages = typicalBoardImages.size();
    float samplingFrequency = Math.round(numberOfImages / NUMBER_OF_SAMPLES_REQUIRED);
    int finalSamplingFrequency = Math.max(1, (int)samplingFrequency);
    numberOfImages = (int)Math.ceil((float)numberOfImages / finalSamplingFrequency);
    float backgroundValuesArray[] = new float[numberOfImages*2]; // 2 slices per sample

    for (float backgroundLocation = MINIMUM_BACKGROUND_LOCATION;
         backgroundLocation <= MAXIMUM_BACKGROUND_LOCATION;
         backgroundLocation += SEARCH_RESOLUTION)
    {
      // iterate through all joints in images and collect profiles
      ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
      imagesIterator.setSamplingFrequency(finalSamplingFrequency);
      ReconstructedImages reconstructedImages;

      int i = 0;

      while ((reconstructedImages = imagesIterator.getNext()) != null)
      {
        ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
        List<JointInspectionData> jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

        for (ComponentInspectionData componentInspectionData : getComponentInspectionDataCollection(jointInspectionDataList))
        {
          JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();

          Locator.locateRectangularComponent(reconstructedImages,
                                             subtype.getInspectionFamily().getSliceNameEnumForLocator(subtype),
                                             padOneJointInspectionData.getComponentInspectionData(),
                                             callerAlgorithm, false);

          for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
          {
            // PWL : This should never occur if numberOfImages is correct, but just to be safe...
            if (i == backgroundValuesArray.length)
              break;

            // get the component region
            RegionOfInterest componentRegionOfInterest = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData);

            // get an average of the two background profiles
            float[] backgroundProfile = getComponentBasedBackgroundProfile(reconstructionRegion,
                                                                           reconstructedSlice,
                                                                           componentRegionOfInterest,
                                                                           padOneJointInspectionData,
                                                                           backgroundLocation * 0.01f,
                                                                           callerAlgorithm, false);

            // add up the gray levels
            backgroundValuesArray[i++] = StatisticsUtil.standardDeviation(backgroundProfile);
          }
        }
        imagesIterator.finishedWithCurrentRegion();
      }
      float backgroundValueStandardDeviation = StatisticsUtil.standardDeviation(backgroundValuesArray);
      float backgroundLocationScore = -1f * backgroundValueStandardDeviation;
      if (backgroundLocationScore > bestBackgroundLocationScore)
      {
        bestBackgroundLocationScore = backgroundLocationScore;
        bestBackgroundLocation = backgroundLocation;
      }
    }
    return bestBackgroundLocation;
  }

  /**
   * Take a component RegionOfInterest, scale it and shift it so that its ends match the given fillet indices.
   *
   * @author Peter Esbensen
   */
  public static RegionOfInterest getComponentRegionOfInterestBasedOnFilletEdges(RegionOfInterest componentRegion,
                                                                                int padOneFilletEdgeIndex,
                                                                                int padTwoFilletEdgeIndex)
  {
    Assert.expect(componentRegion != null);

    RegionOfInterest modifiedComponentRegion = new RegionOfInterest(componentRegion);
    int originalCenterAlongRelativeToOriginalRegion = componentRegion.getLengthAlong() / 2;
    int newCenterAlongRelativeToOriginalRegion = Math.round((padOneFilletEdgeIndex + padTwoFilletEdgeIndex) * 0.5f);
    int centerShift = originalCenterAlongRelativeToOriginalRegion - newCenterAlongRelativeToOriginalRegion;
    int orientationInDegrees = componentRegion.getOrientationInDegrees();
    // need to make sure we center the component region correctly depending on the orientaion
    if ((orientationInDegrees == 0) || (orientationInDegrees == 180))
    {
      modifiedComponentRegion.translateAlongAcross( -centerShift, 0);
    }
    else if ((orientationInDegrees == 90) || (orientationInDegrees == 270))
    {
      modifiedComponentRegion.translateAlongAcross(centerShift, 0);
    }
    else
      Assert.expect(false); // things should have been rotated to an "orthogonal rotation" by now
    modifiedComponentRegion.setLengthAlong(padOneFilletEdgeIndex - padTwoFilletEdgeIndex);
    return modifiedComponentRegion;
  }

  /**
   * Place a rectangle of length one along (ie- a line) at the specified index within the component RegionOfInterest.
   *
   * @author Peter Esbensen
   */
  public static RegionOfInterest createRegionOfInterestToHighlightProfileFeature(RegionOfInterest profileRegion,
                                                                                int filletFeatureIndex)
  {
    Assert.expect(profileRegion != null);
    Assert.expect(filletFeatureIndex >= 0);
    Assert.expect(filletFeatureIndex < profileRegion.getLengthAlong());

    RegionOfInterest modifiedProfileRegion = new RegionOfInterest(profileRegion);
    int originalCenterAlongRelativeToOriginalRegion = profileRegion.getLengthAlong() / 2;
    int centerShift = originalCenterAlongRelativeToOriginalRegion - filletFeatureIndex;
    modifiedProfileRegion.translateAlongAcross( -centerShift, 0 );
    modifiedProfileRegion.setLengthAlong(1);
    return modifiedProfileRegion;
  }
  /**
   * @author Patrick Lacz
   * moved from Pressfit to here by Lam Wai Beng
   */
  public static Image createPinTemplateImage(
      float expectedPinWidthInMM,
      IntegerRef outExpectedPinWidthInPixels,
      float foregroundGraylevel,
      float backgroundGraylevel,
      int borderPixels,
      Subtype subtype)
  {
    Assert.expect(outExpectedPinWidthInPixels != null);

    float floatExpectedPinWidthInPixels = MathUtil.convertNanoMetersToPixelsUsingRound(
     (int)MathUtil.convertMillimetersToNanometers(expectedPinWidthInMM), getNanometersPerPixel(subtype));

    outExpectedPinWidthInPixels.setValue(Math.round(floatExpectedPinWidthInPixels));
    int templateWidth = (int)Math.ceil(floatExpectedPinWidthInPixels) + 2*borderPixels;
    Image pinTemplateImage = new Image(templateWidth, templateWidth);
    Paint.fillImage(pinTemplateImage, backgroundGraylevel);
    Paint.fillCircle(pinTemplateImage,
                     RegionOfInterest.createRegionFromImage(pinTemplateImage),
                     new DoubleCoordinate(templateWidth / 2.0, templateWidth / 2.0),
                     floatExpectedPinWidthInPixels / 2.0f, foregroundGraylevel);
    return pinTemplateImage;
  }
  
  /**
   * XCR-3318 - Oval PTH
   * @author Siew Yeng
   */
  public static Image createOvalShapedPinTemplateImage(
      float expectedPinWidthInMM,
      float expectedPinLengthInMM,
      IntegerRef outExpectedPinWidthInPixels,
      IntegerRef outExpectedPinLengthInPixels,
      float foregroundGraylevel,
      float backgroundGraylevel,
      int borderPixels,
      Subtype subtype)
  {
    Assert.expect(outExpectedPinWidthInPixels != null);

    float floatExpectedPinWidthInPixels = MathUtil.convertNanoMetersToPixelsUsingRound((int)MathUtil.convertMillimetersToNanometers(expectedPinWidthInMM), getNanometersPerPixel(subtype));
    
    float floatExpectedPinLengthInPixels = MathUtil.convertNanoMetersToPixelsUsingRound((int)MathUtil.convertMillimetersToNanometers(expectedPinLengthInMM), getNanometersPerPixel(subtype));

    outExpectedPinWidthInPixels.setValue(Math.round(floatExpectedPinWidthInPixels));
    outExpectedPinLengthInPixels.setValue(Math.round(floatExpectedPinLengthInPixels));
    int templateWidth = (int)Math.ceil(floatExpectedPinWidthInPixels) + 2*borderPixels;
    int templateLength = (int)Math.ceil(floatExpectedPinLengthInPixels) + 2*borderPixels;
    Image pinTemplateImage = new Image(templateWidth, templateLength);
    Paint.fillImage(pinTemplateImage, backgroundGraylevel);
    Paint.fillObround(pinTemplateImage,
                     RegionOfInterest.createRegionFromImage(pinTemplateImage),
                     new DoubleCoordinate(templateWidth / 2.0, templateLength / 2.0),
                     foregroundGraylevel);
    return pinTemplateImage;
  }

  /**
   * This is repeated code. Refactored to make coding easier to read.
   *
   * @author Lim, Lay Ngor modify function name for XCR1648
   * The code is obsolete and will be replace by the function below.
   * Please remove the code if no one use it.
   *
   * @author Lim, Seng Yew
   */
  public static boolean indictMinOrMaxThreshold(Algorithm algorithm,
                                               JointInspectionData jointInspectionData,
                                               SliceNameEnum sliceNameEnum,
                                               IndictmentEnum indictmentEnum,
                                               JointMeasurement measurement,
                                               float thresholdValue,
                                               boolean checkMinimum)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(indictmentEnum != null);
    Assert.expect(measurement != null);

    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    float measuredValue = measurement.getValue();

    boolean fail;
    if (checkMinimum)
      fail = MathUtil.fuzzyLessThan(measuredValue, thresholdValue);
    else
      fail = MathUtil.fuzzyGreaterThan(measuredValue, thresholdValue);
    if (fail)
    {
      // Indict the joint for open.
      postFailingJointTextualDiagnostic(algorithm,
                                        jointInspectionData,
                                        jointInspectionData.getInspectionRegion(),
                                        sliceNameEnum,
                                        measurement,
                                        thresholdValue);

      // Create an Open indictment for insufficient second derivative area.
      JointIndictment jointIndictment = new JointIndictment(indictmentEnum,
                                                                               algorithm,
                                                                               sliceNameEnum);

      // Tie the 'second derivative area' measurement to this joint.
      jointIndictment.addFailingMeasurement(measurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(jointIndictment);

      return false;
    }
    else
    {
      postPassingJointTextualDiagnostic(algorithm,
                                        jointInspectionData,
                                        sliceNameEnum,
                                        jointInspectionData.getInspectionRegion(),
                                        measurement,
                                        thresholdValue);
      return true;
    }
  }

   /**
   * This is repeated code. Refactored to make coding easier to read.
   * Modify to support 
   * a) checking on minimum threshold of a joint
   * b) checking on maximum threshold of a joint
   * c) checking on both minimum and maximum threshold of a joint
   *
   * @author Lim, Seng Yew
   * @author Lim, Lay Ngor modify for XCR1648
   */
  public static boolean indictMinMaxThreshold(Algorithm algorithm,
                                               JointInspectionData jointInspectionData,
                                               SliceNameEnum sliceNameEnum,
                                               IndictmentEnum indictmentEnum,
                                               JointMeasurement measurement,
                                               float minThresholdValue,
                                               float maxThresholdValue,
                                               MinMaxThresholdChekingModeEnum chekingMode)
  {
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(indictmentEnum != null);
    Assert.expect(measurement != null);

    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    float measuredValue = measurement.getValue();
    // joint is passed for minimum & maximum threshold in default.
    boolean measuredValueBelowMinimum = false;
    boolean measuredValueAboveMaximum = false;

    boolean checkMinimum = false;
    boolean checkMaximum = false;
    switch(chekingMode)
    {
      case CHECK_MINIMUM_THRESHOLD_ONLY:
        checkMinimum = true;
        break;
      case CHECK_MAXIMUM_THRESHOLD_ONLY:
        checkMaximum = true;
        break;
      case CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD:
        checkMinimum = true;
        checkMaximum = true;
        break;
      default:
        Assert.expect(false);//unexpected checking mode
        break;
    }
    
    if(checkMinimum)
    {
      if(MathUtil.fuzzyLessThan(measuredValue, minThresholdValue))
      {
      // measured value is below the allowable minimum.
      measuredValueBelowMinimum = true;
      
        // Indict the joint for open.
        postFailingJointTextualDiagnostic(algorithm,
                                            jointInspectionData,
                                            jointInspectionData.getInspectionRegion(),
                                            sliceNameEnum,
                                            measurement,
                                            minThresholdValue);    
      }
    }
    
    //Skip checking for maximumThreshold if joint already failed on minimumThreshold checking.
    //This will gurantee only one failed-indictment is displayed at Diagnostic page for the same joint.
    //Just remove the checking of "measuredValueBelowMinimum" if we want to detect and view both 
    //minimum and maximum threshold inspection results at GUI in future.
    if(measuredValueBelowMinimum == false && checkMaximum == true)
    {
      if(MathUtil.fuzzyGreaterThan(measuredValue, maxThresholdValue))
      {
        // measured value is above the allowable maximum.
        measuredValueAboveMaximum = true;      

        // Indict the joint for open.
        postFailingJointTextualDiagnostic(algorithm,
                                            jointInspectionData,
                                            jointInspectionData.getInspectionRegion(),
                                            sliceNameEnum,
                                            measurement,
                                            maxThresholdValue);        
      }      
    }
    
    //Only need to indict the defect type of this joint for once at binary or xml result file 
    //even the joint failed both minimum and maximum threshold.
    if (measuredValueBelowMinimum || measuredValueAboveMaximum)
    {
      // Create an Open indictment for insufficient second derivative area.
      JointIndictment jointIndictment = new JointIndictment(indictmentEnum,
                                                                               algorithm,
                                                                               sliceNameEnum);

      // Tie the 'second derivative area' measurement to this joint.
      jointIndictment.addFailingMeasurement(measurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(jointIndictment);

      return false;
    }

     //Only post minimum threshold value for passing joint  - to avoid posting duplicate measurement result.  
     postPassingJointTextualDiagnostic(algorithm,
                                        jointInspectionData,
                                        sliceNameEnum,
                                        jointInspectionData.getInspectionRegion(),
                                        measurement,
                                        minThresholdValue);
     return true;
  }

  /**
   * Enhances the image contrast by equalizing the histogram.
   * This code was leveraged from the ImageJ software (free, no licensing required).
   *
   * @author George Booth
   */
  public static void enhanceContrastByEqualizingHistogram(Image image)
  {
    Assert.expect(image != null);

    // get the histogram
    int numHistogramBins = 256;
    RegionOfInterest imageRoi = new RegionOfInterest(0, 0, image.getWidth(), image.getHeight(), 0, RegionShapeEnum.RECTANGULAR);
    int[] histogram = Threshold.histogram(image, imageRoi, numHistogramBins);

    // get the sum of the histogram bins
    double sum = getWeightedValue(histogram, 0);
    for (int i = 1; i < 255; i ++)
    {
      sum += 2 * getWeightedValue(histogram, i);
    }
    sum += getWeightedValue(histogram, 255);

    // figure the scale and create the contrast map lookup table
    double scale = 255.0 / sum;

    int[] lut = new int[256];
    lut[0] = 0;
    sum = getWeightedValue(histogram, 0);
    for (int i = 1; i < 255; i ++)
    {
      double delta = getWeightedValue(histogram, i);
      sum += delta;
      lut[i] = (int)Math.round(sum * scale);
      sum += delta;
    }
    lut[255] = 0;

    // update the image with the new contrast mapping
    int height = image.getHeight();
    int width = image.getWidth();
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
      {
        int grayLevel = (int)image.getPixelValue(x, y);
        image.setPixelValue(x, y, (float)lut[grayLevel]);
      }
  }

  /**
   * Enhances the image contrast by equalizing the histogram.
   * This code was leveraged from the ImageJ software (free, no licensing required).
   *
   * @author Wei Chin
   */
  public static void enhanceContrastByEqualizingHistogramWithShiftValue(Image image, int shiftValue)
  {
    Assert.expect(image != null);

    // get the histogram
    int numHistogramBins = 256;
    RegionOfInterest imageRoi = new RegionOfInterest(0, 0, image.getWidth(), image.getHeight(), 0, RegionShapeEnum.RECTANGULAR);
    int[] histogram = Threshold.histogram(image, imageRoi, numHistogramBins);

    // get the sum of the histogram bins
    double sum = getWeightedValue(histogram, 0);
    for (int i = 1; i < 255; i ++)
    {
      sum += 2 * getWeightedValue(histogram, i);
    }
    sum += getWeightedValue(histogram, 255);

    // figure the scale and create the contrast map lookup table
    double scale = 255.0 / sum;

    int[] lut = new int[256];
    lut[0] = 0;
    sum = getWeightedValue(histogram, 0);
    for (int i = 1; i < 255; i ++)
    {
      double delta = getWeightedValue(histogram, i);;
      sum += delta;
      lut[i] = (int)Math.round(sum * scale);
      sum += delta;
    }
    lut[255] = 0;

    if(shiftValue != 0)
      ProfileUtil.shiftProfile(shiftValue, lut);

    // update the image with the new contrast mapping
    int height = image.getHeight();
    int width = image.getWidth();
    for (int y = 0; y < height; y++)
      for (int x = 0; x < width; x++)
      {
        int grayLevel = (int)image.getPixelValue(x, y);
        image.setPixelValue(x, y, (float)lut[grayLevel]);
      }
  }

  /**
   * Switch for linear vs square root equalization.
   * Linear will produce a straight line plot of sum of counts versus gray levels,
   * Square root is not linear but tends to be less severe.
   *
   * @author George Booth
   */
  private static double getWeightedValue(int[] histogram, int i)
  {
    int h = histogram[i];
//  Linear equalition
//    return (double)h;
//  Square root equalization (less severe)
    return Math.sqrt((double)h);
  }


  /**
   * @param subtype
   * @return
   * @author Swee Yee Wong - new thickness table implementation
   * @author Wei Chin
   */
  public static SolderThickness getSolderThickness(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);
    
    String magnificationName = null;
    if (subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.LOW))
    {
      magnificationName = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_LOW_MAGNIFICATION);
    }
    else
    {
      magnificationName = Config.getInstance().getStringValue(HardwareConfigEnum.XRAY_Z_AXIS_MOTOR_POSITION_LOOKUP_FOR_HIGH_MAGNIFICATION);
    }

    UserGainEnum userGainEnum = subtype.getSubtypeAdvanceSettings().getUserGain();

    SolderThickness solderThickness = null;
    int solderThicknessVersion = subtype.getPanel().getProject().getThicknessTableVersion();
    String projName = subtype.getPanel().getProject().getName();

    if (true)
    {
      SolderThicknessEnum solderThicknessEnum = SolderThicknessEnum.LEAD_SOLDER_SHADED_BY_COPPER;
      String solderThicknessFilePathInProject = FileName.getProjectThicknessTableFullPath(projName, magnificationName, userGainEnum.getSolderThicknessSubFolderName(), solderThicknessEnum.getFileName(), solderThicknessVersion);
      if (FileUtil.exists(solderThicknessFilePathInProject) == false)
      {
        //swee-yee.wong - XCR-3237 Intermittent software crash when generate precision tuning image
        subtype.getPanel().getProject().isProjectSolderThicknessFilesAvailable(false);
      }
      solderThickness = SolderThickness.getInstance(solderThicknessFilePathInProject);
    }
    return solderThickness;
  }

/**
   * @param subtype
   * @return
   * 
   * @author Wei Chin
   */
  public static int getBackgroundSensitivityThreshold(Subtype subtype)
  {
    int backgroundSensitivityThreshold = 0;
    backgroundSensitivityThreshold = (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.BACKGROUND_SENSITIVITY_VALUE);    
    return backgroundSensitivityThreshold;
  }

  /**
   * @author Ngie Xing
   * XCR-2027 Exclude Outlier
   * Return mean without outliers if Intelligent Learning is Enabled and Outliers exist
   * 
   * isLearningData - TRUE if this function is used for Learn ONLY
   */
  public static float meanWithExcludeOutlierDecision(float[] array, int startIndex, int endIndex, boolean isLearningData)
  {
    float mean = 0.0f;
    if (_isIntelligentLearning && isLearningData && array.length >= 2)
    {
      if(_isDeveloperDebugModeOn)
        System.out.println("Please take note, meanWithExcludeOutlierDecision is being called.");
      
      return mean = StatisticsUtil.meanExcludeOutlier(array, startIndex, endIndex);
    }
    else
    {
      return mean = StatisticsUtil.mean(array, startIndex, endIndex);
    }
  }

  /**
   * @author Ngie Xing
   * XCR-2027 Exclude Outlier
   * Use this to get median according to whether Intelligent Learning is turned on in Software Configuration
   */
  public static float medianWithExcludeOutlierDecision(float[] array, boolean allowModification)
  {
    if (_isIntelligentLearning && array.length >= 2)
    {
      if(_isDeveloperDebugModeOn)
        System.out.println("Please take note, medianWithExcludeOutlierDecision is being called.");

      if(allowModification)
        return StatisticsUtil.medianExcludedOutliersAllowModification(array);
      else
        return StatisticsUtil.medianExcludedOutliers(array);
    }
    else
    {
      if(allowModification)
        return StatisticsUtil.medianAllowModification(array);
      else
        return StatisticsUtil.median(array);
    }
  }

  /**
   * @author Ngie Xing
   * XCR-2027 Exclude Outlier
   * Use this to get quartile ranges according to whether Intelligent Learning is turned on in Software Configuration
   */
  public static float[] quartileRangesWithExcludeOutlierDecision(float[] array, boolean allowModification)
  {
    if (_isIntelligentLearning && array.length >= 2)
    {
      if(_isDeveloperDebugModeOn)
        System.out.println("Please take note, quartileRangesWithExcludeOutlierDecision is being called.");

      if(allowModification)
        return StatisticsUtil.quartileRangesExcludedOutliersAllowModification(array);
      else
        return StatisticsUtil.quartileRangesExcludedOutliers(array);
    }
    else
    {
      if (allowModification)
        return StatisticsUtil.quartileRangesAllowModification(array);
      else
        return StatisticsUtil.quartileRanges(array);
    }
  }

  /**
   * @author Ngie Xing
   * XCR-2027 Exclude Outlier
   * Use this to get quartile ranges according to whether Intelligent Learning is turned on in Software Configuration
   */
  public static float percentileWithExcludeOutlierDecision(float[] array, float targetPercentile, boolean allowModification)
  {
    if (_isIntelligentLearning && array.length >= 2)
    {
      if(_isDeveloperDebugModeOn)
        System.out.println("Please take note, PercentileWithExcludeOutlierDecision is being called.");

      if(allowModification)
        return StatisticsUtil.percentileExcludedOutliersAllowModification(array, targetPercentile);
      else
        return StatisticsUtil.percentileExcludedOutliers(array, targetPercentile);
    }
    else
    {
      if(allowModification)
        return StatisticsUtil.percentileAllowModification(array, targetPercentile);
      else
        return StatisticsUtil.percentile(array, targetPercentile);
    }
  }
  
  /**
   * @param reconstructedImages
   * @param diagnosticImage
   * @param roi  --> currently not used, plan to have this to support the correct location of the diagnostic area in the image
   * @param sliceNameEnum 
   * 
   * @author Wei Chin
   */
  public static void addDiagnosticImageIntoReconstructionImages(ReconstructedImages reconstructedImages,
                                                                Image diagnosticImage, 
                                                                RegionOfInterest roi,  
                                                                SliceNameEnum sliceNameEnum)
  {
    SliceNameEnum diagnosticSlice = EnumToUniqueIDLookup.getInstance().getDiagnosticSliceNameEnum(sliceNameEnum);
    if (diagnosticSlice != null )
    {
      Image sliceImage = reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage();
      
      //XCR-2757 - Siew Yeng - fix reference count warning error when reclassify
      // Do not need to add diagnostic image when reclassify
      if(reconstructedImages.hasReconstructedSlice(diagnosticSlice) == false)
      {      
        Image diagnosticImageRelocated = com.axi.guiUtil.BufferedImageUtil.overlayImagesWithWhiteBackground(diagnosticImage, 
                                                                                                            roi, 
                                                                                                            sliceImage.getWidth(), 
                                                                                                            sliceImage.getHeight());
        reconstructedImages.addImage(diagnosticImageRelocated, diagnosticSlice);

        diagnosticImageRelocated.decrementReferenceCount();
      }
    }
  }
  
    /**
   * @param reconstructedImages
   * @param diagnosticImage
   * @param roi  --> currently not used, plan to have this to support the correct location of the diagnostic area in the image
   * @param sliceNameEnum
   * 
   * @author Wei Chin
   */
  public static Image combineDiagnosticImage(ReconstructedImages reconstructedImages, 
                                            Image combineDiagnosticImage, 
                                            Image diagnosticImage, 
                                            RegionOfInterest roi,  
                                            SliceNameEnum sliceNameEnum)
  {
    SliceNameEnum diagnosticSlice = EnumToUniqueIDLookup.getInstance().getDiagnosticSliceNameEnum(sliceNameEnum);
    Image newCombineDiagnosticImage = null;
    if (diagnosticSlice != null )
    {
      Image sliceImage = reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage();
      Image diagnosticImageRelocated = com.axi.guiUtil.BufferedImageUtil.overlayImagesWithWhiteBackground(diagnosticImage, 
                                                                                                          roi, 
                                                                                                          sliceImage.getWidth(), 
                                                                                                          sliceImage.getHeight());
      if(combineDiagnosticImage == null)
      {
        //newCombineDiagnosticImage = Image.createCopy(diagnosticImageRelocated, RegionOfInterest.createRegionFromImage(diagnosticImageRelocated));
        newCombineDiagnosticImage = diagnosticImageRelocated;
      }
      else
      {
        newCombineDiagnosticImage = Arithmetic.addImages(combineDiagnosticImage, RegionOfInterest.createRegionFromImage(combineDiagnosticImage), diagnosticImageRelocated, RegionOfInterest.createRegionFromImage(diagnosticImageRelocated));
        combineDiagnosticImage.decrementReferenceCount();
        diagnosticImageRelocated.decrementReferenceCount();
      }
    }
    return newCombineDiagnosticImage;
  }
  
    /**
   * @param reconstructedImages
   * @param combineDiagnosticImage
   * @param sliceNameEnum 
   * 
   * @author Wei Chin
   */
  public static void addCombineDiagnosticImageIntoReconstructionImages(ReconstructedImages reconstructedImages,Image combineDiagnosticImage, SliceNameEnum sliceNameEnum)
  {
    SliceNameEnum diagnosticSlice = EnumToUniqueIDLookup.getInstance().getDiagnosticSliceNameEnum(sliceNameEnum);
    if (diagnosticSlice != null )
    {
      //XCR-2757 - Siew Yeng - fix reference count warning error when reclassify
      // Do not need to add diagnostic image when reclassify
      if(reconstructedImages.hasReconstructedSlice(diagnosticSlice) == false)
      {
        reconstructedImages.addImage(combineDiagnosticImage, diagnosticSlice);
      }
    }
  }
  
  /**
   * @author Siew Yeng
   */
  public static boolean isSubtypeUsingMaskingMethod(Subtype subtype)
  {
    boolean usingMasking = false;
    if((subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES)))
    {
      String value = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES);
      if(value.equals("True"))
        usingMasking = true;
      else if(value.equals("False"))
        usingMasking = false;
      else
        Assert.expect(false, "Unexpected Masking Void Method value.");
    }

    if(usingMasking)
    {       
      List<String> maskImagePathList = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                                FileName.getMaskImagePatternString(subtype.getShortName()));
      if (maskImagePathList.isEmpty())
      {
        LocalizedString warningText = new LocalizedString("ALGDIAG_MASKING_IMAGE_NOT_AVAILABLE_KEY",
                                                 new Object[] { subtype.getShortName()});

        raiseAlgorithmWarning(warningText);

        usingMasking = false;
      }
    }
    
    return usingMasking;
  }
  
  /**
   * @param subtype
   * @param inspectionFamilyEnum
   * @param jointInspectionDataObjects
   * XCR-3014, Assert when run production/ generate inspection image when turn on Resize for PTH 
   * @return 
   * @author sheng chuan
   */
  public static SliceNameEnum choseSliceToPerformLocatorForJoint(JointInspectionData jointInspectionDataObjects)
  {
    SliceNameEnum sliceNameEnum = null;
    Subtype subtype = jointInspectionDataObjects.getSubtype();
    InspectionFamilyEnum inspectionFamilyEnum = jointInspectionDataObjects.getInspectionFamilyEnum();
    if (inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP))
    {
      if (ChipMeasurementAlgorithm.testAsOpaque(jointInspectionDataObjects))
      {
        sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      }
      else
      {
        sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      }
    }
    else
    {
      sliceNameEnum = subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    }
    return sliceNameEnum;
  }
  
  /**
   * @param subtype
   * @param inspectionFamilyEnum
   * @param jointInspectionDataObjects
   * XCR-3014, Assert when run production/ generate inspection image when turn on Resize for PTH 
   * @return 
   */
  public static SliceNameEnum choseSliceToPerformLocatorForJoint(List<JointInspectionData> jointInspectionDataObjects)
  {
    return choseSliceToPerformLocatorForJoint(jointInspectionDataObjects.get(0));
  }
  
  /**
   * XCR-3014, Assert when run production/ generate inspection image when turn on Resize for PTH 
   * @author sheng chuan
   * @author Lim, Lay Ngor - rename function name.
   */
  public static boolean isAlgorithmAbleToPerformResize(Algorithm algorithm)
  {
    if(algorithm instanceof SharedShortAlgorithm)
      return false;
    else
      return true;
  }
  
  /** 
   * Return true if the algorithm enum is allow to resize while classification.
   * 20 Apr 2016: Short is not allow to resize while classification.
   * @author Lim, Lay Ngor
   */
  public static boolean isAlgorithmEnumAbleToPerformResize(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);
    
    if(algorithmEnum == AlgorithmEnum.SHORT)
      return false;
    else
      return true;
  }
  
  /** 
   * Return true if the algorithm of the input measurement enums is allow to resize while classification.
   * 20 Apr 2016: Short is not allow to resize while classification.
   * @author Lim, Lay Ngor
   */
  public static boolean isMeasurementEnumAbleToPerformResize(MeasurementEnum measurementEnum)
  {
    Assert.expect(measurementEnum != null); 
    
    if(measurementEnum == MeasurementEnum.SHORT_LENGTH ||
       measurementEnum == MeasurementEnum.SHORT_THICKNESS ||
       measurementEnum == MeasurementEnum.SHORT_BROKEN_PIN_AREA_SIZE ||
       measurementEnum == MeasurementEnum.SOLDER_BALL_SHORT_DELTA_THICKNESS)
      return false;
    else
      return true;
  }   
  
  /**
   * @param testSubProgram
   * @param subtype
   * @param resizeIfNeeded
   * @return 
   * XCR-3014, Assert when run production/ generate inspection image when turn on Resize for PTH 
   * @author sheng chuan
   */
  public static double getNanoMeterPerPixelWithResizeConsideration(TestSubProgram testSubProgram,Subtype subtype, boolean ableToPerformResize)
  {
    // Map the ROI from panel coordinates to inspection region relative image coordinates.
    double NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    
    if(testSubProgram.isHighMagnification())
      NANOMETERS_PER_PIXEL = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();

    int scaleFactor = 1;
    if(ableToPerformResize)
    {
      scaleFactor = getResizeFactor(subtype);
    }
    return scaleFactor * 1.0f / NANOMETERS_PER_PIXEL;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static int getResizeFactor(Subtype subtype)
  {
    Assert.expect(subtype != null);
    int resizeFactor = 1;
    if(ImageProcessingAlgorithm.useResize(subtype) )
    {
      resizeFactor = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
    }
    return resizeFactor;
  }
  
  /**
   * @author Siew Yeng
   */
  public static boolean isAbleToStitchComponentImageAtVVTS(Subtype subtype)
  {
    boolean enable = false;
    if((subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS)))
    {
      String value = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS);
      if(value.equals("True"))
        enable = true;
      else if(value.equals("False"))
        enable = false;
      else
        Assert.expect(false, "Unexpected Send All Joint When Fail Component value.");
    }
    
    return enable;
  }
}
