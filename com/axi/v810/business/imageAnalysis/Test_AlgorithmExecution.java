package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Patrick Lacz
 */
class Test_AlgorithmExecution extends AlgorithmUnitTest
{
  /**
   * @author Patrick Lacz
   */
  public Test_AlgorithmExecution()
  {
    // do nothing
  }

  private boolean _printTimeInformation = false;

  /**
   * @author Sunit Bhalla
   */
  private void testProgramWithImageSetCreation(String projectName)
  {
    if (_printTimeInformation)
      System.out.println("Testing " + projectName);

    TimerUtil timer = new TimerUtil();

    Test_StatisticalTuningEngine testStatisticalTuningEngine = new Test_StatisticalTuningEngine();

    timer.start();
    Project project = getProject(projectName);
    timer.stop();
    if (_printTimeInformation)
      System.out.println("Project created: " + timer.getElapsedTimeInMillis() / 1000.0);
    timer.reset();

    try
    {
      timer.start();
      testStatisticalTuningEngine.deleteAllLearnedData(project);
      timer.stop();
      if (_printTimeInformation)
        System.out.println("deleteOldDatabaseData done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     projectName,
                                                                     POPULATED_BOARD);
      imageSetDataList.add(imageSetData);

      timer.start();
      generatePseudoImageSet(project, imageSetData);
      timer.stop();
      if (_printTimeInformation)
        System.out.println("generating images done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      learnPanelUnitTest(project, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
      timer.stop();
      if (_printTimeInformation)
        System.out.println("Learning completed: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();
    }
    catch (DatastoreException ex)
    {
      ex.printStackTrace();
    }

    timer.start();
    ImageSetData imageSet = createImageSetAndSetProgramFilters(project, projectName, POPULATED_BOARD);

    List<ImageSetData> imageSets = new LinkedList<ImageSetData>();
    imageSets.add(imageSet);

    runPanelUnitTestInspection(project, imageSets);
    timer.stop();
    if (_printTimeInformation)
      System.out.println("Tests run: " + timer.getElapsedTimeInMillis() / 1000.0);
    timer.reset();

  }

  /**
   * @author Patrick Lacz
   * @author Sunit Bhalla
   */
  private void testFamiliesAll()
  {

    if (_printTimeInformation)
      System.out.println("Testing Families_all_rlv");

    TimerUtil timer = new TimerUtil();

    Test_StatisticalTuningEngine testStatisticalTuningEngine = new Test_StatisticalTuningEngine();

    timer.start();
    Project project = getProject("FAMILIES_ALL_RLV");
    timer.stop();
    if (_printTimeInformation)
      System.out.println("Project created: " + timer.getElapsedTimeInMillis() / 1000.0);
    timer.reset();

    try
    {
      timer.start();
      testStatisticalTuningEngine.deleteAllLearnedData(project);
      timer.stop();
      if (_printTimeInformation)
        System.out.println("deleteOldDatabaseData done: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();

      timer.start();
      testStatisticalTuningEngine.testLearnPanel(project);
      timer.stop();
      if (_printTimeInformation)
        System.out.println("Learning completed: " + timer.getElapsedTimeInMillis() / 1000.0);
      timer.reset();
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
    }

    timer.start();
    ImageSetData imageSet = createImageSetAndSetProgramFilters(project,
                                                               ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                               POPULATED_BOARD);

    List<ImageSetData> imageSets = new LinkedList<ImageSetData>();
    imageSets.add(imageSet);
    runPanelUnitTestInspection(project, imageSets);
    timer.stop();
    if (_printTimeInformation)
      System.out.println("Tests run: " + timer.getElapsedTimeInMillis() / 1000.0);
    timer.reset();
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // Right now, these tests don't really verify any test results, they
    // just make sure that things don't crash.

    //    testProgramWithImageCreation("41k_joints");
    //    testProgramWithImageCreation("sledge");
//    testProgramWithImageSetCreation("0027Parts");
    testFamiliesAll();
  }


  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmExecution());
  }
}
