package com.axi.v810.business.imageAnalysis;

/**
 * A super class for diagnostic information sent to the gui.
 *
 * @author Patrick Lacz
 */
public class DiagnosticInfo
{
  /**
   * @author Patrick Lacz
   */
  public DiagnosticInfo()
  {
    // do nothing
  }


  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    // do nothing
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    // do nothing
  }


}
