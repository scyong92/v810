package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/*
 Notes on using this program to measure learning times
   1) Go to test() and enable the tests.  (This test is usually not run because it takes so long.)
   2) Go to StatisticalTuningEngine.learnSubtype().  Set printLearningStatus to true so that learning
      times will be printed out for each subtype.
 */

/**
 * @author Sunit Bhalla
 */
class Test_StatisticalTuningPerformance extends AlgorithmUnitTest
{

  final boolean _PRINT_TIME_INFO = true;
  final boolean _DONT_PRINT_TIME_INFO = false;

  /**
   * @author Sunit Bhalla
   */
  public Test_StatisticalTuningPerformance()
  {
    // do nothing
  }


  /**
   * @author Sunit Bhalla
   */
  private void deleteDatabaseData(Project project) throws DatastoreException
  {
    Test_StatisticalTuningEngine testStatisticalTuningEngine = new Test_StatisticalTuningEngine();
    TimerUtil timer = new TimerUtil();

    timer.start();
    testStatisticalTuningEngine.deleteAllLearnedData(project);
    timer.stop();
    System.out.println("Delete database done: " + timer.getElapsedTimeInMillis() / 1000.0);
    timer.reset();
  }


  /**
   * @author Sunit Bhalla
   * This tests learning on an entire panel.
   */
  private void learnPanel(Project project, int numberOfImageSets, boolean printTimeInfo) throws DatastoreException
  {
    Assert.expect(project != null);
    TimerUtil timer = new TimerUtil();

    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();

    for (int i = 0; i < numberOfImageSets; i++)
    {
      String imageName = "PerformanceImages" + i;
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     imageName,
                                                                     POPULATED_BOARD);
      imageSetDataList.add(imageSetData);
    }

    timer.start();
    learnPanelUnitTest(project, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
    timer.stop();
    if (printTimeInfo)
    {
      System.out.println("Learning completed with " + numberOfImageSets + " image set(s): " +
                         timer.getElapsedTimeInMillis() / 60000.0);
    }
    timer.reset();
  }


  /**
     * @author Sunit Bhalla
   */
  private void generateImageRuns(Project project, int numberOfImageRunsToCreate)
  {
    List<ImageSetData> imageSets = new ArrayList<ImageSetData>();
    try
    {
      imageSets = ImageManager.getInstance().getCompatibleImageSetData(project);
    }
    catch (DatastoreException ex)
    {
    }
    for (int i = 0; i < numberOfImageRunsToCreate; i++)
    {
      String imageName = "PerformanceImages" + i;
      System.out.println("ImageName: " + imageName);
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     imageName,
                                                                     POPULATED_BOARD);
      if (imageSets.contains(imageSetData) == false)
        generatePseudoImageSet(project, imageSetData);
    }
  }

  /**
   * @author Sunit Bhalla
   */
  public void runPerformanceTests() throws XrayTesterException
  {
    boolean printTimeInformation = true;

    TimerUtil timer = new TimerUtil();

    timer.start();
    //    Project project = getProject("FAMILIES_ALL_RLV");
    //    Project project = getProject("NEPCON_WIDE_EW");
    Project project = getProject("CYGNUS_ASAP_83");
    //    Project project = getProject("41k_joints");

    timer.stop();
    if (printTimeInformation)
      System.out.println("Project created: " + timer.getElapsedTimeInMillis() / 1000.0);
    timer.reset();

    generateImageRuns(project, 20);

    deleteDatabaseData(project);

    testIncrementalLearning(project);

    // Generate performance data for learning on one image set without using data in the database
    System.out.println("Learning on one panel; no panels in database");
    learnPanel(project, 1, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on two image set without using data in the database
    System.out.println("Learning on two panels; no panels in database");
    learnPanel(project, 2, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on three image set without using data in the database
    System.out.println("Learning on three panels; no panels in database");
    learnPanel(project, 3, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on five image set without using data in the database
    System.out.println("Learning on five panels; no panels in database");
    learnPanel(project, 5, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on ten image set without using data in the database
    System.out.println("Learning on ten panels; no panels in database");
    learnPanel(project, 10, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on ten image set without using data in the database
    System.out.println("Learning on twenty panels; no panels in database");
    learnPanel(project, 20, _PRINT_TIME_INFO);
    deleteDatabaseData(project);


    // Generate performance data for learning on one image set with one image set data in database
    System.out.println("Learning on one panel; one panel in database");
    learnPanel(project, 1, _DONT_PRINT_TIME_INFO);
    learnPanel(project, 1, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on two image sets with two image sets data in database
    System.out.println("Learning on two panels; two panels in database");
    learnPanel(project, 2, _DONT_PRINT_TIME_INFO);
    learnPanel(project, 2, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on three image sets with three image sets data in database
    System.out.println("Learning on three panels; three panels in database");
    learnPanel(project, 3, _DONT_PRINT_TIME_INFO);
    learnPanel(project, 3, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on five image sets with five image sets data in database
    System.out.println("Learning on five panels; five panels in database");
    learnPanel(project, 5, _DONT_PRINT_TIME_INFO);
    learnPanel(project, 5, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on ten image sets with ten image sets data in database
    System.out.println("Learning on ten panels; ten panels in database");
    learnPanel(project, 10, _DONT_PRINT_TIME_INFO);
    learnPanel(project, 10, _PRINT_TIME_INFO);
    deleteDatabaseData(project);

    // Generate performance data for learning on ten image sets with ten image sets data in database
    System.out.println("Learning on twenty panels; twenty panels in database");
    learnPanel(project, 20, _DONT_PRINT_TIME_INFO);
    learnPanel(project, 20, _PRINT_TIME_INFO);
    deleteDatabaseData(project);
  }


  /**
   * @author Sunit Bhalla
   */
  public void testIncrementalLearning(Project project)
  {
    try
    {
      // Generate performance data for learning on one image set five times (tests incremental learning speed)
      System.out.println("Incremental learning test: 1 board; iteration 1");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 2");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 3");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 4");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 5");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 6");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 7");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 8");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 9");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 1 board; iteration 10");
      learnPanel(project, 1, _PRINT_TIME_INFO);
      deleteDatabaseData(project);

      // Generate performance data for learning on five image sets five times (tests incremental learning speed)
      System.out.println("Incremental learning test: 5 boards; iteration 1");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 2");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 3");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 4");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 5");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 6");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 7");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 8");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 9");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      System.out.println("Incremental learning test: 5 boards; iteration 10");
      learnPanel(project, 5, _PRINT_TIME_INFO);
      deleteDatabaseData(project);

    }
    catch (DatastoreException ex)
    {
      ex.printStackTrace();
    }

  }
  /**
   * @author Sunit Bhalla
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // Normally, this test is not run.  It takes a LONG time to run.  To enable, set the following condition to true.
    if (false)
    {
      try
      {
        Config.getInstance().loadIfNecessary();
        runPerformanceTests();
      }
      catch (Throwable ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * @author Sunit Bhalla
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_StatisticalTuningPerformance());
  }
}
