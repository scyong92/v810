package com.axi.v810.business.imageAnalysis;

import com.axi.util.Assert;
import com.axi.util.ArrayUtil;
import com.axi.util.MathUtil;
import com.axi.util.ProfileUtil;

/**
 * Some common algorithm utility methods to extract specific features.  These methods are shared among
 * various algorithms that look for specific features.  Shared code is used to keep the results consistent.
 *
 * @author George Booth
 */
public class AlgorithmFeatureUtil
{

  /**
   * Constructor.
   *
   * All methods are static, so shouldn't ever need to instantiate.
   *
   * @author George Booth
   */
  private AlgorithmFeatureUtil()
  {
    // Do nothing...
  }

  /**
   * Compute the center of mass of a profile given the model that each value represents the mass at that pixel
   * @param profile The profile to be analyzed
   * @param offset The coordinate of the 0th pixel
   * @return center of mass in units of profile samples
   * @author Rick Gaudette
   */
  public static float centerOfMass(float[] profile, float offset) {
      Assert.expect(profile != null);
      Assert.expect(profile.length > 0);
      float totalMass = 0.0F;
      float distanceWeightedMass = 0.0F;
      for (int i = 0; i < profile.length; i++) {
          totalMass += profile[i];
          distanceWeightedMass += profile[i] *  (i - offset);
      }
      return distanceWeightedMass / totalMass;
  }

  /**
   * Locates the heel edge in the specified "smoothed" pad thickness profile.  The heel edge is the
   * location where the profile becomes greater than heelEdgeFractionOfMax * the maximum profile
   * thickness. Returns the interpolated bin number of the heel edge.
   *
   * Used in GullwingMeasurementAlgorithm, AdvancedGullwingMeasurementAlgorithm, QuadFlatNoLeadMeasurementAlgorithm
   *
   * @author Matt Wharton
   * @author George Booth
   * @author Lim, Lay Ngor - Clear tombstone - Support profile reverse search capability for open Chip use.
   */
  public static float locateHeelEdgeUsingThickness(float[] smoothedPadThicknessProfile,
                                                   float heelEdgeFractionOfMax,
                                                   boolean isReverseSearch)
  {
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect((heelEdgeFractionOfMax >= 0f) && (heelEdgeFractionOfMax <= 1f));

    // Find the index of the max value in the thickness profile.
    int maxThicknessIndex = ArrayUtil.maxIndex(smoothedPadThicknessProfile);
    float maxThicknessValue = smoothedPadThicknessProfile[maxThicknessIndex];

    // Find the target thickness value for the heel edge.
    float targetThicknessValue = maxThicknessValue * heelEdgeFractionOfMax;

    // Find the bin whose thickness value most closely matches the target thickness.
    // Find the nearest whole bin first.
    boolean foundHeelEdge = false;
    //LayNgor - Clear tombstone - Support reverse search capability
    int i = 0;
    if(isReverseSearch == false)
    {
      for (i=0; i <= maxThicknessIndex; ++i)
      {
        if (smoothedPadThicknessProfile[i] >= targetThicknessValue)
        {
          foundHeelEdge = true;
          break;
        }
      }
    }
    else
    {
      for (i=smoothedPadThicknessProfile.length-1; i >= maxThicknessIndex; --i)
      {
        if (smoothedPadThicknessProfile[i] >= targetThicknessValue)
        {
          foundHeelEdge = true;
          break;
        }
      }
    }

    float heelEdgeSubpixelBin = 0;
    // If we found the target heel edge thickness, interpolate the subpixel bin.
    if (foundHeelEdge)
    {
      heelEdgeSubpixelBin = i;

      // Now, if necessary, interpolate to find the "fractional" bin number.
      if ((i > 0) && (i < smoothedPadThicknessProfile.length))
      {
        // Calculate the fractional offset between bins.
        float interpolatedOffset = 0f;
        //LayNgor - Clear tombstone - Support reverse search capability
        float thicknessDeltaBetweenProfileBins = 0f;
        if(isReverseSearch == false)
          thicknessDeltaBetweenProfileBins = smoothedPadThicknessProfile[i] - smoothedPadThicknessProfile[i - 1];
        else
          thicknessDeltaBetweenProfileBins = smoothedPadThicknessProfile[i-1] - smoothedPadThicknessProfile[i];
        
        // Protect against dividing by a near zero quantity.
        if (MathUtil.fuzzyGreaterThan(thicknessDeltaBetweenProfileBins, 0f))
        {
          //LayNgor - Clear tombstone - Support reverse search capability
          if(isReverseSearch == false)
            interpolatedOffset = (targetThicknessValue - smoothedPadThicknessProfile[i - 1]) / thicknessDeltaBetweenProfileBins;
          else
            interpolatedOffset = (smoothedPadThicknessProfile[i - 1] - targetThicknessValue)/ thicknessDeltaBetweenProfileBins;
          interpolatedOffset = Math.min(interpolatedOffset, 1f);
        }
        Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(interpolatedOffset, 0f));
        Assert.expect(MathUtil.fuzzyLessThanOrEquals(interpolatedOffset, 1f));

        // Subtract the interpolated fractional offset to get estimated bin number.
        //LayNgor - Clear tombstone - Support reverse search capability
        if(isReverseSearch == false)
          heelEdgeSubpixelBin = heelEdgeSubpixelBin - interpolatedOffset;
        else
          heelEdgeSubpixelBin = heelEdgeSubpixelBin + interpolatedOffset;
      }
    }
    // Otherwise, we'll set the heel edge at the middle bin.
    else
    {
      heelEdgeSubpixelBin = smoothedPadThicknessProfile.length / 2f;
    }

    return heelEdgeSubpixelBin;
  }

  /**
   * Locates the toe edge in the specified "smoothed" pad thickness profile.  The toe edge is the location
   * where the profile becomes greater than targetToeEdgeThickness.  Returns the interpolated bin number of
   * the toe edge. This routine searches from the end of the profile to the endSearchBin.
   *
   * Used in GullwingMeasurementAlgorithm, AdvancedGullwingMeasurementAlgorithm, QuadFlatNoLeadMeasurementAlgorithm
   *
   * @author Matt Wharton
   * @author George Booth
   */
  public static float locateToeEdgeUsingThickness(float[] smoothedPadThicknessProfile,
                                                  float targetToeEdgeThickness,
                                                  int endSearchBin)
  {
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect((endSearchBin >= 0) && (endSearchBin < smoothedPadThicknessProfile.length));

    // Find the bin (within the serach distance) whose thickness value most closely matches the target thickness.
    // Find the nearest whole bin first.
    boolean foundToeEdge = false;
    int i = smoothedPadThicknessProfile.length - 1;
    for (; i > endSearchBin; --i)
    {
      if (smoothedPadThicknessProfile[i] >= targetToeEdgeThickness)
      {
        foundToeEdge = true;
        break;
      }
    }

    float toeEdgeSubpixelBin = 0;

    // If we located the toe edge, interpolate to find the subpixel bin number.
    if (foundToeEdge)
    {
      toeEdgeSubpixelBin = i;

      // Calculate the fractional offset between bins (provided we don't run off the profile).
      if (i < (smoothedPadThicknessProfile.length - 1))
      {
        float interpolatedOffset = 0f;
        float thicknessDeltaBetweenProfileBins = smoothedPadThicknessProfile[i] - smoothedPadThicknessProfile[i + 1];
        // Protect against dividing by a near zero quantity.
        if (MathUtil.fuzzyGreaterThan(thicknessDeltaBetweenProfileBins, 0f))
        {
          interpolatedOffset = (smoothedPadThicknessProfile[i] - targetToeEdgeThickness) / thicknessDeltaBetweenProfileBins;
          interpolatedOffset = Math.min(interpolatedOffset, 1f);
        }
        Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(interpolatedOffset, 0f));
        Assert.expect(MathUtil.fuzzyLessThanOrEquals(interpolatedOffset, 1f));

        // Subtract the interpolated fractional offset to get estimated bin number.
        toeEdgeSubpixelBin -= interpolatedOffset;
      }
    }
    // Otherwise, we'll just use one bin past the end point.
    else
    {
      toeEdgeSubpixelBin = endSearchBin + 1;
    }

    // Ensure that the toe edge sub-pixel bin is always less than the profile length.
    toeEdgeSubpixelBin = Math.min(toeEdgeSubpixelBin, smoothedPadThicknessProfile.length - 1);

    return toeEdgeSubpixelBin;
  }

  /**
   * Finds the maximum heel slope in the specified derivative thickness profile.
   *
   * Used AdvancedGullwingMeasurementAlgorithm, QuadFlatNoLeadMeasurementAlgorithm
   *
   * @author Matt Wharton
   * @author George Booth
   */
  public static float findMaximumHeelSlopeBin(float[] derivativePadThicknessProfileAlong,
                                            int maxThicknessBin,
                                            int heelEdgeBin,
                                            int toeEdgeBin)
  {
    Assert.expect(derivativePadThicknessProfileAlong != null);
    Assert.expect((maxThicknessBin >= 0) && (maxThicknessBin < derivativePadThicknessProfileAlong.length));
    Assert.expect((heelEdgeBin >= 0) && (heelEdgeBin < derivativePadThicknessProfileAlong.length));
    Assert.expect((toeEdgeBin >= 0) && (toeEdgeBin < derivativePadThicknessProfileAlong.length));
    Assert.expect(heelEdgeBin <= toeEdgeBin);
    Assert.expect(heelEdgeBin <= maxThicknessBin);

    // Calculate the heel edge-toe edge distance.
    int heelEdgeToToeEdgeDistanceInPixels = toeEdgeBin - heelEdgeBin;

    // Calculate the heel edge to max thickness bin distance.
    int heelEdgeToMaxThicknessBinDistanceInPixels = maxThicknessBin - heelEdgeBin;

    // Figure out how far out from the heel edge to search (no more than 30% of the fillet length).
    final float MAX_SEARCH_DISTANCE_FRACTION_OF_FILLET_LENGTH = 0.3f;
    int searchDistanceFromHeelEdgeInPixels = heelEdgeToMaxThicknessBinDistanceInPixels;
    if (MathUtil.fuzzyGreaterThan((float)heelEdgeToMaxThicknessBinDistanceInPixels / (float)heelEdgeToToeEdgeDistanceInPixels,
                                  MAX_SEARCH_DISTANCE_FRACTION_OF_FILLET_LENGTH))
    {
      searchDistanceFromHeelEdgeInPixels = (int)Math.ceil((float)heelEdgeToToeEdgeDistanceInPixels *
                                                          MAX_SEARCH_DISTANCE_FRACTION_OF_FILLET_LENGTH);
    }

    int searchStartBin = 0;
    int searchEndBin = heelEdgeBin + searchDistanceFromHeelEdgeInPixels + 1;
    searchEndBin = Math.min(searchEndBin, derivativePadThicknessProfileAlong.length);

    // Find the maximum slope within the search window.
    int maximumHeelSlopeBin = ArrayUtil.maxIndex(derivativePadThicknessProfileAlong, searchStartBin, searchEndBin);

    // interpolate around max to find subpixel bin
    float maximumHeelSlopeSubpixelBin = ProfileUtil.findInterpolatedMaximumIndex(derivativePadThicknessProfileAlong, maximumHeelSlopeBin);

    return maximumHeelSlopeSubpixelBin;
  }

  /**
   * Find the minimum toe slope in the specified derivative thickness profile.
   *
   * Used AdvancedGullwingMeasurementAlgorithm, QuadFlatNoLeadMeasurementAlgorithm
   *
   * @author Matt Wharton
   * @author George Booth
   */
  public static float findMinimumToeSlopeBin(float[] derivativePadThicknessProfileAlong,
                                           int maxThicknessBin,
                                           int heelEdgeBin,
                                           int toeEdgeBin)
  {
    Assert.expect(derivativePadThicknessProfileAlong != null);
    Assert.expect((maxThicknessBin >= 0) && (maxThicknessBin < derivativePadThicknessProfileAlong.length));
    Assert.expect((heelEdgeBin >= 0) && (heelEdgeBin < derivativePadThicknessProfileAlong.length));
    Assert.expect((toeEdgeBin >= 0) && (toeEdgeBin < derivativePadThicknessProfileAlong.length));

    heelEdgeBin = Math.min(heelEdgeBin, maxThicknessBin);
    toeEdgeBin = Math.max(toeEdgeBin, maxThicknessBin);

    // Calculate the heel edge-toe edge distance.
    int heelEdgeToToeEdgeDistanceInPixels = toeEdgeBin - heelEdgeBin;

    // Calculate the max thickness bin to toe edge bin distance.
    int maxThicknessBinToToeEdgeDistanceInPixels = toeEdgeBin - maxThicknessBin;

    // Figure out how far out from the toe edge to search (no more than 30% of the fillet length).
    final float MAX_SEARCH_DISTANCE_FRACTION_OF_FILLET_LENGTH = 0.3f;
    int searchDistanceFromToeEdgeInPixels = maxThicknessBinToToeEdgeDistanceInPixels;
    if (MathUtil.fuzzyGreaterThan((float)maxThicknessBinToToeEdgeDistanceInPixels / (float)heelEdgeToToeEdgeDistanceInPixels,
                                  MAX_SEARCH_DISTANCE_FRACTION_OF_FILLET_LENGTH))
    {
      searchDistanceFromToeEdgeInPixels = (int)Math.ceil((float)heelEdgeToToeEdgeDistanceInPixels *
                                                         MAX_SEARCH_DISTANCE_FRACTION_OF_FILLET_LENGTH);
    }

    int searchStartBin = toeEdgeBin - searchDistanceFromToeEdgeInPixels;
    searchStartBin = Math.max(searchStartBin, 0);
    int searchEndBin = derivativePadThicknessProfileAlong.length;

    // Find the minimum slope within the search window.
    int minimumToeSlopeBin = ArrayUtil.minIndex(derivativePadThicknessProfileAlong, searchStartBin, searchEndBin);

    // interpolate around min to find subpixel bin
    float minimumToeSlopeSubpixelBin = ProfileUtil.findInterpolatedMaximumIndex(derivativePadThicknessProfileAlong, minimumToeSlopeBin);

    return minimumToeSlopeSubpixelBin;
  }

  /**
   * Given the specified pad thickness profile, locates the heel peak (max thickness) within the specified search
   * distance of the specified heel edge bin.  The matching bin number is returned.
   *
   * Used in GullwingMeasurementAlgorithm, AdvancedGullwingMeasurementAlgorithm, QuadFlatNoLeadMeasurementAlgorithm
   *
   * @author Matt Wharton
   * @author George Booth
   */
  public static int locateHeelPeak(float[] smoothedPadThicknessProfile, int heelEdgeBin, int heelSearchDistanceInPixels)
  {
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect((heelEdgeBin >= 0) && (heelEdgeBin < smoothedPadThicknessProfile.length));
    Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(heelSearchDistanceInPixels, 0f));

    // Find the pixel which marks the end-point (exclusive) of the search range.  We want to make sure
    // we don't search outside of the array bounds.
    int endPixel = Math.min((heelEdgeBin + heelSearchDistanceInPixels + 1), smoothedPadThicknessProfile.length);

    // Find the bin number of the max thickess within the search distance.
    int heelPeakBin = ArrayUtil.maxIndex(smoothedPadThicknessProfile, heelEdgeBin, endPixel);

    return heelPeakBin;
  }

  /**
   * Given the specified pad thickness profile, locates the toe peak by simply adding the specified
   * distance to the heel bin.
   *
   * Used in GullwingMeasurementAlgorithm, QuadFlatNoLeadMeasurementAlgorithm
   *
   * @author Matt Wharton
   * @author George Booth
   */
  public static int locateToePeak(float[] smoothedPadThicknessProfile,
                              int heelBin,
                              int toeDistanceInPixels)
  {
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect((heelBin >= 0) && (heelBin < smoothedPadThicknessProfile.length));
    Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(toeDistanceInPixels, 0f));

    // Calculate the toe peak bin.
    int toePeakBin = Math.min(heelBin + toeDistanceInPixels, smoothedPadThicknessProfile.length - 1);

    return toePeakBin;
  }


}
