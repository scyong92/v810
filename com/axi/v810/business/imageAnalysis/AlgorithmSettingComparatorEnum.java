package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;

/**
 * @author Cheah Lee Herng
 */
public class AlgorithmSettingComparatorEnum extends com.axi.util.Enum
{
  private static Map<String, AlgorithmSettingComparatorEnum> _nameToAlgorithmSettingComparatorEnumMap = new TreeMap<String, AlgorithmSettingComparatorEnum>(new AlphaNumericComparator());
  private static int _index = 0;

  public static AlgorithmSettingComparatorEnum GREATER_OR_EQUAL = new AlgorithmSettingComparatorEnum(++_index, "Greater or Equal to");
  public static AlgorithmSettingComparatorEnum GREATER = new AlgorithmSettingComparatorEnum(++_index, "Greater than");
  public static AlgorithmSettingComparatorEnum EQUAL = new AlgorithmSettingComparatorEnum(++_index, "Equal");
  public static AlgorithmSettingComparatorEnum LESS_THAN_OR_EQUAL = new AlgorithmSettingComparatorEnum(++_index, "Less than or Equal to");
  public static AlgorithmSettingComparatorEnum LESS_THAN = new AlgorithmSettingComparatorEnum(++_index, "Less than");

  private final String _name;

  /**
   * @author Cheah Lee Herng
   */
  private AlgorithmSettingComparatorEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name.intern();
    _nameToAlgorithmSettingComparatorEnumMap.put(_name, this);
  }

  /**
   * @author Cheah Lee Herng
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author v
   */
  public String toString()
  {
    return getName();
  }
}
