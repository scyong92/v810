package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

/**
 * Contains information needed to display a data "profile" to the user.
 * This contains the raw data points, suggestions for the axis labels, and suggestions for the axis extents.
 *
 * The data itself is represented as a list of typesafe "Pairs" of MeasurementRegionEnums and Lists of floats which
 * represent the raw data tuples--the number of values in the tuple matches the number of dimensions for the
 * ProfileDiagnosticInfo in question.
 *
 * e.g. { (PAD_REGION, {1, 1}), (HEEL_REGION, {2, 2}) }
 *
 * @author Peter Esbensen
 * @author Matt Wharton
 */
public class MultiDimensionProfileDiagnosticInfo extends ProfileDiagnosticInfo
{
  // -MDW: Sorry about this nasty, nested thing (see above description).  I wish java had the equivalent of a typedef  :(
  private List<Pair<MeasurementRegionEnum, List<Float>>> _data = new LinkedList<Pair<MeasurementRegionEnum, List<Float>>>();

  /**
   * Constructor.
   *
   * @param numberOfDimensions The number of dimensions for this profile.
   * @param axisDescriptions The axis descriptions.  The number of items in this list MUST match the number of dimensions.
   * @param axisExtents The axis extents.  The number of items in this list MUST match the number of dimensions.
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public MultiDimensionProfileDiagnosticInfo(String name,
                                             ProfileTypeEnum profileType,
                                             int numberOfDimensions,
                                             List<LocalizedString> axisDescriptions,
                                             List<Pair<Float, Float>> axisExtents,
                                             MeasurementUnitsEnum profileUnits)
  {
    super(name, profileType, numberOfDimensions, axisDescriptions, axisExtents, profileUnits);
  }

  /**
   * @return a List of ProfileDatum objects
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public List<Pair<MeasurementRegionEnum, List<Float>>> getData()
  {
    Assert.expect(_data != null);
    return _data;
  }

  /**
   * Adds the specified data tuple to the data set.
   *
   * @param measurementRegionEnum A MeasurementRegionEnum which corresponds with the specified tuple.
   * @param tuple A list of floats representing the data tuple.  The size of the list MUST match the number of dimensions
   * for this ProfileDiagnosticInfo.
   * @author Matt Wharton
   */
  public void addDataPoint(MeasurementRegionEnum measurementRegionEnum, List<Float> tuple)
  {
    Assert.expect(measurementRegionEnum != null);
    Assert.expect(tuple != null);
    Assert.expect(tuple.size() == _numberOfDimensions);
    Assert.expect(_data != null);

    _data.add(new Pair<MeasurementRegionEnum, List<Float>>(measurementRegionEnum, tuple));
  }

  /**
   * Clears out the raw data contained in this ProfileDiagnosticInfo.
   *
   * @author Matt Wharton
   */
  void clearData()
  {
    Assert.expect(_data != null);
    _data.clear();
  }
}
