package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.advancedGullwing.*;
import com.axi.v810.business.imageAnalysis.chip.*;
import com.axi.v810.business.imageAnalysis.exposedPad.*;
import com.axi.v810.business.imageAnalysis.gridArray.*;
import com.axi.v810.business.imageAnalysis.gullwing.*;
import com.axi.v810.business.imageAnalysis.largePad.*;
import com.axi.v810.business.imageAnalysis.ovalThroughHole.*;
import com.axi.v810.business.imageAnalysis.pcap.*;
import com.axi.v810.business.imageAnalysis.pressfit.*;
import com.axi.v810.business.imageAnalysis.quadFlatNoLead.*;
import com.axi.v810.business.imageAnalysis.throughHole.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;
import java.util.Map.Entry;

/**
 * An InspectionFamily holds a set of algorithms that are useful in testing a certain
 * kind of joint.  Examples of joint types include "gullwing", "grid array", etc.
 *
 * @author Peter Esbensen
 * @author Matt Wharton
 */
public abstract class InspectionFamily
{
  private static Map<InspectionFamilyEnum, InspectionFamily> _inspectionFamilyEnumToInspectionFamilyMap = new HashMap<InspectionFamilyEnum, InspectionFamily>();
  private static Map<JointTypeEnum, InspectionFamily> _jointTypeEnumToInspectionFamilyMap = new HashMap<JointTypeEnum, InspectionFamily>();

  private Map< AlgorithmEnum, Algorithm > _algorithmEnumToAlgorithmMap = new HashMap< AlgorithmEnum, Algorithm >();
  private InspectionFamilyEnum _inspectionFamilyEnum;

  private Map<JointTypeEnum, Map<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>>> _jointTypeToSettingToMeasurementEnumMap =
      new HashMap<JointTypeEnum,Map<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>>>();

  /**
   * @author Bill Darbie
   */
  static
  {
    InspectionFamily gridArrayInspectionFamily = new GridArrayInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.GRID_ARRAY, gridArrayInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.COLLAPSABLE_BGA, gridArrayInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.NON_COLLAPSABLE_BGA, gridArrayInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.CGA, gridArrayInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.CHIP_SCALE_PACKAGE, gridArrayInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR, gridArrayInspectionFamily);

    InspectionFamily gullwingInspectionFamily = new GullwingInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.GULLWING, gullwingInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.GULLWING, gullwingInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.JLEAD, gullwingInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.LEADLESS, gullwingInspectionFamily);
// now has its own inspection family
//    mapJointTypeEnumToInspectionFamily(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, gullwingInspectionFamily);

    InspectionFamily advancedGullwingInspectionFamily = new AdvancedGullwingInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.ADVANCED_GULLWING, advancedGullwingInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.SURFACE_MOUNT_CONNECTOR, advancedGullwingInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.RF_SHIELD, advancedGullwingInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.SMALL_OUTLINE_LEADLESS, advancedGullwingInspectionFamily);

    InspectionFamily chipInspectionFamily = new ChipInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.CHIP, chipInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.RESISTOR, chipInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.CAPACITOR, chipInspectionFamily);
    // Wei Chin (Tall Cap)
//    mapJointTypeEnumToInspectionFamily(JointTypeEnum.TALL_CAPACITOR, chipInspectionFamily);

    InspectionFamily polarizedCapInspectionFamily = new PolarizedCapInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.POLARIZED_CAP, polarizedCapInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.POLARIZED_CAP, polarizedCapInspectionFamily);

    InspectionFamily throughHoleInspectionFamily = new ThroughHoleInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.THROUGHHOLE, throughHoleInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.THROUGH_HOLE, throughHoleInspectionFamily);

    InspectionFamily pressfitInspectionFamily = new PressfitInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.PRESSFIT, pressfitInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.PRESSFIT, pressfitInspectionFamily);

    InspectionFamily largePadInspectionFamily = new LargePadInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.LARGE_PAD, largePadInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.SINGLE_PAD, largePadInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.LGA, largePadInspectionFamily);

    InspectionFamily quadFlatNoLeadInspectionFamily = new QuadFlatNoLeadInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD, quadFlatNoLeadInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, quadFlatNoLeadInspectionFamily);

    InspectionFamily exposedPadInspectionFamily = new ExposedPadInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.EXPOSED_PAD, exposedPadInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.EXPOSED_PAD, exposedPadInspectionFamily);
    
    //Siew Yeng - XCR-3318 - Oval PTH
    InspectionFamily ovalThroughHoleInspectionFamily = new OvalThroughHoleInspectionFamily();
    mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum.OVAL_THROUGHHOLE, ovalThroughHoleInspectionFamily);
    mapJointTypeEnumToInspectionFamily(JointTypeEnum.OVAL_THROUGH_HOLE, ovalThroughHoleInspectionFamily);
  }

  /**
   * Returns true if the given inspectionFamilyEnum corresponds to one of the special
   * two-pin inspection families.  These families are used for inspection caps,
   * resistors, and polarized caps.  They are special because:
   *
   * - program generation must keep both pins together in the same inspection region
   * - in many respects, they are inspected as a single entity, rather than two separate pads
   *
   * @author Peter Esbensen
   */
  public static boolean isSpecialTwoPinDevice(InspectionFamilyEnum inspectionFamilyEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);

    if ( (inspectionFamilyEnum.equals(InspectionFamilyEnum.CHIP)) ||
         (inspectionFamilyEnum.equals(InspectionFamilyEnum.POLARIZED_CAP)) )
      return true;
    return false;
  }

  /**
   * @author Bill Darbie
   */
  private static void mapInspectionFamilyEnumToInspectionFamily(InspectionFamilyEnum inspectionFamilyEnum,
                                                         InspectionFamily inspectionFamily)
  {
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(inspectionFamily != null);

    InspectionFamily prev = _inspectionFamilyEnumToInspectionFamilyMap.put(inspectionFamilyEnum, inspectionFamily);
    Assert.expect(prev == null);
  }

  /**
   * @author Bill Darbie
   */
  private static void mapJointTypeEnumToInspectionFamily(JointTypeEnum jointTypeEnum,
                                                  InspectionFamily inspectionFamily)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(inspectionFamily != null);

    InspectionFamily prev = _jointTypeEnumToInspectionFamilyMap.put(jointTypeEnum, inspectionFamily);
    Assert.expect(prev == null);
  }

  /**
   * @author Bill Darbie
   */
  public static InspectionFamily getInstance(InspectionFamilyEnum inspectionFamilyEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);

    InspectionFamily inspectionFamily = _inspectionFamilyEnumToInspectionFamilyMap.get(inspectionFamilyEnum);
    Assert.expect(inspectionFamily != null);
    return inspectionFamily;
  }

  /**
   * @author Andy Mechtenberg
   */
  public static InspectionFamily getInstance(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    InspectionFamily inspectionFamily = _jointTypeEnumToInspectionFamilyMap.get(jointTypeEnum);
    Assert.expect(inspectionFamily != null);
    return inspectionFamily;
  }

  /**
   * @author Bill Darbie
   */
  public static List<InspectionFamily> getInspectionFamilies()
  {
    return new ArrayList<InspectionFamily>(_inspectionFamilyEnumToInspectionFamilyMap.values());
  }

  /**
   * @author Peter Esbensen
   */
  protected InspectionFamily(InspectionFamilyEnum inspectionFamilyEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);
    _inspectionFamilyEnum = inspectionFamilyEnum;
  }

  /**
   * @author Matt Wharton
   */
  boolean hasAlgorithm(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);

    return _algorithmEnumToAlgorithmMap.containsKey(algorithmEnum);
  }

  /**
   * @author Peter Esbensen
   */
  Algorithm getAlgorithm(AlgorithmEnum algorithmEnum)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(_algorithmEnumToAlgorithmMap.containsKey(algorithmEnum));
    Algorithm algorithm = _algorithmEnumToAlgorithmMap.get(algorithmEnum);
    Assert.expect(algorithm != null);
    return algorithm;
  }

  /**
   * @author Bill Darbie
   */
  public static InspectionFamily getInspectionFamily(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    InspectionFamily inspectionFamily = _jointTypeEnumToInspectionFamilyMap.get(jointTypeEnum);
    Assert.expect(inspectionFamily != null, "No InspectionFamily assigned to JointTypeEnum: " + jointTypeEnum);
    return inspectionFamily;
  }

  /**
   * @author Bill Darbie
   */
  public static InspectionFamilyEnum getInspectionFamilyEnum(JointTypeEnum jointTypeEnum)
  {
    InspectionFamily inspectionFamily = getInspectionFamily(jointTypeEnum);
    return inspectionFamily.getInspectionFamilyEnum();
  }

  /**
   * @return all Algorithms associated with this InspectionFamily.  This
   * will return both enabled and disabled algorithms.
   * @author Peter Esbensen
   */
  public List<Algorithm> getAlgorithms()
  {
    return new ArrayList<Algorithm>(_algorithmEnumToAlgorithmMap.values());
  }

  /**
   * @author Bill Darbie
   */
  public List<AlgorithmEnum> getAlgorithmEnums()
  {
    return new ArrayList<AlgorithmEnum>(_algorithmEnumToAlgorithmMap.keySet());
  }

  /**
   * @author Peter Esbensen
   */
  public InspectionFamilyEnum getInspectionFamilyEnum()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    return _inspectionFamilyEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public String getName()
  {
    return _inspectionFamilyEnum.getName();
  }

  /**
   * @author Peter Esbensen
   */
  protected void addAlgorithm(Algorithm algorithm)
  {
    Assert.expect(algorithm != null);
    _algorithmEnumToAlgorithmMap.put(algorithm.getAlgorithmEnum(), algorithm);
  }

  /**
   * @author Matt Wharton
   */
  public static boolean hasAlgorithm(InspectionFamilyEnum inspectionFamilyEnum, AlgorithmEnum algorithmEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(algorithmEnum != null);

    InspectionFamily inspectionFamily = InspectionFamily.getInstance(inspectionFamilyEnum);
    return inspectionFamily.hasAlgorithm(algorithmEnum);
  }

  /**
   * @author Bill Darbie
   */
  public static Algorithm getAlgorithm(InspectionFamilyEnum inspectionFamilyEnum, AlgorithmEnum algorithmEnum)
  {
    Assert.expect(inspectionFamilyEnum != null);
    Assert.expect(algorithmEnum != null);

    InspectionFamily inspectionFamily = InspectionFamily.getInstance(inspectionFamilyEnum);
    return inspectionFamily.getAlgorithm(algorithmEnum);
  }

  /**
   * @return a Collection of the slices that will be inspected by this InspectionFamily based on the specified Subtype.
   * @author Matt Wharton
   */
  public abstract Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype);

  /**
   * Returns the SliceNameEnum that corresponds to the applicable pad slice for the given inspection family and
   * specified JointInspectionData.
   *
   * @author Matt Wharton
   */
  public abstract SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype);

  /**
   * @author Peter Esbensen
   */
  public abstract SliceNameEnum getSliceNameEnumForLocator(Subtype subtype);

  /**
   * Returns the slices that the short algorithm should be run on for this inspection family and subtype.
   *
   * By default, this method returns the same slice as the pad slice.
   *
   * @author Patrick Lacz
   */
  public Collection<SliceNameEnum> getShortInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return Collections.singleton(getDefaultPadSliceNameEnum(subtype));
  }

  /**
   * Returns true if this InspectionFamily needs to see all joints in a MeasurementGroup
   * to be able to make all its indictments.  If this returns false, this InspectionFamily
   * does not need to have its classifyMeasurementGroup entry point called.
   *
   * @author Peter Esbensen
   */
  public abstract boolean classifiesAtComponentOrMeasurementGroupLevel();

  /**
   * @author Peter Esbensen
   */
  public static Set<AlgorithmEnum> getAlgorithmEnumsSharedByAllInspectionFamilies()
  {
    List<InspectionFamily> inspectionFamilies = getInspectionFamilies();
    Set<AlgorithmEnum> commonAlgorithms = null;
    for (InspectionFamily inspectionFamily : inspectionFamilies)
    {
      if (commonAlgorithms == null)
      {
        commonAlgorithms = new HashSet<AlgorithmEnum>();
        commonAlgorithms.addAll(inspectionFamily.getAlgorithmEnums());
      }
      else
        commonAlgorithms.retainAll( inspectionFamily.getAlgorithmEnums() );
    }
    return commonAlgorithms;
  }

  /**
   * @author Peter Esbensen
   * @author Andy Mechtenberg
   */
  public void mapAlgorithmSettingToMeasurementEnum(JointTypeEnum jointTypeEnum,
                                                   AlgorithmSetting algorithmSetting,
                                                   SliceNameEnum sliceNameEnum,
                                                   MeasurementEnum measurementEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithmSetting != null);
    Assert.expect(measurementEnum != null);
    Assert.expect(sliceNameEnum != null);

    Map<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>> settingToSliceMeasurementPairMap = _jointTypeToSettingToMeasurementEnumMap.get(jointTypeEnum);
    if (settingToSliceMeasurementPairMap == null)
    {
      settingToSliceMeasurementPairMap = new HashMap<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>>();
      _jointTypeToSettingToMeasurementEnumMap.put(jointTypeEnum, settingToSliceMeasurementPairMap);
    }
    Assert.expect(settingToSliceMeasurementPairMap != null);
    
    List<Pair<SliceNameEnum, MeasurementEnum>> existingMeasurementEnums = settingToSliceMeasurementPairMap.get(algorithmSetting);
    if (existingMeasurementEnums == null)
    {
      existingMeasurementEnums = new ArrayList<Pair<SliceNameEnum, MeasurementEnum>>();
      settingToSliceMeasurementPairMap.put(algorithmSetting, existingMeasurementEnums);
    }
    Assert.expect(existingMeasurementEnums != null);
    Pair<SliceNameEnum, MeasurementEnum> pair = new Pair<SliceNameEnum,MeasurementEnum>(sliceNameEnum, measurementEnum);
    existingMeasurementEnums.add(pair);
  }

  /**
   * @author Andy Mechtenberg
   */
  public boolean doesAssociatedSliceMeasurementPairExist(JointTypeEnum jointTypeEnum, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithmSetting != null);
    
    Map<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>> settingToMeasurementMap = _jointTypeToSettingToMeasurementEnumMap.get(jointTypeEnum);
    if (settingToMeasurementMap != null)
    {
      List<Pair<SliceNameEnum, MeasurementEnum>> sliceMeasurementPairs = settingToMeasurementMap.get(algorithmSetting);
      if ((sliceMeasurementPairs == null) || (sliceMeasurementPairs.isEmpty()))
        return false;
      else
        return true;
    }
    else
      return false;
  }

  /**
   * @author Andy Mechtenberg
   */
  public List<Pair<SliceNameEnum, MeasurementEnum>> getAssociatedSliceMeasurementPairs(JointTypeEnum jointTypeEnum, AlgorithmSetting algorithmSetting)
  {
    Assert.expect(jointTypeEnum != null);
    Assert.expect(algorithmSetting != null);

    Map<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>> settingToMeasurementMap = _jointTypeToSettingToMeasurementEnumMap.get(jointTypeEnum);
    Assert.expect(settingToMeasurementMap != null);
    List<Pair<SliceNameEnum, MeasurementEnum>> sliceMeasPairs = settingToMeasurementMap.get(algorithmSetting);
    Assert.expect(sliceMeasPairs != null);
    return sliceMeasPairs;
  }

  /**
   * Get a list of algorithmSettings related to the SliceNameEnum and MeasurementEnum 
   * @author Ngie Xing, April 2014
   */
  public List<AlgorithmSetting> getAssociatedAlgorithmSettings(JointTypeEnum jointTypeEnum, SliceNameEnum sliceNameEnum, MeasurementEnum measurementEnum)
  {
    Assert.expect(jointTypeEnum != null);
    Map<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>> settingToMeasurementMap = _jointTypeToSettingToMeasurementEnumMap.get(jointTypeEnum);
    Assert.expect(settingToMeasurementMap != null);

    List<AlgorithmSetting> algorithmSettings = new ArrayList<AlgorithmSetting>();

    for (AlgorithmSetting algorithmSetting : settingToMeasurementMap.keySet())
    {
      for (Pair<SliceNameEnum, MeasurementEnum> pair : settingToMeasurementMap.get(algorithmSetting))
      {
        if (pair.getFirst() == sliceNameEnum && pair.getSecond() == measurementEnum)
        {
          algorithmSettings.add(algorithmSetting);
        }
      }
    }
    return algorithmSettings;
  }

  /**
   * @author George A. David
   */
  public static void printInspectionFamilyData()
  {
    Map<String, Set<String>> familyNameToJointTypeNameMap = new HashMap<String, Set<String>>();
    for(Map.Entry<JointTypeEnum, InspectionFamily> entry : _jointTypeEnumToInspectionFamilyMap.entrySet())
    {
      String jointName = entry.getKey().getName();
      String familyName = entry.getValue().getName();
      Set<String> jointNames = familyNameToJointTypeNameMap.get(familyName);
      if(jointNames == null)
      {
        jointNames = new HashSet<String>();
        familyNameToJointTypeNameMap.put(familyName, jointNames);
      }

      jointNames.add(jointName);
    }

    for(Map.Entry<String, Set<String>> entry : familyNameToJointTypeNameMap.entrySet())
    {
      System.out.println("Inspection Family: " + entry.getKey());
//      System.out.println("Joint Types:");
      for(String jointName : entry.getValue())
        System.out.println("   " + jointName);
    }
  }
  
  /**
   * XCR-3551 Custom Measurement XML
   * @author Janan Wong
   * @return list of measurement enums based on joint type
   */
  public List<MeasurementEnum> getMeasurementEnums(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    List<MeasurementEnum> measurementEnum = new ArrayList<MeasurementEnum>();
    Map<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>> algotithmSettingToListPairEnumMap
      = _jointTypeToSettingToMeasurementEnumMap.get(jointTypeEnum);

    Assert.expect(algotithmSettingToListPairEnumMap != null);
    for (Entry<AlgorithmSetting, List<Pair<SliceNameEnum, MeasurementEnum>>> entry : algotithmSettingToListPairEnumMap.entrySet())
    {
      List<Pair<SliceNameEnum, MeasurementEnum>> valueList = entry.getValue();
      for (Pair<SliceNameEnum, MeasurementEnum> pair : valueList)
      {
        measurementEnum.add(pair.getSecond());
      }
    }    
    return measurementEnum;
  }
}
