package com.axi.v810.business.imageAnalysis;

import java.io.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;

/**
 * Unit test class for AlgorithmUtil.
 *
 * @author Matt Wharton
 */
public class Test_AlgorithmUtil extends UnitTest
{
  /**
   * @author Matt Wharton
   */
  private Test_AlgorithmUtil()
  {
    // Do nothing...
  }

  /**
   * @author Peter Esbensen
   */
  public void testTruncateRegionSymmetricallyIntoImageIfNecessary()
  {
    // Create a dummy image for testing.
    final Image image = new Image(4, 4);

    // Verify that regions that already fit within an image aren't shifted.
    int expectedMinX = 0;
    int expectedMinY = 0;
    int expectedMaxX = 3;
    int expectedMaxY = 3;
    RegionOfInterest normalRoi = new RegionOfInterest(0, 0, 4, 4, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, normalRoi);
    Expect.expect(normalRoi.getMinX() == expectedMinX);
    Expect.expect(normalRoi.getMinY() == expectedMinY);
    Expect.expect(normalRoi.getMaxX() == expectedMaxX);
    Expect.expect(normalRoi.getMaxY() == expectedMaxY);

    // Verify that a region off the right side of an image gets shifted back in properly.
    expectedMinX = 1;
    expectedMinY = 0;
    expectedMaxX = 3;
    expectedMaxY = 3;
    RegionOfInterest outOfBoundsRoi = new RegionOfInterest(0, 0, 5, 4, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, outOfBoundsRoi);
    int newMinX = outOfBoundsRoi.getMinX();
    Expect.expect(newMinX == expectedMinX);
    int newMinY = outOfBoundsRoi.getMinY();
    Expect.expect(newMinY == expectedMinY);
    int newMaxX = outOfBoundsRoi.getMaxX();
    Expect.expect(newMaxX == expectedMaxX);
    int newMaxY = outOfBoundsRoi.getMaxY();
    Expect.expect(newMaxY == expectedMaxY);

    // Verify that a region off the left side of an image gets shifted back in properly.
    expectedMinX = 0;
    expectedMinY = 0;
    expectedMaxX = 2;
    expectedMaxY = 3;
    outOfBoundsRoi = new RegionOfInterest(-1, 0, 5, 4, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, outOfBoundsRoi);
    Expect.expect(outOfBoundsRoi.getMinX() == expectedMinX);
    Expect.expect(outOfBoundsRoi.getMinY() == expectedMinY);
    Expect.expect(outOfBoundsRoi.getMaxX() == expectedMaxX);
    Expect.expect(outOfBoundsRoi.getMaxY() == expectedMaxY);

    // Verify that a region off the bottom side of an image gets shifted back in properly.
    expectedMinX = 0;
    expectedMinY = 1;
    expectedMaxX = 3;
    expectedMaxY = 3;
    outOfBoundsRoi = new RegionOfInterest(0, 0, 4, 5, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, outOfBoundsRoi);
    Expect.expect(outOfBoundsRoi.getMinX() == expectedMinX);
    Expect.expect(outOfBoundsRoi.getMinY() == expectedMinY);
    Expect.expect(outOfBoundsRoi.getMaxX() == expectedMaxX);
    Expect.expect(outOfBoundsRoi.getMaxY() == expectedMaxY);

    // Verify that a region off the top side of an image gets shifted back in properly.
    expectedMinX = 0;
    expectedMinY = 0;
    expectedMaxX = 3;
    expectedMaxY = 2;
    outOfBoundsRoi = new RegionOfInterest(0, -1, 4, 5, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, outOfBoundsRoi);
    Expect.expect(outOfBoundsRoi.getMinX() == expectedMinX);
    Expect.expect(outOfBoundsRoi.getMinY() == expectedMinY);
    Expect.expect(outOfBoundsRoi.getMaxX() == expectedMaxX);
    Expect.expect(outOfBoundsRoi.getMaxY() == expectedMaxY);

    // what if a region is off the right by more than the region is wide?!?
    expectedMinX = 3;
    expectedMinY = 0;
    expectedMaxX = 3;
    expectedMaxY = 3;
    outOfBoundsRoi = new RegionOfInterest(0, 0, 10, 4, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, outOfBoundsRoi);
    newMinX = outOfBoundsRoi.getMinX();
    Expect.expect(newMinX == expectedMinX);
    newMinY = outOfBoundsRoi.getMinY();
    Expect.expect(newMinY == expectedMinY);
    newMaxX = outOfBoundsRoi.getMaxX();
    Expect.expect(newMaxX == expectedMaxX);
    newMaxY = outOfBoundsRoi.getMaxY();
    Expect.expect(newMaxY == expectedMaxY);

    image.decrementReferenceCount();
  }


  /**
   * @author Matt Wharton
   */
  public void testShiftRegionIntoImage()
  {
    // Create a dummy image for testing.
    final Image image = new Image(501, 501);

    // Verify that regions that already fit within an image aren't shifted.
    int expectedMinX = 100;
    int expectedMinY = 100;
    int expectedMaxX = 300;
    int expectedMaxY = 300;
    RegionOfInterest normalRoi = new RegionOfInterest(100, 100, 201, 201, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, normalRoi);
    Expect.expect(normalRoi.getMinX() == expectedMinX);
    Expect.expect(normalRoi.getMinY() == expectedMinY);
    Expect.expect(normalRoi.getMaxX() == expectedMaxX);
    Expect.expect(normalRoi.getMaxY() == expectedMaxY);

    // Verify that a region off the left side of an image gets shifted back in properly.
    expectedMinX = 0;
    expectedMinY = 100;
    expectedMaxX = 200;
    expectedMaxY = 300;
    RegionOfInterest outOfBoundsRoi = new RegionOfInterest(-100, 100, 201, 201, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, outOfBoundsRoi);
    Expect.expect(outOfBoundsRoi.getMinX() == expectedMinX);
    Expect.expect(outOfBoundsRoi.getMinY() == expectedMinY);
    Expect.expect(outOfBoundsRoi.getMaxX() == expectedMaxX);
    Expect.expect(outOfBoundsRoi.getMaxY() == expectedMaxY);

    // Verify that a region off the right side of an image gets shifted back in properly.
    expectedMinX = 300;
    expectedMinY = 100;
    expectedMaxX = 500;
    expectedMaxY = 300;
    outOfBoundsRoi = new RegionOfInterest(400, 100, 201, 201, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, outOfBoundsRoi);
    Expect.expect(outOfBoundsRoi.getMinX() == expectedMinX);
    Expect.expect(outOfBoundsRoi.getMinY() == expectedMinY);
    Expect.expect(outOfBoundsRoi.getMaxX() == expectedMaxX);
    Expect.expect(outOfBoundsRoi.getMaxY() == expectedMaxY);

    // Verify that a region off the top side of an image gets shifted back in properly.
    expectedMinX = 100;
    expectedMinY = 0;
    expectedMaxX = 300;
    expectedMaxY = 200;
    outOfBoundsRoi = new RegionOfInterest(100, -100, 201, 201, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, outOfBoundsRoi);
    Expect.expect(outOfBoundsRoi.getMinX() == expectedMinX);
    Expect.expect(outOfBoundsRoi.getMinY() == expectedMinY);
    Expect.expect(outOfBoundsRoi.getMaxX() == expectedMaxX);
    Expect.expect(outOfBoundsRoi.getMaxY() == expectedMaxY);

    // Verify that a region off the bottom side of an image gets shifted back in properly.
    expectedMinX = 100;
    expectedMinY = 300;
    expectedMaxX = 300;
    expectedMaxY = 500;
    outOfBoundsRoi = new RegionOfInterest(100, 400, 201, 201, 0, RegionShapeEnum.RECTANGULAR);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, outOfBoundsRoi);
    Expect.expect(outOfBoundsRoi.getMinX() == expectedMinX);
    Expect.expect(outOfBoundsRoi.getMinY() == expectedMinY);
    Expect.expect(outOfBoundsRoi.getMaxX() == expectedMaxX);
    Expect.expect(outOfBoundsRoi.getMaxY() == expectedMaxY);

    // Verify that ROIs that are wider than the image cause an assert.
    final RegionOfInterest superWideRoi = new RegionOfInterest(100, 100, 1000, 100, 0, RegionShapeEnum.RECTANGULAR);
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, superWideRoi);
      }
    });

    // Verify that ROIs that are taller than the image cause an assert.
    final RegionOfInterest superTallRoi = new RegionOfInterest(100, 100, 100, 1000, 0, RegionShapeEnum.RECTANGULAR);
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, superTallRoi);
      }
    });
    image.decrementReferenceCount();
  }

  /**
   * @author Peter Esbensen
   */
  void testGetCorrectedGrayLevel()
  {
    String solderThicknessFilePath = FileName.getCalibThicknessTableFullPath("M19", "Gain 1", SolderThicknessEnum.LEAD_SOLDER_SHADED_BY_COPPER.getFileName(), 0);
    SolderThickness thicknessTable = SolderThickness.getInstance(solderThicknessFilePath);
    for (float backgroundGrayLevel = 1.0f; backgroundGrayLevel < 255.0; backgroundGrayLevel = backgroundGrayLevel + 1.0f)
    {
      float foregroundGrayLevel = backgroundGrayLevel - 1.0f;
      float correctedGrayLevel = AlgorithmUtil.getCorrectedGreylevel(backgroundGrayLevel, foregroundGrayLevel, thicknessTable, 0);

      // Printing this relationship out is kind of interesting
      // System.out.println((int)backgroundGrayLevel + " " + correctedGrayLevel);

      // not sure how to check that these values make sense.  this is not the most useful test in the world.
      Expect.expect(correctedGrayLevel > 0);
      Expect.expect(correctedGrayLevel < 255);
    }
  }

  /**
   * Main unit test method.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // Test shifting regions back onto images.
    testShiftRegionIntoImage();

    testTruncateRegionSymmetricallyIntoImageIfNecessary();

    testGetCorrectedGrayLevel();
  }

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmUtil());
  }
}
