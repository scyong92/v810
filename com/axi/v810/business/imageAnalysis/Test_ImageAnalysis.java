package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Test class for ImageAnalysis.
 *
 * @author Matt Wharton
 */
public class Test_ImageAnalysis extends UnitTest
{
  // ImageAnalysis instance.
  private ImageAnalysis _imageAnalysis = new ImageAnalysis();

  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public Test_ImageAnalysis()
  {
    super();
  }

  /**
   * Tests the Assert functionality on all of the exposed interface.
   *
   * @author Matt Wharton
   */
  private void testAsserts()
  {
    // Test asserts on ImageAnalysis.classify()
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        _imageAnalysis.classifyMeasurementGroup(null, null);
      }
    });

    // Test asserts on ImageAnalysis.extractMeasurements().
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        try
        {
          PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
          panelInspectionSettings.clearAllInspectionSettings();
          _imageAnalysis.classifyJoints(null);
        }
        catch (DatastoreException dex)
        {
          dex.printStackTrace();
        }
      }
    });

    // Test asserts on ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad().
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
          _imageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(null);
      }
    });
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   * @author Matt Wharton
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // Test asserts.
    testAsserts();
  }

  /**
   * Entry point for test fixture.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ImageAnalysis());
  }
}
