#include <jni.h>

#include <string>
#include <list>

#include "boost/shared_array.hpp"
#include "boost/shared_ptr.hpp"

#include "util/src/sptAssert.h"
#include "../../../util/jniUtil.h"

#include "TextDiagnosticInfoObservableAdapter.h"
#include "ImageDiagnosticInfoObservableAdapter.h"
#include "ProfileDiagnosticInfoObservableAdapter.h"
#include "InspectionEventObservableAdapter.h"

#include "imageAnalysis/src/IasImage.h"
#include "imageAnalysis/src/ImageAnalysis.h"
#include "imageAnalysis/src/legacyDefinitions.h"
#include "imageAnalysis/src/Pad.h"
#include "imageAnalysis/src/Measurement.h"
#include "imageAnalysis/src/Indictment.h"
#include "imageAnalysis/src/TextDiagnosticInfoObservable.h"
#include "imageAnalysis/src/ImageDiagnosticInfoObservable.h"
#include "imageAnalysis/src/InspectionEventObservable.h"
#include "imageAnalysis/src/sharedAlgorithmCode/src/Short.h"

using namespace std;
using namespace boost;
using namespace ias;

#include "com_axi_v810_business_imageAnalysis_ImageAnalysis.h"

typedef struct
{
  int headerOffset;
  int sliceNum;
  int measurementNameId;
  int padId;
  float measurementValue;
  AlgTypeEnum algoType;
  int familyIndex;
  int legacyUnitsFlag;
}
MeasurementRecord;

const int MAX_NEIGHBORS = 4;

typedef struct
{
  int numJoints;
  int imageWidth;
  int imageHeight;
  int nanosPerPixel;
  int diagnosticsOn;  // 1 if diagnostics on, 0 otherwise
}
JointRecordHeader;

typedef struct
{
  int x;
  int y;
  int dx;
  int dy;
  int rotation;
  int shapeEnum;
  int numNeighbors;
  int neighborIds[MAX_NEIGHBORS];
  int padId;
  float pitch;
  float interPadDistance;
}
JointRecord;

/**
 * @author Peter Esbensen
 */
void getImageData(JNIEnv* env, jobject imageAnalysisObject, 
                  jclass inspectionInfoClass, 
                  jobject inspectionInfoObject, 
                  vector< float*>& imagesData)
{
  jmethodID getImageDataID = env->GetMethodID(inspectionInfoClass, "getImageData", "()[Ljava/nio/ByteBuffer;");
  sptAssert(getImageDataID != 0);
  jobjectArray imageDataArrayRef = static_cast<jobjectArray>( env->CallObjectMethod(inspectionInfoObject, getImageDataID) );
  checkForJavaException(env);

  jsize imageDataArraySize = env->GetArrayLength(imageDataArrayRef);

  for (int i=0; i < imageDataArraySize; ++i)
  {
    jobject javaImageByteBufferPtr = env->GetObjectArrayElement( imageDataArrayRef, i );
    imagesData.push_back(reinterpret_cast<float*>(env->GetDirectBufferAddress( javaImageByteBufferPtr)));
    checkForJavaException(env);
  }
}

/**
 * @author Peter Esbensen
 */
jobject createEmptyJointInspectionResultArray(JNIEnv* env, vector< int > const& padIDs)
{
  int numPads = padIDs.size();

  // create a new jobjectArray
  // get the JointInspectionResult class
  jclass jointInspectionResultClass = env->FindClass("com/axi/v810/business/testExec/JointInspectionResult");
  sptAssert(jointInspectionResultClass != 0);
  jmethodID constructorID = env->GetMethodID(jointInspectionResultClass, "<init>", "()V");
  sptAssert(constructorID != 0);

  jobjectArray jointInspectionResultArray = env->NewObjectArray(numPads, 
    jointInspectionResultClass,
    env->NewObject(jointInspectionResultClass, constructorID));
  checkForJavaException(env);
  sptAssert(jointInspectionResultArray != 0);

  // iterate through the array and put in a new JointInspectionResult
  // set the jointInspectionResult's id
   
  for (jsize i=0; i<numPads; ++i)
  {
    jobject jointInspectionResult = env->GetObjectArrayElement(jointInspectionResultArray, i);
    // todo:  make a new "nativeInspectionResult" class to facilitate this.  I don't want to create an actual Pad object (at least I don't think I do), 
    // so let's make a simplified class that just takes the pad id.
  }
  
  return jointInspectionResultArray;
}

/**
 * Adds a measurement name to the given vector.  If the measurement already exists in the vector, it does not add a new one.
 * Returns the index of the name in the vector.
 * This is a helper function for createJointInspectionResultArray
 * @author Peter Esbensen
 */
int addMeasurementName(string const& nameToAdd, vector< string >& nameVector)
{
  const int NOT_FOUND = -1;
  int index = NOT_FOUND;
  for (int i=0; i<nameVector.size(); ++i)
  {
    if (nameVector[i] == nameToAdd)
      index = i;
  }
  if (index == NOT_FOUND)
  {
    nameVector.push_back(nameToAdd);
    return (nameVector.size() - 1);
  }
  return index;
}


/**
 * @author Peter Esbensen
 */
map<int, int> createPadIdToJavaObjectArrayIndexMap(vector<int> const& padIDs)
{
  map<int, int> padIdToJavaObjectArrayIndexMap;
  for (int i=0;i<padIDs.size();++i)
  {
    padIdToJavaObjectArrayIndexMap.insert(pair<int,int>(padIDs[i], i));
  }
  return padIdToJavaObjectArrayIndexMap;
}

/**
 * @author Peter Esbensen
 */
JNIEXPORT jobject JNICALL Java_com_axi_v810_business_imageAnalysis_ImageAnalysis_extractMeasurements(JNIEnv * env, 
                                                                                                             jobject imageAnalysisObject,
                                                                                                             jobject inspectionInfo,
                                                                                                             jobject learnedShortDataBuffer)
{
  try
  {
    jclass nativeInspectionInfoClass = env->FindClass("com/axi/v810/business/imageAnalysis/NativeInspectionInfo");
    sptAssert(nativeInspectionInfoClass != 0);

    jmethodID getNativeDataID = env->GetMethodID(nativeInspectionInfoClass, "getNativeJointData", "()Ljava/nio/ByteBuffer;");
    sptAssert(getNativeDataID != 0);
    jobject nativeData = env->CallObjectMethod(inspectionInfo, getNativeDataID);
    checkForJavaException(env);
    JointRecordHeader* jointRecordHeader = static_cast<JointRecordHeader*>(env->GetDirectBufferAddress(nativeData)); 
    int numJoints = jointRecordHeader->numJoints;
    JointRecord* jointRecords = reinterpret_cast<JointRecord*>(jointRecordHeader + 1); 

    ShortLearnedData* shortLearnedData = static_cast<ShortLearnedData*>(env->GetDirectBufferAddress(learnedShortDataBuffer));

    vector< float* > imagesData;
    getImageData(env, imageAnalysisObject, nativeInspectionInfoClass, inspectionInfo, imagesData);

    //vector< int > padIDs = getPadIDs(env, nativeInspectionInfoClass, inspectionInfo);

    //map<int, int> padIdToJavaObjectArrayIndexMap = createPadIdToJavaObjectArrayIndexMap(padIDs);

    //vector< string > tunedSettingNames = getTunedSettingNames(env, nativeInspectionInfoClass, inspectionInfo);

    //vector< string > padNames = getPadNames(env, nativeInspectionInfoClass, inspectionInfo);

    std::vector< shared_ptr< IasImage > > iasImageSet;
    for (int sliceNumber=0; sliceNumber<imagesData.size(); ++sliceNumber)
    {
      shared_ptr<IasImage> sliceImage( new IasImage(imagesData[sliceNumber], jointRecordHeader->imageHeight, 
        jointRecordHeader->imageWidth, jointRecordHeader->nanosPerPixel, sliceNumber) );
      iasImageSet.push_back( sliceImage );
    }

    std::vector< shared_ptr< Pad > > iasPads;
    for (int i=0; i<numJoints; ++i)
    {
      JointRecord& jointRecord = jointRecords[i];
      CenterRegion padRegion;
      padRegion.shape = static_cast<RegionShape>(jointRecord.shapeEnum);
      padRegion.x = jointRecord.x; 
      padRegion.y = jointRecord.y; 
      padRegion.dx = jointRecord.dx; 
      padRegion.dy = jointRecord.dy; 

      shared_ptr<Pad> pad( new Pad(padRegion, jointRecord.rotation) ); // rotation[i]) );
      pad.get()->setId(jointRecord.padId); //padIDs[i]);
      //pad.get()->setName(padNames[i]);
      //pad.get()->setComponentName(componentNames[i]);

      pad.get()->setPitchInPixels(jointRecord.pitch); // pitchInPixels[i]);
      pad.get()->setInterPadDistanceInPixels(jointRecord.interPadDistance); //interPadDistanceInPixels[i]);

      pad.get()->setShortLearnedData(&(shortLearnedData[ jointRecord.padId ]));

      pad.get()->setAlgorithmFamilyName( "BGA2" );

      iasPads.push_back(pad);
    }
    ImageAnalysis ias(iasImageSet, iasPads);

    // set up Observables
    TextDiagnosticInfoObservableAdapter textObservableAdapter(env, TextDiagnosticInfoObservable::getInstance());
    ImageDiagnosticInfoObservableAdapter imageObservableAdapter(env, inspectionInfo, ImageDiagnosticInfoObservable::getInstance());
    ProfileDiagnosticInfoObservableAdapter profileObservableAdapter(env, inspectionInfo, ProfileDiagnosticInfoObservable::getInstance());
    InspectionEventObservableAdapter inspectionEventObservableAdapter(env, inspectionInfo, imageAnalysisObject, InspectionEventObservable::getInstance());

    vector< shared_ptr< Indictment > > indictments;
    vector< shared_ptr< Measurement > > measurements;

    ias.setDiagnostics( jointRecordHeader->diagnosticsOn );

    ias.extractMeasurements(indictments, measurements);

    /**
    * @todo PE: call the following stuff . . .
    *
    * ias.setThicknessTable();
    *     void setThicknessTable( boost::shared_ptr< ThicknessTable > thicknessTable,
    *     std::map< int, boost::shared_ptr<char> > padIdToDataOffsetMap);
    *
    * void setDiagnostics(bool state);
    *
    * void setAlgoSettings(std::vector< boost::shared_ptr<AlgoSetting> > algoSettings);
    */
    //transferMeasurements(env, nativeInspectionInfoClass, inspectionInfo, padIdToJavaObjectArrayIndexMap, measurements);
  }
  catch (jthrowable javaException)
  {
    env->Throw(javaException);
    return 0;
  }
  //return createJointInspectionResultArray(env, padIDs, measurements, indictments);
  return 0;
}

/**
 * @author Peter Esbensen
 */
JNIEXPORT void JNICALL Java_com_axi_v810_business_imageAnalysis_ImageAnalysis_writeLegacyConfigurationFiles(JNIEnv* env, jobject obj)
{
  ImageAnalysis ias;
  ias.writeLegacyConfigurationFiles();
}
