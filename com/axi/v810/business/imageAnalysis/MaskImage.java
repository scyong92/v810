package com.axi.v810.business.imageAnalysis;

import java.util.*;
import ij.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.ImageSetData;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * @author Administrator
 */
public class MaskImage 
{
  private static final String _MASK_IMAGE_ROTATION_SEPARATOR = FileName.getMaskImageRotationSeparator();
  
  private static MaskImage _createMaskImage = null;
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  private ReconstructedImagesProducer _reconstructedImagesProducer;
  private Map<String, java.awt.image.BufferedImage> _subtypeNameToMaskImageMap = new LinkedHashMap<String, java.awt.image.BufferedImage>();
  private Map<String, java.awt.image.BufferedImage> _subtypeNameToCropMaskImageMap = new LinkedHashMap<String, java.awt.image.BufferedImage>();
  private Map<String, java.awt.image.BufferedImage> _subtypeNameToMisalignedMaskImageMap = new LinkedHashMap<String, java.awt.image.BufferedImage>();
  private Map<String, ImageCoordinate> _subtypeNameToMaskImageXCoordinateMap = new LinkedHashMap<String, ImageCoordinate>();//match location x
  private Map<String, ImageCoordinate> _subtypeNameToMaskImageYCoordinateMap = new LinkedHashMap<String, ImageCoordinate>(); //match location y
  private Map<String, Double> _subtypeNameToMaskImageOriginalRotationMap = new LinkedHashMap<String, Double>(); //Siew Yeng - XCR-2843
  //Siew Yeng - XCR-3545 - this map is use to store original mask image offset location 
  private Map<String, ImageCoordinate> _subtypeNameToMaskImageOffsetMap = new LinkedHashMap<String, ImageCoordinate>();
 
  /**
   * @author Jack Hwee
   */
  public static synchronized MaskImage getInstance()
  {
    if (_createMaskImage == null)
    {
      _createMaskImage = new MaskImage();
    }
    return _createMaskImage;
  }
    
  /**
   * @author Jack Hwee
   */
  private MaskImage()
  {
    // do nothing
  }
  
  /**
   *  @author Jack Hwee
   */
  public void maskImage(Project project,
                        Subtype subtype,
                        List<ImageSetData> imageSetDataList) throws XrayTesterException
  {
    maskImage(project, null, subtype, imageSetDataList);
  }
  
  /**
   * @author Jack Hwee
   * @edited by Siew Yeng - added parameter parentSubtype
   */
  public void maskImage(Project project,
                        Subtype parentSubtype,
                        Subtype subtype,
                        List<ImageSetData> imageSetDataList) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);

    TestProgram testProgram = project.getTestProgram();

    // Make sure that we have at least one image run
    Assert.expect(imageSetDataList.size() > 0);

    // Set filter to look for subtype--this will be used in ManagedOfflineImageSet
    testProgram.clearFilters();
    testProgram.addFilter(true);
    if(parentSubtype == null)
      testProgram.addFilter(subtype);
    else
      testProgram.addFilter(parentSubtype);

    // clear out all the measurements, etc.
    testProgram.startNewInspectionRun();

    maskSubtypeImage(subtype, imageSetDataList.get(0), testProgram);
  }
  
  /**
   *  @author Jack Hwee 
   */
  private void maskSubtypeImage(Subtype subtype, ImageSetData imageSetData, TestProgram testProgram) throws XrayTesterException
  {
    BooleanRef bool = new BooleanRef();

    _imageAcquisitionEngine.initialize();

    _reconstructedImagesProducer = _imageAcquisitionEngine.getReconstructedImagesProducer();

    _reconstructedImagesProducer.acquireOfflineProductionImages(testProgram, imageSetData);
      
    final ReconstructedImages reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(bool);

    if (bool.getValue() == false)
    {    
      ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(SliceNameEnum.PAD);
      com.axi.util.image.Image sliceImage = slice.getOrthogonalImage();  

      ImagePlus sliceImagePlus = new ImagePlus("", sliceImage.getBufferedImage());

      MaskImageConverter convertToMask = new MaskImageConverter();

      convertToMask.applyThreshold(sliceImagePlus);

      if(convertToMask.isInverted() == false)
        convertToMask.setInvertedLut(sliceImagePlus);

      double rotation = reconstructedImages.getReconstructionRegion().getComponent().getDegreesRotationAfterAllRotations();
      com.axi.util.image.Image binaryImage = com.axi.util.image.Image.createFloatImageFromBufferedImage(sliceImagePlus.getBufferedImage());
      
      //String maskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + subtype.getShortName() + ".png";
      //Siew Yeng - XCR-2843 - masking rotation
      String maskImagePath = FileName.getMaskImageWithRotationFullPath(subtype.getPanel().getProject().getName(), subtype.getShortName(), rotation);
      try 
      {
        ImageIoUtil.saveImage(binaryImage, maskImagePath);
      }
      catch (CouldNotCreateFileException ex) 
      {
        CouldNotCreateFileException dex = new CouldNotCreateFileException(maskImagePath);
        dex.initCause(ex);
        dex.printStackTrace();
      }

      sliceImagePlus.close();
      sliceImagePlus.flush();

      binaryImage.decrementReferenceCount();
      sliceImage.decrementReferenceCount();        
    }
  }
  
  /**
   * @author Jack Hwee
   * @author Siew Yeng - added parameter rotation
   */
  public void addSubtypeMaskImage(String subtypeLongName, double rotation, java.awt.image.BufferedImage maskImage) 
  {     
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageMap != null);
    
    String mapKey = subtypeLongName + _MASK_IMAGE_ROTATION_SEPARATOR + rotation;
    _subtypeNameToMaskImageMap.put(mapKey, maskImage);
  }
  
  /**
   * @author Jack Hwee
   * @author Siew Yeng - added parameter rotation
   */
  public void addCropMaskSubtypeImage(String subtypeLongName, double rotation, java.awt.image.BufferedImage maskImage) 
  {
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToCropMaskImageMap != null);
    
    String mapKey = subtypeLongName + _MASK_IMAGE_ROTATION_SEPARATOR + rotation;
    _subtypeNameToCropMaskImageMap.put(mapKey, maskImage);
  }
  
  /**
   * @author Jack Hwee
   * @author Siew Yeng - added parameter rotation
   */
  public java.awt.image.BufferedImage getCropMaskImage(String subtypeLongName, double rotation)
  {     
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToCropMaskImageMap != null);

    String mapKey = subtypeLongName + _MASK_IMAGE_ROTATION_SEPARATOR + rotation;
    java.awt.image.BufferedImage maskImage = _subtypeNameToCropMaskImageMap.get(mapKey);

    return maskImage;
  }
    
  /**
   * @author Jack Hwee
   * @edited by Siew Yeng
   */
  public void addMisalignedMaskSubtypeImage(JointInspectionData jointInspectionData, String subtypeLongName, java.awt.image.BufferedImage maskImage) 
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMisalignedMaskImageMap != null);
    
    String mapKey = jointInspectionData.toString() + "_" + subtypeLongName;
    _subtypeNameToMisalignedMaskImageMap.put(mapKey, maskImage);
  }
    
  /**
   * @author Jack Hwee
   * @author Siew Yeng - added parameter rotation
   */
  public java.awt.image.BufferedImage getSubtypeMaskImage(String subtypeLongName, double rotation)
  {
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageMap != null);

    String mapKey = subtypeLongName + _MASK_IMAGE_ROTATION_SEPARATOR + rotation;
    java.awt.image.BufferedImage maskImage = _subtypeNameToMaskImageMap.get(mapKey);

    return maskImage;
  }
    
  /**
   * @author Jack Hwee
   * @author Siew Yeng
   */
  public java.awt.image.BufferedImage getMisalignedSubtypeMaskImage(JointInspectionData jointInspectionData, String subtypeLongName)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMisalignedMaskImageMap != null);

    String mapKey = jointInspectionData.toString() + "_" + subtypeLongName;
    java.awt.image.BufferedImage maskImage = _subtypeNameToMisalignedMaskImageMap.get(mapKey);

    return maskImage;
  }
    
  /**
   * @author Jack Hwee
   */
  public void removeSubtypeMaskImage(String subtypeLongName)
  {
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageMap != null);

    if(_subtypeNameToMaskImageMap.containsKey(subtypeLongName))
      _subtypeNameToMaskImageMap.remove(subtypeLongName);
  }
    
  /**
   * @author Jack Hwee
   * @edited by Siew Yeng
   */
  public void addMaskImageXCoordinate(JointInspectionData jointInspectionData, String subtypeLongName, ImageCoordinate imageCoordinate) 
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageXCoordinateMap != null);

    String mapKey = jointInspectionData.toString() + "_" + subtypeLongName;
    _subtypeNameToMaskImageXCoordinateMap.put(mapKey, imageCoordinate);
  }

  /**
   * @author Jack Hwee
   * @edited by Siew Yeng
   */
  public ImageCoordinate getSubtypeMaskImageXCoordinate(JointInspectionData jointInspectionData, String subtypeLongName)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageXCoordinateMap != null);

    String mapKey = jointInspectionData.toString() + "_" + subtypeLongName;
    ImageCoordinate imageCoordinate = _subtypeNameToMaskImageXCoordinateMap.get(mapKey);

    return imageCoordinate;
  }
    
  /**
   * @author Jack Hwee
   * @edited by Siew Yeng
   */
  public void addMaskImageYCoordinate(JointInspectionData jointInspectionData, String subtypeLongName, ImageCoordinate imageCoordinate) 
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageYCoordinateMap != null);

    String mapKey = jointInspectionData.toString() + "_" + subtypeLongName;
    _subtypeNameToMaskImageYCoordinateMap.put(mapKey, imageCoordinate);
  }

  /**
   * @author Jack Hwee
   * @edited by Siew Yeng
   */
  public ImageCoordinate getSubtypeMaskImageYCoordinate(JointInspectionData jointInspectionData, String subtypeLongName)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageYCoordinateMap != null);

    String mapKey = jointInspectionData.toString() + "_" + subtypeLongName;
    ImageCoordinate imageCoordinate = _subtypeNameToMaskImageYCoordinateMap.get(mapKey);

    return imageCoordinate;
  }
    
  /**
   * @author Jack Hwee
   */
  public void clearMaskImageMap()
  {
    if (_subtypeNameToMaskImageYCoordinateMap != null)
      _subtypeNameToMaskImageYCoordinateMap.clear();

    if (_subtypeNameToMaskImageXCoordinateMap != null)
      _subtypeNameToMaskImageXCoordinateMap.clear();

    if (_subtypeNameToCropMaskImageMap != null)
      _subtypeNameToCropMaskImageMap.clear();

    if (_subtypeNameToMisalignedMaskImageMap != null)
      _subtypeNameToMisalignedMaskImageMap.clear();

    if (_subtypeNameToMaskImageMap != null)
      _subtypeNameToMaskImageMap.clear();
    
    //Siew Yeng - XCR-2843 - masking rotation
    if (_subtypeNameToMaskImageOriginalRotationMap != null)
      _subtypeNameToMaskImageOriginalRotationMap.clear();
    
    //Siew Yeng - XCR-3545 - masking rotation locator offset
    if(_subtypeNameToMaskImageOffsetMap != null)
      _subtypeNameToMaskImageOffsetMap.clear();
    
  }

  /**
   * @author Jack Hwee
   */
  public void removeCropSubtypeMaskImage(String subtypeLongName)
  {
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToCropMaskImageMap != null);

    if (_subtypeNameToCropMaskImageMap.containsKey(subtypeLongName))
      _subtypeNameToCropMaskImageMap.remove(subtypeLongName);
  }
  
  /**
   * @author Siew Yeng 
   */
  public void setSubtypeMaskImageOriginalRotation(String subtypeLongName, double rotation)
  {
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageOriginalRotationMap != null);

    _subtypeNameToMaskImageOriginalRotationMap.put(subtypeLongName, rotation);
  }
  
  /**
   * @author Siew Yeng 
   */
  public double getSubtypeMaskImageOriginalRotation(String subtypeLongName)
  {
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageOriginalRotationMap != null);

    return _subtypeNameToMaskImageOriginalRotationMap.get(subtypeLongName);
  }
  
  /**
   * Move this offset value function from Locator
   * @author Siew Yeng
   */
  public void setOffsetValueForMask(String subtypeLongName, double rotation, int x, int y) 
  {
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageOffsetMap != null);
    
    String mapKey = subtypeLongName + _MASK_IMAGE_ROTATION_SEPARATOR + rotation;
    _subtypeNameToMaskImageOffsetMap.put(mapKey, new ImageCoordinate(x,y));
  }
  
  /**
   * Move this offset value function from Locator
   * @author Siew Yeng - added parameter rotation
   */
  public ImageCoordinate getOffsetValueForMask(String subtypeLongName, double rotation)
  {     
    Assert.expect(subtypeLongName != null);
    Assert.expect(_subtypeNameToMaskImageOffsetMap != null);

    String mapKey = subtypeLongName + _MASK_IMAGE_ROTATION_SEPARATOR + rotation;
    return _subtypeNameToMaskImageOffsetMap.get(mapKey);
  }
}