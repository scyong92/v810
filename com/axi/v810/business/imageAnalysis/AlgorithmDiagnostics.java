package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;

/**
 * Helper class that the algorithms can use to help post diagnostic messages.
 *
 * @author Matt Wharton
 */
public class AlgorithmDiagnostics
{
  private static AlgorithmDiagnostics _instance = null;

  private InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  private AlgorithmDiagnostics()
  {
    // Do nothing...
  }

  /**
   * Gets a singleton instance of AlgorithmDiagnostics.
   *
   * @author Matt Wharton
   */
  public static synchronized AlgorithmDiagnostics getInstance()
  {
    if (_instance == null)
    {
      _instance = new AlgorithmDiagnostics();
    }

    return _instance;
  }

  /**
   * Posts a diagnostic message to the ImageDiagnosticMessageObservable.
   *
   * sliceNameEnum may be null.
   *
   * @author Patrick Lacz
   */
  public void postDiagnostics(ReconstructionRegion inspectionRegion,
                              SliceNameEnum sliceNameEnum,
                              JointInspectionData jointInspectionData,
                              Algorithm algorithm,
                              boolean invalidateDiagnostics,
                              DiagnosticInfo... diagnosticInfoObjects)
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(algorithm != null);
    Assert.expect(jointInspectionData != null);

    // Only post diagnostics if they are turned on.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getSubtype().getJointTypeEnum(), algorithm))
    {
      // Create a message
      AlgorithmDiagnosticMessage message = new AlgorithmDiagnosticMessage(
          inspectionRegion,
          algorithm,
          invalidateDiagnostics,
          diagnosticInfoObjects);

      if (sliceNameEnum != null)
        message.setSliceNameEnum(sliceNameEnum);

      if (jointInspectionData != null)
        message.setJointInspectionData(jointInspectionData);

      // Post it.
      Assert.expect(_inspectionEventObservable != null);
      InspectionEvent algorithmDiagnosticEvent = new AlgorithmDiagnosticInspectionEvent(message,
                                                                                        InspectionEventEnum.POSTING_DIAGNOSTIC);
      _inspectionEventObservable.sendEventInfo(algorithmDiagnosticEvent);
    }
    else
    {
      // get rid of any references kept around.
      for (DiagnosticInfo diagnosticInfo : diagnosticInfoObjects)
      {
        diagnosticInfo.decrementReferenceCount();
      }
    }
  }

  /**
   * Posts a diagnostic message to the ImageDiagnosticMessageObservable.
   * This version does not require a joint; a subtype may be passed instead.
   *
   * sliceNameEnum may be null.
   *
   * @author Patrick Lacz
   */
  public void postDiagnostics(ReconstructionRegion inspectionRegion,
                              SliceNameEnum sliceNameEnum,
                              Subtype subtype,
                              Algorithm algorithm,
                              boolean invalidateDiagnostics,
                              boolean requestPause,
                              DiagnosticInfo... diagnosticInfoObjects)
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(algorithm != null);
    Assert.expect(subtype != null);

    // Only post diagnostics if they are turned on.
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), algorithm))
    {
      // Create a message
      AlgorithmDiagnosticMessage message = new AlgorithmDiagnosticMessage(
          inspectionRegion,
          algorithm,
          invalidateDiagnostics,
          diagnosticInfoObjects);

      message.setPauseRequested(requestPause);

      if (sliceNameEnum != null)
        message.setSliceNameEnum(sliceNameEnum);

      message.setSubtype(subtype);

      // if there is only one joint in the inspection region, go ahead and report that as the joint we're working on
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getJointInspectionDataList(subtype);
      if (jointInspectionDataObjects.size() == 1)
      {
        message.setJointInspectionData(jointInspectionDataObjects.get(0));
      }

      // Post it.
      Assert.expect(_inspectionEventObservable != null);
      InspectionEvent algorithmDiagnosticEvent = new AlgorithmDiagnosticInspectionEvent(message,
                                                                                        InspectionEventEnum.POSTING_DIAGNOSTIC);
      _inspectionEventObservable.sendEventInfo(algorithmDiagnosticEvent);
    }
    else
    {
      // get rid of any references kept around.
      for (DiagnosticInfo diagnosticInfo : diagnosticInfoObjects)
      {
        diagnosticInfo.decrementReferenceCount();
      }
    }
  }

  /**
   * Posts an event indicating that different DiagnosticInfos will be coming up, but doesn't invalidate the current ones.
   * This is useful, for example, to guide clearing the diagnostics legend without removing the existing graphics.
   *
   * @author Matt Wharton
   */
  public void resetDiagnosticInfosWithoutInvalidating(ReconstructionRegion inspectionRegion,
                                                      Subtype subtype,
                                                      Algorithm algorithm)
  {
    Assert.expect(inspectionRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(algorithm != null);

    // Only post if diagnostics are enabled.
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), algorithm))
    {
      // Create a message.
      AlgorithmDiagnosticMessage message = new AlgorithmDiagnosticMessage(inspectionRegion,
                                                                          algorithm,
                                                                          false,
                                                                          new DiagnosticInfo[0]);

      message.setSubtype(subtype);
      message.setDiagnosticInfosReset(true);

      // Post it.
      Assert.expect(_inspectionEventObservable != null);
      InspectionEvent algorithmDiagnosticEvent = new AlgorithmDiagnosticInspectionEvent(message,
                                                                                        InspectionEventEnum.POSTING_DIAGNOSTIC);
      _inspectionEventObservable.sendEventInfo(algorithmDiagnosticEvent);
    }
  }
}
