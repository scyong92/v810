package com.axi.v810.business.imageAnalysis;

/**
 * Enum which is used to identify what type of algorithm setting this is.
 * Possible values are things like STANDARD, ADDITIONAL, and HIDDEN.
 *
 * The algorithm tuner uses thie enum value to help decide how to display the
 * various settings.
 *
 * @author Matt Wharton
 */
public class AlgorithmSettingTypeEnum extends com.axi.util.Enum
{
  private static int _index = 0;

  public static AlgorithmSettingTypeEnum STANDARD = new AlgorithmSettingTypeEnum(++_index);
  public static AlgorithmSettingTypeEnum ADDITIONAL = new AlgorithmSettingTypeEnum(++_index);
  public static AlgorithmSettingTypeEnum HIDDEN = new AlgorithmSettingTypeEnum(++_index);
  public static AlgorithmSettingTypeEnum SLICE_HEIGHT = new AlgorithmSettingTypeEnum(++_index);
  public static AlgorithmSettingTypeEnum SLICE_HEIGHT_ADDITIONAL = new AlgorithmSettingTypeEnum(++_index);

  /**
   * @author Matt Wharton
   */
  private AlgorithmSettingTypeEnum(int id)
  {
    super(id);
  }
}
