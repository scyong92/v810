package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;

/**
 * This is basically a wrapper for a MeasurementRegionEnum[], including a set method that prevents existing
 * MeasurementRegionEnums from being overwritten.  This is useful if you'd like to make sure that all the MeasurementRegionEnums
 * show up for the user and are not obscuring each other.
 *
 * @author Peter Esbensen
 */
public class MeasurementRegionEnumArrayManager
{
  private MeasurementRegionEnum[] _array;
  private MeasurementRegionEnum[] _backgroundArray;

  public MeasurementRegionEnum[] getFinalArray()
  {
    Assert.expect(_backgroundArray != null);

    if (_array == null)
    {
      // nobody has added any MeasurementRegionEnums other than the background ones, so just return the backgroundArray
      return _backgroundArray;
    }

    Assert.expect(_array.length == _backgroundArray.length);

    MeasurementRegionEnum[] finalArray = new MeasurementRegionEnum[ _array.length ];

    // copy in the background enums
    System.arraycopy(_backgroundArray, 0, finalArray, 0, _backgroundArray.length);

    // now overlay with the foreground enums
    for (int i = 0; i < _array.length; ++i)
    {
      if (_array[i] != null)
      {
        finalArray[i] = _array[i];
      }
    }
    return finalArray;
  }

  /**
   * @author Peter Esbensen
   */
  public void setBackgroundArray(MeasurementRegionEnum[] backgroundArray)
  {
    Assert.expect(backgroundArray != null);
    _backgroundArray = new MeasurementRegionEnum[ backgroundArray.length ];
    System.arraycopy(backgroundArray, 0, _backgroundArray, 0, backgroundArray.length);
  }

  /**
   * Put the given measurementRegionEnum at the specified index in the array.  If that location is already filled, put
   * it in the nearest open index, if one exists.
   *
   * @author Peter Esbensen
   */
  public void addMeasurementRegionEnumWithoutHidingExistingEnums(int index, MeasurementRegionEnum measurementRegionEnum)
  {
    Assert.expect(measurementRegionEnum != null);

    createArrayIfNecessary();

    Assert.expect(index >= 0);
    Assert.expect(index < _array.length);

    if (_array[index] == null)
    {
      _array[index] = measurementRegionEnum;
    }
    else // there is something in the way
    {
      // find the nearest open slot
      boolean doneInserting = false;
      int searchDistance = 1;
      boolean canSearchToLeft = true;
      boolean canSearchToRight = true;

      while (( doneInserting == false ) && ( canSearchToLeft || canSearchToRight ))
      {
        if (canSearchToLeft)
        {
          int indexToLeft = index - searchDistance;
          if (indexToLeft >= 0)
          {
            if (_array[indexToLeft] == null)
            {
              _array[indexToLeft] = measurementRegionEnum;
              doneInserting = true;
            }
          }
          else
          {
            canSearchToLeft = false;
          }
        }
        if ((doneInserting == false) && canSearchToRight)
        {
          int indexToRight = index + searchDistance;
          if (indexToRight < _array.length)
          {
            if (_array[indexToRight] == null)
            {
              _array[indexToRight] = measurementRegionEnum;
              doneInserting = true;
            }
          }
          else
          {
            canSearchToRight = false;
          }
        }
        ++searchDistance;
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void createArrayIfNecessary()
  {
    Assert.expect(_backgroundArray != null, "call setBackgroundArray first");
    if (_array == null)
    {
      _array = new MeasurementRegionEnum[ _backgroundArray.length ];
    }
  }

}
