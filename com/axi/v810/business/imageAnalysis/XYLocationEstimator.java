package com.axi.v810.business.imageAnalysis;

import java.awt.geom.*;
import java.util.*;

import Jama.*;
import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * This is supposed to build up a database of expected and actual xy locations and learn the affine transform map
 * between the two.
 *
 * The idea would be to create a super accurate alignment matrix that could be used instead of the regular one.
 *
 * Right now, I don't have this working.  Doing an SVD seems too slow so my next attempt was to maybe
 * try an incremental gradient descent approach.
 *
 * @author Peter Esbensen
 */
public class XYLocationEstimator
{
  private static XYLocationEstimator _instance;
  private static Map<Point2D, Point2D> _expectedToActualPointMapForX = new HashMap<Point2D,Point2D>();
  private static Map<Point2D, Point2D> _expectedToActualPointMapForY = new HashMap<Point2D,Point2D>();

  /**
   * @author Peter Esbensen
   */
  public synchronized static XYLocationEstimator getInstance()
  {
    if (_instance == null)
      _instance = new XYLocationEstimator();

    return _instance;
  }

  /**
   * @author Peter Esbensen
   */
  public void resetForNewInspection()
  {
    _expectedToActualPointMapForX.clear();
    _expectedToActualPointMapForY.clear();
  }

  /**
   * Right now all this method is basically doing is logging the xyz data.  The code for adding the data to the model
   * is commented out since the model is not coded up at this point.
   *
   * @author Peter Esbensen
   */
  public synchronized void addDataPoint(List<JointInspectionData> jointInspectionDataList,
                                        ReconstructedImages reconstructedImages,
                                        int zHeight)
  {
    boolean doLogging = true;
    boolean doAlignmentCheck = false;
    Matrix xAlignmentMatrix = null;
    Matrix yAlignmentMatrix = null;

    /**
     * @todo add this kind of thing back in once I have a good model computation scheme
    if ((_expectedToActualPointMapForX.size() > 2) && (_expectedToActualPointMapForY.size() > 2))
    {
      xAlignmentMatrix = computeCurrentAlignmentMatrix(_expectedToActualPointMapForX);
      yAlignmentMatrix = computeCurrentAlignmentMatrix(_expectedToActualPointMapForY);
      doAlignmentCheck = true;
    }
    */

    // iterate through each joint in the region and log the xyz data for debugging purposes
    for (JointInspectionData jointInspectionData : jointInspectionDataList)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      Collection<JointMeasurement> jointMeasurements = jointInspectionResult.getAllMeasurements();
      ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
      Collection<ComponentMeasurement> componentMeasurements = componentInspectionData.getComponentInspectionResult().getAllMeasurements();
      float locatorPixelX = 0;
      float locatorPixelY = 0;
      float expectedPixelX = 0;
      float expectedPixelY = 0;
      java.awt.Shape panelShape = null;

      for (JointMeasurement jointMeasurement : jointMeasurements)
      {
        if (jointMeasurement.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_X_LOCATION))
        {
          locatorPixelX = jointMeasurement.getValue();
          expectedPixelX = jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getCenterX();
          panelShape = jointInspectionData.getPad().getShapeRelativeToPanelInNanoMeters();
        }
        if (jointMeasurement.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_Y_LOCATION))
        {
          locatorPixelY = jointMeasurement.getValue();
          expectedPixelY = jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getCenterY();
          panelShape = jointInspectionData.getPad().getShapeRelativeToPanelInNanoMeters();
        }
      }

      for (ComponentMeasurement componentMeasurement : componentMeasurements)
      {
        Component component = componentInspectionData.getComponent();
        if (componentMeasurement.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_X_LOCATION))
        {
          locatorPixelX = componentMeasurement.getValue();
          expectedPixelX = componentInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(true).getCenterX();
          panelShape = component.getShapeSurroundingPadsRelativeToPanelInNanoMeters();
        }
        if (componentMeasurement.getMeasurementEnum().equals(MeasurementEnum.LOCATOR_Y_LOCATION))
        {
          locatorPixelY = componentMeasurement.getValue();
          expectedPixelY = jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels().getCenterY();
          panelShape = component.getShapeSurroundingPadsRelativeToPanelInNanoMeters();
        }
      }

      PanelRectangle panelRectangle = new PanelRectangle(panelShape);
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      SystemFiducialRectangle systemFiducialRectangle = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(panelRectangle,
                                                                                                                             reconstructionRegion.getManualAlignmentMatrix());

      float locatorXOffsetInPixels = locatorPixelX - expectedPixelX;
      float locatorYOffsetInPixels = locatorPixelY - expectedPixelY;
      int locatorYOffsetInNanos = Math.round(locatorYOffsetInPixels * MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel());
      int locatorXOffsetInNanos = Math.round(locatorXOffsetInPixels * MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel());

      systemFiducialRectangle.setMinXY( systemFiducialRectangle.getMinX() + locatorXOffsetInNanos,
                                        systemFiducialRectangle.getMinY() + locatorYOffsetInNanos );

      int jointFailed = 0;  // 0 = did not fail, 1 = failed
      if ((jointInspectionResult.passed() == false) ||
          (componentInspectionData.getComponentInspectionResult().passed() == false))
        jointFailed = 1;

      if (doLogging)
      {
        System.out.println("regionId joint expected actual locatorError zHeight jointFailed " + reconstructionRegion.getRegionId() + " " +
                           jointInspectionDataList.get(0).getFullyQualifiedPadName() + " " +
                           panelRectangle.getCenterX() + " " + panelRectangle.getCenterY() + " "  +
                           systemFiducialRectangle.getCenterX() + " " + systemFiducialRectangle.getCenterY() + " " +
                           locatorXOffsetInNanos + " " + locatorYOffsetInNanos + " " +
                           zHeight + " " + jointFailed);
      }


      /** @todo add this kind of thing back in once we have a working model

      Map<Point2D, Point2D> pointMap = null;
      if (systemFiducialRectangle.getWidth() < systemFiducialRectangle.getHeight())
      {
        pointMap = _expectedToActualPointMapForX;
      }
      else
      {
        pointMap = _expectedToActualPointMapForY;
      }
      pointMap.put(new Point2D.Double(panelRectangle.getCenterX(), panelRectangle.getCenterY()),
                   new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()));

      if ((doAlignmentCheck) && (xAlignmentMatrix != null) && (yAlignmentMatrix != null))
      {
        Assert.expect(xAlignmentMatrix != null);
        Assert.expect(yAlignmentMatrix != null);
        double[][] point = new double[1][3];
        point[0][0] = panelRectangle.getCenterX();
        point[0][1] = panelRectangle.getCenterY();
        point[0][2] = 1.0;
        Matrix pointMatrix = new Matrix(point);
        Matrix predictedXPointMatrix = pointMatrix.times(xAlignmentMatrix);
        Matrix predictedYPointMatrix = pointMatrix.times(yAlignmentMatrix);

        long predictedPointX = Math.round(predictedXPointMatrix.get(0, 0));
        long predictedPointY = Math.round(predictedYPointMatrix.get(0, 1));
//        System.out.println("predicted = " + predictedPointX + " " + predictedPointY);

        long xErrorMils = Math.round((systemFiducialRectangle.getCenterX() - predictedPointX) / 25400.0);
        long yErrorMils = Math.round((systemFiducialRectangle.getCenterY() - predictedPointY) / 25400.0);
//        System.out.println(xErrorMils + " " + yErrorMils);

        System.out.println("joint expected actual locatorError zHeight predicted xyError failed " + jointInspectionDataList.get(0).getFullyQualifiedPadName() + " " +
                           panelRectangle.getCenterX() + " " + panelRectangle.getCenterY() + " "  +
                           systemFiducialRectangle.getCenterX() + " " + systemFiducialRectangle.getCenterY() + " " +
                           locatorXOffsetInNanos + " " + locatorYOffsetInNanos + " " +
                           zHeight + " " + predictedPointX + " " + predictedPointY + " " +
                           xErrorMils + " " + yErrorMils + " " + jointFailed);



        @todo compare to "expected CAD" location to see how far off alignment was
      }
*/
    }

  }

}
