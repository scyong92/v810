package com.axi.v810.business.imageAnalysis.quadFlatNoLead;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * Quad flat pack no-lead (QFN) measurement algorithm.
 *
 * @author George Booth
 */
public class QuadFlatNoLeadMeasurementAlgorithm extends Algorithm
{
  protected final int _MAX_NUMBER_OF_REGIONS_FOR_LEARNING = 100;
  private final float _HEEL_PERCENT_OF_FILLET = 0.20F;
  private final float _CENTER_PERCENT_OF_FILLET = 0.30F;

  // AlgorithmDiagnostics instance.
  private static AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();

//  private int _smoothingKernelLength = 6;

  private static final int _UNUSED_BIN = -1;

  private static final boolean _logAlgorithmProfileData =
      Config.getInstance().getBooleanValue(SoftwareConfigEnum.LOG_ALGORITHM_PROFILE_DATA);
  
  //private static final String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";

  /**
   * Helper class for learning. Profiles are created for all joints being learned and are saved in a map for use later.
   *
   * @author George Booth
   */
  private static class QFNProfileDataForLearning
  {
    private float[] _smoothedProfileWithVoidComp;
    RegionOfInterest _smoothedProfileWithVoidCompRoi; //Siew Yeng
    private float[] _smoothedFirstDerivativeProfile;
    private float[] _smoothedSecondDerivativeProfile;
    private float[] _curvatureProfile;
    
    //Siew Yeng - XCR-2139
    private int _heelBin;
    private int _centerBin;

    /**
     * @author George Booth
     */
    private QFNProfileDataForLearning(float[] smoothedProfileWithVoidComp,
                                     RegionOfInterest smoothedProfileWithVoidCompRoi,
                                     float[] smoothedFirstDerivativeProfile,
                                     float[] smoothedSecondDerivativeProfile,
                                     float[] curvatureProfile)
    {
      Assert.expect(smoothedProfileWithVoidComp != null);
      Assert.expect(smoothedFirstDerivativeProfile != null);
      Assert.expect(smoothedFirstDerivativeProfile.length == smoothedProfileWithVoidComp.length);
      Assert.expect(smoothedSecondDerivativeProfile != null);
      Assert.expect(smoothedSecondDerivativeProfile.length == smoothedProfileWithVoidComp.length);
      Assert.expect(curvatureProfile != null);
      Assert.expect(curvatureProfile.length == smoothedProfileWithVoidComp.length);

      _smoothedProfileWithVoidComp = smoothedProfileWithVoidComp;
      _smoothedProfileWithVoidCompRoi = smoothedProfileWithVoidCompRoi;
      _smoothedFirstDerivativeProfile = smoothedFirstDerivativeProfile;
      _smoothedSecondDerivativeProfile = smoothedSecondDerivativeProfile;
      _curvatureProfile = curvatureProfile;
    }

    /**
     * @author George Booth
     */
    private float[] getSmoothedProfileWithVoidComp()
    {
      Assert.expect(_smoothedProfileWithVoidComp != null);

      return _smoothedProfileWithVoidComp;
    }
    
    /**
     * @author Siew Yeng
     */
    private RegionOfInterest getSmoothedProfileWithVoidCompRoi()
    {
      Assert.expect(_smoothedProfileWithVoidCompRoi != null);

      return _smoothedProfileWithVoidCompRoi;
    }
    
    /**
     * @author George Booth
     */
    private float[] getSmoothedFirstDerivativeProfile()
    {
      Assert.expect(_smoothedFirstDerivativeProfile != null);

      return _smoothedFirstDerivativeProfile;
    }

    /**
     * @author George Booth
     */
    private float[] getSmoothedSecondDerivativeProfile()
    {
      Assert.expect(_smoothedSecondDerivativeProfile != null);

      return _smoothedSecondDerivativeProfile;
    }

    /**
     * @author George Booth
     */
    private float[] getCurvatureProfile()
    {
      Assert.expect(_curvatureProfile != null);

      return _curvatureProfile;
    }
    
    /**
     * @author Siew Yeng
     */
    private void setHeelBin(int heelBin)
    {
      _heelBin = heelBin;
    }
    
    /**
     * @author Siew Yeng
     */
    private void setCenterBin(int centerBin)
    {
      _centerBin = centerBin;
    }
    
    /**
     * @author Siew Yeng
     */
    private int getHeelBin()
    {
      return _heelBin;
    }
    
    /**
     * @author Siew Yeng
     */
    private int getCenterBin()
    {
      return _centerBin;
    }
  }

  /**
   * @author George Booth
   */
  public QuadFlatNoLeadMeasurementAlgorithm(InspectionFamily quadFlatNoLeadInspectionFamily)
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.QUAD_FLAT_NO_LEAD);

    Assert.expect(quadFlatNoLeadInspectionFamily != null);

    // Add the algorithm settings.
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    // Standard thresholds

    // Heel peak search distance from heel edge.
    AlgorithmSetting heelSearchDistanceFromEdgeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE,
        displayOrder++,
        20.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_EDGE_DISTANCE,
        "HTML_DESC_QFN_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(heelSearchDistanceFromEdgeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE);

    // Center offset from heel peak (percent of pin length).
    AlgorithmSetting centerOffsetFromHeelSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET,
        displayOrder++,
        45.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_EDGE_DISTANCE,
        "HTML_DESC_QFN_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(centerOffsetFromHeelSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET);

    // Distance from heel to toe as a percent of pad length.
    AlgorithmSetting toeOffsetFromHeelSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET,
      displayOrder++,
      75.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_EDGE_DISTANCE,
      "HTML_DESC_QFN_MEASUREMENT_(TOE_OFFSET_FROM_HEEL)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(TOE_OFFSET_FROM_HEEL)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(TOE_OFFSET_FROM_HEEL)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(toeOffsetFromHeelSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET);

    // Upward curve start distance from heel edge.
    AlgorithmSetting upwardCurvatureStartDistanceFromHeelEdgeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE,
        displayOrder++,
        40.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_EDGE_DISTANCE,
        "HTML_DESC_QFN_MEASUREMENT_(UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion,
        AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE,
        AlgorithmSettingComparatorEnum.LESS_THAN);
    addAlgorithmSetting(upwardCurvatureStartDistanceFromHeelEdgeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE);

    // Upward curve end distance from heel edge.
    AlgorithmSetting upwardCurvatureEndDistanceFromHeelEdgeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE,
        displayOrder++,
        65.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_TOE_EDGE_DISTANCE,
        "HTML_DESC_QFN_MEASUREMENT_(UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion,
        AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE,
        AlgorithmSettingComparatorEnum.GREATER);
    addAlgorithmSetting(upwardCurvatureEndDistanceFromHeelEdgeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE);

    // Nominal average fillet length
    AlgorithmSetting nominalFilletLengthSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(10.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        nominalFilletLengthSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH);

    addAlgorithmSetting(nominalFilletLengthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH);

    // Nominal average fillet thickness
    AlgorithmSetting nominalFilletThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(4.0f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        nominalFilletThicknessSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_FILLET_THICKNESS);

    addAlgorithmSetting(nominalFilletThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS);

    // Nominal heel thickness.
    AlgorithmSetting nominalHeelThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(4.f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // desc
      "IMG_DESC_QFN_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        nominalHeelThicknessSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS);

    addAlgorithmSetting(nominalHeelThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS);

    // Nominal toe thickness.
    AlgorithmSetting nominalToeThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(4.f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_MEASUREMENT_(NOMINAL_TOE_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(NOMINAL_TOE_THICKNESS)_KEY", // desc
      "IMG_DESC_QFN_MEASUREMENT_(NOMINAL_TOE_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        nominalToeThicknessSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_TOE_THICKNESS);

    addAlgorithmSetting(nominalToeThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS);

    // Nominal center thickness.
    AlgorithmSetting nominalCenterThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(4.f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_MEASUREMENT_(NOMINAL_CENTER_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(NOMINAL_CENTER_THICKNESS)_KEY", // desc
      "IMG_DESC_QFN_MEASUREMENT_(NOMINAL_CENTER_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        nominalCenterThicknessSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS);

    addAlgorithmSetting(nominalCenterThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS);

    // Indicates whether heel void compensation is enabled or not.
    ArrayList<String> allowableHeelVoidCompensationValues = new ArrayList<String>(Arrays.asList("On", "Off"));
    AlgorithmSetting heelVoidCompensationSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_VOID_COMPENSATION,
      displayOrder++,
      "Off",
      allowableHeelVoidCompensationValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(heelVoidCompensationSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_VOID_COMPENSATION);

    // Technique to determine fillet length
    ArrayList<String> allowableFilletLengthTechniqueValues = new ArrayList<String>(Arrays.asList("Thickness", "Slope"));
    AlgorithmSetting filletLengthTechniqueSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_FILLET_LENGTH_TECHNIQUE,
      displayOrder++,
      "Thickness",
      allowableFilletLengthTechniqueValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_MEASUREMENT_(FILLET_LENGTH_TECHNIQUE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(FILLET_LENGTH_TECHNIQUE)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(FILLET_LENGTH_TECHNIQUE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(filletLengthTechniqueSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_FILLET_LENGTH_TECHNIQUE);

    // Additional thresholds

    // Percent of pad width across to use for the primary pad thickness profile.
    AlgorithmSetting padProfileWidthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_WIDTH,
        displayOrder++,
        45.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS,
        "HTML_DESC_QFN_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(padProfileWidthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_WIDTH);

    // Percent of the pad length along to use for the primary pad thickness profile.
    AlgorithmSetting padProfileLengthSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_LENGTH,
      displayOrder++,
      110.f, // default
      0.0f, // min
      200.f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
      "HTML_DESC_QFN_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(padProfileLengthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_LENGTH);

    // Background technique
    // CR31674 - hidden, not useful at this time
    ArrayList<String> allowableBackgroundTechniqueValues = new ArrayList<String>(Arrays.asList("Standard", "Percentile"));
    AlgorithmSetting backgroundTechniqueSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_TECHNIQUE,
      displayOrder++,
      "Standard",
      allowableBackgroundTechniqueValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_MEASUREMENT_(BACKGROUND_TECHNIQUE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(BACKGROUND_TECHNIQUE)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(BACKGROUND_TECHNIQUE)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(backgroundTechniqueSetting);

    // Background region location (as percent of IPD).
    AlgorithmSetting backgroundRegionLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION,
      displayOrder++,
      50.f, // default
      0.0f, // min
      200.f, // max
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_QFN_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionLocation);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION);
    
    // Background region location offset (as absolute offset distance from edge of pad).
    AlgorithmSetting backgroundRegionLocationOffset = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET,
      displayOrder++,
      0.0f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.MILS,
      "HTML_DESC_QFN_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionLocationOffset);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET);

    // Target heel edge thickess as a percent of max pad profile thickness.
    AlgorithmSetting heelEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT,
        displayOrder++,
        25.f, // default
        0.0f, // min
        100f, // max
        MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG,
        "HTML_DESC_QFN_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(heelEdgeSearchThicknessPercentSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT);

    // Target toe edge thickness as a percent of nominal toe thickness.
    AlgorithmSetting toeEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT,
        displayOrder++,
        25.f, // default
        0.0f, // min
        300.f, // max
        MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG,
        "HTML_DESC_QFN_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(toeEdgeSearchThicknessPercentSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT);

    // Technique to determine feature location
    ArrayList<String> allowableFeatureLocationTechniqueValues = new ArrayList<String>(Arrays.asList("Fillet Edges", "Profile Edges"));
    AlgorithmSetting featureLocationTechniqueSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_FEATURE_LOCATION_TECHNIQUE,
      displayOrder++,
      "Fillet Edges",
      allowableFeatureLocationTechniqueValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_MEASUREMENT_(FEATURE_LOCATION_TECHNIQUE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(FEATURE_LOCATION_TECHNIQUE)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(FEATURE_LOCATION_TECHNIQUE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(featureLocationTechniqueSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_MEASUREMENT_FEATURE_LOCATION_TECHNIQUE);

    // Technique to determine toe location
    ArrayList<String> allowableToeLocationTechniqueValues = new ArrayList<String>(Arrays.asList("Percent of Fillet Length", "Fixed Distance"));
    AlgorithmSetting toeLocationTechniqueSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_LOCATION_TECHNIQUE,
        displayOrder++,
        "Percent of Fillet Length",
        allowableToeLocationTechniqueValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_MEASUREMENT_(TOE_LOCATION_TECHNIQUE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(TOE_LOCATION_TECHNIQUE)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(TOE_LOCATION_TECHNIQUE)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(toeLocationTechniqueSetting);

    // Fixed distance of heel edge to toe.
    AlgorithmSetting toeLocationFixedDistanceSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_FIXED_DISTANCE,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(10.f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(1000.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_MEASUREMENT_(TOE_FIXED_DISTANCE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(TOE_FIXED_DISTANCE)_KEY", // desc
      "IMG_DESC_QFN_MEASUREMENT_(TOE_FIXED_DISTANCE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(toeLocationFixedDistanceSetting);

    // Technique to determine center location
    ArrayList<String> allowableCenterLocationTechniqueValues = new ArrayList<String>(Arrays.asList("Percent of Heel-Toe Length", "Percent of Fillet Length"));
    AlgorithmSetting centerLocationTechniqueSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_LOCATION_TECHNIQUE,
        displayOrder++,
        "Percent of Heel-Toe Length",
        allowableCenterLocationTechniqueValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_MEASUREMENT_(CENTER_LOCATION_TECHNIQUE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(CENTER_LOCATION_TECHNIQUE)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(CENTER_LOCATION_TECHNIQUE)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(centerLocationTechniqueSetting);

    // Smoothing kernel length for profile smoothing
    AlgorithmSetting smoothingKernelLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_MEASUREMENT_SMOOTHING_KERNEL_LENGTH,
        displayOrder++,
        6, // default
        1, // min
        12, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_MEASUREMENT_(SMOOTHING_KERNEL_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(SMOOTHING_KERNEL_LENGTH)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(SMOOTHING_KERNEL_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(smoothingKernelLengthSetting);

    // Location of Solder Thickness
    AlgorithmSetting solderThicknessLocationSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_LOCATION,
      displayOrder++,
      20.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
      "HTML_DESC_QFN_MEASUREMENT_(SOLDER_THICKNESS_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(SOLDER_THICKNESS_LOCATION)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(SOLDER_THICKNESS_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(solderThicknessLocationSetting);

    // Nominal Center of Solder Volume
    // CR33010 - setting is hidden until proven useful
    AlgorithmSetting centerOfSolderVolumeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME,
      displayOrder++,
      50.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
      "HTML_DESC_QFN_MEASUREMENT_(NOMINAL_CENTER_OF_SOLDER_VOLUME)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(NOMINAL_CENTER_OF_SOLDER_VOLUME)_KEY", // detailed desc
      "IMG_DESC_QFN_MEASUREMENT_(NOMINAL_CENTER_OF_SOLDER_VOLUME)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(centerOfSolderVolumeSetting);

    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(40.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_QFN_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);
    
    // Added by Lee Herng (4 Mar 2011)
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -200.0f), // minimum value
        MathUtil.convertMilsToMillimeters(200.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_QFN_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed desc
        "IMG_DESC_QFN_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_QFN_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is displayed right before psh in the Slice Setup Tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_QFN_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_QFN_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());

    addMeasurementEnums();
    
    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author George Booth
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_JOINT_OFFSET_FROM_CAD);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_FILLET_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_TOE_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_UPWARD_CURVATURE);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_HEEL_SHARPNESS);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_WIDTH);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_WIDTH);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_NEIGHBOR_LENGTH_DIFFERENCE);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_AT_LOCATION);
    
    //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_FILLET_LENGTH_REGION_OUTLIER);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_REGION_OUTLIER);
    // CR33010 - measurement is commented
    // _jointMeasurementEnums.add(MeasurementEnum.QFN_MEASUREMENT_CENTER_OF_SOLDER_VOLUME);
  }

  /**
   * Runs 'Measurement' on all the specified joints.
   *
   * @author George Booth
   * @Edited By KEe Chin Seong - When runs measurement on joints, those neighbour
   *                             MUST need to be tested joints, else the measurement
   *                             will be empty value or NULL.
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.size() > 0);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    // get the smoothing kernel length
    int smoothingKernelLength = getSmoothingKernelLength(subtype);

    // QFN only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Run Locator.
    Locator.locateJoints(reconstructedImages, padSlice, jointInspectionDataObjects, this, true);

    // Get the applicable image for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);
    Image padSliceImage = reconstructedPadSlice.getOrthogonalImage();

    //Siew Yeng - XCR-2683 - add enhanced image
    if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
    {
      ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, padSlice);
    }
    
    // clear out the locator graphics
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    //Siew Yeng - XCR-3532 - Center thickness region outlier
    List<Float> validCenterThicknessMeasurementsInMils = new ArrayList<Float>();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                jointInspectionData.getInspectionRegion(),
                                                reconstructedPadSlice,
                                                false);

      // Run the QFN measurements on this joint.
      classifyJoint(padSliceImage, padSlice, subtype, jointInspectionData, smoothingKernelLength);

      //Siew Yeng - XCR-3532 - Center thickness region outlier
      JointMeasurement centerThicknessMeasurement = QuadFlatNoLeadMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, padSlice);
      validCenterThicknessMeasurementsInMils.add((float)MathUtil.convertMillimetersToMils(centerThicknessMeasurement.getValue()));	
    }

    //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
    List<Float> validFilletLengthMeasurementsInMils = new ArrayList<Float>();
    
    // now determine the difference between neighbor joints for each joint
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // get neighbors for this joint
      Collection<JointInspectionData> neighboringJoints = jointInspectionData.getNeighboringJointsForQuadFlatNoLead();

      // get length for this joint
      float filletLength = getFilletLengthMeasurement(jointInspectionData, padSlice).getValue();
      validFilletLengthMeasurementsInMils.add((float)MathUtil.convertMillimetersToMils(filletLength));
      
      float maximumDifference = 0.0f;

      for (JointInspectionData neighboringJoint : neighboringJoints)
      {
        //Kee Chin Seong - Only those pad with tested allowed to be neighbour
        if(neighboringJoint.getPad().isInspected())
        {
            float neighborFilletLength = getFilletLengthMeasurement(neighboringJoint, padSlice).getValue();
            float difference = Math.abs(neighborFilletLength - filletLength);
            maximumDifference = Math.max(difference, maximumDifference);
        }
      }

      // save the difference measurement
      JointMeasurement maximumNeighborDistanceMeasurement = new JointMeasurement(
        this,
        MeasurementEnum.QFN_MEASUREMENT_NEIGHBOR_LENGTH_DIFFERENCE,
        MeasurementUnitsEnum.MILLIMETERS,
        jointInspectionData.getPad(),
        padSlice,
        maximumDifference);
     JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
     jointInspectionResult.addMeasurement(maximumNeighborDistanceMeasurement);
    }
    
    //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
    computeRegionOutlierMeasurementsForFilletLength(jointInspectionDataObjects,
                                                    ArrayUtil.convertFloatListToFloatArray(validFilletLengthMeasurementsInMils),
                                                    padSlice);
    computeRegionOutlierMeasurementsForCenterThickness(jointInspectionDataObjects,
                                                        ArrayUtil.convertFloatListToFloatArray(validCenterThicknessMeasurementsInMils),
                                                        padSlice);
  }

  /**
   * Performs the joint classification (measurement) for basic QFN.
   *
   * @author George Booth
   */
  private void classifyJoint(Image image, SliceNameEnum sliceNameEnum,
                               Subtype subtype, JointInspectionData jointInspectionData,
                               int smoothingKernelLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);

    String jointName = jointInspectionData.getPad().getComponentAndPadName();

    // Post a diagnostic to reset the diagnostic infos but not invalidate them.
    _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                         subtype,
                                                         this);

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Keep track of the measurement data we're going to post as diagnostics.
    Collection<JointMeasurement> measurementsToPost = new LinkedList<JointMeasurement>();

    // Get the located joint ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the JointInspectionResult for this joint.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate the distance between CAD position and located position.
    RegionOfInterest cadPositionRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels();
    ImageCoordinate cadPositionCenterCoordinate = new ImageCoordinate(cadPositionRoi.getCenterX(),
                                                                      cadPositionRoi.getCenterY());
    ImageCoordinate locatedJointCenterCoordinate = new ImageCoordinate(locatedJointRoi.getCenterX(),
                                                                       locatedJointRoi.getCenterY());
    double jointOffsetFromCadInPixels = cadPositionCenterCoordinate.distance(locatedJointCenterCoordinate);
    float jointOffsetFromCadInMillis = (float)(jointOffsetFromCadInPixels * MILIMETER_PER_PIXEL);

    // Save the "offset from CAD" measurement.
//    JointMeasurement jointOffsetFromCadMeasurement =
//        new JointMeasurement(this,
//                             MeasurementEnum.QFN_MEASUREMENT_JOINT_OFFSET_FROM_CAD,
//                             MeasurementUnitsEnum.MILLIMETERS,
//                             jointInspectionData.getPad(),
//                             sliceNameEnum,
//                             jointOffsetFromCadInMillis);
//    jointInspectionResult.addMeasurement(jointOffsetFromCadMeasurement);
//    measurementsToPost.add(jointOffsetFromCadMeasurement);

    // Get the smoothed thickness profiles for the pad.
    // We want one profile that only uses a specified (by algorithm setting) fraction of the pad width across
    // and one that uses the entire width across.  We'll use the whole pad width across profile for finding the heel edge
    // and peak (this helps compensate for heel voids).  The fractional pad width across profile will be used for
    // measuring everything else.
    //
    // If heel void compensation is disabled, we'll just use the fractional width profile in lieu of the full width one.
    boolean useHeelVoidCompensation = isHeelVoidCompensationEnabled(subtype);

    // Fractional pad length across profile.
    final float PAD_PROFILE_WIDTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_WIDTH) / 100.f;
    final float PAD_PROFILE_LENGTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_LENGTH) / 100.f;
    Pair<RegionOfInterest, float[]> fractionalWidthPadProfileRoiAndThicknessProfile =
        measureSmoothedPadThicknessProfile(image,
                                           sliceNameEnum,
                                           jointInspectionData,
                                           PAD_PROFILE_WIDTH_FRACTION,
                                           PAD_PROFILE_LENGTH_FRACTION,
                                           true,
                                           smoothingKernelLength);
    RegionOfInterest fractionalWidthPadProfileRoi = fractionalWidthPadProfileRoiAndThicknessProfile.getFirst();
    float[] fractionalWidthPadThicknessProfile = fractionalWidthPadProfileRoiAndThicknessProfile.getSecond();

    // Measure the full pad length across profile.  If void compensation is disabled, just use the fractional
    // profile.
    RegionOfInterest smoothedProfileWithVoidCompRoi = null;
    float[] fullWidthPadThicknessProfile = null;
    if (useHeelVoidCompensation)
    {
      Pair<RegionOfInterest, float[]> fullWidthPadProfileRoiAndThicknessProfile =
          measureSmoothedPadThicknessProfile(image,
                                             sliceNameEnum,
                                             jointInspectionData,
                                             1f,
                                             PAD_PROFILE_LENGTH_FRACTION,
                                             false,
                                             smoothingKernelLength);
      smoothedProfileWithVoidCompRoi = fullWidthPadProfileRoiAndThicknessProfile.getFirst();
      fullWidthPadThicknessProfile = fullWidthPadProfileRoiAndThicknessProfile.getSecond();
    }
    else
    {
      smoothedProfileWithVoidCompRoi = fractionalWidthPadProfileRoi;
      fullWidthPadThicknessProfile = new float[fractionalWidthPadThicknessProfile.length];
      System.arraycopy(fractionalWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile.length);
    }

    // generate a working profile based on the entire profile.
    // This could support the 5DX Fillet Region Limit if needed.
    final float FILLET_REGION_LIMIT_FRACTION = 1.0f;
    float[] smoothedProfileWithVoidComp = createWorkingProfileFromArea(fullWidthPadThicknessProfile,
                                                        FILLET_REGION_LIMIT_FRACTION);

    // create a smoothed derivative profile from the working profile
    float[] smoothedFirstDerivativeProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(smoothedProfileWithVoidComp,
                                                    2,
                                                    MagnificationEnum.getCurrentNorminal(),
                                                    MeasurementUnitsEnum.MILLIMETERS);

    smoothedFirstDerivativeProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeProfile,
                                                                              smoothingKernelLength);

    // max thickness determines heel and toe search regions
    int maxThicknessIndex = ArrayUtil.maxIndex(smoothedProfileWithVoidComp);
    float maxThicknessValue = smoothedProfileWithVoidComp[maxThicknessIndex];

    // Locate the heel edge (max profile thickness determined in locateHeelEdge())
    final float HEEL_EDGE_THICKNESS_FRACTION_OF_MAX =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(smoothedProfileWithVoidComp,
                                               HEEL_EDGE_THICKNESS_FRACTION_OF_MAX, false);

    // get the integer index
    int heelEdgeIndex = Math.max(Math.round(heelEdgeSubpixelBin), 0);

    // Locate the toe edge
    final float TOE_EDGE_THICKNESS_FRACTION_OF_MAX =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    float targetToeEdgeThickness = maxThicknessValue * TOE_EDGE_THICKNESS_FRACTION_OF_MAX;
    float toeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(smoothedProfileWithVoidComp,
                                             targetToeEdgeThickness, maxThicknessIndex);

    // make sure toe edge is >= heel edge
    if (toeEdgeSubpixelBin < heelEdgeSubpixelBin)
    {
      toeEdgeSubpixelBin = heelEdgeSubpixelBin;
    }

    // get the integer index
    int toeEdgeIndex = Math.min(Math.round(toeEdgeSubpixelBin), smoothedProfileWithVoidComp.length - 1);

    // find heel derivative index
    float heelSlopeSubpixelBin = AlgorithmFeatureUtil.findMaximumHeelSlopeBin(smoothedFirstDerivativeProfile,
                                                                              maxThicknessIndex,
                                                                              heelEdgeIndex,
                                                                              toeEdgeIndex);

    int heelSlopeIndex = Math.max(Math.round(heelSlopeSubpixelBin), 0);

    // save the "heel slope" measurement
    float heelSlope = smoothedFirstDerivativeProfile[heelSlopeIndex];
    JointMeasurement heelSlopeMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE,
                                                                 MeasurementUnitsEnum.NONE,
                                                                 jointInspectionData.getPad(),
                                                                 sliceNameEnum,
                                                                 heelSlope);
    jointInspectionResult.addMeasurement(heelSlopeMeasurement);
    measurementsToPost.add(heelSlopeMeasurement);

    // find toe negative derivative index
    float toeNegativeSlopeSubpixelBin = AlgorithmFeatureUtil.findMinimumToeSlopeBin(smoothedFirstDerivativeProfile,
                                                                                    maxThicknessIndex,
                                                                                    heelEdgeIndex,
                                                                                    toeEdgeIndex);
    // make sure toe edge is >= heel edge
    if (toeNegativeSlopeSubpixelBin < heelSlopeSubpixelBin)
    {
      toeNegativeSlopeSubpixelBin = heelSlopeSubpixelBin;
    }

    // get the integer index
    int toeNegativeSlopeIndex = Math.min(Math.round(toeNegativeSlopeSubpixelBin), smoothedProfileWithVoidComp.length - 1);

    // save the "toe slope" measurement
    float toeNegativeSlope = smoothedFirstDerivativeProfile[toeNegativeSlopeIndex];

    // make the normally negative slope positive to flag a "positive" slope as a defect
    float toeSlope = -(toeNegativeSlope);
    JointMeasurement toeSlopeMeasurement = new JointMeasurement(this,
                                                                MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE,
                                                                MeasurementUnitsEnum.NONE,
                                                                jointInspectionData.getPad(),
                                                                sliceNameEnum,
                                                                toeSlope);
    jointInspectionResult.addMeasurement(toeSlopeMeasurement);
    measurementsToPost.add(toeSlopeMeasurement);

    // save the "heel/toe slope sum" measurement
    float heelToeSlopeSum = heelSlope + toeSlope;
    JointMeasurement heelToeSlopeSumMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM,
                                                                       MeasurementUnitsEnum.NONE,
                                                                       jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       heelToeSlopeSum);
    jointInspectionResult.addMeasurement(heelToeSlopeSumMeasurement);
    measurementsToPost.add(heelToeSlopeSumMeasurement);

    // The fillet length and some search regions are based on the user selectable "Fillet Length Technique":
    // "Slope" - (default) bins with maximum heel and toe slopes
    // "Thickness" - bins where profile exceeds the heel and toe search thickness
    float filletEdgeIndex1 = heelSlopeSubpixelBin;
    float filletEdgeIndex2 = toeNegativeSlopeSubpixelBin;
    if (isFilletLengthTechniqueSetToThickness(subtype))
    {
      filletEdgeIndex1 = heelEdgeSubpixelBin;
      filletEdgeIndex2 = toeEdgeSubpixelBin;
    }

    // The feature location search region is based on the user selectable "Feature Location Technique":
    // "Fillet Edges" - (default) search the fillet length as defined above
    // "Profile Edges" - search the entire profile
    int featureLocationStartIndex = Math.max(Math.round(filletEdgeIndex1), 0);
    int featureLocationStopIndex = Math.min(Math.round(filletEdgeIndex2), smoothedProfileWithVoidComp.length - 1);
    if (isFeatureLocationTechniqueSetToProfileEdges(subtype))
    {
      featureLocationStartIndex = 0;
      featureLocationStopIndex = smoothedProfileWithVoidComp.length - 1;
    }

    float featureLocationRegion = (float)(featureLocationStopIndex - featureLocationStartIndex);

    // create a smoothed second derivative profile from the working profile
    float[] smoothedSecondDerivativeProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(smoothedFirstDerivativeProfile,
                                                    2,
                                                    MagnificationEnum.getCurrentNorminal(),
                                                    MeasurementUnitsEnum.MILLIMETERS);

    smoothedSecondDerivativeProfile = ProfileUtil.getSmoothedProfile(smoothedSecondDerivativeProfile,
                                                                              smoothingKernelLength);

    // compute the curvature profile
    float[] curvatureProfile = AlgorithmUtil.createCurvatureProfile(smoothedFirstDerivativeProfile, smoothedSecondDerivativeProfile);

    if (_logAlgorithmProfileData)
    {
       writeProfileToCSVFile("c:/temp/QFN_Profiles_" + jointName + ".csv",
         "smoothedProfileWithVoidComp", smoothedProfileWithVoidComp, 1.0F,
         "smoothedFirstDerivativeProfile", smoothedFirstDerivativeProfile, 0.2F,
         "smoothedSecondDerivativeProfile", smoothedSecondDerivativeProfile, 0.02F,
         "curvatureProfile", curvatureProfile, 0.0002F);
    }

/*  CR33010 Center of Solder Volume measurement commented until proven useful

    // measure center of solder volume (center of area under entire profile)
    float centerOfSolderVolumeInPixelsInMillimeters = getCenterOfSolderVolumeInMillimeters(smoothedProfileWithVoidComp, jointName);

    // figure percentage of pad length along
    int padLengthAlongInPixels = locatedJointRoi.getLengthAlong();
    float padLengthAlongInMillimeters = (float)padLengthAlongInPixels * AlgorithmUtil.getMillimetersPerPixel();
    float centerOfSolderVolumePercentOfPadLength = (centerOfSolderVolumeInPixelsInMillimeters / padLengthAlongInMillimeters) * 100.0f;;

    // Save the center of solder volume measurement.
    JointMeasurement centerOfSolderVolumeMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_MEASUREMENT_CENTER_OF_SOLDER_VOLUME,
                             MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             centerOfSolderVolumePercentOfPadLength);
    jointInspectionResult.addMeasurement(centerOfSolderVolumeMeasurement);
    measurementsToPost.add(centerOfSolderVolumeMeasurement);
*/

    // measure thickness at user specific fraction of profile
    final float SOLDER_THICKNESS_LOCATION_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_LOCATION) / 100.f;
    int solderThicknessBin = Math.round((float)smoothedProfileWithVoidComp.length * SOLDER_THICKNESS_LOCATION_FRACTION);
    float solderThicknesstAtLocation = smoothedProfileWithVoidComp[solderThicknessBin];
    float averageSolderThicknesstAtLocation = 0.0f;
    // average of three bins around location
    if (solderThicknessBin > 0 && solderThicknessBin < smoothedProfileWithVoidComp.length - 1)
    {
      averageSolderThicknesstAtLocation = (smoothedProfileWithVoidComp[solderThicknessBin - 1] +
                                    smoothedProfileWithVoidComp[solderThicknessBin] +
                                    smoothedProfileWithVoidComp[solderThicknessBin + 1]) / 3.0f;
    }
    if (averageSolderThicknesstAtLocation > 0.0f)
    {
//      System.out.println("Board-Joint " +jointInspectionData.getFullyQualifiedPadName() + ": " +
//                         "solder thickness at location = " + (solderThicknesstAtLocation * 39.37f) + " mils, " +
//                         "average = " + (averageSolderThicknesstAtLocation * 39.37f) + " mils, " +
//                         "delta = " + ((averageSolderThicknesstAtLocation - solderThicknesstAtLocation) * 39.37f) + " mils");
    }
    else
    {
//      System.out.println("solder thickness at location = " + (solderThicknesstAtLocation * 39.37f) + " mils, " +
//                         ", average not calculated (location is at end of profile)");
    }
    solderThicknesstAtLocation = averageSolderThicknesstAtLocation;

    // Save the thickness at user specific fraction of profile.
    JointMeasurement solderThicknessAtAtLocationMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_AT_LOCATION,
                             MeasurementUnitsEnum.MILLIMETERS,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             solderThicknesstAtLocation);
    jointInspectionResult.addMeasurement(solderThicknessAtAtLocationMeasurement);
    measurementsToPost.add(solderThicknessAtAtLocationMeasurement);

    // Locate the heel
    final float HEEL_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
    int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
    int heelSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                        HEEL_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
    
    //the reason i put this infront is because to calculate the heelSearchDistanceInPixels first
    //If is < 1, then only i assign it become 1, if is previous case the 1 might be overwritten
    //as the smoothedProfile length might be < 1 or the 0 because the image might be too bright
    //causing the smoothProfile is 35 and feature location Start index is 34 - 1 which is 0
    if (featureLocationStartIndex + heelSearchDistanceInPixels >= smoothedProfileWithVoidComp.length)
    {
      heelSearchDistanceInPixels = smoothedProfileWithVoidComp.length - featureLocationStartIndex - 1;
    }
    // make sure search distance is resonable
    if (heelSearchDistanceInPixels < 1)
    {
      heelSearchDistanceInPixels = 1;
    }

    int heelPlateauBin = _UNUSED_BIN;
    // locate the heel corner (region of maximum negative curvature in heel asearch region)
    int heelCornerBin = locateHeelCornerIndex(curvatureProfile, featureLocationStartIndex, heelSearchDistanceInPixels);
    if (heelCornerBin > _UNUSED_BIN)
    {
      // locate the heel plateau (first relatively flat area in heel search region)
      heelPlateauBin = locateHeelPlateauIndex(smoothedFirstDerivativeProfile, featureLocationStartIndex, heelCornerBin, heelSearchDistanceInPixels);
      if (heelPlateauBin == _UNUSED_BIN)
      {
        heelPlateauBin = heelCornerBin;
      }
    }

    // use 5DX method (max value in heel search region)
    int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(smoothedProfileWithVoidComp,
                                                          featureLocationStartIndex,
                                                          heelSearchDistanceInPixels);

    int heelBin = heelPlateauBin;
    if (heelBin == _UNUSED_BIN)
    {
      heelBin = heelPeakBin;
    }

    // Locate the toe (default - % of fillet length)
    final float TOE_DISTANCE_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET) / 100.f;
    int toeDistanceInPixels = Math.round(featureLocationRegion * TOE_DISTANCE_FRACTION);
    if (isToeLocationTechniqueSetToFixedDistance(subtype))
    {
      final float TOE_FIXED_DISTANCE =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_FIXED_DISTANCE);
      toeDistanceInPixels = MathUtil.convertMillimetersToPixelsUsingCeil(TOE_FIXED_DISTANCE,
                                                                         MathUtil.convertMillimetersToNanometers(MILIMETER_PER_PIXEL));
    }

    int toeStartIndex = featureLocationStartIndex;
    if (isToeLocationTechniqueSetToFixedDistance(subtype))
    {
      toeStartIndex = heelBin;
    }
    int toePeakBin = AlgorithmFeatureUtil.locateToePeak(smoothedProfileWithVoidComp, toeStartIndex, toeDistanceInPixels);
    // make sure toe is >= heel
    if (toePeakBin < heelBin)
    {
      toePeakBin = heelBin;
    }

    float heelToeDistance = (float)(toePeakBin - heelBin);

    // locate the center (default - % of fillet length)
    final float CENTER_DISTANCE_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET) / 100.f;
    int centerDistanceInPixels = Math.round(featureLocationRegion * CENTER_DISTANCE_FRACTION);
    if (isCenterLocationTechniqueSetToPercentHeelToeDistance(subtype))
    {
      centerDistanceInPixels = Math.round(heelToeDistance * CENTER_DISTANCE_FRACTION);
    }

    int centerStartIndex = featureLocationStartIndex;
    if (isCenterLocationTechniqueSetToPercentHeelToeDistance(subtype))
    {
      centerStartIndex = heelBin;
    }
    int centerBin = locateCenterIndex(centerStartIndex, centerDistanceInPixels, heelBin, toePeakBin);

    // determine the slope around the center (sum of slopes around center)
    int searchLength = Math.round(((float)(filletEdgeIndex2 - filletEdgeIndex1) * _CENTER_PERCENT_OF_FILLET) / 2);
    // make sure the search is within the profile (CR30738)
    int startIndex = Math.max(centerBin - searchLength, 0);
    int endIndex = Math.min(centerBin + searchLength, smoothedFirstDerivativeProfile.length);
//    float centerSlope = Math.abs(ArrayUtil.sum(smoothedFirstDerivativeProfile, startIndex, endIndex));
    float centerSlope = ArrayUtil.sum(smoothedFirstDerivativeProfile, startIndex, endIndex);

    // save the "center slope" measurement
    JointMeasurement centerSlopeMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE,
                                                                 MeasurementUnitsEnum.NONE,
                                                                 jointInspectionData.getPad(),
                                                                 sliceNameEnum,
                                                                 centerSlope);
    jointInspectionResult.addMeasurement(centerSlopeMeasurement);
    measurementsToPost.add(centerSlopeMeasurement);

    // save the "sum of slopes" measurement
    float sumOfSlopeChanges = AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(smoothedSecondDerivativeProfile);

    JointMeasurement sumOfSlopeChangesMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES,
                                                                 MeasurementUnitsEnum.NONE,
                                                                 jointInspectionData.getPad(),
                                                                 sliceNameEnum,
                                                                 sumOfSlopeChanges);
    jointInspectionResult.addMeasurement(sumOfSlopeChangesMeasurement);
    measurementsToPost.add(sumOfSlopeChangesMeasurement);

    final float UPWARD_CURVATURE_START_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE) / 100.f;
    int upwardCurvatureStartIndex = featureLocationStartIndex +
                                    (int)(featureLocationRegion * UPWARD_CURVATURE_START_FRACTION);

    final float UPWARD_CURVATURE_STOP_FRACTION =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE) / 100.f;
    int upwardCurvatureStopIndex = featureLocationStartIndex +
                                  (int)(featureLocationRegion * UPWARD_CURVATURE_STOP_FRACTION);

    // save the "upward curvature" measurement
    float upwardCurvature = getAverageCurvature(curvatureProfile, upwardCurvatureStartIndex, upwardCurvatureStopIndex);

    JointMeasurement upwardCurvatureMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.QFN_MEASUREMENT_UPWARD_CURVATURE,
                                                                 MeasurementUnitsEnum.NONE,
                                                                 jointInspectionData.getPad(),
                                                                 sliceNameEnum,
                                                                 upwardCurvature);
    jointInspectionResult.addMeasurement(upwardCurvatureMeasurement);
    measurementsToPost.add(upwardCurvatureMeasurement);

    // save the "heel sharpness" measurement
    float heelSharpness = measureHeelSharpness(curvatureProfile, featureLocationStartIndex, heelSearchDistanceInPixels);

    JointMeasurement healSharpnessMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.QFN_MEASUREMENT_HEEL_SHARPNESS,
                                                                 MeasurementUnitsEnum.NONE,
                                                                 jointInspectionData.getPad(),
                                                                 sliceNameEnum,
                                                                 heelSharpness);
    jointInspectionResult.addMeasurement(healSharpnessMeasurement);
    measurementsToPost.add(healSharpnessMeasurement);

    // Save the "fillet thickness" measurement.
    float filletThickness = measureFilletThickness(smoothedProfileWithVoidComp,
                                                   featureLocationStartIndex, featureLocationStopIndex);
    JointMeasurement filletThicknessMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.QFN_MEASUREMENT_FILLET_THICKNESS,
                                                                       MeasurementUnitsEnum.MILLIMETERS,
                                                                       jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       filletThickness);
    jointInspectionResult.addMeasurement(filletThicknessMeasurement);
    measurementsToPost.add(filletThicknessMeasurement);

    // Save the "fillet length" measurement.
    float filletLength = (filletEdgeIndex2 - filletEdgeIndex1) * MILIMETER_PER_PIXEL;
    // System.out.println(filletEdgeIndex1 + ", " + filletEdgeIndex2 + ", " + filletLength);
    JointMeasurement filletLengthMeasurement = new JointMeasurement(this,
                                                                    MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH,
                                                                    MeasurementUnitsEnum.MILLIMETERS,
                                                                    jointInspectionData.getPad(),
                                                                    sliceNameEnum,
                                                                    filletLength);
    jointInspectionResult.addMeasurement(filletLengthMeasurement);
    measurementsToPost.add(filletLengthMeasurement);

    // Save the heel thickness measurement.
    float heelThickness = smoothedProfileWithVoidComp[heelBin];
    JointMeasurement heelThicknessMeasurement = new JointMeasurement(this,
                                                                     MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS,
                                                                     MeasurementUnitsEnum.MILLIMETERS,
                                                                     jointInspectionData.getPad(),
                                                                     sliceNameEnum,
                                                                     heelThickness);
    jointInspectionResult.addMeasurement(heelThicknessMeasurement);
    measurementsToPost.add(heelThicknessMeasurement);

    // Save the center thickness measurement.
    float centerThickness = smoothedProfileWithVoidComp[centerBin];
    JointMeasurement centerThicknessMeasurement = new JointMeasurement(this,
                                                                    MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS,
                                                                    MeasurementUnitsEnum.MILLIMETERS,
                                                                    jointInspectionData.getPad(),
                                                                    sliceNameEnum,
                                                                    centerThickness);
    jointInspectionResult.addMeasurement(centerThicknessMeasurement);
    measurementsToPost.add(centerThicknessMeasurement);

    // Save the toe thickness measurement.
    float toeThickness = smoothedProfileWithVoidComp[toePeakBin];
    JointMeasurement toeThicknessMeasurement = new JointMeasurement(this,
                                                                    MeasurementEnum.QFN_MEASUREMENT_TOE_THICKNESS,
                                                                    MeasurementUnitsEnum.MILLIMETERS,
                                                                    jointInspectionData.getPad(),
                                                                    sliceNameEnum,
                                                                    toeThickness);
    jointInspectionResult.addMeasurement(toeThicknessMeasurement);
    measurementsToPost.add(toeThicknessMeasurement);

    // Post the measured profile along values.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      // Post a diagnostic to reset the Background/Pad graphics but not invalidate them.
      _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                           subtype,
                                                           this);

      postPadThicknessProfileDiagnostics(jointInspectionData, sliceNameEnum,
                                         measurementsToPost,
                                         smoothedProfileWithVoidCompRoi,
                                         smoothedProfileWithVoidComp,
                                         featureLocationStartIndex,
                                         featureLocationStartIndex + heelSearchDistanceInPixels,
                                         (int)filletEdgeIndex1,
                                         heelBin,
                                         centerBin,
                                         toePeakBin,
                                         (int)filletEdgeIndex2,
                                         upwardCurvatureStartIndex,
                                         upwardCurvatureStopIndex,
                                         solderThicknessBin);

      // Post a diagnostic to reset the diagnostic infos but not invalidate them.
      _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                           subtype,
                                                           this);
    }

    // ------------ heel across measurements -----------------------------

    Collection<JointMeasurement> heelMeasurementsToPost = new LinkedList<JointMeasurement>();

    // find width and slopes across heel
    // create an heel across profile at the heel peak bin
    Pair<RegionOfInterest, float[]> smoothedHeelAcrossThicknessProfileRoiAndThicknessProfile =
      measureSmoothedAcrossProfile(image,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   smoothedProfileWithVoidCompRoi,
                                   smoothedProfileWithVoidComp,
                                   heelBin,
                                   true, 
                                   true,
                                   smoothingKernelLength);
    RegionOfInterest smoothedHeelAcrossRoi = smoothedHeelAcrossThicknessProfileRoiAndThicknessProfile.getFirst();
    float[] smoothedHeelAcrossThicknessProfile = smoothedHeelAcrossThicknessProfileRoiAndThicknessProfile.getSecond();

    // create a smoothed derivative profile from the heel across profile
    float[] smoothedFirstDerivativeHeelAcrossProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(smoothedHeelAcrossThicknessProfile,
                                                    2,
                                                    MagnificationEnum.getCurrentNorminal(),
                                                    MeasurementUnitsEnum.MILLIMETERS);

    smoothedFirstDerivativeHeelAcrossProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeHeelAcrossProfile,
                                                                              smoothingKernelLength);

    if (_logAlgorithmProfileData)
    {
      writeProfileToCSVFile("c:/temp/QFN_ProfileHeelAcross_" + jointName + ".csv",
        "smoothedHeelAcrossThicknessProfile", smoothedHeelAcrossThicknessProfile, 10.0F,
        "smoothedFirstDerivativeHeelAcrossProfile", smoothedFirstDerivativeHeelAcrossProfile, 1.0F);
    }


    // save the "heel across width" measurement
    Pair<Float, Float> heelAcrossEdges = measureLeadingTrailingEdgesAcross(smoothedFirstDerivativeHeelAcrossProfile);
    float heelAcrossLeadingEdgeSubpixelBin = heelAcrossEdges.getFirst().floatValue();
    float heelAcrossTrailingEdgeSubpixelBin = heelAcrossEdges.getSecond().floatValue();
    float heelAcrossWidth = (heelAcrossTrailingEdgeSubpixelBin - heelAcrossLeadingEdgeSubpixelBin) * MILIMETER_PER_PIXEL;
    JointMeasurement heelAcrossWidthMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_WIDTH,
                                                                       MeasurementUnitsEnum.MILLIMETERS,
                                                                       jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       heelAcrossWidth);
    jointInspectionResult.addMeasurement(heelAcrossWidthMeasurement);
    heelMeasurementsToPost.add(heelAcrossWidthMeasurement);

    // save the "heel Across Slope Sum" measurement
    float heelAcrossSlopeSum = AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(smoothedFirstDerivativeHeelAcrossProfile);
    JointMeasurement heelAcrossSlopeSumMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM,
                                                                 MeasurementUnitsEnum.NONE,
                                                                 jointInspectionData.getPad(),
                                                                 sliceNameEnum,
                                                                 heelAcrossSlopeSum);
    jointInspectionResult.addMeasurement(heelAcrossSlopeSumMeasurement);
    heelMeasurementsToPost.add(heelAcrossSlopeSumMeasurement);


    // save the "heel Across Leading Trailing Slope Sum" measurement
    float heelAcrossLeadingTrailingSlopeSum = measureLeadingTrailingSlopeSum(smoothedFirstDerivativeHeelAcrossProfile);
    JointMeasurement heelAcrossLeadingTrailingSlopeSumMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM,
                                                                 MeasurementUnitsEnum.NONE,
                                                                 jointInspectionData.getPad(),
                                                                 sliceNameEnum,
                                                                 heelAcrossLeadingTrailingSlopeSum);
    jointInspectionResult.addMeasurement(heelAcrossLeadingTrailingSlopeSumMeasurement);
    heelMeasurementsToPost.add(heelAcrossLeadingTrailingSlopeSumMeasurement);

    // Post the measured heel across profile values.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      postHeelAcrossProfileDiagnostics(jointInspectionData,
                                       sliceNameEnum,
                                       heelMeasurementsToPost,
                                       smoothedHeelAcrossRoi,
                                       smoothedHeelAcrossThicknessProfile,
                                       heelAcrossLeadingEdgeSubpixelBin,
                                       heelAcrossTrailingEdgeSubpixelBin);

      // Post a diagnostic to reset the diagnostic infos but not invalidate them.
      _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                           subtype,
                                                           this);
    }

    // ------------ center across measurements -----------------------------

    Collection<JointMeasurement> centerMeasurementsToPost = new LinkedList<JointMeasurement>();

    // find width and slopes across center
    // create an center across profile at the center bin
    Pair<RegionOfInterest, float[]> smoothedCenterAcrossThicknessProfileRoiAndThicknessProfile =
      measureSmoothedAcrossProfile(image,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   smoothedProfileWithVoidCompRoi,
                                   smoothedProfileWithVoidComp,
                                   centerBin,
                                   false, 
                                   true,
                                   smoothingKernelLength);
    RegionOfInterest smoothedCenterAcrossRoi = smoothedCenterAcrossThicknessProfileRoiAndThicknessProfile.getFirst();
    float[] smoothedCenterAcrossThicknessProfile = smoothedCenterAcrossThicknessProfileRoiAndThicknessProfile.getSecond();

    // create a smoothed derivative profile from the center across profile
    float[] smoothedFirstDerivativeCenterAcrossProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(smoothedCenterAcrossThicknessProfile,
                                                    2,
                                                    MagnificationEnum.getCurrentNorminal(),
                                                    MeasurementUnitsEnum.MILLIMETERS);

    smoothedFirstDerivativeCenterAcrossProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeCenterAcrossProfile,
                                                                              smoothingKernelLength);

    if (_logAlgorithmProfileData)
    {
      writeProfileToCSVFile("c:/temp/QFN_ProfileCenterAcross_" + jointName + ".csv",
                            "smoothedCenterAcrossThicknessProfile", smoothedCenterAcrossThicknessProfile, 10.0F,
                            "smoothedFirstDerivativeCenterAcrossProfile", smoothedFirstDerivativeCenterAcrossProfile, 1.0F);
    }

    // Save the "center across width" measurement.
    Pair<Float, Float> centerAcrossEdges = measureLeadingTrailingEdgesAcross(smoothedFirstDerivativeCenterAcrossProfile);
    float centerAcrossLeadingEdgeSubpixelBin = centerAcrossEdges.getFirst().floatValue();
    float centerAcrossTrailingEdgeSubpixelBin = centerAcrossEdges.getSecond().floatValue();
    float centerAcrossWidth = (centerAcrossTrailingEdgeSubpixelBin - centerAcrossLeadingEdgeSubpixelBin) * MILIMETER_PER_PIXEL;
    JointMeasurement centerAcrossWidthMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_WIDTH,
                                                                       MeasurementUnitsEnum.MILLIMETERS,
                                                                       jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       centerAcrossWidth);
    jointInspectionResult.addMeasurement(centerAcrossWidthMeasurement);
    centerMeasurementsToPost.add(centerAcrossWidthMeasurement);

    // Save the "center across slope sum" measurement.
    float centerAcrossSlopeSum = AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(smoothedFirstDerivativeCenterAcrossProfile);
    JointMeasurement centerAcrossSlopeSumMeasurement = new JointMeasurement(this,
                                                                            MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM,
                                                                            MeasurementUnitsEnum.NONE,
                                                                            jointInspectionData.getPad(),
                                                                            sliceNameEnum,
                                                                            centerAcrossSlopeSum);
    jointInspectionResult.addMeasurement(centerAcrossSlopeSumMeasurement);
    centerMeasurementsToPost.add(centerAcrossSlopeSumMeasurement);


    // Save the "center across Leading Trailing slope sum" measurement.
    float centerAcrossLeadingTrailingSlopeSum = measureLeadingTrailingSlopeSum(smoothedFirstDerivativeCenterAcrossProfile);
    JointMeasurement centerAcrossLeadingTrailingSlopeSumMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM,
                                                                       MeasurementUnitsEnum.NONE,
                                                                       jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       centerAcrossLeadingTrailingSlopeSum);
    jointInspectionResult.addMeasurement(centerAcrossLeadingTrailingSlopeSumMeasurement);
    centerMeasurementsToPost.add(centerAcrossLeadingTrailingSlopeSumMeasurement);

    // Post the measured heel across profile values.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      postCenterAcrossProfileDiagnostics(jointInspectionData,
                                         sliceNameEnum,
                                         centerMeasurementsToPost,
                                         smoothedCenterAcrossRoi,
                                         smoothedCenterAcrossThicknessProfile,
                                         centerAcrossLeadingEdgeSubpixelBin,
                                         centerAcrossTrailingEdgeSubpixelBin);
    }
  }

  /**
   * @author George Booth
   */
  private void postPadThicknessProfileDiagnostics(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum,
                                                  Collection<JointMeasurement> measurementsToPost,
                                                  RegionOfInterest fractionalWidthPadProfileRoi,
                                                  float[] fractionalWidthPadThicknessProfile,
                                                  int heelSearchStartBin,
                                                  int heelSearchStopBin,
                                                  int heelEdgeBin,
                                                  int heelPeakBin,
                                                  int centerBin,
                                                  int toePeakBin,
                                                  int toeEdgeBin,
                                                  int upwardCurvatureStartIndex,
                                                  int upwardCurvatureStopIndex,
                                                  int solderThicknessBin)
  {
      // Post the measured values.
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      jointInspectionData.getInspectionRegion(),
                                                      jointInspectionData,
                                                      measurementsToPost.toArray(new JointMeasurement[0]));

      // Post the pad thickness profile and graphics.
      // make sure features aren't superimposed
      if (heelPeakBin <= heelEdgeBin)
      {
        heelPeakBin = heelEdgeBin + 1;
      }
      if (centerBin <= heelPeakBin)
      {
        centerBin = heelPeakBin + 1;
      }
      if (toePeakBin >= toeEdgeBin)
      {
        toePeakBin = toeEdgeBin - 1;
      }
      // make sure all are within range
      heelEdgeBin = Math.min(heelEdgeBin, fractionalWidthPadThicknessProfile.length - 1);
      heelPeakBin = Math.min(heelPeakBin, fractionalWidthPadThicknessProfile.length - 1);
      centerBin = Math.min(centerBin, fractionalWidthPadThicknessProfile.length - 1);
      toePeakBin = Math.min(toePeakBin, fractionalWidthPadThicknessProfile.length - 1);
      toeEdgeBin = Math.min(toeEdgeBin, fractionalWidthPadThicknessProfile.length - 1);

      Collection<DiagnosticInfo> profileAndFeaturesDiagnosticInfos =
          createMeasurementRegionDiagInfosForProfile(jointInspectionData,
                                                     fractionalWidthPadProfileRoi,
                                                     fractionalWidthPadThicknessProfile,
                                                     heelEdgeBin,
                                                     heelPeakBin,
                                                     centerBin,
                                                     toePeakBin,
                                                     toeEdgeBin,
                                                     solderThicknessBin);

      ProfileDiagnosticInfo labeledProfileDiagnosticInfo =
          createLabeledPadThicknessProfileDiagInfo(jointInspectionData,
                                                   fractionalWidthPadThicknessProfile,
                                                   heelSearchStartBin,
                                                   heelSearchStopBin,
                                                   heelEdgeBin,
                                                   heelPeakBin,
                                                   centerBin,
                                                   toePeakBin,
                                                   toeEdgeBin,
                                                   upwardCurvatureStartIndex,
                                                   upwardCurvatureStopIndex,
                                                   solderThicknessBin);

      profileAndFeaturesDiagnosticInfos.add(labeledProfileDiagnosticInfo);

      _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                   sliceNameEnum,
                                   jointInspectionData,
                                   this,
                                   false,
                                   profileAndFeaturesDiagnosticInfos.toArray(new DiagnosticInfo[0]));
  }

  /**
   * @author George Booth
   */
  private void postHeelAcrossProfileDiagnostics(JointInspectionData jointInspectionData,
                                                SliceNameEnum sliceNameEnum,
                                                Collection<JointMeasurement> measurementsToPost,
                                                RegionOfInterest smoothedHeelAcrossRoi,
                                                float[] smoothedHeelThicknessProfile,
                                                float heelLeadingEdge,
                                                float heelTrailingEdge)
  {
    // Post a diagnostic showing the labeled across profile and the measured values.
    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    jointInspectionData.getInspectionRegion(),
                                                    jointInspectionData,
                                                    measurementsToPost.toArray(new JointMeasurement[0]));

    Collection<DiagnosticInfo> profileAndFeaturesDiagnosticInfos = new LinkedList<DiagnosticInfo>();

    ProfileDiagnosticInfo labeledProfileDiagnosticInfo =
        createLabeledHeelAcrossThicknessProfileDiagInfo(jointInspectionData,
                                                        smoothedHeelThicknessProfile,
                                                        (int)heelLeadingEdge,
                                                        (int)heelTrailingEdge);
    profileAndFeaturesDiagnosticInfos.add(labeledProfileDiagnosticInfo);

    _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                 sliceNameEnum,
                                 jointInspectionData,
                                 this,
                                 false,
                                 profileAndFeaturesDiagnosticInfos.toArray(new DiagnosticInfo[0]));
  }

  /**
   * @author George Booth
   */
  private void postCenterAcrossProfileDiagnostics(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum,
                                                  Collection<JointMeasurement> measurementsToPost,
                                                  RegionOfInterest smoothedCenterAcrossRoi,
                                                  float[] smoothedCenterThicknessProfile,
                                                  float centerLeadingEdge,
                                                  float centerTrailingEdge)
  {
    // Post a diagnostic showing the labeled across profile and the measured values.
    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    jointInspectionData.getInspectionRegion(),
                                                    jointInspectionData,
                                                    measurementsToPost.toArray(new JointMeasurement[0]));

    Collection<DiagnosticInfo> acrossProfileAndFeaturesDiagnosticInfos = new LinkedList<DiagnosticInfo>();

    ProfileDiagnosticInfo labeledProfileAcrossDiagInfo =
        createLabeledCenterAcrossThicknessProfileDiagInfo(jointInspectionData,
                                                          smoothedCenterThicknessProfile,
                                                          (int)centerLeadingEdge,
                                                          (int)centerTrailingEdge);
    acrossProfileAndFeaturesDiagnosticInfos.add(labeledProfileAcrossDiagInfo);

    _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                 sliceNameEnum,
                                 jointInspectionData,
                                 this,
                                 false,
                                 acrossProfileAndFeaturesDiagnosticInfos.toArray(new DiagnosticInfo[0]));
  }

  /**
   * Makes a smoothed thickness profile for the specified joint.  The profile width is set based on the specified
   * fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad and then taking two background profiles on either
   * side of the pad.  The two background profiles are averaged together and then the foreground and average
   * background profiles are converted into a "pad thickness profile".  Finally, the thickness profile
   * is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedPadThicknessProfile(Image image,
                                                                             SliceNameEnum sliceNameEnum,
                                                                             JointInspectionData jointInspectionData,
                                                                             float fractionOfPadLengthAcross,
                                                                             float fractionOfPadLengthAlong,
                                                                             boolean postProfileRegions, 
                                                                             int smoothingKernelLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the interpad distance.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    // Calculate the placement of the pad profile region.
    RegionOfInterest padProfileRoi = new RegionOfInterest(locatedJointRoi);
    int padProfileRoiLengthAlong = (int)Math.ceil((float)padProfileRoi.getLengthAlong() * fractionOfPadLengthAlong);
    padProfileRoi.setLengthAlong(padProfileRoiLengthAlong);

    int padProfileLengthAcross = (int)Math.ceil((float)padProfileRoi.getLengthAcross() * fractionOfPadLengthAcross);
    padProfileRoi.setLengthAcross(padProfileLengthAcross);
    padProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(padProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, padProfileRoi);
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, padProfileRoi);

    // get background profile
    float[] backgroundGrayLevelProfile;

    if (isStandardBackgroundTechniqueEnabled(jointInspectionData.getSubtype()))
    {
      // CR31674 is not validated. Always use standard technique.

      // Calculate the offsets to the background profile width regions (based on some percentage of the IPD +/- half the profile width).
      Subtype subtype = jointInspectionData.getSubtype();
      final float BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION) / 100.f;
      final int BACKGROUND_PROFILE_WIDTH = 3;
      int locatedJointRoiLengthAcross = locatedJointRoi.getLengthAcross();
      int backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAcross / 2f) +
                                                      ((float)interPadDistanceInPixels * BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD));

      final float backGroundRegionLocationOffsetInMils =  (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET);
      if(backGroundRegionLocationOffsetInMils>0.0f)
      {
        int nanosPerPixel = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
        int backGroundRegionLocationOffsetInPixels = (int)Math.round((float)MathUtil.convertMilsToNanoMetersInteger(backGroundRegionLocationOffsetInMils) /
                                                                 (float)nanosPerPixel);;
        
        backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAcross / 2f) + backGroundRegionLocationOffsetInPixels);
      }
      
      // Calculate the placement of the background profile regions.
      RegionOfInterest backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile1Roi.setLengthAlong(padProfileRoiLengthAlong);
      backgroundProfile1Roi.translateAlongAcross(0, -backgroundProfileRoiOffset);
      backgroundProfile1Roi.setLengthAcross(BACKGROUND_PROFILE_WIDTH);

      RegionOfInterest backgroundProfile2Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile2Roi.setLengthAlong(padProfileRoiLengthAlong);
      backgroundProfile2Roi.translateAlongAcross(0, backgroundProfileRoiOffset);
      backgroundProfile2Roi.setLengthAcross(BACKGROUND_PROFILE_WIDTH);

      // Make sure the background profile ROIs are snapped back onto the image.
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Background Region 1") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
      }
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile2Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Background Region 2") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile2Roi);
      }

      // If applicable, post the background/foreground profile ROIs.
      if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
      {
        if (postProfileRegions)
        {
          MeasurementRegionDiagnosticInfo backgroundProfile1RoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(backgroundProfile1Roi,
                                                  MeasurementRegionEnum.QFN_BACKGROUND_REGION);
          MeasurementRegionDiagnosticInfo backgroundProfile2RoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(backgroundProfile2Roi,
                                                  MeasurementRegionEnum.QFN_BACKGROUND_REGION);

          MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(padProfileRoi,
                                                  MeasurementRegionEnum.QFN_PAD_REGION);

          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       backgroundProfile1RoiDiagInfo,
                                       backgroundProfile2RoiDiagInfo,
                                       padProfileRoiDiagInfo);
        }
      }

      // Measure the background profiles.
      float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
      float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundProfile2Roi);

      // Create an average background gray level profile.
      Assert.expect(backgroundProfile1.length == backgroundProfile2.length);
      backgroundGrayLevelProfile = new float[backgroundProfile1.length];
      for (int i = 0; i < backgroundGrayLevelProfile.length; ++i)
      {
        backgroundGrayLevelProfile[i] = (backgroundProfile1[i] + backgroundProfile2[i]) / 2f;
      }
    }
    else
    {
      // CR31674 is not validated. Always use standard technique.

      // Percentile Background Profile
      RegionOfInterest percentileBackgroundRoi = new RegionOfInterest(padProfileRoi);
      percentileBackgroundRoi.scaleFromCenterAlongAcross(1.0, 2.0);

      // If applicable, post the background/foreground profile ROIs.
      if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
      {
        if (postProfileRegions)
        {
          MeasurementRegionDiagnosticInfo backgroundRoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(percentileBackgroundRoi,
                                                  MeasurementRegionEnum.QFN_BACKGROUND_REGION);

          MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(padProfileRoi,
                                                  MeasurementRegionEnum.QFN_PAD_REGION);

          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       backgroundRoiDiagInfo,
                                       padProfileRoiDiagInfo);
        }
      }
      // Measure the background profile.
      backgroundGrayLevelProfile = ImageFeatureExtraction.orderStatisticProfile(image, percentileBackgroundRoi, 0.85f);
   }


    // Create a thickness profile using the pad profile as foreground and the background profile as background.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(padGrayLevelProfile, backgroundGrayLevelProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      smoothingKernelLength);

    return new Pair<RegionOfInterest, float[]>(padProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
   * Creates a new profile by finding the maximum area within a specified region window of the passed profile.
   *
   * @author George Booth
   */
  private float[] createWorkingProfileFromArea(float[] fullWidthPadThicknessProfile, float filletRegionLimitFraction)
  {
    // return the whole profile if region limit = 100%
    if (filletRegionLimitFraction >= 1.0F)
    {
      float[] newProfile = fullWidthPadThicknessProfile;
      return newProfile;
    }
    // find region in profile with maximum area
    float profileLength = fullWidthPadThicknessProfile.length;
    int maxFilletLengthInPix = (int)(profileLength * filletRegionLimitFraction);
    int leftMarker = 0;
    int rightMarker = maxFilletLengthInPix - 1;
    float currentArea = 0.0F;
    for (int i = leftMarker; i <= rightMarker; i++)
    {
      currentArea += fullWidthPadThicknessProfile[i];
    }
    float bestArea = currentArea;
    int bestLeft = leftMarker;
    int bestRight = rightMarker;
    int last = fullWidthPadThicknessProfile.length - 1;
    for (leftMarker++, rightMarker++; rightMarker <= last; leftMarker++, rightMarker++)
    {
      currentArea -= fullWidthPadThicknessProfile[leftMarker - 1];
      currentArea += fullWidthPadThicknessProfile[rightMarker];
      if (currentArea > bestArea)
      {
        bestArea = currentArea;
        bestLeft = leftMarker;
        bestRight = rightMarker;
      }
    }
    int newProfileLength = bestRight - bestLeft + 1;
    float[] newProfile = new float[newProfileLength];
    for (int i = 0, j = bestLeft; i < newProfileLength; i++, j++)
    {
      newProfile[i] = fullWidthPadThicknessProfile[j];
    }
    return newProfile;
  }

  /**
   * This function returns an average curvature for all of the points within a specified
   * region (bounded by index1 and index2).  This provides a general measure of how "concave-up"
   * or "concave-down" a region is, on average.
   * An average curvature near to zero may indicate that a region is relatively flat, or it
   * may indicate that the curvature in the"concave-down" regions balances out the curvature
   * in the "concave-up regions".
   *
   * @author George Booth
   */
  private float getAverageCurvature(float[] curvatureProfile, int index1, int index2)
  {
    Assert.expect(curvatureProfile != null);
    Assert.expect(curvatureProfile.length > 0);

    int firstIndex = 0;
    int lastIndex = 0;
    if (index1 <= index2)
    {
     firstIndex = index1;
     lastIndex = index2;
    }
    else
    {
      firstIndex = index2;
      lastIndex = index1;
    }
    Assert.expect(firstIndex >= 0);
    Assert.expect(lastIndex <= curvatureProfile.length);

    // compute average curvature over the interval
    float curvatureSum = ProfileUtil.findAreaUnderProfileInterval(
      curvatureProfile, firstIndex, lastIndex);
    float curvatureAverage = curvatureSum / (float)(lastIndex - firstIndex + 1);
    return curvatureAverage;
  }

  /**
   * Searches in the heel search region for the first relatively flat area. This is the first minima
   * past the heel corner.
   *
   * @author George Booth
   */
  private int locateHeelPlateauIndex(float[] smoothedFirstDerivativeProfile, int featureLocationStart, int heelCorner, int heelSearchDistance)
  {
    Assert.expect(smoothedFirstDerivativeProfile != null);
    Assert.expect(featureLocationStart >= 0);
    Assert.expect(heelCorner >= featureLocationStart && heelCorner < featureLocationStart + heelSearchDistance);
    Assert.expect(heelSearchDistance > 0);
    Assert.expect(featureLocationStart + heelSearchDistance < smoothedFirstDerivativeProfile.length);

    int plateauIndex = _UNUSED_BIN;
    // look for first minima or zero crossing past the heel corner in the search region.
    float currentMinimumValue = smoothedFirstDerivativeProfile[heelCorner];
    for (int i = heelCorner + 1; i < featureLocationStart + heelSearchDistance; i++)
    {
      if (smoothedFirstDerivativeProfile[i] > currentMinimumValue ||
          smoothedFirstDerivativeProfile[i] <= 0.0F)
      {
        // increasing slope or zero crossing
        plateauIndex = i - 1;
        break;
      }
      else
      {
        // decreasing slope
        currentMinimumValue = smoothedFirstDerivativeProfile[i];
      }
    }
    return plateauIndex;
  }

  /**
   * Searches "around" the heel edge for the corner of the heel. The corner
   * is where the curvature reaches its lowest value after the heel edge.
   *
   * @author George Booth
   */
  private int locateHeelCornerIndex(float[] curvatureProfile, int featureLocationStart, int heelSearchDistance)
  {
    Assert.expect(curvatureProfile != null);
    Assert.expect(featureLocationStart >= 0 && featureLocationStart < curvatureProfile.length);
    Assert.expect(heelSearchDistance > 0);

    int stop = Math.min(featureLocationStart + heelSearchDistance, curvatureProfile.length);

    int cornerIndex = ArrayUtil.minIndex(curvatureProfile, featureLocationStart, stop);

    // make sure min index has a negative value (downward curve)
    if (curvatureProfile[cornerIndex] < 0.0F)
      return cornerIndex;
    else
      return _UNUSED_BIN;
  }

  /**
   * This function measures the "sharpness" of the heel transition by searching the heel region for
   * the minimum and maximum curvatures.  A sharper transition will have a larger maximum and minimum
   * curvature measurements (because the profile "bends" more quickly.)
   * @author George Booth
   */
  private float measureHeelSharpness(float[] curvatureProfile, int featureLocationStart, int heelSearchDistance)
  {
    Assert.expect(curvatureProfile != null);
    Assert.expect(featureLocationStart >= 0 && featureLocationStart < curvatureProfile.length);

    int stop = Math.min(featureLocationStart + heelSearchDistance + 1, curvatureProfile.length);

    // Find the bin number of the max thickess within the search distance.
    int maxCurvatureIndex = ArrayUtil.maxIndex(curvatureProfile, featureLocationStart, stop);
    int minCurvatureIndex = ArrayUtil.minIndex(curvatureProfile, featureLocationStart, stop);
    float heelSharpness = curvatureProfile[maxCurvatureIndex] - curvatureProfile[minCurvatureIndex];
    return heelSharpness;
  }

  /**
   * Measures a thickness profile around an index.  The profile's ROI spans the pad length across
   * and is oriented orthogonally to the pad.
   *
   * The returned profile is also smoothed.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedAcrossProfile(Image image,
                                                                       SliceNameEnum sliceNameEnum,
                                                                       JointInspectionData jointInspectionData,
                                                                       RegionOfInterest padProfileRoi,
                                                                       float[] smoothedPadThicknessProfile,
                                                                       int indexBin,
                                                                       boolean heelRegion,
                                                                       boolean postProfileRegions,
                                                                       int smoothingKernelLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(padProfileRoi != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect(indexBin >= 0f && indexBin < smoothedPadThicknessProfile.length);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Figure out the across profile width.  For now, just use 40% of the pad length across.
    int padLengthAcrossInPixels = locatedJointRoi.getLengthAcross();
    final float FRACTION_OF_PAD_LENGTH_ACROSS_FOR_PROFILE = 0.4f;
    int acrossProfileWidthInPixels = (int)Math.ceil(padLengthAcrossInPixels *
                                                    FRACTION_OF_PAD_LENGTH_ACROSS_FOR_PROFILE);

    // Create the across profile ROI and adjust it's position accordingly.
    RegionOfInterest acrossProfileRoi = new RegionOfInterest(padProfileRoi);
    acrossProfileRoi.setLengthAcross(padLengthAcrossInPixels);
    acrossProfileRoi.setLengthAlong(acrossProfileWidthInPixels);
    int distanceToPadProfileRoiEdge = padProfileRoi.getLengthAlong() / 2;
    acrossProfileRoi.translateAlongAcross(-distanceToPadProfileRoiEdge + indexBin, 0);

    if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
    {
      acrossProfileRoi.setOrientationInDegrees(90);
    }
    else
    {
      acrossProfileRoi.setOrientationInDegrees(0);
    }


    // Make sure the ROI fits within the image.
    boolean acrossProfileRoiFitsInImage = AlgorithmUtil.checkRegionBoundaries(acrossProfileRoi,
                                                                              image,
                                                                              jointInspectionData.getInspectionRegion(),
                                                                              "Across Heel Profile ROI");
    if (acrossProfileRoiFitsInImage == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, acrossProfileRoi);
    }

    // Measure the across gray level profile.
    float[] acrossGrayLevelProfile = ImageFeatureExtraction.profile(image, acrossProfileRoi);

    // Get the estimated background trend profile from the gray level profile.
    float[] acrossBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(acrossGrayLevelProfile);

    // Using the measured gray level profile and estimated background trend profile, create a
    // thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype()); 
    float[] acrossThicknessProfileInMillimeters =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(acrossGrayLevelProfile, acrossBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedAcrossThicknessProfile = ProfileUtil.getSmoothedProfile(acrossThicknessProfileInMillimeters,
                                                                                    smoothingKernelLength);

    // Post a diagnostic showing the profile across ROI.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo profileAcrossRoiDiagInfo;
        if (heelRegion)
        {
          profileAcrossRoiDiagInfo = new MeasurementRegionDiagnosticInfo(
              acrossProfileRoi, MeasurementRegionEnum.QFN_PROFILE_HEEL_ACROSS_REGION);
        }
        else
        {
          profileAcrossRoiDiagInfo = new MeasurementRegionDiagnosticInfo(
              acrossProfileRoi, MeasurementRegionEnum.QFN_PROFILE_CENTER_ACROSS_REGION);
        }
        _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     profileAcrossRoiDiagInfo);
      }
    }

    return new Pair<RegionOfInterest, float[]>(acrossProfileRoi, smoothedAcrossThicknessProfile);
  }


  /**
   * Determines the sum of the maximum leading and trailing edge slopes of an orthogonal profile
   *
   * @author George Booth
   */
  private float measureLeadingTrailingSlopeSum(float[] firstDerivativeProfile)
  {
    Assert.expect(firstDerivativeProfile != null);
    Assert.expect(firstDerivativeProfile.length > 0);

    // find max outside edges and width between them
    int middleIndex = firstDerivativeProfile.length / 2;
    int maxIndexAcross1 = ArrayUtil.maxIndex(firstDerivativeProfile, 0, middleIndex);
    int maxIndexAcross2 = ArrayUtil.minIndex(firstDerivativeProfile, middleIndex, firstDerivativeProfile.length);

    float loadingEdgeSlope = firstDerivativeProfile[maxIndexAcross1];
    //  use negative of downward trailing edge slope
    float trailingEdgeSlope = -(firstDerivativeProfile[maxIndexAcross2]);

    return loadingEdgeSlope + trailingEdgeSlope;
  }

  /**
   * Determines the width across the maximum leading and trailing edge slopes of an orthogonal profile
   *
   * @author George Booth
   */
  private Pair<Float, Float> measureLeadingTrailingEdgesAcross(float[] firstDerivativeProfile)
  {
    Assert.expect(firstDerivativeProfile != null);
    Assert.expect(firstDerivativeProfile.length > 0);

    // find max outside edges and width between them
    int middleIndex = firstDerivativeProfile.length / 2;
    int leadingEdgeIndex = ArrayUtil.maxIndex(firstDerivativeProfile, 0, middleIndex);
    float leadingEdgeSubpixelBin = ProfileUtil.findInterpolatedMaximumIndex(firstDerivativeProfile, leadingEdgeIndex);

    int trailingEdgeIndex = ArrayUtil.minIndex(firstDerivativeProfile, middleIndex, firstDerivativeProfile.length);
    float trailingEdgeSubpixelBin = ProfileUtil.findInterpolatedMaximumIndex(firstDerivativeProfile, trailingEdgeIndex);

    return new Pair<Float, Float>(leadingEdgeSubpixelBin, trailingEdgeSubpixelBin);
  }

  /**
   * Searches from the toe peak to the end of the first derivative profile looking
   * for the first negative slope. This should place the toe peak just beyond the
   * maximum profile value.
   *
   * @author George Booth
   */
  private int locateFallingIndexOfToePeak(float[] smoothedFirstDerivativeProfile, int toePeakIndex)
  {
    Assert.expect(smoothedFirstDerivativeProfile != null);
    Assert.expect(toePeakIndex >= 0 && toePeakIndex < smoothedFirstDerivativeProfile.length);

    int fallingIndex = toePeakIndex;
    for (int i = toePeakIndex; i < smoothedFirstDerivativeProfile.length; i++)
    {
      if (smoothedFirstDerivativeProfile[i] < 0)
      {
        fallingIndex = i;
        break;
      }
    }
    return fallingIndex;
  }

  /**
   * Measures the fillet length based on the distance between the heel and toe edge subpixel bins.
   *
   * @author George Booth
   */
  private float measureFilletLength(float heelEdgeSubpixelBin, float toeEdgeSubpixelBin, final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(MathUtil.fuzzyLessThanOrEquals(heelEdgeSubpixelBin, toeEdgeSubpixelBin));

    // Calculate the fillet length in millimeters.
    float filletLengthInPixels = toeEdgeSubpixelBin - heelEdgeSubpixelBin;
    final float MILLIMETERS_PER_PIXEL = MILIMETER_PER_PIXEL;
    float filletLengthInMillimeters = filletLengthInPixels * MILLIMETERS_PER_PIXEL;

    return filletLengthInMillimeters;
  }

  /**
   * Determines the centroid of the solder volume along the profile.
   *
   * @author George Booth
   */
  private float getCenterOfSolderVolumeInMillimeters(float[] smoothedPadThicknessProfile, String jointName, final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);

    float centroidLocationInPixels = StatisticsUtil.getCentroid(smoothedPadThicknessProfile);
    // for regression tests: if the profile has values of zero, NaN may returned for the centroid.
    if (Float.isNaN(centroidLocationInPixels))
    {
      // return a number near 0
      centroidLocationInPixels = 0.001f;
    }
    float centroidLocationInMillimeters = centroidLocationInPixels * MILIMETER_PER_PIXEL;

    return centroidLocationInMillimeters;
  }

  /**
   * Measures the average thickness value in the specified profile between the specified heel edge bin and
   * "heel edge bin" + "nominal fillet length".
   *
   * @author George Booth
   */
  private float measureFilletThickness(float[] smoothedPadThicknessProfile,
                                         int heelEdgeBin,
                                         int nominalFilletLengthInPixels)
  {
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect(heelEdgeBin >= 0);

    int toeEdgeBinBasedOnNominalFilletLength = heelEdgeBin + nominalFilletLengthInPixels;
    toeEdgeBinBasedOnNominalFilletLength = Math.min(toeEdgeBinBasedOnNominalFilletLength,
                                                    smoothedPadThicknessProfile.length - 1);

    int numberOfBins = (toeEdgeBinBasedOnNominalFilletLength - heelEdgeBin) + 1;
    Assert.expect(numberOfBins >= 1);

    // Calculate the average thickness value.
    float sumOfThicknessValues = ProfileUtil.findAreaUnderProfileInterval(
       smoothedPadThicknessProfile, heelEdgeBin, toeEdgeBinBasedOnNominalFilletLength);
    float averageThickness = sumOfThicknessValues / (float)numberOfBins;

    return averageThickness;
  }

  /**
   * The center index is a specificed distance along the fillet length from the heel edge
   *
   * @author George Booth
   */
  private int locateCenterIndex(int featureLocationStart, int centerDistanceInPixels, int heelIndex, int toeIndex)
  {
    Assert.expect(featureLocationStart >= 0);
    Assert.expect(centerDistanceInPixels >= 0);
    Assert.expect(heelIndex >= featureLocationStart);
    Assert.expect(toeIndex >= heelIndex);

    int centerIndex = featureLocationStart + centerDistanceInPixels;

    // the center must be between the heel and toe
    if (centerIndex < heelIndex)
    {
      centerIndex = heelIndex;
    }

    if (centerIndex > toeIndex)
    {
      centerIndex = toeIndex;
    }

    return centerIndex;
  }

  /**
   * Creates a Collection of MeasurementRegionDiagnosticInfo objects which indicate the positions of the heel edge,
   * heel peak, center, toe edge, and toe peak.
   *
   * @author George Booth
   */
  private Collection<DiagnosticInfo> createMeasurementRegionDiagInfosForProfile(JointInspectionData jointInspectionData,
                                                                                RegionOfInterest padProfileRoi,
                                                                                float[] smoothedPadThicknessProfile,
                                                                                int heelEdgeBin,
                                                                                int heelPeakBin,
                                                                                int centerBin,
                                                                                int toePeakBin,
                                                                                int toeEdgeBin,
                                                                                int solderThicknessBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(padProfileRoi != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);

    // Create the ROIs for the areas of interest on the profile.
    RegionOfInterest heelEdgeRoi = new RegionOfInterest(padProfileRoi);
    heelEdgeRoi.setLengthAlong(1);
    RegionOfInterest heelPeakRoi = new RegionOfInterest(padProfileRoi);
    heelPeakRoi.setLengthAlong(1);
    RegionOfInterest centerRoi = new RegionOfInterest(padProfileRoi);
    centerRoi.setLengthAlong(1);
    RegionOfInterest toePeakRoi = new RegionOfInterest(padProfileRoi);
    toePeakRoi.setLengthAlong(1);
    RegionOfInterest toeEdgeRoi = new RegionOfInterest(padProfileRoi);
    toeEdgeRoi.setLengthAlong(1);
    RegionOfInterest solderThicknessBinRoi = new RegionOfInterest(padProfileRoi);
    solderThicknessBinRoi.setLengthAlong(1);

    // Set the positions of our ROIs based on whether or not a profile reversal is in effect.
    int padProfileRoiLengthAlong = padProfileRoi.getLengthAlong();
    int distanceToRoiEdge = padProfileRoiLengthAlong / 2;
    heelEdgeRoi.translateAlongAcross(-distanceToRoiEdge + heelEdgeBin, 0);
    heelPeakRoi.translateAlongAcross(-distanceToRoiEdge + heelPeakBin, 0);
    centerRoi.translateAlongAcross(-distanceToRoiEdge + centerBin, 0);
    toePeakRoi.translateAlongAcross(-distanceToRoiEdge + toePeakBin, 0);
    toeEdgeRoi.translateAlongAcross(-distanceToRoiEdge + toeEdgeBin, 0);
    solderThicknessBinRoi.translateAlongAcross(-distanceToRoiEdge + solderThicknessBin, 0);

    // Create our MeasurementRegionDiagnosticInfos and add them to our collection.
    Collection<DiagnosticInfo> measurementRegionDiagnosticInfos = new LinkedList<DiagnosticInfo>();
    if (heelEdgeBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(heelEdgeRoi,
                                           MeasurementRegionEnum.QFN_HEEL_EDGE_REGION));
    }
    if (heelPeakBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(heelPeakRoi,
                                           MeasurementRegionEnum.QFN_HEEL_PEAK_REGION));
    }
    if (centerBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(centerRoi,
                                           MeasurementRegionEnum.QFN_CENTER_REGION));
    }
    if (toePeakBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(toePeakRoi,
                                           MeasurementRegionEnum.QFN_TOE_PEAK_REGION));
    }
    if (toeEdgeBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(toeEdgeRoi,
                                           MeasurementRegionEnum.QFN_TOE_EDGE_REGION));
    }
    if (solderThicknessBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(solderThicknessBinRoi,
                                           MeasurementRegionEnum.QFN_SOLDER_THICKNESS_REGION));
    }

    return measurementRegionDiagnosticInfos;
  }

  /**
   * Creates a 'labeled' version of the specified pad thickness profile that accentuates the
   * various features we measured.
   *
   * @author George Booth
   */
  private ProfileDiagnosticInfo createLabeledPadThicknessProfileDiagInfo(JointInspectionData jointInspectionData,
                                                                         float[] smoothedPadThicknessProfile,
                                                                         int heelSearchStartBin,
                                                                         int heelSearchStopBin,
                                                                         int heelEdgeBin,
                                                                         int heelPeakBin,
                                                                         int centerBin,
                                                                         int toePeakBin,
                                                                         int toeEdgeBin,
                                                                         int upwardCurvatureStartBin,
                                                                         int upwardCurvatureStopBin,
                                                                         int solderThicknessBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);

    Subtype subtype = jointInspectionData.getSubtype();
    final float NOMINAL_TOE_THICKNESS_IN_MILLIS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS);
    float profileMin = 0f;
    float profileMax = NOMINAL_TOE_THICKNESS_IN_MILLIS * 1.5f;
    LocalizedString padThicknessProfileLabelLocalizedString =
        new LocalizedString("ALGDIAG_QFN_MEASUREMENT_PAD_THICKNESS_PROFILE_LABEL_KEY",
                            null);
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        createProfileSubrangeToMeasurementRegionEnumMap(smoothedPadThicknessProfile,
                                                        heelSearchStartBin,
                                                        heelSearchStopBin,
                                                        heelEdgeBin,
                                                        heelPeakBin,
                                                        centerBin,
                                                        toePeakBin,
                                                        toeEdgeBin,
                                                        upwardCurvatureStartBin,
                                                        upwardCurvatureStopBin,
                                                        solderThicknessBin);
    ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
      ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                       ProfileTypeEnum.SOLDER_PROFILE,
                                                                       padThicknessProfileLabelLocalizedString,
                                                                       profileMin,
                                                                       profileMax,
                                                                       smoothedPadThicknessProfile,
                                                                       profileSubrangeToMeasurementRegionEnumMap,
                                                                       MeasurementUnitsEnum.MILLIMETERS);

    return labeledPadThicknessProfileDiagInfo;
  }

  /**
   * Creates a 'labeled' version of the specified pad thickness profile that accentuates the
   * various features we measured.
   *
   * @author George Booth
   */
  private ProfileDiagnosticInfo createLabeledHeelAcrossThicknessProfileDiagInfo(JointInspectionData jointInspectionData,
                                                                                float[] smoothedHeelAcrossThicknessProfile,
                                                                                int leadingEdge,
                                                                                int trailingEdge)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedHeelAcrossThicknessProfile != null);
    Assert.expect(smoothedHeelAcrossThicknessProfile.length > 0);

    Subtype subtype = jointInspectionData.getSubtype();
    final float NOMINAL_TOE_THICKNESS_IN_MILLIS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS);
    float profileMin = 0f;
    float profileMax = NOMINAL_TOE_THICKNESS_IN_MILLIS * 1.5f;
    LocalizedString padThicknessProfileLabelLocalizedString =
        new LocalizedString("ALGDIAG_QFN_MEASUREMENT_HEEL_ACROSS_THICKNESS_PROFILE_LABEL_KEY",
                            null);
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        createAcrossProfileSubrangeToMeasurementRegionEnumMap(smoothedHeelAcrossThicknessProfile,
                                                              leadingEdge, trailingEdge);
    ProfileDiagnosticInfo labeledHeelAcrossThicknessProfileDiagInfo =
      ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                       ProfileTypeEnum.SOLDER_PROFILE,
                                                                       padThicknessProfileLabelLocalizedString,
                                                                       profileMin,
                                                                       profileMax,
                                                                       smoothedHeelAcrossThicknessProfile,
                                                                       profileSubrangeToMeasurementRegionEnumMap,
                                                                       MeasurementUnitsEnum.MILLIMETERS);

    return labeledHeelAcrossThicknessProfileDiagInfo;
  }

  /**
   * Creates a 'labeled' version of the specified pad thickness profile that accentuates the
   * various features we measured.
   *
   * @author George Booth
   */
  private ProfileDiagnosticInfo createLabeledCenterAcrossThicknessProfileDiagInfo(JointInspectionData jointInspectionData,
                                                                                  float[] smoothedCenterAcrossThicknessProfile,
                                                                                  int leadingEdge,
                                                                                  int trailingEdge)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedCenterAcrossThicknessProfile != null);
    Assert.expect(smoothedCenterAcrossThicknessProfile.length > 0);

    Subtype subtype = jointInspectionData.getSubtype();
    final float NOMINAL_TOE_THICKNESS_IN_MILLIS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS);
    float profileMin = 0f;
    float profileMax = NOMINAL_TOE_THICKNESS_IN_MILLIS * 1.5f;
    LocalizedString padThicknessProfileLabelLocalizedString =
        new LocalizedString("ALGDIAG_QFN_MEASUREMENT_CENTER_ACROSS_THICKNESS_PROFILE_LABEL_KEY",
                            null);
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        createAcrossProfileSubrangeToMeasurementRegionEnumMap(smoothedCenterAcrossThicknessProfile,
                                                              leadingEdge, trailingEdge);
    ProfileDiagnosticInfo labeledHeelAcrossThicknessProfileDiagInfo =
      ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                       ProfileTypeEnum.SOLDER_PROFILE,
                                                                       padThicknessProfileLabelLocalizedString,
                                                                       profileMin,
                                                                       profileMax,
                                                                       smoothedCenterAcrossThicknessProfile,
                                                                       profileSubrangeToMeasurementRegionEnumMap,
                                                                       MeasurementUnitsEnum.MILLIMETERS);

    return labeledHeelAcrossThicknessProfileDiagInfo;
  }

  /**
   * @author George Booth
   */
  protected Map<Pair<Integer, Integer>, MeasurementRegionEnum>
      createProfileSubrangeToMeasurementRegionEnumMap(float[] smoothedPadThicknessProfile,
                                                      int heelSearchStartBin,
                                                      int heelSearchStopBin,
                                                      int heelEdgeBin,
                                                      int heelPeakBin,
                                                      int centerBin,
                                                      int toePeakBin,
                                                      int toeEdgeBin,
                                                      int upwardCurvatureStartBin,
                                                      int upwardCurvatureStopBin,
                                                      int solderThicknessBin)
  {
    Assert.expect(smoothedPadThicknessProfile != null);

    // Create a ProfileDiagnosticInfo that accentuates the points of interest in the pad thickness profile.
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer, Integer>, MeasurementRegionEnum>();
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, smoothedPadThicknessProfile.length),
                                                  MeasurementRegionEnum.QFN_PAD_REGION);

    if (upwardCurvatureStartBin != _UNUSED_BIN && upwardCurvatureStopBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(upwardCurvatureStartBin, upwardCurvatureStopBin),
                                                    MeasurementRegionEnum.QFN_UPWARD_CURVATURE_SEARCH_REGION);
    }
    if (heelSearchStartBin != _UNUSED_BIN && heelSearchStopBin != _UNUSED_BIN)
    {
      Assert.expect(heelSearchStartBin >= 0);
      Assert.expect(heelSearchStopBin > heelSearchStartBin && heelSearchStopBin < smoothedPadThicknessProfile.length);
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(heelSearchStartBin, heelSearchStopBin),
                                                    MeasurementRegionEnum.QFN_HEEL_SEARCH_REGION);
    }
    if (heelEdgeBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(heelEdgeBin, heelEdgeBin + 1),
                                                    MeasurementRegionEnum.QFN_HEEL_EDGE_REGION);
    }
    if (heelPeakBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(heelPeakBin, heelPeakBin + 1),
                                                    MeasurementRegionEnum.QFN_HEEL_PEAK_REGION);
    }
    if (centerBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(centerBin, centerBin + 1),
                                                    MeasurementRegionEnum.QFN_CENTER_REGION);
    }
    if (toeEdgeBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(toeEdgeBin, toeEdgeBin + 1),
                                                    MeasurementRegionEnum.QFN_TOE_EDGE_REGION);
    }
    if (toePeakBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(toePeakBin, toePeakBin + 1),
                                                    MeasurementRegionEnum.QFN_TOE_PEAK_REGION);
    }
    if (solderThicknessBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(solderThicknessBin, solderThicknessBin + 1),
                                                    MeasurementRegionEnum.QFN_SOLDER_THICKNESS_REGION);
    }

    return profileSubrangeToMeasurementRegionEnumMap;
  }

  /**
   * @author George Booth
   */
  private Map<Pair<Integer, Integer>, MeasurementRegionEnum>
      createAcrossProfileSubrangeToMeasurementRegionEnumMap(float[] smoothedAcrossThicknessProfile,
                                                            int leadingEdge,
                                                            int trailingEdge)
  {
    Assert.expect(smoothedAcrossThicknessProfile != null);

    // Create a ProfileDiagnosticInfo that accentuates the points of interest in the pad thickness profile.
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer, Integer>, MeasurementRegionEnum>();
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, smoothedAcrossThicknessProfile.length),
                                                    MeasurementRegionEnum.QFN_PAD_REGION);

    if (leadingEdge != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>((int)leadingEdge, (int)leadingEdge + 1),
                                                    MeasurementRegionEnum.QFN_ACROSS_LEADING_EDGE_REGION);
    }

    if (trailingEdge != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>((int)trailingEdge, (int)trailingEdge + 1),
                                                    MeasurementRegionEnum.QFN_ACROSS_TRAILING_EDGE_REGION);
    }

    return profileSubrangeToMeasurementRegionEnumMap;
  }

  /**
   * @author George Booth
   */
  private boolean isStandardBackgroundTechniqueEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // CR31674 is not validated.  Always use standard technique.
    if (true)
      return true;

    boolean standardBackgroundTechniqueEnabled = false;
    String backgroundTechniqueSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_BACKGROUND_TECHNIQUE);
    if (backgroundTechniqueSetting.equals("Standard"))
    {
      standardBackgroundTechniqueEnabled = true;
    }
    else if (backgroundTechniqueSetting.equals("Percentile"))
    {
      standardBackgroundTechniqueEnabled = false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected background technique setting: " + backgroundTechniqueSetting);
    }

    return standardBackgroundTechniqueEnabled;
  }

  /**
   * @author George Booth
   */
  private boolean isHeelVoidCompensationEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    boolean heelVoidCompensationEnabled = false;
    String heelVoidCompensationSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_VOID_COMPENSATION);
    if (heelVoidCompensationSetting.equals("On"))
    {
      heelVoidCompensationEnabled = true;
    }
    else if (heelVoidCompensationSetting.equals("Off"))
    {
      heelVoidCompensationEnabled = false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected heel void compensation setting: " + heelVoidCompensationSetting);
    }

    return heelVoidCompensationEnabled;
  }

  /**
   * @author George Booth
   */
  private boolean isFilletLengthTechniqueSetToThickness(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String filletLengthTechniqueSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_FILLET_LENGTH_TECHNIQUE);
    if (filletLengthTechniqueSetting.equals("Thickness"))
    {
      return true;
    }
    else if (filletLengthTechniqueSetting.equals("Slope"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected fillet length setting: " + filletLengthTechniqueSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private boolean isFeatureLocationTechniqueSetToProfileEdges(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String featureLocationTechniqueSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_FEATURE_LOCATION_TECHNIQUE);
    if (featureLocationTechniqueSetting.equals("Profile Edges"))
    {
      return true;
    }
    else if (featureLocationTechniqueSetting.equals("Fillet Edges"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected feature location setting: " + featureLocationTechniqueSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private boolean isToeLocationTechniqueSetToFixedDistance(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String featureLocationTechniqueSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_LOCATION_TECHNIQUE);
    if (featureLocationTechniqueSetting.equals("Fixed Distance"))
    {
      return true;
    }
    else if (featureLocationTechniqueSetting.equals("Percent of Fillet Length"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected toe location setting: " + featureLocationTechniqueSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private int getSmoothingKernelLength(Subtype subtype)
  {
    Assert.expect(subtype != null);

    int length = ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_SMOOTHING_KERNEL_LENGTH)).intValue();
    return length;
  }

  /**
   * @author George Booth
   */
  private boolean isCenterLocationTechniqueSetToPercentHeelToeDistance(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String featureLocationTechniqueSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_LOCATION_TECHNIQUE);
    if (featureLocationTechniqueSetting.equals("Percent of Heel-Toe Length"))
    {
      return true;
    }
    else if (featureLocationTechniqueSetting.equals("Percent of Fillet Length"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected center location setting: " + featureLocationTechniqueSetting);
      return false;
    }
  }

  /**
   * Gets the heel thickness measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getHeelThicknessMeasurement(JointInspectionData jointInspectionData,
                                                             SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement heelThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS);

    return heelThicknessMeasurement;
  }

  /**
   * Gets the center thickness measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getCenterThicknessMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement centerThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS);

    return centerThicknessMeasurement;
  }

  /**
   * Gets the slope changes measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getSumOfSlopeChangesMeasurement(JointInspectionData jointInspectionData,
                                                            SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES);

    return measurement;
  }

  /**
   * Gets the heel across width measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getHeelAcrossWidthMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_WIDTH);

    return measurement;
  }

  /**
   * Gets the heel across slope sum measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getHeelAcrossSlopeSumMeasurement(JointInspectionData jointInspectionData,
                                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM);

    return measurement;
  }

  /**
   * Gets the heel across leading-trailing slope sum measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getHeelAcrossLeadingTrailingSlopeSumMeasurement(JointInspectionData jointInspectionData,
                                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM);

    return measurement;
  }

  /**
   * Gets the heel across width measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getHeelSharpnessMeasurement(JointInspectionData jointInspectionData,
                                                             SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_HEEL_SHARPNESS);

    return measurement;
  }

  /**
   * Gets the center slope measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getCenterSlopeMeasurement(JointInspectionData jointInspectionData,
                                                           SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE);

    return measurement;
  }

  /**
   * Gets the center across width measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getCenterAcrossWidthMeasurement(JointInspectionData jointInspectionData,
                                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_WIDTH);

    return measurement;
  }

  /**
   * Gets the center across slope sum measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getCenterAcrossSlopeSumMeasurement(JointInspectionData jointInspectionData,
                                                                    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM);

    return measurement;
  }

  /**
   * Gets the center across leading-trailing slope sum measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getCenterAcrossLeadingTrailingSlopeSumMeasurement(JointInspectionData jointInspectionData,
                                                                      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM);

    return measurement;
  }

  /**
   * Gets the upward curvature measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getUpwardCurvatureMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_UPWARD_CURVATURE);

    return measurement;
  }

  /**
   * Gets the heel slope measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getHeelSlopeMeasurement(JointInspectionData jointInspectionData,
                                                         SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement heelSlopeMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE);

    return heelSlopeMeasurement;
  }

  /**
   * Gets the heel slope measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getHeelToeSlopeSumMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM);

    return measurement;
  }

  /**
   * Gets the toe thickness measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getToeThicknessMeasurement(JointInspectionData jointInspectionData,
                                                            SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement toeThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_TOE_THICKNESS);

    return toeThicknessMeasurement;
  }

  /**
   * Gets the toe slope measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getToeSlopeMeasurement(JointInspectionData jointInspectionData,
                                                        SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE);

    return measurement;
  }

  /**
   * Gets the fillet length measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getFilletLengthMeasurement(JointInspectionData jointInspectionData,
                                                            SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement filletLengthMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH);

    return filletLengthMeasurement;
  }

  /**
   * Gets the fillet thickness measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getFilletThicknessMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement filletThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_FILLET_THICKNESS);

    return filletThicknessMeasurement;
  }

  /**
   * Gets the joint offset from CAD measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getJointOffsetFromCadMeasurement(JointInspectionData jointInspectionData,
                                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement jointOffsetFromCadMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_JOINT_OFFSET_FROM_CAD);

    return jointOffsetFromCadMeasurement;
  }

  /**
   * Gets the neighbor fillet length difference.
   *
   * @author George Booth
   */
  public static JointMeasurement getFilletLengthDifferenceMeasurement(JointInspectionData jointInspectionData,
                                                                      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement neighborDifferenceMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_NEIGHBOR_LENGTH_DIFFERENCE);

    return neighborDifferenceMeasurement;
  }

  /**
   * Gets the solder thickness at location measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getSolderThicknessAtLocationMeasurement(JointInspectionData jointInspectionData,
                                                                         SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement solderThicknessAtLocationMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_AT_LOCATION);

    return solderThicknessAtLocationMeasurement;
  }

  /**
   * Gets the center of solder volume [percent of pad length] measurement.
   *
   * @author George Booth
   */
  public static JointMeasurement getCenterOfSolderVolumeMeasurement(JointInspectionData jointInspectionData,
                                                                    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement centerOfSolderVolumeMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_MEASUREMENT_CENTER_OF_SOLDER_VOLUME);

    return centerOfSolderVolumeMeasurement;
  }

  /**
   * Updates the learned nominals for the QFN Measurement algorithm.
   *
   * @author George Booth
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    // We can't update nominals if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, 0);

      return;
    }
    
    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);

    // Dump the existing measurement data from the database.
    subtype.deleteLearnedMeasurementData();

    boolean updateNominalSettingsOnly = true;

    // QFN only runs (and learns) on the Pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Measure the profiles and derivative profiles (both full and fractional pad length across) for each of of the joints.
    Map<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning> profileDataMap =
        measurePadThicknessProfilesForLearning(subtype, typicalBoardImages, padSlice);

    // run learning on all pertinent measurements for all QFN joints
    learnQFNJointMeasurements(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);

    // Update the "nominal fillet length" and "nominal fillet thickness" setting.
    updateNominalFilletLengthAndThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "heel search distance", "nominal heel position" and "nominal heel thickness" settings.
    updateHeelSearchDistanceAndNominalThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "toe offset" and "nominal toe thickness" settings.
    updateToeOffsetAndNominalThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "center offset" and "nominal center thickness" settings.
    updateCenterOffsetAndNominalThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "nominal center of solder volume" setting.
    updateCenterOfSolderVolume(subtype, padSlice);

    // Raise a warning if we don't have enough data points.
    int numberOfProfiles = profileDataMap.size();
    final int MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING = 10;
    if (numberOfProfiles < MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfProfiles);
    }
    
    // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
    if (profileDataMap != null)
    {
      for(Map.Entry<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning> entry : profileDataMap.entrySet())
      {
        Pair<JointInspectionData, ImageSetData> jointInspectionDataPair = entry.getKey();
        jointInspectionDataPair.getFirst().getJointInspectionResult().clearMeasurements();
        jointInspectionDataPair.getFirst().clearJointInspectionResult();
      }
      
      profileDataMap.clear();
      profileDataMap = null;
    }
  }

  /**
   * Learns the algorithm settings for QFN Measurement.
   *
   * @author George Booth
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // We can't learn if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, 0);

      return;
    }

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Let Locator learn its stuff.
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because too complicated
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);

    boolean updateNominalSettingsOnly = false;

    // QFN only runs (and learns) on the Pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Measure the profiles and derivative profiles (both full and fractional pad length across) for each of of the joints.
    Map<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning> profileDataMap =
      measurePadThicknessProfilesForLearning(subtype, typicalBoardImages, padSlice);

    // run learning on all pertinent measurements for all QFN joints
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because consider if the profile is smoothed, outlier is already excluded.
    learnQFNJointMeasurements(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);
    
    //Siew Yeng - XCR-2139 - Learn slope settings
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      //Learn joint across measurement
      learnQFNJointAcrossMeasurements(subtype, typicalBoardImages, padSlice, profileDataMap);
    }

    // Update the "nominal fillet length" and "nominal fillet thickness" setting.
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier inside
    updateNominalFilletLengthAndThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "heel search distance", "nominal heel position" and "nominal heel thickness" settings.
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier inside
    updateHeelSearchDistanceAndNominalThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "toe offset" and "nominal toe thickness" settings.
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier inside
    updateToeOffsetAndNominalThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "center offset" and "nominal center thickness" settings.
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier inside
    updateCenterOffsetAndNominalThickness(subtype, padSlice, updateNominalSettingsOnly);

    // Update the "nominal center of solder volume" setting.
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier inside
    updateCenterOfSolderVolume(subtype, padSlice);

    // Raise a warning if we don't have enough data points.
    int numberOfProfiles = profileDataMap.size();
    final int MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING = 10;
    if (numberOfProfiles < MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfProfiles);
    }
  }

  /**
   * Learns all the required QFN measurement settings.
   *
   * @author George Booth
   */
  private void learnQFNJointMeasurements(Subtype subtype,
                                         SliceNameEnum slice,
                                         Map<Pair<JointInspectionData, ImageSetData>,
                                         QFNProfileDataForLearning> profileDataMap,
                                         final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      QFNProfileDataForLearning profileData = mapEntry.getValue();
      float[] smoothedProfile = profileData.getSmoothedProfileWithVoidComp();
      float[] firstDerivativeProfile = profileData.getSmoothedFirstDerivativeProfile();
      float[] secondDerivativeProfile = profileData.getSmoothedSecondDerivativeProfile();

      // Locate the heel edge (max profile thickness determined in locateHeelEdge())
      final float HEEL_EDGE_THICKNESS_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(smoothedProfile,
                                                 HEEL_EDGE_THICKNESS_FRACTION_OF_MAX, false);

      // get the integer index
      int heelEdgeIndex = Math.max(Math.round(heelEdgeSubpixelBin), 0);

      // Locate the toe edge
      int maxThicknessIndex = ArrayUtil.maxIndex(smoothedProfile);
      float maxThicknessValue = smoothedProfile[maxThicknessIndex];
      final float TOE_EDGE_THICKNESS_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float targetToeEdgeThickness = maxThicknessValue * TOE_EDGE_THICKNESS_FRACTION_OF_MAX;
      float toeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(smoothedProfile,
                                               targetToeEdgeThickness, maxThicknessIndex);

      // make sure toe edge is >= heel edge
      if (toeEdgeSubpixelBin < heelEdgeSubpixelBin)
      {
        toeEdgeSubpixelBin = heelEdgeSubpixelBin;
      }

      // get the integer index
      int toeEdgeIndex = Math.min(Math.round(toeEdgeSubpixelBin), smoothedProfile.length - 1);

      // find heel derivative index
      float heelSlopeSubpixelBin = AlgorithmFeatureUtil.findMaximumHeelSlopeBin(firstDerivativeProfile,
                                                                                maxThicknessIndex,
                                                                                heelEdgeIndex,
                                                                                toeEdgeIndex);

      // find toe negative derivative index
      float toeNegativeSlopeSubpixelBin = AlgorithmFeatureUtil.findMinimumToeSlopeBin(firstDerivativeProfile,
                                                                                      maxThicknessIndex,
                                                                                      heelEdgeIndex,
                                                                                      toeEdgeIndex);

      // make sure toe edge is >= heel edge
      if (toeNegativeSlopeSubpixelBin < heelSlopeSubpixelBin)
      {
        toeNegativeSlopeSubpixelBin = heelSlopeSubpixelBin;
      }

      // The fillet length and some search regions are based on the user selectable "Fillet Length Technique":
      // "Slope" - (default) bins with maximum heel and toe slopes
      // "Thickness" - bins where profile exceeds the heel and toe search thickness
      float filletEdgeIndex1 = heelSlopeSubpixelBin;
      float filletEdgeIndex2 = toeNegativeSlopeSubpixelBin;
      if (isFilletLengthTechniqueSetToThickness(subtype))
      {
        filletEdgeIndex1 = heelEdgeSubpixelBin;
        filletEdgeIndex2 = toeEdgeSubpixelBin;
      }

      // The feature location search region is based on the user selectable "Feature Location Technique":
      // "Fillet Edges" - (default) search the fillet length as defined above
      // "Profile Edges" - search the entire profile
      int featureLocationStartIndex = Math.max(Math.round(filletEdgeIndex1), 0);
      int featureLocationStopIndex = Math.min(Math.round(filletEdgeIndex2), smoothedProfile.length - 1);
      if (isFeatureLocationTechniqueSetToProfileEdges(subtype))
      {
        featureLocationStartIndex = 0;
        featureLocationStopIndex = smoothedProfile.length - 1;
      }

      float featureLocationRegion = (float)(featureLocationStopIndex - featureLocationStartIndex);

      // get the curvature profile
      float[] curvatureProfile = profileData.getCurvatureProfile();

/*  CR33010 Center of Solder Volume measurement commented until proven useful

      // measure center of solder volume (center of area under entire profile)
      float centerOfSolderVolumeInPixelsInMillimeters = getCenterOfSolderVolumeInMillimeters(smoothedProfile, "");

      // figure percentage of pad length along
      int padLengthAlongInPixels = locatedJointRoi.getLengthAlong();
      float padLengthAlongInMillimeters = (float)padLengthAlongInPixels * AlgorithmUtil.getMillimetersPerPixel();
      float centerOfSolderVolumePercentOfPadLength = (centerOfSolderVolumeInPixelsInMillimeters / padLengthAlongInMillimeters) * 100.0f;

      // Save the center of solder volume measurement to the database.
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_LEARNING_CENTER_OF_SOLDER_VOLUME,
                                    centerOfSolderVolumePercentOfPadLength,
                                    true,
                                    true));
*/

      // Locate the heel
      final float HEEL_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      int heelSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                          HEEL_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);

      // make sure search distance is resonable
      if (heelSearchDistanceInPixels < 1)
      {
        heelSearchDistanceInPixels = 1;
      }
      if (featureLocationStartIndex + heelSearchDistanceInPixels >= smoothedProfile.length)
      {
        heelSearchDistanceInPixels = smoothedProfile.length - featureLocationStartIndex - 1;
      }

      int heelPlateauBin = _UNUSED_BIN;
      // locate the heel corner (region of maximum negative curvature in heel asearch region)
      int heelCornerBin = locateHeelCornerIndex(curvatureProfile, featureLocationStartIndex, heelSearchDistanceInPixels);
      if (heelCornerBin > _UNUSED_BIN)
      {
        // locate the heel plateau (first relatively flat area in heel search region)
        heelPlateauBin = locateHeelPlateauIndex(firstDerivativeProfile, featureLocationStartIndex, heelCornerBin, heelSearchDistanceInPixels);
        if (heelPlateauBin == _UNUSED_BIN)
        {
          heelPlateauBin = heelCornerBin;
        }
      }

      // use 5DX method (max value in heel search region)
      int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(smoothedProfile,
                                                            featureLocationStartIndex,
                                                            heelSearchDistanceInPixels);

      int heelBin = heelPlateauBin;
      if (heelBin == _UNUSED_BIN)
      {
        heelBin = heelPeakBin;
      }

      // Locate the toe (default - % of fillet length)
      final float TOE_DISTANCE_FRACTION =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET) / 100.f;
      int toeDistanceInPixels = Math.round(featureLocationRegion * TOE_DISTANCE_FRACTION);
      if (isToeLocationTechniqueSetToFixedDistance(subtype))
      {
        final float TOE_FIXED_DISTANCE =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_FIXED_DISTANCE);
        toeDistanceInPixels = MathUtil.convertMillimetersToPixelsUsingCeil(TOE_FIXED_DISTANCE,
                                                                           MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel());
      }

      int toeStartIndex = featureLocationStartIndex;
      if (isToeLocationTechniqueSetToFixedDistance(subtype))
      {
        toeStartIndex = heelBin;
      }
      int toePeakBin = AlgorithmFeatureUtil.locateToePeak(smoothedProfile, toeStartIndex, toeDistanceInPixels);
      // make sure toe is >= heel
      if (toePeakBin < heelBin)
      {
        toePeakBin = heelBin;
      }

      float heelToeDistance = (float)(toePeakBin - heelBin);

      // locate the center (default - % of fillet length)
      final float CENTER_DISTANCE_FRACTION =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET) / 100.f;
      int centerDistanceInPixels = Math.round(featureLocationRegion * CENTER_DISTANCE_FRACTION);
      if (isCenterLocationTechniqueSetToPercentHeelToeDistance(subtype))
      {
        centerDistanceInPixels = Math.round(heelToeDistance * CENTER_DISTANCE_FRACTION);
      }

      int centerStartIndex = featureLocationStartIndex;
      if (isCenterLocationTechniqueSetToPercentHeelToeDistance(subtype))
      {
        centerStartIndex = heelBin;
      }
      int centerBin = locateCenterIndex(centerStartIndex, centerDistanceInPixels, heelBin, toePeakBin);

      // Save the "fillet thickness" measurement to the database.
      float filletThickness = measureFilletThickness(smoothedProfile,
                                                     featureLocationStartIndex, featureLocationStopIndex);
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because consider if the profile is smoothed, outlier is already excluded.
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_FILLET_THICKNESS,
                                    filletThickness,
                                    true,
                                    true));

      // Save the "fillet length" measurement to the database.
      float filletLength = (filletEdgeIndex2 - filletEdgeIndex1) * MILIMETER_PER_PIXEL;
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH,
                                    filletLength,
                                    true,
                                    true));

      // Save the heel thickness measurement to the database
      float heelThickness = smoothedProfile[heelBin];
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS,
                                    heelThickness,
                                    true,
                                    true));

      // Save the center thickness measurement to the database
      float centerThickness = smoothedProfile[centerBin];
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS,
                                    centerThickness,
                                    true,
                                    true));

      // Save the toe thickness measurement to the database
      float toeThickness = smoothedProfile[toePeakBin];
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_TOE_THICKNESS,
                                    toeThickness,
                                    true,
                                    true));

      // Determine the heel position as a percent of fillet length (heel edge to toe edge)
//      float heelPositionPercentOfFilletLength = ((float)(heelBin - heelDerivativeIndex) / filletLength) * 100.f;
      float heelPosition = (float)(heelBin) * MILIMETER_PER_PIXEL;
      float heelPositionPercentOfFilletLength = (heelPosition / filletLength) * 100.f;

      // Save the heel position measurement to the database.
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_LEARNING_EDGE_TO_HEEL_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG,
                                    heelPositionPercentOfFilletLength,
                                    true,
                                    true));

      // Determine the center position as a percent of fillet length (heel edge to center edge)
//      float centerPositionPercentOfFilletLength = ((float)(centerBin - heelDerivativeIndex) / filletLength) * 100.f;
      float centerPosition = (float)(centerBin) * MILIMETER_PER_PIXEL;
      float centerPositionPercentOfFilletLength = (centerPosition / filletLength) * 100.f;

      // Save the center position measurement to the database.
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_LEARNING_EDGE_TO_CENTER_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG,
                                    centerPositionPercentOfFilletLength,
                                    true,
                                    true));

      // Determine the toe position as a percent of fillet length (heel edge to toe edge)
//      float toePositionPercentOfFilletLength = ((float)(toePeakBin - heelDerivativeIndex) / filletLength) * 100.f;
      float toePosition = (float)(toePeakBin) * MILIMETER_PER_PIXEL;
      float toePositionPercentOfFilletLength = (toePosition / filletLength) * 100.f;

      // Save the toe position measurement to the database.
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_LEARNING_EDGE_TO_TOE_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG,
                                    toePositionPercentOfFilletLength,
                                    true,
                                    true));
      
      //XCR-2139 - Learn the maximum and minimum slope
      // Siew Yeng - save the "heel slope" measurement
      int heelSlopeIndex = Math.max(Math.round(heelSlopeSubpixelBin), 0);
      float heelSlope = firstDerivativeProfile[heelSlopeIndex];
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE,
                                    heelSlope,
                                    true,
                                    true));
      
      // get the integer index
      int toeNegativeSlopeIndex = Math.min(Math.round(toeNegativeSlopeSubpixelBin), smoothedProfile.length - 1);

      // save the "toe slope" measurement
      float toeNegativeSlope = firstDerivativeProfile[toeNegativeSlopeIndex];

      // make the normally negative slope positive to flag a "positive" slope as a defect
      float toeSlope = -(toeNegativeSlope);
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE,
                                    toeSlope,
                                    true,
                                    true));
      
      // save the "heel/toe slope sum" measurement
      float heelToeSlopeSum = heelSlope + toeSlope;
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM,
                                    heelToeSlopeSum,
                                    true,
                                    true));
      
      // determine the slope around the center (sum of slopes around center)
      int searchLength = Math.round(((float)(filletEdgeIndex2 - filletEdgeIndex1) * _CENTER_PERCENT_OF_FILLET) / 2);
      // make sure the search is within the profile (CR30738)
      int startIndex = Math.max(centerBin - searchLength, 0);
      int endIndex = Math.min(centerBin + searchLength, firstDerivativeProfile.length);
  
      float centerSlope = ArrayUtil.sum(firstDerivativeProfile, startIndex, endIndex);
    
      // save the "center slope" measurement
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE,
                                    centerSlope,
                                    true,
                                    true));
      
      // save the "sum of slope changes" measurement
      float sumOfSlopeChanges = AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(secondDerivativeProfile);

      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES,
                                    sumOfSlopeChanges,
                                    true,
                                    true));
      
      profileData.setHeelBin(heelBin);
      profileData.setCenterBin(centerBin);
    }
  }

  /**
   * Updates the nominal fillet length and thickness based on learned measurements.
   *
   * @author George Booth
   */
  private void updateNominalFilletLengthAndThickness(Subtype subtype,
                                                       SliceNameEnum slice,
                                                       boolean updateNominalSettingsOnly) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all the "fillet length" measurements from the database.
    float[] filletLengthMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH,
                                                 true,
                                                 true);

    if (filletLengthMeasurementValues.length > 0)
    {
      // Take the median "fillet Thickness" value and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float medianFilletLength = AlgorithmUtil.medianWithExcludeOutlierDecision(filletLengthMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH, medianFilletLength);
    }

    // Pull all the "fillet thickness" measurements from the database.
    float[] filletThicknessMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.QFN_MEASUREMENT_FILLET_THICKNESS,
                                                 true,
                                                 true);

    if (filletThicknessMeasurementValues.length > 0)
    {
      // Take the median "fillet Thickness" value and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float medianFilletThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(filletThicknessMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS, medianFilletThickness);
    }
  }

  /**
   * Update the heel search distance and thickness based on learned measurements.
   *
   * @author George Booth
   */
  private void updateHeelSearchDistanceAndNominalThickness(Subtype subtype,
                                                                        SliceNameEnum slice,
                                                                        boolean updateNominalSettingsOnly) throws DatastoreException

  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all the "heel position" measurements from the database.
    float[] heelPositionMeasurementValues =
      subtype.getLearnedJointMeasurementValues(slice,
                                               MeasurementEnum.QFN_LEARNING_EDGE_TO_HEEL_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG,
                                               true,
                                               true);

    if (heelPositionMeasurementValues.length > 1)
    {
      // Take the median "heel position" value and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float medianHeelPosition = AlgorithmUtil.medianWithExcludeOutlierDecision(heelPositionMeasurementValues, true);
      if (updateNominalSettingsOnly == false)
      {
        // CR30744 - don't add std deviation
//        float heelPositionStdDeviation = StatisticsUtil.standardDeviation(heelPositionMeasurementValues);
        float heelSearchDistance = medianHeelPosition; // + heelPositionStdDeviation;
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, heelSearchDistance);
      }
    }

    // Pull all the "heel thickness" measurements from the database.
    float[] heelThicknessMeasurementValues =
      subtype.getLearnedJointMeasurementValues(slice,
                                               MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS,
                                               true,
                                               true);

    if (heelThicknessMeasurementValues.length > 0)
    {
      // Take the median "heel thickness" value and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float nominalHeelThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(heelThicknessMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS, nominalHeelThickness);
    }
  }

  /**
   * Update the toe position and nominal thickness based on learned measurements
   *
   * @author George Booth
   */
  private void updateToeOffsetAndNominalThickness(Subtype subtype,
                                                      SliceNameEnum slice,
                                                      boolean updateNominalSettingsOnly) throws DatastoreException

  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all the "toe position" measurements from the database.
    float[] toePositionMeasurementValues =
      subtype.getLearnedJointMeasurementValues(slice,
                                               MeasurementEnum.QFN_LEARNING_EDGE_TO_TOE_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG,
                                               true,
                                               true);

    if (updateNominalSettingsOnly == false && toePositionMeasurementValues.length > 1)
    {
      // CR30744 - don't add std deviation
      // Take the median "toe position" value and set the nominal to that plus one std deviation (just beyond the peak).
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float medianToePosition = AlgorithmUtil.medianWithExcludeOutlierDecision(toePositionMeasurementValues, true);
//      float toePositionStdDeviation = StatisticsUtil.standardDeviation(toePositionMeasurementValues);
      float nominalToePosition = medianToePosition; // + toePositionStdDeviation;
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET, nominalToePosition);
    }

    // Pull all the "toe thickness" measurements from the database.
    float[] toeThicknessMeasurementValues =
      subtype.getLearnedJointMeasurementValues(slice,
                                               MeasurementEnum.QFN_MEASUREMENT_TOE_THICKNESS,
                                               true,
                                               true);

    for (int i = 0; i < toeThicknessMeasurementValues.length; i++)//????? Lim Lay Ngor - is this a bug??????

    if (toeThicknessMeasurementValues.length > 0)
    {
      // Take the median "heel thickness" value and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float nominalToeThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(toeThicknessMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS, nominalToeThickness);
    }
  }

  /**
   * Update the center location and nominal thickness based on learned measurements.
   *
   * @author George Booth
   */
  private void updateCenterOffsetAndNominalThickness(Subtype subtype,
                                                         SliceNameEnum slice,
                                                         boolean updateNominalSettingsOnly) throws DatastoreException

  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all the "center position" measurements from the database.
    float[] centerPositionMeasurementValues =
      subtype.getLearnedJointMeasurementValues(slice,
                                               MeasurementEnum.QFN_LEARNING_EDGE_TO_CENTER_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG,
                                               true,
                                               true);

    if (updateNominalSettingsOnly == false && centerPositionMeasurementValues.length > 0)
    {
      // Take the median "center position" value and set the nominal to that
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float nominalCenterPosition = AlgorithmUtil.medianWithExcludeOutlierDecision(centerPositionMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET, nominalCenterPosition);
    }

    // Pull all the "center thickness" measurements from the database.
    float[] centerThicknessMeasurementValues =
      subtype.getLearnedJointMeasurementValues(slice,
                                               MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS,
                                               true,
                                               true);

    if (centerThicknessMeasurementValues.length > 0)
    {
      // Take the median "center thickness" value and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float nominalCenterThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(centerThicknessMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS, nominalCenterThickness);
    }
  }

  /**
   * Update the center of solder volume based on learned measurements.
   *
   * @author George Booth
   */
  private void updateCenterOfSolderVolume(Subtype subtype,
                                          SliceNameEnum slice) throws DatastoreException

  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all the "center of solder volume" measurements from the database.
    float[] centerOfSolderVolumeMeasurementValues =
      subtype.getLearnedJointMeasurementValues(slice,
                                               MeasurementEnum.QFN_LEARNING_CENTER_OF_SOLDER_VOLUME,
                                               true,
                                               true);

    if (centerOfSolderVolumeMeasurementValues.length > 0)
    {
      // Take the median "center of solder volume" value and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float nominalCenterOfSolderVolume = AlgorithmUtil.medianWithExcludeOutlierDecision(centerOfSolderVolumeMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME,
                              nominalCenterOfSolderVolume);
    }
  }

  /**
   * Measures the pad thickness profiles for learning.
   *
   * @author George Booth
   */
  private Map<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning>
      measurePadThicknessProfilesForLearning(Subtype subtype,
                                             ManagedOfflineImageSet imageSet,
                                             SliceNameEnum slice) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(imageSet != null);
    Assert.expect(slice != null);

    Map<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning> padProfileDataMap =
        new HashMap<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning>();
    final float PAD_PROFILE_WIDTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_WIDTH) / 100.f;
    final float PAD_PROFILE_LENGTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_PAD_PROFILE_LENGTH) / 100.f;
    boolean useHeelVoidCompensation = isHeelVoidCompensationEnabled(subtype);

    // get the smoothing kernel length
    int smoothingKernelLength = getSmoothingKernelLength(subtype);

    ManagedOfflineImageSetIterator imagesIterator = imageSet.iterator();
    int samplingFrequency = Math.max(1, imageSet.size() / _MAX_NUMBER_OF_REGIONS_FOR_LEARNING);
    imagesIterator.setSamplingFrequency(samplingFrequency);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      // Get the image for the relevant slice.
      ImageSetData currentImageSetData = imagesIterator.getCurrentImageSetData();
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(slice);
      Image sliceImage = reconstructedSlice.getOrthogonalImage();

      // Get all the joints in the region which are of the applicable subtype.
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      // Run Locator.
      Locator.locateJoints(reconstructedImages, slice, jointInspectionDataObjects, this, true);

      // Measure the profiles for each joint in the region.
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // Measure the fractional pad length across profile.
        Pair<RegionOfInterest, float[]> fractionalWidthPadProfileRoiAndThicknessProfile =
            measureSmoothedPadThicknessProfile(sliceImage,
                                               slice,
                                               jointInspectionData,
                                               PAD_PROFILE_WIDTH_FRACTION,
                                               PAD_PROFILE_LENGTH_FRACTION,
                                               false,
                                               smoothingKernelLength);
        
        //Siew Yeng - XCR-2139 - Learn Slope Setting
        RegionOfInterest fractionalWidthPadProfileRoi = fractionalWidthPadProfileRoiAndThicknessProfile.getFirst();
        float[] fractionalWidthPadThicknessProfile = fractionalWidthPadProfileRoiAndThicknessProfile.getSecond();

        // Measure the full pad length across profile.  If void compensation is disabled, just use the fractional
        // profile.
        RegionOfInterest smoothedProfileWithVoidCompRoi = null; 
        float[] fullWidthPadThicknessProfile = null;
        if (useHeelVoidCompensation)
        {
          Pair<RegionOfInterest, float[]> fullWidthPadProfileRoiAndThicknessProfile =
              measureSmoothedPadThicknessProfile(sliceImage,
                                                 slice,
                                                 jointInspectionData,
                                                 1f,
                                                 PAD_PROFILE_LENGTH_FRACTION,
                                                 false,
                                                 smoothingKernelLength);
          smoothedProfileWithVoidCompRoi = fullWidthPadProfileRoiAndThicknessProfile.getFirst();
          fullWidthPadThicknessProfile = fullWidthPadProfileRoiAndThicknessProfile.getSecond();
        }
        else
        {
          smoothedProfileWithVoidCompRoi = fractionalWidthPadProfileRoi;
          fullWidthPadThicknessProfile = new float[fractionalWidthPadThicknessProfile.length];
          System.arraycopy(fractionalWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile.length);
        }

        // generate a working profile based on the entire profile
        final float FILLET_REGION_LIMIT_FRACTION = 1.0F;

        float[] smoothedProfileWithVoidComp = createWorkingProfileFromArea(fullWidthPadThicknessProfile,
                                                            FILLET_REGION_LIMIT_FRACTION);

        // create a smoothed derivative profile from the working profile
        float[] smoothedFirstDerivativeProfile =
          AlgorithmUtil.createUnitizedDerivativeProfile(smoothedProfileWithVoidComp,
                                                        2,
                                                        MagnificationEnum.getCurrentNorminal(),
                                                        MeasurementUnitsEnum.MILLIMETERS);

        smoothedFirstDerivativeProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeProfile,
                                                                                  smoothingKernelLength);

        // create a smoothed second derivative profile from the working profile
        float[] smoothedSecondDerivativeProfile =
          AlgorithmUtil.createUnitizedDerivativeProfile(smoothedFirstDerivativeProfile,
                                                        2,
                                                        MagnificationEnum.getCurrentNorminal(),
                                                        MeasurementUnitsEnum.MILLIMETERS);

        smoothedSecondDerivativeProfile = ProfileUtil.getSmoothedProfile(smoothedSecondDerivativeProfile,
                                                                                  smoothingKernelLength);

        // compute the curvature profile
        float[] curvatureProfile = AlgorithmUtil.createCurvatureProfile(smoothedFirstDerivativeProfile, smoothedSecondDerivativeProfile);

        // Add the measured profile data to our map.
        QFNProfileDataForLearning profileDataForLearning =
            new QFNProfileDataForLearning(smoothedProfileWithVoidComp,
                                          smoothedProfileWithVoidCompRoi,
                                          smoothedFirstDerivativeProfile,
                                          smoothedSecondDerivativeProfile,
                                          curvatureProfile);

        padProfileDataMap.put(new Pair<JointInspectionData, ImageSetData>(jointInspectionData, currentImageSetData),
                              profileDataForLearning);
      }

      imagesIterator.finishedWithCurrentRegion();
      
      if (jointInspectionDataObjects != null)
      {
        jointInspectionDataObjects.clear();
        jointInspectionDataObjects = null;
      }
    }

    Assert.expect(padProfileDataMap != null);
    return padProfileDataMap;
  }

  /**
   * Restores all learned QFN Measurement settings back to their defaults (only used for regression tests).
   *
   * @author George Booth
   * @author Ngie Xing
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Delete the Locator learning.
    Locator.setLearnedSettingsToDefaults(subtype);

    // Reset the learned settings back to their default values.
    //Ngie Xing
    for (AlgorithmSettingEnum algorithmSettingEnum : getLearnedAlgorithmSettingEnums())
    {
      subtype.setSettingToDefaultValue(algorithmSettingEnum);
    }
  }

  private void writeProfileToCSVFile(String filePath,
                                     String profileName1, float[] profile1, float scaleFactor1,
                                     String profileName2, float[] profile2, float scaleFactor2)
  {
    String delimiter = ", ";

    // do not append
    try
    {
      FileWriterUtil fileWriter = new FileWriterUtil(filePath, false);
      fileWriter.open();

      String title = "Profile";

      // report title and date
      Date date = new Date();
      if (title != null)
      {
        fileWriter.writeln(title + delimiter + date);
      }
      else
      {
        fileWriter.writeln(delimiter + delimiter + date);
      }
      fileWriter.writeln();

      fileWriter.writeln(profileName1);
      fileWriter.writeln(profileName2);
      fileWriter.writeln();

      // column header line
      StringBuffer headerLine = new StringBuffer();
      headerLine.append("index, value1, value2");
      fileWriter.writeln(headerLine.toString());

      // profile data
      int len = profile1.length;
      if (profile2.length > len)
        len = profile2.length;
      float last1 = profile1[0];
      float last2 = profile2[0];
      for (int i = 0; i < profile1.length; i++)
      {
        StringBuffer reportLine = new StringBuffer();
        reportLine.append(i + delimiter);

        if (i < profile1.length)
        {
          last1 = profile1[i] * scaleFactor1;
          reportLine.append(last1 + delimiter);
        }
        else
          reportLine.append(last1 + delimiter);

        if (i < profile2.length)
        {
          last2 = profile2[i] * scaleFactor2;
          reportLine.append(last2 + delimiter);
        }
        else
          reportLine.append(last2);

        fileWriter.writeln(reportLine.toString());
      }

      fileWriter.close();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  private void writeProfileToCSVFile(String filePath,
                                     String profileName1, float[] profile1, float scaleFactor1,
                                     String profileName2, float[] profile2, float scaleFactor2,
                                     String profileName3, float[] profile3, float scaleFactor3,
                                     String profileName4, float[] profile4, float scaleFactor4)
  {
    Assert.expect(profile2.length == profile1.length);
    Assert.expect(profile3.length == profile1.length);
    Assert.expect(profile4.length == profile1.length);
    String delimiter = ", ";

    // do not append
    try
    {
      FileWriterUtil fileWriter = new FileWriterUtil(filePath, false);
      fileWriter.open();

      String title = "Profile";

      // report title and date
      Date date = new Date();
      if (title != null)
      {
        fileWriter.writeln(title + delimiter + date);
      }
      else
      {
        fileWriter.writeln(delimiter + delimiter + date);
      }
      fileWriter.writeln();

      fileWriter.writeln(profileName1);
      fileWriter.writeln(profileName2);
      fileWriter.writeln(profileName3);
      fileWriter.writeln(profileName4);
      fileWriter.writeln();

      // column header line
      StringBuffer headerLine = new StringBuffer();
      headerLine.append("index, value1, value2, value3, value4");
      fileWriter.writeln(headerLine.toString());

      // profile data
      for (int i = 0; i < profile1.length; i++)
      {
        StringBuffer reportLine = new StringBuffer();
        reportLine.append(i + delimiter);
        reportLine.append(profile1[i] * scaleFactor1 + delimiter);
        reportLine.append(profile2[i] * scaleFactor2 + delimiter);
        reportLine.append(profile3[i] * scaleFactor3 + delimiter);
        reportLine.append(profile4[i] * scaleFactor4);
        fileWriter.writeln(reportLine.toString());
      }

      fileWriter.close();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  /**
   * @author Siew Yeng
   */
  private void learnQFNJointAcrossMeasurements(Subtype subtype,
                                         ManagedOfflineImageSet imageSet,
                                         SliceNameEnum slice,
                                         Map<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning> profileDataMap) throws DatastoreException
  {
    

    Map<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning> padProfileDataMap =
        new HashMap<Pair<JointInspectionData, ImageSetData>, QFNProfileDataForLearning>();

    // get the smoothing kernel length
    int smoothingKernelLength = getSmoothingKernelLength(subtype);

    ManagedOfflineImageSetIterator imagesIterator = imageSet.iterator();
    int samplingFrequency = Math.max(1, imageSet.size() / _MAX_NUMBER_OF_REGIONS_FOR_LEARNING);
    imagesIterator.setSamplingFrequency(samplingFrequency);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      // Get the image for the relevant slice.
      ImageSetData currentImageSetData = imagesIterator.getCurrentImageSetData();
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(slice);
      Image sliceImage = reconstructedSlice.getOrthogonalImage();

      // Get all the joints in the region which are of the applicable subtype.
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      // Run Locator.
      Locator.locateJoints(reconstructedImages, slice, jointInspectionDataObjects, this, true);

      // Measure the profiles for each joint in the region.
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        QFNProfileDataForLearning profileData =
            profileDataMap.get(new Pair<JointInspectionData, ImageSetData>(jointInspectionData, currentImageSetData));

        // ------------ heel across measurements -----------------------------
        // find width and slopes across heel
        // create an heel across profile at the heel peak bin
        Pair<RegionOfInterest, float[]> smoothedHeelAcrossThicknessProfileRoiAndThicknessProfile =
          measureSmoothedAcrossProfile(sliceImage,
                                       slice,
                                       jointInspectionData,
                                       profileData.getSmoothedProfileWithVoidCompRoi(),
                                       profileData.getSmoothedProfileWithVoidComp(),
                                       profileData.getHeelBin(),
                                       true,
                                       true,
                                       smoothingKernelLength);

          //RegionOfInterest smoothedHeelAcrossRoi = smoothedHeelAcrossThicknessProfileRoiAndThicknessProfile.getFirst();
          float[] smoothedHeelAcrossThicknessProfile = smoothedHeelAcrossThicknessProfileRoiAndThicknessProfile.getSecond();

          // create a smoothed derivative profile from the heel across profile
          float[] smoothedFirstDerivativeHeelAcrossProfile =
            AlgorithmUtil.createUnitizedDerivativeProfile(smoothedHeelAcrossThicknessProfile,
                                                          2,
                                                          MagnificationEnum.getCurrentNorminal(),
                                                          MeasurementUnitsEnum.MILLIMETERS);

          smoothedFirstDerivativeHeelAcrossProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeHeelAcrossProfile,
                                                                                  smoothingKernelLength);

          // save the "heel across width" measurement
          //Pair<Float, Float> heelAcrossEdges = measureLeadingTrailingEdgesAcross(smoothedFirstDerivativeHeelAcrossProfile);
          //float heelAcrossLeadingEdgeSubpixelBin = heelAcrossEdges.getFirst().floatValue();
          //float heelAcrossTrailingEdgeSubpixelBin = heelAcrossEdges.getSecond().floatValue();
          //float heelAcrossWidth = (heelAcrossTrailingEdgeSubpixelBin - heelAcrossLeadingEdgeSubpixelBin) * AlgorithmUtil.getMillimetersPerPixel();

          // save the "heel Across Slope Sum" measurement
          float heelAcrossSlopeSum = AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(smoothedFirstDerivativeHeelAcrossProfile);

          // Save the toe position measurement to the database.
          subtype.addLearnedJointMeasurementData(
            new LearnedJointMeasurement(jointInspectionData.getPad(),
                                        slice,
                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM,
                                        heelAcrossSlopeSum,
                                        true,
                                        true));

          // save the "heel Across Leading Trailing Slope Sum" measurement
          float heelAcrossLeadingTrailingSlopeSum = measureLeadingTrailingSlopeSum(smoothedFirstDerivativeHeelAcrossProfile);
          subtype.addLearnedJointMeasurementData(
            new LearnedJointMeasurement(jointInspectionData.getPad(),
                                        slice,
                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM,
                                        heelAcrossLeadingTrailingSlopeSum,
                                        true,
                                        true));

          // ------------ center across measurements -----------------------------

          // find width and slopes across center
          // create an center across profile at the center bin
          Pair<RegionOfInterest, float[]> smoothedCenterAcrossThicknessProfileRoiAndThicknessProfile =
          measureSmoothedAcrossProfile(sliceImage,
                                       slice,
                                       jointInspectionData,
                                       profileData.getSmoothedProfileWithVoidCompRoi(),
                                       profileData.getSmoothedProfileWithVoidComp(),
                                       profileData.getCenterBin(),
                                       false,
                                       true,
                                       smoothingKernelLength);

          //RegionOfInterest smoothedCenterAcrossRoi = smoothedCenterAcrossThicknessProfileRoiAndThicknessProfile.getFirst();
          float[] smoothedCenterAcrossThicknessProfile = smoothedCenterAcrossThicknessProfileRoiAndThicknessProfile.getSecond();

          // create a smoothed derivative profile from the center across profile
          float[] smoothedFirstDerivativeCenterAcrossProfile =
            AlgorithmUtil.createUnitizedDerivativeProfile(smoothedCenterAcrossThicknessProfile,
                                                          2,
                                                          MagnificationEnum.getCurrentNorminal(),
                                                          MeasurementUnitsEnum.MILLIMETERS);

          smoothedFirstDerivativeCenterAcrossProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeCenterAcrossProfile,
                                                                                    smoothingKernelLength);

          // Save the "center across width" measurement.
          //Pair<Float, Float> centerAcrossEdges = measureLeadingTrailingEdgesAcross(smoothedFirstDerivativeCenterAcrossProfile);
          //float centerAcrossLeadingEdgeSubpixelBin = centerAcrossEdges.getFirst().floatValue();
          //float centerAcrossTrailingEdgeSubpixelBin = centerAcrossEdges.getSecond().floatValue();
          //float centerAcrossWidth = (centerAcrossTrailingEdgeSubpixelBin - centerAcrossLeadingEdgeSubpixelBin) * AlgorithmUtil.getMillimetersPerPixel();

          // Save the "center across slope sum" measurement.
          float centerAcrossSlopeSum = AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(smoothedFirstDerivativeCenterAcrossProfile);
          subtype.addLearnedJointMeasurementData(
            new LearnedJointMeasurement(jointInspectionData.getPad(),
                                        slice,
                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM,
                                        centerAcrossSlopeSum,
                                        true,
                                        true));

          // Save the "center across Leading Trailing slope sum" measurement.
          float centerAcrossLeadingTrailingSlopeSum = measureLeadingTrailingSlopeSum(smoothedFirstDerivativeCenterAcrossProfile);
          subtype.addLearnedJointMeasurementData(
            new LearnedJointMeasurement(jointInspectionData.getPad(),
                                        slice,
                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM,
                                        centerAcrossLeadingTrailingSlopeSum,
                                        true,
                                        true));
      }
      
      imagesIterator.finishedWithCurrentRegion();
      
      if (jointInspectionDataObjects != null)
      {
        jointInspectionDataObjects.clear();
        jointInspectionDataObjects = null;
      }
    }
  }
  
  /**
   * XCR-3532
   * @author Siew Yeng
   */
  private void computeRegionOutlierMeasurementsForFilletLength(Collection<JointInspectionData> jointInspectionDataObjects,
                                                                float[] validFilletLength,
                                                                SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(validFilletLength != null);
    Assert.expect(sliceNameEnum != null);

    float expectedFilletLength = 0.0f;

    if (validFilletLength.length > 0)
    {
      expectedFilletLength = StatisticsUtil.median(validFilletLength);
    }
    
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      float filletLength = MathUtil.convertMillimetersToMils(jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                                   MeasurementEnum.QFN_MEASUREMENT_FILLET_LENGTH).getValue());

      float regionOutlierPercent = 0.0f;

      //Siew Yeng - XCR-3358 - Infinity and unreadable measurement value is displayed
      if (validFilletLength.length > 0 && expectedFilletLength > 0)
      {
        regionOutlierPercent = 100.0f * filletLength / expectedFilletLength;
      }
      
      jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                MeasurementEnum.QFN_OPEN_FILLET_LENGTH_REGION_OUTLIER,
                                                                MeasurementUnitsEnum.PERCENT,
                                                                jointInspectionData.getPad(),
                                                                sliceNameEnum,
                                                                regionOutlierPercent));
    }
  }
  
  /**
   * XCR-3532
   * @author Siew Yeng
   */
  private void computeRegionOutlierMeasurementsForCenterThickness(Collection<JointInspectionData> jointInspectionDataObjects,
                                                                  float[] validCenterThickness,
                                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(validCenterThickness != null);
    Assert.expect(sliceNameEnum != null);

    float expectedCenterThickness = 0.0f;

    if (validCenterThickness.length > 0)
    {
      expectedCenterThickness = StatisticsUtil.median(validCenterThickness);
    }
    
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      float centerThickness = MathUtil.convertMillimetersToMils(jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                                   MeasurementEnum.QFN_MEASUREMENT_CENTER_THICKNESS).getValue());

      float regionOutlierPercent = 0.0f;

      //Siew Yeng - XCR-3358 - Infinity and unreadable measurement value is displayed
      if (validCenterThickness.length > 0 && expectedCenterThickness > 0)
      {
        regionOutlierPercent = 100.0f * centerThickness / expectedCenterThickness;
      }
      
      jointInspectionResult.addMeasurement(new JointMeasurement(this,
                                                                MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_REGION_OUTLIER,
                                                                MeasurementUnitsEnum.PERCENT,
                                                                jointInspectionData.getPad(),
                                                                sliceNameEnum,
                                                                regionOutlierPercent));
    }
  }
  
}
