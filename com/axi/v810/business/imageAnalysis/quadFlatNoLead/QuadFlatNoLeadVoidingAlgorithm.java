package com.axi.v810.business.imageAnalysis.quadFlatNoLead;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * Extends the FloodVoidingAlgorithm for QFN
 *
 * @author George Booth
 */
public class QuadFlatNoLeadVoidingAlgorithm extends FloodVoidingAlgorithm
{
  private final int _ALGORITHM_VERSION = 1;

  /**
   * @author George Booth
   */
  public QuadFlatNoLeadVoidingAlgorithm(InspectionFamily quadFlatNoLeadInspectionFamily)
  {
    super(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD, quadFlatNoLeadInspectionFamily);
  }

  /**
   * @author George Booth
   */
  protected void createAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;

    // Joint Voiding Area Threshold
    // If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as
    // being voided.
    AlgorithmSetting failJointPrecentPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_VOIDING_MAXIMUM_VOID_PERCENT,
        displayOrder++,
        30.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_VOIDING_(MAXIMUM_VOID_PERCENT)_KEY", // description URL key
        "HTML_DETAILED_DESC_QFN_VOIDING_(MAXIMUM_VOID_PERCENT)_KEY", // desailed description URL key
        "IMG_DESC_QFN_VOIDING_(MAXIMUM_VOID_PERCENT)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                          failJointPrecentPadSetting,
                                                          SliceNameEnum.PAD,
                                                          MeasurementEnum.QFN_VOIDING_PERCENT);

    addAlgorithmSetting(failJointPrecentPadSetting);

    //Jack Hwee- individual void
     AlgorithmSetting failIndividualJointPrecentPadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT,
        displayOrder++,
        25.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_INDIVIDUAL_VOIDING_(MAXIMUM_INDIVIDUAL_VOID_PERCENT)_KEY", // description URL key
        "HTML_DETAILED_DESC_QFN_INDIVIDUAL_VOIDING_(MAXIMUM_INDIVIDUAL_VOID_PERCENT)_KEY", // desailed description URL key
        "IMG_DESC_QFN_INDIVIDUAL_VOIDING_(MAXIMUM_INDIVIDUAL_VOID_PERCENT)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                          failIndividualJointPrecentPadSetting,
                                                          SliceNameEnum.PAD,
                                                          MeasurementEnum.QFN_INDIVIDUAL_VOIDING_PERCENT);

    addAlgorithmSetting(failIndividualJointPrecentPadSetting);

    // Noise Reduction
    // How much to try to compensate for noise.
    // 0 = no noise reduction
    // 1 = apply blur
    // 2+ = apply blur & NR-1 cycles of opening (dilate/erode cycles) to the void pixels image.
    // The opening will remove smaller features from the image and generally make the regions appear
    // more blocky than without NR.
    //  I expect that this value will normally be 0 to 2.
    AlgorithmSetting noiseReductionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_VOIDING_NOISE_REDUCTION,
        displayOrder++,
        2, // default value
        0, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_VOIDING_(NOISE_REDUCTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_QFN_VOIDING_(NOISE_REDUCTION)_KEY", // desailed description URL key
        "IMG_DESC_QFN_VOIDING_(NOISE_REDUCTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(noiseReductionSetting);
  }

  /**
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.QFN_VOIDING_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_INDIVIDUAL_VOIDING_PERCENT);
  }

  /**
   * @author George Booth
   */
  protected int getNoiseReductionSettingForSlice(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    return ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_VOIDING_NOISE_REDUCTION)).intValue();
  }

  /**
   * @author George Booth
   */
  protected List<SliceNameEnum> selectInspectedSlices(Subtype subtype, ReconstructedImages reconstructedImages)
  {
    Assert.expect(subtype != null);
    Assert.expect(reconstructedImages != null);

    List<SliceNameEnum> sliceNamesToInspect = new LinkedList<SliceNameEnum>();

    // QFN only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    sliceNamesToInspect.add(padSlice);

    return sliceNamesToInspect;
  }

  /**
   * @author George Booth
   * @author Jack Hwee
   */
  public void classifyJoints(ReconstructedImages reconstructedImages, List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
     Assert.expect(jointInspectionDataObjects != null);

     if (jointInspectionDataObjects.isEmpty())
     {
       // nothing to inspect
       return;
     }
     Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

     for (SliceNameEnum sliceNameEnum : selectInspectedSlices(subtype, reconstructedImages))
     {
       int noiseReduction = getNoiseReductionSettingForSlice(subtype, sliceNameEnum);
       float voidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(
          AlgorithmSettingEnum.QFN_VOIDING_MAXIMUM_VOID_PERCENT);
       MeasurementEnum percentMeasurementEnum = MeasurementEnum.QFN_VOIDING_PERCENT;

        float individualVoidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(
          AlgorithmSettingEnum.QFN_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT);
       MeasurementEnum individualpercentMeasurementEnum = MeasurementEnum.QFN_INDIVIDUAL_VOIDING_PERCENT;

       measureVoidingOnSlice(reconstructedImages,
                             sliceNameEnum,
                             jointInspectionDataObjects,
                             subtype,
                             noiseReduction,
                             voidAreaFailThreshold,
                            individualVoidAreaFailThreshold,
                            percentMeasurementEnum,
                            individualpercentMeasurementEnum);
     }
  }

}

