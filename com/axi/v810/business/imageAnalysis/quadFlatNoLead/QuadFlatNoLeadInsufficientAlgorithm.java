package com.axi.v810.business.imageAnalysis.quadFlatNoLead;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Quad flat pack no-lead (QFN) insufficient algorithm.
 *
 * @author George Booth
 */
public class QuadFlatNoLeadInsufficientAlgorithm extends Algorithm
{

  /**
   * @author George Booth
   */
  public QuadFlatNoLeadInsufficientAlgorithm(InspectionFamily quadFlatNoLeadInspectionFamily)
  {
    super(AlgorithmEnum.INSUFFICIENT, InspectionFamilyEnum.QUAD_FLAT_NO_LEAD);

    Assert.expect(quadFlatNoLeadInspectionFamily  != null);

    // Add the algorithm settings.

    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Minimum fillet thickness percent of nominal.
    AlgorithmSetting minFilletThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_INSUFFICIENT_(MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_INSUFFICIENT_(MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_QFN_INSUFFICIENT_(MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minFilletThicknessPercentOfNominalSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_INSUFFICIENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minFilletThicknessPercentOfNominalSetting);

    // Minimum heel thickness percent of nominal.
    AlgorithmSetting minHeelThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        25.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_INSUFFICIENT_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_INSUFFICIENT_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_QFN_INSUFFICIENT_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minHeelThicknessPercentOfNominalSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minHeelThicknessPercentOfNominalSetting);

    // Minimum toe thickness percent of nominal.
    AlgorithmSetting minToeThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_INSUFFICIENT_(MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_INSUFFICIENT_(MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_QFN_INSUFFICIENT_(MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minToeThicknessPercentOfNominalSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_INSUFFICIENT_TOE_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minToeThicknessPercentOfNominalSetting);

    // Minimum center thickness percent of nominal.
    AlgorithmSetting minCenterThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_INSUFFICIENT_(MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_INSUFFICIENT_(MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_QFN_INSUFFICIENT_(MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minCenterThicknessPercentOfNominalSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minCenterThicknessPercentOfNominalSetting);

    addMeasurementEnums();
  }

  /**
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.QFN_INSUFFICIENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_INSUFFICIENT_TOE_THICKNESS_PERCENT_OF_NOMINAL);
  }

  /**
   * Runs 'Insufficient' on all the given joints.
   *
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Ensure the all the JointInspectionData objects have the same subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      Assert.expect(jointInspectionData.getSubtype() == subtype);
    }

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // QFN only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the reconstructed pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Check each joint for insufficient solder.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                inspectionRegion,
                                                reconstructedPadSlice,
                                                false);

      BooleanRef jointPassed = new BooleanRef(true);

      // Run the QFN Insuffient classification.
      classifyJoint(subtype,
                    padSlice,
                    jointInspectionData,
                    jointPassed);


      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }

  /**
   * Does the QFN Open classification on the specified joint.
   *
   * @author George Booth
   */
  protected void classifyJoint(Subtype subtype,
                               SliceNameEnum sliceNameEnum,
                               JointInspectionData jointInspectionData,
                               BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Detect insuffcient fillet thickness
    detectInsufficientFilletThickness(subtype, sliceNameEnum, jointInspectionData, jointPassed);

    // Detect insuffcient heel thickness
    detectInsufficientHeelThickness(subtype, sliceNameEnum, jointInspectionData, jointPassed);

    // Detect insuffcient center thickness
    detectInsufficientCenterThickness(subtype, sliceNameEnum, jointInspectionData, jointPassed);

    // Detect insuffcient toe thickness
    detectInsufficientToeThickness(subtype, sliceNameEnum, jointInspectionData, jointPassed);
  }

  /**
   * Detects insufficient solder on the specified joint by comparing the joint's fillet thickness percent of
   * nominal against the defined tolerance setting.
   *
   * @author George Booth
   */
  private void detectInsufficientFilletThickness(Subtype subtype,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate tested joint's fillet thickness' percent of the nominal fillet thickness.
    final float NOMINAL_FILLET_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS);

    JointMeasurement filletThicknessMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getFilletThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float filletThickness = filletThicknessMeasurement.getValue();

    float filletThicknessPercentOfNominal = (filletThickness / (float)NOMINAL_FILLET_THICKNESS) * 100f;

    JointMeasurement filletThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_INSUFFICIENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             filletThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(filletThicknessPercentOfNominalMeasurement);

    // Check to see if the fillet thickness is within acceptable tolerance of nominal.
    final float MIN_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL);

    if (MathUtil.fuzzyLessThan(filletThicknessPercentOfNominal, MIN_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'insufficient'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment insufficientIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceNameEnum);

      // Add the failing and related measurements.
      insufficientIndictment.addFailingMeasurement(filletThicknessPercentOfNominalMeasurement);
      insufficientIndictment.addRelatedMeasurement(filletThicknessMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(insufficientIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);
    }
  }

  /**
   * Detects insufficient solder on the specified joint by comparing the joint's heel thickness percent of
   * nominal against the defined tolerance setting.
   *
   * @author George Booth
   */
  private void detectInsufficientHeelThickness(Subtype subtype,
                                               SliceNameEnum sliceNameEnum,
                                               JointInspectionData jointInspectionData,
                                               BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate tested joint's heel thickness' percent of the nominal fillet thickness.
    final float NOMINAL_HEEL_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS);

    JointMeasurement heelThicknessMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getHeelThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float heelThickness = heelThicknessMeasurement.getValue();

    float heelThicknessPercentOfNominal = (heelThickness / (float)NOMINAL_HEEL_THICKNESS) * 100f;

    JointMeasurement heelThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_OPEN_HEEL_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             heelThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(heelThicknessPercentOfNominalMeasurement);

    // Check to see if the heel thickness is within acceptable tolerance of nominal.
    final float MIN_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL);

    if (MathUtil.fuzzyLessThan(heelThicknessPercentOfNominal, MIN_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'insufficient'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      heelThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment insufficientIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceNameEnum);

      // Add the failing and related measurements.
      insufficientIndictment.addFailingMeasurement(heelThicknessPercentOfNominalMeasurement);
      insufficientIndictment.addRelatedMeasurement(heelThicknessMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(insufficientIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      heelThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);
    }
  }

  /**
   * Detects insufficient solder on the specified joint by comparing the joint's center thickness percent of
   * nominal against the defined tolerance setting.
   *
   * @author George Booth
   */
  private void detectInsufficientCenterThickness(Subtype subtype,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate tested joint's center thickness' percent of the nominal fillet thickness.
    final float NOMINAL_CENTER_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS);

    JointMeasurement centerThicknessMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float centerThickness = centerThicknessMeasurement.getValue();

    float centerThicknessPercentOfNominal = (centerThickness / (float)NOMINAL_CENTER_THICKNESS) * 100f;

    JointMeasurement centerThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             centerThicknessPercentOfNominal);

    // CR32812 Don�t add a duplicate measurement unless it is needed
//     jointInspectionResult.addMeasurement(centerThicknessPercentOfNominalMeasurement);

    // Check to see if the center thickness is within acceptable tolerance of nominal.
    final float MIN_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL);

    if (MathUtil.fuzzyLessThan(centerThicknessPercentOfNominal, MIN_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // make sure the joint result has the measurement
      jointInspectionResult.addMeasurement(centerThicknessPercentOfNominalMeasurement);

      // Indict the joint for 'insufficient'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment insufficientIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceNameEnum);

      // Add the failing and related measurements.
      insufficientIndictment.addFailingMeasurement(centerThicknessPercentOfNominalMeasurement);
      insufficientIndictment.addRelatedMeasurement(centerThicknessMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(insufficientIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);
    }
  }

  /**
   * Detects insufficient solder on the specified joint by comparing the joint's toe thickness percent of
   * nominal against the defined tolerance setting.
   *
   * @author George Booth
   */
  private void detectInsufficientToeThickness(Subtype subtype,
                                              SliceNameEnum sliceNameEnum,
                                              JointInspectionData jointInspectionData,
                                              BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate tested joint's toe thickness' percent of the nominal fillet thickness.
    final float NOMINAL_TOE_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS);

    JointMeasurement toeThicknessMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getToeThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float toeThickness = toeThicknessMeasurement.getValue();

    float toeThicknessPercentOfNominal = (toeThickness / (float)NOMINAL_TOE_THICKNESS) * 100f;

    JointMeasurement toeThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_INSUFFICIENT_TOE_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             toeThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(toeThicknessPercentOfNominalMeasurement);

    // Check to see if the toe thickness is within acceptable tolerance of nominal.
    final float MIN_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL);

    if (MathUtil.fuzzyLessThan(toeThicknessPercentOfNominal, MIN_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'insufficient'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      toeThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment insufficientIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceNameEnum);

      // Add the failing and related measurements.
      insufficientIndictment.addFailingMeasurement(toeThicknessPercentOfNominalMeasurement);
      insufficientIndictment.addRelatedMeasurement(toeThicknessMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(insufficientIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      toeThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);
    }
  }

}
