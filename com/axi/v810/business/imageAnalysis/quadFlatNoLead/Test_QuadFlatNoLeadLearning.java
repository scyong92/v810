package com.axi.v810.business.imageAnalysis.quadFlatNoLead;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;

public class Test_QuadFlatNoLeadLearning extends AlgorithmUnitTest
{
  private List<AlgorithmSettingEnum> _learnedSettings = new LinkedList<AlgorithmSettingEnum>();
  private Map<AlgorithmSettingEnum, Float> _learnedSettingToExpectedLearnedValueMap =
      new HashMap<AlgorithmSettingEnum, Float>();

  private static final String _TEST_PROJECT_NAME = "GOLAN_BOARD_NEW";
  private static final String _QFN_SUBTYPE_NAME = "QFN013_P065_300SQX140MM_H_LP1_QFN";
  private static final String _IMAGE_SET_NAME = "GOLAN_BOARD_NEW_Image_Set_For_Learning";


  /**
   * @author George Booth
   */
  private Test_QuadFlatNoLeadLearning()
  {
    // Do nothing...
  }

  /**
   * Initializes the "learned settings" data structures for the test.
   *
   * @author George Booth
   */
  private void initializeLearnedSettingsDataStructures()
  {
    Assert.expect(_learnedSettings != null);
    Assert.expect(_learnedSettingToExpectedLearnedValueMap != null);

    _learnedSettings.clear();
    InspectionFamily qfnInspectionFamily = InspectionFamily.getInstance(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD);
    for (Algorithm algorithm : qfnInspectionFamily.getAlgorithms())
    {
      _learnedSettings.addAll(algorithm.getLearnedAlgorithmSettingEnums());
    }

    _learnedSettingToExpectedLearnedValueMap.clear();
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH, 0.736f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS, 0.181f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_TOE_OFFSET, 79.97f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, 20.64f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE, 40.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS, 0.2037f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_CENTER_OFFSET, 46.46f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS, 0.1812f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS, 0.2109f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE, 65.0f);
//    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME, 55.92f);

    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_UPWARD_CURVATURE, 0.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, 130.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 50.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 150.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE, -1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES, 1.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE, -1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE, 0.3f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, -1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_OPEN_SIGNAL, 0.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SHARPNESS, -1000.0f);

    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL, 50.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL, 50.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL, 25.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_INSUFFICIENT_MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL, 50.0f);
    
    //Added by Kee Chin Seong & Lim, Lay Ngor - XCR:1648
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE, 1000.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM, 1000.0f);
  }

  /**
   * Creates a black image set for the specified project and subtype and runs learning against it.
   *
   * @author George Booth
   */
  private void testLearningOnBlackImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the black image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData blackImageSetData = createImageSetAndSetProgramFilters(project,
                                                                        subtype,
                                                                        "testQfnLearningOnBlackImagesImageSet",
                                                                        POPULATED_BOARD);

    generateBlackImageSet(project, blackImageSetData);

    // Run subtype learning.
    imageSetDataList.add(blackImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a white image set for the specified project and subtype and runs learning against it.
   *
   * @author George Booth
   */
  private void testLearningOnWhiteImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the white image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData whiteImageSetData = createImageSetAndSetProgramFilters(project,
                                                                        subtype,
                                                                        "testQfnLearningOnWhiteImagesImageSet",
                                                                        POPULATED_BOARD);
    generateWhiteImageSet(project, whiteImageSetData);

    // Run subtype learning.
    imageSetDataList.add(whiteImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a pseudo-random image set for the specified project and subtype and runs learning against it.
   *
   * @author George Booth
   */
  private void testLearningOnPseudoRandomImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the pseudo random image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData pseudoRandomImageSetData = createImageSetAndSetProgramFilters(project,
                                                                               subtype,
                                                                               "testQfnLearningOnPseudoRandomImagesImageSet",
                                                                               POPULATED_BOARD);
    generateRandomImageSet(project, pseudoRandomImageSetData);

    // Run subtype learning.
    imageSetDataList.add(pseudoRandomImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Main test entry point for QFN Learning.
   *
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

//    System.out.println("Initializing");
    // Initialize the learned settings data structures.
    initializeLearnedSettingsDataStructures();

    // Load up the test project (GOLAN_BOARD_NEW).
//    System.out.println("Loading " + _TEST_PROJECT_NAME);
    Project project = getProject(_TEST_PROJECT_NAME);

    // Lookup our test Subtype.
    Panel panel = project.getPanel();
    Subtype subtype = panel.getSubtype(_QFN_SUBTYPE_NAME);

    try
    {
      // Delete any pre-existing learned data.
      deleteLearnedData(subtype);

      // Check that the learned settings are at their default values.
      verifyAllSettingsAreAtDefault(subtype, _learnedSettings);

      // Learn one of the QFN subtypes.
//      System.out.println("Learning with " + ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE);
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                     POPULATED_BOARD);
      imageSetDataList.add(imageSetData);
      learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

      // Verify that the learned values match what we expect.
      checkLearnedValues(subtype, _learnedSettingToExpectedLearnedValueMap);

      // Check the number of datapoints.
//      System.out.println("Check number of datapoints");
      SliceNameEnum padSlice = SliceNameEnum.PAD;
      final int EXPECTED_NUMBER_OF_DATA_POINTS = 72;
      int actualNumberOfDataPoints = getNumberOfMeasurements(subtype, padSlice, MeasurementEnum.QFN_MEASUREMENT_HEEL_THICKNESS);
      Expect.expect(actualNumberOfDataPoints == EXPECTED_NUMBER_OF_DATA_POINTS,
                    "Actual # data points: " + actualNumberOfDataPoints);

      // Test learning on black images.
//      System.out.println("Learning with black images");
      testLearningOnBlackImages(project, subtype);

      // Test learning on white images.
//      System.out.println("Learning with white images");
      testLearningOnWhiteImages(project, subtype);

      // Test learning on pseudo-random images.
//      System.out.println("Learning with random images");
      testLearningOnPseudoRandomImages(project, subtype);

      // Make sure that we can run learning on an unpopulated board.
//      System.out.println("Learning with unpopulated board");
      testLearningOnlyUnloadedBoards(project, subtype, _learnedSettings);

      // Delete the learned data generated during this test.
      deleteLearnedData(subtype);
    }
    catch (XrayTesterException xtex)
    {
      xtex.printStackTrace();
    }
  }

  /**
   * @author George Booth
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_QuadFlatNoLeadLearning());
  }
}
