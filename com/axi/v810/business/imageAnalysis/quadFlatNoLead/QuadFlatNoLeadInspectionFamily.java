package com.axi.v810.business.imageAnalysis.quadFlatNoLead;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for quad flat no-lead (QFN) joints.
 *
 * @author George Booth
 */
public class QuadFlatNoLeadInspectionFamily extends InspectionFamily
{
  /**
   * @author George Booth
   */
  public QuadFlatNoLeadInspectionFamily()
  {
    super(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD);

    // Associate the individual algorithms with the family.

    // Measurement.
    Algorithm qfnMeasurementAlgorithm = new QuadFlatNoLeadMeasurementAlgorithm(this);
    addAlgorithm(qfnMeasurementAlgorithm);

    // Short.
    Algorithm qfnShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD);
    addAlgorithm(qfnShortAlgorithm);

    // Open.
    Algorithm qfnOpenAlgorithm = new QuadFlatNoLeadOpenAlgorithm(this);
    addAlgorithm(qfnOpenAlgorithm);

    // Insufficient.
    Algorithm qfnInsufficientAlgorithm = new QuadFlatNoLeadInsufficientAlgorithm(this);
    addAlgorithm(qfnInsufficientAlgorithm);

    // Voiding
    Algorithm qfnVoidingAlgorithm = new QuadFlatNoLeadVoidingAlgorithm(this);
    addAlgorithm(qfnVoidingAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author George Booth
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return Collections.singletonList(SliceNameEnum.PAD);
  }

  /**
   * Gets the applicable default SliceNameEnum for the Pad slice for the QFN inspection family.
   *
   * @author George Booth
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Use the standard pad slice.
    return SliceNameEnum.PAD;
  }

  /**
   * @author George Booth
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author George Booth
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }
}
