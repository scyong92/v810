package com.axi.v810.business.imageAnalysis.quadFlatNoLead;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;
import com.axi.v810.business.*;

/**
 * Test class for QuadFlatNoLead Measurement.
 *
 * @author George Booth
 */
public class Test_QuadFlatNoLeadMeasurementAlgorithm extends AlgorithmUnitTest
{
  private static final String _TEST_PROJECT_NAME = "GOLAN_BOARD_NEW";
  private static final String _QFN_SUBTYPE_NAME = "QFN013_P065_300SQX140MM_H_LP1";
  private static final String _QFN_REFDES = "U1_BD1";
  private static final String _QFN_PAD_NAME = "1";

  /**
   * @author George Booth
   */
  private Test_QuadFlatNoLeadMeasurementAlgorithm()
  {
    // Do nothing ...
  }

  /**
   * Main unit test entry point.
   *
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    try
    {
      // Load up the test project (GOLAN_BOARD_NEW).
      Project project = getProject(_TEST_PROJECT_NAME);

      // Run some tests against 'pathological' images (black, white, random shapes).
      // Pick an arbitarary pad to test.
      Panel panel = project.getPanel();
      Component component = panel.getBoards().get(0).getComponent(_QFN_REFDES);
      Pad pad = component.getPad(_QFN_PAD_NAME);

      // Run against a black image.

      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createAndSelectBlackImageSet(project, pad, "qfnTestBlackImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      // Run against a white image.
      imageSetData = createAndSelectWhiteImageSet(project, pad, "qfnTestWhiteImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      // Run against a pseudo-random image.
      createAndSelectPseudoRandomImageSet(project, pad, "qfnTestPseudoRandomImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George Booth
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_QuadFlatNoLeadMeasurementAlgorithm());
  }
}
