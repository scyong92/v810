package com.axi.v810.business.imageAnalysis.quadFlatNoLead;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Quad flat pack no-lead (QFN) open algorithm.
 *
 * @author George Booth
 */
public class QuadFlatNoLeadOpenAlgorithm extends Algorithm
{
  /**
   * @author George Booth
   */
  public QuadFlatNoLeadOpenAlgorithm(InspectionFamily quadFlatNoLeadInspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.QUAD_FLAT_NO_LEAD);

    Assert.expect(quadFlatNoLeadInspectionFamily != null);
    // Add the algorithm settings.

    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Additonal thresholds

    // Minimum fillet length
    AlgorithmSetting minFilletLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH,
        "HTML_DESC_QFN_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minFilletLengthSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_FILLET_LENGTH_PERCENT_OF_NOMINAL);

    addAlgorithmSetting(minFilletLengthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);

    // Maximum acceptable fillet length in mils
    AlgorithmSetting maximumFilletLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
        displayOrder++,
        150.0f, // default
        100.0f, // min
        500.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH,
        "HTML_DESC_QFN_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumFilletLengthSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_FILLET_LENGTH_PERCENT_OF_NOMINAL);

    addAlgorithmSetting(maximumFilletLengthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);

    // Maximum Acceptable Percent of Nominal Center Thickness
    AlgorithmSetting maximumCenterThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        130.0f, // default
        0.0f, // min
        1000.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_NOMINAL_CENTER_THICKNESS,
        "HTML_DESC_QFN_OPEN_(MAXIMUM_CENTER_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_CENTER_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MAXIMUM_CENTER_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumCenterThicknessSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL);

    addAlgorithmSetting(maximumCenterThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL);

    // Minimum acceptable open signal
    AlgorithmSetting minimumOpenSignalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MINIMUM_OPEN_SIGNAL,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(0.0f), // default
        MathUtil.convertMilsToMillimeters(-1000.0f), // min
        MathUtil.convertMilsToMillimeters(1000.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_QFN_OPEN_(MINIMUM_OPEN_SIGNAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_OPEN_SIGNAL)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MINIMUM_OPEN_SIGNAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumOpenSignalSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_OPEN_SIGNAL);

    addAlgorithmSetting(minimumOpenSignalSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_OPEN_SIGNAL);

    // Minimum Acceptable Upward Curvature
    AlgorithmSetting minimumUpwardCurvatureSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MINIMUM_UPWARD_CURVATURE,
        displayOrder++,
        0.0f, // default
        -1000.0f, // min
        1000.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_OPEN_(MINIMUM_UPWARD_CURVATURE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_UPWARD_CURVATURE)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MINIMUM_UPWARD_CURVATURE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumUpwardCurvatureSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_UPWARD_CURVATURE);

    addAlgorithmSetting(minimumUpwardCurvatureSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_UPWARD_CURVATURE);

    // Minimum acceptable heel slope
    AlgorithmSetting minimumHeelSlopeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE,
        displayOrder++,
        0.3f, // default
        -1000.0f, // min
        1000.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_OPEN_(MINIMUM_HEEL_SLOPE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_HEEL_SLOPE)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MINIMUM_HEEL_SLOPE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumHeelSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE);
    addAlgorithmSetting(minimumHeelSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE);
    
    //Lim Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum acceptable heel slope
    AlgorithmSetting maximumHeelSlopeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE,
        displayOrder++,
       1000.f, // default
        -1000.0f, // min
        1000.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_OPEN_(MAXIMUM_HEEL_SLOPE)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_HEEL_SLOPE)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MAXIMUM_HEEL_SLOPE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumHeelSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE);//lim lay ngor: y is MAX!!
    addAlgorithmSetting(maximumHeelSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE);    
    //Lim Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum acceptable sum of slope changes along the pad
    AlgorithmSetting minimumSlopeSumChangesSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES,
      displayOrder++,
      1.0F, // default
      0.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_SLOPE_SUM_CHANGES)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_SLOPE_SUM_CHANGES)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_SLOPE_SUM_CHANGES)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumSlopeSumChangesSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES);

    addAlgorithmSetting(minimumSlopeSumChangesSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES);
    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    AlgorithmSetting maximumSlopeSumChangesSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES,
      displayOrder++,
      2000.0F, // default
      0.0f, // min
      2000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_SLOPE_SUM_CHANGES)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_SLOPE_SUM_CHANGES)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_SLOPE_SUM_CHANGES)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumSlopeSumChangesSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES);

    addAlgorithmSetting(maximumSlopeSumChangesSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum acceptable sum of leading and trailing slopes across heel
    AlgorithmSetting minimumHeelAcrossLeadingTrailingSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE,
      displayOrder++,
      0.0F, // default
      0.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumHeelAcrossLeadingTrailingSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM);

    addAlgorithmSetting(minimumHeelAcrossLeadingTrailingSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum acceptable sum of leading and trailing slopes across heel
    AlgorithmSetting maximumHeelAcrossLeadingTrailingSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE,
      displayOrder++,
      1000.0F, // default
      0.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumHeelAcrossLeadingTrailingSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM);

    addAlgorithmSetting(maximumHeelAcrossLeadingTrailingSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum acceptable sum of slopes across heel
    AlgorithmSetting minimumHeelAcrossSlopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM,
      displayOrder++,
      0.0F, // default
      0.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumHeelAcrossSlopeSumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM);

    addAlgorithmSetting(minimumHeelAcrossSlopeSumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum acceptable sum of slopes across heel
    AlgorithmSetting maximumHeelAcrossSlopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM,
      displayOrder++,
      2000.0F, // default
      0.0f, // min
      2000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_HEEL_ACROSS_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_HEEL_ACROSS_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_HEEL_ACROSS_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumHeelAcrossSlopeSumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM);

    addAlgorithmSetting(maximumHeelAcrossSlopeSumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
    
    // Minimum acceptable width of joint across center (in mils)
    AlgorithmSetting minimumCenterAcrossWidthSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_WIDTH,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(5.0F), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(100.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_WIDTH)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_WIDTH)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_WIDTH)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumCenterAcrossWidthSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_WIDTH);

    addAlgorithmSetting(minimumCenterAcrossWidthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_WIDTH);

    // Maximum Acceptable Neighbor Length Difference
    AlgorithmSetting maximumNeighborLengthDifferenceSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(1000.0F), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(1000.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumNeighborLengthDifferenceSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_NEIGHBOR_LENGTH_DIFFERENCE);

    addAlgorithmSetting(maximumNeighborLengthDifferenceSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE);

    // Additonal thresholds

    // Weight factor for the Heel thickness to compute the Open signal
    AlgorithmSetting heelWeightSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_HEEL_WEIGHT,
      displayOrder++,
      0.0F, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(HEEL_WEIGHT)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(HEEL_WEIGHT)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(HEEL_WEIGHT)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(heelWeightSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_HEEL_WEIGHT);

    // Weight factor for the Toe thickness to compute the Open signal
    AlgorithmSetting toeWeightSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_TOE_WEIGHT,
      displayOrder++,
      1.0F, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(TOE_WEIGHT)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(TOE_WEIGHT)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(TOE_WEIGHT)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(toeWeightSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_TOE_WEIGHT);

    // Weight factor for the Center thickness to compute the Open signal
    AlgorithmSetting centerWeightSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_CENTER_WEIGHT,
      displayOrder++,
      -1.0F, // default
      -100.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(CENTER_WEIGHT)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(CENTER_WEIGHT)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(CENTER_WEIGHT)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(centerWeightSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_CENTER_WEIGHT);

    // Minimum Acceptable Heel Sharpness
    AlgorithmSetting heelSharpnessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SHARPNESS,
      displayOrder++,
      -1000.0F, // default
      -1000.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_HEEL_SHARPNESS)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_HEEL_SHARPNESS)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_HEEL_SHARPNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        heelSharpnessSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_SHARPNESS);
    addAlgorithmSetting(heelSharpnessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SHARPNESS);

    // Minimum acceptable toe slope
    AlgorithmSetting toeSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE,
      displayOrder++,
      -1000.0F, // default
      -1000.0f, // min4q
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_TOE_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_TOE_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_TOE_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        toeSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE);
    addAlgorithmSetting(toeSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE);
    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum acceptable toe slope
    AlgorithmSetting toeSlopeMaximumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE,
      displayOrder++,
      1000.0F, // default
      -1000.0f, // min4q
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_TOE_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_TOE_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_TOE_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        toeSlopeMaximumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE);
    addAlgorithmSetting(toeSlopeMaximumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE); 
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
    
    // Minimum acceptable center slope
    AlgorithmSetting centerSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE,
      displayOrder++,
      -1000.0F, // default
      -1000.0f, // min4q
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_CENTER_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_CENTER_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_CENTER_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        centerSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE);
    addAlgorithmSetting(centerSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE);
    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum acceptable center slope
    AlgorithmSetting centerSlopeMaximumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE,
      displayOrder++,
      1000.0F, // default
      -1000.0f, // min4q
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_CENTER_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_CENTER_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_CENTER_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        centerSlopeMaximumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE);
    addAlgorithmSetting(centerSlopeMaximumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum acceptable sum of heel and toe slopes
    AlgorithmSetting slopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM,
      displayOrder++,
      -1000.0F, // default
      -1000.0f, // min4q
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        slopeSumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM);
    addAlgorithmSetting(slopeSumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM);
    
    // Kee Chin Seong - XCR1648:Maximum acceptable sum of heel and toe slopes
    AlgorithmSetting maxSlopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM,
      displayOrder++,
      2000.0F, // default
      -1000.0f, // min4q
      2000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        slopeSumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM);
    addAlgorithmSetting(maxSlopeSumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM);

    // Minimum acceptable width of joint across heel (in mils)
    AlgorithmSetting minimumHeelAcrossWidthSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_WIDTH,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(0.0F), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(100.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_WIDTH)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_WIDTH)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_HEEL_ACROSS_WIDTH)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumHeelAcrossWidthSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_WIDTH);
    addAlgorithmSetting(minimumHeelAcrossWidthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_WIDTH);

    // Minimum acceptable sum of leading and trailing slopes across center
    AlgorithmSetting minimumCenterAcrossLeadingTrailingSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE,
      displayOrder++,
      0.0F, // default
      0.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumCenterAcrossLeadingTrailingSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM);
    addAlgorithmSetting(minimumCenterAcrossLeadingTrailingSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE);
    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum acceptable sum of leading and trailing slopes across center
    AlgorithmSetting maximumCenterAcrossLeadingTrailingSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE,
      displayOrder++,
      1000.0F, // default
      0.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumCenterAcrossLeadingTrailingSlopeSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM);
    addAlgorithmSetting(maximumCenterAcrossLeadingTrailingSlopeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    //  Minimum acceptable sum of slopes across center
    AlgorithmSetting minimumCenterAcrossSlopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM,
      displayOrder++,
      0.0F, // default
      0.0f, // min
      1000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_CENTER_ACROSS_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumCenterAcrossSlopeSumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM);
    addAlgorithmSetting(minimumCenterAcrossSlopeSumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM);
    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    //  Maximum acceptable sum of slopes across center
    AlgorithmSetting maximumCenterAcrossSlopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM,
      displayOrder++,
      2000.0F, // default
      0.0f, // min
      2000.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_CENTER_ACROSS_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_CENTER_ACROSS_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_CENTER_ACROSS_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumCenterAcrossSlopeSumSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM);
    addAlgorithmSetting(maximumCenterAcrossSlopeSumSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Percentage of center to heel percent
    // NOTE: This is only enable if the the enable setting above indicate ON
    AlgorithmSetting maximumCenterToHeelPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT,
        displayOrder++,
        130.0F, // default
        -100.0f, // min
        300.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_OPEN_(MAXIMUM_CENTER_TO_HEEL_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_CENTER_TO_HEEL_PERCENT)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MAXIMUM_CENTER_TO_HEEL_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumCenterToHeelPercentSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT);
    addAlgorithmSetting(maximumCenterToHeelPercentSetting);

    // Enable center to toe testing if center to heel measurement fail
    ArrayList<String> enableCenterToToePercentTestValues = new ArrayList<String>(Arrays.asList("On", "Off"));
    AlgorithmSetting enableCenterToToePercentTestSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST,
        displayOrder++,
        "Off",
        enableCenterToToePercentTestValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_QFN_OPEN_(ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(enableCenterToToePercentTestSetting);

    // Percentage of center to toe percent
    AlgorithmSetting maximumCenterToToePercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT,
        displayOrder++,
        130.0F, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_OPEN_(MAXIMUM_CENTER_TO_TOE_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_CENTER_TO_TOE_PERCENT)_KEY", // detailed desc
        "IMG_DESC_QFN_OPEN_(MAXIMUM_CENTER_TO_TOE_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumCenterToToePercentSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT);
    addAlgorithmSetting(maximumCenterToToePercentSetting);

    // Minimum Solder Thickness At [User Specified] Location
    AlgorithmSetting minimumSolderThicknessAtLocationSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SOLDER_THICKNESS_AT_LOCATION,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(0.0F), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(100.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_QFN_OPEN_(MINIMUM_SOLDER_THICKNESS_AT_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_SOLDER_THICKNESS_AT_LOCATION)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_SOLDER_THICKNESS_AT_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minimumSolderThicknessAtLocationSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_SOLDER_THICKNESS_AT_LOCATION);
    addAlgorithmSetting(minimumSolderThicknessAtLocationSetting);

    // Maximum center of solder volume location percent of nominal
    // CR33010 - setting is hidden until proven useful
    AlgorithmSetting maximumCenterOfSolderVolumePercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL,
        displayOrder++,
        200.0f, // default
        0.0f, // min
        500.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_QFN_INSUFFICIENT_(MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_QFN_INSUFFICIENT_(MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_QFN_INSUFFICIENT_(MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.HIDDEN,
        currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maximumCenterOfSolderVolumePercentOfNominalSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_MEASUREMENT_CENTER_OF_SOLDER_VOLUME);
    addAlgorithmSetting(maximumCenterOfSolderVolumePercentOfNominalSetting);
    
    //Siew Yeng - XCR-3532 - fillet length outlier
    AlgorithmSetting maxFilletLengthRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER,
      displayOrder++,
      300.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maxFilletLengthRegionOutlierSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_FILLET_LENGTH_REGION_OUTLIER);
    addAlgorithmSetting(maxFilletLengthRegionOutlierSetting);  
     
    AlgorithmSetting minFilletLengthRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER,
      displayOrder++,
      0.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_QFN_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minFilletLengthRegionOutlierSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_FILLET_LENGTH_REGION_OUTLIER);
    addAlgorithmSetting(minFilletLengthRegionOutlierSetting);
    
     AlgorithmSetting maxCenterThicknessRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER,
      displayOrder++,
      300.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_QFN_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        maxCenterThicknessRegionOutlierSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_REGION_OUTLIER);
    addAlgorithmSetting(maxCenterThicknessRegionOutlierSetting);
    
    AlgorithmSetting minCenterThicknessRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.QFN_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER,
      displayOrder++,
      0.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_QFN_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_QFN_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_QFN_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    quadFlatNoLeadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD,
                                                                        minCenterThicknessRegionOutlierSetting,
                                                                        SliceNameEnum.PAD,
                                                                        MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_REGION_OUTLIER);
    addAlgorithmSetting(minCenterThicknessRegionOutlierSetting);
    
    addMeasurementEnums();
  }

  /**
   * @author George Booth
   */
  protected QuadFlatNoLeadOpenAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.OPEN, inspectionFamilyEnum);
  }

  /**
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.QFN_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_OPEN_SIGNAL);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST);
    // CR33010 - measurement is commented
    // _jointMeasurementEnums.add(MeasurementEnum.QFN_OPEN_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL);
  }

  /**
   * Runs 'Open' on all the given joints.
   *
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    // QFN only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

      // Run the QFN Open classification.
      boolean jointPassed = classifyJoint(subtype,
                    padSlice,
                    jointInspectionData);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed);
    }
  }

  /**
   * Does the QFN Open classification on the specified joint.
   *
   * @author George Booth
   */
  protected boolean classifyJoint(Subtype subtype,
                               SliceNameEnum sliceNameEnum,
                               JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    boolean jointPassed = true;

    // Detect opens by checking for maximum fillet length
    jointPassed &= detectOpensDueToFilletLength(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking for maximum center thickness
    jointPassed &= detectOpensDueToCenterThickness(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking the open signal
    jointPassed &= detectOpensDueToOpenSignal(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking upward curvature
    jointPassed &= detectOpensDueToUpwardCurvature(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking heel slope
    jointPassed &= detectOpensDueToHeelSlope(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking center slope
    jointPassed &= detectOpensDueToCenterSlope(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking toe slope
    jointPassed &= detectOpensDueToToeSlope(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking heel / toe slope sum
    jointPassed &= detectOpensDueToHeelToeSlopeSum(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking sum of slope changes
    jointPassed &= detectOpensDueToSumOfSlopeChanges(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking heel across width
    jointPassed &= detectOpensDueToHeelAcrossWidth(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking heel across slope sum
    jointPassed &= detectOpensDueToHeelAcrossSlopeSum(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking heel across leading-trailing slope sum
    jointPassed &= detectOpensDueToHeelAcrossLeadingTrailingSlopeSum(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking center across width
    jointPassed &= detectOpensDueToCenterAcrossWidth(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking center across slope sum
    jointPassed &= detectOpensDueToCenterAcrossSlopeSum(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking center across leading-trailing slope sum
    jointPassed &= detectOpensDueToCenterAcrossLeadingTrailingSlopeSum(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking heel sharpness
    jointPassed &= detectOpensDueToHeelSharpness(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking neighbor difference
    jointPassed &= detectOpensDueToNeighborLengthDifference(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking max heel/toe to center percentage
    jointPassed &= detectOpensDueToCenterToToeOrHeelPercent(subtype, sliceNameEnum, jointInspectionData);

    // Detect opens by checking solder thickness at [user specified] location
    jointPassed &= detectOpensDueToSolderThicknessAtLocation(subtype, sliceNameEnum, jointInspectionData);
    
    // Detect opens by checking fillet length outlier
    jointPassed &= detectOpensDueToFilletLengthRegionOutlier(jointInspectionData, sliceNameEnum);
    
    // Detect opens by checking center thickness outlier
    jointPassed &= detectOpensDueToCenterThicknessRegionOutlier(jointInspectionData, sliceNameEnum);

    // Detect opens by checking center of solder volume location (percent of nominal)
    // CR33010 - measurement is commented
    // jointPassed &= detectOpensDueToCenterOfSolderVolumeLocation(subtype, sliceNameEnum, jointInspectionData);

    return jointPassed;
  }
  
  /**
   * Detects opens using the fillet length.
   *
   * @author George Booth
   */
  private boolean detectOpensDueToFilletLength(Subtype subtype,
                                               SliceNameEnum sliceNameEnum,
                                               JointInspectionData jointInspectionData)

  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    boolean filletLengthBelowMinimum = false;
    boolean filletLengthAboveMaximum = false;

    // Check to see if the fillet length is within acceptable limits.
    final float NOMINAL_FILLET_LENGTH =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH);
    final float MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    final float MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    JointMeasurement filletLengthMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getFilletLengthMeasurement(jointInspectionData, sliceNameEnum);
    float filletLength = filletLengthMeasurement.getValue();
    Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(filletLength, 0f));

    float filletLengthPercentOfNominal = (filletLength / NOMINAL_FILLET_LENGTH) * 100f;
    JointMeasurement filletLengthPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_FILLET_LENGTH_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             filletLengthPercentOfNominal);
    jointInspectionResult.addMeasurement(filletLengthPercentOfNominalMeasurement);

    if (filletLengthPercentOfNominal < MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)
    {
      // Fillet length is below the allowable minimum.
      filletLengthBelowMinimum = true;
    }
    else if (filletLengthPercentOfNominal > MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)
    {
      // Fillet length is above the allowable maximum.
      filletLengthAboveMaximum = true;
    }

    boolean passingJoint = true;

    // Did the joint fail the fillet length check?
    if (filletLengthBelowMinimum || filletLengthAboveMaximum)
    {
      // Create a new indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Tie the applicable measurements to the indictment.
      openIndictment.addFailingMeasurement(filletLengthPercentOfNominalMeasurement);
      openIndictment.addRelatedMeasurement(filletLengthMeasurement);

      if (filletLengthBelowMinimum)
      {
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        jointInspectionData.getInspectionRegion(),
                                                        sliceNameEnum,
                                                        filletLengthPercentOfNominalMeasurement,
                                                        MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
      }
      else if (filletLengthAboveMaximum)
      {
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        jointInspectionData.getInspectionRegion(),
                                                        sliceNameEnum,
                                                        filletLengthPercentOfNominalMeasurement,
                                                        MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
      }

      // Add the indictment to the JointInspectionResult.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      passingJoint = false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletLengthPercentOfNominalMeasurement,
                                                      MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    }
    return passingJoint;
  }

  /**
   * Detect opens due to center thickness
   *
   * @author George Booth
   */
  private boolean detectOpensDueToCenterThickness(Subtype subtype,
                                                  SliceNameEnum sliceNameEnum,
                                                  JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the center thickness is less than maximum
    final float MAXIMUM_CENTER_THICKNESS_FRACTION =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL);

    final float NOMINAL_CENTER_THICKNESS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS);

    JointMeasurement centerThicknessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float centerThickness = centerThicknessMeasurement.getValue();

    float centerThicknessPercentOfNominal = 0.0f;
    if (NOMINAL_CENTER_THICKNESS > 0.0F)
    {
      centerThicknessPercentOfNominal = (centerThickness / NOMINAL_CENTER_THICKNESS) * 100.0f;
    }
    else
    {
      centerThicknessPercentOfNominal = 1000.0f;
    }

    JointMeasurement centerThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             centerThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(centerThicknessPercentOfNominalMeasurement);

    if (centerThicknessPercentOfNominal > MAXIMUM_CENTER_THICKNESS_FRACTION)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerThicknessPercentOfNominalMeasurement,
                                                      MAXIMUM_CENTER_THICKNESS_FRACTION);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(centerThicknessPercentOfNominalMeasurement);
      openIndictment.addRelatedMeasurement(centerThicknessMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    centerThicknessPercentOfNominalMeasurement,
                                                    MAXIMUM_CENTER_THICKNESS_FRACTION);
    return true;
  }

  /**
   * Detect opens due to open signal
   *
   * @author George Booth
   */
  private boolean detectOpensDueToOpenSignal(Subtype subtype,
                                             SliceNameEnum sliceNameEnum,
                                             JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the open signal is less than minimum
    final float MINIMUM_OPEN_SIGNAL =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_OPEN_SIGNAL);

    final float HEEL_WEIGHT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_HEEL_WEIGHT);

    final float CENTER_WEIGHT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_CENTER_WEIGHT);

    final float TOE_WEIGHT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_TOE_WEIGHT);

    JointMeasurement heelThicknessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getHeelThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float heelThickness = heelThicknessMeasurement.getValue();

    JointMeasurement centerThicknessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float centerThickness = centerThicknessMeasurement.getValue();

    JointMeasurement toeThicknessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getToeThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float toeThickness = toeThicknessMeasurement.getValue();

    float openSignal = HEEL_WEIGHT * heelThickness + CENTER_WEIGHT * centerThickness + TOE_WEIGHT * toeThickness;
    JointMeasurement openSignalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_OPEN_OPEN_SIGNAL,
                             MeasurementUnitsEnum.MILLIMETERS,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             openSignal);
    jointInspectionResult.addMeasurement(openSignalMeasurement);

    if (openSignal < MINIMUM_OPEN_SIGNAL)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      openSignalMeasurement,
                                                      MINIMUM_OPEN_SIGNAL);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(openSignalMeasurement);
      openIndictment.addRelatedMeasurement(heelThicknessMeasurement);
      openIndictment.addRelatedMeasurement(centerThicknessMeasurement);
      openIndictment.addRelatedMeasurement(toeThicknessMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    openSignalMeasurement,
                                                    MINIMUM_OPEN_SIGNAL);
    return true;
  }

  /**
   * Detect opens due to upward curvature
   *
   * @author George Booth
   */
  private boolean detectOpensDueToUpwardCurvature(Subtype subtype,
                                                  SliceNameEnum sliceNameEnum,
                                                  JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the upward curvature is less than minimum
    final float MINIMUM_UPWARD_CURVATURE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_UPWARD_CURVATURE);

    JointMeasurement upwardCurvatureMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getUpwardCurvatureMeasurement(jointInspectionData, sliceNameEnum);
    float upwardCurvature = upwardCurvatureMeasurement.getValue();

    if (upwardCurvature < MINIMUM_UPWARD_CURVATURE)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      upwardCurvatureMeasurement,
                                                      MINIMUM_UPWARD_CURVATURE);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(upwardCurvatureMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    upwardCurvatureMeasurement,
                                                    MINIMUM_UPWARD_CURVATURE);
    return true;
  }

  /**
   * Detect opens due to heel slope
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit
   *
   * @author George Booth
   */
  private boolean detectOpensDueToHeelSlope(Subtype subtype,
                                            SliceNameEnum sliceNameEnum,
                                            JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    
    boolean jointPassed = true;

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the heel slope is less than minimum or greater than maximum
    final float MINIMUM_HEEL_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE);
    final float MAXIMUM_HEEL_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE);

    JointMeasurement heelSlopeMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getHeelSlopeMeasurement(jointInspectionData, sliceNameEnum);
    float heelSlope = heelSlopeMeasurement.getValue();

    if (heelSlope < MINIMUM_HEEL_SLOPE)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      heelSlopeMeasurement,
                                                      MINIMUM_HEEL_SLOPE);

      // Flag the joint as failing.
      jointPassed = false;
    }
    else if (heelSlope > MAXIMUM_HEEL_SLOPE)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      heelSlopeMeasurement,
                                                      MAXIMUM_HEEL_SLOPE);

      // Flag the joint as failing.
      jointPassed = false;
    }
    //else // Retain the Flag of joint as passing.
    
    if(jointPassed)
    {
      //Only post minimum threshold value for passing joint.      
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    heelSlopeMeasurement,
                                                    MINIMUM_HEEL_SLOPE);
      return true;
    }
    
    // Joint failing indictment
    // Create an indictment.
    JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

    // Add the failing measurement.
    openIndictment.addFailingMeasurement(heelSlopeMeasurement);

    // Tie the indictment to this joint's results.
    jointInspectionResult.addIndictment(openIndictment);

    return false;    
  }

  /**
   * Detect opens due to heel sharpness
   *
   * @author George Booth
   */
  private boolean detectOpensDueToHeelSharpness(Subtype subtype,
                                                SliceNameEnum sliceNameEnum,
                                                JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the heel sharpness is less than minimum
    final float MINIMUM_HEEL_SHARPNESS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SHARPNESS);

    JointMeasurement heelSharpnessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getHeelSharpnessMeasurement(jointInspectionData, sliceNameEnum);
    float heelSharpness = heelSharpnessMeasurement.getValue();

    if (heelSharpness < MINIMUM_HEEL_SHARPNESS)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      heelSharpnessMeasurement,
                                                      MINIMUM_HEEL_SHARPNESS);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(heelSharpnessMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      heelSharpnessMeasurement,
                                                      MINIMUM_HEEL_SHARPNESS);
    return true;
  }

  /**
   * Detect opens due to sum of slope changes
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit
   *
   * @author George Booth
   */
  private boolean detectOpensDueToSumOfSlopeChanges(Subtype subtype,
                                                    SliceNameEnum sliceNameEnum,
                                                    JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    boolean jointPassed = true;
    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the sum of slope changes is less than minimum or greater than maximum
    final float MINIMUM_SUM_OF_SLOPE_CHANGES =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES);
    final float MAXIMUM_SUM_OF_SLOPE_CHANGES =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES);

    JointMeasurement slopeChangesMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getSumOfSlopeChangesMeasurement(jointInspectionData, sliceNameEnum);
    float slopeChanges = slopeChangesMeasurement.getValue();

    if (slopeChanges < MINIMUM_SUM_OF_SLOPE_CHANGES)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      slopeChangesMeasurement,
                                                      MINIMUM_SUM_OF_SLOPE_CHANGES);

      // Flag the joint as failing.
      jointPassed = false;
    }
    else if (slopeChanges > MAXIMUM_SUM_OF_SLOPE_CHANGES)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      slopeChangesMeasurement,
                                                      MAXIMUM_SUM_OF_SLOPE_CHANGES);

      // Flag the joint as failing.
      jointPassed =  false;
    }
    //else // Retain the Flag of joint as passing.      
    
    if(jointPassed)
    {
      //Only post minimum threshold value for passing joint - to avoid posting duplicate measurement result.  
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      slopeChangesMeasurement,
                                                      MINIMUM_SUM_OF_SLOPE_CHANGES);
      return true;
    }
    
    // Joint failing indictment
    // Create an indictment.
    JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

    // Add the failing measurement.
    openIndictment.addFailingMeasurement(slopeChangesMeasurement);

    // Tie the indictment to this joint's results.
    jointInspectionResult.addIndictment(openIndictment);

    return false;
  }

  /**
   * Detect opens due to center across width
   *
   * @author George Booth
   */
  private boolean detectOpensDueToHeelAcrossWidth(Subtype subtype,
                                                  SliceNameEnum sliceNameEnum,
                                                  JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the center across width sum is less than minimum
    final float MINIMUM_HEEL_ACROSS_WIDTH =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_WIDTH);

    JointMeasurement heelAcrossWidthMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getHeelAcrossWidthMeasurement(jointInspectionData, sliceNameEnum);
    float heelAcrossWidth = heelAcrossWidthMeasurement.getValue();

    if (heelAcrossWidth < MINIMUM_HEEL_ACROSS_WIDTH)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      heelAcrossWidthMeasurement,
                                                      MINIMUM_HEEL_ACROSS_WIDTH);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(heelAcrossWidthMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    heelAcrossWidthMeasurement,
                                                    MINIMUM_HEEL_ACROSS_WIDTH);
    return true;
  }

  /**
   * Detect opens due to heel across slope sums
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization.
   * 
   * @author George Booth
   */
  private boolean detectOpensDueToHeelAcrossSlopeSum(Subtype subtype,
                                                     SliceNameEnum sliceNameEnum,
                                                     JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the JointInspectionResult.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the heel across slope sum is less than minimum or greater than maximum
    final float MINIMUM_HEEL_ACROSS_SLOPE_SUM =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM);
    final float MAXIMUM_HEEL_ACROSS_SLOPE_SUM =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM);    

    JointMeasurement heelAcrossSlopeSumMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getHeelAcrossSlopeSumMeasurement(jointInspectionData, sliceNameEnum);
    //float heelAcrossSlopeSum = heelAcrossSlopeSumMeasurement.getValue();

    return AlgorithmUtil.indictMinMaxThreshold(this,
                                             jointInspectionData,
                                             sliceNameEnum,
                                             IndictmentEnum.OPEN,
                                             heelAcrossSlopeSumMeasurement,
                                             MINIMUM_HEEL_ACROSS_SLOPE_SUM,
                                             MAXIMUM_HEEL_ACROSS_SLOPE_SUM,
                                             AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Detect opens due to heel across leading-trailing slope sums
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization.
   * 
   * @author George Booth
   */
  private boolean detectOpensDueToHeelAcrossLeadingTrailingSlopeSum(Subtype subtype,
                                                                    SliceNameEnum sliceNameEnum,
                                                                    JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the JointInspectionResult.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the heel across leading-trailing slope is less than minimum or greater than maximum
    final float MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE);
    final float MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE);

    JointMeasurement heelAcrossLeadingTrailingSlopeSumMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getHeelAcrossLeadingTrailingSlopeSumMeasurement(jointInspectionData, sliceNameEnum);
    //float heelAcrossLeadingTrailingSlopeSum = heelAcrossLeadingTrailingSlopeSumMeasurement.getValue();

    return AlgorithmUtil.indictMinMaxThreshold(this,
                                             jointInspectionData,
                                             sliceNameEnum,
                                             IndictmentEnum.OPEN,
                                             heelAcrossLeadingTrailingSlopeSumMeasurement,
                                             MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE,
                                             MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE,
                                             AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD);
      //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Detect opens due to center slope
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization. 
   * 
   * @author George Booth
   */
  private boolean detectOpensDueToCenterSlope(Subtype subtype,
                                              SliceNameEnum sliceNameEnum,
                                              JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the JointInspectionResult.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the center slope is less than minimum or greater than maximum
    final float MINIMUM_CENTER_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE);
    final float MAXIMUM_CENTER_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE);    

    JointMeasurement centerSlopeMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getCenterSlopeMeasurement(jointInspectionData, sliceNameEnum);
    //float centerSlope = centerSlopeMeasurement.getValue();

    return AlgorithmUtil.indictMinMaxThreshold(this,
                                             jointInspectionData,
                                             sliceNameEnum,
                                             IndictmentEnum.OPEN,
                                             centerSlopeMeasurement,
                                             MINIMUM_CENTER_SLOPE,
                                             MAXIMUM_CENTER_SLOPE,
                                             AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END    
  }

  /**
   * Detect opens due to center across width
   *
   * @author George Booth
   */
  private boolean detectOpensDueToCenterAcrossWidth(Subtype subtype,
                                                    SliceNameEnum sliceNameEnum,
                                                    JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the center across width sum is less than minimum
    final float MINIMUM_CENTER_ACROSS_WIDTH =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_WIDTH);

    JointMeasurement centerAcrossWidthMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getCenterAcrossWidthMeasurement(jointInspectionData, sliceNameEnum);
    float centerAcrossWidth = centerAcrossWidthMeasurement.getValue();

    if (centerAcrossWidth < MINIMUM_CENTER_ACROSS_WIDTH)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerAcrossWidthMeasurement,
                                                      MINIMUM_CENTER_ACROSS_WIDTH);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(centerAcrossWidthMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    centerAcrossWidthMeasurement,
                                                    MINIMUM_CENTER_ACROSS_WIDTH);
    return true;
 }

  /**
   * Detect opens due to center across slope sums
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization. 
   *
   * @author George Booth
   */
  private boolean detectOpensDueToCenterAcrossSlopeSum(Subtype subtype,
                                                       SliceNameEnum sliceNameEnum,
                                                       JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START   
    // Get the JointInspectionResult.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the center across slope sum is less than minimum and greater than maximum
    final float MINIMUM_CENTER_ACROSS_SLOPE_SUM =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM);

    final float MAXIMUM_CENTER_ACROSS_SLOPE_SUM =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM);

    JointMeasurement centerAcrossSlopeSumMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getCenterAcrossSlopeSumMeasurement(jointInspectionData, sliceNameEnum);
    //float centerAcrossSlopeSum = centerAcrossSlopeSumMeasurement.getValue();

    return AlgorithmUtil.indictMinMaxThreshold(this,
                                             jointInspectionData,
                                             sliceNameEnum,
                                             IndictmentEnum.OPEN,
                                             centerAcrossSlopeSumMeasurement,
                                             MINIMUM_CENTER_ACROSS_SLOPE_SUM,
                                             MAXIMUM_CENTER_ACROSS_SLOPE_SUM,
                                             AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Detect opens due to center across leading-trailing slope sums
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization. 
   *
   * @author George Booth
   */
  private boolean detectOpensDueToCenterAcrossLeadingTrailingSlopeSum(Subtype subtype,
                                                         SliceNameEnum sliceNameEnum,
                                                         JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the JointInspectionResult.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the center across leading-trailing slope is less than minimum and greater than maximum
    final float MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE);
    final float MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE);

    JointMeasurement centerAcrossLeadingTrailingSlopeSumMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getCenterAcrossLeadingTrailingSlopeSumMeasurement(jointInspectionData, sliceNameEnum);
    //float centerAcrossLeadingTrailingSlopeSum = centerAcrossLeadingTrailingSlopeSumMeasurement.getValue();

    return AlgorithmUtil.indictMinMaxThreshold(this,
                                             jointInspectionData,
                                             sliceNameEnum,
                                             IndictmentEnum.OPEN,
                                             centerAcrossLeadingTrailingSlopeSumMeasurement,
                                             MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE,
                                             MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE,
                                             AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Detect opens due to toe slope
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization. 
   * 
   * @author George Booth
   */
  private boolean detectOpensDueToToeSlope(Subtype subtype,
                                           SliceNameEnum sliceNameEnum,
                                           JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START    
    // Get the JointInspectionResult.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the toe slope is less than minimum
    final float MINIMUM_TOE_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE);
    final float MAXIMUM_TOE_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE);    

    JointMeasurement toeSlopeMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getToeSlopeMeasurement(jointInspectionData, sliceNameEnum);
    //float toeSlope = toeSlopeMeasurement.getValue();

    return AlgorithmUtil.indictMinMaxThreshold(this,
                                             jointInspectionData,
                                             sliceNameEnum,
                                             IndictmentEnum.OPEN,
                                             toeSlopeMeasurement,
                                             MINIMUM_TOE_SLOPE,
                                             MAXIMUM_TOE_SLOPE,
                                             AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END    
  }

  /**
   * Detect opens due to heel / toe slope sum
   *
   * @author George Booth
   */
  private boolean detectOpensDueToHeelToeSlopeSum(Subtype subtype,
                                                  SliceNameEnum sliceNameEnum,
                                                  JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the toe slope is less than minimum and greater than maximum
    final float MINIMUM_HEEL_TOE_SLOPE_SUM =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM);
    
    //Kee Chin Seong - XCR1648: Maximum created to get a range under so that can lock the defects
    final float MAXIMUM_HEEL_TOE_SLOPE_SUM = 
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM);      

    JointMeasurement heelToeSlopeSumMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getHeelToeSlopeSumMeasurement(jointInspectionData, sliceNameEnum);
    //float heelToeSlopeSum = heelToeSlopeSumMeasurement.getValue();

    //Kee Chin Seong - XCR1648: Maximum created to get a range under so that can lock the defects    
    return AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            heelToeSlopeSumMeasurement,
                                            MINIMUM_HEEL_TOE_SLOPE_SUM,
                                            MAXIMUM_HEEL_TOE_SLOPE_SUM,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD) ;
  }

  /**
   * Detect opens by checking neighbor difference
   *
   * @author George Booth
   */
  private boolean detectOpensDueToNeighborLengthDifference(Subtype subtype,
                                                           SliceNameEnum sliceNameEnum,
                                                           JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the neighbor fillet length difference > maximum
    final float MAXIMUM_FILLET_LENGTH_DIFFERENCE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE);

    JointMeasurement neighborLengthDifferenceMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getFilletLengthDifferenceMeasurement(jointInspectionData, sliceNameEnum);
    float neighborLengthDifference = neighborLengthDifferenceMeasurement.getValue();

    if (neighborLengthDifference > MAXIMUM_FILLET_LENGTH_DIFFERENCE)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      neighborLengthDifferenceMeasurement,
                                                      MAXIMUM_FILLET_LENGTH_DIFFERENCE);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(neighborLengthDifferenceMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    neighborLengthDifferenceMeasurement,
                                                    MAXIMUM_FILLET_LENGTH_DIFFERENCE);
    return true;
  }

  /**
   * The logic for the program will check the Center to Heel Thickness test first.
   * Then if the Enable Toe Test If Center to Heel Thickness Test Fail is set to Yes,
   * then the program will do the testing for the Center to Toe Thickness testing
   *
   * @author Poh Kheng
   */
  private boolean detectOpensDueToCenterToToeOrHeelPercent(Subtype subtype,
                                             SliceNameEnum sliceNameEnum,
                                             JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    final float MAXIMUM_CENTER_TO_TOE_PERCENT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT);

    final float MAXIMUM_CENTER_TO_HEEL_PERCENT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT);

    JointMeasurement heelThicknessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getHeelThicknessMeasurement(jointInspectionData, sliceNameEnum);

    JointMeasurement centerThicknessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float centerThickness = centerThicknessMeasurement.getValue();

    JointMeasurement toeThicknessMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getToeThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float heelThickness = heelThicknessMeasurement.getValue();
    
    //XCR1664-Make sure denominator not equal to 0
    // Check center to heel percent
    float centerToHeelPercent;
    if (MathUtil.fuzzyEquals(heelThickness, 0f))
    {
      centerToHeelPercent = 999;
    }
    else
    {
      centerToHeelPercent = centerThickness / heelThickness * 100;
    }

    JointMeasurement centerToHeelPercentMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             centerToHeelPercent);
    jointInspectionResult.addMeasurement(centerToHeelPercentMeasurement);

    // Check to see if the center to heel percentage is more than maximum
    if (centerToHeelPercent > MAXIMUM_CENTER_TO_HEEL_PERCENT)
    {

      // If the center to heel fail, we need to check if the enable toe test in enable to check the center to toe percent.
      final String ENABLE_CENTER_TO_TOE_PERCENT_TEST =
        (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST);


      float toeThickness = toeThicknessMeasurement.getValue();
      //XCR1664-Make sure denominator not equal to 0
      // Check center to toe percent
      float centerToToePercent;
      if (MathUtil.fuzzyEquals(toeThickness, 0f))
      {
        centerToToePercent = 999;
      }
      else
      {
        centerToToePercent = centerThickness / toeThickness * 100;
      }

      JointMeasurement centerToToePercentMeasurement =
            new JointMeasurement(this,
              MeasurementEnum.QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT,
              MeasurementUnitsEnum.PERCENT,
              jointInspectionData.getPad(),
              sliceNameEnum,
              centerToToePercent);
      jointInspectionResult.addMeasurement(centerToToePercentMeasurement);

      if(ENABLE_CENTER_TO_TOE_PERCENT_TEST.equals("On"))
      {
        if (centerToToePercent > MAXIMUM_CENTER_TO_TOE_PERCENT)
        {
          // the data will be display below with the toe failure message
        }
        else
        {
          AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                  jointInspectionData,
                                                  sliceNameEnum,
                                                  jointInspectionData.getInspectionRegion(),
                                                  centerToToePercentMeasurement,
                                                  MAXIMUM_CENTER_TO_TOE_PERCENT);
          return true;
        }
      }

      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerToHeelPercentMeasurement,
                                                      MAXIMUM_CENTER_TO_HEEL_PERCENT);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(centerToHeelPercentMeasurement);
      openIndictment.addRelatedMeasurement(centerThicknessMeasurement);
      openIndictment.addRelatedMeasurement(heelThicknessMeasurement);

      // Add in additional toe fail information
      if(ENABLE_CENTER_TO_TOE_PERCENT_TEST.equals("On"))
      {

        // Indict the joint for 'open'.
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerToToePercentMeasurement,
                                                      MAXIMUM_CENTER_TO_TOE_PERCENT);
        openIndictment.addFailingMeasurement(centerToToePercentMeasurement);
        openIndictment.addRelatedMeasurement(toeThicknessMeasurement);
      }

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    centerToHeelPercentMeasurement,
                                                    MAXIMUM_CENTER_TO_HEEL_PERCENT);
    return true;
  }

  /**
   * Detect opens due to solder thickness at [user specified] location
   *
   * @author George Booth
   */
  private boolean detectOpensDueToSolderThicknessAtLocation(Subtype subtype,
                                                            SliceNameEnum sliceNameEnum,
                                                            JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the solder thickness is less than minimum
    final float MINIMUM_SOLDER_THICKNESS_AT_LOCATION =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SOLDER_THICKNESS_AT_LOCATION);

    JointMeasurement solderThicknessAtLocationMeasurement =
      QuadFlatNoLeadMeasurementAlgorithm.getSolderThicknessAtLocationMeasurement(jointInspectionData, sliceNameEnum);
    float solderThicknessAtLocation = solderThicknessAtLocationMeasurement.getValue();

    if (solderThicknessAtLocation < MINIMUM_SOLDER_THICKNESS_AT_LOCATION)
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      solderThicknessAtLocationMeasurement,
                                                      MINIMUM_SOLDER_THICKNESS_AT_LOCATION);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(solderThicknessAtLocationMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    solderThicknessAtLocationMeasurement,
                                                    MINIMUM_SOLDER_THICKNESS_AT_LOCATION);
    return true;
  }

  /**
   * Detect opens due to center of solder volume location (percent of nominal)
   *
   * @author George Booth
   */
  private boolean detectOpensDueToCenterOfSolderVolumeLocation(Subtype subtype,
                                                            SliceNameEnum sliceNameEnum,
                                                            JointInspectionData jointInspectionData)

  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate tested joint's center of solder location percent of the nominal center location
    final float NOMINAL_CENTER_OF_SOLDER_VOLUME =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME);

    JointMeasurement centerOfSolderVolumeMeasurement =
        QuadFlatNoLeadMeasurementAlgorithm.getCenterOfSolderVolumeMeasurement(jointInspectionData, sliceNameEnum);
    float centerOfSolderVolume = centerOfSolderVolumeMeasurement.getValue();

    float centerOfSolderVolumePercentOfNominal = (centerOfSolderVolume / (float)NOMINAL_CENTER_OF_SOLDER_VOLUME) * 100f;

    JointMeasurement centerOfSolderVolumePercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.QFN_OPEN_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             centerOfSolderVolumePercentOfNominal);
    jointInspectionResult.addMeasurement(centerOfSolderVolumePercentOfNominalMeasurement);

    // Check to see if the center of solder volume location is within acceptable tolerance of nominal.
    final float MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL);

    if (MathUtil.fuzzyGreaterThan(centerOfSolderVolumePercentOfNominal, MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerOfSolderVolumeMeasurement,
                                                      MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing and related measurement.
      openIndictment.addFailingMeasurement(centerOfSolderVolumePercentOfNominalMeasurement);
      openIndictment.addRelatedMeasurement(centerOfSolderVolumeMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    centerOfSolderVolumeMeasurement,
                                                    MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL);
    return true;
  }

  /**
   * Resets all learned defect settings back to their default values.
   *
   * @author George Booth
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE);
    }
  }

  /**
   * @author Siew Yeng
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);
    
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      learnHeelAndToeSlopeDefectSettings(subtype, padSlice);
    }
  }
  
  /**
   * Learns the "max heel and toe slope" defect settings.
   * @author Siew Yeng
   */
  private void learnHeelAndToeSlopeDefectSettings(Subtype subtype,
                                                  SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Get all the learned "maximum heel slope" values.
    float[] maximumHeelSlopeValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                              MeasurementEnum.QFN_MEASUREMENT_MAX_HEEL_SLOPE,
                                                                              true,
                                                                              true);

    // We can only do the IQR statistics if we have at least two data points.
    if (maximumHeelSlopeValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(maximumHeelSlopeValues);
      maximumHeelSlopeValues = null;
      float medianMaxHeelSlope = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE))
      {
        float minimumAcceptableHeelSlope = medianMaxHeelSlope - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_SLOPE, minimumAcceptableHeelSlope);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE))
      {
        float maximumAcceptableHeelSlope = medianMaxHeelSlope + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_SLOPE, maximumAcceptableHeelSlope);
      }
    }

    // Get all the learned "maximum toe slope" values.
    float[] maximumToeSlopeValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                             MeasurementEnum.QFN_MEASUREMENT_MAX_TOE_SLOPE,
                                                                             true,
                                                                             true);

    // We can only do the IQR statistics if we have at least two data points.
    if (maximumToeSlopeValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(maximumToeSlopeValues);
      maximumToeSlopeValues = null;
      float medianMaxToeSlope = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE))
      {
        float minimumAcceptableToeSlope = medianMaxToeSlope - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_TOE_SLOPE, minimumAcceptableToeSlope);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE))
      {
        float maximumAcceptableToeSlope = medianMaxToeSlope + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_TOE_SLOPE, maximumAcceptableToeSlope);
      }
    }
    
    // Get all the learned "center slope" values.
    float[] centerSlopeValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                             MeasurementEnum.QFN_MEASUREMENT_CENTER_SLOPE,
                                                                             true,
                                                                             true);

    // We can only do the IQR statistics if we have at least two data points.
    if (centerSlopeValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(centerSlopeValues);
      centerSlopeValues = null;
      float medianCenterSlope = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE))
      {
        float minimumAcceptableCenterSlope = medianCenterSlope - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_SLOPE, minimumAcceptableCenterSlope);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE))
      {
        float maximumAcceptableCenterSlope = medianCenterSlope + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_SLOPE, maximumAcceptableCenterSlope);
      }
    }

    // Get all the learned "heel and toe slope sum" values.
    float[] heelAndToeSlopesSumValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (heelAndToeSlopesSumValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(heelAndToeSlopesSumValues);
      heelAndToeSlopesSumValues = null;
      float medianHeelAndToeSlopeSum = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM))
      {
        float minimumAcceptableHeelAndToeSlopeSum = medianHeelAndToeSlopeSum - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM,
                                minimumAcceptableHeelAndToeSlopeSum);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM))
      {
        float maximumAcceptableHeelAndToeSlopeSum = medianHeelAndToeSlopeSum + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM,
                                maximumAcceptableHeelAndToeSlopeSum);
      }
    }
    
    // Get all the learned "sum of slope changes" values.
    float[] sumOfSlopeChangesValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (sumOfSlopeChangesValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(sumOfSlopeChangesValues);
      sumOfSlopeChangesValues = null;
      float medianSumOfSlopeChanges = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES))
      {
        float minimumAcceptableSumOfSlopeChanges = medianSumOfSlopeChanges - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES,
                                minimumAcceptableSumOfSlopeChanges);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES))
      {
        float maximumAcceptableSumOfSlopeChanges = medianSumOfSlopeChanges + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES,
                                maximumAcceptableSumOfSlopeChanges);
      }
    }
    
    // Get all the learned "Across Heel Sum of Slopes" values.
    float[] acrossHeelSumOfSlopesValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (acrossHeelSumOfSlopesValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(acrossHeelSumOfSlopesValues);
      acrossHeelSumOfSlopesValues = null;
      float medianAcrossHeelSumOfSlopesSum = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM))
      {
        float minimumAcceptableAcrossHeelSumOfSlopes = medianAcrossHeelSumOfSlopesSum - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM,
                                minimumAcceptableAcrossHeelSumOfSlopes);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM))
      {
        float maximumAcceptableAcrossHeelSumOfSlopes = medianAcrossHeelSumOfSlopesSum + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM,
                                maximumAcceptableAcrossHeelSumOfSlopes);
      }
    }
    
    // Get all the learned "Across Heel Leading/Trailing Slope Sum" values.
    float[] acrossHeelLeadingTrailingSlopeSumValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (acrossHeelLeadingTrailingSlopeSumValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(acrossHeelLeadingTrailingSlopeSumValues);
      acrossHeelLeadingTrailingSlopeSumValues = null;
      float medianAcrossHeelLeadingTrailingSlopeSum = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE))
      {
        float minimumAcceptableAcrossHeelLeadingTrailingSlopeSum = medianAcrossHeelLeadingTrailingSlopeSum - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE,
                                minimumAcceptableAcrossHeelLeadingTrailingSlopeSum);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE))
      {
        float maximumAcceptableAcrossHeelLeadingTrailingSlopeSum = medianAcrossHeelLeadingTrailingSlopeSum + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE,
                                maximumAcceptableAcrossHeelLeadingTrailingSlopeSum);
      }
    }
    
    // Get all the learned "Across Center Sum of Slopes" values.
    float[] acrossCenterSumOfSlopesValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (acrossCenterSumOfSlopesValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(acrossCenterSumOfSlopesValues);
      acrossCenterSumOfSlopesValues = null;
      float medianAcrossCenterSumOfSlopes = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM))
      {
        float minimumAcceptableAcrossCenterSumOfSlopes = medianAcrossCenterSumOfSlopes - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM,
                                minimumAcceptableAcrossCenterSumOfSlopes);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM))
      {
        float maximumAcceptableAcrossCenterSumOfSlopes = medianAcrossCenterSumOfSlopes + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM,
                                maximumAcceptableAcrossCenterSumOfSlopes);
      }
    }
    
    // Get all the learned "Across Center Leading/Trailing Slope Sum" values.
    float[] acrossCenterLeadingTrailingSlopeSumValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (acrossCenterLeadingTrailingSlopeSumValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(acrossCenterLeadingTrailingSlopeSumValues);
      acrossCenterLeadingTrailingSlopeSumValues = null;
      float medianAcrossCenterLeadingTrailingSlopeSum = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE))
      {
        float minimumAcceptableAcrossCenterLeadingTrailingSlopeSum = medianAcrossCenterLeadingTrailingSlopeSum - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE,
                                minimumAcceptableAcrossCenterLeadingTrailingSlopeSum);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE))
      {
        float maximumAcceptableAcrossCenterLeadingTrailingSlopeSum = medianAcrossCenterLeadingTrailingSlopeSum + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE,
                                maximumAcceptableAcrossCenterLeadingTrailingSlopeSum);
      }
    }
  }
  
  /**
   * XCR-3532
   * @author Siew Yeng
   */
  private boolean detectOpensDueToFilletLengthRegionOutlier(JointInspectionData jointInspectionData, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    Subtype subtype = jointInspectionData.getSubtype();
    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the "concavity ratio across" measurement.
    JointMeasurement filletLengthRegionOutlierMeasurement = 
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_OPEN_FILLET_LENGTH_REGION_OUTLIER);
    
    float filletLengthRegionOutlier = filletLengthRegionOutlierMeasurement.getValue();

    // Check if the "concavity ratio across" is within acceptable limits.
    final float MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER);
    
    final float MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER);
    
    if (MathUtil.fuzzyGreaterThan(filletLengthRegionOutlier, MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletLengthRegionOutlierMeasurement,
                                                      MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);

      // Create an Open indictment
      JointIndictment filletLengthRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'Fillet Length Region Outlier' measurement to this joint.
      filletLengthRegionOutlierOpenIndictment.addFailingMeasurement(filletLengthRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(filletLengthRegionOutlierOpenIndictment);
      
      return false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletLengthRegionOutlierMeasurement,
                                                      MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);
    }
    
    if(MathUtil.fuzzyLessThan(filletLengthRegionOutlier, MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT))
    {
       // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletLengthRegionOutlierMeasurement,
                                                      MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);

      // Create an Open indictment
      JointIndictment filletLengthRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'Fillet Length Region Outlier' measurement to this joint.
      filletLengthRegionOutlierOpenIndictment.addFailingMeasurement(filletLengthRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(filletLengthRegionOutlierOpenIndictment);
      
      return false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletLengthRegionOutlierMeasurement,
                                                      MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);
    }
    
    return true;
  }
  
  /**
   * @author Siew Yeng
   */
  private boolean detectOpensDueToCenterThicknessRegionOutlier(JointInspectionData jointInspectionData, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    
    Subtype subtype = jointInspectionData.getSubtype();
    
    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the "center thickness region outlier" measurement.
    JointMeasurement centerThicknessRegionOutlierMeasurement = 
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.QFN_OPEN_CENTER_THICKNESS_REGION_OUTLIER);

    float centerThicknessRegionOutlier = centerThicknessRegionOutlierMeasurement.getValue();
    
    // Check if the "concavity ratio across" is within acceptable limits.
    final float MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER);
     final float MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.QFN_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER);
    
    if (MathUtil.fuzzyGreaterThan(centerThicknessRegionOutlier, MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerThicknessRegionOutlierMeasurement,
                                                      MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);

      // Create an Open indictment
      JointIndictment centerThicknessRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'center thickness region outlier' measurement to this joint.
      centerThicknessRegionOutlierOpenIndictment.addFailingMeasurement(centerThicknessRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(centerThicknessRegionOutlierOpenIndictment);
      
      return false;
    }
    else
    {
       AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerThicknessRegionOutlierMeasurement,
                                                      MAXIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);
    }
    
    if (MathUtil.fuzzyLessThan(centerThicknessRegionOutlier, MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerThicknessRegionOutlierMeasurement,
                                                      MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);

      // Create an Open indictment
      JointIndictment centerThicknessRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'center thickness region outlier' measurement to this joint.
      centerThicknessRegionOutlierOpenIndictment.addFailingMeasurement(centerThicknessRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(centerThicknessRegionOutlierOpenIndictment);
      
      return false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerThicknessRegionOutlierMeasurement,
                                                      MINIMUM_ALLOWABLE_REGION_OUTLIER_PERCENT);
    }
    
    return true;
  }
}
