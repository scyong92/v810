package com.axi.v810.business.imageAnalysis;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * Unit test class for AlgorithmFeatureUtil.
 *
 * @author George Booth
 */
public class Test_AlgorithmFeatureUtil extends UnitTest
{
  private final float MILS_PER_MILLIMETER = 39.3700787F;

  private final float[] smoothedProfile = {
    0.012980352F,   // 0
    0.01458327F,    // 1
    0.016991012F,   // 2
    0.01982748F,    // 3
    0.023945333F,   // 4
    0.029436432F,   // 5
    0.03552277F,    // 6
    0.04151057F,    // 7
    0.04704282F,    // 8
    0.052503303F,   // 9
    0.05741254F,    // 10
    0.06156965F,    // 11
    0.06485975F,    // 12
    0.06670289F,    // 13
    0.06765603F,    // 14
    0.067676105F,   // 15
    0.06730575F,    // 16
    0.067145936F,   // 17
    0.06743589F,    // 18
    0.068683766F,   // 19
    0.06992876F,    // 20
    0.07116223F,    // 21
    0.07187804F,    // 22
    0.0723431F,     // 23
    0.072479956F,   // 24
    0.07245124F,    // 25
    0.07222731F,    // 26
    0.07197085F,    // 27
    0.0728178F,     // 28
    0.07556813F,    // 29
    0.080793194F,   // 30
    0.08795899F,    // 31
    0.09608371F,    // 32
    0.103628494F,   // 33
    0.10984977F,    // 34
    0.11475906F,    // 35
    0.118801296F,   // 36
    0.12151432F,    // 37
    0.122760296F,   // 38
    0.122507684F,   // 39
    0.12115166F,    // 40
    0.11861372F,    // 41
    0.11423843F,    // 42
    0.107693695F,   // 43
    0.10007427F,    // 44
    0.09218497F,    // 45
    0.08457128F,    // 46
    0.07636051F,    // 47
    0.06702432F,    // 48
    0.05600578F,    // 49
    0.04400058F,    // 50
    0.03243654F,    // 51
    0.023123287F,   // 52
    0.016283827F,   // 53
    0.012758347F    // 54
  };

  private float[] smoothedProfileWithVoidComp;
  private float[] smoothedFirstDerivativeProfile;
  private int maxThicknessIndex;
  private float maxThicknessValue;
  private float heelEdgeSubpixelBin;
  private float toeEdgeSubpixelBin;
  private float filletLength;

  /**
   * @author George Booth
   */
  private Test_AlgorithmFeatureUtil()
  {
    // Do nothing...
  }

  /**
   * @author George Booth
   */
  private void debug(String message)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ALGORITHM_FEATURE_UTIL_DEBUG))
      System.out.println(message);
  }

  /**
   * @author George Booth
   */
  private void testValue(String valueName, int expectedValue, int actualValue)
  {
    Expect.expect(actualValue == expectedValue, valueName + " = " + actualValue + ", expected " + expectedValue);
  }

  /**
   * @author George Booth
   */
  private void testValue(String valueName, float expectedValue, float actualValue)
  {
    Expect.expect(actualValue == expectedValue, valueName + " = " + actualValue + ", expected " + expectedValue);
  }

  /**
   * @author George Booth
   */
  public void testLocateHeelEdgeUsingThickness()
  {
    // create a sample profile that matches a known good QFN joint
    smoothedProfileWithVoidComp = smoothedProfile;
    debug("  profile length = " + smoothedProfileWithVoidComp.length);
    testValue("profile length", 55, smoothedProfileWithVoidComp.length);

    // max thickness determines heel and toe search regions
    maxThicknessIndex = ArrayUtil.maxIndex(smoothedProfileWithVoidComp);
    maxThicknessValue = smoothedProfileWithVoidComp[maxThicknessIndex];
    debug("  maxThicknessIndex = " + maxThicknessIndex +
          " (" + smoothedProfileWithVoidComp[(int)maxThicknessIndex] * MILS_PER_MILLIMETER + " mils" + ")");
    testValue("maxThicknessIndex", 38, maxThicknessIndex);
    testValue("maxThicknessValue", 0.122760296F, maxThicknessValue);

    // Locate the heel edge (max profile thickness determined in locateHeelEdge())
    final float HEEL_EDGE_THICKNESS_FRACTION_OF_MAX = 0.25F;
    heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(smoothedProfileWithVoidComp,
                                                                                  HEEL_EDGE_THICKNESS_FRACTION_OF_MAX, false);
    debug("  heelEdgeSubpixelBin = " + heelEdgeSubpixelBin +
          " (" + smoothedProfileWithVoidComp[(int)heelEdgeSubpixelBin] * MILS_PER_MILLIMETER + " mils" + ")");
    testValue("heelEdgeSubpixelBin", 5.7940235F, heelEdgeSubpixelBin);
  }

  /**
   * @author George Booth
   */
  public void testLocateToeEdgeUsingThickness()
  {
    // Locate the toe edge
    final float TOE_EDGE_THICKNESS_FRACTION_OF_MAX = 0.25F;
    float targetToeEdgeThickness = maxThicknessValue * TOE_EDGE_THICKNESS_FRACTION_OF_MAX;
    toeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(smoothedProfileWithVoidComp,
                                             targetToeEdgeThickness, maxThicknessIndex);
    debug("  toeEdgeSubpixelBin = " + toeEdgeSubpixelBin +
          " (" + smoothedProfileWithVoidComp[(int)toeEdgeSubpixelBin] * MILS_PER_MILLIMETER + " mils" + ")");
    testValue("toeEdgeSubpixelBin", 50.812477F, toeEdgeSubpixelBin);
  }

  /**
   * @author George Booth
   */
  public void testFindMaximumHeelSlopeBin()
  {
    // create a smoothed derivative profile from the working profile
    smoothedFirstDerivativeProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(smoothedProfileWithVoidComp,
                                                    2,
                                                    MagnificationEnum.getCurrentNorminal(),
                                                    MeasurementUnitsEnum.MILLIMETERS);

    smoothedFirstDerivativeProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeProfile, 3);

    // find heel derivative index
    float heelDerivativeIndex = AlgorithmFeatureUtil.findMaximumHeelSlopeBin(smoothedFirstDerivativeProfile,
                                                                           maxThicknessIndex,
                                                                           (int)heelEdgeSubpixelBin,
                                                                           (int)toeEdgeSubpixelBin);
    debug("  heelDerivativeIndex = " + heelDerivativeIndex +
          " (" + smoothedFirstDerivativeProfile[Math.round(heelDerivativeIndex)] + ")");
    testValue("heelDerivativeIndex", 6.2657175f, heelDerivativeIndex);
  }

  /**
   * @author George Booth
   */
  public void testFindMinimumToeSlopeBin()
  {
    // find toe negative derivative index
    float toeNegativeDerivativeIndex = AlgorithmFeatureUtil.findMinimumToeSlopeBin(smoothedFirstDerivativeProfile,
                                                                                 maxThicknessIndex,
                                                                                 (int)heelEdgeSubpixelBin,
                                                                                 (int)toeEdgeSubpixelBin);
    debug("  toeNegativeDerivativeIndex = " + toeNegativeDerivativeIndex +
          " (" + smoothedFirstDerivativeProfile[Math.round(toeNegativeDerivativeIndex)] + ")");
    testValue("toeNegativeDerivativeIndex", 49.570644f, toeNegativeDerivativeIndex);
  }

  /**
   * @author George Booth
   */
  public void testLocateHeelPeak()
  {
    // Locate the heel peak
    final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG = 0.20F;
    filletLength = toeEdgeSubpixelBin - heelEdgeSubpixelBin;
    debug("  filletLength = " + filletLength + " pixels");
    testValue("filletLength", 45.018456F, filletLength);

    int heelPeakSearchDistanceInPixels = (int)Math.ceil(filletLength * HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
    debug("  heelPeakSearchDistanceInPixels = " + heelPeakSearchDistanceInPixels + " pixels");
    testValue("heelPeakSearchDistanceInPixels", 10, heelPeakSearchDistanceInPixels);

    int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(smoothedProfileWithVoidComp,
                                                          (int)heelEdgeSubpixelBin,
                                                          heelPeakSearchDistanceInPixels);
    debug("  heelPeakBin = " + heelPeakBin +
                      " (" + smoothedProfileWithVoidComp[heelPeakBin] * MILS_PER_MILLIMETER + " mils" + ")");
    testValue("heelPeakBin", 15, heelPeakBin);
  }

  /**
   * @author George Booth
   */
  public void testLocateToePeak()
  {
    // Locate the toe
    final float TOE_DISTANCE_FRACTION = 0.80F;
    int toeDistanceInPixels = (int)(filletLength * TOE_DISTANCE_FRACTION);
    debug("  toeDistanceInPixels = " + toeDistanceInPixels + " pixels");
    testValue("toeDistanceInPixels", 36, toeDistanceInPixels);

    int toePeakBin = AlgorithmFeatureUtil.locateToePeak(smoothedProfileWithVoidComp, (int)heelEdgeSubpixelBin, toeDistanceInPixels);
    debug("  toePeakBin = " + toePeakBin +
                      " (" + smoothedProfileWithVoidComp[toePeakBin] * MILS_PER_MILLIMETER + " mils" + ")");
    testValue("toePeakBin", 41, toePeakBin);
  }

  /**
   * Main unit test method.
   *
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      Config.getInstance().loadIfNecessary();
      testLocateHeelEdgeUsingThickness();
      testLocateToeEdgeUsingThickness();
      testFindMaximumHeelSlopeBin();
      testFindMinimumToeSlopeBin();
      testLocateHeelPeak();
      testLocateToePeak();
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George Booth
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmFeatureUtil());
  }
}
