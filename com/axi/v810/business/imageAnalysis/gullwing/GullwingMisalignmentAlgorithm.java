package com.axi.v810.business.imageAnalysis.gullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;

/**
 * Gullwing Misalignment algorithm.
 *
 * @author Matt Wharton
 */
public class GullwingMisalignmentAlgorithm extends Algorithm
{
  /**
   * @author Matt Wharton
   */
  public GullwingMisalignmentAlgorithm(InspectionFamily gullwingInspectionFamily)
  {
    super(AlgorithmEnum.MISALIGNMENT, InspectionFamilyEnum.GULLWING);

    Assert.expect(gullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(gullwingInspectionFamily);

    // Add the measurement enums.
    addMeasurementEnums();
  }

  /**
   * @author Matt Wharton
   */
  protected GullwingMisalignmentAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.MISALIGNMENT, inspectionFamilyEnum);
    Assert.expect(inspectionFamilyEnum != null);
  }

  /**
   * @author Matt Wharton
   */
  private void addAlgorithmSettings(InspectionFamily gullwingInspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    // Maximum joint offset from CAD.
    AlgorithmSetting maximumJointOffsetFromCadSetting =
        new AlgorithmSetting(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD,
                             displayOrder++,
                             MathUtil.convertMilsToMillimeters(20.f),
                             MathUtil.convertMilsToMillimeters(0.f),
                             MathUtil.convertMilsToMillimeters(50.f),
                             MeasurementUnitsEnum.MILLIMETERS,
                             "HTML_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_JOINT_OFFSET_FROM_CAD)_KEY", // desc
                             "HTML_DETAILED_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_JOINT_OFFSET_FROM_CAD)_KEY", // detailed desc
                             "IMG_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_JOINT_OFFSET_FROM_CAD)_KEY", // image file
                             AlgorithmSettingTypeEnum.STANDARD,
                             currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  maximumJointOffsetFromCadSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  maximumJointOffsetFromCadSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  maximumJointOffsetFromCadSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);
    addAlgorithmSetting(maximumJointOffsetFromCadSetting);

    // Maximum heel position distance from nominal.
    // PE:  This is now considered obsolete and replaced by the Max Heel Offset From Region Neighbors threshold
    AlgorithmSetting maximumHeelPositionDistanceFromNominalSetting =
        new AlgorithmSetting(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL,
                             displayOrder++,
                             20.f,
                             0.f,
                             200.f,
                             MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
                             "HTML_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL)_KEY", // desc
                             "HTML_DETAILED_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL)_KEY", // detailed desc
                             "IMG_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL)_KEY", // image file
                             AlgorithmSettingTypeEnum.HIDDEN,
                             currentVersion);
   gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                 maximumHeelPositionDistanceFromNominalSetting,
                                                                 SliceNameEnum.PAD,
                                                                 MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
   gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                 maximumHeelPositionDistanceFromNominalSetting,
                                                                 SliceNameEnum.PAD,
                                                                 MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
   gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                 maximumHeelPositionDistanceFromNominalSetting,
                                                                 SliceNameEnum.PAD,
                                                                 MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumHeelPositionDistanceFromNominalSetting);

    // Maximum heel position offset from region neighbors.
    AlgorithmSetting maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting =
        new AlgorithmSetting(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS,
                             displayOrder++,
                             MathUtil.convertMilsToMillimeters(10.0f),
                             0.f,
                             MathUtil.convertMilsToMillimeters(200.0f),
                             MeasurementUnitsEnum.MILLIMETERS,
                             "HTML_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS)_KEY", // desc
                             "HTML_DETAILED_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS)_KEY", // detailed desc
                             "IMG_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS)_KEY", // image file
                             AlgorithmSettingTypeEnum.STANDARD,
                             currentVersion);
   gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                 maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting,
                                                                 SliceNameEnum.PAD,
                                                                 MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
   gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                 maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting,
                                                                 SliceNameEnum.PAD,
                                                                 MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
   gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                 maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting,
                                                                 SliceNameEnum.PAD,
                                                                 MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
    addAlgorithmSetting(maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting);

  }

  /**
   * @author Matt Wharton
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
  }

  /**
   * Runs 'Misalignment' on all the given joints.
   *
   * @author Matt Wharton
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

      BooleanRef jointPassed = new BooleanRef(true);

      // Run the basic Gullwing Open classification.
      basicGullwingClassifyJoint(subtype,
                                 padSlice,
                                 jointInspectionData,
                                 jointInspectionDataObjects,
                                 jointPassed,
                                 MILIMETER_PER_PIXEL);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }

  /**
   * Does the 'basic' Gullwing Misalignment classification on the specified joint.

   * @author Matt Wharton
   */
  protected void basicGullwingClassifyJoint(Subtype subtype,
                                          SliceNameEnum sliceNameEnum,
                                          JointInspectionData jointUnderTestJointInspectionData,
                                          Collection<JointInspectionData> allJointsInRegionJointInspectionDataObjects,
                                          BooleanRef jointPassed,
                                          final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointUnderTestJointInspectionData != null);
    Assert.expect(allJointsInRegionJointInspectionDataObjects != null);
    Assert.expect(jointPassed != null);

    // Check for Misalignment based on offset from CAD position.
    detectMisalignedJointUsingJointOffsetFromCadPosition(subtype, sliceNameEnum, jointUnderTestJointInspectionData, jointPassed);

    // Check for misalignment based on heel offset from region neighbors
    detectMisalignedJointUsingFilletPeakOffsetFromRegionNeighbors(subtype, sliceNameEnum, jointUnderTestJointInspectionData, allJointsInRegionJointInspectionDataObjects, jointPassed, MILIMETER_PER_PIXEL);
  }

  /**
   * Detects misaligned joints by comparing cad position to located position.
   *
   * @author Matt Wharton
   */
  private void detectMisalignedJointUsingJointOffsetFromCadPosition(Subtype subtype,
                                                                    SliceNameEnum sliceNameEnum,
                                                                    JointInspectionData jointInspectionData,
                                                                    BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    final float MAXIMUM_JOINT_OFFSET_FROM_CAD_IN_MILLIS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD);
    JointMeasurement jointOffsetFromCadMeasurement =
        GullwingMeasurementAlgorithm.getJointOffsetFromCadMeasurement(jointInspectionData, sliceNameEnum);
    float jointOffsetFromCadInMillis = jointOffsetFromCadMeasurement.getValue();

    if (MathUtil.fuzzyGreaterThan(jointOffsetFromCadInMillis, MAXIMUM_JOINT_OFFSET_FROM_CAD_IN_MILLIS))
    {
      // Indict the joint for 'Misalignment'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      jointOffsetFromCadMeasurement,
                                                      MAXIMUM_JOINT_OFFSET_FROM_CAD_IN_MILLIS);

      // Create an indictment.
      JointIndictment misalignmentIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);

      // Add the failing measurement.
      misalignmentIndictment.addFailingMeasurement(jointOffsetFromCadMeasurement);

      // Tie the indictment to the joint's results.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addIndictment(misalignmentIndictment);

      // Flag the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      jointOffsetFromCadMeasurement,
                                                      MAXIMUM_JOINT_OFFSET_FROM_CAD_IN_MILLIS);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private float getFilletPeakPosition(JointInspectionData jointInspectionData,
                                      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // get the heel position of this joint
    JointMeasurement filletPeakPositionMeasurement = GullwingMeasurementAlgorithm.getFilletPeakPositionMeasurement(jointInspectionData, sliceNameEnum);
    float filletPeakPosition = filletPeakPositionMeasurement.getValue();

    return filletPeakPosition;
  }

  /**
   * @author Peter Esbensen
   */
  private float getExpectedLocationOfFilletPeakBasedOnOtherJointsInRegion(JointInspectionData jointUnderTestJointInspectionData,
                                                                          Collection<JointInspectionData> allJointsInRegionJointInspectionDataObjects,
                                                                          SliceNameEnum sliceNameEnum)
  {
    Assert.expect(allJointsInRegionJointInspectionDataObjects != null);
    Assert.expect(jointUnderTestJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // if there's only one joint in the region, then the expected location is simply that joint's position
    if (allJointsInRegionJointInspectionDataObjects.size() == 1)
    {
      return getFilletPeakPosition( jointUnderTestJointInspectionData, sliceNameEnum );
    }

    // Get the expected location
    List<Float> filletPeakPositions = new ArrayList<Float>();
    for (JointInspectionData jointInspectionData : allJointsInRegionJointInspectionDataObjects)
    {
      // if this is the joint under test, skip it
      if (jointUnderTestJointInspectionData == jointInspectionData)
        continue;

      // get the position of this joint
      float filletPeakPosition = getFilletPeakPosition( jointInspectionData, sliceNameEnum );

      // add it to our array of fillet peak positions
      filletPeakPositions.add(filletPeakPosition);
    }
    // use the median as our expected fillet peak location
    float medianFilletPeakPosition = StatisticsUtil.median( ArrayUtil.convertFloatListToFloatArray(filletPeakPositions) );

    return medianFilletPeakPosition;
  }

  /**
   * Detects misaligned joints by comparing the fillet peak position against the fillet peak position of the other joints in the same region.
   *
   * @author Peter Esbensen
   */
  private void detectMisalignedJointUsingFilletPeakOffsetFromRegionNeighbors(Subtype subtype,
                                                                             SliceNameEnum sliceNameEnum,
                                                                             JointInspectionData jointUnderTestJointInspectionData,
                                                                             Collection<JointInspectionData> allJointsInRegionJointInspectionDataObjects,
                                                                             BooleanRef jointPassed,
                                                                             final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointUnderTestJointInspectionData != null);
    Assert.expect(allJointsInRegionJointInspectionDataObjects != null);
    Assert.expect(jointPassed != null);

    // get the expected location
    float expectedLocationAsImageCoordinate = getExpectedLocationOfFilletPeakBasedOnOtherJointsInRegion(jointUnderTestJointInspectionData,
                                                                                                        allJointsInRegionJointInspectionDataObjects,
                                                                                                        sliceNameEnum);

    // Calculate the measured heel position distance from expected.
    JointMeasurement filletPeakPositionMeasurement =
        GullwingMeasurementAlgorithm.getFilletPeakPositionMeasurement(jointUnderTestJointInspectionData, sliceNameEnum);
    float filletPeakPosition = filletPeakPositionMeasurement.getValue();
    float filletPeakPositionDistanceFromExpectedInPixels = Math.abs(filletPeakPosition - expectedLocationAsImageCoordinate);
    float filletPeakPositionDistanceFromExpectedInMM = MILIMETER_PER_PIXEL * filletPeakPositionDistanceFromExpectedInPixels;

    // Create the fillet peak position offset-from-region-neighbors measurement.
    JointMeasurement filletPeakOffsetFromRegionNeighborsMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS,
                             MeasurementUnitsEnum.MILLIMETERS,
                             jointUnderTestJointInspectionData.getPad(),
                             sliceNameEnum,
                             filletPeakPositionDistanceFromExpectedInMM);
    JointInspectionResult jointInspectionResult = jointUnderTestJointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(filletPeakOffsetFromRegionNeighborsMeasurement);

    // Check to see if the fillet peak position region outlier is within acceptable limits.
    final float GULLWING_MISALIGNMENT_MAXIMUM_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS);
    if (filletPeakPositionDistanceFromExpectedInMM > GULLWING_MISALIGNMENT_MAXIMUM_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS)
    {
      // Indict the joint for 'Misalignment'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointUnderTestJointInspectionData,
                                                      jointUnderTestJointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletPeakOffsetFromRegionNeighborsMeasurement,
                                                      GULLWING_MISALIGNMENT_MAXIMUM_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);

      // Create an indictment.
      JointIndictment misalignmentIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);

      // Add the failing measurement.
      misalignmentIndictment.addFailingMeasurement(filletPeakOffsetFromRegionNeighborsMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(misalignmentIndictment);

      // Flag the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointUnderTestJointInspectionData,
                                                      sliceNameEnum,
                                                      jointUnderTestJointInspectionData.getInspectionRegion(),
                                                      filletPeakOffsetFromRegionNeighborsMeasurement,
                                                      GULLWING_MISALIGNMENT_MAXIMUM_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
    }
  }


  /**
   * Main entry point for algorithm setting learning.
   *
   * @author Matt Wharton
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages)
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Learn the basic Gullwing Misalignment settings.
    learnBasicGullwingSettings(subtype, padSlice);
  }

  /**
   * @author Matt Wharton
   */
  protected void learnBasicGullwingSettings(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // For Leadless joints, we want to open up the Misalignment defect thresholds.
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.LEADLESS))
    {
      float maxHeelPositionDistanceFromNominal =
          (Float)subtype.getAlgorithmSettingMaximumValue(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL, maxHeelPositionDistanceFromNominal);

      float maxJointOffsetFromCadPosition =
          (Float)subtype.getAlgorithmSettingMaximumValue(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD, maxJointOffsetFromCadPosition);
    }
  }

  /**
   * Reset the learned settings back to their defaults.
   *
   * @author Matt Wharton
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD);
  }
}
