package com.axi.v810.business.imageAnalysis.gullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Gullwing measurement algorithm.
 *
 * @author Matt Wharton
 */
public class GullwingMeasurementAlgorithm extends Algorithm
{
  protected final int _MAX_NUMBER_OF_REGIONS_FOR_LEARNING = 100;
  
  public final static String VOID_COMPENSATION_METHOD_NONE = "None";
  public final static String VOID_COMPENSATION_METHOD_GAUSSIAN = "Gaussian";
  protected final static ArrayList<String> VOID_COMPENSATION_METHODS = new ArrayList<String>(Arrays.asList(VOID_COMPENSATION_METHOD_NONE,VOID_COMPENSATION_METHOD_GAUSSIAN));
  
  //protected static final String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";
  
  /**
   * Keeps track of some measured values that aren't reported as measurements, but are needed by more
   * advanced versions of the Gullwing Measurement algorithm.
   *
   * @author Matt Wharton
   */
  protected static class GullwingUnreportedMeasurementValues
  {
    private RegionOfInterest _fullWidthPadProfileRoi;
    private float[] _padThicknessProfileUsingFullPadLengthAcross;
    private RegionOfInterest _fractionalWidthPadProfileRoi;
    private float[] _padThicknessProfileUsingFractionalPadLengthAcross;
    private float _heelEdgeSubpixelBin;
    private float _toeEdgeSubpixelBin;
    private int _heelPeakSearchDistanceInPixels;
    private int _heelPeakBin;
    private int _centerBin;
    private int _toePeakBin;

    /**
     * @author Matt Wharton
     */
    public GullwingUnreportedMeasurementValues(RegionOfInterest fullWidthPadProfileRoi,
                                               float[] padThicknessProfileUsingFullPadLengthAcross,
                                               RegionOfInterest fractionalWidthPadProfileRoi,
                                               float[] padThicknessProfileUsingFractionalPadLengthAcross,
                                               float heelEdgeSubpixelBin,
                                               float toeEdgeSubpixelBin,
                                               int heelPeakSearchDistanceInPixels,
                                               int heelPeakBin,
                                               int centerBin,
                                               int toePeakBin)
    {
      Assert.expect(fullWidthPadProfileRoi != null);
      Assert.expect(padThicknessProfileUsingFullPadLengthAcross != null);
      Assert.expect(fractionalWidthPadProfileRoi != null);
      Assert.expect(padThicknessProfileUsingFractionalPadLengthAcross != null);
      Assert.expect(padThicknessProfileUsingFullPadLengthAcross.length ==
                    padThicknessProfileUsingFractionalPadLengthAcross.length);

      _fullWidthPadProfileRoi = fullWidthPadProfileRoi;
      _padThicknessProfileUsingFullPadLengthAcross = padThicknessProfileUsingFullPadLengthAcross;
      _fractionalWidthPadProfileRoi = fractionalWidthPadProfileRoi;
      _padThicknessProfileUsingFractionalPadLengthAcross = padThicknessProfileUsingFractionalPadLengthAcross;
      _heelEdgeSubpixelBin = heelEdgeSubpixelBin;
      _toeEdgeSubpixelBin = toeEdgeSubpixelBin;
      _heelPeakSearchDistanceInPixels = heelPeakSearchDistanceInPixels;
      _heelPeakBin = heelPeakBin;
      _centerBin = centerBin;
      _toePeakBin = toePeakBin;
    }

    /**
     * @author Matt Wharton
     */
    public RegionOfInterest getFullWidthPadProfileRoi()
    {
      Assert.expect(_fullWidthPadProfileRoi != null);

      return _fullWidthPadProfileRoi;
    }

    /**
     * @author Matt Wharton
     */
    public RegionOfInterest getFractionalWidthPadProfileRoi()
    {
      Assert.expect(_fractionalWidthPadProfileRoi != null);

      return _fractionalWidthPadProfileRoi;
    }

    /**
     * @author Matt Wharton
     */
    public float[] getPadThicknessProfileUsingFullPadLengthAcross()
    {
      Assert.expect(_padThicknessProfileUsingFullPadLengthAcross != null);

      return _padThicknessProfileUsingFullPadLengthAcross;
    }

    /**
     * @author Matt Wharton
     */
    public float[] getPadThicknessProfileUsingFractionalPadLengthAcross()
    {
      Assert.expect(_padThicknessProfileUsingFractionalPadLengthAcross != null);

      return _padThicknessProfileUsingFractionalPadLengthAcross;
    }

    /**
     * @author Matt Wharton
     */
    public float getHeelEdgeSubpixelBin()
    {
      return _heelEdgeSubpixelBin;
    }

    /**
     * @author Matt Wharton
     */
    public float getToeEdgeSubpixelBin()
    {
      return _toeEdgeSubpixelBin;
    }

    /**
     * @author Matt Wharton
     */
    public int getHeelPeakSearchDistanceInPixels()
    {
      return _heelPeakSearchDistanceInPixels;
    }

    /**
     * @author Matt Wharton
     */
    public int getHeelPeakBin()
    {
      return _heelPeakBin;
    }

    /**
     * @author Matt Wharton
     */
    public int getCenterBin()
    {
      return _centerBin;
    }

    /**
     * @author Matt Wharton
     */
    public int getToePeakBin()
    {
      return _toePeakBin;
    }
  }

  /**
   * Keeps track of profile data that we measure during learning.
   *
   * @author Matt Wharton
   */
  protected static class GullwingProfileDataForLearning
  {
    private RegionOfInterest _fullWidthPadProfileRoi;
    private float[] _padThicknessProfileUsingFullPadLengthAcross;
    private float[] _derivativePadThicknessProfileUsingFullPadLengthAcross;
    private RegionOfInterest _fractionalWidthPadProfileRoi;
    private float[] _padThicknessProfileUsingFractionalPadLengthAcross;
    private float[] _derivativePadThicknessProfileUsingFractionalPadLengthAcross;

    /**
     * @author Matt Wharton
     */
    public GullwingProfileDataForLearning(RegionOfInterest fullWidthPadProfileRoi,
                                          float[] padThicknessProfileUsingFullPadLengthAcross,
                                          float[] derivativePadThicknessProfileUsingFullPadLengthAcross,
                                          RegionOfInterest fractionalWidthPadProfileRoi,
                                          float[] padThicknessProfileUsingFractionalPadLengthAcross,
                                          float[] derivativePadThicknessProfileUsingFractionalPadLengthAcross)
    {
      Assert.expect(fullWidthPadProfileRoi != null);
      Assert.expect(padThicknessProfileUsingFullPadLengthAcross != null);
      Assert.expect(derivativePadThicknessProfileUsingFullPadLengthAcross != null);
      Assert.expect(fractionalWidthPadProfileRoi != null);
      Assert.expect(padThicknessProfileUsingFractionalPadLengthAcross != null);
      Assert.expect(derivativePadThicknessProfileUsingFractionalPadLengthAcross != null);
      Assert.expect(padThicknessProfileUsingFullPadLengthAcross.length ==
                    padThicknessProfileUsingFractionalPadLengthAcross.length);
      Assert.expect(padThicknessProfileUsingFullPadLengthAcross.length ==
                    derivativePadThicknessProfileUsingFullPadLengthAcross.length);

      _fullWidthPadProfileRoi = fullWidthPadProfileRoi;
      _padThicknessProfileUsingFullPadLengthAcross = padThicknessProfileUsingFullPadLengthAcross;
      _derivativePadThicknessProfileUsingFullPadLengthAcross = derivativePadThicknessProfileUsingFullPadLengthAcross;
      _fractionalWidthPadProfileRoi = fractionalWidthPadProfileRoi;
      _padThicknessProfileUsingFractionalPadLengthAcross = padThicknessProfileUsingFractionalPadLengthAcross;
      _derivativePadThicknessProfileUsingFractionalPadLengthAcross = derivativePadThicknessProfileUsingFractionalPadLengthAcross;
    }

    /**
     * @author Matt Wharton
     */
    public RegionOfInterest getFullWidthPadProfileRoi()
    {
      Assert.expect(_fullWidthPadProfileRoi != null);

      return _fullWidthPadProfileRoi;
    }

    /**
     * @author Matt Wharton
     */
    public RegionOfInterest getFractionalWidthPadProfileRoi()
    {
      Assert.expect(_fractionalWidthPadProfileRoi != null);

      return _fractionalWidthPadProfileRoi;
    }

    /**
     * @author Matt Wharton
     */
    public float[] getPadThicknessProfileUsingFullPadLengthAcross()
    {
      Assert.expect(_padThicknessProfileUsingFullPadLengthAcross != null);

      return _padThicknessProfileUsingFullPadLengthAcross;
    }

    /**
     * @author Matt Wharton
     */
    public float[] getPadThicknessProfileUsingFractionalPadLengthAcross()
    {
      Assert.expect(_padThicknessProfileUsingFractionalPadLengthAcross != null);

      return _padThicknessProfileUsingFractionalPadLengthAcross;
    }

    /**
     * @author Matt Wharton
     */
    public float[] getDerivativePadThicknessProfileUsingFullPadLengthAcross()
    {
      Assert.expect(_derivativePadThicknessProfileUsingFullPadLengthAcross != null);

      return _derivativePadThicknessProfileUsingFullPadLengthAcross;
    }

    /**
     * @author Matt Wharton
     */
    public float[] getDerivativePadThicknessProfileUsingFractionalPadLengthAcross()
    {
      Assert.expect(_derivativePadThicknessProfileUsingFractionalPadLengthAcross != null);

      return _derivativePadThicknessProfileUsingFractionalPadLengthAcross;
    }
  }

  // AlgorithmDiagnostics instance.
  private static AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();

  // This map keeps track of which joint types require profile flipping.
  protected static final Map<JointTypeEnum, Boolean> _jointTypeToProfileReversalRequiredMap =
      new HashMap<JointTypeEnum,Boolean>();

  protected static final int _SMOOTHING_KERNEL_LENGTH = 3;

  protected static final int _UNUSED_BIN = -1;

  private static final boolean _logAlgorithmProfileData =
      Config.getInstance().getBooleanValue(SoftwareConfigEnum.LOG_ALGORITHM_PROFILE_DATA);

  /**
   * @author Matt Wharton
   */
  static
  {
    initializeJointTypeToProfileReversalRequiredMap();
  }

  /**
   * @author Matt Wharton
   */
  private static void initializeJointTypeToProfileReversalRequiredMap()
  {
    _jointTypeToProfileReversalRequiredMap.put(JointTypeEnum.GULLWING, false);
    _jointTypeToProfileReversalRequiredMap.put(JointTypeEnum.JLEAD, true);
    _jointTypeToProfileReversalRequiredMap.put(JointTypeEnum.LEADLESS, true);
// uses a dedicated family now
//    _jointTypeToProfileReversalRequiredMap.put(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, false);
    _jointTypeToProfileReversalRequiredMap.put(JointTypeEnum.SMALL_OUTLINE_LEADLESS, false);
    _jointTypeToProfileReversalRequiredMap.put(JointTypeEnum.SURFACE_MOUNT_CONNECTOR, false);
    _jointTypeToProfileReversalRequiredMap.put(JointTypeEnum.RF_SHIELD, false);
  }

  /**
   * @author Matt Wharton
   */
  public GullwingMeasurementAlgorithm(InspectionFamily gullwingInspectionFamily)
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.GULLWING);

    Assert.expect(gullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(gullwingInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @author Matt Wharton
   */
  protected GullwingMeasurementAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.MEASUREMENT, inspectionFamilyEnum);
  }

  /**
   * @author Peter Esbensen
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_HEEL_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_TOE_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_LENGTH);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_CENTER_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_PEAK_POSITION);
    //Lim, Lay Ngor - XCR1766 - Add pin length measurement
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_JOINT_PIN_LENGTH);
    
    //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());
  }

  /**
   * Adds the algorithm settings for Gullwing Measurement.
   *
   * @author Matt Wharton
   */
  private void addAlgorithmSettings(InspectionFamily gullwingInspectionFamily)
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    // Indicates whether heel void compensation is enabled or not.
    ArrayList<String> allowableHeelVoidCompensationValues = new ArrayList<String>(Arrays.asList("On", "Off"));
    AlgorithmSetting heelVoidCompensationSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION,
      displayOrder++,
      "Off",
      allowableHeelVoidCompensationValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GULLWING_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // detailed desc
      "IMG_DESC_GULLWING_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(heelVoidCompensationSetting);
    
    // Indicates method to perform profile smoothing
    AlgorithmSetting voidCompensationMethodSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD,
      displayOrder++,
      VOID_COMPENSATION_METHOD_NONE,
      VOID_COMPENSATION_METHODS,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GULLWING_MEASUREMENT_(VOID_COMPENSATION_METHOD)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(VOID_COMPENSATION_METHOD)_KEY", // detailed desc
      "IMG_DESC_GULLWING_MEASUREMENT_(VOID_COMPENSATION_METHOD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(voidCompensationMethodSetting);
    
    // Size of window kernel of Gaussian smoothing
    AlgorithmSetting smoothSensitivitySetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY,
        displayOrder++,
        1, // default
        1, // min
        20, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GULLWING_MEASUREMENT_(SMOOTH_SENSITIVITY)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(SMOOTH_SENSITIVITY)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(SMOOTH_SENSITIVITY)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(smoothSensitivitySetting);

    // Percent of pad length across to use for the primary pad thickness profile.
    AlgorithmSetting padProfileWidthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH,
        displayOrder++,
        45.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS,
        "HTML_DESC_GULLWING_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(padProfileWidthSetting);

    // Percent of the pad length along to use for the primary pad thickness profile.
    AlgorithmSetting padProfileLengthSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH,
      displayOrder++,
      100.f, // default
      0.0f, // min
      200.f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
      "HTML_DESC_GULLWING_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // detailed desc
      "IMG_DESC_GULLWING_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(padProfileLengthSetting);

    // Background region location (as percent of IPD).
    AlgorithmSetting backgroundRegionLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION,
      displayOrder++,
      50.f, // default
      0.0f, // min
      200.f, // max
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // detailed desc
      "IMG_DESC_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionLocation);
    
    // Background region location offset (as absolute offset distance from edge of pad).
    AlgorithmSetting backgroundRegionLocationOffset = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET,
      displayOrder++,
      0.0f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.MILS,
      "HTML_DESC_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // detailed desc
      "IMG_DESC_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionLocationOffset);

    // Target heel edge thickess as a percent of max pad profile thickness.
    AlgorithmSetting heelEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT,
        displayOrder++,
        60.f, // default
        0.0f, // min
        100f, // max
        MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG,
        "HTML_DESC_GULLWING_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(heelEdgeSearchThicknessPercentSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT);

    // Heel peak search distance from heel edge.
    AlgorithmSetting heelSearchDistanceFromEdgeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE,
        displayOrder++,
        15.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_GULLWING_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(heelSearchDistanceFromEdgeSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE);

    // Target toe edge thickness as a percent of nominal toe thickness.
    AlgorithmSetting toeEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT,
        displayOrder++,
        50.f, // default
        0.0f, // min
        300.f, // max
        MeasurementUnitsEnum.PERCENT_OF_NOMINAL_TOE_THICKNESS,
        "HTML_DESC_GULLWING_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(toeEdgeSearchThicknessPercentSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT);

    // Pin length (distance from heel peak to toe peak).
    AlgorithmSetting pinLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(32.f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(500.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GULLWING_MEASUREMENT_(MINIMUM_PIN_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(MINIMUM_PIN_LENGTH)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(MINIMUM_PIN_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(pinLengthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH);

    // Center offset from heel peak (percent of pin length).
    AlgorithmSetting centerOffsetFromHeelSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET,
        displayOrder++,
        60.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PIN_LENGTH,
        "HTML_DESC_GULLWING_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(centerOffsetFromHeelSetting);

    // Nominal heel position.
    AlgorithmSetting nominalHeelPosition = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION,
      displayOrder++,
      40.f,  // default
      -50.f, // min
      150.f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
      "HTML_DESC_GULLWING_MEASUREMENT_(NOMINAL_HEEL_POSITION)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(NOMINAL_HEEL_POSITION)_KEY", // detailed desc
      "IMG_DESC_GULLWING_MEASUREMENT_(NOMINAL_HEEL_POSITION)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  nominalHeelPosition,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_POSITION);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  nominalHeelPosition,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_POSITION);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  nominalHeelPosition,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_POSITION);
    addAlgorithmSetting(nominalHeelPosition);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION);

    // Nominal heel thickness.
    AlgorithmSetting nominalHeelThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(7.f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GULLWING_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // desc
      "IMG_DESC_GULLWING_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  nominalHeelThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  nominalHeelThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  nominalHeelThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS);
    addAlgorithmSetting(nominalHeelThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);

    // Nominal toe thickness.
    AlgorithmSetting nominalToeThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(4.f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GULLWING_MEASUREMENT_(NOMINAL_TOE_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(NOMINAL_TOE_THICKNESS)_KEY", // desc
      "IMG_DESC_GULLWING_MEASUREMENT_(NOMINAL_TOE_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  nominalToeThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_TOE_THICKNESS);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  nominalToeThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_TOE_THICKNESS);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  nominalToeThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_TOE_THICKNESS);
    addAlgorithmSetting(nominalToeThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS);

    // Nominal fillet length.
    AlgorithmSetting nominalFilletLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(38.f), // default
        MathUtil.convertMilsToMillimeters(0.1f), // min
        MathUtil.convertMilsToMillimeters(500.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GULLWING_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  nominalFilletLengthSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  nominalFilletLengthSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  nominalFilletLengthSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH);
    addAlgorithmSetting(nominalFilletLengthSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH);

    // Nominal average fillet thickness.
    AlgorithmSetting nominalFilletThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4.0f), // default
        MathUtil.convertMilsToMillimeters(0.1f), // min
        MathUtil.convertMilsToMillimeters(500.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GULLWING_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_GULLWING_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  nominalFilletThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  nominalFilletThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  nominalFilletThicknessSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS);
    addAlgorithmSetting(nominalFilletThicknessSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_GULLWING_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_GULLWING_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_GULLWING_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);

    // Added by Lee Herng (4 Mar 2011)
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed description URL Key
      "IMG_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_GULLWING_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is displayed right before psh in the Slice Setup Tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab - CR31430
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GULLWING_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GULLWING_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_GULLWING_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));

    // Settings to define background regions for Ventura (default to BOTH for Gullwing)

     // Enable Background Regions
     ArrayList<String> enableBackgroundRegionsValues = new ArrayList<String>(Arrays.asList("Both", "Right", "Left"));
     AlgorithmSetting enableBackgroundRegionsSetting = new AlgorithmSetting(
       AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS,
       displayOrder++,
       "Both",
       enableBackgroundRegionsValues,
       MeasurementUnitsEnum.NONE,
       "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(ENABLE_BACKGROUND_REGIONS)_KEY",
       "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(ENABLE_BACKGROUND_REGIONS)_KEY",
       "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(ENABLE_BACKGROUND_REGIONS)_KEY",
       AlgorithmSettingTypeEnum.HIDDEN,
       currentVersion);
     addAlgorithmSetting(enableBackgroundRegionsSetting);

     // Background Region Width
     AlgorithmSetting backgroundRegionWidthSetting = new AlgorithmSetting(
       AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_WIDTH,
       displayOrder++,
       5.0f, // default value
       5.0f, // minimum value
       100.0f, // maximum value
       MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
       "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_WIDTH)_KEY",
       "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_WIDTH)_KEY",
       "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_WIDTH)_KEY",
       AlgorithmSettingTypeEnum.HIDDEN,
       currentVersion);
     addAlgorithmSetting(backgroundRegionWidthSetting);

    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * Runs 'Measurement' on all the specified joints.
   *
   * @author Matt Wharton
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Run Locator.
    Locator.locateJoints(reconstructedImages, padSlice, jointInspectionDataObjects, this, true);

    // Get the applicable image for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);
    Image padSliceImage = reconstructedPadSlice.getOrthogonalImage();

    //Siew Yeng - XCR-2683 - add enhanced image
    if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
    {
      ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, padSlice);
    }
    
    // clear out the locator graphics
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    //Siew Yeng - XCR-3532- Fillet Length Region Outlier
    List<Float> validFilletLengthMeasurementsInMils = new ArrayList<Float>();
    List<Float> validCenterThicknessMeasurementsInMils = new ArrayList<Float>();
    
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();

      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                jointInspectionData.getInspectionRegion(),
                                                reconstructedPadSlice,
                                                false);

      // Run the basic Gullwing measurements on this joint.
      GullwingUnreportedMeasurementValues unreportedMeasurementValues =
          basicGullwingClassifyJoint(padSliceImage, padSlice, subtype, jointInspectionData, MILIMETER_PER_PIXEL);

      // Post the final pad thickness profile and graphics.
      if (ImageAnalysis.areDiagnosticsEnabled(jointTypeEnum, this))
      {
        RegionOfInterest fractionalWidthPadProfileRoi = unreportedMeasurementValues.getFractionalWidthPadProfileRoi();
        float[] fractionalWidthPadThicknessProfile = unreportedMeasurementValues.getPadThicknessProfileUsingFractionalPadLengthAcross();
        float heelEdgeSubpixelBin = unreportedMeasurementValues.getHeelEdgeSubpixelBin();
        int heelPeakSearchDistanceInPixels = unreportedMeasurementValues.getHeelPeakSearchDistanceInPixels();
        int heelPeakBin = unreportedMeasurementValues.getHeelPeakBin();
        int centerBin = unreportedMeasurementValues.getCenterBin();
        int toePeakBin = unreportedMeasurementValues.getToePeakBin();
        float toeEdgeSubpixelBin = unreportedMeasurementValues.getToeEdgeSubpixelBin();

        Collection<DiagnosticInfo> profileAndFeaturesDiagnosticInfos =
            createMeasurementRegionDiagInfosForProfile(jointInspectionData,
                                                       fractionalWidthPadProfileRoi,
                                                       fractionalWidthPadThicknessProfile,
                                                       (int)heelEdgeSubpixelBin,
                                                       heelPeakBin,
                                                       centerBin,
                                                       toePeakBin,
                                                       (int)toeEdgeSubpixelBin);
        ProfileDiagnosticInfo smoothedPadProfileDiagnosticInfo =
            createLabeledPadThicknessProfileDiagInfo(jointInspectionData,
                                                     fractionalWidthPadThicknessProfile,
                                                     (int)heelEdgeSubpixelBin,
                                                     heelPeakBin,
                                                     heelPeakSearchDistanceInPixels,
                                                     centerBin,
                                                     toePeakBin,
                                                     (int)toeEdgeSubpixelBin);
        profileAndFeaturesDiagnosticInfos.add(smoothedPadProfileDiagnosticInfo);

        _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                     padSlice,
                                     jointInspectionData,
                                     this,
                                     false,
                                     profileAndFeaturesDiagnosticInfos.toArray(new DiagnosticInfo[0]));
      }
      
      //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
      JointMeasurement filletLengthMeasurement = GullwingMeasurementAlgorithm.getFilletLengthMeasurement(jointInspectionData, padSlice);
      validFilletLengthMeasurementsInMils.add((float)MathUtil.convertMillimetersToMils(filletLengthMeasurement.getValue()));
      
      JointMeasurement centerThicknessMeasurement = GullwingMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, padSlice);
      validCenterThicknessMeasurementsInMils.add((float)MathUtil.convertMillimetersToMils(centerThicknessMeasurement.getValue()));
    }
    
    computeRegionOutlierMeasurementsForFilletLength(jointInspectionDataObjects,
                                                    ArrayUtil.convertFloatListToFloatArray(validFilletLengthMeasurementsInMils),
                                                    padSlice);
    computeRegionOutlierMeasurementsForCenterThickness(jointInspectionDataObjects,
                                                        ArrayUtil.convertFloatListToFloatArray(validCenterThicknessMeasurementsInMils),
                                                        padSlice);
  }

  /**
   * Converts the given filletPeakIndex, which is relative to the located joint region-of-interest, into image coordinates.
   *
   * Because the joint regions have varying orientations, this is more complicated than I'd like, but here is the
   * explanation for the varying formula in the code below:
   *
   * Gullwing joints of orientation 0:  In the image, the heel is to the LEFT of the toe.  So the result should be the
   * MINIMUM coordinate along the region PLUS the region-relative index.
   *
   * Gullwing joints of orientation 90:  In the image, the heel is BELOW the toe.  So the result should be the MAXIMUM coordinate
   * along the region MINUS the region-relative index.
   *
   * Gullwing joints of orientation 180:  In the image, the heel is to the RIGHT the toe.  So the result should be the MAXIMUM coordinate
   * along the region MINUS the region-relative index.
   *
   * Gullwing joints of orientation 270:  In the image, the heel is ABOVE the toe.  So the result should be the MINIMUM coordinate
   * along the region PLUS the region-relative index.
   *
   * @author Peter Esbensen
   */
  private float getFilletPeakInImageCoordinates(RegionOfInterest locatedJointRoi,
                                                float filletPeakIndex)
  {
    Assert.expect(locatedJointRoi != null);

    // Determine the fillet peak posistion in image coordinates
    // Start by finding the pixel coordinate representing the first bin in the profile.
    double locatedJointROIStartPixel = locatedJointRoi.getMaxCoordinateAlong();
    int sign = -1;
    if ((locatedJointRoi.getOrientationInDegrees() == 0) || (locatedJointRoi.getOrientationInDegrees() == 270))
    {
      locatedJointROIStartPixel = locatedJointRoi.getMinCoordinateAlong();
      sign = 1;
    }

    // Now add in the fillet peak index
    float filletPeakBinInImageCoordinates = (float)locatedJointROIStartPixel + sign * filletPeakIndex;
    return filletPeakBinInImageCoordinates;
  }

  /**
   * Performs the joint classification (measurement) for basic Gullwing.
   *
   * @author Matt Wharton
   * @author Poh Kheng
   */
  protected GullwingUnreportedMeasurementValues basicGullwingClassifyJoint(Image image,
                                                                           SliceNameEnum sliceNameEnum,
                                                                           Subtype subtype,
                                                                           JointInspectionData jointInspectionData,
                                                                           final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);

    // Post a diagnostic to reset the diagnostic infos but not invalidate them.
    _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                         subtype,
                                                         this);

    // Keep track of the measurement data we're going to post as diagnostics.
    Collection<JointMeasurement> measurementsToPost = new LinkedList<JointMeasurement>();

    // Get the located joint ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the JointInspectionResult for this joint.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate the distance between CAD position and located position.
    RegionOfInterest cadPositionRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels();
    ImageCoordinate cadPositionCenterCoordinate = new ImageCoordinate(cadPositionRoi.getCenterX(),
                                                                      cadPositionRoi.getCenterY());
    ImageCoordinate locatedJointCenterCoordinate = new ImageCoordinate(locatedJointRoi.getCenterX(),
                                                                       locatedJointRoi.getCenterY());
    double jointOffsetFromCadInPixels = cadPositionCenterCoordinate.distance(locatedJointCenterCoordinate);
    float jointOffsetFromCadInMillis = (float)(jointOffsetFromCadInPixels * MILIMETER_PER_PIXEL);

    // Save the "offset from CAD" measurement.
    JointMeasurement jointOffsetFromCadMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD,
                             MeasurementUnitsEnum.MILLIMETERS,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             jointOffsetFromCadInMillis);
    jointInspectionResult.addMeasurement(jointOffsetFromCadMeasurement);
    measurementsToPost.add(jointOffsetFromCadMeasurement);

    // Is profile reversal required based on the joint type?
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
    Assert.expect(_jointTypeToProfileReversalRequiredMap.containsKey(jointTypeEnum));
    boolean profileReversalRequired = _jointTypeToProfileReversalRequiredMap.get(jointTypeEnum);

    // Get the smoothed thickness profiles for the pad.
    // We want one profile that only uses a specified (by algorithm setting) fraction of the pad length along
    // and one that uses the entire width across.  We'll use the whole pad length along profile for finding the heel edge
    // and peak (this helps compensate for heel voids).  The fractional pad length along profile will be used for
    // measuring everything else.
    //
    
    // Determine voiding compensation method
    boolean useGaussianVoidCompensationMethod = isGaussianVoidCompensationMethodEnabled(subtype);
    
    // If heel void compensation is disabled, we'll just use the fractional width profile in lieu of the full width one.
    boolean useHeelVoidCompensation = isHeelVoidCompensationEnabled(subtype);

    // Fractional pad length across profile.
    final float PAD_PROFILE_WIDTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH) / 100.f;
    final float PAD_PROFILE_LENGTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH) / 100.f;
    Pair<RegionOfInterest, float[]> fractionalWidthPadProfileRoiAndThicknessProfile =
        measureSmoothedPadThicknessProfile(image,
                                           sliceNameEnum,
                                           jointInspectionData,
                                           PAD_PROFILE_WIDTH_FRACTION,
                                           PAD_PROFILE_LENGTH_FRACTION,
                                           true);
    RegionOfInterest fractionalWidthPadProfileRoi = fractionalWidthPadProfileRoiAndThicknessProfile.getFirst();
    float[] fractionalWidthPadThicknessProfile = fractionalWidthPadProfileRoiAndThicknessProfile.getSecond();

    // Measure the full pad length across profile.  If void compensation is disabled, just use the fractional
    // profile.
    RegionOfInterest fullWidthPadProfileRoi = null;
    float[] fullWidthPadThicknessProfile = null;
    if (useHeelVoidCompensation)
    {
      Pair<RegionOfInterest, float[]> fullWidthPadProfileRoiAndThicknessProfile =
          measureSmoothedPadThicknessProfile(image,
                                             sliceNameEnum,
                                             jointInspectionData,
                                             1f,
                                             PAD_PROFILE_LENGTH_FRACTION,
                                             false);
      fullWidthPadProfileRoi = fullWidthPadProfileRoiAndThicknessProfile.getFirst();
      fullWidthPadThicknessProfile = fullWidthPadProfileRoiAndThicknessProfile.getSecond();
    }
    else
    {
      fullWidthPadProfileRoi = fractionalWidthPadProfileRoi;
      fullWidthPadThicknessProfile = new float[fractionalWidthPadThicknessProfile.length];
      System.arraycopy(fractionalWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile.length);
    }
    
    // Smooth solder profile thickness by using Gaussian method
    if (useGaussianVoidCompensationMethod)
    {
      int smoothSensitivity = (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY);
      smoothSensitivity = Math.min(smoothSensitivity, image.getWidth());
      smoothSensitivity = Math.min(smoothSensitivity, image.getHeight());
      
      fullWidthPadThicknessProfile = ProfileUtil.getGaussainSmoothedProfile(fullWidthPadThicknessProfile, smoothSensitivity);
      fractionalWidthPadThicknessProfile = ProfileUtil.getGaussainSmoothedProfile(fractionalWidthPadThicknessProfile, smoothSensitivity);
    }

    // Reverse the profiles if needed.
    if (profileReversalRequired)
    {
      ProfileUtil.reverseProfile(fullWidthPadThicknessProfile);
      ProfileUtil.reverseProfile(fractionalWidthPadThicknessProfile);
    }

    /** @todo mdw - this is TEMP CODE to help Vyn with some auto focus debugging */
    if (_logAlgorithmProfileData)
    {
      CSVFileWriterAxi gullwingProfileDataWriter = InspectionEngine.getInstance().getGullwingProfileDataWriter();
      String uniquePadName = jointInspectionData.getFullyQualifiedPadName();
      for (int i = 0; i < fractionalWidthPadThicknessProfile.length; ++i)
      {
        float thicknessProfileValueInMils = MathUtil.convertMillimetersToMils(fractionalWidthPadThicknessProfile[i]);
        gullwingProfileDataWriter.writeDataLine(Arrays.asList(uniquePadName,
                                                              String.valueOf(i),
                                                              String.valueOf(thicknessProfileValueInMils)));
      }
    }

    // Locate the heel edge and peak.
    final float HEEL_EDGE_THICKNESS_FRACTION_OF_MAX =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(fullWidthPadThicknessProfile,
                                               (float)HEEL_EDGE_THICKNESS_FRACTION_OF_MAX, false);
    int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
    final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
    int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                        HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
    int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(fullWidthPadThicknessProfile,
                                                          (int)heelEdgeSubpixelBin,
                                                          heelPeakSearchDistanceInPixels);

    // Estimate the heel thickness.
    float heelThicknessInMillimeters = estimateHeelThickness(image,
                                                             sliceNameEnum,
                                                             jointInspectionData,
                                                             fractionalWidthPadProfileRoi,
                                                             fractionalWidthPadThicknessProfile,
                                                             heelPeakBin);
    // Clamp the heel thickness to the smallest possible non-zero positive float.
    heelThicknessInMillimeters = Math.max(heelThicknessInMillimeters, Float.MIN_VALUE);

    // Save the heel thickness measurement.
    JointMeasurement heelThicknessMeasurement = new JointMeasurement(this,
                                                                     MeasurementEnum.GULLWING_HEEL_THICKNESS,
                                                                     MeasurementUnitsEnum.MILLIMETERS,
                                                                     jointInspectionData.getPad(),
                                                                     sliceNameEnum,
                                                                     heelThicknessInMillimeters);
    jointInspectionResult.addMeasurement(heelThicknessMeasurement);
    measurementsToPost.add(heelThicknessMeasurement);

    // Find the peak thickness in the entire profile (not just within the heel search region)
    float filletPeak = ArrayUtil.interpolatedMaxIndex(fullWidthPadThicknessProfile);

    // Determine the fillet peak posistion in image coordinates
//    float filletPeakBinInImageCoordinates = getFilletPeakInImageCoordinates(locatedJointRoi, filletPeak);

    // Determine the fillet peak posistion in image coordinates RELATIVE TO CAD
    float filletPeakBinInImageCoordinates = getFilletPeakInImageCoordinates(locatedJointRoi, filletPeak);
    float filletLocationAccordingToCad = (float)jointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong();
    float filletPeakLocationRelativeToCad = filletPeakBinInImageCoordinates - filletLocationAccordingToCad;

    // Save the fillet peak position measurement.
    // Normally, we should never have measurements in raw pixel units, but this measurement will not be seen directly by the user.
    // Instead, we will convert it to millimeters in the Misalignment algorithm after comparing it to the region median.
    JointMeasurement filletPeakPositionMeasurement = new JointMeasurement(this,
                                                                          MeasurementEnum.GULLWING_FILLET_PEAK_POSITION,
                                                                          MeasurementUnitsEnum.PIXELS,
                                                                          jointInspectionData.getPad(),
                                                                          sliceNameEnum,
                                                                          filletPeakLocationRelativeToCad);
    jointInspectionResult.addMeasurement(filletPeakPositionMeasurement);

    // Locate the toe edge.
    float targetToeEdgeThickness = getTargetToeEdgeThickness(jointInspectionData, fractionalWidthPadThicknessProfile);
    float toeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(fractionalWidthPadThicknessProfile,
                                             targetToeEdgeThickness,
                                             heelPeakBin);

    // make sure toe edge is >= heel edge
    if (toeEdgeSubpixelBin < heelEdgeSubpixelBin)
    {
      toeEdgeSubpixelBin = heelEdgeSubpixelBin;
    }

    // For the basic "Gullwing" inspection family, locate the toe peak.
    int toePeakBin = _UNUSED_BIN;
    if (isToePeakApplicable())
    {
      final float PIN_LENGTH_IN_MILLIMETERS = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH);
      int pinLengthInPixels = (int)Math.ceil(PIN_LENGTH_IN_MILLIMETERS / MILIMETER_PER_PIXEL);
      toePeakBin = AlgorithmFeatureUtil.locateToePeak(fractionalWidthPadThicknessProfile,
                                                      heelPeakBin,
                                                      pinLengthInPixels);

      // If the (truncated) toe edge and toe peak fall on the same bin, move the toe peak one bin back.
      if ((int)toeEdgeSubpixelBin == toePeakBin)
      {
        toePeakBin = Math.max(toePeakBin - 1, 0);
      }

      //Lim, Lay Ngor - XCR1766 - Add pin length measurement - START 
      final float pinlengthInMm = getPinLengthInMmFromInspectionImage(
        heelPeakBin,
        fractionalWidthPadThicknessProfile,
        MILIMETER_PER_PIXEL);
    
      //Add to measurement data to be display at measurement chart.
      JointMeasurement jointPinLengthMeasurement =
        new JointMeasurement(this,
        MeasurementEnum.GULLWING_JOINT_PIN_LENGTH,
        MeasurementUnitsEnum.MILLIMETERS, 
        jointInspectionData.getPad(),
        sliceNameEnum,
        pinlengthInMm);
      jointInspectionResult.addMeasurement(jointPinLengthMeasurement);
      measurementsToPost.add(jointPinLengthMeasurement);
      //Lim, Lay Ngor - XCR1766 - Add pin length measurement - END
      
      // Get the toe thickness.
      float toeThicknessInMillimeters = fractionalWidthPadThicknessProfile[toePeakBin];

      // Save the toe thickness measurement.
      JointMeasurement toeThicknessMeasurement = new JointMeasurement(this,
                                                                      MeasurementEnum.GULLWING_TOE_THICKNESS,
                                                                      MeasurementUnitsEnum.MILLIMETERS,
                                                                      jointInspectionData.getPad(),
                                                                      sliceNameEnum,
                                                                      toeThicknessInMillimeters);
      jointInspectionResult.addMeasurement(toeThicknessMeasurement);
      measurementsToPost.add(toeThicknessMeasurement);
    }

    // Measure the "fillet length".
    float filletLengthInMillimeters = measureFilletLength(heelEdgeSubpixelBin, toeEdgeSubpixelBin, MILIMETER_PER_PIXEL);

    // Save the "fillet length" measurement.
    JointMeasurement filletLengthMeasurement = new JointMeasurement(this,
                                                                    MeasurementEnum.GULLWING_FILLET_LENGTH,
                                                                    MeasurementUnitsEnum.MILLIMETERS,
                                                                    jointInspectionData.getPad(),
                                                                    sliceNameEnum,
                                                                    filletLengthInMillimeters);
    jointInspectionResult.addMeasurement(filletLengthMeasurement);
    measurementsToPost.add(filletLengthMeasurement);

    // Measure the average thickness between the heel edge and [heel edge + nominal fillet length] (fillet thickness).
    final float NOMINAL_FILLET_LENGTH_IN_MILLIMETERS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH);
    int nominalFilletLengthInPixels = (int)Math.ceil(NOMINAL_FILLET_LENGTH_IN_MILLIMETERS / MILIMETER_PER_PIXEL);
    float filletThicknessInMillimeters = measureFilletThickness(fractionalWidthPadThicknessProfile,
                                                                (int)heelEdgeSubpixelBin,
                                                                nominalFilletLengthInPixels);

    // Save the "fillet thickness" measurement.
    JointMeasurement filletThicknessMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.GULLWING_FILLET_THICKNESS,
                                                                       MeasurementUnitsEnum.MILLIMETERS,
                                                                       jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       filletThicknessInMillimeters);
    jointInspectionResult.addMeasurement(filletThicknessMeasurement);
    measurementsToPost.add(filletThicknessMeasurement);

    // Determine the center.  The technique used depends on if this is "basic" or "advanced" gullwing.
    int centerBin = locateCenter(jointInspectionData,
                                 fractionalWidthPadThicknessProfile,
                                 heelEdgeSubpixelBin,
                                 heelPeakBin,
                                 toeEdgeSubpixelBin,
                                 MILIMETER_PER_PIXEL);

    // Center thickness is simply the thickness value at the determined center profile bin.
    float centerThicknessInMillimeters = fractionalWidthPadThicknessProfile[centerBin];
    // Clamp the center thickness to the smallest possible non-zero positive float.
    centerThicknessInMillimeters = Math.max(centerThicknessInMillimeters, Float.MIN_VALUE);

    // Save the center thickness measurement.
    JointMeasurement centerThicknessMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.GULLWING_CENTER_THICKNESS,
                                                                       MeasurementUnitsEnum.MILLIMETERS,
                                                                       jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       centerThicknessInMillimeters);
    jointInspectionResult.addMeasurement(centerThicknessMeasurement);

    // Compute the center:heel ratio.
    float centerThicknessPercentOfHeelThickness = 100.f;
    if (MathUtil.fuzzyGreaterThan(heelThicknessInMillimeters, 0f))
    {
      centerThicknessPercentOfHeelThickness = (centerThicknessInMillimeters / heelThicknessInMillimeters) * 100.f;
    }

    // Save the center:heel ratio measurement.
    JointMeasurement centerToHeelThicknessPercentMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT,
                             MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             centerThicknessPercentOfHeelThickness);
    jointInspectionResult.addMeasurement(centerToHeelThicknessPercentMeasurement);
    measurementsToPost.add(centerToHeelThicknessPercentMeasurement);

    // Post the measured values.
    if (ImageAnalysis.areDiagnosticsEnabled(jointTypeEnum, this))
    {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                        jointInspectionData.getInspectionRegion(),
                                                        jointInspectionData,
                                                        measurementsToPost.toArray(new JointMeasurement[0]));
    }

    // Return a data structure containing all the 'unreported' measured values for use by more
    // advanced versions of this algorithm.
    GullwingUnreportedMeasurementValues unreportedMeasurementValues =
        new GullwingUnreportedMeasurementValues(fullWidthPadProfileRoi,
                                                fullWidthPadThicknessProfile,
                                                fractionalWidthPadProfileRoi,
                                                fractionalWidthPadThicknessProfile,
                                                heelEdgeSubpixelBin,
                                                toeEdgeSubpixelBin,
                                                heelPeakSearchDistanceInPixels,
                                                heelPeakBin,
                                                centerBin,
                                                toePeakBin);

    return unreportedMeasurementValues;
  }

  /**
   * To get the the pin length in mm from inspection image.
   * Code extracted from function learnPinLengthSetting() & measurePadThicknessProfilesForLearning().
   * @author Lim, Lay Ngor - XCR1766 - Add pin length measurement
   */
  float getPinLengthInMmFromInspectionImage(
    int heelPeakBin,
    float[] fractionalWidthPadThicknessProfile,
    final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(fractionalWidthPadThicknessProfile != null);

    //Start - Code extracted from function measurePadThicknessProfilesForLearning().
    // Get the fractional pad width second derivative profile for the joint in question.    
    float[] fractionalWidthDerivativePadThicknessProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(fractionalWidthPadThicknessProfile,
      2,
      MagnificationEnum.getCurrentNorminal(),
      MeasurementUnitsEnum.MILLIMETERS);
    fractionalWidthDerivativePadThicknessProfile = ProfileUtil.getSmoothedProfile(fractionalWidthDerivativePadThicknessProfile,
      _SMOOTHING_KERNEL_LENGTH);
    //END - Code extracted from function measurePadThicknessProfilesForLearning().

    //START - Original code extracted from function learnPinLengthSetting().    
    final float pinLengthInMM = getPinLengthInMm(
      fractionalWidthDerivativePadThicknessProfile,
      heelPeakBin,
      MILIMETER_PER_PIXEL);
    //END - Original code extracted from function learnPinLengthSetting().
    return pinLengthInMM;
  }    
  
  /**
   * Makes a smoothed thickness profile for the specified joint.  The profile width is set based on the specified
   * fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad and then taking two background profiles on either
   * side of the pad.  The two background profiles are averaged together and then the foreground and average
   * background profiles are converted into a "pad thickness profile".  Finally, the thickness profile
   * is smoothed and returned.
   *
   * @author Matt Wharton
   */
  protected Pair<RegionOfInterest, float[]> measureSmoothedPadThicknessProfile(Image image,
                                                                               SliceNameEnum sliceNameEnum,
                                                                               JointInspectionData jointInspectionData,
                                                                               float fractionOfPadLengthAcross,
                                                                               float fractionOfPadLengthAlong,
                                                                               boolean postProfileRegions) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the interpad distance.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    // Calculate the placement of the pad profile region.
    RegionOfInterest padProfileRoi = new RegionOfInterest(locatedJointRoi);
    int padProfileRoiLengthAlong = (int)Math.ceil((float)padProfileRoi.getLengthAlong() * fractionOfPadLengthAlong);
    padProfileRoi.setLengthAlong(padProfileRoiLengthAlong);
    int padProfileLengthAcross = (int)Math.ceil((float)padProfileRoi.getLengthAcross() * fractionOfPadLengthAcross);
    padProfileRoi.setLengthAcross(padProfileLengthAcross);
    padProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(padProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, padProfileRoi);
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, padProfileRoi);

    // Calculate the offsets to the background profile width regions (based on some percentage of the IPD +/- half the profile width).
    Subtype subtype = jointInspectionData.getSubtype();
    final float BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION) / 100.f;

      final float BACKGROUND_REGION_WIDTH_FRACTION_OF_IPD =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_WIDTH) / 100.f;

    // background profile width is a percentage of IPD or 3, whichever is greater
    int backgroundProfileWidthInPixels = (int)Math.max((float)interPadDistanceInPixels * BACKGROUND_REGION_WIDTH_FRACTION_OF_IPD, 3.0f);

    int locatedJointRoiLengthAcross = locatedJointRoi.getLengthAcross();
    int backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAcross / 2f) + 
                                                    ((float)interPadDistanceInPixels * BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD));
    
    final float backGroundRegionLocationOffsetInMils =  (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET);
    if(backGroundRegionLocationOffsetInMils>0.0f)
    {
      int nanosPerPixel = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      int backGroundRegionLocationOffsetInPixels = (int)Math.round((float)MathUtil.convertMilsToNanoMetersInteger(backGroundRegionLocationOffsetInMils) /
                                                               (float)nanosPerPixel);;

      backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAcross / 2f) + backGroundRegionLocationOffsetInPixels);
    }

    // Calculate the placement of the background profile regions.
    RegionOfInterest backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
    backgroundProfile1Roi.setLengthAlong(padProfileRoiLengthAlong);
    backgroundProfile1Roi.translateAlongAcross(0, -backgroundProfileRoiOffset);
    backgroundProfile1Roi.setLengthAcross(backgroundProfileWidthInPixels);

    RegionOfInterest backgroundProfile2Roi = new RegionOfInterest(locatedJointRoi);
    backgroundProfile2Roi.setLengthAlong(padProfileRoiLengthAlong);
    backgroundProfile2Roi.translateAlongAcross(0, backgroundProfileRoiOffset);
    backgroundProfile2Roi.setLengthAcross(backgroundProfileWidthInPixels);

    // Make sure the background profile ROIs are snapped back onto the image.
    if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region 1") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
    }
    if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile2Roi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region 2") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile2Roi);
    }

    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo backgroundProfile1RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile1Roi,
                                                MeasurementRegionEnum.GULLWING_BACKGROUND_REGION);
        MeasurementRegionDiagnosticInfo backgroundProfile2RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile2Roi,
                                                MeasurementRegionEnum.GULLWING_BACKGROUND_REGION);

        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(padProfileRoi,
                                                MeasurementRegionEnum.GULLWING_PAD_REGION);

        // Only display background profiles that are enabled
        if (isBothBackgroundRegionsEnabled(subtype))
        {
//          System.out.println("Both background regions - display both");
          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       backgroundProfile1RoiDiagInfo,
                                       backgroundProfile2RoiDiagInfo,
                                       padProfileRoiDiagInfo);
        }
        else if (isLeftBackgroundRegionEnabled(subtype))
        {
//          System.out.println("Left background region - display left");
          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       backgroundProfile1RoiDiagInfo,
                                       padProfileRoiDiagInfo);
        }
        else if (isRightBackgroundRegionEnabled(subtype))
        {
//          System.out.println("Right background region - display right");
          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       backgroundProfile2RoiDiagInfo,
                                       padProfileRoiDiagInfo);
        }
        else
        {
          // shouldn't get here
          Assert.expect(false, "Unknown background regionenabled");
        }
      }
    }

    // Measure the background profiles.
    float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
    float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundProfile2Roi);

    // Create an average background gray level profile or use one or the other
    Assert.expect(backgroundProfile1.length == backgroundProfile2.length);
    float[] averageBackgroundProfile = new float[backgroundProfile1.length];
    if (isBothBackgroundRegionsEnabled(subtype))
    {
//      System.out.println("Both background regions - use average");
      for (int i = 0; i < averageBackgroundProfile.length; ++i)
      {
        averageBackgroundProfile[i] = (backgroundProfile1[i] + backgroundProfile2[i]) / 2f;
      }
    }
    else if (isLeftBackgroundRegionEnabled(subtype))
    {
//      System.out.println("Left background regions - use left");
      for (int i = 0; i < averageBackgroundProfile.length; ++i)
      {
        averageBackgroundProfile[i] = backgroundProfile1[i];
      }
    }
    else if (isRightBackgroundRegionEnabled(subtype))
    {
//      System.out.println("Right background regions - use right");
      for (int i = 0; i < averageBackgroundProfile.length; ++i)
      {
        averageBackgroundProfile[i] = backgroundProfile2[i];
      }
    }
    else
    {
      // shouldn't get here
      Assert.expect(false, "Unknown background region enabled");
    }

    // Create a thickness profile using the pad profile as foreground and the average background profile as background.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(padGrayLevelProfile, averageBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      _SMOOTHING_KERNEL_LENGTH);

    return new Pair<RegionOfInterest, float[]>(padProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
   * Estimates the heel thickness of the specified pad thickness profile.  Uses a side profile across
   * the heel to determine whether or a not a heel void is present.  If a heel void is present, uses
   * the max value in the side profile.  Otherwise, just uses the thickness value at the specified
   * heel peak bin.
   *
   * @author Matt Wharton
   */
  protected float estimateHeelThickness(Image image,
                                        SliceNameEnum sliceNameEnum,
                                        JointInspectionData jointInspectionData,
                                        RegionOfInterest padProfileRoi,
                                        float[] smoothedPadThicknessProfile,
                                        int heelPeakBin) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(padProfileRoi != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect((heelPeakBin >= 0f) && (heelPeakBin < smoothedPadThicknessProfile.length));

    ReconstructionRegion inspectionRegion = jointInspectionData.getInspectionRegion();
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();

    // The initial guess for the heel thickness is simply the value of the smoothed pad thickness profile at
    // the heel peak bin.
    float heelThickness = smoothedPadThicknessProfile[heelPeakBin];

    // If enabled, do heel void compensation.
    boolean useHeelVoidCompensation = isHeelVoidCompensationEnabled(jointInspectionData.getSubtype());
    if (useHeelVoidCompensation)
    {
      // Display diag that heel void compensation is enabled.
      if (ImageAnalysis.areDiagnosticsEnabled(jointTypeEnum, this))
      {
        TextualDiagnosticInfo heelVoidCompensationEnabledDiagInfo =
            new TextualDiagnosticInfo(new LocalizedString("ALGDIAG_GULLWING_HEEL_VOID_COMPENSATION_ENABLED_KEY", null));
        _diagnostics.postDiagnostics(inspectionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     heelVoidCompensationEnabledDiagInfo);
      }

      // Measure an orthogonal thickeness profile of the heel region.  This is used for heel void
      // detection and compensation.
      float[] smoothedOrthogonalHeelThicknessProfile = measureSmoothedOrthogonalHeelProfile(image,
                                                                                            sliceNameEnum,
                                                                                            jointInspectionData,
                                                                                            padProfileRoi,
                                                                                            smoothedPadThicknessProfile,
                                                                                            heelPeakBin);

      // Check for heel voids.
      int orthogonalHeelThicknessProfileMaxIndex = ArrayUtil.maxIndex(smoothedOrthogonalHeelThicknessProfile);
      float heelVoidSignal = measureHeelVoidSignal(smoothedOrthogonalHeelThicknessProfile,
                                                   orthogonalHeelThicknessProfileMaxIndex);
      final float MAX_ALLOWABLE_HEEL_VOID_SIGNAL = 0.1f; // Assume a max void signal of 10% for now...
      if (heelVoidSignal > MAX_ALLOWABLE_HEEL_VOID_SIGNAL)
      {
        // We detected a likely heel void.

        // Display diag that void has been detected.
        if (ImageAnalysis.areDiagnosticsEnabled(jointTypeEnum, this))
        {
          TextualDiagnosticInfo detectedHeelVoidDiagInfo =
              new TextualDiagnosticInfo(new LocalizedString("ALGDIAG_GULLWING_MEASUREMENT_HEEL_VOID_DETECTED_KEY", null));
          _diagnostics.postDiagnostics(inspectionRegion,
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       detectedHeelVoidDiagInfo);
        }
      }

      // Bump up the heel thickness if the value at the max index of the orthogonal heel thickness profile is
      // greater than the initial heel thickness estimate.
      heelThickness = Math.max(heelThickness,
                               smoothedOrthogonalHeelThicknessProfile[orthogonalHeelThicknessProfileMaxIndex]);
    }
    else
    {
      // Display diag that heel void compensation is disabled.
      if (ImageAnalysis.areDiagnosticsEnabled(jointTypeEnum, this))
      {
        TextualDiagnosticInfo heelVoidCompensationDisabledDiagInfo =
            new TextualDiagnosticInfo(new LocalizedString("ALGDIAG_GULLWING_HEEL_VOID_COMPENSATION_DISABLED_KEY", null));
        _diagnostics.postDiagnostics(inspectionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     heelVoidCompensationDisabledDiagInfo);
      }
    }

    return heelThickness;
  }

  /**
   * Measures a thickness profile around the heel region.  The profile's ROI spans the pad length across
   * and is orientation orthogonally to the pad.
   *
   * The returned profile is also smoothed.
   *
   * @author Matt Wharton
   */
  private float[] measureSmoothedOrthogonalHeelProfile(Image image,
                                                       SliceNameEnum sliceNameEnum,
                                                       JointInspectionData jointInspectionData,
                                                       RegionOfInterest padProfileRoi,
                                                       float[] smoothedPadThicknessProfile,
                                                       int heelPeakBin) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(padProfileRoi != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect((heelPeakBin >= 0f) && (heelPeakBin < smoothedPadThicknessProfile.length));

    // Get the subtype and joint type.
    Subtype subtype = jointInspectionData.getSubtype();
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Figure out the orthogonal heel profile width.  For now, just use 40% of the pad length across.
    int padLengthAcrossInPixels = locatedJointRoi.getLengthAcross();
    final float FRACTION_OF_PAD_LENGTH_ACROSS_FOR_ORTHOGONAL_HEEL_PROFILE = 0.4f;
    int orthogonalHeelProfileWidthInPixels = (int)Math.ceil(padLengthAcrossInPixels *
                                                            FRACTION_OF_PAD_LENGTH_ACROSS_FOR_ORTHOGONAL_HEEL_PROFILE);

    Assert.expect(_jointTypeToProfileReversalRequiredMap.containsKey(jointTypeEnum));
    boolean profileReversalRequired = _jointTypeToProfileReversalRequiredMap.get(jointTypeEnum);

    // Create the orthogonal heel profile ROI and adjust it's position accordingly.
    RegionOfInterest orthogonalHeelProfileRoi = new RegionOfInterest(padProfileRoi);
    orthogonalHeelProfileRoi.setLengthAcross(padLengthAcrossInPixels);
    orthogonalHeelProfileRoi.setLengthAlong(orthogonalHeelProfileWidthInPixels);
    int distanceToPadProfileRoiEdge = padProfileRoi.getLengthAlong() / 2;
    if (profileReversalRequired)
    {
      orthogonalHeelProfileRoi.translateAlongAcross(distanceToPadProfileRoiEdge - heelPeakBin, 0);
    }
    else
    {
      orthogonalHeelProfileRoi.translateAlongAcross(-distanceToPadProfileRoiEdge + heelPeakBin, 0);
    }

    if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
    {
      orthogonalHeelProfileRoi.setOrientationInDegrees(90);
    }
    else
    {
      orthogonalHeelProfileRoi.setOrientationInDegrees(0);
    }


    // Make sure the ROI fits within the image.
    boolean orthogonalHeelProfileRoiFitsInImage = AlgorithmUtil.checkRegionBoundaries(orthogonalHeelProfileRoi,
                                                                                      image,
                                                                                      jointInspectionData.getInspectionRegion(),
                                                                                      "Orthogonal Heel Profile ROI");
    if (orthogonalHeelProfileRoiFitsInImage == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, orthogonalHeelProfileRoi);
    }

    // Measure the orthogonal heel gray level profile.
    float[] orthogonalHeelGrayLevelProfile = ImageFeatureExtraction.profile(image, orthogonalHeelProfileRoi);

    // Get the estimated background trend profile from the gray level profile.
    float[] orthogonalHeelBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(orthogonalHeelGrayLevelProfile);

    // Using the measured gray level profile and estimated background trend profile, create a
    // thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] orthogonalHeelThicknessProfileInMillimeters =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(orthogonalHeelGrayLevelProfile, orthogonalHeelBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedOrthogonalHeelThicknessProfile = ProfileUtil.getSmoothedProfile(orthogonalHeelThicknessProfileInMillimeters,
                                                                                    _SMOOTHING_KERNEL_LENGTH);

    return smoothedOrthogonalHeelThicknessProfile;
  }

  /**
   * Measures the heel "void signal" using the specified smoothed orthogonal heel thickness profile.
   * This is simply a measurement of the percentage distance (of total profile length) between the
   * profile midpoint and the specified profile max.
   *
   * @author Matt Wharton
   */
  private float measureHeelVoidSignal(float[] smoothedOrthogonalHeelThicknessProfile, int maximumBin)
  {
    Assert.expect(smoothedOrthogonalHeelThicknessProfile != null);
    Assert.expect(smoothedOrthogonalHeelThicknessProfile.length > 0);
    Assert.expect((maximumBin >= 0) && (maximumBin < smoothedOrthogonalHeelThicknessProfile.length));

    // Measure the percentage distance from the max bin to the profile midpoint.
    float profileMidpoint = smoothedOrthogonalHeelThicknessProfile.length / 2f;
    float voidSignal = Math.abs((profileMidpoint - maximumBin) / smoothedOrthogonalHeelThicknessProfile.length);

    return voidSignal;
  }


  /**
   * @return true if the pin length is relevant, false otherwise.
   *
   * @author Matt Wharton
   */
  protected boolean isPinLengthApplicable()
  {
    return true;
  }

  /**
   * @return true if the toe peak location and thickness are relevant, false otherwise.
   *
   * @author Matt Wharton
   */
  protected boolean isToePeakApplicable()
  {
    return true;
  }

  /**
   * Measures the fillet length based on the distance between the heel and toe edge subpixel bins.
   *
   * @author Matt Wharton
   */
  protected float measureFilletLength(float heelEdgeSubpixelBin, float toeEdgeSubpixelBin, final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(MathUtil.fuzzyLessThanOrEquals(heelEdgeSubpixelBin, toeEdgeSubpixelBin));

    // Calculate the fillet length in millimeters.
    float filletLengthInPixels = toeEdgeSubpixelBin - heelEdgeSubpixelBin;
    float filletLengthInMillimeters = filletLengthInPixels * MILIMETER_PER_PIXEL;

    return filletLengthInMillimeters;
  }

  /**
   * Measures the average thickness value in the specified profile between the specified heel edge bin and
   * "heel edge bin" + "nominal fillet length".
   *
   * @author Matt Wharton
   */
  protected float measureFilletThickness(float[] smoothedPadThicknessProfile,
                                         int heelEdgeBin,
                                         int nominalFilletLengthInPixels)
  {
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect(heelEdgeBin >= 0);

    int toeEdgeBinBasedOnNominalFilletLength = heelEdgeBin + nominalFilletLengthInPixels;
    toeEdgeBinBasedOnNominalFilletLength = Math.min(toeEdgeBinBasedOnNominalFilletLength,
                                                    smoothedPadThicknessProfile.length - 1);

    int numberOfBins = (toeEdgeBinBasedOnNominalFilletLength - heelEdgeBin) + 1;
    Assert.expect(numberOfBins >= 1);

    // Calculate the average thickness value.
    float sumOfThicknessValues = 0f;
    for (int i = heelEdgeBin; i <= toeEdgeBinBasedOnNominalFilletLength; ++i)
    {
      sumOfThicknessValues += smoothedPadThicknessProfile[i];
    }

    float averageThickness = sumOfThicknessValues / (float)numberOfBins;

    return averageThickness;
  }

  /**
   * Locates the center given the specified pad thickness profile, heel edge bin, and heel peak bin.
   *
   * @author Matt Wharton
   */
  protected int locateCenter(JointInspectionData jointInspectionData,
                             float[] smoothedPadThicknessProfile,
                             float heelEdgeSubpixelBin,
                             int heelPeakBin,
                             float toeEdgeSubpixelBin,
                             final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect((heelEdgeSubpixelBin >= 0) && (heelEdgeSubpixelBin < smoothedPadThicknessProfile.length));
    Assert.expect((heelPeakBin >= 0) && (heelPeakBin < smoothedPadThicknessProfile.length));
    Assert.expect((toeEdgeSubpixelBin >= 0) && (toeEdgeSubpixelBin < smoothedPadThicknessProfile.length));
    
    // Figure out the appropriate center offset in pixels.
    Subtype subtype = jointInspectionData.getSubtype();
    final float CENTER_OFFSET_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET) / 100.f;
    final float PIN_LENGTH_IN_MILLIMETERS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH);
    float pinLengthInPixels = PIN_LENGTH_IN_MILLIMETERS / MILIMETER_PER_PIXEL;
    int centerOffsetInPixels = (int)Math.ceil((float)pinLengthInPixels * CENTER_OFFSET_FRACTION);

    // Calculate the center bin.
    int centerBin = Math.min(heelPeakBin + centerOffsetInPixels, smoothedPadThicknessProfile.length - 1);

    return centerBin;
  }

  /**
   * Creates a Collection of MeasurementRegionDiagnosticInfo objects which indicate the positions of the heel edge,
   * heel peak, center, toe edge, and toe peak.
   *
   * @author Matt Wharton
   */
  protected Collection<DiagnosticInfo> createMeasurementRegionDiagInfosForProfile(JointInspectionData jointInspectionData,
                                                                                  RegionOfInterest padProfileRoi,
                                                                                  float[] smoothedPadThicknessProfile,
                                                                                  int heelEdgeBin,
                                                                                  int heelPeakBin,
                                                                                  int centerBin,
                                                                                  int toePeakBin,
                                                                                  int toeEdgeBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(padProfileRoi != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);

    // Create the ROIs for the areas of interest on the profile.
    RegionOfInterest heelEdgeRoi = new RegionOfInterest(padProfileRoi);
    heelEdgeRoi.setLengthAlong(1);
    RegionOfInterest heelPeakRoi = new RegionOfInterest(padProfileRoi);
    heelPeakRoi.setLengthAlong(1);
    RegionOfInterest centerRoi = new RegionOfInterest(padProfileRoi);
    centerRoi.setLengthAlong(1);
    RegionOfInterest toePeakRoi = new RegionOfInterest(padProfileRoi);
    toePeakRoi.setLengthAlong(1);
    RegionOfInterest toeEdgeRoi = new RegionOfInterest(padProfileRoi);
    toeEdgeRoi.setLengthAlong(1);

    // Set the positions of our ROIs based on whether or not a profile reversal is in effect.
    int padProfileRoiLengthAlong = padProfileRoi.getLengthAlong();
    int distanceToRoiEdge = padProfileRoiLengthAlong / 2;
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
    Assert.expect(_jointTypeToProfileReversalRequiredMap.containsKey(jointTypeEnum));
    boolean profileReversalRequired = _jointTypeToProfileReversalRequiredMap.get(jointInspectionData.getJointTypeEnum());
    if (profileReversalRequired)
    {
      heelEdgeRoi.translateAlongAcross(distanceToRoiEdge - heelEdgeBin, 0);
      heelPeakRoi.translateAlongAcross(distanceToRoiEdge - heelPeakBin, 0);
      centerRoi.translateAlongAcross(distanceToRoiEdge - centerBin, 0);
      toePeakRoi.translateAlongAcross(distanceToRoiEdge - toePeakBin, 0);
      toeEdgeRoi.translateAlongAcross(distanceToRoiEdge - toeEdgeBin, 0);
    }
    else
    {
      heelEdgeRoi.translateAlongAcross(-distanceToRoiEdge + heelEdgeBin, 0);
      heelPeakRoi.translateAlongAcross(-distanceToRoiEdge + heelPeakBin, 0);
      centerRoi.translateAlongAcross(-distanceToRoiEdge + centerBin, 0);
      toePeakRoi.translateAlongAcross(-distanceToRoiEdge + toePeakBin, 0);
      toeEdgeRoi.translateAlongAcross(-distanceToRoiEdge + toeEdgeBin, 0);
    }

    // Create our MeasurementRegionDiagnosticInfos and add them to our collection.
    Collection<DiagnosticInfo> measurementRegionDiagnosticInfos = new LinkedList<DiagnosticInfo>();
    if (heelEdgeBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(heelEdgeRoi,
                                                                               MeasurementRegionEnum.GULLWING_HEEL_EDGE_REGION));
    }
    if (heelPeakBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(heelPeakRoi,
                                                                               MeasurementRegionEnum.GULLWING_HEEL_PEAK_REGION));
    }
    if (centerBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(centerRoi,
                                                                               MeasurementRegionEnum.GULLWING_CENTER_REGION));
    }
    if (toePeakBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(toePeakRoi,
                                                                               MeasurementRegionEnum.GULLWING_TOE_PEAK_REGION));
    }
    if (toeEdgeBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(toeEdgeRoi,
                                                                               MeasurementRegionEnum.GULLWING_TOE_EDGE_REGION));
    }

    return measurementRegionDiagnosticInfos;
  }

  /**
   * Creates a 'labeled' version of the specified pad thickness profile that accentuates the
   * various features we measured.
   *
   * @author Matt Wharton
   */
  private ProfileDiagnosticInfo createLabeledPadThicknessProfileDiagInfo(JointInspectionData jointInspectionData,
                                                                         float[] smoothedPadThicknessProfile,
                                                                         int heelEdgeBin,
                                                                         int heelPeakBin,
                                                                         int heelPeakSearchDistanceInPixels,
                                                                         int centerBin,
                                                                         int toePeakBin,
                                                                         int toeEdgeBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);

    Subtype subtype = jointInspectionData.getSubtype();
    final float NOMINAL_HEEL_THICKNESS_IN_MILLIS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);
    float profileMin = 0f;
    float profileMax = NOMINAL_HEEL_THICKNESS_IN_MILLIS * 1.5f;
    LocalizedString padThicknessProfileLabelLocalizedString =
        new LocalizedString("ALGDIAG_GULLWING_MEASUREMENT_PAD_THICKNESS_PROFILE_LABEL_KEY",
                            null);
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        createProfileSubrangeToMeasurementRegionEnumMap(smoothedPadThicknessProfile,
                                                        heelEdgeBin,
                                                        heelPeakBin,
                                                        heelPeakSearchDistanceInPixels,
                                                        centerBin,
                                                        toePeakBin,
                                                        toeEdgeBin);
    ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
      ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                       ProfileTypeEnum.SOLDER_PROFILE,
                                                                       padThicknessProfileLabelLocalizedString,
                                                                       profileMin,
                                                                       profileMax,
                                                                       smoothedPadThicknessProfile,
                                                                       profileSubrangeToMeasurementRegionEnumMap,
                                                                       MeasurementUnitsEnum.MILLIMETERS);

    return labeledPadThicknessProfileDiagInfo;
  }

  /**
   * @author Matt Wharton
   */
  protected Map<Pair<Integer, Integer>, MeasurementRegionEnum>
      createProfileSubrangeToMeasurementRegionEnumMap(float[] smoothedPadThicknessProfile,
                                                      int heelEdgeBin,
                                                      int heelPeakBin,
                                                      int heelPeakSearchDistanceInPixels,
                                                      int centerBin,
                                                      int toePeakBin,
                                                      int toeEdgeBin)
  {
    Assert.expect(smoothedPadThicknessProfile != null);

    // Create a ProfileDiagnosticInfo that accentuates the points of interest in the pad thickness profile.
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer, Integer>, MeasurementRegionEnum>();
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, smoothedPadThicknessProfile.length),
                                                  MeasurementRegionEnum.GULLWING_PAD_REGION);
    if (heelEdgeBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(heelEdgeBin, heelEdgeBin + 1),
                                                    MeasurementRegionEnum.GULLWING_HEEL_EDGE_REGION);
      int heelPeakSearchEndBin = Math.min(heelEdgeBin + heelPeakSearchDistanceInPixels + 1, smoothedPadThicknessProfile.length);
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(Math.min(heelEdgeBin + 1, smoothedPadThicknessProfile.length - 1),
                                                                               heelPeakSearchEndBin),
                                                    MeasurementRegionEnum.GULLWING_HEEL_SEARCH_REGION);
    }
    if (heelPeakBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(heelPeakBin, heelPeakBin + 1),
                                                    MeasurementRegionEnum.GULLWING_HEEL_PEAK_REGION);
    }
    if (centerBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(centerBin, centerBin + 1),
                                                    MeasurementRegionEnum.GULLWING_CENTER_REGION);
    }
    if (toePeakBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(toePeakBin, toePeakBin + 1),
                                                    MeasurementRegionEnum.GULLWING_TOE_PEAK_REGION);
    }
    if (toeEdgeBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(toeEdgeBin, toeEdgeBin + 1),
                                                    MeasurementRegionEnum.GULLWING_TOE_EDGE_REGION);
    }

    return profileSubrangeToMeasurementRegionEnumMap;
  }


  /**
   * @author Matt Wharton
   */
  private boolean isHeelVoidCompensationEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    boolean heelVoidCompensationEnabled = false;
    String heelVoidCompensationSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION);
    if (heelVoidCompensationSetting.equals("On"))
    {
      heelVoidCompensationEnabled = true;
    }
    else if (heelVoidCompensationSetting.equals("Off"))
    {
      heelVoidCompensationEnabled = false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected heel void compensation setting: " + heelVoidCompensationSetting);
    }

    return heelVoidCompensationEnabled;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private boolean isGaussianVoidCompensationMethodEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);
    
    boolean gaussianVoidCompensationMethodEnabled = false;
    String voidCompensationMethodSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD);
    if (voidCompensationMethodSetting.equals(VOID_COMPENSATION_METHOD_NONE))
    {
      gaussianVoidCompensationMethodEnabled = false;
    }
    else if (voidCompensationMethodSetting.equals(VOID_COMPENSATION_METHOD_GAUSSIAN))
    {
      gaussianVoidCompensationMethodEnabled = true;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected void compensation method setting: " + gaussianVoidCompensationMethodEnabled);
    }
    
    return gaussianVoidCompensationMethodEnabled;
  }

  /**
   * @author George Booth
   */
  private boolean isBothBackgroundRegionsEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    boolean backgroundRegionEnabled = false;
    String backgroundRegionEnabledSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS);
    if (backgroundRegionEnabledSetting.equals("Both"))
    {
      backgroundRegionEnabled = true;
    }
    else if (backgroundRegionEnabledSetting.equals("Right") ||
             backgroundRegionEnabledSetting.equals("Left"))
    {
      backgroundRegionEnabled = false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected background region enabled setting: " + backgroundRegionEnabledSetting);
    }

    return backgroundRegionEnabled;
  }

  /**
   * @author George Booth
   */
  private boolean isRightBackgroundRegionEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    boolean backgroundRegionEnabled = false;
    String backgroundRegionEnabledSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS);
    if (backgroundRegionEnabledSetting.equals("Right"))
    {
      backgroundRegionEnabled = true;
    }
    else if (backgroundRegionEnabledSetting.equals("Left") ||
             backgroundRegionEnabledSetting.equals("Both"))
    {
      backgroundRegionEnabled = false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected background region enabled setting: " + backgroundRegionEnabledSetting);
    }

    return backgroundRegionEnabled;
  }

  /**
   * @author George Booth
   */
  private boolean isLeftBackgroundRegionEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    boolean backgroundRegionEnabled = false;
    String backgroundRegionEnabledSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS);
    if (backgroundRegionEnabledSetting.equals("Left"))
    {
      backgroundRegionEnabled = true;
    }
    else if (backgroundRegionEnabledSetting.equals("Right") ||
             backgroundRegionEnabledSetting.equals("Both"))
    {
      backgroundRegionEnabled = false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected background region enabled setting: " + backgroundRegionEnabledSetting);
    }

    return backgroundRegionEnabled;
  }

  /**
   * Gets the heel thickness measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getHeelThicknessMeasurement(JointInspectionData jointInspectionData,
                                                             SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement heelThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_HEEL_THICKNESS);

    return heelThicknessMeasurement;
  }

  /**
   * Gets the center thickness measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getCenterThicknessMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement centerThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_CENTER_THICKNESS);

    return centerThicknessMeasurement;
  }

  /**
   * Gets the toe thickness measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getToeThicknessMeasurement(JointInspectionData jointInspectionData,
                                                            SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement toeThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_TOE_THICKNESS);

    return toeThicknessMeasurement;
  }

  /**
   * Gets the center:heel thickness ratio measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getCenterToHeelThicknessPercentMeasurement(JointInspectionData jointInspectionData,
                                                                          SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement centerToHeelThicknessRatioMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);

    return centerToHeelThicknessRatioMeasurement;
  }

  /**
   * Gets the fillet length measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getFilletLengthMeasurement(JointInspectionData jointInspectionData,
                                                            SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement filletLengthMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_FILLET_LENGTH);

    return filletLengthMeasurement;
  }

  /**
   * Gets the fillet thickness measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getFilletThicknessMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement filletThicknessMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_FILLET_THICKNESS);

    return filletThicknessMeasurement;
  }

  /**
   * Gets the joint offset from CAD measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getJointOffsetFromCadMeasurement(JointInspectionData jointInspectionData,
                                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement jointOffsetFromCadMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);

    return jointOffsetFromCadMeasurement;
  }

  /**
   * Gets the heel position measurement.
   *
   * @author Matt Wharton
   */
  static JointMeasurement getFilletPeakPositionMeasurement(JointInspectionData jointInspectionData,
                                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement heelPositionMeasurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_FILLET_PEAK_POSITION);

    return heelPositionMeasurement;
  }


  /**
   * Used to bypass learned nominals for the Gullwing Measurement algorithm.
   * Call parent class only.
   *
   * @author Lim, Seng Yew
   */
  protected void updateNominalsParentOnly(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);
  }

  /**
   * Updates the learned nominals for the Gullwing Measurement algorithm.
   *
   * @author Matt Wharton
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    // We can't update nominals if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, 0);

      return;
    }

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Dump the existing measurement data from the database.
    subtype.deleteLearnedMeasurementData();

    // Gullwing only runs (and learns) on the Pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Measure the profiles and derivative profiles (both full and fractional pad length across) for each of of the joints.
    Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> profileDataMap =
        measurePadThicknessProfilesForLearning(subtype, typicalBoardImages, padSlice);

    updateBasicGullwingNominals(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);
    
    // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
    if (profileDataMap != null)
    {
      for(Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> entry : profileDataMap.entrySet())
      {
        Pair<JointInspectionData, ImageSetData> jointInspectionDataPair = entry.getKey();
        jointInspectionDataPair.getFirst().getJointInspectionResult().clearMeasurements();
        jointInspectionDataPair.getFirst().clearJointInspectionResult();
      }
      
      profileDataMap.clear();
      profileDataMap = null;
    }
  }

  /**
   * @author Matt Wharton
   */
  protected void updateBasicGullwingNominals(Subtype subtype,
                                             SliceNameEnum sliceNameEnum,
                                             Map<Pair<JointInspectionData, ImageSetData>,
                                             GullwingProfileDataForLearning> profileDataMap,
                                             final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(profileDataMap != null);

    // Learn the "nominal heel and toe thickness" settings.
    learnNominalHeelAndToeThickness(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet length" setting.
    learnNominalFilletLength(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet thickness" setting.
    learnNominalFilletThickness(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);

    // Raise a warning if we don't have enough data points.
    int numberOfProfiles = profileDataMap.size();
    final int MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING = 10;
    if (numberOfProfiles < MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfProfiles);
    }
  }

  /**
   * Learns the algorithm settings for Gullwing Measurement.
   *
   * @author Matt Wharton
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // We can't learn if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, 0);

      return;
    }

    // Gullwing only runs (and learns) on the Pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Let Locator learn its stuff.
    //Ngie Xing - XCR-2027 Exclude Outlier , not exclude
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);

    /** @todo mdw - I'm testing out whether or not we can get away with not learning the heel void compensation setting.
     * Kathy thinks we might be able to.
     */
    //    // Before we measure the pad thickness profiles, we need to learn the heel void compensation setting.
//    learnHeelVoidCompensationSetting(subtype);

    // Measure the profiles (both full and fractional pad length across) for each of the joints.
    Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> profileDataMap =
        measurePadThicknessProfilesForLearning(subtype, typicalBoardImages, padSlice);

    // Learn the "heel peak location" setting.
    //Ngie Xing - XCR-2027 Exclude Outlier inside
    learnHeelPeakLocationSetting(subtype, padSlice, profileDataMap);

    // Learn the "pin length" setting.
    //Ngie Xing - XCR-2027 Exclude Outlier inside
    learnPinLengthSetting(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal heel and toe thickness" settings.
    //Ngie Xing - XCR-2027 Exclude Outlier inside
    learnNominalHeelAndToeThickness(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "toe edge" location setting.
    //Ngie Xing - XCR-2027 Exclude Outlier inside
    learnToeEdgeSetting(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);

    /** @todo mdw - Kathy thinks we shouldn't be learning this.  Let's take it out for now. */
//    // Learn the "center offset from heel" setting.
//    learnCenterOffsetFromHeelSetting(subtype, sliceNameEnum, profileDataMap);

    // For JLead, set the center offset to 50%.
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.JLEAD))
    {
      final float JLEAD_CENTER_OFFSET = 50.f;
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET, JLEAD_CENTER_OFFSET);
    }

    // Learn the "nominal fillet length" setting.
    //Ngie Xing - XCR-2027 Exclude Outlier inside
    learnNominalFilletLength(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet thickness" setting.
    //Ngie Xing - XCR-2027 Exclude Outlier inside
    learnNominalFilletThickness(subtype, padSlice, profileDataMap, MILIMETER_PER_PIXEL);

    /*** mdw - as per Kathy, we don't need this for 1.0
    // Measure the "center:heel thickness percents" to enable learning of Open defect settings.
    measureCenterToHeelThicknessPercentForLearning(subtype, padSlice, profileDataMap);
*/

    // Raise a warning if we don't have enough data points.
    int numberOfProfiles = profileDataMap.size();
    final int MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING = 10;
    if (numberOfProfiles < MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfProfiles);
    }
  }

  /**
   * Learns the basic Gullwing Measurement algorithm settings.
   *
   * @author Matt Wharton
   */
  protected void learnBasicGullwingSettingsOLD(Subtype subtype,
                                            SliceNameEnum sliceNameEnum,
                                            Map<Pair<JointInspectionData, ImageSetData>,
                                            GullwingProfileDataForLearning> profileDataMap,
                                            final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(profileDataMap != null);

    InspectionFamilyEnum inspectionFamilyEnum = getInspectionFamilyEnum();

    // Learn the "heel peak location" setting.
    learnHeelPeakLocationSetting(subtype, sliceNameEnum, profileDataMap);

    if (isPinLengthApplicable())
    {
      // Learn the "pin length" setting.
      learnPinLengthSetting(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);
    }

    // Learn the "nominal heel and toe thickness" settings.
    learnNominalHeelAndToeThickness(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);

    if (inspectionFamilyEnum.equals(InspectionFamilyEnum.GULLWING))
    {
      // Learn the "toe edge" location setting.
      learnToeEdgeSetting(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);
    }

    /** @todo mdw - Kathy thinks we shouldn't be learning this.  Let's take it out for now. */
//    // Learn the "center offset from heel" setting.
//    learnCenterOffsetFromHeelSetting(subtype, sliceNameEnum, profileDataMap);

    // Learn the "nominal fillet length" setting.
    learnNominalFilletLength(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet thickness" setting.
    learnNominalFilletThickness(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);

    // Measure the "center:heel thickness percents" to enable learning of Open defect settings.
    measureCenterToHeelThicknessPercentForLearning(subtype, sliceNameEnum, profileDataMap, MILIMETER_PER_PIXEL);

    // Raise a warning if we don't have enough data points.
    int numberOfProfiles = profileDataMap.size();
    final int MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING = 10;
    if (numberOfProfiles < MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfProfiles);
    }
  }

  /**
   * Learns whether or not heel void compensation should be enabled for the specified subtype.
   *
   * @author Matt Wharton
   */
  protected void learnHeelVoidCompensationSetting(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Get the component for the first pad in the subtype and count its pads.
    // If there are 5 pins or less, heel void compensation should be off, otherwise it should be on.
    final int MINIMUM_PADS_NEEDED_FOR_HEEL_VOID_COMPENSATION = 6;
    Collection<Pad> padsInSubtype = subtype.getPads();
    Assert.expect(padsInSubtype.isEmpty() == false);
    Pad firstPadInSubtype = padsInSubtype.iterator().next();
    Component firstComponentInSubtype = firstPadInSubtype.getComponent();
    Collection<Pad> padsOnComponent = firstComponentInSubtype.getPads();
    if (padsOnComponent.size() < MINIMUM_PADS_NEEDED_FOR_HEEL_VOID_COMPENSATION)
    {
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION, "Off");
    }
    else
    {
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION, "On");
    }
  }

  /**
   * Learns the heel edge and heel peak location settings.
   *
   * @author Matt Wharton
   */
  protected void learnHeelPeakLocationSetting(Subtype subtype,
                                              SliceNameEnum slice,
                                              Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> profileDataMap) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Get the full width pad thickness profile.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();

      // Locate the heel edge.
      float heelEdgeFractionOfMaxThickness =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross, heelEdgeFractionOfMaxThickness, false);
      int heelEdgeIndex = (int)heelEdgeSubpixelBin;


      // Find the max thickness value in the full pad width profile.
      // We don't want to search any further than 20% of the pad length along from the heel edge.
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      final float MAX_HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG = 0.2f;
      int maxThicknessSearchDistanceInPixels =
          (int)Math.ceil((float)locatedJointRoiLengthAlong * MAX_HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int maxThicknessSearchStartBin = heelEdgeIndex;
      int maxThicknessSearchEndBin = Math.min(heelEdgeIndex + maxThicknessSearchDistanceInPixels + 1,
                                              padThicknessProfileUsingFullPadLengthAcross.length);
      int maxThicknessIndex = ArrayUtil.maxIndex(padThicknessProfileUsingFullPadLengthAcross,
                                                 maxThicknessSearchStartBin,
                                                 maxThicknessSearchEndBin);
      float maxThicknessValue = padThicknessProfileUsingFullPadLengthAcross[maxThicknessIndex];
      // Clamp the max thickness to the minimum allowable float value.
      maxThicknessValue = Math.max(maxThicknessValue, Float.MIN_VALUE);


      // Calculate the distance (in pixels) between the measured heel edge and the max (assumed to be the heel peak).
      int heelEdgeToPeakDistanceInPixels = maxThicknessIndex - heelEdgeIndex;
      // Figure out what percent the heel edge to peak distance is of the overall pad length along.
      float heelEdgeToPeakDistanceAsPercentOfPadLengthAlong =
          ((float)heelEdgeToPeakDistanceInPixels / (float)locatedJointRoiLengthAlong) * 100.f;

      // Save the heel edge to peak distance (as fraction of pad length along) to the learning database.
      //Ngie Xing - XCR-2027 Exclude Outlier - Not exclude
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_LEARNING_HEEL_EDGE_TO_PEAK_DISTANCE_PERCENT_OF_PAD_LENGTH_ALONG,
                                                                         heelEdgeToPeakDistanceAsPercentOfPadLengthAlong,
                                                                         true,
                                                                         true));
    }

    // Pull out all the "heel edge to peak distance" measurements (new and pre-existing) from the database.
    float[] heelEdgeToPeakDistanceAsPercentOfPadLengthAlongMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_LEARNING_HEEL_EDGE_TO_PEAK_DISTANCE_PERCENT_OF_PAD_LENGTH_ALONG,
                                                 true,
                                                 true);

    if (heelEdgeToPeakDistanceAsPercentOfPadLengthAlongMeasurementValues.length == 0)
      return;

    // Find the 95th percentile "heel edge to peak distance" value.
    //Ngie Xing - XCR-2027 Exclude Outlier
    float estimatedHeelEdgeToPeakDistanceAsPercentOfPadLengthAlong =
      AlgorithmUtil.percentileWithExcludeOutlierDecision(heelEdgeToPeakDistanceAsPercentOfPadLengthAlongMeasurementValues, 0.95f, true);
    // Bump up the estimate a little bit to make sure we're searching far enough.
    final float HEEL_EDGE_TO_PEAK_DISTANCE_CORRECTION_MULTIPLIER = 1.1f;  // mdw - this is really just a guess.
    estimatedHeelEdgeToPeakDistanceAsPercentOfPadLengthAlong *= HEEL_EDGE_TO_PEAK_DISTANCE_CORRECTION_MULTIPLIER;

    // Don't let the heel edge to peak distance percent go above 20%.
    final float MAX_ALLOWABLE_HEEL_EDGE_TO_PEAK_PERCENT = 20.f;
    float learnedHeelEdgeToPeakDistance = Math.min(estimatedHeelEdgeToPeakDistanceAsPercentOfPadLengthAlong,
                                                   MAX_ALLOWABLE_HEEL_EDGE_TO_PEAK_PERCENT);

    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE,
                            learnedHeelEdgeToPeakDistance);
  }

  /**
   * Learns the pin length setting.
   *
   * @author Matt Wharton
   */
  private void learnPinLengthSetting(Subtype subtype,
                                     SliceNameEnum slice,
                                     Map<Pair<JointInspectionData, ImageSetData>,
                                     GullwingProfileDataForLearning> profileDataMap,
                                     final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
//      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();//Lim, Lay Ngor commented, unused variable
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Locate the heel edge and heel peak.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
      final float HEEL_EDGE_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                 (float)HEEL_EDGE_FRACTION_OF_MAX, false);
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                          HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
                                                            (int)heelEdgeSubpixelBin,
                                                            heelPeakSearchDistanceInPixels);

      // Get the fractional pad width second derivative profile for the joint in question.
      float[] derivativePadThicknessProfileUsingFractionalPadLengthAcross =
          profileData.getDerivativePadThicknessProfileUsingFractionalPadLengthAcross();
      
      //Lim, Lay Ngor - XCR1766 - Add pin length measurement - START
      //Centralise the code to a the function getPinLengthInMm() for inspection use as well.   
      final float pinLengthInMM = getPinLengthInMm(
        derivativePadThicknessProfileUsingFractionalPadLengthAcross,
        heelPeakBin,
        MILIMETER_PER_PIXEL);     
      //Lim, Lay Ngor - XCR1766 - Add pin length measurement - END            

      // Save the pin length to the database.
      //Ngie Xing - XCR-2027 Exclude Outlier - Not Exclude
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_LEARNING_PIN_LENGTH,
                                                                         pinLengthInMM,
                                                                         true,
                                                                         true));
    }

    // Pull out all "pin length" measurements from the database.
    float[] pinLengthMeasurementValues = subtype.getLearnedJointMeasurementValues(slice,
                                                                                  MeasurementEnum.GULLWING_LEARNING_PIN_LENGTH,
                                                                                  true,
                                                                                  true);

    if (pinLengthMeasurementValues.length > 0)
    {
      // Find the median pin length and set the threshold to that.
      //Ngie Xing - XCR-2027 Exclude Outlier
      float medianPinLength = AlgorithmUtil.medianWithExcludeOutlierDecision(pinLengthMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH, medianPinLength);
    }
  }

/**
 * To calculate the pin length in mm for a joint.
 * 
 * @author Lim, Lay Ngor - XCR1766 - Add pin length measurement
 */
  float getPinLengthInMm(
    float[] derivativePadThicknessProfileUsingFractionalPadLengthAcross,
    int heelPeakBin,
    final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(derivativePadThicknessProfileUsingFractionalPadLengthAcross != null);    
   
    // Get the fractional pad width second derivative profile for the joint in question.
    float[] secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross =
      AlgorithmUtil.createUnitizedDerivativeProfile(derivativePadThicknessProfileUsingFractionalPadLengthAcross,
      2,
      MagnificationEnum.getCurrentNorminal(),
      MeasurementUnitsEnum.MILLIMETERS);
    secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross =
      ProfileUtil.getSmoothedProfile(secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross,
      _SMOOTHING_KERNEL_LENGTH);

    // Search from the heel peak for the first inflection point (second derivative zero crosses).
    int i = heelPeakBin;
    for (; i < (secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross.length - 1); ++i)
    {
      if (i < (secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross.length - 1))
      {
        // See if the second derivative makes a sign change.  This is indicitive that it crossed zero.
        if (Math.signum(secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross[i]) !=
            Math.signum(secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross[i + 1]))
        {
          break;
        }
      }
    }

    // Start from the inflection point and find the minimum second derivative bin.  This will be our toe peak.
    final int toePeakBin = ArrayUtil.minIndex(secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross,
      i,
      secondDerivativePadThicknessProfileUsingFractionalPadLengthAcross.length);

    // Calculate the pin length.
    Assert.expect(heelPeakBin <= toePeakBin);
    final int pinLengthInPixels = toePeakBin - heelPeakBin;
    final float pinLengthInMM = pinLengthInPixels * MILIMETER_PER_PIXEL;
    return pinLengthInMM;
  }  
  
  /**
   * Gets the target toe edge thickness.  For basic Gullwing, this is based on a percentage of nominal toe thickness.
   *
   * @author Matt Wharton
   */
  protected float getTargetToeEdgeThickness(JointInspectionData jointInspectionData,
                                            float[] fractionalWidthPadThicknessProfile)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(fractionalWidthPadThicknessProfile != null);

    Subtype subtype = jointInspectionData.getSubtype();
    final float NOMINAL_TOE_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS);
    final float TARGET_TOE_EDGE_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    float targetToeEdgeThickness = NOMINAL_TOE_THICKNESS * TARGET_TOE_EDGE_FRACTION;

    return targetToeEdgeThickness;
  }

  /**
   * Learns the "toe edge" setting.
   *
   * @author Matt Wharton
   */
  private void learnToeEdgeSetting(Subtype subtype,
                                   SliceNameEnum slice,
                                   Map<Pair<JointInspectionData, ImageSetData>,
                                   GullwingProfileDataForLearning> profileDataMap,
                                   final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Locate the heel edge.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
      final float HEEL_EDGE_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                 (float)HEEL_EDGE_FRACTION_OF_MAX, false);

      // Locate the heel peak.
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                          HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
                                                            (int)heelEdgeSubpixelBin,
                                                            heelPeakSearchDistanceInPixels);

      // Locate the toe peak.
      float[] padThicknessProfileUsingFractionalPadLengthAcross = profileData.getPadThicknessProfileUsingFractionalPadLengthAcross();
      final float PIN_LENGTH_IN_MILLIMETERS =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH);
      int pinLengthInPixels = (int)Math.ceil(PIN_LENGTH_IN_MILLIMETERS / MILIMETER_PER_PIXEL);
      int toePeakBin = AlgorithmFeatureUtil.locateToePeak(padThicknessProfileUsingFractionalPadLengthAcross,
                                                          heelPeakBin,
                                                          pinLengthInPixels);

      // Get the fractional width derivative pad thickness profile.
      float[] derivativePadThicknessProfileUsingFractionalPadLengthAlong = profileData.getDerivativePadThicknessProfileUsingFractionalPadLengthAcross();

      // Search from the toe peak for the minimum derivative value (minimum slope).  This is our toe edge bin.
      int toeEdgeBin = ArrayUtil.minIndex(derivativePadThicknessProfileUsingFractionalPadLengthAlong,
                                          Math.min(toePeakBin + 1,
                                                   derivativePadThicknessProfileUsingFractionalPadLengthAlong.length - 1),
                                          derivativePadThicknessProfileUsingFractionalPadLengthAlong.length);

      // Get the toe edge thickness and figure out what fraction it is of the nominal toe thickness.
      // Make sure that the toe edge thickness we use is at least 10% less than the minimum thickness between the
      // heel and toe peaks.
      float minimumThicknessInMillimetersBetweenHeelAndToePeaks =
          ArrayUtil.min(padThicknessProfileUsingFractionalPadLengthAcross,
                        heelPeakBin,
                        toePeakBin + 1);
      final float MAX_TOE_EDGE_PERCENT_OF_MIN_HEEL_TO_TOE_PEAK_THICKNESS = 0.9f;
      float maximumAllowableToeEdgeThickness =
          minimumThicknessInMillimetersBetweenHeelAndToePeaks * MAX_TOE_EDGE_PERCENT_OF_MIN_HEEL_TO_TOE_PEAK_THICKNESS;
      float toeEdgeThicknessInMillimeters = Math.min(padThicknessProfileUsingFractionalPadLengthAcross[toeEdgeBin],
                                                     maximumAllowableToeEdgeThickness);
      final float NOMINAL_TOE_THICKNESS =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS);
      float toeEdgeThicknessPercentOfNominalToeThickness = (toeEdgeThicknessInMillimeters / NOMINAL_TOE_THICKNESS) * 100.f;

      // Save the "toe edge thickness percent" value to the database.
      //Ngie Xing - XCR-2027 - Exclude Outlier - Not Excluded
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_LEARNING_TOE_EDGE_THICKNESS_PERCENT_OF_NOMINAL_TOE_THICKNESS,
                                                                         toeEdgeThicknessPercentOfNominalToeThickness,
                                                                         true,
                                                                         true));
    }

    // Pull all the "toe edge thickness percent" measurements from the database.
    float[] toeEdgeThicknessPercentValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_LEARNING_TOE_EDGE_THICKNESS_PERCENT_OF_NOMINAL_TOE_THICKNESS,
                                                 true,
                                                 true);

    if (toeEdgeThicknessPercentValues.length > 0)
    {
      // Take the median "toe edge thickness percent" value.
      //Ngie Xing - XCR-2027 Exclude Outlier
      float medianToeEdgeThicknessPercent = AlgorithmUtil.medianWithExcludeOutlierDecision(toeEdgeThicknessPercentValues, false);

      // Make sure we keep the setting between 40% and 65%.
      final float MIN_ALLOWABLE_LEARNED_TOE_EDGE_THICKNESS_PERCENT = 40.f;
      final float MAX_ALLOWABLE_LEARNED_TOE_EDGE_THICKNESS_PERCENT = 65.f;
      float learnedToeEdgeThicknessPercent = Math.max(medianToeEdgeThicknessPercent,
                                                      MIN_ALLOWABLE_LEARNED_TOE_EDGE_THICKNESS_PERCENT);
      learnedToeEdgeThicknessPercent = Math.min(learnedToeEdgeThicknessPercent,
                                                MAX_ALLOWABLE_LEARNED_TOE_EDGE_THICKNESS_PERCENT);

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT,
                              learnedToeEdgeThicknessPercent);
    }
  }

  /**
   * @author Matt Wharton
   */
  protected void learnNominalHeelAndToeThickness(Subtype subtype,
                                                 SliceNameEnum slice,
                                                 Map<Pair<JointInspectionData, ImageSetData>,
                                                 GullwingProfileDataForLearning> profileDataMap,
                                                 final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    InspectionFamilyEnum inspectionFamilyEnum = getInspectionFamilyEnum();

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Locate the heel edge.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
      final float HEEL_EDGE_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                 (float)HEEL_EDGE_FRACTION_OF_MAX, false);

      // Locate the heel peak.
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                           HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
                                                            (int)heelEdgeSubpixelBin,
                                                            heelPeakSearchDistanceInPixels);

      // Take the thickness at the heel peak.
      float[] padThicknessProfileUsingFractionalPadLengthAcross = profileData.getPadThicknessProfileUsingFractionalPadLengthAcross();
      float heelPeakThicknessInMillimeters = padThicknessProfileUsingFractionalPadLengthAcross[heelPeakBin];

      // Save the heel thickness measurement to the database.
      //Ngie Xing - XCR-2027 Exclude Outlier, Not Exclude
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_HEEL_THICKNESS,
                                                                         heelPeakThicknessInMillimeters,
                                                                         true,
                                                                         true));

      if (isToePeakApplicable())
      {
        // Locate the toe peak.
        final float PIN_LENGTH_IN_MILLIMETERS =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH);
        int pinLengthInPixels = (int)Math.ceil(PIN_LENGTH_IN_MILLIMETERS / MILIMETER_PER_PIXEL);
        int toePeakBin = AlgorithmFeatureUtil.locateToePeak(padThicknessProfileUsingFractionalPadLengthAcross,
                                                            heelPeakBin,
                                                            pinLengthInPixels);

        // Take the thickness at the toe peak.
        float toePeakThicknessInMillimeters = padThicknessProfileUsingFractionalPadLengthAcross[toePeakBin];

        // Save the toe thickness measurement to the database.
        subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                           slice,
                                                                           MeasurementEnum.GULLWING_TOE_THICKNESS,
                                                                           toePeakThicknessInMillimeters,
                                                                           true,
                                                                           true));
      }
    }

    // Pull all the "heel thickness" measurements from the database.
    float[] heelThicknessMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_HEEL_THICKNESS,
                                                 true,
                                                 true);
    if (heelThicknessMeasurementValues.length > 0)
    {
      // Take the median "heel thickness" value and set the nominal to that.
      //Ngie Xing - XCR-2027 Exclude Outlier
      float medianHeelThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(heelThicknessMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS, medianHeelThickness);
    }

    if (isToePeakApplicable())
    {
      // Pull all the "toe thickness" measurements from the database.
      float[] toeThicknessMeasurementValues =
          subtype.getLearnedJointMeasurementValues(slice,
                                                   MeasurementEnum.GULLWING_TOE_THICKNESS,
                                                   true,
                                                   true);
      if (toeThicknessMeasurementValues.length > 0)
      {
        // Take the median "toe thickness" value and set the nominal to that.
        //Ngie Xing - XCR-2027 Exclude Outlier
        float medianToeThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(toeThicknessMeasurementValues, true);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS, medianToeThickness);
      }
    }
  }

  /**
   * @author Matt Wharton
   */
  private void learnCenterOffsetSetting(Subtype subtype,
                                        SliceNameEnum slice,
                                        Map<Pair<JointInspectionData, ImageSetData>,
                                        GullwingProfileDataForLearning> profileDataMap,
                                        final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    InspectionFamilyEnum inspectionFamilyEnum = getInspectionFamilyEnum();

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Locate the heel edge.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
      final float HEEL_EDGE_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                 (float)HEEL_EDGE_FRACTION_OF_MAX, false);

      // Locate the heel peak.
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                          HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
                                                            (int)heelEdgeSubpixelBin,
                                                            heelPeakSearchDistanceInPixels);

      // Locate the toe edge.
      float[] padThicknessProfileUsingFractionalPadLengthAcross = profileData.getPadThicknessProfileUsingFractionalPadLengthAcross();
      final float TOE_EDGE_THICKNESS_FRACTION_OF_NOMINAL_TOE_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      final float NOMINAL_TOE_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS);
      float targetToeEdgeThickness = (float)(TOE_EDGE_THICKNESS_FRACTION_OF_NOMINAL_TOE_THICKNESS * NOMINAL_TOE_THICKNESS);
      float toeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(padThicknessProfileUsingFractionalPadLengthAcross,
                                               targetToeEdgeThickness,
                                               heelPeakBin);

      // Now, let's locate the center.
      // We'll search for the minimum thickness value between the heel peak and halfway between the heel and toe edge.
      float heelEdgeToToeEdgeSubpixelDistance = toeEdgeSubpixelBin - heelEdgeSubpixelBin;
      int centerSearchStartBin = Math.min(heelPeakBin + 1, padThicknessProfileUsingFractionalPadLengthAcross.length - 1);
      int centerSearchEndBin = (int)Math.ceil(heelEdgeSubpixelBin + (heelEdgeToToeEdgeSubpixelDistance / 2f)) + 1;
      centerSearchEndBin = Math.min(Math.max(centerSearchEndBin, centerSearchStartBin + 1),
                                    padThicknessProfileUsingFractionalPadLengthAcross.length);
      int centerBin = ArrayUtil.minIndex(padThicknessProfileUsingFractionalPadLengthAcross,
                                         centerSearchStartBin,
                                         centerSearchEndBin);

      float centerOffsetPercent = calculateCenterOffsetPercent(jointInspectionData,
                                                               heelEdgeSubpixelBin,
                                                               heelPeakBin,
                                                               centerBin,
                                                               toeEdgeSubpixelBin,
                                                               MILIMETER_PER_PIXEL);

      // Add the "center offset" measurement to the database.
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_LEARNING_CENTER_OFFSET,
                                                                         centerOffsetPercent,
                                                                         true,
                                                                         true));
    }

    // Pull all the "center offset from heel" measurements from the database.
    float[] centerOffsetFromHeelMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_LEARNING_CENTER_OFFSET,
                                                 true,
                                                 true);

    if (centerOffsetFromHeelMeasurementValues.length > 0)
    {
      // Take the median "center offset from heel" values and set the threshold to that.
      float medianCenterOffsetFromHeel = StatisticsUtil.median(centerOffsetFromHeelMeasurementValues);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET, medianCenterOffsetFromHeel);
    }
  }

  /**
   * Learns the nominal fillet length.
   *
   * @author Matt Wharton
   * @author Poh Kheng
   */
  protected void learnNominalFilletLength(Subtype subtype,
                                          SliceNameEnum slice,
                                          Map<Pair<JointInspectionData, ImageSetData>,
                                          GullwingProfileDataForLearning> profileDataMap,
                                          final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    InspectionFamilyEnum inspectionFamilyEnum = getInspectionFamilyEnum();

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Locate the heel edge.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
      final float HEEL_EDGE_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                 (float)HEEL_EDGE_FRACTION_OF_MAX, false);

      // Locate the heel peak.
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                           HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
                                                            (int)heelEdgeSubpixelBin,
                                                            heelPeakSearchDistanceInPixels);

      // Locate the toe edge.
      float[] padThicknessProfileUsingFractionalPadLengthAcross = profileData.getPadThicknessProfileUsingFractionalPadLengthAcross();
      float targetToeEdgeThickness = getTargetToeEdgeThickness(jointInspectionData,
                                                               padThicknessProfileUsingFractionalPadLengthAcross);
      float toeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(padThicknessProfileUsingFractionalPadLengthAcross,
                                               targetToeEdgeThickness,
                                               heelPeakBin);
      // make sure toe edge is >= heel edge
      if (toeEdgeSubpixelBin < heelEdgeSubpixelBin)
      {
        toeEdgeSubpixelBin = heelEdgeSubpixelBin;
      }

      // Calculate the fillet length (in mm).
      float filletLengthInMillimeters = measureFilletLength(heelEdgeSubpixelBin, toeEdgeSubpixelBin, MILIMETER_PER_PIXEL);

      // Save the "fillet length" measurement to the database.
      //Ngie Xing - XCR-2027 Exclude Outlier, Not Exclude
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_FILLET_LENGTH,
                                                                         filletLengthInMillimeters,
                                                                         true,
                                                                         true));
    }

    // Pull all the "fillet length" measurements from the database.
    float[] filletLengthMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_FILLET_LENGTH,
                                                 true,
                                                 true);

    if (filletLengthMeasurementValues.length > 0)
    {
      // Take the median "fillet length" value and set the nominal to that.
      //Ngie Xing - XCR-2027 Exclude Outlier
      float medianFilletLength = AlgorithmUtil.medianWithExcludeOutlierDecision(filletLengthMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH, medianFilletLength);
    }
  }

  /**
   * Learns the nominal fillet thickness.
   *
   * @author Matt Wharton
   */
  protected void learnNominalFilletThickness(Subtype subtype,
                                             SliceNameEnum slice,
                                             Map<Pair<JointInspectionData, ImageSetData>,
                                             GullwingProfileDataForLearning> profileDataMap,
                                             final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Locate the heel edge.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
      final float HEEL_EDGE_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                 (float)HEEL_EDGE_FRACTION_OF_MAX, false);

      // Calculate the fillet thickness (in mm).
      float[] padThicknessProfileUsingFractionalPadLengthAcross = profileData.getPadThicknessProfileUsingFractionalPadLengthAcross();
      // Measure the average thickness between the heel edge and [heel edge + nominal fillet length] (fillet thickness).
      final float NOMINAL_FILLET_LENGTH_IN_MILLIMETERS =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH);
      int nominalFilletLengthInPixels = (int)Math.ceil(NOMINAL_FILLET_LENGTH_IN_MILLIMETERS / MILIMETER_PER_PIXEL);
      float filletThicknessInMillimeters = measureFilletThickness(padThicknessProfileUsingFractionalPadLengthAcross,
                                                                  (int)heelEdgeSubpixelBin,
                                                                  nominalFilletLengthInPixels);


      // Save the "fillet thickness" measurement to the database.
      //Ngie Xing, XCR-2027 Exclude Outlier, Not Exclude
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_FILLET_THICKNESS,
                                                                         filletThicknessInMillimeters,
                                                                         true,
                                                                         true));
    }

    // Pull all the "fillet thickness" measurements from the database.
    float[] filletThicknessMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_FILLET_THICKNESS,
                                                 true,
                                                 true);

    if (filletThicknessMeasurementValues.length > 0)
    {
      // Take the median "fillet thickness" value and set the nominal to that.
      //Ngie Xing - XCR-2027 Exclude Outlier
      float medianFilletThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(filletThicknessMeasurementValues, true);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS, medianFilletThickness);
    }
  }

  /**
   * @author Matt Wharton
   */
  protected float calculateCenterOffsetPercent(JointInspectionData jointInspectionData,
                                               float heelEdgeSubpixelBin,
                                               int heelPeakBin,
                                               int centerBin,
                                               float toeEdgeSubpixelBin,
                                               final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(jointInspectionData != null);

    // Calculate the center offset from heel peak percent of pin length.
    Subtype subtype = jointInspectionData.getSubtype();
    final float PIN_LENGTH_IN_MILLIMETERS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH);
    float pinLengthInPixels = PIN_LENGTH_IN_MILLIMETERS / MILIMETER_PER_PIXEL;
    float centerOffsetPercent = ((float)(centerBin - heelPeakBin) / pinLengthInPixels) * 100.f;

    return centerOffsetPercent;
  }

  /**
   * Measures the center:heel thickness percent for learning purposes.
   *
   * @author Matt Wharton
   */
  protected void measureCenterToHeelThicknessPercentForLearning(Subtype subtype,
                                                                SliceNameEnum slice,
                                                                Map<Pair<JointInspectionData, ImageSetData>,
                                                                GullwingProfileDataForLearning> profileDataMap,
                                                                final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    InspectionFamilyEnum inspectionFamilyEnum = getInspectionFamilyEnum();

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileData = mapEntry.getValue();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      // Locate the heel edge.
      float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
      final float HEEL_EDGE_FRACTION_OF_MAX =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
      float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                 (float)HEEL_EDGE_FRACTION_OF_MAX, false);

      // Locate the heel peak.
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
      final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                          HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
                                                            (int)heelEdgeSubpixelBin,
                                                            heelPeakSearchDistanceInPixels);

      // Locate the toe edge.
      float targetToeEdgeThickness = getTargetToeEdgeThickness(jointInspectionData,
                                                               padThicknessProfileUsingFullPadLengthAcross);
      float toeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                               targetToeEdgeThickness,
                                               heelPeakBin);

      // Locate the center.
      float[] padThicknessProfileUsingFractionalPadLengthAcross = profileData.getPadThicknessProfileUsingFractionalPadLengthAcross();
      int centerBin = locateCenter(jointInspectionData,
                                   padThicknessProfileUsingFractionalPadLengthAcross,
                                   heelEdgeSubpixelBin,
                                   heelPeakBin,
                                   toeEdgeSubpixelBin,
                                   MILIMETER_PER_PIXEL);

      // Calculate the center:heel thickness percent.
      float heelPeakThickness = padThicknessProfileUsingFractionalPadLengthAcross[heelPeakBin];
      float centerThickness = padThicknessProfileUsingFractionalPadLengthAcross[centerBin];
      float centerToHeelThicknessPercent = (centerThickness / heelPeakThickness) * 100.f;

      // Save the "center:heel thickness percent" measurement to the database.
      //Ngie Xing - XCR-2027 Exclude Outlier, Not Exclude 
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         slice,
                                                                         MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT,
                                                                         centerToHeelThicknessPercent,
                                                                         true,
                                                                         true));
    }
  }

  /**
   * Measures the pad thickness profiles for learning.
   *
   * @author Matt Wharton
   */
  protected Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning>
      measurePadThicknessProfilesForLearning(Subtype subtype,
                                             ManagedOfflineImageSet imageSet,
                                             SliceNameEnum slice) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(imageSet != null);
    Assert.expect(slice != null);

    Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> padProfileDataMap =
        new HashMap<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning>();
    final float PAD_PROFILE_WIDTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH) / 100.f;
    final float PAD_PROFILE_LENGTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH) / 100.f;
    boolean useHeelVoidCompensation = isHeelVoidCompensationEnabled(subtype);

    ManagedOfflineImageSetIterator imagesIterator = imageSet.iterator();
    int samplingFrequency = Math.max(1, imageSet.size() / _MAX_NUMBER_OF_REGIONS_FOR_LEARNING);
    imagesIterator.setSamplingFrequency(samplingFrequency);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      // Get the image for the relevant slice.
      ImageSetData currentImageSetData = imagesIterator.getCurrentImageSetData();
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(slice);
      Image sliceImage = reconstructedSlice.getOrthogonalImage();

      // Get all the joints in the region which are of the applicable subtype.
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      // Run Locator.
      Locator.locateJoints(reconstructedImages, slice, jointInspectionDataObjects, this, true);

      // Measure the profiles for each joint in the region.
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // Measure the fractional pad length across profile.
        Pair<RegionOfInterest, float[]> fractionalWidthPadProfileRoiAndThicknessProfile =
            measureSmoothedPadThicknessProfile(sliceImage,
                                               slice,
                                               jointInspectionData,
                                               PAD_PROFILE_WIDTH_FRACTION,
                                               PAD_PROFILE_LENGTH_FRACTION,
                                               false);
        RegionOfInterest fractionalWidthPadProfileRoi = fractionalWidthPadProfileRoiAndThicknessProfile.getFirst();
        float[] fractionalWidthPadThicknessProfile = fractionalWidthPadProfileRoiAndThicknessProfile.getSecond();

        // Measure the full pad length across profile.  If void compensation is disabled, just use the fractional
        // profile.
        RegionOfInterest fullWidthPadProfileRoi = null;
        float[] fullWidthPadThicknessProfile = null;
        if (useHeelVoidCompensation)
        {
          Pair<RegionOfInterest, float[]> fullWidthPadProfileRoiAndThicknessProfile =
              measureSmoothedPadThicknessProfile(sliceImage,
                                                 slice,
                                                 jointInspectionData,
                                                 1f,
                                                 PAD_PROFILE_LENGTH_FRACTION,
                                                 false);
          fullWidthPadProfileRoi = fullWidthPadProfileRoiAndThicknessProfile.getFirst();
          fullWidthPadThicknessProfile = fullWidthPadProfileRoiAndThicknessProfile.getSecond();
        }
        else
        {
          fullWidthPadProfileRoi = fractionalWidthPadProfileRoi;
          fullWidthPadThicknessProfile = new float[fractionalWidthPadThicknessProfile.length];
          System.arraycopy(fractionalWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile, 0, fullWidthPadThicknessProfile.length);
        }
            
        //Lim, Lay Ngor - XCR1766 - synchronise inspection code with learning code - START
        // Determine voiding compensation method
        final boolean useGaussianVoidCompensationMethod = isGaussianVoidCompensationMethodEnabled(subtype);       
        // Smooth solder profile thickness by using Gaussian method
        if (useGaussianVoidCompensationMethod)
        {
          int smoothSensitivity = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY);
          smoothSensitivity = Math.min(smoothSensitivity, sliceImage.getWidth());
          smoothSensitivity = Math.min(smoothSensitivity, sliceImage.getHeight());

          fullWidthPadThicknessProfile = ProfileUtil.getGaussainSmoothedProfile(fullWidthPadThicknessProfile, smoothSensitivity);
          fractionalWidthPadThicknessProfile = ProfileUtil.getGaussainSmoothedProfile(fractionalWidthPadThicknessProfile, smoothSensitivity);
        }
        //Lim, Lay Ngor - XCR1766 - synchronise inspection code with learning code - END

        // Flip the profiles if needed.
        JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
        Assert.expect(_jointTypeToProfileReversalRequiredMap.containsKey(jointTypeEnum));
        boolean profileReversalRequired = _jointTypeToProfileReversalRequiredMap.get(jointTypeEnum);
        if (profileReversalRequired)
        {
          ProfileUtil.reverseProfile(fullWidthPadThicknessProfile);
          ProfileUtil.reverseProfile(fractionalWidthPadThicknessProfile);
        }

        // Take the derivative profiles.
        float[] fullWidthDerivativePadThicknessProfile =
            AlgorithmUtil.createUnitizedDerivativeProfile(fullWidthPadThicknessProfile,
                                                          2,
                                                          MagnificationEnum.getCurrentNorminal(),
                                                          MeasurementUnitsEnum.MILLIMETERS);
        fullWidthDerivativePadThicknessProfile = ProfileUtil.getSmoothedProfile(fullWidthDerivativePadThicknessProfile,
                                                                                _SMOOTHING_KERNEL_LENGTH);
        float[] fractionalWidthDerivativePadThicknessProfile =
            AlgorithmUtil.createUnitizedDerivativeProfile(fractionalWidthPadThicknessProfile,
                                                          2,
                                                          MagnificationEnum.getCurrentNorminal(),
                                                          MeasurementUnitsEnum.MILLIMETERS);
        fractionalWidthDerivativePadThicknessProfile = ProfileUtil.getSmoothedProfile(fractionalWidthDerivativePadThicknessProfile,
                                                                                      _SMOOTHING_KERNEL_LENGTH);

        // Add the measured profile data to our map.
        GullwingProfileDataForLearning profileDataForLearning =
            new GullwingProfileDataForLearning(fullWidthPadProfileRoi,
                                               fullWidthPadThicknessProfile,
                                               fullWidthDerivativePadThicknessProfile,
                                               fractionalWidthPadProfileRoi,
                                               fractionalWidthPadThicknessProfile,
                                               fractionalWidthDerivativePadThicknessProfile);
        padProfileDataMap.put(new Pair<JointInspectionData, ImageSetData>(jointInspectionData, currentImageSetData),
                              profileDataForLearning);
      }

      imagesIterator.finishedWithCurrentRegion();
      
      // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
      if (jointInspectionDataObjects != null)
      {
        jointInspectionDataObjects.clear();
        jointInspectionDataObjects = null;
      }
    }

    Assert.expect(padProfileDataMap != null);
    return padProfileDataMap;
  }

  /**
   * Restores all learned Gullwing Measurement settings back to their defaults.
   *
   * @author Matt Wharton
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    /** @todo mdw - not sure if we want to necessarily reset these when learned data is deleted. */
    // Delete the Locator learning.
    Locator.setLearnedSettingsToDefaults(subtype);

    // Reset the learned settings back to their default values.
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION);
  }
  
  /**
   * XCR-3532
   * @author Siew Yeng
   */  
  protected void computeRegionOutlierMeasurementsForFilletLength(Collection<JointInspectionData> jointInspectionDataObjects,
                                                                 float[] validFilletLength,
                                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(validFilletLength != null);
    Assert.expect(sliceNameEnum != null);
    
    float expectedFilletLength = 0.0f;

    if (validFilletLength.length > 0)
    {
      expectedFilletLength = StatisticsUtil.median(validFilletLength);
    }
    
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      float filletLength = MathUtil.convertMillimetersToMils(jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                                   MeasurementEnum.GULLWING_FILLET_LENGTH).getValue());

      float regionOutlierPercent = 0.0f;

      //Siew Yeng - XCR-3358 - Infinity and unreadable measurement value is displayed
      if (validFilletLength.length > 0 && expectedFilletLength > 0)
      {
        regionOutlierPercent = 100.0f * filletLength / expectedFilletLength;
      }
      
      JointMeasurement filletLengthRegionOutlierMeasurement = new JointMeasurement(this,
                                                                MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER,
                                                                MeasurementUnitsEnum.PERCENT,
                                                                jointInspectionData.getPad(),
                                                                sliceNameEnum,
                                                                regionOutlierPercent);
              
      jointInspectionResult.addMeasurement(filletLengthRegionOutlierMeasurement);  
    }
  }
  
  /**
   * XCR-3532
   * @author Siew Yeng
   */
  protected void computeRegionOutlierMeasurementsForCenterThickness(Collection<JointInspectionData> jointInspectionDataObjects,
                                                                    float[] validCenterThickness,
                                                                    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(validCenterThickness != null);
    Assert.expect(sliceNameEnum != null);
    
    float expectedCenterThickness = 0.0f;

    if (validCenterThickness.length > 0)
    {
      expectedCenterThickness = StatisticsUtil.median(validCenterThickness);
    }
    
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      float centerThickness = MathUtil.convertMillimetersToMils(jointInspectionResult.getJointMeasurement(sliceNameEnum,
                                                                                                   MeasurementEnum.GULLWING_CENTER_THICKNESS).getValue());

      float regionOutlierPercent = 0.0f;

      //Siew Yeng - XCR-3358 - Infinity and unreadable measurement value is displayed
      if (validCenterThickness.length > 0 && expectedCenterThickness > 0)
      {
        regionOutlierPercent = 100.0f * centerThickness / expectedCenterThickness;
      }
      
      JointMeasurement centerThicknessRegionOutlierMeasurement = new JointMeasurement(this,
                                                                MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER,
                                                                MeasurementUnitsEnum.PERCENT,
                                                                jointInspectionData.getPad(),
                                                                sliceNameEnum,
                                                                regionOutlierPercent);
      jointInspectionResult.addMeasurement(centerThicknessRegionOutlierMeasurement);
    }
  }
}
