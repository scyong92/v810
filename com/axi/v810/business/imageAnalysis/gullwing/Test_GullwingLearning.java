package com.axi.v810.business.imageAnalysis.gullwing;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;

public class Test_GullwingLearning extends AlgorithmUnitTest
{
  private List<AlgorithmSettingEnum> _learnedSettings = new LinkedList<AlgorithmSettingEnum>();
  private Map<AlgorithmSettingEnum, Float> _learnedSettingToExpectedLearnedValueMap =
      new HashMap<AlgorithmSettingEnum, Float>();

  private static final String _TEST_PROJECT_NAME = "FAMILIES_ALL_RLV";
  private static final String _GULLWING_SUBTYPE_NAME = "so00001_Gullwing";

  /**
   * @author Matt Wharton
   */
  private Test_GullwingLearning()
  {
    // Do nothing...
  }

  /**
   * Initializes the "learned settings" data structures for the test.
   *
   * @author Matt Wharton
   */
  private void initializeLearnedSettingsDataStructures()
  {
    Assert.expect(_learnedSettings != null);
    Assert.expect(_learnedSettingToExpectedLearnedValueMap != null);

    _learnedSettings.clear();
    InspectionFamily gullwingInspectionFamily = InspectionFamily.getInstance(InspectionFamilyEnum.GULLWING);
    for (Algorithm algorithm : gullwingInspectionFamily.getAlgorithms())
    {
      _learnedSettings.addAll(algorithm.getLearnedAlgorithmSettingEnums());
    }

    _learnedSettingToExpectedLearnedValueMap.clear();
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH, 0.8349f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, 23.59f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS, 0.0648f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS, 0.0531f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS, 0.0631f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, 68.76f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_PIN_LENGTH, 0.24f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET, 6.67f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT, 109.62f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 108.61f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 91.4f);
  }

  /**
   * Creates a black image set for the specified project and subtype and runs learning against it.
   *
   * @author Matt Wharton
   */
  private void testLearningOnBlackImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the black image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData blackImageSetData = createImageSetAndSetProgramFilters(project,
                                                                        subtype,
                                                                        "testGullwingLearningOnBlackImagesImageSet",
                                                                        POPULATED_BOARD);

    generateBlackImageSet(project, blackImageSetData);

    // Run subtype learning.
    imageSetDataList.add(blackImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a white image set for the specified project and subtype and runs learning against it.
   *
   * @author Matt Wharton
   */
  private void testLearningOnWhiteImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the white image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData whiteImageSetData = createImageSetAndSetProgramFilters(project,
                                                                        subtype,
                                                                        "testGullwingLearningOnWhiteImagesImageSet",
                                                                        POPULATED_BOARD);
    generateWhiteImageSet(project, whiteImageSetData);

    // Run subtype learning.
    imageSetDataList.add(whiteImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a pseudo-random image set for the specified project and subtype and runs learning against it.
   *
   * @author Matt Wharton
   */
  private void testLearningOnPseudoRandomImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the pseudo random image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData pseudoRandomImageSetData = createImageSetAndSetProgramFilters(project,
                                                                               subtype,
                                                                               "testGullwingLearningOnPseudoRandomImagesImageSet",
                                                                               POPULATED_BOARD);
    generateRandomImageSet(project, pseudoRandomImageSetData);

    // Run subtype learning.
    imageSetDataList.add(pseudoRandomImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Main test entry point for Gullwing Learning.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    // Initialize the learned settings data structures.
    initializeLearnedSettingsDataStructures();

    // Load up the test project (FAMILIES_ALL_RLV).
    Project project = getProject(_TEST_PROJECT_NAME);

    // Lookup our test Subtype.
    Panel panel = project.getPanel();
    Subtype subtype = panel.getSubtype(_GULLWING_SUBTYPE_NAME);

    try
    {
      // Delete any pre-existing learned data.
      deleteLearnedData(subtype);

      // Check that the learned settings are at their default values.
      verifyAllSettingsAreAtDefault(subtype, _learnedSettings);

      // Learn one of the Gullwing subtypes.
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                     POPULATED_BOARD);
      imageSetDataList.add(imageSetData);
      learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

      /** @todo: mdw - add this back in once we get some better test images.  At present Locator position
       * can shift by a pixel or so (probably due to memory alignment or some IPP optimization).
       * Since these images have flat backgronds, being off by even one pixel can throw off measurements quite
       * a bit.
       */
      // Verify that the learned values match what we expect.
      // checkLearnedValues(subtype, _learnedSettingToExpectedLearnedValueMap);

      // Check the number of datapoints.
      SliceNameEnum padSlice = SliceNameEnum.PAD;
      final int EXPECTED_NUMBER_OF_DATA_POINTS = 140;
      int actualNumberOfDataPoints = getNumberOfMeasurements(subtype, padSlice, MeasurementEnum.GULLWING_HEEL_THICKNESS);
      Expect.expect(actualNumberOfDataPoints == EXPECTED_NUMBER_OF_DATA_POINTS,
                    "Actual # data points: " + actualNumberOfDataPoints);

      // Test learning on black images.
      testLearningOnBlackImages(project, subtype);

      // Test learning on white images.
      testLearningOnWhiteImages(project, subtype);

      // Test learning on pseudo-random images.
      testLearningOnPseudoRandomImages(project, subtype);

      // Make sure that we can run learning on an unpopulated board.
      testLearningOnlyUnloadedBoards(project, subtype, _learnedSettings);

      // Delete the learned data generated during this test.
      deleteLearnedData(subtype);
    }
    catch (XrayTesterException xtex)
    {
      xtex.printStackTrace();
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_GullwingLearning());
  }
}
