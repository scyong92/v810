package com.axi.v810.business.imageAnalysis.gullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Gullwing open algorithm.
 *
 * @author Matt Wharton
 */
public class GullwingOpenAlgorithm extends Algorithm
{
  /**
   * @author Matt Wharton
   */
  public GullwingOpenAlgorithm(InspectionFamily gullwingInspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.GULLWING);

    Assert.expect(gullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(gullwingInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @author Matt Wharton
   */
  protected GullwingOpenAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.OPEN, inspectionFamilyEnum);
  }

  /**
   * @author Peter Esbensen
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
  }

  /**
   * Adds the algorithm settings for Gullwing Open.
   *
   * @author Matt Wharton
   */
  private void addAlgorithmSettings(InspectionFamily gullwingInspectionFamily)
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Minimum heel thickness (as percent of nominal).
    AlgorithmSetting minimumHeelThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        60.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GULLWING_OPEN_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_OPEN_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_GULLWING_OPEN_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  minimumHeelThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  minimumHeelThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  minimumHeelThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minimumHeelThicknessPercentOfNominalSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL);

    // Minimum fillet length (as percent of nominal).
    AlgorithmSetting minimumFilletLengthPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  minimumFilletLengthPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  minimumFilletLengthPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  minimumFilletLengthPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minimumFilletLengthPercentOfNominalSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);

    // Maximum fillet length (as percent of nominal).
    AlgorithmSetting maximumFilletLengthPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
        displayOrder++,
        150.0f, // default
        100.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  maximumFilletLengthPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  maximumFilletLengthPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  maximumFilletLengthPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(maximumFilletLengthPercentOfNominalSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);

    // Maximum center:heel thickness percent.
    AlgorithmSetting maxCenterToHeelThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT,
        displayOrder++,
        85.f, // default
        0.0f, // min
        200f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
        "HTML_DESC_GULLWING_OPEN_(MAX_CENTER_TO_HEEL_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_OPEN_(MAX_CENTER_TO_HEEL_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_GULLWING_OPEN_(MAX_CENTER_TO_HEEL_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  maxCenterToHeelThicknessPercentSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  maxCenterToHeelThicknessPercentSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  maxCenterToHeelThicknessPercentSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);
    addAlgorithmSetting(maxCenterToHeelThicknessPercentSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT);

    //Siew Yeng - XCR-3532 Fillet Length Outlier
    AlgorithmSetting maxFilletLengthRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER,
      displayOrder++,
      300.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                          maxFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                          maxFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                          maxFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    addAlgorithmSetting(maxFilletLengthRegionOutlierSetting);  
    
    
    AlgorithmSetting minFilletLengthRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER,
      displayOrder++,
      0.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                          minFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                          minFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                          minFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    addAlgorithmSetting(minFilletLengthRegionOutlierSetting);
    
    
     AlgorithmSetting maxCenterThicknessRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER,
      displayOrder++,
      300.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GULLWING_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_GULLWING_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                          maxCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                          maxCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                          maxCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    addAlgorithmSetting(maxCenterThicknessRegionOutlierSetting);
    
    AlgorithmSetting minCenterThicknessRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER,
      displayOrder++,
      0.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_GULLWING_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_GULLWING_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_GULLWING_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                          minCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                          minCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                          minCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    addAlgorithmSetting(minCenterThicknessRegionOutlierSetting);
  }

  /**
   * Runs 'Open' on all the given joints.
   *
   * @author Matt Wharton
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

      BooleanRef jointPassed = new BooleanRef(true);

      // Run the basic Gullwing Open classification.
      basicGullwingClassifyJoint(subtype,
                                 padSlice,
                                 jointInspectionData,
                                 jointPassed);
      
      //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
      // Check for opens based on fillet length region outlier
      detectOpensUsingFilletLengthRegionOutlier(jointInspectionData,
                                                padSlice, 
                                                (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER),
                                                (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER),
                                                jointPassed);
    
      // Check for opens based on center thickness region outlier
      detectOpensUsingCenterThicknessRegionOutlier(jointInspectionData, 
                                                    padSlice,
                                                   (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER),
                                                   (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER),
                                                    jointPassed);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }
  
  /**
   * Does the 'basic' Gullwing Open classification on the specified joint.
   *
   * @author Matt Wharton
   */
  protected void basicGullwingClassifyJoint(Subtype subtype,
                                            SliceNameEnum sliceNameEnum,
                                            JointInspectionData jointInspectionData,
                                            BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Detect opens by looking for insufficient heel.
    detectOpensDueToInsufficientHeel(subtype, sliceNameEnum, jointInspectionData, jointPassed);

    // Detect opens using the fillet length.
    detectOpensUsingFilletLength(subtype, sliceNameEnum, jointInspectionData, jointPassed);

    // Detect opens using the center:heel thickness percent.
    detectOpensUsingCenterToHeelThicknessPercent(subtype, sliceNameEnum, jointInspectionData, jointPassed);
  }

  /**
   * Detect opens due to insufficient heel.
   *
   * @author Matt Wharton
   */
  private void detectOpensDueToInsufficientHeel(Subtype subtype,
                                                SliceNameEnum sliceNameEnum,
                                                JointInspectionData jointInspectionData,
                                                BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the heel thickness is within acceptable limits.
    final float NOMINAL_HEEL_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);
    final float MIMIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    JointMeasurement heelThicknessMeasurement =
      GullwingMeasurementAlgorithm.getHeelThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float heelThickness = heelThicknessMeasurement.getValue();

    float heelThicknessPercentOfNominal = (heelThickness / NOMINAL_HEEL_THICKNESS) * 100f;
    JointMeasurement heelThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             heelThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(heelThicknessPercentOfNominalMeasurement);

    if (MathUtil.fuzzyLessThan(heelThicknessPercentOfNominal, MIMIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      heelThicknessPercentOfNominalMeasurement,
                                                      MIMIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(heelThicknessPercentOfNominalMeasurement);
      openIndictment.addRelatedMeasurement(heelThicknessMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      heelThicknessPercentOfNominalMeasurement,
                                                      MIMIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    }
  }

  /**
   * Detects opens using the fillet length.
   *
   * @author Matt Wharton
   */
  private void detectOpensUsingFilletLength(Subtype subtype,
                                            SliceNameEnum sliceNameEnum,
                                            JointInspectionData jointInspectionData,
                                            BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    boolean filletLengthBelowMinimum = false;
    boolean filletLengthAboveMaximum = false;

    // Check to see if the fillet length is within acceptable limits.
    final float NOMINAL_FILLET_LENGTH =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH);
    final float MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    final float MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    JointMeasurement filletLengthMeasurement =
        GullwingMeasurementAlgorithm.getFilletLengthMeasurement(jointInspectionData, sliceNameEnum);
    float filletLength = filletLengthMeasurement.getValue();
    Assert.expect(MathUtil.fuzzyGreaterThanOrEquals(filletLength, 0f));

    float filletLengthPercentOfNominal = (filletLength / NOMINAL_FILLET_LENGTH) * 100f;
    JointMeasurement filletLengthPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             filletLengthPercentOfNominal);
    jointInspectionResult.addMeasurement(filletLengthPercentOfNominalMeasurement);

    if (MathUtil.fuzzyLessThan(filletLengthPercentOfNominal, MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL))
    {
      // Fillet length is below the allowable minimum.
      filletLengthBelowMinimum = true;
    }
    else if (MathUtil.fuzzyGreaterThan(filletLengthPercentOfNominal, MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL))
    {
      // Fillet length is above the allowable maximum.
      filletLengthAboveMaximum = true;
    }


    // Did the joint fail the fillet length check?
    if (filletLengthBelowMinimum || filletLengthAboveMaximum)
    {
      // Create a new indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Tie the applicable measurements to the indictment.
      openIndictment.addFailingMeasurement(filletLengthPercentOfNominalMeasurement);
      openIndictment.addRelatedMeasurement(filletLengthMeasurement);

      if (filletLengthBelowMinimum)
      {
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        jointInspectionData.getInspectionRegion(),
                                                        sliceNameEnum,
                                                        filletLengthPercentOfNominalMeasurement,
                                                        MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
      }
      else if (filletLengthAboveMaximum)
      {
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        jointInspectionData.getInspectionRegion(),
                                                        sliceNameEnum,
                                                        filletLengthPercentOfNominalMeasurement,
                                                        MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
      }

      // Add the indictment to the JointInspectionResult.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletLengthPercentOfNominalMeasurement,
                                                      MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    }
  }

  /**
   * Detects opens using the center:heel thickness percent.
   *
   * @author Matt Wharton
   */
  private void detectOpensUsingCenterToHeelThicknessPercent(Subtype subtype,
                                                            SliceNameEnum sliceNameEnum,
                                                            JointInspectionData jointInspectionData,
                                                            BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Check to see if the center:heel thickness percent is within acceptable limits.
    final float MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT);
    JointMeasurement centerToHeelThicknessPercentMeasurement =
        GullwingMeasurementAlgorithm.getCenterToHeelThicknessPercentMeasurement(jointInspectionData, sliceNameEnum);
    float centerToHeelThicknessPercent = centerToHeelThicknessPercentMeasurement.getValue();
    if (MathUtil.fuzzyGreaterThan(centerToHeelThicknessPercent, MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT))
    {
      // Center:heel thickness ratio is outside of acceptable limits!
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerToHeelThicknessPercentMeasurement,
                                                      MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT);

      // Create a new indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Tie the applicable measurements to the indictment.
      openIndictment.addFailingMeasurement(centerToHeelThicknessPercentMeasurement);
      openIndictment.addRelatedMeasurement(GullwingMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, sliceNameEnum));
      openIndictment.addRelatedMeasurement(GullwingMeasurementAlgorithm.getHeelThicknessMeasurement(jointInspectionData, sliceNameEnum));

      // Add the indictment to the JointInspectionResult.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerToHeelThicknessPercentMeasurement,
                                                      MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT);
    }
  }

  /**
   * Learns the defect settings for Gullwing Open.
   *
   * @author Matt Wharton
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    /*** MDW - as per Kathy, I'm not going to learn defect thresholds in the 1.0 release. */
    /**
    // We can't learn if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      return;
    }

    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Learn the basic Gullwing Open settings.
    learnBasicGullwingSettings(subtype, padSlice);
*/
  }

  /**
   * Learns the basic Gullwing Open settings.
   *
   * @author Matt Wharton
   */
  protected void learnBasicGullwingSettings(Subtype subtype, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Learn the "fillet length" defect settings.
    learnFilletLengthDefectSettings(subtype, sliceNameEnum);

    // Learn the "center:heel thickness ratio" defect setting.
    learnCenterToHeelThicknessPercentDefectSetting(subtype, sliceNameEnum);
  }

  /**
   * Learns the "fillet length" defect settings.
   *
   * @author Matt Wharton
   */
  private void learnFilletLengthDefectSettings(Subtype subtype, SliceNameEnum slice) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all the fillet length measurements from the database.
    float[] filletLengthLearnedMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_FILLET_LENGTH,
                                                 true,
                                                 true);

    final float NOMINAL_FILLET_LENGTH =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH);

    // We can only do the IQR statistics if we have at least two data points.
    if (filletLengthLearnedMeasurementValues.length > 1)
    {
      // Take the "five number summary" of the data and set the "mimimum fillet thickness percent of nominal"
      // and "maximum fillet thickness percent of nominal" defect settings to be 3 IQRs less than
      // and greater than the median.
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(filletLengthLearnedMeasurementValues);
      float medianFilletLengthPercentOfNominal = (quartileRanges[2] / NOMINAL_FILLET_LENGTH) * 100f;
      float interQuartileRange = ((quartileRanges[3] - quartileRanges[1]) / NOMINAL_FILLET_LENGTH) * 100f;

      float minimumFilletLengthPercentOfNominal = medianFilletLengthPercentOfNominal - (3f * interQuartileRange);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
                              minimumFilletLengthPercentOfNominal);
      float maximumFilletLengthPercentOfNominal = medianFilletLengthPercentOfNominal + (3f * interQuartileRange);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
                              maximumFilletLengthPercentOfNominal);
    }
  }

  /**
   * Learns the "center:heel thickness percent" defect setting.
   *
   * @author Matt Wharton
   */
  private void learnCenterToHeelThicknessPercentDefectSetting(Subtype subtype, SliceNameEnum slice) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all of the "center:heel thickness ratio" measurements from the database.
    float[] centerToHeelThicknessPercentValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (centerToHeelThicknessPercentValues.length > 1)
    {
      // Take the "five number summary" of the data and set the "maximum center:heel thickness ratio"
      // setting to 3 IQRs above the median.
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(centerToHeelThicknessPercentValues);
      float medianCenterToHeelThicknessPercent = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      float maximumCenterToHeelThicknessPercent = medianCenterToHeelThicknessPercent + (3f * interQuartileRange);

      // Make sure the the maximum center:heel thickness isn't set above 85%.
      final float MAX_ALLOWABLE_LEARNED_MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT = 85.f;
      float learnedMaximumCenterToHeelThicknessPercent = Math.min(maximumCenterToHeelThicknessPercent,
                                                                  MAX_ALLOWABLE_LEARNED_MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT);

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT,
                              learnedMaximumCenterToHeelThicknessPercent);
    }
  }

  /**
   * Resets all learned defect settings back to their default values.
   *
   * @author Matt Wharton
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
  }
  
  /**
   * XCR-3532
   * @author Siew Yeng
   */
  protected void detectOpensUsingFilletLengthRegionOutlier(JointInspectionData jointInspectionData, 
                                                           SliceNameEnum sliceNameEnum,
                                                           float minimumAllowableRegionOutlierPercent,
                                                           float maximumAllowableRegionOutlierPercent,
                                                           BooleanRef jointPassed)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    
    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the "Fillet Length Region Outlier" measurement.
    JointMeasurement filletLengthRegionOutlierMeasurement = 
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);

    float filletLengthRegionOutlier = filletLengthRegionOutlierMeasurement.getValue();
    // Check if the "Fillet Length Region Outlier" is within acceptable limits.
    if (MathUtil.fuzzyGreaterThan(filletLengthRegionOutlier, maximumAllowableRegionOutlierPercent))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletLengthRegionOutlierMeasurement,
                                                      maximumAllowableRegionOutlierPercent);

      // Create an Open indictment
      JointIndictment filletLengthRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'Fillet Length Region Outlier' measurement to this joint.
      filletLengthRegionOutlierOpenIndictment.addFailingMeasurement(filletLengthRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(filletLengthRegionOutlierOpenIndictment);
      
      // Mark the joint as failing.
      jointPassed.setValue(false);
      
      return;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletLengthRegionOutlierMeasurement,
                                                      maximumAllowableRegionOutlierPercent);
    }
    
    if(MathUtil.fuzzyLessThan(filletLengthRegionOutlier, minimumAllowableRegionOutlierPercent))
    {
       // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletLengthRegionOutlierMeasurement,
                                                      minimumAllowableRegionOutlierPercent);

      // Create an Open indictment
      JointIndictment filletLengthRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'Fillet Length Region Outlier' measurement to this joint.
      filletLengthRegionOutlierOpenIndictment.addFailingMeasurement(filletLengthRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(filletLengthRegionOutlierOpenIndictment);
      
      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletLengthRegionOutlierMeasurement,
                                                      minimumAllowableRegionOutlierPercent);
    }
  }
  
  /**
   * XCR-3532
   * @author Siew Yeng
   */
  protected void detectOpensUsingCenterThicknessRegionOutlier(JointInspectionData jointInspectionData, 
                                                              SliceNameEnum sliceNameEnum,
                                                              float minimumAllowableRegionOutlierPercent,
                                                              float maximumAllowableRegionOutlierPercent,
                                                              BooleanRef jointPassed)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    
    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the "center thickness region outlier" measurement.
    JointMeasurement centerThicknessRegionOutlierMeasurement = 
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);

    float centerThicknessRegionOutlier = centerThicknessRegionOutlierMeasurement.getValue();
    
    if (MathUtil.fuzzyGreaterThan(centerThicknessRegionOutlier, maximumAllowableRegionOutlierPercent))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerThicknessRegionOutlierMeasurement,
                                                      maximumAllowableRegionOutlierPercent);

      // Create an Open indictment
      JointIndictment centerThicknessRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'center thickness region outlier' measurement to this joint.
      centerThicknessRegionOutlierOpenIndictment.addFailingMeasurement(centerThicknessRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(centerThicknessRegionOutlierOpenIndictment);
      
      // Mark the joint as failing.
      jointPassed.setValue(false);
      
      return;
    }
    else
    {
       AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerThicknessRegionOutlierMeasurement,
                                                      maximumAllowableRegionOutlierPercent);
    }
    
    if (MathUtil.fuzzyLessThan(centerThicknessRegionOutlier, minimumAllowableRegionOutlierPercent))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerThicknessRegionOutlierMeasurement,
                                                      minimumAllowableRegionOutlierPercent);

      // Create an Open indictment
      JointIndictment centerThicknessRegionOutlierOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'center thickness region outlier' measurement to this joint.
      centerThicknessRegionOutlierOpenIndictment.addFailingMeasurement(centerThicknessRegionOutlierMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(centerThicknessRegionOutlierOpenIndictment);
      
      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerThicknessRegionOutlierMeasurement,
                                                      minimumAllowableRegionOutlierPercent);
    }
  }
}
