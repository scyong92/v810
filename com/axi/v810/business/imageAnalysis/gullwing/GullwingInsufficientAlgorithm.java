package com.axi.v810.business.imageAnalysis.gullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Gullwing insufficient algorithm.
 *
 * @author Matt Wharton
 */
public class GullwingInsufficientAlgorithm extends Algorithm
{
  /**
   * @author Matt Wharton
   */
  public GullwingInsufficientAlgorithm(InspectionFamily gullwingInspectionFamily)
  {
    super(AlgorithmEnum.INSUFFICIENT, InspectionFamilyEnum.GULLWING);

    Assert.expect(gullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(gullwingInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @author Peter Esbensen
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
  }

  /**
   * @author Matt Wharton
   */
  protected GullwingInsufficientAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.INSUFFICIENT, inspectionFamilyEnum);
  }

  /**
   * Adds the algorithm settings for the Gullwing Insufficient algorithm.
   *
   * @author Matt Wharton
   */
  private void addAlgorithmSettings(InspectionFamily gullwingInspectionFamily)
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Minimum thickness percent of nominal.
    AlgorithmSetting minThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        60.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GULLWING_INSUFFICIENT_(MIN_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_INSUFFICIENT_(MIN_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_GULLWING_INSUFFICIENT_(MIN_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.GULLWING,
                                                                  minThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.JLEAD,
                                                                  minThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LEADLESS,
                                                                  minThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minThicknessPercentOfNominalSetting);
  }

  /**
   * Runs 'Insufficient' on all the given joints.
   *
   * @author Matt Wharton
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Ensure the all the JointInspectionData objects have the same subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      Assert.expect(jointInspectionData.getSubtype() == subtype);
    }

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the reconstructed pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Check each joint for insufficient solder.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                inspectionRegion,
                                                reconstructedPadSlice,
                                                false);

      BooleanRef jointPassed = new BooleanRef(true);
      detectInsufficientSolder(reconstructedPadSlice, jointInspectionData, jointPassed);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }

  /**
   * Detects insufficient solder on the specified joint by comparing the joint's fillet thickness percent of
   * nominal against the defined tolerance setting.
   *
   * @author Matt Wharton
   */
  private void detectInsufficientSolder(ReconstructedSlice reconstructedSlice,
                                        JointInspectionData jointInspectionData,
                                        BooleanRef jointPassed)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the SliceNameEnum.
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate tested joint's fillet thickness' percent of the nominal fillet thickness.
    final float NOMINAL_FILLET_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS);
    JointMeasurement filletThicknessMeasurement =
        GullwingMeasurementAlgorithm.getFilletThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float filletThickness = filletThicknessMeasurement.getValue();
    float filletThicknessPercentOfNominal = (filletThickness / (float)NOMINAL_FILLET_THICKNESS) * 100f;
    JointMeasurement filletThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             filletThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(filletThicknessPercentOfNominalMeasurement);

    // Check to see if the fillet thickness is within acceptable tolerance of nominal.
    final float MIN_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL);
    if (MathUtil.fuzzyLessThan(filletThicknessPercentOfNominal, MIN_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'insufficient'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment insufficientIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceNameEnum);

      // Add the failing and related measurements.
      insufficientIndictment.addFailingMeasurement(filletThicknessPercentOfNominalMeasurement);
      insufficientIndictment.addRelatedMeasurement(filletThicknessMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(insufficientIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      MIN_THICKNESS_PERCENT_OF_NOMINAL);
    }
  }

  /**
   * @author Matt Wharton
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // We can't learn if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      return;
    }

    /** mdw - Kathy doesn't think we should be learning the insufficient threshold.
     * Perhaps we can revisit this later (i.e. post 1.0).
     */
    //    // Gullwing Insufficient only runs on the pad slice.
//    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
//
//    // Learn the "insufficient fillet thickness" defect setting.
//    learnInsufficientFilletThicknessDefectSetting(subtype, padSlice);
  }

  /**
   * @author Matt Wharton
   */
  private void learnInsufficientFilletThicknessDefectSetting(Subtype subtype,
                                                             SliceNameEnum slice) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull the learned "fillet thickness" measurement values from the database.
    float[] filletThicknessLearnedMeasurementValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.GULLWING_FILLET_THICKNESS,
                                                 true,
                                                 true);

    // For each "fillet thickness" measurement, calculate its percent of nominal.
    float[] filletThicknessPercentOfNominalValues = new float[filletThicknessLearnedMeasurementValues.length];
    final float NOMINAL_FILLET_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS);
    for (int i = 0; i < filletThicknessLearnedMeasurementValues.length; ++i)
    {
      filletThicknessPercentOfNominalValues[i] = (filletThicknessLearnedMeasurementValues[i] / NOMINAL_FILLET_THICKNESS) * 100f;
    }

    // We can only do the IQR statistics if we have at least two data points.
    if (filletThicknessPercentOfNominalValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRanges(filletThicknessPercentOfNominalValues);
      float medianFilletThicknessPercentOfNominal = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];
      float minimumFilletThicknessPercentOfNominal = medianFilletThicknessPercentOfNominal - (3f * interQuartileRange);

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL,
                              minimumFilletThicknessPercentOfNominal);
    }
  }
}
