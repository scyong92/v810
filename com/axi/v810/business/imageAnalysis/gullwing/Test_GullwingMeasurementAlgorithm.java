package com.axi.v810.business.imageAnalysis.gullwing;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;
import com.axi.v810.business.*;

/**
 * Test class for Gullwing Measurement.
 *
 * @author Matt Wharton
 */
public class Test_GullwingMeasurementAlgorithm extends AlgorithmUnitTest
{
  private static final String _TEST_PROJECT_NAME = "FAMILIES_ALL_RLV";
  private static final String _GULLWING_SUBTYPE_NAME = "so00001_Gullwing";
  private static final String _GULLWING_REFDES = "U1";
  private static final String _GULLWING_PAD_NAME = "1";

  /**
   * @author Matt Wharton
   */
  private Test_GullwingMeasurementAlgorithm()
  {
    // Do nothing ...
  }

  /**
   * Main unit test entry point.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    try
    {
      // Load up the test project (FAMILIES_ALL_RLV).
      Project project = getProject(_TEST_PROJECT_NAME);

      // Select the "normal" image set.
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = getNormalImageSet(project);
      imageSetDataList.add(imageSetData);

      // Run a subtype test on one of the Gullwing subtypes.
      Panel panel = project.getPanel();
      Subtype subtype = panel.getSubtype(_GULLWING_SUBTYPE_NAME);
      runSubtypeUnitTestInspection(project, subtype, imageSetDataList);

      // Now we want to run some tests against 'pathological' images (black, white, random shapes).
      // Pick an arbitarary pad to test.
      Component component = panel.getBoards().get(0).getComponent(_GULLWING_REFDES);
      Pad pad = component.getPad(_GULLWING_PAD_NAME);

      // Run against a black image.

      imageSetData = createAndSelectBlackImageSet(project, pad, "gullwingTestBlackImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      // Run against a white image.
      imageSetData = createAndSelectWhiteImageSet(project, pad, "gullwingTestWhiteImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      // Run against a pseudo-random image.
      createAndSelectPseudoRandomImageSet(project, pad, "gullwingTestPseudoRandomImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_GullwingMeasurementAlgorithm());
  }
}
