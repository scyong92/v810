package com.axi.v810.business.imageAnalysis.gullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;

/**
 * InspectionFamily for gullwing, jlead, and socket joints.
 *
 * @author Matt Wharton
 */
public class GullwingInspectionFamily extends InspectionFamily
{

  /**
   * @author Matt Wharton
   */
  public GullwingInspectionFamily()
  {
    super(InspectionFamilyEnum.GULLWING);

    // Associate the individual algorithms with the family.

    // Measurement.
    Algorithm gullwingMeasurementAlgorithm = new GullwingMeasurementAlgorithm(this);
    addAlgorithm(gullwingMeasurementAlgorithm);

    // Short.
    Algorithm gullwingShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.GULLWING);
    addAlgorithm(gullwingShortAlgorithm);

    // Open.
    Algorithm gullwingOpenAlgorithm = new GullwingOpenAlgorithm(this);
    addAlgorithm(gullwingOpenAlgorithm);

    // Insufficient.
    Algorithm gullwingInsufficientAlgorithm = new GullwingInsufficientAlgorithm(this);
    addAlgorithm(gullwingInsufficientAlgorithm);
    
    // Excess.
    Algorithm gullwingExcessAlgorithm = new GullwingExcessAlgorithm(this);
    addAlgorithm(gullwingExcessAlgorithm);

    // Misalignment.
    Algorithm gullwingMisalignmentAlgorithm = new GullwingMisalignmentAlgorithm(this);
    addAlgorithm(gullwingMisalignmentAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);
    List<SliceNameEnum> sliceNameEnums = new ArrayList<SliceNameEnum>();
    sliceNameEnums.add(SliceNameEnum.PAD);
    if (SharedShortAlgorithm.allowHighShortDetection(subtype.getJointTypeEnum()))
    {
      sliceNameEnums.add(SliceNameEnum.HIGH_SHORT);
    }
    return sliceNameEnums;
  }

  /**
   * @author Peter Esbensen
   */
  public Collection<SliceNameEnum> getShortInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);
    List<SliceNameEnum> sliceNameEnums = new ArrayList<SliceNameEnum>();
    sliceNameEnums.add(SliceNameEnum.PAD);
    if (SharedShortAlgorithm.allowHighShortDetection(subtype.getJointTypeEnum()))
    {
      sliceNameEnums.add(SliceNameEnum.HIGH_SHORT);
    }
    return sliceNameEnums;
  }

  /**
   * Gets the applicable default SliceNameEnum for the Pad slice for the Gullwing inspection family.
   *
   * @author Matt Wharton
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Use the standard pad slice.
    return SliceNameEnum.PAD;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }
}
