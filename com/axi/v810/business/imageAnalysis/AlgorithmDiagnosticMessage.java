package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.panelSettings.*;

/**
 * Encapsulates an image along with diagnostic information associated with the image.  For example, this
 * may be used to display region of interest on an image to the user.
 *
 * @author Peter Esbensen
 * @author Matt Wharton
 */
public class AlgorithmDiagnosticMessage
{
  private Image _image = null;
  private ReconstructionRegion _inspectionRegion = null;
  private SliceNameEnum _sliceNameEnum = null;
  private JointInspectionData _jointInspectionData = null;
  private Algorithm _algorithm = null;
  private boolean _invalidatePreviousDiagnostics = false;
  private boolean _resetDiagnosticInfos = false;
  private boolean _pauseRequested = false;
  private Subtype _subtype = null;
  private int _focusConfirmationOffsetInNanoMeters = 0;

  private Collection<DiagnosticInfo> _diagnosticInfoCollection = new LinkedList<DiagnosticInfo>();

  private static FocusConfirmationEngine _focusConfirmationEngine = FocusConfirmationEngine.getInstance();

  /**
   * @author Patrick Lacz
   */
  public AlgorithmDiagnosticMessage(
      ReconstructionRegion inspectionRegion,
      SliceNameEnum sliceNameEnum,
      JointInspectionData jointInspectionData,
      Algorithm algorithm,
      boolean invalidatePreviousDiagnostics,
      DiagnosticInfo... diagnosticInfoObjects)
  {
    Assert.expect(inspectionRegion != null);

    _inspectionRegion = inspectionRegion;
    _jointInspectionData = jointInspectionData;
    _sliceNameEnum = sliceNameEnum;
    _algorithm = algorithm;
    _invalidatePreviousDiagnostics = invalidatePreviousDiagnostics;
    if (jointInspectionData != null)
      _subtype = jointInspectionData.getSubtype();
    _focusConfirmationOffsetInNanoMeters = _focusConfirmationEngine.getFocusConfirmationOffsetInNanometersForCurrentThread();

    Collections.addAll(_diagnosticInfoCollection, diagnosticInfoObjects);
  }

  /**
   * @author Patrick Lacz
   */
  public AlgorithmDiagnosticMessage(
      ReconstructionRegion inspectionRegion,
      Algorithm algorithm,
      boolean invalidatePreviousDiagnostics,
      DiagnosticInfo... diagnosticInfoObjects)
  {
    Assert.expect(inspectionRegion != null);
    _inspectionRegion = inspectionRegion;
    _algorithm = algorithm;
    _invalidatePreviousDiagnostics = invalidatePreviousDiagnostics;
    _focusConfirmationOffsetInNanoMeters = _focusConfirmationEngine.getFocusConfirmationOffsetInNanometersForCurrentThread();

    Collections.addAll(_diagnosticInfoCollection, diagnosticInfoObjects);
  }

  /**
   * @author Peter Esbensen
   */
  void setPauseRequested(boolean pauseRequested)
  {
    _pauseRequested = pauseRequested;
  }

  /**
   * @author Peter Esbensen
   */
  public boolean getPauseRequested()
  {
    return _pauseRequested;
  }


  /**
   * @author George A. David
   */
  public boolean hasImage()
  {
    return _image != null;
  }

  /**
   * @author Peter Esbensen
   */
  public Image getImage()
  {
    Assert.expect(_image != null);
    return _image;
  }

  /**
   * @author Patrick Lacz
   */
  public void setImage(Image image)
  {
    Assert.expect(image != null);
    _image = image;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasJointInspectionData()
  {
    return _jointInspectionData != null;
  }

  /**
   * @author Matt Wharton
   */
  public JointInspectionData getJointInspectionData()
  {
    Assert.expect(_jointInspectionData != null);

    return _jointInspectionData;
  }

  /**
   * @author Patrick Lacz
   */
  public void setJointInspectionData(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    _jointInspectionData = jointInspectionData;
    _subtype = jointInspectionData.getSubtype();
  }

  /**
   * @author Matt Wharton
   */
  public Algorithm getAlgorithm()
  {
    Assert.expect(_algorithm != null);

    return _algorithm;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasAlgorithm()
  {
    return _algorithm != null;
  }

  /**
   * @author Patrick Lacz
   */
  public void setAlgorithm(Algorithm algorithm)
  {
    Assert.expect(algorithm != null);
    _algorithm = algorithm;
  }

  /**
   * Returns the InspectionRegion associated with this Image.  This is useful in figuring out
   * where this image is on the panel.
   *
   * @author Peter Esbensen
   */
  public ReconstructionRegion getInspectionRegion()
  {
    Assert.expect(_inspectionRegion != null);
    return _inspectionRegion;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasSliceNameEnum()
  {
    return _sliceNameEnum != null;
  }

  /**
   * Gets the associated SliceNameEnum.
   *
   * @author Matt Wharton
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);

    return _sliceNameEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasSubtype()
  {
    return _subtype != null;
  }

  /**
   * @author Patrick Lacz
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);
    return _subtype;
  }

  /**
   * @author Patrick Lacz
   */
  public void setSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);
    _subtype = subtype;
  }


  /**
   * @author Patrick Lacz
   */
  public Collection<DiagnosticInfo> getDiagnosticInfoList()
  {
    Assert.expect(_diagnosticInfoCollection != null);
    return _diagnosticInfoCollection;
  }

  /**
   * Gets the associated ProfileDiagnosticInfo.
   *
   * @return the associated ProfileDiagnosticInfo.
   * @author Matt Wharton
   */
  public ProfileDiagnosticInfo getProfileDiagnosticInfo()
  {
    for (DiagnosticInfo info : _diagnosticInfoCollection)
    {
      if (info instanceof ProfileDiagnosticInfo)
        return (ProfileDiagnosticInfo)info;
    }
    Assert.expect(false); // should use hasProfileDiagnosticInfo
    return null;
  }

  /**
   * Gets the associated MeasurementRegionDiagnosticInfo.
   *
   * @return the associated MeasurementRegionDiagnosticInfo.
   * @author Matt Wharton
   */
  public MeasurementRegionDiagnosticInfo getMeasurementRegionDiagnosticInfo()
  {
    for (DiagnosticInfo info : _diagnosticInfoCollection)
    {
      if (info instanceof MeasurementRegionDiagnosticInfo)
        return (MeasurementRegionDiagnosticInfo)info;
    }
    Assert.expect(false); // should use hasMeasurementRegionDiagnosticInfo
    return null;
  }

  /**
   * Gets the associated TextualDiagnosticInfo.
   *
   * @return the associated TextualDiagnosticInfo.
   * @author Matt Wharton
   */
  public TextualDiagnosticInfo getTextualDiagnosticInfo()
  {
    for (DiagnosticInfo info : _diagnosticInfoCollection)
    {
      if (info instanceof TextualDiagnosticInfo)
        return (TextualDiagnosticInfo)info;
    }
    Assert.expect(false); // should use hasMeasurementRegionDiagnosticInfo
    return null;
  }

  /**
   * Determines whether or not ProfileDiagnosticInfo is present.
   *
   * @return true if ProfileDiagnosticInfo is available, false otherwise.
   * @author Matt Wharton
   */
  public boolean hasProfileDiagnosticInfo()
  {
    for (DiagnosticInfo info : _diagnosticInfoCollection)
    {
      if (info instanceof ProfileDiagnosticInfo)
        return true;
    }
    return false;
  }

  /**
   * Determines whether or not MeasurementRegionDiagnosticInfo is present.
   *
   * @return true if MeasurementRegionDiagnosticInfo is available, false otherwise.
   * @author Matt Wharton
   */
  public boolean hasMeasurementRegionDiagnosticInfo()
  {
    for (DiagnosticInfo info : _diagnosticInfoCollection)
    {
      if (info instanceof MeasurementRegionDiagnosticInfo)
        return true;
    }
    return false;
  }

  /**
   * Determines whether or not TextualDiagnosticInfo is present.
   *
   * @return true if TextualDiagnosticInfo is available, false otherwise.
   * @author Matt Wharton
   */
  public boolean hasTextualDiagnosticInfo()
  {
    for (DiagnosticInfo info : _diagnosticInfoCollection)
    {
      if (info instanceof TextualDiagnosticInfo)
        return true;
    }
    return false;
  }

  /**
   * Determines whether or any graphical diagnostics are present.
   * @author Patrick Lacz
   */
  public boolean hasAnyGraphicalDiagnosticInfo()
  {
    Assert.expect(_diagnosticInfoCollection != null);

    boolean hasAGraphicalDiagnosticInfo = false;
    for (DiagnosticInfo info : _diagnosticInfoCollection)
    {
      if (info instanceof TextualDiagnosticInfo)
        continue;
      if (info instanceof MeasurementRegionDiagnosticInfo ||
          info instanceof ProfileDiagnosticInfo ||
          info instanceof InspectionImageDiagnosticInfo ||
          info instanceof OverlayImageDiagnosticInfo ||
          info instanceof OverlaySolderImageDiagnosticInfo ||
          info instanceof OverlayExpectedImageDiagnosticInfo ||
          info instanceof OverlayLargestVoidImageDiagnosticInfo ||
          info instanceof OverlayWettingCoverageImageDiagnosticInfo)
      {
        hasAGraphicalDiagnosticInfo = true;
        break;
      }
      else
      {
        Assert.expect(false, "Unknown diagnostic info type.");
      }
    }

    return hasAGraphicalDiagnosticInfo;
  }

  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    if (_image != null)
      _image.incrementReferenceCount();

    for (DiagnosticInfo diagnosticInfo : _diagnosticInfoCollection)
      diagnosticInfo.incrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    if (_image != null)
      _image.decrementReferenceCount();

    for (DiagnosticInfo diagnosticInfo : _diagnosticInfoCollection)
      diagnosticInfo.decrementReferenceCount();
  }

  /**
   * Determines whether or not any previous diagnostics are to be invalidated.
   *
   * @return true if the previous diagnostics should be invalidated, false otherwise.
   * @author Matt Wharton
   */
  public boolean arePreviousDiagnosticsInvalidated()
  {
    return _invalidatePreviousDiagnostics;
  }

  /**
   * @return true if a 'fresh' set of diagnostics infos is to be expected, false otherwise.
   *
   * @author Matt Wharton
   */
  public boolean areDiagnosticInfosReset()
  {
    return _resetDiagnosticInfos;
  }

  /**
   * @author Matt Wharton
   */
  void setDiagnosticInfosReset(boolean resetDiagnosticInfos)
  {
    _resetDiagnosticInfos = resetDiagnosticInfos;
  }

  /**
   * @author Matt Wharton
   */
  public int getFocusConfirmationOffsetInNanoMeters()
  {
    return _focusConfirmationOffsetInNanoMeters;
  }
}
