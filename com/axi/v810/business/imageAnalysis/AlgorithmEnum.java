package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;
import java.util.*;

/**
 * @author Peter Esbensen
 */
public class AlgorithmEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  private static Map<String, AlgorithmEnum> _nameToAlgorithmEnumMap = new HashMap<String, AlgorithmEnum>();

  // PE:  The Measurement algorithm should always have the lowest index because
  // the index is determines the order that the algorithms will be run in.
  public static final AlgorithmEnum MEASUREMENT = new AlgorithmEnum(++_index, "Measurement");
  public static final AlgorithmEnum SHORT = new AlgorithmEnum(++_index, "Short");
  public static final AlgorithmEnum VOIDING = new AlgorithmEnum(++_index, "Voiding");
  public static final AlgorithmEnum OPEN = new AlgorithmEnum(++_index, "Open");
  public static final AlgorithmEnum INSUFFICIENT = new AlgorithmEnum(++_index, "Insufficient");
  public static final AlgorithmEnum MISALIGNMENT = new AlgorithmEnum(++_index, "Misalignment");
  public static final AlgorithmEnum EXCESS = new AlgorithmEnum(++_index, "Excess");

  private String _name;

  /**
   * @author Peter Esbensen
   */
  private AlgorithmEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name;
    AlgorithmEnum algEnum = _nameToAlgorithmEnumMap.put(name, this);
    Assert.expect(algEnum == null);
  }

  /**
   * Get all the Enums.
   *
   * @author Peter Esbensen
   */
  static Collection<AlgorithmEnum> getAllEnums()
  {
    return _nameToAlgorithmEnumMap.values();
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return _name;
  }


  /**
   * @author Bill Darbie
   */
  public static AlgorithmEnum getAlgorithmEnum(String algorithmEnumName)
  {
    Assert.expect(algorithmEnumName != null);
    AlgorithmEnum algEnum = _nameToAlgorithmEnumMap.get(algorithmEnumName);

    Assert.expect(algEnum != null);
    return algEnum;
  }
}
