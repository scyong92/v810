package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

public class Test_ProfileDiagnosticInfo extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  private Test_ProfileDiagnosticInfo()
  {
    // Do nothing...
  }

  /**
   * @author Peter Esbensen
   */
  private void testCreateSingleDimensionProfileDiagnosticInfo()
  {
    ProfileTypeEnum profileTypeEnum = ProfileTypeEnum.SOLDER_PROFILE;
    LocalizedString axisDescription = new LocalizedString("testing 1 2 3", null);
    float dataMin = -1.23f;
    float dataMax = 4.56f;
    float[] data = new float[2];
    data[0] = 1.2f;
    data[1] = 2.3f;

    SingleDimensionProfileDiagnosticInfo profileDiagnosticInfo =
        ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo("padName",
                                                                         ProfileTypeEnum.SOLDER_PROFILE,
                                                                         axisDescription,
                                                                         dataMin,
                                                                         dataMax,
                                                                         data,
                                                                         MeasurementRegionEnum.PAD_REGION,
                                                                         MeasurementUnitsEnum.MILLIMETERS);

    List<LocalizedString> axisDescriptionsToCheck = profileDiagnosticInfo.getAxisDescriptions();
    Expect.expect(axisDescriptionsToCheck.size() == 1);
    Expect.expect(axisDescriptionsToCheck.get(0) == axisDescription);
    Expect.expect(profileDiagnosticInfo.getProfileType().equals(profileTypeEnum));
    List<Pair<Float, Float>> axisExtents = profileDiagnosticInfo.getAxisExtents();
    Expect.expect(axisExtents.get(0).getFirst() == dataMin);
    Expect.expect(axisExtents.get(0).getSecond() == dataMax);
    List<Pair<MeasurementRegionEnum, Float>> dataToCheck = profileDiagnosticInfo.getData();
    for (int i = 0 ; i < dataToCheck.size(); ++i)
    {
      Pair<MeasurementRegionEnum, Float> dataPair = dataToCheck.get(i);
      Expect.expect(dataPair.getFirst().equals(MeasurementRegionEnum.PAD_REGION));
      float dataValue = dataPair.getSecond();
      Expect.expect(dataValue == data[i]);
    }
  }


  /**
   * Main test method.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testCreateSingleDimensionProfileDiagnosticInfo();
  }

  /**
   * Test fixture entry point.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ProfileDiagnosticInfo());
  }
}
