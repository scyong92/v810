package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.util.*;

/**
 * A diagnostic message that requests an solder image overlay on the inspection image.
 *
 * @author George Booth
 */
public class OverlaySolderImageDiagnosticInfo extends DiagnosticInfo
{

  private Image _overlayImage = null;
  private RegionOfInterest _regionOfInterestInInspectionImage = null;
  private String _diagnosticLegendLabel = StringLocalizer.keyToString("SOLDER_AREA_DIAGNOSTIC_KEY");

  /**
   * @author George Booth
   */
  public OverlaySolderImageDiagnosticInfo(Image overlayImage, RegionOfInterest roi)
  {
    Assert.expect(overlayImage != null);
    Assert.expect(roi != null);

    _overlayImage = overlayImage;
    _overlayImage.incrementReferenceCount();
    _regionOfInterestInInspectionImage = roi;
  }

  /**
   * @author George Booth
   */
  public Image getOverlayImage()
  {
    Assert.expect(_overlayImage != null);

    return _overlayImage;
  }

  /**
   * @author George Booth
   */
  public RegionOfInterest getRegionOfInterestInInspectionImage()
  {
    Assert.expect(_regionOfInterestInInspectionImage != null);

    return _regionOfInterestInInspectionImage;
  }

  /**
   * @author George Booth
   */
  public void incrementReferenceCount()
  {
    _overlayImage.incrementReferenceCount();
  }

  /**
   * @author George Booth
   */
  public void decrementReferenceCount()
  {
    _overlayImage.decrementReferenceCount();
  }

}
