package com.axi.v810.business.imageAnalysis.largePad;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import ij.ImagePlus;

/**
 * Large Pad insuffcient algorithm.
 * Added on 09-Jun-2009
 *
 * @author Lim, Seng Yew
 */
public class LargePadInsufficientAlgorithm extends Algorithm
{
  public final static String _THICKNESS_METHOD = "Thickness Method";
  public final static String _AREA_METHOD = "Area Method";
  private final static ArrayList<String> LARGE_PAD_INSUFFICIENT_METHOD_CHOICES = new ArrayList<String>(Arrays.asList(_THICKNESS_METHOD,_AREA_METHOD));
  
  /**
   * @author Lim, Seng Yew
   */
  public LargePadInsufficientAlgorithm(InspectionFamily largePadInspectionFamily)
  {
    super(AlgorithmEnum.INSUFFICIENT, InspectionFamilyEnum.LARGE_PAD);

    Assert.expect(largePadInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(largePadInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @author Lim, Seng Yew
   */
  protected void addMeasurementEnums()
  {
      _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_AREA_PERCENT);
//    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
//    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
  }

  /**
   * Adds the algorithm settings for Large Pad Insufficient.
   *
   * @author Lim, Seng Yew
   */
  private void addAlgorithmSettings(InspectionFamily largePadInspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    //Jack Hwee
    AlgorithmSetting insufficientDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD, // setting enum
        displayOrder++, // display order,
        _THICKNESS_METHOD,
        LARGE_PAD_INSUFFICIENT_METHOD_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_METHOD)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_METHOD)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_METHOD)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDetection);
    
    AlgorithmSetting sensitivityThreshold = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY,
        displayOrder++,
        4, // default value
        1, // minimum value
        10, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_SENSITIVITY)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_SENSITIVITY)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_SENSITIVITY)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(sensitivityThreshold);
    
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_UNTESTED_BORDER_SIZE,
        displayOrder++,
        0.0f,
        0.0f,
        1.0f,
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_UNTESTED_BORDER_SIZE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_UNTESTED_BORDER_SIZE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_UNTESTED_BORDER_SIZE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion));
    
    AlgorithmSetting minimumInsufficientArea = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD,
        displayOrder++,
        5.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(minimumInsufficientArea);
    
    AlgorithmSetting minimumInsufficientAreaUserDefinedSlice1 = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_1,
        displayOrder++,
        5.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD_USER_DEFINED_SLICE_1)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD_USER_DEFINED_SLICE_1)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD_USER_DEFINED_SLICE_1)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    minimumInsufficientAreaUserDefinedSlice1.setSpecificSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
    minimumInsufficientAreaUserDefinedSlice1.filter(JointTypeEnum.LGA);
    addAlgorithmSetting(minimumInsufficientAreaUserDefinedSlice1);
    
    AlgorithmSetting minimumInsufficientAreaUserDefinedSlice2 = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_2,
        displayOrder++,
        5.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD_USER_DEFINED_SLICE_2)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD_USER_DEFINED_SLICE_2)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(INSUFFICIENT_MINIMUM_AREA_THRESHOLD_USER_DEFINED_SLICE_2)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    minimumInsufficientAreaUserDefinedSlice2.setSpecificSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
    minimumInsufficientAreaUserDefinedSlice2.filter(JointTypeEnum.LGA);
    addAlgorithmSetting(minimumInsufficientAreaUserDefinedSlice2);
    
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minimumInsufficientArea,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_AREA_PERCENT );
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minimumInsufficientArea,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_AREA_PERCENT );
    
    // Minimum Thickness
    AlgorithmSetting minimumThickness = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_THICKNESS,
        displayOrder++,
        0.0381f, //default
        0.f, // min
        0.635f, //max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(MINIMUM_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(MINIMUM_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(MINIMUM_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(minimumThickness);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minimumThickness,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_THICKNESS );
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minimumThickness,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_THICKNESS );

      // Maximum Sum of Slope Changes
    AlgorithmSetting maximumSumOfSlopeChanges = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES,
        displayOrder++,
        50.f, // default
        0.f, // min
        10000.f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_INSUFFICIENT_(MAXIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // desc
        "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(MAXIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // detailed desc
        "IMG_DESC_LARGEPAD_INSUFFICIENT_(MAXIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumSumOfSlopeChanges);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          maximumSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES );
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          maximumSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES );

  }

  /**
   * Main entry point for Large Pad Insufficient.
   * Added on 09-Jun-2009 (sixth day of my 2nd son was born)
   *
   * @author Lim, Seng Yew
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    
    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    LargePadMeasurementAlgorithm measurementAlgo = (LargePadMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.LARGE_PAD, AlgorithmEnum.MEASUREMENT);
    Collection<SliceNameEnum> inspectedSlices = measurementAlgo.getInspectionSliceOrder(subtype);

    if (isAreaMethod(subtype))
    {
      classifySlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.PAD,
              (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD),
              (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY),
              MILIMETER_PER_PIXEL
              );
      
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1))
      {
        classifySlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
               (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_1),
               (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY),
               MILIMETER_PER_PIXEL
               ); 
      }
      
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2))
      {
        classifySlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
               (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_2),
               (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY),
               MILIMETER_PER_PIXEL
               ); 
      }
    }
    else
    {
      // Large Pad runs on the pad slice.
      SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

      classifySliceForPadThickness(subtype, reconstructedImages, jointInspectionDataObjects,
                     inspectionRegion, padSlice); 
      
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1))
      {
        classifySliceForPadThickness(subtype, reconstructedImages, jointInspectionDataObjects,
                     inspectionRegion, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1); 
      }
      
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2))
      {
        classifySliceForPadThickness(subtype, reconstructedImages, jointInspectionDataObjects,
                     inspectionRegion, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2); 
      }
    }
    inspectedSlices.clear();
  }

  /**
   * Detects insufficient thickness in located pad
   *
   * @author Lim, Seng Yew
   */
  private void detectMinimumThickness(Image image,
                                Subtype subtype,
                                SliceNameEnum sliceNameEnum,
                                JointInspectionData jointInspectionData,
                                BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the joint inspection result.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check if the slope sum along within acceptable limits.
    JointMeasurement padThicknessMeasurement = LargePadMeasurementAlgorithm.getPadThicknessMeasurement(
        jointInspectionData, sliceNameEnum);
    //XCR2380 - Khaw Chek Hau: Single Pad Insufficient Minimum Pad Thickness Algorithm Bug
    final float minimumPadThicknessInMillimeters =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MINIMUM_THICKNESS);
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.INSUFFICIENT,
                                            padThicknessMeasurement,
                                            minimumPadThicknessInMillimeters,
                                            Float.NaN,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_THRESHOLD_ONLY)
                                            == false)
    {
      jointPassed.setValue(false);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Detects insufficient thickness in located pad
   *
   * @author Lim, Seng Yew
   */
  private void detectMaximumSumOfSlopeChanges(Image image,
                                Subtype subtype,
                                SliceNameEnum sliceNameEnum,
                                JointInspectionData jointInspectionData,
                                BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the joint inspection result.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check if the slope sum along within acceptable limits.
    JointMeasurement sumOfSlopeChangesMeasurement = LargePadMeasurementAlgorithm.getSumOfSlopeChangesMeasurement(
        jointInspectionData, sliceNameEnum);
    final float MAXIMUM_SUM_OF_SLOPE_CHANGES =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES);
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.INSUFFICIENT,
                                            sumOfSlopeChangesMeasurement,
                                            Float.NaN,
                                            MAXIMUM_SUM_OF_SLOPE_CHANGES,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MAXIMUM_THRESHOLD_ONLY)
                                            == false)    
    {
      jointPassed.setValue(false);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }
  
  /**
   * @author Jack Hwee
   */
  static public boolean isAreaMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_METHOD);

    if (voidingChoice.equals(_AREA_METHOD))
    {
      return true;
    }

    return false;
  }
  
   /**
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Jack Hwee
   */
  private void classifySlice(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float minimumInsufficientAreaInPercent,
      float insufficientAreaDetectionSensitivity,
      final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    Image sliceImage = slice.getOrthogonalImage();

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
   
    float untestedBorderSizeInMM = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_UNTESTED_BORDER_SIZE);
    int untestedBorderSizeInPixels = (int)Math.ceil(untestedBorderSizeInMM / MILIMETER_PER_PIXEL);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);

    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);
      RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(joint);
      
      if ((jointRoi.getWidth() - 2*untestedBorderSizeInPixels < 5.0) || (jointRoi.getHeight() - 2*untestedBorderSizeInPixels < 5.0))
      {
        // @todo PWL : post warning about the untested region being too large
        LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                          new Object[] {joint.getFullyQualifiedPadName()});
        AlgorithmUtil.raiseAlgorithmWarning(warningText);
        continue;
      }
        
      jointRoi.setWidthKeepingSameCenter(jointRoi.getWidth() - 2*untestedBorderSizeInPixels);
      jointRoi.setHeightKeepingSameCenter(jointRoi.getHeight() - 2*untestedBorderSizeInPixels);
      
      double gaussianBlurRadius = 25.0;
        
      // if we apply a blur, we need to use a different image/roi
      Image imageToUse = sliceImage;
      
      imageToUse = Filter.convolveLowpass(sliceImage, jointRoi);
           
      ImagePlus gaussianBlurBuffImg ;
    
      gaussianBlurBuffImg = new ImagePlus("", imageToUse.getBufferedImage());
      
      if (gaussianBlurBuffImg != null)
      {  
        GaussianBlur gaussianBlur = new GaussianBlur();   
        gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurRadius);
      }
      else
      {
        GaussianBlur gaussianBlur = new GaussianBlur();
        gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurRadius);
      }

      gaussianBlurBuffImg.lockSilently();
      
      Image filterGaussImage = Image.createFloatImageFromBufferedImage(gaussianBlurBuffImg.getBufferedImage());
   
      gaussianBlurBuffImg.close();
      gaussianBlurBuffImg.flush(); 
      
      Image totalGaussVoidImage;  
      totalGaussVoidImage = Arithmetic.subtractImages(imageToUse, filterGaussImage);
    
      filterGaussImage.decrementReferenceCount();
            
      if (insufficientAreaDetectionSensitivity > 5)
      {
        int numHistogramBins = 256;
        int[] histogram = Threshold.histogram(imageToUse, RegionOfInterest.createRegionFromImage(imageToUse), numHistogramBins);
        int level = Threshold.getAutoThreshold(histogram);
        
        if (insufficientAreaDetectionSensitivity == 6) level = level - 1;
        if (insufficientAreaDetectionSensitivity == 7) level = level - 2;
        if (insufficientAreaDetectionSensitivity == 8) level = level - 3;
        if (insufficientAreaDetectionSensitivity == 9) level = level - 4;
        if (insufficientAreaDetectionSensitivity == 10) level = level - 5;
                         
        Threshold.threshold(imageToUse, (float) level, 0.0f, (float)level, 255.0f);
        
        totalGaussVoidImage.decrementReferenceCount();
        totalGaussVoidImage = Image.createCopy(imageToUse, RegionOfInterest.createRegionFromImage(imageToUse));       
      }
      else
      {
        Threshold.threshold(totalGaussVoidImage, (float)(5 - insufficientAreaDetectionSensitivity), 255.0f, (float)(5 - insufficientAreaDetectionSensitivity), 0.0f);
        Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 0.0f); 
      }
           
      float [] largestVoidImageArray = Threshold.floodFillInsufficient(totalGaussVoidImage);
            
      Image insufficientAreaImage = Image.createFloatImageFromArray(imageToUse.getWidth(), imageToUse.getHeight(), largestVoidImageArray);
  
      // count the number of "void" pixels
      int numberOfVoidPixels = Threshold.countPixelsInRange(insufficientAreaImage, 100.0f, 255.0f);            
      
      // Store the percent voiding
      int numberOfPixelsTested = jointRoi.getWidth()*jointRoi.getHeight();
      
      float voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;
     
      JointInspectionResult jointResult = joint.getJointInspectionResult();
      JointMeasurement insufficientPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_AREA_PERCENT,
          MeasurementUnitsEnum.PERCENT,
          joint.getPad(), sliceNameEnum, voidPercent);

      jointResult.addMeasurement(insufficientPercentMeasurement);

      if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, insufficientPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     new OverlayImageDiagnosticInfo(insufficientAreaImage, jointRoi));
      }
      imageToUse.decrementReferenceCount();
      insufficientAreaImage.decrementReferenceCount();
      totalGaussVoidImage.decrementReferenceCount();
      gaussianBlurBuffImg.unlock();

      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > minimumInsufficientAreaInPercent);
     
      if (jointFailed)
      {
        JointIndictment insufficientPercentIndictment = new JointIndictment(IndictmentEnum.INSUFFICIENT,
            this, sliceNameEnum);
        insufficientPercentIndictment.addFailingMeasurement(insufficientPercentMeasurement);
        jointResult.addIndictment(insufficientPercentIndictment);
      }

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !jointFailed);
    }
  }
  
    /**
   * @author Jack Hwee
   */
  private void classifySliceForPadThickness(Subtype subtype, ReconstructedImages reconstructedImages,
                                            List<JointInspectionData> jointInspectionDataObjects,
                                            ReconstructionRegion inspectionRegion, SliceNameEnum sliceNameEnum)
  {
      // Get the applicable ReconstructedSlice for the pad slice.
      ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

      // Get the image for processing
      Image imageToInspect = reconstructedPadSlice.getOrthogonalImage();
 
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

      // Iterate thru each joint.
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

        BooleanRef jointPassed = new BooleanRef(true);

        // Check for opens based on second derivative area.
        detectMinimumThickness(imageToInspect, subtype, sliceNameEnum, jointInspectionData, jointPassed);
        detectMaximumSumOfSlopeChanges(imageToInspect, subtype, sliceNameEnum, jointInspectionData, jointPassed);

        AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                                jointInspectionData,
                                                                inspectionRegion,
                                                                sliceNameEnum,
                                                                jointPassed.getValue());
      }
  }
  
  /**
   * @author Siew Yeng
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);
    
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      learnSlopeSettings(subtype, padSlice);
    }
  }
  
  /**
   * Learns the slope settings.
   * @author Siew Yeng
   */
  private void learnSlopeSettings(Subtype subtype, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Get all the learned "Sum of slope changes" values.
    float[] sumOfSlopeChangesValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                              MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES,
                                                                              true,
                                                                              true);

    // We can only do the IQR statistics if we have at least two data points.
    if (sumOfSlopeChangesValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(sumOfSlopeChangesValues);
      sumOfSlopeChangesValues = null;
      float medianSumOfSlopeChanges = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES) == 
        subtype.getAlgorithmSettingDefaultValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES))
      {
        float maximumAcceptableSumOfSlopeChanges = medianSumOfSlopeChanges + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES, maximumAcceptableSumOfSlopeChanges);
      }
    }
  }
  
   /**
   * Resets all learned defect settings back to their default values.
   * @author Siew Yeng
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES);
    }
  }
}
