package com.axi.v810.business.imageAnalysis.largePad;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Large Pad Open algorithm.
 * Added on 29-May-2009
 *
 * @author Lim, Seng Yew
 */
public class LargePadOpenAlgorithm extends Algorithm
{
  /**
   * @author Lim, Seng Yew
   */
  public LargePadOpenAlgorithm(InspectionFamily largePadInspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.LARGE_PAD);

    Assert.expect(largePadInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(largePadInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @author Lim, Seng Yew
   */
  protected void addMeasurementEnums()
  {
//    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
//    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
  }

  /**
   * Adds the algorithm settings for Large Pad Open.
   * Added on 29-May-2009
   *
   * @author Lim, Seng Yew
   */
  private void addAlgorithmSettings(InspectionFamily largePadInspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    // Minimum leading slope along.
    AlgorithmSetting minLeadingSlopeAlong = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG,
      displayOrder++,
      4.5f, // default
      0.f, // min
      25.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MINIMUM_LEADING_SLOPE_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MINIMUM_LEADING_SLOPE_ALONG)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MINIMUM_LEADING_SLOPE_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(minLeadingSlopeAlong);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minLeadingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minLeadingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum leading slope along.
    AlgorithmSetting maxLeadingSlopeAlong = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG,
      displayOrder++,
      255.f, // default
      0.f, // min
      255.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MAXIMUM_LEADING_SLOPE_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MAXIMUM_LEADING_SLOPE_ALONG)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MAXIMUM_LEADING_SLOPE_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(maxLeadingSlopeAlong);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          maxLeadingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          maxLeadingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum trailing slope along.
    AlgorithmSetting minTrailingSlopeAlong = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG,
      displayOrder++,
      4.5f, // default
      0.f, // min
      25.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MINIMUM_TRAILING_SLOPE_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MINIMUM_TRAILING_SLOPE_ALONG)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MINIMUM_TRAILING_SLOPE_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(minTrailingSlopeAlong);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minTrailingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minTrailingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum trailing slope along.
    AlgorithmSetting maxTrailingSlopeAlong = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG,
      displayOrder++,
      255.f, // default
      0.f, // min
      255.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MAXIMUM_TRAILING_SLOPE_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MAXIMUM_TRAILING_SLOPE_ALONG)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MAXIMUM_TRAILING_SLOPE_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(maxTrailingSlopeAlong);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          maxTrailingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          maxTrailingSlopeAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
    
    // Minimum slope sum along.
    AlgorithmSetting minSlopeSumChangesAlong = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG,
      displayOrder++,
      9.f, // default
      0.f, // min
      25.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MINIMUM_SLOPE_SUM_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MINIMUM_SLOPE_SUM_ALONG)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MINIMUM_SLOPE_SUM_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(minSlopeSumChangesAlong);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minSlopeSumChangesAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minSlopeSumChangesAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum slope sum along.
    AlgorithmSetting maxSlopeSumChangesAlong = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG,
      displayOrder++,
      255.f, // default
      0.f, // min
      255.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MAXIMUM_SLOPE_SUM_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MAXIMUM_SLOPE_SUM_ALONG)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MAXIMUM_SLOPE_SUM_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(maxSlopeSumChangesAlong);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          maxSlopeSumChangesAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          maxSlopeSumChangesAlong,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum leading slope across.
    AlgorithmSetting minLeadingSlopeAcross = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS,
      displayOrder++,
      4.5f, // default
      0.f, // min
      25.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MINIMUM_LEADING_SLOPE_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MINIMUM_LEADING_SLOPE_ACROSS)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MINIMUM_LEADING_SLOPE_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(minLeadingSlopeAcross);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minLeadingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minLeadingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum leading slope across.
    AlgorithmSetting maxLeadingSlopeAcross = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS,
      displayOrder++,
      255.f, // default
      0.f, // min
      255.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MAXIMUM_LEADING_SLOPE_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MAXIMUM_LEADING_SLOPE_ACROSS)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MAXIMUM_LEADING_SLOPE_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(maxLeadingSlopeAcross);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          maxLeadingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          maxLeadingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END    

    // Minimum trailing slope across.
    AlgorithmSetting minTrailingSlopeAcross = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS,
      displayOrder++,
      4.5f, // default
      0.f, // min
      25.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MINIMUM_TRAILING_SLOPE_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MINIMUM_TRAILING_SLOPE_ACROSS)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MINIMUM_TRAILING_SLOPE_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(minTrailingSlopeAcross);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minTrailingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minTrailingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum trailing slope across.
    AlgorithmSetting maxTrailingSlopeAcross = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS,
      displayOrder++,
      255.f, // default
      0.f, // min
      255.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MAXIMUM_TRAILING_SLOPE_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MAXIMUM_TRAILING_SLOPE_ACROSS)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MAXIMUM_TRAILING_SLOPE_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(maxTrailingSlopeAcross);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          maxTrailingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          maxTrailingSlopeAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
    
    // Minimum slope sum across.
    AlgorithmSetting minSlopeSumChangesAcross = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS,
      displayOrder++,
      9.f, // default
      0.f, // min
      25.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MINIMUM_SLOPE_SUM_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MINIMUM_SLOPE_SUM_ACROSS)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MINIMUM_SLOPE_SUM_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(minSlopeSumChangesAcross);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          minSlopeSumChangesAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          minSlopeSumChangesAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum slope sum across.
    AlgorithmSetting maxSlopeSumChangesAcross = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS,
      displayOrder++,
      255.f, // default
      0.f, // min
      255.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_OPEN_(MAXIMUM_SLOPE_SUM_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_OPEN_(MAXIMUM_SLOPE_SUM_ACROSS)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_OPEN_(MAXIMUM_SLOPE_SUM_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(maxSlopeSumChangesAcross);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                          maxSlopeSumChangesAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS);
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                          maxSlopeSumChangesAcross,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Main entry point for Large Pad Open.
   * Added on 29-May-2009
   *
   * @author Lim, Seng Yew
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    // Large Pad runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    // Get the image for processing
    Image imageToInspect = reconstructedPadSlice.getOrthogonalImage();

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

      BooleanRef jointPassed = new BooleanRef(true);

      // Check for opens based on second derivative area.
      detectOpensAlong(imageToInspect, subtype, padSlice, jointInspectionData, jointPassed);
      detectOpensAcross(imageToInspect, subtype, padSlice, jointInspectionData, jointPassed);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }

  /**
   * Detects opens along located pad
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization.
   * 
   * @author Lim, Seng Yew
   */
  private void detectOpensAlong(Image image,
                                Subtype subtype,
                                SliceNameEnum sliceNameEnum,
                                JointInspectionData jointInspectionData,
                                BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the joint inspection result.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    JointMeasurement leadingSlopeMeasurement = LargePadMeasurementAlgorithm.getLeadingSlopeAlongMeasurement(
        jointInspectionData, sliceNameEnum);
    
    // Check if the leading slope along within acceptable minimum & maximum limits.
    final float MINIMUM_LEADING_SLOPE_ALONG =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG);
    final float MAXIMUM_LEADING_SLOPE_ALONG =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG);    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            leadingSlopeMeasurement,
                                            MINIMUM_LEADING_SLOPE_ALONG,
                                            MAXIMUM_LEADING_SLOPE_ALONG,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD)
                                            == false)
    {
      jointPassed.setValue(false);
    }

    JointMeasurement trailingSlopeMeasurement = LargePadMeasurementAlgorithm.getTrailingSlopeAlongMeasurement(
        jointInspectionData, sliceNameEnum);

    // Check if the trailing slope along within acceptable minimum & maximum limits.
    final float MINIMUM_TRAILING_SLOPE_ALONG =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG);
    final float MAXIMUM_TRAILING_SLOPE_ALONG =
      (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG);    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            trailingSlopeMeasurement,
                                            MINIMUM_TRAILING_SLOPE_ALONG,
                                            MAXIMUM_TRAILING_SLOPE_ALONG,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD)
                                            == false)
    {
      jointPassed.setValue(false);
    }

    JointMeasurement slopeSumMeasurement = LargePadMeasurementAlgorithm.getSlopeSumAlongMeasurement(
        jointInspectionData, sliceNameEnum);

    // Check if the slope sum along within acceptable minimum & maximum limits.
    final float MINIMUM_SLOPE_SUM_ALONG =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG);
    final float MAXIMUM_SLOPE_SUM_ALONG =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG);
    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            slopeSumMeasurement,
                                            MINIMUM_SLOPE_SUM_ALONG,
                                            MAXIMUM_SLOPE_SUM_ALONG,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD)
                                            == false)
    {
      jointPassed.setValue(false);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Detects opens across located pad
   * Lim Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization.
   *
   * @author Lim, Seng Yew
   */
  private void detectOpensAcross(Image image,
                                Subtype subtype,
                                SliceNameEnum sliceNameEnum,
                                JointInspectionData jointInspectionData,
                                BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Get the joint inspection result.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check if the leading slope across within acceptable minimum & maximum limits.
    JointMeasurement leadingSlopeMeasurement = LargePadMeasurementAlgorithm.getLeadingSlopeAcrossMeasurement(
        jointInspectionData, sliceNameEnum);
    final float MINIMUM_LEADING_SLOPE_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS);
    final float MAXIMUM_LEADING_SLOPE_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS);    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            leadingSlopeMeasurement,
                                            MINIMUM_LEADING_SLOPE_ACROSS,
                                            MAXIMUM_LEADING_SLOPE_ACROSS,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD)
                                            == false)
    {
      jointPassed.setValue(false);
    }

    // Check if the trailing slope across within acceptable minimum & maximum limits.
    JointMeasurement trailingSlopeMeasurement = LargePadMeasurementAlgorithm.getTrailingSlopeAcrossMeasurement(
        jointInspectionData, sliceNameEnum);
    final float MINIMUM_TRAILING_SLOPE_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS);
    final float MAXIMUM_TRAILING_SLOPE_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS);    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            trailingSlopeMeasurement,
                                            MINIMUM_TRAILING_SLOPE_ACROSS,
                                            MAXIMUM_TRAILING_SLOPE_ACROSS,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD)
                                            == false)
    {
      jointPassed.setValue(false);
    }

    // Check if the slope sum across within acceptable minimum & maximum limits.
    JointMeasurement slopeSumMeasurement = LargePadMeasurementAlgorithm.getSlopeSumAcrossMeasurement(
        jointInspectionData, sliceNameEnum);
    final float MINIMUM_SLOPE_SUM_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS);
    final float MAXIMUM_SLOPE_SUM_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS);    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            slopeSumMeasurement,
                                            MINIMUM_SLOPE_SUM_ACROSS,
                                            MAXIMUM_SLOPE_SUM_ACROSS,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD)
                                            == false)
    {
      jointPassed.setValue(false);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * @author Siew Yeng
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);
    
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      learnSlopeSettings(subtype, padSlice);
    }
  }
  
  /**
   * Learns the slope settings.
   * @author Siew Yeng
   */
  private void learnSlopeSettings(Subtype subtype, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Get all the learned "Leading Slope Along" values.
    float[] leadingSlopeAlongValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                              MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG,
                                                                              true,
                                                                              true);

    // We can only do the IQR statistics if we have at least two data points.
    if (leadingSlopeAlongValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(leadingSlopeAlongValues);
      leadingSlopeAlongValues = null;
      float medianleadingSlopeAlong = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG))
      {
        float minimumAcceptableLeadingSlopeAlong = medianleadingSlopeAlong - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG, minimumAcceptableLeadingSlopeAlong);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG))
      {
        float maximumAcceptableLeadingSlopeAlong = medianleadingSlopeAlong + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG, maximumAcceptableLeadingSlopeAlong);
      }
    }

    // Get all the learned "Trailing Slope Along" values.
    float[] trailingSlopeAlongValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                             MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG,
                                                                             true,
                                                                             true);

    // We can only do the IQR statistics if we have at least two data points.
    if (trailingSlopeAlongValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(trailingSlopeAlongValues);
      trailingSlopeAlongValues = null;
      float medianTrailingSlopeAlong = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG))
      {
        float minimumAcceptableTrailingSlopeAlong = medianTrailingSlopeAlong - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG, minimumAcceptableTrailingSlopeAlong);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG))
      {
        float maximumAcceptableTrailingSlopeAlong = medianTrailingSlopeAlong + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG, maximumAcceptableTrailingSlopeAlong);
      }
    }
    
    // Get all the learned "Slope Sum Along" values.
    float[] slopeSumAlongValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                             MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG,
                                                                             true,
                                                                             true);

    // We can only do the IQR statistics if we have at least two data points.
    if (slopeSumAlongValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(slopeSumAlongValues);
      slopeSumAlongValues = null;
      float medianSlopeSumAlong = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG))
      {
        float minimumAcceptableSlopeSumAlong = medianSlopeSumAlong - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG, minimumAcceptableSlopeSumAlong);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG))
      {
        float maximumAcceptableSlopeSumAlong = medianSlopeSumAlong + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG, maximumAcceptableSlopeSumAlong);
      }
    }

    // Get all the learned "Leading Slope Across" values.
    float[] leadingSlopeAcrossValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (leadingSlopeAcrossValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(leadingSlopeAcrossValues);
      leadingSlopeAcrossValues = null;
      float medianLeadingSlopeAcross = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS))
      {
        float minimumAcceptableLeadingSlopeAcross = medianLeadingSlopeAcross - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS,
                                minimumAcceptableLeadingSlopeAcross);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS))
      {
        float maximumAcceptableLeadingSlopeAcross = medianLeadingSlopeAcross + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS,
                                maximumAcceptableLeadingSlopeAcross);
      }
    }
    
    // Get all the learned "Trailing Slope Across" values.
    float[] trailingSlopeAcrossValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (trailingSlopeAcrossValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(trailingSlopeAcrossValues);
      trailingSlopeAcrossValues = null;
      float medianTrailingSlopeAcross = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS))
      {
        float minimumAcceptableTrailingSlopeAcross = medianTrailingSlopeAcross - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS,
                                minimumAcceptableTrailingSlopeAcross);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS))
      {
        float maximumAcceptableTrailingSlopeAcross = medianTrailingSlopeAcross + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS,
                                maximumAcceptableTrailingSlopeAcross);
      }
    }
    
    // Get all the learned "Slope Sum Across" values.
    float[] slopeSumAcrossValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS,
                                                 true,
                                                 true);
    
    // We can only do the IQR statistics if we have at least two data points.
    if (slopeSumAcrossValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(slopeSumAcrossValues);
      slopeSumAcrossValues = null;
      float medianSlopeSumAcross = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS))
      {
        float minimumAcceptableSlopeSumAcross = medianSlopeSumAcross - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS,
                                minimumAcceptableSlopeSumAcross);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS))
      {
        float maximumAcceptableSlopeSumAcross = medianSlopeSumAcross + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS,
                                maximumAcceptableSlopeSumAcross);
      }
    }
  }
  
  /**
   * Resets all learned defect settings back to their default values.
   * @author Siew Yeng
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS);
    }
  }
}
