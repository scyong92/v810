package com.axi.v810.business.imageAnalysis.largePad;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for large pads, such as the center joint of QFNs.
 *
 * @author Patrick Lacz
 */
public class LargePadInspectionFamily extends InspectionFamily
{
  /**
   * @author Patrick Lacz
   */
  public LargePadInspectionFamily()
  {
    super(InspectionFamilyEnum.LARGE_PAD);

    // Associate the individual algorithms with the family.

    // Measurement
    Algorithm largePadMeasurementAlgorithm = new LargePadMeasurementAlgorithm();
    addAlgorithm(largePadMeasurementAlgorithm);

    // Short.
    Algorithm largePadShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.LARGE_PAD);
    addAlgorithm(largePadShortAlgorithm);

    // Voiding
    Algorithm largePadVoidingAlgorithm = new LargePadVoidingAlgorithm(this);
    addAlgorithm(largePadVoidingAlgorithm);

    /** @todo PWL : finish up the flood voiding algorithm and use it instead of the large pad voiding algorithm. */
    /** @todo GLB : existing projects with learned data will assert if the new algorithm is used;
                    update SubtypeSettingsReader as needed */
//    Algorithm largePadVoidingAlgorithm = new FloodVoidingAlgorithm(InspectionFamilyEnum.LARGE_PAD);
//    addAlgorithm(largePadVoidingAlgorithm);

    // Open
    // Added by Lim, Seng Yew - 28-May-2009
    Algorithm largePadOpenAlgorithm = new LargePadOpenAlgorithm(this);
    addAlgorithm(largePadOpenAlgorithm);

    // Insufficient
    // Added by Lim, Seng Yew - 28-May-2009
    Algorithm largePadInsufficientAlgorithm = new LargePadInsufficientAlgorithm(this);
    addAlgorithm(largePadInsufficientAlgorithm);
}

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);
    
    Collection<SliceNameEnum> inspectedSlices = new LinkedList<SliceNameEnum>();
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    
    inspectedSlices.add(SliceNameEnum.PAD);
    
    if (jointTypeEnum.equals(JointTypeEnum.SINGLE_PAD) ||
        jointTypeEnum.equals(JointTypeEnum.LGA))
    {
      int numberOfUserDefinedSlice = Algorithm.getNumberOfSlice(subtype);
      if (numberOfUserDefinedSlice == 1)
      {
        inspectedSlices.add(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
      }
      else if (numberOfUserDefinedSlice == 2)
      {
        inspectedSlices.add(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
        inspectedSlices.add(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
      }
    }

    return inspectedSlices;
  }

  /**
   * Gets the applicable default SliceNameEnum for the Pad slice for the Large Pad inspection family.
   *
   * @author Patrick Lacz
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Use the standard pad slice.
    return SliceNameEnum.PAD;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }


  /**
   * @author Patrick Lacz
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return true;
  }
}
