package com.axi.v810.business.imageAnalysis.largePad;

import java.util.*;

import com.axi.guiUtil.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelDesc.Pad;
import com.axi.v810.hardware.*;
import ij.ImagePlus;
import ij.process.ImageStatistics;

/**
 * Based on the 5dx Paste Voiding algorithm.
 * The Large Pad Voiding algorithm is very similar to the Grid Array voiding algorithm.
 * First, an expected image is created. The area of the pad is divided into sub-regions.
 * In each sub-region, a percentile-based sample is taken. That graylevel is used as the
 * background color for the entire sub-region.
 * <p>
 * The regions are combined into one image which acts as the 'expected' graylevel.
 * Like grid array voiding, the actual and expected images are used to create a thickness
 * image. Void pixels are identified by applying a threshold to this image.
 * <p>
 * This algorithm is expected to work well for pads that have a uniform graylevel. * Possible future improvements focus on handling non-uniform looking regions.
 * Interpolation of graylevels or possibly learning the background would be straight-forward
 * improvements. Also, a test of circularity based on David Gine's research could be used
 * to distinguish ellipitcal voids from the (normally) rectangular shading changes in the
 * component.
 *
 * @author Patrick Lacz
 */
public class LargePadVoidingAlgorithm extends Algorithm
{
  private static final float _VOID_PIXEL_INTENSITY = 200.f;

  private final int _ALGORITHM_VERSION = 1;
  
  private final static String _THICKNESS_METHOD = "Thickness Method";
  public final static String _FLOODFILL_METHOD = "FloodFill Method";
  public final static String _VARIABLE_PAD_METHOD = "Variable Pad Method";
  public final static String _CIRCULAR_VOID_METHOD = "Circular Void Method";
  private final static String _TRUE = "True";
  private final static String _FALSE = "False";
  private final static ArrayList<String> LARGE_PAD_VOIDING_CHOICES = new ArrayList<String>(Arrays.asList(_THICKNESS_METHOD,_FLOODFILL_METHOD, _VARIABLE_PAD_METHOD, _CIRCULAR_VOID_METHOD));
  private final static ArrayList<String> USING_MASKING_CHOICES = new ArrayList<String>(Arrays.asList(_TRUE,_FALSE));
  private final static String GAUSSIAN_BLUR_COMMAND = "Gaussian Blur...";
  private static float RADIUS_COMMAND = 25.0f;
  private final static String _UNTESTED_BORDER_SIZE_STANDARD_METHOD = "Standard";
  private final static String _UNTESTED_BORDER_SIZE_CUSTOMIZE_METHOD = "Customize";
  private final static ArrayList<String> UNTESTED_BORDER_SIZE_CHOICES = new ArrayList<String>(Arrays.asList(_UNTESTED_BORDER_SIZE_STANDARD_METHOD,_UNTESTED_BORDER_SIZE_CUSTOMIZE_METHOD));
  public final static String _MASKING_MATCHING_METHOD = "Matching Method";
  public final static String _MASKING_FIXED_METHOD = "Fixed Method";
  private final static ArrayList<String> LARGE_PAD_MASKING_LOCATOR_CHOICES = new ArrayList<String>(Arrays.asList(_MASKING_MATCHING_METHOD,_MASKING_FIXED_METHOD));
  
  private final static ArrayList<String> _ENABLE_SUBREGION_CHOICES = new ArrayList<String>(Arrays.asList(_TRUE,_FALSE));
  private List<AlgorithmSettingEnum> _subSubtypeAlgorithmSettingEnumsIgnoreList = new ArrayList();
  /**
   * @author Patrick
   */
  public LargePadVoidingAlgorithm(InspectionFamily largePadInspectionFamily)
  {
    super(AlgorithmEnum.VOIDING, InspectionFamilyEnum.LARGE_PAD);

    int displayOrder = 0;

    AlgorithmSetting voidingVersion = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION, // setting enum
        displayOrder++, // display order,
        "3",
        new ArrayList<>(Arrays.asList(new String[]{"1","2","3"})),
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_VERSION)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_VERSION)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_VERSION)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(voidingVersion);
    _subSubtypeAlgorithmSettingEnumsIgnoreList.add(voidingVersion.getAlgorithmSettingEnum());
    
    AlgorithmSetting enableSubRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION, // setting enum
        displayOrder++, // display order,
        _TRUE,
        _ENABLE_SUBREGION_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_ENABLE_SUBREGION)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_ENABLE_SUBREGION)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_ENABLE_SUBREGION)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(enableSubRegion);
    
     //Jack Hwee
     AlgorithmSetting voidDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD, // setting enum
        displayOrder++, // display order,
        _FLOODFILL_METHOD,
        LARGE_PAD_VOIDING_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_METHOD)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_METHOD)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_METHOD)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(voidDetection);
    
     //Jack Hwee
     AlgorithmSetting maskingVoidDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES, // setting enum
        displayOrder++, // display order,
        _FALSE,
        USING_MASKING_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_USE_MASKING)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_USE_MASKING)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_USE_MASKING)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(maskingVoidDetection);
    
    // Indicates method to locate the mask image to odd-shaped single pad
    AlgorithmSetting maskingLocatorMethod = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_LOCATOR_METHOD,
      displayOrder++,
      _MASKING_MATCHING_METHOD,
      LARGE_PAD_MASKING_LOCATOR_CHOICES,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_USE_MASKING_LOCATOR_METHOD)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_USE_MASKING_LOCATOR_METHOD)_KEY", // detailed description URL Key
      "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_USE_MASKING_LOCATOR_METHOD)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.STANDARD,
      _ALGORITHM_VERSION);
    addAlgorithmSetting(maskingLocatorMethod);
    _subSubtypeAlgorithmSettingEnumsIgnoreList.add(maskingLocatorMethod.getAlgorithmSettingEnum());
    
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(1.2f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS, // really MM
        "HTML_DESC_LARGEPAD_VOIDING_(THICKNESS_THRESHOLD_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(THICKNESS_THRESHOLD_PAD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(THICKNESS_THRESHOLD_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION));
    
    AlgorithmSetting sensitivityForVariablePadBorder = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY,
        displayOrder++,
        3, // default value
        1, // minimum value
        10, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_VARIABLE_PAD_BORDER_SENSITIVITY)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_VARIABLE_PAD_BORDER_SENSITIVITY)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_VARIABLE_PAD_BORDER_SENSITIVITY)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(sensitivityForVariablePadBorder);
    
    AlgorithmSetting sensitivityForVariablePadVoiding = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY,
        displayOrder++,
        8, // default value
        1, // minimum value
        10, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_VARIABLE_PAD_VOIDING_SENSITIVITY)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_VARIABLE_PAD_VOIDING_SENSITIVITY)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_VARIABLE_PAD_VOIDING_SENSITIVITY)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(sensitivityForVariablePadVoiding);
    
    AlgorithmSetting thresholdOfGaussianBlur = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR,
        displayOrder++,
        25.0f, // default value
        5.0f, // minimum value
        500.0f, // maximum value //Anthony Fong
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_GAUSSIAN_BLUR_THRESHOLD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_GAUSSIAN_BLUR_THRESHOLD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_GAUSSIAN_BLUR_THRESHOLD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(thresholdOfGaussianBlur);
    
    AlgorithmSetting numberOfVoidLayer = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER,
        displayOrder++,
        1, // default value
        1, // minimum value
        3, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_NUMBER_OF_IMAGE_LAYER)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_NUMBER_OF_IMAGE_LAYER)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_NUMBER_OF_IMAGE_LAYER)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(numberOfVoidLayer);
    
      AlgorithmSetting voidBorderThreshold = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE,
        displayOrder++,
        8, // default value
        1, // minimum value
        10, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_ONE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_ONE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_ONE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(voidBorderThreshold);
    
    AlgorithmSetting layerTwoSensitivity = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO,
        displayOrder++,
        1.0f, // default value
        0.0f, // minimum value
        3.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_TWO)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_TWO)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_TWO)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(layerTwoSensitivity);
    
      AlgorithmSetting layerThreeSensitivity = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE,
        displayOrder++,
        1.0f, // default value
        0.0f, // minimum value
        3.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_THREE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_THREE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_THREE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(layerThreeSensitivity);
    
    AlgorithmSetting additionalLayerSensitivity = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL,
        displayOrder++,
        0, // default value
        0, // minimum value
        10, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_ADDITIONAL)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_ADDITIONAL)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_SENSITIVITY_LAYER_ADDITIONAL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(additionalLayerSensitivity);

    Collection<AlgorithmSetting> circularVoidAlgorithms = CircularVoidingAlgorithm.createSharedVoidingAlgorithmSettings(largePadInspectionFamily, this, 
                                                                                                                                   JointTypeEnum.SINGLE_PAD, SliceNameEnum.PAD, 
                                                                                                                                   displayOrder, _ALGORITHM_VERSION);
    displayOrder += circularVoidAlgorithms.size();
    
    // Joint Voiding Threshold
    // If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as
    // being voided. This value is usually larger than the component-level voiding.
    AlgorithmSetting jointVoiding = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD,
        displayOrder++,
        5.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(jointVoiding);
    _subSubtypeAlgorithmSettingEnumsIgnoreList.add(jointVoiding.getAlgorithmSettingEnum());

    // Joint Individual Voiding Threshold
    // If the area of pixels classified as voiding is greater than this percent, the joint should be indicted as
    // being voided. This value is usually larger than the component-level voiding.
    AlgorithmSetting jointIndividualVoiding = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD,
        displayOrder++,
        4.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);   
    addAlgorithmSetting(jointIndividualVoiding);
    _subSubtypeAlgorithmSettingEnumsIgnoreList.add(jointIndividualVoiding.getAlgorithmSettingEnum());
    
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                  jointVoiding,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.LARGE_PAD_VOIDING_PERCENT);

    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                  jointIndividualVoiding,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT);
    
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                  jointVoiding,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.LARGE_PAD_VOIDING_PERCENT);

    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                  jointIndividualVoiding,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT);
    
     AlgorithmSetting jointIndividualVoidingAcceptableGapDistance = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        30.0f, // maximum value
        MeasurementUnitsEnum.MILS,
        "HTML_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(ACCEPTABLE_GAP_DISTANCE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(ACCEPTABLE_GAP_DISTANCE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(ACCEPTABLE_GAP_DISTANCE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);   
    addAlgorithmSetting(jointIndividualVoidingAcceptableGapDistance);
    _subSubtypeAlgorithmSettingEnumsIgnoreList.add(jointIndividualVoidingAcceptableGapDistance.getAlgorithmSettingEnum());
    
    AlgorithmSetting jointVoidingUserDefinedSlice1 = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1,
        displayOrder++,
        5.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_ONE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_ONE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_ONE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
   jointVoidingUserDefinedSlice1.setSpecificSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
   jointVoidingUserDefinedSlice1.filter(JointTypeEnum.LGA);
   addAlgorithmSetting(jointVoidingUserDefinedSlice1);
   _subSubtypeAlgorithmSettingEnumsIgnoreList.add(jointVoidingUserDefinedSlice1.getAlgorithmSettingEnum());
   
   AlgorithmSetting jointIndividualVoidingUserDefinedSlice1 = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1,
        displayOrder++,
        4.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_ONE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_ONE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_ONE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);   
   jointIndividualVoidingUserDefinedSlice1.setSpecificSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
   jointIndividualVoidingUserDefinedSlice1.filter(JointTypeEnum.LGA);
   addAlgorithmSetting(jointIndividualVoidingUserDefinedSlice1);
   _subSubtypeAlgorithmSettingEnumsIgnoreList.add(jointIndividualVoidingUserDefinedSlice1.getAlgorithmSettingEnum());
   
   largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                  jointVoidingUserDefinedSlice1,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
                                                                  MeasurementEnum.LARGE_PAD_VOIDING_PERCENT);

    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                  jointIndividualVoidingUserDefinedSlice1,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
                                                                  MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT);
    
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                  jointVoidingUserDefinedSlice1,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
                                                                  MeasurementEnum.LARGE_PAD_VOIDING_PERCENT);

    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                  jointIndividualVoidingUserDefinedSlice1,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
                                                                  MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT);
    
   AlgorithmSetting jointVoidingUserDefinedSlice2 = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2,
        displayOrder++,
        5.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_TWO)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_TWO)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(FAIL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_TWO)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);
   jointVoidingUserDefinedSlice2.setSpecificSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
   jointVoidingUserDefinedSlice2.filter(JointTypeEnum.LGA);
   addAlgorithmSetting(jointVoidingUserDefinedSlice2);
   _subSubtypeAlgorithmSettingEnumsIgnoreList.add(jointVoidingUserDefinedSlice2.getAlgorithmSettingEnum());
   
   AlgorithmSetting jointIndividualVoidingUserDefinedSlice2 = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2,
        displayOrder++,
        4.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_TWO)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_TWO)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_INDIVIDUAL_VOIDING_(FAIL_INDIVIDUAL_JOINT_PERCENT_PAD_USER_DEFINED_SLICE_TWO)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION);   
   jointIndividualVoidingUserDefinedSlice2.setSpecificSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
   jointIndividualVoidingUserDefinedSlice2.filter(JointTypeEnum.LGA);
   addAlgorithmSetting(jointIndividualVoidingUserDefinedSlice2);
   _subSubtypeAlgorithmSettingEnumsIgnoreList.add(jointIndividualVoidingUserDefinedSlice2.getAlgorithmSettingEnum());
   
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                  jointVoidingUserDefinedSlice2,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
                                                                  MeasurementEnum.LARGE_PAD_VOIDING_PERCENT);

    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                  jointIndividualVoidingUserDefinedSlice2,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
                                                                  MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT);
    
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                  jointVoidingUserDefinedSlice2,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
                                                                  MeasurementEnum.LARGE_PAD_VOIDING_PERCENT);

    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.LGA,
                                                                  jointIndividualVoidingUserDefinedSlice2,
                                                                  SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
                                                                  MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT);
    
  
   
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(500.0f), // default value
        MathUtil.convertMilsToMillimeters(5.0f), // minimum value
        MathUtil.convertMilsToMillimeters(1000.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_VOIDING_(BACKGROUND_REGION_SIZE_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(BACKGROUND_REGION_SIZE_PAD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(BACKGROUND_REGION_SIZE_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        _ALGORITHM_VERSION));

    // The Percent-of-area to count the joint towards the component-fail threshold. Generally this
    // percentage is lower than the Joint Voiding Threshold.
    AlgorithmSetting componentVoiding = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD,
        displayOrder++,
        100.0f, // default value
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_VOIDING_(COMPONENT_FAIL_PERCENT_PAD)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(COMPONENT_FAIL_PERCENT_PAD)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(COMPONENT_FAIL_PERCENT_PAD)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);

    addAlgorithmSetting(componentVoiding);
    _subSubtypeAlgorithmSettingEnumsIgnoreList.add(componentVoiding.getAlgorithmSettingEnum());
    
    largePadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SINGLE_PAD,
                                                                  componentVoiding,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT);
    // Noise Reduction
    // How much to try to compensate for noise.
    // 0 = no noise reduction
    // 1 = apply blur
    // 2+ = apply blur & NR-1 cycles of opening (dilate/erode cycles) to the void pixels image.
    // The opening will remove smaller features from the image and generally make the regions appear
    // more blocky than without NR.
    //  I expect that this value will normally be 0 to 2.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION,
        displayOrder++,
        3, // default value
        0, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(NOISE_REDUCTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(NOISE_REDUCTION)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(NOISE_REDUCTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));

    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION_FOR_FLOODFILL,
        displayOrder++,
        1, // default value
        1, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(NOISE_REDUCTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(NOISE_REDUCTION)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(NOISE_REDUCTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));

    /* @author Bee Hoon
     * Separate the large pad xy limit and xy offset limit from Untested Border Size
     * Left Untested Border Side Offset is the offset value from the left side of joint ROI
     * Bottom Untested Border Side Offset is the offset value from the bottom side of joint ROI
     * Right Untested Border Side Offset is the offset value from the right side of joint ROI
     * Top Untested Border Side Offset is the offset value from the top side of joint ROI
     */
      AlgorithmSetting untestedBorderSize = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD, // setting enum
        displayOrder++, // display order,
        _UNTESTED_BORDER_SIZE_STANDARD_METHOD,
        UNTESTED_BORDER_SIZE_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_METHOD)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_METHOD)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_METHOD)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(untestedBorderSize);
    
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE,
        displayOrder++,
        0.0f,
        0.0f,
        1.0f,
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));

    // @author Bee Hoon
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET,
        displayOrder++,
        0.0f,
        0.0f,
        1.0f,
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSETH)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));
    
    // @author Bee Hoon
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET,
        displayOrder++,
        0.0f,
        0.0f,
        1.0f,
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));
    
    // @author Bee Hoon
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET,
        displayOrder++,
        0.0f,
        0.0f,
        1.0f,
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));
    
    // @author Bee Hoon
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET,
        displayOrder++,
        0.0f,
        0.0f,
        1.0f,
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));
    
    
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE,
        displayOrder++,
        100f * 0.75f * (1.f - (float)Math.PI / 4.f), // default value  - this came from Patrick.  Not sure how he settled on this.
        0.0f, // min
        100.0f,  // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_LARGEPAD_VOIDING_(LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE)_KEY", // description URL key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE)_KEY", // desailed description URL key
        "IMG_DESC_LARGEPAD_VOIDING_(LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));
    
      AlgorithmSetting multiThresholdVoidDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_USING_MULTI_THRESHOLD, // setting enum
        displayOrder++, // display order,
        _FALSE,
        USING_MASKING_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_MULTI_THESHOLD)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_MULTI_THESHOLD)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_MULTI_THESHOLD)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(multiThresholdVoidDetection);
    
    AlgorithmSetting fitPolynomialVoidDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_USING_FIT_POLYNOMIAL, // setting enum
        displayOrder++, // display order,
        _FALSE,
        USING_MASKING_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_FIT_POLYNOMIAL_THRESHOLD)_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_FIT_POLYNOMIAL_THRESHOLD)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_VOIDING_(VOIDING_FLOODFILL_FIT_POLYNOMIAL_THRESHOLD)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(fitPolynomialVoidDetection);
    
    //Wei Chin - XCR-2387
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveDiagnoticVoidImageAlgorithmSetting(displayOrder, _ALGORITHM_VERSION));
    _subSubtypeAlgorithmSettingEnumsIgnoreList.add(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES);
    
    //@Edited By Kee Chin Seong - Circular Voiding Algorithm
    for (AlgorithmSetting algSetting : circularVoidAlgorithms)
      addAlgorithmSetting(algSetting);
    
    addMeasurementEnums();
  }

  /**
   * @author Peter Esbensen
   * @Edited By Kee Chin Seong - Circular Voiding Algorithm
   * 
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_VOIDING_PERCENT);

    if (Config.isComponentLevelClassificationEnabled())
      _componentMeasurementEnums.add(MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT);
    else
      _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT);

    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT);
    
    _jointMeasurementEnums.add(MeasurementEnum.SHARED_VOIDING_DEFINITE_VOIDING_GREY_LEVEL);
    _jointMeasurementEnums.add(MeasurementEnum.SHARED_VOIDING_DIFFERENCE_FROM_NOMINAL_PERCENTAGE);
    _jointMeasurementEnums.add(MeasurementEnum.SHARED_VOIDING_EXONERATED_VOIDING_GREY_LEVEL);
  }

  /**
   * @author Patrick Lacz
   * @Edited By Kee Chin Seong - Circular Voiding Algorithm
   */
  public void classifyJoints(
      ReconstructedImages reconstructedImages,
      List<JointInspectionData> jointInspectionDataObjects)
      throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    
    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtypeOfJoints);
    
    LargePadMeasurementAlgorithm measurementAlgo = (LargePadMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.LARGE_PAD, AlgorithmEnum.MEASUREMENT);
    Collection<SliceNameEnum> inspectedSlices = measurementAlgo.getInspectionSliceOrder(subtypeOfJoints);

    int noiseReduction = (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION);
    
    Boolean usingMasking = isUsingMaskingMethod(subtypeOfJoints);
    
    Boolean usingMultiThresholdDetection = isUsingMultiThresholdDetection(subtypeOfJoints);
    
    Boolean usingFitPolynomialDetection = isUsingFitPolynomialDetection(subtypeOfJoints);

    int version = Integer.parseInt((String)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION));
    
    if(isThicknessMethod(subtypeOfJoints))
    {
      classifySlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.PAD,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD),
                    noiseReduction,
                    MILIMETER_PER_PIXEL);
        
      // Added by Jack Hwee
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1))
      {
        classifySlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                    noiseReduction,
                    MILIMETER_PER_PIXEL);
      }
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2))
      {
        classifySlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                    noiseReduction,
                    MILIMETER_PER_PIXEL);
      }
    }            
    else if(isFloodFillMethod(subtypeOfJoints))
    {
      if(version == 1)
      {
        classifyFloodFillSlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.PAD,                     
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD),
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER),
                         usingMasking,
                         usingMultiThresholdDetection,
                         usingFitPolynomialDetection,
                         MILIMETER_PER_PIXEL);
      }
      else if(version == 2 || version == 3)//Siew Yeng - XCR-3603
      {
        classifyFloodFillSlice2(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.PAD,                     
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD),
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER),
                         usingMasking,
                         usingMultiThresholdDetection,
                         usingFitPolynomialDetection,
                         MILIMETER_PER_PIXEL);
      }
       
      // Added by Jack Hwee
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1))
      {
        if(version == 1)
        {
          classifyFloodFillSlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,                     
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER),
                         usingMasking,
                         usingMultiThresholdDetection,
                         usingFitPolynomialDetection,
                         MILIMETER_PER_PIXEL);
        }
        else if(version == 2 || version == 3)//Siew Yeng - XCR-3637
        {
          classifyFloodFillSlice2(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,                     
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER),
                         usingMasking,
                         usingMultiThresholdDetection,
                         usingFitPolynomialDetection,
                         MILIMETER_PER_PIXEL);
        }
      }
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2))
      {
        if(version == 1)
        {
          classifyFloodFillSlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,                     
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER),
                         usingMasking,
                         usingMultiThresholdDetection,
                         usingFitPolynomialDetection,
                         MILIMETER_PER_PIXEL); 
        }
        else if(version == 2 || version == 3)//Siew Yeng - XCR-3637
        {
          classifyFloodFillSlice2(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,                     
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                        (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE),
                        (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER),
                         usingMasking,
                         usingMultiThresholdDetection,
                         usingFitPolynomialDetection,
                         MILIMETER_PER_PIXEL); 
        }
      }
    }
    else if(isVariablePadMethod(subtypeOfJoints))
    {
      classifyVariablePadSlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.PAD,                     
                      (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD),
                      (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD),
                      (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY),
                      (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY),
                      MILIMETER_PER_PIXEL
                      );
       
             // Added by Jack Hwee
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1))
      {
        classifyVariablePadSlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1,                     
                      (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                      (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1),
                      (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY),
                      (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY),
                      MILIMETER_PER_PIXEL
                      );
      }
      if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2))
      {
        classifyVariablePadSlice(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2,                     
                      (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                      (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2),
                      (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY),
                      (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY),
                      MILIMETER_PER_PIXEL
                      );
      }
    }
    // Added By Kee Chin Seong - Circular Voiding Algorithm
    else if(isUsingCircularVoidMethod(subtypeOfJoints))
    {
       try
       {  
         CircularVoidingAlgorithm.classifyJointsCircularVoid(reconstructedImages, jointInspectionDataObjects, SliceNameEnum.PAD);
       }
       catch(XrayTesterException ex)
       {
          //do nothing yet.
       }
    }
  }

  /**
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Patrick Lacz
   *//*
  private void classifySlice(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float subRegionSizeInMM,
      float voidThicknessThreshold,
      float voidAreaFailThreshold,
      float individualVoidAreaFailThreshold,
      int noiseReduction) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    Image sliceImage = slice.getOrthogonalImage();

    // determine settings from noise reduction.
    // noise reduction of 1 = blur the image, 2 and above : apply opening afterwards.
    boolean applyBlurToJointImages = noiseReduction >= 1;
    int numberOfTimesToOpenVoidPixelImage = Math.max(0, noiseReduction - 1);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    SliceNameEnum padSliceNameEnum = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);

    float untestedBorderSizeInMM = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE);
    int untestedBorderSizeInPixels = (int)Math.ceil(untestedBorderSizeInMM / AlgorithmUtil.getMillimetersPerPixel());
    float leftOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET);
    int leftOffsetInPixels = (int)Math.ceil(leftOffset / AlgorithmUtil.getMillimetersPerPixel());
    float bottomOffset= (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET);
    int bottomOffsetInPixels = (int)Math.ceil(bottomOffset / AlgorithmUtil.getMillimetersPerPixel());
    float rightOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET);
    int rightOffsetInPixels = (int)Math.ceil(rightOffset / AlgorithmUtil.getMillimetersPerPixel());
    float topOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET);
    int topOffsetInPixels = (int)Math.ceil(topOffset / AlgorithmUtil.getMillimetersPerPixel());

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);

    int jointIndex = 0;
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);
      RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(joint);

      // Untested Border Size Method - Standard or Customize
      if (isUntestedBorderSizeStandardMethod(subtypeOfJoints))
      {
        if ((jointRoi.getWidth() - 2*untestedBorderSizeInPixels < 5.0) || (jointRoi.getHeight() - 2*untestedBorderSizeInPixels < 5.0))
        {
          // @todo PWL : post warning about the untested region being too large
          LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                            new Object[] {joint.getFullyQualifiedPadName()});
          AlgorithmUtil.raiseAlgorithmWarning(warningText);
          continue;
        }
        
        jointRoi.setWidthKeepingSameCenter(jointRoi.getWidth() - 2*untestedBorderSizeInPixels);
        jointRoi.setHeightKeepingSameCenter(jointRoi.getHeight() - 2*untestedBorderSizeInPixels);
      }
      else if (isUntestedBorderSizeCustomizeMethod(subtypeOfJoints))
      {
        if ((jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels < 5.0) || 
            (jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels < 5.0))
        {
          LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                            new Object[] {joint.getFullyQualifiedPadName()});
          AlgorithmUtil.raiseAlgorithmWarning(warningText);
          continue;
        }
                
        jointRoi.setMinXY(jointRoi.getMinX() + leftOffsetInPixels, jointRoi.getMinY() + topOffsetInPixels);
        jointRoi.setWidthKeepingSameMinX(jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels);
        jointRoi.setHeightKeepingSameMinY(jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels);
      }

      // if we apply a blur, we need to use a different image/roi
      Image imageToUse = sliceImage;
      RegionOfInterest roiToUse = jointRoi;

      if (applyBlurToJointImages)
      {
        imageToUse = Filter.convolveLowpass(sliceImage, jointRoi);
        roiToUse = RegionOfInterest.createRegionFromImage(imageToUse);
      }
      else
      {
        imageToUse.incrementReferenceCount();
      }
      
      //Siew Yeng - Optimization needed in future.
      //1. Creating masking - code redundant in classifyFloodFillSlice function and this function.
      //2. usingMasking flag - too many if/else condition, can be optimize. Just need to check once and set the variable needed accordingly. 
      //                       For eg, roi and jointRoiForUsingMasking
      boolean usingMasking = isUsingMaskingMethod(subtypeOfJoints);
      
      Image sliceImageForMasking = slice.getOrthogonalImage();
      MaskImage createMaskImage =  MaskImage.getInstance();

      Image maskSliceImage = null;
      Image maskImage = null;
      RegionOfInterest adjustedJointRoiForMasking = null;
      RegionOfInterest jointRoiForUsingMask = null;
      
       // Store the percent voiding if using masking
      int numberOfPixelsTestedForMasking = 0;
      
      if (usingMasking)
      {
        if (createMaskImage.getMisalignedSubtypeMaskImage(joint.toString()) != null)
        {
          java.awt.image.BufferedImage misalignedMaskImage = createMaskImage.getMisalignedSubtypeMaskImage(joint.toString());
          maskImage = Image.createFloatImageFromBufferedImage(misalignedMaskImage);

          // get the imageCoordinate of horizontal and vertical edges of mask image
          ImageCoordinate x = createMaskImage.getSubtypeMaskImageXCoordinate(joint.toString());
          ImageCoordinate y = createMaskImage.getSubtypeMaskImageYCoordinate(joint.toString());
       
          int width = x.getX() - x.getY();
          int height = y.getX() - y.getY();
         
          RegionOfInterest cropRegion = null;
          
          // if the width and height is below certain values meaning the mask image is not correct or the edge detection is not performing well,
          // hence cropRegion and adjustedJointRoiForMasking has to be determined by mask image itself. 
          if (width < 5 || height< 5)
          {
            java.awt.image.BufferedImage maskBufferedImage = createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getShortName());
            maskImage.decrementReferenceCount();
            maskImage = Image.createFloatImageFromBufferedImage(maskBufferedImage);
            cropRegion = RegionOfInterest.createRegionFromImage(maskImage);
            adjustedJointRoiForMasking = new RegionOfInterest(cropRegion);
            width = maskImage.getWidth();
            height = maskImage.getHeight();
            maskBufferedImage.flush();
          }
          else
          {          
            cropRegion = RegionOfInterest.createLineRegionFromRegionBorder(x.getY(), y.getY(), x.getX(), y.getX(), 0, RegionShapeEnum.RECTANGULAR);
            adjustedJointRoiForMasking = new RegionOfInterest(cropRegion);
        
            RegionOfInterest jointRoi2 = Locator.getRegionOfInterestAtMeasuredLocation(joint);
                 
            // if the reconstruction image has more than one pad, and the mask image has only one pad, "special" cropRegion has to be created.
            if (reconstructionRegion.getPads().size() > 1)
            {
              width = jointRoi2.getWidth();            
              height = jointRoi2.getHeight();
              
              if (Math.abs(cropRegion.getMinX() - jointRoi2.getMinX()) > 20)         
              { 
                cropRegion.setRect(jointRoi2.getMinX(), cropRegion.getMinY(), width, height);
              }

              if (Math.abs(cropRegion.getMinY() - jointRoi2.getMinY()) > 20)
              {
                cropRegion.setRect(cropRegion.getMinX(), jointRoi2.getMinY(), width, height);
              }
              
              cropRegion.setRect(cropRegion.getMinX(), cropRegion.getMinY(), width, height);          
            }           
          }
          
          jointRoiForUsingMask = cropRegion;
          
          if (cropRegion.fitsWithinImage(sliceImageForMasking) == false)
          {
            width = jointRoi.getWidth();
            height = jointRoi.getHeight();
             
            cropRegion.setRect(jointRoi.getMinX(), jointRoi.getMinY(), width, height);        

            AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_MASK_IMAGE_NOT_INSPECTING_WARNING_KEY",
                                                                  new String[]
                                                                  {joint.getComponent().getReferenceDesignator()}));           
          }

          Image newImage = new Image(width, height);
          Transform.copyImageIntoImage(sliceImageForMasking, cropRegion, newImage, RegionOfInterest.createRegionFromImage(newImage));  
          maskSliceImage = new Image(width, height);
          Transform.copyImageIntoImage(newImage, RegionOfInterest.createRegionFromImage(newImage), maskSliceImage, RegionOfInterest.createRegionFromImage(maskSliceImage));
          numberOfPixelsTestedForMasking = Threshold.countPixelsInRange(maskImage, 0.0f, 0.5f);
          newImage.decrementReferenceCount();
          misalignedMaskImage.flush();
        }
        else if (createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getShortName()) != null)
        {    
          maskSliceImage = new Image(sliceImageForMasking);
          
          java.awt.image.BufferedImage maskBufferedImage = createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getShortName());
          maskImage = Image.createFloatImageFromBufferedImage(maskBufferedImage);

          jointRoiForUsingMask = RegionOfInterest.createRegionFromImage(maskImage);
          adjustedJointRoiForMasking = new RegionOfInterest(jointRoiForUsingMask);

          numberOfPixelsTestedForMasking = Threshold.countPixelsInRange(maskImage, 0.0f, 0.5f);

          maskBufferedImage.flush();              
        }
        else
        {
          usingMasking = false;
        }
      }
      
      // create the estimated background
      Image expectedImage;
      List<RegionOfInterest> listOfSubregions;
      
      if(usingMasking)
      {
        expectedImage = new Image(maskSliceImage.getWidth(), maskSliceImage.getHeight());
        listOfSubregions = createBackgroundSubRegions(subRegionSizeInMM, jointRoiForUsingMask);
      }
      else
      {
        expectedImage = new Image(jointRoi.getWidth(), jointRoi.getHeight());
        listOfSubregions = createBackgroundSubRegions(subRegionSizeInMM, jointRoi);
      }

      AlgorithmDiagnostics algorithmDiagnostics = AlgorithmDiagnostics.getInstance();

      // draw background region diagnostics so the customer-programmer can have some guess at what is going on.
      Subtype subtype = joint.getSubtype();
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
      {
        DiagnosticInfo backgroundRegionDiagnosticInfoArray[] = new DiagnosticInfo[listOfSubregions.size()];
        int i = 0;
        for (RegionOfInterest subregion : listOfSubregions)
          backgroundRegionDiagnosticInfoArray[i++] = new MeasurementRegionDiagnosticInfo(subregion, MeasurementRegionEnum.BACKGROUND_REGION);

        algorithmDiagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(), padSliceNameEnum, joint, this, false,
                                             backgroundRegionDiagnosticInfoArray);
      }
      for (RegionOfInterest subregion : listOfSubregions)
      {
        // compute the graylevel
        // get the background as some percentage of the gray levels.
        float backgroundPercentile = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE);

        float expectedGraylevel = Statistics.getPercentile(sliceImage, subregion, backgroundPercentile * 0.01f );

        // fill in the 'expected' image.
        RegionOfInterest subregionInBackgroundImage;
        if(usingMasking)
        {
          subregionInBackgroundImage = new RegionOfInterest(subregion.getMinX() - jointRoiForUsingMask.getMinX(),
                                                          subregion.getMinY() - jointRoiForUsingMask.getMinY(),
                                                          subregion.getWidth(),
                                                          subregion.getHeight(),
                                                          0, RegionShapeEnum.RECTANGULAR);
        }
        else
        {
          subregionInBackgroundImage = new RegionOfInterest(subregion.getMinX() - jointRoi.getMinX(),
                                                          subregion.getMinY() - jointRoi.getMinY(),
                                                          subregion.getWidth(),
                                                          subregion.getHeight(),
                                                          0, RegionShapeEnum.RECTANGULAR);
        }
        Paint.fillRegionOfInterest(expectedImage,
                                   subregionInBackgroundImage,
                                   expectedGraylevel);

      }
      
      // pixelwise, perform thickness table lookups. (bg=src, fg=expected), gives positive
      // values if the src is brighter than the expected. Returned thickness is the amount of
      // solder we are NOT seeing (eg. voiding)
      SolderThickness thicnessTable = AlgorithmUtil.getSolderThickness(subtype);
      Image voidPixelsImage;
      
      if(usingMasking)
      {
        voidPixelsImage = AlgorithmUtil.createThicknessImage(expectedImage,
                                                            RegionOfInterest.createRegionFromImage(expectedImage),
                                                            maskSliceImage,
                                                            RegionOfInterest.createRegionFromImage(maskSliceImage),
                                                            thicnessTable);
      }
      else
      {
        voidPixelsImage = AlgorithmUtil.createThicknessImage(expectedImage,
                                                            RegionOfInterest.createRegionFromImage(expectedImage),
                                                            imageToUse,
                                                            roiToUse,
                                                            thicnessTable);
      }
      expectedImage.decrementReferenceCount();

      // threshold this thickness table with the thickness threshold.
      // Pixels with a value of 1.0 came out with a missing thickness greater than the threshold -- these are potential voids.
      // the thickness table is in units of mils.
      float voidThicknessThresholdInMils = MathUtil.convertMillimetersToMils(voidThicknessThreshold);
      Threshold.threshold(voidPixelsImage, voidThicknessThresholdInMils, 0.0f, voidThicknessThresholdInMils, _VOID_PIXEL_INTENSITY);

      // Noise Reduction
      if (numberOfTimesToOpenVoidPixelImage > 0)
      {
        // false call avoidance idea PWL : Add a one-pixel boundary around the image that will operate with noise reduction. 

        Image noiseReducedVoidPixelsImage = Filter.opening(voidPixelsImage, RegionOfInterest.createRegionFromImage(voidPixelsImage), numberOfTimesToOpenVoidPixelImage);
        voidPixelsImage.decrementReferenceCount();
        voidPixelsImage = noiseReducedVoidPixelsImage;
      }
      
      if(usingMasking)
      {        
        Threshold.threshold(maskImage, 1.0f, _VOID_PIXEL_INTENSITY, 2.0f, 0.0f);

        Arithmetic.andImages(maskImage, adjustedJointRoiForMasking,
             voidPixelsImage, RegionOfInterest.createRegionFromImage(voidPixelsImage), voidPixelsImage, 
             RegionOfInterest.createRegionFromImage(voidPixelsImage));
      }
      
      if (usingMasking)
      {
        maskImage.decrementReferenceCount();
        maskSliceImage.decrementReferenceCount();       
      }
      
      imageToUse.decrementReferenceCount();
      
      //Jack Hwee-individual void
     // BufferedImage  voidPixelsBufferedImage = voidPixelsImage.getBufferedImage();

      // count the number of "void" pixels
      int numberOfVoidPixels = Threshold.countPixelsInRange(voidPixelsImage, _VOID_PIXEL_INTENSITY-0.5f, _VOID_PIXEL_INTENSITY+0.5f);

      // Store the percent voiding
      int numberOfPixelsTested = jointRoi.getWidth()*jointRoi.getHeight();
      
      float voidPercent = 0;
      
      if (usingMasking)
        voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTestedForMasking;
      else
        voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;

      //Jack Hwee-individual void
      //Siew Yeng - XCR-2210 - Combine two voids if the two voids are within an acceptable gap distance
      float [] largestVoidImageArray;
      float gapDistanceInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE);
      if(gapDistanceInMils > 0)
      {
        int gapDistanceInPixels = (int)Math.ceil(gapDistanceInMils / AlgorithmUtil.getMilsPerPixel());
        
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(voidPixelsImage, gapDistanceInPixels);
      }
      else
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(voidPixelsImage);
      
      //create image with largest void area only
      Image largestVoidImage = Image.createFloatImageFromArray(voidPixelsImage.getWidth(), voidPixelsImage.getHeight(), largestVoidImageArray);
      
      largestVoidImageArray = null;

      //Jack Hwee-individual void
//      int numberOfIndividualVoidPixels = Threshold.individualVoid (voidPixelsImage,individualVoidAreaFailThreshold, numberOfPixelsTested);
      int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 255 - 0.5f, 255.f);
      
      float individualVoidPercent = 0;
      
      if(usingMasking)
        individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTestedForMasking;
      else
        individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTested;

      JointInspectionResult jointResult = joint.getJointInspectionResult();
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);

      jointResult.addMeasurement(voidPercentMeasurement);

      // //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

      jointResult.addMeasurement(individualVoidPercentMeasurement);  

      if (usingMasking)
      {
        jointRoi = jointRoiForUsingMask;
      }

      if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, voidPercentMeasurement);
        
        //Siew Yeng - XCR-2566 - Display Voiding Percentage Value on Diagnostic Image
        OverlayImageDiagnosticInfo diagnosticInfo = new OverlayImageDiagnosticInfo(voidPixelsImage, jointRoi);
        String measurementString = MeasurementUnitsEnum.formatNumberIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), voidPercentMeasurement.getValue());
        String measurementLabel = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), measurementString);
        diagnosticInfo.setMeasurementLabel(measurementLabel);
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     diagnosticInfo);

       //Jack Hwee-individual void
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, individualVoidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     new OverlayLargestVoidImageDiagnosticInfo(largestVoidImage, jointRoi));
      }
      
      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype))   
      {
        //Siew Yeng - add Largest void image into diagnostic slice
        java.awt.image.BufferedImage diagnosticBufferedImage = 
          com.axi.guiUtil.BufferedImageUtil.overlayImages(voidPixelsImage.getAlphaBufferedImage(java.awt.Color.WHITE), 
          largestVoidImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.DIAGNOSTIC_LARGEST_VOIDING_PIXELS.getColor()), 
          java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
        
        Image diagnosticImage = Image.createFloatImageFromBufferedImage(diagnosticBufferedImage);
        
        //Siew Yeng - XCR-2566
        AlgorithmUtil.addDiagnosticImageIntoReconstructionImages(reconstructedImages, diagnosticImage, jointRoi, sliceNameEnum);
        
        diagnosticBufferedImage.flush();
        diagnosticImage.decrementReferenceCount();
      }
      
      voidPixelsImage.decrementReferenceCount();
      largestVoidImage.decrementReferenceCount();

      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > voidAreaFailThreshold);
      boolean individualJointFailed = (individualVoidPercent > individualVoidAreaFailThreshold);

      if (jointFailed)
      {
        JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
            this, sliceNameEnum);
        voidPercentIndictment.addFailingMeasurement(voidPercentMeasurement);
        jointResult.addIndictment(voidPercentIndictment);
      }

      if (individualJointFailed)
      {
        JointIndictment individualVoidPercentIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING,
            this, sliceNameEnum);
        individualVoidPercentIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        jointResult.addIndictment(individualVoidPercentIndictment);
      } 

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !jointFailed);
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !individualJointFailed);

      ++jointIndex;
    }
  }*/
  
  /**
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Patrick Lacz
   */
  private void classifySlice(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float subRegionSizeInMM,
      float voidThicknessThreshold,
      float voidAreaFailThreshold,
      float individualVoidAreaFailThreshold,
      int noiseReduction,
      final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    Image sliceImage = slice.getOrthogonalImage();

    // determine settings from noise reduction.
    // noise reduction of 1 = blur the image, 2 and above : apply opening afterwards.
    boolean applyBlurToJointImages = noiseReduction >= 1;
    int numberOfTimesToOpenVoidPixelImage = Math.max(0, noiseReduction - 1);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    SliceNameEnum padSliceNameEnum = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);
    boolean usingMasking = isUsingMaskingMethod(subtypeOfJoints);
    int jointIndex = 0;
    Image combineDiagnosticVoidImage = null;
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);
      RegionOfInterest jointRoi = getRegionOfInterest(joint, subtypeOfJoints, MILIMETER_PER_PIXEL);//Locator.getRegionOfInterestAtMeasuredLocation(joint); 
      
      IntegerRef numberOfPixelTestedRef = new IntegerRef();
      Pair<Image, RegionOfInterest> voidPixelImageAndRoi = classifySliceForEachJoint(sliceImage,
                                reconstructionRegion,
                                padSliceNameEnum,
                                joint,
                                subtypeOfJoints,
                                jointRoi,
                                applyBlurToJointImages,
                                usingMasking,
                                subRegionSizeInMM,
                                voidThicknessThreshold,
                                numberOfTimesToOpenVoidPixelImage,
                                numberOfPixelTestedRef,
                                MILIMETER_PER_PIXEL);
      
      Image voidPixelsImage = voidPixelImageAndRoi.getFirst();

      if(subtypeOfJoints.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtypeOfJoints.getSubSubtypes())
        {          
          if(subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION).equals(_FALSE))
            continue;
          
          Image finalSubRegionImage = classifySubSubtypeForEachJoint(sliceImage,
                                                                      reconstructionRegion,
                                                                      joint,
                                                                      jointRoi,
                                                                      subtypeOfJoints,
                                                                      subSubtype,
                                                                      sliceNameEnum,
                                                                      voidPixelsImage.getWidth(),
                                                                      voidPixelsImage.getHeight(),
                                                                      voidPixelImageAndRoi.getSecond(),
                                                                      MILIMETER_PER_PIXEL);

          Arithmetic.orImages(voidPixelsImage,  RegionOfInterest.createRegionFromImage(voidPixelsImage),
                 finalSubRegionImage, RegionOfInterest.createRegionFromImage(finalSubRegionImage), voidPixelsImage, 
                 RegionOfInterest.createRegionFromImage(voidPixelsImage));

          finalSubRegionImage.decrementReferenceCount();
        }
      }
      
      jointRoi = voidPixelImageAndRoi.getSecond();
      //Jack Hwee-individual void
     // BufferedImage  voidPixelsBufferedImage = voidPixelsImage.getBufferedImage();

      int numberOfPixelsTested = numberOfPixelTestedRef.getValue();
      // count the number of "void" pixels
      int numberOfVoidPixels = Threshold.countPixelsInRange(voidPixelsImage, _VOID_PIXEL_INTENSITY - 0.5f, 255.f);
      
      float voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;

      //Jack Hwee-individual void
      //Siew Yeng - XCR-2210 - Combine two voids if the two voids are within an acceptable gap distance
      float [] largestVoidImageArray;
      float gapDistanceInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE);
      if(gapDistanceInMils > 0)
      {
        int gapDistanceInPixels = (int)Math.ceil(gapDistanceInMils / MathUtil.convertMillimetersToMils(MILIMETER_PER_PIXEL));
        
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(voidPixelsImage, gapDistanceInPixels);
      }
      else
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(voidPixelsImage);
      
      //create image with largest void area only
      Image largestVoidImage = Image.createFloatImageFromArray(voidPixelsImage.getWidth(), voidPixelsImage.getHeight(), largestVoidImageArray);
      
      largestVoidImageArray = null;

      //Jack Hwee-individual void
      int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 255 - 0.5f, 255.f);
      
      float individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTested;

      JointInspectionResult jointResult = joint.getJointInspectionResult();
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);

      jointResult.addMeasurement(voidPercentMeasurement);

      // //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

      jointResult.addMeasurement(individualVoidPercentMeasurement);  
      
      if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, voidPercentMeasurement);
        
        //Siew Yeng - XCR-2566 - Display Voiding Percentage Value on Diagnostic Image
        OverlayImageDiagnosticInfo diagnosticInfo = new OverlayImageDiagnosticInfo(voidPixelsImage, jointRoi);
        String measurementString = MeasurementUnitsEnum.formatNumberIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), voidPercentMeasurement.getValue());
        String measurementLabel = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), measurementString);
        diagnosticInfo.setMeasurementLabel(measurementLabel);
        
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     diagnosticInfo);

       //Jack Hwee-individual void
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, individualVoidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     new OverlayLargestVoidImageDiagnosticInfo(largestVoidImage, jointRoi));
      }

      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > voidAreaFailThreshold);
      boolean individualJointFailed = (individualVoidPercent > individualVoidAreaFailThreshold);

      if (jointFailed)
      {
        JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
            this, sliceNameEnum);
        voidPercentIndictment.addFailingMeasurement(voidPercentMeasurement);
        jointResult.addIndictment(voidPercentIndictment);
      }

      if (individualJointFailed)
      {
        JointIndictment individualVoidPercentIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING,
            this, sliceNameEnum);
        individualVoidPercentIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        jointResult.addIndictment(individualVoidPercentIndictment);
      } 

      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image 
      if ((jointFailed || individualJointFailed) &&
           ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtypeOfJoints))
      {
        //Siew Yeng - add Largest void image into diagnostic slice
         java.awt.image.BufferedImage diagnosticBufferedImage = 
          com.axi.guiUtil.BufferedImageUtil.overlayImages(voidPixelsImage.getAlphaBufferedImage(java.awt.Color.WHITE), 
          largestVoidImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.DIAGNOSTIC_LARGEST_VOIDING_PIXELS.getColor()), 
          java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
        
        Image diagnosticImage = Image.createFloatImageFromBufferedImage(diagnosticBufferedImage);
        
        combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, 
                                                                          combineDiagnosticVoidImage, 
                                                                          diagnosticImage, 
                                                                          jointRoi, 
                                                                          sliceNameEnum);
        
        diagnosticBufferedImage.flush();
        diagnosticImage.decrementReferenceCount();
      }
      
      voidPixelsImage.decrementReferenceCount();
      largestVoidImage.decrementReferenceCount();
      
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !jointFailed);
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !individualJointFailed);

      ++jointIndex;
    }
    
    if(combineDiagnosticVoidImage != null)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
      combineDiagnosticVoidImage.decrementReferenceCount();
    }
  }
  
  /**
   * Thickness method for each joint
   * @author Siew Yeng
   */
  private Pair<Image, RegionOfInterest> classifySliceForEachJoint(Image sliceImage,
                                                                  ReconstructionRegion reconstructionRegion,
                                                                  SliceNameEnum padSliceNameEnum,
                                                                  JointInspectionData joint,
                                                                  Subtype subtypeOfJoints,
                                                                  RegionOfInterest jointRoi,
                                                                  boolean applyBlurToJointImages,
                                                                  boolean usingMasking,
                                                                  float subRegionSizeInMM,
                                                                  float voidThicknessThreshold,
                                                                  int numberOfTimesToOpenVoidPixelImage,
                                                                  IntegerRef numberOfPixelTestedRef,
                                                                  final float MILIMETER_PER_PIXEL) throws DatastoreException 
  {
    Image imageToUse = null;
    RegionOfInterest roiToUse = RegionOfInterest.createRegionFromImage(sliceImage);

    Image maskImage = null;
      
    // Store the percent voiding
    int numberOfPixelsTested = jointRoi.getWidth() * jointRoi.getHeight();
    
    //Siew Yeng - XCR-3319 - Software crash due to masking
    RegionOfInterest jointRoiForUsingMask = null;
    if(usingMasking)
    {
      Pair<Image,RegionOfInterest> maskImageAndRoi = createMaskImage(sliceImage, reconstructionRegion, jointRoi, joint, subtypeOfJoints);
      maskImage = maskImageAndRoi.getFirst();
      jointRoiForUsingMask = maskImageAndRoi.getSecond();

      if(maskImage == null)
      {
        usingMasking = false;
      }
      else
      {
        // if the reconstruction image has more than one pad, jointRoiForUsingMask need to be separated
        if (reconstructionRegion.getPads().size() == 1)
        {
          jointRoi = jointRoiForUsingMask;
        }
        roiToUse = jointRoi;
        numberOfPixelsTested = Threshold.countPixelsInRange(maskImage, 0.0f, 0.5f);
      }
    }
      
    // if we apply a blur, we need to use a different image/roi
    if (applyBlurToJointImages)
    {
      imageToUse = Filter.convolveLowpass(sliceImage, jointRoi);
      roiToUse = RegionOfInterest.createRegionFromImage(imageToUse);
    }
    else
    {
      imageToUse = new Image(roiToUse.getWidth(), roiToUse.getHeight());
      Transform.copyImageIntoImage(sliceImage, roiToUse, imageToUse, RegionOfInterest.createRegionFromImage(imageToUse));

      if(usingMasking)
        roiToUse = RegionOfInterest.createRegionFromImage(imageToUse);
      else
        roiToUse = jointRoi;
    }
 
    // create the estimated background
    Image expectedImage = new Image(jointRoi.getWidth(), jointRoi.getHeight());
    List<RegionOfInterest> listOfSubregions = createBackgroundSubRegions(subRegionSizeInMM, jointRoi, MILIMETER_PER_PIXEL);

    AlgorithmDiagnostics algorithmDiagnostics = AlgorithmDiagnostics.getInstance();

    // draw background region diagnostics so the customer-programmer can have some guess at what is going on.
    Subtype subtype = joint.getSubtype();
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
    {
      DiagnosticInfo backgroundRegionDiagnosticInfoArray[] = new DiagnosticInfo[listOfSubregions.size()];
      int i = 0;
      for (RegionOfInterest subregion : listOfSubregions)
        backgroundRegionDiagnosticInfoArray[i++] = new MeasurementRegionDiagnosticInfo(subregion, MeasurementRegionEnum.BACKGROUND_REGION);

      algorithmDiagnostics.postDiagnostics(reconstructionRegion, padSliceNameEnum, joint, this, false,
                                           backgroundRegionDiagnosticInfoArray);
    }
      
    for (RegionOfInterest subregion : listOfSubregions)
    {
      // compute the graylevel
      // get the background as some percentage of the gray levels.
      float backgroundPercentile = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE);

      float expectedGraylevel = Statistics.getPercentile(sliceImage, subregion, backgroundPercentile * 0.01f );

      // fill in the 'expected' image.
      RegionOfInterest subregionInBackgroundImage = new RegionOfInterest(subregion.getMinX() - jointRoi.getMinX(),
                                                        subregion.getMinY() - jointRoi.getMinY(),
                                                        subregion.getWidth(),
                                                        subregion.getHeight(),
                                                        0, RegionShapeEnum.RECTANGULAR);

      Paint.fillRegionOfInterest(expectedImage,
                                 subregionInBackgroundImage,
                                 expectedGraylevel);
    }
      
    // pixelwise, perform thickness table lookups. (bg=src, fg=expected), gives positive
    // values if the src is brighter than the expected. Returned thickness is the amount of
    // solder we are NOT seeing (eg. voiding)
    SolderThickness thicnessTable = AlgorithmUtil.getSolderThickness(subtype);
    Image voidPixelsImage = AlgorithmUtil.createThicknessImage(expectedImage,
                                                          RegionOfInterest.createRegionFromImage(expectedImage),
                                                          imageToUse,
                                                          roiToUse,
                                                          thicnessTable);

    expectedImage.decrementReferenceCount();

    // threshold this thickness table with the thickness threshold.
    // Pixels with a value of 1.0 came out with a missing thickness greater than the threshold -- these are potential voids.
    // the thickness table is in units of mils.
    float voidThicknessThresholdInMils = MathUtil.convertMillimetersToMils(voidThicknessThreshold);
    Threshold.threshold(voidPixelsImage, voidThicknessThresholdInMils, 0.0f, voidThicknessThresholdInMils, _VOID_PIXEL_INTENSITY);

    // Noise Reduction
    if (numberOfTimesToOpenVoidPixelImage > 0)
    {
      /** false call avoidance idea PWL : Add a one-pixel boundary around the image that will operate with noise reduction. */

      Image noiseReducedVoidPixelsImage = Filter.opening(voidPixelsImage, RegionOfInterest.createRegionFromImage(voidPixelsImage), numberOfTimesToOpenVoidPixelImage);
      voidPixelsImage.decrementReferenceCount();
      voidPixelsImage = noiseReducedVoidPixelsImage;
    }
      
    if(usingMasking)
    {        
      Threshold.threshold(maskImage, 1.0f, _VOID_PIXEL_INTENSITY, 2.0f, 0.0f);

      //Siew Yeng - XCR-3319 - always use jointRoiForUsingMask for maskImage because this is the actual roi for maskImage
      Arithmetic.andImages(maskImage, jointRoiForUsingMask,
              voidPixelsImage, RegionOfInterest.createRegionFromImage(voidPixelsImage), voidPixelsImage,
              RegionOfInterest.createRegionFromImage(voidPixelsImage));

      maskImage.decrementReferenceCount();
    }
      
    imageToUse.decrementReferenceCount();
    
    numberOfPixelTestedRef.setValue(numberOfPixelsTested);
    
    return new Pair(voidPixelsImage, jointRoi);
  }
  
  /**
   * Algorithm Version 1
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Jack Hwee
   */
   private void classifyFloodFillSlice(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float voidAreaFailThreshold,
      float individualVoidAreaFailThreshold,
      int floodFillSensitivity,
      int numberOfImageLayer,
      Boolean usingMasking,
      Boolean usingMultiThresholdDetection,
      Boolean usingFitPolynomialDetection,
      final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);
    
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    Image sliceImage = slice.getOrthogonalImage();
    Image sliceImageForMasking = slice.getOrthogonalImage();
   
    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();

//    float untestedBorderSizeInMM = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE);
//    int untestedBorderSizeInPixels = (int)Math.ceil(untestedBorderSizeInMM / AlgorithmUtil.getMillimetersPerPixel());
//    float leftOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET);
//    int leftOffsetInPixels = (int)Math.ceil(leftOffset / AlgorithmUtil.getMillimetersPerPixel());
//    float bottomOffset= (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET);
//    int bottomOffsetInPixels = (int)Math.ceil(bottomOffset / AlgorithmUtil.getMillimetersPerPixel());
//    float rightOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET);
//    int rightOffsetInPixels = (int)Math.ceil(rightOffset / AlgorithmUtil.getMillimetersPerPixel());
//    float topOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET);
//    int topOffsetInPixels = (int)Math.ceil(topOffset / AlgorithmUtil.getMillimetersPerPixel());
    
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);

    Image combineDiagnosticVoidImage = null;
    // For the first iteration through the joints, extract the joint's image normalized to the nominal size.
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);
      RegionOfInterest jointRoi = getRegionOfInterest(joint, subtypeOfJoints, MILIMETER_PER_PIXEL);/*Locator.getRegionOfInterestAtMeasuredLocation(joint);
       
      // Untested Border Size Method - Standard or Customize
      if (isUntestedBorderSizeStandardMethod(subtypeOfJoints))
      {      
        if ((jointRoi.getWidth() - 2*untestedBorderSizeInPixels < 5.0) || (jointRoi.getHeight() - 2*untestedBorderSizeInPixels < 5.0))
        {
          // @todo PWL : post warning about the untested region being too large
          LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                            new Object[] {joint.getFullyQualifiedPadName()});
          AlgorithmUtil.raiseAlgorithmWarning(warningText);
          continue;
        }
        
        jointRoi.setWidthKeepingSameCenter(jointRoi.getWidth() - 2*untestedBorderSizeInPixels);
        jointRoi.setHeightKeepingSameCenter(jointRoi.getHeight() - 2*untestedBorderSizeInPixels);
      }
      else if (isUntestedBorderSizeCustomizeMethod(subtypeOfJoints))
      {
        if ((jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels < 5.0) || 
             (jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels < 5.0))
        {
          LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                            new Object[] {joint.getFullyQualifiedPadName()});
          AlgorithmUtil.raiseAlgorithmWarning(warningText);
          continue;
        }
        
        jointRoi.setMinXY(jointRoi.getMinX() + leftOffsetInPixels, jointRoi.getMinY() + topOffsetInPixels);
        jointRoi.setWidthKeepingSameMinX(jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels);
        jointRoi.setHeightKeepingSameMinY(jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels);
      }*/
      
      double gaussianBlurRadius = 25.0;
        
      // if we apply a blur, we need to use a different image/roi
      Image imageToUse = sliceImage;
      
      imageToUse = Filter.convolveLowpass(sliceImage, jointRoi);
      
      RegionOfInterest adjustedJointRoiForMasking = null;
      
      Image imageToUseForThresholding = new Image(imageToUse.getWidth(), imageToUse.getHeight());     

      Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
     
      Image maskSliceImage = null;
      
      Image maskImage = null;
      
      RegionOfInterest jointRoiForUsingMask = null;
      
       // Store the percent voiding if using masking
      int numberOfPixelsTestedForMasking = 0;
   
      String maskImagePath = Directory.getAlgorithmLearningDir(subtypeOfJoints.getPanel().getProject().getName()) + java.io.File.separator + "mask2#" + subtypeOfJoints.getShortName() + ".png";
      MaskImage createMaskImage =  MaskImage.getInstance();
    
      if (usingMasking)
      {
        if (createMaskImage.getMisalignedSubtypeMaskImage(joint, subtypeOfJoints.getLongName()) != null)
        {
          java.awt.image.BufferedImage misalignedMaskImage = createMaskImage.getMisalignedSubtypeMaskImage(joint, subtypeOfJoints.getLongName());
          maskImage = Image.createFloatImageFromBufferedImage(misalignedMaskImage);

          //Siew Yeng - XCR-3708 - production crash due to single pad masking
          RegionOfInterest cropRegion = null;
          
          // get the imageCoordinate of horizontal and vertical edges of mask image          
         if(createMaskImage.getSubtypeMaskImageXCoordinate(joint, subtypeOfJoints.getLongName()) == null || 
            createMaskImage.getSubtypeMaskImageYCoordinate(joint, subtypeOfJoints.getLongName()) == null)
          {
            cropRegion = Locator.getCropRegionFromMaskImage(maskImage);
          }
          else
          {
            // get the imageCoordinate of horizontal and vertical edges of mask image
            ImageCoordinate x = createMaskImage.getSubtypeMaskImageXCoordinate(joint, subtypeOfJoints.getLongName());
            ImageCoordinate y = createMaskImage.getSubtypeMaskImageYCoordinate(joint, subtypeOfJoints.getLongName());
            
            cropRegion = RegionOfInterest.createLineRegionFromRegionBorder(x.getY(), y.getY(), x.getX(), y.getX(), 0, RegionShapeEnum.RECTANGULAR);
          }
       
          int width = cropRegion.getWidth();
          int height = cropRegion.getHeight();

          // if the width and height is below certain values meaning the mask image is not correct or the edge detection is not performing well,
          // hence cropRegion and adjustedJointRoiForMasking has to be determined by mask image itself. 
          if (width < 5 || height< 5)
          {
            java.awt.image.BufferedImage maskBufferedImage = createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getLongName(), joint.getComponent().getDegreesRotationAfterAllRotations());
            maskImage.decrementReferenceCount();
            maskImage = Image.createFloatImageFromBufferedImage(maskBufferedImage);
            cropRegion = RegionOfInterest.createRegionFromImage(maskImage);
            adjustedJointRoiForMasking = new RegionOfInterest(cropRegion);
            width = maskImage.getWidth();
            height = maskImage.getHeight();
            maskBufferedImage.flush();
          }
          else
          {
            adjustedJointRoiForMasking = new RegionOfInterest(cropRegion);
        
            RegionOfInterest jointRoi2 = Locator.getRegionOfInterestAtMeasuredLocation(joint);
                 
            // if the reconstruction image has more than one pad, and the mask image has only one pad, "special" cropRegion has to be created.
            if (reconstructionRegion.getPads().size() > 1)
            {
              width = jointRoi2.getWidth();            
              height = jointRoi2.getHeight();
              
              //Siew Yeng - fix masking matching problem
              if (Math.abs(cropRegion.getMinX() - jointRoi2.getMinX()) > 0)         
              { 
                cropRegion.setRect(jointRoi2.getMinX(), cropRegion.getMinY(), width, height);
              }

              if (Math.abs(cropRegion.getMinY() - jointRoi2.getMinY()) > 0)
              {
                cropRegion.setRect(cropRegion.getMinX(), jointRoi2.getMinY(), width, height);
              }
              
//              cropRegion.setRect(cropRegion.getMinX(), cropRegion.getMinY(), width, height);          
            }           
          }
          
          jointRoiForUsingMask = cropRegion;
          
          if (cropRegion.fitsWithinImage(sliceImageForMasking) == false)
          {
             width = jointRoi.getWidth();
             height = jointRoi.getHeight();
             
             cropRegion.setRect(jointRoi.getMinX(), jointRoi.getMinY(), width, height);        
             
             AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_MASK_IMAGE_NOT_INSPECTING_WARNING_KEY",
                                                                  new String[]
                                                                  {joint.getComponent().getReferenceDesignator()}));           
          }

          Image newImage = new Image(width, height);
          Transform.copyImageIntoImage(sliceImageForMasking, cropRegion, newImage, RegionOfInterest.createRegionFromImage(newImage));  
          maskSliceImage = new Image(width, height);
          Transform.copyImageIntoImage(newImage, RegionOfInterest.createRegionFromImage(newImage), maskSliceImage, RegionOfInterest.createRegionFromImage(maskSliceImage));
          numberOfPixelsTestedForMasking = Threshold.countPixelsInRange(maskImage, 0.0f, 0.5f);
          newImage.decrementReferenceCount();
          misalignedMaskImage.flush();
        }
        else if (createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getLongName(), joint.getComponent().getDegreesRotationAfterAllRotations()) != null)
        {                       
          maskSliceImage = new Image(sliceImageForMasking);
          
          java.awt.image.BufferedImage maskBufferedImage = createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getLongName(), joint.getComponent().getDegreesRotationAfterAllRotations());
          maskImage = Image.createFloatImageFromBufferedImage(maskBufferedImage);

          jointRoiForUsingMask = RegionOfInterest.createRegionFromImage(maskImage);
          adjustedJointRoiForMasking = new RegionOfInterest(jointRoiForUsingMask);

          numberOfPixelsTestedForMasking = Threshold.countPixelsInRange(maskImage, 0.0f, 0.5f);

          maskBufferedImage.flush();              
        }
        else
        {
          usingMasking = false;
        }
     }

     ImagePlus gaussianBlurBuffImg ;
     /////////////////////////////////////////////////////////////////////////////////////////////////////////////
      if (usingMasking)
          gaussianBlurBuffImg = new ImagePlus("", maskSliceImage.getBufferedImage());
      else
          gaussianBlurBuffImg = new ImagePlus("", imageToUse.getBufferedImage());
      
      float gaussianBlurThreshold = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR);
      
      if (gaussianBlurBuffImg != null)
      {  
        GaussianBlur gaussianBlur = new GaussianBlur();

        if (usingMasking)
            gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurThreshold);
        else
        {
          gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurThreshold);
        }
      }
      else
      {
          gaussianBlurBuffImg.setImage(maskSliceImage.getBufferedImage());
          GaussianBlur gaussianBlur = new GaussianBlur();
         
          if (usingMasking)
             gaussianBlur.blurGaussian(gaussianBlurBuffImg.getProcessor(), 25.0, 25.0, 0.01);
          else
             gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurRadius);
      }

      gaussianBlurBuffImg.lockSilently();
      
      Image filterGaussImage = Image.createFloatImageFromBufferedImage(gaussianBlurBuffImg.getBufferedImage());
   
      gaussianBlurBuffImg.close();
      gaussianBlurBuffImg.flush();       
      
      Image totalGaussVoidImage;
      
      if (usingMasking)
          totalGaussVoidImage = Arithmetic.subtractImages(maskSliceImage, filterGaussImage);
      else
          totalGaussVoidImage = Arithmetic.subtractImages(imageToUse, filterGaussImage);
      
      filterGaussImage.decrementReferenceCount();

      /////////////////Variablepad method as additional layer/////////////////////////
      int variablePadBorderSensitivity = (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL);
      
      Image finalVariablePadImage = null; 
      
      if(variablePadBorderSensitivity > 0)
      {
        //XCR-2169 - Additional Layer for Floodfill method - handle additional layer when use masking
        Image imageToUseForAdditionalLayer;

        if(usingMasking)
          imageToUseForAdditionalLayer = Image.createCopy(maskSliceImage, RegionOfInterest.createRegionFromImage(maskSliceImage));
        else
          imageToUseForAdditionalLayer = Image.createCopy(imageToUse, RegionOfInterest.createRegionFromImage(imageToUse));
        
        Image variablePadImage =  new Image(imageToUseForAdditionalLayer.getWidth(), imageToUseForAdditionalLayer.getHeight());
        
        if (variablePadBorderSensitivity > 6)
        {
          variablePadImage.decrementReferenceCount();
          variablePadImage = Image.createCopy(imageToUseForAdditionalLayer, RegionOfInterest.createRegionFromImage(imageToUseForAdditionalLayer));       
          int numHistogramBins = 256;
          int[] histogram = Threshold.histogram(imageToUseForAdditionalLayer, RegionOfInterest.createRegionFromImage(imageToUseForAdditionalLayer), numHistogramBins);
          int level = Threshold.getAutoThreshold(histogram);

          //if (variablePadBorderSensitivity == 6) level = level - 1;
          if (variablePadBorderSensitivity == 7) level = level - 2;
          if (variablePadBorderSensitivity == 8) level = level - 3;
          if (variablePadBorderSensitivity == 9) level = level - 4;
          if (variablePadBorderSensitivity == 10) level = level - 5;

          Threshold.threshold(variablePadImage, (float) level, 0.0f, (float)level, 255.0f);
        }
        else
        {
          Transform.copyImageIntoImage(totalGaussVoidImage, variablePadImage);
          if (variablePadBorderSensitivity == 5) 
            Threshold.threshold(variablePadImage, (float)(1), 255.0f, (float)(1), 0.0f);
          else if (variablePadBorderSensitivity == 6) 
            Threshold.threshold(variablePadImage, (float)(0), 255.0f, (float)(0), 0.0f);
          else
            Threshold.threshold(variablePadImage, (float)(5 - variablePadBorderSensitivity), 255.0f, (float)(5 - variablePadBorderSensitivity), 0.0f);

          Threshold.threshold(variablePadImage, (float)(5), 255.0f, (float)(5), 0.0f); 
        }

        float [] insufficientVoidImageArray = Threshold.floodFillInsufficient(variablePadImage);
        finalVariablePadImage = Image.createFloatImageFromArray(imageToUseForAdditionalLayer.getWidth(), imageToUseForAdditionalLayer.getHeight(), insufficientVoidImageArray);
        
        variablePadImage.decrementReferenceCount();
        
        float [] padAreaArray = Threshold.floodFillPadSolderArea(finalVariablePadImage);
        Image imageToBeDilated = Image.createFloatImageFromArray(imageToUseForAdditionalLayer.getWidth(), imageToUseForAdditionalLayer.getHeight(), padAreaArray);
        
        finalVariablePadImage.decrementReferenceCount();
        imageToUseForAdditionalLayer.decrementReferenceCount();
        
        Threshold.threshold(imageToBeDilated, (float)(5), 255.0f, (float)(5), 0.0f); 
        if (variablePadBorderSensitivity == 5 || variablePadBorderSensitivity == 6)
          finalVariablePadImage = Filter.dilate(imageToBeDilated, RegionOfInterest.createRegionFromImage(imageToBeDilated), 5);  
        else
          finalVariablePadImage = Filter.dilate(imageToBeDilated);  

        if(numberOfImageLayer == 2 || numberOfImageLayer == 3)
        {
          Threshold.threshold(finalVariablePadImage, 100.0f , 255.0f, 200.0f, 0.0f);
        }
        imageToBeDilated.decrementReferenceCount();
      }
      ///////////////////////Smoothing///////////////////////////////////////////////////////////////////////////////////////////////////
      ImagePlus gaussianSmoothingBuffImg;
  
      gaussianSmoothingBuffImg = new ImagePlus("", totalGaussVoidImage.getBufferedImage());
          
      GaussianBlur gaussianBlur = new GaussianBlur();
      gaussianBlur.blur(gaussianSmoothingBuffImg.getProcessor(), 5.0f);
    
      Image  totalGaussVoidSmoothingImage = Image.createFloatImageFromBufferedImage(gaussianSmoothingBuffImg.getBufferedImage());
      Transform.copyImageIntoImage(totalGaussVoidSmoothingImage, totalGaussVoidImage);
      totalGaussVoidSmoothingImage.decrementReferenceCount();
          
      gaussianSmoothingBuffImg.close();
      gaussianSmoothingBuffImg.flush(); 
       
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////     
      if (numberOfImageLayer == 1)
      {
          if (usingMasking)
          {
            if (usingMultiThresholdDetection)
            {             
              imageToUseForThresholding = createMultiThresholdImageForMasking(imageToUseForThresholding, maskSliceImage);
            
              Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 0.0f, (float)(10 - floodFillSensitivity), 255.0f); 

              Threshold.threshold(totalGaussVoidImage, (float)(5), 0.0f, (float)(5), 255.0f); 

              Image totalGaussVoidImageForMultiThreshold = new Image (maskSliceImage.getWidth(), maskSliceImage.getHeight());

              Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                     imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                     RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

              totalGaussVoidImage.decrementReferenceCount();
              totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
              Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

              totalGaussVoidImageForMultiThreshold.decrementReferenceCount();
            }
            else
            {
              int numHistogramBins = 256;
              int[] histogram = Threshold.histogram(totalGaussVoidImage, RegionOfInterest.createRegionFromImage(totalGaussVoidImage), numHistogramBins);
              int level = Threshold.getAutoThreshold(histogram);

              float floodFactor = 0;

              int stepSize = (int)level/2;
              if (floodFillSensitivity == 1 || floodFillSensitivity == 2) floodFactor = 0.0f;
              if (floodFillSensitivity == 3 || floodFillSensitivity == 4) floodFactor = (float)level - stepSize; 
              if (floodFillSensitivity == 5 || floodFillSensitivity == 6) floodFactor = (float)level;
              if (floodFillSensitivity == 7 || floodFillSensitivity == 8) floodFactor = (float)level + stepSize; 
              if (floodFillSensitivity == 9 || floodFillSensitivity == 10) floodFactor = (float)level + stepSize + stepSize; 
         
              Threshold.threshold(totalGaussVoidImage, (float) level - floodFactor, 0.0f, (float)level - floodFactor, 255.0f); 
            }
            
            if (usingFitPolynomialDetection)
            {
              imageToUseForThresholding = createFitPolynomialImageForMasking(imageToUseForThresholding, maskSliceImage);

              Image totalGaussVoidImageForMultiThreshold = new Image (maskSliceImage.getWidth(), maskSliceImage.getHeight());

              Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                     imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                     RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

              totalGaussVoidImage.decrementReferenceCount();
              totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
              Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

              totalGaussVoidImageForMultiThreshold.decrementReferenceCount();
            }
          }
          else
          {
            if (usingMultiThresholdDetection)
            {
               imageToUseForThresholding = createMultiThresholdImage(imageToUseForThresholding, imageToUse);
   
               Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 255.0f, (float)(10 - floodFillSensitivity), 0.0f); 
               Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 0.0f); 

               Image totalGaussVoidImageForMultiThreshold = new Image (imageToUse.getWidth(), imageToUse.getHeight());

               Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                     imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                     RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

                totalGaussVoidImage.decrementReferenceCount();
                totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
                Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

                totalGaussVoidImageForMultiThreshold.decrementReferenceCount();
           }
           else            
           {
                Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 255.0f, (float)(10 - floodFillSensitivity), 0.0f); 
                Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 0.0f); 
           }
            
            if (usingFitPolynomialDetection)
            {
              imageToUseForThresholding = createFitPolynomialImage(imageToUseForThresholding, imageToUse);

              Image totalGaussVoidImageForMultiThreshold = new Image (imageToUse.getWidth(), imageToUse.getHeight());

              Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                     imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                     RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

              totalGaussVoidImage.decrementReferenceCount();
              totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
              Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

              totalGaussVoidImageForMultiThreshold.decrementReferenceCount();
            }
          }
      }
      else
      {
          if (usingMasking)
          {
            int numHistogramBins = 256;   
            int[] histogram = Threshold.histogram(totalGaussVoidImage, RegionOfInterest.createRegionFromImage(totalGaussVoidImage), numHistogramBins);
            int level = Threshold.getAutoThreshold(histogram);

            float floodFactor = 0;

            int stepSize = (int)level/2;
            if (floodFillSensitivity == 1 || floodFillSensitivity == 2) floodFactor = 0.0f;
            if (floodFillSensitivity == 3 || floodFillSensitivity == 4) floodFactor = (float)level - stepSize; 
            if (floodFillSensitivity == 5 || floodFillSensitivity == 6) floodFactor = (float)level;
            if (floodFillSensitivity == 7 || floodFillSensitivity == 8) floodFactor = (float)level + stepSize; 
            if (floodFillSensitivity == 9 || floodFillSensitivity == 10) floodFactor = (float)level + stepSize + stepSize;  

            Threshold.threshold(totalGaussVoidImage, (float) level - floodFactor, 0.0f, (float)level - floodFactor, 255.0f);         
          }
          else
          {
            if (usingMultiThresholdDetection)
            {            
              imageToUseForThresholding = createMultiThresholdImage(imageToUseForThresholding, imageToUse);
            
              Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 0.0f, (float)(10 - floodFillSensitivity), 255.0f); 

              Threshold.threshold(totalGaussVoidImage, (float)(5), 0.0f, (float)(5), 255.0f); 

              Image totalGaussVoidImageForMultiThreshold = new Image (imageToUse.getWidth(), imageToUse.getHeight());

              Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                   imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                   RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

              totalGaussVoidImage.decrementReferenceCount();
              totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
              Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

              totalGaussVoidImageForMultiThreshold.decrementReferenceCount();

              Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 5.0f); 
           }
            else            
            {
              Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 255.0f, (float)(10 - floodFillSensitivity), 0.0f);
            }
            
            if (usingFitPolynomialDetection)
            {
              imageToUseForThresholding = createFitPolynomialImage(imageToUseForThresholding, imageToUse);
       
              Image totalGaussVoidImageForMultiThreshold = new Image (imageToUse.getWidth(), imageToUse.getHeight());

              Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                   imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                   RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

              totalGaussVoidImage.decrementReferenceCount();
              totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
              Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

              totalGaussVoidImageForMultiThreshold.decrementReferenceCount();

              Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 5.0f); 
           }
         }        
      }
   
      int jointImageWidth = imageToUse.getWidth();
      int jointImageHeight = imageToUse.getHeight();
      
      Image totalCenterVoidImage;
      
      if (usingMasking)
      {
          totalCenterVoidImage = new Image(jointRoiForUsingMask.getWidth(), jointRoiForUsingMask.getHeight());
      }
      else
      {
          totalCenterVoidImage = new Image(jointImageWidth, jointImageHeight);
      }
      
      if (numberOfImageLayer == 1)
      {
         Transform.copyImageIntoImage(totalGaussVoidImage, totalCenterVoidImage);
        
        //Variable Pad Method as Additional Layer
        if(variablePadBorderSensitivity > 0)
        {
          Arithmetic.orImages(finalVariablePadImage,  RegionOfInterest.createRegionFromImage(finalVariablePadImage),
               totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
               RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
        }
      }
    
      if (numberOfImageLayer == 2 || numberOfImageLayer == 3)
      {
          if (usingMasking)
          {
             jointImageWidth = maskSliceImage.getWidth();
             jointImageHeight = maskSliceImage.getHeight();
          }
          
          float floodFactor = 3 - (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO);
 
          Image centerVoidImage;
          
          if (usingMasking)
              centerVoidImage = detectImageVoid(totalGaussVoidImage, jointImageWidth, jointImageHeight, floodFactor);
          
          else
              centerVoidImage = detectImageVoid(imageToUse, jointImageWidth, jointImageHeight, floodFactor);
        
          Image resizedCenterVoidImage;

          if (usingMasking)
              resizedCenterVoidImage = Transform.resizeImage(centerVoidImage, totalGaussVoidImage.getWidth(), totalGaussVoidImage.getHeight());
          else
              resizedCenterVoidImage = Transform.resizeImage(centerVoidImage, jointImageWidth, jointImageHeight);

          if (usingMasking)
          {         
              Image maskTotalCenterVoidImage  =  Arithmetic.subtractImages(resizedCenterVoidImage,  RegionOfInterest.createRegionFromImage(resizedCenterVoidImage),
                     totalGaussVoidImage, RegionOfInterest.createRegionFromImage(totalGaussVoidImage));

              Transform.copyImageIntoImage(maskTotalCenterVoidImage, totalCenterVoidImage);

              maskTotalCenterVoidImage.decrementReferenceCount();
          }
          else
          {
               Arithmetic.andImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                 resizedCenterVoidImage, RegionOfInterest.createRegionFromImage(resizedCenterVoidImage), totalCenterVoidImage, 
                 RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
          }
          
          //Variable Pad Method as Additional Layer
          if(variablePadBorderSensitivity > 0)
          {
            Arithmetic.andImages(finalVariablePadImage,  RegionOfInterest.createRegionFromImage(finalVariablePadImage),
                   totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
                   RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
          }
          
           // Added by Jack Hwee - move this function from down to here to fix intermittent java crash     
          if (numberOfImageLayer == 2 && usingMasking)
          {
            Threshold.threshold(totalCenterVoidImage, 100.0f, 0.0f, 100.0f, 255.0f);
            
            Arithmetic.orImages(maskImage, adjustedJointRoiForMasking,
              totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
              RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
          }
          centerVoidImage.decrementReferenceCount();
          resizedCenterVoidImage.decrementReferenceCount();  
      }
      
      if (numberOfImageLayer == 3)
      {
          if (usingMasking)
          {
             jointImageWidth = maskSliceImage.getWidth();
             jointImageHeight = maskSliceImage.getHeight();
          }

          float floodFactor = 3 - (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE);

          Image boundVoidImage;
      
          if (usingMasking)       
              boundVoidImage = detectRightImageBound(totalGaussVoidImage, jointImageWidth, jointImageHeight, floodFactor);
         
          else
              boundVoidImage = detectRightImageBound(imageToUse, jointImageWidth, jointImageHeight, floodFactor);           
                  
          float imageExceedsNormalMean = Statistics.mean(boundVoidImage);
        
          // if mean of the image exceed certain value, the return image of detectRightImageBound is failed.
          if (imageExceedsNormalMean < 180.0f)
          {
             int width = boundVoidImage.getWidth();
             int height = boundVoidImage.getHeight();
              
             boundVoidImage.decrementReferenceCount();
             float[] white = new float[width * height]; 
             Arrays.fill(white, 255.0f); 

             boundVoidImage = Image.createFloatImageFromArray(width, height, white);         
          }
          
          // for debug purpose
//         String maskImagePath19 = Directory.getAlgorithmLearningDir(subtypeOfJoints.getPanel().getProject().getName()) + java.io.File.separator + "totalGaussVoidImage" + subtypeOfJoints.getShortName() + ".png";
//            try {
//                XrayImageIoUtil.savePngImage(totalGaussVoidImage, maskImagePath19);
//            } catch (DatastoreException ex) {
//                Logger.getLogger(LargePadVoidingAlgorithm.class.getName()).log(Level.SEVERE, null, ex);
//            } 
          
          Image resizedCenterVoidImage;
          
          if (usingMasking)
          {            
             resizedCenterVoidImage = Transform.resizeImage(boundVoidImage, totalCenterVoidImage.getWidth(), totalCenterVoidImage.getHeight());
            
             // this condition is to handle different type of createMaskImage above
             if(RegionOfInterest.createRegionFromImage(totalCenterVoidImage).equals(RegionOfInterest.createRegionFromImage(maskImage)))
             {
                if (imageExceedsNormalMean < 180.0f == false)
                    Threshold.threshold(resizedCenterVoidImage, 60, 255.0f, 61, 0.0f); 
            
               Arithmetic.andImages(resizedCenterVoidImage,  RegionOfInterest.createRegionFromImage(resizedCenterVoidImage),
                   totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
                   RegionOfInterest.createRegionFromImage(totalCenterVoidImage));     
             }
             else
             {                                      
                //Threshold.threshold(totalCenterVoidImage, 60, 255.0f, 61, 0.0f);
                
                Arithmetic.andImages(resizedCenterVoidImage,  RegionOfInterest.createRegionFromImage(resizedCenterVoidImage),
                 totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
                 RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
               
                //Threshold.threshold(totalCenterVoidImage, 1.0f, 255.0f, 1.0f, 0.0f);
                Threshold.threshold(totalCenterVoidImage, 60.0f, 0.0f, 61.0f, 255.0f);       
             }
               
             resizedCenterVoidImage.decrementReferenceCount();
             
             // Added by Jack Hwee - move this function from down to here to fix intermittent java crash
             Arithmetic.orImages(maskImage, adjustedJointRoiForMasking,
               totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
               RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
          }
          else
          {
            Arithmetic.andImages(boundVoidImage,  RegionOfInterest.createRegionFromImage(boundVoidImage),
              totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
              RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
          }
          boundVoidImage.decrementReferenceCount();
      }
      
      if(variablePadBorderSensitivity > 0)
      {
        finalVariablePadImage.decrementReferenceCount();
      }
      
      imageToUse.decrementReferenceCount();
      imageToUseForThresholding.decrementReferenceCount();

      if (usingMasking && numberOfImageLayer == 1)
      {
          Threshold.threshold(maskImage, 1.0f, 255.0f, 2.0f, 0.0f);

          Arithmetic.andImages(maskImage, adjustedJointRoiForMasking,
                 totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
                 RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
      }
   
      if (numberOfImageLayer == 2 || numberOfImageLayer == 3)
      {                 
        if (usingMasking)
        {
           //Commented out by Jack Hwee as call this function will cause intermittent crash
           //Arithmetic.orImages(maskImage, adjustedJointRoiForMasking,
           //   totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
           //  RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
                    
           Threshold.threshold(totalCenterVoidImage, 60.0f, 255.0f, 61.0f, 0.0f); 
  
           // this condition is to handle different type of createMaskImage above, condition below need to do masking once again to make sure all the bounding noise is removed
           if(RegionOfInterest.createRegionFromImage(totalCenterVoidImage).equals(RegionOfInterest.createRegionFromImage(maskImage)))
           {
               Image noiseReducedTotalCenterVoidImage = Arithmetic.subtractImages(totalCenterVoidImage, maskImage);

               Transform.copyImageIntoImage(noiseReducedTotalCenterVoidImage, totalCenterVoidImage);

               noiseReducedTotalCenterVoidImage.decrementReferenceCount();
           }     
        }
        else
          Threshold.threshold(totalCenterVoidImage, 100.0f, 255.0f, 100.0f, 0.0f); 
      }
      
      // count the number of "void" pixels
      int numberOfVoidPixels = Threshold.countPixelsInRange(totalCenterVoidImage, 100.0f, 255.0f);            
      
      // Store the percent voiding
      int numberOfPixelsTested = jointRoi.getWidth()*jointRoi.getHeight();
      
      float voidPercent = 0;
      
      if (usingMasking)
          voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTestedForMasking;
      else
          voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;
 
      //Siew Yeng - XCR-2210 - Combine two voids if the two voids are within an acceptable gap distance
      float [] largestVoidImageArray;
      float gapDistanceInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE);
      if(gapDistanceInMils > 0)
      {
        int gapDistanceInPixels = (int)Math.ceil(gapDistanceInMils / MathUtil.convertMillimetersToMils(MILIMETER_PER_PIXEL));
        
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(totalCenterVoidImage, gapDistanceInPixels);
      }
      else
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(totalCenterVoidImage);
      
      //create image with largest void area only
      Image largestVoidImage = Image.createFloatImageFromArray(totalCenterVoidImage.getWidth(), totalCenterVoidImage.getHeight(), largestVoidImageArray);
      largestVoidImageArray = null;
      
      //Jack Hwee-individual void
      int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 100.f, 255.0f);
      float individualVoidPercent = 0;
       
      if (usingMasking)
         individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTestedForMasking;
      else
          individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTested;
       
      JointInspectionResult jointResult = joint.getJointInspectionResult();
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);

      jointResult.addMeasurement(voidPercentMeasurement);

      // //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

      jointResult.addMeasurement(individualVoidPercentMeasurement);  
      
      if (usingMasking)
      {
        jointRoi = jointRoiForUsingMask;
      }
      
      if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, voidPercentMeasurement);

        if (usingMasking)
        {          
          if (FileUtil.exists(maskImagePath))
          { 
              try 
              {
                  FileUtil.delete(maskImagePath);
              } 
              catch (CouldNotDeleteFileException ex) 
              {
                  ex.printStackTrace();
              }
          }
        }
    
        if (usingMasking == false)
        {
          Image borderImage = new Image(jointRoi.getWidth(), jointRoi.getHeight());
          Paint.fillImage(borderImage, 0.0f);
  
          for (int i = 0; i < jointRoi.getHeight(); i++)
          {
            for (int j = 0; j < jointRoi.getWidth(); j++)
            {
              if (j == 0 || i == 0 || j == jointRoi.getWidth() - 1 || i ==jointRoi.getHeight() - 1)
              {
                borderImage.setPixelValue(j, i, 255.0f);
              }
            }
          }
        
          // Display total border area
          _diagnostics.postDiagnostics(reconstructionRegion,
                                       sliceNameEnum,
                                       joint,
                                       this,
                                       false,
                                       new OverlaySolderImageDiagnosticInfo(borderImage, jointRoi));
          
          borderImage.decrementReferenceCount();
        }
        
        //Siew Yeng - XCR-2566 - Display Voiding Percentage Value on Diagnostic Image
        OverlayImageDiagnosticInfo diagnosticInfo = new OverlayImageDiagnosticInfo(totalCenterVoidImage, jointRoi);
        String measurementString = MeasurementUnitsEnum.formatNumberIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), voidPercentMeasurement.getValue());
        String measurementLabel = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), measurementString);
        diagnosticInfo.setMeasurementLabel(measurementLabel);
        // Display total void area
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     diagnosticInfo);
                
       //Display Largest void area
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, individualVoidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     new OverlayLargestVoidImageDiagnosticInfo(largestVoidImage, jointRoi)); 
      }
      
      if (usingMasking)
           maskImage.decrementReferenceCount();
      
      totalGaussVoidImage.decrementReferenceCount();
  
      gaussianBlurBuffImg.unlock();
          
      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > voidAreaFailThreshold);
      boolean individualJointFailed = (individualVoidPercent > individualVoidAreaFailThreshold);

      if (jointFailed)
      {
        JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
            this, sliceNameEnum);
        voidPercentIndictment.addFailingMeasurement(voidPercentMeasurement);
        jointResult.addIndictment(voidPercentIndictment);
      }

      if (individualJointFailed)
      {
        JointIndictment individualVoidPercentIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING,
            this, sliceNameEnum);
        individualVoidPercentIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        jointResult.addIndictment(individualVoidPercentIndictment);
      } 

      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      if ((jointFailed || individualJointFailed) &&
           ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtypeOfJoints))
      {
        //Siew Yeng - add Largest void image into diagnostic slice
        java.awt.image.BufferedImage diagnosticBufferedImage = 
          com.axi.guiUtil.BufferedImageUtil.overlayImages(totalCenterVoidImage.getAlphaBufferedImage(java.awt.Color.WHITE), 
          largestVoidImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.DIAGNOSTIC_LARGEST_VOIDING_PIXELS.getColor()), 
          java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
        
        Image diagnosticImage = Image.createFloatImageFromBufferedImage(diagnosticBufferedImage);
        
          combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, 
                                                                            combineDiagnosticVoidImage, 
                                                                            diagnosticImage, 
                                                                            jointRoi, 
                                                                            sliceNameEnum);
        
        diagnosticBufferedImage.flush();
        diagnosticImage.decrementReferenceCount();
      }
      
      totalCenterVoidImage.decrementReferenceCount();
      largestVoidImage.decrementReferenceCount();
      
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !jointFailed);
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !individualJointFailed);
     
      if (usingMasking)
      {
        maskSliceImage.decrementReferenceCount();       
      }    
    }  
    
    if(combineDiagnosticVoidImage != null)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
      combineDiagnosticVoidImage.decrementReferenceCount();
    }
  }  
   
  /**
   * Algorithm version 2 - Re-structure floodfill method
   * End image of each layer - void must be in white, background in black
   * @author Siew Yeng
   */
  private void classifyFloodFillSlice2(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float voidAreaFailThreshold,
      float individualVoidAreaFailThreshold,
      int floodFillSensitivity,
      int numberOfImageLayer,
      Boolean usingMasking,
      Boolean usingMultiThresholdDetection,
      Boolean usingFitPolynomialDetection,
      final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);
    
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    Image sliceImage = slice.getOrthogonalImage();
   
    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);
    
    Image combineDiagnosticVoidImage = null;
    // For the first iteration through the joints, extract the joint's image normalized to the nominal size.
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);
      RegionOfInterest jointRoi = getRegionOfInterest(joint, subtypeOfJoints, MILIMETER_PER_PIXEL);
      
      IntegerRef numberOfPixelsTestedRef = new IntegerRef();
      Pair<Image, RegionOfInterest> totalCenterVoidImageAndNumberOfPixelsTested = classifyFloodFillSliceForEachJoint(sliceImage,
                                                                                                                reconstructionRegion,
                                                                                                                joint,
                                                                                                                jointRoi,
                                                                                                                subtypeOfJoints,
                                                                                                                floodFillSensitivity,
                                                                                                                numberOfImageLayer,
                                                                                                                usingMasking,
                                                                                                                usingMultiThresholdDetection,
                                                                                                                usingFitPolynomialDetection,
                                                                                                                numberOfPixelsTestedRef);
      
      Image totalCenterVoidImage = totalCenterVoidImageAndNumberOfPixelsTested.getFirst();
      
      int numberOfPixelsTested = numberOfPixelsTestedRef.getValue();

      if(subtypeOfJoints.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtypeOfJoints.getSubSubtypes())
        {
          if(subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION).equals(_FALSE))
            continue;
          
          Image finalSubRegionImage = classifySubSubtypeForEachJoint(sliceImage,
                                                                      reconstructionRegion,
                                                                      joint,
                                                                      jointRoi,
                                                                      subtypeOfJoints,
                                                                      subSubtype,
                                                                      sliceNameEnum,
                                                                      totalCenterVoidImage.getWidth(),
                                                                      totalCenterVoidImage.getHeight(),
                                                                      totalCenterVoidImageAndNumberOfPixelsTested.getSecond(),
                                                                      MILIMETER_PER_PIXEL);
          
          Arithmetic.orImages(totalCenterVoidImage,  RegionOfInterest.createRegionFromImage(totalCenterVoidImage),
                 finalSubRegionImage, RegionOfInterest.createRegionFromImage(finalSubRegionImage), totalCenterVoidImage, 
                 RegionOfInterest.createRegionFromImage(totalCenterVoidImage));

          finalSubRegionImage.decrementReferenceCount();
        }
      }
      
      jointRoi = totalCenterVoidImageAndNumberOfPixelsTested.getSecond();
      // count the number of "void" pixels
      int numberOfVoidPixels = Threshold.countPixelsInRange(totalCenterVoidImage, 100.0f, 255.0f);            

      float voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;
 
      //Siew Yeng - XCR-2210 - Combine two voids if the two voids are within an acceptable gap distance
      float [] largestVoidImageArray;
      float gapDistanceInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE);
      if(gapDistanceInMils > 0)
      {
        int gapDistanceInPixels = (int)Math.ceil(gapDistanceInMils / MathUtil.convertMillimetersToMils(MILIMETER_PER_PIXEL));
        
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(totalCenterVoidImage, gapDistanceInPixels);
      }
      else
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(totalCenterVoidImage);
      
      //create image with largest void area only
      Image largestVoidImage = Image.createFloatImageFromArray(totalCenterVoidImage.getWidth(), totalCenterVoidImage.getHeight(), largestVoidImageArray);
      largestVoidImageArray = null;
      
      //Jack Hwee-individual void
      int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 100.f, 255.0f);
      float individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTested;
       
      JointInspectionResult jointResult = joint.getJointInspectionResult();
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);

      jointResult.addMeasurement(voidPercentMeasurement);

      //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

      jointResult.addMeasurement(individualVoidPercentMeasurement);  
      
      if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, voidPercentMeasurement);
        
        if (usingMasking == false)
        {
          Image borderImage = new Image(jointRoi.getWidth(), jointRoi.getHeight());
          Paint.fillImage(borderImage, 0.0f);
  
          for (int i = 0; i < jointRoi.getHeight(); i++)
          {
            for (int j = 0; j < jointRoi.getWidth(); j++)
            {
              if (j == 0 || i == 0 || j == jointRoi.getWidth() - 1 || i ==jointRoi.getHeight() - 1)
              {
                borderImage.setPixelValue(j, i, 255.0f);
              }
            }
          }
        
          // Display total border area
          _diagnostics.postDiagnostics(reconstructionRegion,
                                       sliceNameEnum,
                                       joint,
                                       this,
                                       false,
                                       new OverlaySolderImageDiagnosticInfo(borderImage, jointRoi));
          
          borderImage.decrementReferenceCount();
        }
        
        //Siew Yeng - XCR-2566 - Display Voiding Percentage Value on Diagnostic Image
        OverlayImageDiagnosticInfo diagnosticInfo = new OverlayImageDiagnosticInfo(totalCenterVoidImage, jointRoi);
        String measurementString = MeasurementUnitsEnum.formatNumberIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), voidPercentMeasurement.getValue());
        String measurementLabel = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), measurementString);
        diagnosticInfo.setMeasurementLabel(measurementLabel);
        
        // Display total void area
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     diagnosticInfo);
                
       //Display Largest void area
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, individualVoidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     new OverlayLargestVoidImageDiagnosticInfo(largestVoidImage, jointRoi)); 
      }
          
      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > voidAreaFailThreshold);
      boolean individualJointFailed = (individualVoidPercent > individualVoidAreaFailThreshold);

      if (jointFailed)
      {
        JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
            this, sliceNameEnum);
        voidPercentIndictment.addFailingMeasurement(voidPercentMeasurement);
        jointResult.addIndictment(voidPercentIndictment);
      }

      if (individualJointFailed)
      {
        JointIndictment individualVoidPercentIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING,
            this, sliceNameEnum);
        individualVoidPercentIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        jointResult.addIndictment(individualVoidPercentIndictment);
      } 
      
      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      if ((jointFailed || individualJointFailed) &&
           ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtypeOfJoints))
      {
        //Siew Yeng - add Largest void image into diagnostic slice
        java.awt.image.BufferedImage diagnosticBufferedImage = 
          com.axi.guiUtil.BufferedImageUtil.overlayImages(totalCenterVoidImage.getAlphaBufferedImage(java.awt.Color.WHITE), 
          largestVoidImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.DIAGNOSTIC_LARGEST_VOIDING_PIXELS.getColor()), 
          java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
        
        Image diagnosticImage = Image.createFloatImageFromBufferedImage(diagnosticBufferedImage);
        
          combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, 
                                                                            combineDiagnosticVoidImage, 
                                                                            diagnosticImage, 
                                                                            jointRoi, 
                                                                            sliceNameEnum);
        
        diagnosticBufferedImage.flush();
        diagnosticImage.decrementReferenceCount();
      }
      
      totalCenterVoidImage.decrementReferenceCount();
      largestVoidImage.decrementReferenceCount();

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !jointFailed);
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !individualJointFailed);   
    }  
    
    if(combineDiagnosticVoidImage != null)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
      combineDiagnosticVoidImage.decrementReferenceCount();
    }
  }
  
  /**
   * Algorithm version 2
   * End image of each layer - void must be in white, background in black
   * @author Siew Yeng
   */
  private Pair<Image,RegionOfInterest> classifyFloodFillSliceForEachJoint(Image sliceImage,
                                                      ReconstructionRegion reconstructionRegion,
                                                      JointInspectionData joint,
                                                      RegionOfInterest jointRoi,
                                                      Subtype subtypeOfJoints,
                                                      int floodFillSensitivity,
                                                      int numberOfImageLayer,
                                                      Boolean usingMasking,
                                                      Boolean usingMultiThresholdDetection,
                                                      Boolean usingFitPolynomialDetection,
                                                      IntegerRef numberOfPixelTested)
  {
    Assert.expect(sliceImage != null);
    Assert.expect(joint != null);
        
    //Siew Yeng - XCR-3841 - noise reduction for floodfill
    int version = Integer.parseInt((String)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_VERSION));
    int noiseReduction = (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION_FOR_FLOODFILL);
    int numberOfTimesToOpenVoidPixelImage = Math.max(0, noiseReduction - 1);
    
    // if we apply a blur, we need to use a different image/roi
    Image imageToUse = Filter.convolveLowpass(sliceImage, jointRoi);

    Image imageToUseForThresholding = new Image(imageToUse.getWidth(), imageToUse.getHeight());     
    Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);

    Image maskImage = null;

    // Store the percent voiding
    int numberOfPixelsTested = jointRoi.getWidth() * jointRoi.getHeight();

    String maskImagePath = Directory.getAlgorithmLearningDir(subtypeOfJoints.getPanel().getProject().getName()) + java.io.File.separator + "mask2#" + subtypeOfJoints.getShortName() + ".png";
    //Siew Yeng - XCR-3319 - Software crash due to masking
    RegionOfInterest jointRoiForUsingMask = null;
    if (usingMasking)
    {
      Pair<Image,RegionOfInterest> maskImageAndRoi = createMaskImage(sliceImage, reconstructionRegion, jointRoi, joint, subtypeOfJoints);
      maskImage = maskImageAndRoi.getFirst();
      jointRoiForUsingMask = maskImageAndRoi.getSecond();

      if(maskImage == null)
      {
        usingMasking = false;
        LocalizedString warningText = new LocalizedString("ALGDIAG_MASK_IMAGE_CREATE_FAILED_KEY",
                                                 new Object[] {subtypeOfJoints.getShortName()});

        AlgorithmUtil.raiseAlgorithmWarning(warningText);
      }
      else
      {
        // if the reconstruction image has more than one pad, jointRoiForUsingMask need to be separated
        if (reconstructionRegion.getPads().size() == 1)
          jointRoi = jointRoiForUsingMask;
        
        imageToUse.decrementReferenceCount();
        imageToUse = new Image(jointRoi.getWidth(), jointRoi.getHeight());
        Transform.copyImageIntoImage(sliceImage, jointRoi, imageToUse, RegionOfInterest.createRegionFromImage(imageToUse));

        numberOfPixelsTested = Threshold.countPixelsInRange(maskImage, 0.0f, 0.5f);
      }
    }

    ImagePlus gaussianBlurBuffImg = new ImagePlus("", imageToUse.getBufferedImage());

    float gaussianBlurThreshold = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR);

    GaussianBlur gaussianBlur = new GaussianBlur();
    gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurThreshold);

    gaussianBlurBuffImg.lockSilently();

    Image filterGaussImage = Image.createFloatImageFromBufferedImage(gaussianBlurBuffImg.getBufferedImage());

    gaussianBlurBuffImg.close();
    gaussianBlurBuffImg.flush();       
    
    Image totalGaussVoidImage = Arithmetic.subtractImages(imageToUse, filterGaussImage);
    
    filterGaussImage.decrementReferenceCount();

    /////////////////Variablepad method as additional layer/////////////////////////
    int variablePadBorderSensitivity = (Integer)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL);

    Image finalVariablePadImage = null; 

    if(variablePadBorderSensitivity > 0)
    {
      //XCR-2169 - Additional Layer for Floodfill method - handle additional layer when use masking
      Image imageToUseForAdditionalLayer = Image.createCopy(imageToUse, RegionOfInterest.createRegionFromImage(imageToUse));

      Image variablePadImage =  new Image(imageToUseForAdditionalLayer.getWidth(), imageToUseForAdditionalLayer.getHeight());

      if (variablePadBorderSensitivity > 6)
      {
        variablePadImage.decrementReferenceCount();
        variablePadImage = Image.createCopy(imageToUseForAdditionalLayer, RegionOfInterest.createRegionFromImage(imageToUseForAdditionalLayer));       
        int numHistogramBins = 256;
        int[] histogram = Threshold.histogram(imageToUseForAdditionalLayer, RegionOfInterest.createRegionFromImage(imageToUseForAdditionalLayer), numHistogramBins);
        int level = Threshold.getAutoThreshold(histogram);

        //if (variablePadBorderSensitivity == 6) level = level - 1;
        if (variablePadBorderSensitivity == 7) level = level - 2;
        if (variablePadBorderSensitivity == 8) level = level - 3;
        if (variablePadBorderSensitivity == 9) level = level - 4;
        if (variablePadBorderSensitivity == 10) level = level - 5;

        Threshold.threshold(variablePadImage, (float) level, 0.0f, (float)level, 255.0f);
      }
      else
      {
        Transform.copyImageIntoImage(totalGaussVoidImage, variablePadImage);
        if (variablePadBorderSensitivity == 5) 
          Threshold.threshold(variablePadImage, (float)(1), 255.0f, (float)(1), 0.0f);
        else if (variablePadBorderSensitivity == 6) 
          Threshold.threshold(variablePadImage, (float)(0), 255.0f, (float)(0), 0.0f);
        else
          Threshold.threshold(variablePadImage, (float)(5 - variablePadBorderSensitivity), 255.0f, (float)(5 - variablePadBorderSensitivity), 0.0f);

        Threshold.threshold(variablePadImage, (float)(5), 255.0f, (float)(5), 0.0f); 
      }

      float [] insufficientVoidImageArray = Threshold.floodFillInsufficient(variablePadImage);
      finalVariablePadImage = Image.createFloatImageFromArray(imageToUseForAdditionalLayer.getWidth(), imageToUseForAdditionalLayer.getHeight(), insufficientVoidImageArray);

      variablePadImage.decrementReferenceCount();

      float [] padAreaArray = Threshold.floodFillPadSolderArea(finalVariablePadImage);
      Image imageToBeDilated = Image.createFloatImageFromArray(imageToUseForAdditionalLayer.getWidth(), imageToUseForAdditionalLayer.getHeight(), padAreaArray);

      finalVariablePadImage.decrementReferenceCount();
      imageToUseForAdditionalLayer.decrementReferenceCount();

      Threshold.threshold(imageToBeDilated, (float)(5), 255.0f, (float)(5), 0.0f); 
      if (variablePadBorderSensitivity == 5 || variablePadBorderSensitivity == 6)
        finalVariablePadImage = Filter.dilate(imageToBeDilated, RegionOfInterest.createRegionFromImage(imageToBeDilated), 5);  
      else
        finalVariablePadImage = Filter.dilate(imageToBeDilated);  

      imageToBeDilated.decrementReferenceCount();
      
    }
    ///////////////////////Smoothing///////////////////////////////////////////////////////////////////////////////////////////////////
    ImagePlus gaussianSmoothingBuffImg = new ImagePlus("", totalGaussVoidImage.getBufferedImage());

    //GaussianBlur gaussianBlur2 = new GaussianBlur();
    gaussianBlur.blur(gaussianSmoothingBuffImg.getProcessor(), 5.0f);

    Image totalGaussVoidSmoothingImage = Image.createFloatImageFromBufferedImage(gaussianSmoothingBuffImg.getBufferedImage());
    Transform.copyImageIntoImage(totalGaussVoidSmoothingImage, totalGaussVoidImage);
    totalGaussVoidSmoothingImage.decrementReferenceCount();

    gaussianSmoothingBuffImg.close();
    gaussianSmoothingBuffImg.flush(); 
    gaussianBlurBuffImg.unlock();

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    
    //1. 1st layer  + multithreshold + polynomial fitting
    int jointImageWidth = imageToUse.getWidth();
    int jointImageHeight = imageToUse.getHeight();

    Image totalCenterVoidImage = new Image(jointImageWidth, jointImageHeight);

    //////////////////////////////////////////////////////////////////////////////////////////////////
    //multithreshold
    if (usingMultiThresholdDetection)
    {             
      if(usingMasking)
      {
        imageToUseForThresholding = createMultiThresholdImageForMasking(imageToUseForThresholding, imageToUse);

        Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 0.0f, (float)(10 - floodFillSensitivity), 255.0f); 
        Threshold.threshold(totalGaussVoidImage, (float)(5), 0.0f, (float)(5), 255.0f); 
      }
      else
      {
        imageToUseForThresholding = createMultiThresholdImage(imageToUseForThresholding, imageToUse);

        Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 255.0f, (float)(10 - floodFillSensitivity), 0.0f); 
        Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 0.0f); 
      }

      Image totalGaussVoidImageForMultiThreshold = new Image (imageToUse.getWidth(), imageToUse.getHeight());

      Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                 imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                 RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

      totalGaussVoidImage.decrementReferenceCount();
      totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
      Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

      totalGaussVoidImageForMultiThreshold.decrementReferenceCount();
    }
    else
    {
      if(usingMasking)
      {
        int numHistogramBins = 256;
        int[] histogram = Threshold.histogram(totalGaussVoidImage, RegionOfInterest.createRegionFromImage(totalGaussVoidImage), numHistogramBins);
        int level = Threshold.getAutoThreshold(histogram);

        float floodFactor = 0;

        //Siew Yeng - XCR-3603 - fix void detection does not show difference when change sensitivity for floodfill with masking
        float stepSize = 0;
        
        if(version == 2)
        {
          stepSize = level/10;
        }
        else if(version == 3)
        {
          stepSize = (float)level/10;
        }
              
        floodFactor = level - (11-floodFillSensitivity) * stepSize;
        
        Threshold.threshold(totalGaussVoidImage, (float) level - floodFactor, 0.0f, (float)level - floodFactor, 255.0f);
      }
      else
      {
        Threshold.threshold(totalGaussVoidImage, (float)(10 - floodFillSensitivity), 255.0f, (float)(10 - floodFillSensitivity), 0.0f); 
        Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 0.0f);
      }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Polynomial fitting
    if (usingFitPolynomialDetection)
    {
      if(usingMasking)
        imageToUseForThresholding = createFitPolynomialImageForMasking(imageToUseForThresholding, imageToUse);
      else
        imageToUseForThresholding = createFitPolynomialImage(imageToUseForThresholding, imageToUse);

      Image totalGaussVoidImageForMultiThreshold = new Image (imageToUse.getWidth(), imageToUse.getHeight());

      Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
                       imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), totalGaussVoidImageForMultiThreshold, 
                       RegionOfInterest.createRegionFromImage(totalGaussVoidImageForMultiThreshold));

      totalGaussVoidImage.decrementReferenceCount();
      totalGaussVoidImage = new Image(totalGaussVoidImageForMultiThreshold.getWidth(), totalGaussVoidImageForMultiThreshold.getHeight());
      Transform.copyImageIntoImage(totalGaussVoidImageForMultiThreshold, totalGaussVoidImage);

      totalGaussVoidImageForMultiThreshold.decrementReferenceCount();
    }

    if(numberOfImageLayer == 1)
    {
      Transform.copyImageIntoImage(totalGaussVoidImage, totalCenterVoidImage);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //2. 2nd layer
    if (numberOfImageLayer >= 2)
    {
      float floodFactor = 3 - (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO);

      Image centerVoidImage;

      if (usingMasking)
        centerVoidImage = detectImageVoid(totalGaussVoidImage, jointImageWidth, jointImageHeight, floodFactor);
      else
        centerVoidImage = detectImageVoid(imageToUse, jointImageWidth, jointImageHeight, floodFactor);

      Image resizedCenterVoidImage = Transform.resizeImage(centerVoidImage, jointImageWidth, jointImageHeight);

      if(usingMasking)
      {      
        Image maskTotalCenterVoidImage = Arithmetic.subtractImages(resizedCenterVoidImage, RegionOfInterest.createRegionFromImage(resizedCenterVoidImage),
               totalGaussVoidImage, RegionOfInterest.createRegionFromImage(totalGaussVoidImage));

        Transform.copyImageIntoImage(maskTotalCenterVoidImage, totalCenterVoidImage);

        maskTotalCenterVoidImage.decrementReferenceCount();

        Threshold.threshold(totalCenterVoidImage, 60.0f, 255.0f, 61.0f, 0.0f);
      }
      else
      {
        Threshold.threshold(resizedCenterVoidImage, 100.f, 255.0f, 100.0f, 0.0f); 

        Arithmetic.orImages(totalGaussVoidImage,  RegionOfInterest.createRegionFromImage(totalGaussVoidImage),
          resizedCenterVoidImage, RegionOfInterest.createRegionFromImage(resizedCenterVoidImage), totalCenterVoidImage, 
          RegionOfInterest.createRegionFromImage(totalCenterVoidImage));

      }

      centerVoidImage.decrementReferenceCount();
      resizedCenterVoidImage.decrementReferenceCount();  
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //3. 3rd layer 
    if(numberOfImageLayer >= 3)
    {
      float floodFactor = 3 - (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE);

      Image boundVoidImage;

      if(usingMasking)       
        boundVoidImage = detectRightImageBound(totalGaussVoidImage, jointImageWidth, jointImageHeight, floodFactor);
      else
        boundVoidImage = detectRightImageBound(imageToUse, jointImageWidth, jointImageHeight, floodFactor);           

      float imageExceedsNormalMean = Statistics.mean(boundVoidImage);

      // if mean of the image exceed certain value, the return image of detectRightImageBound is failed.
      if (imageExceedsNormalMean < 180.0f)
      {
        int width = boundVoidImage.getWidth();
        int height = boundVoidImage.getHeight();

        boundVoidImage.decrementReferenceCount();
        float[] white = new float[width * height]; 
        Arrays.fill(white, 255.0f); 

        boundVoidImage = Image.createFloatImageFromArray(width, height, white);         
      }

      if (usingMasking)
      {
        Image resizedCenterVoidImage = Transform.resizeImage(boundVoidImage, totalCenterVoidImage.getWidth(), totalCenterVoidImage.getHeight());

        // this condition is to handle different type of createMaskImage above
        if(RegionOfInterest.createRegionFromImage(totalCenterVoidImage).equals(RegionOfInterest.createRegionFromImage(maskImage)))
        {
          if (imageExceedsNormalMean < 180.0f == false)
            Threshold.threshold(resizedCenterVoidImage, 60, 255.0f, 61, 0.0f); 

          Arithmetic.andImages(resizedCenterVoidImage, RegionOfInterest.createRegionFromImage(resizedCenterVoidImage),
             totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
             RegionOfInterest.createRegionFromImage(totalCenterVoidImage));     
        }
        else
        {    
          Threshold.threshold(resizedCenterVoidImage, 60, 255.0f, 61, 0.0f); 

          Arithmetic.orImages(resizedCenterVoidImage,  RegionOfInterest.createRegionFromImage(resizedCenterVoidImage),
            totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
            RegionOfInterest.createRegionFromImage(totalCenterVoidImage));

          Threshold.threshold(totalCenterVoidImage, 60.0f, 0.0f, 61.0f, 255.0f);       
        }

        resizedCenterVoidImage.decrementReferenceCount();
      }
      else
      {
        Threshold.threshold(boundVoidImage, 100.0f, 255.0f, 100.0f, 0.0f);

        Arithmetic.orImages(boundVoidImage,  RegionOfInterest.createRegionFromImage(boundVoidImage),
          totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
          RegionOfInterest.createRegionFromImage(totalCenterVoidImage));
      }
      boundVoidImage.decrementReferenceCount();
    }

    if (numberOfImageLayer >=2)
    {                 
      if (usingMasking)
      {                    
        // this condition is to handle different type of createMaskImage above, condition below need to do masking once again to make sure all the bounding noise is removed
        if(RegionOfInterest.createRegionFromImage(totalCenterVoidImage).equals(RegionOfInterest.createRegionFromImage(maskImage)))
        {
          Image noiseReducedTotalCenterVoidImage = Arithmetic.subtractImages(totalCenterVoidImage, maskImage);

          Transform.copyImageIntoImage(noiseReducedTotalCenterVoidImage, totalCenterVoidImage);

          noiseReducedTotalCenterVoidImage.decrementReferenceCount();
        }     
      }
    }

    imageToUse.decrementReferenceCount();
    imageToUseForThresholding.decrementReferenceCount();
    totalGaussVoidImage.decrementReferenceCount();

    //Variable Pad Method as Additional Layer
    if(variablePadBorderSensitivity > 0)
    {
      Arithmetic.orImages(finalVariablePadImage,  RegionOfInterest.createRegionFromImage(finalVariablePadImage),
             totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
             RegionOfInterest.createRegionFromImage(totalCenterVoidImage));

      finalVariablePadImage.decrementReferenceCount();
    }

    //Siew Yeng - XCR-3841 - noise reduction for floodfill
    if (numberOfTimesToOpenVoidPixelImage > 0)
    {
      /** false call avoidance idea PWL : Add a one-pixel boundary around the image that will operate with noise reduction. */

      Image noiseReducedVoidPixelsImage = Filter.opening(totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), numberOfTimesToOpenVoidPixelImage);
      totalCenterVoidImage.decrementReferenceCount();
      totalCenterVoidImage = noiseReducedVoidPixelsImage;
    }
    
    if (usingMasking)
    {
      Threshold.threshold(maskImage, 1.0f, 255.0f, 2.0f, 0.0f);

      //Siew Yeng - XCR-3319 - always use jointRoiForUsingMask for maskImage because this is the actual roi for maskImage
      Arithmetic.andImages(maskImage, jointRoiForUsingMask,
               totalCenterVoidImage, RegionOfInterest.createRegionFromImage(totalCenterVoidImage), totalCenterVoidImage, 
               RegionOfInterest.createRegionFromImage(totalCenterVoidImage)); 

      maskImage.decrementReferenceCount();
    } 
    
    numberOfPixelTested.setValue(numberOfPixelsTested);
    
    return new Pair(totalCenterVoidImage, jointRoi);
  }
  
   /**
   * Perform the classifyJoints routine on a specific slice, provided these settings.
   * The code is structured this way because there were a large number of Algorithm Settings specific to each slice.
   * @author Jack Hwee
   */
  private void classifyVariablePadSlice(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects,
      SliceNameEnum sliceNameEnum,
      float voidAreaFailThreshold,
      float individualVoidAreaFailThreshold,
      int variablePadBorderSensitivity,
      int variablePadVoidingSensitivity,
      final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    Image sliceImage = slice.getOrthogonalImage();

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    
//    float untestedBorderSizeInMM = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE);
//    int untestedBorderSizeInPixels = (int)Math.ceil(untestedBorderSizeInMM / AlgorithmUtil.getMillimetersPerPixel());
//    float leftOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET);
//    int leftOffsetInPixels = (int)Math.ceil(leftOffset / AlgorithmUtil.getMillimetersPerPixel());
//    float bottomOffset= (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET);
//    int bottomOffsetInPixels = (int)Math.ceil(bottomOffset / AlgorithmUtil.getMillimetersPerPixel());
//    float rightOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET);
//    int rightOffsetInPixels = (int)Math.ceil(rightOffset / AlgorithmUtil.getMillimetersPerPixel());
//    float topOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET);
//    int topOffsetInPixels = (int)Math.ceil(topOffset / AlgorithmUtil.getMillimetersPerPixel());
    
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);

    Image combineDiagnosticVoidImage = null;
    for (JointInspectionData joint : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, joint, joint.getInspectionRegion(), slice, false);
      RegionOfInterest jointRoi = getRegionOfInterest(joint, subtypeOfJoints, MILIMETER_PER_PIXEL);/*Locator.getRegionOfInterestAtMeasuredLocation(joint);
      
           // Untested Border Size Method - Standard or Customize
      if (isUntestedBorderSizeStandardMethod(subtypeOfJoints))
      {      
        if ((jointRoi.getWidth() - 2*untestedBorderSizeInPixels < 5.0) || (jointRoi.getHeight() - 2*untestedBorderSizeInPixels < 5.0))
        {
          // @todo PWL : post warning about the untested region being too large
          LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                            new Object[] {joint.getFullyQualifiedPadName()});
          AlgorithmUtil.raiseAlgorithmWarning(warningText);
          continue;
        }
        
        jointRoi.setWidthKeepingSameCenter(jointRoi.getWidth() - 2*untestedBorderSizeInPixels);
        jointRoi.setHeightKeepingSameCenter(jointRoi.getHeight() - 2*untestedBorderSizeInPixels);
      }
      else if (isUntestedBorderSizeCustomizeMethod(subtypeOfJoints))
      {
        if ((jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels < 5.0) || 
             (jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels < 5.0))
        {
          LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                            new Object[] {joint.getFullyQualifiedPadName()});
          AlgorithmUtil.raiseAlgorithmWarning(warningText);
          continue;
        }
        
        jointRoi.setMinXY(jointRoi.getMinX() + leftOffsetInPixels, jointRoi.getMinY() + topOffsetInPixels);
        jointRoi.setWidthKeepingSameMinX(jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels);
        jointRoi.setHeightKeepingSameMinY(jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels);
      }*/
      
      double gaussianBlurRadius = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR);
        
      // if we apply a blur, we need to use a different image/roi
      Image imageToUse = sliceImage;
      
      imageToUse = Filter.convolveLowpass(sliceImage, jointRoi);
           
      ImagePlus gaussianBlurBuffImg ;
    
      gaussianBlurBuffImg = new ImagePlus("", imageToUse.getBufferedImage());
      
      if (gaussianBlurBuffImg != null)
      {  
        GaussianBlur gaussianBlur = new GaussianBlur();   
        gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurRadius);
      }
      else
      {
        gaussianBlurBuffImg.setImage(imageToUse.getBufferedImage());
        GaussianBlur gaussianBlur = new GaussianBlur();
        gaussianBlur.blur(gaussianBlurBuffImg.getProcessor(), gaussianBlurRadius);
      }

      gaussianBlurBuffImg.lockSilently();
      
      Image filterGaussImage = Image.createFloatImageFromBufferedImage(gaussianBlurBuffImg.getBufferedImage());
   
      gaussianBlurBuffImg.close();
      gaussianBlurBuffImg.flush(); 
      
      Image totalGaussVoidImage;  
      totalGaussVoidImage = Arithmetic.subtractImages(imageToUse, filterGaussImage);
 
      Image variablePadImage = new Image(imageToUse.getWidth(), imageToUse.getHeight());
     
      filterGaussImage.decrementReferenceCount();
    
      if (variablePadBorderSensitivity > 6)
      {
        int numHistogramBins = 256;
        int[] histogram = Threshold.histogram(imageToUse, RegionOfInterest.createRegionFromImage(imageToUse), numHistogramBins);
        int level = Threshold.getAutoThreshold(histogram);
        
        //if (variablePadBorderSensitivity == 6) level = level - 1;
        if (variablePadBorderSensitivity == 7) level = level - 2;
        if (variablePadBorderSensitivity == 8) level = level - 3;
        if (variablePadBorderSensitivity == 9) level = level - 4;
        if (variablePadBorderSensitivity == 10) level = level - 5;
     
        Threshold.threshold(imageToUse, (float) level, 0.0f, (float)level, 255.0f);
        
        variablePadImage.decrementReferenceCount();
        variablePadImage = Image.createCopy(imageToUse, RegionOfInterest.createRegionFromImage(imageToUse));       
      }
      else
      {
        Transform.copyImageIntoImage(totalGaussVoidImage, variablePadImage);
        if (variablePadBorderSensitivity == 5) 
          Threshold.threshold(variablePadImage, (float)(1), 255.0f, (float)(1), 0.0f);
        else if (variablePadBorderSensitivity == 6) 
          Threshold.threshold(variablePadImage, (float)(0), 255.0f, (float)(0), 0.0f);
        else
          Threshold.threshold(variablePadImage, (float)(5 - variablePadBorderSensitivity), 255.0f, (float)(5 - variablePadBorderSensitivity), 0.0f);
        
        Threshold.threshold(variablePadImage, (float)(5), 255.0f, (float)(5), 0.0f); 
      }
  
      Threshold.threshold(totalGaussVoidImage, (float)(10 - variablePadVoidingSensitivity), 255.0f, (float)(10 - variablePadVoidingSensitivity), 0.0f);
      Threshold.threshold(totalGaussVoidImage, (float)(5), 255.0f, (float)(5), 0.0f); 
      
      float [] insufficientVoidImageArray = Threshold.floodFillInsufficient(variablePadImage);
            
      Image finalVariablePadImage = Image.createFloatImageFromArray(imageToUse.getWidth(), imageToUse.getHeight(), insufficientVoidImageArray);
  
      Image finalVoidImage = Arithmetic.subtractImages(totalGaussVoidImage, finalVariablePadImage);
             
      // count the number of "void" pixels
      int numberOfVoidPixels = Threshold.countPixelsInRange(finalVoidImage, 100.0f, 255.0f);            
 
      float [] padAreaArray = Threshold.floodFillPadSolderArea(finalVariablePadImage);
      finalVariablePadImage.decrementReferenceCount();
        
      Image imageToBeDilated = Image.createFloatImageFromArray(imageToUse.getWidth(), imageToUse.getHeight(), padAreaArray);
  
      Threshold.threshold(imageToBeDilated, (float)(5), 255.0f, (float)(5), 0.0f); 
      
      if (variablePadBorderSensitivity == 5 || variablePadBorderSensitivity == 6)
        finalVariablePadImage = Filter.dilate(imageToBeDilated, RegionOfInterest.createRegionFromImage(imageToBeDilated), 5);  
      else
        finalVariablePadImage = Filter.dilate(imageToBeDilated);  
  
      // Store the percent voiding
      int numberOfPixelsTested = Threshold.countPixelsInRange(finalVariablePadImage, 0.0f, 100.0f);   
  
      float voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;
      
      //Siew Yeng - XCR-2210 - Combine two voids if the two voids are within an acceptable gap distance
      float [] largestVoidImageArray;
      float gapDistanceInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE);
      if(gapDistanceInMils > 0)
      {
        int gapDistanceInPixels = (int)Math.ceil(gapDistanceInMils / MathUtil.convertMillimetersToMils(MILIMETER_PER_PIXEL));
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(finalVoidImage, gapDistanceInPixels);
      }
      else
        largestVoidImageArray = Threshold.floodFillLargestIndividualVoid(finalVoidImage);
      
      //create image with largest void area only
      Image largestVoidImage = Image.createFloatImageFromArray(finalVoidImage.getWidth(), finalVoidImage.getHeight(), largestVoidImageArray);
      largestVoidImageArray = null;
      
      //Jack Hwee-individual void
      int numberOfIndividualVoidPixels = Threshold.countPixelsInRange(largestVoidImage, 100.0f, 255.0f);
      float individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTested;
      
      JointInspectionResult jointResult = joint.getJointInspectionResult();
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, voidPercent);

      jointResult.addMeasurement(voidPercentMeasurement);
      
      // //Jack Hwee-individual void
      JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          joint.getPad(), sliceNameEnum, individualVoidPercent);

      jointResult.addMeasurement(individualVoidPercentMeasurement);  
      
      if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
      {
        Threshold.threshold(finalVariablePadImage, (float)(5), 25.0f, (float)(5), 0.0f); 
        
//        RegionOfInterest solderPadRegion = jointRoi;
//        solderPadRegion.setWidthKeepingSameCenter(jointRoi.getWidth()  - 2*untestedBorderSizeInPixels);
//        solderPadRegion.setHeightKeepingSameCenter(jointRoi.getHeight()  - 2*untestedBorderSizeInPixels);
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     new OverlayImageDiagnosticInfo(finalVariablePadImage, jointRoi));
        
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, voidPercentMeasurement);
         
        //Siew Yeng - XCR-2566 - Display Voiding Percentage Value on Diagnostic Image
        OverlayImageDiagnosticInfo diagnosticInfo = new OverlayImageDiagnosticInfo(finalVoidImage, jointRoi);
        String measurementString = MeasurementUnitsEnum.formatNumberIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), voidPercentMeasurement.getValue());
        String measurementLabel = MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(voidPercentMeasurement.getMeasurementUnitsEnum(), measurementString);
        diagnosticInfo.setMeasurementLabel(measurementLabel);
        
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     diagnosticInfo);
        
        //Display Largest void area
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, joint, individualVoidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     joint,
                                     this,
                                     false,
                                     new OverlayLargestVoidImageDiagnosticInfo(largestVoidImage, jointRoi)); 
      }
      
      imageToUse.decrementReferenceCount();
      variablePadImage.decrementReferenceCount();
      finalVariablePadImage.decrementReferenceCount();
      totalGaussVoidImage.decrementReferenceCount();
      imageToBeDilated.decrementReferenceCount();
      gaussianBlurBuffImg.unlock();

      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > voidAreaFailThreshold);
      boolean individualJointFailed = (individualVoidPercent > individualVoidAreaFailThreshold);

      if (jointFailed)
      {
        JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
            this, sliceNameEnum);
        voidPercentIndictment.addFailingMeasurement(voidPercentMeasurement);
        jointResult.addIndictment(voidPercentIndictment);
      }

      if (individualJointFailed)
      {
        JointIndictment individualVoidPercentIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING,
            this, sliceNameEnum);
        individualVoidPercentIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        jointResult.addIndictment(individualVoidPercentIndictment);
      } 
      
      //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
      if ((jointFailed || individualJointFailed) &&
           ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtypeOfJoints))
      {
        //Siew Yeng - add Largest void image into diagnostic slice
        java.awt.image.BufferedImage diagnosticBufferedImage = 
          com.axi.guiUtil.BufferedImageUtil.overlayImages(finalVoidImage.getAlphaBufferedImage(java.awt.Color.WHITE), 
          largestVoidImage.getAlphaBufferedImage(com.axi.guiUtil.LayerColorEnum.DIAGNOSTIC_LARGEST_VOIDING_PIXELS.getColor()), 
          java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
        
        Image diagnosticImage = Image.createFloatImageFromBufferedImage(diagnosticBufferedImage);
        
          combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, 
                                                                            combineDiagnosticVoidImage, 
                                                                            diagnosticImage, 
                                                                            jointRoi, 
                                                                            sliceNameEnum);
        
        diagnosticBufferedImage.flush();
        diagnosticImage.decrementReferenceCount();
      }
      
      finalVoidImage.decrementReferenceCount();
      largestVoidImage.decrementReferenceCount();

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !jointFailed);
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, joint, reconstructionRegion, sliceNameEnum, !individualJointFailed);
    }
    
    if(combineDiagnosticVoidImage != null)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
      combineDiagnosticVoidImage.decrementReferenceCount();
    }
  }
  
  /**
   * @author Patrick Lacz
   */
  private List<RegionOfInterest> createBackgroundSubRegions(float subRegionSizeInMM, RegionOfInterest jointRoi, final float MILIMETER_PER_PIXEL)
  {
    List<RegionOfInterest> listOfSubregions = new ArrayList<RegionOfInterest>();

    if (jointRoi.getRegionShapeEnum().equals(RegionShapeEnum.RECTANGULAR) == false)
    {
      RegionOfInterest copyOfJointRoi = new RegionOfInterest(jointRoi);
      copyOfJointRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);
      jointRoi = copyOfJointRoi;
    }

    int subRegionSizeInPixels = Math.round(subRegionSizeInMM / MILIMETER_PER_PIXEL);
    
    int subRegionWidthInPixels;
    int subRegionHeightInPixels;
    
    // Siew Yeng - XCR-2391 - LargePad Voiding Thickness Method improvement
    // Max limit of background region is change from 500 mils to 1000mils.
    // If background region size > 500mils, then only do this checking so that won't affect existing customers that using thickness method
    // If background region size > 500mils and it is > jointWidth / jointLength, make the sub region size the same with joint size
    float previousLimit = MathUtil.convertMilsToMillimeters(500);
    //Siew Yeng - XCR-3840 - set region size to joint size if the region size defined is maximum value
    float maximumValue = (Float)getAlgorithmSetting(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD).getMaximumValue(_ALGORITHM_VERSION);
    if((subRegionSizeInMM == maximumValue) || 
       (subRegionSizeInMM > previousLimit && subRegionSizeInPixels > jointRoi.getWidth()))
      subRegionWidthInPixels = jointRoi.getWidth();
    else
      subRegionWidthInPixels = subRegionSizeInPixels;

    if((subRegionSizeInMM == maximumValue) || 
       (subRegionSizeInMM > previousLimit && subRegionSizeInPixels > jointRoi.getHeight()))
      subRegionHeightInPixels = jointRoi.getHeight();
    else
      subRegionHeightInPixels = subRegionSizeInPixels;
     
      
    RegionOfInterest subRegion = new RegionOfInterest(jointRoi);
    subRegion.setWidthKeepingSameMinX(subRegionWidthInPixels);
    subRegion.setHeightKeepingSameMinY(subRegionHeightInPixels);

    // try to center the big regions and split the remainder near the edge of the region.
    float expectedSubRegionsAcrossX = jointRoi.getWidth() / subRegionWidthInPixels;
    int remainderInPixels = jointRoi.getWidth() - subRegionWidthInPixels * (int)Math.floor(expectedSubRegionsAcrossX);
    int startX = jointRoi.getMinX() + remainderInPixels / 2 - subRegionWidthInPixels;

    float expectedSubRegionsAcrossY = jointRoi.getHeight() / subRegionHeightInPixels;
    remainderInPixels = jointRoi.getHeight() - subRegionHeightInPixels * (int)Math.floor(expectedSubRegionsAcrossY);
    int startY = jointRoi.getMinY() + remainderInPixels / 2 - subRegionHeightInPixels;
    
    subRegion.setMinXY(startX, startY);

    // for each subregion, compute a background graylevel,
    while (subRegion.getMinY() <= jointRoi.getMaxY())
    {
      // go back to the beginning of the row.
      subRegion.setMinXY(startX, subRegion.getMinY());
      while (subRegion.getMinX() <= jointRoi.getMaxX())
      {
        if (subRegion.intersectsRegionOfInterest(jointRoi))
        {
          RegionOfInterest intersectedSubRegion = RegionOfInterest.createRegionFromIntersection(subRegion, jointRoi);

          Assert.expect(intersectedSubRegion.getWidth() > 0 && intersectedSubRegion.getHeight() > 0);
          listOfSubregions.add(new RegionOfInterest(intersectedSubRegion));
        }
        subRegion.translateXY(subRegion.getWidth(), 0);
      }
      subRegion.translateXY(0, subRegion.getHeight());
    }
    return listOfSubregions;
  }

  /**
   * @author Patrick Lacz
   */
  protected void classifyMeasurementGroup(MeasurementGroup measurementGroup,
                                          Map<Pad, ReconstructionRegion> padToRegionMap) throws XrayTesterException
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(padToRegionMap != null);

    Subtype subtype = measurementGroup.getSubtype();
    
    //Kee Chin Seong - Circular is not suppose have subtype running component level measurement
    if(isUsingCircularVoidMethod(subtype))
       return;
    
    classifyMeasurementGroupSlice(measurementGroup,
                                  SliceNameEnum.PAD,
                                  ((Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD)).floatValue());
  }
  
  /**
   * @author Siew Yeng
   */
  protected void classifyMeasurementGroups(List<MeasurementGroup> measurementGroups,
                                          Map<Pad, ReconstructionRegion> padToRegionMap) throws XrayTesterException
  {
    Assert.expect(measurementGroups != null);
    Assert.expect(padToRegionMap != null);

    classifyMeasurementGroupsSlice(measurementGroups,
                                  SliceNameEnum.PAD);
  }

  /**
   * For the measurement group, we examine the number of joints failing a different threshold than the joint indictments.
   * Unlike Grid Array, this is intended mostly for oddly shapped pads where the CAD has been broken down so that
   * A single pad is logically composed of several smaller pads.
   *
   * @author Patrick Lacz
   */
  private void classifyMeasurementGroupSlice(MeasurementGroup measurementGroup,
                                             SliceNameEnum sliceName,
                                             float percentOfAreaThreshold)
  {
    Assert.expect(measurementGroup != null);
    Assert.expect(sliceName != null);

    Subtype subtype = measurementGroup.getSubtype();

    float areaFailing = 0.f;
    float totalArea = 0.f;

    // compute the area of voiding in each joint and the total area of all the joints.
    for (JointInspectionResult jointResult : measurementGroup.getJointMeasurements())
    {
      if (jointResult.hasJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.LARGE_PAD_VOIDING_PERCENT))
      {
        float percentFailing = jointResult.getJointMeasurement(sliceName, MeasurementEnum.LARGE_PAD_VOIDING_PERCENT).getValue();

        // all we are interested in is width and height, so it is okay if this is not associated to an orientation.
        RegionOfInterest jointRoi = jointResult.getJointInspectionData().getRegionOfInterestRelativeToInspectionRegionInPixels();
        float area = jointRoi.getWidth() * jointRoi.getHeight();
        areaFailing += ( 0.01f * percentFailing ) * area;
        totalArea += area;
      }
    }

    // convert to a percent
    float percentFailing = 100.0f * ( areaFailing / totalArea );
    ComponentInspectionData componentInspectionData = measurementGroup.getComponentInspectionData();

    ComponentMeasurement percentFailingMeasurement  = null;
    JointMeasurement jointPercentFailingMeasurement = null;
    float percentFailingFloatValue = 0.0f;
  
    if (Config.isComponentLevelClassificationEnabled())
    {
      percentFailingMeasurement = new ComponentMeasurement(this,
          measurementGroup.getSubtype(),
          MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT,
          componentInspectionData.getComponent(),
          sliceName,
          percentFailing);      
      componentInspectionData.getComponentInspectionResult().addMeasurement(percentFailingMeasurement);
      
      percentFailingFloatValue = percentFailingMeasurement.getValue();
    }
    else
    {
      jointPercentFailingMeasurement = new JointMeasurement(
            this,
            MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT,
            MeasurementUnitsEnum.PERCENT,
            componentInspectionData.getPadOneJointInspectionData().getPad(),
            sliceName,
            percentFailing);
      componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addMeasurement(jointPercentFailingMeasurement);
      
      percentFailingFloatValue = jointPercentFailingMeasurement.getValue();
    }

    // compare with the threshold
    if (percentFailing >= percentOfAreaThreshold)
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (percentFailingMeasurement != null)
        {
          ComponentIndictment voidingIndictment = new ComponentIndictment(IndictmentEnum.COMPONENT_VOIDING,
                                                                          this,
                                                                          sliceName,
                                                                          subtype,
                                                                          subtype.getJointTypeEnum());
          voidingIndictment.addFailingMeasurement(percentFailingMeasurement);
          componentInspectionData.getComponentInspectionResult().addIndictment(voidingIndictment);
        }        
      }
      else
      {
        if (jointPercentFailingMeasurement != null)
        {
          JointIndictment voidingIndictment = new JointIndictment(IndictmentEnum.COMPONENT_VOIDING, this, sliceName);
          voidingIndictment.addFailingMeasurement(jointPercentFailingMeasurement);
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(voidingIndictment);
        }
      }
    }
  }

  /**
   * For the measurement group, we examine the number of joints failing a different threshold than the joint indictments.
   * Unlike Grid Array, this is intended mostly for oddly shapped pads where the CAD has been broken down so that
   * A single pad is logically composed of several smaller pads.
   * Classify measurementGroups for component level
   *
   * @author Siew Yeng
   */
  private void classifyMeasurementGroupsSlice(List<MeasurementGroup> measurementGroups,
                                             SliceNameEnum sliceName)
  {
    Assert.expect(measurementGroups != null);
    Assert.expect(sliceName != null);

    List<Subtype> subtypes = new ArrayList();

    float areaFailing = 0.f;
    float totalArea = 0.f;

    for(MeasurementGroup measurementGroup : measurementGroups)
    {
      subtypes.add(measurementGroup.getSubtype());
      // compute the area of voiding in each joint and the total area of all the joints.
      for (JointInspectionResult jointResult : measurementGroup.getJointMeasurements())
      {
        if (jointResult.hasJointMeasurement(SliceNameEnum.PAD, MeasurementEnum.LARGE_PAD_VOIDING_PERCENT))
        {
          float percentFailing = jointResult.getJointMeasurement(sliceName, MeasurementEnum.LARGE_PAD_VOIDING_PERCENT).getValue();

          // all we are interested in is width and height, so it is okay if this is not associated to an orientation.
          RegionOfInterest jointRoi = jointResult.getJointInspectionData().getRegionOfInterestRelativeToInspectionRegionInPixels();
          
          //Siew Yeng - XCR-2168 - Component Level Voiding for Mixed Subtype 
          // - when use masking total area must follow the total pixel of mask image, should not be jointRoi.
          float area;
          
          if(isUsingMaskingMethod(measurementGroup.getSubtype()))
          {
            java.awt.image.BufferedImage bufferedImage = MaskImage.getInstance().getSubtypeMaskImage(measurementGroup.getSubtype().getLongName(), measurementGroup.getComponentInspectionData().getComponent().getDegreesRotationAfterAllRotations());
            Image maskImage = Image.createFloatImageFromBufferedImage(bufferedImage);
            area = Threshold.countPixelsInRange(maskImage, 0.0f, 0.5f);
            
            maskImage.decrementReferenceCount();
          }
          else
            area = jointRoi.getWidth() * jointRoi.getHeight();
          
          Assert.expect(area != 0);
          
          areaFailing += ( 0.01f * percentFailing ) * area;
          totalArea += area;
        }
      }
    }

    // convert to a percent
    float percentFailing = 100.0f * ( areaFailing / totalArea );
    ComponentInspectionData componentInspectionData = measurementGroups.get(0).getComponentInspectionData();

    ComponentMeasurement percentFailingMeasurement  = null;
    JointMeasurement jointPercentFailingMeasurement = null;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      percentFailingMeasurement = new ComponentMeasurement(this,
          subtypes,
          MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT,
          MeasurementUnitsEnum.PERCENT,
          componentInspectionData.getComponent(),
          sliceName,
          percentFailing);      
      componentInspectionData.getComponentInspectionResult().addMeasurement(percentFailingMeasurement);
    }
    else
    {
      jointPercentFailingMeasurement = new JointMeasurement(
            this,
            MeasurementEnum.LARGE_PAD_COMPONENT_VOIDING_PERCENT,
            MeasurementUnitsEnum.PERCENT,
            componentInspectionData.getPadOneJointInspectionData().getPad(),
            sliceName,
            percentFailing);
      componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addMeasurement(jointPercentFailingMeasurement);
    }

    // compare with the threshold
    if (percentFailing >= ((Float)subtypes.get(0).getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD)).floatValue())
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (percentFailingMeasurement != null)
        {
          ComponentIndictment voidingIndictment = new ComponentIndictment(IndictmentEnum.COMPONENT_VOIDING,
                                                                          this,
                                                                          sliceName,
                                                                          subtypes,
                                                                          subtypes.get(0).getJointTypeEnum());
          voidingIndictment.addFailingMeasurement(percentFailingMeasurement);
          componentInspectionData.getComponentInspectionResult().addIndictment(voidingIndictment);
        }
      }
      else
      {
        if (jointPercentFailingMeasurement != null)
        {
          JointIndictment voidingIndictment = new JointIndictment(IndictmentEnum.COMPONENT_VOIDING, this, sliceName);
          voidingIndictment.addFailingMeasurement(jointPercentFailingMeasurement);
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(voidingIndictment);
        }
      }
    }
  }
  
  /**
   * This method learns a decent value for subregion size.
   * That value is arbitrarily chosen to be the minimum of half the size of the longest dimension.
   * This should give reasonably large regions given the size of the pad. The resolution should only need
   * to be refined if there are a large number of shading artifacts over the area of the pad.
   * @author Patrick Lacz
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Find the smallest value of: half the longest dimension of the pad.
    // @todo, this learned value should be maxed out at some value other than the setting's max value.
    float smallestSubregionSizeInMM = MathUtil.convertMilsToMillimeters(100.f);

    ManagedOfflineImageSet imageSetToUse = typicalBoardImages;
    if (imageSetToUse.getReconstructionRegions().isEmpty())
      imageSetToUse = unloadedBoardImages;

    for (ReconstructionRegion region : imageSetToUse.getReconstructionRegions())
    {
      for (JointInspectionData jointInspectionData : region.getInspectableJointInspectionDataList(subtype))
      {
        PanelRectangle panelRectInNM = jointInspectionData.getRegionRectangleRelativeToPanelOriginInNanoMeters();
        double longestDimensionInNM = Math.max(panelRectInNM.getWidth(), panelRectInNM.getHeight());
        float halfLongestDimensionInMM = (float)(MathUtil.convertNanometersToMillimeters(longestDimensionInNM) / 2.0);

        if (smallestSubregionSizeInMM > halfLongestDimensionInMM)
          smallestSubregionSizeInMM = halfLongestDimensionInMM;
      }
    }
    
    CircularVoidingAlgorithm.learnAlgorithmSettings(subtype, typicalBoardImages, unloadedBoardImages);
    
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD, smallestSubregionSizeInMM);
  }


  /**
   * @author Patrick Lacz
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD);
  }
  
  private final float _FLOOD_FILL_INTENSITY = 300.f; // larger than the image values
  
   /**
   * @author Jack Hwee
   */
  private Image detectRightImageBound(Image aggregateMedianImage, int widthOfJointImage, int heightOfJointImage, float floodFactor)
  {
    Assert.expect(aggregateMedianImage != null);
    Assert.expect(widthOfJointImage > 0);
    Assert.expect(heightOfJointImage > 0);

    // the flood fill image has a one pixel border around the entire image. This is used so that the flood fill algorithm
    // visits all the pixels around the edge of the image.
    Image floodFillImage = new Image(widthOfJointImage, heightOfJointImage);
   
    Paint.fillImage(floodFillImage, _FLOOD_FILL_INTENSITY);
   
    Transform.copyImageIntoImage(aggregateMedianImage, RegionOfInterest.createRegionFromImage(aggregateMedianImage), floodFillImage, RegionOfInterest.createRegionFromImage(floodFillImage));

    // allow decreasing gradients from the edge, but not increasing (or just barely - the .25 should cover up the bumps)
    Paint.floodFillGradient(floodFillImage,
                            RegionOfInterest.createRegionFromImage(floodFillImage),
                            0, 0,
                            _FLOOD_FILL_INTENSITY,
                            2.f * _FLOOD_FILL_INTENSITY, floodFactor);
    
       return floodFillImage;
  }
  
   /**
   * @author Jack Hwee
   */
   private Image detectImageVoid(Image aggregateMedianImage, int widthOfJointImage, int heightOfJointImage, float floodFactor)
   {
    Assert.expect(aggregateMedianImage != null);
    Assert.expect(widthOfJointImage > 0);
    Assert.expect(heightOfJointImage > 0);

    RegionOfInterest subImageRoi = new RegionOfInterest(0, 0, widthOfJointImage, heightOfJointImage, 0, RegionShapeEnum.RECTANGULAR);

    // the flood fill image has a one pixel border around the entire image. This is used so that the flood fill algorithm
    // visits all the pixels around the edge of the image.
    Image floodFillImage = new Image(widthOfJointImage + 2, heightOfJointImage + 2);
 
    Paint.fillImage(floodFillImage, _FLOOD_FILL_INTENSITY);
       
    RegionOfInterest floodFillRoi = new RegionOfInterest(subImageRoi);
    floodFillRoi.setMinXY(1, 1);
  
    subImageRoi.translateXY(0, 0);

    Transform.copyImageIntoImage(aggregateMedianImage, subImageRoi, floodFillImage, floodFillRoi);

    // allow decreasing gradients from the edge, but not increasing (or just barely - the .25 should cover up the bumps)
    Paint.floodFillGradient(floodFillImage,
                            RegionOfInterest.createRegionFromImage(floodFillImage),
                            0, 0,
                            _FLOOD_FILL_INTENSITY,
                            2.f * _FLOOD_FILL_INTENSITY, floodFactor);
      
    return floodFillImage;
 
  }
   
   /*
    * @author Kee Chin Seong
    */
   static public boolean isUsingCircularVoidMethod(Subtype subtype)
   {
      Assert.expect(subtype != null);
      String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD);

      if (voidingChoice.equals(_CIRCULAR_VOID_METHOD))
      {
        return true;
      }

      return false;
   }
   
     /**
   * @author Jack Hwee
   */
  static public boolean isThicknessMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD);

    if (voidingChoice.equals(_THICKNESS_METHOD))
    {
      return true;
    }

    return false;
  }

   /**
   * @author Jack Hwee
   */
  static public boolean isFloodFillMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD);

    if (voidingChoice.equals(_FLOODFILL_METHOD))
    {
      return true;
    }

    return false;
  }
  
   /**
   * @author Jack Hwee
   */
  static public boolean isVariablePadMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_METHOD);

    if (voidingChoice.equals(_VARIABLE_PAD_METHOD))
    {
      return true;
    }

    return false;
  }
  
  /**
   * @author Jack Hwee
   */
  static public boolean isUsingMaskingMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
//    String usingMasking = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES);
//
//    if (usingMasking.equals(_TRUE))
//    {
//      return true;
//    }
//
//    return false;
    //Siew Yeng
    return AlgorithmUtil.isSubtypeUsingMaskingMethod(subtype);
  }
  
    /**
   * @author Jack Hwee
   */
  static public boolean isUsingMultiThresholdDetection(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String usingMultiThresholdDetection = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MULTI_THRESHOLD);

    if (usingMultiThresholdDetection.equals(_TRUE))
    {
      return true;
    }

    return false;
  }
  
   /**
   * @author Jack Hwee
   */
  static public boolean isUsingFitPolynomialDetection(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String usingMultiThresholdDetection = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_FIT_POLYNOMIAL);

    if (usingMultiThresholdDetection.equals(_TRUE))
    {
      return true;
    }

    return false;
  }
  
  /**
   * @author Jack Hwee
   */
  static public void convertToGrayMaskImageIfNecessary(String path)
  {
    ImagePlus maskImage = ij.IJ.openImage(path);

    if (maskImage.getType() == ImagePlus.GRAY8)
    {         
      maskImage.flush();
      maskImage.close();
      return;
    }
    else
    {
      ij.process.ImageConverter img = new ij.process.ImageConverter(maskImage);
      img.convertToGray8();
      ij.IJ.save(maskImage, path);

      maskImage.flush();
      maskImage.close();
    } 
  }
 
  /**
   * For Masking, the value for background subtraction is higher
   * @author Jack Hwee
   */
  private Image createMultiThresholdImageForMasking(Image imageToUseForThresholding, Image imageToUse)
  {
     int numHistogramBins = 256;   
     int[] histogram = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
  
     DoubleRef mean = new DoubleRef();
     DoubleRef stdDev = new DoubleRef();

     Statistics.meanStdDev(imageToUseForThresholding, mean, stdDev);

     float thresholdOffset = 255;
     float thresholdOffset2 = 255;
     float thresholdOffset3 = 255;
     float thresholdOffset4 = 255;
     float thresholdOffset5 = 255;     
     float thresholdOffset6 = 255;     
     float thresholdOffset7 = 255;     

     float meanFloat = (float)mean.getValue();

     if (stdDev.getValue() > 6.5f)
     {             
       int level = Threshold.getAutoThreshold(histogram);
       int level2 = AutoThresholder.triangle(histogram);
       int level7 = AutoThresholder.minErrorI(histogram);
      
       if (level > 0)
           thresholdOffset = Math.abs(meanFloat - level);
       if (level2 > 0)
           thresholdOffset2 = Math.abs(meanFloat - level2);
       if (level7 > 0)
          thresholdOffset7 = Math.abs(meanFloat - level7);

       if (meanFloat < level2 && level2 > 0)
           thresholdOffset2 = 255;

       if (level == level2)
           thresholdOffset2 = 255;
       
       if (level == level7)
           thresholdOffset7 = 255;
  
       ImagePlus imageToUseForThresholdingImagePlus;
       imageToUseForThresholdingImagePlus = new ImagePlus("", imageToUse.getBufferedImage());

       imageToUseForThresholding.decrementReferenceCount();
       imageToUseForThresholding = new Image(imageToUse.getWidth(), imageToUse.getHeight());

       BackgroundSubtracter.getInstance().subtractBackround(imageToUseForThresholdingImagePlus.getProcessor(), 300);
       Image  backgroundSubtractResultImage = Image.createFloatImageFromBufferedImage(imageToUseForThresholdingImagePlus.getBufferedImage());
       Transform.copyImageIntoImage(backgroundSubtractResultImage, imageToUseForThresholding);
       backgroundSubtractResultImage.decrementReferenceCount();

       imageToUseForThresholdingImagePlus.close();
       imageToUseForThresholdingImagePlus.flush(); 

       if (thresholdOffset < thresholdOffset2 && thresholdOffset < thresholdOffset3 && thresholdOffset < thresholdOffset7)
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level = Threshold.getAutoThreshold(histogramAfterSubtractBackground);
         
         if (level < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level = Threshold.getAutoThreshold(histogram);
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level), 0.0f, (float)(level), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if ((thresholdOffset2 < thresholdOffset3 && thresholdOffset2 < thresholdOffset && thresholdOffset2 < thresholdOffset7))
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);

         if (level2 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);
         }
          
         Threshold.threshold(imageToUseForThresholding, (float)(level2), 0.0f, (float)(level2), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if ((thresholdOffset7 < thresholdOffset2 && thresholdOffset7 < thresholdOffset && thresholdOffset7 < thresholdOffset3))
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level7 = AutoThresholder.minErrorI(histogramAfterSubtractBackground);
         
         if (level7 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level7 = AutoThresholder.minErrorI(histogramAfterSubtractBackground);
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level7), 0.0f, (float)(level7), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if (thresholdOffset2 == thresholdOffset3)
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);
         
         if (level2 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level2), 0.0f, (float)(level2), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
     }
     else
     {
       int level4 = AutoThresholder.yen(histogram);            
       int level5 = AutoThresholder.maxEntropy(histogram);    
       int level6 = AutoThresholder.li(histogram);
     
       if (level4 > 0)
           thresholdOffset4 = Math.abs(meanFloat - level4);
       if (level5 > 0)
           thresholdOffset5 = Math.abs(meanFloat - level5);
       if (level6 > 0)
           thresholdOffset6 = Math.abs(meanFloat - level6);

       if (level4 == level5 && level5 == level6)
           thresholdOffset5 = 255;

       ImagePlus imageToUseForThresholdingImagePlus;
       imageToUseForThresholdingImagePlus = new ImagePlus("", imageToUse.getBufferedImage());

       imageToUseForThresholding.decrementReferenceCount();
       imageToUseForThresholding = new Image(imageToUse.getWidth(), imageToUse.getHeight());

       BackgroundSubtracter.getInstance().subtractBackround(imageToUseForThresholdingImagePlus.getProcessor(), 300);
       Image  backgroundSubtractResultImage = Image.createFloatImageFromBufferedImage(imageToUseForThresholdingImagePlus.getBufferedImage());
       Transform.copyImageIntoImage(backgroundSubtractResultImage, imageToUseForThresholding);
       backgroundSubtractResultImage.decrementReferenceCount();

       imageToUseForThresholdingImagePlus.close();
       imageToUseForThresholdingImagePlus.flush(); 

       if (thresholdOffset4 < thresholdOffset5)
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level4 = AutoThresholder.yen(histogramAfterSubtractBackground);   
         
         if (level4 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level4 = AutoThresholder.yen(histogramAfterSubtractBackground); 
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level4), 0.0f, (float)(level4), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if (thresholdOffset5 < thresholdOffset6 )
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level5 = AutoThresholder.maxEntropy(histogramAfterSubtractBackground);   
         
         if (level5 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level5 = AutoThresholder.maxEntropy(histogramAfterSubtractBackground);   
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level5), 0.0f, (float)(level5), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level6 = AutoThresholder.li(histogramAfterSubtractBackground);  
         
         if (level6 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level6 = AutoThresholder.li(histogramAfterSubtractBackground);  
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level6), 0.0f, (float)(level6), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
     }

     return imageToUseForThresholding;             
  }
  
  /**
   * @author Jack Hwee
   */
   private Image createMultiThresholdImage(Image imageToUseForThresholding, Image imageToUse)
   {
      int numHistogramBins = 256;   
      int[] histogram = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
    
      float thresholdOffset = 255;
      float thresholdOffset2 = 255;
      float thresholdOffset3 = 255;
      float thresholdOffset4 = 255;
      float thresholdOffset5 = 255;
      float thresholdOffset6 = 255;     
      float thresholdOffset7 = 255;     
     
      DoubleRef mean = new DoubleRef();
      DoubleRef stdDev = new DoubleRef();
  
      Statistics.meanStdDev(imageToUseForThresholding, mean, stdDev);

      float meanFloat = (float)mean.getValue();
      
      ImagePlus imageToUseForThresholdingImagePlus2;
      imageToUseForThresholdingImagePlus2 = new ImagePlus("", imageToUseForThresholding.getBufferedImage());

      double xMoment = imageToUseForThresholdingImagePlus2.getStatistics(ImageStatistics.CENTER_OF_MASS).xCenterOfMass;
      double yMoment = imageToUseForThresholdingImagePlus2.getStatistics(ImageStatistics.CENTER_OF_MASS).yCenterOfMass;
     
      double factor = yMoment / xMoment;
      
      imageToUseForThresholdingImagePlus2.close();
      imageToUseForThresholdingImagePlus2.flush(); 

      if (stdDev.getValue() > 6.5f && MathUtil.fuzzyLessThan(factor, 1.0) && MathUtil.fuzzyGreaterThan(factor, 0.8))
      {
       int level = Threshold.getAutoThreshold(histogram);
       int level2 = AutoThresholder.triangle(histogram);
       int level7 = AutoThresholder.minErrorI(histogram);
       
       if (level > 0)
           thresholdOffset = Math.abs(meanFloat - level);
       if (level2 > 0)
           thresholdOffset2 = Math.abs(meanFloat - level2);
       if (level7 > 0)
          thresholdOffset7 = Math.abs(meanFloat - level7);

       if (meanFloat < level2 && level2 > 0)
           thresholdOffset2 = 255;

       if (level == level2)
           thresholdOffset2 = 255;
       
       if (level == level7)
           thresholdOffset7 = 255;
       
       ImagePlus imageToUseForThresholdingImagePlus;
       imageToUseForThresholdingImagePlus = new ImagePlus("", imageToUseForThresholding.getBufferedImage());
 
       BackgroundSubtracter.getInstance().subtractBackround(imageToUseForThresholdingImagePlus.getProcessor(), 50);
       Image  backgroundSubtractResultImage = Image.createFloatImageFromBufferedImage(imageToUseForThresholdingImagePlus.getBufferedImage());
       Transform.copyImageIntoImage(backgroundSubtractResultImage, imageToUseForThresholding);
       backgroundSubtractResultImage.decrementReferenceCount();

       imageToUseForThresholdingImagePlus.close();
       imageToUseForThresholdingImagePlus.flush(); 
  
       if (thresholdOffset < thresholdOffset2 && thresholdOffset < thresholdOffset3 && thresholdOffset < thresholdOffset7)
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level = Threshold.getAutoThreshold(histogramAfterSubtractBackground);
         
         if (level < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level = Threshold.getAutoThreshold(histogram);
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level), 0.0f, (float)(level), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if ((thresholdOffset2 < thresholdOffset3 && thresholdOffset2 < thresholdOffset && thresholdOffset2 < thresholdOffset7))
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);

         if (level2 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);
         }
          
         Threshold.threshold(imageToUseForThresholding, (float)(level2), 0.0f, (float)(level2), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if ((thresholdOffset7 < thresholdOffset2 && thresholdOffset7 < thresholdOffset && thresholdOffset7 < thresholdOffset3))
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level7 = AutoThresholder.minErrorI(histogramAfterSubtractBackground);
         
         if (level7 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level7 = AutoThresholder.minErrorI(histogramAfterSubtractBackground);
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level7), 0.0f, (float)(level7), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if (thresholdOffset2 == thresholdOffset3)
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);
         
         if (level2 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level2 = AutoThresholder.triangle(histogramAfterSubtractBackground);
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level2), 0.0f, (float)(level2), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
     }
     else
     {
       int level6 = AutoThresholder.yen(histogram);            
       int level5 = AutoThresholder.maxEntropy(histogram);    
       int level4 = AutoThresholder.li(histogram);
     
       if (level4 > 0)
           thresholdOffset4 = Math.abs(meanFloat - level4);
       if (level5 > 0)
           thresholdOffset5 = Math.abs(meanFloat - level5);
       if (level6 > 0)
           thresholdOffset6 = Math.abs(meanFloat - level6);

       if (level4 == level5 && level5 == level6)
           thresholdOffset5 = 255;

       ImagePlus imageToUseForThresholdingImagePlus;
       imageToUseForThresholdingImagePlus = new ImagePlus("", imageToUseForThresholding.getBufferedImage());
  
       BackgroundSubtracter.getInstance().subtractBackround(imageToUseForThresholdingImagePlus.getProcessor(), 50);
       Image  backgroundSubtractResultImage = Image.createFloatImageFromBufferedImage(imageToUseForThresholdingImagePlus.getBufferedImage());
       Transform.copyImageIntoImage(backgroundSubtractResultImage, imageToUseForThresholding);
       backgroundSubtractResultImage.decrementReferenceCount();

       imageToUseForThresholdingImagePlus.close();
       imageToUseForThresholdingImagePlus.flush(); 
       
       if (thresholdOffset4 < thresholdOffset5)
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level4 = AutoThresholder.li(histogramAfterSubtractBackground);   
         
         if (level4 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level4 = AutoThresholder.li(histogramAfterSubtractBackground); 
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level4), 0.0f, (float)(level4), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else if (thresholdOffset5 < thresholdOffset6 )
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level5 = AutoThresholder.maxEntropy(histogramAfterSubtractBackground);   
         
         if (level5 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level5 = AutoThresholder.maxEntropy(histogramAfterSubtractBackground);   
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level5), 0.0f, (float)(level5), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
       else
       {
         int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
         level6 = AutoThresholder.li(histogramAfterSubtractBackground);  
         
         if (level6 < 0)
         {
           Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
           level6 = AutoThresholder.li(histogramAfterSubtractBackground);  
         }

         Threshold.threshold(imageToUseForThresholding, (float)(level6), 0.0f, (float)(level6), 255.0f); 
         Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  
       }
     }
     return imageToUseForThresholding;
   }
   
   /**
   * For Masking, the value for background subtraction is higher
   * @author Jack Hwee
   */
  private Image createFitPolynomialImageForMasking(Image imageToUseForThresholding, Image imageToUse)
  {
    int numHistogramBins = 256;   
   
    ImagePlus imageToUseForThresholdingImagePlus;
    imageToUseForThresholdingImagePlus = new ImagePlus("", imageToUse.getBufferedImage());

    imageToUseForThresholding.decrementReferenceCount();
    imageToUseForThresholding = new Image(imageToUse.getWidth(), imageToUse.getHeight());

    PolynomialFitting.getInstance().setFittingOrder(imageToUseForThresholdingImagePlus, 10, 10, 0, false, false);
    PolynomialFitting.getInstance().run(imageToUseForThresholdingImagePlus.getProcessor());

    Image  backgroundSubtractResultImage = Image.createFloatImageFromBufferedImage(imageToUseForThresholdingImagePlus.getBufferedImage());
    Transform.copyImageIntoImage(backgroundSubtractResultImage, imageToUseForThresholding);
    backgroundSubtractResultImage.decrementReferenceCount();

    imageToUseForThresholdingImagePlus.close();
    imageToUseForThresholdingImagePlus.flush(); 

    int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
    int level = AutoThresholder.minErrorI(histogramAfterSubtractBackground);

    if (level < 0)
    {
      Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
      level = AutoThresholder.minErrorI(histogramAfterSubtractBackground);
    }

    Threshold.threshold(imageToUseForThresholding, (float)(level), 0.0f, (float)(level), 255.0f); 
    Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  

    return imageToUseForThresholding;             
  }
  
   /**
   * ImageJ polynomial fitting algorithm
   * @author Jack Hwee
   */
  private Image createFitPolynomialImage(Image imageToUseForThresholding, Image imageToUse)
  {
    int numHistogramBins = 256;   
  
    ImagePlus imageToUseForThresholdingImagePlus;
    imageToUseForThresholdingImagePlus = new ImagePlus("", imageToUse.getBufferedImage());

    PolynomialFitting.getInstance().setFittingOrder(imageToUseForThresholdingImagePlus, 10, 10, 0, false, false);
    PolynomialFitting.getInstance().run(imageToUseForThresholdingImagePlus.getProcessor());

    Image  backgroundSubtractResultImage = Image.createFloatImageFromBufferedImage(imageToUseForThresholdingImagePlus.getBufferedImage());
    Transform.copyImageIntoImage(backgroundSubtractResultImage, imageToUseForThresholding);
    backgroundSubtractResultImage.decrementReferenceCount();

    imageToUseForThresholdingImagePlus.close();
    imageToUseForThresholdingImagePlus.flush(); 

    int[] histogramAfterSubtractBackground = Threshold.histogram(imageToUseForThresholding, RegionOfInterest.createRegionFromImage(imageToUseForThresholding), numHistogramBins);
    int level = AutoThresholder.minErrorI(histogramAfterSubtractBackground);

    if (level < 0)
    {
      Transform.copyImageIntoImage(imageToUse, imageToUseForThresholding);
      level = AutoThresholder.minErrorI(histogramAfterSubtractBackground);
    }

    Threshold.threshold(imageToUseForThresholding, (float)(level), 0.0f, (float)(level), 255.0f); 
    Threshold.threshold(imageToUseForThresholding, (float)(200), 0.0f, (float)(200), 255.0f);  

    return imageToUseForThresholding;             
  }
  
  /**
   * @author Bee Hoon
   */
  static public boolean isUntestedBorderSizeStandardMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD);

    if (voidingChoice.equals(_UNTESTED_BORDER_SIZE_STANDARD_METHOD))
    {
      return true;
    }

    return false;
  }
  
  /**
   * @author Bee Hoon
   */
  static public boolean isUntestedBorderSizeCustomizeMethod(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String voidingChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD);

    if (voidingChoice.equals(_UNTESTED_BORDER_SIZE_CUSTOMIZE_METHOD))
    {
      return true;
    }

    return false;
  }
  
  /**
   * @author Siew Yeng
   * @return jointRoi
   */
  private RegionOfInterest getRegionOfInterest(JointInspectionData joint, Subtype subtypeOfJoints, final float MILIMETER_PER_PIXEL)
  {
    float untestedBorderSizeInMM = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE);
    int untestedBorderSizeInPixels = (int)Math.ceil(untestedBorderSizeInMM / MILIMETER_PER_PIXEL);
    float leftOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET);
    int leftOffsetInPixels = (int)Math.ceil(leftOffset / MILIMETER_PER_PIXEL);
    float bottomOffset= (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET);
    int bottomOffsetInPixels = (int)Math.ceil(bottomOffset / MILIMETER_PER_PIXEL);
    float rightOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET);
    int rightOffsetInPixels = (int)Math.ceil(rightOffset / MILIMETER_PER_PIXEL);
    float topOffset = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET);
    int topOffsetInPixels = (int)Math.ceil(topOffset / MILIMETER_PER_PIXEL);
    
    RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(joint);

      // Untested Border Size Method - Standard or Customize
    if (isUntestedBorderSizeStandardMethod(subtypeOfJoints))
    {
      if ((jointRoi.getWidth() - 2*untestedBorderSizeInPixels < 5.0) || (jointRoi.getHeight() - 2*untestedBorderSizeInPixels < 5.0))
      {
        // @todo PWL : post warning about the untested region being too large
        LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                          new Object[] {joint.getFullyQualifiedPadName()});
        AlgorithmUtil.raiseAlgorithmWarning(warningText);
      }

      jointRoi.setWidthKeepingSameCenter(jointRoi.getWidth() - 2*untestedBorderSizeInPixels);
      jointRoi.setHeightKeepingSameCenter(jointRoi.getHeight() - 2*untestedBorderSizeInPixels);
    }
    else if (isUntestedBorderSizeCustomizeMethod(subtypeOfJoints))
    {
      if ((jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels < 5.0) || 
          (jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels < 5.0))
      {
        LocalizedString warningText = new LocalizedString("ALGDIAG_UNTESTED_BORDER_SIZE_JOINT_ROI_TOO_SMALL_WARNING_KEY",
                                                          new Object[] {joint.getFullyQualifiedPadName()});
        AlgorithmUtil.raiseAlgorithmWarning(warningText);
      }

      jointRoi.setMinXY(jointRoi.getMinX() + leftOffsetInPixels, jointRoi.getMinY() + topOffsetInPixels);
      jointRoi.setWidthKeepingSameMinX(jointRoi.getWidth() - leftOffsetInPixels - rightOffsetInPixels);
      jointRoi.setHeightKeepingSameMinY(jointRoi.getHeight() - bottomOffsetInPixels - topOffsetInPixels);
    }
    
    return jointRoi;
  }
  
  /**
   * This is factor out from classifyFloodFillSliceMethod function.
   * @author Siew Yeng
   * @return MaskImage,RoiOfMask
   */
  private Pair<Image,RegionOfInterest> createMaskImage(Image sliceImageForMasking, 
                                                      ReconstructionRegion reconstructionRegion, 
                                                      RegionOfInterest jointRoi,
                                                      JointInspectionData joint, 
                                                      Subtype subtypeOfJoints)
  {
    MaskImage createMaskImage =  MaskImage.getInstance();
    Image maskImage = null;
//    RegionOfInterest adjustedJointRoiForMasking = null;
    RegionOfInterest jointRoiForUsingMask = null;
    
    if (createMaskImage.getMisalignedSubtypeMaskImage(joint, subtypeOfJoints.getLongName()) != null)
    {
      java.awt.image.BufferedImage misalignedMaskImage = createMaskImage.getMisalignedSubtypeMaskImage(joint, subtypeOfJoints.getLongName());
      maskImage = Image.createFloatImageFromBufferedImage(misalignedMaskImage);
      
      //Siew Yeng - XCR-3708 - production crash due to single pad masking
      RegionOfInterest cropRegion;
          
      // get the imageCoordinate of horizontal and vertical edges of mask image          
      if(createMaskImage.getSubtypeMaskImageXCoordinate(joint, subtypeOfJoints.getLongName()) == null || 
        createMaskImage.getSubtypeMaskImageYCoordinate(joint, subtypeOfJoints.getLongName()) == null)
      {
        cropRegion = Locator.getCropRegionFromMaskImage(maskImage);
      }
      else
      {
        // get the imageCoordinate of horizontal and vertical edges of mask image
        ImageCoordinate x = createMaskImage.getSubtypeMaskImageXCoordinate(joint, subtypeOfJoints.getLongName());
        ImageCoordinate y = createMaskImage.getSubtypeMaskImageYCoordinate(joint, subtypeOfJoints.getLongName());

        cropRegion = RegionOfInterest.createLineRegionFromRegionBorder(x.getY(), y.getY(), x.getX(), y.getX(), 0, RegionShapeEnum.RECTANGULAR);
      }

      int width = cropRegion.getWidth();
      int height = cropRegion.getHeight();

      // if the width and height is below certain values meaning the mask image is not correct or the edge detection is not performing well,
      // hence cropRegion and adjustedJointRoiForMasking has to be determined by mask image itself. 
      if (width < 5 || height< 5)
      {
        java.awt.image.BufferedImage maskBufferedImage = createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getLongName(), joint.getComponent().getDegreesRotationAfterAllRotations());
        maskImage.decrementReferenceCount();
        maskImage = Image.createFloatImageFromBufferedImage(maskBufferedImage);
        cropRegion = RegionOfInterest.createRegionFromImage(maskImage);
//        adjustedJointRoiForMasking = new RegionOfInterest(cropRegion);
        width = maskImage.getWidth();
        height = maskImage.getHeight();
        maskBufferedImage.flush();
      }
          
      jointRoiForUsingMask = cropRegion;

      //Siew Yeng - XCR-3319 - do this checking only for reconstruction region with one pad only
      if (reconstructionRegion.getPads().size() == 1 && cropRegion.fitsWithinImage(sliceImageForMasking) == false)
      {
        width = jointRoi.getWidth();
        height = jointRoi.getHeight();
         
        cropRegion.setRect(jointRoi.getMinX(), jointRoi.getMinY(), width, height);        

        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_MASK_IMAGE_NOT_INSPECTING_WARNING_KEY",
                                                              new String[]
                                                              {joint.getComponent().getReferenceDesignator()}));           
      }
      misalignedMaskImage.flush();
    }
    else if (createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getLongName(), joint.getComponent().getDegreesRotationAfterAllRotations()) != null)
    {                       
      java.awt.image.BufferedImage maskBufferedImage = createMaskImage.getSubtypeMaskImage(subtypeOfJoints.getLongName(), joint.getComponent().getDegreesRotationAfterAllRotations());
      maskImage = Image.createFloatImageFromBufferedImage(maskBufferedImage);
        
      //Siew Yeng - XCR-3708
      RegionOfInterest cropRegion = Locator.getCropRegionFromMaskImage(maskImage);

      int width = cropRegion.getWidth();
      int height = cropRegion.getHeight();

      // if the width and height is below certain values meaning the mask image is not correct or the edge detection is not performing well,
      // hence cropRegion and adjustedJointRoiForMasking has to be determined by mask image itself. 
      if (width < 5 || height< 5)
      {
        cropRegion = RegionOfInterest.createRegionFromImage(maskImage);
//          adjustedJointRoiForMasking = new RegionOfInterest(cropRegion);
        width = maskImage.getWidth();
        height = maskImage.getHeight();
        maskBufferedImage.flush();
      }
      
      jointRoiForUsingMask = cropRegion;

      //Siew Yeng - XCR-3319 - check this only for reconstruction region with ONE pad only
      if (reconstructionRegion.getPads().size() == 1 && cropRegion.fitsWithinImage(sliceImageForMasking) == false)
      {
        width = jointRoi.getWidth();
        height = jointRoi.getHeight();

        cropRegion.setRect(jointRoi.getMinX(), jointRoi.getMinY(), width, height);        

        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_MASK_IMAGE_NOT_INSPECTING_WARNING_KEY",
                                                              new String[]
                                                              {joint.getComponent().getReferenceDesignator()}));           
      }
      
      maskBufferedImage.flush();              
    }
    
    return new Pair(maskImage, jointRoiForUsingMask);
  }
  
  
  
  /**
   * @author Siew Yeng
   */
  private Image classifySubSubtypeForEachJoint(Image sliceImage,
                                              ReconstructionRegion reconstructionRegion,
                                              JointInspectionData joint,
                                              RegionOfInterest jointRoi,
                                              Subtype parentSubtype,
                                              Subtype subSubtype,
                                              SliceNameEnum sliceNameEnum,
                                              int finalImageWidth,
                                              int finalImageHeight,
                                              RegionOfInterest finalImageRoi,
                                              final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Pair<Image,RegionOfInterest> subRegionImageAndRoi = null;
    IntegerRef numberOfPixelTested = new IntegerRef();
    if(isThicknessMethod(subSubtype))
    {
      SliceNameEnum padSliceNameEnum = subSubtype.getInspectionFamily().getDefaultPadSliceNameEnum(subSubtype);
      int noiseReduction = (Integer)subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NOISE_REDUCTION);
      boolean applyBlurToJointImages = noiseReduction >= 1;
      int numberOfTimesToOpenVoidPixelImage = Math.max(0, noiseReduction - 1);
      
      
      subRegionImageAndRoi = classifySliceForEachJoint(sliceImage,
                                            reconstructionRegion,
                                            padSliceNameEnum,
                                            joint,
                                            subSubtype,
                                            jointRoi,
                                            applyBlurToJointImages,
                                            isUsingMaskingMethod(parentSubtype) || isUsingMaskingMethod(subSubtype),
                                            (Float)subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD),
                                            (Float)subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD),
                                            numberOfTimesToOpenVoidPixelImage,
                                            numberOfPixelTested,
                                            MILIMETER_PER_PIXEL);
    }
    else if(isFloodFillMethod(subSubtype))
    {
      subRegionImageAndRoi = classifyFloodFillSliceForEachJoint(sliceImage,
                                                reconstructionRegion,
                                                joint,
                                                jointRoi,
                                                subSubtype,
                                                (Integer)subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE),
                                                (Integer)subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER),
                                                isUsingMaskingMethod(parentSubtype) || isUsingMaskingMethod(subSubtype),
                                                isUsingMultiThresholdDetection(subSubtype),
                                                isUsingFitPolynomialDetection(subSubtype),
                                                numberOfPixelTested);
    }
    
    Image subRegionImage = subRegionImageAndRoi.getFirst();
    RegionOfInterest subRegionRoi = subRegionImageAndRoi.getSecond();

    Image tempImage =  new Image(sliceImage.getWidth(), sliceImage.getHeight());
    Paint.fillImage(tempImage, 0.f);
    Image finalSubRegionImage = BufferedImageUtil.overlayImages(tempImage, subRegionImage, subRegionRoi);
    tempImage.decrementReferenceCount();
    subRegionImage.decrementReferenceCount();

    Image newImage = new Image(finalImageWidth, finalImageHeight);
    Transform.copyImageIntoImage(finalSubRegionImage, finalImageRoi, newImage, RegionOfInterest.createRegionFromImage(newImage));
    finalSubRegionImage.decrementReferenceCount();
    finalSubRegionImage = new Image(finalImageWidth, finalImageHeight);
    Transform.copyImageIntoImage(newImage, finalSubRegionImage);
    newImage.decrementReferenceCount();
    
    if (ImageAnalysis.areDiagnosticsEnabled(joint.getSubtype().getJointTypeEnum(), this))
    {
      _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 joint,
                                 this,
                                 false,
                                 new OverlayImageDiagnosticInfo(finalSubRegionImage, finalImageRoi, subSubtype.getShortName()));
    }
    return finalSubRegionImage;
  }
  
  /**
   * @author Siew Yeng
   */
  public List<AlgorithmSettingEnum> getAlgorithmSettingEnumsIgnoreListForSubSubtype()
  {
    return _subSubtypeAlgorithmSettingEnumsIgnoreList;
  }
}

