package com.axi.v810.business.imageAnalysis.largePad;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.util.Assert;
import com.axi.util.MathUtil;
import com.axi.v810.business.testResults.MeasurementUnitsEnum;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * Just locate the joints.
 *
 * @author Patrick Lacz
 */
public class LargePadMeasurementAlgorithm extends Algorithm
{
  protected final int _MAX_NUMBER_OF_REGIONS_FOR_LEARNING = 100; // Siew Yeng - XCR-2139
  
  private final static String _MIN_NUMBER_OF_USER_DEFINED_SLICE = "0";
  private final static String _MAX_NUMBER_OF_USER_DEFINED_SLICE = "2";
  
  private final static ArrayList<String> _LARGE_PAD_NUMBER_OF_SLICE = new ArrayList<String>(Arrays.asList(_MIN_NUMBER_OF_USER_DEFINED_SLICE, "1", _MAX_NUMBER_OF_USER_DEFINED_SLICE));
  //private static final String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";

  /**
   * Keeps track of profile data that we measure during learning.
   * @author Siew Yeng
   */
  protected static class LargePadProfileDataForLearning
  {
    private float[] _padMinusBackgroundProfile;
    private float[] _padMinusBackgroundProfile2;
    private float[] _padMinusBackgroundProfileAcross;

    /**
     * @author Siew Yeng
     */
    public LargePadProfileDataForLearning(float[] padMinusBackgroundProfile,
                                          float[] padMinusBackgroundProfile2,
                                          float[] padMinusBackgroundProfileAcross)
    {
      Assert.expect(padMinusBackgroundProfile != null);
      Assert.expect(padMinusBackgroundProfile2 != null);
      Assert.expect(padMinusBackgroundProfileAcross != null);

      _padMinusBackgroundProfile = padMinusBackgroundProfile;
      _padMinusBackgroundProfile2 = padMinusBackgroundProfile2;
      _padMinusBackgroundProfileAcross = padMinusBackgroundProfileAcross;
    }

    /**
     * @author Siew Yeng
     */
    public float[] getPadMinusBackgroundProfile()
    {
      Assert.expect(_padMinusBackgroundProfile != null);

      return _padMinusBackgroundProfile;
    }
    
    /**
     * @author Siew Yeng
     */
    public float[] getPadMinusBackgroundProfile2()
    {
      Assert.expect(_padMinusBackgroundProfile2 != null);

      return _padMinusBackgroundProfile2;
    }
    
    /**
     * @author Siew Yeng
     */
    public float[] getPadMinusBackgroundProfileAcross()
    {
      Assert.expect(_padMinusBackgroundProfileAcross != null);

      return _padMinusBackgroundProfileAcross;
    }
  }
  
  /**
   * @author Patrick Lacz
   */
  LargePadMeasurementAlgorithm()
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.LARGE_PAD);


    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -150.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);
    
    // Added by Lee Herng (4 Mar 2011)
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -200.0f), // minimum value
        MathUtil.convertMilsToMillimeters(200.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed desc
        "IMG_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_LARGEPAD_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is displayed right before psh in the Slice Setup Tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
        
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_LARGEPAD_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));

    AlgorithmSetting openInsufficientPadWidth = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH,
      displayOrder++,
      100.f, // default
      0.f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_PAD_WIDTH)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_PAD_WIDTH)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_PAD_WIDTH)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(openInsufficientPadWidth);

    AlgorithmSetting openInsufficientPadHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT,
      displayOrder++,
      100.f, // default
      0.f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_PAD_HEIGHT)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_PAD_HEIGHT)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_PAD_HEIGHT)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(openInsufficientPadHeight);

    AlgorithmSetting openInsufficientIPDPercent = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT,
      displayOrder++,
      100.f, // default
      0.f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_IPD_PERCENT)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_IPD_PERCENT)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_MEASUREMENT_(OPEN_INSUFFICIENT_IPD_PERCENT)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(openInsufficientIPDPercent);
    
    // Kok Chun, Tan - XCR-2852
    AlgorithmSetting backgroundRegionLocation = new AlgorithmSetting(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT,
        displayOrder++,
        0f, // default
        0f, // min
        200f, // max
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_LARGEPAD_MEASUREMENT_(MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // desc
        "HTML_DETAILED_DESC_LARGEPAD_MEASUREMENT_(MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // detailed desc
        "IMG_DESC_LARGEPAD_MEASUREMENT_(MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(backgroundRegionLocation);

    /* ************************************
     * SPECIAL THRESHOLDS - START SECTION *
     * ************************************/
    //
    // The following 3 thresholds are used mainly for insufficient algorithm ONLY.
    // You will notice that their enum names are specified as INSUFFICIENT, but were put here because they will be used here.

    // SmoothingFactor for along profile and for open and insufficient algorithm ONLY.
    // Same as "Span size" in 5DX FET algorithm,
    // 1) this threshold only used for along profile only, after getting the locatedPadProfile[].
    // 2) across profile don't do any smoothing at all.
    AlgorithmSetting smoothingFactor = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR,
      displayOrder++,
      6, // default
      1, // min
      40, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_LARGEPAD_INSUFFICIENT_(SMOOTHING_FACTOR)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(SMOOTHING_FACTOR)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_INSUFFICIENT_(SMOOTHING_FACTOR)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(smoothingFactor);

    // This determine the percentage of area to used for histogram profiling.
    // Same as "Pad Profile Width" in 5DX FET algorithm
    AlgorithmSetting histogramWidthAlongFactor = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_HISTOGRAM_AREA_REDUCTION,
      displayOrder++,
      100.f, // default
      0.f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_LARGEPAD_INSUFFICIENT_(HISTOGRAM_AREA_REDUCTION)_KEY", // desc
      "HTML_DETAILED_DESC_LARGEPAD_INSUFFICIENT_(HISTOGRAM_AREA_REDUCTION)_KEY", // detailed desc
      "IMG_DESC_LARGEPAD_INSUFFICIENT_(HISTOGRAM_AREA_REDUCTION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(histogramWidthAlongFactor);

    //Siew Yeng - XCR-3094
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList("False", "True"));
    AlgorithmSetting enableSaveAllJointImageWhenFailComponent = new AlgorithmSetting(
        AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS,
        displayOrder++,
        "False",
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // description URL key
        "HTML_DETAILED_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // desailed description URL key
        "IMG_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(enableSaveAllJointImageWhenFailComponent);
    
    //
    /* **********************************
     * SPECIAL THRESHOLDS - END SECTION *
     * **********************************/
    // Added by Jack Hwee 
    AlgorithmSetting userDefineNumOfSlice = new AlgorithmSetting(
        AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE, // setting enum
        displayOrder++, // display order,
        _MIN_NUMBER_OF_USER_DEFINED_SLICE,
        _LARGE_PAD_NUMBER_OF_SLICE,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_LARGEPAD_NUMBER_OF_USER_DEFINED_SLICE_KEY", // description URL Key
        "HTML_DETAILED_DESC_LARGEPAD_(NUMBER_OF_USER_DEFINED_SLICE)_KEY", // detailed description URL Key
        "IMG_DESC_LARGEPAD_(NUMBER_OF_USER_DEFINED_SLICE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(userDefineNumOfSlice);
    
    AlgorithmSetting userDefinedSlice1 = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_1, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_LARGEPAD_USER_DEFINED_(USER_DEFINED_SLICE_ONE)_KEY", // description URL Key
      "HTML_DETAILED_DESC_LARGEPAD_USER_DEFINED_(USER_DEFINED_SLICE_1)_KEY", // detailed description URL Key
      "IMG_DESC_LARGEPAD_USER_DEFINED_(USER_DEFINED_SLICE_1)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice1);
    
    AlgorithmSetting userDefinedSlice2 = new AlgorithmSetting(
      AlgorithmSettingEnum.LARGE_PAD_USER_DEFINED_SLICE_2, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_LARGEPAD_USER_DEFINED_(USER_DEFINED_SLICE_TWO)_KEY", // description URL Key
      "HTML_DETAILED_DESC_LARGEPAD_USER_DEFINED_(USER_DEFINED_SLICE_2)_KEY", // detailed description URL Key
      "IMG_DESC_LARGEPAD_USER_DEFINED_(USER_DEFINED_SLICE_2)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefinedSlice2);

    addMeasurementEnums();

    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();

    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author Peter Esbensen
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG);
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG);
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG);
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS);
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS);
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS);

    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());
  }

  /**
   * Classify all the given joints.
   * @author Patrick Lacz
   */
  public void classifyJoints(ReconstructedImages reconstructedImages, List<JointInspectionData> jointInspectionDataObjects)
      throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();

    SliceNameEnum padSliceNameEnum = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);

    Locator.locateJoints(reconstructedImages, padSliceNameEnum, jointInspectionDataObjects, this, true);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSliceNameEnum);

    // Get the image for processing
    Image imageToInspect = reconstructedPadSlice.getOrthogonalImage();

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // IMPORTANT NOTES : Also do pad thickness & sum of slope changes inside measureJointAlong. Both for detecting insufficients.
      measureJointAlong (reconstructedImages, imageToInspect, subtypeOfJoints, padSliceNameEnum, jointInspectionData);
      measureJointAcross(reconstructedImages, imageToInspect, subtypeOfJoints, padSliceNameEnum, jointInspectionData);
    }
    
    //Siew Yeng - XCR-2683 - add enhanced image
    if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtypeOfJoints))
    {
      ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, padSliceNameEnum);
    }
    
    //Added by Jack Hwee - to add measurement into UserDefinedSlice1 and UserdefinedSlice2
    LargePadMeasurementAlgorithm measurementAlgo = (LargePadMeasurementAlgorithm)InspectionFamily.getAlgorithm(InspectionFamilyEnum.LARGE_PAD, AlgorithmEnum.MEASUREMENT);
    Collection<SliceNameEnum> inspectedSlices = measurementAlgo.getInspectionSliceOrder(subtypeOfJoints);
    
    if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1))
    {
      Locator.locateJoints(reconstructedImages, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1, jointInspectionDataObjects, this, true);
      // Get the applicable ReconstructedSlice for the pad slice.
      reconstructedPadSlice = reconstructedImages.getReconstructedSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);

      // Get the image for processing
      imageToInspect = reconstructedPadSlice.getOrthogonalImage();

      // Iterate thru each joint.
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // IMPORTANT NOTES : Also do pad thickness & sum of slope changes inside measureJointAlong. Both for detecting insufficients.
        measureJointAlong (reconstructedImages, imageToInspect, subtypeOfJoints, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1, jointInspectionData);
        measureJointAcross(reconstructedImages, imageToInspect, subtypeOfJoints, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1, jointInspectionData);
      }
      
      //Siew Yeng - XCR-2683 - add enhanced image
      if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtypeOfJoints))
      {
        ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_1);
      }
    }
      
    if (inspectedSlices.contains(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2))
    {
      Locator.locateJoints(reconstructedImages, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2, jointInspectionDataObjects, this, true);
      // Get the applicable ReconstructedSlice for the pad slice.
      reconstructedPadSlice = reconstructedImages.getReconstructedSlice(SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);

      // Get the image for processing
      imageToInspect = reconstructedPadSlice.getOrthogonalImage();

      // Iterate thru each joint.
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // IMPORTANT NOTES : Also do pad thickness & sum of slope changes inside measureJointAlong. Both for detecting insufficients.
        measureJointAlong (reconstructedImages, imageToInspect, subtypeOfJoints, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2, jointInspectionData);
        measureJointAcross(reconstructedImages, imageToInspect, subtypeOfJoints, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2, jointInspectionData);
      }
      
      //Siew Yeng - XCR-2683 - add enhanced image
      if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtypeOfJoints))
      {
        ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, SliceNameEnum.LARGE_PAD_USER_DEFINED_SLICE_2);
      }
    }
    inspectedSlices.clear();
  }

  /**
   * Calculate percentile of cumulative histogram
   *
   * @author Lim, Seng Yew
   */
  private float calculateHistogramPercentile(ReconstructedImages reconstructedImages,
                                             Image image,
                                             JointInspectionData jointInspectionData,
                                             float percentile)
  {
    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    Subtype subtypeOfJoints = jointInspectionData.getSubtype();

    SliceNameEnum padSliceNameEnum = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Initialize all region of interest
    RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Resize when necessary
    final float HISTOGRAM_AREA_WIDTH_ALONG_FRACTION =
       (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_HISTOGRAM_AREA_REDUCTION) / 100.f;
    int desiredWidth = (int)Math.round((jointRoi.getLengthAlong()*HISTOGRAM_AREA_WIDTH_ALONG_FRACTION) + 0.5);
    jointRoi.setLengthAlong(desiredWidth);

    // Create histogram profile.
    int totalPixelCount = jointRoi.getHeight() * jointRoi.getWidth();
    int desiredPixelCount = Math.round(totalPixelCount * percentile);
    int[] histogramProfile = ImageFeatureExtraction.histogramProfile(image, jointRoi, 256); // 0-255

    int cumulativeCount = 0;
    float graylevelPercentile = 0.0f;
    int i;
    for (i=0; i < 256; ++i)
    {
      cumulativeCount += histogramProfile[i];
      if (cumulativeCount > desiredPixelCount)
      {
        if (i >= 0 && i <= 255)
          cumulativeCount -= histogramProfile[i];
        break;
      }
    }
    if (i < 256)
    {
      graylevelPercentile = (float)(i - 1) + (desiredPixelCount - cumulativeCount) / (float)histogramProfile[i];
    }
    else
    {
      // Show warning if cannot find the center of cumulative histogram.
    }

    // Display diagnostic when necessary
    if (ImageAnalysis.areDiagnosticsEnabled(subtypeOfJoints.getJointTypeEnum(), this))
    {
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo =
          new MeasurementRegionDiagnosticInfo(jointRoi,
                                              MeasurementRegionEnum.GRAYLEVEL_HISTOGRAM_PROFILE_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(inspectionRegion,
                                                         padSliceNameEnum,
                                                         subtypeOfJoints,
                                                         this, false, false,
                                                         componentRegionDiagnosticInfo);
      float[] histogramProfileFloat = new float[256];
      for (i=0; i < 256; ++i)
      {
        histogramProfileFloat[i] = histogramProfile[i];
      }
      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[histogramProfileFloat.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.GRAYLEVEL_HISTOGRAM_PROFILE_REGION);
      LocalizedString histogramProfileLabelLocalizedString =
        new LocalizedString("GRAYLEVEL_HISTOGRAM_PROFILE_REGION_KEY", null);
      ProfileDiagnosticInfo histogramProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           histogramProfileLabelLocalizedString,
                                                                           0.0f,
                                                                           ArrayUtil.max(histogramProfileFloat) * 1.1f,
                                                                           histogramProfileFloat,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.PIXELS);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   padSliceNameEnum,
                                   subtypeOfJoints,
                                   this, false, true,
                                   histogramProfileDiagInfo);
    }
    
    return graylevelPercentile;
  }

  /**
   * Get measurements along located pad
   *
   * @author Lim, Seng Yew
   */
  private void measureJointAlong(ReconstructedImages reconstructedImages,
                                Image image,
                                Subtype subtype,
                                SliceNameEnum sliceNameEnum,
                                JointInspectionData jointInspectionData) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    Subtype subtypeOfJoints = jointInspectionData.getSubtype();

    SliceNameEnum padSliceNameEnum = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the IPD.
    int interPadDistance = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();
    
    float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f * (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT);

    final int SMOOTH_FACTOR = 6;
    final int DERIVATIVE_FACTOR = 4;

    // Initialize all region of interest
    RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
    RegionOfInterest backgroundROIOne = new RegionOfInterest(jointRoi);
    RegionOfInterest backgroundROITwo = new RegionOfInterest(jointRoi);

    // Resize area to extract located pad profile.
    final float PAD_WIDTH_ALONG =
       (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH) / 100.f;
    final float PAD_HEIGHT_ALONG =
       (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT) / 100.f;
    final float IPD_PERCENT =
       (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT) / 100.f;
    int wantedPadHeight = Math.round(jointRoi.getLengthAcross() * PAD_HEIGHT_ALONG);
    int halfInterPadDistance = Math.round(interPadDistance * IPD_PERCENT) / 2;
    int wantedWidth = Math.round(jointRoi.getLengthAlong() * PAD_WIDTH_ALONG) + Math.round(interPadDistance * IPD_PERCENT); // Original FET algo in 5DX don't consider IPD. Requested by Jeremy P.

    // Resize area to extract both background profile.
    int wantedHeight = 3;
    // Wei Chin Fixed for region cannot fit the image assert. 17 June (Plexus)
    wantedWidth = Math.min(wantedWidth, image.getWidth() - 1);

    backgroundROIOne.setLengthAlong(wantedWidth);
    backgroundROIOne.setLengthAcross(wantedHeight);
    backgroundROITwo.setLengthAlong(wantedWidth);
    backgroundROITwo.setLengthAcross(wantedHeight);
    // Move/Translate backgroundROIOne to correct location
    backgroundROIOne.translateAlongAcross(0, jointRoi.getLengthAcross()/2 + 3 + ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)));
    backgroundROITwo.translateAlongAcross(0, -jointRoi.getLengthAcross()/2 - 3 - ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)));
    // Make sure both backgroundROI are within image boundary
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image,backgroundROIOne);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image,backgroundROITwo);
    // Adjust jointROI only after both background ROI are adjusted correctly, becos original jointRoi info needed.
    wantedHeight = Math.max(3,wantedPadHeight);
    jointRoi.setLengthAlong(wantedWidth);
    jointRoi.setLengthAcross(wantedHeight);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image,jointRoi);

    // Measure the profiles.
    Assert.expect(backgroundROIOne.getLengthAlong() == backgroundROITwo.getLengthAlong(),
            "Error in Large Pad Open algorithm - length along are not the same between upper (" + backgroundROIOne.getLengthAlong() + ") and lower background {" + backgroundROITwo.getLengthAlong() + ").");
    Assert.expect(jointRoi.getLengthAlong() == backgroundROIOne.getLengthAlong(),
            "Error in Large Pad Open algorithm - length along are not the same between located pad (" + jointRoi.getLengthAlong() + ") and background (" + backgroundROIOne.getLengthAlong() + ").");
    float[] locatedPadProfile  = ImageFeatureExtraction.profile(image, jointRoi);
    float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundROIOne);
    float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundROITwo);

    locatedPadProfile  = ProfileUtil.getSmoothedProfile(locatedPadProfile,SMOOTH_FACTOR);
    backgroundProfile1 = ProfileUtil.getSmoothedProfile(backgroundProfile1,SMOOTH_FACTOR);
    backgroundProfile2 = ProfileUtil.getSmoothedProfile(backgroundProfile2,SMOOTH_FACTOR);

    // Display diagnostic when necessary
    if (ImageAnalysis.areDiagnosticsEnabled(subtypeOfJoints.getJointTypeEnum(), this))
    {
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo =
          new MeasurementRegionDiagnosticInfo(jointRoi,
                                              MeasurementRegionEnum.PAD_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(inspectionRegion,
                                                         padSliceNameEnum,
                                                         subtypeOfJoints,
                                                         this, true, false,
                                                         componentRegionDiagnosticInfo);
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo1 =
          new MeasurementRegionDiagnosticInfo(backgroundROIOne,
                                              MeasurementRegionEnum.BACKGROUND_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(inspectionRegion,
                                                         padSliceNameEnum,
                                                         subtypeOfJoints,
                                                         this, false, false,
                                                         componentRegionDiagnosticInfo1);
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo2 =
          new MeasurementRegionDiagnosticInfo(backgroundROITwo,
                                              MeasurementRegionEnum.BACKGROUND_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(inspectionRegion,
                                                         padSliceNameEnum,
                                                         subtypeOfJoints,
                                                         this, false, false,
                                                         componentRegionDiagnosticInfo2);
    }
    // Calculate average background profile
    float backgroundGraylevelAverage = 0.0f;
    float[] padMinusBackgroundProfile = new float[backgroundProfile1.length];
    for (int i = 0; i < backgroundProfile1.length; ++i)
    {
      padMinusBackgroundProfile[i] = Math.max(0.0f, (backgroundProfile1[i] + backgroundProfile2[i]) / 2.0f - locatedPadProfile[i]);
      backgroundGraylevelAverage += (backgroundProfile1[i] + backgroundProfile2[i]) / 2.0f;
      locatedPadProfile[i] = padMinusBackgroundProfile[i]; // Save the original values before derivatives.
    }
    // 5DX FET do smooth after getting the locatedPadProfile[], so I just follow so.
    // Use insufficientSmoothingFactor threshold instead of fixed value 6.
    int insufficientSmoothingFactor =
      ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR)).intValue();
    padMinusBackgroundProfile = ProfileUtil.getSmoothedProfile(padMinusBackgroundProfile,insufficientSmoothingFactor);
    padMinusBackgroundProfile = ProfileUtil.createDerivativeProfile(padMinusBackgroundProfile, DERIVATIVE_FACTOR);

    // Display diagnostic when necessary
    if (ImageAnalysis.areDiagnosticsEnabled(subtypeOfJoints.getJointTypeEnum(), this))
    {
      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[locatedPadProfile.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.PAD_REGION);
      LocalizedString padRegionLabelLocalizedString =
        new LocalizedString("PAD_REGION_KEY", null);
      ProfileDiagnosticInfo locatedPadProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padRegionLabelLocalizedString,
                                                                           0.0f,
                                                                           Math.max(ArrayUtil.max(locatedPadProfile) * 1.1f,ArrayUtil.max(locatedPadProfile) * -1.1f),
                                                                           locatedPadProfile,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.NONE);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   padSliceNameEnum,
                                   subtypeOfJoints,
                                   this, false, true,
                                   locatedPadProfileDiagInfo);
    }
    if (ImageAnalysis.areDiagnosticsEnabled(subtypeOfJoints.getJointTypeEnum(), this))
    {
      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[padMinusBackgroundProfile.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.FIRST_DERIVATIVES_PROFILE_REGION);
      LocalizedString padRegionLabelLocalizedString =
        new LocalizedString("FIRST_DERIVATIVES_PROFILE_REGION_KEY", null);
      ProfileDiagnosticInfo padMinusBackgroundProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padRegionLabelLocalizedString,
                                                                           Math.min(ArrayUtil.min(padMinusBackgroundProfile) * 1.1f,ArrayUtil.min(padMinusBackgroundProfile) * -1.1f),
                                                                           Math.max(ArrayUtil.max(padMinusBackgroundProfile) * 1.1f,ArrayUtil.max(padMinusBackgroundProfile) * -1.1f),
                                                                           padMinusBackgroundProfile,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.NONE);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   padSliceNameEnum,
                                   subtypeOfJoints,
                                   this, false, true,
                                   padMinusBackgroundProfileDiagInfo);
    }

    // FOR INSUFFICIENT - START #1
    // Calculate 50 percentile of cumulative histogram
    backgroundGraylevelAverage /= backgroundProfile1.length;
    float padThicknessInMils = backgroundGraylevelAverage - calculateHistogramPercentile(reconstructedImages, image, jointInspectionData, 0.50f);
    // Have to convert from graylevel to mils
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    padThicknessInMils = Math.min(padThicknessInMils, 255);
    padThicknessInMils = Math.max(padThicknessInMils, 0);
    backgroundGraylevelAverage = Math.min(backgroundGraylevelAverage, 255);
    backgroundGraylevelAverage = Math.max(backgroundGraylevelAverage, 0);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    padThicknessInMils = thicknessTable.getThicknessInMils(backgroundGraylevelAverage, padThicknessInMils, backgroundSensitivityOffset);
    float padThicknessInMillimeters = MathUtil.convertMilsToMillimeters(padThicknessInMils);
    
    //XCR2380 - Khaw Chek Hau: Single Pad Insufficient Minimum Pad Thickness Algorithm Bug
    // Create measurement object
    JointMeasurement padThicknessMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_THICKNESS,
                             MeasurementUnitsEnum.MILLIMETERS,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             padThicknessInMillimeters);
    jointInspectionResult.addMeasurement(padThicknessMeasurement);
    // FOR INSUFFICIENT - END #1

    float leadingSlopeAlong = ArrayUtil.max(padMinusBackgroundProfile);
    float trailingSlopeAlong = ArrayUtil.min(padMinusBackgroundProfile) * -1.0f;
    float slopeSumAlong = leadingSlopeAlong + trailingSlopeAlong;

    // FOR INSUFFICIENT - START #2
    // Calculate sum of slope changes
    float sumOfSlopeChanges = 0.0f;
    padMinusBackgroundProfile = ProfileUtil.createDerivativeProfile(padMinusBackgroundProfile, DERIVATIVE_FACTOR);
    for (int i = 0; i < padMinusBackgroundProfile.length; ++i)
    {
      sumOfSlopeChanges += Math.abs(padMinusBackgroundProfile[i]);
    }
    // Create measurement object
    JointMeasurement sumOfSlopeChangesMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             sumOfSlopeChanges);
    jointInspectionResult.addMeasurement(sumOfSlopeChangesMeasurement);
    // FOR INSUFFICIENT - END #2

    // Create measurement object
    JointMeasurement leadingSlopeMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             leadingSlopeAlong);
    jointInspectionResult.addMeasurement(leadingSlopeMeasurement);

    // Create measurement object
    JointMeasurement trailingSlopeMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             trailingSlopeAlong);
    jointInspectionResult.addMeasurement(trailingSlopeMeasurement);

    // Create measurement object
    JointMeasurement slopeSumMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             slopeSumAlong);
    jointInspectionResult.addMeasurement(slopeSumMeasurement);

    // Post a diagnostic showing the labeled along profile and the measured values.
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
    {
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      inspectionRegion,
                                                      jointInspectionData,
                                                      leadingSlopeMeasurement,
                                                      trailingSlopeMeasurement,
                                                      slopeSumMeasurement,
                                                      padThicknessMeasurement,
                                                      sumOfSlopeChangesMeasurement);
      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[padMinusBackgroundProfile.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.SECOND_DERIVATIVES_PROFILE_REGION);
      LocalizedString padRegionLabelLocalizedString =
        new LocalizedString("SECOND_DERIVATIVES_PROFILE_REGION_KEY", null);
      ProfileDiagnosticInfo padMinusBackgroundProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padRegionLabelLocalizedString,
                                                                           Math.min(ArrayUtil.min(padMinusBackgroundProfile) * 1.1f,ArrayUtil.min(padMinusBackgroundProfile) * -1.1f),
                                                                           Math.max(ArrayUtil.max(padMinusBackgroundProfile) * 1.1f,ArrayUtil.max(padMinusBackgroundProfile) * -1.1f),
                                                                           padMinusBackgroundProfile,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.NONE);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   padSliceNameEnum,
                                   subtypeOfJoints,
                                   this, false, true,
                                   padMinusBackgroundProfileDiagInfo);
    }
  }

  /**
   * Get measurements across located pad
   *
   * @author Lim, Seng Yew
   */
  private void measureJointAcross(ReconstructedImages reconstructedImages,
                                Image image,
                                Subtype subtype,
                                SliceNameEnum sliceNameEnum,
                                JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    Subtype subtypeOfJoints = jointInspectionData.getSubtype();

    SliceNameEnum padSliceNameEnum = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the IPD.
    int interPadDistance = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();
    
    float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f * (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT);

    final int SMOOTH_FACTOR = 6;
    final int DERIVATIVE_FACTOR = 4;

    // Initialize all region of interest
    RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
    RegionOfInterest backgroundROIOne = new RegionOfInterest(jointRoi);
    RegionOfInterest backgroundROITwo = new RegionOfInterest(jointRoi);

    // Resize area to extract located pad profile.
    // Resize area to extract located pad profile.
    final float PAD_WIDTH_ALONG =
       (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH) / 100.f;
    final float PAD_HEIGHT_ALONG =
       (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT) / 100.f;
    final float IPD_PERCENT =
       (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT) / 100.f;
    int wantedPadWidth = Math.round(jointRoi.getLengthAlong() * PAD_HEIGHT_ALONG);
    int halfInterPadDistance = Math.round(interPadDistance * IPD_PERCENT) / 2;
    int wantedHeight = Math.round(jointRoi.getLengthAcross() * PAD_WIDTH_ALONG) + Math.round(interPadDistance * IPD_PERCENT); // Original FET algo in 5DX don't consider IPD. Requested by Jeremy P.

    // Resize area to extract both background profile.
    int wantedWidth = 3;
    // Wei Chin Fixed for Plexus Eval (18 June 2010)
    wantedHeight = Math.min(wantedHeight, image.getHeight() -1);

    backgroundROIOne.setLengthAlong(wantedWidth);
    backgroundROIOne.setLengthAcross(wantedHeight);
    backgroundROITwo.setLengthAlong(wantedWidth);
    backgroundROITwo.setLengthAcross(wantedHeight);
    // Move/Translate backgroundROIOne to correct location
    backgroundROIOne.translateAlongAcross(jointRoi.getLengthAlong()/2 + 3 + ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)),0);
    backgroundROITwo.translateAlongAcross(-jointRoi.getLengthAlong()/2 - 3 - ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)),0);
    // Make sure both backgroundROI are within image boundary
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image,backgroundROIOne);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image,backgroundROITwo);
    // Adjust jointROI only after both background ROI are adjusted correctly, becos original jointRoi info needed.
    wantedWidth = Math.max(3,wantedPadWidth);
    jointRoi.setLengthAlong(wantedWidth);
    jointRoi.setLengthAcross(wantedHeight);
    AlgorithmUtil.shiftRegionIntoImageIfNecessary(image,jointRoi);

    // Measure the profiles.
    Assert.expect(backgroundROIOne.getLengthAcross() == backgroundROITwo.getLengthAcross(),
            "Error in Large Pad Open algorithm - length across are not the same between upper (" + backgroundROIOne.getLengthAcross() + ") and lower background {" + backgroundROITwo.getLengthAcross() + ").");
    Assert.expect(jointRoi.getLengthAcross() == backgroundROIOne.getLengthAcross(),
            "Error in Large Pad Open algorithm - length across are not the same between located pad (" + jointRoi.getLengthAcross() + ") and background (" + backgroundROIOne.getLengthAcross() + ").");
    // Have to change the orientation of ROI before profile, because we want to profile across. It profile along by default.
    int currentOrientation = jointRoi.getOrientationInDegrees() + 90;
    jointRoi.setOrientationInDegrees(currentOrientation);
    float[] locatedPadProfile  = ImageFeatureExtraction.profile(image, jointRoi);

    currentOrientation = backgroundROIOne.getOrientationInDegrees() + 90;
    backgroundROIOne.setOrientationInDegrees(currentOrientation);
    float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundROIOne);

    currentOrientation = backgroundROITwo.getOrientationInDegrees() + 90;
    backgroundROITwo.setOrientationInDegrees(currentOrientation);
    float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundROITwo);

    locatedPadProfile  = ProfileUtil.getSmoothedProfile(locatedPadProfile,SMOOTH_FACTOR);
    backgroundProfile1 = ProfileUtil.getSmoothedProfile(backgroundProfile1,SMOOTH_FACTOR);
    backgroundProfile2 = ProfileUtil.getSmoothedProfile(backgroundProfile2,SMOOTH_FACTOR);

    // Display diagnostic when necessary
    if (ImageAnalysis.areDiagnosticsEnabled(subtypeOfJoints.getJointTypeEnum(), this))
    {
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo =
          new MeasurementRegionDiagnosticInfo(jointRoi,
                                              MeasurementRegionEnum.PAD_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(inspectionRegion,
                                                         padSliceNameEnum,
                                                         subtypeOfJoints,
                                                         this, true, false,
                                                         componentRegionDiagnosticInfo);
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo1 =
          new MeasurementRegionDiagnosticInfo(backgroundROIOne,
                                              MeasurementRegionEnum.BACKGROUND_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(inspectionRegion,
                                                         padSliceNameEnum,
                                                         subtypeOfJoints,
                                                         this, false, false,
                                                         componentRegionDiagnosticInfo1);
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo2 =
          new MeasurementRegionDiagnosticInfo(backgroundROITwo,
                                              MeasurementRegionEnum.BACKGROUND_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(inspectionRegion,
                                                         padSliceNameEnum,
                                                         subtypeOfJoints,
                                                         this, false, false,
                                                         componentRegionDiagnosticInfo2);
    }

    // Calculate average background profile
    float[] padMinusBackgroundProfile = new float[backgroundProfile1.length];
    for (int i = 0; i < backgroundProfile1.length; ++i)
    {
      padMinusBackgroundProfile[i] = Math.max(0.0f, (backgroundProfile1[i] + backgroundProfile2[i]) / 2.0f - locatedPadProfile[i]);
      locatedPadProfile[i] = padMinusBackgroundProfile[i]; // Save the original values before derivatives.
    }
    // 5DX FET don't do smooth after getting the locatedPadProfile[], but Jeremy Pemberton said should not skip this.
    // Use insufficientSmoothingFactor threshold instead of fixed value 6.
    int insufficientSmoothingFactor =
      ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR)).intValue();
    padMinusBackgroundProfile = ProfileUtil.getSmoothedProfile(padMinusBackgroundProfile,insufficientSmoothingFactor); // Use smoothingFactor threshold instead
    padMinusBackgroundProfile = ProfileUtil.createDerivativeProfile(padMinusBackgroundProfile, DERIVATIVE_FACTOR);

    // Display diagnostic when necessary
    if (ImageAnalysis.areDiagnosticsEnabled(subtypeOfJoints.getJointTypeEnum(), this))
    {
      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[locatedPadProfile.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.PAD_REGION);
      LocalizedString padRegionLabelLocalizedString =
        new LocalizedString("PAD_REGION_KEY", null);
      ProfileDiagnosticInfo locatedPadProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padRegionLabelLocalizedString,
                                                                           0.0f,
                                                                           Math.max(ArrayUtil.max(locatedPadProfile) * 1.1f,ArrayUtil.max(locatedPadProfile) * -1.1f),
                                                                           locatedPadProfile,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.NONE);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   padSliceNameEnum,
                                   subtypeOfJoints,
                                   this, false, true,
                                   locatedPadProfileDiagInfo);
    }
    if (ImageAnalysis.areDiagnosticsEnabled(subtypeOfJoints.getJointTypeEnum(), this))
    {
      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[padMinusBackgroundProfile.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.FIRST_DERIVATIVES_PROFILE_REGION);
      LocalizedString padRegionLabelLocalizedString =
        new LocalizedString("FIRST_DERIVATIVES_PROFILE_REGION_KEY", null);
      ProfileDiagnosticInfo padMinusBackgroundProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padRegionLabelLocalizedString,
                                                                           Math.min(ArrayUtil.min(padMinusBackgroundProfile) * 1.1f,ArrayUtil.min(padMinusBackgroundProfile) * -1.1f),
                                                                           Math.max(ArrayUtil.max(padMinusBackgroundProfile) * 1.1f,ArrayUtil.max(padMinusBackgroundProfile) * -1.1f),
                                                                           padMinusBackgroundProfile,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.NONE);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   padSliceNameEnum,
                                   subtypeOfJoints,
                                   this, false, true,
                                   padMinusBackgroundProfileDiagInfo);
    }

    float leadingSlopeAcross = ArrayUtil.max(padMinusBackgroundProfile);
    float trailingSlopeAcross = ArrayUtil.min(padMinusBackgroundProfile) * -1.0f;
    float slopeSumAcross = leadingSlopeAcross + trailingSlopeAcross;

    // Create measurement object
    JointMeasurement leadingSlopeMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             leadingSlopeAcross);
    jointInspectionResult.addMeasurement(leadingSlopeMeasurement);

    // Create measurement object
    JointMeasurement trailingSlopeMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             trailingSlopeAcross);
    jointInspectionResult.addMeasurement(trailingSlopeMeasurement);

    // Create measurement object
    JointMeasurement slopeSumMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             slopeSumAcross);
    jointInspectionResult.addMeasurement(slopeSumMeasurement);

    // Post a diagnostic showing the labeled along profile and the measured values.
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
    {
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      inspectionRegion,
                                                      jointInspectionData,
                                                      leadingSlopeMeasurement,
                                                      trailingSlopeMeasurement,
                                                      slopeSumMeasurement);
    }
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getLeadingSlopeAlongMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getTrailingSlopeAlongMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getSlopeSumAlongMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getLeadingSlopeAcrossMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getTrailingSlopeAcrossMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getSlopeSumAcrossMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getPadThicknessMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_INSUFFICIENT_PAD_THICKNESS);
  }

  /**
   * @author Lim, Seng Yew
   */
  public static JointMeasurement getSumOfSlopeChangesMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES);
  }
  
  /**
   * @author Jack Hwee
  */
  private static int getNumberOfUserDefinedSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE);

    return Integer.parseInt(numberOfSlice);
  }
  
    /**
   * @param subtype
   * @return List<SliceNameEnum>
   * @author Jack Hwee
   */
  public List<SliceNameEnum> getInspectionSliceOrder(Subtype subtype)
  {
    Assert.expect(subtype != null);

    InspectionFamily thisFamily= getInspectionFamily();
    List<SliceNameEnum> slicesInOrderOfInspection = (List<SliceNameEnum>)thisFamily.getOrderedInspectionSlices(subtype);
    // DO NOT ADD new slices here. Add slices in InspectionFamily.
    // Remove slices  not required for Algorithm here
    
    return slicesInOrderOfInspection ;
  }

  /**
   * @author Siew Yeng
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);
    
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Let Locator learn its stuff.
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);
    
    // Measure the profiles and derivative profiles (both full and fractional pad length across) for each of of the joints.
    Map<Pair<JointInspectionData, ImageSetData>, LargePadProfileDataForLearning> profileDataMap =
        measurePadThicknessProfilesForLearning(subtype, typicalBoardImages, padSlice);

    if(subtype.isEnabledSlopeSettingsLearning())
    {
      // run learning on all pertinent measurements for all large pad joints
      learnLargePadJointMeasurementsForSlope(subtype, padSlice, profileDataMap);
    }
  }
  
  /**
   * @author Siew Yeng
   */
  private Map<Pair<JointInspectionData, ImageSetData>, LargePadProfileDataForLearning>
      measurePadThicknessProfilesForLearning(Subtype subtype,
                                             ManagedOfflineImageSet imageSet,
                                             SliceNameEnum slice) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(imageSet != null);
    Assert.expect(slice != null);
    
    Map<Pair<JointInspectionData, ImageSetData>, LargePadProfileDataForLearning> padProfileDataMap =
        new HashMap<Pair<JointInspectionData, ImageSetData>, LargePadProfileDataForLearning>();
    
    // Resize area to extract located pad profile.
    final float PAD_WIDTH_ALONG =
       (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH) / 100.f;
    final float PAD_HEIGHT_ALONG =
       (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT) / 100.f;
    final float IPD_PERCENT =
       (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT) / 100.f;
    
    ManagedOfflineImageSetIterator imagesIterator = imageSet.iterator();
    int samplingFrequency = Math.max(1, imageSet.size() / _MAX_NUMBER_OF_REGIONS_FOR_LEARNING);
    imagesIterator.setSamplingFrequency(samplingFrequency);

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      // Get the image for the relevant slice.
      ImageSetData currentImageSetData = imagesIterator.getCurrentImageSetData();
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(slice);
      Image sliceImage = reconstructedSlice.getOrthogonalImage();

      // Get all the joints in the region which are of the applicable subtype.
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      // Run Locator.
      Locator.locateJoints(reconstructedImages, slice, jointInspectionDataObjects, this, true);

      final int SMOOTH_FACTOR = 6;
      final int DERIVATIVE_FACTOR = 4;
        
      // Measure the profiles for each joint in the region.
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // Get the IPD.
        int interPadDistance = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();
        
        float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f * (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT);
    
        // Initialize all region of interest
        RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
        RegionOfInterest backgroundROIOne = new RegionOfInterest(jointRoi);
        RegionOfInterest backgroundROITwo = new RegionOfInterest(jointRoi);
        
        // ---------------------Joint Along-----------------------------------------------------
        
        int wantedPadHeight = Math.round(jointRoi.getLengthAcross() * PAD_HEIGHT_ALONG);
        int wantedWidth = Math.round(jointRoi.getLengthAlong() * PAD_WIDTH_ALONG) + Math.round(interPadDistance * IPD_PERCENT); 
        // Resize area to extract both background profile.
        int wantedHeight = 3;
        
        backgroundROIOne.setLengthAlong(wantedWidth);
        backgroundROIOne.setLengthAcross(wantedHeight);
        backgroundROITwo.setLengthAlong(wantedWidth);
        backgroundROITwo.setLengthAcross(wantedHeight);
        
        // Move/Translate backgroundROIOne to correct location
        backgroundROIOne.translateAlongAcross(0, jointRoi.getLengthAcross()/2 + 3 + ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)));
        backgroundROITwo.translateAlongAcross(0, -jointRoi.getLengthAcross()/2 - 3 - ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)));
        // Make sure both backgroundROI are within image boundary
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(sliceImage,backgroundROIOne);
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(sliceImage,backgroundROITwo);
        // Adjust jointROI only after both background ROI are adjusted correctly, becos original jointRoi info needed.
        wantedHeight = Math.max(3,wantedPadHeight);
        jointRoi.setLengthAlong(wantedWidth);
        jointRoi.setLengthAcross(wantedHeight);
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(sliceImage,jointRoi);
        
        // Measure the profiles.
        Assert.expect(backgroundROIOne.getLengthAlong() == backgroundROITwo.getLengthAlong(),
                "Error in Large Pad Open algorithm - length along are not the same between upper (" + backgroundROIOne.getLengthAlong() + ") and lower background {" + backgroundROITwo.getLengthAlong() + ").");
        Assert.expect(jointRoi.getLengthAlong() == backgroundROIOne.getLengthAlong(),
                "Error in Large Pad Open algorithm - length along are not the same between located pad (" + jointRoi.getLengthAlong() + ") and background (" + backgroundROIOne.getLengthAlong() + ").");
        float[] locatedPadProfile  = ImageFeatureExtraction.profile(sliceImage, jointRoi);
        float[] backgroundProfile1 = ImageFeatureExtraction.profile(sliceImage, backgroundROIOne);
        float[] backgroundProfile2 = ImageFeatureExtraction.profile(sliceImage, backgroundROITwo);
        
        locatedPadProfile  = ProfileUtil.getSmoothedProfile(locatedPadProfile,SMOOTH_FACTOR);
        backgroundProfile1 = ProfileUtil.getSmoothedProfile(backgroundProfile1,SMOOTH_FACTOR);
        backgroundProfile2 = ProfileUtil.getSmoothedProfile(backgroundProfile2,SMOOTH_FACTOR);
        
        float[] padMinusBackgroundProfile = new float[backgroundProfile1.length];
        for (int i = 0; i < backgroundProfile1.length; ++i)
        {
          padMinusBackgroundProfile[i] = Math.max(0.0f, (backgroundProfile1[i] + backgroundProfile2[i]) / 2.0f - locatedPadProfile[i]);
          locatedPadProfile[i] = padMinusBackgroundProfile[i]; // Save the original values before derivatives.
        }
        
        // 5DX FET do smooth after getting the locatedPadProfile[], so I just follow so.
        // Use insufficientSmoothingFactor threshold instead of fixed value 6.
        int insufficientSmoothingFactor =
          ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR)).intValue();
        padMinusBackgroundProfile = ProfileUtil.getSmoothedProfile(padMinusBackgroundProfile,insufficientSmoothingFactor);
        padMinusBackgroundProfile = ProfileUtil.createDerivativeProfile(padMinusBackgroundProfile, DERIVATIVE_FACTOR);
        
        float[] padMinusBackgroundProfile2 = ProfileUtil.createDerivativeProfile(padMinusBackgroundProfile, DERIVATIVE_FACTOR);

        // ---------------------Joint Across-----------------------------------------------------
        
        // Initialize all region of interest
        jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
        backgroundROIOne = new RegionOfInterest(jointRoi);
        backgroundROITwo = new RegionOfInterest(jointRoi);
        
        int acrossWantedPadWidth = Math.round(jointRoi.getLengthAlong() * PAD_HEIGHT_ALONG);
        int halfInterPadDistance = Math.round(interPadDistance * IPD_PERCENT) / 2;
        int acrossWantedHeight = Math.round(jointRoi.getLengthAcross() * PAD_WIDTH_ALONG) + Math.round(interPadDistance * IPD_PERCENT); // Original FET algo in 5DX don't consider IPD. Requested by Jeremy P.

        // Resize area to extract both background profile.
        int acrossWantedWidth = 3;
        // Wei Chin Fixed for Plexus Eval (18 June 2010)
        acrossWantedHeight = Math.min(acrossWantedHeight, sliceImage.getHeight() -1);

        backgroundROIOne.setLengthAlong(acrossWantedWidth);
        backgroundROIOne.setLengthAcross(acrossWantedHeight);
        backgroundROITwo.setLengthAlong(acrossWantedWidth);
        backgroundROITwo.setLengthAcross(acrossWantedHeight);
        // Move/Translate backgroundROIOne to correct location
        backgroundROIOne.translateAlongAcross(jointRoi.getLengthAlong()/2 + 3 + ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)),0);
        backgroundROITwo.translateAlongAcross(-jointRoi.getLengthAlong()/2 - 3 - ((int)(backgroundRegionShiftAsFractionOfInterPadDistance*interPadDistance)),0);
        // Make sure both backgroundROI are within image boundary
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(sliceImage,backgroundROIOne);
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(sliceImage,backgroundROITwo);
        // Adjust jointROI only after both background ROI are adjusted correctly, becos original jointRoi info needed.
        acrossWantedWidth = Math.max(3,acrossWantedPadWidth);
        jointRoi.setLengthAlong(acrossWantedWidth);
        jointRoi.setLengthAcross(acrossWantedHeight);
        AlgorithmUtil.shiftRegionIntoImageIfNecessary(sliceImage,jointRoi);

        // Measure the profiles.
        Assert.expect(backgroundROIOne.getLengthAcross() == backgroundROITwo.getLengthAcross(),
                "Error in Large Pad Open algorithm - length across are not the same between upper (" + backgroundROIOne.getLengthAcross() + ") and lower background {" + backgroundROITwo.getLengthAcross() + ").");
        Assert.expect(jointRoi.getLengthAcross() == backgroundROIOne.getLengthAcross(),
                "Error in Large Pad Open algorithm - length across are not the same between located pad (" + jointRoi.getLengthAcross() + ") and background (" + backgroundROIOne.getLengthAcross() + ").");
        // Have to change the orientation of ROI before profile, because we want to profile across. It profile along by default.
        int currentOrientation = jointRoi.getOrientationInDegrees() + 90;
        jointRoi.setOrientationInDegrees(currentOrientation);
        float[] locatedPadProfileAcross  = ImageFeatureExtraction.profile(sliceImage, jointRoi);

        currentOrientation = backgroundROIOne.getOrientationInDegrees() + 90;
        backgroundROIOne.setOrientationInDegrees(currentOrientation);
        float[] backgroundProfile1Across = ImageFeatureExtraction.profile(sliceImage, backgroundROIOne);

        currentOrientation = backgroundROITwo.getOrientationInDegrees() + 90;
        backgroundROITwo.setOrientationInDegrees(currentOrientation);
        float[] backgroundProfile2Across = ImageFeatureExtraction.profile(sliceImage, backgroundROITwo);

        locatedPadProfileAcross  = ProfileUtil.getSmoothedProfile(locatedPadProfileAcross,SMOOTH_FACTOR);
        backgroundProfile1Across = ProfileUtil.getSmoothedProfile(backgroundProfile1Across,SMOOTH_FACTOR);
        backgroundProfile2Across = ProfileUtil.getSmoothedProfile(backgroundProfile2Across,SMOOTH_FACTOR);

        // Calculate average background profile
        float[] padMinusBackgroundProfileAcross = new float[backgroundProfile1Across.length];
        for (int i = 0; i < backgroundProfile1Across.length; ++i)
        {
          padMinusBackgroundProfileAcross[i] = Math.max(0.0f, (backgroundProfile1Across[i] + backgroundProfile2Across[i]) / 2.0f - locatedPadProfileAcross[i]);
          locatedPadProfileAcross[i] = padMinusBackgroundProfileAcross[i]; // Save the original values before derivatives.
        }

        padMinusBackgroundProfileAcross = ProfileUtil.getSmoothedProfile(padMinusBackgroundProfileAcross,insufficientSmoothingFactor); // Use smoothingFactor threshold instead
        padMinusBackgroundProfileAcross = ProfileUtil.createDerivativeProfile(padMinusBackgroundProfileAcross, DERIVATIVE_FACTOR);

        // Add the measured profile data to our map.
        LargePadProfileDataForLearning profileDataForLearning =
            new LargePadProfileDataForLearning(padMinusBackgroundProfile,
                                            padMinusBackgroundProfile2,
                                            padMinusBackgroundProfileAcross);

        padProfileDataMap.put(new Pair<JointInspectionData, ImageSetData>(jointInspectionData, currentImageSetData),
                              profileDataForLearning);
      }
      
      imagesIterator.finishedWithCurrentRegion();
      
      if (jointInspectionDataObjects != null)
      {
        jointInspectionDataObjects.clear();
        jointInspectionDataObjects = null;
      }
    }
    
    Assert.expect(padProfileDataMap != null);
    return padProfileDataMap;
  }
      
  /**
   * @author Siew Yeng
   */
  private void learnLargePadJointMeasurementsForSlope(Subtype subtype, 
                                                      SliceNameEnum slice, 
                                                      Map<Pair<JointInspectionData, ImageSetData>, 
                                                      LargePadProfileDataForLearning> profileDataMap) throws DatastoreException
  {
    
    Assert.expect(subtype != null);
    Assert.expect(slice != null);
    Assert.expect(profileDataMap != null);

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, LargePadProfileDataForLearning> mapEntry : profileDataMap.entrySet())
    {
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      LargePadProfileDataForLearning profileData = mapEntry.getValue();
      float[] padMinusBackgroundProfile = profileData.getPadMinusBackgroundProfile();
      float[] padMinusBackgroundProfile2 = profileData.getPadMinusBackgroundProfile2();
      float[] padMinusBackgroundProfileAcross = profileData.getPadMinusBackgroundProfileAcross();
      
      float leadingSlopeAlong = ArrayUtil.max(padMinusBackgroundProfile);
      float trailingSlopeAlong = ArrayUtil.min(padMinusBackgroundProfile) * -1.0f;
      float slopeSumAlong = leadingSlopeAlong + trailingSlopeAlong;
      
      // Calculate sum of slope changes
      float sumOfSlopeChanges = 0.0f;
      for (int i = 0; i < padMinusBackgroundProfile2.length; ++i)
      {
        sumOfSlopeChanges += Math.abs(padMinusBackgroundProfile2[i]);
      }
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES,
                                    sumOfSlopeChanges,
                                    true,
                                    true));
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ALONG,
                                    leadingSlopeAlong,
                                    true,
                                    true));
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG,
                                    trailingSlopeAlong,
                                    true,
                                    true));
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ALONG,
                                    slopeSumAlong,
                                    true,
                                    true));
      
      float leadingSlopeAcross = ArrayUtil.max(padMinusBackgroundProfileAcross);
      float trailingSlopeAcross = ArrayUtil.min(padMinusBackgroundProfileAcross) * -1.0f;
      float slopeSumAcross = leadingSlopeAcross + trailingSlopeAcross;
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS,
                                    leadingSlopeAcross,
                                    true,
                                    true));
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS,
                                    leadingSlopeAcross,
                                    true,
                                    true));
      
      subtype.addLearnedJointMeasurementData(
        new LearnedJointMeasurement(jointInspectionData.getPad(),
                                    slice,
                                    MeasurementEnum.LARGE_PAD_OPEN_SLOPE_SUM_ACROSS,
                                    slopeSumAcross,
                                    true,
                                    true));
    }
  }
}
