package com.axi.v810.business.imageAnalysis.calibration;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * @todo Remove this before shipping
 *
 * @author Peter Esbensen
 */
public class CalibrationInspectionFamily extends InspectionFamily
{
  /**
   * @author Peter Esbensen
   */
  public CalibrationInspectionFamily()
  {
    super(InspectionFamilyEnum.CALIBRATION);
    Algorithm chipMeasurementAlgorithm = new CalibrationMeasurementAlgorithm();
    addAlgorithm(chipMeasurementAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return Collections.singletonList(SliceNameEnum.PAD);
  }

  /**
   * Returns the appropriate default SliceNameEnum for the Pad slice on the Chip inspection family given the joint
   * type of the specified JointInspectionData.
   *
   * @author Peter Esbensen
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return SliceNameEnum.PAD;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }


  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }
}
