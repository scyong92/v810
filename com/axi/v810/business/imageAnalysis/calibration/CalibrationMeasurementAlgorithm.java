package com.axi.v810.business.imageAnalysis.calibration;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;


/**
 * This is a simple algorithm to measure the gray levels of a pad's region of interest.
 * This is just for doing thickness calibration during the Alfalfa phase.
 *
 * @todo Remove this before shipping
 *
 * @author Peter Esbensen
 */
public class CalibrationMeasurementAlgorithm extends Algorithm
{

  /**
   * @author Peter Esbensen
   */
  public CalibrationMeasurementAlgorithm()
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.CALIBRATION);
    addMeasurementEnums();
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.CALIBRATION_GRAY_LEVEL);
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructedSlice reconstructedSlice = reconstructedImages.getFirstSlice();
    Image image = reconstructedSlice.getImage();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      RegionOfInterest regionOfInterest = jointInspectionData.getOrthogonalRegionOfInterestInPixels();

      float averageGrayLevel = Statistics.mean( image, regionOfInterest );

      JointMeasurement jointMeasurement = new JointMeasurement(this, MeasurementEnum.CALIBRATION_GRAY_LEVEL,
          MeasurementUnitsEnum.GRAYLEVEL, jointInspectionData.getPad(), sliceNameEnum,
          averageGrayLevel, true);

      jointInspectionData.getJointInspectionResult().addMeasurement(jointMeasurement);
    }
  }


}
