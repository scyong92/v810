package com.axi.v810.business.imageAnalysis.pressfit;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for pressfit joints.
 *
 * @author Peter Esbensen
 */
public class PressfitInspectionFamily extends InspectionFamily
{
  /**
   * @author Peter Esbensen
   */
  public PressfitInspectionFamily()
  {
    super(InspectionFamilyEnum.PRESSFIT);
    Algorithm pressfitMeasurementAlgorithm = new PressfitMeasurementAlgorithm(this);
    addAlgorithm(pressfitMeasurementAlgorithm);
    Algorithm pressfitInsufficentAlgorithm = new PressfitOpenAlgorithm(this);
    addAlgorithm(pressfitInsufficentAlgorithm);
    Algorithm pressfitShortAlgorithm = new CircularShortAlgorithm(InspectionFamilyEnum.PRESSFIT);
    addAlgorithm(pressfitShortAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Collection<SliceNameEnum> inspectedSlices = new LinkedList();
    
    inspectedSlices.addAll(PressfitMeasurementAlgorithm.getAllBarrelSliceEnumList(subtype));
        
    inspectedSlices.add(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);

    return inspectedSlices;
  }

  /**
   * Returns the applicable SliceNameEnum for a throughhole's pad slice.
   * Try to choose the best slice for locator to run on. This is the barrel slice.
   *
   * @todo PE rename this?
   *
   * @author Patrick Lacz
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return SliceNameEnum.THROUGHHOLE_BARREL;
  }

  /**
   * @author Patrick Lacz
   */
  public Collection<SliceNameEnum> getShortInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return Collections.singleton(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }

}
