package com.axi.v810.business.imageAnalysis.pressfit;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.imageAnalysis.throughHole.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * @author Patrick Lacz
 */
public class PressfitMeasurementAlgorithm extends Algorithm
{
  private final static String _THROUGH_HOLE_IN_MILS = "Thickness";
  private final static String _THROUGH_HOLE_IN_PERCENTAGE = "Percentage";

  private final static int _MIN_NUMBER_OF_BARREL_SLICE = 1;
  private final static int _MAX_NUMBER_OF_BARREL_SLICE = 10;

  private final static ArrayList<String> _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE  = new ArrayList<String>();
  private final static ArrayList<String> _THROUGH_HOLE_WORKING_UNIT = new ArrayList<String>(Arrays.asList(_THROUGH_HOLE_IN_PERCENTAGE, _THROUGH_HOLE_IN_MILS));
  
  //private final static String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";
  
  static
  {
    for(int i = _MIN_NUMBER_OF_BARREL_SLICE ; i<=_MAX_NUMBER_OF_BARREL_SLICE; i ++)
      _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE.add(Integer.toString(i));
  }
  /**
   * @author Patrick Lacz
   */
  public PressfitMeasurementAlgorithm(InspectionFamily inspectionFamily)
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.PRESSFIT);
    Assert.expect(inspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    AlgorithmSetting sliceWorkingUnit = new AlgorithmSetting(
        AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT, // setting enum
        displayOrder++, // display order,
        _THROUGH_HOLE_IN_PERCENTAGE,
        _THROUGH_HOLE_WORKING_UNIT,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(sliceWorkingUnit);

    AlgorithmSetting userDefineNumOfSlice = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE, // setting enum
        displayOrder++, // display order,
        new Integer(_MIN_NUMBER_OF_BARREL_SLICE).toString(),
        _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(userDefineNumOfSlice);

    displayOrder = createBarrel1SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel2SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel3SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel4SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel5SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel6SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel7SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel8SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel9SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarrel10SlicesSetting(displayOrder, currentVersion, inspectionFamily);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleComponentSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      0.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_COMPONENT_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_COMPONENT_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_COMPONENT_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleComponentSliceheight);

    /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting componentSideShortSliceheightthickness = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS, // setting enum
//      displayOrder++, // display order,
      2001, // LC: this display order number is a hack so that the Slice Setup tab in Fine Tuning displays the settings in the order desired by marketing
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-25.0f), // minimum value
      MathUtil.convertMilsToMillimeters(25.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(componentSideShortSliceheightthickness);

    // Added by Lee Herng (4 Mar 2011)
    AlgorithmSetting throughHoleAutoFocusMidBallOffset = new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_PRESSFIT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleAutoFocusMidBallOffset);
    
    AlgorithmSetting pinWidth = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_PIN_WIDTH,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(10.f), // recommended good starting point from Vyn
      0.01f, // minimum value
      5.0f, // maximum value
      MeasurementUnitsEnum.MILLIMETERS, // units
      "HTML_DESC_PRESSFIT_(PIN_WIDTH)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(PIN_WIDTH)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(PIN_WIDTH)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(pinWidth);
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_PRESSFIT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));
    
    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is displayed right before psh in the Slice Setup Tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));    
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    AlgorithmSetting predictiveSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2002, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "On",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_PRESSFIT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(predictiveSliceheight);
    
    //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
    ArrayList<String> artifactBasedRegionDirectionSeperationOptions = new ArrayList<String>(Arrays.asList("None", "Horizontal", "Vertical"));
    AlgorithmSetting artifactBasedRegionDirectionSeperation = new AlgorithmSetting(
      AlgorithmSettingEnum.ARTIFACT_BASED_REGION_SEPERATION_DIRECTION,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "None",
      artifactBasedRegionDirectionSeperationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_PRESSFIT_(ARTIFACT_BASED_REGION_SEPERATION_DIRECTION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(ARTIFACT_BASED_REGION_SEPERATION_DIRECTION)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(ARTIFACT_BASED_REGION_SEPERATION_DIRECTION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(artifactBasedRegionDirectionSeperation);
    
    // XCR-3703 System Crash When Load Pre-5.8 version Recipe that contain PSP On Pressfit and PTH Components
    AlgorithmSetting pspSearchRangeLowLimit = new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(-20.0f), // default value
        MathUtil.convertMilsToMillimeters(-50.0f), // minimum value
        MathUtil.convertMilsToMillimeters(-2.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.HIDDEN,
        currentVersion);
    addAlgorithmSetting(pspSearchRangeLowLimit);
    
    AlgorithmSetting pspSearchRangeHighLimit = new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(20.0f), // default value
        MathUtil.convertMilsToMillimeters(2.0f), // minimum value
        MathUtil.convertMilsToMillimeters(50.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.HIDDEN,
        currentVersion);
    addAlgorithmSetting(pspSearchRangeHighLimit);

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PRESSFIT_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PRESSFIT_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_PRESSFIT_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);

    addMeasurementEnums();
    
    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.PRESSFIT_GRAYLEVEL);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());
  }

  /**
   * @author Patrick Lacz
   */
  public Collection<AlgorithmSettingEnum> getLearnedAlgorithmSettings()
  {
    List<AlgorithmSettingEnum> learnedSettingsList = new ArrayList<AlgorithmSettingEnum>();

    learnedSettingsList.add(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL);
    learnedSettingsList.add(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL);
    learnedSettingsList.add(AlgorithmSettingEnum.PRESSFIT_PIN_WIDTH);
    return learnedSettingsList;
  }

  /**
   * @todo PE delete
   * @author Patrick Lacz
  static public SliceNameEnum getBarrelSliceName(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String barrelSliceChoice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_BARREL_SLICE);

    return SliceNameEnum.getEnumFromName(barrelSliceChoice);
  }
  */

  /**
   * @author Patrick Lacz
   */
  public static JointMeasurement getCorrectedGraylevelMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    //System.out.println(sliceNameEnum);
   
    JointInspectionResult result = jointInspectionData.getJointInspectionResult();
    Assert.expect(result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.PRESSFIT_GRAYLEVEL));

    return result.getJointMeasurement(sliceNameEnum, MeasurementEnum.PRESSFIT_GRAYLEVEL);
  }

  /**
   * @author Patrick Lacz
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.size() > 0);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();

    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Precompute the template matching code for finding the pin.
    Image pinTemplateImage = null;
    IntegerRef expectedPinWidthInPixels = new IntegerRef(0);

    float expectedSizeInMM = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_PIN_WIDTH);

    int borderPixels = 2;
    int scaleFactor = AlgorithmUtil.getResizeFactor(subtypeOfJoints);
    expectedSizeInMM = expectedSizeInMM * scaleFactor;
    
    pinTemplateImage = AlgorithmUtil.createPinTemplateImage(expectedSizeInMM, expectedPinWidthInPixels, 100.f, 200.f, borderPixels, subtypeOfJoints);

    // The locator slice is the barrel slice, but just to be sure nothing breaks with this assumption, we handle it separately.
    SliceNameEnum locatorSlice = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);
    Locator.locateJoints(reconstructedImages, locatorSlice, jointInspectionDataObjects, this, true);

    // Inspect only the barrel slice.
    //SliceNameEnum sliceNameEnum = SliceNameEnum.THROUGHHOLE_BARREL;
    // set up the list of slices to be inspected
    //Chin-seong.kee - We not going to fixed Barrel only but will have barrel2, 3 and 4 !
    SliceNameEnum sliceNameEnumArray[] = getSliceNameEnumArray(subtypeOfJoints);

    //loop throught every barrel slice - chin-seong.kee
    for (SliceNameEnum sliceNameEnum : sliceNameEnumArray)
    {
        if(reconstructedImages.hasReconstructedSlice(sliceNameEnum) == false)
          continue;
      
        ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
        Image image = slice.getOrthogonalImage();
        
        //Siew Yeng - XCR-2683 - add enhanced image
        if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtypeOfJoints))
        {
          ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, sliceNameEnum);
        }
      
        //    System.out.println(sliceNameEnum);
        // display the new image
        AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);

        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                    jointInspectionData,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    slice,
                                                    false);
          // Measure the graylevel of the joint
          RegionOfInterest barrelRegion = AlgorithmUtil.getRegionOfInterestOfThroughholeBarrel(jointInspectionData, true);
          RegionOfInterest padRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
          barrelRegion.setCenterXY(padRegion.getCenterX(), padRegion.getCenterY());

          RegionOfInterest regionToMeasureGraylevel = new RegionOfInterest(barrelRegion);

          // For pressfit, we want to measure the graylevel of the pin, not the joint.
          regionToMeasureGraylevel = getPressfitGraylevelMeasurementRegion(pinTemplateImage, expectedPinWidthInPixels.getValue(), image, barrelRegion, borderPixels);

          regionToMeasureGraylevel = RegionOfInterest.createRegionFromIntersection(regionToMeasureGraylevel, image);
          // The barrel is always circular; at the very least, it won't hurt to take a circular average even if the
          // pad is rectangular (eg. pin 1)
          regionToMeasureGraylevel.setRegionShapeEnum(RegionShapeEnum.OBROUND);

          RegionOfInterest exclutionRegionForBackgroundMeasurement = RegionOfInterest.createRegionFromIntersection(barrelRegion, image);

          _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(), sliceNameEnum, jointInspectionData, this, false,
                                       new MeasurementRegionDiagnosticInfo(barrelRegion, MeasurementRegionEnum.BARREL_REGION),
                                       new MeasurementRegionDiagnosticInfo(regionToMeasureGraylevel, MeasurementRegionEnum.PIN_REGION));



      //XCR1171 - joint profile diagnostic
      RegionOfInterest componentRegionOfInterest = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
      RegionOfInterest regionAcrossComponent = new RegionOfInterest(componentRegionOfInterest);
      regionAcrossComponent.scaleFromCenterAlongAcross(1.3, 1.0);
    
      //Ngie Xing, PCR-43, Software crash when Run Test for Pressfit Image
      if(AlgorithmUtil.checkRegionBoundaries(regionAcrossComponent, image, jointInspectionData.getInspectionRegion(), "component region of interest")== false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, regionAcrossComponent);
      }
      
      float[] grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(slice.getOrthogonalImage(), regionAcrossComponent);

      float[] estimatedBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(grayLevelProfileAcrossComponent);

      // Create a thickness profile.
      SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtypeOfJoints);
      int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtypeOfJoints);
      float[] thicknessProfileAcrossInMillimeters = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent, estimatedBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

//?? Why Call Twice? Commented by Wei Chin
//      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent, estimatedBackgroundProfile, thicknessTable);

      float[] componentThicknessProfile = ThroughHoleAlgorithmUtil.getComponentThicknessProfileForPressfitOrThroughHole(slice,
              reconstructedImages.getReconstructionRegion(),
              componentRegionOfInterest,
              jointInspectionData, this, true);

      if (regionAcrossComponent.getOrientationInDegrees() == 180 || regionAcrossComponent.getOrientationInDegrees() == 360)
      {
        float[] reverseComponentThicknessProfile = new float[componentThicknessProfile.length];
        int j = 0;
        for (int i = componentThicknessProfile.length - 1; i >= 0; i--)
        {
          reverseComponentThicknessProfile[j++] = componentThicknessProfile[i];
        }
        componentThicknessProfile = reverseComponentThicknessProfile;
      }
      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[thicknessProfileAcrossInMillimeters.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.COMPONENT_PROFILE_REGION);
      LocalizedString padThicknessProfileLabelLocalizedString =
              new LocalizedString("ALGIAG_CHIP_MEASUREMENT_OPAQUE_CHIP_EDGE_LOCATION_PROFILE_KEY", null);
      ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo = ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
              ProfileTypeEnum.SOLDER_PROFILE,
              padThicknessProfileLabelLocalizedString,
              0.0f, (ArrayUtil.max(componentThicknessProfile) * 1.1f),
              componentThicknessProfile,
              measurementRegionEnums,
              MeasurementUnitsEnum.MILS);

      _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(),
              sliceNameEnum,
              jointInspectionData.getSubtype(),
              this, false, true,
              labeledPadThicknessProfileDiagInfo);

      float correctedGraylevel = AlgorithmUtil.measureCorrectedGraylevelFromPercentile(image,
          jointInspectionData,
          regionToMeasureGraylevel,
          exclutionRegionForBackgroundMeasurement,
          .95f,
          sliceNameEnum,
          reconstructedImages.getReconstructionRegion(),
          this, false);

      JointMeasurement correctedGraylevelMeasurement = new JointMeasurement(
              this,
              MeasurementEnum.PRESSFIT_GRAYLEVEL,
              MeasurementUnitsEnum.GRAYLEVEL,
              jointInspectionData.getPad(),
              sliceNameEnum,
              (float)correctedGraylevel);

          // record the measurement data
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
              reconstructedImages.getReconstructionRegion(),
              jointInspectionData,
              correctedGraylevelMeasurement);

          JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
          jointInspectionResult.addMeasurement(correctedGraylevelMeasurement);
        }
    }
    if (pinTemplateImage!=null)
      pinTemplateImage.decrementReferenceCount();
  }

   /**
   * Return the list of slices that will be inspected.  The number of slices.
   *
   * @author chin-seong.kee
   */
  private SliceNameEnum[] getSliceNameEnumArray(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // set up the list of slices to be inspected
    List<SliceNameEnum> sliceNameEnumList = new ArrayList();

    sliceNameEnumList.addAll(getAllBarrelSliceEnumList(subtype));
    
    return sliceNameEnumList.toArray(new SliceNameEnum[sliceNameEnumList.size()]);
  }

  /**
   * @author Patrick Lacz
   */
  private RegionOfInterest getPressfitGraylevelMeasurementRegion(Image pinTemplateImage, int expectedPinWidthInPixels, Image image,
      RegionOfInterest jointRegion, int borderPixels)
  {
    Assert.expect(pinTemplateImage != null);
    Assert.expect(expectedPinWidthInPixels != 0);
    Assert.expect(image != null);
    Assert.expect(jointRegion != null);

    if (pinTemplateImage.getWidth() > jointRegion.getWidth() || pinTemplateImage.getHeight() > jointRegion.getHeight())
      return new RegionOfInterest(jointRegion);

    // find the pin within the region of the barrel
    DoubleRef matchQuality = new DoubleRef(0.0);
    ImageCoordinate matchedPosition = Filter.matchTemplate(image,
                                                           pinTemplateImage,
                                                           jointRegion,
                                                           MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                           matchQuality);

    RegionOfInterest regionToMeasureGraylevel = new RegionOfInterest(jointRegion);
    regionToMeasureGraylevel.setMinXY(matchedPosition.getX() + borderPixels, matchedPosition.getY() + borderPixels);
    regionToMeasureGraylevel.setWidthKeepingSameMinX(expectedPinWidthInPixels);
    regionToMeasureGraylevel.setHeightKeepingSameMinY(expectedPinWidthInPixels);
    return regionToMeasureGraylevel;
  }

  /**
   * @author Patrick Lacz
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {

    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);

    SliceNameEnum padSliceName = subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // for each image set
    ManagedOfflineImageSetIterator imageSetIterator = typicalBoardImages.iterator();
    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imageSetIterator.getNext()) != null)
    {
      ReconstructionRegion region = reconstructedImages.getReconstructionRegion();

      // find the location of the joints
      Locator.locateJoints(reconstructedImages, padSliceName, region.getInspectableJointInspectionDataList(subtype), this, true);

      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude inside, because not so applicable & not so direct.
      recordPressfitLearningMeasurements(subtype, reconstructedImages);
      imageSetIterator.finishedWithCurrentRegion();
    }

    // done with getting data from the image sets
    boolean learnOnlyNominals = false;
    learnSettingsFromDatabase(subtype, learnOnlyNominals);
  }

  /**
   * @author Patrick Lacz
   */
  private void recordPressfitLearningMeasurements(Subtype subtype, ReconstructedImages images) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(images != null);

    ReconstructionRegion region = images.getReconstructionRegion();
    if (region.getJointInspectionDataList().isEmpty())
      return;

    float expectedSizeInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_PIN_WIDTH);

    IntegerRef expectedPinDiameterInPixels = new IntegerRef(0);
    int borderPixels = 2;
    Image pinTemplateImage = AlgorithmUtil.createPinTemplateImage(expectedSizeInMM, expectedPinDiameterInPixels, 100.f, 200.f, borderPixels, subtype);

    List<SliceNameEnum> barrelSliceEnumList = getAllBarrelSliceEnumList(subtype);
    for(SliceNameEnum barrelSliceNameEnum : barrelSliceEnumList)
    {
        for (JointInspectionData jointInspectionData : region.getInspectableJointInspectionDataList(subtype))
        {
          // Measure the size of the pin.
          RegionOfInterest barrelRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

          ReconstructedSlice slice = images.getReconstructedSliceIgnoreNullImages(barrelSliceNameEnum);
          if(slice == null)
            continue;
          
          Image image = slice.getOrthogonalImage();

          RegionOfInterest innerGraylevelRegion = getPressfitGraylevelMeasurementRegion(pinTemplateImage,
                                                                                        expectedPinDiameterInPixels.getValue(),
                                                                                        image,
                                                                                        barrelRegion,
                                                                                        borderPixels);

          innerGraylevelRegion = RegionOfInterest.createRegionFromIntersection(innerGraylevelRegion, barrelRegion);

          double correctedGrayLevelMeasurement = AlgorithmUtil.measureCorrectedGraylevelFromPercentile(
                image,
                jointInspectionData,
                innerGraylevelRegion,
                barrelRegion,
                .95f,
                slice.getSliceNameEnum(),
                images.getReconstructionRegion(),
                this, false);

          LearnedJointMeasurement correctedGrayLevelLearnedMeasurement =
                new LearnedJointMeasurement(jointInspectionData.getPad(),
                                            slice.getSliceNameEnum(),
                                            MeasurementEnum.PRESSFIT_GRAYLEVEL,
                                            (float)correctedGrayLevelMeasurement,
                                            true,
                                            true);
          //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because not so applicable & not so direct.          
          subtype.addLearnedJointMeasurementData(correctedGrayLevelLearnedMeasurement);
        }
    }
    pinTemplateImage.decrementReferenceCount();
  }

  /**
   * This stage of learning takes data stored in the database and figures out appropriate settings for the Algorithm Settings.
   * No images are required at this point.
   *
   * @author Patrick Lacz
   */
  private void learnSettingsFromDatabase(Subtype subtype, boolean learnOnlyNominals) throws DatastoreException
  {
    Assert.expect(subtype != null);

    // Learn barrel slice, depending on number of barrel slice defined.
    List<SliceNameEnum> barrelSliceEnumList = getAllBarrelSliceEnumList(subtype);
    for(SliceNameEnum barrelSliceNameEnum : barrelSliceEnumList)
    {
        // do pressfit learning
        float barrelGraylevelValues[] = subtype.getLearnedJointMeasurementValues(barrelSliceNameEnum, MeasurementEnum.PRESSFIT_GRAYLEVEL, true, true);

        if (barrelGraylevelValues.length > 2)
        {
          //Lim, Lay Ngor - XCR-2027 Exclude Outlier
          float barrelGraylevelQuartiles[] = AlgorithmUtil.quartileRangesWithExcludeOutlierDecision(barrelGraylevelValues, false);

          Assert.expect(barrelGraylevelQuartiles.length == 5);
          final int MEDIAN = 2;
          final int THIRD_QUARTILE = 3;

          if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_2))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_2_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_3))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_3_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_4))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_4_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_5))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_5_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_6))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_6_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_7))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_7_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_8))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_8_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_9))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_9_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_10))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_10_CORRECTED_GRAYLEVEL, barrelGraylevelQuartiles[MEDIAN]);
          }
          
          if (learnOnlyNominals == false)
          {
            // These are really insufficient algorithm settings. I hate setting them here, but since we already have the data...
            float insufficientSettingForBarrel = Math.max(3 * (barrelGraylevelQuartiles[THIRD_QUARTILE] - barrelGraylevelQuartiles[MEDIAN]), 10.f);

            if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_2))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_2, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_3))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_3, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_4))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_4, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_5))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_5, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_6))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_6, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_7))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_7, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_8))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_8, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_9))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_9, insufficientSettingForBarrel);
            }
            else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_10))
            {
                subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_10, insufficientSettingForBarrel);
            }
          }
        }

        checkForLimitedData(subtype, barrelSliceNameEnum);
    }
  }


  /**
   * This method verifies that learning is done on enough data.  If not, a limited data warning is displayed to the
   * user.
   *
   * @author Sunit Bhalla
   * @author Patrick Lacz
   */
  private void checkForLimitedData(Subtype subtype, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    final int MINIMUM_DATAPOINTS_NEEDED = 64;
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    /** optimize PWL : Optimize computing the number of measurements and put it in a utility method */
    float[] existingMeasurementsInDatabase = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                                      MeasurementEnum.PRESSFIT_GRAYLEVEL,
                                                                                      true,
                                                                                      true);

    int numberOfMeasurements = existingMeasurementsInDatabase.length;
    if (numberOfMeasurements < MINIMUM_DATAPOINTS_NEEDED)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfMeasurements);
    }
  }


  /**
   * @author Patrick Lacz
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Locator.setLearnedSettingsToDefaults(subtype);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL);
  }


  /**
   * @author Seng-Yew Lim
   * This method updates the nominals.
   * Code exactly the same as learnAlgorithmSettings(...) except learnOnlyNominals == true.
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    // Clear all existing measurement data
    subtype.deleteLearnedMeasurementData();

    SliceNameEnum padSliceName = subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // for each image set
    ManagedOfflineImageSetIterator imageSetIterator = typicalBoardImages.iterator();
    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imageSetIterator.getNext()) != null)
    {
      ReconstructionRegion region = reconstructedImages.getReconstructionRegion();

      // find the location of the joints
      List<JointInspectionData> jointInspectionDataList = region.getInspectableJointInspectionDataList(subtype);
      Locator.locateJoints(reconstructedImages, padSliceName, jointInspectionDataList, this, true);

      recordPressfitLearningMeasurements(subtype, reconstructedImages);
      imageSetIterator.finishedWithCurrentRegion();
      
      // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
      if (jointInspectionDataList != null)
      {
        for(JointInspectionData jointInspectionData : jointInspectionDataList)
        {
          jointInspectionData.getJointInspectionResult().clearMeasurements();
          jointInspectionData.clearJointInspectionResult();
        }
        
        jointInspectionDataList.clear();
        jointInspectionDataList = null;
      }
    }

    // done with getting data from the image sets
    boolean learnOnlyNominals = true;
    learnSettingsFromDatabase(subtype, learnOnlyNominals);
  }

  /**
   * @author Cheah Lee Herng 
   */
  static public List<SliceNameEnum> getAllBarrelSliceEnumList(Subtype subtype)
  {
    Assert.expect(subtype != null);
      
    List<SliceNameEnum> barrelSliceEnumList = new ArrayList();
    int numberOfBarrelSlices = getNumberOfSlice(subtype);

    if (numberOfBarrelSlices >= 1)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL);
    }
    if (numberOfBarrelSlices >= 2)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_2);
    }
    if (numberOfBarrelSlices >= 3)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_3);
    }
    if (numberOfBarrelSlices >= 4)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_4);
    }
    if (numberOfBarrelSlices >= 5)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_5);
    }
    if (numberOfBarrelSlices >= 6)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_6);
    }
    if (numberOfBarrelSlices >= 7)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_7);
    }
    if (numberOfBarrelSlices >= 8)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_8);
    }
    if (numberOfBarrelSlices >= 9)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_9);
    }
    if (numberOfBarrelSlices >= 10)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_10);
    }
    return barrelSliceEnumList;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel1SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }

    /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel2SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_2_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_2,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
  
    /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel3SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_3_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_3,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
  
    /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel4SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_4_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_4,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
  
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel5SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_5_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_5,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel6SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_6_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_6,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
  
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel7SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_7_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_7,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel8SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_8_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_8,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
  
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel9SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_9_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
  
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   */
  public int createBarrel10SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting nominalCorrectedGraylevel = new AlgorithmSetting(
      AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_10_CORRECTED_GRAYLEVEL,
      displayOrder++,
      150.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // description URL key
      "HTML_DETAILED_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // desailed description URL key
      "IMG_DESC_PRESSFIT_(NOMINAL_BARREL_CORRECTED_GRAYLEVEL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          nominalCorrectedGraylevel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_10,
                                                          MeasurementEnum.PRESSFIT_GRAYLEVEL);
    addAlgorithmSetting(nominalCorrectedGraylevel);
    
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting throughHoleBarrelSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10, // setting enum
      displayOrder++, // display order,
      75.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_PRESSFIT_(THROUGHHOLE_BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(throughHoleBarrelSliceheight);

    /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
      MathUtil.convertMilsToMillimeters(500.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    return displayOrder;
  }
  
}
