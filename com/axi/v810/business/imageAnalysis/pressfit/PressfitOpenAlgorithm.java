package com.axi.v810.business.imageAnalysis.pressfit;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * @author Patrick Lacz
 */
public class PressfitOpenAlgorithm extends Algorithm
{
  /**
   * @author Patrick Lacz
   */
  public PressfitOpenAlgorithm(InspectionFamily inspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.PRESSFIT);
    Assert.expect(inspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    displayOrder = createBarel1SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel2SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel3SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel4SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel5SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel6SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel7SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel8SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel9SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    displayOrder = createBarel10SlicesOpenSettings(displayOrder, currentVersion, inspectionFamily);
    
    
    addMeasurementEnums();
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
  }

  /**
   * classifyJoints
   *
   * @author Patrick Lacz
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null );
    Assert.expect(jointInspectionDataObjects.size() > 0);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    SliceNameEnum barrelSliceName = SliceNameEnum.THROUGHHOLE_BARREL;
    //Chin-seong,kee
    SliceNameEnum barrelSliceName2 = SliceNameEnum.THROUGHHOLE_BARREL_2;
    SliceNameEnum barrelSliceName3 = SliceNameEnum.THROUGHHOLE_BARREL_3;
    SliceNameEnum barrelSliceName4 = SliceNameEnum.THROUGHHOLE_BARREL_4;
    SliceNameEnum barrelSliceName5 = SliceNameEnum.THROUGHHOLE_BARREL_5;
    SliceNameEnum barrelSliceName6 = SliceNameEnum.THROUGHHOLE_BARREL_6;
    SliceNameEnum barrelSliceName7 = SliceNameEnum.THROUGHHOLE_BARREL_7;
    SliceNameEnum barrelSliceName8 = SliceNameEnum.THROUGHHOLE_BARREL_8;
    SliceNameEnum barrelSliceName9 = SliceNameEnum.THROUGHHOLE_BARREL_9;
    SliceNameEnum barrelSliceName10 = SliceNameEnum.THROUGHHOLE_BARREL_10;
    
    Collection<SliceNameEnum> reconstructedSlices = reconstructedImages.getSliceNames();

    int numberOfSlices = getNumberOfSlice(subtypeOfJoints);
    // inspect only the barrel slice
    Assert.expect(reconstructedSlices.contains(barrelSliceName));
    classifySlice(reconstructedImages, barrelSliceName, jointInspectionDataObjects,
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL),
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL),
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL));

    // inspect only the barrel slice
    if(reconstructedSlices.contains(barrelSliceName2) &&  numberOfSlices >= 2)
    {
      classifySlice(reconstructedImages, barrelSliceName2, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_2_CORRECTED_GRAYLEVEL));
    }

    // inspect only the barrel slice
    if(reconstructedSlices.contains(barrelSliceName3) &&  numberOfSlices >= 3)
    {
      classifySlice(reconstructedImages, barrelSliceName3, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_3_CORRECTED_GRAYLEVEL));
    }

    // inspect only the barrel slice
    if(reconstructedSlices.contains(barrelSliceName4) && numberOfSlices >= 4)
    {
      classifySlice(reconstructedImages, barrelSliceName4, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_4_CORRECTED_GRAYLEVEL));
    }
    
    // inspect only the barrel slice 5
    if(reconstructedSlices.contains(barrelSliceName5) && numberOfSlices >= 5)
    {
      classifySlice(reconstructedImages, barrelSliceName5, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_5_CORRECTED_GRAYLEVEL));
    }
    
    // inspect only the barrel slice 6
    if(reconstructedSlices.contains(barrelSliceName6) && numberOfSlices >= 6)
    {
      classifySlice(reconstructedImages, barrelSliceName6, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_6_CORRECTED_GRAYLEVEL));
    }
    
    // inspect only the barrel slice 7
    if(reconstructedSlices.contains(barrelSliceName7) && numberOfSlices >= 7)
    {
      classifySlice(reconstructedImages, barrelSliceName7, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_7_CORRECTED_GRAYLEVEL));
    }
    
    // inspect only the barrel slice
    if(reconstructedSlices.contains(barrelSliceName8) && numberOfSlices >= 8)
    {
      classifySlice(reconstructedImages, barrelSliceName8, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_8_CORRECTED_GRAYLEVEL));
    }
    
        // inspect only the barrel slice
    if(reconstructedSlices.contains(barrelSliceName9) && numberOfSlices >= 9)
    {
      classifySlice(reconstructedImages, barrelSliceName9, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_9_CORRECTED_GRAYLEVEL));
    }
    
        // inspect only the barrel slice
    if(reconstructedSlices.contains(barrelSliceName10) && numberOfSlices >= 10)
    {
      classifySlice(reconstructedImages, barrelSliceName10, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.PRESSFIT_NOMINAL_BARREL_10_CORRECTED_GRAYLEVEL));
    }

  }

  /**
   * @author Patrick Lacz
   */
  private void classifySlice(ReconstructedImages reconstructedImages,
                             SliceNameEnum sliceNameEnum,
                             List<JointInspectionData> jointInspectionDataObjects,
                             float maxDifferenceFromNominal,
                             float maxDifferenceFromRegion,
                             float nominalGraylevel)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);

    float sumOfGraylevels = 0.f;

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    Assert.expect(jointInspectionDataObjects.isEmpty() == false);
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    // compute the average graylevel for this region and slice.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // do not post joint diagnostics here -- this is computing the region average.

      // retrieve the graylevel measurement that was taken in the Measurement algorithm.
      JointMeasurement graylevelMeasurement = PressfitMeasurementAlgorithm.getCorrectedGraylevelMeasurement(
          jointInspectionData, sliceNameEnum);
      float correctedGraylevel = graylevelMeasurement.getValue();

      // keep a sum so that we can create an average for the inspection region for the next step.
      sumOfGraylevels += correctedGraylevel;
    }
    float averageGraylevel = sumOfGraylevels / jointInspectionDataObjects.size();


    // Now classify any joints that fail the difference from region average.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // notify the user of the beginning of a new joint
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                reconstructedImages.getReconstructionRegion(),
                                                reconstructedSlice,
                                                false);

      JointMeasurement graylevelMeasurement = PressfitMeasurementAlgorithm.getCorrectedGraylevelMeasurement(jointInspectionData, sliceNameEnum);

      // test against the nominal graylevel
      boolean passedNominal = classifyByDifferenceFromNominal(reconstructedImages, nominalGraylevel, maxDifferenceFromNominal, jointInspectionData, graylevelMeasurement);

      // test against the average for the region
      boolean passedRegion = classifyByDifferenceFromRegionAverage(reconstructedImages, maxDifferenceFromRegion, averageGraylevel, jointInspectionData, graylevelMeasurement);

      // Show the pass/fail diagnostic
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              reconstructedImages.getReconstructionRegion(),
                                                              sliceNameEnum,
                                                              passedNominal && passedRegion);
    }
  }

  /**
   * @author Patrick Lacz
   */
  private boolean classifyByDifferenceFromRegionAverage(ReconstructedImages reconstructedImages,
      float maxDifferenceFromRegion,
      float averageGraylevel,
      JointInspectionData jointInspectionData,
      JointMeasurement graylevelMeasurement)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(graylevelMeasurement != null);

    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();
    float correctedGraylevel = graylevelMeasurement.getValue();

    // compute the difference from the region average. (the second way we can indict the joint)
    float differenceFromRegionAverage = correctedGraylevel - averageGraylevel;

    JointMeasurement differenceFromAverageMeasurement = new JointMeasurement(this,
        MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION,
        MeasurementUnitsEnum.GRAYLEVEL,
        jointInspectionData.getPad(),
        sliceName,
        differenceFromRegionAverage);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    differenceFromAverageMeasurement);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(differenceFromAverageMeasurement);

    // if difference from region Average > Maximum Allowable for this slice:
    if (differenceFromRegionAverage > maxDifferenceFromRegion)
    {
      // reject as missing
      JointIndictment indictment = new JointIndictment(IndictmentEnum.MISSING, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(differenceFromAverageMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      differenceFromAverageMeasurement,
                                                      maxDifferenceFromRegion);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }

  /**
   * @author Patrick Lacz
   */
  private boolean classifyByDifferenceFromNominal(ReconstructedImages reconstructedImages,
                                                  float nominalGraylevel,
                                                  float maxDifferenceFromNominal,
                                                  JointInspectionData jointInspectionData,
                                                  JointMeasurement graylevelMeasurement)
  {
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();

    float correctedGraylevel = graylevelMeasurement.getValue();
    float differenceFromNominal = correctedGraylevel - nominalGraylevel;
    JointMeasurement differenceFromNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL,
        MeasurementUnitsEnum.GRAYLEVEL,
        jointInspectionData.getPad(),
        sliceName,
        differenceFromNominal);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    differenceFromNominalMeasurement);

    jointInspectionResult.addMeasurement(differenceFromNominalMeasurement);

    // if difference from nominal > Maximum Allowable for this slice
    if (differenceFromNominal > maxDifferenceFromNominal)
    {
      // reject as missing
      JointIndictment indictment = new JointIndictment(IndictmentEnum.MISSING, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(differenceFromNominalMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      differenceFromNominalMeasurement,
                                                      maxDifferenceFromNominal);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel1SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel2SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_2,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_2,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_2,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_2,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel3SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_3,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_3,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_3,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_3,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel4SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_4,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_4,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_4,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_4,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel5SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_5,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_5,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_5,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_5,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel6SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_6,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_6,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_6,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_6,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel7SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_7,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_7,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_7,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_7,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel8SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_8,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_8,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_8,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_8,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel9SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_9,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_9,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
 /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   */
  public int createBarel10SlicesOpenSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily)
  {
    AlgorithmSetting maximumFromNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_10,
        displayOrder++,
        50.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromNominal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_10,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumFromNominal);
    
    AlgorithmSetting maximumFromRegion = new AlgorithmSetting(
        AlgorithmSettingEnum.PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_10,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_PRESSFIT_(OPEN_MAXIMUM_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.PRESSFIT,
                                                          maximumFromRegion,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_10,
                                                          MeasurementEnum.PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(maximumFromRegion);
    
    return displayOrder;
  }
  
}
