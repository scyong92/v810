package com.axi.v810.business.imageAnalysis.exposedPad;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Exposed Pad insufficient algorithm (placeholder in case Insufficient is ever needed)
 *
 * @author George Booth
 */
public class ExposedPadInsufficientAlgorithm extends Algorithm
{

  /**
   * @author George Booth
   */
  public ExposedPadInsufficientAlgorithm()
  {
    super(AlgorithmEnum.INSUFFICIENT, InspectionFamilyEnum.EXPOSED_PAD);

    // Add the algorithm settings.
    addAlgorithmSettings();

    addMeasurementEnums();
  }

  /**
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
//    _jointMeasurementEnums.add(MeasurementEnum.???);
  }

  /**
   * Adds the algorithm settings for the Exposed Pad Insufficient algorithm.
   *
   * @author George Booth
   */
  private void addAlgorithmSettings()
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Standard

    // Maximum Void Percent
    AlgorithmSetting maximumVoidPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_INSUFFICIENT_MAX_VOID_PERCENT,
        displayOrder++,
        30.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_EXPOSED_PAD_INSUFFICIENT_(MAX_VOID_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_INSUFFICIENT_(MAX_VOID_PERCENT)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_INSUFFICIENT_(MAX_VOID_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumVoidPercentSetting);
  }

  /**
   * Runs 'Insufficient' on all the given joints
   *
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Ensure the all the JointInspectionData objects have the same subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      Assert.expect(jointInspectionData.getSubtype() == subtype);
    }

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Exposed Pad only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the reconstructed pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Check each joint for insufficient solder.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                inspectionRegion,
                                                reconstructedPadSlice,
                                                false);

      BooleanRef jointPassed = new BooleanRef(true);

      // Run the Exposed Pad Insuffient classification.
      classifyJoint(subtype,
                    padSlice,
                    jointInspectionData,
                    jointPassed);


      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }

  /**
   * Does the Exposed Pad Insufficient classification on the specified joint.
   *
   * @author George Booth
   */
  protected void classifyJoint(Subtype subtype,
                               SliceNameEnum sliceNameEnum,
                               JointInspectionData jointInspectionData,
                               BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);
  }

}
