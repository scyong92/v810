package com.axi.v810.business.imageAnalysis.exposedPad;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

/**
 * <p>Title: GapResult</p>
 *
 * <p>Description: Utility class to store gap measurement information</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class GapInformation
{
  private String _gapName;
  private int _startIndex;
  private int _centerIndex;
  private int _endIndex;
  private GapMeasurementEnumGroup _gapMeasurementEnumGroup;
  private LocalizedString _gapProfileLabel;

  private static final int _UNUSED_BIN = -1;

  /**
   * @author George Booth
   */
  public GapInformation(String gapName, int start, int center, int end,
                        GapMeasurementEnumGroup gapMeasurementEnumGroup,
                        LocalizedString gapProfileLabel)
  {
    Assert.expect(gapName != null);
    Assert.expect(start >= 0);
    Assert.expect(start <= center && center <= end);
    Assert.expect(end > start);
    Assert.expect(gapMeasurementEnumGroup != null);
    Assert.expect(gapProfileLabel != null);

    _gapName = gapName;
    _startIndex = start;
    _centerIndex = center;
    _endIndex = end;
    _gapMeasurementEnumGroup = gapMeasurementEnumGroup;
    _gapProfileLabel = gapProfileLabel;
  }

  /**
   * @author George Booth
   */
  public String getGapName()
  {
    return _gapName;
  }

  /**
   * @author George Booth
   */
  public int getStartIndex()
  {
    return _startIndex;
  }

  /**
   * @author George Booth
   */
  public int getCenterIndex()
  {
    return _centerIndex;
  }

  /**
   * @author George Booth
   */
  public int getEndIndex()
  {
    return _endIndex;
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getInnerGapMeasurementEnum()
  {
    Assert.expect(_gapMeasurementEnumGroup != null);

    return _gapMeasurementEnumGroup.getInnerGapMeasurementEnum();
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getOuterGapMeasurementEnum()
  {
    Assert.expect(_gapMeasurementEnumGroup != null);

    return _gapMeasurementEnumGroup.getOuterGapMeasurementEnum();
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getGapRatioMeasurementEnum()
  {
    Assert.expect(_gapMeasurementEnumGroup != null);

    return _gapMeasurementEnumGroup.getGapRatioMeasurementEnum();
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getGapThicknessMeasurementEnum()
  {
    Assert.expect(_gapMeasurementEnumGroup != null);

    return _gapMeasurementEnumGroup.getGapThicknessMeasurementEnum();
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getFilledGapThicknessMeasurementEnum()
  {
    Assert.expect(_gapMeasurementEnumGroup != null);

    return _gapMeasurementEnumGroup.getFilledGapThicknessMeasurementEnum();
  }

  /**
   * @author George Booth
   */
  public LocalizedString getGapProfileLabel()
  {
    return _gapProfileLabel;
  }


}
