package com.axi.v810.business.imageAnalysis.exposedPad;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import java.util.*;
import com.axi.v810.business.*;

/**
 * Test class for QuadFlatNoLead Measurement.
 *
 * @author George Booth
 */
public class Test_ExposedPadMeasurementAlgorithm extends AlgorithmUnitTest
{
  private static final String _TEST_PROJECT_NAME = "0027Parts_ExposedPad";
  private static final String _EXPOSEDPAD_SUBTYPE_NAME = "S020_exposedPad";
  private static final String _EXPOSEDPAD_REFDES = "U7";
  private static final String _EXPOSEDPAD_PAD_NAME = "21";

  /**
   * @author George Booth
   */
  private Test_ExposedPadMeasurementAlgorithm()
  {
    // Do nothing ...
  }

  /**
   * Main unit test entry point.
   *
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    try
    {
//      System.out.println("Load up the test project (" + _TEST_PROJECT_NAME + ")");
      Project project = loadProject(_TEST_PROJECT_NAME);

      // Run some tests against 'pathological' images (black, white, random shapes).
      // Pick an arbitarary pad to test.
      Panel panel = project.getPanel();
      Component component = panel.getBoards().get(0).getComponent(_EXPOSEDPAD_REFDES);
      Pad pad = component.getPad(_EXPOSEDPAD_PAD_NAME);

//      System.out.println("Run against a black image");

      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createAndSelectBlackImageSet(project, pad, "expPadTestBlackImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

//      System.out.println("Run against a white image");
      imageSetData = createAndSelectWhiteImageSet(project, pad, "expPadTestWhiteImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

//      System.out.println("Run against a pseudo-random image");
      createAndSelectPseudoRandomImageSet(project, pad, "expPadTestPseudoRandomImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author George Booth
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ExposedPadMeasurementAlgorithm());
  }
}
