package com.axi.v810.business.imageAnalysis.exposedPad;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;

public class Test_ExposedPadLearning extends AlgorithmUnitTest
{
  private List<AlgorithmSettingEnum> _learnedSettings = new LinkedList<AlgorithmSettingEnum>();
  private Map<AlgorithmSettingEnum, Float> _learnedSettingToExpectedLearnedValueMap =
      new HashMap<AlgorithmSettingEnum, Float>();

  private static final String _TEST_PROJECT_NAME = "0027Parts_ExposedPad";
  private static final String _EXPOSED_PAD_SUBTYPE_NAME = "S020_exposedPad_ExposedPad";

  /**
   * @author George Booth
   */
  private Test_ExposedPadLearning()
  {
    // Do nothing...
  }

  /**
   * Initializes the "learned settings" data structures for the test.
   *
   * @author George Booth
   */
  private void initializeLearnedSettingsDataStructures()
  {
    Assert.expect(_learnedSettings != null);
    Assert.expect(_learnedSettingToExpectedLearnedValueMap != null);

    _learnedSettings.clear();
    InspectionFamily exposedPadInspectionFamily = InspectionFamily.getInstance(InspectionFamilyEnum.EXPOSED_PAD);
    for (Algorithm algorithm : exposedPadInspectionFamily.getAlgorithms())
    {
      _learnedSettings.addAll(algorithm.getLearnedAlgorithmSettingEnums());
    }

    _learnedSettingToExpectedLearnedValueMap.clear();
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ALONG, 25.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG, 50.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS, 50.0f);
//    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_ALONG_METHOD, Nan);
//    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_ACROSS_METHOD, Nan);
//    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS, Nan);
//    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS, Nan);
//    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ALONG_MEASUREMENTS, Nan);
//    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ACROSS_MEASUREMENTS, Nan);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ACROSS, 10.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ALONG, 15.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_CLIFF_SLOPE, 0.1f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_FALLOFF_SLOPE, 0.03f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS, 0.1524f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ACROSS, 15.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS, 0.508f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG, 0.508f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ALONG, 0.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ACROSS, 0.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_SMOOTHING_LENGTH, 6.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG, 60.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS, 60.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ALONG, 0.508f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ACROSS, 0.635f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ALONG, 1.27f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ACROSS, 1.27f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ALONG, 0.0508f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ACROSS, 0.0508f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_ALLOWED_GAP_FAILURES, 0.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_VOID_PERCENT, 30.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS, 0.0889f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE, 8.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MINIMUM_VOID_AREA, 0.0064516f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_LEVEL_OF_NOISE_REDUCTION, 2.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS, 0.635f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG, 0.635f);
  }

  /**
   * Creates a black image set for the specified project and subtype and runs learning against it.
   *
   * @author George Booth
   */
  private void testLearningOnBlackImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the black image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData blackImageSetData = createImageSetAndSetProgramFilters(project,
                                                                        subtype,
                                                                        "expPadTestLearningOnBlackImagesImageSet",
                                                                        POPULATED_BOARD);

    generateBlackImageSet(project, blackImageSetData);

    // Run subtype learning.
    imageSetDataList.add(blackImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a white image set for the specified project and subtype and runs learning against it.
   *
   * @author George Booth
   */
  private void testLearningOnWhiteImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the white image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData whiteImageSetData = createImageSetAndSetProgramFilters(project,
                                                                        subtype,
                                                                        "expPadTestLearningOnWhiteImagesImageSet",
                                                                        POPULATED_BOARD);
    generateWhiteImageSet(project, whiteImageSetData);

    // Run subtype learning.
    imageSetDataList.add(whiteImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a pseudo-random image set for the specified project and subtype and runs learning against it.
   *
   * @author George Booth
   */
  private void testLearningOnPseudoRandomImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the pseudo random image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData pseudoRandomImageSetData = createImageSetAndSetProgramFilters(project,
                                                                               subtype,
                                                                               "expPadTestLearningOnPseudoRandomImagesImageSet",
                                                                               POPULATED_BOARD);
    generateRandomImageSet(project, pseudoRandomImageSetData);

    // Run subtype learning.
    imageSetDataList.add(pseudoRandomImageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Main test entry point for Exposed Pad Learning.
   *
   * @author George Booth
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

//    System.out.println("Initializing");
    // Initialize the learned settings data structures.
    initializeLearnedSettingsDataStructures();

    // Load up the test project (0027Parts_ExposedPad).
//    System.out.println("Loading " + _TEST_PROJECT_NAME);
    Project project = loadProject(_TEST_PROJECT_NAME);

    // Lookup our test Subtype.
    Panel panel = project.getPanel();
    Subtype subtype = panel.getSubtype(_EXPOSED_PAD_SUBTYPE_NAME);

    try
    {
      // Delete any pre-existing learned data.
      deleteLearnedData(subtype);

      // Check that the learned settings are at their default values.
      verifyAllSettingsAreAtDefault(subtype, _learnedSettings);

      // Learn one of the QFN subtypes.
//      System.out.println("Learning with " + ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE);
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                     POPULATED_BOARD);
      imageSetDataList.add(imageSetData);
      learnSubtypeUnitTest(project, subtype, imageSetDataList, _DONT_LEARN_SUBTYPES, _DONT_LEARN_SHORT, _LEARN_EXPECTED_IMAGES);

      // Verify that the learned values match what we expect.
      checkLearnedValues(subtype, _learnedSettingToExpectedLearnedValueMap);

      // Check the number of datapoints.
//      System.out.println("Check number of datapoints");
      SliceNameEnum padSlice = SliceNameEnum.PAD;
      final int EXPECTED_NUMBER_OF_DATA_POINTS = 0;
      int actualNumberOfDataPoints = getNumberOfMeasurements(subtype, padSlice, MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_1);
      Expect.expect(actualNumberOfDataPoints == EXPECTED_NUMBER_OF_DATA_POINTS,
                    "Actual # data points: " + actualNumberOfDataPoints);

      // Test learning on black images.
//      System.out.println("Learning with black images");
      testLearningOnBlackImages(project, subtype);

      // Test learning on white images.
//      System.out.println("Learning with white images");
      testLearningOnWhiteImages(project, subtype);

      // Test learning on pseudo-random images.
//      System.out.println("Learning with random images");
      testLearningOnPseudoRandomImages(project, subtype);

      // Make sure that we can run learning on an unpopulated board.
//      System.out.println("Learning with unpopulated board");
//      testLearningOnlyUnloadedBoards(project, subtype, _learnedSettings);

      // Delete the learned data generated during this test.
      deleteLearnedData(subtype);
    }
    catch (XrayTesterException xtex)
    {
      xtex.printStackTrace();
    }
  }

  /**
   * @author George Booth
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ExposedPadLearning());
  }
}
