package com.axi.v810.business.imageAnalysis.exposedPad;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

/**
 * <p>Title: GapResult</p>
 *
 * <p>Description: Utility class to store gap measurement enums</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class GapMeasurementEnumGroup
{
  private MeasurementEnum _innerGapMeasurementEnum;
  private MeasurementEnum _outerGapMeasurementEnum;
  private MeasurementEnum _gapRatioMeasurementEnum;
  private MeasurementEnum _gapThicknessMeasurementEnum;
  private MeasurementEnum _filledGapThicknessMeasurementEnum;

  public GapMeasurementEnumGroup(MeasurementEnum innerGapMeasurementEnum,
                                 MeasurementEnum outerGapMeasurementEnum,
                                 MeasurementEnum gapRatioMeasurementEnum,
                                 MeasurementEnum gapThicknessMeasurementEnum,
                                 MeasurementEnum filledGapThicknessMeasurementEnum)
  {
    Assert.expect(innerGapMeasurementEnum != null);
    Assert.expect(outerGapMeasurementEnum != null);
    Assert.expect(gapRatioMeasurementEnum != null);
    Assert.expect(gapThicknessMeasurementEnum != null);
    Assert.expect(filledGapThicknessMeasurementEnum != null);

    _innerGapMeasurementEnum = innerGapMeasurementEnum;
    _outerGapMeasurementEnum = outerGapMeasurementEnum;
    _gapRatioMeasurementEnum = gapRatioMeasurementEnum;
    _gapThicknessMeasurementEnum = gapThicknessMeasurementEnum;
    _filledGapThicknessMeasurementEnum = filledGapThicknessMeasurementEnum;
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getInnerGapMeasurementEnum()
  {
    Assert.expect(_innerGapMeasurementEnum != null);

    return _innerGapMeasurementEnum;
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getOuterGapMeasurementEnum()
  {
    Assert.expect(_outerGapMeasurementEnum != null);

    return _outerGapMeasurementEnum;
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getGapRatioMeasurementEnum()
  {
    Assert.expect(_gapRatioMeasurementEnum != null);

    return _gapRatioMeasurementEnum;
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getGapThicknessMeasurementEnum()
  {
    Assert.expect(_gapThicknessMeasurementEnum != null);

    return _gapThicknessMeasurementEnum;
  }

  /**
   * @author George Booth
   */
  public MeasurementEnum getFilledGapThicknessMeasurementEnum()
  {
    Assert.expect(_filledGapThicknessMeasurementEnum != null);

    return _filledGapThicknessMeasurementEnum;
  }

}
