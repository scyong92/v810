package com.axi.v810.business.imageAnalysis.exposedPad;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

/**
 * <p>Title: GapResult</p>
 *
 * <p>Description: Utility class to store gap joint measurements</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class GapJointMeasurementGroup
{
  private JointMeasurement _filledGapThicknessJointMeasurement;
  private JointMeasurement _innerGapJointMeasurement;
  private JointMeasurement _outerGapJointMeasurement;
  private JointMeasurement _gapRatioJointMeasurement;
  private JointMeasurement _gapThicknessJointMeasurement;

  /**
   * @author George Booth
   */
  public GapJointMeasurementGroup()
  {
    // do nothing
  }

  /**
   * @author George Booth
   */
  public GapJointMeasurementGroup(JointMeasurement filledGapThicknessJointMeasurement,
                                  JointMeasurement innerGapJointMeasurement,
                                  JointMeasurement outerGapJointMeasurement,
                                  JointMeasurement gapRatioJointMeasurement,
                                  JointMeasurement gapThicknessJointMeasurement)
  {
    Assert.expect(filledGapThicknessJointMeasurement != null);
    Assert.expect(innerGapJointMeasurement != null);
    Assert.expect(outerGapJointMeasurement != null);
    Assert.expect(gapRatioJointMeasurement != null);
    Assert.expect(gapThicknessJointMeasurement != null);

    _filledGapThicknessJointMeasurement = filledGapThicknessJointMeasurement;
    _innerGapJointMeasurement = innerGapJointMeasurement;
    _outerGapJointMeasurement = outerGapJointMeasurement;
    _gapRatioJointMeasurement = gapRatioJointMeasurement;
    _gapThicknessJointMeasurement = gapThicknessJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public void setFilledGapThicknessJointMeasurement(JointMeasurement filledGapThicknessJointMeasurement)
  {
    Assert.expect(filledGapThicknessJointMeasurement != null);

    _filledGapThicknessJointMeasurement = filledGapThicknessJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public JointMeasurement getFilledGapThicknessJointMeasurement()
  {
    Assert.expect(_filledGapThicknessJointMeasurement != null);

    return _filledGapThicknessJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public void setInnerGapJointMeasurement(JointMeasurement innerGapJointMeasurement)
  {
    Assert.expect(innerGapJointMeasurement != null);

    _innerGapJointMeasurement = innerGapJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public JointMeasurement getInnerGapJointMeasurement()
  {
    Assert.expect(_innerGapJointMeasurement != null);

    return _innerGapJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public void setOuterGapJointMeasurement(JointMeasurement outerGapJointMeasurement)
  {
    Assert.expect(outerGapJointMeasurement != null);

    _outerGapJointMeasurement = outerGapJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public JointMeasurement getOuterGapJointMeasurement()
  {
    Assert.expect(_outerGapJointMeasurement != null);

    return _outerGapJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public void setGapRatioJointMeasurement(JointMeasurement gapRatioJointMeasurement)
  {
    Assert.expect(gapRatioJointMeasurement != null);

    _gapRatioJointMeasurement = gapRatioJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public JointMeasurement getGapRatioJointMeasurement()
  {
    Assert.expect(_gapRatioJointMeasurement != null);

    return _gapRatioJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public void setGapThicknessJointMeasurement(JointMeasurement gapThicknessJointMeasurement)
  {
    Assert.expect(gapThicknessJointMeasurement != null);

    _gapThicknessJointMeasurement = gapThicknessJointMeasurement;
  }

  /**
   * @author George Booth
   */
  public JointMeasurement getGapThicknessJointMeasurement()
  {
    Assert.expect(_gapThicknessJointMeasurement != null);

    return _gapThicknessJointMeasurement;
  }

}
