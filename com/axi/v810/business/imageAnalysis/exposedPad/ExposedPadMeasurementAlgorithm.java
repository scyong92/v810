package com.axi.v810.business.imageAnalysis.exposedPad;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.ImageSetData;

/**
 * Exposed Pad measurement algorithm
 *
 * Allows for measurement using 0..3 along and across profiles with up to 4 gaps in each.
 *
 *   Along gap names:
 *            North Gap 1     North Gap 2     North Gap 3     North Gap 4
 *            Middle Gap 1    Middle Gap 2    Middle Gap 3    Middle Gap 4
 *            South Gap 1     South Gap 2     South Gap  3    South Gap 4
 *
 *   Across Gap names:
 *            West Gap 4     Center Gap 4   East Gap 4
 *            West Gap 3     Center Gap 3   East Gap 3
 *            West Gap 2     Center Gap 2   East Gap 2
 *            West Gap 1     Center Gap 1   East Gap 1
 *
 * @author George Booth
 */
public class ExposedPadMeasurementAlgorithm extends Algorithm
{
  private boolean _devDebug = false;
  private String _me = "ExposedPadMeasurementAlgorithm: ";

  protected final int _MAX_NUMBER_OF_REGIONS_FOR_LEARNING = 100;

  private String _gapName = "";
  private final boolean USE_ALONG_PROFILE = true;
  private final boolean USE_ACROSS_PROFILE = false;

  // AlgorithmDiagnostics instance.
  private static AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();

//  private int _profileSmoothingLength = 6;

  private static final int _UNUSED_BIN = -1;

  private static final boolean _logAlgorithmProfileData =
      Config.getInstance().getBooleanValue(SoftwareConfigEnum.LOG_ALGORITHM_PROFILE_DATA);
  
  //private static final String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";

  private static List<GapMeasurementEnumGroup> _northGapMeasurementEnumList = new ArrayList<GapMeasurementEnumGroup>();
  private static List<GapMeasurementEnumGroup> _middleGapMeasurementEnumList = new ArrayList<GapMeasurementEnumGroup>();
  private static List<GapMeasurementEnumGroup> _southGapMeasurementEnumList = new ArrayList<GapMeasurementEnumGroup>();
  private static List<GapMeasurementEnumGroup> _westGapMeasurementEnumList = new ArrayList<GapMeasurementEnumGroup>();
  private static List<GapMeasurementEnumGroup> _centerGapMeasurementEnumList = new ArrayList<GapMeasurementEnumGroup>();
  private static List<GapMeasurementEnumGroup> _eastGapMeasurementEnumList = new ArrayList<GapMeasurementEnumGroup>();

  /**
   * @author George Booth
   */
  public ExposedPadMeasurementAlgorithm()
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.EXPOSED_PAD);

    // Add the algorithm settings.
    addAlgorithmSettings();

    createMeasurementEnumLists();

    addMeasurementEnums();

    _devDebug = Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);
  }

  /**
   * @author George Booth
   */
  private ExposedPadMeasurementAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.MEASUREMENT, inspectionFamilyEnum);
  }

  /**
   * There can be 0 to 6 gaps per edge to be measured.  Create some structures to hold the unique
   * measurement enums for all the gaps.
   *
   * @author George Booth
   */
  private void createMeasurementEnumLists()
  {
    _northGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_1));
    _northGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_2));
    _northGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_3));
    _northGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_4));

    _middleGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_1));
    _middleGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_2));
    _middleGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_3));
    _middleGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_4));

    _southGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_1));
    _southGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_2));
    _southGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_3));
    _southGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_4));

    _westGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_1));
    _westGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_2));
    _westGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_3));
    _westGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_4));

    _centerGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_1));
    _centerGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_2));
    _centerGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_3));
    _centerGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_4));

    _eastGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_1,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_1));
    _eastGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_2,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_2));
    _eastGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_3,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_3));
    _eastGapMeasurementEnumList.add(new GapMeasurementEnumGroup(
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_4,
        MeasurementEnum.EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_4));
  }

  /**
   * @author George Booth
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());
    
    _jointMeasurementEnums.add(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_JOINT_OFFSET_FROM_CAD);

    for (GapMeasurementEnumGroup gapMeassurementEnumGroup : _northGapMeasurementEnumList)
    {
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getInnerGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getOuterGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapRatioMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapThicknessMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getFilledGapThicknessMeasurementEnum());
    }

    for (GapMeasurementEnumGroup gapMeassurementEnumGroup : _middleGapMeasurementEnumList)
    {
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getInnerGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getOuterGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapRatioMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapThicknessMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getFilledGapThicknessMeasurementEnum());
    }

    for (GapMeasurementEnumGroup gapMeassurementEnumGroup : _southGapMeasurementEnumList)
    {
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getInnerGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getOuterGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapRatioMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapThicknessMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getFilledGapThicknessMeasurementEnum());
    }

    for (GapMeasurementEnumGroup gapMeassurementEnumGroup : _westGapMeasurementEnumList)
    {
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getInnerGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getOuterGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapRatioMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapThicknessMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getFilledGapThicknessMeasurementEnum());
    }

    for (GapMeasurementEnumGroup gapMeassurementEnumGroup : _centerGapMeasurementEnumList)
    {
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getInnerGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getOuterGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapRatioMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapThicknessMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getFilledGapThicknessMeasurementEnum());
    }

    for (GapMeasurementEnumGroup gapMeassurementEnumGroup : _eastGapMeasurementEnumList)
    {
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getInnerGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getOuterGapMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapRatioMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getGapThicknessMeasurementEnum());
      _jointMeasurementEnums.add(gapMeassurementEnumGroup.getFilledGapThicknessMeasurementEnum());
    }

    _jointMeasurementEnums.add(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE);
  }

  /**
   * Adds the algorithm settings for Exposed Pad Measurement.
   *
   * @author George Booth
   */
  private void addAlgorithmSettings()
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;


    // Add the shared locator settings.
    // CR30753 not validated.  Don't use CustomLocator settings.
    // Collection<AlgorithmSetting> locatorAlgorithmSettings = CustomLocator.createAlgorithmSettings(displayOrder, currentVersion);
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    // Standard thresholds

    // Number of Profile Across Measurements
    ArrayList<String> allowableNumberOfProfileAcrossMeasurementValues =
      new ArrayList<String>(Arrays.asList("None", "1", "2", "3"));
    AlgorithmSetting numberOfProfileAcrossMeasurementsSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS,
      displayOrder++,
      "None",
      allowableNumberOfProfileAcrossMeasurementValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_PROFILE_ACROSS_MEASUREMENTS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_PROFILE_ACROSS_MEASUREMENTS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_PROFILE_ACROSS_MEASUREMENTS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(numberOfProfileAcrossMeasurementsSetting);

    // Profile Location Across
    AlgorithmSetting profileLocationAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ACROSS,
      displayOrder++,
      25.0f, // default
      0.0f, // min
      50.0f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ACROSS,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_LOCATION_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_LOCATION_ACROSS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_LOCATION_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(profileLocationAcrossSetting);

    // Profile Width Across
    AlgorithmSetting profileWidthAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ACROSS,
      displayOrder++,
      25.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ACROSS,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_WIDTH_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_WIDTH_ACROSS)_KEY", // desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_WIDTH_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(profileWidthAcrossSetting);

    // Number of Gap Across Measurements
    ArrayList<String> allowableNumberOfGapAcrossMeasurementValues =
      new ArrayList<String>(Arrays.asList("1", "2", "3", "4"));
    AlgorithmSetting numberOfGapAcrossMeasurementsSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ACROSS_MEASUREMENTS,
      displayOrder++,
      "1",
      allowableNumberOfGapAcrossMeasurementValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_GAP_ACROSS_MEASUREMENTS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_GAP_ACROSS_MEASUREMENTS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_GAP_ACROSS_MEASUREMENTS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(numberOfGapAcrossMeasurementsSetting);

    // Background Technique Across
    ArrayList<String> allowableBackgroundTechniqueAcrossValues =
      new ArrayList<String>(Arrays.asList("Average", "Interior"));
    AlgorithmSetting backgroundTechniqueAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ACROSS,
      displayOrder++,
      "Average",
      allowableBackgroundTechniqueAcrossValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE_ACROSS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(backgroundTechniqueAcrossSetting);

    // Number of Profile Along Measurements
    ArrayList<String> allowableNumberOfProfileAlongMeasurementValues =
      new ArrayList<String>(Arrays.asList("None", "1", "2", "3"));
    AlgorithmSetting numberOfProfileAlongMeasurementsSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS,
      displayOrder++,
      "None",
      allowableNumberOfProfileAlongMeasurementValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_PROFILE_ALONG_MEASUREMENTS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_PROFILE_ALONG_MEASUREMENTS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_PROFILE_ALONG_MEASUREMENTS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(numberOfProfileAlongMeasurementsSetting);

    // Profile Location Along
    AlgorithmSetting profileLocationAlongSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ALONG,
      displayOrder++,
      25.0f, // default
      0.0f, // min
      50.0f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ALONG,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_LOCATION_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_LOCATION_ALONG)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_LOCATION_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(profileLocationAlongSetting);

    // Profile Width Along
    AlgorithmSetting profileWidthAlongSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ALONG,
      displayOrder++,
      25.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ACROSS,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_WIDTH_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_WIDTH_ALONG)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_WIDTH_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(profileWidthAlongSetting);

    // Number of Gap Along Measurements
    ArrayList<String> allowableNumberOfGapAlongMeasurementValues =
      new ArrayList<String>(Arrays.asList("1", "2", "3", "4"));
    AlgorithmSetting numberOfGapAlongMeasurementsSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ALONG_MEASUREMENTS,
      displayOrder++,
      "1",
      allowableNumberOfGapAlongMeasurementValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_GAP_ALONG_MEASUREMENTS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_GAP_ALONG_MEASUREMENTS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(NUMBER_OF_GAP_ALONG_MEASUREMENTS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(numberOfGapAlongMeasurementsSetting);

    // Background Technique Along
    ArrayList<String> allowableBackgroundTechniqueAlongValues =
      new ArrayList<String>(Arrays.asList("Average", "Interior"));
    AlgorithmSetting backgroundTechniqueAlongSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ALONG,
      displayOrder++,
      "Average",
      allowableBackgroundTechniqueAlongValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE_ALONG)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(backgroundTechniqueAlongSetting);

    // Additional

    // Background Technique
    ArrayList<String> allowableBackgroundTechniqueValues =
      new ArrayList<String>(Arrays.asList(
        "Standard",
        "Average Value Across",
        "Average Value Along",
        "Average Image Value",
        "Nominal Background Value"));
    AlgorithmSetting backgroundTechniqueSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE,
      displayOrder++,
      "Standard",
      allowableBackgroundTechniqueValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_TECHNIQUE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundTechniqueSetting);

    // Nominal Background Value
    AlgorithmSetting nominalBackgroundValueSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NOMINAL_BACKGROUND_VALUE,
      displayOrder++,
      90.0f, // default
      0.0f, // min
      255.0f, // max
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(NOMINAL_BACKGROUND_VALUE)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(NOMINAL_BACKGROUND_VALUE)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(NOMINAL_BACKGROUND_VALUE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(nominalBackgroundValueSetting);

    // Contrast Enhancement
    ArrayList<String> allowableContrastEnhancementValues =
      new ArrayList<String>(Arrays.asList(
        "On",
        "Off"));
    AlgorithmSetting contrastEnhancementSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CONTRAST_ENHANCEMENT,
      displayOrder++,
      "Off",
      allowableContrastEnhancementValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(CONTRAST_ENHANCEMENT)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(CONTRAST_ENHANCEMENT)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(CONTRAST_ENHANCEMENT)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(contrastEnhancementSetting);

    // Background Location Across
    AlgorithmSetting backgroundLocationAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS,
      displayOrder++,
      50.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_LOCATION_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_LOCATION_ACROSS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_LOCATION_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundLocationAcrossSetting);

    // Center Search Distance Across
    AlgorithmSetting centerSearchDistanceAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(20.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(CENTER_SEARCH_DISTANCE_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(CENTER_SEARCH_DISTANCE_ACROSS)_KEY", // desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(CENTER_SEARCH_DISTANCE_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(centerSearchDistanceAcrossSetting);

    // Gap Offset Across
    AlgorithmSetting gapOffsetAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ACROSS,
      displayOrder++,
      0.0f, // default
      -75.0f, // min
      75.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_OFFSET_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_OFFSET_ACROSS)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_OFFSET_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(gapOffsetAcrossSetting);

    // Background Location Along
    AlgorithmSetting backgroundLocationAlongSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG,
      displayOrder++,
      50.0f, // default
      0.0f, // min
      300.0f, // max
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_LOCATION_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_LOCATION_ALONG)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(BACKGROUND_LOCATION_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundLocationAlongSetting);

    // Center Search Distance Along
    AlgorithmSetting centerSearchDistanceAlongSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(20.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(CENTER_SEARCH_DISTANCE_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(CENTER_SEARCH_DISTANCE_ALONG)_KEY", // desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(CENTER_SEARCH_DISTANCE_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(centerSearchDistanceAlongSetting);

    // Gap Offset Along
    AlgorithmSetting gapOffsetAlongSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ALONG,
      displayOrder++,
      0.0f, // default
      -75.0f, // min
      75.0f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_OFFSET_ALONG)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_OFFSET_ALONG)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_OFFSET_ALONG)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(gapOffsetAlongSetting);

    // Maximum Expected Thickness
    AlgorithmSetting maximumExpectedThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(5.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(25.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(MAXIMUM_EXPECTED_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(MAXIMUM_EXPECTED_THICKNESS)_KEY", // desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(MAXIMUM_EXPECTED_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(maximumExpectedThicknessSetting);

    // Gap Cliff Slope
    AlgorithmSetting gapCliffSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_CLIFF_SLOPE,
      displayOrder++,
      0.1f, // default
      -10.0f, // min
      10.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_CLIFF_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_CLIFF_SLOPE)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_CLIFF_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(gapCliffSlopeSetting);

    // Gap Falloff Slope
    AlgorithmSetting gapFalloffSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_FALLOFF_SLOPE,
      displayOrder++,
      0.03f, // default
      -10.0f, // min
      10.0f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_FALLOFF_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_FALLOFF_SLOPE)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(GAP_FALLOFF_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(gapFalloffSlopeSetting);

    // Profile Smoothing Length
    AlgorithmSetting profileSmoothingLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_SMOOTHING_LENGTH,
        displayOrder++,
        6, // default
        1, // min
        12, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_SMOOTHING_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_SMOOTHING_LENGTH)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(PROFILE_SMOOTHING_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(profileSmoothingLengthSetting);

    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(40.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed description URL Key
        "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);
    
    // Added by Lee Herng (4 Mar 2011)
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -200.0f), // minimum value
        MathUtil.convertMilsToMillimeters(200.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // description URL Key
        "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed description URL Key
        "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion));
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is right before psh in the Slice Setup tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_EXPOSED_PAD_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_EXPOSED_PAD_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));

    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * Updates the learned nominals for the Exposed Pad Measurement algorithm.
   *
   * @author George Booth
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // We can't update nominals if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, 0);

      return;
    }

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    // Dump the existing measurement data from the database.
    subtype.deleteLearnedMeasurementData();

    // Exposed Pad only runs (and learns) on the Pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // learn the nominal background value if standard backgrounds aren't used
    if (isStandardBackgroundEnabled(subtype) == false)
    {
      learnNominalBackgroundValue(subtype,
                                  typicalBoardImages,
                                  unloadedBoardImages);
    }
  }

  /**
   * Learns the algorithm settings for Exposed Pad Measurement.
   *
   * @author George Booth
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // We can't learn if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, 0);

      return;
    }

    // Let Locator learn its stuff.
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);

    // learn the nominal background value if standard backgrounds aren't used
    if (isStandardBackgroundEnabled(subtype) == false)
    {
      learnNominalBackgroundValue(subtype,
                                  typicalBoardImages,
                                  unloadedBoardImages);
    }
  }

  private void learnNominalBackgroundValue(Subtype subtype,
                                           ManagedOfflineImageSet typicalBoardImages,
                                           ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    ManagedOfflineImageSet imageSetToUse = typicalBoardImages;
    if (imageSetToUse.getReconstructionRegions().isEmpty())
      imageSetToUse = unloadedBoardImages;

    // Exposed Pad only runs on the pad slice.
    SliceNameEnum sliceNameEnum = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    ManagedOfflineImageSetIterator imagesIterator = imageSetToUse.iterator();
    ReconstructedImages reconstructedImages;

    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ImageSetData currentImageSetData = imagesIterator.getCurrentImageSetData();
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

      // Get all the joints in the region which are of the applicable subtype.
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      // Run Locator.
      Locator.locateJoints(reconstructedImages, sliceNameEnum, jointInspectionDataObjects, this, true);

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        String jointName = jointInspectionData.getFullyQualifiedPadName();
        jointName = currentImageSetData.getUserDescription() + "_" + jointName.replace(' ', '_');

        Image rawInspectedImage = reconstructedSlice.getOrthogonalImage();

        // Enhance the contrast if needed
        if (isContrastEnhancementEnabled(subtype))
        {
          AlgorithmUtil.enhanceContrastByEqualizingHistogram(rawInspectedImage);
        }

        float averageBackgroundValue =
            ExposedPadMeasurementAlgorithm.getAverageBackgroundValue(rawInspectedImage, jointInspectionData);

        // use the average background value for the entire background
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because too complicated - involves image analysis.
        subtype.addLearnedJointMeasurementData(
           new LearnedJointMeasurement(jointInspectionData.getPad(),
                           sliceNameEnum,
                            MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE,
                            averageBackgroundValue,
                            true,
                            true));
        jointInspectionData.getJointInspectionResult().clearMeasurements();
        jointInspectionData.clearJointInspectionResult();
      }
      imagesIterator.finishedWithCurrentRegion();
      
      // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
      if (jointInspectionDataObjects != null)
      {
        for(JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          jointInspectionData.getJointInspectionResult().clearMeasurements();
          jointInspectionData.clearJointInspectionResult();
        }
        
        jointInspectionDataObjects.clear();
        jointInspectionDataObjects = null;
      }
    }
    // Update the "nominal background value" setting.
    updateNominalBackgroundValue(subtype, sliceNameEnum);
    // Added by Seng Yew on 22-Apr-2011
    // Need to be careful when call this. Have to make sure JointMeasurement & ComponentMeasurement instances created before this reset are not used anywhere.
    // JointInspectionResult::addMeasurement(...) & ComponentInspectionResult::addMeasurement(...) checks for duplicate _id when add.
    // After call this function, any new JointMeasurement & ComponentMeasurement instances created will start with zero again, and cause duplicate _id and cause crashes.
    subtype.getPanel().getProject().getTestProgram().startNewInspectionRun();  // make sure we clear out the measurements for the next run (WRONG USAGE PREVIOUSLY)
  }

  /**
   * Runs 'Measurement' on all the specified joints.
   *
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.size() > 0);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    // get the smoothing kernel length
    int profileSmoothingLength = getProfileSmoothingLength(subtype);

    // Exposed Pad only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Run Locator.
    // CR30753 not validated.  Don't use CustomLocator.
    // CustomLocator.customLocateJoints(reconstructedImages, padSlice, jointInspectionDataObjects, this);
    Locator.locateJoints(reconstructedImages, padSlice, jointInspectionDataObjects, this, true);

    // Get the applicable image for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);
    Image padSliceImage = reconstructedPadSlice.getOrthogonalImage();

    //Siew Yeng - XCR-2683 - add enhanced image
    if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
    {
      ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, padSlice);
    }
    
    // Enhance the contrast if needed
    if (isContrastEnhancementEnabled(subtype))
    {
      AlgorithmUtil.enhanceContrastByEqualizingHistogram(padSliceImage);
    }

    // clear out the locator graphics
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                jointInspectionData.getInspectionRegion(),
                                                reconstructedPadSlice,
                                                false);

      // Run the Exposed Pad measurements on this joint.
      classifyJoint(padSliceImage, padSlice, subtype, jointInspectionData, profileSmoothingLength);
    }
  }

  /**
   * Performs the joint classification (measurement) for Exposed Pad
   *
   * @author George Booth
   */
  private void classifyJoint(Image image, SliceNameEnum sliceNameEnum,
                               Subtype subtype, JointInspectionData jointInspectionData,
                               int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);

    String jointName = jointInspectionData.getPad().getComponentAndPadName();
    debug("----");
    debug("jointName = " + jointName);

//    saveImageAsPngFile(image, "c:/temp/!" + jointName + "measImage.png");

    // Post a diagnostic to reset the diagnostic infos but not invalidate them.
    _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                         subtype,
                                                         this);

    // Keep track of the measurement data we're going to post as diagnostics.
    Collection<JointMeasurement> measurementsToPost = new LinkedList<JointMeasurement>();

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Get the located joint ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the JointInspectionResult for this joint.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate the distance between CAD position and located position.
    RegionOfInterest cadPositionRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels();
    ImageCoordinate cadPositionCenterCoordinate = new ImageCoordinate(cadPositionRoi.getCenterX(),
                                                                      cadPositionRoi.getCenterY());
    ImageCoordinate locatedJointCenterCoordinate = new ImageCoordinate(locatedJointRoi.getCenterX(),
                                                                       locatedJointRoi.getCenterY());
    double jointOffsetFromCadInPixels = cadPositionCenterCoordinate.distance(locatedJointCenterCoordinate);
    float jointOffsetFromCadInMillis = (float)(jointOffsetFromCadInPixels * MILIMETER_PER_PIXEL);

    // Save the "offset from CAD" measurement.
//    JointMeasurement jointOffsetFromCadMeasurement =
//        new JointMeasurement(this,
//                             MeasurementEnum.EXPOSED_PAD_MEASUREMENT_JOINT_OFFSET_FROM_CAD,
//                             MeasurementUnitsEnum.MILLIMETERS,
//                             jointInspectionData.getPad(),
//                             sliceNameEnum,
//                             jointOffsetFromCadInMillis);
//    jointInspectionResult.addMeasurement(jointOffsetFromCadMeasurement);
//    measurementsToPost.add(jointOffsetFromCadMeasurement);

    //----------------------------------------------
    //  Measurements
    //----------------------------------------------

    // Post the initial profile values
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      Assert.expect(measurementsToPost != null);
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      jointInspectionData.getInspectionRegion(),
                                                      jointInspectionData,
                                                      measurementsToPost.toArray(new JointMeasurement[0]));
    }

    // Profile Along measurements

    if (isProfileAlongMeasurementEnabled(subtype))
    {
      // Get a non-standard along profile to use as a "background".  This is the pad profile without
      // the normal background subtraction techniques
      final float PAD_ALONG_PROFILE_WIDTH_FRACTION = 0.0f;
//         (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PAD_PROFILE_WIDTH) / 100.f;
      final float PAD_ALONG_PROFILE_LENGTH_FRACTION = 1.0f;

      Pair<RegionOfInterest, float[]> smoothedPadAlongProfileRoiAndThicknessProfile =
        measureSmoothedPadThicknessProfileWithoutProcessingBackground(image,
                                                                      sliceNameEnum,
                                                                      jointInspectionData,
                                                                      USE_ALONG_PROFILE,
                                                                      PAD_ALONG_PROFILE_WIDTH_FRACTION,
                                                                      PAD_ALONG_PROFILE_LENGTH_FRACTION,
                                                                      true);

      RegionOfInterest smoothedPadAlongProfileRoi = smoothedPadAlongProfileRoiAndThicknessProfile.getFirst();
      float[] smoothedPadAlongProfile = smoothedPadAlongProfileRoiAndThicknessProfile.getSecond();

      // profile uses the specified width
      final float GAP_PROFILE_WIDTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ALONG) / 100.0f;
      // profile uses the entire pad length
      final float GAP_PROFILE_LENGTH_FRACTION = 1.0f;

      // profile is located in the pad
      final float PROFILE_ALONG_POSITION_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ALONG) / 100.0f;

      int numberOfProfilesAlong = getNumberOfProfileAlongMeasurements(subtype);
      int numberOfGapsAlong = getNumberOfGapAlongMeasurements(subtype);
      float gapOffsetFractionAlong = getGapOffsetFractionAlong(subtype);

      if (numberOfProfilesAlong == 2 || numberOfProfilesAlong == 3)
      {
        //-----------------------------------------------------------------------------------
        // North Gaps
        //-----------------------------------------------------------------------------------

        List<GapInformation> northGapInformationList = createGapInformationList(
            subtype,
            smoothedPadAlongProfile,
            numberOfGapsAlong,
            gapOffsetFractionAlong,
            "North",
            _northGapMeasurementEnumList,
            "ALGDIAG_EXPOSED_PAD_MEASUREMENT_NORTH_GAP_PROFILE_LABEL_KEY");
        debugGapInformationList(northGapInformationList);

        // north gap profile is centered around "location %" below top
        final float NORTH_GAP_PROFILE_LOCATION = PROFILE_ALONG_POSITION_FRACTION;

        for (GapInformation northGapInformation : northGapInformationList)
        {
          preformGapMeasurements(image,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 smoothedPadAlongProfile,
                                 smoothedPadAlongProfileRoi,
                                 USE_ALONG_PROFILE,
                                 GAP_PROFILE_WIDTH_FRACTION,
                                 GAP_PROFILE_LENGTH_FRACTION,
                                 NORTH_GAP_PROFILE_LOCATION,
                                 northGapInformation,
                                 MILIMETER_PER_PIXEL,
                                 profileSmoothingLength);
        }
      }

      if (numberOfProfilesAlong == 1 || numberOfProfilesAlong == 3)
      {
        //-----------------------------------------------------------------------------------
        // Middle Gaps
        //-----------------------------------------------------------------------------------

        List<GapInformation> middleGapInformationList = createGapInformationList(
            subtype,
            smoothedPadAlongProfile,
            numberOfGapsAlong,
            gapOffsetFractionAlong,
            "Middle",
            _middleGapMeasurementEnumList,
            "ALGDIAG_EXPOSED_PAD_MEASUREMENT_MIDDLE_GAP_PROFILE_LABEL_KEY");
        debugGapInformationList(middleGapInformationList);

        // middle gap profile is centered
        final float MIDDLE_GAP_PROFILE_LOCATION = 0.50f;

        for (GapInformation middleGapInformation : middleGapInformationList)
        {
          preformGapMeasurements(image,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 smoothedPadAlongProfile,
                                 smoothedPadAlongProfileRoi,
                                 USE_ALONG_PROFILE,
                                 GAP_PROFILE_WIDTH_FRACTION,
                                 GAP_PROFILE_LENGTH_FRACTION,
                                 MIDDLE_GAP_PROFILE_LOCATION,
                                 middleGapInformation,
                                 MILIMETER_PER_PIXEL,
                                 profileSmoothingLength);
        }
      }

      if (numberOfProfilesAlong == 2 || numberOfProfilesAlong == 3)
      {
        //-----------------------------------------------------------------------------------
        // South Gaps
        //-----------------------------------------------------------------------------------

        List<GapInformation> southGapInformationList = createGapInformationList(
            subtype,
            smoothedPadAlongProfile,
            numberOfGapsAlong,
            gapOffsetFractionAlong,
            "South",
            _southGapMeasurementEnumList,
            "ALGDIAG_EXPOSED_PAD_MEASUREMENT_SOUTH_GAP_PROFILE_LABEL_KEY");
        debugGapInformationList(southGapInformationList);

        // south profile is centered around "location %" above bottom
        final float SOUTH_GAP_PROFILE_LOCATION = 1.0f - PROFILE_ALONG_POSITION_FRACTION;

        for (GapInformation southGapInformation : southGapInformationList)
        {
          preformGapMeasurements(image,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 smoothedPadAlongProfile,
                                 smoothedPadAlongProfileRoi,
                                 USE_ALONG_PROFILE,
                                 GAP_PROFILE_WIDTH_FRACTION,
                                 GAP_PROFILE_LENGTH_FRACTION,
                                 SOUTH_GAP_PROFILE_LOCATION,
                                 southGapInformation,
                                 MILIMETER_PER_PIXEL,
                                 profileSmoothingLength);
        }
      }
    }

    // Profile Across measurements

    // If profile across measurements are enabled, get 1, 2, or 3 across profiles of specified width and position
    if (isProfileAcrossMeasurementEnabled(subtype))
    {
      // Get a non-standard across profile to use as a "background".  This is the pad profile without
      // the normal background subtraction techniques
      final float PAD_ACROSS_PROFILE_WIDTH_FRACTION = 1.0f;
      final float PAD_ACROSS_PROFILE_LENGTH_FRACTION = 0.0f;
//          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROF_WIDTH_ACROSS) / 100.f;
      Pair<RegionOfInterest, float[]> smoothedPadAcrossProfileRoiAndThicknessProfile =
        measureSmoothedPadThicknessProfileWithoutProcessingBackground(image,
                                                                      sliceNameEnum,
                                                                      jointInspectionData,
                                                                      USE_ACROSS_PROFILE,
                                                                      PAD_ACROSS_PROFILE_WIDTH_FRACTION,
                                                                      PAD_ACROSS_PROFILE_LENGTH_FRACTION,
                                                                      true);

      RegionOfInterest smoothedPadAcrossProfileRoi = smoothedPadAcrossProfileRoiAndThicknessProfile.getFirst();
      float[] smoothedPadAcrossProfile = smoothedPadAcrossProfileRoiAndThicknessProfile.getSecond();

      // profile uses the specified width
      final float GAP_PROFILE_WIDTH_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ACROSS) / 100.0f;
      // profile uses the entire pad length
      final float GAP_PROFILE_LENGTH_FRACTION = 1.0f;

      // profile uses the specified width
      final float PROFILE_ACROSS_POSITION_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ACROSS) / 100.0f;

      int numberOfProfilesAcross = getNumberOfProfileAcrossMeasurements(subtype);
      int numberOfGapsAcross = getNumberOfGapAcrossMeasurements(subtype);
      float gapOffsetFractionAcross = getGapOffsetFractionAcross(subtype);

      if (numberOfProfilesAcross == 2 || numberOfProfilesAcross == 3)
      {
        //-----------------------------------------------------------------------------------
        // West Gaps
        //-----------------------------------------------------------------------------------

        List<GapInformation> westGapInformationList = createGapInformationList(
            subtype,
            smoothedPadAcrossProfile,
            numberOfGapsAcross,
            gapOffsetFractionAcross,
            "West",
            _westGapMeasurementEnumList,
            "ALGDIAG_EXPOSED_PAD_MEASUREMENT_WEST_GAP_PROFILE_LABEL_KEY");
        debugGapInformationList(westGapInformationList);

        // west profile is centered around "location %" from left
        final float WEST_GAP_PROFILE_LOCATION = PROFILE_ACROSS_POSITION_FRACTION;

        for (GapInformation westGapInformation : westGapInformationList)
        {
          preformGapMeasurements(image,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 smoothedPadAcrossProfile,
                                 smoothedPadAcrossProfileRoi,
                                 USE_ACROSS_PROFILE,
                                 GAP_PROFILE_WIDTH_FRACTION,
                                 GAP_PROFILE_LENGTH_FRACTION,
                                 WEST_GAP_PROFILE_LOCATION,
                                 westGapInformation,
                                 MILIMETER_PER_PIXEL,
                                 profileSmoothingLength);
        }
      }

      if (numberOfProfilesAcross == 1 || numberOfProfilesAcross == 3)
      {
        //-----------------------------------------------------------------------------------
        // Center Gaps
        //-----------------------------------------------------------------------------------

        List<GapInformation> centerGapInformationList = createGapInformationList(
            subtype,
            smoothedPadAcrossProfile,
            numberOfGapsAcross,
            gapOffsetFractionAcross,
            "Center",
            _centerGapMeasurementEnumList,
            "ALGDIAG_EXPOSED_PAD_MEASUREMENT_CENTER_GAP_PROFILE_LABEL_KEY");
        debugGapInformationList(centerGapInformationList);

        // center profile is centered
        final float CENTER_GAP_PROFILE_LOCATION = 0.50f;

        for (GapInformation centerGapInformation : centerGapInformationList)
        {
          preformGapMeasurements(image,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 smoothedPadAcrossProfile,
                                 smoothedPadAcrossProfileRoi,
                                 USE_ACROSS_PROFILE,
                                 GAP_PROFILE_WIDTH_FRACTION,
                                 GAP_PROFILE_LENGTH_FRACTION,
                                 CENTER_GAP_PROFILE_LOCATION,
                                 centerGapInformation,
                                 MILIMETER_PER_PIXEL,
                                 profileSmoothingLength);
        }
      }

      if (numberOfProfilesAcross == 2 || numberOfProfilesAcross == 3)
      {
        //-----------------------------------------------------------------------------------
        // East Gaps
        //-----------------------------------------------------------------------------------

        List<GapInformation> eastGapInformationList = createGapInformationList(
            subtype,
            smoothedPadAcrossProfile,
            numberOfGapsAcross,
            gapOffsetFractionAcross,
            "East",
            _eastGapMeasurementEnumList,
            "ALGDIAG_EXPOSED_PAD_MEASUREMENT_EAST_GAP_PROFILE_LABEL_KEY");
        debugGapInformationList(eastGapInformationList);

        // east profile is centered around "location %" from right
        final float EAST_GAP_PROFILE_LOCATION = 1.0f - PROFILE_ACROSS_POSITION_FRACTION;

        for (GapInformation eastGapInformation : eastGapInformationList)
        {
          preformGapMeasurements(image,
                                 sliceNameEnum,
                                 jointInspectionData,
                                 smoothedPadAcrossProfile,
                                 smoothedPadAcrossProfileRoi,
                                 USE_ACROSS_PROFILE,
                                 GAP_PROFILE_WIDTH_FRACTION,
                                 GAP_PROFILE_LENGTH_FRACTION,
                                 EAST_GAP_PROFILE_LOCATION,
                                 eastGapInformation,
                                 MILIMETER_PER_PIXEL,
                                 profileSmoothingLength);
        }
      }
    }
  }

//----------------------------------------------------------------------------------------------------------
//   Measurement Methods
//----------------------------------------------------------------------------------------------------------

  private GapResult preformGapMeasurements(Image image,
                                      SliceNameEnum sliceNameEnum,
                                      JointInspectionData jointInspectionData,
                                      float[] smoothedPadProfile,
                                      RegionOfInterest smoothedPadProfileRoi,
                                      boolean useAlongProfile,
                                      float gapProfileWidthFraction,
                                      float gapProfileLengthFraction,
                                      float gapProfileLocationFraction,
                                      GapInformation gapInformation,
                                      final float MILIMETER_PER_PIXEL,
                                      int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedPadProfile != null && smoothedPadProfile.length > 0);
    Assert.expect(gapInformation != null);

    MeasurementEnum filledGapThicknessMeasurementEnum = gapInformation.getFilledGapThicknessMeasurementEnum();
    MeasurementEnum innerGapMeasurementEnum = gapInformation.getInnerGapMeasurementEnum();
    MeasurementEnum outerGapMeasurementEnum = gapInformation.getOuterGapMeasurementEnum();
    MeasurementEnum gapRatioMeasurementEnum = gapInformation.getGapRatioMeasurementEnum();
    MeasurementEnum gapThicknessMeasurementEnum = gapInformation.getGapThicknessMeasurementEnum();
    LocalizedString gapProfileLabel =  gapInformation.getGapProfileLabel();
    String gapName = gapInformation.getGapName();

    Assert.expect(filledGapThicknessMeasurementEnum != null);
    Assert.expect(innerGapMeasurementEnum != null);
    Assert.expect(outerGapMeasurementEnum != null);
    Assert.expect(gapRatioMeasurementEnum != null);
    Assert.expect(gapThicknessMeasurementEnum != null);
    Assert.expect(gapProfileLabel != null);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    Subtype subtype = jointInspectionData.getSubtype();

    Pair<RegionOfInterest, float[]> smoothedGapProfileRoiAndThicknessProfile;

    if (useAlongProfile)
    {
      if (isStandardBackgroundEnabled(subtype))
      {
        if (isAverageBackgroundProfileAlongEnabled(subtype))
        {
          smoothedGapProfileRoiAndThicknessProfile =
              measureSmoothedGapThicknessAlongProfileWithAverageBackground(image,
                                                                           sliceNameEnum,
                                                                           jointInspectionData,
                                                                           smoothedPadProfile,
                                                                           gapProfileWidthFraction,
                                                                           gapProfileLengthFraction,
                                                                           gapProfileLocationFraction,
                                                                           true,
                                                                           profileSmoothingLength);
        }
        else
        {
          smoothedGapProfileRoiAndThicknessProfile =
              measureSmoothedGapThicknessAlongProfileWithInterpolatedBackground(image,
                                                                                sliceNameEnum,
                                                                                jointInspectionData,
                                                                                smoothedPadProfile,
                                                                                smoothedPadProfileRoi,
                                                                                gapProfileWidthFraction,
                                                                                gapProfileLengthFraction,
                                                                                gapProfileLocationFraction,
                                                                                true,
                                                                                profileSmoothingLength);
        }
      }
      else
      {
        smoothedGapProfileRoiAndThicknessProfile =
            measureSmoothedGapThicknessAlongProfileWithValueBackground(image,
                                                                       sliceNameEnum,
                                                                       jointInspectionData,
                                                                       smoothedPadProfile,
                                                                       gapProfileWidthFraction,
                                                                       gapProfileLengthFraction,
                                                                       gapProfileLocationFraction,
                                                                       true,
                                                                       profileSmoothingLength);
      }
    }
    else
    {
      if (isStandardBackgroundEnabled(subtype))
      {
        if (isAverageBackgroundProfileAcrossEnabled(subtype))
        {
          smoothedGapProfileRoiAndThicknessProfile =
              measureSmoothedGapThicknessAcrossProfileWithAverageBackground(image,
                                                                            sliceNameEnum,
                                                                            jointInspectionData,
                                                                            smoothedPadProfile,
                                                                            gapProfileWidthFraction,
                                                                            gapProfileLengthFraction,
                                                                            gapProfileLocationFraction,
                                                                            true,
                                                                            profileSmoothingLength);
        }
        else
        {
          smoothedGapProfileRoiAndThicknessProfile =
              measureSmoothedGapThicknessAcrossProfileWithInterpolatedBackground(image,
                                                                                 sliceNameEnum,
                                                                                 jointInspectionData,
                                                                                 smoothedPadProfile,
                                                                                 smoothedPadProfileRoi,
                                                                                 gapProfileWidthFraction,
                                                                                 gapProfileLengthFraction,
                                                                                 gapProfileLocationFraction,
                                                                                 true,
                                                                                 profileSmoothingLength);
        }
      }
      else
      {
        smoothedGapProfileRoiAndThicknessProfile =
            measureSmoothedGapThicknessAcrossProfileWithValueBackground(image,
                                                                        sliceNameEnum,
                                                                        jointInspectionData,
                                                                        smoothedPadProfile,
                                                                        gapProfileWidthFraction,
                                                                        gapProfileLengthFraction,
                                                                        gapProfileLocationFraction,
                                                                        true,
                                                                        profileSmoothingLength);
      }
    }

    RegionOfInterest smoothedGapProfileRoi = smoothedGapProfileRoiAndThicknessProfile.getFirst();
    float[] smoothedGapProfile = smoothedGapProfileRoiAndThicknessProfile.getSecond();

    debug(gapName + " Gap");
    int segmentStart = gapInformation.getStartIndex();
    int gapCenter = gapInformation.getCenterIndex();
    int segmentEnd = gapInformation.getEndIndex();

    // simplified x6000 gap measurement (not used currently)
//    GapResult gapResult1 = measureGapInThicknessProfile(jointInspectionData.getSubtype(), smoothedGapProfile, useAlongProfile, segmentStart, gapCenter, segmentEnd);
//    if (_debug) gapResult1.outputGapMeasurements("x6000 " + gapName + " ");

    // complicated 5DX gap measurement (used for the time being)
    GapResult gapResult = measureGap5DX(jointInspectionData, smoothedGapProfile, useAlongProfile, segmentStart, gapCenter, segmentEnd, MILIMETER_PER_PIXEL);
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.EXPOSED_PAD_MEASUREMENT_DEBUG))
      gapResult.outputGapMeasurements("5DX " + gapName + " ");

    Collection<JointMeasurement> gapMeasurementsToPost = new LinkedList<JointMeasurement>();

    JointMeasurement filledGapMeasurement = new JointMeasurement(this,
                                                                filledGapThicknessMeasurementEnum,
                                                                MeasurementUnitsEnum.MILLIMETERS,
                                                                jointInspectionData.getPad(),
                                                                sliceNameEnum,
                                                                gapResult.getFilledGapThicknessInMillimeters());
    jointInspectionResult.addMeasurement(filledGapMeasurement);
    gapMeasurementsToPost.add(filledGapMeasurement);

    if (gapResult.isFilledGapPassing() == false)
    {
      JointMeasurement innerGapMeasurement = new JointMeasurement(this,
                                                                  innerGapMeasurementEnum,
                                                                  MeasurementUnitsEnum.MILLIMETERS,
                                                                  jointInspectionData.getPad(),
                                                                  sliceNameEnum,
                                                                  gapResult.getLengthOfInnerGapInMillimeters());
      jointInspectionResult.addMeasurement(innerGapMeasurement);
      gapMeasurementsToPost.add(innerGapMeasurement);

      JointMeasurement outerGapMeasurement = new JointMeasurement(this,
                                                                  outerGapMeasurementEnum,
                                                                  MeasurementUnitsEnum.MILLIMETERS,
                                                                  jointInspectionData.getPad(),
                                                                  sliceNameEnum,
                                                                  gapResult.getLengthOfOuterGapInMillimeters());
      jointInspectionResult.addMeasurement(outerGapMeasurement);
      gapMeasurementsToPost.add(outerGapMeasurement);

      float innerOuterGapRatioLeft = (innerGapMeasurement.getValue() / outerGapMeasurement.getValue()) * 100.0f;
      JointMeasurement innerOuterGapRatioMeasurement = new JointMeasurement(this,
                                                                            gapRatioMeasurementEnum,
                                                                            MeasurementUnitsEnum.PERCENT,
                                                                            jointInspectionData.getPad(),
                                                                            sliceNameEnum,
                                                                            innerOuterGapRatioLeft);
      jointInspectionResult.addMeasurement(innerOuterGapRatioMeasurement);
      gapMeasurementsToPost.add(innerOuterGapRatioMeasurement);

      JointMeasurement gapThicknessMeasurement = new JointMeasurement(this,
                                                                      gapThicknessMeasurementEnum,
                                                                      MeasurementUnitsEnum.MILLIMETERS,
                                                                      jointInspectionData.getPad(),
                                                                      sliceNameEnum,
                                                                      gapResult.getMinimumOuterGapThicknessInMillimeters());
      jointInspectionResult.addMeasurement(gapThicknessMeasurement);
      gapMeasurementsToPost.add(gapThicknessMeasurement);
    }

    // post gap measurements
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                           jointInspectionData.getSubtype(),
                                                           this);

      Assert.expect(gapMeasurementsToPost != null);
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      jointInspectionData.getInspectionRegion(),
                                                      jointInspectionData,
                                                      gapMeasurementsToPost.toArray(new JointMeasurement[0]));

      postGapThicknessProfileDiagnostics(jointInspectionData, sliceNameEnum,
                                         smoothedGapProfileRoi,
                                         smoothedGapProfile,
                                         gapResult,
                                         gapProfileLabel);
    }
    return gapResult;
  }

  /**
   * Find the gap based on 1st and 2nd derivative of the profile.
   *
   * New for x6000
   *
   * @author George Booth
   */
  private GapResult measureGapInThicknessProfile(Subtype subtype,
                                                 float[] gapThicknessProfile,
                                                 boolean useAlongProfile,
                                                 int xstartIndex,
                                                 int centerIndex,
                                                 int xendIndex,
                                                 final float MILIMETER_PER_PIXEL,
                                                 int profileSmoothingLength)
  {
    Assert.expect(subtype != null);
    Assert.expect(gapThicknessProfile != null);
    Assert.expect(gapThicknessProfile.length > 0);
    Assert.expect(centerIndex >= 0 && centerIndex < gapThicknessProfile.length);

    float centerSearchDistanceInMillimeters;
    if (useAlongProfile)
    {
      centerSearchDistanceInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG);
    }
    else
    {
      centerSearchDistanceInMillimeters =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS);
    }
    float centerSearchDistanceInPixels = centerSearchDistanceInMillimeters / MILIMETER_PER_PIXEL;
    int halfCenterSearchDistanceInPixels = (int)Math.round(centerSearchDistanceInPixels / 2.0f);

    int startIndex = Math.max(0, centerIndex - halfCenterSearchDistanceInPixels);
    int endIndex = Math.min(centerIndex + halfCenterSearchDistanceInPixels, gapThicknessProfile.length);

    GapResult result = new GapResult();
    result.setGapSearchRange(startIndex, endIndex);
/*
    // new for Bosch
    // Get the average thickness across the gap search area. If this is greater than the (Open) Filled Gap Thickness
    // threshold, the gap is considered passing and no further gap measurements are needed.
    float areaUnderProfileInterval = ProfileUtil.findAreaUnderProfileInterval(gapThicknessProfile,
                                centerSearchFirstIndex, centerSearchFinalIndex);
    // interval is inclusive (first through final)
    float numBins = (float)(centerSearchFinalIndex - centerSearchFirstIndex + 1);
    float averageGapThickness = areaUnderProfileInterval / numBins;
    gapResult.setFilledGapThicknessInMillimeters(averageGapThickness);
    System.out.println("averageGapThickness = " + MathUtil.convertMillimetersToMils(averageGapThickness) + " mils");

    if (useAlongProfile)
    {
      gapResult.setFilledGapThresholdInMillimeters(ExposedPadOpenAlgorithm.getFilledGapThicknessAlong(subtype));
    }
    else
    {
      gapResult.setFilledGapThresholdInMillimeters(ExposedPadOpenAlgorithm.getFilledGapThicknessAcross(subtype));
    }

    if (gapResult.isFilledGapPassing())
    {
      System.out.println("passed FILLED_GAP_THICKNESS_THRESHOLD");
      return gapResult;
    }
*/
    // create a smoothed first derivative profile from the working profile
    float[] smoothedFirstDerivativeProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(gapThicknessProfile,
                                                    2,
                                                    MagnificationEnum.getCurrentNorminal(),
                                                    MeasurementUnitsEnum.MILLIMETERS);

    smoothedFirstDerivativeProfile = ProfileUtil.getSmoothedProfile(smoothedFirstDerivativeProfile,
                                                                              profileSmoothingLength);

    // create a smoothed second derivative profile from the working profile
    float[] smoothedSecondDerivativeProfile =
      AlgorithmUtil.createUnitizedDerivativeProfile(smoothedFirstDerivativeProfile,
                                                    2,
                                                    MagnificationEnum.getCurrentNorminal(),
                                                    MeasurementUnitsEnum.MILLIMETERS);

    smoothedSecondDerivativeProfile = ProfileUtil.getSmoothedProfile(smoothedSecondDerivativeProfile,
                                                                              profileSmoothingLength);

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.EXPOSED_PAD_MEASUREMENT_DEBUG))
      writeProfileToCSVFile("c:/temp/!profile" + _gapName + ".csv",
      "smoothedProfileWithVoidComp", gapThicknessProfile, 1.0F,
      "smoothedFirstDerivativeProfile", smoothedFirstDerivativeProfile, 0.2F,
      "smoothedSecondDerivativeProfile", smoothedSecondDerivativeProfile, 0.02F,
      "smoothedSecondDerivativeProfile", smoothedSecondDerivativeProfile, 0.02F);

    // find the index of the maximum downward slope at start of gap
    int leftEdgeIndex = ArrayUtil.minIndex(smoothedFirstDerivativeProfile, startIndex, centerIndex);
    if (leftEdgeIndex == startIndex)
    {
      leftEdgeIndex = startIndex + 1;
    }
    // find the index of the maximum upward slope at end of gap
    int rightEdgeIndex = ArrayUtil.maxIndex(smoothedFirstDerivativeProfile, centerIndex, endIndex);
    if (rightEdgeIndex == endIndex)
    {
      rightEdgeIndex = endIndex - 1;
    }

    // top left of the gap is where it turns down - the max negative slope of the 2nd derivative before the max slope
    int leftOuterIndex = ArrayUtil.minIndex(smoothedSecondDerivativeProfile, startIndex, leftEdgeIndex);
    if (leftOuterIndex >= leftEdgeIndex)
    {
      leftOuterIndex = Math.max(leftEdgeIndex - 1, 0);
    }
    result.setLeftOuterIndex(leftOuterIndex);

    // bottom left of the gap is where it flattens out - the max positive slope of the 2nd derivative after the max slope
    int leftInnerIndex = ArrayUtil.maxIndex(smoothedSecondDerivativeProfile, leftEdgeIndex, centerIndex);
    if (leftInnerIndex == leftEdgeIndex)
    {
      leftInnerIndex = Math.min(leftEdgeIndex + 1, centerIndex);
    }
    result.setLeftInnerIndex(leftInnerIndex);

    // bottom right of the gap is where it turns up - the max positive slope of the 2nd derivative before the max slope
    int rightInnerIndex = ArrayUtil.maxIndex(smoothedSecondDerivativeProfile, centerIndex, rightEdgeIndex);
    if (rightInnerIndex == rightEdgeIndex)
    {
      rightInnerIndex = Math.max(rightEdgeIndex - 1, centerIndex);
    }
    result.setRightInnerIndex(rightInnerIndex);

    // top right of the gap is where it flattens out - the max negative slope of the 2nd derivative after the max slope
    int rightOuterIndex = ArrayUtil.minIndex(smoothedSecondDerivativeProfile, rightEdgeIndex, endIndex);
    if (rightOuterIndex == rightEdgeIndex)
    {
      rightOuterIndex = Math.min(rightEdgeIndex + 1, gapThicknessProfile.length - 1);
    }
    result.setRightOuterIndex(rightOuterIndex);

    // save thickness at indexes of outer gap
    result.setRightOuterGapThicknessInMillimeters(gapThicknessProfile[rightOuterIndex]);
    result.setLeftOuterGapThicknessInMillimeters(gapThicknessProfile[leftOuterIndex]);

    return result;
  }

  /**
   * Measures the gap width in a smoothed thickness profile.  This assumes the gap is roughly centered in the profile.
   *
   * This was ported from 5DX FET/HSSOP code written by Patrick Lacz
   *
   * @author George Booth
   */
  private GapResult measureGap5DX(JointInspectionData jointInspectionData,
                               float[] gapThicknessProfile,
                               boolean useAlongProfile,
                               int segmentStart,
                               int calcGapCenter,
                               int segmentEnd,
                               final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(gapThicknessProfile != null);
    Assert.expect(gapThicknessProfile.length > 0);
    Assert.expect(segmentStart >= 0);
    Assert.expect(segmentStart <= calcGapCenter && calcGapCenter <= segmentEnd);
    Assert.expect(segmentEnd < gapThicknessProfile.length);
    Assert.expect(segmentStart <= segmentEnd);

    Subtype subtype = jointInspectionData.getSubtype();

    // assume gap is near middle of profile segment
    final float STARTING_CENTER_LOCATIION = 0.5f;
    float centerSearchDistanceInMillimeters = 0.0f;
    if (useAlongProfile)
    {
      centerSearchDistanceInMillimeters =
          (Float)jointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG);
    }
    else
    {
      centerSearchDistanceInMillimeters =
          (Float)jointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS);
    }

    float centerSearchDistanceInPixels = centerSearchDistanceInMillimeters / MILIMETER_PER_PIXEL;
    int halfCenterSearchDistanceInPixels = (int)Math.round(centerSearchDistanceInPixels / 2.0f);

    int profileSegmentLength = segmentEnd - segmentStart;
    int centerLocationIndex = (int)((float)profileSegmentLength * STARTING_CENTER_LOCATIION) + segmentStart;
    centerLocationIndex = calcGapCenter;

    // get first index and make sure it's in range
    int centerSearchFirstIndex = centerLocationIndex - halfCenterSearchDistanceInPixels;
    centerSearchFirstIndex = Math.max(segmentStart, centerSearchFirstIndex);
    centerSearchFirstIndex = Math.min(segmentEnd, centerSearchFirstIndex);

    // get last index and make sure it's in range
    int centerSearchFinalIndex = centerLocationIndex + halfCenterSearchDistanceInPixels;
    centerSearchFinalIndex = Math.max(segmentStart, centerSearchFinalIndex);
    centerSearchFinalIndex = Math.min(segmentEnd, centerSearchFinalIndex);

    float smallestDiscardedMinimum = 0.0f;
    boolean redoTestUsingOrginalCenter = false;
    GapResult gapResult = new GapResult();
    gapResult.setGapSearchRange(centerSearchFirstIndex, centerSearchFinalIndex);

    // new for Bosch
    // Get the average thickness across the gap search area. If this is greater than the (Open) Filled Gap Thickness
    // threshold, the gap is considered passing and no further gap measurements are needed.
    float areaUnderProfileInterval = ProfileUtil.findAreaUnderProfileInterval(gapThicknessProfile,
                                centerSearchFirstIndex, centerSearchFinalIndex);
    // interval is inclusive (first through final)
    float numBins = (float)(centerSearchFinalIndex - centerSearchFirstIndex + 1);
    float averageGapThickness = areaUnderProfileInterval / numBins;
    gapResult.setFilledGapThicknessInMillimeters(averageGapThickness);

    if (useAlongProfile)
    {
      gapResult.setFilledGapThresholdInMillimeters(ExposedPadOpenAlgorithm.getFilledGapThicknessAlong(subtype));
    }
    else
    {
      gapResult.setFilledGapThresholdInMillimeters(ExposedPadOpenAlgorithm.getFilledGapThicknessAcross(subtype));
    }

    if (gapResult.isFilledGapPassing())
    {
      return gapResult;
    }

    // If the result is too far from the original center, take the measurement again using the original center
    ArrayList<Integer> previousCenterSearchMinIndexes = new ArrayList<Integer>();
    do
    {
      redoTestUsingOrginalCenter = false;
      int centerSearchMinIndex = findSmallestLocalMinima(gapThicknessProfile,
                                                         centerSearchFirstIndex,
                                                         centerSearchFinalIndex,
                                                         smallestDiscardedMinimum);

      // Try to take the measurement from this point
      if (previousCenterSearchMinIndexes.contains(centerSearchMinIndex))
      {
        // don't loop infinitely over previous entries
        break;
      }
      previousCenterSearchMinIndexes.add(centerSearchMinIndex);
      measureGapInThicknessProfile5DX(subtype, gapThicknessProfile, segmentStart, centerSearchMinIndex, segmentEnd, gapResult, MILIMETER_PER_PIXEL);

      int gapCenter = gapResult.getOuterGapCenter();
      int originalCenter = centerLocationIndex;
      int gapSearchCenter = gapResult.getSearchCenterIndex();
      float relativeDistanceFromOriginalCenter = Math.abs((float)(gapCenter - gapSearchCenter)) / (float) profileSegmentLength;
      if (relativeDistanceFromOriginalCenter > 0.05f)
      {
        if (originalCenter < gapResult.getLeftOuterIndex() || originalCenter > gapResult.getRightOuterIndex())
        {
          redoTestUsingOrginalCenter = true;
          smallestDiscardedMinimum = gapThicknessProfile[centerSearchMinIndex];
        }
      }

    } while (redoTestUsingOrginalCenter);

    return gapResult;
  }

  /**
   * Returns midpoint of two indicies if no local minima is found
   *
   * This was ported from 5DX FET/HSSOP code written by Patrick Lacz
   *
   * @author George Booth
   */
  private int findSmallestLocalMinima(float[] gapThicknessProfile,
                                      int startIndex,
                                      int finalIndex,
                                      float searchAboveValue)
  {
    Assert.expect(gapThicknessProfile != null);
    Assert.expect(gapThicknessProfile.length > 0);
    Assert.expect(startIndex >= 0 && startIndex < gapThicknessProfile.length);
    Assert.expect(finalIndex >= 0 && finalIndex < gapThicknessProfile.length);
    Assert.expect(startIndex <= finalIndex);

    float smallValue     = 0.000002f;   // millimeters^2 per pixel, was 0.0001 mils^2/pixel in 5DX
    float verySmallValue = 0.0000002f;  // millimeters,             was 0.00001 mils in 5DX

    // create a derivative profile.
    float[] derivatives = new float[finalIndex - startIndex];
    for (int i = 0; i < finalIndex - startIndex; ++i)
    {
      derivatives[i] = gapThicknessProfile[i + startIndex + 1] - gapThicknessProfile[i + startIndex];
    }

    // look for zero-crossings
    List<Integer> extremaList = new LinkedList<Integer>();
    // don't use first or last - we need to access i-1 and i+1 later
    for (int i = 1; i < derivatives.length - 1; ++i)
    {
      if (derivatives[i] * derivatives[i] < smallValue)
        extremaList.add(i);
      else if (derivatives[i] * derivatives[i + 1] < 0.0f) // change of sign
        extremaList.add(i);
      else if (gapThicknessProfile[startIndex + i - 1] <= verySmallValue &&
               gapThicknessProfile[startIndex + i]     <= verySmallValue &&
               gapThicknessProfile[startIndex + i + 1] <= verySmallValue)
        extremaList.add(i);
    }

    // determine if zero-crossings are maxima, minima, or saddles.  Keep unknowns.
    List<Integer> minimaList = new LinkedList<Integer>();
    for (int index : extremaList)
    {
      if (derivatives[index - 1] <= smallValue && derivatives[index + 1] >= -smallValue)
        minimaList.add(index);
    }

    int smallestMinimaIndex = (startIndex + finalIndex) / 2;
    float smallestMinima = Float.MAX_VALUE;
    for (int minimaIndex : minimaList)
    {
      float valueAtMinima = gapThicknessProfile[startIndex + minimaIndex];
      if (valueAtMinima <= searchAboveValue)
        continue;
      if (smallestMinima > valueAtMinima)
      {
        smallestMinimaIndex = startIndex + minimaIndex;
        smallestMinima = valueAtMinima;
      }
    }

    return smallestMinimaIndex;
  }

  /**
   * This was ported from 5DX FET/HSSOP code written by Patrick Lacz
   *
   * @author George Booth
   */
  private void measureGapInThicknessProfile5DX(Subtype subtype,
                                               float[] gapThicknessProfile,
                                               int segmentStart,
                                               int centerIndex,
                                               int segmentEnd,
                                               GapResult gapResult,
                                               final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(subtype != null);
    Assert.expect(gapThicknessProfile != null);
    Assert.expect(gapThicknessProfile.length > 0);
    Assert.expect(segmentStart >= 0 && segmentStart <= centerIndex);
    Assert.expect(centerIndex >= segmentStart && centerIndex <= segmentEnd);
    Assert.expect(segmentEnd >= segmentStart && segmentEnd < gapThicknessProfile.length);

    final float cliffSlope =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_CLIFF_SLOPE);
    final float falloffSlope =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_FALLOFF_SLOPE);

    float millimetersPerPixel = MILIMETER_PER_PIXEL;

    findTopOfGapPointsUsingThetaProfile(gapThicknessProfile, segmentStart, centerIndex, segmentEnd, 0.1f, gapResult, MILIMETER_PER_PIXEL);

    // make sure the top gap points are resonable
    int leftOuterIndex = gapResult.getLeftOuterIndex();
    int rightOuterIndex = gapResult.getRightOuterIndex();
    if (leftOuterIndex < segmentStart)
    {
      leftOuterIndex = segmentStart;
      gapResult.setLeftOuterIndex(leftOuterIndex);
    }
    if (rightOuterIndex < leftOuterIndex)
    {
      rightOuterIndex = segmentEnd;
      gapResult.setRightOuterIndex(rightOuterIndex);
    }

    // save thickness at indexes of outer gap
    gapResult.setRightOuterGapThicknessInMillimeters(gapThicknessProfile[rightOuterIndex]);
    gapResult.setLeftOuterGapThicknessInMillimeters(gapThicknessProfile[leftOuterIndex]);

    gapResult.setLeftInnerIndex(findCliffBottomCusp(gapThicknessProfile,
                                                      leftOuterIndex,
                                                      rightOuterIndex, //centerIndex,
                                                      1, 3, //5,
                                                      cliffSlope,
                                                      falloffSlope,
                                                      millimetersPerPixel));

    gapResult.setRightInnerIndex(findCliffBottomCusp(gapThicknessProfile,
                                                       rightOuterIndex,
                                                       leftOuterIndex, //centerIndex,
                                                       -1, 3, //5,
                                                       cliffSlope,
                                                       falloffSlope,
                                                       millimetersPerPixel));
  }

  /**
  * Find the top of the gap by using an angular measurement within a specified profile range.
  * I have found this to be the most robust technique for finding the outer gap measurement
  * This is similar to ambient occlusion techniques from rendering.
  *
  * This was ported from 5DX FET/HSSOP code written by Patrick Lacz but modified to limit the search
  * to the specified in/max range.
  *
  * @author George Booth
  */
  private void findTopOfGapPointsUsingThetaProfile(float[] thicknessProfile,
                                                   int minIndex,
                                                   int centerIndex,
                                                   int maxIndex,
                                                   float cutoffFractionThreshold,
                                                   GapResult gapResult,
                                                   final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(thicknessProfile != null);
    Assert.expect(thicknessProfile.length > 0);
    Assert.expect(minIndex >= 0);
    Assert.expect(minIndex <= centerIndex && centerIndex <= maxIndex);
    Assert.expect(maxIndex < thicknessProfile.length);

    int startAwayFromCenter = 5;

    // the min and max T (theta) are for display purposes only. I have left their computation in
    // so that it is easier to debug
    float minT = Float.MAX_VALUE;
    float maxT = Float.MIN_VALUE;

    float[] thetaProfile = new float[thicknessProfile.length];
    for (int t = 0 ; t <= thetaProfile.length - 1 ; ++t)
    {
      float dx = Math.abs(t - centerIndex) * MILIMETER_PER_PIXEL;
      float dy = thicknessProfile[centerIndex] - thicknessProfile[t];

      float theta = (float)Math.atan2(dx, dy);
      if (dx == 0.0f)
        theta = (float)Math.atan2(1.0f, 0.0f);
      minT = Math.min(minT, theta);
      maxT = Math.max(maxT, theta);
      thetaProfile[t] = theta;
    }

    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true 
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.EXPOSED_PAD_MEASUREMENT_DEBUG))
      writeProfileToCSVFile("c:/temp/!thetaProfile.csv",
                          "thicknessProfile", thicknessProfile, 1.0f,
                          "thetaProfile", thetaProfile, 0.1f);

    int leftOuterIndex = ArrayUtil.maxIndex(thetaProfile, minIndex, centerIndex);

    float valueAtCenter = thicknessProfile[centerIndex];
    int minBinIndex = ArrayUtil.minIndex(thicknessProfile, minIndex, maxIndex);
    int maxBinIndex = ArrayUtil.maxIndex(thicknessProfile, minIndex, maxIndex);
    float valueAtMin = thicknessProfile[minBinIndex];
    float valueAtMax = thicknessProfile[maxBinIndex];

    /** @todo Lacz The following line of code is rather ugly.  See if it can be obviated.
     *     Perhaps by adding the "startAwayFromCenter" search modifier, it may be un-necessary. */
    float maximumValueToStopSearch = valueAtCenter - 0.00001f - cutoffFractionThreshold * (valueAtMax - valueAtMin);
    float maxValueSeen = Float.MIN_VALUE;
    float indexOfMaxValueSeen = 0.0f;
    for (int i = centerIndex - startAwayFromCenter ; i > minIndex ; --i)
    {
      float thetaHere = thetaProfile[i];
      if (thetaHere > maxValueSeen)
      {
        maxValueSeen = thetaHere;
        indexOfMaxValueSeen = i;
      }

      float valueHere = thicknessProfile[i];
      float slopeHere = valueHere - thicknessProfile[i+5];
      if (valueHere < maximumValueToStopSearch)
        break;
    }
    leftOuterIndex = (int)indexOfMaxValueSeen;


    int rightOuterIndex = ArrayUtil.maxIndex(thetaProfile, centerIndex, maxIndex);

    maxValueSeen = Float.MIN_VALUE;
    indexOfMaxValueSeen = 0;
    for (int i = centerIndex + startAwayFromCenter; i < maxIndex; ++i)
    {
      float thetaHere = thetaProfile[i];
      if (thetaHere > maxValueSeen)
      {
        maxValueSeen = thetaHere;
        indexOfMaxValueSeen = i;
      }

      float valueHere = thicknessProfile[i];
      float slopeHere = valueHere - thicknessProfile[i-5];
      if (valueHere < maximumValueToStopSearch)
        break;
    }
    rightOuterIndex = (int)indexOfMaxValueSeen;

    if (rightOuterIndex == leftOuterIndex)
      rightOuterIndex++;
      /** @todo Lacz There might be some sanity checking we can do on these points, such as chosing the
        highest index (within some tolerance) with the highest thickness value?    */

    gapResult.setLeftOuterIndex(leftOuterIndex);
    gapResult.setRightOuterIndex(rightOuterIndex);
  }

  /**
   * Look for the point where the slope levels out after the indicated point.
  *
  * This was ported from 5DX FET/HSSOP code written by Patrick Lacz
  *
  * @author George Booth
  */
  private int findCliffBottomCusp(float[] profile,
                                  int startIndex,
                                  int originalStartIndex,
                                  int direction, // -1 or 1
                                  int slopeFilterSize, // always positive
                                  float cliffSlope,
                                  float falloffSlope,
                                  float millimetersPerPixel)
  {
    Assert.expect(profile != null);
    Assert.expect(profile.length > 0);
    Assert.expect(direction == -1 || direction == 1);
    Assert.expect(slopeFilterSize > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < profile.length);

    boolean haveSeenCliffSlope = false;
    float maxSlopeSeenSoFar = Float.MIN_VALUE;

    for (int i = startIndex ; i != originalStartIndex ; i += direction)
    {
      float valueHere = profile[i];
      int indexForSlope = i - direction * slopeFilterSize;
      // make sure the index is in range in case we are near the edges of the profile
      if (indexForSlope < 0 || indexForSlope >= profile.length)
      {
        continue;
      }
      float valueForSlope = profile[indexForSlope];
      float slope = (valueForSlope - valueHere) / (millimetersPerPixel * slopeFilterSize);

      if (slope > maxSlopeSeenSoFar)
      {
        maxSlopeSeenSoFar = slope;
      }

      if (haveSeenCliffSlope == false && slope > cliffSlope)
      {
        haveSeenCliffSlope = true;
      }

      if (haveSeenCliffSlope && slope < maxSlopeSeenSoFar/2)
      {
        // done, this is the point we were looking for
        // this is kind of a sanity check. We may want to modify this condition
        // If we can, it would be good to replace the falloffSlope completely with this test.
        return i;
      }

      if (haveSeenCliffSlope && slope < falloffSlope)
      {
        // done, this is the point we were looking for
        return i;
      }
    }

    // didn't find a point, return an index of -1 to flag no cusp
    return _UNUSED_BIN;
  }

//----------------------------------------------------------------------------------------------------------
//   Bosch Measure Profiles
//----------------------------------------------------------------------------------------------------------

  /**
   * Makes a smoothed thickness profile for the specified gap.  The profile width is set based on the
   * specified fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad. A background profile dervied from a single background
   * grey level value. The foreground and background profiles are converted into a "pad thickness profile".
   * Finally, the thickness profile is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedGapThicknessAcrossProfileWithValueBackground(
                                                 Image image,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 float[] smoothedPadProfile,
                                                 float fractionOfPadLengthAcross,
                                                 float fractionOfPadLengthAlong,
                                                 float fractionOfGapPosition,
                                                 boolean postProfileRegions,
                                                 int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the interpad distance.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    // Calculate the placement of the pad profile region.
    RegionOfInterest acrossProfileRoi = new RegionOfInterest(locatedJointRoi);
    int acrossProfileRoiLengthAcross = (int)Math.ceil((float)locatedJointRoi.getLengthAcross() * fractionOfPadLengthAlong);
    acrossProfileRoi.setLengthAcross(acrossProfileRoiLengthAcross);

    int acrossProfileRoiLengthAlong = (int)Math.ceil((float)locatedJointRoi.getLengthAlong() * fractionOfPadLengthAcross);
    acrossProfileRoi.setLengthAlong(acrossProfileRoiLengthAlong);
    acrossProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    double newCenter = locatedJointRoi.getMinCoordinateAlong() + (locatedJointRoi.getLengthAlong() * (double)fractionOfGapPosition);
    int offset = (int)(newCenter - locatedJointRoi.getCenterAlong());
    acrossProfileRoi.translateAlongAcross(offset, 0);

    if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
    {
      acrossProfileRoi.setOrientationInDegrees(90);
    }
    else
    {
      acrossProfileRoi.setOrientationInDegrees(0);
    }

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(acrossProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, acrossProfileRoi);
    }

    // get the single value background profile and Rois based on the Background Technique settings
    Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> backgroundProfileAndRois =
      getSingleValueBackgroundValueAndRois(image,
                                           jointInspectionData,
                                           false);

    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        Pair<RegionOfInterest, RegionOfInterest> backgroundRois = backgroundProfileAndRois.getSecond();
        RegionOfInterest backgroundProfile1Roi = backgroundRois.getFirst();
        RegionOfInterest backgroundProfile2Roi = backgroundRois.getSecond();

        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(acrossProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION);

        MeasurementRegionDiagnosticInfo backgroundProfile1RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile1Roi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);
        if (backgroundProfile2Roi != null)
        {
          MeasurementRegionDiagnosticInfo backgroundProfile2RoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(backgroundProfile2Roi,
                                                  MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       padProfileRoiDiagInfo,
                                       backgroundProfile1RoiDiagInfo,
                                       backgroundProfile2RoiDiagInfo);
        }
        else
        {
          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       padProfileRoiDiagInfo,
                                       backgroundProfile1RoiDiagInfo);
        }
      }
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, acrossProfileRoi);

    // Get the single value background profile.
    float averageBackgroundValue = backgroundProfileAndRois.getFirst();
    float[] backgroundProfile = new float[padGrayLevelProfile.length];
    for (int i = 0; i < padGrayLevelProfile.length; i++)
    {
      backgroundProfile[i] = averageBackgroundValue;
    }

    // Create a thickness profile using the pad profile as foreground and the average background profile as background.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(padGrayLevelProfile, backgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      profileSmoothingLength);

    return new Pair<RegionOfInterest, float[]>(acrossProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
   * Makes a smoothed thickness profile for the specified gap.  The profile width is set based on the
   * specified fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad. A background profile dervied from a single background
   * grey level value. The foreground and background profiles are converted into a "pad thickness profile".
   * Finally, the thickness profile is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedGapThicknessAlongProfileWithValueBackground(
                                                 Image image,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 float[] smoothedPadProfile,
                                                 float fractionOfPadLengthAcross,
                                                 float fractionOfPadLengthAlong,
                                                 float fractionOfGapPosition,
                                                 boolean postProfileRegions,
                                                 int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Calculate the placement of the pad profile region.
    RegionOfInterest alongProfileRoi = new RegionOfInterest(locatedJointRoi);
    int padProfileRoiLengthAlong = (int)Math.ceil((float)alongProfileRoi.getLengthAlong() * fractionOfPadLengthAlong);
    alongProfileRoi.setLengthAlong(padProfileRoiLengthAlong);

    int padProfileLengthAcross = (int)Math.ceil((float)alongProfileRoi.getLengthAcross() * fractionOfPadLengthAcross);
    alongProfileRoi.setLengthAcross(padProfileLengthAcross);

    alongProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    double newCenter = locatedJointRoi.getMinCoordinateAcross() + (locatedJointRoi.getLengthAcross() * (double)fractionOfGapPosition);
    int offset = (int)(newCenter - locatedJointRoi.getCenterAcross());
    alongProfileRoi.translateAlongAcross(0, offset);

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(alongProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, alongProfileRoi);
    }

    // get the single value background profile and Rois based on the Background Technique settings
    Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> backgroundProfileAndRois =
      getSingleValueBackgroundValueAndRois(image,
                                           jointInspectionData,
                                           true);

    Pair<RegionOfInterest, RegionOfInterest> backgroundRois = backgroundProfileAndRois.getSecond();
    RegionOfInterest backgroundProfile1Roi = backgroundRois.getFirst();
    RegionOfInterest backgroundProfile2Roi = backgroundRois.getSecond();

    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(alongProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION);

        MeasurementRegionDiagnosticInfo backgroundProfile1RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile1Roi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);
        if (backgroundProfile2Roi != null)
        {
          MeasurementRegionDiagnosticInfo backgroundProfile2RoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(backgroundProfile2Roi,
                                                  MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       padProfileRoiDiagInfo,
                                       backgroundProfile1RoiDiagInfo,
                                       backgroundProfile2RoiDiagInfo);
        }
        else
        {
          _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       false,
                                       padProfileRoiDiagInfo,
                                       backgroundProfile1RoiDiagInfo);
        }
      }
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, alongProfileRoi);

    // Get the single value background profile.
    float averageBackgroundValue = backgroundProfileAndRois.getFirst();
    float[] backgroundProfile = new float[padGrayLevelProfile.length];
    for (int i = 0; i < padGrayLevelProfile.length; i++)
    {
      backgroundProfile[i] = averageBackgroundValue;
    }

    // Create a thickness profile using the pad profile as foreground and the average background profile as background.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(padGrayLevelProfile, backgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      profileSmoothingLength);

    return new Pair<RegionOfInterest, float[]>(alongProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
   * Determines the average background value based on Measurement Technnique settings. This is used by learning
   * to generate the background image.
   *
   * @author George Booth
   */
  public static float getAverageBackgroundValue(Image image,
                                         JointInspectionData jointInspectionData)
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);

    Subtype subtype = jointInspectionData.getSubtype();

    float averageBackgroundValue = 0.0f;
    boolean forAlongProfile = true;

    if (isAverageBackgroundValueAcrossEnabled(subtype))
    {
//      System.out.println("using Average Background Value Across");
      Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> averageBackgroundAndRois =
          getAverageBackgroundValueAcross(image,
                                          jointInspectionData,
                                          forAlongProfile);

      averageBackgroundValue = averageBackgroundAndRois.getFirst();
    }
    else if (isAverageBackgroundValueAlongEnabled(subtype))
    {
//      System.out.println("using Average Background Value Along");
      Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> averageBackgroundAndRois =
          getAverageBackgroundValueAlong(image,
                                          jointInspectionData,
                                          forAlongProfile);

      averageBackgroundValue = averageBackgroundAndRois.getFirst();
    }
    else if (isAverageBackgroundValueFromImageEnabled(subtype))
    {
//      System.out.println("using Average Background Value from Image");
      Pair<Float, RegionOfInterest> averageBackgroundAndRoi =
          getAverageBackgroundValueFromImage(image,
                                             jointInspectionData);

      averageBackgroundValue = averageBackgroundAndRoi.getFirst();
    }
    else if (isNominalBackgroundValueEnabled(subtype))
    {
//      System.out.println("using User Supplied Background Value");
      Pair<Float, RegionOfInterest> averageBackgroundAndRoi =
          getNominalBackgroundValue(image, jointInspectionData);

      averageBackgroundValue = averageBackgroundAndRoi.getFirst();
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting");
    }

//    System.out.println("  averageBackgroundValue = " + averageBackgroundValue);

    return averageBackgroundValue;
  }

  /**
   * Creates the average background ROIs based on Measurement Technnique settings. This is used by voiding
   * to display the background regions.
   *
   * @author George Booth
   */
  public static Pair<RegionOfInterest, RegionOfInterest> getAverageBackgroundRois(Image image,
                                                                                  JointInspectionData jointInspectionData)
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);

    Subtype subtype = jointInspectionData.getSubtype();

    Pair<RegionOfInterest, RegionOfInterest> averageBackgroundRois = new Pair<RegionOfInterest,RegionOfInterest>(null, null);
    boolean forAlongProfile = true;

    // Standard voiding background uses the entire image
    if (isStandardBackgroundEnabled(subtype) ||
        isAverageBackgroundValueFromImageEnabled(subtype) ||
        isNominalBackgroundValueEnabled(subtype))
    {
//      System.out.println("using Average Background Value from Image");
      Pair<Float, RegionOfInterest> averageBackgroundAndRoi =
          getAverageBackgroundValueFromImage(image,
                                             jointInspectionData);

      averageBackgroundRois.setFirst(averageBackgroundAndRoi.getSecond());
    }
    else if (isAverageBackgroundValueAcrossEnabled(subtype))
    {
//      System.out.println("using Average Background Value Across");
      Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> averageBackgroundAndRois =
          getAverageBackgroundValueAcross(image,
                                          jointInspectionData,
                                          forAlongProfile);

      averageBackgroundRois = averageBackgroundAndRois.getSecond();
    }
    else if (isAverageBackgroundValueAlongEnabled(subtype))
    {
//      System.out.println("using Average Background Value Along");
      Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> averageBackgroundAndRois =
          getAverageBackgroundValueAlong(image,
                                          jointInspectionData,
                                          forAlongProfile);

      averageBackgroundRois = averageBackgroundAndRois.getSecond();
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting");
    }

//    System.out.println("  averageBackgroundValue = " + averageBackgroundValue);

    return averageBackgroundRois;
  }

  /**
   * Creates a single value background profile and one or two rois based on Measurement Technnique settings.
   *
   * @author George Booth
   */
  private Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> getSingleValueBackgroundValueAndRois(
                                                                    Image image,
                                                                    JointInspectionData jointInspectionData,
                                                                    boolean forAlongProfile)
  {
    Assert.expect(jointInspectionData != null);

    Subtype subtype = jointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    RegionOfInterest backgroundProfile1ROI = null;
    RegionOfInterest backgroundProfile2ROI = null;
    float averageBackgroundValue = 0.0f;

    // if we aren't showing diagnostics and we have already found a background value, use it
    // we don't need the background ROIs
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this) == false &&
        hasAverageBackgroundValueMeasurement(jointInspectionData, sliceNameEnum))
    {
//      System.out.println("Using existing average background for gap analysis");
      JointMeasurement averageBackgroundValueMeasurement =
      ExposedPadMeasurementAlgorithm.getAverageBackgroundValueMeasurement(jointInspectionData, sliceNameEnum);
      averageBackgroundValue = averageBackgroundValueMeasurement.getValue();
      return new Pair<Float, Pair<RegionOfInterest, RegionOfInterest>>(averageBackgroundValue,
           new Pair<RegionOfInterest, RegionOfInterest>(backgroundProfile1ROI, backgroundProfile2ROI));
    }

    if (isAverageBackgroundValueAcrossEnabled(subtype))
    {
//      System.out.println("using Average Background Value Across");
      Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> averageBackgroundAndRois =
          getAverageBackgroundValueAcross(image,
                                          jointInspectionData,
                                          forAlongProfile);

      averageBackgroundValue = averageBackgroundAndRois.getFirst();
      Pair<RegionOfInterest, RegionOfInterest> backgroundRois = averageBackgroundAndRois.getSecond();
      backgroundProfile1ROI = backgroundRois.getFirst();
      backgroundProfile2ROI = backgroundRois.getSecond();
    }
    else if (isAverageBackgroundValueAlongEnabled(subtype))
    {
//      System.out.println("using Average Background Value Along");
      Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> averageBackgroundAndRois =
          getAverageBackgroundValueAlong(image,
                                          jointInspectionData,
                                          forAlongProfile);

      averageBackgroundValue = averageBackgroundAndRois.getFirst();
      Pair<RegionOfInterest, RegionOfInterest> backgroundRois = averageBackgroundAndRois.getSecond();
      backgroundProfile1ROI = backgroundRois.getFirst();
      backgroundProfile2ROI = backgroundRois.getSecond();
    }
    else if (isAverageBackgroundValueFromImageEnabled(subtype))
    {
//      System.out.println("using Average Background Value from Image");
      Pair<Float, RegionOfInterest> averageBackgroundAndRoi =
          getAverageBackgroundValueFromImage(image,
                                             jointInspectionData);

      averageBackgroundValue = averageBackgroundAndRoi.getFirst();
      backgroundProfile1ROI = averageBackgroundAndRoi.getSecond();
    }
    else if (isNominalBackgroundValueEnabled(subtype))
    {
//      System.out.println("using User Supplied Background Value");
      Pair<Float, RegionOfInterest> averageBackgroundAndRoi =
          getNominalBackgroundValue(image, jointInspectionData);

      averageBackgroundValue = averageBackgroundAndRoi.getFirst();
      backgroundProfile1ROI = averageBackgroundAndRoi.getSecond();
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting");
      return null;
    }

//    System.out.println("  averageBackgroundValue = " + averageBackgroundValue);
    // save in joint results
    JointMeasurement averageBackgroundValueMeasurement =
      new JointMeasurement(this,
                           MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE,
                           MeasurementUnitsEnum.GRAYLEVEL,
                           jointInspectionData.getPad(),
                           getInspectionFamily().getDefaultPadSliceNameEnum(subtype),
                           averageBackgroundValue);
    jointInspectionData.getJointInspectionResult().addMeasurement(averageBackgroundValueMeasurement);

    return new Pair<Float, Pair<RegionOfInterest, RegionOfInterest>>(averageBackgroundValue,
         new Pair<RegionOfInterest, RegionOfInterest>(backgroundProfile1ROI, backgroundProfile2ROI));
  }

  /**
   * Gets an average value and 1 or 2 Rois from the background region defined by the Background Across region settings
   *
   * @author George Booth
   */
  private static Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> getAverageBackgroundValueAcross(
                                                                  Image image,
                                                                  JointInspectionData jointInspectionData,
                                                                  boolean useAlongProfile)
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);

    final int BACKGROUND_PROFILE_LENGTH_ALONG = 6;

    Subtype subtype = jointInspectionData.getSubtype();
    float averageValue = 0.0f;
    float[] padGrayLevelProfile = null;
    RegionOfInterest backgroundProfile1Roi = null;
    RegionOfInterest backgroundProfile2Roi = null;

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
    int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
    int locatedJointRoiLengthAcross = locatedJointRoi.getLengthAcross();

    // Get the interpad distance.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    if (isAverageBackgroundProfileAcrossEnabled(subtype))
    {
      // get left and right background regions

      // Calculate the offsets to the background profile width regions (based on some percentage of the IPD +/- half the profile width).
      final float BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS) / 100.f;
      int backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAlong / 2.0f) +
                                                      ((float)interPadDistanceInPixels * BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD));

      // Calculate the placement of the background profile regions.
      backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile1Roi.setLengthAlong(BACKGROUND_PROFILE_LENGTH_ALONG);
      backgroundProfile1Roi.setLengthAcross(locatedJointRoiLengthAcross);
      backgroundProfile1Roi.translateAlongAcross(-backgroundProfileRoiOffset, 0);

      backgroundProfile2Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile2Roi.setLengthAlong(BACKGROUND_PROFILE_LENGTH_ALONG);
      backgroundProfile2Roi.setLengthAcross(locatedJointRoiLengthAcross);
      backgroundProfile2Roi.translateAlongAcross(backgroundProfileRoiOffset, 0);

      if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
      {
        backgroundProfile1Roi.setOrientationInDegrees(90);
        backgroundProfile2Roi.setOrientationInDegrees(90);
      }
      else
      {
        backgroundProfile1Roi.setOrientationInDegrees(0);
        backgroundProfile2Roi.setOrientationInDegrees(0);
      }

      // Make sure the background profile ROIs are snapped back onto the image.
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Background Region 1") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
      }
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile2Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Background Region 2") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile2Roi);
      }

      // Measure the background profiles.
      float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
      float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundProfile2Roi);

      // Create an average background gray level profile.
      Assert.expect(backgroundProfile1.length == backgroundProfile2.length);
      padGrayLevelProfile = new float[backgroundProfile1.length];
      for (int i = 0; i < padGrayLevelProfile.length; ++i)
      {
        padGrayLevelProfile[i] = (backgroundProfile1[i] + backgroundProfile2[i]) / 2f;
      }
    }
    else
    {
      // get center background region

      // Calculate the placement of the pad profile region.
      backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile1Roi.setLengthAlong(BACKGROUND_PROFILE_LENGTH_ALONG);
      backgroundProfile1Roi.setLengthAcross(locatedJointRoiLengthAcross);

      if (useAlongProfile == false)
      {
        if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
        {
          backgroundProfile1Roi.setOrientationInDegrees(90);
        }
        else
        {
          backgroundProfile1Roi.setOrientationInDegrees(0);
        }
      }

      // Shift the pad profile region back into the image if needed.
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Pad Profile Region") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
      }

      // Measure the pad gray level profile.
      padGrayLevelProfile = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
    }

    Assert.expect(padGrayLevelProfile != null);
    Assert.expect(backgroundProfile1Roi != null);
    if (isAverageBackgroundProfileAcrossEnabled(subtype))
      Assert.expect(backgroundProfile2Roi != null);

    // get the average value from the profile
    float totalGrayLevelValue = 0.0f;
    for (int i = 0; i < padGrayLevelProfile.length; i++)
    {
      totalGrayLevelValue += padGrayLevelProfile[i];
    }
    averageValue = totalGrayLevelValue / padGrayLevelProfile.length;

    return new Pair<Float, Pair<RegionOfInterest, RegionOfInterest>>(averageValue,
           new Pair<RegionOfInterest, RegionOfInterest>(backgroundProfile1Roi, backgroundProfile2Roi));
  }

  /**
   * Gets an average value and 1 or 2 Rois from the background region defined by the Background Along region settings
   *
   * @author George Booth
   */
  private static Pair<Float, Pair<RegionOfInterest, RegionOfInterest>> getAverageBackgroundValueAlong(Image image,
                                                                  JointInspectionData jointInspectionData,
                                                                  boolean useAlongProfile)
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);

    final int BACKGROUND_PROFILE_LENGTH_ACROSS = 6;

    Subtype subtype = jointInspectionData.getSubtype();
    float averageValue = 0.0f;
    float[] padGrayLevelProfile = null;
    RegionOfInterest backgroundProfile1Roi = null;
    RegionOfInterest backgroundProfile2Roi = null;

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
    int locatedJointRoiLengthAcross = locatedJointRoi.getLengthAcross();
    int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();

    // Get the interpad distance.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    if (isAverageBackgroundProfileAlongEnabled(subtype))
    {
      // get top and bottom background regions

      // Calculate the offsets to the background profile width regions (based on some percentage of the IPD +/- half the profile width).
      final float BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG) / 100.f;
      int backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAcross / 2f) +
                                                      ((float)interPadDistanceInPixels * BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD));

      // Calculate the placement of the background profile regions.
      backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile1Roi.setLengthAlong(locatedJointRoiLengthAlong);
      backgroundProfile1Roi.setLengthAcross(BACKGROUND_PROFILE_LENGTH_ACROSS);
      backgroundProfile1Roi.translateAlongAcross(0, -backgroundProfileRoiOffset);

      backgroundProfile2Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile2Roi.setLengthAlong(locatedJointRoiLengthAlong);
      backgroundProfile2Roi.setLengthAcross(BACKGROUND_PROFILE_LENGTH_ACROSS);
      backgroundProfile2Roi.translateAlongAcross(0, backgroundProfileRoiOffset);

      // Make sure the background profile ROIs are snapped back onto the image.
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Background Region 1") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
      }
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile2Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Background Region 2") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile2Roi);
      }

      // Measure the background profiles.
      float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
      float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundProfile2Roi);

      // Create an average background gray level profile.
      Assert.expect(backgroundProfile1.length == backgroundProfile2.length);
      padGrayLevelProfile = new float[backgroundProfile1.length];
      for (int i = 0; i < padGrayLevelProfile.length; ++i)
      {
        padGrayLevelProfile[i] = (backgroundProfile1[i] + backgroundProfile2[i]) / 2f;
      }
    }
    else
    {
      // get middle background region

      // Calculate the placement of the pad profile region.
      backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
      backgroundProfile1Roi.setLengthAlong(locatedJointRoiLengthAlong);
      backgroundProfile1Roi.setLengthAcross(BACKGROUND_PROFILE_LENGTH_ACROSS);

      if (useAlongProfile == false)
      {
        if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
        {
          backgroundProfile1Roi.setOrientationInDegrees(90);
        }
        else
        {
          backgroundProfile1Roi.setOrientationInDegrees(0);
        }
      }

      // Shift the pad profile region back into the image if needed.
      if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                              image,
                                              jointInspectionData.getInspectionRegion(),
                                              "Pad Profile Region") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
      }

      // Measure the pad gray level profile.
      padGrayLevelProfile = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
    }

    Assert.expect(padGrayLevelProfile != null);
    Assert.expect(backgroundProfile1Roi != null);
    if (isAverageBackgroundProfileAlongEnabled(subtype))
      Assert.expect(backgroundProfile2Roi != null);

    // get the average value from the profile
    float totalGrayLevelValue = 0.0f;
    for (int i = 0; i < padGrayLevelProfile.length; i++)
    {
      totalGrayLevelValue += padGrayLevelProfile[i];
    }
    averageValue = totalGrayLevelValue / padGrayLevelProfile.length;

    return new Pair<Float, Pair<RegionOfInterest, RegionOfInterest>>(averageValue,
           new Pair<RegionOfInterest, RegionOfInterest>(backgroundProfile1Roi, backgroundProfile2Roi));
  }

  /**
   * Gets an average background value and 1 Roi from the entire image
   *
   * @author George Booth
   */
  private static Pair<Float, RegionOfInterest> getAverageBackgroundValueFromImage(Image image,
                                                                  JointInspectionData jointInspectionData)
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);

    float averageValue = 0.0f;
    RegionOfInterest backgroundRoi = null;

    String jointName = jointInspectionData.getFullyQualifiedPadName();


    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // create an roi for the entire image
    backgroundRoi = new RegionOfInterest(locatedJointRoi);
    backgroundRoi.setLengthAlong(image.getWidth() - 3);
    backgroundRoi.setLengthAcross(image.getHeight() - 3);

    if (AlgorithmUtil.checkRegionBoundaries(backgroundRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundRoi);
    }

    // analyze the histogram to automatically determine a good binarization threshold
    int numHistogramBins = 256;
    RegionOfInterest imageRoi = new RegionOfInterest(0, 0, image.getWidth(), image.getHeight(), 0, RegionShapeEnum.RECTANGULAR);
    int[] histogram = Threshold.histogram(image, imageRoi, numHistogramBins);

    int midPoint = (int)Threshold.getAutoThreshold(histogram);
//    System.out.println("midpoint threshold = " + midPoint);

    int foregroundMaxCount = 0;
    for (int i = 0; i <= midPoint; i++)
    {
//      System.out.println(i + "," + histogram[i]);
      if (histogram[i] > foregroundMaxCount)
      {
        foregroundMaxCount = histogram[i];
      }
    }

    int backgroundMaxCount = 0;
    int backgroundMaxIndex = 0;
    int backgroundLastZeroCount = 255;
    for (int i = numHistogramBins - 1; i > midPoint; i--)
    {
//      System.out.println(i + "," + histogram[i]);
      if (histogram[i] == 0)
      {
        backgroundLastZeroCount = i;
      }
      if (histogram[i] > backgroundMaxCount)
      {
        backgroundMaxCount = histogram[i];
        backgroundMaxIndex = i;
      }
    }

//    System.out.println("foreground max = " + foregroundMaxCount + " @ " + foregroundMaxIndex + " last foreground 0 = " + foregroundLastZeroCount);
//    System.out.println("background max = " + backgroundMaxCount + " @ " + backgroundMaxIndex + " last background 0 = " + backgroundLastZeroCount);
    if (backgroundMaxIndex < midPoint || backgroundMaxIndex > backgroundLastZeroCount)
    {
      backgroundMaxIndex = midPoint + ((backgroundLastZeroCount - midPoint) / 2);
//      System.out.println("adjusting background max = " + backgroundMaxIndex);
    }

    averageValue = backgroundMaxIndex;
    return new Pair<Float, RegionOfInterest>(averageValue, backgroundRoi);
  }

  /**
   * Gets a nominal background value (learned or from user)
   *
   * @author George Booth
   */
  private static Pair<Float, RegionOfInterest> getNominalBackgroundValue(Image image,
                                                                         JointInspectionData jointInspectionData)
  {
    Assert.expect(image != null);
    Assert.expect(jointInspectionData != null);

    float averageValue = 0.0f;
    RegionOfInterest backgroundRoi = null;

    String jointName = jointInspectionData.getFullyQualifiedPadName();

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // create an roi for the entire image
    backgroundRoi = new RegionOfInterest(locatedJointRoi);
    backgroundRoi.setLengthAlong(image.getWidth() - 3);
    backgroundRoi.setLengthAcross(image.getHeight() - 3);

    if (AlgorithmUtil.checkRegionBoundaries(backgroundRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundRoi);
    }

    averageValue = getNominalBackgroundValue(jointInspectionData.getSubtype());

    return new Pair<Float, RegionOfInterest>(averageValue, backgroundRoi);
  }

  /**
   * Updates the nominal background value based on learned measurements.
   *
   * @author George Booth
   */
  private void updateNominalBackgroundValue(Subtype subtype,
                                            SliceNameEnum slice) throws DatastoreException
 {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

    // Pull all the "background value" measurements from the database.
    float[] averageBackgroundValues =
        subtype.getLearnedJointMeasurementValues(slice,
                                                 MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE,
                                                 true,
                                                 true);

    if (averageBackgroundValues.length > 0)
    {
      // Take the median "average Background Value" and set the nominal to that.
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float medianBackgroundValue = AlgorithmUtil.medianWithExcludeOutlierDecision(averageBackgroundValues, true);
//      System.out.println("  medianBackgroundValue = " + medianBackgroundValue);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NOMINAL_BACKGROUND_VALUE, medianBackgroundValue);
    }
  }

//  /**
//   * Enhances the image contrast by equalizing the histogram.
//   * This code was leveraged from the ImageJ software (free, no licensing required).
//   *
//   * @author George Booth
//   */
//  public static void enhanceContrast(Image image)
//  {
//    Assert.expect(image != null);
//
//    // get the histogram
//    int numHistogramBins = 256;
//    RegionOfInterest imageRoi = new RegionOfInterest(0, 0, image.getWidth(), image.getHeight(), 0, RegionShapeEnum.RECTANGULAR);
//    int[] histogram = Threshold.histogram(image, imageRoi, numHistogramBins);
//
//    // get the sum of the histogram bins
//    double sum = getWeightedValue(histogram, 0);
//    for (int i = 1; i < 255; i ++)
//    {
//      sum += 2 * getWeightedValue(histogram, i);
//    }
//    sum += getWeightedValue(histogram, 255);
//
//    // figure the scale and create the contrast map lookup table
//    double scale = 255.0 / sum;
//
//    int[] lut = new int[256];
//    lut[0] = 0;
//    sum = getWeightedValue(histogram, 0);
//    for (int i = 1; i < 255; i ++)
//    {
//      double delta = getWeightedValue(histogram, i);;
//      sum += delta;
//      lut[i] = (int)Math.round(sum * scale);
//      sum += delta;
//    }
//    lut[255] = 0;
//
//    // update the image with the new contrast mapping
//    int height = image.getHeight();
//    int width = image.getWidth();
//    for (int y = 0; y < height; y++)
//      for (int x = 0; x < width; x++)
//      {
//        int grayLevel = (int)image.getPixelValue(x, y);
//        image.setPixelValue(x, y, (float)lut[grayLevel]);
//      }
//  }
//
//  /**
//   * Switch for linear vs square root equalization.
//   * Linear will produce a straight line plot of sum of counts versus gray levels,
//   * Square root is not linear but tends to be less severe.
//   *
//   * @author George Booth
//   */
//  private static double getWeightedValue(int[] histogram, int i)
//  {
//    int h = histogram[i];
////  Linear equalition
////    return (double)h;
////  Square root equalization (less severe)
//    return Math.sqrt((double)h);
//  }

//----------------------------------------------------------------------------------------------------------
//   Measure Profiles
//----------------------------------------------------------------------------------------------------------

  /**
   * Makes a smoothed thickness profile for the specified gap.  The profile width is set based on the
   * specified fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad. The smoothedPadProfile is used to create an
   * interpolated background profile. The foreground and background profiles are converted into a "pad
   * thickness profile".  Finally, the thickness profile is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedGapThicknessAcrossProfileWithInterpolatedBackground(
                                                 Image image,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 float[] backgroundProfile,
                                                 RegionOfInterest backgroundProfileRoi,
                                                 float fractionOfPadLengthAcross,
                                                 float fractionOfPadLengthAlong,
                                                 float fractionOfGapPosition,
                                                 boolean postProfileRegions,
                                                 int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Calculate the placement of the pad profile region.
    RegionOfInterest acrossProfileRoi = new RegionOfInterest(locatedJointRoi);
    int acrossProfileRoiLengthAcross = (int)Math.ceil((float)locatedJointRoi.getLengthAcross() * fractionOfPadLengthAlong);
    acrossProfileRoi.setLengthAcross(acrossProfileRoiLengthAcross);

    int acrossProfileRoiLengthAlong = (int)Math.ceil((float)locatedJointRoi.getLengthAlong() * fractionOfPadLengthAcross);
    acrossProfileRoi.setLengthAlong(acrossProfileRoiLengthAlong);
    acrossProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    double newCenter = locatedJointRoi.getMinCoordinateAlong() + (locatedJointRoi.getLengthAlong() * (double)fractionOfGapPosition);
    int offset = (int)(newCenter - locatedJointRoi.getCenterAlong());
    acrossProfileRoi.translateAlongAcross(offset, 0);

    if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
    {
      acrossProfileRoi.setOrientationInDegrees(90);
    }
    else
    {
      acrossProfileRoi.setOrientationInDegrees(0);
    }

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(acrossProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, acrossProfileRoi);
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, acrossProfileRoi);

    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo backgroundProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(acrossProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION);

        _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     backgroundProfileRoiDiagInfo,
                                     padProfileRoiDiagInfo);
      }
    }

    // Create a thickness profile using the pad profile as foreground and the smooothedPadProfile as a background
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
      createThicknessProfileUsingInterpolatedBackground(padGrayLevelProfile, backgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      profileSmoothingLength);

    return new Pair<RegionOfInterest, float[]>(acrossProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
   * Makes a smoothed thickness profile for the specified gap.  The profile width is set based on the
   * specified fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad. The smoothedPadProfile is used to create an
   * interpolated background profile. The foreground and background profiles are converted into a "pad
   * thickness profile".  Finally, the thickness profile is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedGapThicknessAcrossProfileWithAverageBackground(
                                                 Image image,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 float[] smoothedPadProfile,
                                                 float fractionOfPadLengthAcross,
                                                 float fractionOfPadLengthAlong,
                                                 float fractionOfGapPosition,
                                                 boolean postProfileRegions,
                                                 int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the interpad distance.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    // Calculate the placement of the pad profile region.
    RegionOfInterest acrossProfileRoi = new RegionOfInterest(locatedJointRoi);
    int acrossProfileRoiLengthAcross = (int)Math.ceil((float)locatedJointRoi.getLengthAcross() * fractionOfPadLengthAlong);
    acrossProfileRoi.setLengthAcross(acrossProfileRoiLengthAcross);

    int acrossProfileRoiLengthAlong = (int)Math.ceil((float)locatedJointRoi.getLengthAlong() * fractionOfPadLengthAcross);
    acrossProfileRoi.setLengthAlong(acrossProfileRoiLengthAlong);
    acrossProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    double newCenter = locatedJointRoi.getMinCoordinateAlong() + (locatedJointRoi.getLengthAlong() * (double)fractionOfGapPosition);
    int offset = (int)(newCenter - locatedJointRoi.getCenterAlong());
    acrossProfileRoi.translateAlongAcross(offset, 0);

    if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
    {
      acrossProfileRoi.setOrientationInDegrees(90);
    }
    else
    {
      acrossProfileRoi.setOrientationInDegrees(0);
    }

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(acrossProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, acrossProfileRoi);
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, acrossProfileRoi);


    // Calculate the offsets to the background profile width regions (based on some percentage of the IPD +/- half the profile width).
    Subtype subtype = jointInspectionData.getSubtype();
    final float BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS) / 100.f;
    final int BACKGROUND_PROFILE_LENGTH = 3;
    int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
    int backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAlong / 2f) +
                                                    ((float)interPadDistanceInPixels * BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD));

    // Calculate the placement of the background profile regions.
    RegionOfInterest backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
    backgroundProfile1Roi.setLengthAlong(BACKGROUND_PROFILE_LENGTH);
    backgroundProfile1Roi.translateAlongAcross(-backgroundProfileRoiOffset, 0);
    backgroundProfile1Roi.setLengthAcross(acrossProfileRoiLengthAcross);

    RegionOfInterest backgroundProfile2Roi = new RegionOfInterest(locatedJointRoi);
    backgroundProfile2Roi.setLengthAlong(BACKGROUND_PROFILE_LENGTH);
    backgroundProfile2Roi.translateAlongAcross(backgroundProfileRoiOffset, 0);
    backgroundProfile2Roi.setLengthAcross(acrossProfileRoiLengthAcross);

    if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
    {
      backgroundProfile1Roi.setOrientationInDegrees(90);
      backgroundProfile2Roi.setOrientationInDegrees(90);
    }
    else
    {
      backgroundProfile1Roi.setOrientationInDegrees(0);
      backgroundProfile2Roi.setOrientationInDegrees(0);
    }


    // Make sure the background profile ROIs are snapped back onto the image.
    if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region 1") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
    }
    if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile2Roi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region 2") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile2Roi);
    }

    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo backgroundProfile1RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile1Roi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

        MeasurementRegionDiagnosticInfo backgroundProfile2RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile2Roi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(acrossProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION);

        _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     backgroundProfile1RoiDiagInfo,
                                     backgroundProfile2RoiDiagInfo,
                                     padProfileRoiDiagInfo);
      }
    }

    // Measure the background profiles.
    float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
    float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundProfile2Roi);

    // Create an average background gray level profile.
    Assert.expect(backgroundProfile1.length == backgroundProfile2.length);
    float[] averageBackgroundProfile = new float[backgroundProfile1.length];
    for (int i = 0; i < averageBackgroundProfile.length; ++i)
    {
      averageBackgroundProfile[i] = (backgroundProfile1[i] + backgroundProfile2[i]) / 2f;
    }

    if (isAverageBackgroundValueAcrossEnabled(subtype))
    {
      float sumOfValues = 0.0f;
      for (int i = 0; i < averageBackgroundProfile.length; i++)
      {
        sumOfValues += averageBackgroundProfile[i];
      }
      float averageValue = sumOfValues / averageBackgroundProfile.length;
//      System.out.println("across averageValue = " + averageValue);
      for (int i = 0; i < averageBackgroundProfile.length; i++)
      {
        averageBackgroundProfile[i] = averageValue;
      }
    }

    // Create a thickness profile using the pad profile as foreground and the average background profile as background.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(padGrayLevelProfile, averageBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      profileSmoothingLength);

    return new Pair<RegionOfInterest, float[]>(acrossProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
   * Makes a smoothed thickness profile for the specified gap.  The profile width is set based on the
   * specified fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad. The smoothedPadProfile is used to create an
   * interpolated background profile. The foreground and background profiles are converted into a "pad
   * thickness profile".  Finally, the thickness profile is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedGapThicknessAlongProfileWithInterpolatedBackground(
                                                 Image image,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 float[] backgroundProfile,
                                                 RegionOfInterest backgroundProfileRoi,
                                                 float fractionOfPadLengthAcross,
                                                 float fractionOfPadLengthAlong,
                                                 float fractionOfGapPosition,
                                                 boolean postProfileRegions,
                                                 int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Calculate the placement of the pad profile region.
    RegionOfInterest alongProfileRoi = new RegionOfInterest(locatedJointRoi);
    int padProfileRoiLengthAlong = (int)Math.ceil((float)alongProfileRoi.getLengthAlong() * fractionOfPadLengthAlong);
    alongProfileRoi.setLengthAlong(padProfileRoiLengthAlong);

    int padProfileLengthAcross = (int)Math.ceil((float)alongProfileRoi.getLengthAcross() * fractionOfPadLengthAcross);
    alongProfileRoi.setLengthAcross(padProfileLengthAcross);

    alongProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    double newCenter = locatedJointRoi.getMinCoordinateAcross() + (locatedJointRoi.getLengthAcross() * (double)fractionOfGapPosition);
    int offset = (int)(newCenter - locatedJointRoi.getCenterAcross());
    alongProfileRoi.translateAlongAcross(0, offset);

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(alongProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, alongProfileRoi);
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, alongProfileRoi);

    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo backgroundProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(alongProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION);

        _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     backgroundProfileRoiDiagInfo,
                                     padProfileRoiDiagInfo);
      }
    }

    // Create a thickness profile using the pad profile as foreground and the smooothedPadProfile as a background
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
      createThicknessProfileUsingInterpolatedBackground(padGrayLevelProfile, backgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      profileSmoothingLength);

    return new Pair<RegionOfInterest, float[]>(alongProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
   * Makes a smoothed thickness profile for the specified gap.  The profile width is set based on the
   * specified fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad and then taking two background profiles on either
   * side of the pad.  The two background profiles are averaged together and then the foreground and average
   * background profiles are converted into a "pad thickness profile".  Finally, the thickness profile
   * is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedGapThicknessAlongProfileWithAverageBackground(
                                                 Image image,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 float[] smoothedPadProfile,
                                                 float fractionOfPadLengthAcross,
                                                 float fractionOfPadLengthAlong,
                                                 float fractionOfGapPosition,
                                                 boolean postProfileRegions,
                                                 int profileSmoothingLength) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Get the interpad distance.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    // Calculate the placement of the pad profile region.
    RegionOfInterest alongProfileRoi = new RegionOfInterest(locatedJointRoi);
    int padProfileRoiLengthAlong = (int)Math.ceil((float)alongProfileRoi.getLengthAlong() * fractionOfPadLengthAlong);
    alongProfileRoi.setLengthAlong(padProfileRoiLengthAlong);

    int padProfileLengthAcross = (int)Math.ceil((float)alongProfileRoi.getLengthAcross() * fractionOfPadLengthAcross);
    alongProfileRoi.setLengthAcross(padProfileLengthAcross);

    alongProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    double newCenter = locatedJointRoi.getMinCoordinateAcross() + (locatedJointRoi.getLengthAcross() * (double)fractionOfGapPosition);
    int offset = (int)(newCenter - locatedJointRoi.getCenterAcross());
    alongProfileRoi.translateAlongAcross(0, offset);

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(alongProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, alongProfileRoi);
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, alongProfileRoi);

    // Calculate the offsets to the background profile width regions (based on some percentage of the IPD +/- half the profile width).
    Subtype subtype = jointInspectionData.getSubtype();
    final float BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG) / 100.f;
    final int BACKGROUND_PROFILE_WIDTH = 3;
    int locatedJointRoiLengthAcross = locatedJointRoi.getLengthAcross();
    int backgroundProfileRoiOffset = (int)Math.ceil(((float)locatedJointRoiLengthAcross / 2f) +
                                                    ((float)interPadDistanceInPixels * BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD));

    // Calculate the placement of the background profile regions.
    RegionOfInterest backgroundProfile1Roi = new RegionOfInterest(locatedJointRoi);
    backgroundProfile1Roi.setLengthAlong(padProfileRoiLengthAlong);
    backgroundProfile1Roi.translateAlongAcross(0, -backgroundProfileRoiOffset);
    backgroundProfile1Roi.setLengthAcross(BACKGROUND_PROFILE_WIDTH);

    RegionOfInterest backgroundProfile2Roi = new RegionOfInterest(locatedJointRoi);
    backgroundProfile2Roi.setLengthAlong(padProfileRoiLengthAlong);
    backgroundProfile2Roi.translateAlongAcross(0, backgroundProfileRoiOffset);
    backgroundProfile2Roi.setLengthAcross(BACKGROUND_PROFILE_WIDTH);

    // Make sure the background profile ROIs are snapped back onto the image.
    if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile1Roi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region 1") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile1Roi);
    }
    if (AlgorithmUtil.checkRegionBoundaries(backgroundProfile2Roi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Background Region 2") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, backgroundProfile2Roi);
    }

    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo backgroundProfile1RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile1Roi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);
        MeasurementRegionDiagnosticInfo backgroundProfile2RoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(backgroundProfile2Roi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(alongProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION);

        _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     backgroundProfile1RoiDiagInfo,
                                     backgroundProfile2RoiDiagInfo,
                                     padProfileRoiDiagInfo);
      }
    }

    // Measure the background profiles.
    float[] backgroundProfile1 = ImageFeatureExtraction.profile(image, backgroundProfile1Roi);
    float[] backgroundProfile2 = ImageFeatureExtraction.profile(image, backgroundProfile2Roi);

    // Create an average background gray level profile.
    Assert.expect(backgroundProfile1.length == backgroundProfile2.length);
    float[] averageBackgroundProfile = new float[backgroundProfile1.length];
    for (int i = 0; i < averageBackgroundProfile.length; ++i)
    {
      averageBackgroundProfile[i] = (backgroundProfile1[i] + backgroundProfile2[i]) / 2f;
    }

    if (isAverageBackgroundValueAlongEnabled(subtype))
    {
      float sumOfValues = 0.0f;
      for (int i = 0; i < averageBackgroundProfile.length; i++)
      {
        sumOfValues += averageBackgroundProfile[i];
      }
      float averageValue = sumOfValues / averageBackgroundProfile.length;
//      System.out.println("along averageValue = " + averageValue);
      for (int i = 0; i < averageBackgroundProfile.length; i++)
      {
        averageBackgroundProfile[i] = averageValue;
      }
    }

    // Create a thickness profile using the pad profile as foreground and the average background profile as background.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] padThicknessProfileInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(padGrayLevelProfile, averageBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
                                                                                      profileSmoothingLength);

    return new Pair<RegionOfInterest, float[]>(alongProfileRoi, smoothedPadThicknessProfileInMillimeters);
  }

  /**
  * Create a thickness profile by taking samples from the given graylevel profiles and interpolating a background for
  * the thickness profile.
  *
  * This was ported from 5DX FET/HSSOP code written by Patrick Lacz
  *
  * @author George Booth
  */
  private float[] createThicknessProfileUsingInterpolatedBackground(float[] foregroundProfile,
                                                                    float[] backgroundProfile,
                                                                    SolderThickness thicknessTable,
                                                                    int backgroundSensitivityOffset)
  {
    Assert.expect(foregroundProfile != null);
    Assert.expect(foregroundProfile.length > 0);
    Assert.expect(backgroundProfile != null);
    Assert.expect(backgroundProfile.length > 0);
    Assert.expect(thicknessTable != null);

    int foregroundProfileLength = foregroundProfile.length;
    int foregroundProfileCenter = foregroundProfile.length / 2;

    int minIndexOfFirstHalf = ArrayUtil.minIndex(foregroundProfile, 0, foregroundProfileCenter);
    float minOfFirstHalf = foregroundProfile[minIndexOfFirstHalf];

    int minIndexOfSecondHalf = ArrayUtil.minIndex(foregroundProfile, foregroundProfileCenter + 1, foregroundProfileLength - 1);
    float minOfSecondHalf = foregroundProfile[minIndexOfSecondHalf];

    int backgroundProfileLength = backgroundProfile.length;
    int backgroundProfileCenter = backgroundProfile.length / 2;

    float firstHalfEstimateFromBackground = AlgorithmUtil.findPercentile(
      backgroundProfile, 0, backgroundProfileCenter, 0.875f);
    float secondHalfEstimateFromBackground = AlgorithmUtil.findPercentile(
      backgroundProfile, backgroundProfileCenter + 1, backgroundProfileLength - 1, 0.875f);

    float startValue = Math.max(minOfFirstHalf, firstHalfEstimateFromBackground);
    float endValue = Math.max(minOfSecondHalf, secondHalfEstimateFromBackground);

    float[] interpProfile = new float[foregroundProfileLength];
    for (int i = 0; i < foregroundProfileLength; i++)
    {
      float u = (float)(i) / (float)(foregroundProfileLength - 1);

      interpProfile[i] = (1.0f - u) * startValue + u * endValue;
    }

    float[] thicknessProfile = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(foregroundProfile, interpProfile, thicknessTable, backgroundSensitivityOffset);

    return thicknessProfile;
  }

  /**
   * Makes a smoothed thickness profile for the specified joint.  The profile width is set based on the specified
   * fraction of pad length across.
   *
   * This works by taking a foreground profile along the pad and ignoring any background profile information.
   * The thickness profile is smoothed and returned.
   *
   * @author George Booth
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedPadThicknessProfileWithoutProcessingBackground(
      Image image,
      SliceNameEnum sliceNameEnum,
      JointInspectionData jointInspectionData,
      boolean useAlongProfile,
      float fractionOfPadLengthAcross,
      float fractionOfPadLengthAlong,
      boolean postProfileRegions)
  {
    Assert.expect(image != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the located pad ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    Subtype subtype = jointInspectionData.getSubtype();

    // Calculate the placement of the pad profile region.
    RegionOfInterest padProfileRoi = new RegionOfInterest(locatedJointRoi);
    int padProfileRoiLengthAlong = 3;
    if (fractionOfPadLengthAlong > 0.0f)
    {
      padProfileRoiLengthAlong = (int)Math.ceil((float)padProfileRoi.getLengthAlong() * fractionOfPadLengthAlong);
    }
    padProfileRoi.setLengthAlong(padProfileRoiLengthAlong);

    int padProfileLengthAcross = 3;
    if (fractionOfPadLengthAcross > 0.0f)
    {
      padProfileLengthAcross = (int)Math.ceil((float)padProfileRoi.getLengthAcross() * fractionOfPadLengthAcross);
    }
    padProfileRoi.setLengthAcross(padProfileLengthAcross);
    padProfileRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    if (useAlongProfile == false)
    {
      if (locatedJointRoi.getOrientationInDegrees() % 180 == 0)
      {
        padProfileRoi.setOrientationInDegrees(90);
      }
      else
      {
        padProfileRoi.setOrientationInDegrees(0);
      }
    }

    // Shift the pad profile region back into the image if needed.
    if (AlgorithmUtil.checkRegionBoundaries(padProfileRoi,
                                            image,
                                            jointInspectionData.getInspectionRegion(),
                                            "Pad Profile Region") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, padProfileRoi);
    }

    // Measure the pad gray level profile.
    float[] padGrayLevelProfile = ImageFeatureExtraction.profile(image, padProfileRoi);

/*
    // If applicable, post the background/foreground profile ROIs.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions &&
          (useAlongProfile && isAverageBackgroundProfileAlongEnabled(subtype) == false ||
           useAlongProfile == false && isAverageBackgroundProfileAcrossEnabled(subtype) == false))
      {
        MeasurementRegionDiagnosticInfo padProfileRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(padProfileRoi,
                                                MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

        _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     padProfileRoiDiagInfo);
      }
    }
*/
    // Create a thickness profile using the pad profile as foreground and an zero background profile as background.
//    float[] averageBackgroundProfile = new float[padGrayLevelProfile.length];
//    for (int i = 0; i < padGrayLevelProfile.length; i++)
//    {
//      averageBackgroundProfile[i] = 0.0f;
//    }
//    float[] padThicknessProfileInMillimeters =
//        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(padGrayLevelProfile, averageBackgroundProfile);

//    float[] smoothedPadThicknessProfileInMillimeters = ProfileUtil.getSmoothedProfile(padThicknessProfileInMillimeters,
//                                                                                      _ProfileSmoothingLength);

//    return new Pair<RegionOfInterest, float[]>(padProfileRoi, smoothedPadThicknessProfileInMillimeters);
    return new Pair<RegionOfInterest, float[]>(padProfileRoi, padGrayLevelProfile);
  }

//----------------------------------------------------------------------------------------------------------
//   Utility Methods
//-----------------------------------------------------------------------------------------------------------

  /**
   * @author George Booth
   */
  private int getProfileSmoothingLength(Subtype subtype)
  {
    Assert.expect(subtype != null);

    int length = ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_PROFILE_SMOOTHING_LENGTH)).intValue();
    return length;
  }

  /**
   * "Standard", "Average Value Across", "Average Value Along", "Average Image Value", "Nominal Background Value"
   * @author George Booth
   */
  public static boolean isStandardBackgroundEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String standardBackgroundSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE);

    if (standardBackgroundSetting.equals("Standard"))
    {
      return true;
    }
    else if (standardBackgroundSetting.equals("Average Value Across") ||
             standardBackgroundSetting.equals("Average Value Along") ||
             standardBackgroundSetting.equals("Average Image Value") ||
             standardBackgroundSetting.equals("Nominal Background Value"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting: " + standardBackgroundSetting);
      return false;
    }
  }

  /**
   * "Standard", "Average Value Across", "Average Value Along", "Average Image Value", "Nominal Background Value"
   * @author George Booth
   */
  private static boolean isAverageBackgroundValueAcrossEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String standardBackgroundSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE);

    if (standardBackgroundSetting.equals("Average Value Across"))
    {
      return true;
    }
    else if (standardBackgroundSetting.equals("Standard") ||
             standardBackgroundSetting.equals("Average Value Along") ||
             standardBackgroundSetting.equals("Average Image Value") ||
             standardBackgroundSetting.equals("Nominal Background Value"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting: " + standardBackgroundSetting);
      return false;
    }
  }

  /**
   * "Standard", "Average Value Across", "Average Value Along", "Average Image Value", "Nominal Background Value"
   * @author George Booth
   */
  private static boolean isAverageBackgroundValueAlongEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String standardBackgroundSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE);

    if (standardBackgroundSetting.equals("Average Value Along"))
    {
      return true;
    }
    else if (standardBackgroundSetting.equals("Standard") ||
             standardBackgroundSetting.equals("Average Value Across") ||
             standardBackgroundSetting.equals("Average Image Value") ||
             standardBackgroundSetting.equals("Nominal Background Value"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting: " + standardBackgroundSetting);
      return false;
    }
  }

  /**
   * "Standard", "Average Value Across", "Average Value Along", "Average Image Value", "Nominal Background Value"
   * @author George Booth
   */
  private static boolean isAverageBackgroundValueFromImageEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String standardBackgroundSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE);

    if (standardBackgroundSetting.equals("Average Image Value"))
    {
      return true;
    }
    else if (standardBackgroundSetting.equals("Standard") ||
             standardBackgroundSetting.equals("Average Value Across") ||
             standardBackgroundSetting.equals("Average Value Along") ||
             standardBackgroundSetting.equals("Nominal Background Value"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting: " + standardBackgroundSetting);
      return false;
    }
  }

  /**
   * "Standard", "Average Value Across", "Average Value Along", "Average Image Value", "Nominal Background Value"
   * @author George Booth
   */
  private static boolean isNominalBackgroundValueEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String standardBackgroundSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE);

    if (standardBackgroundSetting.equals("Nominal Background Value"))
    {
      return true;
    }
    else if (standardBackgroundSetting.equals("Standard") ||
             standardBackgroundSetting.equals("Average Value Across") ||
             standardBackgroundSetting.equals("Average Value Along") ||
             standardBackgroundSetting.equals("Average Image Value"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique setting: " + standardBackgroundSetting);
      return false;
    }
  }

  /**
   * "On", Off"
   * @author George Booth
   */
  public static boolean isContrastEnhancementEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String contrastEnhancementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_CONTRAST_ENHANCEMENT);

    if (contrastEnhancementSetting.equals("On"))
    {
      return true;
    }
    else if (contrastEnhancementSetting.equals("Off"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Contrast Enhancement setting: " + contrastEnhancementSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private static int getNominalBackgroundValue(Subtype subtype)
  {
    Assert.expect(subtype != null);

    int value = ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NOMINAL_BACKGROUND_VALUE)).intValue();
    return value;
  }

  /**
   * @author George Booth
   */
  private static boolean isAverageBackgroundProfileAlongEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String averageBackgroundSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ALONG);

    if (averageBackgroundSetting.equals("Average"))
    {
      return true;
    }
    else if (averageBackgroundSetting.equals("Interior"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique Along setting: " + averageBackgroundSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  private static boolean isAverageBackgroundProfileAcrossEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String averageBackgroundSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ACROSS);

    if (averageBackgroundSetting.equals("Average"))
    {
      return true;
    }
    else if (averageBackgroundSetting.equals("Interior"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Background Technique Across setting: " + averageBackgroundSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  public static boolean isProfileAcrossMeasurementEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String profileAcrossMeasurementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS);

    if (profileAcrossMeasurementSetting.equals("1") || profileAcrossMeasurementSetting.equals("2") ||
        profileAcrossMeasurementSetting.equals("3"))
    {
      return true;
    }
    else if (profileAcrossMeasurementSetting.equals("None"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected number of profiles across setting: " + profileAcrossMeasurementSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  public static int getNumberOfProfileAcrossMeasurements(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String profileAcrossMeasurementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS);

    if (profileAcrossMeasurementSetting.equals("None"))
      return 0;
    else if (profileAcrossMeasurementSetting.equals("1"))
      return 1;
    else if (profileAcrossMeasurementSetting.equals("2"))
      return 2;
    else if (profileAcrossMeasurementSetting.equals("3"))
      return 3;
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected number of profiles across setting: " + profileAcrossMeasurementSetting);
      return 0;
    }
  }

  /**
   * @author George Booth
   */
  public static int getNumberOfGapAcrossMeasurements(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String gapAcrossMeasurementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ACROSS_MEASUREMENTS);

    if (gapAcrossMeasurementSetting.equals("1"))
      return 1;
    else if (gapAcrossMeasurementSetting.equals("2"))
      return 2;
    else if (gapAcrossMeasurementSetting.equals("3"))
      return 3;
    else if (gapAcrossMeasurementSetting.equals("4"))
      return 4;
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected number of gaps across setting: " + gapAcrossMeasurementSetting);
      return 0;
    }
  }

  /**
   * @author George Booth
   */
  public static float getGapOffsetFractionAcross(Subtype subtype)
  {
    Assert.expect(subtype != null);

    float gapOffsetAcrossPercentSetting =
      ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ACROSS)).floatValue();

    Assert.expect(gapOffsetAcrossPercentSetting >= -75.0f && gapOffsetAcrossPercentSetting <= 75.0f,
                  "Unexpected gap offset percent setting: " + gapOffsetAcrossPercentSetting);

    float gapOffsetFraction = gapOffsetAcrossPercentSetting / 100.0f;
    return gapOffsetFraction;
  }

  /**
   * @author George Booth
   */
  public static boolean isProfileAlongMeasurementEnabled(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String profileAlongMeasurementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS);

    if (profileAlongMeasurementSetting.equals("1") || profileAlongMeasurementSetting.equals("2") ||
        profileAlongMeasurementSetting.equals("3"))
    {
      return true;
    }
    else if (profileAlongMeasurementSetting.equals("None"))
    {
      return false;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected number of profiles along setting: " + profileAlongMeasurementSetting);
      return false;
    }
  }

  /**
   * @author George Booth
   */
  public static int getNumberOfProfileAlongMeasurements(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String profileAlongMeasurementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS);

    if (profileAlongMeasurementSetting.equals("None"))
      return 0;
    else if (profileAlongMeasurementSetting.equals("1"))
      return 1;
    else if (profileAlongMeasurementSetting.equals("2"))
      return 2;
    else if (profileAlongMeasurementSetting.equals("3"))
      return 3;
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected number of profiles along setting: " + profileAlongMeasurementSetting);
      return 0;
    }
  }

  /**
   * @author George Booth
   */
  public static int getNumberOfGapAlongMeasurements(Subtype subtype)
  {
    Assert.expect(subtype != null);

    String gapAlongMeasurementSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ALONG_MEASUREMENTS);

    if (gapAlongMeasurementSetting.equals("None"))
      return 0;
    else if (gapAlongMeasurementSetting.equals("1"))
      return 1;
    else if (gapAlongMeasurementSetting.equals("2"))
      return 2;
    else if (gapAlongMeasurementSetting.equals("3"))
      return 3;
    else if (gapAlongMeasurementSetting.equals("4"))
      return 4;
    else if (gapAlongMeasurementSetting.equals("5"))
      return 5;
    else if (gapAlongMeasurementSetting.equals("6"))
      return 6;
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected number of gaps along setting: " + gapAlongMeasurementSetting);
      return 0;
    }
  }

  /**
   * @author George Booth
   */
  public static float getGapOffsetFractionAlong(Subtype subtype)
  {
    Assert.expect(subtype != null);

    float gapOffsetAlongPercentSetting =
      ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ALONG)).floatValue();

    Assert.expect(gapOffsetAlongPercentSetting >= -75.0f && gapOffsetAlongPercentSetting <= 75.0f,
                  "Unexpected gap offset percent across setting: " + gapOffsetAlongPercentSetting);

    float gapOffsetFraction = gapOffsetAlongPercentSetting / 100.0f;
    return gapOffsetFraction;
  }

//----------------------------------------------------------------------------------------------------------
//   Post Diagnostics
//----------------------------------------------------------------------------------------------------------


  /**
   * @author George Booth
   */
  private void postGapThicknessProfileDiagnostics(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum,
                                                  RegionOfInterest gapProfileRoi,
                                                  float[] gapProfile,
                                                  GapResult gapResult,
                                                  LocalizedString gapProfileLabel)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(gapProfileRoi != null);
    Assert.expect(gapProfile != null);
    Assert.expect(gapProfile.length > 0);

    // Post the pad thickness profile and graphics.

    Collection<DiagnosticInfo> profileAndFeaturesDiagnosticInfos =
        createMeasurementRegionDiagInfosForGapAlongProfile(gapProfileRoi,
                                                           gapResult);


    ProfileDiagnosticInfo labeledProfileDiagnosticInfo =
        createLabeledGapProfileDiagInfo(jointInspectionData,
                                        gapProfile,
                                        gapProfileLabel,
                                        gapResult);
    profileAndFeaturesDiagnosticInfos.add(labeledProfileDiagnosticInfo);

    _diagnostics.postDiagnostics(jointInspectionData.getInspectionRegion(),
                                 sliceNameEnum,
                                 jointInspectionData,
                                 this,
                                 false,
                                 profileAndFeaturesDiagnosticInfos.toArray(new DiagnosticInfo[0]));
  }

  /**
   * Creates a Collection of MeasurementRegionDiagnosticInfo objects which indicate the positions of the gap indexes
   *
   * @author George Booth
   */
  private Collection<DiagnosticInfo> createMeasurementRegionDiagInfosForGapAlongProfile(RegionOfInterest gapThicknessProfileRoi,
                                                                                        GapResult gapResult)
  {
    // get gap results; make sure they don't overlap in graphics
    // Note: some indexes may be "_UNSED_BIN" if a proper gap was not found
    int outerGapLeftIndex = gapResult.getLeftOuterIndex();

    int innerGapLeftIndex = gapResult.getLeftInnerIndex();
    if (outerGapLeftIndex != _UNUSED_BIN && innerGapLeftIndex != _UNUSED_BIN)
    {
      innerGapLeftIndex = Math.max(innerGapLeftIndex, outerGapLeftIndex + 1);
    }

    int innerGapRightIndex = gapResult.getRightInnerIndex();
    if (innerGapLeftIndex != _UNUSED_BIN && innerGapRightIndex != _UNUSED_BIN)
    {
      innerGapRightIndex = Math.max(innerGapRightIndex, innerGapLeftIndex + 1);
    }

    int outerGapRightIndex = gapResult.getRightOuterIndex();
    if (innerGapRightIndex != _UNUSED_BIN && outerGapRightIndex != _UNUSED_BIN)
    {
      outerGapRightIndex = Math.max(outerGapRightIndex, outerGapLeftIndex + 1);
    }

    // Create the ROIs for the areas of interest on the profile.
    RegionOfInterest outerGapLeftRoi = new RegionOfInterest(gapThicknessProfileRoi);
    outerGapLeftRoi.setLengthAlong(1);

    RegionOfInterest innerGapLeftRoi = new RegionOfInterest(gapThicknessProfileRoi);
    innerGapLeftRoi.setLengthAlong(1);

    RegionOfInterest innerGapRightRoi = new RegionOfInterest(gapThicknessProfileRoi);
    innerGapRightRoi.setLengthAlong(1);

    RegionOfInterest outerGapRightRoi = new RegionOfInterest(gapThicknessProfileRoi);
    outerGapRightRoi.setLengthAlong(1);

    // Set the positions of our ROIs based on whether or not a profile reversal is in effect.
    int gapThicknessProfileRoiLengthAlong = gapThicknessProfileRoi.getLengthAlong();
    int gapDistanceToRoiEdge = gapThicknessProfileRoiLengthAlong / 2;

    // Create our MeasurementRegionDiagnosticInfos and add them to our collection.
    Collection<DiagnosticInfo> measurementRegionDiagnosticInfos = new LinkedList<DiagnosticInfo>();

    // don't show outer gap markers if either one was not found
    if (outerGapLeftIndex != _UNUSED_BIN && outerGapRightIndex != _UNUSED_BIN)
    {
      outerGapLeftRoi.translateAlongAcross( -gapDistanceToRoiEdge + outerGapLeftIndex, 0);
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(outerGapLeftRoi,
                                                                               MeasurementRegionEnum.EXPOSED_PAD_ALONG_OUTER_GAP_REGION));

      outerGapRightRoi.translateAlongAcross( -gapDistanceToRoiEdge + outerGapRightIndex, 0);
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(outerGapRightRoi,
                                                                               MeasurementRegionEnum.EXPOSED_PAD_ALONG_OUTER_GAP_REGION));
    }

    // don't show inner gap markers if either one was not found
    if (innerGapLeftIndex != _UNUSED_BIN && innerGapRightIndex != _UNUSED_BIN)
    {
      innerGapLeftRoi.translateAlongAcross( -gapDistanceToRoiEdge + innerGapLeftIndex, 0);
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(innerGapLeftRoi,
                                                                               MeasurementRegionEnum.EXPOSED_PAD_ALONG_INNER_GAP_REGION));

      innerGapRightRoi.translateAlongAcross( -gapDistanceToRoiEdge + innerGapRightIndex, 0);
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(innerGapRightRoi,
                                                                               MeasurementRegionEnum.EXPOSED_PAD_ALONG_INNER_GAP_REGION));
    }

    return measurementRegionDiagnosticInfos;
  }

  /**
   * Creates a 'labeled' version of the gap profile that accentuates the features we measured.
   *
   * @author George Booth
   */
  private ProfileDiagnosticInfo createLabeledGapProfileDiagInfo(JointInspectionData jointInspectionData,
                                                                           float[] gapThicknessProfile,
                                                                           LocalizedString gapProfileLabel,
                                                                           GapResult gapResult)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(gapThicknessProfile != null);
    Assert.expect(gapThicknessProfile.length > 0);
    Assert.expect(gapProfileLabel != null);
    Assert.expect(gapResult != null);

    Subtype subtype = jointInspectionData.getSubtype();
    final float MAX_EXPECTED_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS);
    float profileMin = 0f;
    float profileMax = MAX_EXPECTED_THICKNESS * 1.5f;
//    LocalizedString bottomGapProfileLabelLocalizedString =
//        new LocalizedString("ALGDIAG_EXPOSED_PAD_MEASUREMENT_BOTTOM_GAP_PROFILE_LABEL_KEY",
//                            null);
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        createProfileSubrangeToMeasurementRegionEnumMap(gapThicknessProfile,
                                                        gapResult);
    ProfileDiagnosticInfo labeledGapProfileDiagInfo =
        ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                         ProfileTypeEnum.SOLDER_PROFILE,
                                                                         gapProfileLabel,
                                                                         profileMin,
                                                                         profileMax,
                                                                         gapThicknessProfile,
                                                                         profileSubrangeToMeasurementRegionEnumMap,
                                                                         MeasurementUnitsEnum.MILLIMETERS);

    return labeledGapProfileDiagInfo;
  }

  /**
   * @author George Booth
   */
  protected Map<Pair<Integer, Integer>, MeasurementRegionEnum>
      createProfileSubrangeToMeasurementRegionEnumMap(float[] gapThicknessProfile,
                                                      GapResult gapResult)
  {
    Assert.expect(gapThicknessProfile != null);

    int gapSearchStartIndex = gapResult.getSearchStartIndex();
    int gapSearchEndIndex = gapResult.getSearchEndIndex();
    int outerGapLeftIndex = gapResult.getLeftOuterIndex();
    int innerGapLeftIndex = gapResult.getLeftInnerIndex();
    int innerGapRightIndex = gapResult.getRightInnerIndex();
    int outerGapRightIndex = gapResult.getRightOuterIndex();

    // Create a ProfileDiagnosticInfo that accentuates the points of interest in the gap thickness profile.
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer, Integer>, MeasurementRegionEnum>();
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, gapThicknessProfile.length),
                                                  MeasurementRegionEnum.EXPOSED_PAD_PAD_REGION);

    if (gapSearchStartIndex != _UNUSED_BIN && gapSearchEndIndex != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(gapSearchStartIndex, gapSearchEndIndex),
                                                    MeasurementRegionEnum.EXPOSED_PAD_GAP_REGION);
    }
    // don't show outer gap markers if either one was not found
    if (outerGapLeftIndex != _UNUSED_BIN && outerGapRightIndex != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(outerGapLeftIndex, outerGapLeftIndex + 1),
                                                    MeasurementRegionEnum.EXPOSED_PAD_ALONG_OUTER_GAP_REGION);

      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(outerGapRightIndex, outerGapRightIndex + 1),
                                                    MeasurementRegionEnum.EXPOSED_PAD_ALONG_OUTER_GAP_REGION);
    }
    // don't show inner gap markers if either one was not found
    if (innerGapLeftIndex != _UNUSED_BIN && innerGapRightIndex != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(innerGapLeftIndex, innerGapLeftIndex + 1),
                                                    MeasurementRegionEnum.EXPOSED_PAD_ALONG_INNER_GAP_REGION);

      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(innerGapRightIndex, innerGapRightIndex + 1),
                                                    MeasurementRegionEnum.EXPOSED_PAD_ALONG_INNER_GAP_REGION);
    }

    return profileSubrangeToMeasurementRegionEnumMap;
  }

  /**
   * Gets all north gap measurements
   *
   * @author George Booth
   */
  public static List<GapJointMeasurementGroup> getNorthGapMeasurementList(JointInspectionData jointInspectionData,
                                                                          SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_northGapMeasurementEnumList != null);

    return getGapMeasurementList(jointInspectionData, sliceNameEnum, _northGapMeasurementEnumList);
  }

  /**
   * Gets all middle gap measurements
   *
   * @author George Booth
   */
  public static List<GapJointMeasurementGroup> getMiddleGapMeasurementList(JointInspectionData jointInspectionData,
                                                                           SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_middleGapMeasurementEnumList != null);

    return getGapMeasurementList(jointInspectionData, sliceNameEnum, _middleGapMeasurementEnumList);
  }

  /**
   * Gets all south gap measurements
   *
   * @author George Booth
   */
  public static List<GapJointMeasurementGroup> getSouthGapMeasurementList(JointInspectionData jointInspectionData,
                                                                          SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_southGapMeasurementEnumList != null);

    return getGapMeasurementList(jointInspectionData, sliceNameEnum, _southGapMeasurementEnumList);
  }

  /**
   * Gets all west gap measurements
   *
   * @author George Booth
   */
  public static List<GapJointMeasurementGroup> getWestGapMeasurementList(JointInspectionData jointInspectionData,
                                                                         SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_westGapMeasurementEnumList != null);

    return getGapMeasurementList(jointInspectionData, sliceNameEnum, _westGapMeasurementEnumList);
  }

  /**
   * Gets all center gap measurements
   *
   * @author George Booth
   */
  public static List<GapJointMeasurementGroup> getCenterGapMeasurementList(JointInspectionData jointInspectionData,
                                                                           SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_centerGapMeasurementEnumList != null);

    return getGapMeasurementList(jointInspectionData, sliceNameEnum, _centerGapMeasurementEnumList);
  }

  /**
   * Gets all east gap measurements
   *
   * @author George Booth
   */
  public static List<GapJointMeasurementGroup> getEastGapMeasurementList(JointInspectionData jointInspectionData,
                                                                         SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_eastGapMeasurementEnumList != null);

    return getGapMeasurementList(jointInspectionData, sliceNameEnum, _eastGapMeasurementEnumList);
  }

  /**
   * Gets all gap measurements for a specified list
   *
   * @author George Booth
   */
  private static List<GapJointMeasurementGroup> getGapMeasurementList(JointInspectionData jointInspectionData,
                                                                      SliceNameEnum sliceNameEnum,
                                                                      List<GapMeasurementEnumGroup> gapMeasurementEnumList)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(gapMeasurementEnumList != null);

    List<GapJointMeasurementGroup> gapJointMeasurementGroupList = new ArrayList<GapJointMeasurementGroup>();

    // Pull the measurements out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    for (GapMeasurementEnumGroup gapMeasurementEnumGroup : gapMeasurementEnumList)
    {
      GapJointMeasurementGroup gapJointMeasurementGroup = new GapJointMeasurementGroup();

      // all enabled gaps have the filled gap thickness measurement
      MeasurementEnum measurementEnumToGet = gapMeasurementEnumGroup.getFilledGapThicknessMeasurementEnum();
      if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, measurementEnumToGet))
      {
        gapJointMeasurementGroup.setFilledGapThicknessJointMeasurement(
            jointInspectionResult.getJointMeasurement(sliceNameEnum, measurementEnumToGet));

        // if the filled gap thickness measurement exceeded the threshold, the gap is passing
        // and no other measurements exist.
        measurementEnumToGet = gapMeasurementEnumGroup.getInnerGapMeasurementEnum();
        if (jointInspectionResult.hasJointMeasurement(sliceNameEnum, measurementEnumToGet))
        {
          gapJointMeasurementGroup.setInnerGapJointMeasurement(
              jointInspectionResult.getJointMeasurement(sliceNameEnum, measurementEnumToGet));

          // if the innger gap measurement exists, they all should exist.
          measurementEnumToGet = gapMeasurementEnumGroup.getOuterGapMeasurementEnum();
          Assert.expect(jointInspectionResult.hasJointMeasurement(sliceNameEnum, measurementEnumToGet));

          gapJointMeasurementGroup.setOuterGapJointMeasurement(
              jointInspectionResult.getJointMeasurement(sliceNameEnum, measurementEnumToGet));

          measurementEnumToGet = gapMeasurementEnumGroup.getGapRatioMeasurementEnum();
          Assert.expect(jointInspectionResult.hasJointMeasurement(sliceNameEnum, measurementEnumToGet));

          gapJointMeasurementGroup.setGapRatioJointMeasurement(
              jointInspectionResult.getJointMeasurement(sliceNameEnum, measurementEnumToGet));

          measurementEnumToGet = gapMeasurementEnumGroup.getGapThicknessMeasurementEnum();
          Assert.expect(jointInspectionResult.hasJointMeasurement(sliceNameEnum, measurementEnumToGet));

          gapJointMeasurementGroup.setGapThicknessJointMeasurement(
              jointInspectionResult.getJointMeasurement(sliceNameEnum, measurementEnumToGet));
        }

        gapJointMeasurementGroupList.add(gapJointMeasurementGroup);
      }
    }

    return gapJointMeasurementGroupList;
  }

  /**
   * Checks if the average background value measurement exists
   *
   * @author George Booth
   */
  public static boolean hasAverageBackgroundValueMeasurement(JointInspectionData jointInspectionData,
                                                             SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    return jointInspectionResult.hasJointMeasurement(sliceNameEnum, MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE);
  }

  /**
   * Gets the average background value measurement
   *
   * @author George Booth
   */
  public static JointMeasurement getAverageBackgroundValueMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    // caller should check this
    Assert.expect(hasAverageBackgroundValueMeasurement(jointInspectionData, sliceNameEnum));

    // Pull the measurement out of the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement measurement =
        jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE);

    return measurement;
  }


  /**
   * Gap measurement information.  This list has the gap name, start and end of the gap area, gap measurement enums
   * and the the gap profile name for one to six gaps.
   *
   * The gapMeasurementEnumList parameter  is ordered as:
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_xxx_1
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_xxx_1
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_xxx_1
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_xxx_1
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_INNER_GAP_xxx_2
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_OUTER_GAP_xxx_2
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_RATIO_xxx_2
   *    MeasurementEnum.EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_xxx_2
   *      etc
   *
   * @author George Booth
   */
  private List<GapInformation> createGapInformationList(Subtype subtype,
                                                        float[] profile,
                                                        int numberOfGaps,
                                                        float gapOffsetFraction,
                                                        String gapName,
                                                        List<GapMeasurementEnumGroup> gapMeasurementEnumGroupList,
                                                        String localizedStringKey)
  {
    Assert.expect(subtype != null);
    Assert.expect(profile != null && profile.length > 0);
    Assert.expect(numberOfGaps >= 0);
    Assert.expect(gapOffsetFraction >= -1.0f && gapOffsetFraction <= 1.0f);
    Assert.expect(gapName != null);
    Assert.expect(gapMeasurementEnumGroupList != null);
    Assert.expect(localizedStringKey != null);

    List<GapInformation> gapInformationList = new ArrayList<GapInformation>();

    if (numberOfGaps == 2 && gapOffsetFraction != 0.0f)
    {
      // gap is offset - it is located "gapOffsetFraction" from the center of the segment
      //
      // Example: Gap is +50% frpm center of segment
      //
      //            Segment 1                       Segment 2
      // 0 10 20 30 ...              profileLen/2                    profileLen-1
      // +---------------+---------------+--------------------------------+
      // |      +-+      .               |                      +-+       |
      // |      |G|      .               |                      |G|       |
      // |      +-+      .               |                      +-+       |
      // +-------+-------+-------+-------+-------+-------+-------+--------+
      //       +50%     0%     -50%            -50%      0%    +50%
      //
      //  <----search area----->
      // the gap search will be reduced by the offset.
      //
      int gapSegmentHalfSize = profile.length / 4;
      int middle = profile.length / 2;

      int segmentMiddle = middle - gapSegmentHalfSize;
      int offset = (int)((float)gapSegmentHalfSize * gapOffsetFraction);
      int gapCenter = segmentMiddle - offset;
      int searchStart = 0;
      int searchEnd = middle;
      if (offset > 0.0F)
      {
        searchEnd = middle - offset;
      }
      else
      {
        searchStart = -offset;
      }
      String gapTag = gapName + " 1";
      GapInformation gap1Information = new GapInformation(
          gapName + "1",
          searchStart, gapCenter, searchEnd,
          gapMeasurementEnumGroupList.get(0),
          new LocalizedString(localizedStringKey, new Object[]
                             {"1"}));
      gapInformationList.add(gap1Information);

      gapCenter = middle + segmentMiddle + offset;
      searchStart = middle;
      searchEnd = profile.length - 1;
      if (offset > 0.0F)
      {
        searchStart = middle + offset;
      }
      else
      {
        searchEnd = profile.length - 1 + offset;
      }
      gapTag = gapName + " 2";
      GapInformation gap2Information = new GapInformation(
          gapTag,
          searchStart, gapCenter, searchEnd,
          gapMeasurementEnumGroupList.get(1),
          new LocalizedString(localizedStringKey, new Object[]
                             {"2"}));
      gapInformationList.add(gap2Information);
    }
    else
    {
      int gapSegmentSize = profile.length / numberOfGaps;
      int start = 0;
      int center = start + (gapSegmentSize / 2);
      int end = start + gapSegmentSize;
      for (int i = 0; i < numberOfGaps; i++)
      {
        if (i + 1 == numberOfGaps)
        {
          // make sure last segment stops at last profile bin
          end = profile.length - 1;
        }
        String gapTag = gapName + " " + (i + 1);
        GapInformation gapInformation = new GapInformation(
            gapTag,
            start, center, end,
            gapMeasurementEnumGroupList.get(i),
            new LocalizedString(localizedStringKey, new Object[]
                                {"" + (i + 1)}));
        gapInformationList.add(gapInformation);

        start = end + 1;
        center += gapSegmentSize;
        end += gapSegmentSize;
      }
    }
    return gapInformationList;
  }

  /**
   * debug gap information list
   *
   * @author George Booth
   */
  private void debugGapInformationList(List<GapInformation> gapInformationList)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (!Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.EXPOSED_PAD_MEASUREMENT_DEBUG))
      return;

    Assert.expect(gapInformationList != null);

    int index = 0;
    for (GapInformation gapInformation : gapInformationList)
    {
      System.out.println("gap information " + index++);
      System.out.println("  Gap name = " + gapInformation.getGapName());
      System.out.println("  Start index = " + gapInformation.getStartIndex() +
                         ",  End index = " + gapInformation.getEndIndex());
      System.out.println("  Inner Meas Enum = " + gapInformation.getInnerGapMeasurementEnum().getName());
      System.out.println("  Outer Meas Enum = " + gapInformation.getOuterGapMeasurementEnum().getName());
      System.out.println("  Ratio Meas Enum = " + gapInformation.getGapRatioMeasurementEnum().getName());
      System.out.println("  Gap Profile Label = " +
                         StringLocalizer.keyToString(gapInformation.getGapProfileLabel()));
    }
  }

  /**
   * Voiding Measurements
   *
   * @author George Booth
   */
  static void ProcessJointVoidMeasurements(Image inspectionImage,
                                           Image backgroundImage,
                                           int orientation)
  {
/*
    string learningKey = getIdentifierForLearning(dataPtr);

    float voidingPercentage = 0.f;

    float learnPercentile = 0.20f; // chosen experimentally by Patrick and Jeremy.

    if (WritingLearnedSubtypeData(callingAlgHdr))
    {

      //    float learnPercentile = FloatThreshValue( callingAlgHdr->ThresholdsPtr, JointSubtype( dataPtr ), LEARN_PERCENT ) / 100.f;
      // We are performing subtype learning, not a normal inspection
      ExpectedImageVoidingAlgorithm voidingAlgorithm;
      voidingAlgorithm.learnExpectedImage(dataPtr,
        learningKey.c_str(),
        inspectionImage,
        backgroundImage,
        PadLoc_B(dataPtr),
        orientation,
        learnPercentile,
        callingAlgHdr);
    }
    else
    {
      // not performing learning, normal inspection
      ExpectedImageVoidingAlgorithm voidingAlgorithm;
      t_BorderRegion padBorderRoi = PadLoc_B(dataPtr);

      if (voidingAlgorithm.canInspect(learningKey.c_str(), padBorderRoi, learnPercentile) == false)
      {
        // oops, we can't inspect this type. Do something different?
        AutoPrintf(callingAlgHdr, "Voiding: Cannot inspect this joint, please re-run subtype learning.\n");
      }
      else
      {
        float voidingThreshold = FloatThreshValue(callingAlgHdr->ThresholdsPtr, JointSubtype(dataPtr), VOIDING_THRESHOLD);
        float solderThicknessThreshold = FloatThreshValue(callingAlgHdr->ThresholdsPtr, JointSubtype(dataPtr), SOLDER_THRESH);

        voidingAlgorithm.findVoidingPixels(
          dataPtr,
          learningKey.c_str(),
          inspectionImage,
          backgroundImage,
          padBorderRoi,
          orientation,
          //ExpectedImageVoidingAlgorithm::GRAYLEVEL_DIFFERENCE,
          ExpectedImageVoidingAlgorithm::PERCENT_OF_INVERSE_GRAYLEVEL,
          voidingThreshold,
          solderThicknessThreshold,
          callingAlgHdr,
          learnPercentile);

        // show some diagnostics  - the current expected image.
        if ( callingAlgHdr->DiagFlags & DRAW_DYNAMIC_PLOTS )
        {
          ///* @todo : reuse the computed expected graylevel image.

          t_FBPtr copyOfInspectionImage = AllocateFrameBuffer(inspectionImage->Width, inspectionImage->Height);
          t_PrimBorderData primBorderData;
          primBorderData.Orientation = 0;
          primBorderData.NumRegions = 1;
          primBorderData.BorderRegion1.X1 = 0;
          primBorderData.BorderRegion1.Y1 = 0;
          primBorderData.BorderRegion1.X2 = inspectionImage->Width;
          primBorderData.BorderRegion1.Y2 = inspectionImage->Height;
          primBorderData.BorderRegion1.Shape = RECTANGLE;

          CopyFB_B(inspectionImage, &primBorderData, copyOfInspectionImage);

          // modify the copied image to show the diagnostics.

          t_CenterRegion padCenterRoi = PadLoc_C(dataPtr);
          voidingAlgorithm.copyExpectedGraylevelImageIntoInspectionImage(copyOfInspectionImage,
            padCenterRoi,
            orientation);

          FGDisplayFB( copyOfInspectionImage );
          AutoDrawRegion_C(callingAlgHdr, copyOfInspectionImage, &padCenterRoi, BLUE, orientation);
          AutoPrintf(callingAlgHdr, "This image shows the expected solder thickness for this joint.\n");
          AutoPrintf(callingAlgHdr, "The pixels shown in BLACK are not inspected for voiding. Adjust the \"Minimum Voiding Region Thickness\" setting to modify this region.\n");
          AutoPause( callingAlgHdr, ACCEPT );
          FreeFrameBuffer(copyOfInspectionImage);

          FGDisplayFB( inspectionImage );
          AutoPause( callingAlgHdr, ACCEPT );
        }

        float milsAreaThreshold = FloatThreshValue( callingAlgHdr->ThresholdsPtr, JointSubtype( dataPtr ), MIN_VOID_AREA );
        float milsToPixelsRatio = MilsToPixels( dataPtr, 1.0f);
        float squareMilInPixels = milsToPixelsRatio*milsToPixelsRatio;
        float pixelAreaThreshold = milsAreaThreshold * squareMilInPixels;
        //float hysteresisThreshold = FloatThreshValue(callingAlgHdr->ThresholdsPtr, JointSubtype(dataPtr), HYSTERESIS_THRESH);
        //float slopeThreshold = FloatThreshValue(callingAlgHdr->ThresholdsPtr, JointSubtype(dataPtr), SLOPE_THRESH);

        voidingAlgorithm.removeVoidsSmallerThanNPixels(pixelAreaThreshold);
        //voidingAlgorithm.applyHysteresis(hysteresisThreshold, slopeThreshold);

        vector<pair<int, int> > voidingPixelLocations;
        int numberOfVoidingPixels = voidingAlgorithm.getVoidingPixels(voidingPixelLocations);
        voidingPercentage = 100.0f*(float)numberOfVoidingPixels / (float)(voidingAlgorithm.getNumberOfTestedPixels());

        if ( callingAlgHdr->DiagFlags & DRAW_DYNAMIC_PLOTS )
        {
          if (numberOfVoidingPixels > 0)
          {
            t_ColorPoint *colorPts = (t_ColorPoint *)SYS_malloc( numberOfVoidingPixels * sizeof(t_ColorPoint) );
            t_ColorPoint *colorPtIterator = colorPts;

            if ( colorPts != NULL )
            {
              for (int i = 0; i < numberOfVoidingPixels; i++ )
              {
                colorPtIterator->p.X = padBorderRoi.X1 + voidingPixelLocations[i].first;
                colorPtIterator->p.Y = padBorderRoi.Y1 + voidingPixelLocations[i].second;
                colorPtIterator->color = CYAN;
                colorPtIterator++;
              }
              DrawPointGroup( inspectionImage, numberOfVoidingPixels, colorPts, orientation );

              SYS_free( colorPts );
            }
            else
            {
              AlgWarning( dataPtr, "Error allocating memory for drawing void pixels\n");
            }
          }
        }

        AutoPause( callingAlgHdr, ACCEPT );

        voidingAlgorithm.clearInspectionResult();
      }
    }

    t_FETSPCData * SPCDataPtr = &FETSPCData [SYS_ProcId()];
    SPCDataPtr->VoidPercent = voidingPercentage;
*/
  }


  private void debug(String message)
  {
    //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.EXPOSED_PAD_MEASUREMENT_DEBUG))
      System.out.println(_me + message);
  }

  private static void writeProfileToCSVFile(String filePath,
                                     String profileName1, float[] profile1, float scaleFactor1,
                                     String profileName2, float[] profile2, float scaleFactor2)
  {
    String delimiter = ", ";

    // do not append
    try
    {
      FileWriterUtil fileWriter = new FileWriterUtil(filePath, false);
      fileWriter.open();

      String title = "Profile";

      // report title and date
      Date date = new Date();
      if (title != null)
      {
        fileWriter.writeln(title + delimiter + date);
      }
      else
      {
        fileWriter.writeln(delimiter + delimiter + date);
      }
      fileWriter.writeln();

      fileWriter.writeln(profileName1);
      fileWriter.writeln(profileName2);
      fileWriter.writeln();

      // column header line
      StringBuffer headerLine = new StringBuffer();
      headerLine.append("index, value1, value2");
      fileWriter.writeln(headerLine.toString());

      // profile data
      int len = profile1.length;
      if (profile2.length > len)
        len = profile2.length;
      float last1 = profile1[0];
      float last2 = profile2[0];
      for (int i = 0; i < profile1.length; i++)
      {
        StringBuffer reportLine = new StringBuffer();
        reportLine.append(i + delimiter);

        if (i < profile1.length)
        {
          last1 = profile1[i] * scaleFactor1;
          reportLine.append(last1 + delimiter);
        }
        else
          reportLine.append(last1 + delimiter);

        if (i < profile2.length)
        {
          last2 = profile2[i] * scaleFactor2;
          reportLine.append(last2 + delimiter);
        }
        else
          reportLine.append(last2);

        fileWriter.writeln(reportLine.toString());
      }

      fileWriter.close();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

  private static void writeProfileToCSVFile(String filePath,
                                     String profileName1, float[] profile1, float scaleFactor1,
                                     String profileName2, float[] profile2, float scaleFactor2,
                                     String profileName3, float[] profile3, float scaleFactor3,
                                     String profileName4, float[] profile4, float scaleFactor4)
  {
    Assert.expect(profile2.length == profile1.length);
    Assert.expect(profile3.length == profile1.length);
    Assert.expect(profile4.length == profile1.length);
    String delimiter = ", ";

    // do not append
    try
    {
      FileWriterUtil fileWriter = new FileWriterUtil(filePath, false);
      fileWriter.open();

      String title = "Profile";

      // report title and date
      Date date = new Date();
      if (title != null)
      {
        fileWriter.writeln(title + delimiter + date);
      }
      else
      {
        fileWriter.writeln(delimiter + delimiter + date);
      }
      fileWriter.writeln();

      fileWriter.writeln(profileName1);
      fileWriter.writeln(profileName2);
      fileWriter.writeln(profileName3);
      fileWriter.writeln(profileName4);
      fileWriter.writeln();

      // column header line
      StringBuffer headerLine = new StringBuffer();
      headerLine.append("index, value1, value2, value3, value4");
      fileWriter.writeln(headerLine.toString());

      // profile data
      for (int i = 0; i < profile1.length; i++)
      {
        StringBuffer reportLine = new StringBuffer();
        reportLine.append(i + delimiter);
        reportLine.append(profile1[i] * scaleFactor1 + delimiter);
        reportLine.append(profile2[i] * scaleFactor2 + delimiter);
        reportLine.append(profile3[i] * scaleFactor3 + delimiter);
        reportLine.append(profile4[i] * scaleFactor4);
        fileWriter.writeln(reportLine.toString());
      }

      fileWriter.close();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

}
