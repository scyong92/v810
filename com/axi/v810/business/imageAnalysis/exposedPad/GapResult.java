package com.axi.v810.business.imageAnalysis.exposedPad;

import com.axi.util.Assert;
import com.axi.v810.business.imageAnalysis.AlgorithmUtil;
/**
 * <p>Title: GapResult</p>
 *
 * <p>Description: Utility class to store gap measurement results</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 */
public class GapResult
{
  private String _gapName;
  private boolean _gapSearchSet;
  private int _startIndex;
  private int _endIndex;
  private int _leftOuterIndex;
  private int _rightOuterIndex;
  private int _leftInnerIndex;
  private int _rightInnerIndex;
  private float _leftOuterGapThicknessInMillimeters;
  private float _rightOuterGapThicknessInMillimeters;
  private float _filledGapThicknessInMillimeters;
  private float _filledGapThresholdInMillimeters;

  private static final int _UNUSED_BIN = -1;
  final float MILS_PER_MILLIMETER = 39.3700787F;


  /**
   * @author George Booth
   */
  public GapResult()
  {
    _gapName = "";
    _gapSearchSet = false;
    _startIndex = _UNUSED_BIN;
    _endIndex = _UNUSED_BIN;
    _leftOuterIndex = _UNUSED_BIN;
    _rightOuterIndex = _UNUSED_BIN;
    _leftInnerIndex = _UNUSED_BIN;
    _rightInnerIndex = _UNUSED_BIN;
    _leftOuterGapThicknessInMillimeters = -1.0f;
    _rightOuterGapThicknessInMillimeters = -1.0f;
    _filledGapThicknessInMillimeters = -1.0f;
    _filledGapThresholdInMillimeters = -1.0f;
  }

  /**
   * For testing only
   *
   * @author George Booth
   */
  public GapResult(int leftOuterIndex, int rightOuterIndex)
  {
    _gapName = "Test";
    _gapSearchSet = true;
    _startIndex = leftOuterIndex - 10;
    _endIndex = rightOuterIndex + 10;
    _leftOuterIndex = leftOuterIndex;
    _rightOuterIndex = rightOuterIndex;
    _leftInnerIndex = leftOuterIndex + 10;
    _rightInnerIndex = rightOuterIndex - 10;
  }

  /**
   * @author George Booth
   */
  public void setGapName(String name)
  {
    _gapName = name;
  }

  /**
   * @author George Booth
   */
  public String getGapName()
  {
    return _gapName;
  }

  /**
   * @author George Booth
   */
  public void setGapSearchRange(int startIndex, int endIndex)
  {
    Assert.expect(startIndex > -1);
    Assert.expect(endIndex > -1);

    _startIndex = startIndex;
    _endIndex = endIndex;
    _gapSearchSet = true;
  }

  /**
   * @author George Booth
   */
  public boolean isGapSearchSet()
  {
    return _gapSearchSet;
  }

  /**
   * @author George Booth
   */
  public int getSearchStartIndex()
  {
    Assert.expect(_gapSearchSet == true);

    return _startIndex;
  }

  /**
   * @author George Booth
   */
  public int getSearchEndIndex()
  {
    Assert.expect(_gapSearchSet == true);

    return _endIndex;
  }

  /**
   * @author George Booth
   */
  public int getSearchCenterIndex()
  {
    Assert.expect(_gapSearchSet == true);

    return (_endIndex - _startIndex) / 2 + _startIndex;
  }

  /**
   * @author George Booth
   */
  public int getOuterGapCenter()
  {
    Assert.expect(_leftOuterIndex <= _rightOuterIndex);

    return (_rightInnerIndex - _leftInnerIndex) / 2 + _leftInnerIndex;
  }

  /**
   * @author George Booth
   */
  public int getInnerGapCenter()
  {
    Assert.expect(_leftInnerIndex <= _rightInnerIndex);

    return (_rightInnerIndex - _leftInnerIndex) / 2 + _leftInnerIndex;
  }

  /**
   * @author George Booth
   */
  public void setLeftOuterIndex(int leftOuterIndex)
  {
    _leftOuterIndex = leftOuterIndex;
  }

  /**
   * @author George Booth
   */
  public int getLeftOuterIndex()
  {
    return _leftOuterIndex;
  }


  /**
   * @author George Booth
   */
  public void setRightOuterIndex(int rightOuterIndex)
  {
    _rightOuterIndex = rightOuterIndex;
  }

  /**
   * @author George Booth
   */
  public int getRightOuterIndex()
  {
    return _rightOuterIndex;
  }

  /**
   * @author George Booth
   */
  public void setLeftInnerIndex(int leftInnerIndex)
  {
    _leftInnerIndex = leftInnerIndex;
  }

  /**
   * @author George Booth
   */
  public int getLeftInnerIndex()
  {
    return _leftInnerIndex;
  }

  /**
   * @author George Booth
   */
  public void setRightInnerIndex(int rightInnerIndex)
  {
    _rightInnerIndex = rightInnerIndex;
  }

  /**
   * @author George Booth
   */
  public int getRightInnerIndex()
  {
    return _rightInnerIndex;
  }

  /**
   * @author George Booth
   */
  public float getLengthOfOuterGapInPixels()
  {
    if (_leftOuterIndex == _UNUSED_BIN || _rightOuterIndex == _UNUSED_BIN)
      return 0.0f;
    else
      return Math.max((float)(_rightOuterIndex - _leftOuterIndex), 0.f);
  }

  /**
   * @author George Booth
   */
  public float getLengthOfOuterGapInMillimeters()
  {
    return AlgorithmUtil.getMillimetersPerPixel() * getLengthOfOuterGapInPixels();
  }

  /**
   * @author George Booth
   */
  public float getLengthOfInnerGapInPixels()
  {
    if (_leftInnerIndex == _UNUSED_BIN || _rightInnerIndex == _UNUSED_BIN)
      return 0.0f;
    else
      return Math.max((float)(_rightInnerIndex - _leftInnerIndex), 0.f);
  }

  /**
   * @author George Booth
   */
  public float getLengthOfInnerGapInMillimeters()
  {
    return AlgorithmUtil.getMillimetersPerPixel() * getLengthOfInnerGapInPixels();
  }

  /**
   * @author George Booth
   */
  public void setRightOuterGapThicknessInMillimeters(float thicknessInMillimeters)
  {
    _rightOuterGapThicknessInMillimeters = thicknessInMillimeters;
  }

  /**
   * @author George Booth
   */
  public void setLeftOuterGapThicknessInMillimeters(float thicknessInMillimeters)
  {
    _leftOuterGapThicknessInMillimeters = thicknessInMillimeters;
  }

  /**
   * @author George Booth
   */
  public float getAverageOuterGapThicknessInMillimeters()
  {
    float averageGapThickness = (_rightOuterGapThicknessInMillimeters + _leftOuterGapThicknessInMillimeters) / 2.0f;
    return averageGapThickness;
  }

  /**
   * @author George Booth
   */
  public float getMinimumOuterGapThicknessInMillimeters()
  {
    if (_rightOuterGapThicknessInMillimeters < _leftOuterGapThicknessInMillimeters)
      return _rightOuterGapThicknessInMillimeters;
    else
      return _leftOuterGapThicknessInMillimeters;
  }

  /**
   * @author George Booth
   */
  public void setFilledGapThicknessInMillimeters(float thicknessInMillimeters)
  {
    _filledGapThicknessInMillimeters = thicknessInMillimeters;
  }

  /**
   * @author George Booth
   */
  public float getFilledGapThicknessInMillimeters()
  {
    return _filledGapThicknessInMillimeters;
  }

  /**
   * @author George Booth
   */
  public void setFilledGapThresholdInMillimeters(float thresholdInMillimeters)
  {
    _filledGapThresholdInMillimeters = thresholdInMillimeters;
  }

  /**
   * @author George Booth
   */
  public float getFilledGapThresholdInMillimeters()
  {
    return _filledGapThresholdInMillimeters;
  }

  /**
   * @author George Booth
   */
  public boolean isFilledGapPassing()
  {
    Assert.expect(_filledGapThicknessInMillimeters > -1.0f);
    Assert.expect(_filledGapThresholdInMillimeters > -1.0f);
    return _filledGapThicknessInMillimeters >= _filledGapThresholdInMillimeters;
  }



  /**
   * debug only
   *
   * @author George Booth
   */
  public void outputGapMeasurements(String message)
  {
    System.out.println(message + "Gap Measurements: ");
    System.out.println("  Outer Gap: " + getLeftOuterIndex() + ", " +
                       getRightOuterIndex() + ", " +
                       getLengthOfOuterGapInMillimeters() * MILS_PER_MILLIMETER + " mils");
    System.out.println("  Inner Gap: " + getLeftInnerIndex() + ", " +
                       getRightInnerIndex() + ", " +
                       getLengthOfInnerGapInMillimeters() * MILS_PER_MILLIMETER + " mils");
    System.out.println("  Average Outer Gap Thickness: " +
                       getAverageOuterGapThicknessInMillimeters() * MILS_PER_MILLIMETER + " mils");
  }

}
