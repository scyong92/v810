package com.axi.v810.business.imageAnalysis.exposedPad;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for Exposed Pad joints.
 *
 * @author George Booth
 */
public class ExposedPadInspectionFamily extends InspectionFamily
{
  /**
   * @author George Booth
   */
  public ExposedPadInspectionFamily()
  {
    super(InspectionFamilyEnum.EXPOSED_PAD);

    // Associate the individual algorithms with the family.

    // Measurement
    Algorithm exposedPadMeasurementAlgorithm = new ExposedPadMeasurementAlgorithm();
    addAlgorithm(exposedPadMeasurementAlgorithm);

    // Short
    Algorithm exposedPadShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.EXPOSED_PAD);
    addAlgorithm(exposedPadShortAlgorithm);

    // Open
    Algorithm exposedPadOpenAlgorithm = new ExposedPadOpenAlgorithm(this);
    addAlgorithm(exposedPadOpenAlgorithm);

    // Insufficient
//  The Insufficient algorith for Exposed Pad is not defined this time
//    Algorithm exposedPadInsufficientAlgorithm = new ExposedPadInsufficientAlgorithm();
//    addAlgorithm(exposedPadInsufficientAlgorithm);

    // Voiding
    Algorithm exposedPadVoidingAlgorithm = new ExposedPadVoidingAlgorithm(this);
    addAlgorithm(exposedPadVoidingAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author George Booth
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return Collections.singletonList(SliceNameEnum.PAD);
  }

  /**
   * Gets the applicable default SliceNameEnum for the Pad slice for the Exposed Pad inspection family.
   *
   * @author George Booth
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Use the standard pad slice.
    return SliceNameEnum.PAD;
  }

  /**
   * @author George Booth
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author George Booth
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }
}
