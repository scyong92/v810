package com.axi.v810.business.imageAnalysis.exposedPad;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Exposed Pad open algorithm
 *
 * @author George Booth
 */
public class ExposedPadOpenAlgorithm extends Algorithm
{

  /**
   * @author George Booth
   */
  public ExposedPadOpenAlgorithm(InspectionFamily exposedPadInspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.EXPOSED_PAD);
    Assert.expect(exposedPadInspectionFamily != null);
    // Add the algorithm settings.
    addAlgorithmSettings(exposedPadInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @author George Booth
   */
  protected ExposedPadOpenAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.OPEN, inspectionFamilyEnum);
  }

  /**
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.EXPOSED_PAD_OPEN_NUMBER_FAILING_GAPS);
  }

  /**
   * Adds the algorithm settings for Exposed Pad Open
   *
   * @author George Booth
   */
  private void addAlgorithmSettings(InspectionFamily exposedPadInspectionFamily)
  {
    Assert.expect(exposedPadInspectionFamily != null);
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // Standard thresholds

    // Maximum Inner Percent of Outer Gap Percent Across
    AlgorithmSetting maximumInnerPercentOfOuterGapAcrossSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS,
        displayOrder++,
        30.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumInnerPercentOfOuterGapAcrossSetting);

    // Maximum Outer Gap Length Across
    AlgorithmSetting maximumOuterGapLengthAcrossSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ACROSS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(50.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(100.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_OUTER_GAP_LENGTH_ACROSS)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_OUTER_GAP_LENGTH_ACROSS)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_OUTER_GAP_LENGTH_ACROSS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumOuterGapLengthAcrossSetting);

    // Minimum Outer Gap Thickness Across
    AlgorithmSetting minimumOuterGapThicknessAcrossSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ACROSS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(50.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MINIMUM_OUTER_GAP_THICKNESS_ACROSS)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MINIMUM_OUTER_GAP_THICKNESS_ACROSS)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MINIMUM_OUTER_GAP_THICKNESS_ACROSS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(minimumOuterGapThicknessAcrossSetting);

    // Maximum Inner Percent of Outer Gap Percent Along
    AlgorithmSetting maximumInnerPercentOfOuterGapAlongSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG,
        displayOrder++,
        30.0f, // default
        0.0f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumInnerPercentOfOuterGapAlongSetting);

    // Maximum Outer Gap Length Along
    AlgorithmSetting maximumOuterGapLengthAlongSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ALONG,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(50.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(100.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_OUTER_GAP_LENGTH_ALONG)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_OUTER_GAP_LENGTH_ALONG)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_OUTER_GAP_LENGTH_ALONG)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumOuterGapLengthAlongSetting);

    // Minimum Outer Gap Thickness Along
    AlgorithmSetting minimumOuterGapThicknessAlongSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ALONG,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(2.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(50.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MINIMUM_OUTER_GAP_THICKNESS_ALONG)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MINIMUM_OUTER_GAP_THICKNESS_ALONG)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MINIMUM_OUTER_GAP_THICKNESS_ALONG)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(minimumOuterGapThicknessAlongSetting);

    // Additonal thresholds

    // Maximum Inner Gap Length Across
    AlgorithmSetting maximumInnerGapLengthAcrossSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ACROSS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(15.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(100.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_GAP_LENGTH_ACROSS)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_GAP_LENGTH_ACROSS)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_GAP_LENGTH_ACROSS)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(maximumInnerGapLengthAcrossSetting);

    // Filled Gap Thickness Across
    AlgorithmSetting filledGapThicknessAcrossSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(25.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(100.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(FILLED_GAP_THICKNESS_ACROSS)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(FILLED_GAP_THICKNESS_ACROSS)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(FILLED_GAP_THICKNESS_ACROSS)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(filledGapThicknessAcrossSetting);

    // Maximum Inner Gap Length Along
    AlgorithmSetting maximumInnerGapLengthAlongSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ALONG,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(15.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(100.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_GAP_LENGTH_ALONG)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_GAP_LENGTH_ALONG)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(MAXIMUM_INNER_GAP_LENGTH_ALONG)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(maximumInnerGapLengthAlongSetting);

    // Filled Gap Thickness Along
    AlgorithmSetting filledGapThicknessAlongSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(25.0f), // default
        MathUtil.convertMilsToMillimeters(0.0f), // min
        MathUtil.convertMilsToMillimeters(100.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_EXPOSED_PAD_OPEN_(FILLED_GAP_THICKNESS_ALONG)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(FILLED_GAP_THICKNESS_ALONG)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(FILLED_GAP_THICKNESS_ALONG)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(filledGapThicknessAlongSetting);

    // Allowed Gap Failures
    AlgorithmSetting allowedGapFailuresSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_OPEN_ALLOWED_GAP_FAILURES,
        displayOrder++,
        0, // default
        0, // min
        24, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_EXPOSED_PAD_OPEN_(ALLOWED_GAP_FAILURES)_KEY", // desc
        "HTML_DETAILED_DESC_EXPOSED_PAD_OPEN_(ALLOWED_GAP_FAILURES)_KEY", // detailed desc
        "IMG_DESC_EXPOSED_PAD_OPEN_(ALLOWED_GAP_FAILURES)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    exposedPadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.EXPOSED_PAD,
                                                                    allowedGapFailuresSetting,
                                                                    SliceNameEnum.PAD,
                                                                    MeasurementEnum.EXPOSED_PAD_OPEN_NUMBER_FAILING_GAPS);
    addAlgorithmSetting(allowedGapFailuresSetting);
  }

  /**
   * @author George Booth
   */
  public static float getFilledGapThicknessAcross(Subtype subtype)
  {
    Assert.expect(subtype != null);

    float filledGapThicknessAcrossSetting =
      ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS)).floatValue();

    return filledGapThicknessAcrossSetting;
  }

  /**
   * @author George Booth
   */
  public static float getFilledGapThicknessAlong(Subtype subtype)
  {
    Assert.expect(subtype != null);

    float filledGapThicknessAlongSetting =
      ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG)).floatValue();

    return filledGapThicknessAlongSetting;
  }


  /**
   * Runs 'Open' on all the given joints.
   *
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    // Exposed Pad only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

      // Run the Exposed Pad Open classification.
      boolean jointPassed = classifyJoint(subtype,
                    padSlice,
                    jointInspectionData);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed);
    }
  }

  /**
   * Does the Exposed Pad Open classification on the specified joint.
   *
   * @author George Booth
   */
  protected boolean classifyJoint(Subtype subtype,
                               SliceNameEnum sliceNameEnum,
                               JointInspectionData jointInspectionData)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    boolean jointPassed = true;
    int _numberOfFailingGaps = 0;
    // save any failing indictments until we detemine if enough gaps have failed to fail the joint
    List<JointIndictment> failingIndictmentList = new ArrayList<JointIndictment>();

    boolean enableProfilesAlong = ExposedPadMeasurementAlgorithm.isProfileAlongMeasurementEnabled(subtype);
    int numberOfProfilesAlong = ExposedPadMeasurementAlgorithm.getNumberOfProfileAlongMeasurements(subtype);
    boolean enableProfilesAcross = ExposedPadMeasurementAlgorithm.isProfileAcrossMeasurementEnabled(subtype);
    int numberOfProfilesAcross = ExposedPadMeasurementAlgorithm.getNumberOfProfileAcrossMeasurements(subtype);

    // Detect opens by checking inner solder gap as percent of outer gap
    if (enableProfilesAlong)
    {
      final float FILLED_GAP_THICKNESS_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG);

      final float MAXIMUM_INNER_GAP_LENGTH_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ALONG);

      final float MAXIMUM_OUTER_GAP_LENGTH_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ALONG);

      final float MAXIMUM_GAP_ALONG_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG);

      final float MINIMUM_GAP_THICKNESS_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ALONG);

      if (numberOfProfilesAlong == 2 || numberOfProfilesAlong == 3)
      {
        // north gaps
        List<GapJointMeasurementGroup> northGapMeasurementList =
            ExposedPadMeasurementAlgorithm.getNorthGapMeasurementList(jointInspectionData, sliceNameEnum);

        for (GapJointMeasurementGroup gapJointMeasurementGroup : northGapMeasurementList)
        {
          boolean northGapPassed = true;

          JointMeasurement filledGapThicknessAlongNorthMeasurement = gapJointMeasurementGroup.getFilledGapThicknessJointMeasurement();

          boolean filledGapThicknessPassed = filledGapThicknessIsPassing(sliceNameEnum, jointInspectionData,
                                                                         FILLED_GAP_THICKNESS_ALONG,
                                                                         filledGapThicknessAlongNorthMeasurement);

          // Only check other measurements if the filled gap thickness failed
          if (filledGapThicknessPassed == false)
          {
            JointMeasurement innerGapAlongNorthMeasurement = gapJointMeasurementGroup.getInnerGapJointMeasurement();
            JointMeasurement outerGapAlongNorthMeasurement = gapJointMeasurementGroup.getOuterGapJointMeasurement();
            JointMeasurement gapRatioAlongNorthMeasurement = gapJointMeasurementGroup.getGapRatioJointMeasurement();
            JointMeasurement gapThicknessAlongNorthMeasurement = gapJointMeasurementGroup.getGapThicknessJointMeasurement();

            northGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                        MAXIMUM_INNER_GAP_LENGTH_ALONG,
                                                        innerGapAlongNorthMeasurement,
                                                        failingIndictmentList);

            northGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                        MAXIMUM_OUTER_GAP_LENGTH_ALONG,
                                                        outerGapAlongNorthMeasurement,
                                                        failingIndictmentList);

            northGapPassed &= detectOpensDueToGapRatio(sliceNameEnum, jointInspectionData,
                                                       MAXIMUM_GAP_ALONG_FRACTION,
                                                       innerGapAlongNorthMeasurement,
                                                       outerGapAlongNorthMeasurement,
                                                       gapRatioAlongNorthMeasurement,
                                                       failingIndictmentList);

            northGapPassed &= detectOpensDueToGapThickness(sliceNameEnum, jointInspectionData,
                                                           MINIMUM_GAP_THICKNESS_ALONG,
                                                           gapThicknessAlongNorthMeasurement,
                                                           failingIndictmentList);
          }
          if (northGapPassed == false)
          {
            _numberOfFailingGaps++;
            jointPassed = false;
          }
        }
      }

      if (numberOfProfilesAlong == 1 || numberOfProfilesAlong == 3)
      {
        // middle gaps
        List<GapJointMeasurementGroup> middleGapMeasurementList =
            ExposedPadMeasurementAlgorithm.getMiddleGapMeasurementList(jointInspectionData, sliceNameEnum);

        for (GapJointMeasurementGroup gapJointMeasurementGroup : middleGapMeasurementList)
        {
          boolean middleGapPassed = true;

          JointMeasurement filledGapThicknessAlongMiddleMeasurement = gapJointMeasurementGroup.getFilledGapThicknessJointMeasurement();

          boolean filledGapThicknessPassed = filledGapThicknessIsPassing(sliceNameEnum, jointInspectionData,
                                                               FILLED_GAP_THICKNESS_ALONG,
                                                               filledGapThicknessAlongMiddleMeasurement);

          // Only check other measurements if the filled gap thickness failed
          if (filledGapThicknessPassed == false)
          {
            JointMeasurement innerGapAlongMiddleMeasurement = gapJointMeasurementGroup.getInnerGapJointMeasurement();
            JointMeasurement outerGapAlongMiddleMeasurement = gapJointMeasurementGroup.getOuterGapJointMeasurement();
            JointMeasurement gapRatioAlongMiddleMeasurement = gapJointMeasurementGroup.getGapRatioJointMeasurement();
            JointMeasurement gapThicknessAlongMiddleMeasurement = gapJointMeasurementGroup.getGapThicknessJointMeasurement();

            middleGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                         MAXIMUM_INNER_GAP_LENGTH_ALONG,
                                                         innerGapAlongMiddleMeasurement,
                                                         failingIndictmentList);

            middleGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                         MAXIMUM_OUTER_GAP_LENGTH_ALONG,
                                                         outerGapAlongMiddleMeasurement,
                                                         failingIndictmentList);

            middleGapPassed &= detectOpensDueToGapRatio(sliceNameEnum, jointInspectionData,
                                                        MAXIMUM_GAP_ALONG_FRACTION,
                                                        innerGapAlongMiddleMeasurement,
                                                        outerGapAlongMiddleMeasurement,
                                                        gapRatioAlongMiddleMeasurement,
                                                        failingIndictmentList);

            middleGapPassed &= detectOpensDueToGapThickness(sliceNameEnum, jointInspectionData,
                                                            MINIMUM_GAP_THICKNESS_ALONG,
                                                            gapThicknessAlongMiddleMeasurement,
                                                            failingIndictmentList);
          }
          if (middleGapPassed == false)
          {
            _numberOfFailingGaps++;
            jointPassed = false;
          }
        }
      }

      if (numberOfProfilesAlong == 2 || numberOfProfilesAlong == 3)
      {
        // south gaps
        List<GapJointMeasurementGroup> southGapMeasurementList =
            ExposedPadMeasurementAlgorithm.getSouthGapMeasurementList(jointInspectionData, sliceNameEnum);

        for (GapJointMeasurementGroup gapJointMeasurementGroup : southGapMeasurementList)
        {
          boolean southGapPassed = true;

          JointMeasurement filledGapThicknessAlongSouthMeasurement = gapJointMeasurementGroup.getFilledGapThicknessJointMeasurement();

          boolean filledGapThicknessPassed = filledGapThicknessIsPassing(sliceNameEnum, jointInspectionData,
                                                               FILLED_GAP_THICKNESS_ALONG,
                                                               filledGapThicknessAlongSouthMeasurement);

          // Only check other measurements if the filled gap thickness failed
          if (filledGapThicknessPassed == false)
          {
            JointMeasurement innerGapAlongSouthMeasurement = gapJointMeasurementGroup.getInnerGapJointMeasurement();
            JointMeasurement outerGapAlongSouthMeasurement = gapJointMeasurementGroup.getOuterGapJointMeasurement();
            JointMeasurement gapRatioAlongSouthMeasurement = gapJointMeasurementGroup.getGapRatioJointMeasurement();
            JointMeasurement gapThicknessAlongSouthMeasurement = gapJointMeasurementGroup.getGapThicknessJointMeasurement();

            southGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                        MAXIMUM_INNER_GAP_LENGTH_ALONG,
                                                        innerGapAlongSouthMeasurement,
                                                        failingIndictmentList);

            southGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                        MAXIMUM_OUTER_GAP_LENGTH_ALONG,
                                                        outerGapAlongSouthMeasurement,
                                                        failingIndictmentList);

            southGapPassed &= detectOpensDueToGapRatio(sliceNameEnum, jointInspectionData,
                                                       MAXIMUM_GAP_ALONG_FRACTION,
                                                       innerGapAlongSouthMeasurement,
                                                       outerGapAlongSouthMeasurement,
                                                       gapRatioAlongSouthMeasurement,
                                                       failingIndictmentList);

            southGapPassed &= detectOpensDueToGapThickness(sliceNameEnum, jointInspectionData,
                                                           MINIMUM_GAP_THICKNESS_ALONG,
                                                           gapThicknessAlongSouthMeasurement,
                                                           failingIndictmentList);
          }
          if (southGapPassed == false)
          {
            _numberOfFailingGaps++;
            jointPassed = false;
          }
        }
      }
    }

    if (enableProfilesAcross)
    {
      final float FILLED_GAP_THICKNESS_ACROSS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS);

      final float MAXIMUM_INNER_GAP_LENGTH_ACROSS =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ACROSS);

      final float MAXIMUM_OUTER_GAP_LENGTH_ACROSS =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ACROSS);

      final float MAXIMUM_GAP_ACROSS_FRACTION =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS);

      final float MINIMUM_GAP_THICKNESS_ACROSS =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ACROSS);

      if (numberOfProfilesAcross == 2 || numberOfProfilesAcross == 3)
      {
        // west gaps
        List<GapJointMeasurementGroup> westGapMeasurementList =
            ExposedPadMeasurementAlgorithm.getWestGapMeasurementList(jointInspectionData, sliceNameEnum);

        for (GapJointMeasurementGroup gapJointMeasurementGroup : westGapMeasurementList)
        {
          boolean westGapPassed = true;

          JointMeasurement filledGapThicknessAlongWestMeasurement = gapJointMeasurementGroup.getFilledGapThicknessJointMeasurement();

          boolean filledGapThicknessPassed = filledGapThicknessIsPassing(sliceNameEnum, jointInspectionData,
                                                               FILLED_GAP_THICKNESS_ACROSS,
                                                               filledGapThicknessAlongWestMeasurement);

          // Only check other measurements if the filled gap thickness failed
          if (filledGapThicknessPassed == false)
          {
            JointMeasurement innerGapAcrossWestMeasurement = gapJointMeasurementGroup.getInnerGapJointMeasurement();
            JointMeasurement outerGapAcrossWestMeasurement = gapJointMeasurementGroup.getOuterGapJointMeasurement();
            JointMeasurement gapRatioAcrossWestMeasurement = gapJointMeasurementGroup.getGapRatioJointMeasurement();
            JointMeasurement gapThicknessAcrossWestMeasurement = gapJointMeasurementGroup.getGapThicknessJointMeasurement();

            westGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                       MAXIMUM_INNER_GAP_LENGTH_ACROSS,
                                                       innerGapAcrossWestMeasurement,
                                                       failingIndictmentList);

            westGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                       MAXIMUM_OUTER_GAP_LENGTH_ACROSS,
                                                       outerGapAcrossWestMeasurement,
                                                       failingIndictmentList);

            westGapPassed &= detectOpensDueToGapRatio(sliceNameEnum, jointInspectionData,
                                                      MAXIMUM_GAP_ACROSS_FRACTION,
                                                      innerGapAcrossWestMeasurement,
                                                      outerGapAcrossWestMeasurement,
                                                      gapRatioAcrossWestMeasurement,
                                                      failingIndictmentList);

            westGapPassed &= detectOpensDueToGapThickness(sliceNameEnum, jointInspectionData,
                                                          MINIMUM_GAP_THICKNESS_ACROSS,
                                                          gapThicknessAcrossWestMeasurement,
                                                          failingIndictmentList);
          }
          if (westGapPassed == false)
          {
            _numberOfFailingGaps++;
            jointPassed = false;
          }
        }
      }

      if (numberOfProfilesAcross == 1 || numberOfProfilesAcross == 3)
      {
        // center gaps
        List<GapJointMeasurementGroup> centerGapMeasurementList =
            ExposedPadMeasurementAlgorithm.getCenterGapMeasurementList(jointInspectionData, sliceNameEnum);

        for (GapJointMeasurementGroup gapJointMeasurementGroup : centerGapMeasurementList)
        {
          boolean centerGapPassed = true;

          JointMeasurement filledGapThicknessAlongCenterMeasurement = gapJointMeasurementGroup.getFilledGapThicknessJointMeasurement();

          boolean filledGapThicknessPassed = filledGapThicknessIsPassing(sliceNameEnum, jointInspectionData,
                                                               FILLED_GAP_THICKNESS_ACROSS,
                                                               filledGapThicknessAlongCenterMeasurement);

          // Only check other measurements if the filled gap thickness failed
          if (filledGapThicknessPassed == false)
          {
            JointMeasurement innerGapAcrossCenterMeasurement = gapJointMeasurementGroup.getInnerGapJointMeasurement();
            JointMeasurement outerGapAcrossCenterMeasurement = gapJointMeasurementGroup.getOuterGapJointMeasurement();
            JointMeasurement gapRatioAcrossCenterMeasurement = gapJointMeasurementGroup.getGapRatioJointMeasurement();
            JointMeasurement gapThicknessAcrossCenterMeasurement = gapJointMeasurementGroup.getGapThicknessJointMeasurement();

            centerGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                         MAXIMUM_INNER_GAP_LENGTH_ACROSS,
                                                         innerGapAcrossCenterMeasurement,
                                                         failingIndictmentList);

            centerGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                         MAXIMUM_OUTER_GAP_LENGTH_ACROSS,
                                                         outerGapAcrossCenterMeasurement,
                                                         failingIndictmentList);

            centerGapPassed &= detectOpensDueToGapRatio(sliceNameEnum, jointInspectionData,
                                                        MAXIMUM_GAP_ACROSS_FRACTION,
                                                        innerGapAcrossCenterMeasurement,
                                                        outerGapAcrossCenterMeasurement,
                                                        gapRatioAcrossCenterMeasurement,
                                                        failingIndictmentList);

            centerGapPassed &= detectOpensDueToGapThickness(sliceNameEnum, jointInspectionData,
                                                            MINIMUM_GAP_THICKNESS_ACROSS,
                                                            gapThicknessAcrossCenterMeasurement,
                                                            failingIndictmentList);
          }
          if (centerGapPassed == false)
          {
            _numberOfFailingGaps++;
            jointPassed = false;
          }
        }
      }

      if (numberOfProfilesAcross == 2 || numberOfProfilesAcross == 3)
      {
        // east gaps
        List<GapJointMeasurementGroup> eastGapMeasurementList =
            ExposedPadMeasurementAlgorithm.getEastGapMeasurementList(jointInspectionData, sliceNameEnum);

        for (GapJointMeasurementGroup gapJointMeasurementGroup : eastGapMeasurementList)
        {
          boolean eastGapPassed = true;

          JointMeasurement filledGapThicknessAlongEastMeasurement = gapJointMeasurementGroup.getFilledGapThicknessJointMeasurement();

          boolean filledGapThicknessPassed = filledGapThicknessIsPassing(sliceNameEnum, jointInspectionData,
                                                              FILLED_GAP_THICKNESS_ACROSS,
                                                              filledGapThicknessAlongEastMeasurement);

          // Only check other measurements if the filled gap thickness failed
          if (filledGapThicknessPassed == false)
          {
            JointMeasurement innerGapAcrossEastMeasurement = gapJointMeasurementGroup.getInnerGapJointMeasurement();
            JointMeasurement outerGapAcrossEastMeasurement = gapJointMeasurementGroup.getOuterGapJointMeasurement();
            JointMeasurement gapRatioAcrossEastMeasurement = gapJointMeasurementGroup.getGapRatioJointMeasurement();
            JointMeasurement gapThicknessAcrossEastMeasurement = gapJointMeasurementGroup.getGapThicknessJointMeasurement();

            eastGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                       MAXIMUM_INNER_GAP_LENGTH_ACROSS,
                                                       innerGapAcrossEastMeasurement,
                                                       failingIndictmentList);

            eastGapPassed &= detectOpensDueToGapLength(sliceNameEnum, jointInspectionData,
                                                       MAXIMUM_OUTER_GAP_LENGTH_ACROSS,
                                                       outerGapAcrossEastMeasurement,
                                                       failingIndictmentList);

            eastGapPassed &= detectOpensDueToGapRatio(sliceNameEnum, jointInspectionData,
                                                      MAXIMUM_GAP_ACROSS_FRACTION,
                                                      innerGapAcrossEastMeasurement,
                                                      outerGapAcrossEastMeasurement,
                                                      gapRatioAcrossEastMeasurement,
                                                      failingIndictmentList);

            eastGapPassed &= detectOpensDueToGapThickness(sliceNameEnum, jointInspectionData,
                                                          MINIMUM_GAP_THICKNESS_ACROSS,
                                                          gapThicknessAcrossEastMeasurement,
                                                          failingIndictmentList);
          }
          if (eastGapPassed == false)
          {
            _numberOfFailingGaps++;
            jointPassed = false;
          }
        }
      }
    }

    jointPassed = detectOpensDueToNumberOfFailingGaps(subtype, sliceNameEnum, jointInspectionData, _numberOfFailingGaps);

    // failing gap indictments have been held until we determine if enough gaps have failed
    if (jointPassed == false)
    {
      // add any failing indictments
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      for (JointIndictment openIndictment : failingIndictmentList)
      {
        jointInspectionResult.addIndictment(openIndictment);
      }
    }

    return jointPassed;
  }

  /**
   * Detects opens due to filled gap thickness. This is a special case - if this passes, the joint passes.
   * If it fails, other gap measurements are used to determine pass/fail.  This will not create a failing
   * indictment.
   *
   * @author George Booth
   */
  private boolean filledGapThicknessIsPassing(SliceNameEnum sliceNameEnum,
                                              JointInspectionData jointInspectionData,
                                              float minimumGapThickness,
                                              JointMeasurement gapThicknessMeasurement)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(gapThicknessMeasurement != null);

    // Check to see if the filled gap thickness is less than the threshold
    float filledGapThickness = gapThicknessMeasurement.getValue();

    if (MathUtil.fuzzyLessThan(filledGapThickness, minimumGapThickness))
    {
      // No indictment, need to check other measurements.
      // Flag this test as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    gapThicknessMeasurement,
                                                    minimumGapThickness);
    return true;
  }

  /**
   * Detects opens due to inner gap as percent of outer gap
   *
   * @author George Booth
   */
  private boolean detectOpensDueToGapRatio(SliceNameEnum sliceNameEnum,
                                           JointInspectionData jointInspectionData,
                                           float maximumGapRatioFraction,
                                           JointMeasurement innerGapMeasurement,
                                           JointMeasurement outerGapMeasurement,
                                           JointMeasurement gapRatioMeasurement,
                                           List<JointIndictment> failingIndictmentList)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(innerGapMeasurement != null);
    Assert.expect(outerGapMeasurement != null);
    Assert.expect(gapRatioMeasurement != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the gap ratio is less than the maximum
    float gapRatio = gapRatioMeasurement.getValue();

    if (MathUtil.fuzzyGreaterThan(gapRatio, maximumGapRatioFraction))
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      gapRatioMeasurement,
                                                      maximumGapRatioFraction);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(gapRatioMeasurement);
      openIndictment.addRelatedMeasurement(innerGapMeasurement);
      openIndictment.addRelatedMeasurement(outerGapMeasurement);

      // Tie the indictment to this joint's results.
//      jointInspectionResult.addIndictment(openIndictment);
      // Save the indictment until we determine if enough gaps have failed
      failingIndictmentList.add(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    gapRatioMeasurement,
                                                    maximumGapRatioFraction);
    return true;
  }

  /**
   * Detects opens due to outgap average thickness
   *
   * @author George Booth
   */
  private boolean detectOpensDueToGapThickness(SliceNameEnum sliceNameEnum,
                                               JointInspectionData jointInspectionData,
                                               float minimumGapThickness,
                                               JointMeasurement gapThicknessMeasurement,
                                               List<JointIndictment> failingIndictmentList)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(gapThicknessMeasurement != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the gap ratio is less than the maximum
    float averageGapThickness = gapThicknessMeasurement.getValue();

    if (MathUtil.fuzzyLessThan(averageGapThickness, minimumGapThickness))
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      gapThicknessMeasurement,
                                                      minimumGapThickness);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(gapThicknessMeasurement);

      // Tie the indictment to this joint's results.
//      jointInspectionResult.addIndictment(openIndictment);
      // Save the indictment until we determine if enough gaps have failed
      failingIndictmentList.add(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    gapThicknessMeasurement,
                                                    minimumGapThickness);
    return true;
  }

  /**
   * Detects opens due to gap length
   *
   * @author George Booth
   */
  private boolean detectOpensDueToGapLength(SliceNameEnum sliceNameEnum,
                                            JointInspectionData jointInspectionData,
                                            float maximumGapLength,
                                            JointMeasurement gapLengthMeasurement,
                                            List<JointIndictment> failingIndictmentList)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(gapLengthMeasurement != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Check to see if the outer gap is less than the maximum
    float gapLength = gapLengthMeasurement.getValue();

    if (MathUtil.fuzzyGreaterThan(gapLength, maximumGapLength))
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      gapLengthMeasurement,
                                                      maximumGapLength);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(gapLengthMeasurement);

      // Tie the indictment to this joint's results.
//      jointInspectionResult.addIndictment(openIndictment);
      // Save the indictment until we determine if enough gaps have failed
      failingIndictmentList.add(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    gapLengthMeasurement,
                                                    maximumGapLength);
    return true;
  }


  /**
   * Detects opens due to number of failing gaps
   *
   * @author George Booth
   */
  private boolean detectOpensDueToNumberOfFailingGaps(Subtype subtype,
                                                      SliceNameEnum sliceNameEnum,
                                                      JointInspectionData jointInspectionData,
                                                      int numberOfFailingGaps)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    final int MAXIMUM_NUMBER_FAILING_GAPS =
      (Integer)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_OPEN_ALLOWED_GAP_FAILURES);

    JointMeasurement numberOfFailingGapsMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.EXPOSED_PAD_OPEN_NUMBER_FAILING_GAPS,
                             MeasurementUnitsEnum.NONE,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             numberOfFailingGaps);
    jointInspectionResult.addMeasurement(numberOfFailingGapsMeasurement);

    if (MathUtil.fuzzyGreaterThan(numberOfFailingGaps, MAXIMUM_NUMBER_FAILING_GAPS))
    {
      // Indict the joint for 'open'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      numberOfFailingGapsMeasurement,
                                                      MAXIMUM_NUMBER_FAILING_GAPS);

      // Create an indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      // Add the failing measurement.
      openIndictment.addFailingMeasurement(numberOfFailingGapsMeasurement);

      // Tie the indictment to this joint's results.
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    numberOfFailingGapsMeasurement,
                                                    MAXIMUM_NUMBER_FAILING_GAPS);
    return true;
  }

}
