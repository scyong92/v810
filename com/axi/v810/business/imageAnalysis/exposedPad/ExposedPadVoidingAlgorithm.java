package com.axi.v810.business.imageAnalysis.exposedPad;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.XrayTesterException;

/**
 * Extends the FloodVoidingAlgorithm for Exposed Pad
 *
 * @author George Booth
 */
public class ExposedPadVoidingAlgorithm extends ExpectedImageVoidingAlgorithm
{
  private final int _ALGORITHM_VERSION = 1;

  /**
   * @author George Booth
   */
  public ExposedPadVoidingAlgorithm(InspectionFamily exposedPadInspectionFamily)
  {
    super(exposedPadInspectionFamily, InspectionFamilyEnum.EXPOSED_PAD);
    Assert.expect(exposedPadInspectionFamily != null);
  }

  /**
   * @author George Booth
   */
  protected void createAlgorithmSettings(InspectionFamily exposedPadInspectionFamily)
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;

    // Standard

    // Maximum Void Pecent
    AlgorithmSetting maximumVoidPecentSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_VOID_PERCENT,
      displayOrder++,
      30.0f, // default value
      0.0f, // minimum value
      100.0f, // maximum value
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_EXPOSED_PAD_VOIDING_(MAXIMUM_VOID_PERCENT)_KEY", // description URL key
      "HTML_DETAILED_DESC_EXPOSED_PAD_VOIDING_(MAXIMUM_VOID_PERCENT)_KEY", // desailed description URL key
      "IMG_DESC_EXPOSED_PAD_VOIDING_(MAXIMUM_VOID_PERCENT)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      _ALGORITHM_VERSION);
    exposedPadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.EXPOSED_PAD,
                                                                     maximumVoidPecentSetting,
                                                                     SliceNameEnum.PAD,
                                                                     MeasurementEnum.EXPOSED_PAD_VOIDING_PERCENT);
    addAlgorithmSetting(maximumVoidPecentSetting);

     // Maximum Individual Void Pecent
 /*   AlgorithmSetting maximumIndividualVoidPecentSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT,
      displayOrder++,
      25.0f, // default value
      0.0f, // minimum value
      100.0f, // maximum value
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_EXPOSED_PAD_INDIVIDUAL_VOIDING_(MAXIMUM_INDIVIDUAL_VOID_PERCENT)_KEY", // description URL key
      "HTML_DETAILED_DESC_EXPOSED_PAD_INDIVIDUAL_VOIDING_(MAXIMUM_INDIVIDUAL_VOID_PERCENT)_KEY", // desailed description URL key
      "IMG_DESC_EXPOSED_PAD_INDIVIDUAL_VOIDING_(MAXIMUM_INDIVIDUAL_VOID_PERCENT)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      _ALGORITHM_VERSION);
    exposedPadInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.EXPOSED_PAD,
                                                                     maximumIndividualVoidPecentSetting,
                                                                     SliceNameEnum.PAD,
                                                                     MeasurementEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_PERCENT);
    addAlgorithmSetting(maximumIndividualVoidPecentSetting);  */

    // Solder Thickness
    AlgorithmSetting solderThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(1.0f), // default
      MathUtil.convertMilsToMillimeters(0.0f), // min
      MathUtil.convertMilsToMillimeters(25.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_EXPOSED_PAD_VOIDING_(SOLDER_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_VOIDING_(SOLDER_THICKNESS)_KEY", // desc
      "IMG_DESC_EXPOSED_PAD_VOIDING_(SOLDER_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      _ALGORITHM_VERSION);
    addAlgorithmSetting(solderThicknessSetting);

    // Additional

    // Voiding Technique
    // CR32282 not validated. Force to "Minimum Thickness"
    ArrayList<String> allowableVoidingTechniqueValues =
      new ArrayList<String>(Arrays.asList(
        "Maximum Thickness",
        "Minimum Thickness",
        "Threshold Mask"));
    AlgorithmSetting voidingTechniqueSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_VOIDING_TECHNIQUE,
      displayOrder++,
      "Maximum Thickness",
      allowableVoidingTechniqueValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_EXPOSED_PAD_VOIDING_(VOIDING_TECHNIQUE)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_VOIDING_(VOIDING_TECHNIQUE)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_VOIDING_(VOIDING_TECHNIQUE)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      _ALGORITHM_VERSION);
    addAlgorithmSetting(voidingTechniqueSetting);

    // Maximum Gray Level Difference
    AlgorithmSetting maximumGrayLevelDifferenceSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE,
      displayOrder++,
      8.0f, // default
      0.0f, // min
      100.0f, // max
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_EXPOSED_PAD_VOIDING_(MAXIMUM_GRAY_LEVEL_DIFFERENCE)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_VOIDING_(MAXIMUM_GRAY_LEVEL_DIFFERENCE)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_VOIDING_(MAXIMUM_GRAY_LEVEL_DIFFERENCE)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      _ALGORITHM_VERSION);
    addAlgorithmSetting(maximumGrayLevelDifferenceSetting);

    // Minimum Void Area
    AlgorithmSetting minimumVoidAreaSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MINIMUM_VOID_AREA,
      displayOrder++,
      MathUtil.convertSquareMilsToSquareMillimeters(10.0f),   // default
      MathUtil.convertSquareMilsToSquareMillimeters(0.0f),    // min
      MathUtil.convertSquareMilsToSquareMillimeters(100.0f),  // max
      MeasurementUnitsEnum.SQUARE_MILLIMETERS,
      "HTML_DESC_EXPOSED_PAD_VOIDING_(MINIMUM_VOID_AREA)_KEY", // desc
      "HTML_DETAILED_DESC_EXPOSED_PAD_VOIDING_(MINIMUM_VOID_AREA)_KEY", // detailed desc
      "IMG_DESC_EXPOSED_PAD_VOIDING_(MINIMUM_VOID_AREA)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      _ALGORITHM_VERSION);
    addAlgorithmSetting(minimumVoidAreaSetting);

    // Level of Noise Reduction
    // How much to try to compensate for noise.
    // 0 = no noise reduction
    // 1 = apply blur
    // 2+ = apply blur & NR-1 cycles of opening (dilate/erode cycles) to the void pixels image.
    // The opening will remove smaller features from the image and generally make the regions appear
    // more blocky than without NR.
    //  I expect that this value will normally be 0 to 2.
    AlgorithmSetting levelOfNoiseReductionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_LEVEL_OF_NOISE_REDUCTION,
        displayOrder++,
        2, // default value
        0, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_EXPOSED_PAD_VOIDING_(LEVEL_OF_NOISE_REDUCTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_EXPOSED_PAD_VOIDING_(LEVEL_OF_NOISE_REDUCTION)_KEY", // desailed description URL key
        "IMG_DESC_EXPOSED_PAD_VOIDING_(LEVEL_OF_NOISE_REDUCTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.HIDDEN,
        _ALGORITHM_VERSION);
    addAlgorithmSetting(levelOfNoiseReductionSetting);
  }

  /**
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.EXPOSED_PAD_VOIDING_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.LOCATOR_X_LOCATION);
    _jointMeasurementEnums.add(MeasurementEnum.LOCATOR_Y_LOCATION);

  }

  /**
   * @author George Booth
   */
  protected int getNoiseReductionSettingForSlice(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    return ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_LEVEL_OF_NOISE_REDUCTION)).intValue();
  }

  /**
   * @author George Booth
   */
  protected List<SliceNameEnum> selectInspectedSlices(Subtype subtype, ReconstructedImages reconstructedImages)
  {
    Assert.expect(subtype != null);
    Assert.expect(reconstructedImages != null);

    List<SliceNameEnum> sliceNamesToInspect = new LinkedList<SliceNameEnum>();

    // Exposed Pad only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    sliceNamesToInspect.add(padSlice);

    return sliceNamesToInspect;
  }

  /**
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages, List<JointInspectionData> jointInspectionDataObjects) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    if (jointInspectionDataObjects.isEmpty())
    {
      // nothing to inspect
      return;
    }
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    for (SliceNameEnum sliceNameEnum : selectInspectedSlices(subtype, reconstructedImages))
    {
      int noiseReduction = getNoiseReductionSettingForSlice(subtype, sliceNameEnum);
      float voidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(
          AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_VOID_PERCENT);

     //   float individualVoidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(
     //     AlgorithmSettingEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT);

      MeasurementEnum percentMeasurementEnum = MeasurementEnum.EXPOSED_PAD_VOIDING_PERCENT;
     //  MeasurementEnum individualPercentMeasurementEnum = MeasurementEnum.EXPOSED_PAD_INDIVIDUAL_VOIDING_PERCENT;

      measureVoidingOnSlice(reconstructedImages,
                            sliceNameEnum,
                            jointInspectionDataObjects,
                            subtype,
                            noiseReduction,
                            voidAreaFailThreshold,
                         //   individualVoidAreaFailThreshold,
                            percentMeasurementEnum,
                            MILIMETER_PER_PIXEL);
                          //  individualPercentMeasurementEnum);
    }
  }
}

