echo "Creating image sets for image analysis unit tests . . . "
echo "********************************************************"
echo "\nCreating normal image set"
unitTesting=true java -classpath $VIEW_DIR/axi/java/classes com.axi.v810.internalTools.ImageSetGenerator -project:nepcon_narrow_pke -imageSetRoot:$UNIT_TEST_DIR/offlineXrayImages -imageSet:normalUnitTestImageSet

echo "\nCreating all-black image set"
unitTesting=true java -classpath $VIEW_DIR/axi/java/classes com.axi.v810.internalTools.ImageSetGenerator -project:nepcon_narrow_pke -imageSetRoot:$UNIT_TEST_DIR/offlineXrayImages -imageSet:blackUnitTestImageSet -special:black

echo "\nCreating all-white image set"
unitTesting=true java -classpath $VIEW_DIR/axi/java/classes com.axi.v810.internalTools.ImageSetGenerator -project:nepcon_narrow_pke -imageSetRoot:$UNIT_TEST_DIR/offlineXrayImages -imageSet:whiteUnitTestImageSet -special:white

echo "\nCreating image set with random contents"
unitTesting=true java -classpath $VIEW_DIR/axi/java/classes com.axi.v810.internalTools.ImageSetGenerator -project:nepcon_narrow_pke -imageSetRoot:$UNIT_TEST_DIR/offlineXrayImages -imageSet:randomUnitTestImageSet -special:random
