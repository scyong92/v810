package com.axi.v810.business.imageAnalysis.throughHole;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;


/**
 * Extracting some of the clutter from ChipMeasurementAlgorithm into utility methods.
 *
 * @author Patrick Lacz
 */
public class ThroughHoleAlgorithmUtil
{
  /**
   * @author Patrick Lacz
   */
  public ThroughHoleAlgorithmUtil()
  {
    // do nothing
    // this class has only static methods.
  }


  /**
   * @author Peter Esbensen
   */
  public static float[] getComponentProfile(ComponentInspectionData componentInspectionData,
                                            ReconstructionRegion reconstructionRegion,
                                            ReconstructedSlice reconstructedSlice,
                                            RegionOfInterest componentRegionOfInterest,
                                            Algorithm callingAlgorithm,
                                            boolean postDiagnostics)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);

    Image image = reconstructedSlice.getOrthogonalImage();

    RegionOfInterest adjustedRegionOfInterest = new RegionOfInterest(componentRegionOfInterest);

    adjustedRegionOfInterest.scaleFromCenterAlongAcross(1.3, 1.0);
    //Ngie Xing, XCR-2023, May 2014, Assert when PTH Dimensions are too Large
    if (AlgorithmUtil.checkRegionBoundaries(adjustedRegionOfInterest, image, reconstructionRegion, "component region of interest") == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, adjustedRegionOfInterest);
    }
    float[] componentProfile = ImageFeatureExtraction.profile(image, adjustedRegionOfInterest);

    if (postDiagnostics)
    {
      if (ImageAnalysis.areDiagnosticsEnabled(componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum(), callingAlgorithm))
      {
        MeasurementRegionDiagnosticInfo componentDiagnosticRegion =
            new MeasurementRegionDiagnosticInfo(adjustedRegionOfInterest,
                                                MeasurementRegionEnum.COMPONENT_PROFILE_REGION);
												
        AlgorithmDiagnostics.getInstance().postDiagnostics(reconstructionRegion,
                                                           reconstructedSlice.getSliceNameEnum(),
                                                           componentInspectionData.getPadOneJointInspectionData(),
                                                           callingAlgorithm,
                                                           false,
                                                           componentDiagnosticRegion);
      }
    }
    return componentProfile;
  }

  /**
   * @author Peter Esbensen
   */
  public static float[] getComponentThicknessProfileForPressfitOrThroughHole(ReconstructedSlice reconstructedSlice,
                                                     ReconstructionRegion reconstructionRegion,
                                                     RegionOfInterest componentRegionOfInterest,
                                                     JointInspectionData padOneJointInspectionData,
                                                     Algorithm callingAlgorithm,
                                                     boolean postDiagnostics) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(callingAlgorithm != null);

    Image image = reconstructedSlice.getOrthogonalImage();
    RegionOfInterest componentProfileRegionOfInterest = new RegionOfInterest(componentRegionOfInterest);
    // Ensure the component ROI is constrained to the image boundaries.
    if (AlgorithmUtil.checkRegionBoundaries(componentProfileRegionOfInterest, image, reconstructionRegion, "component region of interest") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, componentProfileRegionOfInterest);
    }

    // get a profile over the component
    float[] componentProfile = ThroughHoleAlgorithmUtil.getComponentProfile(padOneJointInspectionData.getComponentInspectionData(),
                                                                     reconstructionRegion, reconstructedSlice,
                                                                     componentProfileRegionOfInterest, callingAlgorithm, postDiagnostics);



    float[] backgroundProfile = new float[componentProfile.length];
    Arrays.fill(backgroundProfile, 255.0f);
    
    //Swee Yee Wong - XCR-3300 Thickness profile is capped at 100 in PTH subtype
    //Shouldn't set the background profile to 255, it should use the measured graylevel to convert from graylevel to thickness
    //currently only change this for new thickness table
    int projectThicknessTableVersion = reconstructionRegion.getBoard().getPanel().getProject().getThicknessTableVersion();
    
    if(projectThicknessTableVersion != 0)
    {
      Subtype subtype = padOneJointInspectionData.getSubtype();

      float regionOuterEdgeAsFractionOfInterPadDistance = 0;
      if (callingAlgorithm.getInspectionFamilyEnum().equals(InspectionFamilyEnum.THROUGHHOLE))
      {
        regionOuterEdgeAsFractionOfInterPadDistance = 0.01f
          * (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION);
      }
      else
      {
        regionOuterEdgeAsFractionOfInterPadDistance = 0.01f * 80f;
      }

      RegionOfInterest adjustedRegionOfInterest = new RegionOfInterest(componentRegionOfInterest);

      adjustedRegionOfInterest.scaleFromCenterAlongAcross(1.3, 1.0);
      //Ngie Xing, XCR-2023, May 2014, Assert when PTH Dimensions are too Large
      if (AlgorithmUtil.checkRegionBoundaries(adjustedRegionOfInterest, image, reconstructionRegion, "component region of interest") == false)
      {
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, adjustedRegionOfInterest);
      }

      backgroundProfile = AlgorithmUtil.getComponentBasedBackgroundProfile(reconstructionRegion,
                                                                           reconstructedSlice,
                                                                           adjustedRegionOfInterest,
                                                                           padOneJointInspectionData,
                                                                           regionOuterEdgeAsFractionOfInterPadDistance,
                                                                           callingAlgorithm, postDiagnostics);
    }
    
    // compute component thickness profile
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(padOneJointInspectionData.getSubtype());
    int backgroundSensitivityThreshold = AlgorithmUtil.getBackgroundSensitivityThreshold(padOneJointInspectionData.getSubtype());
    float[] componentThicknessProfile = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMils(componentProfile, backgroundProfile, thicknessTable, backgroundSensitivityThreshold);

    return componentThicknessProfile;
  }

}
