package com.axi.v810.business.imageAnalysis.throughHole;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import ij.ImagePlus;

/**
 * @author Patrick Lacz
 */
public class ThroughHoleInsufficientAlgorithm extends Algorithm
{
  private static final String _WETTING_OPTIONS_TRUE = "True";
  private static final String _WETTING_OPTIONS_FALSE = "False";
  
    /**
   *For the detection of Void Volume in Joint.
   * Lim Lay Ngor PIPA
   */
  //START
  private static final String _VOID_VOLUME_MEASUREMENT_OPTIONS_TRUE = "True";
  private static final String _VOID_VOLUME_MEASUREMENT_OPTIONS_FALSE = "False";
  private static final String _VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_TRUE = "True";
  private static final String _VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_FALSE = "False";

  class JointDataForAllSlices
  {
//    SliceNameEnum sliceNameEnum;
    JointInspectionData jointInspectionData;
    int zHeightInNanoMeters;
    int voidValuePerSliceInPixel;
    float numberOfPixelsTestedForSolder;
    //float voidValuePerSliceInNanoMeters;  
    //float voidValuePerSliceInPercentage;  
      
    JointDataForAllSlices()
    {
      zHeightInNanoMeters = 0;
      voidValuePerSliceInPixel = 0;
      numberOfPixelsTestedForSolder = 0.0f;
      //voidValuePerSliceInNanoMeters = 0.0f;                
      //voidValuePerSliceInPercentage = 0.0f;
    }
  }
  //END

  /**
   * @author Patrick Lacz
   */
  public ThroughHoleInsufficientAlgorithm(InspectionFamily inspectionFamily)
  {
    super(AlgorithmEnum.INSUFFICIENT, InspectionFamilyEnum.THROUGHHOLE);
    Assert.expect(inspectionFamily != null);

    addAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }
  
  /**
   * @author Siew Yeng
   */
  public ThroughHoleInsufficientAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.INSUFFICIENT, inspectionFamilyEnum);
  }
  
  /**
   * @author Siew Yeng
   */
  private void addAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;
    
    ArrayList<String> wettingCoverageOptions = new ArrayList<String>(Arrays.asList(_WETTING_OPTIONS_TRUE, _WETTING_OPTIONS_FALSE));
    AlgorithmSetting enableInsufficientWettingCoverage = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE,
        displayOrder++,
        _WETTING_OPTIONS_FALSE,
        wettingCoverageOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_WETTING_COVERAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_WETTING_COVERAGE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_WETTING_COVERAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(enableInsufficientWettingCoverage);

     AlgorithmSetting insufficientWettingCoverage = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE,
        displayOrder++,
        75.0f, // default value (disabled)
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_WETTING_COVERAGE_PERCENTAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_WETTING_COVERAGE_PERCENTAGE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_WETTING_COVERAGE_PERCENTAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientWettingCoverage,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE);
    addAlgorithmSetting(insufficientWettingCoverage); 
    
    //Siew Yeng - PTH wetting coverage improvement
    AlgorithmSetting insufficientVoidingPercentage = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE,
        displayOrder++,
        25.0f, // default value (disabled)
        0.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_VOIDING_PERCENTAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_VOIDING_PERCENTAGE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_VOIDING_PERCENTAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientVoidingPercentage,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE);
    addAlgorithmSetting(insufficientVoidingPercentage); 
    
    AlgorithmSetting insufficientDegreeCoverage = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE,
        displayOrder++,
        270.0f, // default value (disabled)
        0.0f, // minimum value
        360.0f, // maximum value
        MeasurementUnitsEnum.DEGREES,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DEGREE_COVERAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DEGREE_COVERAGE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DEGREE_COVERAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientDegreeCoverage,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE);
    addAlgorithmSetting(insufficientDegreeCoverage); 
    
    //Start Lim Lay Ngor PIPA 
    ArrayList<String> voidVolumeOptions = new ArrayList<String>(Arrays.asList(_VOID_VOLUME_MEASUREMENT_OPTIONS_TRUE, _VOID_VOLUME_MEASUREMENT_OPTIONS_FALSE));
    AlgorithmSetting enableInsufficientVoidVolume = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT,
      displayOrder++,
      _VOID_VOLUME_MEASUREMENT_OPTIONS_FALSE,
      voidVolumeOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(enableInsufficientVoidVolume);
    
    ArrayList<String> voidVolumeDirectionFromComponentToPinOptions = new ArrayList<String>(Arrays.asList(_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_TRUE, _VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_FALSE));
    AlgorithmSetting enableInsufficientVoidVolumeDirectionFromComponentToPin = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN,
      displayOrder++,
      _VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_FALSE,
      voidVolumeDirectionFromComponentToPinOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(enableInsufficientVoidVolumeDirectionFromComponentToPin);    

    AlgorithmSetting insufficientBarrelVolumePercentage = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE,
      displayOrder++,
      25.0f, // default value (disabled)
      0.0f, // minimum value
      100.0f, // maximum value
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_VOID_VOLUME_PERCENTAGE)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_VOID_VOLUME_PERCENTAGE)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_VOID_VOLUME_PERCENTAGE)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientBarrelVolumePercentage,
                                                          SliceNameEnum.THROUGHHOLE_BARREL,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE);
    addAlgorithmSetting(insufficientBarrelVolumePercentage);
    //End Lim Lay Ngor PIPA 

    AlgorithmSetting insufficientDifferenceFromNominalPinside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientDifferenceFromNominalPinside,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(insufficientDifferenceFromNominalPinside);  

    //By Lim Lay Ngor PIPA
   AlgorithmSetting insufficientDifferenceFromNominalComponentside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientDifferenceFromNominalComponentside,
                                                          SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(insufficientDifferenceFromNominalComponentside);  
    
     AlgorithmSetting wettingCoverageSensitivity = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY,
        displayOrder++,
        5.0f, // default value (disabled)
        1.0f, // minimum value
        10.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          wettingCoverageSensitivity,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY);
   
    addAlgorithmSetting(wettingCoverageSensitivity); 

    AlgorithmSetting insufficientDifferenceFromRegionPinside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientDifferenceFromRegionPinside,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(insufficientDifferenceFromRegionPinside);

     AlgorithmSetting insufficientDifferenceFromRegionComponentside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          insufficientDifferenceFromRegionComponentside,
                                                          SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(insufficientDifferenceFromRegionComponentside);

    //Siew Yeng - XCR-3318
    List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>(Arrays.asList(JointTypeEnum.THROUGH_HOLE));
    
    displayOrder = createBarel1SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel2SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel3SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel4SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel5SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel6SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel7SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel8SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel9SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel10SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
  }

  /**
   * @author Peter Esbensen
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE);
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE);
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE);
    //By Lim Lay Ngor PIPA
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE);
  }

  /**
   * classifyJoints
   *
   * @author Patrick Lacz
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null );
    Assert.expect(jointInspectionDataObjects.size() > 0);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtypeOfJoints);
    SliceNameEnum barrelSliceName = SliceNameEnum.THROUGHHOLE_BARREL;

    Collection<SliceNameEnum> reconstructedSlices = reconstructedImages.getSliceNames();

    int numberOfSlices = getNumberOfSlice(subtypeOfJoints);
   
    // Check if wetting coverage is enable/disable
    boolean isWettingCoverageEnableForSubtype = false;
    if (subtypeOfJoints.doesAlgorithmSettingExist(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE))
    {
        java.io.Serializable value = subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE);
        Assert.expect(value instanceof String);
        if (value.equals(_WETTING_OPTIONS_FALSE))
          isWettingCoverageEnableForSubtype = false;
        else if (value.equals(_WETTING_OPTIONS_TRUE))
          isWettingCoverageEnableForSubtype = true;
        else
          Assert.expect(false, "Unexpected Enable Wetting Coverage value.");
    }

    //By Lim Lay Ngor PIPA - To check is void volume inspection is enable/disable
    boolean isVoidVolumeMeasurementEnableForSubtype = false;
    if (subtypeOfJoints.doesAlgorithmSettingExist(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT))
    {
      java.io.Serializable value = subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT);
      Assert.expect(value instanceof String);
      if(value.equals(_VOID_VOLUME_MEASUREMENT_OPTIONS_FALSE))
        isVoidVolumeMeasurementEnableForSubtype = false;
      else if(value.equals(_VOID_VOLUME_MEASUREMENT_OPTIONS_TRUE))
        isVoidVolumeMeasurementEnableForSubtype = true;
      else
        Assert.expect(false, "Unexpected Enable Void Volume Measurement value");
    }
    
    //By Lim Lay Ngor PIPA - To check is the sequence of Void Volume detection from Component to Pin(usually for PIPA use) is enable/disable
    boolean isVoidVolumeSequenceComponentToPinEnable= true;
     if (subtypeOfJoints.doesAlgorithmSettingExist(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN))
    {
      java.io.Serializable value = subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN);
      Assert.expect(value instanceof String);
      if(value.equals(_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_FALSE))
        isVoidVolumeSequenceComponentToPinEnable = false;
      else if(value.equals(_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_TRUE))
        isVoidVolumeSequenceComponentToPinEnable = true;
      else
        Assert.expect(false, "Unexpected value for Enable Void Volume Direction From Component To Pin");
    }   

    Map<SliceNameEnum, List<JointDataForAllSlices>> voidVolumeJoinDataMap = null;
    if(isVoidVolumeMeasurementEnableForSubtype)
      voidVolumeJoinDataMap = new HashMap<>();
     
    // for each slice in [bottom, barrel, top]
    //By Lim Lay Ngor PIPA
    Assert.expect(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_PIN_SIDE));
    classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_PIN_SIDE, jointInspectionDataObjects,
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE),
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE),
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL),
                  isWettingCoverageEnableForSubtype,
                  isVoidVolumeMeasurementEnableForSubtype,
                  voidVolumeJoinDataMap);

    Assert.expect(reconstructedSlices.contains(barrelSliceName));
    classifySlice(reconstructedImages, barrelSliceName, jointInspectionDataObjects,
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL),
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL),
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL),
                  isWettingCoverageEnableForSubtype,
                  isVoidVolumeMeasurementEnableForSubtype,
                  voidVolumeJoinDataMap);

    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_2) && numberOfSlices >= 2)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_2, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_2),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }

    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_3) && numberOfSlices >= 3)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_3, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_3),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }

    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_4) && numberOfSlices >= 4)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_4, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_4),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_5) && numberOfSlices >= 5)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_5, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_5),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_6) && numberOfSlices >= 6)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_6, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_6),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_7) && numberOfSlices >= 7)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_7, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_7),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_8) && numberOfSlices >= 8)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_8, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_8),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_9) && numberOfSlices >= 9)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_9, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_9),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_10) && numberOfSlices >= 10)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_10, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_10),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL),
                    isWettingCoverageEnableForSubtype,
                    isVoidVolumeMeasurementEnableForSubtype,
                    voidVolumeJoinDataMap);
    }    
    
     /**
     * @author Lim Lay Ngor PIPA
     * PinInPaste algorithm : 
     * Map only available if isWettingCoverageEnableForSubtype = true
     * Assume z-height is valid and not duplicate.
     */
    if (isVoidVolumeMeasurementEnableForSubtype && isWettingCoverageEnableForSubtype
      && reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL))
    {
      //Not checking for all slices because slices data added at jointInspectionDataObjects already checked before calling ClassifySlice.
      //Add to classify Component Slice
      Assert.expect(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE));
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, jointInspectionDataObjects,
        (Float) subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE),
        (Float) subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE),
        (Float) subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL),
        isWettingCoverageEnableForSubtype,
        isVoidVolumeMeasurementEnableForSubtype,
        voidVolumeJoinDataMap);
    
      classifyVolume(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL, jointInspectionDataObjects,
        (Float) subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE),
        isVoidVolumeSequenceComponentToPinEnable,
        voidVolumeJoinDataMap,
        MILIMETER_PER_PIXEL);
    }
  }

  /**
   * @author Lim Lay Ngor PIPA 
   * Classify By Void Volume In Percent for a Joint
   * Recommend to set Void Volume sequence 
   * - from Component to Pin for Pin In Paste(PIP) 
   * - from Pin to Component for Pin Through Hole(PTH)
   */
  public void classifyVolume(ReconstructedImages reconstructedImages,
    SliceNameEnum sliceNameEnum,
    List<JointInspectionData> jointInspectionDataObjects,
    float minAllowableDifferenceVolumeFromNominal,
    boolean isVoidVolumeSequenceComponentToPinEnable,
    Map<SliceNameEnum, List<JointDataForAllSlices>> voidVolumeJoinDataMap,
    final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(minAllowableDifferenceVolumeFromNominal >= 0);
    Assert.expect(jointInspectionDataObjects.isEmpty() == false);
    Assert.expect(voidVolumeJoinDataMap != null);
    Assert.expect(voidVolumeJoinDataMap.isEmpty() == false);//If no _voidVolumeJoinDataMap data is added, means it might be case missing
                                                            //THROUGHHOLE_SOLDER_SIGNAL measurement which not expected to be happen.
    
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      List<JointDataForAllSlices> sortedJointDatalist = new ArrayList<>();
      //LN: Fix the bug if two joints under single slice
      //Arrange all slice data base on joint
      for(SliceNameEnum slice : voidVolumeJoinDataMap.keySet())
      {
        List<JointDataForAllSlices> jointDatalist = new ArrayList<>(voidVolumeJoinDataMap.get(slice));
        for(JointDataForAllSlices data : jointDatalist)
        {
          if(data.jointInspectionData == jointInspectionData)
            sortedJointDatalist.add(data);
        }
      }
    
      if(sortedJointDatalist.isEmpty() )
        continue;//No matching joint for this slice
      
      if (isVoidVolumeSequenceComponentToPinEnable)
      {
        //Sorting _voidVolumeJoinDataMap base on barrel zHeight in ascending order
        //(from -ve to +ve, from Component slice to Pin slice, recommend use for PIP). 
        Collections.sort(sortedJointDatalist, new Comparator<JointDataForAllSlices>()
        {
          @Override
          public int compare(JointDataForAllSlices jointData1, JointDataForAllSlices jointData2)
          {
            Assert.expect(jointData1 != null);
            Assert.expect(jointData2 != null);
            JointDataForAllSlices data1 = (JointDataForAllSlices) jointData1;
            JointDataForAllSlices data2 = (JointDataForAllSlices) jointData2;
            return (data1.zHeightInNanoMeters - data2.zHeightInNanoMeters);
          }
        });
      }
      else
      {
        //Sorting _voidVolumeJoinDataMap base on barrel zHeight in decending order
        //(from +ve to -ve, from Pin slice to Component slice, recommend use for PTH). 
        Collections.sort(sortedJointDatalist, new Comparator<JointDataForAllSlices>()
        {
          @Override
          public int compare(JointDataForAllSlices jointData1, JointDataForAllSlices jointData2)
          {
            Assert.expect(jointData1 != null);
            Assert.expect(jointData2 != null);
            JointDataForAllSlices data1 = (JointDataForAllSlices) jointData1;
            JointDataForAllSlices data2 = (JointDataForAllSlices) jointData2;
            return (data2.zHeightInNanoMeters - data1.zHeightInNanoMeters);//the only different is here for both sorting
          }
        });
      }
    
      double totalVoidVolumeAssumptionInNanoMeters = 0.0;
      double totalExpectedPastePerJointInPixels = 0.0; //Per Joint means all slices involve from Pin to Component
      //Void Height assumption: Assume the void across from one slice to another.   
      //Suggested to check the component height availability before proceed to calculation.
      //final int componentSideHeight = reconstructedImages.getReconstructedSlice(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE).getHeightInNanometers();
      //System.out.println("componentSideHeight: " + componentSideHeight);
      //System.out.println("AlgorithmUtil.getMillimetersPerPixel: " + AlgorithmUtil.getMillimetersPerPixel());
      //System.out.println("LN data size" + sortedJointDatalist.size());    
      for (int i = 0; i < sortedJointDatalist.size(); ++i)
      {    
        if (i < sortedJointDatalist.size() - 1)//not the last one
        {
          //Calculate the void volume - convert voidValuePerSliceInPixel to voidValuePerSliceInNanoMeters
          totalVoidVolumeAssumptionInNanoMeters
            += sortedJointDatalist.get(i).voidValuePerSliceInPixel * MILIMETER_PER_PIXEL * 1000.0f
            * Math.abs(sortedJointDatalist.get(i).zHeightInNanoMeters - sortedJointDatalist.get(i + 1).zHeightInNanoMeters);

          //Calculate the expected solder without void volume - convert numberOfPixelsTestedForSolder to nanometers
          totalExpectedPastePerJointInPixels
            += sortedJointDatalist.get(i).numberOfPixelsTestedForSolder * MILIMETER_PER_PIXEL * 1000.0f
            * Math.abs(sortedJointDatalist.get(i).zHeightInNanoMeters - sortedJointDatalist.get(i + 1).zHeightInNanoMeters);

          //System.out.println("LN data i" + i + ", " + sortedJointDatalist.get(i).zHeightInNanoMeters + ", " +  sortedJointDatalist.get(i).sliceNameEnum
          //  +  ", " + sortedJointDatalist.get(i).voidValuePerSliceInPixel);
        }
        else //last slice
        {
            //Add last slice void data without height concern
          //Calculate the void volume - convert voidValuePerSliceInPixel to voidValuePerSliceInNanoMeters
          totalVoidVolumeAssumptionInNanoMeters
            += sortedJointDatalist.get(i).voidValuePerSliceInPixel * MILIMETER_PER_PIXEL * 1000.0f;

          //Calculate the expected solder without void volume - convert numberOfPixelsTestedForSolder to nanometers
          totalExpectedPastePerJointInPixels
            += sortedJointDatalist.get(i).numberOfPixelsTestedForSolder * MILIMETER_PER_PIXEL * 1000.0f;

          //System.out.println("LN data i" + i + ", " + sortedJointDatalist.get(i).zHeightInNanoMeters + ", " +  sortedJointDatalist.get(i).sliceNameEnum
          //  +  ", " + sortedJointDatalist.get(i).voidValuePerSliceInPixel);
        }
      }
    
      //System.out.println("totalVoidVolumeAssumptionInNanoMeters: " + totalVoidVolumeAssumptionInNanoMeters);
      jointInspectionData.getBoard().getBoardSettings().getBoardZOffsetInNanometers();
      //AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, reconstructedImages.getReconstructionRegion(), reconstructedSlice, true);
      //Here may need to evaluate - had modify to not passing the reconstructedSlice data so that it show "All" slices.
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, reconstructedImages.getReconstructionRegion(), true);      
      
      //It is not expected that a joint will have 0 expected pasted per joint.
      Assert.expect(totalExpectedPastePerJointInPixels > 0);
      final double  voidVolumePercentagePerJoint = totalVoidVolumeAssumptionInNanoMeters / totalExpectedPastePerJointInPixels * 100.0;
    
      JointMeasurement voidVolumePercentMeasurement = new JointMeasurement(this,
        MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE,
        MeasurementUnitsEnum.PERCENT,
        jointInspectionData.getPad(), sliceNameEnum, (float) voidVolumePercentagePerJoint); 
      AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructedImages.getReconstructionRegion(), jointInspectionData, voidVolumePercentMeasurement);

      //Add inspection result to "result" tab.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addMeasurement(voidVolumePercentMeasurement);

      boolean passVoidVolume = true;
      if (voidVolumePercentagePerJoint > minAllowableDifferenceVolumeFromNominal)
      {
        passVoidVolume = false;
        // reject as insufficient - move report to fail tab
        JointIndictment indictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceNameEnum);
        //indictment.addRelatedMeasurement(graylevelMeasurement);
        indictment.addFailingMeasurement(voidVolumePercentMeasurement);
        jointInspectionResult.addIndictment(indictment);
        // indicate that the joint failed.
      }

      // Show the pass/fail diagnostic for the volume
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
        jointInspectionData,
        reconstructedImages.getReconstructionRegion(),
        sliceNameEnum,
        passVoidVolume); //is this enough or we should do -> passedNominal && passedRegion);
    }
  }

  /**
   * @author Patrick Lacz
   */
  private void classifySlice(ReconstructedImages reconstructedImages,
                             SliceNameEnum sliceNameEnum,
                             List<JointInspectionData> jointInspectionDataObjects,
                             float minAllowableDifferenceFromNominal,
                             float minAllowableDifferenceFromRegion, //Lim Lay Ngor: some how no use in the function already
                             float nominalGraylevel,
                             boolean isWettingCoverageEnable,
                             boolean isVoidVolumeMeasurementEnable,
                             Map<SliceNameEnum, List<JointDataForAllSlices>> voidVolumeJoinDataMap)//By Lim Lay Ngor PIPA
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);

    float sumOfGraylevels = 0.f;

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    Assert.expect(jointInspectionDataObjects.isEmpty() == false);
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    int numberOfValidJointInspectionDataObjects = 0;

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);
    //System.out.println(reconstructedImages.getSliceNames());

    List<JointDataForAllSlices> voidVolumeJointDatalist = new ArrayList<JointDataForAllSlices>();
    // compute the average graylevel for this region and slice.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // do not post joint diagnostics here -- this is computing the region average.
      //System.out.println(sliceNameEnum);
      if (ThroughHoleMeasurementAlgorithm.hasCorrectedGraylevelMeasurement(jointInspectionData, sliceNameEnum) == false)
        continue;

      // retrieve the graylevel measurement that was taken in the Measurement algorithm.
      JointMeasurement graylevelMeasurement = ThroughHoleMeasurementAlgorithm.getCorrectedGraylevelMeasurement(
          jointInspectionData, sliceNameEnum);
      float correctedGraylevel = graylevelMeasurement.getValue();

      // keep a sum so that we can create an average for the inspection region for the next step.
      sumOfGraylevels += correctedGraylevel;
      numberOfValidJointInspectionDataObjects++;
    }

    float averageGraylevel = Float.NaN;
    if (numberOfValidJointInspectionDataObjects != 0)
      averageGraylevel = sumOfGraylevels / numberOfValidJointInspectionDataObjects;
     
    // Now classify any joints that fail the difference from region average.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      //System.out.println(sliceNameEnum);
      if (ThroughHoleMeasurementAlgorithm.hasCorrectedGraylevelMeasurement(jointInspectionData, sliceNameEnum) == false)
        continue; //This will skip all void, wetting and void volume measurement for this joint without warning.
                  //But this is not suppose will happen.
        
      // notify the user of the beginning of a new joint
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                reconstructedImages.getReconstructionRegion(),
                                                reconstructedSlice,
                                                false);

      JointMeasurement graylevelMeasurement = ThroughHoleMeasurementAlgorithm.getCorrectedGraylevelMeasurement(jointInspectionData, sliceNameEnum);

      // test against the nominal graylevel
      boolean passedNominal = classifyByDifferenceFromNominal(reconstructedImages, nominalGraylevel, minAllowableDifferenceFromNominal, jointInspectionData, graylevelMeasurement);

      // test against the average for the region
      boolean passedRegion = classifyByPercentOfRegionAverage(reconstructedImages, minAllowableDifferenceFromRegion, averageGraylevel, jointInspectionData, graylevelMeasurement);
      
      if (isWettingCoverageEnable)
      {
        //Siew Yeng - XCR-3318
        float wettingCoverageThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE);
        float wettingCoverageSensitivity = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY);
        float voidingPercentageThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE);
        float degreeCoverageThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE);
        
        final double NANOMETERS_TO_PIXELS = 1.0 / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
        
        double fractionfBarrelDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST) / 100.0f;
        double pinDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH) * fractionfBarrelDiameter;
        pinDiameter =  new Double(MathUtil.convertUnits(pinDiameter, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS));
        
        Image expectedPinJointImage = new Image(reconstructedSlice.getImage().getWidth(), reconstructedSlice.getImage().getHeight());
        Image expectedBarrelJointImage = new Image(reconstructedSlice.getImage().getWidth(), reconstructedSlice.getImage().getHeight());
        Image jointImage = new Image(reconstructedSlice.getImage().getWidth(), reconstructedSlice.getImage().getHeight());
        Transform.copyImageIntoImage(reconstructedSlice.getImage(), jointImage);

        RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
        // scaling for barrel diameter
        RegionOfInterest barrelRegionForThicknessMeasurement = new RegionOfInterest(jointRoi);
        barrelRegionForThicknessMeasurement.scaleFromCenterXY(fractionfBarrelDiameter, fractionfBarrelDiameter);
        barrelRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(barrelRegionForThicknessMeasurement, reconstructedSlice.getOrthogonalImage());
        barrelRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.OBROUND);

        LandPatternPad landPatternPad = jointInspectionData.getPad().getLandPatternPad();
        ThroughHoleLandPatternPad thLandPatternPad = (ThroughHoleLandPatternPad)landPatternPad;

        Paint.fillImage(expectedPinJointImage, 255.f);
        Paint.fillImage(expectedBarrelJointImage, 255.f);
        
        //Siew Yeng - temporary fix for getting incorrect center value for horizontal image
        DoubleCoordinate pinCenter = new DoubleCoordinate((float)jointRoi.getCenterAcross(), (float)jointRoi.getCenterAlong());
        DoubleCoordinate barrelCenter = new DoubleCoordinate((float)barrelRegionForThicknessMeasurement.getCenterAcross(), (float)barrelRegionForThicknessMeasurement.getCenterAlong());
        
        if(jointRoi.getOrientationInDegrees() == 0 || jointRoi.getOrientationInDegrees() == 180)
        {
          pinCenter = new DoubleCoordinate((float)jointRoi.getCenterAlong(), (float)jointRoi.getCenterAcross());
          barrelCenter = new DoubleCoordinate((float)barrelRegionForThicknessMeasurement.getCenterAlong(), (float)barrelRegionForThicknessMeasurement.getCenterAcross());
        }
        
        float numberOfPixelsTestedForPin = (float)(Paint.fillCircle(expectedPinJointImage, RegionOfInterest.createRegionFromImage(expectedPinJointImage),
          //new DoubleCoordinate((float)jointRoi.getCenterAcross(), (float)jointRoi.getCenterAlong()),
         pinCenter,
         pinDiameter * NANOMETERS_TO_PIXELS / 2.0f, 30.0f));

        float numberOfPixelsTested = (float)(Paint.fillCircle(expectedBarrelJointImage, RegionOfInterest.createRegionFromImage(expectedBarrelJointImage),
        //new DoubleCoordinate((float)barrelRegionForThicknessMeasurement.getCenterAcross(), (float)barrelRegionForThicknessMeasurement.getCenterAlong()),
        barrelCenter,
        barrelRegionForThicknessMeasurement.getWidth() / 2.0f, 30.0f));
        
        AutoLocalThreshold autoLocalThreshold = new AutoLocalThreshold();

        ImagePlus jointImageImagePlus;
        jointImageImagePlus = new ImagePlus("", jointImage.getBufferedImage());

//        autoLocalThreshold.Sauvola(jointImageImagePlus,  (int)((thLandPatternPad.getHoleDiameterInNanoMeters() * NANOMETERS_TO_PIXELS) / wettingCoverageSensitivity), 0.1, thLandPatternPad.getHoleDiameterInNanoMeters() * NANOMETERS_TO_PIXELS, false);
        int radius = Math.min(thLandPatternPad.getHoleWidthInNanoMeters(), thLandPatternPad.getHoleLengthInNanoMeters());
        autoLocalThreshold.Sauvola(jointImageImagePlus,  (int)((radius * NANOMETERS_TO_PIXELS) / wettingCoverageSensitivity), 0.1, radius * NANOMETERS_TO_PIXELS, false);
        jointImage.decrementReferenceCount();
        jointImage = Image.createFloatImageFromBufferedImage(jointImageImagePlus.getBufferedImage());
        
        jointImageImagePlus.close();
        jointImageImagePlus.flush(); 
        
        Image wettingCoverageImage = Arithmetic.subtractImages(expectedBarrelJointImage,  RegionOfInterest.createRegionFromImage(expectedBarrelJointImage),
                  jointImage, RegionOfInterest.createRegionFromImage(jointImage));

        Image finalWettingCoverageImage = Arithmetic.subtractImages(expectedPinJointImage,  RegionOfInterest.createRegionFromImage(expectedPinJointImage),
            wettingCoverageImage, RegionOfInterest.createRegionFromImage(wettingCoverageImage));
        
        Threshold.threshold(finalWettingCoverageImage, 225.0f - 1.0f, 0.0f, 225.0f + 1.0f, 0.0f); 
        
        _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(),
                              sliceNameEnum,
                              jointInspectionData,
                              this,
                              false,
                              new OverlayWettingCoverageImageDiagnosticInfo(finalWettingCoverageImage, RegionOfInterest.createRegionFromImage(finalWettingCoverageImage))); 

        int numberOfVoidPixels = Threshold.countPixelsInRange(finalWettingCoverageImage, 225.0f-0.5f, 225.0f+0.5f);

        // Store the percent wetting coverage
        float voidPercent = 100.f * (float)numberOfVoidPixels / (numberOfPixelsTested - numberOfPixelsTestedForPin);        

        //By Lim Lay Ngor PIPA added - Add data to the map
        //START
        if(isVoidVolumeMeasurementEnable)
        {
          JointDataForAllSlices localJointSlicesData = new JointDataForAllSlices();
//          localJointSlicesData.sliceNameEnum = sliceNameEnum;
          localJointSlicesData.jointInspectionData = jointInspectionData;
          localJointSlicesData.zHeightInNanoMeters = reconstructedSlice.getHeightInNanometers();          
          localJointSlicesData.numberOfPixelsTestedForSolder = numberOfPixelsTested - numberOfPixelsTestedForPin;
          localJointSlicesData.voidValuePerSliceInPixel = numberOfVoidPixels;
          //localJointSlicesData.voidValuePerSliceInNanoMeters = numberOfVoidPixels * AlgorithmUtil.getMillimetersPerPixel() * 1000.0f;          
          //localJointSlicesData.voidValuePerSliceInPercentage = voidPercent;
          
          voidVolumeJointDatalist.add(localJointSlicesData);
          //System.out.println("LNLim voidPercent: " +  voidPercent + " ," + sliceNameEnum);
          //System.out.println("LNLim numberOfVoidPixels:" + numberOfVoidPixels + ", numberOfPixelsTested: " + numberOfPixelsTested 
          //  + ", numberOfPixelsTestedForPin: " +numberOfPixelsTestedForPin);
        }
        //END
        
        wettingCoverageImage.decrementReferenceCount();
        //finalWettingCoverageImage.decrementReferenceCount();
        jointImage.decrementReferenceCount();
        expectedPinJointImage.decrementReferenceCount();
        expectedBarrelJointImage.decrementReferenceCount();
        
        // test against the wetting coverage
        boolean passedWettingCoverage = classifyByPercentOfWettingCoverage(reconstructedImages, wettingCoverageThreshold, voidPercent, jointInspectionData, graylevelMeasurement);
        
        //test against voiding percentage
        boolean passedVoidingPercentage = classifyByPercentOfVoiding(reconstructedImages, voidingPercentageThreshold, voidPercent, jointInspectionData, graylevelMeasurement);
        
        
        //Siew Yeng - degree coverage
        Image degreeOfVoidingOuterRegionImage = new Image(reconstructedSlice.getImage().getWidth(), reconstructedSlice.getImage().getHeight());
        
        Paint.fillImage(degreeOfVoidingOuterRegionImage, 255.f);

        float numberOfPixelsTestedForRingProfileOuterRegion = (float)(Paint.fillCircle(degreeOfVoidingOuterRegionImage, RegionOfInterest.createRegionFromImage(degreeOfVoidingOuterRegionImage),
          pinCenter, (pinDiameter * NANOMETERS_TO_PIXELS / 2.0f)+1.0f, 30.0f));
        
        Image degreeOfVoidingImage  =  Arithmetic.subtractImages(finalWettingCoverageImage,  RegionOfInterest.createRegionFromImage(finalWettingCoverageImage),
            degreeOfVoidingOuterRegionImage, RegionOfInterest.createRegionFromImage(degreeOfVoidingOuterRegionImage));
        
        Threshold.threshold(degreeOfVoidingImage, 195.0f - 1.0f, 0.0f, 195.0f + 1.0f, 0.0f); 
        
        int degreeOfVoidingPixels = Threshold.countPixelsInRange(degreeOfVoidingImage, 195.0f-0.5f, 195.0f+0.5f);
        
        float degreeOfVoidingPercent = 100*(float)degreeOfVoidingPixels /(numberOfPixelsTestedForRingProfileOuterRegion-numberOfPixelsTestedForPin);

        finalWettingCoverageImage.decrementReferenceCount();
        degreeOfVoidingOuterRegionImage.decrementReferenceCount();
        degreeOfVoidingImage.decrementReferenceCount();
        
        //test against degree coverage
        boolean passedDegreeCoverage = classifyByDegreeOfCoverage(reconstructedImages, degreeCoverageThreshold, degreeOfVoidingPercent, jointInspectionData, graylevelMeasurement);
        
        // Show the pass/fail diagnostic
        AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                                jointInspectionData,
                                                                reconstructedImages.getReconstructionRegion(),
                                                                sliceNameEnum,
                                                                passedNominal && passedRegion && passedWettingCoverage && passedVoidingPercentage && passedDegreeCoverage);
      }
      else
      {
        // Show the pass/fail diagnostic
        AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                                jointInspectionData,
                                                                reconstructedImages.getReconstructionRegion(),
                                                                sliceNameEnum,
                                                                passedNominal && passedRegion);
      }
    }
    
    if(voidVolumeJointDatalist.size() > 0)
    {    
      Assert.expect(voidVolumeJoinDataMap != null);//not suppose to null if it is isVoidVolumeMeasurementEnable=true.
      voidVolumeJoinDataMap.put(sliceNameEnum, voidVolumeJointDatalist);
    }
  }

  /**
   * @author Patrick Lacz
   */
  private boolean classifyByPercentOfRegionAverage(ReconstructedImages reconstructedImages,
                                                   float maxDifferenceFromRegionAverage,
                                                   float averageGraylevel,
                                                   JointInspectionData jointInspectionData,
                                                   JointMeasurement graylevelMeasurement)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(graylevelMeasurement != null);

    if (Float.isNaN(averageGraylevel))
    {
      return true;
    }

    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();
    float correctedGraylevel = graylevelMeasurement.getValue();

    //System.out.println(graylevelMeasurement.getSliceNameEnum());
    // compute the difference from the region average. (the second way we can indict the joint)
    float differenceFromRegionAverage = averageGraylevel - correctedGraylevel;

    //System.out.println(differenceFromRegionAverage + " with Slice " + sliceName);
    JointMeasurement differenceFromAverageMeasurement = new JointMeasurement(this,
                                                                        MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION,
                                                                        MeasurementUnitsEnum.NONE,
                                                                        jointInspectionData.getPad(),
                                                                        sliceName,
                                                                        differenceFromRegionAverage);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    differenceFromAverageMeasurement);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(differenceFromAverageMeasurement);

    // if difference from region Average > Maximum Allowable for this slice:
    if (differenceFromRegionAverage > maxDifferenceFromRegionAverage)
    {
      // reject as insufficient
      JointIndictment indictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(differenceFromAverageMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      differenceFromAverageMeasurement,
                                                      maxDifferenceFromRegionAverage);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }

  /**
   * @author Patrick Lacz
   */
  private boolean classifyByDifferenceFromNominal(ReconstructedImages reconstructedImages,
                                                  float nominalGraylevel,
                                                  float maxDifferenceFromNominal,
                                                  JointInspectionData jointInspectionData,
                                                  JointMeasurement graylevelMeasurement)
  {
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();

    float correctedGraylevel = graylevelMeasurement.getValue();
    float differenceFromNominal = nominalGraylevel - correctedGraylevel;

    //System.out.println(differenceFromNominal + " with Slice " + sliceName + " in pad " + jointInspectionData.getPad());
    JointMeasurement differenceFromNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL,
        MeasurementUnitsEnum.GRAYLEVEL,
        jointInspectionData.getPad(),
        sliceName,
        differenceFromNominal);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    differenceFromNominalMeasurement);
    jointInspectionResult.addMeasurement(differenceFromNominalMeasurement);

    // if difference from nominal > Maximum Allowable for this slice
    if (differenceFromNominal > maxDifferenceFromNominal)
    {
      // reject as insufficient
      JointIndictment indictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(differenceFromNominalMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      differenceFromNominalMeasurement,
                                                      maxDifferenceFromNominal);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }
  
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel1SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);   
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                            insufficientDifferenceFromNominalBarrel,
                                                            SliceNameEnum.THROUGHHOLE_BARREL,
                                                            MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                            insufficientDifferenceFromRegionBarrel,
                                                            SliceNameEnum.THROUGHHOLE_BARREL,
                                                            MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel2SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_2,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);   
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_2,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_2,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel3SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_3,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_3,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_3,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel4SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_4,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_4,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_4,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel5SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_5,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_5,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_5,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }

  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel6SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_6,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_6,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_6,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel7SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_7,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_7,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_7,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel8SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_8,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_8,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_8,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel9SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_9,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarel10SlicesInsufficientSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting insufficientDifferenceFromNominalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10,
        displayOrder++,
        0.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromNominalBarrel);
    
    AlgorithmSetting insufficientDifferenceFromRegionBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_10,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(insufficientDifferenceFromRegionBarrel);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromNominalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_10,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          insufficientDifferenceFromRegionBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_10,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    }
    
    return displayOrder;
  }
  
   /**
   * @author Jack Hwee
   */
  private boolean classifyByPercentOfWettingCoverage(ReconstructedImages reconstructedImages,
                                                   float wettingCoverageThreshold,
                                                   float voidPercent,
                                                   JointInspectionData jointInspectionData,
                                                   JointMeasurement graylevelMeasurement)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(graylevelMeasurement != null);

    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();
   
    float wettingCoveragePercentage = 100.0f - voidPercent;

    JointMeasurement wettingCoveragePercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE,
          MeasurementUnitsEnum.PERCENT,
          jointInspectionData.getPad(), sliceName, wettingCoveragePercentage);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructedImages.getReconstructionRegion(), jointInspectionData, wettingCoveragePercentMeasurement);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(wettingCoveragePercentMeasurement);  

    // if wettingCoveragePercentage < Minimum Allowable for this slice:
    if (wettingCoveragePercentage < wettingCoverageThreshold)
    {
      // reject as insufficient
      JointIndictment indictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(wettingCoveragePercentMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      wettingCoveragePercentMeasurement,
                                                      wettingCoverageThreshold);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }
  
  /**
   * @author Siew Yeng, Phang
   */
  private boolean classifyByPercentOfVoiding(ReconstructedImages reconstructedImages,
                                             float vodingPercentageThreshold,
                                             float voidPercent,
                                             JointInspectionData jointInspectionData,
                                             JointMeasurement graylevelMeasurement)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(graylevelMeasurement != null);

    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();

    JointMeasurement voidingPercentMeasurement = new JointMeasurement(this,
          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE,
          MeasurementUnitsEnum.PERCENT,
          jointInspectionData.getPad(), sliceName, voidPercent);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructedImages.getReconstructionRegion(), jointInspectionData, voidingPercentMeasurement);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(voidingPercentMeasurement);  

    // if voiding percentage > Maximum Allowable for this slice:
    if (voidPercent > vodingPercentageThreshold)
    {
      // reject as insufficient
      JointIndictment indictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(voidingPercentMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      voidingPercentMeasurement,
                                                      vodingPercentageThreshold);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }
  
   /**
   * @author Siew Yeng, Phang
   */
  private boolean classifyByDegreeOfCoverage(ReconstructedImages reconstructedImages,
                                             float degreeCoverageThreshold,
                                             float degreeOfVoidingPercent,
                                             JointInspectionData jointInspectionData,
                                             JointMeasurement graylevelMeasurement)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(graylevelMeasurement != null);

    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();

    float degreeCoverage = (100.0f - degreeOfVoidingPercent)/100.0f * 360.0f;
    
    JointMeasurement degreeCoverageMeasurement = new JointMeasurement(this,
          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE,
          MeasurementUnitsEnum.DEGREES,
          jointInspectionData.getPad(), sliceName, degreeCoverage);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructedImages.getReconstructionRegion(), jointInspectionData, degreeCoverageMeasurement);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(degreeCoverageMeasurement);  

    // if degree coverage < Minimum Allowable for this slice:
    if (degreeCoverage < degreeCoverageThreshold)
    {
      // reject as insufficient
      JointIndictment indictment = new JointIndictment(IndictmentEnum.INSUFFICIENT, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(degreeCoverageMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      degreeCoverageMeasurement,
                                                      degreeCoverageThreshold);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }
}
