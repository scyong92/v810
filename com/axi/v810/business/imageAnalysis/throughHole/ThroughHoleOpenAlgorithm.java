package com.axi.v810.business.imageAnalysis.throughHole;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * @author George Booth
 */
public class ThroughHoleOpenAlgorithm extends Algorithm
{
  /**
   * @author George Booth
   */
  public ThroughHoleOpenAlgorithm(InspectionFamily inspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.THROUGHHOLE);
    Assert.expect(inspectionFamily != null);

    addAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }
  
  /**
   * @author Siew Yeng
   */
  public ThroughHoleOpenAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.OPEN, inspectionFamilyEnum);
  }
  
  /**
   * @author Siew Yeng
   */
  private void addAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    AlgorithmSetting openDifferenceFromNominalProtrusion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_EXTRUSION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_EXTRUSION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_EXTRUSION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          openDifferenceFromNominalProtrusion,
                                                          SliceNameEnum.THROUGHHOLE_PROTRUSION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(openDifferenceFromNominalProtrusion); 

    AlgorithmSetting openDifferenceFromRegionProtrusion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_PROTRUSION,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_EXTRUSION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_EXTRUSION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_EXTRUSION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          openDifferenceFromRegionProtrusion,
                                                          SliceNameEnum.THROUGHHOLE_PROTRUSION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(openDifferenceFromRegionProtrusion); 

    AlgorithmSetting openDifferenceFromNominalInsertion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          openDifferenceFromNominalInsertion,
                                                          SliceNameEnum.THROUGHHOLE_INSERTION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(openDifferenceFromNominalInsertion); 

    AlgorithmSetting openDifferenceFromRegionInsertion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_INSERTION,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_INSERTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_INSERTION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_INSERTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          openDifferenceFromRegionInsertion,
                                                          SliceNameEnum.THROUGHHOLE_INSERTION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(openDifferenceFromRegionInsertion); 
  }

  /**
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION);
  }

  /**
   * classifyJoints
   *
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null );
    Assert.expect(jointInspectionDataObjects.size() > 0);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();

    Collection<SliceNameEnum> reconstructedSlices = reconstructedImages.getSliceNames();

    if (ThroughHoleMeasurementAlgorithm.hasProtrusionSlice(subtypeOfJoints))
    {
      SliceNameEnum extrusionSliceName = SliceNameEnum.THROUGHHOLE_PROTRUSION;

      Assert.expect(reconstructedSlices.contains(extrusionSliceName));
      classifySlice(reconstructedImages, extrusionSliceName, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_PROTRUSION),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL));
    }

    if (ThroughHoleMeasurementAlgorithm.hasInsertionSlice(subtypeOfJoints))
    {
      SliceNameEnum insertionSliceName = SliceNameEnum.THROUGHHOLE_INSERTION;

      Assert.expect(reconstructedSlices.contains(insertionSliceName));
      classifySlice(reconstructedImages, insertionSliceName, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_INSERTION),
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL));
    }
  }

  /**
   * @author George Booth
   */
  private void classifySlice(ReconstructedImages reconstructedImages,
                             SliceNameEnum sliceNameEnum,
                             List<JointInspectionData> jointInspectionDataObjects,
                             float minAllowableDifferenceFromNominal,
                             float minAllowableDifferenceFromRegion,
                             float nominalGraylevel)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);

    float sumOfGraylevels = 0.f;

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    Assert.expect(jointInspectionDataObjects.isEmpty() == false);
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    int numberOfValidJointInspectionDataObjects = 0;

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    // compute the average graylevel for this region and slice.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // do not post joint diagnostics here -- this is computing the region average.

      if (ThroughHoleMeasurementAlgorithm.hasCorrectedGraylevelMeasurement(jointInspectionData, sliceNameEnum) == false)
        continue;  /** @todo PE should we warn the user here? */

      // retrieve the graylevel measurement that was taken in the Measurement algorithm.
      JointMeasurement graylevelMeasurement = ThroughHoleMeasurementAlgorithm.getCorrectedGraylevelMeasurement(
          jointInspectionData, sliceNameEnum);
      float correctedGraylevel = graylevelMeasurement.getValue();

      // keep a sum so that we can create an average for the inspection region for the next step.
      sumOfGraylevels += correctedGraylevel;
      numberOfValidJointInspectionDataObjects++;
    }

    float averageGraylevel = Float.NaN;
    if (numberOfValidJointInspectionDataObjects != 0)
      averageGraylevel = sumOfGraylevels / numberOfValidJointInspectionDataObjects;

    // Now classify any joints that fail the difference from region average.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      if (ThroughHoleMeasurementAlgorithm.hasCorrectedGraylevelMeasurement(jointInspectionData, sliceNameEnum) == false)
        continue;

      // notify the user of the beginning of a new joint
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                reconstructedImages.getReconstructionRegion(),
                                                reconstructedSlice,
                                                false);

      JointMeasurement graylevelMeasurement = ThroughHoleMeasurementAlgorithm.getCorrectedGraylevelMeasurement(jointInspectionData, sliceNameEnum);

      // test against the nominal graylevel
      boolean passedNominal = classifyByDifferenceFromNominal(reconstructedImages, nominalGraylevel, minAllowableDifferenceFromNominal, jointInspectionData, graylevelMeasurement);

      // test against the average for the region
      boolean passedRegion = classifyByPercentOfRegionAverage(reconstructedImages, minAllowableDifferenceFromRegion, averageGraylevel, jointInspectionData, graylevelMeasurement);

      // Show the pass/fail diagnostic
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              reconstructedImages.getReconstructionRegion(),
                                                              sliceNameEnum,
                                                              passedNominal && passedRegion);
    }
  }

  /**
   * @author George Booth
   */
  private boolean classifyByPercentOfRegionAverage(ReconstructedImages reconstructedImages,
                                                   float maxDifferenceFromRegionAverage,
                                                   float averageGraylevel,
                                                   JointInspectionData jointInspectionData,
                                                   JointMeasurement graylevelMeasurement)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(graylevelMeasurement != null);

    if (Float.isNaN(averageGraylevel))
    {
      return true;
    }

    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();
    float correctedGraylevel = graylevelMeasurement.getValue();

    // compute the difference from the region average. (the second way we can indict the joint)
    float differenceFromRegionAverage = averageGraylevel - correctedGraylevel;

    JointMeasurement differenceFromAverageMeasurement = new JointMeasurement(this,
                                                                        MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION,
                                                                        MeasurementUnitsEnum.NONE,
                                                                        jointInspectionData.getPad(),
                                                                        sliceName,
                                                                        differenceFromRegionAverage);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    differenceFromAverageMeasurement);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(differenceFromAverageMeasurement);

    // if difference from region Average > Maximum Allowable for this slice:
    if (differenceFromRegionAverage > maxDifferenceFromRegionAverage)
    {
      // reject as open
      JointIndictment indictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(differenceFromAverageMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      differenceFromAverageMeasurement,
                                                      maxDifferenceFromRegionAverage);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }

  /**
   * @author George Booth
   */
  private boolean classifyByDifferenceFromNominal(ReconstructedImages reconstructedImages,
                                                  float nominalGraylevel,
                                                  float maxDifferenceFromNominal,
                                                  JointInspectionData jointInspectionData,
                                                  JointMeasurement graylevelMeasurement)
  {
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();

    float correctedGraylevel = graylevelMeasurement.getValue();
    float differenceFromNominal = nominalGraylevel - correctedGraylevel;

    JointMeasurement differenceFromNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL,
        MeasurementUnitsEnum.GRAYLEVEL,
        jointInspectionData.getPad(),
        sliceName,
        differenceFromNominal);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    differenceFromNominalMeasurement);
    jointInspectionResult.addMeasurement(differenceFromNominalMeasurement);

    // if difference from nominal > Maximum Allowable for this slice
    if (differenceFromNominal > maxDifferenceFromNominal)
    {
      // reject as open
      JointIndictment indictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceName);
      indictment.addRelatedMeasurement(graylevelMeasurement);
      indictment.addFailingMeasurement(differenceFromNominalMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      differenceFromNominalMeasurement,
                                                      maxDifferenceFromNominal);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }
}
