package com.axi.v810.business.imageAnalysis.throughHole;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;

/**
 * The Throughhole (PTH) measurement algorithm is relatively straight forward. We want to figure out how much
 * solder is in the barrel at a couple of slices. One slice is the Pin-Side slice (opposite the component), and
 * the other is selected by the user to be somewhere in the barrel or at the top (the Component-side slice).
 *
 * A 4th "protrusion" slice was added below the Pin Side Slice to determine if a pin was really in the hole (the
 * end of the pin protrudes). Solder in the barrel was not a perfect indicator of the pin being there also.  For similar
 * reasoning, we also added an Insertion slice above the board.  Both of these slices are optional and not active by
 * default.
 *
 * <pre>
 *                             SliceNameEnum                 FocusInstruction
 *                                                (ZOffsets have sign flipped based on board side)
 *  _____________________
 * |      Component      |
 * |_____________________|
 *    |    |    |    |      THROUGHHOLE_INSERTION       PERCENT_BETWEEN_EDGES, ZOffset(boardThickness + userDefinedOffsetInNanos)
 *  __|____|____|____|___   THROUGHHOLE_COMPONENT_SIDE  PERCENT_BETWEEN_EDGES, ZOffset(userDefinedBarrelFraction*boardThickness)
 * |  |    |    |    |   |
 * |  |    |    |    |   |  THROUGHHOLE_BARREL          PERCENT_BETWEEN_EDGES, ZOffset(userDefinedBarrelFraction*boardThickness)
 * |  |    |Board    |   |
 * |  |    |    |    |   |
 * |  |    |    |    |   |
 * |__|____|____|____|___|  THROUGHHOLE_PIN_SIDE        PERCENT_BETWEEN_EDGES, ZOffset(userDefinedBarrelFraction*boardThickness)
 *    |    |    |    |
 *    |    |    |    |      THROUGHHOLE_PROTRUSION      PERCENT_BETWEEN_EDGES, ZOffset(userDefinedOffsetInNanos)
 *
 * </pre>
 *
 * The focus code is elsewhere, but as of this writing, the position of the barrel slice is dependent on the board
 * thickness as defined by the project.  The position of the slice is relative to the auto-focused pin-side-slice.
 *
 * We run locator on the barrel slice - that's where the signal is generally the strongest - even if the barrel isn't filled.  Locator
 * automatically uses the barrel size rather than the pad dimensions to perform this step.
 * Notice - if the barrel is really empty (no solder at all), locator will probably find the wrong location. The measurement should
 * still be substantially lower than the nominal, however.
 *
 * On the two inspected slices, we take the MEAN of the circular barrel region, and the MEDIAN of the surrounding background region.
 * The MEDIAN was chosen to avoid shading artifacts that are common in through-hole components.
 *
 * The corrected graylevel is obtained, and then reversed to match the intuition of our users (low=bad). Kathy Adelson reviewed some
 * of the alternative measurements and decided she liked the range and orientation of this one the most. This also allows us to use the
 * same "% of nominal" defect thresholds for the insufficient algorithm.
 *
 * A component side slice is always taken in addition to the barrel slice. The Short algorithm has the option of executing on this slice.
 *
 * @author Patrick Lacz
 */
public class ThroughHoleMeasurementAlgorithm extends Algorithm
{
  private final static String _NO_PIN_DETECTION_SLICES = "None";
  private final static String _PROTRUSION_ONLY = "Protrusion Only";
  private final static String _INSERTION_ONLY = "Insertion Only";
  private final static String _PROTRUSION_AND_INSERTION = "Both Protrusion and Insertion";
  private final static String _THROUGH_HOLE_IN_MILS = "Thickness";
  private final static String _THROUGH_HOLE_IN_PERCENTAGE = "Percentage";
  private final static String _PROJECTION_SLICE_ONLY = "2.5D Only"; //Broken Pin
  private final static String _PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES = "Pin, Component, 2.5D";//Broken Pin
  
  private final static ArrayList<String> _PIN_DETECTION_CHOICES = new ArrayList(Arrays.asList( _NO_PIN_DETECTION_SLICES, _PROTRUSION_ONLY, _INSERTION_ONLY, _PROTRUSION_AND_INSERTION));
  private final static int _BORDER_PIXELS=2;

  private final static String _ON = "On";
  private final static String _OFF = "Off";  

  private final static int _MIN_NUMBER_OF_BARREL_SLICE = 1;
  private final static int _MAX_NUMBER_OF_BARREL_SLICE = 10;

  private final static ArrayList<String> _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE  = new ArrayList();
  private final static ArrayList<String> _THROUGH_HOLE_WORKING_UNIT = new ArrayList(Arrays.asList(_THROUGH_HOLE_IN_PERCENTAGE, _THROUGH_HOLE_IN_MILS));
  
  //private final static String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";
  
  static
  {
    for(int i = _MIN_NUMBER_OF_BARREL_SLICE ; i<=_MAX_NUMBER_OF_BARREL_SLICE; i ++)
      _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE.add(Integer.toString(i));
  }
  
  /**
   * @author Siew Yeng
   */
  protected ThroughHoleMeasurementAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.MEASUREMENT, inspectionFamilyEnum);
  }
  
  /**
   * @author Patrick Lacz
   */
  public ThroughHoleMeasurementAlgorithm(InspectionFamily inspectionFamily)
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.THROUGHHOLE);
    Assert.expect(inspectionFamily != null);

    addAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }
  
  /**
   * @author Siew Yeng
   */
  private void addAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    AlgorithmSetting sliceWorkingUnit = new AlgorithmSetting(
        AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT, // setting enum
        displayOrder++, // display order,
        _THROUGH_HOLE_IN_PERCENTAGE,
        _THROUGH_HOLE_WORKING_UNIT,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(SLICEHEIGHT_WORKING_UNIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(SLICEHEIGHT_WORKING_UNIT)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(SLICEHEIGHT_WORKING_UNIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(sliceWorkingUnit);

    AlgorithmSetting userDefineNumOfSlice = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE, // setting enum
        displayOrder++, // display order,
        new Integer(_MIN_NUMBER_OF_BARREL_SLICE).toString(),
        _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(USER_DEFINED_NUMBER_OF_SLICE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(USER_DEFINED_NUMBER_OF_SLICE)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(USER_DEFINED_NUMBER_OF_SLICE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(userDefineNumOfSlice);

    AlgorithmSetting nominalPinsideSolderSignal = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL,
      displayOrder++,
      230.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(NOMINAL_PINSIDE_SOLDER_SIGNAL)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_PINSIDE_SOLDER_SIGNAL)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(NOMINAL_PINSIDE_SOLDER_SIGNAL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          nominalPinsideSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalPinsideSolderSignal);
    
    //By Lim, Lay Ngor PIPA
    AlgorithmSetting nominalComponentsideSolderSignal = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL,
      displayOrder++,
      230.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          nominalComponentsideSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalComponentsideSolderSignal);
    
    //Siew Yeng - XCR-3318
    List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>(Arrays.asList(JointTypeEnum.THROUGH_HOLE));
    
    displayOrder = createBarrel1SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel2SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel3SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel4SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel5SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel6SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel7SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel8SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel9SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel10SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    //Broken Pin
    displayOrder = createProjectionSlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);

    /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting pinSideSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      0.0f, // default value
      -50.0f, // minimum value
      50.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(pinSideSliceheight);

        /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting pinSideSliceheightinThickness = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-25.0f), // minimum value
      MathUtil.convertMilsToMillimeters(25.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(pinSideSliceheightinThickness);

    AlgorithmSetting pinDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_PIN_DETECTION, // setting enum
        displayOrder++, // display order,
        _NO_PIN_DETECTION_SLICES,
        _PIN_DETECTION_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinDetection);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting insertionSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_INSERTION_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(20.0f), // default value
      MathUtil.convertMilsToMillimeters(-20.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(INSERTION_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(INSERTION_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(INSERTION_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(insertionSliceheight);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting protrusionSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(20.0f), // default value
      MathUtil.convertMilsToMillimeters(-10.0f), // minimum value
      MathUtil.convertMilsToMillimeters(150.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(THROUGHHOLE_EXTRUSION_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(THROUGHHOLE_EXTRUSION_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(THROUGHHOLE_EXTRUSION_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(protrusionSliceheight);

    /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting componentSideShortSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT, // setting enum
//      displayOrder++, // display order,
      2000, // LC: this display order number is a hack so that the Slice Setup tab in Fine Tuning displays the settings in the order desired by marketing
      100.0f, // default value
      80.0f, // minimum value
      120.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(componentSideShortSliceheight);

     /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting componentSideShortSliceheightthickness = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS, // setting enum
//      displayOrder++, // display order,
      2001, // LC: this display order number is a hack so that the Slice Setup tab in Fine Tuning displays the settings in the order desired by marketing
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-25.0f), // minimum value
      MathUtil.convertMilsToMillimeters(25.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(componentSideShortSliceheightthickness);

    // Added by Lee Herng (4 Mar 2011)
    AlgorithmSetting autoFocusMidBoardOffset = new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
      2002, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(autoFocusMidBoardOffset);
    
    addAlgorithmSetting(new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH,
            displayOrder++,
            10.f, // default value
            0.5f,  // minimum value
            300.f, // maximum value
            MeasurementUnitsEnum.MILS,
            "HTML_DESC_THROUGHHOLE_(PIN_WIDTH)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(PIN_WIDTH)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(PIN_WIDTH)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.ADDITIONAL,
            currentVersion));
    
    addAlgorithmSetting(new AlgorithmSetting(
          AlgorithmSettingEnum.THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL,
          displayOrder++,
          120.0f, // default value
          100.0f, // minimum value
          180.0f, // maximum value
          MeasurementUnitsEnum.PERCENT,
          "HTML_DESC_THROUGHHOLE_(PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL)_KEY", // description URL key
          "HTML_DETAILED_DESC_THROUGHHOLE_(PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL)_KEY", // desailed description URL key
          "IMG_DESC_THROUGHHOLE_(PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL)_KEY", // image description URL key
          AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion));

    AlgorithmSetting nominalProtrusionSolderSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL,
        displayOrder++,
        230.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(NOMINAL_EXTRUSION_SOLDER_SIGNAL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_EXTRUSION_SOLDER_SIGNAL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(NOMINAL_EXTRUSION_SOLDER_SIGNAL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL, /** @todo PE see if people mind me moving this to Additional */
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          nominalProtrusionSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_PROTRUSION,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalProtrusionSolderSignal);

    AlgorithmSetting nominalInsertionSolderSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL,
        displayOrder++,
        230.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(NOMINAL_INSERTION_SOLDER_SIGNAL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_INSERTION_SOLDER_SIGNAL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(NOMINAL_INSERTION_SOLDER_SIGNAL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL, /** @todo PE see if people mind me moving this to Additional */
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          nominalInsertionSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_INSERTION,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalInsertionSolderSignal);

    AlgorithmSetting percentDiameterToTest = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST,
      displayOrder++,
      100.0f, // default value
      50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_THROUGHHOLE_(PERCENT_OF_DIAMETER_TO_TEST)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(PERCENT_OF_DIAMETER_TO_TEST)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(PERCENT_OF_DIAMETER_TO_TEST)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(percentDiameterToTest);

    AlgorithmSetting backgroundRegionInnerEdgeLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION, // setting enum
      displayOrder++,
      40.f,
      0f,
      100f,
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_THROUGHHOLE_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionInnerEdgeLocation);

    AlgorithmSetting backgroundRegionOuterEdgeLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION, // setting enum
      displayOrder++,
      85.f,
      0f,
      100f,
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_THROUGHHOLE_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionOuterEdgeLocation);

    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));
    
    final ArrayList<String> onOrOffSettings = new ArrayList<String>(Arrays.asList(_OFF, _ON));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    AlgorithmSetting userDefinedWaveletLevel = new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      2009, // this is done to make sure this threshold is displayed right before psh in the Slice Setup tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(userDefinedWaveletLevel);
    
    AlgorithmSetting predictiveSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2012, // this is done to make sure this threshold is displayed last in the Slice Setup tab - CR31430
      _ON,
      onOrOffSettings,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
      addAlgorithmSetting(predictiveSliceheight);
      
    // XCR-3703 System Crash When Load Pre-5.8 version Recipe that contain PSP On Pressfit and PTH Components
    AlgorithmSetting pspSearchRangeLowLimit = new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(-20.0f), // default value
        MathUtil.convertMilsToMillimeters(-50.0f), // minimum value
        MathUtil.convertMilsToMillimeters(-2.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.HIDDEN,
        currentVersion);
    addAlgorithmSetting(pspSearchRangeLowLimit);
    
    AlgorithmSetting pspSearchRangeHighLimit = new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(20.0f), // default value
        MathUtil.convertMilsToMillimeters(2.0f), // minimum value
        MathUtil.convertMilsToMillimeters(50.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.HIDDEN,
        currentVersion);
    addAlgorithmSetting(pspSearchRangeHighLimit);

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);
    
    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size(); 
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author Peter Esbensen
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());
  }

  /**
   * @author Peter Esbensen
   */
  static public boolean hasProtrusionSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String pinDetectionSliceChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_DETECTION);

    if (pinDetectionSliceChoice.equals(_PROTRUSION_ONLY) || pinDetectionSliceChoice.equals(_PROTRUSION_AND_INSERTION))
    {
      return true;
    }

    return false;
  }

  /**
   * @author Peter Esbensen
   */
  static public boolean hasInsertionSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    String pinDetectionSliceChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_DETECTION);

    if (pinDetectionSliceChoice.equals(_INSERTION_ONLY) || pinDetectionSliceChoice.equals(_PROTRUSION_AND_INSERTION))
    {
      return true;
    }

    return false;

  }

  /**
   * @author Patrick Lacz
   */
  public static boolean hasCorrectedGraylevelMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionResult result = jointInspectionData.getJointInspectionResult();
    return result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  public static boolean hasCorrectedExcessGraylevelMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionResult result = jointInspectionData.getJointInspectionResult();
    return result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
  }

  /**
   * @author Patrick Lacz
   */
  public static JointMeasurement getCorrectedGraylevelMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionResult result = jointInspectionData.getJointInspectionResult();
    Assert.expect(result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL));

    return result.getJointMeasurement(sliceNameEnum, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  public static JointMeasurement getCorrectedExcessGraylevelMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionResult result = jointInspectionData.getJointInspectionResult();
    Assert.expect(result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL));

    return result.getJointMeasurement(sliceNameEnum, MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
  }
  
  /**
   * @author Patrick Lacz
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.size() > 0);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();

    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // set up the list of slices to be inspected
    SliceNameEnum sliceNameEnumArray[] = getSliceNameEnumArray(subtypeOfJoints);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtypeOfJoints.getAlgorithmSettingValue(
        AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtypeOfJoints.getAlgorithmSettingValue(
      AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION) / 100.f;
    final float barrelRegionFractionOfDiameter = (Float)subtypeOfJoints.getAlgorithmSettingValue(
      AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST) / 100.f;
    
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    final float barrelExcessRegionFractionOfDiameter = (Float)subtypeOfJoints.getAlgorithmSettingValue(
      AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST) / 100.f;
    
    float expectedSizeInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH);
    final float expectedSizeInMM=  MathUtil.convertMilsToMillimeters(expectedSizeInMils);
    final float pinSearchDiameter = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL) / 100.f;
    Image pinTemplateImage=null;
    IntegerRef expectedPinWidthInPixels = new IntegerRef(0);
    
    SliceNameEnum locatorSlice = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);
    Locator.locateJoints(reconstructedImages, locatorSlice, jointInspectionDataObjects, this, true);

    if (regionInnerEdgeAsFractionOfInterPadDistance >= regionOuterEdgeAsFractionOfInterPadDistance)
    {
      // Raise a warning to the user that we can't run under these circumstances.
      LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
        "ALGDIAG_BACKGROUND_OUTER_EDGE_LOCATION_LESS_THEN_INNER_EDGE_LOCATION_WARNING_KEY",
        new Object[] { subtypeOfJoints.getLongName() });
      AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);

      return;
    }
    for (SliceNameEnum sliceNameEnum : sliceNameEnumArray)
    {
      ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);

      //Siew Yeng - XCR-2683 - add enhanced image
      if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtypeOfJoints))
      {
        ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, sliceNameEnum);
      }
      
      // measure using pin or barrel, depends on slice
      if ( sliceNameEnum == sliceNameEnum.THROUGHHOLE_PROTRUSION
                                           || sliceNameEnum==sliceNameEnum.THROUGHHOLE_INSERTION )
      {
        // executed only once within a clasiifyJoints call
        if (pinTemplateImage==null)
        {
          pinTemplateImage = AlgorithmUtil.createPinTemplateImage(expectedSizeInMM, expectedPinWidthInPixels, 100.f, 200.f, _BORDER_PIXELS, subtypeOfJoints);
          // if pin is too big then use default gray level
          RegionOfInterest barrelRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionDataObjects.get(0));
          // pin diameter logically must be smaller than barrel diameter, issue warning message!
          if ( (barrelRegion.getWidth() < expectedPinWidthInPixels.getValue())
                    &&  (barrelRegion.getHeight() < expectedPinWidthInPixels.getValue()) )
          {
             LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
                 "ALGDIAG_PTH_PIN_DIAMETER_INVALID_KEY",
                  new Object[] { subtypeOfJoints.getLongName() });
                  AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);
          }
        }
      }
    
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {

           AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                    jointInspectionData,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    slice,
                                                    false);
           JointMeasurement measureReversedGraylevelOfBarrelRegion=null;
           //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
           JointMeasurement measureReversedExcessGraylevelOfBarrelRegion=null;

           if (  sliceNameEnum == sliceNameEnum.THROUGHHOLE_PROTRUSION
                                           || sliceNameEnum==sliceNameEnum.THROUGHHOLE_INSERTION )
           {
             measureReversedGraylevelOfBarrelRegion = measureReversedGraylevelOfPinRegion(
                jointInspectionData,
                reconstructedImages,
                sliceNameEnum,
                pinTemplateImage,
                expectedPinWidthInPixels.getValue(),
                expectedPinWidthInPixels.getValue(),
                _BORDER_PIXELS,
                pinSearchDiameter,
                regionInnerEdgeAsFractionOfInterPadDistance,
                regionOuterEdgeAsFractionOfInterPadDistance);
           }
           else
           {
             // Measure the graylevel of the joint
              measureReversedGraylevelOfBarrelRegion = measureReversedGraylevelOfBarrelRegion(
                 jointInspectionData,
                 reconstructedImages,
                 sliceNameEnum,
                 barrelRegionFractionOfDiameter,
                 regionInnerEdgeAsFractionOfInterPadDistance,
                 regionOuterEdgeAsFractionOfInterPadDistance);

              //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
              if (subtypeOfJoints.isAlgorithmEnabled(AlgorithmEnum.EXCESS))
              {
                measureReversedExcessGraylevelOfBarrelRegion = measureReversedExcessGraylevelOfBarrelRegion(
                   jointInspectionData,
                   reconstructedImages,
                   sliceNameEnum,
                   barrelExcessRegionFractionOfDiameter,
                   regionInnerEdgeAsFractionOfInterPadDistance,
                   regionOuterEdgeAsFractionOfInterPadDistance);
              }
              
              //Siew Yeng
              basicThroughHoleClassifyJoint(reconstructedImages, jointInspectionData, sliceNameEnum);
           }
          JointMeasurement correctedGraylevelMeasurement = measureReversedGraylevelOfBarrelRegion;
          // record the measurement data
          JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
          jointInspectionResult.addMeasurement(correctedGraylevelMeasurement);
          
          //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
          if (subtypeOfJoints.isAlgorithmEnabled(AlgorithmEnum.EXCESS))
          {
            JointMeasurement correctedExcessGraylevelMeasurement = measureReversedExcessGraylevelOfBarrelRegion;
            jointInspectionResult.addMeasurement(correctedExcessGraylevelMeasurement);
          }
        }
      } // end slice enum

    if (pinTemplateImage!=null)
      pinTemplateImage.decrementReferenceCount();
  }
  
  /**
   * Pull out from classifyJoint
   * @author Siew Yeng
   */
  protected void basicThroughHoleClassifyJoint(ReconstructedImages reconstructedImages, 
                                               JointInspectionData jointInspectionData,
                                               SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Subtype subtypeOfJoints = jointInspectionData.getSubtype();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    
    ///XCR1171 - joint profile diagnostic
    RegionOfInterest componentRegionOfInterest = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
    RegionOfInterest regionAcrossComponent = new RegionOfInterest(componentRegionOfInterest);
    regionAcrossComponent.scaleFromCenterAlongAcross(1.3, 1.0);
             
    float[] grayLevelProfileAcrossComponent = null;
    Image rotatedImage = null;
             
    long padDegreeRotation = Math.round(jointInspectionData.getPad().getDegreesRotationAfterAllRotations());
             
    // Due to rotation could cause the scaled ROI to expand over the slice image,
    // we need first rotate the image according to the pad degree rotation before proceed further.
    if ((padDegreeRotation % 90) != 0)
    {
      AffineTransform rotation = AffineTransform.getRotateInstance( Math.toRadians(padDegreeRotation) );
      // XCR-3014, Assert when run production/ generate inspection image when turn on Resize for PTH 
      rotatedImage = Transform.applyAffineTransform(rotation, reconstructedImages.getFirstSlice().getOrthogonalImage(), 0.0f);

      //Kee Chin Seong - Truncate to fit in the image
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(rotatedImage, regionAcrossComponent);
      grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(rotatedImage, regionAcrossComponent);
    }
    else
    {
      //Ngie Xing, XCR-2023, May 2014, Assert when PTH Dimensions are too Large
      // XCR-3014, Assert when run production/ generate inspection image when turn on Resize for PTH
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(reconstructedImages.getFirstSlice().getOrthogonalImage(), regionAcrossComponent);
      grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(reconstructedImages.getFirstSlice().getOrthogonalImage(), regionAcrossComponent);
    }
    float[] estimatedBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(grayLevelProfileAcrossComponent);
    // Create a thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtypeOfJoints);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtypeOfJoints);
    float[] thicknessProfileAcrossInMillimeters = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent, estimatedBackgroundProfile, thicknessTable, backgroundSensitivityOffset);
    // Why Calling Twice? Commented by Wei Chin
//             AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent, estimatedBackgroundProfile);
    float[] componentThicknessProfile = ThroughHoleAlgorithmUtil.getComponentThicknessProfileForPressfitOrThroughHole(slice,
                                        reconstructedImages.getReconstructionRegion(),
                                        componentRegionOfInterest,
                                        jointInspectionData, this, true);
    if (regionAcrossComponent.getOrientationInDegrees() == 180 || regionAcrossComponent.getOrientationInDegrees() == 360)
    {
      float[] reverseComponentThicknessProfile = new float[componentThicknessProfile.length];
      int j = 0;
      for (int i = componentThicknessProfile.length - 1; i >= 0; i--)
      {
        reverseComponentThicknessProfile[j++] = componentThicknessProfile[i];
      }
      componentThicknessProfile = reverseComponentThicknessProfile;
    }
    MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[thicknessProfileAcrossInMillimeters.length];
    Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.COMPONENT_PROFILE_REGION);
    LocalizedString padThicknessProfileLabelLocalizedString =
            new LocalizedString("ALGIAG_CHIP_MEASUREMENT_OPAQUE_CHIP_EDGE_LOCATION_PROFILE_KEY", null);
    ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo = ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getFullyQualifiedPadName(),
            ProfileTypeEnum.SOLDER_PROFILE,
            padThicknessProfileLabelLocalizedString,
            0.0f, (ArrayUtil.max(componentThicknessProfile) * 1.1f),
            componentThicknessProfile,
            measurementRegionEnums,
            MeasurementUnitsEnum.MILS);

    _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(),
            sliceNameEnum,
            jointInspectionData.getSubtype(),
            this, false, true,
            labeledPadThicknessProfileDiagInfo);

    if (rotatedImage != null)
      rotatedImage.decrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  protected JointMeasurement measureReversedGraylevelOfBarrelRegion(JointInspectionData jointInspectionData,
                                                             ReconstructedImages reconstructedImages,
                                                             SliceNameEnum sliceNameEnum,
                                                             float fractionfBarrelDiameter,
                                                             float regionInnerEdgeAsFractionOfInterPadDistance,
                                                             float regionOuterEdgeAsFractionOfInterPadDistance) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    Image image = reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage();

    RegionOfInterest barrelRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // The barrel is always circular; at the very least, it won't hurt to take a circular average even if the
    // pad is rectangular (eg. pin 1)

    RegionOfInterest barrelRegionForThicknessMeasurement = new RegionOfInterest(barrelRegion);
    barrelRegionForThicknessMeasurement.scaleFromCenterXY(fractionfBarrelDiameter, fractionfBarrelDiameter);
    barrelRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(barrelRegionForThicknessMeasurement, image);
    barrelRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.OBROUND);
    float barrelGraylevel = Statistics.mean(image, barrelRegionForThicknessMeasurement);

    // compute the background graylevel from the region around the barrel.
    // we may want to change this in the future to bias the sample towards the lighter pixels due to
    // reconstruction artefacts prevalent in throughhole images.
    int interPadDistance = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    RegionOfInterest outerBackgroundRegionForThicknessMeasurement = new RegionOfInterest(barrelRegion);
    outerBackgroundRegionForThicknessMeasurement.setWidthKeepingSameCenter(
            (int)Math.ceil(interPadDistance*regionOuterEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getWidth());
    outerBackgroundRegionForThicknessMeasurement.setHeightKeepingSameCenter(
            (int)Math.ceil(interPadDistance*regionOuterEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getHeight());
    outerBackgroundRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(outerBackgroundRegionForThicknessMeasurement, image);
    outerBackgroundRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    RegionOfInterest exclusionRegionForBackgroundMeasurement = new RegionOfInterest(barrelRegion);
    exclusionRegionForBackgroundMeasurement.setWidthKeepingSameCenter(
          (int)Math.ceil(interPadDistance*regionInnerEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getWidth());
    exclusionRegionForBackgroundMeasurement.setHeightKeepingSameCenter(
          (int)Math.ceil(interPadDistance*regionInnerEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getHeight());
    exclusionRegionForBackgroundMeasurement = RegionOfInterest.createRegionFromIntersection(exclusionRegionForBackgroundMeasurement, image);
    exclusionRegionForBackgroundMeasurement.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    // make absolutely certain we have a valid region for collecting the background.
    if (outerBackgroundRegionForThicknessMeasurement.fitsWithinRegionOfInterest(exclusionRegionForBackgroundMeasurement))
    {
      exclusionRegionForBackgroundMeasurement = new RegionOfInterest(outerBackgroundRegionForThicknessMeasurement);
      exclusionRegionForBackgroundMeasurement.setWidthKeepingSameCenter(outerBackgroundRegionForThicknessMeasurement.getWidth() - 2);
      exclusionRegionForBackgroundMeasurement.setHeightKeepingSameCenter(outerBackgroundRegionForThicknessMeasurement.getHeight() - 2);
    }

    float backgroundGraylevel = Statistics.medianAroundRegion(image, outerBackgroundRegionForThicknessMeasurement, exclusionRegionForBackgroundMeasurement);

    // combine the background and barrel graylevels into a corrected graylevel (equivalent to thickness)
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float correctedGraylevel = AlgorithmUtil.getCorrectedGreylevel(backgroundGraylevel, barrelGraylevel, thicknessTable, backroundSensitivityOffset);

    float reversedGraylevel = 255.0f - correctedGraylevel;

    JointMeasurement reversedGraylevelMeasurement = new JointMeasurement(
            this,
            MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL,
            MeasurementUnitsEnum.NONE,
            jointInspectionData.getPad(),
            sliceNameEnum,
            reversedGraylevel);

    // show what we did.
    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    reversedGraylevelMeasurement);
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(), sliceNameEnum, jointInspectionData, this, false,
          new MeasurementRegionDiagnosticInfo(barrelRegionForThicknessMeasurement, MeasurementRegionEnum.BARREL_REGION),
          new MeasurementRegionDiagnosticInfo(outerBackgroundRegionForThicknessMeasurement, MeasurementRegionEnum.BACKGROUND_REGION),
          new MeasurementRegionDiagnosticInfo(exclusionRegionForBackgroundMeasurement, MeasurementRegionEnum.BACKGROUND_REGION));
    }
    return reversedGraylevelMeasurement;
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected JointMeasurement measureReversedExcessGraylevelOfBarrelRegion(JointInspectionData jointInspectionData,
                                                                          ReconstructedImages reconstructedImages,
                                                                          SliceNameEnum sliceNameEnum,
                                                                          float fractionfBarrelDiameter,
                                                                          float regionInnerEdgeAsFractionOfInterPadDistance,
                                                                          float regionOuterEdgeAsFractionOfInterPadDistance) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    Image image = reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage();

    RegionOfInterest barrelRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // The barrel is always circular; at the very least, it won't hurt to take a circular average even if the
    // pad is rectangular (eg. pin 1)

    RegionOfInterest barrelRegionForThicknessMeasurement = new RegionOfInterest(barrelRegion);
    barrelRegionForThicknessMeasurement.scaleFromCenterXY(fractionfBarrelDiameter, fractionfBarrelDiameter);
    barrelRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(barrelRegionForThicknessMeasurement, image);
    barrelRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.OBROUND);
    float barrelGraylevel = Statistics.mean(image, barrelRegionForThicknessMeasurement);

    // compute the background graylevel from the region around the barrel.
    // we may want to change this in the future to bias the sample towards the lighter pixels due to
    // reconstruction artefacts prevalent in throughhole images.
    int interPadDistance = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    RegionOfInterest outerBackgroundRegionForThicknessMeasurement = new RegionOfInterest(barrelRegion);
    outerBackgroundRegionForThicknessMeasurement.setWidthKeepingSameCenter(
            (int)Math.ceil(interPadDistance*regionOuterEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getWidth());
    outerBackgroundRegionForThicknessMeasurement.setHeightKeepingSameCenter(
            (int)Math.ceil(interPadDistance*regionOuterEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getHeight());
    outerBackgroundRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(outerBackgroundRegionForThicknessMeasurement, image);
    outerBackgroundRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    RegionOfInterest exclusionRegionForBackgroundMeasurement = new RegionOfInterest(barrelRegion);
    exclusionRegionForBackgroundMeasurement.setWidthKeepingSameCenter(
          (int)Math.ceil(interPadDistance*regionInnerEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getWidth());
    exclusionRegionForBackgroundMeasurement.setHeightKeepingSameCenter(
          (int)Math.ceil(interPadDistance*regionInnerEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getHeight());
    exclusionRegionForBackgroundMeasurement = RegionOfInterest.createRegionFromIntersection(exclusionRegionForBackgroundMeasurement, image);
    exclusionRegionForBackgroundMeasurement.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    // make absolutely certain we have a valid region for collecting the background.
    if (outerBackgroundRegionForThicknessMeasurement.fitsWithinRegionOfInterest(exclusionRegionForBackgroundMeasurement))
    {
      exclusionRegionForBackgroundMeasurement = new RegionOfInterest(outerBackgroundRegionForThicknessMeasurement);
      exclusionRegionForBackgroundMeasurement.setWidthKeepingSameCenter(outerBackgroundRegionForThicknessMeasurement.getWidth() - 2);
      exclusionRegionForBackgroundMeasurement.setHeightKeepingSameCenter(outerBackgroundRegionForThicknessMeasurement.getHeight() - 2);
    }

    float backgroundGraylevel = Statistics.medianAroundRegion(image, outerBackgroundRegionForThicknessMeasurement, exclusionRegionForBackgroundMeasurement);

    // combine the background and barrel graylevels into a corrected graylevel (equivalent to thickness)
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float correctedGraylevel = AlgorithmUtil.getCorrectedGreylevel(backgroundGraylevel, barrelGraylevel, thicknessTable, backroundSensitivityOffset);

    float reversedGraylevel = 255.0f - correctedGraylevel;

    JointMeasurement reversedGraylevelMeasurement = new JointMeasurement(
            this,
            MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL,
            MeasurementUnitsEnum.NONE,
            jointInspectionData.getPad(),
            sliceNameEnum,
            reversedGraylevel);

    // show what we did.
    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    reversedGraylevelMeasurement);
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(), sliceNameEnum, jointInspectionData, this, false,
          new MeasurementRegionDiagnosticInfo(barrelRegionForThicknessMeasurement, MeasurementRegionEnum.EXCESS_SOLDER_REGION),
          new MeasurementRegionDiagnosticInfo(outerBackgroundRegionForThicknessMeasurement, MeasurementRegionEnum.BACKGROUND_REGION),
          new MeasurementRegionDiagnosticInfo(exclusionRegionForBackgroundMeasurement, MeasurementRegionEnum.BACKGROUND_REGION));
    }
    return reversedGraylevelMeasurement;
  }
  
  /**
   * This method verifies that learning is done on enough data.  If not, a limited data warning is displayed to the
   * user.
   *
   * @author Sunit Bhalla
   * @author Patrick Lacz
   */
  private void checkForLimitedData(Subtype subtype, SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    final int MINIMUM_DATAPOINTS_NEEDED = 64;
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    /** optimize PWL : Optimize computing the number of measurements and put it in a utility method */
    float[] existingMeasurementsInDatabase = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                                      MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL,
                                                                                      true,
                                                                                      true);

    int numberOfMeasurements = existingMeasurementsInDatabase.length;
    if (numberOfMeasurements < MINIMUM_DATAPOINTS_NEEDED)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfMeasurements);
    }
  }

  /**
   * @author Patrick Lacz
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {

    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH, 110.f);
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH, 110.f);

    SliceNameEnum padSliceName = subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // for each image set
    final int _MAX_NUMBER_OF_REGIONS_TO_LEARN_ON = 100;
    ManagedOfflineImageSetIterator imageSetIterator = typicalBoardImages.iterator();
    if (typicalBoardImages.size() > _MAX_NUMBER_OF_REGIONS_TO_LEARN_ON)
    {
      imageSetIterator.setSamplingFrequency(typicalBoardImages.size() / _MAX_NUMBER_OF_REGIONS_TO_LEARN_ON);
    }
    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imageSetIterator.getNext()) != null)
    {
      ReconstructionRegion region = reconstructedImages.getReconstructionRegion();

      // find the location of the joints
      Locator.locateJoints(reconstructedImages, padSliceName, region.getInspectableJointInspectionDataList(subtype), this, true);

      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because too complicated - image gray value analysis
      recordPTHLearningMeasurements(subtype, reconstructedImages);

      imageSetIterator.finishedWithCurrentRegion();
    }

    // done with getting data from the image sets
    boolean learnOnlyNominals = false;
    learnSettingsFromDatabase(subtype, learnOnlyNominals);
  }


  /**
   * @author Patrick Lacz
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    SliceNameEnum padSliceName = subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // for each image set
    ManagedOfflineImageSetIterator imageSetIterator = typicalBoardImages.iterator();
    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = imageSetIterator.getNext()) != null)
    {
      ReconstructionRegion region = reconstructedImages.getReconstructionRegion();

      // find the location of the joints
      List<JointInspectionData> jointInspectionDataList = region.getInspectableJointInspectionDataList(subtype);
      Locator.locateJoints(reconstructedImages, padSliceName, jointInspectionDataList, this, true);

      recordPTHLearningMeasurements(subtype, reconstructedImages);

      imageSetIterator.finishedWithCurrentRegion();
      
      // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
      if (jointInspectionDataList != null)
      {
        for(JointInspectionData jointInspectionData : jointInspectionDataList)
        {
          jointInspectionData.getJointInspectionResult().clearMeasurements();
          jointInspectionData.clearJointInspectionResult();
        }
        
        jointInspectionDataList.clear();
        jointInspectionDataList = null;
      }
    }

    // done with getting data from the image sets
    boolean learnOnlyNominals = true;
    learnSettingsFromDatabase(subtype, learnOnlyNominals);
  }

  /**
   * Three measurements are recorded in the learning database for PTH:
   *   'nominal filled graylevel' : the graylevel that we think is filled for this joint.
   *   'nominal empty graylevel' : the graylevel that we think is not filled for this joint.
   *   'cutoff percent' : the percent through the hole that we think changes from filled to empty
   *
   * @author Patrick Lacz
   */
  private void recordPTHLearningMeasurements(Subtype subtype, ReconstructedImages images) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(images != null);

    ReconstructionRegion region = images.getReconstructionRegion();

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION) / 100.f;
    final float barrelRegionFractionOfDiameter = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST) / 100.f;
    final float excessSolderRegionFractionOfDiameter = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST) / 100.f;

    for (JointInspectionData jointInspectionData : region.getInspectableJointInspectionDataList(subtype))
    {
      measureThicknessesForJointLearning(images,
                                         jointInspectionData,
                                         barrelRegionFractionOfDiameter,
                                         excessSolderRegionFractionOfDiameter,
                                         regionInnerEdgeAsFractionOfInterPadDistance,
                                         regionOuterEdgeAsFractionOfInterPadDistance);
    }
  }

  private final float _TARGET_LEARNED_BARREL_THRESHOLD = 230.f;
  //By Lim Lay Ngor PIPA - the value is base on barrel threshold??????
  private final float _TARGET_LEARNED_COMPONENTSIDE_THRESHOLD = 230.f;
  private final float _TARGET_LEARNED_PINSIDE_THRESHOLD = 200.f;
  private final float _TARGET_LEARNED_PROTRUSION_AND_INSERTION_THRESHOLD = 200.f;

  private final float _MINIMUM_BARREL_DEFECT_THRESHOLD = 3.f;
  private final float _MINIMUM_PINSIDE_DEFECT_THRESHOLD = 15.f;
  //By Lim Lay Ngor PIPA - the value is base on barrel threshold??????
  private final float _MINIMUM_COMPONENTSIDE_DEFECT_THRESHOLD = 3.f;
  private final float _MINIMUM_PROTRUSION_AND_INSERTION_DEFECT_THRESHOLD = 15.f;

  /**
   * This stage of learning takes data stored in the database and figures out appropriate settings for the Algorithm Settings.
   * No images are required at this point.
   *
   * For Throughhole, there is a significant difference between pressfit and other throughhole (PTH) learning.
   * Thus this method merely chooses between these specializations.
   * @author Patrick Lacz
   */
  private void learnSettingsFromDatabase(Subtype subtype, boolean modifyOnlyNominals) throws DatastoreException
  {
    Assert.expect(subtype != null);

    float values[];

    values = subtype.getLearnedJointMeasurementValues(SliceNameEnum.THROUGHHOLE_PIN_SIDE, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL,
        true, true);

    // We are also learning insufficient algorithm settings. I hate setting them here, but since we already have the data...
    if (values.length >= 2)
    {
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float nominalPinsideSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);
      values = null;

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL, nominalPinsideSetting);
      if (modifyOnlyNominals == false)
      {
        // 'learned' defect level for pinside is 200 gl.
        float insufficientSettingForPinside = nominalPinsideSetting - _TARGET_LEARNED_PINSIDE_THRESHOLD;
        insufficientSettingForPinside = Math.max(insufficientSettingForPinside, _MINIMUM_PINSIDE_DEFECT_THRESHOLD);

        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE, insufficientSettingForPinside);
      }
    }
    
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    if (modifyOnlyNominals == false)
    {
      values = subtype.getLearnedJointMeasurementValues(SliceNameEnum.THROUGHHOLE_PIN_SIDE, MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL,
          true, true);

      if (values.length >= 2)
      {
        float maximumExcessSolderSignalPinsideSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);
        values = null;
        
        subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                this, 
                                AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE, 
                                maximumExcessSolderSignalPinsideSetting);
      }
    }

    //By Lim Lay Ngor PIPA - learn component algorithm settings. Copy Pin slice learning
    //START
   values = subtype.getLearnedJointMeasurementValues(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL,
        true, true);    
    if (values.length >= 2)
    {
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      float nominalComponentsideSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);  
      values = null;

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL, nominalComponentsideSetting);
            
      if (modifyOnlyNominals == false)
      {
        // 'learned' defect level for componentside is 230 gl - base on barrel
        float insufficientSettingForComponentside = nominalComponentsideSetting - _TARGET_LEARNED_COMPONENTSIDE_THRESHOLD;
        insufficientSettingForComponentside = Math.max(insufficientSettingForComponentside, _MINIMUM_COMPONENTSIDE_DEFECT_THRESHOLD);        
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE, insufficientSettingForComponentside);
      }
    }
    //END
    
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    if (modifyOnlyNominals == false)
    {
      values = subtype.getLearnedJointMeasurementValues(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL,
          true, true);

      if (values.length >= 2)
      {
        float maximumExcessSolderSignalComponentsideSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);
        values = null;
        
        subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                this, 
                                AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE, 
                                maximumExcessSolderSignalComponentsideSetting);
      }
    }
      
    // Learn barrel slice, depending on number of barrel slice defined.
    List<SliceNameEnum> barrelSliceEnumList = getAllBarrelSliceEnumList(subtype);
    for(SliceNameEnum barrelSliceNameEnum : barrelSliceEnumList)
    {
      values = subtype.getLearnedJointMeasurementValues(barrelSliceNameEnum, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL, true, true);
      if (values.length >= 2)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier
        float nominalBarrelSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);
        values = null;

        if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_2))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_3))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_4))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_5))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_6))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_7))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_8))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_9))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_10))
        {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL, nominalBarrelSetting);
        }
        else
          System.out.println("Warning: Invalid barrel! : " + barrelSliceNameEnum);

        if (modifyOnlyNominals == false)
        {
          // 'learned' defect level for barrel is 230 gl.
          float insufficientSettingForBarrel = nominalBarrelSetting - _TARGET_LEARNED_BARREL_THRESHOLD;
          insufficientSettingForBarrel = Math.max(insufficientSettingForBarrel, _MINIMUM_BARREL_DEFECT_THRESHOLD);

          if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_2))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_3))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_4))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_5))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_6))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_7))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_8))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_9))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9, insufficientSettingForBarrel);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_10))
          {
              subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10, insufficientSettingForBarrel);
          }
          else
            System.out.println("Warning: Invalid barrel! : " + barrelSliceNameEnum);
        }
      }
      
      //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
      if (modifyOnlyNominals == false)
      {
        values = subtype.getLearnedJointMeasurementValues(barrelSliceNameEnum, MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL, true, true);

        if (values.length >= 2)
        {
          float maximumExcessSolderSignalComponentsideSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);
          values = null;

          if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_2))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_3))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_4))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_5))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_6))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_7))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_8))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_9))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else if (barrelSliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_BARREL_10))
          {
            subtype.setLearnedValue(subtype.getJointTypeEnum(), 
                                    this, 
                                    AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10, 
                                    maximumExcessSolderSignalComponentsideSetting);
          }
          else
            System.out.println("Warning: Invalid barrel! : " + barrelSliceNameEnum);
        }
      }
        
      checkForLimitedData(subtype, barrelSliceNameEnum);
    }

    if (hasProtrusionSlice(subtype))
    {
      SliceNameEnum protrusionSliceName = SliceNameEnum.THROUGHHOLE_PROTRUSION;
      values = subtype.getLearnedJointMeasurementValues(protrusionSliceName, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL, true, true);
      if (values.length >= 2)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier
        float nominalProtrusionSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);             
        values = null;
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL, nominalProtrusionSetting);
        if (modifyOnlyNominals == false)
        {
          // 'learned' defect level for protrusion is 230 gl.
          float openSettingForProtrusion = nominalProtrusionSetting - _TARGET_LEARNED_PROTRUSION_AND_INSERTION_THRESHOLD;
          openSettingForProtrusion = Math.max(openSettingForProtrusion, _MINIMUM_PROTRUSION_AND_INSERTION_DEFECT_THRESHOLD);

          subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION, openSettingForProtrusion);
        }
      }
    }

    if (hasInsertionSlice(subtype))
    {
      SliceNameEnum insertionSliceName = SliceNameEnum.THROUGHHOLE_INSERTION;
      values = subtype.getLearnedJointMeasurementValues(insertionSliceName, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL, true, true);
      if (values.length >= 2)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier
        float nominalInsertionSetting = AlgorithmUtil.medianWithExcludeOutlierDecision(values, true);
        values = null;
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL, nominalInsertionSetting);
        if (modifyOnlyNominals == false)
        {
          // 'learned' defect level for insertion is 230 gl.
          float openSettingForInsertion = nominalInsertionSetting - _TARGET_LEARNED_PROTRUSION_AND_INSERTION_THRESHOLD;
          openSettingForInsertion = Math.max(openSettingForInsertion, _MINIMUM_PROTRUSION_AND_INSERTION_DEFECT_THRESHOLD);

          subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION, openSettingForInsertion);
        }
      }
    }
    
    // Jack Hwee - Broken pins
    if (hasProjectionSliceForShort(subtype))
    {
       values = subtype.getLearnedJointMeasurementValues(SliceNameEnum.CAMERA_0, MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL, true, true);
        if (values.length >= 2)
        {
          float nominalBarrelSetting = StatisticsUtil.medianAllowModification(values);
          values = null;
          
           subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROJECTION_SOLDER_SIGNAL, nominalBarrelSetting);
        }  
    }
  }

  /**
   * Return the list of slices that will be inspected.  The number of slices depends on the user's PIN_DETECTION setting.
   *
   * @author Peter Esbensen
   */
  protected SliceNameEnum[] getSliceNameEnumArray(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // set up the list of slices to be inspected
    List<SliceNameEnum> sliceNameEnumList = new ArrayList();
    sliceNameEnumList.add(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
    
    //By Lim Lay Ngor PIPA - want to add only if PIPA enable or as default?
    sliceNameEnumList.add(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
    
    //Khaw Chek Hau - XCR2276: PTH Barrel Slice's Solder Signal is Duplicated
    //SliceNameEnum.THROUGHHOLE_BARREL is already included in getAllBarrelSliceEnumList below
    //sliceNameEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL);
    
    sliceNameEnumList.addAll(getAllBarrelSliceEnumList(subtype));
    
    if (hasProtrusionSlice(subtype))
    {
      sliceNameEnumList.add(SliceNameEnum.THROUGHHOLE_PROTRUSION);
    }
    if (hasInsertionSlice(subtype))
    {
      sliceNameEnumList.add(SliceNameEnum.THROUGHHOLE_INSERTION);
    }
    // Jack Hwee - broken pins
    if (hasProjectionSliceForShort(subtype))
    {
      sliceNameEnumList.add(SliceNameEnum.CAMERA_0);
    }

    return sliceNameEnumList.toArray(new SliceNameEnum[sliceNameEnumList.size()]);
  }

  /**
   * @author Patrick Lacz
   */
  private void measureThicknessesForJointLearning(ReconstructedImages reconstructedImages,
                                                  JointInspectionData jointInspectionData,
                                                  float fractionOfDiameterToTest,
                                                  float excessSolderRegionFractionOfDiameter,
                                                  float regionInnerEdgeAsFractionOfInterPadDistance,
                                                  float regionOuterEdgeAsFractionOfInterPadDistance)
      throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);

    Subtype subtype = jointInspectionData.getSubtype();

    SliceNameEnum sliceNameEnumArray[] = getSliceNameEnumArray(subtype);

    for (SliceNameEnum sliceNameEnum : sliceNameEnumArray)
    {
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - Not exclude because too complicated - image gray value analysis
      JointMeasurement correctedGraylevelMeasurement = measureReversedGraylevelOfBarrelRegion(jointInspectionData,
                                                             reconstructedImages,
                                                             sliceNameEnum,
                                                             fractionOfDiameterToTest,
                                                             regionInnerEdgeAsFractionOfInterPadDistance,
                                                             regionOuterEdgeAsFractionOfInterPadDistance);

      float correctedGraylevel = correctedGraylevelMeasurement.getValue();

      LearnedJointMeasurement graylevelLearnedMeasurement =
          new LearnedJointMeasurement(jointInspectionData.getPad(),
                                      sliceNameEnum,
                                      MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL,
                                      correctedGraylevel,
                                      true,
                                      true);
      subtype.addLearnedJointMeasurementData(graylevelLearnedMeasurement);
      
      JointMeasurement correctedExcessGraylevelMeasurement = measureReversedExcessGraylevelOfBarrelRegion(jointInspectionData,
                                                             reconstructedImages,
                                                             sliceNameEnum,
                                                             excessSolderRegionFractionOfDiameter,
                                                             regionInnerEdgeAsFractionOfInterPadDistance,
                                                             regionOuterEdgeAsFractionOfInterPadDistance);

      float correctedExcessGraylevel = correctedExcessGraylevelMeasurement.getValue();

      LearnedJointMeasurement excessGraylevelLearnedMeasurement =
          new LearnedJointMeasurement(jointInspectionData.getPad(),
                                      sliceNameEnum,
                                      MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL,
                                      correctedExcessGraylevel,
                                      true,
                                      true);
      subtype.addLearnedJointMeasurementData(excessGraylevelLearnedMeasurement);
    }
  }

  /**
   * @author Patrick Lacz
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Locator.setLearnedSettingsToDefaults(subtype);

    List<AlgorithmSettingEnum> insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList = getAllInsufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList(subtype);
    for(AlgorithmSettingEnum insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnum : insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList)
    {
      subtype.setSettingToDefaultValue(insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnum);
    }
    
    //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE);
    
    List<AlgorithmSettingEnum> maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList = getAllMaximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList(subtype);
    for(AlgorithmSettingEnum maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnum : maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList)
    {
      subtype.setSettingToDefaultValue(maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnum);
    }
    
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION);
    
    List<AlgorithmSettingEnum> barrelSliceAlgorithmSettingEnumList = getAllNominalBarrelSolderSignalAlgorithmSettingEnumList(subtype);
    for(AlgorithmSettingEnum barrelSliceAlgorithmSettingEnum : barrelSliceAlgorithmSettingEnumList)
    {
      subtype.setSettingToDefaultValue(barrelSliceAlgorithmSettingEnum);
    }

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH);
  }
  
  /**
   * @author Lam Wai Beng
   * @author Patrick Lacz
   * @author Siew Yeng - added parameter expectedPinLengthInPixels
   */
  protected JointMeasurement measureReversedGraylevelOfPinRegion(JointInspectionData jointInspectionData,
                                                             ReconstructedImages reconstructedImages,
                                                             SliceNameEnum sliceNameEnum,
                                                             Image pinTemplateImage,
                                                             int expectedPinWidthInPixels,
                                                             int expectedPinLengthInPixels,
                                                             int borderPixels, // of pinTemplateImage
                                                             float pinSearchDiameterPercent, // % of barrel diameter for circle ROI
                                                             float regionInnerEdgeAsFractionOfInterPadDistance,
                                                             float regionOuterEdgeAsFractionOfInterPadDistance) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect( jointInspectionData != null);
    Assert.expect( pinTemplateImage != null);
    Assert.expect(expectedPinWidthInPixels != 0);
    Assert.expect(expectedPinLengthInPixels != 0);
    Assert.expect(borderPixels >= 0 );

    // Will not handle barrel slices
    Assert.expect(sliceNameEnum == sliceNameEnum.THROUGHHOLE_PROTRUSION
                                           || sliceNameEnum==sliceNameEnum.THROUGHHOLE_INSERTION) ;

    Image image = reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage();
    // Located barrel region
    RegionOfInterest barrelRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
    RegionOfInterest pinRegionForThicknessMeasurement = new RegionOfInterest(barrelRegion);
    RegionOfInterest pinSearchRegion = new RegionOfInterest(barrelRegion);
    // create the circular ROI to search for pin.
    // Pin search region may be bigger than barrel region to allow possibilities of bent pin
    pinSearchRegion.scaleFromCenterXY(pinSearchDiameterPercent,pinSearchDiameterPercent);
    pinSearchRegion=RegionOfInterest.createRegionFromIntersection(pinSearchRegion,image);
    pinSearchRegion.setRegionShapeEnum(RegionShapeEnum.OBROUND);

    // don't call this function if you don't need to do a template search! Wastefull
    if( pinTemplateImage.getWidth() < pinSearchRegion.getWidth()
                  && pinTemplateImage.getHeight() < pinSearchRegion.getHeight() )
    {
      // find the pin within the pin search region
      DoubleRef matchQuality = new DoubleRef(0.0);
      ImageCoordinate matchedPosition = Filter.matchTemplate(image,
                                                             pinTemplateImage,
                                                             pinSearchRegion,
                                                             MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                             matchQuality);

      pinRegionForThicknessMeasurement.setMinXY(matchedPosition.getX() + borderPixels, matchedPosition.getY() + borderPixels);
      pinRegionForThicknessMeasurement.setWidthKeepingSameMinX(expectedPinWidthInPixels);
      pinRegionForThicknessMeasurement.setHeightKeepingSameMinY(expectedPinLengthInPixels);
     }
    else // no template locating required since the pin diameter is larger than the search region!
    {
      pinRegionForThicknessMeasurement.setWidthKeepingSameCenter(expectedPinWidthInPixels);
      pinRegionForThicknessMeasurement.setHeightKeepingSameCenter(expectedPinLengthInPixels);
    }
    pinRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(pinRegionForThicknessMeasurement, image);
    pinRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.OBROUND);


    // compute the background graylevel from the region around the pin

    float pinGraylevel = Statistics.mean(image,pinRegionForThicknessMeasurement );
    int interPadDistance = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels();

    RegionOfInterest outerBackgroundRegionForThicknessMeasurement = new RegionOfInterest(barrelRegion);
    outerBackgroundRegionForThicknessMeasurement.setWidthKeepingSameCenter(
            (int)Math.ceil(interPadDistance*regionOuterEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getWidth());
    outerBackgroundRegionForThicknessMeasurement.setHeightKeepingSameCenter(
            (int)Math.ceil(interPadDistance*regionOuterEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getHeight());
    outerBackgroundRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(outerBackgroundRegionForThicknessMeasurement, image);
    outerBackgroundRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    RegionOfInterest exclusionRegionForBackgroundMeasurement = new RegionOfInterest(barrelRegion);
    exclusionRegionForBackgroundMeasurement.setWidthKeepingSameCenter(
          (int)Math.ceil(interPadDistance*regionInnerEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getWidth());
    exclusionRegionForBackgroundMeasurement.setHeightKeepingSameCenter(
          (int)Math.ceil(interPadDistance*regionInnerEdgeAsFractionOfInterPadDistance)*2+barrelRegion.getHeight());
    exclusionRegionForBackgroundMeasurement = RegionOfInterest.createRegionFromIntersection(exclusionRegionForBackgroundMeasurement, image);
    exclusionRegionForBackgroundMeasurement.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);

    // make absolutely certain we have a valid region for collecting the background.
    if (outerBackgroundRegionForThicknessMeasurement.fitsWithinRegionOfInterest(exclusionRegionForBackgroundMeasurement))
    {
      exclusionRegionForBackgroundMeasurement = new RegionOfInterest(outerBackgroundRegionForThicknessMeasurement);
      exclusionRegionForBackgroundMeasurement.setWidthKeepingSameCenter(outerBackgroundRegionForThicknessMeasurement.getWidth() - 2);
      exclusionRegionForBackgroundMeasurement.setHeightKeepingSameCenter(outerBackgroundRegionForThicknessMeasurement.getHeight() - 2);
    }

    float backgroundGraylevel = Statistics.medianAroundRegion(image, outerBackgroundRegionForThicknessMeasurement, exclusionRegionForBackgroundMeasurement);

    // combine the background and barrel graylevels into a corrected graylevel (equivalent to thickness)
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float correctedGraylevel = AlgorithmUtil.getCorrectedGreylevel(backgroundGraylevel, pinGraylevel, thicknessTable, backroundSensitivityOffset);

    float reversedGraylevel = 255.0f - correctedGraylevel;

   JointMeasurement reversedGraylevelMeasurement = new JointMeasurement(
            this,
            MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL,
            MeasurementUnitsEnum.NONE,
            jointInspectionData.getPad(),
            sliceNameEnum,
            reversedGraylevel);

    // show what we did.
    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    reversedGraylevelMeasurement);
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(), sliceNameEnum, jointInspectionData, this, false,
          new MeasurementRegionDiagnosticInfo(pinRegionForThicknessMeasurement, MeasurementRegionEnum.PIN_REGION2),
          new MeasurementRegionDiagnosticInfo(pinSearchRegion, MeasurementRegionEnum.LOCATE_SEARCH_REGION),
          new MeasurementRegionDiagnosticInfo(outerBackgroundRegionForThicknessMeasurement, MeasurementRegionEnum.BACKGROUND_REGION),
          new MeasurementRegionDiagnosticInfo(exclusionRegionForBackgroundMeasurement, MeasurementRegionEnum.BACKGROUND_REGION));


    }
    return reversedGraylevelMeasurement;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  static public List<SliceNameEnum> getAllBarrelSliceEnumList(Subtype subtype)
  {
    Assert.expect(subtype != null);
      
    List<SliceNameEnum> barrelSliceEnumList = new ArrayList();
    int numberOfBarrelSlices = getNumberOfSlice(subtype);
    
    barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL);
      
    if(numberOfBarrelSlices >= 2)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_2);
    }
    if(numberOfBarrelSlices >= 3)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_3);
    }
    if(numberOfBarrelSlices >= 4)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_4);
    }
    if(numberOfBarrelSlices >= 5)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_5);
    }
    if(numberOfBarrelSlices >= 6)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_6);
    }
    if(numberOfBarrelSlices >= 7)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_7);
    }
    if(numberOfBarrelSlices >= 8)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_8);
    }
    if(numberOfBarrelSlices >= 9)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_9);
    }
    if(numberOfBarrelSlices >= 10)
    {
      barrelSliceEnumList.add(SliceNameEnum.THROUGHHOLE_BARREL_10);
    }
    
    return barrelSliceEnumList;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private List<AlgorithmSettingEnum> getAllNominalBarrelSolderSignalAlgorithmSettingEnumList(Subtype subtype)
  {
    Assert.expect(subtype != null);

    List<AlgorithmSettingEnum> nominalBarrelSolderSignalAlgorithmSettingEnumList = new ArrayList();
    int numberOfBarrelSlices = getNumberOfSlice(subtype);

    nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL);
    if (numberOfBarrelSlices >= 2)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 3)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 4)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 5)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 6)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 7)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 8)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 9)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL);
    }
    if (numberOfBarrelSlices >= 10)
    {
      nominalBarrelSolderSignalAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL);
    }
    

    return nominalBarrelSolderSignalAlgorithmSettingEnumList;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private List<AlgorithmSettingEnum> getAllInsufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList(Subtype subtype)
  {
      Assert.expect(subtype != null);
      
      List<AlgorithmSettingEnum> insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList = new ArrayList();
      int numberOfBarrelSlices = getNumberOfSlice(subtype);
      
      insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL);
      
      if(numberOfBarrelSlices >= 2)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2);
      }
      if(numberOfBarrelSlices >= 3)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3);
      }
      if(numberOfBarrelSlices >= 4)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4);
      }
      if(numberOfBarrelSlices >= 5)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5);
      }
      if(numberOfBarrelSlices >= 6)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6);
      }
      if(numberOfBarrelSlices >= 7)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7);
      }
      if(numberOfBarrelSlices >= 8)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8);
      }
      if(numberOfBarrelSlices >= 9)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9);
      }
      if(numberOfBarrelSlices >= 10)
      {
        insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10);
      }
      return insufficientDifferenceFromNominalBarrelSliceAlgorithmSettingEnumList;
  }
  
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel1SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);

    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel2SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_2,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel3SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_3,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel4SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_4,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel5SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_5,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel6SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_6,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel7SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_7,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel8SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_8,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel9SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  protected int createBarrel10SlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalBarrelSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_BARREL_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);
    addAlgorithmSetting(nominalBarrelSolderSignal);
    
        /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10, // setting enum
      displayOrder++, // display order,
      50.0f, // default value
      -50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Keys
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeight);

   /** @todo PE figure out default value */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting barrelSliceHeightinMils = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BARREL_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(barrelSliceHeightinMils);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalBarrelSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_10,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
   /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily 
   * @author Wei Chin
   * @author Siew Yeng - add parameter jointTypeEnums
   */
  //Broken Pin
  protected int createProjectionSlicesSetting(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting nominalProjectionSolderSignal = new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROJECTION_SOLDER_SIGNAL,
            displayOrder++,
            230.0f, // default value
            0.0f, // minimum value
            255.0f, // maximum value
            MeasurementUnitsEnum.NONE,
            "HTML_DESC_THROUGHHOLE_(NOMINAL_PROJECTION_SOLDER_SIGNAL)_KEY", // description URL key
            "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_PROJECTION_SOLDER_SIGNAL)_KEY", // desailed description URL key
            "IMG_DESC_THROUGHHOLE_(NOMINAL_PROJECTION_SOLDER_SIGNAL)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.STANDARD,
            currentVersion);  
    addAlgorithmSetting(nominalProjectionSolderSignal);
    
    //Siew Yeng - XCR-3318
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          nominalProjectionSolderSignal,
                                                          SliceNameEnum.CAMERA_0,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    }

    return displayOrder;
  }
  
   /**
   * This is just a utility routine to help the ThroughHole Inspection Family
   * know which slices it needs to inspect.
   *
   * @author Jack Hwee
   */
  //Broken Pin
  static public boolean hasProjectionSliceForShort(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // you shouldn't be calling this except from ThroughHole
//    Assert.expect(subtype.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE));

    //Siew Yeng - XCR-3318
    if(subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE))
    {
      String inspectionSlice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE);

      SliceNameEnum.getEnumFromIndex( SliceNameEnum.CAMERA_0.getId()).getName();
      //Broken Pin
          //Lim, Lay Ngor - XCR 3238 - Obsolete the previous software config checking on GenerateMultiAngleImage.
      //Use the project setting to ensure generateMultiAngleImagesEnable.
      //boolean isGenerateMultiAngleImagesEnabled = RepairImagesConfigManager.getInstance().isGenerateMultiAngleImagesEnabled();//Obsolete the previous config key and change to use the new project key
      boolean isGenerateMultiAngleImagesEnabledFromProjectSetting = Project.getCurrentlyLoadedProject().isGenerateMultiAngleImage();
      //if (isGenerateMultiAngleImagesEnabled && (inspectionSlice.equals(_PROJECTION_SLICE_ONLY) || inspectionSlice.equals(_PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES)))
      if (isGenerateMultiAngleImagesEnabledFromProjectSetting && (inspectionSlice.equals(_PROJECTION_SLICE_ONLY) || inspectionSlice.equals(_PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES)))
      {
        return true;
      }
    }
    return false;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  private List<AlgorithmSettingEnum> getAllMaximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList(Subtype subtype)
  {
    Assert.expect(subtype != null);

    List<AlgorithmSettingEnum> maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList = new ArrayList();
    int numberOfBarrelSlices = getNumberOfSlice(subtype);

    maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL);

    if(numberOfBarrelSlices >= 2)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2);
    }
    if(numberOfBarrelSlices >= 3)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3);
    }
    if(numberOfBarrelSlices >= 4)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4);
    }
    if(numberOfBarrelSlices >= 5)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5);
    }
    if(numberOfBarrelSlices >= 6)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6);
    }
    if(numberOfBarrelSlices >= 7)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7);
    }
    if(numberOfBarrelSlices >= 8)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8);
    }
    if(numberOfBarrelSlices >= 9)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9);
    }
    if(numberOfBarrelSlices >= 10)
    {
      maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList.add(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10);
    }
    return maximumExcessSolderSignalBarrelSliceAlgorithmSettingEnumList;
  }
}
