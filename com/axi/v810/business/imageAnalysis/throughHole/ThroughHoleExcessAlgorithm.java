package com.axi.v810.business.imageAnalysis.throughHole;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * @author Khaw Chek Hau
 * @XCR3745: Support PTH Excess Solder Algorithm
 */
public class ThroughHoleExcessAlgorithm extends Algorithm
{
  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  public ThroughHoleExcessAlgorithm(InspectionFamily inspectionFamily)
  {
    super(AlgorithmEnum.EXCESS, InspectionFamilyEnum.THROUGHHOLE);
    Assert.expect(inspectionFamily != null);

    addAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  public ThroughHoleExcessAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.EXCESS, inspectionFamilyEnum);
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  private void addAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;
    
    AlgorithmSetting percentExcessDiameterToTest = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST,
      displayOrder++,
      100.0f, // default value
      20.0f, // minimum value
      180.0f, // maximum value
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_THROUGHHOLE_(PERCENT_OF_EXCESS_DIAMETER_TO_TEST)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(PERCENT_OF_EXCESS_DIAMETER_TO_TEST)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(PERCENT_OF_EXCESS_DIAMETER_TO_TEST)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(percentExcessDiameterToTest);
    
    AlgorithmSetting maximumExcessSolderSignalPinside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_PINSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_PINSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_PINSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          maximumExcessSolderSignalPinside,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    addAlgorithmSetting(maximumExcessSolderSignalPinside);  
    
   AlgorithmSetting maximumExcessSolderSignalComponentside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_COMPONENTSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_COMPONENTSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_COMPONENTSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.THROUGH_HOLE,
                                                          maximumExcessSolderSignalComponentside,
                                                          SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    addAlgorithmSetting(maximumExcessSolderSignalComponentside);  

    List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>(Arrays.asList(JointTypeEnum.THROUGH_HOLE));
    
    displayOrder = createBarrel1SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel2SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel3SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel4SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel5SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel6SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel7SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel8SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel9SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel10SlicesExcessSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null );
    Assert.expect(jointInspectionDataObjects.size() > 0);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();
    SliceNameEnum barrelSliceName = SliceNameEnum.THROUGHHOLE_BARREL;

    Collection<SliceNameEnum> reconstructedSlices = reconstructedImages.getSliceNames();

    int numberOfSlices = getNumberOfSlice(subtypeOfJoints);  
     
    // for each slice in [bottom, barrel, top]
    Assert.expect(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_PIN_SIDE));
    classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_PIN_SIDE, jointInspectionDataObjects,
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE));
    
    Assert.expect(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE));
    classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, jointInspectionDataObjects,
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE));

    Assert.expect(reconstructedSlices.contains(barrelSliceName));
    classifySlice(reconstructedImages, barrelSliceName, jointInspectionDataObjects,
                  (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL));

    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_2) && numberOfSlices >= 2)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_2, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2));
    }

    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_3) && numberOfSlices >= 3)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_3, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3));
    }

    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_4) && numberOfSlices >= 4)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_4, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4));
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_5) && numberOfSlices >= 5)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_5, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5));
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_6) && numberOfSlices >= 6)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_6, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6));
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_7) && numberOfSlices >= 7)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_7, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7));
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_8) && numberOfSlices >= 8)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_8, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8));
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_9) && numberOfSlices >= 9)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_9, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9));
    }
    
    if(reconstructedSlices.contains(SliceNameEnum.THROUGHHOLE_BARREL_10) && numberOfSlices >= 10)
    {
      classifySlice(reconstructedImages, SliceNameEnum.THROUGHHOLE_BARREL_10, jointInspectionDataObjects,
                    (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10));
    }
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  private void classifySlice(ReconstructedImages reconstructedImages,
                             SliceNameEnum sliceNameEnum,
                             List<JointInspectionData> jointInspectionDataObjects,
                             float maxExcessSolderSignalThreshold)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    Assert.expect(jointInspectionDataObjects.isEmpty() == false);
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);
     
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      if (ThroughHoleMeasurementAlgorithm.hasCorrectedExcessGraylevelMeasurement(jointInspectionData, sliceNameEnum) == false)
        continue;
        
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                reconstructedImages.getReconstructionRegion(),
                                                reconstructedSlice,
                                                false);

      JointMeasurement graylevelMeasurement = ThroughHoleMeasurementAlgorithm.getCorrectedExcessGraylevelMeasurement(jointInspectionData, sliceNameEnum);

      // test against the maximum excess solder signal
      boolean passedMaximumExcess = classifyByMaximumExcessSolderSignal(reconstructedImages, maxExcessSolderSignalThreshold, jointInspectionData, graylevelMeasurement);

      double fractionfBarrelDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST) / 100.0f;
      double pinDiameter = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH) * fractionfBarrelDiameter;
      pinDiameter =  new Double(MathUtil.convertUnits(pinDiameter, MathUtilEnum.MILS, MathUtilEnum.NANOMETERS));
      
      Image jointImage = new Image(reconstructedSlice.getImage().getWidth(), reconstructedSlice.getImage().getHeight());
      Transform.copyImageIntoImage(reconstructedSlice.getImage(), jointImage);

      RegionOfInterest jointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
      // scaling for barrel diameter
      RegionOfInterest barrelRegionForThicknessMeasurement = new RegionOfInterest(jointRoi);
      barrelRegionForThicknessMeasurement.scaleFromCenterXY(fractionfBarrelDiameter, fractionfBarrelDiameter);
      barrelRegionForThicknessMeasurement = RegionOfInterest.createRegionFromIntersection(barrelRegionForThicknessMeasurement, reconstructedSlice.getOrthogonalImage());
      barrelRegionForThicknessMeasurement.setRegionShapeEnum(RegionShapeEnum.OBROUND);

      // Show the pass/fail diagnostic
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              reconstructedImages.getReconstructionRegion(),
                                                              sliceNameEnum,
                                                              passedMaximumExcess);
    }
  }

  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  private boolean classifyByMaximumExcessSolderSignal(ReconstructedImages reconstructedImages,
                                                      float maxExcessSolderSignalThreshold,
                                                      JointInspectionData jointInspectionData,
                                                      JointMeasurement graylevelMeasurement)
  {
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    SliceNameEnum sliceName = graylevelMeasurement.getSliceNameEnum();

    float correctedGraylevel = graylevelMeasurement.getValue();

    JointMeasurement maxExcessSolderSignalMeasurement = new JointMeasurement(this,
        MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL,
        MeasurementUnitsEnum.GRAYLEVEL,
        jointInspectionData.getPad(),
        sliceName,
        correctedGraylevel);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructedImages.getReconstructionRegion(),
                                                    jointInspectionData,
                                                    maxExcessSolderSignalMeasurement);
    jointInspectionResult.addMeasurement(maxExcessSolderSignalMeasurement);

    if (correctedGraylevel > maxExcessSolderSignalThreshold)
    {
      // reject as excess
      JointIndictment indictment = new JointIndictment(IndictmentEnum.EXCESS, this, sliceName);
      indictment.addFailingMeasurement(maxExcessSolderSignalMeasurement);
      jointInspectionResult.addIndictment(indictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructedImages.getReconstructionRegion(),
                                                      sliceName,
                                                      maxExcessSolderSignalMeasurement,
                                                      maxExcessSolderSignalThreshold);

      // indicate that the joint failed.
      return false;
    }
    return true;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel1SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);   
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                            excessMaximumSolderSignalBarrel,
                                                            SliceNameEnum.THROUGHHOLE_BARREL,
                                                            MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel2SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_2,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel3SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_3,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel4SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_4,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel5SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_5,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }

  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel6SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_6,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel7SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_7,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel8SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_8,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel9SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_9,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @param inspectionFamily
   * @return 
   * 
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  protected int createBarrel10SlicesExcessSettings(int displayOrder, int currentVersion, InspectionFamily inspectionFamily, List<JointTypeEnum> jointTypeEnums)
  {
    AlgorithmSetting excessMaximumSolderSignalBarrel = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10,
        displayOrder++,
        255.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(EXCESS_MAXIMUM_SOLDER_SIGNAL_BARREL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(excessMaximumSolderSignalBarrel);
    
    for(JointTypeEnum jointTypeEnum : jointTypeEnums)
    {
      inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                          excessMaximumSolderSignalBarrel,
                                                          SliceNameEnum.THROUGHHOLE_BARREL_10,
                                                          MeasurementEnum.THROUGHHOLE_EXCESS_SOLDER_SIGNAL);
    }
    
    return displayOrder;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  public boolean algorithmIsEnabledByDefault(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return false;
  }
}
