#############################################################################
# FILE   : Makefile
#
# PURPOSE: To provide a standard Makefile template for use in SPT.
#          This template handles compiling Java files including
#          rmic and javah.  To use this template, copy it into
#          your directory and replace all XXXX with the appropriate
#          things as described in the comments.  In some cases you will
#          want to replace the XXXX with nothing.  Each java directory
#          should have one Makefile following this template in it.
#          This Makefile will create the following files in each directly:
#            - class files
#            - RMI stub class files (if needed)
#            - JNI .h files (if needed)
#            - JNI dll file (if needed)
#          This Makefile will not create a jar file.  There will be
#          one main Makefile under the agt5dx directory that will be
#          responsible for calling the other directory Makefiles.  It
#          will also create the application jar or jar files.  There
#          is no need to create individual directory level jar files
#
#          Responsibilities of the main Makefile:
#            - check that the proper environment variables are set
#              for the Makefiles to work
#            - check that software architecture is followed by looking
#              at .java imports
#            - call all directory Makefiles
#            - create the application jar file or files
#
#          Responsibilites of the directory Makefiles:
#            - create all .class, .h, and .dll files
#            - move appropriate files into the release directory
#
# NOTE   : This Makefile uses the following environment variables:
#          JAVACOPTS - options for the javac compiler
#          JAVAHOPTS - options for the javah compiler
#          RMICOPTS  - options for the rmic compiler
#          CL        - options for Microsoft visual c++ compiler
#          JAVA_HOME - location for java jdk (not jre)
#
# AUTHOR : Bill Darbie
##############################################################################
# make this work like a POSIX Makefile - standard is good
.POSIX:

# set what shell to use
SHELL=sh

##############################################################################
# define dependencies for a make
##############################################################################
all: 

##############################################################################
# general Makefile setup
##############################################################################
MAKEFILE_VERSION="1.11"

##############################################################################
# set the current directory relative to the base java directory 
# in other words com/axi/XXXX
##############################################################################
THIS_DIR=com/axi/v810/business/imageAnalysis/throughHole

##############################################################################
# set any flags you want to go to the c++ compile
##############################################################################
CC_COMMON_FLAGS=

CC_NODEBUG_FLAGS= \
  $(CC_COMMON_FLAGS) \
  $(CC_COMMON_NODEBUG_FLAGS)
  
CC_DEBUG_FLAGS= \
  $(CC_COMMON_FLAGS) \
  $(CC_COMMON_DEBUG_FLAGS)

##############################################################################
# set any -I options for the c++ compiler
##############################################################################
CC_INCLUDES= \
  $(CC_COMMON_INCLUDE) \
  -I$(BOOSTROOT)

##############################################################################
# set all the libraries to link to here
##############################################################################
CC_DEBUG_LINK_LIBS= \
  $(CC_COMMON_LINK_LIBS)

CC_NODEBUG_LINK_LIBS= \
  $(CC_COMMON_LINK_LIBS)

##############################################################################
# list all .class files that have a main method in them that are NOT
# regression tests (test files go in JAVA_TEST_CLASS_FILES)
##############################################################################
JAVA_MAINS=

##############################################################################
# list .class files to be compiled for the customer here
# do not include unit or regression test files here or any files listed
# in JAVA_MAINS
# EXAMPLE: JAVA_CLASS_FILES=Bill.class
##############################################################################
JAVA_CLASS_FILES= \
  ThroughHoleAlgorithmUtil.class \
  ThroughHoleExcessAlgorithm.class \
  ThroughHoleInspectionFamily.class \
  ThroughHoleMeasurementAlgorithm.class \
  ThroughHoleInsufficientAlgorithm.class \
  ThroughHoleOpenAlgorithm.class


##############################################################################
# list all .class files that need to be built for unit or regression testing
# here
# EXAMPLE: JAVA_TEST_CLASS_FILES=TestBill.java
##############################################################################
JAVA_TEST_CLASS_FILES=

##############################################################################
# list all _Stub.class files that need rmic to be run on here
# EXAMPLE: RMI_CLASS_FILES=Bill_Stub.class
##############################################################################
RMI_CLASS_FILES=

##############################################################################
# for each file listed in RMI_CLASS_FILES put a corresponding rule here that
# follows the form:
# Bill_Stub.class: Bill.class
#     $(BUILD_RMI_STUB)
# Be sure to put a tab before the $(BUILD_RMI_STUB)
##############################################################################

##############################################################################
# list all .h files that need javah run on them here
# EXAMPLE: JAVAH_FILES=com_axi_mtd_agt5dx_hardware_Bill.h
##############################################################################
JAVAH_FILES=


##############################################################################
# for each file listed in JAVAH_FILES put a corresponding rule here that
# follows the form:
# Bill.h: Bill.class
#     $(BUILD_JNI_HEADER)
# Be sure to put a tab before the $(BUILD_JNI_HEADER)
##############################################################################

##############################################################################
# list the name of the dll that needs to be created
# EXAMPLE: JNI_DLL_FILE=nativeInt.dll
##############################################################################
JNI_DLL_FILE=

all: $(JNI_DLL_FILE)

##############################################################################
# list all the .cpp files that need to be compiled to create the JNI .dll file
# that you listed above
# EXAMPLE: JNI_CPPL_FILES=com_axi_mtd_Bill.cpp
##############################################################################
JNI_CPP_FILES=


##############################################################################
# create a rule for your dll file here
# It should follow the form:
# Bill.dll: $(JNI_CPP_FILES) $(JAVAH_FILES)
#     $(BUILD_JNI_DLL)   
##############################################################################
nativeImageAnalysis.dll: $(JNI_CPP_FILES) $(JAVAH_FILES)
	$(BUILD_JNI_DLL)

##############################################################################
# define a rule to move any files from the current directory to the 
# release directory.  The release directory should contain all files
# that the customer will need in the customer directory format.
# This includes files like .properties and .gif
# Do NOT copy .class files into the release directory
#
# REMEMBER TO FILL IN THE localClean TARGET TO REMOVE THE SAME FILES

# NOTE that the following macros are provided for you use:
#  $(RELEASE_DIR)
#  $(RELEASE_CONFIG_DIR)
#  $(RELEASE_PROPS_DIR)
#  $(RELEASE_IMAGES_DIR)
#  $(RELEASE_BIN)
#  $(RELEASE_LIB)
##############################################################################
release: alwaysDoThis
#	@echo "\n#### copying generated files to the release directory ####"
#	XXXX -  put in any required cp commands here or remove this line and the line above if nothing needs to be done

##############################################################################
# do a remove on all files that you put into the release directory here
#  $(RELEASE_DIR)
#  $(RELEASE_CONFIG_DIR)
#  $(RELEASE_PROPS_DIR)
#  $(RELEASE_IMAGES_DIR)
#  $(RELEASE_BIN)
#  $(RELEASE_LIB)
##############################################################################
clean: localClean
localClean: alwaysDoThis
#	@command="rm -f MACRO/XXXX"; \
#        echo "  $$command"; \
#        $$command

##############################################################################
# now include all the rules to build the files listed above
##############################################################################
include ../../../javaCommonMakefile
