package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.ImageSetData;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;
import ij.ImagePlus;
import ij.process.ImageConverter;
import java.util.*;

/**
 *
 * @author Jack Hwee
 */
public class MaskImageCompatibilityCheck
{
  private static MaskImageCompatibilityCheck _maskImageCompatibilityCheck = null;
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  private ReconstructedImagesProducer _reconstructedImagesProducer;
  private Map<Subtype, Boolean> _subtypeToImageResolutionCompatibilityBoolMap = new LinkedHashMap<Subtype, Boolean>();
  private Map<Subtype, DoubleCoordinate> _subtypeToImageResolutionBoolMap = new LinkedHashMap<Subtype, DoubleCoordinate>();
  private Map<Subtype, DoubleCoordinate> _subtypeToImageResolutionMap = new LinkedHashMap<Subtype, DoubleCoordinate>();
  private ReconstructedImages _reconstructedImages = null;
    
  /**
   * @author Jack Hwee
   */
  public static synchronized MaskImageCompatibilityCheck getInstance()
  {
    if (_maskImageCompatibilityCheck == null)
    {
      _maskImageCompatibilityCheck = new MaskImageCompatibilityCheck();
    }
    return _maskImageCompatibilityCheck;
  }
    
  /**
   * @author Jack Hwee
   */
  private MaskImageCompatibilityCheck()
  {
    // do nothing
  }
 
  /**
   * @author Jack Hwee 
   */
  public void checkMaskImageResolution(Project project, java.util.List<ImageSetData> selectedImageSets) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(selectedImageSets != null);

    _subtypeToImageResolutionCompatibilityBoolMap.clear();
    _subtypeToImageResolutionBoolMap.clear();
    java.util.List<Subtype> subtypes = project.getPanel().getInspectedSubtypes();

    for (Subtype subtype : subtypes)
    {
      if(subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.LARGE_PAD) == false)
        continue;
      
      //Siew Yeng - XCR-2843 - masking rotation
      List<String> files = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                            FileName.getMaskImagePatternString(subtype.getShortName()));
      if(files.isEmpty() == false)
      {
        _reconstructedImages = getReconstructedImages(project, selectedImageSets, subtype);

        checkMaskImageResolutionForEachSubtype(subtype);

        if(subtype.hasSubSubtypes())
        {
          for(Subtype subSubtype : subtype.getSubSubtypes())
          {
            checkMaskImageResolutionForEachSubtype(subSubtype);
          }
        }

        if(_reconstructedImages != null)
        {
          _reconstructedImages.decrementReferenceCount();
          _reconstructedImages = null;
        }
      }
    }
  }
  
  /**
   * Factor out from checkMaskImageResolution to support subSubtype Masking
   * @author Siew Yeng
   */
  public void checkMaskImageResolutionForEachSubtype(Subtype subtype)
  {
    //String maskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + subtype.getShortName() + ".png";
    //Siew Yeng - XCR-2843 - masking rotation
    List<String> files = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                            FileName.getMaskImagePatternString(subtype.getShortName()));
    if(files.isEmpty() == false)
    {
      String maskImagePath = files.get(0);
      if (_reconstructedImages != null)
      {
        SliceNameEnum sliceNameEnum = subtype.getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
        ReconstructedSlice slice = _reconstructedImages.getReconstructedSlice(sliceNameEnum);
        com.axi.util.image.Image sliceImage = slice.getOrthogonalImage();
        _subtypeToImageResolutionMap.put(subtype, new DoubleCoordinate(sliceImage.getWidth(), sliceImage.getHeight()));

        try
        {
          convertToGrayMaskImageIfNecessary(maskImagePath);

          Image maskImage = XrayImageIoUtil.loadImage(maskImagePath);

          DoubleCoordinate dimension = new DoubleCoordinate(maskImage.getWidth(), maskImage.getHeight());
          _subtypeToImageResolutionBoolMap.put(subtype, dimension);

          if ((sliceImage.getWidth() == maskImage.getWidth()) && (sliceImage.getHeight() == maskImage.getHeight()))
          {
            _subtypeToImageResolutionCompatibilityBoolMap.put(subtype, true);
          }
          else
          {
            _subtypeToImageResolutionCompatibilityBoolMap.put(subtype, false);
          }

          maskImage.decrementReferenceCount();
        }
        catch (DatastoreException ex)
        {
          DatastoreException dex = new CannotReadDatastoreException(maskImagePath);
          dex.initCause(ex);
          dex.printStackTrace();
        }
      }
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public ReconstructedImages getReconstructedImages(Project project, java.util.List<ImageSetData> selectedImageSets, Subtype subtype) throws DatastoreException, XrayTesterException
  {
    ReconstructedImages reconstructedImages = null;
    BooleanRef bool = new BooleanRef();

    TestProgram testProgram = project.getTestProgram();

    // Set filter to look for subtype--this will be used in ManagedOfflineImageSet
    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(subtype);

    // clear out all the measurements, etc.
    testProgram.startNewInspectionRun();

    _imageAcquisitionEngine.initialize();

    _reconstructedImagesProducer = _imageAcquisitionEngine.getReconstructedImagesProducer();

    _reconstructedImagesProducer.acquireOfflineProductionImages(testProgram, selectedImageSets.get(0));

    reconstructedImages = _imageAcquisitionEngine.getReconstructedImages(bool);
    
    if (reconstructedImages != null)
    {
      _reconstructedImagesProducer.freeReconstructedImages(reconstructedImages.getReconstructionRegion());
      _reconstructedImagesProducer.clear();
      _imageAcquisitionEngine.freeReconstructedImages(reconstructedImages.getReconstructionRegion());
      _imageAcquisitionEngine.clear();
    }

    if (bool.getValue() == false)
    {
      return reconstructedImages;
    }
    else
    {
      return null;
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public Map<Subtype, Boolean> getSubtypeToImageResolutionCompatibilityBoolMap()
  {
    return _subtypeToImageResolutionCompatibilityBoolMap;
  }
  
  /**
   * @author Jack Hwee
   */
  public Map<Subtype, DoubleCoordinate> getSubtypeToImageResolutionBoolMap()
  {
    return _subtypeToImageResolutionBoolMap;
  }
  
  /**
   * @author Jack Hwee
   */
  public Map<Subtype, DoubleCoordinate> getSubtypeToImageResolutionMap()
  {
    return _subtypeToImageResolutionMap;
  }
  
  /**
   * @author Jack Hwee
   */
  static private void convertToGrayMaskImageIfNecessary(String path)
  {
    ImagePlus maskImage = ij.IJ.openImage(path);

    if (maskImage.getType() == ImagePlus.GRAY8)
    {
      maskImage.flush();
      maskImage.close();
      // return false;
    }
    else
    {
      ImageConverter img = new ImageConverter(maskImage);
      img.convertToGray8();
      ij.IJ.save(maskImage, path);

      maskImage.flush();
      maskImage.close();
      // return true;
    }
  }
  
  /**
   * @author Jack Hwee
   */
  public void clearReconstructedImages()
  {
    _subtypeToImageResolutionCompatibilityBoolMap.clear();
    _subtypeToImageResolutionBoolMap.clear();
    _subtypeToImageResolutionCompatibilityBoolMap.clear();
  }
}
