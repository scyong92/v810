package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;
import com.axi.v810.business.testExec.*;

/**
 * Specialization of InspectionEvent that pertains to algorithm diagnostics.
 *
 * @author Matt Wharton
 */
public class AlgorithmDiagnosticInspectionEvent extends InspectionEvent
{
  private AlgorithmDiagnosticMessage _algorithmDiagnosticMessage = null;

  /**
   * Constructor (posting diagnostic).
   *
   * @author Matt Wharton
   */
  public AlgorithmDiagnosticInspectionEvent(
      AlgorithmDiagnosticMessage algorithmDiagMessage,
      InspectionEventEnum eventEnum)
  {
    super(eventEnum);

    Assert.expect(algorithmDiagMessage != null);
    Assert.expect(eventEnum != null);
    Assert.expect(eventEnum.equals(InspectionEventEnum.POSTING_DIAGNOSTIC));
    _algorithmDiagnosticMessage = algorithmDiagMessage;
  }

  /**
   * @author Matt Wharton
   */
  public AlgorithmDiagnosticMessage getAlgorithmDiagnosticMessage()
  {
    Assert.expect(_algorithmDiagnosticMessage != null);

    return _algorithmDiagnosticMessage;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasAlgorithmDiagnosticMessage()
  {
    return _algorithmDiagnosticMessage != null;
  }


  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    _algorithmDiagnosticMessage.incrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    _algorithmDiagnosticMessage.decrementReferenceCount();
  }

}
