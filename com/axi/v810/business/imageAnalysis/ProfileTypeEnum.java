package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;

/**
 * Represents the different types of profiles that are supported.
 *
 * @author Matt Wharton
 */
public class ProfileTypeEnum extends com.axi.util.Enum
{
    private static int _index = 0;

    public static final ProfileTypeEnum SOLDER_PROFILE = new ProfileTypeEnum(_index++);
    public static final ProfileTypeEnum BACKGROUND_PROFILE = new ProfileTypeEnum(_index++);

    /**
     * @author Matt Wharton
     */
    private ProfileTypeEnum(int id)
    {
      super(id);
    }
}
