package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;

public class Test_MeasurementRegionEnumArrayManager extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Peter Esbensen
   */
  private Test_MeasurementRegionEnumArrayManager()
  {
    // Do nothing...
  }

  /**
   * Main test method.
   *
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    MeasurementRegionEnumArrayManager measurementRegionEnumArrayManager = new MeasurementRegionEnumArrayManager();

    // create an array of BACKGROUND_REGION to act as background
    MeasurementRegionEnum[] expectedArray = new MeasurementRegionEnum[10];
    Arrays.fill(expectedArray, MeasurementRegionEnum.BACKGROUND_REGION);

    // setBackgroundArray
    measurementRegionEnumArrayManager.setBackgroundArray(expectedArray);

    // make sure that worked
    MeasurementRegionEnum[] resultingArray = measurementRegionEnumArrayManager.getFinalArray();
    Expect.expect( Arrays.equals(resultingArray, expectedArray) );

    // put in a new enum
    measurementRegionEnumArrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(5, MeasurementRegionEnum.FILLET_REGION);
    expectedArray[5] = MeasurementRegionEnum.FILLET_REGION;

    // put in a second enum
    measurementRegionEnumArrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(6, MeasurementRegionEnum.BARREL_REGION);
    expectedArray[6] = MeasurementRegionEnum.BARREL_REGION;

    // try putting a third enum on top of the first one
    measurementRegionEnumArrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(5, MeasurementRegionEnum.PIN_REGION);
    expectedArray[4] = MeasurementRegionEnum.PIN_REGION;

    // make sure that all worked
    resultingArray = measurementRegionEnumArrayManager.getFinalArray();
    Expect.expect( Arrays.equals(resultingArray, expectedArray));
  }

  /**
   * Test fixture entry point.
   *
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_MeasurementRegionEnumArrayManager());
  }

}
