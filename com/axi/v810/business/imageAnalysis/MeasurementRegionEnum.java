package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;

/**
 * Enumeration of different types of measurement regions.

 * @author Matt Wharton
 */
public class MeasurementRegionEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  private LocalizedString _regionDescription = null;
  private static SortedSet<MeasurementRegionEnum> _setOfAllEnums = new TreeSet<MeasurementRegionEnum>();

  public static final MeasurementRegionEnum CAD_LOCATION_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("CAD_LOCATION_REGION_KEY", null));
  public static final MeasurementRegionEnum LOCATE_SEARCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("LOCATE_SEARCH_REGION_KEY", null));
  public static final MeasurementRegionEnum LOCATE_MATCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("LOCATE_MATCH_REGION_KEY", null));
  public static final MeasurementRegionEnum PAD_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PAD_REGION_KEY", null));
  public static final MeasurementRegionEnum BARREL_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("BARREL_REGION_KEY", null));
  public static final MeasurementRegionEnum PIN_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PIN_REGION_KEY", null));
  public static final MeasurementRegionEnum MEASURED_JOINT_BOUNDARY = new MeasurementRegionEnum(_index++, new LocalizedString("MEASURED_JOINT_BOUNDARY_KEY", null));
  public static final MeasurementRegionEnum BACKGROUND_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("BACKGROUND_REGION_KEY", null));
  public static final MeasurementRegionEnum SHORT_BACKGROUND_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SHORT_BACKGROUND_REGION_KEY", null));
  public static final MeasurementRegionEnum SHORT_PROFILE_START_BIN_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SHORT_PROFILE_START_BIN_REGION_KEY", null));
  public static final MeasurementRegionEnum SHORT_PROFILE_END_BIN_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SHORT_PROFILE_END_BIN_REGION_KEY", null));
  public static final MeasurementRegionEnum SHORT_PROFILE_DISABLE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SHORT_PROFILE_DISABLE_REGION_KEY", null));
  public static final MeasurementRegionEnum PASSING_JOINT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PASSING_JOINT_REGION_KEY", null));
  public static final MeasurementRegionEnum FAILING_JOINT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("FAILING_JOINT_REGION_KEY", null));
  public static final MeasurementRegionEnum DEFINITE_SHORT_DEFECT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("DEFINITE_SHORT_DEFECT_REGION_KEY", null));
  public static final MeasurementRegionEnum QUESTIONABLE_SHORT_DEFECT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("QUESTIONABLE_SHORT_DEFECT_REGION_KEY", null));
  public static final MeasurementRegionEnum EXONERATED_SHORT_DEFECT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("EXONERATED_SHORT_DEFECT_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_BACKGROUND_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("BACKGROUND_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_PAD_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PAD_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_HEEL_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("HEEL_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_HEEL_SEARCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("HEEL_SEARCH_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_HEEL_PEAK_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("HEEL_PEAK_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_CENTER_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("CENTER_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_TOE_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("TOE_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum GULLWING_TOE_PEAK_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("TOE_PEAK_REGION_KEY", null));
  public static final MeasurementRegionEnum ADVANCED_GULLWING_MAX_HEEL_SLOPE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("MAX_HEEL_SLOPE_REGION_KEY", null));
  public static final MeasurementRegionEnum ADVANCED_GULLWING_MAX_TOE_SLOPE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("MAX_TOE_SLOPE_REGION_KEY", null));
  public static final MeasurementRegionEnum ADVANCED_GULLWING_PROFILE_ACROSS_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PROFILE_ACROSS_REGION_KEY", null));
  public static final MeasurementRegionEnum ADVANCED_GULLWING_SIDE_FILLET_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SIDE_FILLET_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum ADVANCED_GULLWING_SIDE_FILLET_HEEL_SEARCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SIDE_FILLET_HEEL_SEARCH_REGION_KEY", null));
  public static final MeasurementRegionEnum ADVANCED_GULLWING_SIDE_FILLET_HEEL_PEAK_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SIDE_FILLET_HEEL_PEAK_REGION_KEY", null));
  public static final MeasurementRegionEnum ADVANCED_GULLWING_SIDE_FILLET_CENTER_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SIDE_FILLET_CENTER_REGION_KEY", null));
  public static final MeasurementRegionEnum FILLET_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("FILLET_REGION_KEY", null));
  public static final MeasurementRegionEnum LOWER_FILLET_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("LOWER_FILLET_REGION_KEY", null));
  public static final MeasurementRegionEnum ALIGNMENT_PAD_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("ALIGNMENT_PAD_REGION_KEY", null));
  public static final MeasurementRegionEnum ALIGNMENT_CONTEXT_PAD_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("ALIGNMENT_CONTEXT_PAD_REGION_KEY", null));
  public static final MeasurementRegionEnum OUTER_FILLET_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("OUTER_FILLET_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum GRID_ARRAY_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("GRID_ARRAY_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum COMPONENT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("COMPONENT_REGION_KEY", null));
  public static final MeasurementRegionEnum INNER_FILLET_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("INNER_FILLET_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum UPPER_FILLET_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("UPPER_FILLET_REGION_KEY", null));
  public static final MeasurementRegionEnum COMPONENT_PROFILE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("COMPONENT_PROFILE_REGION_KEY", null));
  public static final MeasurementRegionEnum OPEN_SIGNAL_SEARCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("OPEN_SIGNAL_SEARCH_REGION_KEY", null));
  public static final MeasurementRegionEnum MARGINAL_VOID_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("MARGINAL_VOID_REGION_KEY", null));
  public static final MeasurementRegionEnum GRID_ARRAY_EDGE_SEARCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("EDGE_SEARCH_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_BACKGROUND_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("BACKGROUND_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_PAD_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PAD_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_HEEL_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("HEEL_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_HEEL_SEARCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("HEEL_SEARCH_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_UPWARD_CURVATURE_SEARCH_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("UPWARD_CURVATURE_SEARCH_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_HEEL_PEAK_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("HEEL_PEAK_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_CENTER_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("CENTER_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_TOE_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("TOE_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_TOE_PEAK_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("TOE_PEAK_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_SOLDER_THICKNESS_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SOLDER_THICKNESS_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_PROFILE_ACROSS_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PROFILE_ACROSS_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_PROFILE_HEEL_ACROSS_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("HEEL_ACROSS_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_PROFILE_CENTER_ACROSS_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("CENTER_ACROSS_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_ACROSS_LEADING_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("LEADING_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum QFN_ACROSS_TRAILING_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("TRAILING_EDGE_REGION_KEY", null));
  public static final MeasurementRegionEnum EXPOSED_PAD_BACKGROUND_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("BACKGROUND_REGION_KEY", null));
  public static final MeasurementRegionEnum EXPOSED_PAD_PAD_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("PAD_REGION_KEY", null));
  public static final MeasurementRegionEnum EXPOSED_PAD_GAP_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("GAP_REGION_KEY", null));
  public static final MeasurementRegionEnum EXPOSED_PAD_ALONG_INNER_GAP_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("ALONG_INNER_GAP_REGION_KEY", null));
  public static final MeasurementRegionEnum EXPOSED_PAD_ALONG_OUTER_GAP_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("ALONG_OUTER_GAP_REGION_KEY", null));
  public static final MeasurementRegionEnum ECCENTRICITY_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("ECCENTRICITY_REGION_KEY", null));
  // In PTH Measurment locator algo, this PIN_REGION is used together LOCATE_SEARCH_REGION. But both have too similar color, orange.
  // This PIN_REGION2 is created just to have different color than PIN_REGION, which is blue.
  public static final MeasurementRegionEnum PIN_REGION2 = new MeasurementRegionEnum(_index++, new LocalizedString("PIN_REGION2_KEY", null));
  public static final MeasurementRegionEnum MEASURED_FILLET_THICKNESS_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("MEASURED_FILLET_THICKNESS_REGION_KEY", null));
  public static final MeasurementRegionEnum GRAYLEVEL_HISTOGRAM_PROFILE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("GRAYLEVEL_HISTOGRAM_PROFILE_REGION_KEY", null));
  public static final MeasurementRegionEnum FIRST_DERIVATIVES_PROFILE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("FIRST_DERIVATIVES_PROFILE_REGION_KEY", null));
  public static final MeasurementRegionEnum SECOND_DERIVATIVES_PROFILE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SECOND_DERIVATIVES_PROFILE_REGION_KEY", null));
  public static final MeasurementRegionEnum SLUG_EDGE_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SLUG_EDGE_REGION_KEY", null));
  //To Display Broken Pin learn region
  public static final MeasurementRegionEnum LEARN_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("LEARN_REGION_KEY", null));

    
  //Kee Chin Seong - For RF Connector
  public static final MeasurementRegionEnum VOID_PROFILE_START_BIN_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("SHORT_PROFILE_START_BIN_REGION_KEY", null));
  public static final MeasurementRegionEnum VOID_BACKGROUND_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("VOID_BACKGROUND_REGION_KEY", null));
  public static final MeasurementRegionEnum DEFINITE_VOID_DEFECT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("DEFINITE_VOID_DEFECT_REGION_KEY", null));
  public static final MeasurementRegionEnum QUESTIONABLE_VOID_DEFECT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("QUESTIONABLE_VOID_DEFECT_REGION_KEY", null));
  public static final MeasurementRegionEnum EXONERATED_VOID_DEFECT_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("EXONERATED_VOID_DEFECT_REGION_KEY", null));
  
  //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
  public static final MeasurementRegionEnum EXCESS_SOLDER_REGION = new MeasurementRegionEnum(_index++, new LocalizedString("EXCESS_SOLDER_REGION_KEY", null));

  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  private MeasurementRegionEnum(int id, LocalizedString regionDescription)
  {
    super(id);
    _regionDescription = regionDescription;
    _setOfAllEnums.add(this);
  }

  /**
   * @author Matt Wharton
   */
  public LocalizedString getRegionDescription()
  {
    Assert.expect(_regionDescription != null);

    return _regionDescription;
  }

  /**
   * @author Patrick Lacz
   */
  public static SortedSet<MeasurementRegionEnum> getSetOfAllEnums()
  {
    Assert.expect(_setOfAllEnums != null);
    return _setOfAllEnums;
  }
}
