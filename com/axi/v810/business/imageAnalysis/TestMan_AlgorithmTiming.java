package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.util.XrayTesterException;

/**
 * @author Matt Wharton
 */
public class TestMan_AlgorithmTiming extends UnitTest
{
  private static final ImageManager _imageManager = ImageManager.getInstance();

  private static final String _TEST_PROJECT_NAME = "CYGNUS_ASAP_83";
  private static final String _TEST_IMAGE_SET_NAME = "2006-09-27_14-58-38-219";

  /**
   * @author Matt Wharton
   */
  private TestMan_AlgorithmTiming()
  {
  }

  /**
   * Main test method
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      // Create an ImageAnalysis instance.
      ImageAnalysis imageAnalysis = new ImageAnalysis();

      // Load the project.
      BooleanRef abortedDuringLoad = new BooleanRef();
      Project project = Project.load(_TEST_PROJECT_NAME, abortedDuringLoad);
      Assert.expect(abortedDuringLoad.getValue() == false);

      // Load in the image set data.
      ImageSetData imageSetDataToUse = null;
      List<ImageSetData> imageSetDataList = _imageManager.getCompatibleImageSetData(project);
      for (ImageSetData candidateImageSetData : imageSetDataList)
      {
        if (candidateImageSetData.getImageSetName().equals(_TEST_IMAGE_SET_NAME))
        {
          imageSetDataToUse = candidateImageSetData;
          break;
        }
      }

      PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
      panelInspectionSettings.clearAllInspectionSettings();
      panelInspectionSettings.setInspectedSubtypes(project.getPanel());
      InspectionEngine.getInstance().setPanelInspectionSettings(panelInspectionSettings);

      TestProgram testProgram = project.getTestProgram();
      testProgram.clearFilters();

      for (ReconstructionRegion reconstructionRegion : testProgram.getFilteredInspectionRegions())
      {
        // Load in the images.
        ReconstructedImages reconstructedImages = _imageManager.loadInspectionImages(imageSetDataToUse,
                                                                                     reconstructionRegion);

        // Call the applicable algorithms for the joints in the region.
        imageAnalysis.classifyJoints(reconstructedImages);
      }

      ImageAnalysis.printTimingStats(true);
    }
    catch (XrayTesterException xtex)
    {
      xtex.printStackTrace();
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new TestMan_AlgorithmTiming());
  }
}
