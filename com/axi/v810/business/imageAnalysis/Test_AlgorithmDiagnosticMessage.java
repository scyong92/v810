package com.axi.v810.business.imageAnalysis;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * Test class for ImageDiagnosticMessage.
 *
 * @author Matt Wharton
 */
public class Test_AlgorithmDiagnosticMessage extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public Test_AlgorithmDiagnosticMessage()
  {
    // Do nothing.
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   * @author Matt Wharton
   */
  public void test(BufferedReader in, PrintWriter out)
  {
  }

  /**
   * Main entry point for the test fixture.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AlgorithmDiagnosticMessage());
  }
}
