package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;

/**
 * Comparator for Algorithms to see which should come first in an inspection.
 * Basically, we ensure that the Measurement algorithm is first.  We also want to run GridArrayVoiding
 * before running GridArrayOpen.  Other algorithms are sorted by their internal Enum id.
 *
 * @author Peter Esbensen
 */
public class AlgorithmInspectionOrderComparator implements Comparator<Algorithm>
{
  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  public int compare(Algorithm algorithm1, Algorithm algorithm2)
  {
    Assert.expect(algorithm1 != null);
    Assert.expect(algorithm2 != null);

    AlgorithmEnum algorithm1Enum = algorithm1.getAlgorithmEnum();
    AlgorithmEnum algorithm2Enum = algorithm2.getAlgorithmEnum();

    return AlgorithmEnumInspectionOrderComparator.compareAlgorithmEnums(algorithm1Enum, algorithm2Enum);
  }
}
