package com.axi.v810.business.imageAnalysis.ovalThroughHole;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.throughHole.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Siew Yeng
 */
public class OvalThroughHoleOpenAlgorithm extends ThroughHoleOpenAlgorithm
{
  /**
   * @author Siew Yeng
   */
  public OvalThroughHoleOpenAlgorithm(InspectionFamily inspectionFamily)
  {
    super(InspectionFamilyEnum.OVAL_THROUGHHOLE);
    Assert.expect(inspectionFamily != null);

    addAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }
  
  /**
   * @author Siew Yeng
   */
  private void addAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    AlgorithmSetting openDifferenceFromNominalProtrusion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_EXTRUSION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_EXTRUSION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_EXTRUSION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          openDifferenceFromNominalProtrusion,
                                                          SliceNameEnum.THROUGHHOLE_PROTRUSION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(openDifferenceFromNominalProtrusion); 

    AlgorithmSetting openDifferenceFromRegionProtrusion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_PROTRUSION,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_EXTRUSION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_EXTRUSION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_EXTRUSION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          openDifferenceFromRegionProtrusion,
                                                          SliceNameEnum.THROUGHHOLE_PROTRUSION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(openDifferenceFromRegionProtrusion); 

    AlgorithmSetting openDifferenceFromNominalInsertion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          openDifferenceFromNominalInsertion,
                                                          SliceNameEnum.THROUGHHOLE_INSERTION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(openDifferenceFromNominalInsertion); 

    AlgorithmSetting openDifferenceFromRegionInsertion = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_INSERTION,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_INSERTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_INSERTION)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(OPEN_DIFFERENCE_FROM_REGION_INSERTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          openDifferenceFromRegionInsertion,
                                                          SliceNameEnum.THROUGHHOLE_INSERTION,
                                                          MeasurementEnum.THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(openDifferenceFromRegionInsertion); 
  }
}
