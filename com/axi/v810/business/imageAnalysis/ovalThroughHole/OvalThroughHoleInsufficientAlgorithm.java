package com.axi.v810.business.imageAnalysis.ovalThroughHole;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.imageAnalysis.throughHole.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import ij.ImagePlus;

/**
 * @author Siew Yeng
 */
public class OvalThroughHoleInsufficientAlgorithm extends ThroughHoleInsufficientAlgorithm
{
  private static final String _WETTING_OPTIONS_TRUE = "True";
  private static final String _WETTING_OPTIONS_FALSE = "False";
  
    /**
   *For the detection of Void Volume in Joint.
   * Lim Lay Ngor PIPA
   */
  //START
  private static final String _VOID_VOLUME_MEASUREMENT_OPTIONS_TRUE = "True";
  private static final String _VOID_VOLUME_MEASUREMENT_OPTIONS_FALSE = "False";
  private static final String _VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_TRUE = "True";
  private static final String _VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN_OPTIONS_FALSE = "False";

  /**
   * @author Siew Yeng
   */
  public OvalThroughHoleInsufficientAlgorithm(InspectionFamily inspectionFamily)
  {
    super(InspectionFamilyEnum.OVAL_THROUGHHOLE);
    Assert.expect(inspectionFamily != null);

    addAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }
  
  /**
   * @author Siew Yeng
   */
  private void addAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    AlgorithmSetting insufficientDifferenceFromNominalPinside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          insufficientDifferenceFromNominalPinside,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(insufficientDifferenceFromNominalPinside);  

    //By Lim Lay Ngor PIPA
   AlgorithmSetting insufficientDifferenceFromNominalComponentside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE,
        displayOrder++,
        15.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          insufficientDifferenceFromNominalComponentside,
                                                          SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL);
    addAlgorithmSetting(insufficientDifferenceFromNominalComponentside);  
    
    AlgorithmSetting insufficientDifferenceFromRegionPinside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          insufficientDifferenceFromRegionPinside,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(insufficientDifferenceFromRegionPinside);

     AlgorithmSetting insufficientDifferenceFromRegionComponentside = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE,
        displayOrder++,
        255.0f, // default value (disabled)
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          insufficientDifferenceFromRegionComponentside,
                                                          SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION);
    addAlgorithmSetting(insufficientDifferenceFromRegionComponentside);

    List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>(Arrays.asList(JointTypeEnum.OVAL_THROUGH_HOLE));
    
    displayOrder = createBarel1SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel2SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel3SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel4SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel5SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel6SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel7SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel8SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel9SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarel10SlicesInsufficientSettings(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
  }
}
