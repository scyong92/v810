package com.axi.v810.business.imageAnalysis.ovalThroughHole;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for oval-shaped through hole joints
 *
 * @author Siew Yeng
 */
public class OvalThroughHoleInspectionFamily extends InspectionFamily
{
  /**
   * @author Siew Yeng
   */
  public OvalThroughHoleInspectionFamily()
  {
    super(InspectionFamilyEnum.OVAL_THROUGHHOLE);

    Algorithm ovalThroughHoleMeasurementAlgorithm = new OvalThroughHoleMeasurementAlgorithm(this);
    addAlgorithm(ovalThroughHoleMeasurementAlgorithm);

    Algorithm ovalThroughHoleOpenAlgorithm = new OvalThroughHoleOpenAlgorithm(this);
    addAlgorithm(ovalThroughHoleOpenAlgorithm);

    Algorithm ovalThroughHoleInsufficentAlgorithm = new OvalThroughHoleInsufficientAlgorithm(this);
    addAlgorithm(ovalThroughHoleInsufficentAlgorithm);

    Algorithm ovalThroughHoleShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.OVAL_THROUGHHOLE);
    addAlgorithm(ovalThroughHoleShortAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Collection<SliceNameEnum> inspectedSlices = new LinkedList<SliceNameEnum>();

    inspectedSlices.addAll(OvalThroughHoleMeasurementAlgorithm.getAllBarrelSliceEnumList(subtype));    
    inspectedSlices.add(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
    inspectedSlices.add(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);

    // even though the user might not inspect Protrusion for the pin, we need to include it for Short algorithm (learning will always try
    // to learn all slices)
    inspectedSlices.add(SliceNameEnum.THROUGHHOLE_PROTRUSION);

    // The insertion slice truly is optional depending on the user setting
    if (OvalThroughHoleMeasurementAlgorithm.hasInsertionSlice(subtype))
    {
      inspectedSlices.add(SliceNameEnum.THROUGHHOLE_INSERTION);
    }
        
    // Jack Hwee - broken pins
    if (OvalThroughHoleMeasurementAlgorithm.hasProjectionSliceForShort(subtype))
      inspectedSlices.add(SliceNameEnum.CAMERA_0);

    return inspectedSlices;

  }

  /**
   * Returns the applicable SliceNameEnum for a throughhole's pad slice.
   * Try to choose the best slice for locator to run on. This is the barrel slice.
   *
   * @author Patrick Lacz
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return SliceNameEnum.THROUGHHOLE_BARREL;
  }

  /**
   * Returns the applicable SliceNameEnum for a throughhole's top and bottom slices.
   * These are the slices that short should be ran on.
   *
   * @author Patrick Lacz
   */
  public Collection<SliceNameEnum> getShortInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);
    //Broken Pin
    if (OvalThroughHoleMeasurementAlgorithm.hasProjectionSliceForShort(subtype))
    {
      Collection<SliceNameEnum> inspectedSlices = new LinkedList<SliceNameEnum>();
      inspectedSlices.add(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
      inspectedSlices.add(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
      inspectedSlices.add(SliceNameEnum.THROUGHHOLE_PROTRUSION);     
      inspectedSlices.add(SliceNameEnum.CAMERA_0);
      return inspectedSlices;
    }
    else
      return Arrays.asList(SliceNameEnum.THROUGHHOLE_PIN_SIDE, SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE, SliceNameEnum.THROUGHHOLE_PROTRUSION);
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }

}
