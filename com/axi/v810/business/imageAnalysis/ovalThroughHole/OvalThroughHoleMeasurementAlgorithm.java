package com.axi.v810.business.imageAnalysis.ovalThroughHole;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.imageAnalysis.throughHole.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * The Throughhole (PTH) measurement algorithm is relatively straight forward. We want to figure out how much
 * solder is in the barrel at a couple of slices. One slice is the Pin-Side slice (opposite the component), and
 * the other is selected by the user to be somewhere in the barrel or at the top (the Component-side slice).
 *
 * A 4th "protrusion" slice was added below the Pin Side Slice to determine if a pin was really in the hole (the
 * end of the pin protrudes). Solder in the barrel was not a perfect indicator of the pin being there also.  For similar
 * reasoning, we also added an Insertion slice above the board.  Both of these slices are optional and not active by
 * default.
 *
 * <pre>
 *                             SliceNameEnum                 FocusInstruction
 *                                                (ZOffsets have sign flipped based on board side)
 *  _____________________
 * |      Component      |
 * |_____________________|
 *    |    |    |    |      THROUGHHOLE_INSERTION       PERCENT_BETWEEN_EDGES, ZOffset(boardThickness + userDefinedOffsetInNanos)
 *  __|____|____|____|___   THROUGHHOLE_COMPONENT_SIDE  PERCENT_BETWEEN_EDGES, ZOffset(userDefinedBarrelFraction*boardThickness)
 * |  |    |    |    |   |
 * |  |    |    |    |   |  THROUGHHOLE_BARREL          PERCENT_BETWEEN_EDGES, ZOffset(userDefinedBarrelFraction*boardThickness)
 * |  |    |Board    |   |
 * |  |    |    |    |   |
 * |  |    |    |    |   |
 * |__|____|____|____|___|  THROUGHHOLE_PIN_SIDE        PERCENT_BETWEEN_EDGES, ZOffset(userDefinedBarrelFraction*boardThickness)
 *    |    |    |    |
 *    |    |    |    |      THROUGHHOLE_PROTRUSION      PERCENT_BETWEEN_EDGES, ZOffset(userDefinedOffsetInNanos)
 *
 * </pre>
 *
 * The focus code is elsewhere, but as of this writing, the position of the barrel slice is dependent on the board
 * thickness as defined by the project.  The position of the slice is relative to the auto-focused pin-side-slice.
 *
 * We run locator on the barrel slice - that's where the signal is generally the strongest - even if the barrel isn't filled.  Locator
 * automatically uses the barrel size rather than the pad dimensions to perform this step.
 * Notice - if the barrel is really empty (no solder at all), locator will probably find the wrong location. The measurement should
 * still be substantially lower than the nominal, however.
 *
 * On the two inspected slices, we take the MEAN of the circular barrel region, and the MEDIAN of the surrounding background region.
 * The MEDIAN was chosen to avoid shading artifacts that are common in through-hole components.
 *
 * The corrected graylevel is obtained, and then reversed to match the intuition of our users (low=bad). Kathy Adelson reviewed some
 * of the alternative measurements and decided she liked the range and orientation of this one the most. This also allows us to use the
 * same "% of nominal" defect thresholds for the insufficient algorithm.
 *
 * A component side slice is always taken in addition to the barrel slice. The Short algorithm has the option of executing on this slice.
 *
 * @author Patrick Lacz
 */
public class OvalThroughHoleMeasurementAlgorithm extends ThroughHoleMeasurementAlgorithm
{
  private final static String _NO_PIN_DETECTION_SLICES = "None";
  private final static String _PROTRUSION_ONLY = "Protrusion Only";
  private final static String _INSERTION_ONLY = "Insertion Only";
  private final static String _PROTRUSION_AND_INSERTION = "Both Protrusion and Insertion";
  private final static String _THROUGH_HOLE_IN_MILS = "Thickness";
  private final static String _THROUGH_HOLE_IN_PERCENTAGE = "Percentage";
  private final static String _PROJECTION_SLICE_ONLY = "2.5D Only"; //Broken Pin
  private final static String _PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES = "Pin, Component, 2.5D";//Broken Pin
  
  private final static ArrayList<String> _PIN_DETECTION_CHOICES = new ArrayList(Arrays.asList( _NO_PIN_DETECTION_SLICES, _PROTRUSION_ONLY, _INSERTION_ONLY, _PROTRUSION_AND_INSERTION));
  private final static int _BORDER_PIXELS=2;

  private final static String _ON = "On";
  private final static String _OFF = "Off";
  private final static String _AUTO = "Auto";

  private final static int _MIN_NUMBER_OF_BARREL_SLICE = 1;
  private final static int _MAX_NUMBER_OF_BARREL_SLICE = 10;

  private final static ArrayList<String> _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE  = new ArrayList();
  private final static ArrayList<String> _THROUGH_HOLE_WORKING_UNIT = new ArrayList(Arrays.asList(_THROUGH_HOLE_IN_PERCENTAGE, _THROUGH_HOLE_IN_MILS));
  
  //private final static String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";
  
  protected final static String _PIN_SIZE_DIAMETER = "Diameter";
  protected final static String _PIN_SIZE_CUSTOM = "Custom";
  
  
  static
  {
    for(int i = _MIN_NUMBER_OF_BARREL_SLICE ; i<=_MAX_NUMBER_OF_BARREL_SLICE; i ++)
      _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE.add(Integer.toString(i));
  }
  
  /**
   * @author Siew Yeng
   */
  public OvalThroughHoleMeasurementAlgorithm(InspectionFamily inspectionFamily)
  {
    super(InspectionFamilyEnum.OVAL_THROUGHHOLE);
    Assert.expect(inspectionFamily != null);

    addAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }
  
  /**
   * @author Siew Yeng
   */
  private void addAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    // Add the shared locator settings.
    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    AlgorithmSetting sliceWorkingUnit = new AlgorithmSetting(
        AlgorithmSettingEnum.SLICEHEIGHT_WORKING_UNIT, // setting enum
        displayOrder++, // display order,
        _THROUGH_HOLE_IN_PERCENTAGE,
        _THROUGH_HOLE_WORKING_UNIT,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(SLICEHEIGHT_WORKING_UNIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(SLICEHEIGHT_WORKING_UNIT)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(SLICEHEIGHT_WORKING_UNIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(sliceWorkingUnit);

    AlgorithmSetting userDefineNumOfSlice = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE, // setting enum
        displayOrder++, // display order,
        new Integer(_MIN_NUMBER_OF_BARREL_SLICE).toString(),
        _THROUGH_HOLE_BARREL_NUMBER_OF_SLICE,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(USER_DEFINED_NUMBER_OF_SLICE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(USER_DEFINED_NUMBER_OF_SLICE)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(USER_DEFINED_NUMBER_OF_SLICE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(userDefineNumOfSlice);

    AlgorithmSetting nominalPinsideSolderSignal = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL,
      displayOrder++,
      230.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(NOMINAL_PINSIDE_SOLDER_SIGNAL)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_PINSIDE_SOLDER_SIGNAL)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(NOMINAL_PINSIDE_SOLDER_SIGNAL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          nominalPinsideSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_PIN_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalPinsideSolderSignal);
    
    //By Lim, Lay Ngor PIPA
    AlgorithmSetting nominalComponentsideSolderSignal = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL,
      displayOrder++,
      230.0f, // default value
      0.0f, // minimum value
      255.0f, // maximum value
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          nominalComponentsideSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalComponentsideSolderSignal);
    
    List<JointTypeEnum> jointTypeEnums = new ArrayList<JointTypeEnum>(Arrays.asList(JointTypeEnum.OVAL_THROUGH_HOLE));
    
    displayOrder = createBarrel1SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel2SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel3SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel4SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel5SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel6SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel7SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel8SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel9SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    displayOrder = createBarrel10SlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);
    //Broken Pin
    displayOrder = createProjectionSlicesSetting(displayOrder, currentVersion, inspectionFamily, jointTypeEnums);

    /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting pinSideSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      0.0f, // default value
      -50.0f, // minimum value
      50.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(pinSideSliceheight);

        /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting pinSideSliceheightinThickness = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-25.0f), // minimum value
      MathUtil.convertMilsToMillimeters(25.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(THROUGHHOLE_PIN_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(pinSideSliceheightinThickness);

    AlgorithmSetting pinDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_PIN_DETECTION, // setting enum
        displayOrder++, // display order,
        _NO_PIN_DETECTION_SLICES,
        _PIN_DETECTION_CHOICES,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // description URL Key
        "HTML_DETAILED_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // detailed description URL Key
        "IMG_DESC_THROUGHHOLE_(PIN_DETECTION)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinDetection);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting insertionSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_INSERTION_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(20.0f), // default value
      MathUtil.convertMilsToMillimeters(-20.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(INSERTION_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(INSERTION_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(INSERTION_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(insertionSliceheight);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting protrusionSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PROTRUSION_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(20.0f), // default value
      MathUtil.convertMilsToMillimeters(-10.0f), // minimum value
      MathUtil.convertMilsToMillimeters(150.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(THROUGHHOLE_EXTRUSION_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(THROUGHHOLE_EXTRUSION_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(THROUGHHOLE_EXTRUSION_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(protrusionSliceheight);

    /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting componentSideShortSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT, // setting enum
//      displayOrder++, // display order,
      2000, // LC: this display order number is a hack so that the Slice Setup tab in Fine Tuning displays the settings in the order desired by marketing
      100.0f, // default value
      80.0f, // minimum value
      120.0f, // maximum value
      MeasurementUnitsEnum.PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM,
      "HTML_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(componentSideShortSliceheight);

     /** @todo PE figure out default value */
    /** @todo PE review min and max */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting componentSideShortSliceheightthickness = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS, // setting enum
//      displayOrder++, // display order,
      2001, // LC: this display order number is a hack so that the Slice Setup tab in Fine Tuning displays the settings in the order desired by marketing
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-25.0f), // minimum value
      MathUtil.convertMilsToMillimeters(25.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(COMPONENT_SIDE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(componentSideShortSliceheightthickness);

    // Added by Lee Herng (4 Mar 2011)
    AlgorithmSetting autoFocusMidBoardOffset = new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
      2002, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_THROUGHHOLE_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(autoFocusMidBoardOffset);
    
    addAlgorithmSetting(new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ALONG,
            displayOrder++,
            10.f, // default value
            0.5f,  // minimum value
            500.f, // maximum value
            MeasurementUnitsEnum.MILS,
            "HTML_DESC_OVAL_THROUGHHOLE_(PIN_SIZE_ALONG)_KEY", // description URL key
            "HTML_DETAILED_DESC_OVAL_THROUGHHOLE_(PIN_SIZE_ALONG)_KEY", // desailed description URL key
            "IMG_DESC_OVAL_THROUGHHOLE_(PIN_SIZE_ALONG)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.ADDITIONAL,
            currentVersion));
    
    addAlgorithmSetting(new AlgorithmSetting(
            AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ACROSS,
            displayOrder++,
            10.f, // default value
            0.5f,  // minimum value
            500.f, // maximum value
            MeasurementUnitsEnum.MILS,
            "HTML_DESC_OVAL_THROUGHHOLE_(PIN_SIZE_ACROSS)_KEY", // description URL key
            "HTML_DETAILED_DESC_OVAL_THROUGHHOLE_(PIN_SIZE_ACROSS)_KEY", // desailed description URL key
            "IMG_DESC_OVAL_THROUGHHOLE_(PIN_SIZE_ACROSS)_KEY", // image description URL key
            AlgorithmSettingTypeEnum.ADDITIONAL,
            currentVersion));

    addAlgorithmSetting(new AlgorithmSetting(
          AlgorithmSettingEnum.THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL,
          displayOrder++,
          120.0f, // default value
          100.0f, // minimum value
          180.0f, // maximum value
          MeasurementUnitsEnum.PERCENT,
          "HTML_DESC_THROUGHHOLE_(PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL)_KEY", // description URL key
          "HTML_DETAILED_DESC_THROUGHHOLE_(PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL)_KEY", // desailed description URL key
          "IMG_DESC_THROUGHHOLE_(PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL)_KEY", // image description URL key
          AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion));

    AlgorithmSetting nominalProtrusionSolderSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL,
        displayOrder++,
        230.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(NOMINAL_EXTRUSION_SOLDER_SIGNAL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_EXTRUSION_SOLDER_SIGNAL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(NOMINAL_EXTRUSION_SOLDER_SIGNAL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL, /** @todo PE see if people mind me moving this to Additional */
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          nominalProtrusionSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_PROTRUSION,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalProtrusionSolderSignal);

    AlgorithmSetting nominalInsertionSolderSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL,
        displayOrder++,
        230.0f, // default value
        0.0f, // minimum value
        255.0f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_THROUGHHOLE_(NOMINAL_INSERTION_SOLDER_SIGNAL)_KEY", // description URL key
        "HTML_DETAILED_DESC_THROUGHHOLE_(NOMINAL_INSERTION_SOLDER_SIGNAL)_KEY", // desailed description URL key
        "IMG_DESC_THROUGHHOLE_(NOMINAL_INSERTION_SOLDER_SIGNAL)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL, /** @todo PE see if people mind me moving this to Additional */
        currentVersion);
    inspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.OVAL_THROUGH_HOLE,
                                                          nominalInsertionSolderSignal,
                                                          SliceNameEnum.THROUGHHOLE_INSERTION,
                                                          MeasurementEnum.THROUGHHOLE_SOLDER_SIGNAL);
    addAlgorithmSetting(nominalInsertionSolderSignal);

    AlgorithmSetting percentDiameterToTest = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST,
      displayOrder++,
      100.0f, // default value
      50.0f, // minimum value
      150.0f, // maximum value
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_THROUGHHOLE_(PERCENT_OF_DIAMETER_TO_TEST)_KEY", // description URL key
      "HTML_DETAILED_DESC_THROUGHHOLE_(PERCENT_OF_DIAMETER_TO_TEST)_KEY", // desailed description URL key
      "IMG_DESC_THROUGHHOLE_(PERCENT_OF_DIAMETER_TO_TEST)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(percentDiameterToTest);

    AlgorithmSetting backgroundRegionInnerEdgeLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION, // setting enum
      displayOrder++,
      40.f,
      0f,
      100f,
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_THROUGHHOLE_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BACKGROUND_REGION_INNER_EDGE_LOCATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionInnerEdgeLocation);

    AlgorithmSetting backgroundRegionOuterEdgeLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION, // setting enum
      displayOrder++,
      85.f,
      0f,
      100f,
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_THROUGHHOLE_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(BACKGROUND_REGION_OUTER_EDGE_LOCATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionOuterEdgeLocation);

    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));
    
    final ArrayList<String> onOrOffSettings = new ArrayList<String>(Arrays.asList(_OFF, _ON, _AUTO));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    AlgorithmSetting userDefinedWaveletLevel = new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      2009, // this is done to make sure this threshold is displayed right before psh in the Slice Setup tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(userDefinedWaveletLevel);
    
    AlgorithmSetting predictiveSliceheight = new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2012, // this is done to make sure this threshold is displayed last in the Slice Setup tab - CR31430
      _ON,
      onOrOffSettings,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_THROUGHHOLE_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_THROUGHHOLE_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_THROUGHHOLE_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion);
      addAlgorithmSetting(predictiveSliceheight);

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);
    
    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size(); 
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author Patrick Lacz
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.size() > 0);

    Subtype subtypeOfJoints = jointInspectionDataObjects.get(0).getSubtype();

    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // set up the list of slices to be inspected
    SliceNameEnum sliceNameEnumArray[] = getSliceNameEnumArray(subtypeOfJoints);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtypeOfJoints.getAlgorithmSettingValue(
        AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtypeOfJoints.getAlgorithmSettingValue(
      AlgorithmSettingEnum.THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION) / 100.f;
    final float barrelRegionFractionOfDiameter = (Float)subtypeOfJoints.getAlgorithmSettingValue(
      AlgorithmSettingEnum.THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST) / 100.f;
//    float expectedSizeInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_WIDTH);
//    final float expectedSizeInMM=  MathUtil.convertMilsToMillimeters(expectedSizeInMils);
    final float pinSearchDiameter = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL) / 100.f;
    Image pinTemplateImage=null;
    IntegerRef expectedPinWidthInPixels = new IntegerRef(0);
    IntegerRef expectedPinLengthInPixels = new IntegerRef(0);
    
    SliceNameEnum locatorSlice = subtypeOfJoints.getInspectionFamily().getDefaultPadSliceNameEnum(subtypeOfJoints);
    Locator.locateJoints(reconstructedImages, locatorSlice, jointInspectionDataObjects, this, true);

    if (regionInnerEdgeAsFractionOfInterPadDistance >= regionOuterEdgeAsFractionOfInterPadDistance)
    {
      // Raise a warning to the user that we can't run under these circumstances.
      LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
        "ALGDIAG_BACKGROUND_OUTER_EDGE_LOCATION_LESS_THEN_INNER_EDGE_LOCATION_WARNING_KEY",
        new Object[] { subtypeOfJoints.getLongName() });
      AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);

      return;
    }
    for (SliceNameEnum sliceNameEnum : sliceNameEnumArray)
    {
      ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtypeOfJoints, this);

      //Siew Yeng - XCR-2683 - add enhanced image
      if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtypeOfJoints))
      {
        ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, sliceNameEnum);
      }
      /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      // measure using pin or barrel, depends on slice
      if ( sliceNameEnum == sliceNameEnum.THROUGHHOLE_PROTRUSION
                                           || sliceNameEnum==sliceNameEnum.THROUGHHOLE_INSERTION )
      {
        // executed only once within a clasiifyJoints call
        if (pinTemplateImage==null)
        {
          // if pin is too big then use default gray level
          RegionOfInterest barrelRegion = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionDataObjects.get(0));
          
          //oval shaped pin
          float expectedSizeAlongInMM = 0.f;
          float expectedSizeAcrossInMM =0.f;

          float expectedSizeAlongInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ALONG);
          float expectedSizeAcrossInMils = (Float)subtypeOfJoints.getAlgorithmSettingValue(AlgorithmSettingEnum.THROUGHHOLE_PIN_SIZE_ACROSS);

          expectedSizeAlongInMM =  MathUtil.convertMilsToMillimeters(expectedSizeAlongInMils);
          expectedSizeAcrossInMM =  MathUtil.convertMilsToMillimeters(expectedSizeAcrossInMils);

          if(barrelRegion.getOrientationInDegrees() == 90 || barrelRegion.getOrientationInDegrees() == 270)
          {
            pinTemplateImage = AlgorithmUtil.createOvalShapedPinTemplateImage(expectedSizeAcrossInMM, expectedSizeAlongInMM, 
                                                                          expectedPinWidthInPixels, expectedPinLengthInPixels, 
                                                                          100.f, 200.f, _BORDER_PIXELS, subtypeOfJoints);
          }
          else
          {
            pinTemplateImage = AlgorithmUtil.createOvalShapedPinTemplateImage(expectedSizeAlongInMM, expectedSizeAcrossInMM, 
                                                                              expectedPinWidthInPixels, expectedPinLengthInPixels, 
                                                                              100.f, 200.f, _BORDER_PIXELS, subtypeOfJoints);
          }
          
          
          // pin diameter logically must be smaller than barrel diameter, issue warning message!
          if ((barrelRegion.getWidth() < expectedPinWidthInPixels.getValue())
                 &&(barrelRegion.getHeight() < expectedPinWidthInPixels.getValue()))
          {
             LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
                 "ALGDIAG_PTH_PIN_DIAMETER_INVALID_KEY",
                  new Object[] { subtypeOfJoints.getLongName() });
                  AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);
          }
        }
      }
      
      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                 jointInspectionData,
                                                 reconstructedImages.getReconstructionRegion(),
                                                 slice,
                                                 false);
        
        JointMeasurement measureReversedGraylevelOfBarrelRegion=null;
        if (  sliceNameEnum == sliceNameEnum.THROUGHHOLE_PROTRUSION
           || sliceNameEnum == sliceNameEnum.THROUGHHOLE_INSERTION )
        {
          measureReversedGraylevelOfBarrelRegion = measureReversedGraylevelOfPinRegion(jointInspectionData,
             reconstructedImages,
             sliceNameEnum,
             pinTemplateImage,
             expectedPinWidthInPixels.getValue(),
             expectedPinLengthInPixels.getValue(),
             _BORDER_PIXELS,
             pinSearchDiameter,
             regionInnerEdgeAsFractionOfInterPadDistance,
             regionOuterEdgeAsFractionOfInterPadDistance);
        }
        else
        {
          // Measure the graylevel of the joint
          measureReversedGraylevelOfBarrelRegion = measureReversedGraylevelOfBarrelRegion(
              jointInspectionData,
              reconstructedImages,
              sliceNameEnum,
              barrelRegionFractionOfDiameter,
              regionInnerEdgeAsFractionOfInterPadDistance,
              regionOuterEdgeAsFractionOfInterPadDistance);

           basicThroughHoleClassifyJoint(reconstructedImages, jointInspectionData, sliceNameEnum);
        }
        JointMeasurement correctedGraylevelMeasurement = measureReversedGraylevelOfBarrelRegion;
        // record the measurement data
        JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
        jointInspectionResult.addMeasurement(correctedGraylevelMeasurement);
      }
    } // end slice enum

    if (pinTemplateImage!=null)
      pinTemplateImage.decrementReferenceCount();
  }
}
