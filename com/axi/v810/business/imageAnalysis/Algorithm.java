package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * An Algorithm is a part of the classification process.  Most Algorithms are
 * designed to detect a certain defect type, like an open or a short.  Some
 * Algorithms are purely for extracting measurements from images.
 *
 * @author Peter Esbensen
 */
public abstract class Algorithm
{
  protected AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();
  private AlgorithmEnum _algorithmEnum;
  private InspectionFamilyEnum _inspectionFamilyEnum;
  private int _version = 0;
  private Collection<AlgorithmSetting> _algorithmSettings = new TreeSet<AlgorithmSetting>();
  private Map<AlgorithmSettingEnum,
              AlgorithmSetting> _settingEnumToSettingMap = new HashMap<AlgorithmSettingEnum, AlgorithmSetting>();
  protected Collection<MeasurementEnum> _jointMeasurementEnums = new TreeSet<MeasurementEnum>();
  protected Collection<MeasurementEnum> _componentMeasurementEnums = new TreeSet<MeasurementEnum>();
  protected Collection<AlgorithmSettingEnum> _learnedAlgorithmSettingEnums = new TreeSet<AlgorithmSettingEnum>();

  /**
   * Get a list of all the joint-based MeasurementEnums that can be produced
   * by this Algorithm.
   *
   * @author Peter Esbensen
   */
  public Collection<MeasurementEnum> getJointMeasurementEnums()
  {
    Assert.expect(_jointMeasurementEnums != null);

    return _jointMeasurementEnums;
  }

  /**
   * Get a list of all the component-based MeasurementEnums that can be produced
   * by this Algorithm.
   *
   * @author Peter Esbensen
   */
  public Collection<MeasurementEnum> getComponentMeasurementEnums()
  {
    Assert.expect(_componentMeasurementEnums != null);

    return _componentMeasurementEnums;
  }

  /**
   * @author Matt Wharton
   */
  public Collection<AlgorithmSettingEnum> getLearnedAlgorithmSettingEnums()
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    return _learnedAlgorithmSettingEnums;
  }

  /**
   * @author Sunit Bhalla
   * This method sets all legal algorithms to "enabled" when a subtype is created.  The method can be overloaded
   * if a specific algorithm should not be "enabled" by default.
   */
  public boolean algorithmIsEnabledByDefault(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return true;
  }

  /**
   * @author Peter Esbensen
   */
  protected Algorithm(AlgorithmEnum algorithmEnum, InspectionFamilyEnum inspectionFamilyEnum)
  {
    Assert.expect(algorithmEnum != null);
    Assert.expect(inspectionFamilyEnum != null);

    _algorithmEnum = algorithmEnum;
    _inspectionFamilyEnum = inspectionFamilyEnum;

    _version = 1;

  }

  /**
   * @author Peter Esbensen
   */
  public String getName()
  {
    return _algorithmEnum.getName();
  }

  /**
   * @author Bill Darbie
   */
  public InspectionFamilyEnum getInspectionFamilyEnum()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    return _inspectionFamilyEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public InspectionFamily getInspectionFamily()
  {
    Assert.expect(_inspectionFamilyEnum != null);
    return InspectionFamily.getInstance(_inspectionFamilyEnum);
  }

  /**
   * @author Peter Esbensen
   */
  public AlgorithmEnum getAlgorithmEnum()
  {
    Assert.expect(_algorithmEnum != null);
    return _algorithmEnum;
  }

  /**
   * @author Bill Darbie
   */
  public int getVersion()
  {
    Assert.expect(_version > 0);
    return _version;
  }

  /**
   * @author Peter Esbensen
   */
  public List<AlgorithmSetting> getAlgorithmSettings(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(_algorithmSettings != null);
    Assert.expect(jointTypeEnum != null);

    List<AlgorithmSetting> algorithmSettings = new ArrayList<AlgorithmSetting>();
    for (AlgorithmSetting algorithmSetting : _algorithmSettings)
    {
      if (algorithmSetting.filterContains(jointTypeEnum) == false)
        algorithmSettings.add(algorithmSetting);
    }
    return algorithmSettings;
  }
  
  /**
   * @author Wong Ngie Xing
   * XCR1792, Able to Copy PTH and PRESSFIT Barrel Slices Number
   */
  public List<AlgorithmSetting> getAlgorithmSettingsExcludingSliceHeight(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(_algorithmSettings != null);
    Assert.expect(jointTypeEnum != null);

    List<AlgorithmSetting> algorithmSettings = new ArrayList<AlgorithmSetting>();
    for (AlgorithmSetting algorithmSetting : _algorithmSettings)
    {
      if ((algorithmSetting.filterContains(jointTypeEnum) == false)
           && (algorithmSetting.getAlgorithmSettingTypeEnum(this.getVersion()) != AlgorithmSettingTypeEnum.SLICE_HEIGHT
                && algorithmSetting.getAlgorithmSettingTypeEnum(this.getVersion()) != AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL))
        algorithmSettings.add(algorithmSetting);
    }
    return algorithmSettings;
  }

  /**
   * @author Peter Esbensen
   */
  public AlgorithmSetting getAlgorithmSetting(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(_settingEnumToSettingMap != null);
    Assert.expect(algorithmSettingEnum != null);

    AlgorithmSetting algorithmSetting = _settingEnumToSettingMap.get(algorithmSettingEnum);
    Assert.expect(algorithmSetting != null);
    return algorithmSetting;
  }

  /**
   * @author Bill Darbie
   */
  public Collection<AlgorithmSettingEnum> getAlgorithmSettingEnums()
  {
    Collection<AlgorithmSettingEnum> algorithmSettingEnums = new TreeSet<AlgorithmSettingEnum>();
    for (AlgorithmSetting algorithmSetting : _algorithmSettings)
    {
      algorithmSettingEnums.add(algorithmSetting.getAlgorithmSettingEnum());
    }

    return algorithmSettingEnums;
  }


  /**
   * @author Peter Esbensen
   */
  public boolean doesAlgorithmSettingExist(AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    if (_settingEnumToSettingMap.containsKey(algorithmSettingEnum))
      return true;
    return false;
  }

  /**
   * @author Sunit Bhalla
   */
  public Serializable getSettingName(AlgorithmSettingEnum settingEnum,
                                     int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getName();
  }


  /**
   * @author Peter Esbensen
   */
  public Serializable getSettingDefaultValue(AlgorithmSettingEnum settingEnum,
                                             int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getDefaultValue(algorithmVersion);
  }

  /**
   * @author Andy Mechtenberg
   */
  public Serializable getSettingValuesList(AlgorithmSettingEnum settingEnum,
                                           int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getValuesList(algorithmVersion);
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getSettingMinimumValue(AlgorithmSettingEnum settingEnum,
                                             int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getMinimumValue(algorithmVersion);
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getSettingMaximumValue(AlgorithmSettingEnum settingEnum,
                                             int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getMaximumValue(algorithmVersion);
  }

  /**
   * @author Andy Mechtenberg
   */
  public String getSettingUnits(AlgorithmSettingEnum settingEnum, int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getUnits(algorithmVersion).getName();
  }

  /**
   * @author Patrick Lacz
   */
  public MeasurementUnitsEnum getSettingUnitsEnum(AlgorithmSettingEnum settingEnum, int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getUnits(algorithmVersion);
  }

  /**
   * @author Andy Mechtenberg
   */
  public URL getSettingDescriptionURL(AlgorithmSettingEnum settingEnum,
                                      int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getDescriptionURL(algorithmVersion);
  }

  /**
   * @author Andy Mechtenberg
   */
  public URL getSettingDetailedDescriptionURL(AlgorithmSettingEnum settingEnum,
                                              int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getDetailedDescriptionURL(algorithmVersion);
  }

  /**
   * @author Andy Mechtenberg
   */
  public URL getSettingImageURL(AlgorithmSettingEnum settingEnum,
                               int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));
    return _settingEnumToSettingMap.get(settingEnum).getImageURL(algorithmVersion);
  }

  /**
   * Gets the AlgorithmSettingTypeEnum for the specified AlgorithmSettingEnum and version number.
   *
   * @author Matt Wharton
   */
  public AlgorithmSettingTypeEnum getAlgorithmSettingTypeEnum(AlgorithmSettingEnum settingEnum,
                                                              int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(doesAlgorithmSettingExist(settingEnum));

    return _settingEnumToSettingMap.get(settingEnum).getAlgorithmSettingTypeEnum(algorithmVersion);
  }

  /**
   * @author Peter Esbensen
   */
  protected void addAlgorithmSetting(AlgorithmSetting newSetting)
  {
    Assert.expect(newSetting != null);
    _algorithmSettings.add(newSetting);
    _settingEnumToSettingMap.put(newSetting.getAlgorithmSettingEnum(), newSetting);
  }

  /**
   * Learns the algorithm settings for the specified subtype.
   *
   * @author Sunit Bhalla
   * @author Matt Wharton
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Do nothing...applicable algorithms will override this method as needed.
  }

  /**
   * Learns any joint specific data for all joints in the specified subtype.
   * A prime example of this is short background profiles.
   *
   * @author Matt Wharton
   */
  public void learnJointSpecificData(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Do nothing...applicable algorithms will override this method as needed.
  }
  
  /**
   * @param learnedSubtype
   * @param board
   * @param typicalBoardImages
   * @param unloadedBoardImages
   * @throws DatastoreException 
   * 
   * @author Kee Chin Seong
   */
  public void learnJointSpecificData(Subtype learnedSubtype, Board board,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(board != null);
    Assert.expect(learnedSubtype != null);
  }

  /**
   * Learns any expected image data for all joints in the specified subtype.
   * Returns true if any images were learned for the subtype.
   *
   * @author George Booth
   */
  public boolean learnExpectedImageData(Subtype subtype,
                                        ManagedOfflineImageSet typicalBoardImages,
                                        ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Do nothing...applicable algorithms will override this method as needed.
    return false;
  }
 
  /**
   * Clear Tombstone
   * @author Sheng-chuan.yong
   */
  public boolean learnImageTemplateData(Subtype subtype,Board board,
                                        ManagedOfflineImageSet typicalBoardImages,
                                        ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Do nothing...applicable algorithms will override this method as needed.
    return false;
  }
  
  /**
   * @author Matt Wharton
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Edited by Lee Herng 28 Nov 2014 - I temporarily commented out ImageProcessingAlgorithm.updateNominals(...).
    //                                   This is because if we have a very shaded image set, it will actually get the min/max
    //                                   gray-level from the shaded image and set the value in threshold. This will overwrite
    //                                   user-defined min/max gray-level value.
    // Do nothing...applicable algorithms will override this method.
    // Not until Seng Yew implement new feature as below.
    //ImageProcessingAlgorithm.updateNominals(subtype, typicalBoardImages, unloadedBoardImages, this);
//
//    // Added by Seng Yew on 22-Apr-2011
//    // Image normalization by subtype feature - only applicable if algorithm setttings "Update Graylevel", "Graylevel Minimum" & "Graylevel Maximum" are defined.
//
//    if (doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE) && 
//        doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM) &&
//        doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM))
//    {
//      if (typicalBoardImages.size() == 0)
//        return;
//
//      String graylevelUpdateOption = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE);
//      boolean updateGraylevel = graylevelUpdateOption.equalsIgnoreCase("True");
//
//      if (updateGraylevel)
//      {
//        float minV = 255.0f;
//        float maxV = 0.0f;
//        ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
//        ReconstructedImages reconstructedImages;
//        while ((reconstructedImages = imagesIterator.getNext()) != null)
//        {
//          for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
//          {
//            float minInImage = Statistics.minValue(reconstructedSlice.getImage());
//            float maxInImage = Statistics.maxValue(reconstructedSlice.getImage());
//            if (minV > minInImage)
//              minV = minInImage;
//            if (maxV < maxInImage)
//              maxV = maxInImage;
//          }
//          imagesIterator.finishedWithCurrentRegion();
//        }
//        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM, minV);
//        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM, maxV+1);
//      }
//    }
  }

  /**
   * Enable interactive learning
   *
   * @author George Booth
   */
  public void enableInteractiveLearning(boolean enabled)
  {
    // Do nothing...applicable algorithms will override this method as needed.
  }

  /**
   * For interactive learning, set a postive user input (defined by algorithm)
   * @author George Booth
   */
  public void setPostiveUserInput()
  {
    // Do nothing...applicable algorithms will override this method as needed.
  }

  /**
   * For interactive learning, set a negative user input (defined by algorithm)
   * @author George Booth
   */
  public void setNegativeUserInput()
  {
    // Do nothing...applicable algorithms will override this method as needed.
  }

  /**
   * @author Sunit Bhalla
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Do nothing...applicable Algorithms will override this method as needed.
  }

  /**
   * @author Matt Wharton
   */
  public void deleteLearnedJointSpecificData(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    // Do nothing...applicable Algorithms will override this method as needed.
  }

  /**
   * @author Peter Esbensen
   */
  public void deleteLearnedJointSpecificData(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    // Do nothing...applicable Algorithms will override this method as needed.
  }

  /**
   * @author George Booth
   */
  public void deleteLearnedExpectedImageData(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    // Do nothing...applicable Algorithms will override this method as needed.
  }

  /**
   * @author George Booth
   */
  public void deleteLearnedExpectedImageData(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    // Do nothing...applicable Algorithms will override this method as needed.
  }

  
  /**
   * Clear Tombstone
   * @author Sheng-chuan.yong
   */
  public void deleteLearnedImageTemplateData(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    // Do nothing...applicable Algorithms will override this method as needed.
  }
  
  /**
   * Clear Tombstone
   * @author Sheng-chuan.yong
   */
  public void deleteLearnedImageTemplateData(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    // Do nothing...applicable Algorithms will override this method as needed.
  }

  /**
   * Classify all the given joints.  Implementations of this method MUST be
   * threadsafe.
   *
   * @author Peter Esbensen
   */
  public abstract void classifyJoints(ReconstructedImages reconstructedImages,
                                      List<JointInspectionData> jointInspectionDataObjects) throws XrayTesterException;

  /**
   * Classify joints in the MeasurementGroup.  Algorithms that require measurements
   * for other joints will need to use this entry point.  One example of this
   * is the Open Outlier algorithm.
   * Algorithms do not have to define their own classifyMeasurementGroup if they
   * don't have a need for it;  this default implementation will run instead (and do nothing).
   *
   * @author Peter Esbensen
   */
  protected void classifyMeasurementGroup(MeasurementGroup measurementGroup,
                                          Map<Pad, ReconstructionRegion> padToRegionMap) throws XrayTesterException
  {
    // do nothing
  }
  
  /**
   * @author Siew Yeng
   */
  protected void classifyMeasurementGroups(List<MeasurementGroup> measurementGroup,
                                          Map<Pad, ReconstructionRegion> padToRegionMap) throws XrayTesterException
  {
    // do nothing
  }

  /**
   * Assert that all the given jointInspectionData objects are of the same subtype.
   * This is just a helper method for algorithms.
   *
   * @author Peter Esbensen
   */
  protected void assertAllJointInspectionDataObjectsAreSameSubtype(List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(jointInspectionDataObjects != null);

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      Assert.expect(subtype == jointInspectionData.getSubtype());
    }
  }
  
  //get number of slice that been set
  // @author Cheah Lee Herng
  // @author chin-seong, kee
  static public int getNumberOfSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);
    
    String numberOfSlice = null;
    JointTypeEnum jointType = subtype.getJointTypeEnum();
    
    if (jointType.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
        jointType.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
        jointType.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
        jointType.equals(JointTypeEnum.CGA))
    {
      numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE);
    }
    else if (jointType.equals(JointTypeEnum.THROUGH_HOLE) ||
             jointType.equals(JointTypeEnum.OVAL_THROUGH_HOLE) || //Siew Yeng - XCR-3318 - Oval PTH
             jointType.equals(JointTypeEnum.PRESSFIT))
    {
      numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_NUMBER_OF_SLICE);
    }
    else if (jointType.equals(JointTypeEnum.SURFACE_MOUNT_CONNECTOR) || jointType.equals(JointTypeEnum.RF_SHIELD) || jointType.equals(JointTypeEnum.SMALL_OUTLINE_LEADLESS))
    {
      numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE);
    }
    else if (jointType.equals(JointTypeEnum.SINGLE_PAD) ||
            jointType.equals(JointTypeEnum.LGA))
    {
      numberOfSlice = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE);
    }
    
    return Integer.parseInt(numberOfSlice);
  }
  
  /**
   * @author Swee Yee Wong
   * This method will learn and update the algorithm setting if necessary during program generation
   */
  public void learnAndUpdateAlgorithmSettingIfNecessaryDuringProgramGeneration(Subtype subtype,
                                                                  AlgorithmSettingEnum algSettingEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(algSettingEnum != null);
    // Do nothing...applicable Algorithms will override this method as needed.
  }

  /**
   * Algorithm settings ignore list for sub-subtype
   * @author Siew Yeng
   */
  public List<AlgorithmSettingEnum> getAlgorithmSettingEnumsIgnoreListForSubSubtype()
  {
    return null;
  }
}
