package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;

/**
 * Comparator for Algorithms to see which should come first in an inspection.
 * Basically, we ensure that the Measurement algorithm is first.  Other
 * algorithms are sorted by their internal Enum id.
 *
 * @author Peter Esbensen
 */
public class AlgorithmEnumInspectionOrderComparator implements Comparator<AlgorithmEnum>
{
  /**
   * @author Peter Esbensen
   */
  public AlgorithmEnumInspectionOrderComparator()
  {
    super();
    // do nothing
  }

  /**
   * This is a help function that has the same return value rules as compare().  I created it so that AlgorithmEnumInspectionOrderComparator
   * and AlgorithmInspectionOrderComparator can share the same code.
   *
   * @author Peter Esbensen
   */
  static int compareAlgorithmEnums(final AlgorithmEnum algorithm1Enum, final AlgorithmEnum algorithm2Enum)
  {
    Assert.expect(algorithm1Enum != null);
    Assert.expect(algorithm2Enum != null);

    if (algorithm1Enum == algorithm2Enum)
      return 0;

    final int algorithm1EnumId = algorithm1Enum.getId();
    final int algorithm2EnumId = algorithm2Enum.getId();

    if (algorithm1EnumId < algorithm2EnumId)
      return -1;
    else if (algorithm1EnumId > algorithm2EnumId)
      return 1;
    return 0;
  }

  /**
   * @author Peter Esbensen
   */
  public int compare(AlgorithmEnum algorithm1Enum, AlgorithmEnum algorithm2Enum)
  {
    Assert.expect(algorithm1Enum != null);
    Assert.expect(algorithm2Enum != null);

    return compareAlgorithmEnums(algorithm1Enum, algorithm2Enum);
  }
}
