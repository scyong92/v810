package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * An AlgorithmSetting is a user-adjustable parameter that affects the behavior of the
 * classification algorithms.
 *
 * @author Peter Esbensen
 */
public class AlgorithmSetting implements Comparable
{
  private int _displayOrder = -1;
  private final Map<Integer, Serializable> _algorithmVersionToDefaultMap = new HashMap<Integer, Serializable>();
  private final Map<Integer, Serializable> _algorithmVersionToMinimumValueMap = new HashMap<Integer, Serializable>();
  private final Map<Integer, Serializable> _algorithmVersionToMaximumValueMap = new HashMap<Integer, Serializable>();
  private final Map<Integer, Serializable> _algorithmVersionToValuesListMap = new HashMap<Integer, Serializable>();
  private final Map<Integer, URL> _algorithmVersionToDescriptionURLMap = new HashMap<Integer, URL>();
  private final Map<Integer, URL> _algorithmVersionToDetailedDescriptionURLMap = new HashMap<Integer, URL>();
  private final Map<Integer, URL> _algorithmVersionToImageURLMap = new HashMap<Integer, URL>();
  private final Map<Integer, MeasurementUnitsEnum> _algorithmVersionToUnitsMap = new HashMap<Integer, MeasurementUnitsEnum>();
  private AlgorithmSettingEnum _algorithmSettingEnum;
  private final Map<Integer, AlgorithmSettingTypeEnum> _algorithmVersionToAlgorithmSettingTypeEnum =
      new HashMap<Integer,AlgorithmSettingTypeEnum>();
  private final List<JointTypeEnum> _jointTypeEnumFilter = new ArrayList<JointTypeEnum>();
  private AlgorithmSettingEnum _dependentAlgorithmSettingEnum;  // Sometimes an algorithm setting value depends on another algorithm setting value
  private AlgorithmSettingComparatorEnum _algorithmSettingComparatorEnum;
  private SliceNameEnum _algorithmForSpecificSlice = null;

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  /** @todo wpd - matt or peter or patrick too many parameters - use get/set methods instead - typically keep # params to 3 or so */
  public AlgorithmSetting(AlgorithmSettingEnum algorithmSettingEnum,
                          int displayOrder,
                          Serializable defaultValue,
                          Serializable minimumValue,
                          Serializable maximumValue,
                          MeasurementUnitsEnum units,
                          String descriptionURLKey,
                          String detailedDescriptionURLKey,
                          String imageURLKey,
                          AlgorithmSettingTypeEnum algorithmSettingTypeEnum,
                          int algorithmVersion)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(displayOrder >= 0);
    Assert.expect(defaultValue != null);
    Assert.expect(minimumValue != null);
    Assert.expect(maximumValue != null);
    Assert.expect(units != null);
    Assert.expect(descriptionURLKey != null);
    Assert.expect(detailedDescriptionURLKey != null);
    Assert.expect(imageURLKey != null);
    Assert.expect(algorithmSettingTypeEnum != null);
    Assert.expect(algorithmVersion > 0);

    _displayOrder = displayOrder;
    _algorithmVersionToDefaultMap.put(algorithmVersion, defaultValue);
    _algorithmVersionToMinimumValueMap.put(algorithmVersion, minimumValue);
    _algorithmVersionToMaximumValueMap.put(algorithmVersion, maximumValue);
    _algorithmVersionToUnitsMap.put(algorithmVersion, units);

    URL descriptionURL = convertKeyToURL(descriptionURLKey);
    _algorithmVersionToDescriptionURLMap.put(algorithmVersion, descriptionURL);

    URL detailedDescriptionURL = convertKeyToURL(detailedDescriptionURLKey);
    _algorithmVersionToDetailedDescriptionURLMap.put(algorithmVersion, detailedDescriptionURL);

    URL imageURL = convertKeyToURL(imageURLKey);
    _algorithmVersionToImageURLMap.put(algorithmVersion, imageURL);

    _algorithmSettingEnum = algorithmSettingEnum;
    _algorithmVersionToAlgorithmSettingTypeEnum.put(algorithmVersion, algorithmSettingTypeEnum);

    _dependentAlgorithmSettingEnum = null;
    _algorithmSettingComparatorEnum = null;
  }

  /**
   * @author Cheah Lee Herng
   */
  public AlgorithmSetting(AlgorithmSettingEnum algorithmSettingEnum,
                          int displayOrder,
                          Serializable defaultValue,
                          Serializable minimumValue,
                          Serializable maximumValue,
                          MeasurementUnitsEnum units,
                          String descriptionURLKey,
                          String detailedDescriptionURLKey,
                          String imageURLKey,
                          AlgorithmSettingTypeEnum algorithmSettingTypeEnum,
                          int algorithmVersion,
                          AlgorithmSettingEnum dependentAlgorithmSettingEnum,
                          AlgorithmSettingComparatorEnum algorithmSettingComparatorEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(displayOrder >= 0);
    Assert.expect(defaultValue != null);
    Assert.expect(minimumValue != null);
    Assert.expect(maximumValue != null);
    Assert.expect(units != null);
    Assert.expect(descriptionURLKey != null);
    Assert.expect(detailedDescriptionURLKey != null);
    Assert.expect(imageURLKey != null);
    Assert.expect(algorithmSettingTypeEnum != null);
    Assert.expect(algorithmVersion > 0);
    Assert.expect(dependentAlgorithmSettingEnum != null);
    Assert.expect(algorithmSettingComparatorEnum != null);

    _displayOrder = displayOrder;
    _algorithmVersionToDefaultMap.put(algorithmVersion, defaultValue);
    _algorithmVersionToMinimumValueMap.put(algorithmVersion, minimumValue);
    _algorithmVersionToMaximumValueMap.put(algorithmVersion, maximumValue);
    _algorithmVersionToUnitsMap.put(algorithmVersion, units);

    URL descriptionURL = convertKeyToURL(descriptionURLKey);
    _algorithmVersionToDescriptionURLMap.put(algorithmVersion, descriptionURL);

    URL detailedDescriptionURL = convertKeyToURL(detailedDescriptionURLKey);
    _algorithmVersionToDetailedDescriptionURLMap.put(algorithmVersion, detailedDescriptionURL);

    URL imageURL = convertKeyToURL(imageURLKey);
    _algorithmVersionToImageURLMap.put(algorithmVersion, imageURL);

    _algorithmSettingEnum = algorithmSettingEnum;
    _algorithmVersionToAlgorithmSettingTypeEnum.put(algorithmVersion, algorithmSettingTypeEnum);

    _dependentAlgorithmSettingEnum = dependentAlgorithmSettingEnum;
    _algorithmSettingComparatorEnum = algorithmSettingComparatorEnum;
  }

  /**
   * @author Matt Wharton
   */
  public AlgorithmSetting(AlgorithmSettingEnum algorithmSettingEnum,
                          int displayOrder,
                          Serializable defaultValue,
                          Serializable valuesList,
                          MeasurementUnitsEnum units,
                          String descriptionURLKey,
                          String detailedDescriptionURLKey,
                          String imageURLKey,
                          AlgorithmSettingTypeEnum algorithmSettingTypeEnum,
                          int algorithmVersion)
  {
    Assert.expect(displayOrder >= 0);
    Assert.expect(defaultValue != null);
    Assert.expect(valuesList != null);
    Assert.expect(units != null);
    Assert.expect(descriptionURLKey != null);
    Assert.expect(detailedDescriptionURLKey != null);
    Assert.expect(imageURLKey != null);
    Assert.expect(algorithmSettingEnum != null);
    Assert.expect(algorithmSettingTypeEnum != null);
    Assert.expect(algorithmVersion > 0);

    _displayOrder = displayOrder;
    _algorithmVersionToDefaultMap.put(algorithmVersion, defaultValue);
    _algorithmVersionToValuesListMap.put(algorithmVersion, valuesList);
    _algorithmVersionToUnitsMap.put(algorithmVersion, units);

    URL descriptionURL = convertKeyToURL(descriptionURLKey);
    _algorithmVersionToDescriptionURLMap.put(algorithmVersion, descriptionURL);

    URL detailedDescriptionURL = convertKeyToURL(detailedDescriptionURLKey);
    _algorithmVersionToDetailedDescriptionURLMap.put(algorithmVersion, detailedDescriptionURL);

    URL imageURL = convertKeyToURL(imageURLKey);
    _algorithmVersionToImageURLMap.put(algorithmVersion, imageURL);

    _algorithmSettingEnum = algorithmSettingEnum;
    _algorithmVersionToAlgorithmSettingTypeEnum.put(algorithmVersion, algorithmSettingTypeEnum);

    _dependentAlgorithmSettingEnum = null;
    _algorithmSettingComparatorEnum = null;
  }

  /**
   * @author Peter Esbensen
   */
  public int compareTo(Object object)
  {
    final AlgorithmSetting algorithmSetting = (AlgorithmSetting)object;

    return getName().compareTo(algorithmSetting.getName());
  }

  /**
   * @author Andy Mechtenberg
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Peter Esbensen
   */
  public String getName()
  {
    Assert.expect(_algorithmSettingEnum != null);
    return _algorithmSettingEnum.getName();
  }

  /**
   * @author Peter Esbensen
   */
  void setDisplayOrder(final int displayOrder)
  {
    Assert.expect(displayOrder != -1);
    _displayOrder = displayOrder;
  }

  /**
   * @author Bill Darbie
   */
  public int getDisplayOrder()
  {
    Assert.expect(_displayOrder != -1);
    return _displayOrder;
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getDefaultValue(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    final Serializable value = _algorithmVersionToDefaultMap.get(algorithmVersion);
    Assert.expect(value != null);

    return value;
  }

  /**
   * @author Peter Esbensen
   */
  void setAlgorithmDefault(final int algorithmVersion, final Serializable algorithmDefault)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(algorithmDefault != null);

    final Serializable prev = _algorithmVersionToDefaultMap.put(algorithmVersion, algorithmDefault);
    Assert.expect(prev == null);
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getMaximumValue(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    final Serializable value = _algorithmVersionToMaximumValueMap.get(algorithmVersion);
    Assert.expect(value != null);
    return value;
  }

  /**
   * @author Peter Esbensen
   */
  void setMaximumValue(final int algorithmVersion, final Serializable algorithmDefault)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(algorithmDefault != null);

    final Serializable prev = _algorithmVersionToMaximumValueMap.put(algorithmVersion, algorithmDefault);
    Assert.expect(prev == null);
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasMaximumValue(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(_algorithmVersionToMaximumValueMap != null);

    return _algorithmVersionToMaximumValueMap.containsKey(algorithmVersion);
  }

  /**
   * @author Peter Esbensen
   */
  public Serializable getMinimumValue(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    final Serializable value = _algorithmVersionToMinimumValueMap.get(algorithmVersion);
    Assert.expect(value != null);
    return value;
  }

  /**
   * @author Peter Esbensen
   */
  void setMinimumValue(final int algorithmVersion, final Serializable algorithmDefault)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(algorithmDefault != null);

    final Serializable prev = _algorithmVersionToMinimumValueMap.put(algorithmVersion, algorithmDefault);
    Assert.expect(prev == null);
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasMinimumValue(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(_algorithmVersionToMinimumValueMap != null);

    return _algorithmVersionToMinimumValueMap.containsKey(algorithmVersion);
  }

  /**
   * @author Matt Wharton
   */
  public Serializable getValuesList(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(_algorithmVersionToValuesListMap != null);

    final Serializable valuesList = _algorithmVersionToValuesListMap.get(algorithmVersion);
    Assert.expect(valuesList != null);

    return valuesList;
  }

  /**
   * @author Matt Wharton
   */
  void setValuesList(final int algorithmVersion, final Serializable valuesList)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(valuesList != null);
    Assert.expect(_algorithmVersionToValuesListMap != null);

    final Serializable prevValuesList = _algorithmVersionToValuesListMap.put(algorithmVersion, valuesList);
    Assert.expect(prevValuesList != null);
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasValuesList(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    return _algorithmVersionToValuesListMap.containsKey(algorithmVersion);
  }

  /**
   * @author Andy Mechtenberg
   */
  public MeasurementUnitsEnum getUnits(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    final MeasurementUnitsEnum units = _algorithmVersionToUnitsMap.get(algorithmVersion);
    Assert.expect(units != null);
    return units;
  }

  /**
   * @author Andy Mechtenberg
   */
  public URL getDescriptionURL(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    final URL descriptionURL = _algorithmVersionToDescriptionURLMap.get(algorithmVersion);
    Assert.expect(descriptionURL != null);
    return descriptionURL;
  }

  /**
   * @author Andy Mechtenberg
   */
  public URL getDetailedDescriptionURL(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    final URL descriptionURL = _algorithmVersionToDetailedDescriptionURLMap.get(algorithmVersion);

    return descriptionURL;
  }

  /**
   * @author Andy Mechtenberg
   */
  public URL getImageURL(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);

    final URL descriptionURL = _algorithmVersionToImageURLMap.get(algorithmVersion);

    return descriptionURL;
  }

  /**
   * @author Peter Esbensen
   */
  public AlgorithmSettingEnum getAlgorithmSettingEnum()
  {
    Assert.expect(_algorithmSettingEnum != null);
    return _algorithmSettingEnum;
  }

  /**
   * @author Bill Darbie
   */
  void setAlgorithmSettingEnum(final AlgorithmSettingEnum algorithmSettingEnum)
  {
    Assert.expect(algorithmSettingEnum != null);
    _algorithmSettingEnum = algorithmSettingEnum;
  }

  /**
   * @author Matt Wharton
   */
  public AlgorithmSettingTypeEnum getAlgorithmSettingTypeEnum(final int algorithmVersion)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(_algorithmVersionToAlgorithmSettingTypeEnum != null);

    final AlgorithmSettingTypeEnum algorithmSettingTypeEnum = _algorithmVersionToAlgorithmSettingTypeEnum.get(algorithmVersion);
    Assert.expect(algorithmSettingTypeEnum != null);
    return algorithmSettingTypeEnum;
  }

  /**
   * @author Matt Wharton
   */
  void setAlgorithmSettingTypeEnum(final int algorithmVersion, final AlgorithmSettingTypeEnum algorithmSettingTypeEnum)
  {
    Assert.expect(algorithmVersion > 0);
    Assert.expect(algorithmSettingTypeEnum != null);

    final AlgorithmSettingTypeEnum previousEntry = _algorithmVersionToAlgorithmSettingTypeEnum.put(
      algorithmVersion,
      algorithmSettingTypeEnum);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Andy Mechtenberg
   * @author Patrick Lacz
   */
  protected URL convertKeyToURL(final String key)
  {
    URL docsURL = null;

    if (key.length() > 0) // checking for empty string
    {
      final String valueOfKey = StringLocalizer.keyToString(key);

      if (valueOfKey.length() > 0)
      {
        try
        {
          docsURL = new URL(Directory.getDocsDir() + File.separator + valueOfKey);
        }
        catch (MalformedURLException ex)
        {
          docsURL = null;
        }
      } // end if valid key found
    } // end if valid string passed
    return docsURL;
  }

  /**
   * @author Peter Esbensen
   */
  public void filter(final JointTypeEnum jointTypeEnum)
  {
    Assert.expect(_jointTypeEnumFilter != null);
    Assert.expect(_jointTypeEnumFilter.contains(jointTypeEnum) == false); // don't add things twice . . . at the very least, it is sloppy to add things twice
    _jointTypeEnumFilter.add(jointTypeEnum);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean filterContains(final JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    return _jointTypeEnumFilter.contains(jointTypeEnum);
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean hasDependentAlgorithmSettingEnum()
  {
    if (_dependentAlgorithmSettingEnum == null)
        return false;
    else
        return true;
  }

  /**
   * @author Cheah Lee Herng
   */
  public AlgorithmSettingEnum getDependentAlgorithmSettingEnum()
  {
    Assert.expect(_dependentAlgorithmSettingEnum != null);
    return _dependentAlgorithmSettingEnum;
  }

  /**
   * @author Cheah Lee Herng
   */
  public boolean hasAlgorithmSettingComparatorEnum()
  {
    if (_algorithmSettingComparatorEnum == null)
        return false;
    else
        return true;
  }

  /**
   * @author Cheah Lee Herng
   */
  public AlgorithmSettingComparatorEnum getAlgorithmSettingComparatorEnum()
  {
    Assert.expect(_algorithmSettingComparatorEnum != null);
    return _algorithmSettingComparatorEnum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean isSliceSpecific()
  {
      if (_algorithmForSpecificSlice == null)
          return false;
      else
          return true;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public SliceNameEnum getSpecificSlice()
  {
      Assert.expect(_algorithmForSpecificSlice != null);
      return _algorithmForSpecificSlice;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setSpecificSlice(SliceNameEnum slice)
  {
      Assert.expect(slice != null);
      _algorithmForSpecificSlice = slice;
  }
}
