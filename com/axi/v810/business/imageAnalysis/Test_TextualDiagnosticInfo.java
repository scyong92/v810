package com.axi.v810.business.imageAnalysis;

import java.io.*;

import com.axi.util.*;

/**
 * Test class for TextualDiagnosticInfo.
 *
 * @author Matt Wharton
 */
public class Test_TextualDiagnosticInfo extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public Test_TextualDiagnosticInfo()
  {
    // Do nothing...
  }

  /**
   * Tests the 'happy path'.
   *
   * @author Matt Wharton
   */
  private void testHappyPath()
  {
    // Test that the constuctor and associated getters work properly.
    LocalizedString localizedTextMessage1 = new LocalizedString("bogus", null);
    TextualDiagnosticInfo textualDiagInfo = new TextualDiagnosticInfo(localizedTextMessage1);
    Expect.expect(textualDiagInfo.getDiagnosticText() == localizedTextMessage1, "Text message doesn't match!");

    // Test that the setters work properly.
    LocalizedString localizedTextMessage2 = new LocalizedString("foo", null);
    textualDiagInfo.setDiagnosticText(localizedTextMessage2);
    Expect.expect(textualDiagInfo.getDiagnosticText() == localizedTextMessage2, "Text message doesn't match!");
  }

  /**
   * Tests that the asserts are working properly.
   *
   * @author Matt Wharton
   */
  private void testAsserts()
  {
    // Test that the constructor asserts when passed null paraemeters.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new TextualDiagnosticInfo(null);
      }
    });

    // Test that the setters assert when passed null parameters.
    final TextualDiagnosticInfo textualDiagInfo = new TextualDiagnosticInfo(new LocalizedString("foo", null));
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        textualDiagInfo.setDiagnosticText(null);
      }
    });
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   * @author Matt Wharton
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // Test the 'happy path'.
    testHappyPath();

    // Test the asserts.
    testAsserts();
  }

  /**
   * Main entry point for the test fixture.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_TextualDiagnosticInfo());
  }
}
