package com.axi.v810.business.imageAnalysis.advancedGullwing;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gullwing.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * Advanced Gullwing insuffcient algorithm.  This simply uses the same insufficient detection technique as
 * standard Gullwing.
 *
 * @author Matt Wharton
 */
public class AdvancedGullwingInsufficientAlgorithm extends GullwingInsufficientAlgorithm
{
  /**
   * @author Matt Wharton
   */
  public AdvancedGullwingInsufficientAlgorithm(InspectionFamily advancedGullwingInspectionFamily)
  {
    super(InspectionFamilyEnum.ADVANCED_GULLWING);

    Assert.expect(advancedGullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(advancedGullwingInspectionFamily);

    super.addMeasurementEnums();
  }

  /**
   * Adds the algorithm settings for Advanced Gullwing Insufficient.
   *
   * @author Matt Wharton
   */
  private void addAlgorithmSettings(InspectionFamily advancedGullwingInspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    // Minimum Fillet Thickness % of Nominal
    AlgorithmSetting minimumFilletThicknessPercentOfNominal = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        60.f, // default
        0.f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_ADVANCED_GULLWING_INSUFFICIENT_(MIN_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_INSUFFICIENT_(MIN_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_INSUFFICIENT_(MIN_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minimumFilletThicknessPercentOfNominal,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL );
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minimumFilletThicknessPercentOfNominal,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL );
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minimumFilletThicknessPercentOfNominal,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL );

    addAlgorithmSetting(minimumFilletThicknessPercentOfNominal);
  }

  /**
   * MDW - The 'classifyJoints', 'learn' and 'deleteLearnedJoint' procedures for Advanced Gullwing Insufficient
   * are identical to Gullwing Insufficient.  As such, we will just use the inherited versions of these
   * methods.
   *
   * I wish all algorithms were this easy!
   */
  }
