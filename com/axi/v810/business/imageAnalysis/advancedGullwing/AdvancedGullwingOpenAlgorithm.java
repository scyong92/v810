package com.axi.v810.business.imageAnalysis.advancedGullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gullwing.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;

/**
 * Advanced Gullwing Open algorithm.
 *
 * @author Matt Wharton
 */
public class AdvancedGullwingOpenAlgorithm extends GullwingOpenAlgorithm
{
  /**
   * @author Matt Wharton
   */
  public AdvancedGullwingOpenAlgorithm(InspectionFamily advancedGullwingInspectionFamily)
  {
    super(InspectionFamilyEnum.ADVANCED_GULLWING);

    Assert.expect(advancedGullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(advancedGullwingInspectionFamily);

    super.addMeasurementEnums();
  }

  /**
   * Adds the algorithm settings for Advanced Gullwing Open.
   *
   * @author Matt Wharton
   */
  private void addAlgorithmSettings(InspectionFamily advancedGullwingInspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    // Minimum allowable heel slope.
    AlgorithmSetting minHeelSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE,
      displayOrder++,
      0.6f, // default
      -1000.f, // min
      1000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minHeelSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minHeelSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minHeelSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);
    addAlgorithmSetting(minHeelSlopeSetting);
    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum allowable heel slope.
    AlgorithmSetting maxHeelSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE,
      displayOrder++,
      1000.f, // default
      -1000.f, // min
      1000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxHeelSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxHeelSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxHeelSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);
    addAlgorithmSetting(maxHeelSlopeSetting);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum allowable toe slope.
    AlgorithmSetting minToeSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE,
      displayOrder++,
      0.3f, // default
      -1000.f, // min
      1000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minToeSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minToeSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minToeSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);
    addAlgorithmSetting(minToeSlopeSetting);
    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum allowable toe slope.
    AlgorithmSetting maxToeSlopeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE,
      displayOrder++,
      1000.f, // default
      -1000.f, // min
      1000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxToeSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxToeSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxToeSlopeSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);
    addAlgorithmSetting(maxToeSlopeSetting);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum allowable heel and toe slope sum.
    AlgorithmSetting minHeelAndToeSlopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM,
      displayOrder++,
      1.1f, // default
      -2000.f, // min
      2000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minHeelAndToeSlopeSumSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minHeelAndToeSlopeSumSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minHeelAndToeSlopeSumSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);
    addAlgorithmSetting(minHeelAndToeSlopeSumSetting);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Maximum allowable heel and toe slope sum.
    AlgorithmSetting maxHeelAndToeSlopeSumSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM,
      displayOrder++,
      2000.f, // default
      -2000.f, // min
      2000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxHeelAndToeSlopeSumSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxHeelAndToeSlopeSumSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxHeelAndToeSlopeSumSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);
    addAlgorithmSetting(maxHeelAndToeSlopeSumSetting);
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum sum of slope changes.
    AlgorithmSetting minSumOfSlopeChanges = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES,
      displayOrder++,
      3.f, // default
      0.f, // min
      1000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);
    addAlgorithmSetting(minSumOfSlopeChanges);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START    
    // Maximum sum of slope changes.
    AlgorithmSetting maxSumOfSlopeChanges = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES,
      displayOrder++,
      1000.f, // default
      0.f, // min
      1000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxSumOfSlopeChanges,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);
    addAlgorithmSetting(maxSumOfSlopeChanges);    
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Minimum heel thickness (as percent of nominal).
    AlgorithmSetting minimumHeelThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        60.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minimumHeelThicknessPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minimumHeelThicknessPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minimumHeelThicknessPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minimumHeelThicknessPercentOfNominalSetting);

    // Minimum fillet length (as percent of nominal).
    AlgorithmSetting minimumFilletLengthPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
        displayOrder++,
        50.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minimumFilletLengthPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minimumFilletLengthPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minimumFilletLengthPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(minimumFilletLengthPercentOfNominalSetting);

    // Maximum fillet length (as percent of nominal).
    // Maximum limit can be more than 300% but below 400% so far. Set to 500% to cater for any future needs.
    AlgorithmSetting maximumFilletLengthPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL,
        displayOrder++,
        150.f, // default
        100.f, // min
        500.f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maximumFilletLengthPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maximumFilletLengthPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maximumFilletLengthPercentOfNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL);
    addAlgorithmSetting(maximumFilletLengthPercentOfNominalSetting);

    // Maximum center:heel thickness percent.
    AlgorithmSetting maxCenterToHeelThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT,
        displayOrder++,
        85.f, // default
        0.0f, // min
        200.f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
        "HTML_DESC_ADVANCED_GULLWING_OPEN_(MAX_CENTER_TO_HEEL_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MAX_CENTER_TO_HEEL_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_OPEN_(MAX_CENTER_TO_HEEL_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxCenterToHeelThicknessPercentSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxCenterToHeelThicknessPercentSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxCenterToHeelThicknessPercentSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT);
    addAlgorithmSetting(maxCenterToHeelThicknessPercentSetting);

    // Maximum center:heel thickness percent across.
    AlgorithmSetting maxCenterToHeelThicknessPercentAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS,
      displayOrder++,
      100.f, // default
      0.f, // min
      200.f, // max
      MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxCenterToHeelThicknessPercentAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxCenterToHeelThicknessPercentAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxCenterToHeelThicknessPercentAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT);
    addAlgorithmSetting(maxCenterToHeelThicknessPercentAcrossSetting);
    
    // Maximum center:heel thickness percent across.
    AlgorithmSetting maxCenterToHeel2ThicknessPercentAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS,
      displayOrder++,
      200.f, // default
      0.f, // min
      250.f, // max
      MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxCenterToHeel2ThicknessPercentAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxCenterToHeel2ThicknessPercentAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxCenterToHeel2ThicknessPercentAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT);
    addAlgorithmSetting(maxCenterToHeel2ThicknessPercentAcrossSetting);

    // Minimum concavity ratio across.
    AlgorithmSetting minConcavityRatioAcrossSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS,
      displayOrder++,
      -1000.f, // default
      -1000.f, // min
      1000.f, // max
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minConcavityRatioAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minConcavityRatioAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minConcavityRatioAcrossSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO);

    addAlgorithmSetting(minConcavityRatioAcrossSetting);
    
    //Siew Yeng - XCR-3532 - fillet length outlier
    AlgorithmSetting maxFilletLengthRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER,
      displayOrder++,
      300.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    addAlgorithmSetting(maxFilletLengthRegionOutlierSetting);  
    
    
    AlgorithmSetting minFilletLengthRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER,
      displayOrder++,
      0.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_FILLET_LENGTH_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minFilletLengthRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH_REGION_OUTLIER);
    addAlgorithmSetting(minFilletLengthRegionOutlierSetting);  
    
    
    AlgorithmSetting maxCenterThicknessRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER,
      displayOrder++,
      300.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(MAXIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maxCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maxCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maxCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    addAlgorithmSetting(maxCenterThicknessRegionOutlierSetting);
    
    AlgorithmSetting minCenterThicknessRegionOutlierSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER,
      displayOrder++,
      0.f, // default
      0.f, // min
      300.f, // max
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_OPEN_(MINIMUM_CENTER_THICKNESS_REGION_OUTLIER)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          minCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          minCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          minCenterThicknessRegionOutlierSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_CENTER_THICKNESS_REGION_OUTLIER);
    addAlgorithmSetting(minCenterThicknessRegionOutlierSetting);
  }

  /**
   * Main entry point for Advanced Gullwing Open.
   *
   * @author Matt Wharton
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

      BooleanRef jointPassed = new BooleanRef(true);

      // Run the basic Gullwing Open classification.
      basicGullwingClassifyJoint(subtype,
                                 padSlice,
                                 jointInspectionData,
                                 jointPassed);

      // Now run the Advanced Gullwing specific open checks.

      // Check for opens based on heel and toe slopes.
      detectOpensUsingHeelAndToeSlopeChecks(subtype, padSlice, jointInspectionData, jointPassed);

      // Check for opens based on second derivative area.
      detectOpensUsingSumOfSlopeChanges(subtype, padSlice, jointInspectionData, jointPassed);

      // Check for opens based on the center:heel thickness ratio across.
      detectOpensUsingCenterToHeelThicknessPercentAcross(subtype, padSlice, jointInspectionData, jointPassed);

      // Check for opens based on the concavity ratio across.
      detectOpensUsingConcavityRatioAcross(subtype, padSlice, jointInspectionData, jointPassed);

      //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
      // Check for opens based on fillet length region outlier
      detectOpensUsingFilletLengthRegionOutlier(jointInspectionData, 
                                                padSlice, 
                                                (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER),
                                                (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER), 
                                                jointPassed);
      
      // Check for opens based on center thickness region outlier
      detectOpensUsingCenterThicknessRegionOutlier(jointInspectionData, 
                                                    padSlice, 
                                                    (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER),
                                                    (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER), 
                                                    jointPassed);
      
      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }
  
  /**
   * Detects opens by looking at the heel slope, the toe slope, and the sum of these two slopes.
   * Lim, Lay Ngor modify: XCR1648: Add Maximum Slope Limit & Code standardization.
   *
   * @author Matt Wharton
   */
  private void detectOpensUsingHeelAndToeSlopeChecks(Subtype subtype,
                                                     SliceNameEnum sliceNameEnum,
                                                     JointInspectionData jointInspectionData,
                                                     BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the max heel slope measurement.
    JointMeasurement maxHeelSlopeMeasurement =
      AdvancedGullwingMeasurementAlgorithm.getMaximumHeelSlopeMeasurement(jointInspectionData, sliceNameEnum);

    // Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    // Check if the "max heel slope" is within acceptable minimum & maximum limits.
    final float MINIMUM_ALLOWABLE_HEEL_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE);
    
    final float MAXIMUM_ALLOWABLE_HEEL_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE);
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            maxHeelSlopeMeasurement,
                                            MINIMUM_ALLOWABLE_HEEL_SLOPE,
                                            MAXIMUM_ALLOWABLE_HEEL_SLOPE,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD) 
                                            == false)
    {
      jointPassed.setValue(false);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

    // Get the max toe slope measurement.
    JointMeasurement maxToeSlopeMeasurement =
      AdvancedGullwingMeasurementAlgorithm.getMaximumToeSlopeMeasurement(jointInspectionData, sliceNameEnum);

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START    
    // Check if the "max toe slope" is within acceptable minimum & maximum limits.
    final float MINIMUM_ALLOWABLE_TOE_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE);
 
    final float MAXIMUM_ALLOWABLE_TOE_SLOPE =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE);
    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            maxToeSlopeMeasurement,
                                            MINIMUM_ALLOWABLE_TOE_SLOPE,
                                            MAXIMUM_ALLOWABLE_TOE_SLOPE,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD) 
                                            == false)
    {
      jointPassed.setValue(false);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END    

    // Get the "sum of max heel and toe slopes" measurement.
    JointMeasurement sumOfMaxHeelAndToeSlopesMeasurement =
      AdvancedGullwingMeasurementAlgorithm.getMaximumHeelAndToeSlopeSum(jointInspectionData, sliceNameEnum);
    float sumOfMaxHeelAndToeSlopes = sumOfMaxHeelAndToeSlopesMeasurement.getValue();

    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
    boolean sumOfMaxHeelAndToeSlopesValueBelowMinimum = false;
    boolean sumOfMaxHeelAndToeSlopesValueAboveMaximum = false;
    // Check if the "sum of max heel and toe slopes" is within acceptable minimum & maximum limits.
    final float MINIMUM_ALLOWABLE_HEEL_AND_TOE_SLOPE_SUM =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM);    
    final float MAXIMUM_ALLOWABLE_HEEL_AND_TOE_SLOPE_SUM =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM);
    
    if (MathUtil.fuzzyLessThan(sumOfMaxHeelAndToeSlopes, MINIMUM_ALLOWABLE_HEEL_AND_TOE_SLOPE_SUM))
    {
      // sumOfMaxHeelAndToeSlopes is below the allowable minimum.
      sumOfMaxHeelAndToeSlopesValueBelowMinimum = true;
      
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      sumOfMaxHeelAndToeSlopesMeasurement,
                                                      MINIMUM_ALLOWABLE_HEEL_AND_TOE_SLOPE_SUM);
    }
    else if (MathUtil.fuzzyGreaterThan(sumOfMaxHeelAndToeSlopes, MAXIMUM_ALLOWABLE_HEEL_AND_TOE_SLOPE_SUM))
    {
      // sumOfMaxHeelAndToeSlopes is above the allowable maximum.
      sumOfMaxHeelAndToeSlopesValueAboveMaximum = true;
        
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      sumOfMaxHeelAndToeSlopesMeasurement,
                                                      MAXIMUM_ALLOWABLE_HEEL_AND_TOE_SLOPE_SUM);
    }
    
    if (sumOfMaxHeelAndToeSlopesValueBelowMinimum || sumOfMaxHeelAndToeSlopesValueAboveMaximum)
    {
      // Create an Open indictment for insufficient heel and toe slope sum.
      JointIndictment heelAndToeSlopeSumOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                             this,
                                                                             sliceNameEnum);

      // Tie the 'heel slope', 'toe slope', and 'heel and toe slope sum' measurements to the indictment.
      heelAndToeSlopeSumOpenIndictment.addFailingMeasurement(sumOfMaxHeelAndToeSlopesMeasurement);
      heelAndToeSlopeSumOpenIndictment.addRelatedMeasurement(maxHeelSlopeMeasurement);
      heelAndToeSlopeSumOpenIndictment.addRelatedMeasurement(maxToeSlopeMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(heelAndToeSlopeSumOpenIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      //Only post minimum threshold value for passing joint - to avoid posting duplicate measurement result.
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sumOfMaxHeelAndToeSlopesMeasurement,
                                                      MINIMUM_ALLOWABLE_HEEL_AND_TOE_SLOPE_SUM);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END    
  }

  /**
   * Detects opens by looking at the sum of slope changes.
   * Lim, Lay Ngor - Modify - XCR1648: Add Maximum Slope Limit & Code standardization.
   * 
   * @author Matt Wharton
   */
  private void detectOpensUsingSumOfSlopeChanges(Subtype subtype,
                                                 SliceNameEnum sliceNameEnum,
                                                 JointInspectionData jointInspectionData,
                                                 BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the joint inspection result.
    //JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the "sum of slope changes" measurement.
    JointMeasurement sumOfSlopeChangesMeasurement =
      AdvancedGullwingMeasurementAlgorithm.getSumOfSlopeChangesMeasurement(jointInspectionData, sliceNameEnum);

    // Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START    
    // Check if the "second derivative area" is within acceptable minimum & maximum limits.    
    final float MINIMUM_ALLOWABLE_SUM_OF_SLOPE_CHANGES =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES);
    final float MAXIMUM_ALLOWABLE_SUM_OF_SLOPE_CHANGES =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES);
    
    if (AlgorithmUtil.indictMinMaxThreshold(this,
                                            jointInspectionData,
                                            sliceNameEnum,
                                            IndictmentEnum.OPEN,
                                            sumOfSlopeChangesMeasurement,
                                            MINIMUM_ALLOWABLE_SUM_OF_SLOPE_CHANGES,
                                            MAXIMUM_ALLOWABLE_SUM_OF_SLOPE_CHANGES,
                                            AlgorithmUtil.MinMaxThresholdChekingModeEnum.CHECK_MINIMUM_AND_MAXIMUM_THRESHOLD)
                                            == false)
    {
      jointPassed.setValue(false);
    }
    //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  }

  /**
   * Detects opens by looking at the center:heel thickness percent across.
   *
   * @author Matt Wharton
   */
  private void detectOpensUsingCenterToHeelThicknessPercentAcross(Subtype subtype,
                                                                  SliceNameEnum sliceNameEnum,
                                                                  JointInspectionData jointInspectionData,
                                                                  BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the "center:heel thickness percent across" measurement.
    JointMeasurement centerToHeelThicknessPercentAcrossMeasurement =
      AdvancedGullwingMeasurementAlgorithm.getCenterToHeelThicknessPercentAcrossMeasurement(jointInspectionData,
                                                                                            sliceNameEnum);
    float centerToHeelThicknessPercentAcross = centerToHeelThicknessPercentAcrossMeasurement.getValue();

    // Check if the "center:heel thickness percent across" is within acceptable limits.
    final float MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS);
    if (MathUtil.fuzzyGreaterThan(centerToHeelThicknessPercentAcross, MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerToHeelThicknessPercentAcrossMeasurement,
                                                      MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS);

      // Create an Open indictment for an excessive 'center:heel thickness ratio across'.
      JointIndictment centerToHeelThicknessRatioAcrossOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                                           this,
                                                                                           sliceNameEnum);

      // Tie the 'center:heel thickness ratio across' measurement to this joint.
      centerToHeelThicknessRatioAcrossOpenIndictment.addFailingMeasurement(centerToHeelThicknessPercentAcrossMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(centerToHeelThicknessRatioAcrossOpenIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerToHeelThicknessPercentAcrossMeasurement,
                                                      MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS);
    }
    
    // Get the "center:heel thickness percent across" measurement.
    JointMeasurement centerToHeel2ThicknessPercentAcrossMeasurement =
      AdvancedGullwingMeasurementAlgorithm.getCenterToHeel2ThicknessPercentAcrossMeasurement(jointInspectionData,
                                                                                             sliceNameEnum);
    float centerToHeel2ThicknessPercentAcross = centerToHeel2ThicknessPercentAcrossMeasurement.getValue();
    
    // Check if the "center:heel thickness percent across" is within acceptable limits.
    final float MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS);
    if (MathUtil.fuzzyGreaterThan(centerToHeel2ThicknessPercentAcross, MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerToHeel2ThicknessPercentAcrossMeasurement,
                                                      MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS);

      // Create an Open indictment for an excessive 'center:heel thickness ratio across'.
      JointIndictment centerToHeelThicknessRatioAcrossOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                                           this,
                                                                                           sliceNameEnum);

      // Tie the 'center:heel thickness ratio across' measurement to this joint.
      centerToHeelThicknessRatioAcrossOpenIndictment.addFailingMeasurement(centerToHeel2ThicknessPercentAcrossMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(centerToHeelThicknessRatioAcrossOpenIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      centerToHeel2ThicknessPercentAcrossMeasurement,
                                                      MAXIMUM_ALLOWABLE_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS);
    }
  }

  /**
   * Detects opens by looking at the concavity ratio across.
   *
   * @author Matt Wharton
   */
  private void detectOpensUsingConcavityRatioAcross(Subtype subtype,
                                                    SliceNameEnum sliceNameEnum,
                                                    JointInspectionData jointInspectionData,
                                                    BooleanRef jointPassed)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the joint inspection result.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Get the "concavity ratio across" measurement.
    JointMeasurement concavityRatioAcrossMeasurement =
      AdvancedGullwingMeasurementAlgorithm.getConcavityRatioAcrossMeasurement(jointInspectionData,
                                                                              sliceNameEnum);
    float concavityRatioAcross = concavityRatioAcrossMeasurement.getValue();

    // Check if the "concavity ratio across" is within acceptable limits.
    final float MINIMUM_ALLOWABLE_CONCAVITY_RATIO_ACROSS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS);
    if (MathUtil.fuzzyLessThan(concavityRatioAcross, MINIMUM_ALLOWABLE_CONCAVITY_RATIO_ACROSS))
    {
      // Indict the joint for open.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      concavityRatioAcrossMeasurement,
                                                      MINIMUM_ALLOWABLE_CONCAVITY_RATIO_ACROSS);

      // Create an Open indictment for an insufficient 'concavity ratio across'.
      JointIndictment concavityRatioAcrossOpenIndictment = new JointIndictment(IndictmentEnum.OPEN,
                                                                               this,
                                                                               sliceNameEnum);

      // Tie the 'concavity ratio across' measurement to this joint.
      concavityRatioAcrossOpenIndictment.addFailingMeasurement(concavityRatioAcrossMeasurement);

      // Add the indictment to this joint's results.
      jointInspectionResult.addIndictment(concavityRatioAcrossOpenIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      concavityRatioAcrossMeasurement,
                                                      MINIMUM_ALLOWABLE_CONCAVITY_RATIO_ACROSS);
    }
  }

  /**
   * Main entry point for Advanced Gullwing Open learning.
   *
   * @author Matt Wharton
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Advanced Gullwing only runs (and learns) on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    
    /*** mdw - as per Kathy, we don't need this for 1.0
    // Learn the basic Gullwing Open settings.
    learnBasicGullwingSettings(subtype, padSlice);
    */  
    // Learn the Advanced Gullwing Open specific stuff...
    //Siew Yeng - XCR-2139 - Learn the maximum and minimum slope
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      // Learn the "heel and toe slope" defect settings.
      learnHeelAndToeSlopeDefectSettings(subtype, padSlice);

      // Learn the "second derivative area" defect setting.
      learnSecondDerivativeAreaDefectSetting(subtype, padSlice);
    }

    /*
    // Learn the "center:heel thickness ratio across" defect setting.
    learnCenterToHeelThicknessRatioAcrossDefectSetting(subtype, padSlice);

    // Learn the "concavity ratio across" defect setting.
    learnConcavityRatioAcrossDefectSetting(subtype, padSlice);
    */
  }

  /**
   * Learns the "max heel and toe slope" defect settings.
   *
   * @author Matt Wharton
   */
  private void learnHeelAndToeSlopeDefectSettings(Subtype subtype,
                                                  SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Get all the learned "maximum heel slope" values.
    float[] maximumHeelSlopeValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                              MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE,
                                                                              true,
                                                                              true);

    // We can only do the IQR statistics if we have at least two data points.
    if (maximumHeelSlopeValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(maximumHeelSlopeValues);
      maximumHeelSlopeValues = null;
      float medianMaxHeelSlope = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      //Siew Yeng - XCR-2139 - Learn the maximum and minimum slope
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE))
      {
        float minimumAcceptableHeelSlope = medianMaxHeelSlope - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE, minimumAcceptableHeelSlope);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE))
      {
        //Kee Chin Seong - XCR1648: Maximum Slope
        float maximumAcceptableHeelSlope = medianMaxHeelSlope + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE, maximumAcceptableHeelSlope);
      }
    }

    // Get all the learned "maximum toe slope" values.
    float[] maximumToeSlopeValues = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                             MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE,
                                                                             true,
                                                                             true);

    // We can only do the IQR statistics if we have at least two data points.
    if (maximumToeSlopeValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(maximumToeSlopeValues);
      maximumToeSlopeValues = null;
      float medianMaxToeSlope = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      //Siew Yeng - XCR-2139 - Learn the maximum and minimum slope
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE))
      {
        float minimumAcceptableToeSlope = medianMaxToeSlope - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE, minimumAcceptableToeSlope);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE))
      {
        //Kee Chin Seong - XCR1648: Maximum Slope
        float maximumAcceptableToeSlope = medianMaxToeSlope + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE, maximumAcceptableToeSlope);
      }
    }

    // Get all the learned "maximum heel and toe slope sum" values.
    float[] maximumHeelAndToeSlopesSumValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (maximumHeelAndToeSlopesSumValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(maximumHeelAndToeSlopesSumValues);
      maximumHeelAndToeSlopesSumValues = null;
      float medianMaxHeelAndToeSlopeSum = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      //Siew Yeng - XCR-2139 - Learn the maximum and minimum slope
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM))
      {
        float minimumAcceptableHeelAndToeSlopeSum = medianMaxHeelAndToeSlopeSum - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM,
                                minimumAcceptableHeelAndToeSlopeSum);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM))
      {
        //Kee Chin Seong - XCR1648: Maximum Slopes
        float maximumAcceptableHeelAndToeSlopeSum = medianMaxHeelAndToeSlopeSum + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM,
                                maximumAcceptableHeelAndToeSlopeSum);
      }
    }
  }

  /**
   * Learns the "second derivative area" defect setting.
   *
   * @author Matt Wharton
   */
  private void learnSecondDerivativeAreaDefectSetting(Subtype subtype,
                                                      SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Get all the learned "second derivative area" values.
    float[] secondDerivativeAreaValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (secondDerivativeAreaValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(secondDerivativeAreaValues);
      secondDerivativeAreaValues = null;
      float medianSecondDerivativeArea = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      //Siew Yeng - XCR-2139 - Learn the maximum and minimum slope
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES))
      {
        float minimumAcceptableSecondDerivativeArea = medianSecondDerivativeArea - (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES,
                                minimumAcceptableSecondDerivativeArea);
      }
      
      if(subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES))
      {
        //Kee Chin Seong - XCR1648: learn for maximum sum of slope too
        float maximumAcceptableSecondDerivativeArea = medianSecondDerivativeArea + (3f * interQuartileRange);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES,
                                maximumAcceptableSecondDerivativeArea);
      }
    }
  }

  /**
   * Learns the "center:heel thickness ratio across" defect setting.
   *
   * @author Matt Wharton
   */
  private void learnCenterToHeelThicknessRatioAcrossDefectSetting(Subtype subtype,
                                                                  SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Get all the learned "center:heel thickness percent across values" values.
    float[] centerToHeelThicknessPercentAcrossValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (centerToHeelThicknessPercentAcrossValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(centerToHeelThicknessPercentAcrossValues);
      centerToHeelThicknessPercentAcrossValues = null;
      float medianCenterToHeelThicknessPercentAcross = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      float maximumCenterToHeelThicknessPercentAcross = medianCenterToHeelThicknessPercentAcross + (3f * interQuartileRange);

      // Make sure that the max center to heel thickness percent across doesn't get learned above 85%.
      final float MAX_ALLOWABLE_LEARNED_MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS = 85.f;
      float learnedMaximumCenterToHeelThicknessPercentAcross = Math.min(maximumCenterToHeelThicknessPercentAcross,
                                                                        MAX_ALLOWABLE_LEARNED_MAXIMUM_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS);

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS,
                              learnedMaximumCenterToHeelThicknessPercentAcross);
    }
  }

  /**
   * Learns the "concavity ratio across" defect setting.
   *
   * @author Matt Wharton
   */
  private void learnConcavityRatioAcrossDefectSetting(Subtype subtype,
                                                      SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    // Get all the learned "concavity ratio across" values.
    float[] concavityRatioAcrossValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO,
                                                 true,
                                                 true);

    // We can only do the IQR statistics if we have at least two data points.
    if (concavityRatioAcrossValues.length > 1)
    {
      float[] quartileRanges = StatisticsUtil.quartileRangesAllowModification(concavityRatioAcrossValues);
      concavityRatioAcrossValues = null;
      float medianConcavityRatioAcross = quartileRanges[2];
      float interQuartileRange = quartileRanges[3] - quartileRanges[1];

      float minimumAcceptableConcavityRatioAcross = medianConcavityRatioAcross - (3f * interQuartileRange);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS,
                              minimumAcceptableConcavityRatioAcross);
    }
  }

  /**
   * Restores the Advanced Gullwing Open settings back to their defaults.
   *
   * @author Matt Wharton
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    /** @todo mdw - not sure if we want to necessarily reset these when learned data is deleted. */
    // Reset the basic Gullwing Open settings back to their defaults.
    super.setLearnedSettingsToDefaults(subtype);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS);
    
    //Siew Yeng - XCR-2594 - Learned Slope Settings fail to reset
    if(subtype.isEnabledSlopeSettingsLearning())
    {
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE);
      //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE);
      //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
    }
  }
}
