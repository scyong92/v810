package com.axi.v810.business.imageAnalysis.advancedGullwing;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.*;
import java.util.*;

/**
 * Test class for Advanced Gullwing Measurement.
 *
 * @author Matt Wharton
 */
public class Test_AdvancedGullwingMeasurementAlgorithm extends AlgorithmUnitTest
{
  private static final String _TEST_PROJECT_NAME = "FAMILIES_ALL_RLV";
  private static final String _ADVANCED_GULLWING_SUBTYPE_NAME = "dummy_SMTConnector";
  private static final String _ADVANCED_GULLWING_REFDES = "P1";
  private static final String _ADVANCED_GULLWING_PAD_NAME = "1";

  /**
   * @author Matt Wharton
   */
  private Test_AdvancedGullwingMeasurementAlgorithm()
  {
    // Do nothing ...
  }

  /**
   * Main unit test entry point.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    try
    {
      // Load up the test project (FAMILIES_ALL_RLV).
      Project project = getProject(_TEST_PROJECT_NAME);

      // Select the "normal" image set.
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = getNormalImageSet(project);

      // Run a subtype test on one of the Gullwing subtypes.
      Panel panel = project.getPanel();
      Subtype subtype = panel.getSubtype(_ADVANCED_GULLWING_SUBTYPE_NAME);
      imageSetDataList.add(imageSetData);
      runSubtypeUnitTestInspection(project, subtype, imageSetDataList);

      // Now we want to run some tests against 'pathological' images (black, white, random shapes).
      // Pick an arbitarary pad to test.
      Component component = panel.getBoards().get(0).getComponent(_ADVANCED_GULLWING_REFDES);
      Pad pad = component.getPad(_ADVANCED_GULLWING_PAD_NAME);

      // Run against a black image.
      imageSetData = createAndSelectBlackImageSet(project, pad, "gullwingTestBlackImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      // Run against a white image.
      imageSetData = createAndSelectWhiteImageSet(project, pad, "gullwingTestWhiteImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      // Run against a pseudo-random image.
      imageSetData = createAndSelectPseudoRandomImageSet(project, pad, "gullwingTestPseudoRandomImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AdvancedGullwingMeasurementAlgorithm());
  }
}
