package com.axi.v810.business.imageAnalysis.advancedGullwing;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.util.*;

/**
 * @author Matt Wharton
 */
public class Test_AdvancedGullwingLearning extends AlgorithmUnitTest
{
  private List<AlgorithmSettingEnum> _learnedSettings = new LinkedList<AlgorithmSettingEnum>();
  private Map<AlgorithmSettingEnum, Float> _learnedSettingToExpectedLearnedValueMap =
      new HashMap<AlgorithmSettingEnum, Float>();

  private static final String _TEST_PROJECT_NAME = "FAMILIES_ALL_RLV";
  private static final String _ADVANCED_GULLWING_SUBTYPE_NAME = "dummy_SMTConnector";

  /**
   * @author Matt Wharton
   */
  private Test_AdvancedGullwingLearning()
  {
    // Do nothing...
  }

  /**
   * Initializes the "learned settings" data structures for the test.
   *
   * @author Matt Wharton
   */
  private void initializeLearnedSettingsDataStructures()
  {
    Assert.expect(_learnedSettings != null);
    Assert.expect(_learnedSettingToExpectedLearnedValueMap != null);

    _learnedSettings.clear();
    InspectionFamily advancedGullwingInspectionFamily = InspectionFamily.getInstance(InspectionFamilyEnum.ADVANCED_GULLWING);
    for (Algorithm algorithm : advancedGullwingInspectionFamily.getAlgorithms())
    {
      _learnedSettings.addAll(algorithm.getLearnedAlgorithmSettingEnums());
    }

    _learnedSettingToExpectedLearnedValueMap.clear();
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH, 0.8609f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE, 23.19f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS, 0.0529f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS, 0.0632f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT, 72.28f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_CENTER_OFFSET, 5.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT, 60.6f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE, 35.29f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE, 47.34f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT, 104.13f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 105.54f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL, 94.46f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE, 0.0020f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES, 0.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS, -2.5f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE, -0.061f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS, 105.03f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM, -0.059f);
	//Kee Chin Seong - XCR1648: Maximum Slope
	_learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM, 10.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES, 10.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE, 10.0f);
    _learnedSettingToExpectedLearnedValueMap.put(AlgorithmSettingEnum.ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE, 10.0f);
  }

  /**
   * Creates a black image set for the specified project and subtype and runs learning against it.
   *
   * @author Matt Wharton
   */
  private void testLearningOnBlackImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the black image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData blackImageSetData = createImageSetAndSetProgramFilters(project,
                                                                        subtype,
                                                                        "testAdvancedGullwingLearningOnBlackImagesImageSet",
                                                                        POPULATED_BOARD);
    imageSetDataList.add(blackImageSetData);
    generateBlackImageSet(project, blackImageSetData);

    // Run subtype learning.
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a white image set for the specified project and subtype and runs learning against it.
   *
   * @author Matt Wharton
   */
  private void testLearningOnWhiteImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the white image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData whiteImageSetData = createImageSetAndSetProgramFilters(project,
                                                              subtype,
                                                              "testAdvancedGullwingLearningOnWhiteImagesImageSet",
                                                              POPULATED_BOARD);
    imageSetDataList.add(whiteImageSetData);
    generateWhiteImageSet(project, whiteImageSetData);

    // Run subtype learning.
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Creates a pseudo-random image set for the specified project and subtype and runs learning against it.
   *
   * @author Matt Wharton
   */
  private void testLearningOnPseudoRandomImages(Project project, Subtype subtype)
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create the pseudo random image set.
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData pseudoRandomImageSetData = createImageSetAndSetProgramFilters(project,
                                                                               subtype,
                                                                               "testAdvancedGullwingLearningOnPseudoRandomImagesImageSet",
                                                                               POPULATED_BOARD);
    imageSetDataList.add(pseudoRandomImageSetData);
    generateRandomImageSet(project, pseudoRandomImageSetData);

    // Run subtype learning.
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);
  }

  /**
   * Main test entry point for Advanced Gullwing Learning.
   *
   * @author Matt Wharton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    // Initialize the learned settings data structures.
    initializeLearnedSettingsDataStructures();

    // Load up the test project (FAMILIES_ALL_RLV).
    Project project = getProject(_TEST_PROJECT_NAME);

    // Lookup our test Subtype.
    Panel panel = project.getPanel();
    Subtype subtype = panel.getSubtype(_ADVANCED_GULLWING_SUBTYPE_NAME);

    try
    {
      // Delete any pre-existing learned data.
      deleteLearnedData(subtype);

      // Check that the learned settings are at their default values.
      verifyAllSettingsAreAtDefault(subtype, _learnedSettings);

      // Learn one of the Gullwing subtypes.
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                     POPULATED_BOARD);
      imageSetDataList.add(imageSetData);
      learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

      // Verify that the learned values match what we expect.
//      checkLearnedValues(subtype, _learnedSettingToExpectedLearnedValueMap);

      // Check the number of datapoints.
      SliceNameEnum padSlice = SliceNameEnum.PAD;
      final int EXPECTED_NUMBER_OF_DATA_POINTS = 100;
      int actualNumberOfDataPoints = getNumberOfMeasurements(subtype, padSlice, MeasurementEnum.GULLWING_HEEL_THICKNESS);
      Expect.expect(actualNumberOfDataPoints == EXPECTED_NUMBER_OF_DATA_POINTS,
                    "Actual # data points: " + actualNumberOfDataPoints);

      // Test learning on black images.
      testLearningOnBlackImages(project, subtype);

      // Test learning on white images.
      testLearningOnWhiteImages(project, subtype);

      // Test learning on pseudo-random images.
      testLearningOnPseudoRandomImages(project, subtype);

      // Make sure that we can run learning on an unpopulated board.
      testLearningOnlyUnloadedBoards(project, subtype, _learnedSettings);

      // Delete the learned data generated during this test.
      deleteLearnedData(subtype);
    }
    catch (XrayTesterException xtex)
    {
      xtex.printStackTrace();
    }
  }

  /**
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_AdvancedGullwingLearning());
  }
}
