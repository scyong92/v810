package com.axi.v810.business.imageAnalysis.advancedGullwing;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gullwing.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import java.util.*;

/**
 * Advanced Gullwing Misalignment algorithm.  This simply uses the same misalignment technique as
 * standard Gullwing.
 *
 * @author Matt Wharton
 */
public class AdvancedGullwingMisalignmentAlgorithm extends GullwingMisalignmentAlgorithm
{
  /**
   * @author Matt Wharton
   */
  public AdvancedGullwingMisalignmentAlgorithm(InspectionFamily advancedGullwingInspectionFamily)
  {
    super(InspectionFamilyEnum.ADVANCED_GULLWING);

    Assert.expect(advancedGullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(advancedGullwingInspectionFamily);

    // Add the measurement enums.
    addMeasurementEnums();
  }

  /**
   * @author Matt Wharton
   * @author Rick Gaudette
   */
  private void addAlgorithmSettings(InspectionFamily advancedGullwingInspectionFamily)
  {
    int displayOrder = 1;
    int currentVersion = 1;

    // Maximum joint offset from CAD.
    AlgorithmSetting maximumJointOffsetFromCadSetting =
        new AlgorithmSetting(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD,
                             displayOrder++,
                             MathUtil.convertMilsToMillimeters(20.f),
                             MathUtil.convertMilsToMillimeters(0.f),
                             MathUtil.convertMilsToMillimeters(50.f),
                             MeasurementUnitsEnum.MILLIMETERS,
                             "HTML_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_JOINT_OFFSET_FROM_CAD)_KEY", // desc
                             "HTML_DETAILED_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_JOINT_OFFSET_FROM_CAD)_KEY", // detailed desc
                             "IMG_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_JOINT_OFFSET_FROM_CAD)_KEY", // image file
                             AlgorithmSettingTypeEnum.STANDARD,
                             currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maximumJointOffsetFromCadSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maximumJointOffsetFromCadSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maximumJointOffsetFromCadSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_JOINT_OFFSET_FROM_CAD);
    addAlgorithmSetting(maximumJointOffsetFromCadSetting);

    // Maximum heel position distance from nominal.
    AlgorithmSetting maximumHeelPositionDistanceFromNominalSetting =
        new AlgorithmSetting(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL,
                             displayOrder++,
                             20.f,
                             0.f,
                             200.f,
                             MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
                             "HTML_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL)_KEY", // desc
                             "HTML_DETAILED_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL)_KEY", // detailed desc
                             "IMG_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL)_KEY", // image file
                             AlgorithmSettingTypeEnum.HIDDEN,
                             currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          maximumHeelPositionDistanceFromNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          maximumHeelPositionDistanceFromNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          maximumHeelPositionDistanceFromNominalSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL);
    addAlgorithmSetting(maximumHeelPositionDistanceFromNominalSetting);

    // Maximum heel position offset from region neighbors.
    AlgorithmSetting maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting =
        new AlgorithmSetting(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS,
                             displayOrder++,
                             MathUtil.convertMilsToMillimeters(20.0f),
                             0.f,
                             MathUtil.convertMilsToMillimeters(200.0f),
                             MeasurementUnitsEnum.MILLIMETERS,
                             "HTML_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS)_KEY", // desc
                             "HTML_DETAILED_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS)_KEY", // detailed desc
                             "IMG_DESC_GULLWING_MISALIGNMENT_(MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS)_KEY", // image file
                             AlgorithmSettingTypeEnum.STANDARD,
                             currentVersion);
   advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                         maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting,
                                                                         SliceNameEnum.PAD,
                                                                         MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
   advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                         maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting,
                                                                         SliceNameEnum.PAD,
                                                                         MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
   advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                         maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting,
                                                                         SliceNameEnum.PAD,
                                                                         MeasurementEnum.GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS);
    addAlgorithmSetting(maximumHeelPositionDistanceFromExpectedBasedOnRegionNeighborsSetting);

    // Maximum heel to pad center difference from nominal
    AlgorithmSetting heelToPadCenterDifferenceFromNominalSetting =
        new AlgorithmSetting(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL,
                             displayOrder++,
                             MathUtil.convertMilsToMillimeters(20.0f),
                             0.f,
                             MathUtil.convertMilsToMillimeters(200.0f),
                             MeasurementUnitsEnum.MILLIMETERS,
                             "HTML_DESC_ADVANCED_GULLWING_MISALIGNMENT_(HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL)_KEY",
                             "HTML_DETAILED_DESC_ADVANCED_GULLWING_MISALIGNMENT_(HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL)_KEY",
                             "IMG_DESC_ADVANCED_GULLWING_MISALIGNMENT_(HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL)_KEY",
                             AlgorithmSettingTypeEnum.STANDARD,
                             currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          heelToPadCenterDifferenceFromNominalSetting, SliceNameEnum.PAD,
                                                                          MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER);
    addAlgorithmSetting(heelToPadCenterDifferenceFromNominalSetting);
    
    // Maximum mass shift across percent pad length across
    AlgorithmSetting maximumMassShiftAcrossPercent =
        new AlgorithmSetting(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_MASS_SHIFT_ACROSS_PERCENT,
                             displayOrder++,
                             50.0F,
                             0.0F,
                             200.0F,
                             MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ACROSS,
                             "HTML_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_MASS_SHIFT_ACROSS_PERCENT)_KEY",
                             "HTML_DETAILED_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_MASS_SHIFT_ACROSS_PERCENT)_KEY",
                             "IMG_DESC_ADVANCED_GULLWING_MISALIGNMENT_(MAXIMUM_MASS_SHIFT_ACROSS_PERCENT)_KEY",
                             AlgorithmSettingTypeEnum.STANDARD,
                             currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                            maximumMassShiftAcrossPercent, SliceNameEnum.PAD,
                            MeasurementEnum.ADVANCED_GULLWING_MASS_SHIFT_ACROSS);
    addAlgorithmSetting(maximumMassShiftAcrossPercent);
  }

  /**
   * The main entry point for Advanced Gullwing Misalignment.
   *
   * @author Rick Gaudette
   */
  public void classifyJoints(ReconstructedImages reconstructedImages, List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Verify that all JointInspectionDataObjects are the same subtype as the first one in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the Subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    
    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the applicable ReconstructedSlice for the pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Iterate thru each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedPadSlice, false);

      BooleanRef jointPassed = new BooleanRef(true);

      // Run the basic Gullwing misalignment classification.
      basicGullwingClassifyJoint(subtype, padSlice, jointInspectionData, jointInspectionDataObjects, jointPassed, MILIMETER_PER_PIXEL);

      //  Check for the amount of shift along
      detectShiftAlong(subtype, padSlice, jointInspectionData, jointPassed);
      
      //  Check for the amount of shift along
      detectShiftAcross(subtype, padSlice, jointInspectionData, jointPassed);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                      jointInspectionData,
                      inspectionRegion,
                      padSlice,
                      jointPassed.getValue());
      
    }
  }

  /**
   *  Detect misalignment along by comparing the measured heel to pad center to the nominal value
   * 
   * @author Rick Gaudette
   */
  private void detectShiftAlong(Subtype subtype,
                  SliceNameEnum sliceNameEnum,
                  JointInspectionData jointInspectionData,
                  BooleanRef jointPassed) {
      Assert.expect(subtype != null);
      Assert.expect(sliceNameEnum != null);
      Assert.expect(jointInspectionData != null);
      Assert.expect(jointPassed != null);

      // Get the joint inspection result.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      // Get the nominal heel to pad center measurement
      final float NOMINAL_HEEL_TO_PAD_CENTER =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER);
      
      // Get the heel to pad center measurement and computer the difference from nominal
      JointMeasurement heelToPadCenterMeasurement =
        AdvancedGullwingMeasurementAlgorithm.getHeelToPadCenter(jointInspectionData, sliceNameEnum);
      float heelToPadCenterDifferenceFromNominal = Math.abs(heelToPadCenterMeasurement.getValue() - NOMINAL_HEEL_TO_PAD_CENTER);

      // Check if the difference is greater than the limit.
      final float MAX_HEEL_PAD_DIFFERENCE_FROM_NOMINAL =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL);
    
      if (MathUtil.fuzzyGreaterThan(heelToPadCenterDifferenceFromNominal, MAX_HEEL_PAD_DIFFERENCE_FROM_NOMINAL))
      {
        // Indict the joint for open.
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        jointInspectionData.getInspectionRegion(),
                                                        sliceNameEnum,
                                                        heelToPadCenterMeasurement,
                                                        MAX_HEEL_PAD_DIFFERENCE_FROM_NOMINAL);

        // Create an indictment.
        JointIndictment misalignmentIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);

        // Add the failing measurement.
        misalignmentIndictment.addFailingMeasurement(heelToPadCenterMeasurement);

        // Tie the indictment to the joint's results.
        jointInspectionResult.addIndictment(misalignmentIndictment);

        // Flag the joint as failing.
        jointPassed.setValue(false);
      }
      else
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        sliceNameEnum,
                                                        jointInspectionData.getInspectionRegion(),
                                                        heelToPadCenterMeasurement,
                                                        MAX_HEEL_PAD_DIFFERENCE_FROM_NOMINAL);
      }
  }

  /**
   * Detect misalignment across by measuring the center of mass of the across profile
   * @author Rick Gaudette
   */
  private void detectShiftAcross(Subtype subtype,
                  SliceNameEnum sliceNameEnum,
                  JointInspectionData jointInspectionData,
                  BooleanRef jointPassed) {
      Assert.expect(subtype != null);
      Assert.expect(sliceNameEnum != null);
      Assert.expect(jointInspectionData != null);
      Assert.expect(jointPassed != null);
      // Get the joint inspection result.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      // Get the nominal heel to pad center measurement
      final float MAX_MASS_SHIFT_ACROSS_PCT =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_MASS_SHIFT_ACROSS_PERCENT);
      
      // Get the heel to pad center measurement and computer the difference from nominal
      JointMeasurement massShiftAcrossPctMeasurement = 
        AdvancedGullwingMeasurementAlgorithm.getMassShiftAcross(jointInspectionData, sliceNameEnum);

      // Check if the absolute value of the percentage is greater than the limit.
      if (MathUtil.fuzzyGreaterThan(Math.abs(massShiftAcrossPctMeasurement.getValue()), MAX_MASS_SHIFT_ACROSS_PCT))
      {
        // Indict the joint for open.
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        jointInspectionData.getInspectionRegion(),
                                                        sliceNameEnum,
                                                        massShiftAcrossPctMeasurement,
                                                        MAX_MASS_SHIFT_ACROSS_PCT);

        // Create an indictment.
        JointIndictment misalignmentIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);

        // Add the failing measurement.
        misalignmentIndictment.addFailingMeasurement(massShiftAcrossPctMeasurement);

        // Tie the indictment to the joint's results.
        jointInspectionResult.addIndictment(misalignmentIndictment);

        // Flag the joint as failing.
        jointPassed.setValue(false);
      }
      else
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        sliceNameEnum,
                                                        jointInspectionData.getInspectionRegion(),
                                                        massShiftAcrossPctMeasurement,
                                                        MAX_MASS_SHIFT_ACROSS_PCT);
      }

  }
}