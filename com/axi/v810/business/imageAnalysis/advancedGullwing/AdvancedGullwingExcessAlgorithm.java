package com.axi.v810.business.imageAnalysis.advancedGullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
/**
 *
 * @author wei-chin.chong
 */
public class AdvancedGullwingExcessAlgorithm extends Algorithm
{
  /**
   * @param gullwingInspectionFamily
   * @author Wei Chin, Chong
   */
  public AdvancedGullwingExcessAlgorithm(InspectionFamily gullwingInspectionFamily)
  {
    super(AlgorithmEnum.EXCESS, InspectionFamilyEnum.ADVANCED_GULLWING);

    Assert.expect(gullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(gullwingInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @auhtor Wei Chin, Chong
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    _jointMeasurementEnums.add(MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
  }

  /**
   * @param inspectionFamilyEnum
   * @author Wei Chin, Chong
   */
  protected AdvancedGullwingExcessAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.EXCESS, inspectionFamilyEnum);
  }

  /**
   * @param gullwingInspectionFamily
   * @author Wei Chin, Chong
   */
  private void addAlgorithmSettings(InspectionFamily gullwingInspectionFamily)
  {
    Assert.expect(_learnedAlgorithmSettingEnums != null);

    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    // maximum thickness percent of nominal.
    AlgorithmSetting maxFilletThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_EXCESS_MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        200.0f, // default
        0.0f, // min
        400.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GULLWING_EXCESS_(MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_EXCESS_(MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_GULLWING_EXCESS_(MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);

    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                  maxFilletThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                  maxFilletThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL);

    addAlgorithmSetting(maxFilletThicknessPercentOfNominalSetting);


    // maximum Heel thickness percent of nominal.
    AlgorithmSetting maxHeelThicknessPercentOfNominalSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.GULLWING_EXCESS_MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL,
        displayOrder++,
        200.0f, // default
        0.0f, // min
        400.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_GULLWING_EXCESS_(MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // desc
        "HTML_DETAILED_DESC_GULLWING_EXCESS_(MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // detailed desc
        "IMG_DESC_GULLWING_EXCESS_(MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);

    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                  maxHeelThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    gullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                  maxHeelThicknessPercentOfNominalSetting,
                                                                  SliceNameEnum.PAD,
                                                                  MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL);

    addAlgorithmSetting(maxHeelThicknessPercentOfNominalSetting);
   
  }

  /**
   * @author Wei Chin, Chong
   * This method sets all legal algorithms to "disable" when a subtype is created.  
   */
  public boolean algorithmIsEnabledByDefault(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return false;
  }

  /**
   * Runs 'Excess' on all the given joints.
   *
   * @author Wei Chin, Chong
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    // Ensure the all the JointInspectionData objects have the same subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      Assert.expect(jointInspectionData.getSubtype() == subtype);
    }

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Get the reconstructed pad slice.
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    // Check each joint for excess solder.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                inspectionRegion,
                                                reconstructedPadSlice,
                                                false);

      BooleanRef jointPassed = new BooleanRef(true);
      detectExcessSolder(reconstructedPadSlice, jointInspectionData, jointPassed);

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                              jointInspectionData,
                                                              inspectionRegion,
                                                              padSlice,
                                                              jointPassed.getValue());
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  private void detectExcessSolder(ReconstructedSlice reconstructedSlice,
                                  JointInspectionData jointInspectionData,
                                  BooleanRef jointPassed)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(jointPassed != null);

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the SliceNameEnum.
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    // Get the JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    // Calculate tested joint's fillet thickness' percent of the nominal fillet thickness.
    final float NOMINAL_FILLET_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS);
    JointMeasurement filletThicknessMeasurement =
        AdvancedGullwingMeasurementAlgorithm.getFilletThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float filletThickness = filletThicknessMeasurement.getValue();
    float filletThicknessPercentOfNominal = (filletThickness / (float)NOMINAL_FILLET_THICKNESS) * 100f;
    JointMeasurement filletThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             filletThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(filletThicknessPercentOfNominalMeasurement);

    // Check to see if the fillet thickness is within acceptable tolerance of nominal.
    final float MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    if (MathUtil.fuzzyGreaterThan(filletThicknessPercentOfNominal, MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'excess'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment excessIndictment = new JointIndictment(IndictmentEnum.EXCESS, this, sliceNameEnum);

      // Add the failing and related measurements.
      excessIndictment.addFailingMeasurement(filletThicknessPercentOfNominalMeasurement);
      excessIndictment.addRelatedMeasurement(filletThicknessMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(excessIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    }


    // Calculate tested joint's heel thickness' percent of the nominal heel thickness.
    final float NOMINAL_HEEL_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);
    JointMeasurement heelThicknessMeasurement =
        AdvancedGullwingMeasurementAlgorithm.getHeelThicknessMeasurement(jointInspectionData, sliceNameEnum);
    float heelThickness = heelThicknessMeasurement.getValue();
    float heelThicknessPercentOfNominal = (heelThickness / (float)NOMINAL_HEEL_THICKNESS) * 100f;
    JointMeasurement heelThicknessPercentOfNominalMeasurement =
        new JointMeasurement(this,
                             MeasurementEnum.GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL,
                             MeasurementUnitsEnum.PERCENT,
                             jointInspectionData.getPad(),
                             sliceNameEnum,
                             heelThicknessPercentOfNominal);
    jointInspectionResult.addMeasurement(heelThicknessPercentOfNominalMeasurement);

    // Check to see if the fillet thickness is within acceptable tolerance of nominal.
    final float MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_EXCESS_MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    if (MathUtil.fuzzyGreaterThan(heelThicknessPercentOfNominal, MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL))
    {
      // Indict the joint for 'excess'.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      heelThicknessPercentOfNominalMeasurement,
                                                      MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL);

      // Create an indictment.
      JointIndictment heelExcessIndictment = new JointIndictment(IndictmentEnum.EXCESS, this, sliceNameEnum);

      // Add the failing and related measurements.
      heelExcessIndictment.addFailingMeasurement(heelThicknessPercentOfNominalMeasurement);
      heelExcessIndictment.addRelatedMeasurement(heelThicknessMeasurement);

      // Tie the indictment to the joint's results.
      jointInspectionResult.addIndictment(heelExcessIndictment);

      // Mark the joint as failing.
      jointPassed.setValue(false);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      jointInspectionData.getInspectionRegion(),
                                                      heelThicknessPercentOfNominalMeasurement,
                                                      MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL);
    }
    
  }

  /**
   * @author Wei Chin, Chong
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages)
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // We can't learn if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      return;
    }

    /** mdw - Kathy doesn't think we should be learning the excess threshold.
     * Perhaps we can revisit this later (i.e. post 1.0).
     */
    //    // Gullwing Insufficient only runs on the pad slice.
//    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
//
//    // Learn the "insufficient fillet thickness" defect setting.
//    learnExcesstFilletThicknessDefectSetting(subtype, padSlice);
  }

  /**
   * @author Matt Wharton
   */
  private void learnExcesstFilletThicknessDefectSetting(Subtype subtype,
                                                        SliceNameEnum slice) 
  {
    Assert.expect(subtype != null);
    Assert.expect(slice != null);

//    // Pull the learned "fillet thickness" measurement values from the database.
//    float[] filletThicknessLearnedMeasurementValues =
//        subtype.getLearnedJointMeasurementValues(slice,
//                                                 MeasurementEnum.GULLWING_FILLET_THICKNESS,
//                                                 true,
//                                                 true);
//
//    // For each "fillet thickness" measurement, calculate its percent of nominal.
//    float[] filletThicknessPercentOfNominalValues = new float[filletThicknessLearnedMeasurementValues.length];
//    final float NOMINAL_FILLET_THICKNESS =
//        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS);
//    for (int i = 0; i < filletThicknessLearnedMeasurementValues.length; ++i)
//    {
//      filletThicknessPercentOfNominalValues[i] = (filletThicknessLearnedMeasurementValues[i] / NOMINAL_FILLET_THICKNESS) * 100f;
//    }
//
//    // We can only do the IQR statistics if we have at least two data points.
//    if (filletThicknessPercentOfNominalValues.length > 1)
//    {
//      float[] quartileRanges = StatisticsUtil.quartileRanges(filletThicknessPercentOfNominalValues);
//      float medianFilletThicknessPercentOfNominal = quartileRanges[2];
//      float interQuartileRange = quartileRanges[3] - quartileRanges[1];
//      float maximumFilletThicknessPercentOfNominal = medianFilletThicknessPercentOfNominal + (3f * interQuartileRange);
//
//      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.GULLWING_EXCESS_MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
//                              maximumFilletThicknessPercentOfNominal);
//    }
  }
}
