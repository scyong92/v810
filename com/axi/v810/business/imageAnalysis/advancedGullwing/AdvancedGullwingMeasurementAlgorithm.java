package com.axi.v810.business.imageAnalysis.advancedGullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gullwing.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;

/**
 * @author Matt Wharton
 * @author Rick Gaudette
 */
public class AdvancedGullwingMeasurementAlgorithm extends GullwingMeasurementAlgorithm
{
  private static class AcrossProfileLearnedSettings
  {
    private float _sideFilletEdgePercentOfMaxThickness;
    private float _sideFilletHeelPeakSearchDistancePercentOfFilletLength;
    private float _sideFilletCenterOffsetPercentOfFilletLength;
    private float _sideFilletCenterToHeelThicknessPercent;

    /**
     * @author Matt Wharton
     */
    public AcrossProfileLearnedSettings(float sideFilletEdgePercentOfMaxThickness,
                                        float sideFilletHeelPeakSearchDistancePercentOfFilletLength,
                                        float sideFilletCenterOffsetPercentOfFilletLength,
                                        float sideFilletCenterToHeelThicknessPercent)
    {
      Assert.expect((sideFilletEdgePercentOfMaxThickness >= 0f) && (sideFilletEdgePercentOfMaxThickness <= 100.f));
      Assert.expect((sideFilletHeelPeakSearchDistancePercentOfFilletLength >= 0f)
                    && (sideFilletHeelPeakSearchDistancePercentOfFilletLength <= 100.f));
      Assert.expect((sideFilletCenterOffsetPercentOfFilletLength >= 0f)
                    && (sideFilletCenterOffsetPercentOfFilletLength <= 100.f));

      _sideFilletEdgePercentOfMaxThickness = sideFilletEdgePercentOfMaxThickness;
      _sideFilletHeelPeakSearchDistancePercentOfFilletLength = sideFilletHeelPeakSearchDistancePercentOfFilletLength;
      _sideFilletCenterOffsetPercentOfFilletLength = sideFilletCenterOffsetPercentOfFilletLength;
      _sideFilletCenterToHeelThicknessPercent = sideFilletCenterToHeelThicknessPercent;
    }

    /**
     * @author Matt Wharton
     */
    public float getSideFilletEdgePercentOfMaxThickness()
    {
      return _sideFilletEdgePercentOfMaxThickness;
    }

    /**
     * @author Matt Wharton
     */
    public float getSideFilletHeelPeakSearchDistancePercentOfFilletLength()
    {
      return _sideFilletHeelPeakSearchDistancePercentOfFilletLength;
    }

    /**
     * @author Matt Wharton
     */
    public float getSideFilletCenterOffsetPercentOfFilletLength()
    {
      return _sideFilletCenterOffsetPercentOfFilletLength;
    }

    /**
     * @author Matt Wharton
     */
    public float getSideFilletCenterToHeelThicknessPercent()
    {
      return _sideFilletCenterToHeelThicknessPercent;
    }
  }

  private static class SideFilletEdgeEnum extends com.axi.util.Enum
  {
    private static  int _index = 0;

    public static final SideFilletEdgeEnum LEFT_SIDE_FILLET_EDGE = new SideFilletEdgeEnum(_index++);
    public static final SideFilletEdgeEnum RIGHT_SIDE_FILLET_EDGE = new SideFilletEdgeEnum(_index++);

    /**
     * @author Matt Wharton
     */
    private SideFilletEdgeEnum(int id)
    {
      super(id);
    }
  }

  private static AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();

  private static final boolean _DEBUG = false;
  
  private final static int _MIN_NUMBER_OF_SLICE = 1;
  private final static int _MAX_NUMBER_OF_SLICE = 10;
  private final static ArrayList<String> _ADVANCED_GULLWING_NUMBER_OF_SLICE  = new ArrayList();
   
  static
  {
    for(int i = _MIN_NUMBER_OF_SLICE ; i<=_MAX_NUMBER_OF_SLICE; i ++)
      _ADVANCED_GULLWING_NUMBER_OF_SLICE.add(Integer.toString(i));
  }
  
   /**
   * @author Cheah Lee Herng 
   */
  static public List<SliceNameEnum> getAllBarrelSliceEnumList(Subtype subtype)
  {
    Assert.expect(subtype != null);
      
    List<SliceNameEnum> barrelSliceEnumList = new ArrayList();
    int numberOfBarrelSlices = getNumberOfSlice(subtype);

    if (numberOfBarrelSlices >= 1)
    {
      barrelSliceEnumList.add(SliceNameEnum.PAD);
    }
    if (numberOfBarrelSlices >= 2)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_2);
    }
    if (numberOfBarrelSlices >= 3)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_3);
    }
    if (numberOfBarrelSlices >= 4)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_4);
    }
    if (numberOfBarrelSlices >= 5)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_5);
    }
    if (numberOfBarrelSlices >= 6)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_6);
    }
    if (numberOfBarrelSlices >= 7)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_7);
    }
    if (numberOfBarrelSlices >= 8)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_8);
    }
    if (numberOfBarrelSlices >= 9)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_9);
    }
    if (numberOfBarrelSlices >= 10)
    {
      barrelSliceEnumList.add(SliceNameEnum.CONNECTOR_BARREL_10);
    }
    return barrelSliceEnumList;
  }

  /**
   * @author Matt Wharton
   */
  public AdvancedGullwingMeasurementAlgorithm(InspectionFamily advancedGullwingInspectionFamily)
  {
    super(InspectionFamilyEnum.ADVANCED_GULLWING);

    Assert.expect(advancedGullwingInspectionFamily != null);

    // Add the algorithm settings.
    addAlgorithmSettings(advancedGullwingInspectionFamily);

    addMeasurementEnums();
  }

  /**
   * @author Peter Esbensen
   * @author Rick Gaudette
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_MASS_SHIFT_ACROSS);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_2_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());

    super.addMeasurementEnums();
  }


  /**
   * Adds the algorithm settings for Advanced Gullwing Measurement.
   *
   * @author Matt Wharton
   * @author Rick Gaudette
   */
  private void addAlgorithmSettings(InspectionFamily advancedGullwingInspectionFamily)
  {
    Assert.expect(advancedGullwingInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);

    // Add the shared locator settings.
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();
    
    AlgorithmSetting userDefineNumOfSlice = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE, // setting enum
      displayOrder++, // display order,
      new Integer(_MIN_NUMBER_OF_SLICE).toString(),
      _ADVANCED_GULLWING_NUMBER_OF_SLICE,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_SLICE)_KEY", // description URL Key
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_SLICE)_KEY", // detailed description URL Key
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_SLICE)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion);
    addAlgorithmSetting(userDefineNumOfSlice);

    // Indicates whether heel void compensation is enabled or not.
    ArrayList<String> allowableHeelVoidCompensationValues = new ArrayList<String>(Arrays.asList("On", "Off"));
    AlgorithmSetting heelVoidCompensationSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION,
      displayOrder++,
      "Off",
      allowableHeelVoidCompensationValues,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_VOID_COMPENSATION)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(heelVoidCompensationSetting);
    
    // Indicates method to perform profile smoothing
    AlgorithmSetting voidCompensationMethodSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD,
      displayOrder++,
      VOID_COMPENSATION_METHOD_NONE,
      VOID_COMPENSATION_METHODS,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(VOID_COMPENSATION_METHOD)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(VOID_COMPENSATION_METHOD)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(VOID_COMPENSATION_METHOD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(voidCompensationMethodSetting);
    
    // Size of window kernel of Gaussian smoothing
    AlgorithmSetting smoothSensitivitySetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY,
        displayOrder++,
        1, // default
        1, // min
        20, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(SMOOTH_SENSITIVITY)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(SMOOTH_SENSITIVITY)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(SMOOTH_SENSITIVITY)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(smoothSensitivitySetting);

    // Percent of pad width across to use for the primary pad thickness profile.
    AlgorithmSetting padProfileWidthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH,
        displayOrder++,
        45.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_PROFILE_WIDTH)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(padProfileWidthSetting);

    // Percent of the pad length along to use for the primary pad thickness profile.
    AlgorithmSetting padProfileLengthSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH,
      displayOrder++,
      100.f, // default
      0.0f, // min
      200.f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_PROFILE_LENGTH)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(padProfileLengthSetting);

    // Background region location (as percent of IPD).
    AlgorithmSetting backgroundRegionLocation = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION,
      displayOrder++,
      50.f, // default
      0.0f, // min
      200.f, // max
      MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionLocation);
    
    // Background region location offset (as absolute offset distance from edge of pad).
    AlgorithmSetting backgroundRegionLocationOffset = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET,
      displayOrder++,
      0.0f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.MILS,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_LOCATION_OFFSET)_KEY", // image file
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(backgroundRegionLocationOffset);

    // Target heel edge thickess as a percent of max pad profile thickness along.
    AlgorithmSetting heelEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT,
        displayOrder++,
        60.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(heelEdgeSearchThicknessPercentSetting);

    // Heel peak search distance from heel edge.
    AlgorithmSetting heelSearchDistanceFromEdgeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE,
        displayOrder++,
        15.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(HEEL_SEARCH_DISTANCE_FROM_EDGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(heelSearchDistanceFromEdgeSetting);

    // Target toe edge thickness as a percent of max thickness.
    AlgorithmSetting toeEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT,
        displayOrder++,
        35.f, // default
        0.0f, // min
        300.f, // max
        MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(TOE_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(toeEdgeSearchThicknessPercentSetting);

    // Center offset from heel peak (percent of fillet length).
    AlgorithmSetting centerOffsetFromHeelSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_CENTER_OFFSET,
        displayOrder++,
        60.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(CENTER_OFFSET_FROM_HEEL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(centerOffsetFromHeelSetting);

    // Across profile location (percent of fillet length along).
    AlgorithmSetting acrossProfileLocationSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_LOCATION,
      displayOrder++,
      55.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(ACROSS_PROFILE_LOCATION)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(ACROSS_PROFILE_LOCATION)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(ACROSS_PROFILE_LOCATION)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(acrossProfileLocationSetting);

    // Across profile width (percent of fillet length along).
    AlgorithmSetting acrossProfileWidthSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_WIDTH,
      displayOrder++,
      25.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(ACROSS_PROFILE_WIDTH)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(ACROSS_PROFILE_WIDTH)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(ACROSS_PROFILE_WIDTH)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(acrossProfileWidthSetting);

    // Target side fillet edge thickness as percent of max pad profile thickness across.
    AlgorithmSetting sideFilletEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT,
      displayOrder++,
      35.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ACROSS,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_EDGE_THICKNESS_PERCENT)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_EDGE_THICKNESS_PERCENT)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_EDGE_THICKNESS_PERCENT)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(sideFilletEdgeSearchThicknessPercentSetting);

    // Side fillet search distance (as percent of distance between side edges).
    AlgorithmSetting sideFilletSearchDistanceSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE,
      displayOrder++,
      45.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_SIDE_FILLET_LENGTH,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_SEARCH_DISTANCE_FROM_EDGE)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_SEARCH_DISTANCE_FROM_EDGE)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_SEARCH_DISTANCE_FROM_EDGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(sideFilletSearchDistanceSetting);

    // Side fillet center offset from edge (as percent of distance between side fillet edges).
    AlgorithmSetting sideFilletCenterOffsetFromEdgeSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE,
      displayOrder++,
      55.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_SIDE_FILLET_LENGTH,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_CENTER_OFFSET_FROM_EDGE)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_CENTER_OFFSET_FROM_EDGE)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(SIDE_FILLET_CENTER_OFFSET_FROM_EDGE)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(sideFilletCenterOffsetFromEdgeSetting);

    // Nominal heel position.
    // PE:  This has been obsoleted and is now hidden from the user.
    AlgorithmSetting nominalHeelPosition = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION,
      displayOrder++,
      40.f,  // default
      -50.f, // min
      150.f, // max
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_POSITION)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_POSITION)_KEY", // detailed desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_POSITION)_KEY", // image file
      AlgorithmSettingTypeEnum.HIDDEN,
      currentVersion);
    addAlgorithmSetting(nominalHeelPosition);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION);

    // Nominal heel thickness.
    AlgorithmSetting nominalHeelThicknessSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS,
      displayOrder++,
      MathUtil.convertMilsToMillimeters(7.f), // default
      MathUtil.convertMilsToMillimeters(0.1f), // min
      MathUtil.convertMilsToMillimeters(500.0f), // max
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // desc
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // desc
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_THICKNESS)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          nominalHeelThicknessSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_THICKNESS);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          nominalHeelThicknessSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_THICKNESS);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          nominalHeelThicknessSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_HEEL_THICKNESS);
    addAlgorithmSetting(nominalHeelThicknessSetting);

    // Nominal fillet length.
    AlgorithmSetting nominalFilletLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(38.f), // default
        MathUtil.convertMilsToMillimeters(0.1f), // min
        MathUtil.convertMilsToMillimeters(500.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_FILLET_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          nominalFilletLengthSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          nominalFilletLengthSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          nominalFilletLengthSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_LENGTH);
    addAlgorithmSetting(nominalFilletLengthSetting);

    // Nominal average fillet thickness.
    AlgorithmSetting nominalFilletThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4.0f), // default
        MathUtil.convertMilsToMillimeters(0.1f), // min
        MathUtil.convertMilsToMillimeters(500.0f), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_FILLET_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RF_SHIELD,
                                                                          nominalFilletThicknessSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_THICKNESS);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SMALL_OUTLINE_LEADLESS,
                                                                          nominalFilletThicknessSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_THICKNESS);
    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                                                                          nominalFilletThicknessSetting,
                                                                          SliceNameEnum.PAD,
                                                                          MeasurementEnum.GULLWING_FILLET_THICKNESS);
    addAlgorithmSetting(nominalFilletThicknessSetting);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-40.0f), // minimum value
      MathUtil.convertMilsToMillimeters(40.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);

    // Added by Lee Herng (4 Mar 2011)
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
      displayOrder++, // display order,
      MathUtil.convertMilsToMillimeters(0.0f), // default value
      MathUtil.convertMilsToMillimeters(-200.0f), // minimum value
      MathUtil.convertMilsToMillimeters(200.0f), // maximum value
      MeasurementUnitsEnum.MILLIMETERS,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // description URL Key
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed description URL Key
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is right before psh in the Slice Setup tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));

    // Settings to enable background regions for Ventura

    // Enable Background Regions
    ArrayList<String> enableBackgroundRegionsValues = new ArrayList<String>(Arrays.asList("Both", "Right", "Left"));
    AlgorithmSetting enableBackgroundRegionsSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS,
        displayOrder++,
        "Both",
        enableBackgroundRegionsValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(ENABLE_BACKGROUND_REGIONS)_KEY",
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(ENABLE_BACKGROUND_REGIONS)_KEY",
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(ENABLE_BACKGROUND_REGIONS)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(enableBackgroundRegionsSetting);

    // Background Region Width
    AlgorithmSetting backgroundRegionWidthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_WIDTH,
        displayOrder++,
        5.0f, // default value
        5.0f, // minimum value
        100.0f, // maximum value
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_WIDTH)_KEY",
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_WIDTH)_KEY",
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(BACKGROUND_REGION_WIDTH)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(backgroundRegionWidthSetting);
    
    // Pad center reference
    ArrayList<String> padCenterReferenceValues = new ArrayList<String>(Arrays.asList("CAD", "Locator"));
    AlgorithmSetting padCenterReferenceSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_PAD_CENTER_REFERENCE,
        displayOrder++,
        "Locator",
        padCenterReferenceValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_CENTER_REFERENCE)_KEY",
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_CENTER_REFERENCE)_KEY",
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(PAD_CENTER_REFERENCE)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(padCenterReferenceSetting);

    // Nominal heel to pad center.
    AlgorithmSetting nominalHeelToPadCenterSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(20.0F), // default
        MathUtil.convertMilsToMillimeters(-1000.0F), // min
        MathUtil.convertMilsToMillimeters(1000.0F), // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_TO_PAD_CENTER)_KEY",
        "HTML_DETAILED_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_TO_PAD_CENTER)_KEY",
        "IMG_DESC_ADVANCED_GULLWING_MEASUREMENT_(NOMINAL_HEEL_TO_PAD_CENTER)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(nominalHeelToPadCenterSetting);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER);

    advancedGullwingInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.SURFACE_MOUNT_CONNECTOR,
                    nominalHeelToPadCenterSetting,
                    SliceNameEnum.PAD,
                    MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER);
    
    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END

    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * Main Advanced Gullwing Measurement entry point.
   *
   * @author Matt Wharton
   * @author Rick Gaudette
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.isEmpty() == false);

    // Verify that all JointInspectionDataObjects are the same subtype as the first on in the list.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Get the subtype.
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    
    // Advanced Gullwing only runs on the pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);
    ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(padSlice);
    Image padSliceImage = reconstructedPadSlice.getOrthogonalImage();

    //Siew Yeng - XCR-2683 - add enhanced image
    if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
    {
      ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, padSlice);
    }
    // Run Locator.
    Locator.locateJoints(reconstructedImages, padSlice, jointInspectionDataObjects, this, true);

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, padSlice, subtype, this);

    if(_DEBUG)
        System.out.println("JointID,Heel To Pad Center (mm),Mass Shift Across(%)");
    
    //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
    List<Float> validFilletLengthMeasurementsInMils = new ArrayList<Float>();
    List<Float> validCenterThicknessMeasurementsInMils = new ArrayList<Float>();
    
    // Make measurements on each joint.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                jointInspectionData,
                                                inspectionRegion,
                                                reconstructedPadSlice,
                                                false);

      // Get the joint inspection result.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

      // Run the basic gullwing measurement procedures on the joint.
      GullwingUnreportedMeasurementValues unreportedMeasurementValues =
          basicGullwingClassifyJoint(padSliceImage, padSlice, subtype, jointInspectionData, MILIMETER_PER_PIXEL);

      // Now we need to do the Advanced Gullwing specific stuff (profile across, etc).

      // Look up the 'along' pad thickness ROIs and profiles (both full and fractional pad width across).
      RegionOfInterest fullWidthPadProfileRoi = unreportedMeasurementValues.getFullWidthPadProfileRoi();
      float[] padThicknessProfileAlongUsingFullPadLengthAcross =
          unreportedMeasurementValues.getPadThicknessProfileUsingFullPadLengthAcross();
      RegionOfInterest fractionalWidthPadProfileRoi = unreportedMeasurementValues.getFractionalWidthPadProfileRoi();
      float[] padThicknessProfileAlongUsingFractionalPadLengthAcross =
          unreportedMeasurementValues.getPadThicknessProfileUsingFractionalPadLengthAcross();

      // Take the derivative of the full and fractional pad width profiles.
      float[] derivativePadThicknessProfileAlongUsingFullPadLengthAcross =
          AlgorithmUtil.createUnitizedDerivativeProfile(padThicknessProfileAlongUsingFullPadLengthAcross,
                                                        2,
                                                        MagnificationEnum.getCurrentNorminal(),
                                                        MeasurementUnitsEnum.MILLIMETERS);
      derivativePadThicknessProfileAlongUsingFullPadLengthAcross =
          ProfileUtil.getSmoothedProfile(derivativePadThicknessProfileAlongUsingFullPadLengthAcross, _SMOOTHING_KERNEL_LENGTH);
      float[] derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross =
          AlgorithmUtil.createUnitizedDerivativeProfile(padThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                        2,
                                                        MagnificationEnum.getCurrentNorminal(),
                                                        MeasurementUnitsEnum.MILLIMETERS);
      derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross =
          ProfileUtil.getSmoothedProfile(derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross, _SMOOTHING_KERNEL_LENGTH);

      // Find the maximum heel slope.
      float heelEdgeSubpixelBin = unreportedMeasurementValues.getHeelEdgeSubpixelBin();
      int heelEdgeBin = (int)heelEdgeSubpixelBin;
      int heelPeakSearchDistanceInPixels = unreportedMeasurementValues.getHeelPeakSearchDistanceInPixels();
      int heelPeakBin = unreportedMeasurementValues.getHeelPeakBin();
      int centerBin = unreportedMeasurementValues.getCenterBin();
      float toeEdgeSubpixelBin = unreportedMeasurementValues.getToeEdgeSubpixelBin();
      int toeEdgeBin = (int)toeEdgeSubpixelBin;
      int fullPadLengthAcrossProfileMaxThicknessBin = ArrayUtil.maxIndex(padThicknessProfileAlongUsingFullPadLengthAcross);
      int maximumHeelSlopeAlongBin = (int)AlgorithmFeatureUtil.findMaximumHeelSlopeBin(derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                                                  fullPadLengthAcrossProfileMaxThicknessBin,
                                                                                  heelEdgeBin,
                                                                                  toeEdgeBin);
      float maximumHeelSlopeAlong = derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross[maximumHeelSlopeAlongBin];

      // Save the "maximum heel slope" measurement.
      JointMeasurement maximumHeelSlopeMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE,
                               MeasurementUnitsEnum.NONE,
                               jointInspectionData.getPad(),
                               padSlice,
                               maximumHeelSlopeAlong);
      jointInspectionResult.addMeasurement(maximumHeelSlopeMeasurement);

      // Find the minimum toe slope and convert it to "maximum negative" (i.e. multiply it by -1).
      int fractionalPadLengthAcrossProfileMaxThicknessBin = ArrayUtil.maxIndex(padThicknessProfileAlongUsingFractionalPadLengthAcross);
      int minimumToeSlopeAlongBin = (int)AlgorithmFeatureUtil.findMinimumToeSlopeBin(derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                                                fractionalPadLengthAcrossProfileMaxThicknessBin,
                                                                                heelEdgeBin,
                                                                                toeEdgeBin);
      float minimumToeSlopeAlong = derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross[minimumToeSlopeAlongBin];
      float maximumNegativeToeSlopeAlong = -minimumToeSlopeAlong;

      // Save the "maximum toe slope" measurement.
      JointMeasurement maximumToeSlopeMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE,
                               MeasurementUnitsEnum.NONE,
                               jointInspectionData.getPad(),
                               padSlice,
                               maximumNegativeToeSlopeAlong);
      jointInspectionResult.addMeasurement(maximumToeSlopeMeasurement);

      // Sum the maximum heel and toe slope values.
      float sumOfMaxHeelAndToeSlopes = maximumHeelSlopeAlong + maximumNegativeToeSlopeAlong;

      // Save the "slope sum" measurement.
      JointMeasurement sumOfMaxHeelAndToeSlopesMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM,
                               MeasurementUnitsEnum.NONE,
                               jointInspectionData.getPad(),
                               padSlice,
                               sumOfMaxHeelAndToeSlopes);
      jointInspectionResult.addMeasurement(sumOfMaxHeelAndToeSlopesMeasurement);

      // Take the second derivative of the profile along using fractional pad width across.
      float[] secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross =
          AlgorithmUtil.createUnitizedDerivativeProfile(derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                        2,
                                                        MagnificationEnum.getCurrentNorminal(),
                                                        MeasurementUnitsEnum.MILLIMETERS);
      secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross =
          ProfileUtil.getSmoothedProfile(secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                         _SMOOTHING_KERNEL_LENGTH);

      // Measure the area under the second derivative profile along curve.
      float areaInMillimetersUnderSecondDerivativePadThicknessProfileAlong =
          AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross);

      // Save the "second derivative area" measurement.
      JointMeasurement areaUnderSecondDerivativePadThicknessProfileAlongMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES,
                               MeasurementUnitsEnum.NONE,
                               jointInspectionData.getPad(),
                               padSlice,
                               areaInMillimetersUnderSecondDerivativePadThicknessProfileAlong);
      jointInspectionResult.addMeasurement(areaUnderSecondDerivativePadThicknessProfileAlongMeasurement);
      
      float heelToPadCenterValue_pix = calculateHeelToPadCenter_pixels(jointInspectionData, subtype, heelPeakBin);

      float heelToPadCenterValue_mm = heelToPadCenterValue_pix * MILIMETER_PER_PIXEL;
      JointMeasurement heelToPadCenterMeasurement = new JointMeasurement(this,
          MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER,
          MeasurementUnitsEnum.MILLIMETERS,
          jointInspectionData.getPad(),
          padSlice,
          heelToPadCenterValue_mm);
      jointInspectionResult.addMeasurement(heelToPadCenterMeasurement);

      float MILS_PER_MILLIMETER = 39.3700787F;
      float[] profile = padThicknessProfileAlongUsingFullPadLengthAcross;
      float[] firstDerivProfile = derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross;
      String jointName = jointInspectionData.getPad().getComponentAndPadName();
      debug("jointName = " + jointName);
      debug("  profile length = " + profile.length);
      debug("  maxThicknessBin = " + fullPadLengthAcrossProfileMaxThicknessBin +
        " (" + profile[(int)fullPadLengthAcrossProfileMaxThicknessBin] * MILS_PER_MILLIMETER + " mils" + ")");
      debug("  heelEdgeSubpixelBin = " + heelEdgeSubpixelBin +
        " (" + profile[(int)heelEdgeSubpixelBin] * MILS_PER_MILLIMETER + " mils" + ")");
      debug("  toeEdgeSubpixelBin = " + toeEdgeSubpixelBin +
        " (" + profile[(int)toeEdgeSubpixelBin] * MILS_PER_MILLIMETER + " mils" + ")");

      debug("  heelSlopeBin = " + maximumHeelSlopeAlongBin +
        " (" + firstDerivProfile[(int)maximumHeelSlopeAlongBin] + ")");
      debug("  toeSlopeBin = " + minimumToeSlopeAlongBin +
        " (" + firstDerivProfile[(int)minimumToeSlopeAlongBin] + ")");
      debug("  negativeToeSlope = " + maximumNegativeToeSlopeAlong);
      debug("  heelToeSlopeSum = " + sumOfMaxHeelAndToeSlopes);

      debug("  heelPeakSearchDistance = " + heelPeakSearchDistanceInPixels + " pixels");
      debug("  heelPeakBin = " + heelPeakBin +
            " (" + profile[(int)heelPeakBin] * MILS_PER_MILLIMETER + " mils" + ")");
      debug("  heelCornerBin  N/A");
      debug("  toePeakBin     N/A");
      debug("  centerBin      N/A");
      debug("  centerSlope    N/A");

      debug("  sumOfSlopeChanges = " + areaInMillimetersUnderSecondDerivativePadThicknessProfileAlong);

      // Post a diagnostic showing the labeled along profile and the measured values.
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                        inspectionRegion,
                                                        jointInspectionData,
                                                        maximumHeelSlopeMeasurement,
                                                        maximumToeSlopeMeasurement,
                                                        sumOfMaxHeelAndToeSlopesMeasurement,
                                                        areaUnderSecondDerivativePadThicknessProfileAlongMeasurement,
                                                        heelToPadCenterMeasurement);

        Collection<DiagnosticInfo> profileAndFeaturesDiagnosticInfos =
            createMeasurementRegionDiagInfosForProfile(jointInspectionData,
                                                       fractionalWidthPadProfileRoi,
                                                       padThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                       heelEdgeBin,
                                                       heelPeakBin,
                                                       centerBin,
                                                       toeEdgeBin,
                                                       maximumHeelSlopeAlongBin,
                                                       minimumToeSlopeAlongBin);

        ProfileDiagnosticInfo labeledProfileDiagInfo =
            createLabeledPadThicknessProfileDiagInfo(jointInspectionData,
                                                     padThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                     heelEdgeBin,
                                                     heelPeakBin,
                                                     heelPeakSearchDistanceInPixels,
                                                     centerBin,
                                                     toeEdgeBin,
                                                     maximumHeelSlopeAlongBin,
                                                     minimumToeSlopeAlongBin);
        profileAndFeaturesDiagnosticInfos.add(labeledProfileDiagInfo);

        _diagnostics.postDiagnostics(inspectionRegion,
                                     padSlice,
                                     jointInspectionData,
                                     this,
                                     false,
                                     profileAndFeaturesDiagnosticInfos.toArray(new DiagnosticInfo[0]));

        // Post a diagnostic to reset the diagnostic infos but not invalidate them.
        _diagnostics.resetDiagnosticInfosWithoutInvalidating(jointInspectionData.getInspectionRegion(),
                                                             subtype,
                                                             this);
      }

      // Measure the profile across.
      final float ACROSS_PROFILE_LOCATION_AS_FRACTION_OF_FILLET_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_LOCATION) / 100.f;
      final float ACROSS_PROFILE_WIDTH_AS_FRACTION_OF_FILLET_LENGTH_ALONG =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_WIDTH) / 100.f;
      float measuredFilletLengthInPixels = toeEdgeSubpixelBin - heelEdgeSubpixelBin;
      float acrossProfileRoiCenterOffsetFromHeelEdge = measuredFilletLengthInPixels * ACROSS_PROFILE_LOCATION_AS_FRACTION_OF_FILLET_LENGTH_ALONG;
      int acrossProfileRoiCenterOffset = (int)Math.ceil(heelEdgeSubpixelBin + acrossProfileRoiCenterOffsetFromHeelEdge);
      int acrossProfileRoiLengthAcross = (int)Math.ceil(measuredFilletLengthInPixels * ACROSS_PROFILE_WIDTH_AS_FRACTION_OF_FILLET_LENGTH_ALONG);
      Pair<RegionOfInterest, float[]> acrossRoiAndSmoothedThicknessProfile =
          measureSmoothedThicknessProfileAcross(padSliceImage,
                                                inspectionRegion,
                                                padSlice,
                                                subtype,
                                                jointInspectionData,
                                                fullWidthPadProfileRoi,
                                                acrossProfileRoiCenterOffset,
                                                acrossProfileRoiLengthAcross,
                                                true);
      RegionOfInterest acrossProfileRoi = acrossRoiAndSmoothedThicknessProfile.getFirst();
      float[] smoothedThicknessProfileAcross = acrossRoiAndSmoothedThicknessProfile.getSecond();

      // Calculate the center of mass assuming the center of the profile is the origin
      float offset = (float) smoothedThicknessProfileAcross.length / 2.0F;
      float massShiftAcross_pix = AlgorithmFeatureUtil.centerOfMass(smoothedThicknessProfileAcross, offset);

      // Convert the measurement to percentage of the pad width
      int padWidth = jointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAcross();
      float massShiftAcross_pct = massShiftAcross_pix /  padWidth * 100.0F;
      JointMeasurement massShiftAcrossMeasurement = new JointMeasurement(this,
                      MeasurementEnum.ADVANCED_GULLWING_MASS_SHIFT_ACROSS,
                      MeasurementUnitsEnum.PERCENT_OF_PAD_WIDTH_ACROSS,
                      jointInspectionData.getPad(),
                      padSlice,
                      massShiftAcross_pct);

      jointInspectionResult.addMeasurement(massShiftAcrossMeasurement);

      if(_DEBUG)
          System.out.printf("%s,%f,%f\n", jointInspectionData.getFullyQualifiedPadName(), heelToPadCenterValue_mm, massShiftAcross_pct);
      
      // Find the side fillet edges on the across profile.
      Pair<Integer, Integer> sideFilletEdges = findSideFilletEdges(subtype, smoothedThicknessProfileAcross);
      int leftSideFilletEdgeBin = sideFilletEdges.getFirst();
      int rightSideFilletEdgeBin = sideFilletEdges.getSecond();

      // Find the 'dominant edge' and the heel and center thickness of the profile across.
      int sideFilletLengthInPixels = rightSideFilletEdgeBin - leftSideFilletEdgeBin;
      final float SIDE_FILLET_HEEL_SEARCH_DISTANCE_FRACTION =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int sideFilletHeelPeakSearchDistanceInPixels =
          (int)Math.ceil((float)sideFilletLengthInPixels * SIDE_FILLET_HEEL_SEARCH_DISTANCE_FRACTION);
      final float SIDE_FILLET_CENTER_DISTANCE_FRACTION =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE) / 100.f;
      int sideFilletCenterDistanceFromFilletEdgeInPixels =
          (int)Math.ceil((float)sideFilletLengthInPixels * SIDE_FILLET_CENTER_DISTANCE_FRACTION);
      List<Pair<SideFilletEdgeEnum, Pair<Integer, Integer>>> sideFilletEdgeAndHeelAndCenterLocations =
          findSideFilletDominantEdgeAndHeelAndCenterLocations(subtype,
                                                              smoothedThicknessProfileAcross,
                                                              leftSideFilletEdgeBin,
                                                              rightSideFilletEdgeBin,
                                                              sideFilletHeelPeakSearchDistanceInPixels,
                                                              sideFilletCenterDistanceFromFilletEdgeInPixels);
      SideFilletEdgeEnum dominantSideFilletEdge = sideFilletEdgeAndHeelAndCenterLocations.get(0).getFirst();
      SideFilletEdgeEnum recessiveSideFilletEdge = sideFilletEdgeAndHeelAndCenterLocations.get(1).getFirst();
      Pair<Integer, Integer> sideFilletHeelAndCenterLocations = sideFilletEdgeAndHeelAndCenterLocations.get(0).getSecond();
      Pair<Integer, Integer> sideFilletHeel2AndCenterLocations = sideFilletEdgeAndHeelAndCenterLocations.get(1).getSecond();
      int sideFilletHeelPeakBin = sideFilletHeelAndCenterLocations.getFirst();
      int sideFilletCenterBin = sideFilletHeelAndCenterLocations.getSecond();
      int sideFilletHeel2PeakBin = sideFilletHeel2AndCenterLocations.getFirst();
      int sideFilletCenter2Bin = sideFilletHeel2AndCenterLocations.getSecond();
      
      float sideFilletHeelThicknessInMillimeters = smoothedThicknessProfileAcross[sideFilletHeelPeakBin];
      float sideFilletCenterThicknessInMillimeters = smoothedThicknessProfileAcross[sideFilletCenterBin];
      float sideFilletHeel2ThicknessInMillimeters = smoothedThicknessProfileAcross[sideFilletHeel2PeakBin];

      // Save the "across heel thickness" measurement.
      JointMeasurement acrossHeelThicknessMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_THICKNESS,
                               MeasurementUnitsEnum.MILLIMETERS,
                               jointInspectionData.getPad(),
                               padSlice,
                               sideFilletHeelThicknessInMillimeters);
      jointInspectionResult.addMeasurement(acrossHeelThicknessMeasurement);
      
      // Save the "across heel thickness" measurement.
      JointMeasurement acrossHeel2ThicknessMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_2_THICKNESS,
                               MeasurementUnitsEnum.MILLIMETERS,
                               jointInspectionData.getPad(),
                               padSlice,
                               sideFilletHeel2ThicknessInMillimeters);
      jointInspectionResult.addMeasurement(acrossHeel2ThicknessMeasurement);

      // Save the "across center thickness" measurement.
      JointMeasurement acrossCenterThicknessMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_THICKNESS,
                               MeasurementUnitsEnum.MILLIMETERS,
                               jointInspectionData.getPad(),
                               padSlice,
                               sideFilletCenterThicknessInMillimeters);
      jointInspectionResult.addMeasurement(acrossCenterThicknessMeasurement);

      // Calculate the across center:heel thickness ratio.
      float sideFilletCenterToHeelThicknessRatio = 1.f;
      if (MathUtil.fuzzyGreaterThan(sideFilletHeelThicknessInMillimeters, 0f))
      {
        sideFilletCenterToHeelThicknessRatio = sideFilletCenterThicknessInMillimeters / sideFilletHeelThicknessInMillimeters;
      }
      
      // Calculate the across center:heel 2 thickness ratio.
      float sideFilletCenterToHeel2ThicknessRatio = 1.f;
      if (MathUtil.fuzzyGreaterThan(sideFilletHeel2ThicknessInMillimeters, 0f))
      {
        sideFilletCenterToHeel2ThicknessRatio = sideFilletCenterThicknessInMillimeters / sideFilletHeel2ThicknessInMillimeters;
      }

      // Save the "across center:heel thickness ratio" measurement.
      JointMeasurement acrossCenterToHeelThicknessRatioMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT,
                               MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
                               jointInspectionData.getPad(),
                               padSlice,
                               100.0f * sideFilletCenterToHeelThicknessRatio);
      jointInspectionResult.addMeasurement(acrossCenterToHeelThicknessRatioMeasurement);
      
      // Save the "across center:heel 2 thickness ratio" measurement.
      JointMeasurement acrossCenterToHeel2ThicknessRatioMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT,
                               MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
                               jointInspectionData.getPad(),
                               padSlice,
                               100.0f * sideFilletCenterToHeel2ThicknessRatio);
      jointInspectionResult.addMeasurement(acrossCenterToHeel2ThicknessRatioMeasurement);

      // Measure the "concavity ratio" of the across profile.
      float acrossConcavityRatio = measureConcavityRatio(smoothedThicknessProfileAcross);

      // Save the "concavity ratio across" measurement.
      JointMeasurement acrossConcavityRatioMeasurement =
          new JointMeasurement(this,
                               MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO,
                               MeasurementUnitsEnum.NONE,
                               jointInspectionData.getPad(),
                               padSlice,
                               acrossConcavityRatio);
      jointInspectionResult.addMeasurement(acrossConcavityRatioMeasurement);

      // Post a diagnostic showing the labeled across profile and the measured values.
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                        inspectionRegion,
                                                        jointInspectionData,
                                                        acrossCenterToHeelThicknessRatioMeasurement,
                                                        acrossCenterToHeel2ThicknessRatioMeasurement,
                                                        acrossConcavityRatioMeasurement,
                                                        massShiftAcrossMeasurement);
        
        Collection<DiagnosticInfo> acrossProfileAndFeaturesDiagnosticInfos = new LinkedList<DiagnosticInfo>();

        ProfileDiagnosticInfo labeledProfileAcrossDiagInfo =
            createLabeledPadThicknessProfileAcrossDiagInfo(jointInspectionData,
                                                           smoothedThicknessProfileAcross,
                                                           leftSideFilletEdgeBin,
                                                           rightSideFilletEdgeBin,
                                                           dominantSideFilletEdge,
                                                           sideFilletHeelPeakSearchDistanceInPixels,
                                                           sideFilletHeelPeakBin,
                                                           sideFilletCenterBin,
                                                           sideFilletHeel2PeakBin);
        
        acrossProfileAndFeaturesDiagnosticInfos.add(labeledProfileAcrossDiagInfo);

        _diagnostics.postDiagnostics(inspectionRegion,
                                     padSlice,
                                     jointInspectionData,
                                     this,
                                     false,
                                     acrossProfileAndFeaturesDiagnosticInfos.toArray(new DiagnosticInfo[0]));
      }
      
      //Siew Yeng - XCR-3532 - Fillet Length Region Outlier
      JointMeasurement filletLengthMeasurement = GullwingMeasurementAlgorithm.getFilletLengthMeasurement(jointInspectionData, padSlice);
      validFilletLengthMeasurementsInMils.add((float)MathUtil.convertMillimetersToMils(filletLengthMeasurement.getValue()));
      
       JointMeasurement centerThicknessMeasurement = GullwingMeasurementAlgorithm.getCenterThicknessMeasurement(jointInspectionData, padSlice);
      validCenterThicknessMeasurementsInMils.add((float)MathUtil.convertMillimetersToMils(centerThicknessMeasurement.getValue()));
    }
    
    computeRegionOutlierMeasurementsForFilletLength(jointInspectionDataObjects,
                                                    ArrayUtil.convertFloatListToFloatArray(validFilletLengthMeasurementsInMils),
                                                    padSlice);
    computeRegionOutlierMeasurementsForCenterThickness(jointInspectionDataObjects,
                                                        ArrayUtil.convertFloatListToFloatArray(validCenterThicknessMeasurementsInMils),
                                                        padSlice);
  }

  /**
   * Measures a smoothed thickness profile across.
   *
   * @author Matt Wharton
   */
  private Pair<RegionOfInterest, float[]> measureSmoothedThicknessProfileAcross(Image image,
                                                                                ReconstructionRegion inspectionRegion,
                                                                                SliceNameEnum sliceNameEnum,
                                                                                Subtype subtype,
                                                                                JointInspectionData jointInspectionData,
                                                                                RegionOfInterest padProfileRoi,
                                                                                int acrossProfileRoiCenterLocationOffset,
                                                                                int acrossProfileRoiLengthAcross,
                                                                                boolean postProfileRegions) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(padProfileRoi != null);

    boolean profileReversalRequired = _jointTypeToProfileReversalRequiredMap.get(jointInspectionData.getJointTypeEnum());

    // Get the located joint ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

    // Create the ROI.
    RegionOfInterest profileAcrossRoi = new RegionOfInterest(padProfileRoi);
    profileAcrossRoi.setLengthAlong(acrossProfileRoiLengthAcross);
    // Lengthen the region to extend to the edges of the profile along background regions.
    int interPadDistanceInPixels = jointInspectionData.getInterPadDistanceInPixels();
    final float BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION) / 100.f;
    int extendedAcrossProfileLengthAlong =
        (int)Math.ceil(2f * (float)interPadDistanceInPixels * BACKGROUND_PROFILE_OFFSET_FRACTION_OF_IPD) + locatedJointRoi.getLengthAcross();
    
    final float backGroundRegionLocationOffsetInMils =  (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET);
    if(backGroundRegionLocationOffsetInMils>0.0f)
    {
      int nanosPerPixel = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      int backGroundRegionLocationOffsetInPixels = (int)Math.round((float)MathUtil.convertMilsToNanoMetersInteger(backGroundRegionLocationOffsetInMils) /
                                                               (float)nanosPerPixel);;

      extendedAcrossProfileLengthAlong = (int)Math.ceil(((float)locatedJointRoi.getLengthAcross() / 2f) + backGroundRegionLocationOffsetInPixels);
    }
    
    profileAcrossRoi.setLengthAcross(extendedAcrossProfileLengthAlong);
    int distanceToRoiEdge = padProfileRoi.getLengthAlong() / 2;
    if (profileReversalRequired)
    {
      profileAcrossRoi.translateAlongAcross(distanceToRoiEdge - acrossProfileRoiCenterLocationOffset, 0);
    }
    else
    {
      profileAcrossRoi.translateAlongAcross(-distanceToRoiEdge + acrossProfileRoiCenterLocationOffset, 0);
    }
    profileAcrossRoi.setOrientationInDegrees(MathUtil.getDegreesWithin0To359(padProfileRoi.getOrientationInDegrees() + 90));


    // Make sure the ROI fits within the image.
    boolean profileAcrossRoiFitsInImage = AlgorithmUtil.checkRegionBoundaries(profileAcrossRoi,
                                                                              image,
                                                                              jointInspectionData.getInspectionRegion(),
                                                                              "Profile Across ROI");
    if (profileAcrossRoiFitsInImage == false)
    {
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, profileAcrossRoi);
    }

    // Measure the gray level profile.
    float[] grayLevelProfileAcross = ImageFeatureExtraction.profile(image, profileAcrossRoi);

    // Create an estimated background trend profile.
    float[] backgroundTrendProfile = ProfileUtil.getBackgroundTrendProfile(grayLevelProfileAcross);

    // Create a thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] thicknessProfileAcrossInMillimeters =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcross, backgroundTrendProfile, thicknessTable, backgroundSensitivityOffset);

    // Smooth the thickness profile.
    float[] smoothedThicknessProfileAcrossInMillimeters =
        ProfileUtil.getSmoothedProfile(thicknessProfileAcrossInMillimeters, _SMOOTHING_KERNEL_LENGTH);

    // Post a diagnostic showing the profile across ROI.
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      if (postProfileRegions)
      {
        MeasurementRegionDiagnosticInfo profileAcrossRoiDiagInfo =
            new MeasurementRegionDiagnosticInfo(profileAcrossRoi, MeasurementRegionEnum.ADVANCED_GULLWING_PROFILE_ACROSS_REGION);

        _diagnostics.postDiagnostics(inspectionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     profileAcrossRoiDiagInfo);
      }
    }

    return new Pair<RegionOfInterest, float[]>(profileAcrossRoi, smoothedThicknessProfileAcrossInMillimeters);
  }

  /**
   * @param subtype
   * @param smoothedThicknessProfileAcross
   * @return 
   */
  private Pair<Integer, Integer> findSideFilletEdges(Subtype subtype, float[] smoothedThicknessProfileAcross)
  {
    Assert.expect(subtype != null);
    Assert.expect(smoothedThicknessProfileAcross != null);
    Assert.expect(smoothedThicknessProfileAcross.length > 0);

    // Find the fillet edges on each side of the across profile.
    float maxProfileAcrossThickness = ArrayUtil.max(smoothedThicknessProfileAcross);
    final float TARGET_ACROSS_FILLET_EDGE_FRACTION_OF_MAX_THICKNESS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    float targetSideFilletEdgeThickness = maxProfileAcrossThickness * TARGET_ACROSS_FILLET_EDGE_FRACTION_OF_MAX_THICKNESS;
    int leftSideFilletEdgeBin = 0;
    for (int i = 0; i < smoothedThicknessProfileAcross.length; ++i)
    {
      if (MathUtil.fuzzyGreaterThanOrEquals(smoothedThicknessProfileAcross[i], targetSideFilletEdgeThickness))
      {
        leftSideFilletEdgeBin = i;
        break;
      }
    }
    int rightSideFilletEdgeBin = smoothedThicknessProfileAcross.length;
    for (int i = smoothedThicknessProfileAcross.length - 1; i >= 0; --i)
    {
      if (MathUtil.fuzzyGreaterThanOrEquals(smoothedThicknessProfileAcross[i], targetSideFilletEdgeThickness))
      {
        rightSideFilletEdgeBin = i;
        break;
      }
    }
    Assert.expect(leftSideFilletEdgeBin <= rightSideFilletEdgeBin);

    return new Pair<Integer, Integer>(leftSideFilletEdgeBin, rightSideFilletEdgeBin);
  }

  /**
   * Using the specified profile across, measures both the heel and center thickness values with respect to the
   * two profile sides.  The heel/center locations which yield the smallest center:heel thickness
   * ratio is returned along with an enum indicating the "dominant" edge.
   *
   * @author Matt Wharton
   */
  private List< Pair<SideFilletEdgeEnum, Pair<Integer, Integer>> > findSideFilletDominantEdgeAndHeelAndCenterLocations(Subtype subtype,
                                                                                                               float[] smoothedThicknessProfileAcross,
                                                                                                               int leftSideFilletEdgeBin,
                                                                                                               int rightSideFilletEdgeBin,
                                                                                                               int sideFilletHeelPeakSearchDistanceInPixels,
                                                                                                               int sideFilletCenterDistanceFromFilletEdgeInPixels)
  {
    Assert.expect(subtype != null);
    Assert.expect(smoothedThicknessProfileAcross != null);
    Assert.expect(smoothedThicknessProfileAcross.length > 0);

    // Now find the heel and center thickness with respect to both side fillet edges.
    int leftSideHeelPeakSearchStartBin = leftSideFilletEdgeBin;
    int leftSideHeelPeakSearchEndBin = Math.min(leftSideFilletEdgeBin + sideFilletHeelPeakSearchDistanceInPixels + 1,
                                                smoothedThicknessProfileAcross.length);
    int leftSideHeelPeakBin = ArrayUtil.maxIndex(smoothedThicknessProfileAcross,
                                                 leftSideHeelPeakSearchStartBin,
                                                 leftSideHeelPeakSearchEndBin);
    float leftSideHeelPeakThickness = smoothedThicknessProfileAcross[leftSideHeelPeakBin];
    int leftSideCenterBin = leftSideFilletEdgeBin + sideFilletCenterDistanceFromFilletEdgeInPixels;
    float leftSideCenterThickness = smoothedThicknessProfileAcross[leftSideCenterBin];

    int rightSideHeelPeakSearchStartBin = Math.max(0, rightSideFilletEdgeBin - sideFilletHeelPeakSearchDistanceInPixels);
    int rightSideHeelPeakSearchEndBin = rightSideFilletEdgeBin + 1;
    int rightSideHeelPeakBin = ArrayUtil.maxIndex(smoothedThicknessProfileAcross,
                                                  rightSideHeelPeakSearchStartBin,
                                                  rightSideHeelPeakSearchEndBin);
    float rightSideHeelPeakThickness = smoothedThicknessProfileAcross[rightSideHeelPeakBin];
    int rightSideCenterBin = rightSideFilletEdgeBin - sideFilletCenterDistanceFromFilletEdgeInPixels;
    float rightSideCenterThickness = smoothedThicknessProfileAcross[rightSideCenterBin];

    // Now figure out which side's heel and center thickness values gives the best "open signal"
    // (i.e. smallest center:heel thickness ratio).
    float leftSideCenterToHeelThicknessRatio = 1f;
    if (MathUtil.fuzzyGreaterThan(leftSideHeelPeakThickness, 0f))
    {
      leftSideCenterToHeelThicknessRatio = leftSideCenterThickness / leftSideHeelPeakThickness;
    }
    float rightSideCenterToHeelThicknessRatio = 1f;
    if (MathUtil.fuzzyGreaterThan(rightSideHeelPeakThickness, 0f))
    {
      rightSideCenterToHeelThicknessRatio = rightSideCenterThickness / rightSideHeelPeakThickness;
    }
    
    List <Pair<SideFilletEdgeEnum, Pair<Integer, Integer>> > listOfEdgeAndHeelAndCenterLocationsPair =
        new ArrayList< Pair<SideFilletEdgeEnum, Pair<Integer, Integer>>>();
    Pair<SideFilletEdgeEnum, Pair<Integer, Integer>> dominantEdgeAndHeelAndCenterLocationsPair =
        new Pair<SideFilletEdgeEnum, Pair<Integer, Integer>>();
    Pair<SideFilletEdgeEnum, Pair<Integer, Integer>> recessiveEdgeAndHeelAndCenterLocationsPair =
        new Pair<SideFilletEdgeEnum, Pair<Integer, Integer>>();
    if (MathUtil.fuzzyLessThanOrEquals(leftSideCenterToHeelThicknessRatio, rightSideCenterToHeelThicknessRatio))
    {
      dominantEdgeAndHeelAndCenterLocationsPair.setPair(SideFilletEdgeEnum.LEFT_SIDE_FILLET_EDGE,
                                                        new Pair<Integer, Integer>(leftSideHeelPeakBin, leftSideCenterBin));
      recessiveEdgeAndHeelAndCenterLocationsPair.setPair(SideFilletEdgeEnum.RIGHT_SIDE_FILLET_EDGE,
                                                        new Pair<Integer, Integer>(rightSideHeelPeakBin, rightSideCenterBin));
    }
    else
    {
      dominantEdgeAndHeelAndCenterLocationsPair.setPair(SideFilletEdgeEnum.RIGHT_SIDE_FILLET_EDGE,
                                                        new Pair<Integer, Integer>(rightSideHeelPeakBin, rightSideCenterBin));
      recessiveEdgeAndHeelAndCenterLocationsPair.setPair(SideFilletEdgeEnum.LEFT_SIDE_FILLET_EDGE,
                                                        new Pair<Integer, Integer>(leftSideHeelPeakBin, leftSideCenterBin));
    }
    
    listOfEdgeAndHeelAndCenterLocationsPair.add(dominantEdgeAndHeelAndCenterLocationsPair);
    listOfEdgeAndHeelAndCenterLocationsPair.add(recessiveEdgeAndHeelAndCenterLocationsPair);
    return listOfEdgeAndHeelAndCenterLocationsPair;
  }

  /**
   * Calculates the ratio of concave up to concave down bins in the specified profile.
   *
   * @author Matt Wharton
   */
  private float measureConcavityRatio(float[] profile)
  {
    Assert.expect(profile != null);
    Assert.expect(profile.length > 0);

    // Get a second derivative profile.
    float[] derivativeProfile = AlgorithmUtil.createUnitizedDerivativeProfile(profile,
                                                                              2,
                                                                              MagnificationEnum.getCurrentNorminal(),
                                                                              MeasurementUnitsEnum.MILLIMETERS);
    derivativeProfile = ProfileUtil.getSmoothedProfile(derivativeProfile, _SMOOTHING_KERNEL_LENGTH);
    float[] secondDerivativeProfile = AlgorithmUtil.createUnitizedDerivativeProfile(derivativeProfile,
                                                                                    2,
                                                                                    MagnificationEnum.getCurrentNorminal(),
                                                                                    MeasurementUnitsEnum.MILLIMETERS);
    secondDerivativeProfile = ProfileUtil.getSmoothedProfile(secondDerivativeProfile, _SMOOTHING_KERNEL_LENGTH);

    int numberOfConcaveUpBins = 0;
    int numberOfConcaveDownBins = 0;
    for (float secondDerivativeValue : secondDerivativeProfile)
    {
      if (Math.signum(secondDerivativeValue) >= 0)
      {
        ++numberOfConcaveUpBins;
      }
      else
      {
        ++numberOfConcaveDownBins;
      }
    }

    // If there are no concave down bins, just return Float.MAX_VALUE
    float concavityRatio = Float.MAX_VALUE;
    if (numberOfConcaveDownBins > 0)
    {
      concavityRatio = numberOfConcaveUpBins / numberOfConcaveDownBins;
    }

    return concavityRatio;
  }

  /**
   * Creates a Collection of MeasurementRegionDiagnosticInfo objects which indicate the positions of the heel edge,
   * heel peak, center, toe edge, max heel slope, and max toe slope.
   *
   * @author Matt Wharton
   */
  private Collection<DiagnosticInfo> createMeasurementRegionDiagInfosForProfile(JointInspectionData jointInspectionData,
                                                                                RegionOfInterest padProfileRoi,
                                                                                float[] smoothedPadThicknessProfile,
                                                                                int heelEdgeBin,
                                                                                int heelPeakBin,
                                                                                int centerBin,
                                                                                int toeEdgeBin,
                                                                                int maxHeelSlopeBin,
                                                                                int maxToeSlopeBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(padProfileRoi != null);
    Assert.expect(smoothedPadThicknessProfile != null);

    // Get the initial diagnostic info collection from basic gullwing.
    Collection<DiagnosticInfo> measurementRegionDiagnosticInfos =
        super.createMeasurementRegionDiagInfosForProfile(jointInspectionData,
                                                         padProfileRoi,
                                                         smoothedPadThicknessProfile,
                                                         heelEdgeBin,
                                                         heelPeakBin,
                                                         centerBin,
                                                         _UNUSED_BIN,
                                                         toeEdgeBin);

    // Now add the advanced gullwing specific diagnostic infos.
    RegionOfInterest maxHeelSlopeRoi = new RegionOfInterest(padProfileRoi);
    maxHeelSlopeRoi.setLengthAlong(1);
    RegionOfInterest maxToeSlopeRoi = new RegionOfInterest(padProfileRoi);
    maxToeSlopeRoi.setLengthAlong(1);

    // Set the positions of our ROIs based on whether or not a profile reversal is in effect.
    int padProfileRoiLengthAlong = padProfileRoi.getLengthAlong();
    int distanceToRoiEdge = padProfileRoiLengthAlong / 2;
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
    Assert.expect(_jointTypeToProfileReversalRequiredMap.containsKey(jointTypeEnum));
    boolean profileReversalRequired = _jointTypeToProfileReversalRequiredMap.get(jointInspectionData.getJointTypeEnum());
    if (profileReversalRequired)
    {
      maxHeelSlopeRoi.translateAlongAcross(distanceToRoiEdge - maxHeelSlopeBin, 0);
      maxToeSlopeRoi.translateAlongAcross(distanceToRoiEdge - maxToeSlopeBin, 0);
    }
    else
    {
      maxHeelSlopeRoi.translateAlongAcross(-distanceToRoiEdge + maxHeelSlopeBin, 0);
      maxToeSlopeRoi.translateAlongAcross(-distanceToRoiEdge + maxToeSlopeBin, 0);
    }


    if (maxHeelSlopeBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(maxHeelSlopeRoi,
                                                                               MeasurementRegionEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE_REGION));
    }
    if (maxToeSlopeBin != _UNUSED_BIN)
    {
      measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(maxToeSlopeRoi,
                                                                               MeasurementRegionEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE_REGION));
    }

    return measurementRegionDiagnosticInfos;
  }


  /**
   * Creates a 'labeled' version of the specified pad thickness profile that accentuates the
   * various features we measured.
   *
   * @author Matt Wharton
   */
  private ProfileDiagnosticInfo createLabeledPadThicknessProfileDiagInfo(JointInspectionData jointInspectionData,
                                                                         float[] smoothedPadThicknessProfile,
                                                                         int heelEdgeBin,
                                                                         int heelPeakBin,
                                                                         int heelPeakSearchDistanceInPixels,
                                                                         int centerBin,
                                                                         int toeEdgeBin,
                                                                         int maxHeelSlopeBin,
                                                                         int maxToeSlopeBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);

    Subtype subtype = jointInspectionData.getSubtype();
    final float NOMINAL_HEEL_THICKNESS_IN_MILLIS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);
    float profileMin = 0f;
    float profileMax = NOMINAL_HEEL_THICKNESS_IN_MILLIS * 1.5f;
    LocalizedString padThicknessProfileLabelLocalizedString =
        new LocalizedString("ALGDIAG_GULLWING_MEASUREMENT_PAD_THICKNESS_PROFILE_LABEL_KEY",
                            null);
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        createProfileSubrangeToMeasurementRegionEnumMap(smoothedPadThicknessProfile,
                                                        heelEdgeBin,
                                                        heelPeakBin,
                                                        heelPeakSearchDistanceInPixels,
                                                        centerBin,
                                                        toeEdgeBin,
                                                        maxHeelSlopeBin,
                                                        maxToeSlopeBin);
    ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
      ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                       ProfileTypeEnum.SOLDER_PROFILE,
                                                                       padThicknessProfileLabelLocalizedString,
                                                                       profileMin,
                                                                       profileMax,
                                                                       smoothedPadThicknessProfile,
                                                                       profileSubrangeToMeasurementRegionEnumMap,
                                                                       MeasurementUnitsEnum.MILLIMETERS);

    return labeledPadThicknessProfileDiagInfo;
  }

  /**
   * @author Matt Wharton
   */
  private Map<Pair<Integer, Integer>, MeasurementRegionEnum>
      createProfileSubrangeToMeasurementRegionEnumMap(float[] smoothedPadThicknessProfile,
                                                      int heelEdgeBin,
                                                      int heelPeakBin,
                                                      int heelPeakSearchDistanceInPixels,
                                                      int centerBin,
                                                      int toeEdgeBin,
                                                      int maxHeelSlopeBin,
                                                      int maxToeSlopeBin)
  {
    Assert.expect(smoothedPadThicknessProfile != null);

    // Get the initial map based on the 'basic' gullwing values.
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        super.createProfileSubrangeToMeasurementRegionEnumMap(smoothedPadThicknessProfile,
                                                              heelEdgeBin,
                                                              heelPeakBin,
                                                              heelPeakSearchDistanceInPixels,
                                                              centerBin,
                                                              _UNUSED_BIN,
                                                              toeEdgeBin);

    // Update with the advanced gullwing specific locations.
    if (maxHeelSlopeBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(maxHeelSlopeBin, maxHeelSlopeBin + 1),
                                                    MeasurementRegionEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE_REGION);
    }
    if (maxToeSlopeBin != _UNUSED_BIN)
    {
      profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(maxToeSlopeBin, maxToeSlopeBin + 1),
                                                    MeasurementRegionEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE_REGION);
    }

    return profileSubrangeToMeasurementRegionEnumMap;
  }

  /**
   * @author Matt Wharton
   */
  private Collection<DiagnosticInfo> createMeasurementRegionDiagInfosForAcrossProfile(JointInspectionData jointInspectionData,
                                                                                      RegionOfInterest acrossProfileRoi,
                                                                                      float[] smoothedPadThicknessProfileAcross,
                                                                                      int leftSideFilletEdgeBin,
                                                                                      int rightSideFilletEdgeBin,
                                                                                      int sideFilletHeelPeakBin,
                                                                                      int sideFilletCenterBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(acrossProfileRoi != null);
    Assert.expect(smoothedPadThicknessProfileAcross != null);
    Assert.expect((leftSideFilletEdgeBin >= 0) && (leftSideFilletEdgeBin < smoothedPadThicknessProfileAcross.length));
    Assert.expect((rightSideFilletEdgeBin >= 0) && (rightSideFilletEdgeBin < smoothedPadThicknessProfileAcross.length));
    Assert.expect((sideFilletHeelPeakBin >= 0) && (sideFilletHeelPeakBin < smoothedPadThicknessProfileAcross.length));
    Assert.expect((sideFilletCenterBin >= 0) && (sideFilletCenterBin < smoothedPadThicknessProfileAcross.length));

    // Create the ROIs for the areas of interest on the profile.
    int acrossProfileRoiLengthAlong = acrossProfileRoi.getLengthAlong();
    int distanceToRoiEdge = acrossProfileRoiLengthAlong / 2;
    RegionOfInterest leftSideFilletEdgeRoi = new RegionOfInterest(acrossProfileRoi);
    leftSideFilletEdgeRoi.setLengthAlong(1);
    leftSideFilletEdgeRoi.translateAlongAcross(-distanceToRoiEdge + leftSideFilletEdgeBin, 0);
    RegionOfInterest rightSideFilletEdgeRoi = new RegionOfInterest(acrossProfileRoi);
    rightSideFilletEdgeRoi.setLengthAlong(1);
    rightSideFilletEdgeRoi.translateAlongAcross(-distanceToRoiEdge + rightSideFilletEdgeBin, 0);
    RegionOfInterest sideFilletHeelPeakRoi = new RegionOfInterest(acrossProfileRoi);
    sideFilletHeelPeakRoi.setLengthAlong(1);
    sideFilletHeelPeakRoi.translateAlongAcross(-distanceToRoiEdge + sideFilletHeelPeakBin, 0);
    RegionOfInterest sideFilletCenterRoi = new RegionOfInterest(acrossProfileRoi);
    sideFilletCenterRoi.setLengthAlong(1);
    sideFilletCenterRoi.translateAlongAcross(-distanceToRoiEdge + sideFilletCenterBin, 0);

    // Create our MeasurementRegionDiagnosticInfos and add them to our collection.
    Collection<DiagnosticInfo> measurementRegionDiagnosticInfos = new LinkedList<DiagnosticInfo>();
    measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(leftSideFilletEdgeRoi,
                                                                             MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_EDGE_REGION));
    measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(rightSideFilletEdgeRoi,
                                                                             MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_EDGE_REGION));
    measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(sideFilletHeelPeakRoi,
                                                                             MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_HEEL_PEAK_REGION));
    measurementRegionDiagnosticInfos.add(new MeasurementRegionDiagnosticInfo(sideFilletCenterRoi,
                                                                             MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_CENTER_REGION));

    return measurementRegionDiagnosticInfos;
  }

  /**
   * @author Matt Wharton
   */
  private ProfileDiagnosticInfo createLabeledPadThicknessProfileAcrossDiagInfo(JointInspectionData jointInspectionData,
                                                                               float[] smoothedPadThicknessProfileAcross,
                                                                               int leftSideFilletEdgeBin,
                                                                               int rightSideFilletEdgeBin,
                                                                               SideFilletEdgeEnum dominantSideFilletEdge,
                                                                               int sideFilletHeelPeakSearchDistanceInPixels,
                                                                               int sideFilletHeelPeakBin,
                                                                               int sideFilletCenterBin,
                                                                               int sideFilletHeel2PeakBin)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedPadThicknessProfileAcross != null);
    Assert.expect(dominantSideFilletEdge != null);
    Assert.expect(smoothedPadThicknessProfileAcross.length > 0);
    Assert.expect(leftSideFilletEdgeBin <= rightSideFilletEdgeBin);
    Assert.expect((leftSideFilletEdgeBin >= 0) && (leftSideFilletEdgeBin < smoothedPadThicknessProfileAcross.length));
    Assert.expect((rightSideFilletEdgeBin >= 0) && (rightSideFilletEdgeBin < smoothedPadThicknessProfileAcross.length));
    Assert.expect((sideFilletHeelPeakBin >= 0) && (sideFilletHeelPeakBin < smoothedPadThicknessProfileAcross.length));
    Assert.expect((sideFilletHeel2PeakBin >= 0) && (sideFilletHeel2PeakBin < smoothedPadThicknessProfileAcross.length));
    Assert.expect((sideFilletCenterBin >= 0) && (sideFilletCenterBin < smoothedPadThicknessProfileAcross.length));

    // Create a ProfileDiagnosticInfo that accentuates the points of interest in the pad thickness profile across.
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer, Integer>, MeasurementRegionEnum>();
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, smoothedPadThicknessProfileAcross.length),
                                                  MeasurementRegionEnum.ADVANCED_GULLWING_PROFILE_ACROSS_REGION);
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(leftSideFilletEdgeBin, leftSideFilletEdgeBin + 1),
                                                  MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_EDGE_REGION);
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(rightSideFilletEdgeBin, rightSideFilletEdgeBin + 1),
                                                  MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_EDGE_REGION);
    
    int sideFilletHeelPeakSearchStartBin = Math.min(leftSideFilletEdgeBin + 1, smoothedPadThicknessProfileAcross.length - 1);
    int sideFilletHeelPeakSearchEndBin = Math.min(leftSideFilletEdgeBin + sideFilletHeelPeakSearchDistanceInPixels + 1,
                                                    smoothedPadThicknessProfileAcross.length);
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(sideFilletHeelPeakSearchStartBin,
                                                                               sideFilletHeelPeakSearchEndBin),
                                                    MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_HEEL_SEARCH_REGION);
    
    sideFilletHeelPeakSearchStartBin = Math.max(rightSideFilletEdgeBin - sideFilletHeelPeakSearchDistanceInPixels, 0);
    sideFilletHeelPeakSearchEndBin = rightSideFilletEdgeBin;
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(sideFilletHeelPeakSearchStartBin,
                                                                               sideFilletHeelPeakSearchEndBin),
                                                    MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_HEEL_SEARCH_REGION);    
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(sideFilletHeelPeakBin, sideFilletHeelPeakBin + 1),
                                                  MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_HEEL_PEAK_REGION);
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(sideFilletHeel2PeakBin, sideFilletHeel2PeakBin + 1),
                                                  MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_HEEL_PEAK_REGION);
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(sideFilletCenterBin, sideFilletCenterBin + 1),
                                                  MeasurementRegionEnum.ADVANCED_GULLWING_SIDE_FILLET_CENTER_REGION);

    Subtype subtype = jointInspectionData.getSubtype();
    final float NOMINAL_HEEL_THICKNESS_IN_MILLIS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);
    float profileMin = 0f;
    float profileMax = NOMINAL_HEEL_THICKNESS_IN_MILLIS * 1.5f;
    LocalizedString padThicknessProfileAcrossLabelLocalizedString =
        new LocalizedString("ALGDIAG_ADVANCED_GULLWING_MEASUREMENT_PAD_THICKNESS_PROFILE_ACROSS_LABEL_KEY", null);
    ProfileDiagnosticInfo labeledPadThicknessProfileAcrossDiagInfo =
        ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                         ProfileTypeEnum.SOLDER_PROFILE,
                                                                         padThicknessProfileAcrossLabelLocalizedString,
                                                                         profileMin,
                                                                         profileMax,
                                                                         smoothedPadThicknessProfileAcross,
                                                                         profileSubrangeToMeasurementRegionEnumMap,
                                                                         MeasurementUnitsEnum.MILLIMETERS);

    return labeledPadThicknessProfileAcrossDiagInfo;
  }

  /**
   * @return true if the pin length is relevant, false otherwise.
   *
   * @author Matt Wharton
   */
  protected boolean isPinLengthApplicable()
  {
    return false;
  }

  /**
   * @return true if the toe peak location and thickness are relevant, false otherwise.
   *
   * @author Matt Wharton
   */
  protected boolean isToePeakApplicable()
  {
    return false;
  }

  /**
   * Gets the target toe edge thickness.  For advanced Gullwing, this is based on a percentage of max thickness.
   *
   * @author Matt Wharton
   */
  protected float getTargetToeEdgeThickness(JointInspectionData jointInspectionData,
                                            float[] fractionalWidthPadThicknessProfile)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(fractionalWidthPadThicknessProfile != null);

    Subtype subtype = jointInspectionData.getSubtype();
    final float MAX_THICKNESS = ArrayUtil.max(fractionalWidthPadThicknessProfile);
    final float TARGET_TOE_EDGE_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    float targetToeEdgeThickness = MAX_THICKNESS * TARGET_TOE_EDGE_FRACTION;

    return targetToeEdgeThickness;
  }

  /**
   * Locates the center given the specified pad thickness profile, heel edge bin, heel peak bin, and toe edge bin.
   *
   * @author Matt Wharton
   */
  protected int locateCenter(JointInspectionData jointInspectionData,
                             float[] smoothedPadThicknessProfile,
                             float heelEdgeSubpixelBin,
                             int heelPeakBin,
                             float toeEdgeSubpixelBin,
                             final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(smoothedPadThicknessProfile != null);
    Assert.expect(smoothedPadThicknessProfile.length > 0);
    Assert.expect((heelEdgeSubpixelBin >= 0) && (heelEdgeSubpixelBin < smoothedPadThicknessProfile.length));
    Assert.expect((heelPeakBin >= 0) && (heelPeakBin < smoothedPadThicknessProfile.length));
    Assert.expect((toeEdgeSubpixelBin >= 0) && (toeEdgeSubpixelBin < smoothedPadThicknessProfile.length));

    // Figure out the appropriate center offset in pixels.
    Subtype subtype = jointInspectionData.getSubtype();
    final float CENTER_OFFSET_FRACTION =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_CENTER_OFFSET) / 100.f;
    float filletLengthInPixels = toeEdgeSubpixelBin - heelEdgeSubpixelBin;
    int centerOffsetInPixels = (int)Math.ceil((float)filletLengthInPixels * CENTER_OFFSET_FRACTION);

    // Calculate the center bin.
    int centerBin = Math.min((int)heelEdgeSubpixelBin + centerOffsetInPixels, smoothedPadThicknessProfile.length - 1);

    return centerBin;
  }

  /**
   * @author Matt Wharton
   */
  protected float calculateCenterOffsetPercent(JointInspectionData jointInspectionData,
                                               float heelEdgeSubpixelBin,
                                               int heelPeakBin,
                                               int centerBin,
                                               float toeEdgeSubpixelBin)
  {
    Assert.expect(jointInspectionData != null);

    // Calculate the center offset from heel edge as a percent of fillet length.
    float filletLengthInPixels = toeEdgeSubpixelBin - heelEdgeSubpixelBin;
    float centerOffsetPercent = ((float)(centerBin - heelEdgeSubpixelBin) / filletLengthInPixels) * 100.f;

    return centerOffsetPercent;
  }

  /**
   * Calculate the heel to pad center distance in pixels
   * @author Rick Gaudette
   */
  private float calculateHeelToPadCenter_pixels(JointInspectionData jointInspectionData,
                                                Subtype subtype,
                                                int heelPeakBin)
  {
      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
      float heelToPadCenterValue_pix = ((float) locatedJointRoi.getCenterAlong() / 2.0F - heelPeakBin);

      // Check to see if we are referencing from the CAD center
      // FIXME this needs to be verified!!!
      final String PAD_CENTER_REFERENCE =
          (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_PAD_CENTER_REFERENCE);
      if (PAD_CENTER_REFERENCE.equalsIgnoreCase("CAD")) {

          double cadOffsetAlong =  locatedJointRoi.getMinCoordinateAlong()
              - jointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinCoordinateAlong();

          if ((locatedJointRoi.getOrientationInDegrees() == 90) || (locatedJointRoi.getOrientationInDegrees() == 180))
          {
              cadOffsetAlong *= -1.0;
          }
          heelToPadCenterValue_pix += cadOffsetAlong;
      }
      return heelToPadCenterValue_pix;
  }

  /**
   * Gets the maximum heel slope measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getMaximumHeelSlopeMeasurement(JointInspectionData jointInspectionData,
                                                                SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement maxHeelSlopeMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE);

    return maxHeelSlopeMeasurement;
  }

  /**
   * Gets the maximum (negative) toe slope measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getMaximumToeSlopeMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement maxToeSlopeMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE);

    return maxToeSlopeMeasurement;
  }

  /**
   * Gets the "Heel To Pad Center" measurement.
   * @author Rick Gaudette
   */
  public static  JointMeasurement getHeelToPadCenter(JointInspectionData jointInspectionData,
                                                     SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement heelToPadCenterMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER);

    return heelToPadCenterMeasurement;
  }

  /**
   * Gets the "Mass shift Across" measurement.
   * @author Rick Gaudette
   */
  public static  JointMeasurement getMassShiftAcross(JointInspectionData jointInspectionData,
                                                     SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement massShiftAcrossMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_MASS_SHIFT_ACROSS);

    return massShiftAcrossMeasurement;
  }

  /**
   * Gets the "sum of max heel and toe slopes" measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getMaximumHeelAndToeSlopeSum(JointInspectionData jointInspectionData,
                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement maxHeelAndToeSlopesSumMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM);

    return maxHeelAndToeSlopesSumMeasurement;
  }

  /**
   * Gets the "second derivative area" measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getSumOfSlopeChangesMeasurement(JointInspectionData jointInspectionData,
                                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement sumOfSlopeChangesMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES);

    return sumOfSlopeChangesMeasurement;
  }

  /**
   * Gets the "center:heel thickness percent across" measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getCenterToHeelThicknessPercentAcrossMeasurement(JointInspectionData jointInspectionData,
                                                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement centerToHeelThicknessPercentAcrossMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT);

    return centerToHeelThicknessPercentAcrossMeasurement;
  }
  
  /**
   * Gets the "center:heel 2 thickness percent across" measurement.
   *
   * @author Wei Chin
   */
  public static JointMeasurement getCenterToHeel2ThicknessPercentAcrossMeasurement(JointInspectionData jointInspectionData,
                                                                                        SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement centerToHeelThicknessPercentAcrossMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT);

    return centerToHeelThicknessPercentAcrossMeasurement;
  }

  /**
   * Gets the "concavity ratio across" measurement.
   *
   * @author Matt Wharton
   */
  public static JointMeasurement getConcavityRatioAcrossMeasurement(JointInspectionData jointInspectionData,
                                                                    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    // Pull the measurement out of the applicable JointInspectionResult.
    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    JointMeasurement concavityRatioAcrossMeasurement =
      jointInspectionResult.getJointMeasurement(sliceNameEnum, MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO);

    return concavityRatioAcrossMeasurement;
  }

  /**
   * @author Peter Esbensen
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    // Need to bypass direct parent to avoid measurePadThicknessProfilesForLearning(...) being executed twice.
    updateNominalsParentOnly(subtype,typicalBoardImages,unloadedBoardImages);

    if (typicalBoardImages.size() == 0)
      return;

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    // Advanced Gullwing only runs (and learns) on the Pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Measure the profiles (both full and fractional pad length across) for each of the joints.
    Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> alongProfileDataMap =
        measurePadThicknessProfilesForLearning(subtype, typicalBoardImages, padSlice);

    // Learn the "nominal heel and toe thickness" settings.
    learnNominalHeelAndToeThickness(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet length" setting.
    learnNominalFilletLength(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet thickness" setting.
    learnNominalFilletThickness(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "heel to pad center" nominal value for this subtype
    learnNominalHeelToPadCenter(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);
    
    // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
    if (alongProfileDataMap != null)
    {
      for(Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> entry : alongProfileDataMap.entrySet())
      {
        Pair<JointInspectionData, ImageSetData> jointInspectionDataPair = entry.getKey();
        jointInspectionDataPair.getFirst().getJointInspectionResult().clearMeasurements();
        jointInspectionDataPair.getFirst().clearJointInspectionResult();
      }
      
      alongProfileDataMap.clear();
      alongProfileDataMap = null;
    }
  }
  
  /**
   * @author Matt Wharton
   * @author Rick Gaudette
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // We can't learn if there are no typical board images.
    if (typicalBoardImages.size() == 0)
    {
      return;
    }

    
    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    // Advanced Gullwing only runs (and learns) on the Pad slice.
    SliceNameEnum padSlice = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    // Let Locator learn its stuff.
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);

    /** @todo mdw - I'm testing out whether or not we can get away with not learning the heel void compensation setting.
     * Kathy thinks we might be able to.
     */
    //    // Before we measure the pad thickness profiles, we need to learn the heel void compensation setting.
//    learnHeelVoidCompensationSetting(subtype);

    // Measure the profiles (both full and fractional pad length across) for each of the joints.
    Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> alongProfileDataMap =
        measurePadThicknessProfilesForLearning(subtype, typicalBoardImages, padSlice);

    // Learn the basic Gullwing Measurement settings.

    // Learn the "heel peak location" setting.
    learnHeelPeakLocationSetting(subtype, padSlice, alongProfileDataMap);

    // Learn the "nominal heel and toe thickness" settings.
    learnNominalHeelAndToeThickness(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet length" setting.
    learnNominalFilletLength(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);

    // Learn the "nominal fillet thickness" setting.
    learnNominalFilletThickness(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);

    // Measure the "center:heel thickness percents" to enable learning of Open defect settings.
    //Ngie Xing - XCR-2027 Exclude Outlier, Not exclude
    measureCenterToHeelThicknessPercentForLearning(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);

    // Now, we need to learn the Advanced Gullwing specific settings.
    
    // Learn the "heel to pad center" nominal value for this subtype
    //Ngie Xing - XCR-2027 Exclude Outlier, Exclude inside
    learnNominalHeelToPadCenter(subtype, padSlice, alongProfileDataMap, MILIMETER_PER_PIXEL);
    
    // Learn the settings for the "across profile".
    //Ngie Xing - XCR-2027 Exclude Outlier, Exclude inside
    Map<Pair<JointInspectionData, ImageSetData>, float[]> profileAcrossMap = learnProfileAcrossSettings(subtype,
                                                                                                        padSlice,
                                                                                                        alongProfileDataMap,
                                                                                                        typicalBoardImages);

    /*** mdw - as per Kathy, we don't need this for 1.0
     * Siew Yeng - XCR-2139 - Learn the maximum and minimum slope
     * Re-enable this for slope learning
     */
    // Make the necessary measurements needed for the Advanced Gullwing defect algorithm learning.
    if(subtype.isEnabledSlopeSettingsLearning())
      makeAdvancedGullwingMeasurementsForDefectAlgorithmLearning(subtype,
                                                                 padSlice,
                                                                 alongProfileDataMap,
                                                                 profileAcrossMap);

    // Raise a warning if we don't have enough data points.
    int numberOfProfiles = alongProfileDataMap.size();
    final int MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING = 10;
    if (numberOfProfiles < MINIMUM_DATA_POINTS_NEEDED_FOR_REASONABLE_LEARNING)
    {
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfProfiles);
    }
  }


  /**
   * Learn the nominal "heel to pad center" value for this subtype
   * @author Rick Gaudette
   */
  private void learnNominalHeelToPadCenter(Subtype subtype,
                                           SliceNameEnum slice,
                                           Map<Pair<JointInspectionData, ImageSetData>,
                                           GullwingProfileDataForLearning> profileDataMap,
                                           final float MILIMETER_PER_PIXEL)
  throws DatastoreException
  {
      Assert.expect(subtype != null);
      Assert.expect(slice != null);
      for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileDataMap.entrySet())
      {
          Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
          JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
          ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
          GullwingProfileDataForLearning profileData = mapEntry.getValue();
          // Get the located joint ROI.
          RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

          int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();

          // Locate the heel edge and peak.
          float[] padThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
          final float HEEL_EDGE_THICKNESS_FRACTION_OF_MAX =
              (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
          float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
                                                     (float)HEEL_EDGE_THICKNESS_FRACTION_OF_MAX, false);

          final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
              (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
          int heelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                              HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
          int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
                                                                (int)heelEdgeSubpixelBin,
                                                                heelPeakSearchDistanceInPixels);

          float heelToPadCenterValue_pix = calculateHeelToPadCenter_pixels(jointInspectionData,  subtype, heelPeakBin);

          float heelToPadCenterValue_mm = heelToPadCenterValue_pix * MILIMETER_PER_PIXEL;
          
          if(_DEBUG)
              System.out.printf("%s,%f\n", jointInspectionData.getFullyQualifiedPadName(), heelToPadCenterValue_mm);      
          //Ngie Xing - XCR-2027 Exclude Outlier, Not Exclude
          LearnedJointMeasurement heelToPadCenterMeasurement = 
              new LearnedJointMeasurement(jointInspectionData.getPad(),
                  slice,
                  MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER,
                  heelToPadCenterValue_mm,
                  true,
                  true);
          subtype.addLearnedJointMeasurementData(heelToPadCenterMeasurement);

      }

      // Pull all the "heel to pad center" measurements from the database.
      float[] heelToPadCenterMeasurementValues =
          subtype.getLearnedJointMeasurementValues(slice,
              MeasurementEnum.ADVANCED_GULLWING_HEEL_TO_PAD_CENTER,
              true,
              true);

      if (heelToPadCenterMeasurementValues.length > 0)
      {
        // Take the median "fillet length" value and set the nominal to that.
         //Ngie Xing - XCR-2027 Exclude Outlier
        float medianHeelToPadCenter = AlgorithmUtil.medianWithExcludeOutlierDecision(heelToPadCenterMeasurementValues, true);
        subtype.setLearnedValue(subtype.getJointTypeEnum(),
            this,
            AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER,
            medianHeelToPadCenter);
      }


  }

  /**
   * @author Matt Wharton
   */
  private Map<Pair<JointInspectionData, ImageSetData>, float[]>
      learnProfileAcrossSettings(Subtype subtype,
                                 SliceNameEnum sliceNameEnum,
                                 Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> alongProfileDataMap,
                                 ManagedOfflineImageSet typicalBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(alongProfileDataMap != null);
    Assert.expect(typicalBoardImages != null);

    // Get some setting values we'll need later
    final float ALONG_HEEL_EDGE_FRACTION_OF_MAX_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
    final float ACROSS_PROFILE_LOCATION_AS_FRACTION_OF_FILLET_LENGTH_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_LOCATION) / 100.f;
    final float ACROSS_PROFILE_WIDTH_AS_FRACTION_OF_FILLET_LENGTH_ALONG =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_WIDTH) / 100.f;

    // First, let's measure the across profiles.
    Map<Pair<JointInspectionData, ImageSetData>, float[]> acrossProfilesMap =
      new HashMap<Pair<JointInspectionData, ImageSetData>, float[]>();
    ManagedOfflineImageSetIterator imageIterator = typicalBoardImages.iterator();
    int samplingFrequency = Math.max(1, typicalBoardImages.size() / _MAX_NUMBER_OF_REGIONS_FOR_LEARNING);
    imageIterator.setSamplingFrequency(samplingFrequency);

    ReconstructedImages reconstructedImages = null;
    while ((reconstructedImages = imageIterator.getNext()) != null)
    {
      ImageSetData currentImageSetData = imageIterator.getCurrentImageSetData();
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
      Image image = reconstructedSlice.getOrthogonalImage();
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // Get the located joint ROI.
        RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
        int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();

        // Get the along ROIs and profiles.
        GullwingProfileDataForLearning profileData =
            alongProfileDataMap.get(new Pair<JointInspectionData, ImageSetData>(jointInspectionData, currentImageSetData));
        RegionOfInterest alongFullWidthPadProfileRoi = profileData.getFullWidthPadProfileRoi();
        float[] alongPadThicknessProfileUsingFullPadLengthAcross = profileData.getPadThicknessProfileUsingFullPadLengthAcross();
        RegionOfInterest alongFractionalWidthPadProfileRoi = profileData.getFractionalWidthPadProfileRoi();
        float[] alongPadThicknessProfileUsingFractionalPadLengthAcross = profileData.getPadThicknessProfileUsingFractionalPadLengthAcross();

        // Locate the heel and toe edges (along).
        float alongHeelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(alongPadThicknessProfileUsingFullPadLengthAcross,
                                                        ALONG_HEEL_EDGE_FRACTION_OF_MAX_THICKNESS, false);
        int alongHeelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                                 HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
        int alongHeelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(alongPadThicknessProfileUsingFullPadLengthAcross,
                                                                   (int)alongHeelEdgeSubpixelBin,
                                                                   alongHeelPeakSearchDistanceInPixels);
        float targetToeEdgeThickness = getTargetToeEdgeThickness(jointInspectionData,
                                                                 alongPadThicknessProfileUsingFractionalPadLengthAcross);
        float alongToeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(alongPadThicknessProfileUsingFractionalPadLengthAcross,
                                                      targetToeEdgeThickness,
                                                      alongHeelPeakBin);

        // Calculate the fillet length along in pixels.
        float filletLengthAlongInPixels = alongToeEdgeSubpixelBin - alongHeelEdgeSubpixelBin;

        // Measure the profile across.
        float acrossProfileRoiCenterOffsetFromHeelEdge = filletLengthAlongInPixels * ACROSS_PROFILE_LOCATION_AS_FRACTION_OF_FILLET_LENGTH_ALONG;
        int acrossProfileRoiCenterOffset = (int)Math.ceil(alongHeelEdgeSubpixelBin + acrossProfileRoiCenterOffsetFromHeelEdge);
        int acrossProfileLengthAlong = (int)Math.ceil(filletLengthAlongInPixels * ACROSS_PROFILE_WIDTH_AS_FRACTION_OF_FILLET_LENGTH_ALONG);
        Pair<RegionOfInterest, float[]> acrossRoiAndThicknessProfile =
            measureSmoothedThicknessProfileAcross(image,
                                                  inspectionRegion,
                                                  sliceNameEnum,
                                                  subtype,
                                                  jointInspectionData,
                                                  alongFullWidthPadProfileRoi,
                                                  acrossProfileRoiCenterOffset,
                                                  acrossProfileLengthAlong,
                                                  false);
        RegionOfInterest acrossProfileRoi = acrossRoiAndThicknessProfile.getFirst();
        float[] thicknessProfileAcross = acrossRoiAndThicknessProfile.getSecond();

        // Measure the side fillet feature location data.
        measureAcrossProfileSettingsForLearning(jointInspectionData, sliceNameEnum, thicknessProfileAcross);

        // Add the profile to our map.
        acrossProfilesMap.put(new Pair<JointInspectionData, ImageSetData>(jointInspectionData, currentImageSetData),
                              thicknessProfileAcross);
      }

      imageIterator.finishedWithCurrentRegion();
    }

    // Now, let's take some summary statistics and set the applicable algorithm settings.

    // Pull all the learned "side fillet edge thickness percent" measurements from the database.
    float[] sideFilletEdgeThicknessPercentValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.ADVANCED_GULLWING_LEARNING_SIDE_FILLET_EDGE_PERCENT_OF_MAX_THICKNESS,
                                                 true,
                                                 true);
    if (sideFilletEdgeThicknessPercentValues.length > 0)
    {
      // Take the median "side fillet edge thickness percent".
      //Ngie Xing - XCR-2027 Exclude Outlier
      float medianSideFilletEdgeThicknessPercent = AlgorithmUtil.medianWithExcludeOutlierDecision(sideFilletEdgeThicknessPercentValues, false);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT,
                              medianSideFilletEdgeThicknessPercent);
    }

    // Pull all the "side fillet heel peak search distance percent" measurements from the database.
    float[] sideFilletHeelPeakSearchDistancePercentValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.ADVANCED_GULLWING_LEARNING_EDGE_TO_PEAK_DISTANCE,
                                                 true,
                                                 true);
    if (sideFilletHeelPeakSearchDistancePercentValues.length > 0)
    {
      // Find the 95th percentile "side fillet heel peak search distance" value.
      //Ngie Xing - XCR-2027 Exclude Outliers
      float estimatedSideFilletHeelPeakSearchDistance = AlgorithmUtil.percentileWithExcludeOutlierDecision(sideFilletHeelPeakSearchDistancePercentValues, 0.95f, false);
      // Bump up the estimate a little bit to make sure we're searching far enough.
      final float HEEL_EDGE_TO_PEAK_DISTANCE_CORRECTION_MULTIPLIER = 1.1f; // mdw - this is really just a guess.
      estimatedSideFilletHeelPeakSearchDistance *= HEEL_EDGE_TO_PEAK_DISTANCE_CORRECTION_MULTIPLIER;

      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE,
                              estimatedSideFilletHeelPeakSearchDistance);
    }

    // Pull all the "side fillet center offset percent" measurements from the database.
    float[] sideFilletCenterOffsetPercentValues =
        subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                 MeasurementEnum.ADVANCED_GULLWING_LEARNING_CENTER_OFFSET,
                                                 true,
                                                 true);
    if (sideFilletCenterOffsetPercentValues.length > 0)
    {
      // Take the median "side fillet center offset fraction".
      //Ngie Xing - XCR-2027 Exclude Outliers 
      float medianSideFilletCenterOffsetPercent = AlgorithmUtil.medianWithExcludeOutlierDecision(sideFilletCenterOffsetPercentValues, false);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE,
                              medianSideFilletCenterOffsetPercent);
    }

    return acrossProfilesMap;
  }

  /**
   * @author Matt Wharton
   */
  private void measureAcrossProfileSettingsForLearning(JointInspectionData jointInspectionData,
                                                       SliceNameEnum sliceNameEnum,
                                                       float[] acrossProfile) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(acrossProfile != null);

    Subtype subtype = jointInspectionData.getSubtype();

    // Find the max thickness bin in the across profile (and the thickness at that bin).
    int acrossProfileMaxThicknessBin = ArrayUtil.maxIndex(acrossProfile);
    float acrossProfileMaxThickness = Math.max(acrossProfile[acrossProfileMaxThicknessBin], Float.MIN_VALUE);

    // Take the across derivative profile.
    float[] acrossDerivativeProfile = AlgorithmUtil.createUnitizedDerivativeProfile(acrossProfile,
                                                                                    2,
                                                                                    MagnificationEnum.getCurrentNorminal(),
                                                                                    MeasurementUnitsEnum.MILLIMETERS);
    acrossDerivativeProfile = ProfileUtil.getSmoothedProfile(acrossDerivativeProfile, _SMOOTHING_KERNEL_LENGTH);

    // Locate the left side fillet edge (place of max slope).
    int leftSideFilletEdgeSearchStartBin = 0;
    int leftSideFilletEdgeSearchEndBin = acrossProfileMaxThicknessBin;
    int leftSideFilletEdgeBin = ArrayUtil.maxIndex(acrossDerivativeProfile,
                                                   leftSideFilletEdgeSearchStartBin,
                                                   leftSideFilletEdgeSearchEndBin);
    float leftSideFilletEdgeThickness = acrossProfile[leftSideFilletEdgeBin];
    float leftSideFilletEdgePercentOfMaxThickness = (leftSideFilletEdgeThickness / acrossProfileMaxThickness) * 100.f;

    // Locate the right side fillet edge (place of min slope).
    int rightSideFilletEdgeSearchStartBin = Math.min(acrossProfileMaxThicknessBin + 1, acrossProfile.length - 1);
    int rightSideFilletEdgeSearchEndBin = acrossProfile.length;
    int rightSideFilletEdgeBin = ArrayUtil.minIndex(acrossDerivativeProfile,
                                                    rightSideFilletEdgeSearchStartBin,
                                                    rightSideFilletEdgeSearchEndBin);
    float rightSideFilletEdgeThickness = acrossProfile[rightSideFilletEdgeBin];
    float rightSideFilletEdgePercentOfMaxThickness = (rightSideFilletEdgeThickness / acrossProfileMaxThickness) * 100.f;

    // Calculate the side fillet length (in pixels).
    int sideFilletLengthInPixels = Math.max(rightSideFilletEdgeBin - leftSideFilletEdgeBin, 1);

    // Now we want to learn the side fillet edge thickness percent of max and
    // the distance from fillet edge to side heel peak.
    // To do this, we'll take the minimum of the distance between the left side fillet edge and the max thickness
    // bin and the distance between the right side fillet edge and the max thickness bin and use whatever side
    // edge is closer to the max thickness.
    float sideFilletEdgePercentOfMaxThickness = 0f;
    float sideFilletHeelPeakSearchDistanceAsPercentOfFilletLength = 0f;
    int leftSideEdgeToMaxThicknessBinDistance = Math.abs(acrossProfileMaxThicknessBin - leftSideFilletEdgeBin);
    int rightSideEdgeToMaxThicknessBinDistance = Math.abs(rightSideFilletEdgeBin - acrossProfileMaxThicknessBin);
    if (leftSideEdgeToMaxThicknessBinDistance <= rightSideEdgeToMaxThicknessBinDistance)
    {
      sideFilletEdgePercentOfMaxThickness = leftSideFilletEdgePercentOfMaxThickness;
      sideFilletHeelPeakSearchDistanceAsPercentOfFilletLength =
          ((float)leftSideEdgeToMaxThicknessBinDistance / (float)sideFilletLengthInPixels) * 100.f;
    }
    else
    {
      sideFilletEdgePercentOfMaxThickness = rightSideFilletEdgePercentOfMaxThickness;
      sideFilletHeelPeakSearchDistanceAsPercentOfFilletLength =
          ((float)rightSideEdgeToMaxThicknessBinDistance / (float)sideFilletLengthInPixels) * 100.f;
    }

    // Find the midpoint between the two side fillet edges.  We'll assume this to be our center.
    int centerBin = (int)Math.round((float)leftSideFilletEdgeBin + ((float)sideFilletLengthInPixels / 2f));

    // Determine whether we should be relative to the left or right side and set the center offset
    // accordingly.
    float sideFilletCenterOffsetAsPercentOfFilletLength = 0f;
    int leftSideEdgeToCenterBinDistance = centerBin - leftSideFilletEdgeBin;
    int rightSideEdgeToCenterBinDistance = rightSideFilletEdgeBin - centerBin;
    if (leftSideEdgeToMaxThicknessBinDistance <= rightSideEdgeToMaxThicknessBinDistance)
    {
      sideFilletCenterOffsetAsPercentOfFilletLength =
          ((float)leftSideEdgeToCenterBinDistance / (float)sideFilletLengthInPixels) * 100.f;
    }
    else
    {
      sideFilletCenterOffsetAsPercentOfFilletLength =
          ((float)rightSideEdgeToCenterBinDistance / (float)sideFilletLengthInPixels) * 100.f;
    }

    // Save our measured side fillet location data to the learning database.
    subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       MeasurementEnum.ADVANCED_GULLWING_LEARNING_SIDE_FILLET_EDGE_PERCENT_OF_MAX_THICKNESS,
                                                                       sideFilletEdgePercentOfMaxThickness,
                                                                       true,
                                                                       true));
    subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       MeasurementEnum.ADVANCED_GULLWING_LEARNING_EDGE_TO_PEAK_DISTANCE,
                                                                       sideFilletHeelPeakSearchDistanceAsPercentOfFilletLength,
                                                                       true,
                                                                       true));
    subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                       sliceNameEnum,
                                                                       MeasurementEnum.ADVANCED_GULLWING_LEARNING_CENTER_OFFSET,
                                                                       sideFilletCenterOffsetAsPercentOfFilletLength,
                                                                       true,
                                                                       true));
  }

  /**
   * Makes the measurements necessary to enable the Advanced Gullwing defect algorithms to learn their stuff.
   *
   * @author Matt Wharton
   */
  private void makeAdvancedGullwingMeasurementsForDefectAlgorithmLearning(
      Subtype subtype,
      SliceNameEnum sliceNameEnum,
      Map<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> profileAlongDataMap,
      Map<Pair<JointInspectionData, ImageSetData>, float[]> profileAcrossMap) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(profileAlongDataMap != null);
    Assert.expect(profileAcrossMap != null);

    // Get the heel and toe edge along location settings.
    final float ALONG_HEEL_EDGE_FRACTION_OF_MAX_THICKNESS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
    final float ALONG_TOE_EDGE_FRACTION =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;

    for (Map.Entry<Pair<JointInspectionData, ImageSetData>, GullwingProfileDataForLearning> mapEntry : profileAlongDataMap.entrySet())
    {
      // Get the JointInspectionData and the thickness profiles along.
      Pair<JointInspectionData, ImageSetData> jointAndImageSetDataPair = mapEntry.getKey();
      JointInspectionData jointInspectionData = jointAndImageSetDataPair.getFirst();
      ImageSetData imageSetData = jointAndImageSetDataPair.getSecond();
      GullwingProfileDataForLearning profileAlongData = mapEntry.getValue();
      float[] padThicknessProfileAlongUsingFullPadLengthAcross =
          profileAlongData.getPadThicknessProfileUsingFullPadLengthAcross();
      float[] padThicknessProfileAlongUsingFractionalPadLengthAcross =
          profileAlongData.getPadThicknessProfileUsingFractionalPadLengthAcross();

      // Get the derivative thickness profiles along.
      float[] derivativePadThicknessProfileAlongUsingFullPadLengthAcross =
          profileAlongData.getDerivativePadThicknessProfileUsingFullPadLengthAcross();
      float[] derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross =
          profileAlongData.getDerivativePadThicknessProfileUsingFractionalPadLengthAcross();

      // Get the located joint ROI.
      RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
      int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();

      // Locate the heel and toe edges (along).
      float alongHeelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileAlongUsingFullPadLengthAcross,
                                                      ALONG_HEEL_EDGE_FRACTION_OF_MAX_THICKNESS, false);
      int alongHeelPeakSearchDistanceInPixels = (int)Math.ceil((float)locatedJointRoiLengthAlong *
                                                               HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
      int alongHeelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileAlongUsingFullPadLengthAcross,
                                                                 (int)alongHeelEdgeSubpixelBin,
                                                                 alongHeelPeakSearchDistanceInPixels);
      final float MAX_THICKNESS = ArrayUtil.max(padThicknessProfileAlongUsingFractionalPadLengthAcross);
      float targetToeEdgeThickness = (float)(MAX_THICKNESS * ALONG_TOE_EDGE_FRACTION);
      float alongToeEdgeSubpixelBin = AlgorithmFeatureUtil.locateToeEdgeUsingThickness(padThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                    targetToeEdgeThickness,
                                                    alongHeelPeakBin);

      // Find the maximum heel slope and save the measurement to the learning database.
      int fullPadLengthAcrossProfileMaxThicknessBin = ArrayUtil.maxIndex(padThicknessProfileAlongUsingFullPadLengthAcross);
      int maximumHeelSlopeAlongBin =
          (int)AlgorithmFeatureUtil.findMaximumHeelSlopeBin(derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                       fullPadLengthAcrossProfileMaxThicknessBin,
                                                       (int)alongHeelEdgeSubpixelBin,
                                                       (int)alongToeEdgeSubpixelBin);
      float maximumHeelSlopeAlong = derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross[maximumHeelSlopeAlongBin];
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_SLOPE,
                                                                         maximumHeelSlopeAlong,
                                                                         true,
                                                                         true));

      // Find the minimum toe slope and convert it to "maximum negative" (i.e. multiply it by -1).
      // Save this measurement to the learning database.
      int fractionalPadLengthAcrossProfileMaxThicknessBin = ArrayUtil.maxIndex(padThicknessProfileAlongUsingFractionalPadLengthAcross);
      int minimumToeSlopeAlongBin =
          (int)AlgorithmFeatureUtil.findMinimumToeSlopeBin(derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                      fractionalPadLengthAcrossProfileMaxThicknessBin,
                                                      (int)alongHeelEdgeSubpixelBin,
                                                      (int)alongToeEdgeSubpixelBin);
      float minimumToeSlopeAlong = derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross[minimumToeSlopeAlongBin];
      float maximumNegativeToeSlopeAlong = -minimumToeSlopeAlong;
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.ADVANCED_GULLWING_MAX_TOE_SLOPE,
                                                                         maximumNegativeToeSlopeAlong,
                                                                         true,
                                                                         true));

      // Sum the maximum heel and toe slope values and save this value to the learning database.
      float sumOfMaxHeelAndToeSlopes = maximumHeelSlopeAlong + maximumNegativeToeSlopeAlong;
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM,
                                                                         sumOfMaxHeelAndToeSlopes,
                                                                         true,
                                                                         true));

      // Take the second derivative of the profile along using fractional pad width across.
      float[] secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross =
          AlgorithmUtil.createUnitizedDerivativeProfile(derivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                                        2,
                                                        MagnificationEnum.getCurrentNorminal(),
                                                        MeasurementUnitsEnum.MILLIMETERS);
      secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross =
          ProfileUtil.getSmoothedProfile(secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross,
                                         _SMOOTHING_KERNEL_LENGTH);

      // Measure the area under the second derivative profile along curve and save it to the learning database.
      float areaInMillimetersUnderSecondDerivativePadThicknessProfileAlong =
          AlgorithmUtil.measureAreaUnderProfileCurveInMillimeters(secondDerivativePadThicknessProfileAlongUsingFractionalPadLengthAcross);
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES,
                                                                         areaInMillimetersUnderSecondDerivativePadThicknessProfileAlong,
                                                                         true,
                                                                         true));

      //Siew Yeng - XCR-2139 
      // This function is never called before i re-enabled for slope learning
      // Comment out this part because i only need slope learning from upper part of this function
      /* 
      // Find the side fillet edges on the across profile.
      float[] profileAcross = profileAcrossMap.get(jointAndImageSetDataPair);
      Pair<Integer, Integer> sideFilletEdges = findSideFilletEdges(subtype, profileAcross);
      int leftSideFilletEdgeBin = sideFilletEdges.getFirst();
      int rightSideFilletEdgeBin = sideFilletEdges.getSecond();

      // Find the 'dominant edge' and the heel and center thickness of the profile across.
      int sideFilletLengthInPixels = rightSideFilletEdgeBin - leftSideFilletEdgeBin;
      final float SIDE_FILLET_HEEL_SEARCH_DISTANCE_FRACTION =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
      int sideFilletHeelPeakSearchDistanceInPixels =
          (int)Math.ceil((float)sideFilletLengthInPixels * SIDE_FILLET_HEEL_SEARCH_DISTANCE_FRACTION);
      final float SIDE_FILLET_CENTER_DISTANCE_FRACTION =
          (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE) / 100.f;
      int sideFilletCenterDistanceFromFilletEdgeInPixels =
          (int)Math.ceil((float)sideFilletLengthInPixels * SIDE_FILLET_CENTER_DISTANCE_FRACTION);
      
      List<Pair<SideFilletEdgeEnum, Pair<Integer, Integer>>> sideFilletDominantEdgeAndHeelAndCenterLocations =
          findSideFilletDominantEdgeAndHeelAndCenterLocations(subtype,
                                                              profileAcross,
                                                              leftSideFilletEdgeBin,
                                                              rightSideFilletEdgeBin,
                                                              sideFilletHeelPeakSearchDistanceInPixels,
                                                              sideFilletCenterDistanceFromFilletEdgeInPixels);
      SideFilletEdgeEnum dominantSideFilletEdge = sideFilletDominantEdgeAndHeelAndCenterLocations.get(0).getFirst();
      Pair<Integer, Integer> sideFilletHeelAndCenterLocations = sideFilletDominantEdgeAndHeelAndCenterLocations.get(0).getSecond();
      int sideFilletHeelPeakBin = sideFilletHeelAndCenterLocations.getFirst();
      int sideFilletCenterBin = sideFilletHeelAndCenterLocations.getSecond();
      float sideFilletHeelThicknessInMillimeters = profileAcross[sideFilletHeelPeakBin];
      float sideFilletCenterThicknessInMillimeters = profileAcross[sideFilletCenterBin];

      // Calculate the across center:heel thickness percent and save it to the learning database.
      float acrossCenterToHeelThicknessPercent = Float.MAX_VALUE;
      if (MathUtil.fuzzyGreaterThan(sideFilletHeelThicknessInMillimeters, 0f))
      {
        acrossCenterToHeelThicknessPercent = (sideFilletCenterThicknessInMillimeters / sideFilletHeelThicknessInMillimeters) * 100.f;
      }
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT,
                                                                         acrossCenterToHeelThicknessPercent,
                                                                         true,
                                                                         true));

      // Measure the across profile "concavity ratio" and save it to the learning database.
      float acrossConcavityRatio = measureConcavityRatio(profileAcross);
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(jointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO,
                                                                         acrossConcavityRatio,
                                                                         true,
                                                                         true));
      */
    }
  }

  /**
   * Restores all Advanced Gullwing Measurement learned settings back to their defaults.
   *
   * @author Matt Wharton
   * @author Rick Gaudette
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    //Ngie Xing, Delete Locator learning.
    Locator.setLearnedSettingsToDefaults(subtype);

    // Reset all the basic Gullwing Measurement specific settings back to their defaults.
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_CENTER_OFFSET);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION);

    // Reset all the Advanced Gullwing Measurement specific settings back to their defaults.
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER);

  }

  private void debug(String message)
  {
    if (false)
      System.out.println("SMT Measurement: " + message);
  }
}
