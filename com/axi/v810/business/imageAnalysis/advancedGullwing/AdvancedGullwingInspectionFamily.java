package com.axi.v810.business.imageAnalysis.advancedGullwing;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * This InspectionFamily is used to test 'Connector' joint types.
 *
 * @author Matt Wharton
 */
public class AdvancedGullwingInspectionFamily extends InspectionFamily
{
  /**
   * @author Matt Wharton
   */
  public AdvancedGullwingInspectionFamily()
  {
    super(InspectionFamilyEnum.ADVANCED_GULLWING);

    // Associate the individual algorithms with the family.

    // Measurement
    Algorithm advancedGullwingMeasurementAlgorithm = new AdvancedGullwingMeasurementAlgorithm(this);
    addAlgorithm(advancedGullwingMeasurementAlgorithm);

    // Short
    Algorithm advancedGullwingShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.ADVANCED_GULLWING);
    addAlgorithm(advancedGullwingShortAlgorithm);

    // Open
    Algorithm advancedGullwingOpenAlgorithm = new AdvancedGullwingOpenAlgorithm(this);
    addAlgorithm(advancedGullwingOpenAlgorithm);

    // Insufficient
    Algorithm advancedGullwingInsufficientAlgorithm = new AdvancedGullwingInsufficientAlgorithm(this);
    addAlgorithm(advancedGullwingInsufficientAlgorithm);

    // Excess
    Algorithm advancedGullwingExcessAlgorithm = new AdvancedGullwingExcessAlgorithm(this);
    addAlgorithm(advancedGullwingExcessAlgorithm);

    // Misalignment.
    Algorithm advancedGullwingMisalignmentAlgorithm = new AdvancedGullwingMisalignmentAlgorithm(this);
    addAlgorithm(advancedGullwingMisalignmentAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);
    
    Collection<SliceNameEnum> inspectedSlices = new LinkedList();
    
    inspectedSlices.addAll(AdvancedGullwingMeasurementAlgorithm.getAllBarrelSliceEnumList(subtype));
        
    //inspectedSlices.add(SliceNameEnum.PAD);

    return inspectedSlices;      
    //return Collections.singletonList(SliceNameEnum.PAD);
  }

  /**
   * Gets the applicable default SliceNameEnum for the Pad slice for the Connector inspection family.
   *
   * @author Matt Wharton
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Use the standard pad slice.
    return SliceNameEnum.PAD;
  }

  /**
   * @author Matt Wharton
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }

  /**
  * @author Peter Esbensen
  */
 public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
 {
   Assert.expect(subtype != null);
   return getDefaultPadSliceNameEnum(subtype);
 }

}
