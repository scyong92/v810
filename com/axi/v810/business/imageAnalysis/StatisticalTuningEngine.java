package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.util.*;

/**
 * This is the engine for Statistical Tuning.  Statistical Tuning automatically sets
 * AlgorithmSettings based on inspection runs of typical and (optionally) unloaded
 * panels.
 * @author Sunit Bhalla
 * @author George Booth (added Expected Image learning support)
 */
public class StatisticalTuningEngine
{
  private static StatisticalTuningEngine _statisticalTuningEngine = null;

  private boolean _shouldCancelLearning;
  private boolean _shouldCancelDeletingLearnedData;
  private boolean _interactiveLearning = false;

  // Keeps track of whether or not the Short learning status has been synced.
  private boolean _shortLearningStatusSynced = false;
  // Keeps track of whether or not the Expected Image learning status has been synced.
  private boolean _expectedImageLearningStatusSynced = false;
  //Broken Pin
  // Keeps track of whether or not the Short learning status has been synced.
  private boolean _brokenPinLearningStatusSynced = false;  
  //ShengChuan - Clear Tombstone
  // Keeps track of whether or not the Expected Image template learning status has been synced.
  private boolean _expectedImageTemplateLearningStatusSynced = false;

  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();


  /**
   * @author Sunit Bhalla
   */
 public static synchronized StatisticalTuningEngine getInstance()
 {
   if (_statisticalTuningEngine == null)
   {
     _statisticalTuningEngine = new StatisticalTuningEngine();
   }
   return _statisticalTuningEngine;
 }

  /**
   * @author Sunit Bhalla
   */
  private StatisticalTuningEngine()
  {
    _shouldCancelLearning = false;
    _shouldCancelDeletingLearnedData = false;

  }

  /**
   * @author George Booth
   */
  public void enableInteractiveLearning(boolean interactiveLearning)
  {
    _interactiveLearning = interactiveLearning;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized boolean isLearningCancelled()
  {
    // This function will only return 'true' once in a row -- so use the value it returns!
    if (_shouldCancelLearning)
    {
      _shouldCancelLearning = false;
      return true;
    }
    return false;
  }

  /**
   * @author Sunit Bhalla
   */
  public synchronized boolean isDeletingCancelled()
  {
    // This function will only return 'true' once in a row -- so use the value it returns!
    if (_shouldCancelDeletingLearnedData)
    {
      _shouldCancelDeletingLearnedData = false;
      return true;
    }
    return false;
  }

  /**
   * Called by the GUI to stop a learning in-progress.
   *
   * @author Patrick Lacz
   */
  public synchronized void cancelLearning()
  {
    _shouldCancelLearning = true;
  }

  /**
   * Called by the GUI to cancel the cancel request.
   *
   * This is needed if only one subtype is being learned - the
   * isLearningCancelled() method is not called until the next learning
   * cycle causing the flag to be rest too late.
   *
   * @author George Booth
   */
  public synchronized void uncancelLearning()
  {
    _shouldCancelLearning = false;
  }

  /**
   * Called by the GUI to stop "deleting learned data" in-progress.
   *
   * @author Sunit Bhalla
   */
  public synchronized void cancelDeletingLearnedData()
  {
    _shouldCancelDeletingLearnedData = true;
  }

  /**
   * @author Sunit Bhalla
   * This method is ONLY used for debugging learning issues.  By default, no diagnostics are drawn during inital
   * tuning.  Calling this method will enable diagnostics to be drawn.
   * Add a call to this method in learnSubtype after the clearPanelInspectionSettings() call.
   *
   * When running the TDE, diagnostics must first be drawn in the fine tuning interface before they can be drawn
   * in the initial tuning screen.  (Otherwise, an exception in removeRenderers() will occur.)
   */
  private void turnOnDiagnosticsForInitialTuning()
  {
    PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
    panelInspectionSettings.clearAllInspectionSettings();
    panelInspectionSettings.setAllJointTypesAllAlgorithmsDiagnosticsEnabled(true);
  }

  /**
   * Frees any JointInspectionResults and ComponentInspectionResults that were generated as part of learning.
   *
   * @author Matt Wharton
   */
  private void freeInspectionResultsGeneratedDuringLearning(TestProgram testProgram, Subtype subtype)
  {
    Assert.expect(testProgram != null);
    Assert.expect(subtype != null);

    for (Pad pad : subtype.getPads())
    {
      if (pad.isInspected())
      {
        JointInspectionData jointInspectionData = testProgram.getJointInspectionData(pad);
        jointInspectionData.clearJointInspectionResult();

        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        componentInspectionData.clearComponentInspectionResult();
      }
    }
  }

  /**
   * A seventh parameter was added to learnSubtype() to handle expected images. This method
   * preserves the old method signature for all the existing algorithm regression tests.
   *
   * @author George Booth
   * @author - ShengChuan - Clear Tombstone - add learnTemplateData
   */
  public void learnSubtype(Project project,
                           Subtype subtype,
                           List<ImageSetData> imageSetDataList,
                           boolean learnAlgorithmSettings,
                           boolean learnJointSpecificData,
                           boolean learnTemplateData,
                           boolean learningMultipleSubtypes) throws XrayTesterException
  {
    learnSubtype(project, subtype, null, imageSetDataList, learnAlgorithmSettings, false,
                 learnJointSpecificData, learnTemplateData, false, learningMultipleSubtypes, false);
  }

  /**
   * @author Kee Chin Seong
   *  - Applying all boards' short profile by using first board only.
   * @Edited by Siew Yeng - added learnSlopeSettings
   * @author - ShengChuan - Clear Tombstone - add learnTemplateData
   */
  public void learnSubtype(Project project,
                           Subtype subtype,
                           Board board,
                           Board learnedBoard,
                           List<ImageSetData> imageSetDataList,
                           boolean learnAlgorithmSettings,
                           boolean learnSlopeSettings,
                           boolean learnJointSpecificData,
                           boolean learnTemplateData,
                           boolean learnExpectedImageData,
                           boolean learningMultipleSubtypes,
                           boolean learnExcludeLibrarySubtype,
                           boolean isApplyBoardToBoardLearning) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);

    if(learnExcludeLibrarySubtype && subtype.isImportedPackageLibrary())
    {
      learnAlgorithmSettings = false;
      learnExpectedImageData = false;
    }

    TestProgram testProgram = project.getTestProgram();
    TimerUtil timer = new TimerUtil();

    boolean printLearningStatus = false;

    if (printLearningStatus)
    {
      System.out.print("Subtype " + subtype.getShortName() + " (" + subtype.getJointTypeEnum().getName() + ") ");
    }

    timer.start();

    // Make sure that the Short learning status is up to date.
    syncShortLearningStatus(project);
    // Make sure that the Expected Image learning status is up to date.
    syncExpectedImageLearningStatus(project);
    //Broken Pin
    // Make sure that the BrokenPin learning status is up to date.
    syncBrokenPinLearningStatus(project);
    //ShengChuan - Clear Tombstone
    // Make sure that the Expected Image template learning status is up to date.
    syncExpectedImageTemplateLearningStatus(project);

    // Make sure that we have at least one image run
    Assert.expect(imageSetDataList.size() > 0);

    // Set filter to look for subtype--this will be used in ManagedOfflineImageSet
    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(subtype);
    
    if (board != null)
      testProgram.addFilter(board);

    // clear out all the measurements, etc.
    testProgram.startNewInspectionRun();
    clearPanelInspectionSettings();

    // uncomment this line to enable diagnostics for learning
    //turnOnDiagnosticsForInitialTuning();

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter(); //Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear tombstone

    try
    {
      // if this subtype uses interactive learning and it is enabled,
      // display the Interactive Learning dialog start screen while the learning
      // database is opened.
      if (subtype.isInteractiveLearningAllowed() && _interactiveLearning)
      {
        InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

        InteractiveLearningStartedInspectionEvent inspectionEvent =
            new InteractiveLearningStartedInspectionEvent(InspectionEventEnum.INTERACTIVE_LEARNING_STARTED);
        // no data for this event
        _inspectionEventObservable.sendEventInfo(inspectionEvent);
      }

      // Connect to the algorithm learning database if needed.
      if (learningMultipleSubtypes == false)
      {
        openConnectionToSubtypeLearning(algorithmSubtypeLearning);
      }
      if (learningMultipleSubtypes == false)
      {
        openConnectionToShortsLearningDatabase(testProgram, algorithmShortsLearning);
      }
      if (learningMultipleSubtypes == false)
      {
        openConnectionToExpectedImageLearningDatabase(testProgram, algorithmExpectedImageLearning);
      }
      //Broken Pin
      if (learningMultipleSubtypes == false)
      {
        openConnectionToBrokenPinLearningDatabase(testProgram, algorithmBrokenPinLearning);
      }
      //ShengChuan - Clear Tombstone
      if (learningMultipleSubtypes == false && learnTemplateData)
      {
        openConnectionToExpectedImageTemplateLearningDatabase(testProgram, algorithmExpectedImageTemplateLearning);
      }

      Collection<ImageSetData> typicalImageSetDataCollection = new ArrayList<ImageSetData>();
      Collection<ImageSetData> unloadedImageSetDataCollection = new ArrayList<ImageSetData>();

      for (ImageSetData imageSetData : imageSetDataList)
      {
        if (imageSetData.isBoardPopulated())
          typicalImageSetDataCollection.add(imageSetData);
        else
          unloadedImageSetDataCollection.add(imageSetData);
      }

      ManagedOfflineImageSet typicalPanelsManagedOfflineImageSet = new ManagedOfflineImageSet(testProgram, typicalImageSetDataCollection);
      ManagedOfflineImageSet unloadedPanelsManagedOfflineImageSet = new ManagedOfflineImageSet(testProgram, unloadedImageSetDataCollection);


      if (learningMultipleSubtypes == false)
          sendInitialTuningStartMessage(1);
        
      sendSubtypeLearningStartedMessage(subtype);

      if ((typicalPanelsManagedOfflineImageSet.size() == 0) && (unloadedPanelsManagedOfflineImageSet.size() == 0))
      {
        AlgorithmUtil.raiseNoImagesForInitialTuningWarning(subtype);
      }
      else
      {
        // ok , if user want to apply all board then use this learning.
        if(isApplyBoardToBoardLearning == true)
        {
          subtype.enableInteractiveLearning(_interactiveLearning);
          subtype.applyLearningToSubtype(learnedBoard,
                                         typicalPanelsManagedOfflineImageSet,
                                         unloadedPanelsManagedOfflineImageSet,
                                         learnAlgorithmSettings,
                                         learnJointSpecificData,
                                         learnTemplateData,
                                         learnExpectedImageData);
        }
        else
        {
          subtype.enableInteractiveLearning(_interactiveLearning);
          
          //Siew Yeng XCR2139 - learn slope settings
          if(learnAlgorithmSettings && learnSlopeSettings)
            subtype.setEnabledLearnSlopeSettings(learnSlopeSettings);
          
          subtype.learn(typicalPanelsManagedOfflineImageSet,
                        unloadedPanelsManagedOfflineImageSet,
                        learnAlgorithmSettings,
                        learnJointSpecificData,
                        learnTemplateData,
                        learnExpectedImageData);
        }
        
        // Free any inspection results that were generated during learning.
        freeInspectionResultsGeneratedDuringLearning(testProgram, subtype);
      }

      timer.stop();

      typicalPanelsManagedOfflineImageSet.cleanUp();
      unloadedPanelsManagedOfflineImageSet.cleanUp();

      sendSubtypeLearningCompletedMessage();

      if (learningMultipleSubtypes == false)
        sendInitialTuningCompletedMessage();

      if (printLearningStatus)
      {
        System.out.println("learned: " + timer.getElapsedTimeInMillis() / 1000.0);
      }

    }
    finally
    {
      if (learningMultipleSubtypes == false)
      {
        //ShengChuan - Clear Tombstone
        if (learnTemplateData)
        {
          finishUpTemplateLearning(algorithmExpectedImageTemplateLearning);
        }
        //Broken Pin & ShengChuan - Clear Tombstone
        finishUpLearning(algorithmSubtypeLearning, algorithmShortsLearning, 
          algorithmExpectedImageLearning, algorithmBrokenPinLearning);
      }
      // if this subtype uses interactive learning and it is enabled,
      // hide the Interactive Learning dialog
      if (subtype.isInteractiveLearningAllowed() && _interactiveLearning)
      {
        InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

        InteractiveLearningCompletedInspectionEvent inspectionEvent =
            new InteractiveLearningCompletedInspectionEvent(InspectionEventEnum.INTERACTIVE_LEARNING_COMPLETED);
        // no data for this event
        _inspectionEventObservable.sendEventInfo(inspectionEvent);
      }
      testProgram.clearFilters(); //XCR1498, Chnee Khang Wah, 2012-09-14
    }
  }
  
  /**
   * @param learningMultipleSubtypes should be set to true if this is being called by learnPanel or learnJointType and
   * the begin/end procedures like opening and closing the file handlers and the GUI learning beginning and end notifications
   * are being managed by those routines.
   *
   * @author Sunit Bhalla
   * @author Matt Wharton
   * @author George Booth
   * @Edited by Siew Yeng - added learnSlopeSettings
   * @author - ShengChuan - Clear Tombstone - add learnTemplateData
   */
  public void learnSubtype(Project project,
                           Subtype subtype,
                           Board board,
                           List<ImageSetData> imageSetDataList,
                           boolean learnAlgorithmSettings,
                           boolean learnSlopeSettings,
                           boolean learnJointSpecificData,
                           boolean learnTemplateData,
                           boolean learnExpectedImageData,
                           boolean learningMultipleSubtypes,
                           boolean learnExcludeLibrarySubtype) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);

    if(learnExcludeLibrarySubtype && subtype.isImportedPackageLibrary())
    {
      learnAlgorithmSettings = false;
      learnExpectedImageData = false;
    }

    TestProgram testProgram = project.getTestProgram();
    TimerUtil timer = new TimerUtil();

    boolean printLearningStatus = false;

    if (printLearningStatus)
    {
      System.out.print("Subtype " + subtype.getShortName() + " (" + subtype.getJointTypeEnum().getName() + ") ");
    }

    timer.start();

    // Make sure that the Short learning status is up to date.
    syncShortLearningStatus(project);
    // Make sure that the Expected Image learning status is up to date.
    syncExpectedImageLearningStatus(project);
    //Broken Pin
    // Make sure that the Broken Pin learning status is up to date.
    syncBrokenPinLearningStatus(project);
    //ShengChuan - Clear Tombstone
    // Make sure that the Expected Image template learning status is up to date.
    syncExpectedImageTemplateLearningStatus(project);

    // Make sure that we have at least one image run
    Assert.expect(imageSetDataList.size() > 0);

    // Set filter to look for subtype--this will be used in ManagedOfflineImageSet
    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(subtype);
    
    if (board != null)
      testProgram.addFilter(board);

    // clear out all the measurements, etc.
    testProgram.startNewInspectionRun();
    clearPanelInspectionSettings();

    // uncomment this line to enable diagnostics for learning
    //turnOnDiagnosticsForInitialTuning();

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter(); //Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear Tombstone

    try
    {
      // if this subtype uses interactive learning and it is enabled,
      // display the Interactive Learning dialog start screen while the learning
      // database is opened.
      if (subtype.isInteractiveLearningAllowed() && _interactiveLearning)
      {
        InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

        InteractiveLearningStartedInspectionEvent inspectionEvent =
            new InteractiveLearningStartedInspectionEvent(InspectionEventEnum.INTERACTIVE_LEARNING_STARTED);
        // no data for this event
        _inspectionEventObservable.sendEventInfo(inspectionEvent);
      }

      // Connect to the algorithm learning database if needed.
      if (learningMultipleSubtypes == false)
      {
        openConnectionToSubtypeLearning(algorithmSubtypeLearning);
      }
      if (learningMultipleSubtypes == false)
      {
        openConnectionToShortsLearningDatabase(testProgram, algorithmShortsLearning);
      }
      if (learningMultipleSubtypes == false)
      {
        openConnectionToExpectedImageLearningDatabase(testProgram, algorithmExpectedImageLearning);
      }
      //Broken Pin
      if (learningMultipleSubtypes == false)
      {
        openConnectionToBrokenPinLearningDatabase(testProgram, algorithmBrokenPinLearning);
      }
      //ShengChuan - Clear Tombstone
      if (learningMultipleSubtypes == false && learnTemplateData)
      {
        openConnectionToExpectedImageTemplateLearningDatabase(testProgram, algorithmExpectedImageTemplateLearning);
      }

      Collection<ImageSetData> typicalImageSetDataCollection = new ArrayList<ImageSetData>();
      Collection<ImageSetData> unloadedImageSetDataCollection = new ArrayList<ImageSetData>();

      for (ImageSetData imageSetData : imageSetDataList)
      {
        if (imageSetData.isBoardPopulated())
          typicalImageSetDataCollection.add(imageSetData);
        else
          unloadedImageSetDataCollection.add(imageSetData);
      }

      ManagedOfflineImageSet typicalPanelsManagedOfflineImageSet = new ManagedOfflineImageSet(testProgram, typicalImageSetDataCollection);
      ManagedOfflineImageSet unloadedPanelsManagedOfflineImageSet = new ManagedOfflineImageSet(testProgram, unloadedImageSetDataCollection);


      if (learningMultipleSubtypes == false)
        sendInitialTuningStartMessage(1);

      sendSubtypeLearningStartedMessage(subtype);

      if ((typicalPanelsManagedOfflineImageSet.size() == 0) && (unloadedPanelsManagedOfflineImageSet.size() == 0))
      {
        AlgorithmUtil.raiseNoImagesForInitialTuningWarning(subtype);
      }
      else
      {
        subtype.enableInteractiveLearning(_interactiveLearning);
        //Siew Yeng XCR-2139 - learn slope settings
        if(learnAlgorithmSettings && learnSlopeSettings)
          subtype.setEnabledLearnSlopeSettings(learnSlopeSettings);
        
        subtype.learn(typicalPanelsManagedOfflineImageSet,
                      unloadedPanelsManagedOfflineImageSet,
                      learnAlgorithmSettings,
                      learnJointSpecificData,
                      learnTemplateData,
                      learnExpectedImageData);

        // Free any inspection results that were generated during learning.
        freeInspectionResultsGeneratedDuringLearning(testProgram, subtype);
      }

      timer.stop();

      typicalPanelsManagedOfflineImageSet.cleanUp();
      unloadedPanelsManagedOfflineImageSet.cleanUp();

      sendSubtypeLearningCompletedMessage();

      if (learningMultipleSubtypes == false)
        sendInitialTuningCompletedMessage();

      if (printLearningStatus)
      {
        System.out.println("learned: " + timer.getElapsedTimeInMillis() / 1000.0);
      }

    }
    finally
    {
      if (learningMultipleSubtypes == false)
      {
        //ShengChuan - Clear Tombstone
        if (learnTemplateData)
        {
          finishUpTemplateLearning(algorithmExpectedImageTemplateLearning);
        }
        //Broken Pin & ShengChuan - Clear Tombstone
        finishUpLearning(algorithmSubtypeLearning, algorithmShortsLearning, algorithmExpectedImageLearning,
          algorithmBrokenPinLearning);
      }
      // if this subtype uses interactive learning and it is enabled,
      // hide the Interactive Learning dialog
      if (subtype.isInteractiveLearningAllowed() && _interactiveLearning)
      {
        InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

        InteractiveLearningCompletedInspectionEvent inspectionEvent =
            new InteractiveLearningCompletedInspectionEvent(InspectionEventEnum.INTERACTIVE_LEARNING_COMPLETED);
        // no data for this event
        _inspectionEventObservable.sendEventInfo(inspectionEvent);
      }
      testProgram.clearFilters(); //XCR1498, Chnee Khang Wah, 2012-09-14
    }
  }

  /**
   * @author Yong Sheng Chuan
   */
  public void learnSubtypeExposureSetting(Map<Subtype, List<StageSpeedEnum>> subtypeToExposureSettingMap) throws DatastoreException
  {
    Assert.expect(subtypeToExposureSettingMap != null);
    
    sendInitialTuningStartMessage(subtypeToExposureSettingMap.size());
    for (Map.Entry<Subtype, List<StageSpeedEnum>> entry : subtypeToExposureSettingMap.entrySet())
    {
      if (isLearningCancelled())
        break;
      
      Subtype subtype = entry.getKey();
      sendSubtypeLearningStartedMessage(subtype);
      subtype.getSubtypeAdvanceSettings().setStageSpeedEnum(entry.getValue());
      subtype.getSubtypeAdvanceSettings().setIsAutoExposureSettingLearnt(true);
      Collection<String> speedString = new ArrayList<String>();
      for (StageSpeedEnum speedEnum : subtype.getSubtypeAdvanceSettings().getStageSpeedList())
      {
        speedString.add(speedEnum.toString());
      }
      String stageSpeedString = StringUtil.join(speedString, ',');
      
      LocalizedString messageText = new LocalizedString("ALGDIAG_EXPOSURE_LEARNING_MESSAGE_KEY",
                                                      new Object[] { subtype.getLongName(),
                                                                     stageSpeedString });
      InspectionEvent inspectionEvent = new AlgorithmMessageInspectionEvent(messageText);
      InspectionEventObservable.getInstance().sendEventInfo(inspectionEvent);
      
      sendSubtypeLearningCompletedMessage();
    }
    sendInitialTuningCompletedMessage();
  }
  
  /**
   * A fifth parameter was added to learnPanel() to handle expected images. This method
   * preserves the old method signature for all the existing algorithm regression tests.
   *
   * @author George Booth
   * @author ShengChuan - Clear Tombstone - Add learnTemplateData
   */
  public void learnPanel(Project project,
                         List<ImageSetData> imageSetDataList,
                         boolean learnAlgorithmSettings,
                         boolean learnJointSpecificData,
                         boolean learnTemplateData) throws XrayTesterException
  {
    learnPanel(project,imageSetDataList, learnAlgorithmSettings, false, learnJointSpecificData, learnTemplateData, false, false);
  }

  /**
   * @author Sunit Bhalla
   * @author Matt Wharton
   * @author George Booth
   * @Edited by Siew Yeng - added learnSlopeSettings
   * @author ShengChuan - Clear Tombstone - Add learnTemplateData
   */
  public void learnPanel(Project project,
                         List<ImageSetData> imageSetDataList,
                         boolean learnAlgorithmSettings,
                         boolean learnSlopeSettings,
                         boolean learnJointSpecificData,
                         boolean learnTemplateData,
                         boolean learnExpectedImageData,
                         boolean learnExcludeLibrarySubtype) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(imageSetDataList != null);

    learn(project, null, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, learnExcludeLibrarySubtype);
  }
  
  /**
   * @author Cheah Lee Herng 
   * @Edited by Siew Yeng - added learnSlopeSettings
   * @author ShengChuan - Clear Tombstone - Add learnTemplateData
   */
  public void learnBoard(Project project,
                         Board board,
                         List<ImageSetData> imageSetDataList,
                         boolean learnAlgorithmSettings,
                         boolean learnSlopeSettings,
                         boolean learnJointSpecificData,
                         boolean learnTemplateData,
                         boolean learnExpectedImageData,
                         boolean learnExcludeLibrarySubtype) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(board != null);
    Assert.expect(imageSetDataList != null);

    learn(project, board, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, learnExcludeLibrarySubtype);
  }
  
  /*
   * @author Kee Chin Seong
   * @Edited by Siew Yeng - added learnSlopeSettings
   * @author ShengChuan - Clear Tombstone - Add learnTemplateData
   */
  public void applyLearnedBoardProfileToAllBoard(Project project,
                         Board learnedBoard,
                         Subtype subtype, 
                         JointTypeEnum jointType,
                         List<ImageSetData> imageSetDataList,
                         boolean learnAlgorithmSettings,
                         boolean learnSlopeSettings,
                         boolean learnJointSpecificData,
                         boolean learnTemplateData,
                         boolean learnExpectedImageData,
                         boolean learnExcludeLibrarySubtype) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(learnedBoard != null);
    Assert.expect(imageSetDataList != null);

    for(Board board : project.getPanel().getBoards())
    {
      // XCR-3150 System crash when learn short to all board
      if (isLearningCancelled())
          break;

      if(board.equals(learnedBoard) == false)
      {
        sendApplyLearnedBoardShortProfileToBoard(learnedBoard, board);
        if(subtype != null)
        {
          for(Subtype unlearnedSubtype : board.getSubtypes())
          {
            if(unlearnedSubtype.getShortName().matches(subtype.getShortName()))
            {
              //to do :: only one subtype learned
              //learn(project, learnedBoard, board, unlearnedSubtype, null, imageSetDataList, learnAlgorithmSettings, learnJointSpecificData, learnExpectedImageData, learnExcludeLibrarySubtype, true);
              sendInitialTuningStartMessage(1);
              learn(project, learnedBoard, board, unlearnedSubtype, null, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, learnExcludeLibrarySubtype, true);
              break;
            }
          }
        }
        else if(jointType != null)
        {
          learn(project, learnedBoard, board, null, jointType, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, learnExcludeLibrarySubtype, true);
        }
        else
        {
          learn(project, learnedBoard, board, null, null, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, learnExcludeLibrarySubtype, true);
        }
      }
    }
  }
  
  /**
   * @author Kee Chin Seong
   *  - Applying all boards' short profile by using first board only.
   * @Edited by Siew Yeng - added learnSlopeSettings
   * @author ShengChuan - Clear Tombstone - Add learnTemplateData
   */
  private void learn(Project project,
                     Board learnedBoard,
                     Board board,
                     Subtype subtype,
                     JointTypeEnum jointType,
                     List<ImageSetData> imageSetDataList,
                     boolean learnAlgorithmSettings,
                     boolean learnSlopeSettings,
                     boolean learnJointSpecificData,
                     boolean learnTemplateData,
                     boolean learnExpectedImageData,
                     boolean learnExcludeLibrarySubtype,
                     boolean isApplyBoardToBoardLearning) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(learnedBoard != null);

    TestProgram testProgram = project.getTestProgram();
        
    if (board != null)
    {
      // Set filter to look for subtype--this will be used in ManagedOfflineImageSet
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(board);
    }

    // Make sure that the Short learning status is up to date.
    syncShortLearningStatus(project);
    // Make sure that the Expected Image learning status is up to date.
    syncExpectedImageLearningStatus(project);
    //Broken Pin
    // Make sure that the BrokenPin learning status is up to date.
    syncBrokenPinLearningStatus(project);    
    //ShengChuan - Clear Tombstone
    // Make sure that the Expected Image template learning status is up to date.
    syncExpectedImageTemplateLearningStatus(project);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter();//Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear Tombstone
    
    try
    {
      // Open a connection to the algorithm learning database.
      //Broken Pin & ShengChuan - Clear Tombstone
      if(learnTemplateData)
      {
        openConnectionToTemplateLearningDatabase(testProgram, algorithmExpectedImageTemplateLearning);
      }
      openConnectionToAlgorithmLearningDatabase(testProgram, algorithmSubtypeLearning, 
        algorithmShortsLearning, algorithmExpectedImageLearning, algorithmBrokenPinLearning);
      
      

      List<Subtype> subtypes = new ArrayList<Subtype>();
      
      if(jointType != null)
      {
        if(learnJointSpecificData || learnExcludeLibrarySubtype == false)
           subtypes = project.getPanel().getInspectedSubtypes(jointType);
        else
           subtypes = project.getPanel().getInspectedAndUnImportedSubtypes(jointType);
      }
      else if(subtype != null)
      {
        subtypes.add(subtype);
      }
      else
      {
        if (learnJointSpecificData || learnExcludeLibrarySubtype == false)
          subtypes = new ArrayList<Subtype>(project.getPanel().getInspectedSubtypesUnsorted());
        else
          subtypes = new ArrayList<Subtype>(project.getPanel().getInspectedAndUnImportedSubtypesUnsorted());
      }
      
      // alert the GUI that we are starting initial tuning
      sendInitialTuningStartMessage(subtypes.size());

      TimerUtil learnPanelTimer = new TimerUtil();
      learnPanelTimer.start();

      // iterate over each subtype on the panel and call learnSubtype()
      for (Subtype subType : subtypes)
      {
        if (isLearningCancelled())
          break;
        
        learnSubtype(project, subType, board, learnedBoard, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, true, learnExcludeLibrarySubtype, isApplyBoardToBoardLearning);
      }

      learnPanelTimer.stop();
      
      sendInitialTuningCompletedMessage();
    }
    finally
    {
      //Broken Pin & ShengChuan - Clear Tombstone
      if(learnTemplateData)
      {
        finishUpTemplateLearning(algorithmExpectedImageTemplateLearning);
      }
      finishUpLearning(algorithmSubtypeLearning, algorithmShortsLearning, algorithmExpectedImageLearning,
        algorithmBrokenPinLearning);
    }
  }
  
  /**
   * @author Cheah Lee Herng 
   * @Edited by Siew Yeng - added learnSlopeSettings
   * @author ShengChuan - Clear Tombstone - Add learnTemplateData
   */
  private void learn(Project project,
                     Board board,
                     List<ImageSetData> imageSetDataList,
                     boolean learnAlgorithmSettings,
                     boolean learnSlopeSettings,
                     boolean learnJointSpecificData,
                     boolean learnTemplateData,
                     boolean learnExpectedImageData,
                     boolean learnExcludeLibrarySubtype) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(imageSetDataList != null);

    TestProgram testProgram = project.getTestProgram();
        
    if (board != null)
    {
      // Set filter to look for subtype--this will be used in ManagedOfflineImageSet
      testProgram.clearFilters();
      testProgram.addFilter(true);
      testProgram.addFilter(board);
    }

    // Make sure that the Short learning status is up to date.
    syncShortLearningStatus(project);
    // Make sure that the Expected Image learning status is up to date.
    syncExpectedImageLearningStatus(project);
    //Broken Pin
    // Make sure that the BrokenPin learning status is up to date.
    syncBrokenPinLearningStatus(project);
    //ShengChuan - Clear Tombstone 
    // Make sure that the Expected Image template learning status is up to date.
    syncExpectedImageTemplateLearningStatus(project);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter();//Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear Tombstone
    
    try
    {
      //Broken Pin & ShengChuan - Clear Tombstone
      // Open a connection to the algorithm learning database.
      if(learnTemplateData)
      {
        openConnectionToTemplateLearningDatabase(testProgram, algorithmExpectedImageTemplateLearning);
      }
      openConnectionToAlgorithmLearningDatabase(testProgram, algorithmSubtypeLearning, 
        algorithmShortsLearning, algorithmExpectedImageLearning, algorithmBrokenPinLearning);

      Set<Subtype> subtypes = null;
      
      if (learnJointSpecificData || learnExcludeLibrarySubtype == false)
        subtypes = project.getPanel().getInspectedSubtypesUnsorted();
      else
        subtypes = project.getPanel().getInspectedAndUnImportedSubtypesUnsorted();

      // alert the GUI that we are starting initial tuning
      sendInitialTuningStartMessage(subtypes.size());

      TimerUtil learnPanelTimer = new TimerUtil();
      learnPanelTimer.start();

      // iterate over each subtype on the panel and call learnSubtype()
      for (Subtype subtype : subtypes)
      {
        if (isLearningCancelled())
          break;
        
        learnSubtype(project, subtype, board, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, true, learnExcludeLibrarySubtype);
      }

      learnPanelTimer.stop();
      //      System.out.println("  [Sunit] Panel learning time total: " + learnPanelTimer.getElapsedTimeInMillis() / 1000.0);

      sendInitialTuningCompletedMessage();
    }
    finally
    {
      //Broken Pin & ShengChuan - Clear Tombstone
      if(learnTemplateData)
      {
        finishUpTemplateLearning(algorithmExpectedImageTemplateLearning);
      }
      finishUpLearning(algorithmSubtypeLearning, algorithmShortsLearning, algorithmExpectedImageLearning,
        algorithmBrokenPinLearning);
    }
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   * @author George Booth
   * @author Lim, Lay Ngor - Add broken pin learning
   * @author ShengChuan - Clear Tombstone - Add Template Image Learning
   */
  private void finishUpLearning(AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning,
                                AlgorithmShortsLearningReaderWriter algorithmShortsLearning,
                                AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning,
                                AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning) throws DatastoreException
  {
    Assert.expect(algorithmSubtypeLearning != null);
    Assert.expect(algorithmShortsLearning != null);
    Assert.expect(algorithmExpectedImageLearning != null);
    Assert.expect(algorithmBrokenPinLearning != null);//Broken Pin

    // Sanity check
    Assert.expect(algorithmShortsLearning.isOpen());
    Assert.expect(algorithmSubtypeLearning.isOpen());
    Assert.expect(algorithmExpectedImageLearning.isOpen());
    Assert.expect(algorithmBrokenPinLearning.isOpen());//Broken Pin

    // close the connection to the learning databases
    //Broken Pin & Shengchuan - Clear Tombstone
    closeAlgorithmLearningDatabaseConnections(algorithmSubtypeLearning, 
      algorithmShortsLearning, algorithmExpectedImageLearning, algorithmBrokenPinLearning);

    // PE - This is normally only used during inspections . . . I think maybe it's required in case you want to
    // run learning with diagnostics on, which is a nice debugging feature Sunit added
    InspectionStateController.getInstance().reset();

    // PE - Not totally sure why this is necessary, but Sunit or Matt have added this
    _shortLearningStatusSynced = false;
    _expectedImageLearningStatusSynced = false;
    _brokenPinLearningStatusSynced = false;//Broken Pin
  }
  
  /**
   * close template learning file
   * @param algorithmExpectedImageTemplateLearning
   * @throws DatastoreException 
   */
  private void finishUpTemplateLearning(AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning) throws DatastoreException
  {
    Assert.expect(algorithmExpectedImageTemplateLearning != null);//Shengchuan - Clear Tombstone
    Assert.expect(algorithmExpectedImageTemplateLearning.isOpen());//Shengchuan - Clear Tombstone

    // close the connection to the learning databases
    //Shengchuan - Clear Tombstone
    algorithmExpectedImageTemplateLearning.close();
    _expectedImageTemplateLearningStatusSynced = false;//ShengChuan - Clear Tobmstone
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   * @author George Booth
   * @author Lim, Lay Ngor - Add broken pin learning
   * @author ShengChuan - Clear Tombstone - Add Template Image Learning
   */
  private void closeAlgorithmLearningDatabaseConnections(AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning,
                                                         AlgorithmShortsLearningReaderWriter algorithmShortsLearning,
                                                         AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning,
                                                         AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning) throws DatastoreException


  {
    Assert.expect(algorithmSubtypeLearning != null);
    Assert.expect(algorithmShortsLearning != null);
    Assert.expect(algorithmExpectedImageLearning != null);
    Assert.expect(algorithmBrokenPinLearning != null);//Broken Pin

    // Close the algorithm learning database connection.
    algorithmSubtypeLearning.close();
    algorithmShortsLearning.close();
    algorithmExpectedImageLearning.close();
    algorithmBrokenPinLearning.close();//Broken Pin
  }

  /**
   *  Delete and relearn the Joint based learned data for the specified JointTypeEnum.  Note that ONLY learned corresponding
   * to the images in the imageSetDataList will be deleted.  So this will not necessarily delete the entire JointType's
   * learned data.
   *
   * @author Peter Esbensen
   */
  public void relearnJointSpecificDataForPanel(Project project,
                                               List<ImageSetData> imageSetDataList,
                                               boolean learnExcludeLibrarySubtype ) throws XrayTesterException

  {
    Assert.expect(project != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    relearnJointSpecifcData(project, null, imageSetDataList, learnExcludeLibrarySubtype);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void relearnJointSpecificDataForBoard(Project project,
                                               Board board,
                                               List<ImageSetData> imageSetDataList,
                                               boolean learnExcludeLibrarySubtype ) throws XrayTesterException

  {
    Assert.expect(project != null);
    Assert.expect(board != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    relearnJointSpecifcData(project, board, imageSetDataList, learnExcludeLibrarySubtype);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void relearnJointSpecifcData(Project project,
                                       Board board,
                                       List<ImageSetData> imageSetDataList,
                                       boolean learnExcludeLibrarySubtype ) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);
    
    try
    {
      // alert the GUI that we are starting relearning
      sendInitialTuningStartMessage(project.getPanel().getInspectedSubtypes().size());

      // iterate through all the subtypes in the joint type and relearn
      for (Subtype subtype : project.getPanel().getInspectedSubtypes())
      {
        // if a cancel was requested, break out of here
        if (isLearningCancelled())
          break;

        // relearn

        processRelearningJointSpecificDataForSubtype(project, subtype, board, imageSetDataList, learnExcludeLibrarySubtype);
      }
      // alert the GUI that we are finished with this process
      sendInitialTuningCompletedMessage();
    }
    finally
    {
      finishUpRelearning();
    }
  }

  /**
   *  Delete and relearn the Joint based learned data for the specified JointTypeEnum.  Note that ONLY learned corresponding
   * to the images in the imageSetDataList will be deleted.  So this will not necessarily delete the entire JointType's
   * learned data.
   *
   * @author Peter Esbensen
   */
  public void relearnJointSpecificDataForJointType(Project project,
                                                   JointTypeEnum jointType,
                                                   Board board,
                                                   List<ImageSetData> imageSetDataList,
                                                   boolean learnExcludeLibrarySubtype) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(jointType != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    try
    {
      // alert the GUI that we are starting initial tuning
      sendInitialTuningStartMessage(project.getPanel().getInspectedSubtypes(jointType).size());

      // iterate through all the subtypes in the joint type and relearn
      for (Subtype subtype : project.getPanel().getInspectedSubtypes(jointType))
      {
        // if a cancel was requested, break out of here
        if (isLearningCancelled())
          break;

        // relearn
        processRelearningJointSpecificDataForSubtype(project, subtype, board, imageSetDataList, learnExcludeLibrarySubtype);
      }
      // alert the GUI that we are finished with this process
      sendInitialTuningCompletedMessage();
    }
    finally
    {
      finishUpRelearning();
    }
  }

  /**
   *  Delete and relearn the Joint based learned data for the specified JointTypeEnum.  Note that ONLY learned corresponding
   * to the images in the imageSetDataList will be deleted.  So this will not necessarily delete the entire JointType's
   * learned data.
   *
   * @author Peter Esbensen
   */
  public void relearnJointSpecificDataExceptForJointType(Project project,
                                                         JointTypeEnum jointType,
                                                         Board board,
                                                         List<ImageSetData> imageSetDataList,
                                                         boolean learnExcludeLibrarySubtype) throws XrayTesterException

  {
    Assert.expect(project != null);
    Assert.expect(jointType != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    try
    {
      // get the list of subtypes to address
      List<Subtype> subtypes = project.getPanel().getInspectedSubtypes();
      Iterator it = subtypes.iterator();
      while(it.hasNext())
      {
        Subtype subtype = (Subtype)it.next();
        if (subtype.getJointTypeEnum().equals(jointType))
          it.remove();
      }

      // alert the GUI that we are starting initial tuning
      sendInitialTuningStartMessage(subtypes.size());

      // iterate through all the subtypes in the joint type and relearn
      for (Subtype subtype : subtypes)
      {
        // if a cancel was requested, break out of here
        if (isLearningCancelled())
          break;

        // relearn
        processRelearningJointSpecificDataForSubtype(project, subtype, board, imageSetDataList, learnExcludeLibrarySubtype);
      }
      // alert the GUI that we are finished with this process
      sendInitialTuningCompletedMessage();
    }
    finally
    {
      finishUpRelearning();
    }
  }

  /**
   * Delete and relearn the joint specific learned data for the specified Subtype.  Note that ONLY learned corresponding
   * to the images in the imageSetDataList will be deleted.  So this will not necessarily delete the entire Subtype's
   * learned data.
   *
   * @author Peter Esbensen
   */
  public void relearnJointSpecificDataForSubtype(Project project,
                                                 Subtype subtype,
                                                 Board board,
                                                 List<ImageSetData> imageSetDataList,
                                                 boolean learnExcludeLibrarySubtype) throws XrayTesterException

  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);


    try
    {
      // alert the GUI that we are starting learning
      int numberOfSubtypesBeingLearned = 1;
      sendInitialTuningStartMessage(numberOfSubtypesBeingLearned);

      // do the relearning
      processRelearningJointSpecificDataForSubtype(project, subtype, board, imageSetDataList, learnExcludeLibrarySubtype);

      // alert the GUI that we are done
      sendInitialTuningCompletedMessage();
    }
    finally
    {
      finishUpRelearning();
    }
  }

  /**
   *  Delete and relearn the Joint based learned data for the specified JointTypeEnum.  Note that ONLY learned corresponding
   * to the images in the imageSetDataList will be deleted.  So this will not necessarily delete the entire JointType's
   * learned data.
   *
   * @author Peter Esbensen
   */
  public void relearnJointSpecificDataExceptForSubtype(Project project,
                                                       Subtype subtypeToExclude,
                                                       Board board,
                                                       List<ImageSetData> imageSetDataList,
                                                       boolean learnExcludeLibrarySubtype) throws XrayTesterException

  {
    Assert.expect(project != null);
    Assert.expect(subtypeToExclude != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    try
    {
      // get the list of subtypes to address
      List<Subtype> subtypes = project.getPanel().getInspectedSubtypes();
      Iterator it = subtypes.iterator();
      while (it.hasNext())
      {
        Subtype subtype = (Subtype)it.next();
        if (subtype.equals(subtypeToExclude))
          it.remove();
      }

      // alert the GUI that we are starting initial tuning
      sendInitialTuningStartMessage(subtypes.size());

      // iterate through all the subtypes in the joint type and relearn
      for (Subtype subtype : subtypes)
      {
        // if a cancel was requested, break out of here
        if (isLearningCancelled())
          break;

        // relearn
        processRelearningJointSpecificDataForSubtype(project, subtype, board, imageSetDataList, learnExcludeLibrarySubtype);
      }
      // alert the GUI that we are finished with this process
      sendInitialTuningCompletedMessage();
    }
    finally
    {
      finishUpRelearning();
    }
  }


  /**
   * @author Peter Esbensen
   * @author George Booth
   */
  private void finishUpRelearning()
  {
    // PE - I think this is needed for the debug-only feature of having diagnostics on during learning
    InspectionStateController.getInstance().reset();

    // PE - I don't fully understand this but am following Sunit/Matt's convention by calling this here
    _shortLearningStatusSynced = false;
    _expectedImageLearningStatusSynced = false;
    _brokenPinLearningStatusSynced = false;//Broken Pin
    _expectedImageTemplateLearningStatusSynced = false;//ShengChuan - Clear Tombstone
  }


  /**
   * Right now, we only have Short and Expected Image Learning for Joint Specific Learned data.  If we ever add more,
   * this method and possibly some others will have to be updated to properly delete that new data.
   *
   * @author Peter Esbensen
   * @author George Booth
   */
  private void processRelearningJointSpecificDataForSubtype(Project project,
                                                            Subtype subtype,
                                                            Board board,
                                                            List<ImageSetData> imageSetDataList,
                                                            boolean learnExcludeLibrarySubtype) throws XrayTesterException

  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    if(learnExcludeLibrarySubtype && subtype.isImportedPackageLibrary())
      return;

    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter();//Broken Pin

    // Make sure that the Short learning status is up to date.
    syncShortLearningStatus(project);
    // Make sure that the ExpectedImage learning status is up to date.
    syncExpectedImageLearningStatus(project);
    //Broken Pin
    // Make sure that the BrokenPin learning status is up to date.
    syncBrokenPinLearningStatus(project);
    //ShengChuan - Clear Tombstone
    // Make sure that the Expected Image template learning status is up to date.
    syncExpectedImageTemplateLearningStatus(project);

    try
    {
      TestProgram testProgram = project.getTestProgram();

      // open a connection to the learning database
      openConnectionToShortsLearningDatabase(testProgram, algorithmShortsLearning);
      openConnectionToExpectedImageLearningDatabase(testProgram, algorithmExpectedImageLearning);
      openConnectionToBrokenPinLearningDatabase(testProgram, algorithmBrokenPinLearning);//Broken Pin

      // delete the data
      deleteShortLearnedData(subtype, imageSetDataList);
      deleteExpectedImageLearnedData(subtype, imageSetDataList);
      deleteBrokenPinLearnedData(subtype, imageSetDataList);//Broken Pin
    }
    finally
    {
      // close the database . . . this is apparently necessary or else subsequent learning database operations will fail
      algorithmShortsLearning.close();
      algorithmExpectedImageLearning.close();
      algorithmBrokenPinLearning.close();//Broken Pin
    }

    // Make sure that the Short learning status is up to date  PE:  I don't think this is really necessary
    syncShortLearningStatus(project);
    syncExpectedImageLearningStatus(project);
    syncBrokenPinLearningStatus(project);//Broken Pin
    syncExpectedImageTemplateLearningStatus(project);//ShengChuan - Clear Tombstone

    try
    {
      // open a connection to the learning database
      openConnectionToShortsLearningDatabase(project.getTestProgram(), algorithmShortsLearning);
      openConnectionToExpectedImageLearningDatabase(project.getTestProgram(), algorithmExpectedImageLearning);
      openConnectionToBrokenPinLearningDatabase(project.getTestProgram(), algorithmBrokenPinLearning);//Broken Pin

      // now learn the subtype using the given image sets
      boolean learnAlgorithmSettings = false; // we only relearn Short and Expected Image
      boolean learnJointSpecificData = true; // learning Short
      boolean learnExpectedImageData = true; // learning Expected Image
      boolean learnExpectedImageTemplateData = false; //ShengChuan - Clear Tombstone - exclude Expected Image
      boolean learningMultipleSubtypes = true; // this means that the learnSubtype() should not handle the GUI begin/end notifications, that will be done in this method instead
      //ShengChuan - Clear Tombstone - Add learnExpectedImageTemplateData
      learnSubtype(project, subtype, board, imageSetDataList, learnAlgorithmSettings, false, learnJointSpecificData, learnExpectedImageTemplateData, learnExpectedImageData, learningMultipleSubtypes, learnExcludeLibrarySubtype);
    }
    finally
    {
      // close the database
      algorithmShortsLearning.close();
      algorithmExpectedImageLearning.close();
      algorithmBrokenPinLearning.close();//Broken Pin
    }
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   */
  private void syncShortLearningStatus(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    // Make sure that the Short learning status is up to date.
    if (_shortLearningStatusSynced == false)
    {
      Panel panel = project.getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      panelSettings.syncShortLearningStatusForAllSubtypes();

      _shortLearningStatusSynced = true;
    }
  }

  /**
   * @author George Booth
   */
  private void syncExpectedImageLearningStatus(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    // Make sure that the Expected Image learning status is up to date.
    if (_expectedImageLearningStatusSynced == false)
    {
      Panel panel = project.getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      panelSettings.syncExpectedImageLearningStatusForAllSubtypes();

      _expectedImageLearningStatusSynced = true;
    }
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   * @author Lim, Lay Ngor - Broken Pin
   */
  private void syncBrokenPinLearningStatus(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    // Make sure that the Short learning status is up to date.
    if (_brokenPinLearningStatusSynced == false)
    {
      Panel panel = project.getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      panelSettings.syncBrokenPinLearningStatusForAllSubtypes();

      _brokenPinLearningStatusSynced = true;
    }
  }

  /**
   * Clear Tombstone
   * @author sheng chuan
   */
  private void syncExpectedImageTemplateLearningStatus(Project project) throws DatastoreException
  {
    Assert.expect(project != null);

    // Make sure that the Short learning status is up to date.
    if (_expectedImageTemplateLearningStatusSynced == false)
    {
      Panel panel = project.getPanel();
      PanelSettings panelSettings = panel.getPanelSettings();
      panelSettings.syncExpectedImageTemplateLearningStatusForAllSubtypes();

      _expectedImageTemplateLearningStatusSynced = true;
    }
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   * @author George Booth
   * @author Lim, Lay Ngor - Add broken pin learning
   */
  private void openConnectionToAlgorithmLearningDatabase(TestProgram testProgram,
                                                         AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning,
                                                         AlgorithmShortsLearningReaderWriter algorithmShortsLearning,
                                                         AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning,
                                                         AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning) throws DatastoreException
  {
    Assert.expect(testProgram != null);
    Assert.expect(algorithmSubtypeLearning != null);
    Assert.expect(algorithmShortsLearning != null);
    Assert.expect(algorithmExpectedImageLearning != null);
    Assert.expect(algorithmBrokenPinLearning != null);//Broken Pin

    openConnectionToSubtypeLearning(algorithmSubtypeLearning);
    openConnectionToShortsLearningDatabase(testProgram, algorithmShortsLearning);
    openConnectionToExpectedImageLearningDatabase(testProgram, algorithmExpectedImageLearning);
    openConnectionToBrokenPinLearningDatabase(testProgram, algorithmBrokenPinLearning);//Broken Pin
  }
  
  /**
   * @author ShengChuan - Clear Tombstone - Add Template Image Learning
   */
  private void openConnectionToTemplateLearningDatabase(TestProgram testProgram,
                                                         AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning) throws DatastoreException
  {
    Assert.expect(testProgram != null);
    Assert.expect(algorithmExpectedImageTemplateLearning != null);
    
    openConnectionToExpectedImageTemplateLearningDatabase(testProgram, algorithmExpectedImageTemplateLearning);//ShengChuan - Clear Tombstone
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   */
  private void openConnectionToSubtypeLearning(AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning) throws DatastoreException
  {
    algorithmSubtypeLearning.close();
    algorithmSubtypeLearning.open();
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   */
  void openConnectionToShortsLearningDatabase(TestProgram testProgram,
                                                      AlgorithmShortsLearningReaderWriter algorithmShortsLearning) throws DatastoreException
  {
    Assert.expect(testProgram != null);
    Assert.expect(algorithmShortsLearning != null);

    algorithmShortsLearning.close();
    algorithmShortsLearning.open();
  }

  /**
   * @author George Booth
   */
  void openConnectionToExpectedImageLearningDatabase(TestProgram testProgram,
                                                     AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning) throws DatastoreException
  {
    Assert.expect(testProgram != null);
    Assert.expect(algorithmExpectedImageLearning != null);

    algorithmExpectedImageLearning.close();
    algorithmExpectedImageLearning.open();
  }

  /**
   * @author Lim, Lay Ngor - Broken Pin
   */
  void openConnectionToBrokenPinLearningDatabase(TestProgram testProgram,
                                                 AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning) throws DatastoreException
  {
    Assert.expect(testProgram != null);
    Assert.expect(algorithmBrokenPinLearning != null);

    algorithmBrokenPinLearning.close();
    algorithmBrokenPinLearning.open();
  }

  /**
   * @author ShengChuan - Clear Tombstone
   */
  void openConnectionToExpectedImageTemplateLearningDatabase(TestProgram testProgram,
                                                     AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning) throws DatastoreException
  {
    Assert.expect(testProgram != null);
    Assert.expect(algorithmExpectedImageTemplateLearning != null);

    algorithmExpectedImageTemplateLearning.close();
    algorithmExpectedImageTemplateLearning.open();
  }

  /**
   * A sixth parameter was added to learnJointType() to handle expected images. This method
   * preserves the old method signature for all the existing algorithm regression tests.
   *
   * @author George Booth
   * @author ShengChuan - Clear Tombstone - Add Template Image Learning
   */
  public void learnJointType(Project project,
                             JointTypeEnum jointType,
                             List<ImageSetData> imageSetDataList,
                             boolean learnAlgorithmSettings,
                             boolean learnJointSpecificData,
                             boolean learnTemplateData) throws XrayTesterException
  {
    learnJointType(project, jointType, null, imageSetDataList, learnAlgorithmSettings, false, learnJointSpecificData, learnTemplateData, false, false);
  }

  /**
   * @author Sunit Bhalla
   * @author George Booth
   * @author ShengChuan - Clear Tombstone - Add Template Image Learning
   */
  public void learnJointType(Project project,
                             JointTypeEnum jointType,
                             Board board,
                             List<ImageSetData> imageSetDataList,
                             boolean learnAlgorithmSettings,
                             boolean learnSlopeSettings,
                             boolean learnJointSpecificData,
                             boolean learnTemplateData,
                             boolean learnExpectedImageData,
                             boolean learnExcludeLibrarySubtype) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(jointType != null);
    Assert.expect(imageSetDataList != null);

    TestProgram testProgram = project.getTestProgram();

    // Make sure that the Short learning status is up to date.
    syncShortLearningStatus(project);
    // Make sure that the Short learning status is up to date.
    syncExpectedImageLearningStatus(project);
    //Broken Pin
    // Make sure that the BrokenPin learning status is up to date.
    syncBrokenPinLearningStatus(project);
    //ShengChuan - Clear Tombstone
    // Make sure that the Expected Image template learning status is up to date.
    syncExpectedImageTemplateLearningStatus(project);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter();//Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear Tombstone

    try
    {
      // Open a connection to the algorithm learning database.
      //Broken Pin & ShengChuan - Clear Tombstone
      if(learnTemplateData)
      {
        openConnectionToTemplateLearningDatabase(testProgram, algorithmExpectedImageTemplateLearning);
      }
      openConnectionToAlgorithmLearningDatabase(testProgram, algorithmSubtypeLearning, 
        algorithmShortsLearning, algorithmExpectedImageLearning, algorithmBrokenPinLearning);

      List<Subtype> subtypes = null;

      if(learnJointSpecificData || learnExcludeLibrarySubtype == false)
        subtypes = project.getPanel().getInspectedSubtypes(jointType);
      else
        subtypes = project.getPanel().getInspectedAndUnImportedSubtypes(jointType);

      sendInitialTuningStartMessage(subtypes.size());
      for (Subtype subtype : subtypes)
      {
        if (isLearningCancelled())
          break;

        learnSubtype(project, subtype, board, imageSetDataList, learnAlgorithmSettings, learnSlopeSettings, learnJointSpecificData, learnTemplateData, learnExpectedImageData, true, learnExcludeLibrarySubtype);
      }
      sendInitialTuningCompletedMessage();

      subtypes.clear();
      subtypes = null;
    }
    finally
    {
      //Broken Pin & ShengChuan - Clear Tombstone
      if(learnTemplateData)
      {
        finishUpTemplateLearning(algorithmExpectedImageTemplateLearning);
      }
      finishUpLearning(algorithmSubtypeLearning, algorithmShortsLearning, algorithmExpectedImageLearning,
        algorithmBrokenPinLearning);
    }
  }

  /**
   * Delete the short learned data for each joint of the given subtype in each image in the imageSetDataList.
   * Note: a joint will NOT have its data deleted if it is not in the imageSetDataList.
   *
   * @author Peter Esbensen
   */
  public void deleteShortLearnedData(Subtype subtype,
                                     List<ImageSetData> imageSetDataList) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = subtype.getPanel().getProject().getAlgorithmShortsLearningReaderWriter();

    // let anybody who cares (ie- the GUI) know that we are starting
    sendSubtypeDeletingLearnedDataStartedMessage(subtype);

    Assert.expect(algorithmShortsLearning.isOpen());

    // put the imageSetDataList into a ManagedOfflineImageSet . . . this is needed for the delete
    TestProgram testProgram = subtype.getPanel().getProject().getTestProgram();

    // set up the test program filters
    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(subtype);

    // do the actual delete
    subtype.deleteLearnedJointSpecificData(testProgram.getFilteredInspectionRegions(), imageSetDataList);

    // let people know that we are done
    sendSubtypeDeletingLearnedDataCompletedMessage();
  }

  /**
   * Delete the expected image learned data for each joint of the given subtype in each image in the imageSetDataList.
   * Note: a joint will NOT have its data deleted if it is not in the imageSetDataList.
   *
   * @author George Booth
   */
  public void deleteExpectedImageLearnedData(Subtype subtype,
                                     List<ImageSetData> imageSetDataList) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = subtype.getPanel().getProject().getAlgorithmExpectedImageLearningReaderWriter();

    // let anybody who cares (ie- the GUI) know that we are starting
    sendSubtypeDeletingLearnedDataStartedMessage(subtype);

    Assert.expect(algorithmExpectedImageLearning.isOpen());

    // put the imageSetDataList into a ManagedOfflineImageSet . . . this is needed for the delete
    TestProgram testProgram = subtype.getPanel().getProject().getTestProgram();

    // set up the test program filters
    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(subtype);

    // do the actual delete
    subtype.deleteLearnedExpectedImageData(testProgram.getFilteredInspectionRegions(), imageSetDataList);

    // let people know that we are done
    sendSubtypeDeletingLearnedDataCompletedMessage();
  }

  /**
   * Delete the broken pin learned data for each joint of the given subtype in each image in the imageSetDataList.
   * Note: a joint will NOT have its data deleted if it is not in the imageSetDataList.
   *
   * @author Peter Esbensen
   * @author Lim, Lay Ngor - Broken Pin
   */
  public void deleteBrokenPinLearnedData(Subtype subtype,
                                     List<ImageSetData> imageSetDataList) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);
    Assert.expect(imageSetDataList.size() > 0);

    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = subtype.getPanel().getProject().getAlgorithmBrokenPinLearningReaderWriter();

    // let anybody who cares (ie- the GUI) know that we are starting
    sendSubtypeDeletingLearnedDataStartedMessage(subtype);

    Assert.expect(algorithmBrokenPinLearning.isOpen());

    // put the imageSetDataList into a ManagedOfflineImageSet . . . this is needed for the delete
    TestProgram testProgram = subtype.getPanel().getProject().getTestProgram();

    // set up the test program filters
    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(subtype);

    // do the actual delete
    subtype.deleteLearnedJointSpecificData(testProgram.getFilteredInspectionRegions(), imageSetDataList);

    // let people know that we are done
    sendSubtypeDeletingLearnedDataCompletedMessage();
  }
  
  /**
   *  @param subtype the subtype for which the learned data is being deleted.
   *  @param deleteLearnedDataForAllAlgorithmsExceptShort indicates whether data should be deleted for all
   *  algorithms except for short.
   *  @param deleteLearnedDataForShort indicates whether short data should be deleted.
   *  @param deleteLearnedDataForExpectedImage indicates whether expected image data should be deleted.
   *  @author Sunit Bhalla
   *  @author George Booth
   *  @author Lim, Lay Ngor - Broken pin
   *  @author ShengChuan - clear tombstone - TemplateImage
   */
  public void deleteSubtypeLearnedData(Subtype subtype,
                                       boolean deleteLearnedDataForAllAlgorithmsExceptShort,
                                       boolean deleteLearnedDataForShort,
                                       boolean deleteLearnedDataForExpectedImage,
                                       boolean deleteLearnedDataForBrokenPin,//Broken Pin
                                       boolean deleteTemplateImage,
                                       boolean deletingMultipleSubtypes) throws XrayTesterException
  {
    Assert.expect(subtype != null);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = subtype.getPanel().getProject().getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = subtype.getPanel().getProject().getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = subtype.getPanel().getProject().getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = subtype.getPanel().getProject().getAlgorithmBrokenPinLearningReaderWriter();//Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = subtype.getPanel().getProject().getAlgorithmExpectedImageTemplateLearningReaderWriter();

    if (deletingMultipleSubtypes == false)
      sendDeletingLearnedDataStartMessage(1);

    sendSubtypeDeletingLearnedDataStartedMessage(subtype);

    try
    {
      // Open a connection to the learning database if needed.
      if (deletingMultipleSubtypes == false)
      {
        algorithmSubtypeLearning.close();
        algorithmSubtypeLearning.open();

        algorithmShortsLearning.close();
        algorithmShortsLearning.open();

        algorithmExpectedImageLearning.close();
        algorithmExpectedImageLearning.open();
        
        //Broken Pin
        algorithmBrokenPinLearning.close();
        algorithmBrokenPinLearning.open();        

        //ShengChuan - Clear Tombstone
        algorithmExpectedImageTemplateLearning.close();
        algorithmExpectedImageTemplateLearning.open();
      }
      else
      {
        Assert.expect(algorithmSubtypeLearning.isOpen());
        Assert.expect(algorithmShortsLearning.isOpen());
        Assert.expect(algorithmExpectedImageLearning.isOpen());
        Assert.expect(algorithmBrokenPinLearning.isOpen());//Broken Pin
        Assert.expect(algorithmExpectedImageTemplateLearning.isOpen());//ShengChuan - Clear Tombstone
      }

      if (deleteLearnedDataForAllAlgorithmsExceptShort)
        subtype.deleteLearnedAlgorithmSettingsData();
      if (deleteLearnedDataForShort)
        subtype.deleteLearnedJointSpecificData();
      if (deleteLearnedDataForExpectedImage)
        subtype.deleteLearnedExpectedImageData();
      //Broken Pin
      if (deleteLearnedDataForBrokenPin)
        subtype.deleteLearnedJointSpecificData();      
      //ShengChuan - Clear Tombstone
      if (deleteTemplateImage)
        subtype.deleteLearnedExpectedImageTemplateData();
    }
    finally
    {
      // Close the connection to the learning database if we opened it in this method.
      if (deletingMultipleSubtypes == false)
      {
        algorithmSubtypeLearning.close();
        algorithmShortsLearning.close();
        algorithmExpectedImageLearning.close();
        algorithmBrokenPinLearning.close();//Broken Pin
        algorithmExpectedImageTemplateLearning.close();//ShengChuan - Clear Tombstone
      }
      sendSubtypeDeletingLearnedDataCompletedMessage();
      if (deletingMultipleSubtypes == false)
        sendDeletingLearnedDataCompletedMessage();
    }
  }

  /**
   *  @param project is the project for which settings are being deleted.
   *  @param jointType is the joint type for which data is being deleted
   *  @param deleteLearnedDataForAllAlgorithmsExceptShort indicates whether data should be deleted for all
   *  algorithms except for short.
   *  @param deleteLearnedDataForShort indicates whether short data should be deleted.
   *  @param deleteLearnedDataForExpectedImage indicates whether expected iamge data should be deleted.
   *  @author Sunit Bhalla
   *  @author George Booth
   *  @author Lim, Lay Ngor - Broken pin
   *  @author ShengChuan - clear tombstone - TemplateImage
   */
  public void deleteJointTypeLearnedData(Project project,
                                         JointTypeEnum jointType,
                                         boolean deleteLearnedDataForAllAlgorithmsExceptShort,
                                         boolean deleteLearnedDataForShort,
                                         boolean deleteLearnedDataForExpectedImage,
                                         boolean deleteLearnedDataForBrokenPin,
                                         boolean deleteLearnedTemplateImage) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(jointType != null);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter();//Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear Tombstone

    sendDeletingLearnedDataStartMessage(project.getPanel().getSubtypesUnsorted(jointType).size());

    try
    {
      // Open a connection to the algorithm learning database.
      algorithmSubtypeLearning.close();
      algorithmSubtypeLearning.open();

      algorithmShortsLearning.close();
      algorithmShortsLearning.open();

      algorithmExpectedImageLearning.close();
      algorithmExpectedImageLearning.open();
      
      //Broken Pin
      algorithmBrokenPinLearning.close();
      algorithmBrokenPinLearning.open();      

      //ShengChuan - Clear Tombstone
      algorithmExpectedImageTemplateLearning.close();
      algorithmExpectedImageTemplateLearning.open();
      
      for (Subtype subtype : project.getPanel().getSubtypesUnsorted(jointType))
      {
        if (isDeletingCancelled())
          break;

        //Broken Pin
        //deleteLearnedDataForBrokenPin now share using the short status!
        deleteSubtypeLearnedData(subtype, deleteLearnedDataForAllAlgorithmsExceptShort, deleteLearnedDataForShort,
                                 deleteLearnedDataForExpectedImage, deleteLearnedDataForBrokenPin, deleteLearnedTemplateImage, true);
      }
    }
    finally
    {
      // Close the algorithm learning database.
      algorithmSubtypeLearning.close();
      algorithmShortsLearning.close();
      algorithmExpectedImageLearning.close();
      algorithmBrokenPinLearning.close();//Broken Pin
      algorithmExpectedImageTemplateLearning.close();

      sendDeletingLearnedDataCompletedMessage();
    }
  }

  /**
   *  @param project is the project for which settings are being deleted.
   *  @param deleteLearnedDataForAllAlgorithmsExceptShort indicates whether data should be deleted for all
   *  algorithms except for short.
   *  @param deleteLearnedDataForShort indicates whether short data should be deleted.
   *  @param deleteLearnedDataForExpectedImage indicates whether expected image data should be deleted.
   *  @author Sunit Bhalla
   *  @author George Booth
   *  @author Lim, Lay Ngor - Broken pin
   *  @author ShengChuan - clear tombstone - TemplateImage
   */
  public void deletePanelLearnedData(Project project,
                                     boolean deleteLearnedDataForAllAlgorithmsExceptShort,
                                     boolean deleteLearnedDataForShort,
                                     boolean deleteLearnedDataForExpectedImage,
                                     boolean deleteLearnedDataForBrokenPin,
                                     boolean deleteLearnedTemplateImage) throws XrayTesterException
  {
    Assert.expect(project != null);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
    AlgorithmShortsLearningReaderWriter algorithmShortsLearning = project.getAlgorithmShortsLearningReaderWriter();
    AlgorithmExpectedImageLearningReaderWriter algorithmExpectedImageLearning = project.getAlgorithmExpectedImageLearningReaderWriter();
    AlgorithmBrokenPinLearningReaderWriter algorithmBrokenPinLearning = project.getAlgorithmBrokenPinLearningReaderWriter();//Broken Pin
    AlgorithmExpectedImageTemplateLearningReaderWriter algorithmExpectedImageTemplateLearning = project.getAlgorithmExpectedImageTemplateLearningReaderWriter();//ShengChuan - Clear Tombstone

    sendDeletingLearnedDataStartMessage(project.getPanel().getSubtypes().size());

    try
    {
      // Open a connection to the algorithm learning database.
      algorithmSubtypeLearning.close();
      algorithmSubtypeLearning.open();

      algorithmShortsLearning.close();
      algorithmShortsLearning.open();

      algorithmExpectedImageLearning.close();
      algorithmExpectedImageLearning.open();

      //Broken Pin
      algorithmBrokenPinLearning.close();
      algorithmBrokenPinLearning.open();
      
      //ShengChuan - Clear Tombstone
      algorithmExpectedImageTemplateLearning.close();
      algorithmExpectedImageTemplateLearning.open();

      if (deleteLearnedDataForAllAlgorithmsExceptShort)
        project.deleteLearnedJointMeasurements();
      if (deleteLearnedDataForShort)
        project.deleteShortProfileLearning();
      if (deleteLearnedDataForExpectedImage)
        project.deleteExpectedImageLearning();
      //Broken Pin
      if (deleteLearnedDataForBrokenPin)
        project.deleteBrokenPinDataLearning();
      //ShengChuan - Clear Tombstone
      if (deleteLearnedTemplateImage)
        project.deleteExpectedImageTemplateLearning();

      for (Subtype subtype : project.getPanel().getSubtypes())
      {
        if (isDeletingCancelled())
          break;

        //Broken Pin & ShengChuan - Clear Tombstone
        deleteSubtypeLearnedData(subtype, deleteLearnedDataForAllAlgorithmsExceptShort, deleteLearnedDataForShort,
                                 deleteLearnedDataForExpectedImage, deleteLearnedDataForShort, deleteLearnedTemplateImage, true);
      }
    }
    finally
    {
      // Close the algorithm learning database.
      algorithmSubtypeLearning.close();
      algorithmShortsLearning.close();
      algorithmExpectedImageLearning.close();
      algorithmBrokenPinLearning.close();//Broken Pin
      algorithmExpectedImageTemplateLearning.close();
      sendDeletingLearnedDataCompletedMessage();
    }
  }

  /**
   *  @author Sunit Bhalla
   *  This method calls opens up the learning database and calls subtype.updateNominals().
   */
  public void updateNominals(Project project,
                             Subtype subtype,
                             Board board,
                             List<ImageSetData> imageSetDataList) throws XrayTesterException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);
    Assert.expect(imageSetDataList != null);

    TestProgram testProgram = project.getTestProgram();

    // Make sure that we have at least one image run
    Assert.expect(imageSetDataList.size() > 0);

    // Set filter to look for subtype--this will be used in ManagedOfflineImageSet
    testProgram.clearFilters();
    testProgram.addFilter(true);
    testProgram.addFilter(subtype);
    
    if (board != null)
    {
      testProgram.addFilter(board);
    }

    // clear out all the measurements, etc.
    testProgram.startNewInspectionRun();
    clearPanelInspectionSettings();

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();

    try
    {
      algorithmSubtypeLearning.open();

      Collection<ImageSetData> typicalImageSetDataCollection = new ArrayList<ImageSetData>();
      Collection<ImageSetData> unloadedImageSetDataCollection = new ArrayList<ImageSetData>();

      for (ImageSetData imageSetData : imageSetDataList)
      {
        if (imageSetData.isBoardPopulated())
          typicalImageSetDataCollection.add(imageSetData);
        else
          unloadedImageSetDataCollection.add(imageSetData);
      }

      ManagedOfflineImageSet typicalPanelsManagedOfflineImageSet = new ManagedOfflineImageSet(testProgram, typicalImageSetDataCollection);
      ManagedOfflineImageSet unloadedPanelsManagedOfflineImageSet = new ManagedOfflineImageSet(testProgram, unloadedImageSetDataCollection);

      subtype.updateNominals(typicalPanelsManagedOfflineImageSet, unloadedPanelsManagedOfflineImageSet);

      typicalPanelsManagedOfflineImageSet.cleanUp();
      unloadedPanelsManagedOfflineImageSet.cleanUp();

    }
    finally
    {
      InspectionStateController.getInstance().reset();
      algorithmSubtypeLearning.close();
    }
  }

  /**
   * Notify GUI when a subtype is about to be learned so that the subtype and joint type can be displayed.
   *
   * @author Sunit Bhalla
   */
  private void sendSubtypeLearningStartedMessage(Subtype subtype)
  {
    InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
    InspectionEvent inspectionEvent = new SubtypeLearningStartedInspectionEvent(subtype);
    _inspectionEventObservable.sendEventInfo(inspectionEvent);
  }

    /*
   * @author Kee Chin Seong
   */
  private void sendApplyLearnedBoardShortProfileToBoard(Board learnedBoard, Board board)
  {
    Assert.expect(board != null);
    Assert.expect(learnedBoard != null);
    
    LocalizedString informationText = new LocalizedString("ALGDIAG_APPLYING_BOARD_BY_USING_LEARNED_BOARD_KEY",
                                                      new Object[] { learnedBoard.getName(),
                                                                     board.getName()});

    InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
    Assert.expect(_inspectionEventObservable != null);

    InspectionEvent inspectionEvent = new ApplyLearnedBoardShortProfileToOtherBoardEvent(informationText);
    _inspectionEventObservable.sendEventInfo(inspectionEvent);
  }


 /**
  * Notify GUI when a subtype has been learned so the progress bar can be updated.
  *
  * @author Sunit Bhalla
  */
 private void sendSubtypeLearningCompletedMessage()
 {
   InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
   InspectionEvent inspectionEvent = new SubtypeLearningCompletedInspectionEvent();
   _inspectionEventObservable.sendEventInfo(inspectionEvent);
 }

   /**
   * Notify GUI how many subtypes are going to be learned
   *
   * @author Sunit Bhalla
   */
  private void sendInitialTuningStartMessage(int numberOfSubtypesToLearn)
  {
    InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
    InspectionEvent inspectionEvent = new InitialTuningStartedInspectionEvent(numberOfSubtypesToLearn);
    _inspectionEventObservable.sendEventInfo(inspectionEvent);
  }


 /**
  * Notify GUI when learning is complete
  *
  * @author Sunit Bhalla
  */
 private void sendInitialTuningCompletedMessage()
 {
   InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
   InspectionEvent inspectionEvent = new InitialTuningCompletedInspectionEvent();
   _inspectionEventObservable.sendEventInfo(inspectionEvent);
 }

 /**
  * Notify GUI when a subtype's learned data is about to be deleted so that the subtype and joint type can be
  * displayed (if desired).
  *
  * @author Sunit Bhalla
  */
 private void sendSubtypeDeletingLearnedDataStartedMessage(Subtype subtype)
 {
   InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
   InspectionEvent inspectionEvent = new SubtypeDeletingLearnedDataStartedInspectionEvent(subtype);
   _inspectionEventObservable.sendEventInfo(inspectionEvent);
 }


/**
 * Notify GUI when a subtype's learned data has been deleted so the progress bar can be updated.
 *
 * @author Sunit Bhalla
 */
private void sendSubtypeDeletingLearnedDataCompletedMessage()
{
  InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
  InspectionEvent inspectionEvent = new SubtypeDeletingLearnedDataCompletedInspectionEvent();
  _inspectionEventObservable.sendEventInfo(inspectionEvent);
}

  /**
  * Notify GUI how many subtypes are going to have their learned data deleted.
  *
  * @author Sunit Bhalla
  */
 private void sendDeletingLearnedDataStartMessage(int numberOfSubtypesToDelete)
 {
   InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
   InspectionEvent inspectionEvent = new DeletingLearnedDataStartedInspectionEvent(numberOfSubtypesToDelete);
   _inspectionEventObservable.sendEventInfo(inspectionEvent);
 }


 /**
  * Notify GUI when deleting learned data is completed.
  *
  * @author Sunit Bhalla
  */
 private void sendDeletingLearnedDataCompletedMessage()
 {
   InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();
   InspectionEvent inspectionEvent = new DeletingLearnedDataCompletedInspectionEvent();
   _inspectionEventObservable.sendEventInfo(inspectionEvent);
 }

 /**
  * @author Sunit Bhalla
  */
 private void clearPanelInspectionSettings()
 {
   PanelInspectionSettings panelInspectionSettings = PanelInspectionSettings.getInstance();
   panelInspectionSettings.clearAllInspectionSettings();
   InspectionEngine.getInstance().setPanelInspectionSettings(panelInspectionSettings);
 }

}
