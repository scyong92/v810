package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Matt Wharton
 */
public class SingleDimensionProfileDiagnosticInfo extends ProfileDiagnosticInfo
{
  private List<Pair<MeasurementRegionEnum, Float>> _data = new LinkedList<Pair<MeasurementRegionEnum, Float>>();

  /**
   * @author Matt Wharton
   */
  public SingleDimensionProfileDiagnosticInfo(String name,
                                              ProfileTypeEnum profileTypeEnum,
                                              LocalizedString axisDescription,
                                              float dataMin,
                                              float dataMax,
                                              MeasurementUnitsEnum profileUnits)
  {
    super(name,
          profileTypeEnum,
          1,
          Collections.singletonList(axisDescription),
          Collections.singletonList(new Pair<Float, Float>(dataMin, dataMax)),
          profileUnits);

    Assert.expect(dataMin <= dataMax, "Profile axis minimum exceeds the maximum!");
  }

  /**
   * @author Matt Wharton
   */
  public List<Pair<MeasurementRegionEnum, Float>> getData()
  {
    Assert.expect(_data != null);

    return _data;
  }

  /**
   * @author Matt Wharton
   */
  public void addDataPoint(MeasurementRegionEnum measurementRegionEnum, float value)
  {
    Assert.expect(measurementRegionEnum != null);
    Assert.expect(_data != null);

    _data.add(new Pair<MeasurementRegionEnum, Float>(measurementRegionEnum, value));
  }

  /**
   * Clears out the raw data contained in this ProfileDiagnosticInfo.
   *
   * @author Matt Wharton
   */
  void clearData()
  {
    Assert.expect(_data != null);

    _data.clear();
  }
}
