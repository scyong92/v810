package com.axi.v810.business.imageAnalysis;

import java.awt.*;
import java.awt.geom.*;
import java.io.*;

import com.axi.util.*;

/**
 * Test class for MeasurementRegionDiagnosticInfo.
 *
 * @author Matt Wharton
 * @version 1.0
 */
public class Test_MeasurementRegionDiagnosticInfo extends UnitTest
{
  /**
   * Constructor.
   *
   * @author Matt Wharton
   */
  public Test_MeasurementRegionDiagnosticInfo()
  {
    // Do nothing.
  }

  /**
   * Tests the 'happy path'.
   *
   * @author Matt Wharton
   */
  private void testHappyPath()
  {
    // Test that the constructor sets things up properly.
    java.awt.Shape region1 = new Rectangle2D.Float();
    MeasurementRegionEnum measurementRegionEnum1 = MeasurementRegionEnum.PAD_REGION;
    MeasurementRegionDiagnosticInfo measurementRegionDiagInfo = new MeasurementRegionDiagnosticInfo(
      region1,
      measurementRegionEnum1);

    Expect.expect(measurementRegionDiagInfo.getDiagnosticRegion() == region1, "Regions don't match!");
    Expect.expect(
      measurementRegionDiagInfo.getMeasurementRegionEnum() == measurementRegionEnum1,
      "Region enums don't match!");

    // Test that the setter methods are working properly.
    java.awt.Shape region2 = new Ellipse2D.Float();
    measurementRegionDiagInfo.setDiagnosticRegion(region2);
    Expect.expect(measurementRegionDiagInfo.getDiagnosticRegion() == region2, "Regions don't match!");

    MeasurementRegionEnum measurementRegionEnum2 = MeasurementRegionEnum.PAD_REGION;
    measurementRegionDiagInfo.setMeasurementRegionEnum(measurementRegionEnum2);
    Expect.expect(
      measurementRegionDiagInfo.getMeasurementRegionEnum() == measurementRegionEnum2,
      "Region enums don't match!");
  }

  /**
   * Tests that the asserts are working properly.
   *
   * @author Matt Wharton
   */
  private void testAsserts()
  {
    // Test that the constructor asserts when it receives null parameters.
    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MeasurementRegionDiagnosticInfo((Shape)null, MeasurementRegionEnum.PAD_REGION);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        new MeasurementRegionDiagnosticInfo(new Rectangle2D.Float(), null);
      }
    });

    // Test that the setters assert when passed nulls.
    final MeasurementRegionDiagnosticInfo measurementRegionDiagInfo = new MeasurementRegionDiagnosticInfo(
        new Rectangle2D.Float(),
        MeasurementRegionEnum.PAD_REGION);

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        measurementRegionDiagInfo.setDiagnosticRegion(null);
      }
    });

    Expect.expectAssert(new RunnableWithExceptions()
    {
      public void run()
      {
        measurementRegionDiagInfo.setMeasurementRegionEnum(null);
      }
    });
  }

  /**
   * Main test method.
   *
   * @param in The input stream.
   * @param out The output stream.
   * @author Matt Wharton
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    // Test the 'happy path'.
    testHappyPath();

    // Test the asserts.
    testAsserts();
  }

  /**
   * Main entry point for test fixture.
   *
   * @author Matt Wharton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_MeasurementRegionDiagnosticInfo());
  }
}
