package com.axi.v810.business.imageAnalysis;

import java.io.*;
import java.util.*;

import com.axi.util.*;


/**
 * A SettingEnum is used to identify each individual Setting within an Algorithm.
 *
 * The name strings are what are shown to the user.  The keys that are written to our internal project files
 * for these algorithm settings are in EnumStringLookup.
 *
 * If you make a modification to the names here, be sure to change:
 * com.axi.v810.datastore.projWriters.EnumStringLookup
 *
 * @author Peter Esbensen
 * @author Sunit Bhalla
 */
public class AlgorithmSettingEnum extends com.axi.util.Enum implements Serializable, Comparable
{
  private static Map<String, AlgorithmSettingEnum> _nameToAlgorithmSettingEnumMap = new TreeMap<String, AlgorithmSettingEnum>(new AlphaNumericComparator());

  private static int _index = -1;

  public static final AlgorithmSettingEnum TEMP_SETTING = new AlgorithmSettingEnum(++_index, "Search Distance");

  // GRID ARRAY : MEASUREMENT
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PAD = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_LOWERPAD = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_MIDBALL = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_PACKAGE = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - Package");

  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_PROFILE_SEARCH_DISTANCE = new AlgorithmSettingEnum(++_index, "Profile Search Distance");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Edge Detection Technique");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_ENABLE_JOINT_BASED_EDGE_DETECTION = new AlgorithmSettingEnum(++ _index, "Enable Joint Based Edge Detection");
  
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD = new AlgorithmSettingEnum(++_index, "Nominal Diameter - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Nominal Diameter - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PAD = new AlgorithmSettingEnum(++_index, "Nominal Thickness - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_LOWERPAD = new AlgorithmSettingEnum(++_index, "Nominal Thickness - Lower Pad");

  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL = new AlgorithmSettingEnum(++_index, "Nominal Diameter - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_MIDBALL = new AlgorithmSettingEnum(++_index, "Nominal Thickness - Midball");

  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE = new AlgorithmSettingEnum(++_index, "Nominal Diameter - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_PACKAGE = new AlgorithmSettingEnum(++_index, "Nominal Thickness - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_THICKNESS_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Nominal Thickness - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_INNER_EDGE_LOCATION = new AlgorithmSettingEnum(++_index, "Background Region Inner Edge Location");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_BACKGROUND_REGION_OUTER_EDGE_LOCATION = new AlgorithmSettingEnum(++_index, "Background Region Outer Edge Location");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_USE_FIXED_ECCENTRICITY_AXIS = new AlgorithmSettingEnum(++_index, "Use Fixed Eccentricity Axis");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_FIXED_ECCENTRICITY_AXIS = new AlgorithmSettingEnum(++_index, "Fixed Eccentricity Axis");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_CHECK_ALL_SLICES_FOR_SHORT = new AlgorithmSettingEnum(++_index, "Check All Slices For Shorts");
  // added by wei chin 14-Jul
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_CONTRAST_ENHANCEMENT = new AlgorithmSettingEnum(++_index, "Contrast Enhancement");

  // Added by Lee Herng 5-April-2012
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_EDGE_DETECTION_PERCENT_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Edge Detection Percent - User Defined Slice 20");
  
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Nominal Diameter - User Defined Slice 20");

  // public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_VARIABLE_HEIGHT_CONNECTOR_PAD_SLICE_OFFSET_OBSOLETE = new AlgorithmSettingEnum(++_index, "Obsolete Pad Slice Offset");

  // GRID ARRAY : OPEN
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PAD = new AlgorithmSettingEnum(++_index, "Minimum Diameter - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Minimum Diameter - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_MIDBALL = new AlgorithmSettingEnum(++_index, "Minimum Diameter - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_PACKAGE = new AlgorithmSettingEnum(++_index, "Minimum Diameter - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_DIAMETER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Minimum Diameter - User Defined Slice 20");
  
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PAD = new AlgorithmSettingEnum(++_index, "Maximum Diameter - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Maximum Diameter - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_MIDBALL = new AlgorithmSettingEnum(++_index, "Maximum Diameter - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_PACKAGE = new AlgorithmSettingEnum(++_index, "Maximum Diameter - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_DIAMETER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Maximum Diameter - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PAD = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_MIDBALL = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_PACKAGE = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Outlier - User Defined Slice 20");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PAD = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_MIDBALL = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_PACKAGE = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_NEIGHBOR_OUTLIER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Minimum Neighbor Outlier - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Thickness");

  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PAD = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_MIDBALL = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_PACKAGE = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Maximum Region Outlier - User Defined Slice 20");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PAD = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_MIDBALL = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_PACKAGE = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_REGION_OUTLIER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Minimum Region Outlier - User Defined Slice 20");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_PERCENT_JOINTS_WITH_INSUFF_DIAMETER = new AlgorithmSettingEnum(++_index, "Maximum Percent of Joints with Marginal Diameter");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMIM_DIAMETER_MARGINAL = new AlgorithmSettingEnum(++_index, "Minimum Marginal Diameter - Midball");

  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_PAD = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_PAD = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_LOWERPAD = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_LOWERPAD = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_MIDBALL = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_MIDBALL = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Minimum Eccentricity - User Defined Slice 20");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_ECCENTRICITY_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Maximum Eccentricity - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_VOID_AREA_LIMIT_FOR_NEIGHBOR_OUTLIER = new AlgorithmSettingEnum(++_index, "Void Area Limit for Neighbor Outlier");

  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_PAD = new AlgorithmSettingEnum(++_index, "Maximum HIP Outlier - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_PAD = new AlgorithmSettingEnum(++_index, "Minimum HIP Outlier - Pad");

  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MULTIPASS_INSPECTION = new AlgorithmSettingEnum(++_index, "Multipass Inspection");
  
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MAXIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum( ++_index, "Maximum HIP Outlier User Defined Slice 20");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_OPEN_MINIMUM_HIP_OUTLIER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum( ++_index, "Minimum HIP Outlier User Defined Slice 20");
  //Chin Seong, Kee - GULLWING : EXCESS
  public static final AlgorithmSettingEnum GULLWING_EXCESS_MAX_FILLET_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Maximum Fillet Thickness");
  public static final AlgorithmSettingEnum GULLWING_EXCESS_MAX_HEEL_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Maximum Heel Thickness");
  public static final AlgorithmSettingEnum GULLWING_EXCESS_MAX_TOE_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Maximum Toe Thickness");

  // GRID ARRAY : MISALIGNMENT
  public static final AlgorithmSettingEnum GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_EXPECTED = new AlgorithmSettingEnum(++_index, "Maximum Offset from Region Joints");
  public static final AlgorithmSettingEnum GRID_ARRAY_MISALIGNMENT_MAXIMUM_OFFSET_FROM_CAD_POSITION = new AlgorithmSettingEnum(++_index, "Maximum Offset from CAD Position");

  // GRID ARRAY : VOIDING
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PAD = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_LOWERPAD = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_PACKAGE = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_MIDBALL = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_PERCENT_OF_DIAMETER_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Percent of Diameter to Test - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PAD = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_LOWERPAD = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_PACKAGE = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_MIDBALL = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_THICKNESS_THRESHOLD_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_METHOD = new AlgorithmSettingEnum(++_index, "Void Method");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_BORDER_THRESHOLD = new AlgorithmSettingEnum(++_index, "Void Border Threshold");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD = new AlgorithmSettingEnum(++_index, "Void FloodFill Sensitivity Threshold");

  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PAD = new AlgorithmSettingEnum(++_index, "Void Area Threshold - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PAD = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PAD = new AlgorithmSettingEnum(++_index, "Individual Void Diameter Threshold - Pad"); //Siew Yeng - XCR-3515
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_LOWERPAD = new AlgorithmSettingEnum(++_index, "Void Area Threshold - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_LOWERPAD = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_PACKAGE = new AlgorithmSettingEnum(++_index, "Void Area Threshold - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_PACKAGE = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_PACKAGE = new AlgorithmSettingEnum(++_index, "Individual Void Diameter Threshold - Package"); //Siew Yeng - XCR-3515
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_MIDBALL = new AlgorithmSettingEnum(++_index, "Void Area Threshold - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_MIDBALL = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_DIAMETER_MIDBALL = new AlgorithmSettingEnum(++_index, "Individual Void Diameter Threshold - Midball"); //Siew Yeng - XCR-3515
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Void Area Threshold - User Defined Slice 20");
  public static final AlgorithmSettingEnum GRID_ARRAY_INDIVIDUAL_VOIDING_FAIL_JOINT_AREA_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Individual Void Area Threshold - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PAD = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_LOWERPAD = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_PACKAGE = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_MIDBALL = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_MARGINAL_VOID_AREA_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Marginal Void Area Threshold - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PAD = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_LOWERPAD = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - Lower Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_PACKAGE = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_MIDBALL = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 1");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 2");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 3");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 4");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 5");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 6");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 7");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 8");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 9");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 10");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 11");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 12");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 13");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 14");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 15");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 16");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 17");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 18");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 19");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_FAIL_COMPONENT_PERCENT_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "Percent of Marginal Voids to Fail Component - User Defined Slice 20");

  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_NOISE_REDUCTION = new AlgorithmSettingEnum(++_index, "Level of Noise Reduction");
  public static final AlgorithmSettingEnum GRID_ARRAY_VOIDING_NUMBER_OF_IMAGES_TO_SAMPLE = new AlgorithmSettingEnum(++_index, "Number of Images to Sample");

  public static final AlgorithmSettingEnum GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PAD = new AlgorithmSettingEnum(++_index, "Minimum Percentage of Nominal Thickness - Pad");
  public static final AlgorithmSettingEnum GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_PACKAGE = new AlgorithmSettingEnum(++_index, "Minimum Percentage of Nominal Thickness - Package");
  public static final AlgorithmSettingEnum GRID_ARRAY_INSUFFICIENT_MINIMUM_PERCENT_OF_NOMINAL_THICKNESS_MIDBALL = new AlgorithmSettingEnum(++_index, "Minimum Percentage of Nominal Thickness - Midball");
  
  public static final AlgorithmSettingEnum GRID_ARRAY_NUMBER_OF_USER_DEFINED_SLICE = new AlgorithmSettingEnum(++_index, "Number of User Defined Slice");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "User Defined Slice 1 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "User Defined Slice 2 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_3 = new AlgorithmSettingEnum(++_index, "User Defined Slice 3 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_4 = new AlgorithmSettingEnum(++_index, "User Defined Slice 4 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_5 = new AlgorithmSettingEnum(++_index, "User Defined Slice 5 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_6 = new AlgorithmSettingEnum(++_index, "User Defined Slice 6 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_7 = new AlgorithmSettingEnum(++_index, "User Defined Slice 7 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_8 = new AlgorithmSettingEnum(++_index, "User Defined Slice 8 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_9 = new AlgorithmSettingEnum(++_index, "User Defined Slice 9 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_10 = new AlgorithmSettingEnum(++_index, "User Defined Slice 10 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_11 = new AlgorithmSettingEnum(++_index, "User Defined Slice 11 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_12 = new AlgorithmSettingEnum(++_index, "User Defined Slice 12 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_13 = new AlgorithmSettingEnum(++_index, "User Defined Slice 13 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_14 = new AlgorithmSettingEnum(++_index, "User Defined Slice 14 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_15 = new AlgorithmSettingEnum(++_index, "User Defined Slice 15 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_16 = new AlgorithmSettingEnum(++_index, "User Defined Slice 16 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_17 = new AlgorithmSettingEnum(++_index, "User Defined Slice 17 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_18 = new AlgorithmSettingEnum(++_index, "User Defined Slice 18 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_19 = new AlgorithmSettingEnum(++_index, "User Defined Slice 19 Offset from Midball");
  public static final AlgorithmSettingEnum GRID_ARRAY_USER_DEFINED_SLICE_20 = new AlgorithmSettingEnum(++_index, "User Defined Slice 20 Offset from Midball");
  public static final AlgorithmSettingEnum MAXIMUM_NUMBER_PINS_FAILED_MIDBALL_VOID = new AlgorithmSettingEnum(++_index, "Maximum Number of Pins fail midball void");
  
  // GULLWING : MEASUREMENT
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD = new AlgorithmSettingEnum(++_index, "Void Compensation Method");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION = new AlgorithmSettingEnum(++_index, "Heel Void Compensation");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY = new AlgorithmSettingEnum(++_index, "Smooth Sensitivity");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH = new AlgorithmSettingEnum(++_index, "Pad Profile Width");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH = new AlgorithmSettingEnum(++_index, "Pad Profile Length");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION = new AlgorithmSettingEnum(++_index, "Background Region Location");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET = new AlgorithmSettingEnum(++_index, "Background Region Location Offset");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Heel Edge Search Thickness");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE = new AlgorithmSettingEnum(++_index, "Heel Search Distance From Edge");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Toe Edge Search Thickness");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_PIN_LENGTH = new AlgorithmSettingEnum(++_index, "Pin Length");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_CENTER_OFFSET = new AlgorithmSettingEnum(++_index, "Center Offset from Heel");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH = new AlgorithmSettingEnum(++_index, "Nominal Fillet Length");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Fillet Thickness");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Heel Thickness");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_NOMINAL_TOE_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Toe Thickness");
  public static final AlgorithmSettingEnum GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION = new AlgorithmSettingEnum(++_index, "Nominal Heel Position");

  // GULLWING : OPEN
  public static final AlgorithmSettingEnum GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Heel Thickness");
  public static final AlgorithmSettingEnum GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Fillet Length");
  public static final AlgorithmSettingEnum GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Maximum Fillet Length");
  public static final AlgorithmSettingEnum GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Center to Heel Thickness Percent");
  //Siew Yeng - XCR-3532
  public static final AlgorithmSettingEnum GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Maximum Fillet Length Region Outlier");
  public static final AlgorithmSettingEnum GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Minimum Fillet Length Region Outlier");
  public static final AlgorithmSettingEnum GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Maximum Center Thickness Region Outlier");
  public static final AlgorithmSettingEnum GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Minimum Center Thickness Region Outlier");
  
  // GULLWING : INSUFFICIENT
  public static final AlgorithmSettingEnum GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Fillet Thickness");

  // GULLWING : MISALIGNMENT
  public static final AlgorithmSettingEnum GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD = new AlgorithmSettingEnum(++_index, "Maximum Joint Offset from CAD");
  public static final AlgorithmSettingEnum GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL = new AlgorithmSettingEnum(++_index, "Maximum Heel Position Distance from Nominal");
  public static final AlgorithmSettingEnum GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS = new AlgorithmSettingEnum(++_index, "Maximum Fillet Peak Offset From Region Neighbors");

  // ADVANCED GULLWING : MEASUREMENT
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD = GULLWING_MEASUREMENT_VOID_COMPENSATION_METHOD;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION = GULLWING_MEASUREMENT_HEEL_VOID_COMPENSATION;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY = GULLWING_MEASUREMENT_SMOOTH_SENSITIVITY;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH = GULLWING_MEASUREMENT_PAD_PROFILE_WIDTH;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH = GULLWING_MEASUREMENT_PAD_PROFILE_LENGTH;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION = GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET = GULLWING_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT = GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE = GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT = GULLWING_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_CENTER_OFFSET = GULLWING_MEASUREMENT_CENTER_OFFSET;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_LOCATION = new AlgorithmSettingEnum(++_index, "Across Profile Location");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_ACROSS_PROFILE_WIDTH = new AlgorithmSettingEnum(++_index, "Across Profile Width");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_EDGE_SEARCH_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Side Fillet Edge Thickness");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_HEEL_SEARCH_DISTANCE_FROM_EDGE = new AlgorithmSettingEnum(++_index, "Side Fillet Search Distance");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_SIDE_FILLET_CENTER_OFFSET_FROM_EDGE = new AlgorithmSettingEnum(++_index, "Side Fillet Center Offset");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH = GULLWING_MEASUREMENT_NOMINAL_FILLET_LENGTH;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS = GULLWING_MEASUREMENT_NOMINAL_FILLET_THICKNESS;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS = GULLWING_MEASUREMENT_NOMINAL_HEEL_THICKNESS;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION = GULLWING_MEASUREMENT_NOMINAL_HEEL_POSITION;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_ENABLE_BACKGROUND_REGIONS = new AlgorithmSettingEnum(++_index, "Enable Background Regions");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_BACKGROUND_REGION_WIDTH = new AlgorithmSettingEnum(++_index, "Background Region Width");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_NOMINAL_HEEL_TO_PAD_CENTER = new AlgorithmSettingEnum(++_index, "Nominal Heel to Pad Center");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MEASUREMENT_PAD_CENTER_REFERENCE = new AlgorithmSettingEnum(++_index, "Pad Center Reference");

  // ADVANCED GULLWING : OPEN
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL = GULLWING_OPEN_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL = GULLWING_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL = GULLWING_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT = GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_SLOPE = new AlgorithmSettingEnum(++_index, "Minimum Heel Slope");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MINIMUM_TOE_SLOPE = new AlgorithmSettingEnum(++_index, "Minimum Toe Slope");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Minimum Sum of Heel and Toe Slopes");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MINIMUM_SUM_OF_SLOPE_CHANGES = new AlgorithmSettingEnum(++_index, "Minimum Sum of Slope Changes");
  //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_SLOPE = new AlgorithmSettingEnum(++_index, "Maximum Heel Slope");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAXIMUM_TOE_SLOPE = new AlgorithmSettingEnum(++_index, "Maximum Toe Slope");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Maximum Sum of Heel and Toe Slopes");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAXIMUM_SUM_OF_SLOPE_CHANGES = new AlgorithmSettingEnum(++_index, "Maximum Sum of Slope Changes");
  //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_THICKNESS_PERCENT_ACROSS = new AlgorithmSettingEnum(++_index, "Across Maximum Center to Heel Thickness Percent");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAX_CENTER_TO_HEEL_2_THICKNESS_PERCENT_ACROSS = new AlgorithmSettingEnum(++_index, "Across Maximum Center to Heel 2 Thickness Percent");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MINIMUM_CONCAVITY_RATIO_ACROSS = new AlgorithmSettingEnum(++_index, "Across Minimum Concavity Ratio");
  //Siew Yeng - XCR-3532
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Maximum Fillet Length Region Outlier");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Minimum Fillet Length Region Outlier");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Maximum Center Thickness Region Outlier");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Minimum Center Thickness Region Outlier");
  
  // ADVANCED GULLWING : INSUFFICIENT
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL = GULLWING_INSUFFICIENT_MIN_THICKNESS_PERCENT_OF_NOMINAL;

  // ADVANCED GULLWING : MISALIGNMENT
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD = GULLWING_MISALIGNMENT_MAXIMUM_JOINT_OFFSET_FROM_CAD;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL = GULLWING_MISALIGNMENT_MAXIMUM_HEEL_POSITION_DISTANCE_FROM_NOMINAL;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS = GULLWING_MISALIGNMENT_MAXIMUM_OFFSET_FROM_REGION_NEIGHBORS;
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MISALIGNMENT_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL = new AlgorithmSettingEnum(++_index, "Heel To Pad Center Difference from Nominal");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_MISALIGNMENT_MAXIMUM_MASS_SHIFT_ACROSS_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Mass Shift Across Percent");

  // SHARED : MEASUREMENT (LOCATOR)
  public static final AlgorithmSettingEnum LOCATE_EFFECTIVE_WIDTH = new AlgorithmSettingEnum(++_index, "Effective Width");
  public static final AlgorithmSettingEnum LOCATE_EFFECTIVE_LENGTH = new AlgorithmSettingEnum(++_index, "Effective Length");
  public static final AlgorithmSettingEnum LOCATE_EFFECTIVE_SHIFT_ACROSS = new AlgorithmSettingEnum(++_index, "Effective Shift Across");
  public static final AlgorithmSettingEnum LOCATE_EFFECTIVE_SHIFT_ALONG = new AlgorithmSettingEnum(++_index, "Effective Shift Along");
  public static final AlgorithmSettingEnum LOCATE_SEARCH_WINDOW_ACROSS = new AlgorithmSettingEnum(++_index, "Search Window Across");
  public static final AlgorithmSettingEnum LOCATE_SEARCH_WINDOW_ALONG = new AlgorithmSettingEnum(++_index, "Search Window Along");
  public static final AlgorithmSettingEnum LOCATE_BORDER_WIDTH = new AlgorithmSettingEnum(++_index, "Border Size");
  public static final AlgorithmSettingEnum LOCATE_SNAPBACK_DISTANCE = new AlgorithmSettingEnum(++_index, "Snapback Distance");
  public static final AlgorithmSettingEnum LOCATE_METHOD = new AlgorithmSettingEnum(++_index, "Locate Method");
  
  //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE = new AlgorithmSettingEnum(++_index, "Component Across Profile Size");

  // SHARED : SHORT
  public static final AlgorithmSettingEnum SHARED_SHORT_INSPECTION_SLICE = new AlgorithmSettingEnum(++_index, "Inspection Slice");
  public static final AlgorithmSettingEnum SHARED_SHORT_HIGH_SHORT_DETECTION = new AlgorithmSettingEnum(++_index, "High Short Detection");
  public static final AlgorithmSettingEnum SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION = new AlgorithmSettingEnum(++_index, "Protrusion High Short Detection");
  public static final AlgorithmSettingEnum SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "High Short Slice Height");
  public static final AlgorithmSettingEnum SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS = new AlgorithmSettingEnum(++_index, "High Short Minimum Thickness");
  public static final AlgorithmSettingEnum SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS = new AlgorithmSettingEnum(++_index, "Protrusion High Short Minimum Thickness");
  public static final AlgorithmSettingEnum SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS = new AlgorithmSettingEnum(++_index, "Projection Short Minimum Thickness");//Broken Pin
  public static final AlgorithmSettingEnum SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS = new AlgorithmSettingEnum(++_index, "Maximum Short Thickness");
  public static final AlgorithmSettingEnum SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS = new AlgorithmSettingEnum(++_index, "Component Side Maximum Short Thickness");
  public static final AlgorithmSettingEnum SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS = new AlgorithmSettingEnum(++_index, "Pin Side Maximum Short Thickness");
  public static final AlgorithmSettingEnum SHARED_SHORT_REGION_INNER_EDGE_LOCATION = new AlgorithmSettingEnum(++_index, "Region Inner Edge Location");
  public static final AlgorithmSettingEnum SHARED_SHORT_REGION_OUTER_EDGE_LOCATION = new AlgorithmSettingEnum(++_index, "Region Outer Edge Location");
  public static final AlgorithmSettingEnum SHARED_SHORT_REGION_REFERENCE_POSITION = new AlgorithmSettingEnum(++_index, "Region Reference Position");
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_TEST_REGION_HEAD = new AlgorithmSettingEnum(++_index, "Enable Test Region Head");
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_TEST_REGION_RIGHT = new AlgorithmSettingEnum(++_index, "Enable Test Region Right");
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_TEST_REGION_TAIL = new AlgorithmSettingEnum(++_index, "Enable Test Region Tail");
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_TEST_REGION_LEFT = new AlgorithmSettingEnum(++_index, "Enable Test Region Left");
  public static final AlgorithmSettingEnum SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION = new AlgorithmSettingEnum(++_index, "Minimum Pixels for Short ROI Region");
  //Siew Yeng - XCR-3318 - Oval PTH
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS = new AlgorithmSettingEnum(++_index, "Enable Test Region 4 Corners ");
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER = new AlgorithmSettingEnum(++_index, "Enable Test Region Inner Corner");
  
  //Lim, Lay Ngor - XCR1743:Benchmark
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_TEST_REGION_CENTER = new AlgorithmSettingEnum(++_index, "Enable Test Region Center");  
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_MULTI_ROI_CENTER_REGION = new AlgorithmSettingEnum(++_index, "Enable Multiple ROI Center Region");    
  public static final AlgorithmSettingEnum SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Center Solder Ball Short Thickness");
  public static final AlgorithmSettingEnum SHARED_SHORT_CENTER_EFFECTIVE_LENGTH = new AlgorithmSettingEnum(++_index, "Center Effective Length");
  public static final AlgorithmSettingEnum SHARED_SHORT_CENTER_EFFECTIVE_WIDTH = new AlgorithmSettingEnum(++_index, "Center Effective Width");
  //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
  public static final AlgorithmSettingEnum SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH = new AlgorithmSettingEnum(++_index, "Maximum Short Length");  
  //Lim, Lay Ngor - XCR2100 - Broken Pin
  public static final AlgorithmSettingEnum SHARED_SHORT_ENABLE_BROKEN_PIN = new AlgorithmSettingEnum(++_index, "Enable Broken Pin");
  public static final AlgorithmSettingEnum SHARED_SHORT_BROKEN_PIN_AREA_SIZE = new AlgorithmSettingEnum(++_index, "Broken Pin Area Size");
  public static final AlgorithmSettingEnum SHARED_SHORT_BROKEN_PIN_THRESHOLD_RATIO = new AlgorithmSettingEnum(++_index, "Broken Pin Threshold Ratio");  
  
  // THROUGHHOLE : MEASUREMENT
  // PE:  Some of these throughhole sliceheight settings are obsoleted by the User Defined sliceheights feature.
  //  I have left them here though because I use them when automatically converting old programs.
  // public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICE_OBSOLETE = new AlgorithmSettingEnum(++_index, "Barrel Slice Position");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_2_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 2");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_3_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 3");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_4_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 4");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_5_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 5");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_6_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 6");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_7_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 7");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_8_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 8");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_9_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 9");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_BARREL_10_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Barrel Slice 10");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_PINSIDE_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Pin Side");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_PROJECTION_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - 2D/Projection Slice");//Broken Pin
  public static final AlgorithmSettingEnum THROUGHHOLE_PERCENT_OF_DIAMETER_TO_TEST = new AlgorithmSettingEnum(++_index, "Percent Diameter for Pin Side & Barrel Slices");
  public static final AlgorithmSettingEnum THROUGHHOLE_BACKGROUND_REGION_INNER_EDGE_LOCATION = new AlgorithmSettingEnum(++_index, "Background Region Inner Edge Location");;
  public static final AlgorithmSettingEnum THROUGHHOLE_BACKGROUND_REGION_OUTER_EDGE_LOCATION = new AlgorithmSettingEnum(++_index, "Background Region Outer Edge Location");
  public static final AlgorithmSettingEnum THROUGHHOLE_EXTRUSION_SLICE_OBSOLETE = new AlgorithmSettingEnum(++_index, "Extrusion Slice Position");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_PROTRUSION_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Protrusion");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_INSERTION_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Insertion");
  public static final AlgorithmSettingEnum THROUGHHOLE_PROTRUSION_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Protrusion Slice Height");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSERTION_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Insertion Slice Height");
  public static final AlgorithmSettingEnum THROUGHHOLE_PIN_DETECTION = new AlgorithmSettingEnum(++_index, "Pin Detection");
  public static final AlgorithmSettingEnum THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Component Side Short Slice Height");
  public static final AlgorithmSettingEnum THROUGHHOLE_PIN_SIDE_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Pin Side Slice Height");
  public static final AlgorithmSettingEnum THROUGHHOLE_PIN_WIDTH = new AlgorithmSettingEnum(++_index, "Pin Diameter");
  public static final AlgorithmSettingEnum THROUGHHOLE_PIN_SEARCH_DIAMETER_PERCENT_OF_BARREL = new AlgorithmSettingEnum(++_index, "Percent Diameter for Insertion & Protrusion Slices");
  //Siew Yeng - XCR-3318 - Oval PTH
  public static final AlgorithmSettingEnum THROUGHHOLE_PIN_SIZE_ALONG = new AlgorithmSettingEnum(++_index, "Pin Size Along");
  public static final AlgorithmSettingEnum THROUGHHOLE_PIN_SIZE_ACROSS = new AlgorithmSettingEnum(++_index, "Pin Size Across");
  
  // THROUHHOLE : OPEN
  public static final AlgorithmSettingEnum THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_PROTRUSION = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Protrusion");
  public static final AlgorithmSettingEnum THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_PROTRUSION = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Protrusion");
  public static final AlgorithmSettingEnum THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL_INSERTION = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Insertion");
  public static final AlgorithmSettingEnum THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION_INSERTION = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Insertion");


  // THROUHHOLE : INSUFFICIENT
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_2 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 2");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_3 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 3");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_4 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 4");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_5 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 5");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_6 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 6");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_7 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 7");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_8 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 8");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_9 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 9");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_BARREL_10 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Barrel Slice 10");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_PINSIDE = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Pin Side");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_ENABLE_WETTING_COVERAGE = new AlgorithmSettingEnum(++_index, "Enable Wetting Coverage");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE = new AlgorithmSettingEnum(++_index, "Wetting Coverage");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY = new AlgorithmSettingEnum(++_index, "Wetting Coverage Sensitivity");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE = new AlgorithmSettingEnum(++_index, "Voiding Percentage");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE = new AlgorithmSettingEnum(++_index, "Degree Coverage");
  
  /**
   * THROUGHHOLE: INSUFFICIENT - VOID VOLUME PERCENTAGE
   * @author Lim, Lay Ngor PIPA
   */
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE = new AlgorithmSettingEnum(++_index, "Void Volume Percentage");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_MEASUREMENT= new AlgorithmSettingEnum(++_index, "Enable Void Volume Measurement");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_ENABLE_VOID_VOLUME_DIRECTION_FROM_COMPONENT_TO_PIN= new AlgorithmSettingEnum(++_index, "Enable Void Volume Direction From Component Side To Pin Slice(PIP)");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL_COMPONENTSIDE = new AlgorithmSettingEnum(++_index, "Maximum Difference from Nominal Solder Signal - Component Side");  
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_COMPONENTSIDE = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Component Side");
  public static final AlgorithmSettingEnum THROUGHHOLE_NOMINAL_COMPONENTSIDE_SOLDER_SIGNAL = new AlgorithmSettingEnum(++_index, "Nominal Solder Signal - Component Side");
  
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_2 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 2");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_3 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 3");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_4 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 4");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_5 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 5");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_6 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 6");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_7 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 7");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_8 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 8");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_9 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 9");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_BARREL_10 = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Barrel Slice 10");
  public static final AlgorithmSettingEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION_PINSIDE = new AlgorithmSettingEnum(++_index, "Maximum Difference from Region Solder Signal - Pin Side");
  
  /**
   * @author Khaw Chek Hau
   * @XCR3745: Support PTH Excess Solder Algorithm
   */
  public static final AlgorithmSettingEnum THROUGHHOLE_PERCENT_OF_EXCESS_DIAMETER_TO_TEST = new AlgorithmSettingEnum(++_index, "Percent Diameter for Excess Barrel Slices");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_COMPONENTSIDE = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Component Side");  
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_2 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 2");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_3 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 3");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_4 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 4");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_5 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 5");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_6 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 6");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_7 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 7");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_8 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 8");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_9 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 9");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_BARREL_10 = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Barrel Slice 10");
  public static final AlgorithmSettingEnum THROUGHHOLE_MAXIMUM_EXCESS_SOLDER_SIGNAL_PINSIDE = new AlgorithmSettingEnum(++_index, "Maximum Excess Solder Signal - Pin Side");
  
  // PRESSFIT : MEASUREMENT
  public static final AlgorithmSettingEnum PRESSFIT_PIN_WIDTH = new AlgorithmSettingEnum(++_index, "Pin Width");
  // public static final AlgorithmSettingEnum PRESSFIT_BARREL_SLICE_OBSOLETE = new AlgorithmSettingEnum(++_index, "Barrel Slice Position");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_2_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 2");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_3_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 3");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_4_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 4");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_5_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 5");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_6_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 6");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_7_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 7");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_8_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 8");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_9_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 9");
  public static final AlgorithmSettingEnum PRESSFIT_NOMINAL_BARREL_10_CORRECTED_GRAYLEVEL = new AlgorithmSettingEnum(++_index, "Nominal Corrected Greylevel - Barrel Slice 10");
  
  // PRESSFIT : INSUFFICIENT
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_2 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 2");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_3 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 3");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_4 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 4");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_5 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 5");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_6 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 6");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_7 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 7");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_8 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 8");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_9 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 9");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_NOMINAL_BARREL_10 = new AlgorithmSettingEnum(++_index, "Maximum Nominal Difference - Barrel Slice 10");
  
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_2 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 2");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_3 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 3");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_4 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 4");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_5 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 5");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_6 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 6");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_7 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 7");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_8 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 8");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_9 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 9");
  public static final AlgorithmSettingEnum PRESSFIT_OPEN_MAXIMUM_FROM_REGION_BARREL_10 = new AlgorithmSettingEnum(++_index, "Maximum Region Difference - Barrel Slice 10");
  
  // CHIP : MEASUREMENT
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Fillet Thickness - Opaque");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Fillet Thickness - Clear");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE = new AlgorithmSettingEnum(++_index, "Body Thickness Required for Opaque Test");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH = new AlgorithmSettingEnum(++_index, "Nominal Component Body Length - Opaque");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH = new AlgorithmSettingEnum(++_index, "Nominal Component Body Length - Clear");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET = new AlgorithmSettingEnum(++_index, "Lower Fillet Location Offset");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH = new AlgorithmSettingEnum(++_index, "Open Signal Search Length");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET = new AlgorithmSettingEnum(++_index, "Upper Fillet Location Offset");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT = new AlgorithmSettingEnum(++_index, "Background Profile Location");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Component Side Search Thickness");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS = new AlgorithmSettingEnum(++_index, "Pad Percent To Measure Fillet Thickness");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX = new AlgorithmSettingEnum(++_index, "Maximum Component Tilt Ratio");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE = new AlgorithmSettingEnum(++_index, "Edge Location Technique - Opaque");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD = new AlgorithmSettingEnum(++_index, "Update Nominal Fillet Thickness Method");
  // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR = new AlgorithmSettingEnum(++_index, "Nominal Along Offset - Clear");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE = new AlgorithmSettingEnum(++_index, "Nominal Along Offset - Opaque");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION = new AlgorithmSettingEnum(++_index, "Estimated Maximum Rotation");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG = new AlgorithmSettingEnum(++_index, "Across Profile Offset from Upper Fillet");
  //Lim, Lay Ngor - Clear Tombstone
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_CENTER_OFFSET = new AlgorithmSettingEnum(++_index, "Center Offset From Upper Fillet - Clear");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD = new AlgorithmSettingEnum(++_index, "Upper Fillet Detection Method");  
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Fillet Edge Search Thickness");
  public static final AlgorithmSettingEnum CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE = new AlgorithmSettingEnum(++_index, "Upper Fillet Search Distance From Edge");

  // CHIP : OPEN
  public static final AlgorithmSettingEnum CHIP_OPEN_MINIMUM_BODY_LENGTH = new AlgorithmSettingEnum(++_index, "Minimum Component Body Length");
  public static final AlgorithmSettingEnum CHIP_OPEN_MAXIMUM_BODY_LENGTH = new AlgorithmSettingEnum(++_index, "Maximum Component Body Length");
  public static final AlgorithmSettingEnum CHIP_OPEN_OPAQUE_OPEN_SIGNAL = new AlgorithmSettingEnum(++_index, "Maximum Opaque Open Signal");
  public static final AlgorithmSettingEnum CHIP_OPEN_CLEAR_OPEN_SIGNAL = new AlgorithmSettingEnum(++_index, "Minimum Clear Open Signal");
  public static final AlgorithmSettingEnum CHIP_OPEN_MAXIMUM_FILLET_GAP = new AlgorithmSettingEnum(++_index, "Maximum Fillet Gap");
  public static final AlgorithmSettingEnum CHIP_OPEN_MINIMUM_BODY_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Body Thickness - Opaque");
  public static final AlgorithmSettingEnum CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Body Thickness - Clear");
  public static final AlgorithmSettingEnum CHIP_OPEN_MINIMUM_BODY_WIDTH = new AlgorithmSettingEnum(++_index, "Minimum Component Width Across");
  //Lim, Lay Ngor - Clear Tombstone
  public static final AlgorithmSettingEnum CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE = new AlgorithmSettingEnum(++_index, "Maximum Center to Upper Fillet Thickness Percent - Clear");
  public static final AlgorithmSettingEnum CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE = new AlgorithmSettingEnum(++_index, "Maximum Solder Area Percentage - Clear");
  //sheng chuan pattern match - Clear Tombstone
  public static final AlgorithmSettingEnum CHIP_OPEN_DETECTION_METHOD = new AlgorithmSettingEnum(++_index, "Clear Open Second Level Detection Method");//Original name as "Matching Method"  
  public static final AlgorithmSettingEnum CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE  = new AlgorithmSettingEnum(++_index, "Minimum Template Matching Percentage");
  public static final AlgorithmSettingEnum CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE = new AlgorithmSettingEnum(++_index, "Maximum Roundness Percentage");
  //Lim, Lay Ngor - Opaque Tombstone
  public static final AlgorithmSettingEnum CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION = new AlgorithmSettingEnum(++_index, "Enable Opaque Fillet Width Detection");//XCR-3359
  public static final AlgorithmSettingEnum CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO = new AlgorithmSettingEnum(++_index, "Minimum Fillet Width Across Joint Ratio - Opaque");  
  public static final AlgorithmSettingEnum CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE = new AlgorithmSettingEnum(++_index, "Minimum Partial Fillet Thickness Percentage - Opaque");  
  
  // CHIP : MISALIGNMENT
  // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
  public static final AlgorithmSettingEnum CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG = new AlgorithmSettingEnum(++_index, "Maximum Component Shift Along");
  public static final AlgorithmSettingEnum CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS = new AlgorithmSettingEnum(++_index, "Maximum Component Shift Across");
  public static final AlgorithmSettingEnum CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION = new AlgorithmSettingEnum(++_index, "Maximum Component Rotation");
  public static final AlgorithmSettingEnum CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE = new AlgorithmSettingEnum(++_index, "Maximum Misalignment Along Delta - Opaque");
  public static final AlgorithmSettingEnum CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR = new AlgorithmSettingEnum(++_index, "Maximum Misalignment Along Delta - Clear");
 
  //Kok Chun, Tan - Motion Blur
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE = new AlgorithmSettingEnum(++_index, "Motion Blur");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_MOTION_BLUR_MASK_SCALE = new AlgorithmSettingEnum(++_index, "Mask Scale");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_MOTION_BLUR_GRAYLEVEL_OFFSET = new AlgorithmSettingEnum(++_index, "Graylevel Offset");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_MOTION_BLUR_DIRECTION = new AlgorithmSettingEnum(++_index, "MB Direction");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_MOTION_BLUR_ITERATION = new AlgorithmSettingEnum(++_index, "MB Iteration");
  
  //Kok Chun, Tan - Shading Removal
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE = new AlgorithmSettingEnum(++_index, "Shading Removal");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_SHADING_REMOVAL_BLUR_DISTANCE = new AlgorithmSettingEnum(++_index, "Blur Distance");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_SHADING_REMOVAL_KEEP_OUT_DISTANCE = new AlgorithmSettingEnum(++_index, "Keep Out Distance");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_SHADING_REMOVAL_DIRECTION = new AlgorithmSettingEnum(++_index, "SR Direction");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_SHADING_REMOVAL_DESIRED_BACKGROUND = new AlgorithmSettingEnum(++_index, "Desired Background");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_SHADING_REMOVAL_FILTERING_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Filtering Technique");
  
  
  // CHIP : INSUFFICIENT
  public static final AlgorithmSettingEnum CHIP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Fillet Thickness");

  // CHIP : EXCESS
  public static final AlgorithmSettingEnum CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS = new AlgorithmSettingEnum(++_index, "Maximum Fillet Thickness");

  // PCAP : MEASUREMENT
  public static final AlgorithmSettingEnum PCAP_MEASUREMENT_NOMINAL_PAD_ONE_THICKNESS = new AlgorithmSettingEnum(++_index, "Pin One Nominal Fillet Thickness");
  public static final AlgorithmSettingEnum PCAP_MEASUREMENT_NOMINAL_PAD_TWO_THICKNESS = new AlgorithmSettingEnum(++_index, "Pin Two Nominal Fillet Thickness");
  public static final AlgorithmSettingEnum PCAP_MEASUREMENT_LOWER_FILLET_OFFSET = new AlgorithmSettingEnum(++_index, "Lower Fillet Location Offset");
  public static final AlgorithmSettingEnum PCAP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH = new AlgorithmSettingEnum(++_index, "Open Signal Search Length");
  public static final AlgorithmSettingEnum PCAP_MEASUREMENT_UPPER_FILLET_OFFSET = new AlgorithmSettingEnum(++_index, "Upper Fillet Location Offset");
  public static final AlgorithmSettingEnum PCAP_MEASUREMENT_FILLET_EDGE_PERCENT = new AlgorithmSettingEnum(++_index, "Outer Fillet Edge Percent");
  public static final AlgorithmSettingEnum PCAP_MEASUREMENT_BACKGROUND_REGION_SHIFT = new AlgorithmSettingEnum(++_index, "Background Profile Location");

  // PCAP : MISALIGNMENT
  public static final AlgorithmSettingEnum PCAP_MISALIGNMENT_POLARITY_CHECK_METHODS = new AlgorithmSettingEnum(++_index, "Polarity Check Methods");
  public static final AlgorithmSettingEnum PCAP_MISALIGNMENT_MINIMUM_POLARITY_SIGNAL = new AlgorithmSettingEnum(++_index, "Minimum Polarity Signal");
  public static final AlgorithmSettingEnum PCAP_MISALIGNMENT_SLUG_EDGE_THRESHOLD = new AlgorithmSettingEnum(++_index, "Slug Edge Threshold");
  public static final AlgorithmSettingEnum PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ACROSS = new AlgorithmSettingEnum(++_index, "Slug Edge Region Length Across");
  public static final AlgorithmSettingEnum PCAP_MISALIGNMENT_SLUG_EDGE_REGION_LENGTH_ALONG = new AlgorithmSettingEnum(++_index, "Slug Edge Region Length Along");
  public static final AlgorithmSettingEnum PCAP_MISALIGNMENT_SLUG_EDGE_REGION_POSITION = new AlgorithmSettingEnum(++_index, "Slug Edge Region Position");
  
  // PCAP : INSUFFICIENT
  public static final AlgorithmSettingEnum PCAP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Fillet Thickness");

  // PCAP : OPEN
  public static final AlgorithmSettingEnum PCAP_OPEN_PAD_ONE_MINIMUM_OPEN_SIGNAL = new AlgorithmSettingEnum(++_index, "Pin One Open Signal");
  public static final AlgorithmSettingEnum PCAP_OPEN_PAD_TWO_MINIMUM_OPEN_SIGNAL = new AlgorithmSettingEnum(++_index, "Pin Two Open Signal");
  public static final AlgorithmSettingEnum PCAP_OPEN_MINIMUM_SLUG_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Slug Thickness");

  // PCAP : EXCESS
  public static final AlgorithmSettingEnum PCAP_EXCESS_MAXIMUM_FILLET_THICKNESS = new AlgorithmSettingEnum(++_index, "Maximum Fillet Thickness");

  // LARGE PAD : MEASUREMENT
  public static final AlgorithmSettingEnum LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_WIDTH = new AlgorithmSettingEnum(++_index, "Pad Width for Open/Insufficient");
  public static final AlgorithmSettingEnum LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_PAD_HEIGHT = new AlgorithmSettingEnum(++_index, "Pad Height for Open/Insufficient");
  public static final AlgorithmSettingEnum LARGE_PAD_MEASUREMENT_OPEN_INSUFFICIENT_IPD_PERCENT = new AlgorithmSettingEnum(++_index, "Interpad Distance for Open/Insufficient");
  public static final AlgorithmSettingEnum LARGE_PAD_MEASUREMENT_BACKGROUND_REGION_SHIFT = new AlgorithmSettingEnum(++_index, "Background Profile Location");
  public static final AlgorithmSettingEnum LARGE_PAD_NUMBER_OF_USER_DEFINED_SLICE = new AlgorithmSettingEnum(++_index, "Number of User Defined Slice For Large Pad");
  public static final AlgorithmSettingEnum LARGE_PAD_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "User Defined Slice 1 Offset from Large Pad");
  public static final AlgorithmSettingEnum LARGE_PAD_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "User Defined Slice 2 Offset from Large Pad");

  // LARGE PAD : VOIDING
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_BACKGROUND_REGION_SIZE_PAD = new AlgorithmSettingEnum(++_index, "Size of Background Regions");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_THICKNESS_THRESHOLD_PAD = new AlgorithmSettingEnum(++_index, "Void Thickness Threshold");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD = new AlgorithmSettingEnum(++_index, "Joint Voiding Threshold");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_FAIL_COMPONENT_PERCENT_PAD = new AlgorithmSettingEnum(++_index, "Component Voiding Threshold");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_NOISE_REDUCTION = new AlgorithmSettingEnum(++_index, "Level of Noise Reduction");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_NOISE_REDUCTION_FOR_FLOODFILL = new AlgorithmSettingEnum(++_index, "FloodFill Level of Noise Reduction");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_METHOD = new AlgorithmSettingEnum(++_index, "Untested Border Size Method");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE = new AlgorithmSettingEnum(++_index, "Untested Border Size");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_LEFT_SIDE_OFFSET = new AlgorithmSettingEnum(++_index, "Left Side Untested Border Offset");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_BOTTOM_SIDE_OFFSET = new AlgorithmSettingEnum(++_index, "Bottom Side Untested Border Offset");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_RIGHT_SIDE_OFFSET = new AlgorithmSettingEnum(++_index, "Right Side Untested Border Offset");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_UNTESTED_BORDER_SIZE_TOP_SIDE_OFFSET = new AlgorithmSettingEnum(++_index, "Top Side Untested Border Offset");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_BACKGROUND_PERCENTILE = new AlgorithmSettingEnum(++_index, "Background Percentile");
  public static final AlgorithmSettingEnum LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD = new AlgorithmSettingEnum(++_index, "Joint Individual Voiding Threshold");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_METHOD = new AlgorithmSettingEnum(++_index, "Void Method");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_FLOODFILL_SENSITIVITY_THRESHOLD_FOR_LAYER_ONE = new AlgorithmSettingEnum(++_index, "Void FloodFill threshold of Image Layer One");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_NUMBER_OF_IMAGE_LAYER = new AlgorithmSettingEnum(++_index, "Void FloodFill Number of Image Layer");
  public static final AlgorithmSettingEnum LARGE_PAD_USING_MASKING_CHOICES = new AlgorithmSettingEnum(++_index, "Masking Void Method");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_THRESHOLD_OF_GAUSSIAN_BLUR = new AlgorithmSettingEnum(++_index, "Void FloodFill threshold of Gaussian Blur");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_TWO = new AlgorithmSettingEnum(++_index, "Void FloodFill threshold of Image Layer Two");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_THREE = new AlgorithmSettingEnum(++_index, "Void FloodFill threshold of Image Layer Three");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_THRESHOLD_OF_IMAGE_LAYER_ADDITIONAL = new AlgorithmSettingEnum(++_index, "Void FloodFill threshold of Image Layer - Pad Border");
  public static final AlgorithmSettingEnum LARGE_PAD_USING_MULTI_THRESHOLD = new AlgorithmSettingEnum(++_index, "Multi-threshold Detection");
  public static final AlgorithmSettingEnum LARGE_PAD_USING_FIT_POLYNOMIAL = new AlgorithmSettingEnum(++_index, "Polynomial Fitting");
  public static final AlgorithmSettingEnum LARGE_PAD_USING_MASKING_LOCATOR_METHOD = new AlgorithmSettingEnum(++_index, "Masking Locator Method");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_VARIABLE_PAD_BORDER_SENSIVITY = new AlgorithmSettingEnum(++_index, "Variable Pad Border Sensitivity");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_VARIABLE_PAD_VOIDING_SENSIVITY = new AlgorithmSettingEnum(++_index, "Variable Pad Voiding Sensitivity");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Joint Voiding Threshold for User Defined Slice 1");
  public static final AlgorithmSettingEnum LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Joint Individual Voiding Threshold for User Defined Slice 1");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Joint Voiding Threshold for User Defined Slice 2");
  public static final AlgorithmSettingEnum LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD_FOR_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Joint Individual Voiding Threshold for User Defined Slice 2");
  public static final AlgorithmSettingEnum LARGE_PAD_INDIVIDUAL_VOIDING_ACCEPTABLE_GAP_DISTANCE = new AlgorithmSettingEnum(++_index, "Joint Individual Voiding Acceptable Gap Distance");
  //Siew Yeng - XCR-2764
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_ENABLE_SUBREGION = new AlgorithmSettingEnum(++_index, "Enable Sub Region");
  public static final AlgorithmSettingEnum LARGE_PAD_VOIDING_VERSION = new AlgorithmSettingEnum(++_index, "Algorithm Version");
  
  // LARGE PAD : OPEN
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ALONG = new AlgorithmSettingEnum(++_index, "Min Leading Slope Along");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ALONG = new AlgorithmSettingEnum(++_index, "Min Trailing Slope Along");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ALONG = new AlgorithmSettingEnum(++_index, "Min Slope Sum Along");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MINIMUM_LEADING_SLOPE_ACROSS = new AlgorithmSettingEnum(++_index, "Min Leading Slope Across");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MINIMUM_TRAILING_SLOPE_ACROSS = new AlgorithmSettingEnum(++_index, "Min Trailing Slope Across");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MINIMUM_SLOPE_SUM_ACROSS = new AlgorithmSettingEnum(++_index, "Min Slope Sum Across");
  //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - START
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ALONG = new AlgorithmSettingEnum(++_index, "Max Leading Slope Along");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ALONG = new AlgorithmSettingEnum(++_index, "Max Trailing Slope Along");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ALONG = new AlgorithmSettingEnum(++_index, "Max Slope Sum Along");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MAXIMUM_LEADING_SLOPE_ACROSS = new AlgorithmSettingEnum(++_index, "Max Leading Slope Across");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MAXIMUM_TRAILING_SLOPE_ACROSS = new AlgorithmSettingEnum(++_index, "Max Trailing Slope Across");
  public static final AlgorithmSettingEnum LARGE_PAD_OPEN_MAXIMUM_SLOPE_SUM_ACROSS = new AlgorithmSettingEnum(++_index, "Max Slope Sum Across");
  //Lim, Lay Ngor - XCR1648: Add Maximum Slope Limit - END

  // LARGE PAD : INSUFFICIENT
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_MINIMUM_THICKNESS = new AlgorithmSettingEnum(++_index, "Minimum Thickness");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_MAXIMUM_SUM_OF_SLOPE_CHANGES = new AlgorithmSettingEnum(++_index, "Maximum Sum of Slope Changes");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_SMOOTHING_FACTOR = new AlgorithmSettingEnum(++_index, "Smoothing Factor");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_HISTOGRAM_AREA_REDUCTION = new AlgorithmSettingEnum(++_index, "Histogram Area Reduction");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_METHOD = new AlgorithmSettingEnum(++_index, "Insufficient Detection Method");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_AREA_METHOD_SENSITIVITY = new AlgorithmSettingEnum(++_index, "Insufficient Area Detection Sensitivity");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD = new AlgorithmSettingEnum(++_index, "Insufficient Minimum Area Threshold");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_UNTESTED_BORDER_SIZE = new AlgorithmSettingEnum(++_index, "Insufficient Untested Border Size");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_1 = new AlgorithmSettingEnum(++_index, "Insufficient Minimum Area Threshold for User Defined Slice 1");
  public static final AlgorithmSettingEnum LARGE_PAD_INSUFFICIENT_MINIMUM_AREA_THRESHOLD_FOR_USER_DEFINED_SLICE_2 = new AlgorithmSettingEnum(++_index, "Insufficient Minimum Area Threshold for User Defined Slice 2");
  
  // QFN : MEASUREMENT
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_HEEL_VOID_COMPENSATION = new AlgorithmSettingEnum(++_index, "Heel Void Compensation");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_PAD_PROFILE_WIDTH = new AlgorithmSettingEnum(++_index, "Pad Profile Width");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_PAD_PROFILE_LENGTH = new AlgorithmSettingEnum(++_index, "Pad Profile Length");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION = new AlgorithmSettingEnum(++_index, "Background Region Location");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_BACKGROUND_REGION_LOCATION_OFFSET = new AlgorithmSettingEnum(++_index, "Background Region Location Offset");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Heel Edge Search Thickness");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE = new AlgorithmSettingEnum(++_index, "Heel Search Distance From Edge");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_TOE_EDGE_SEARCH_THICKNESS_PERCENT = new AlgorithmSettingEnum(++_index, "Toe Edge Search Thickness");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_CENTER_OFFSET = new AlgorithmSettingEnum(++_index, "Center Offset from Heel");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_NOMINAL_FILLET_LENGTH = new AlgorithmSettingEnum(++_index, "Nominal Fillet Length");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_NOMINAL_FILLET_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Fillet Thickness");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_NOMINAL_HEEL_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Heel Thickness");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_NOMINAL_TOE_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Toe Thickness");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_NOMINAL_CENTER_THICKNESS = new AlgorithmSettingEnum(++_index, "Nominal Center Thickness");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_FILLET_LENGTH_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Fillet Length Technique");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_FEATURE_LOCATION_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Feature Location Technique");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_TOE_OFFSET = new AlgorithmSettingEnum(++_index, "Toe Location");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_UPWARD_CURVATURE_START_DISTANCE_FROM_HEEL_EDGE = new AlgorithmSettingEnum(++_index, "Upward Curvature Start");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_UPWARD_CURVATURE_END_DISTANCE_FROM_HEEL_EDGE = new AlgorithmSettingEnum(++_index, "Upward Curvature End");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_TOE_LOCATION_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Toe Location Technique");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_TOE_FIXED_DISTANCE = new AlgorithmSettingEnum(++_index, "Toe Fixed Distance");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_CENTER_LOCATION_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Center Location Technique");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_SMOOTHING_KERNEL_LENGTH = new AlgorithmSettingEnum(++_index, "Profile Smoothing Length");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_BACKGROUND_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Background Technique");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_SOLDER_THICKNESS_LOCATION = new AlgorithmSettingEnum(++_index, "Solder Thickness Location");
  public static final AlgorithmSettingEnum QFN_MEASUREMENT_NOMINAL_CENTER_OF_SOLDER_VOLUME = new AlgorithmSettingEnum(++_index, "Nominal Center of Solder Volume");

  // QFN : OPEN
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Fillet Length");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_FILLET_LENGTH_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Maximum Fillet Length");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL  = new AlgorithmSettingEnum(++_index, "Maximum Center Thickness");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_OPEN_SIGNAL = new AlgorithmSettingEnum(++_index, "Minimum Open Signal");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_UPWARD_CURVATURE = new AlgorithmSettingEnum(++_index, "Minimum Upward Curvature");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_HEEL_SLOPE = new AlgorithmSettingEnum(++_index, "Minimum Heel Slope");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_SLOPE_SUM_CHANGES = new AlgorithmSettingEnum(++_index, "Minimum Sum of Slope Changes");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE = new AlgorithmSettingEnum(++_index, "Across Minimum Heel Leading/Trailing Slope Sum");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_HEEL_ACROSS_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Across Minimum Heel Sum of Slopes");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_CENTER_ACROSS_WIDTH = new AlgorithmSettingEnum(++_index, "Minimum Width Across Center");
  public static final AlgorithmSettingEnum QFN_OPEN_HEEL_WEIGHT = new AlgorithmSettingEnum(++_index, "Heel Thickness Multiplier");
  public static final AlgorithmSettingEnum QFN_OPEN_TOE_WEIGHT = new AlgorithmSettingEnum(++_index, "Toe Thickness Multiplier");
  public static final AlgorithmSettingEnum QFN_OPEN_CENTER_WEIGHT = new AlgorithmSettingEnum(++_index, "Center Thickness Multiplier");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_HEEL_SHARPNESS = new AlgorithmSettingEnum(++_index, "Minimum Heel Sharpness");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_TOE_SLOPE = new AlgorithmSettingEnum(++_index, "Minimum Toe Slope");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_CENTER_SLOPE = new AlgorithmSettingEnum(++_index, "Minimum Center Slope");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_HEEL_TOE_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Minimum Sum of Heel and Toe Slopes");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_HEEL_ACROSS_WIDTH = new AlgorithmSettingEnum(++_index, "Minimum Width Across Heel");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE = new AlgorithmSettingEnum(++_index, "Across Minimum Center Leading/Trailing Slope Sum");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_CENTER_ACROSS_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Across Minimum Center Sum of Slopes");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_NEIGHBOR_LENGTH_DIFFERENCE = new AlgorithmSettingEnum(++_index, "Maximum Neighbor Length Difference");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Center To Toe Thickness Percent");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Center To Heel Thickness Percent");
  public static final AlgorithmSettingEnum QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST = new AlgorithmSettingEnum(++_index, "Enable Toe Test If Center to Heel Thickness Test Fail");
  public static final AlgorithmSettingEnum QFN_OPEN_MINIMUM_SOLDER_THICKNESS_AT_LOCATION = new AlgorithmSettingEnum(++_index, "Minimum Solder Thickness At Location");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Maximum Center Of Solder Volume Location Percent of Nominal");
  //Lim, Lay Ngor, Kee Chin Seong - XCR1648: Add Maximum Slope Limit - START    
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_HEEL_SLOPE = new AlgorithmSettingEnum(++_index, "Maximum Heel Slope");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_HEEL_TOE_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Maximum Sum of Heel and Toe Slopes");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_SLOPE_SUM_CHANGES = new AlgorithmSettingEnum(++_index, "Maximum Sum of Slope Changes");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_HEEL_ACROSS_LEADING_TRAILING_SLOPE = new AlgorithmSettingEnum(++_index, "Across Maximum Heel Leading/Trailing Slope Sum");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_HEEL_ACROSS_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Across Maximum Heel Sum of Slopes");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_TOE_SLOPE = new AlgorithmSettingEnum(++_index, "Maximum Toe Slope");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_CENTER_SLOPE = new AlgorithmSettingEnum(++_index, "Maximum Center Slope");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_CENTER_ACROSS_LEADING_TRAILING_SLOPE = new AlgorithmSettingEnum(++_index, "Across Maximum Center Leading/Trailing Slope Sum");
  public static final AlgorithmSettingEnum QFN_OPEN_MAXIMUM_CENTER_ACROSS_SLOPE_SUM = new AlgorithmSettingEnum(++_index, "Across Maximum Center Sum of Slopes");  
  //Lim Lay Ngor - XCR1648: Add Maximum Slope Limit - END
  //Siew Yeng - XCR-3532
  public static final AlgorithmSettingEnum QFN_OPEN_MAX_FILLET_LENGTH_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Maximum Fillet Length Region Outlier");
  public static final AlgorithmSettingEnum QFN_OPEN_MIN_FILLET_LENGTH_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Minimum Fillet Length Region Outlier");
  public static final AlgorithmSettingEnum QFN_OPEN_MAX_CENTER_THICKNESS_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Maximum Center Thickness Region Outlier");
  public static final AlgorithmSettingEnum QFN_OPEN_MIN_CENTER_THICKNESS_REGION_OUTLIER = new AlgorithmSettingEnum(++_index, "Minimum Center Thickness Region Outlier");
  
  // QFN : INSUFFICIENT
  public static final AlgorithmSettingEnum QFN_INSUFFICIENT_MINIMUM_FILLET_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Fillet Thickness Percent of Nominal");
  public static final AlgorithmSettingEnum QFN_INSUFFICIENT_MINIMUM_HEEL_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Heel Thickness Percent of Nominal");
  public static final AlgorithmSettingEnum QFN_INSUFFICIENT_MINIMUM_CENTER_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Center Thickness Percent of Nominal");
  public static final AlgorithmSettingEnum QFN_INSUFFICIENT_MINIMUM_TOE_THICKNESS_PERCENT_OF_NOMINAL = new AlgorithmSettingEnum(++_index, "Minimum Toe Thickness Percent of Nominal");

  // QFN : VOIDING
  public static final AlgorithmSettingEnum QFN_VOIDING_MAXIMUM_VOID_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Void Percent of Pad Area");
  public static final AlgorithmSettingEnum QFN_VOIDING_NOISE_REDUCTION = new AlgorithmSettingEnum(++_index, "Level of Noise Reduction");
  public static final AlgorithmSettingEnum QFN_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Individual Void Percent of Pad Area");

  // EXPOSED PAD : MEASUREMENT
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ACROSS_MEASUREMENTS = new AlgorithmSettingEnum(++_index, "Number of Profile Across Measurements");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ACROSS = new AlgorithmSettingEnum(++_index, "Profile Location Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ACROSS = new AlgorithmSettingEnum(++_index, "Profile Width Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ACROSS_MEASUREMENTS = new AlgorithmSettingEnum(++_index, "Number of Gap Across Measurements");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ACROSS = new AlgorithmSettingEnum(++_index, "Background Technique Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_NUMBER_PROFILE_ALONG_MEASUREMENTS = new AlgorithmSettingEnum(++_index, "Number of Profile Along Measurements");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_PROFILE_LOCATION_ALONG = new AlgorithmSettingEnum(++_index, "Profile Location Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_PROFILE_WIDTH_ALONG = new AlgorithmSettingEnum(++_index, "Profile Width Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_NUMBER_GAP_ALONG_MEASUREMENTS = new AlgorithmSettingEnum(++_index, "Number of Gap Along Measurements");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE_ALONG = new AlgorithmSettingEnum(++_index, "Background Technique Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ACROSS = new AlgorithmSettingEnum(++_index, "Background Location Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ACROSS = new AlgorithmSettingEnum(++_index, "Center Search Distance Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ACROSS = new AlgorithmSettingEnum(++_index, "Gap Offset Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_BACKGROUND_LOCATION_ALONG = new AlgorithmSettingEnum(++_index, "Background Location Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_CENTER_SEARCH_DISTANCE_ALONG = new AlgorithmSettingEnum(++_index, "Center Search Distance Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_GAP_OFFSET_ALONG = new AlgorithmSettingEnum(++_index, "Gap Offset Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS = new AlgorithmSettingEnum(++_index, "Maximum Expected Thickness");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_GAP_CLIFF_SLOPE = new AlgorithmSettingEnum(++_index, "Gap Cliff Slope");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_GAP_FALLOFF_SLOPE = new AlgorithmSettingEnum(++_index, "Gap Falloff Slope");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_PROFILE_SMOOTHING_LENGTH = new AlgorithmSettingEnum(++_index, "Profile Smoothing Length");
  // public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_CTR_SEARCH_DIST_OBSOLETE = new AlgorithmSettingEnum(++_index, "Center Search Distance");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_BACKGROUND_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Background Technique");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_NOMINAL_BACKGROUND_VALUE = new AlgorithmSettingEnum(++_index, "Nominal Background Value");
  public static final AlgorithmSettingEnum EXPOSED_PAD_MEASUREMENT_CONTRAST_ENHANCEMENT = new AlgorithmSettingEnum(++_index, "Contrast Enhancement");

  // EXPOSED PAD : OPEN
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ACROSS = new AlgorithmSettingEnum(++_index, "Maximum Inner % of Outer Gap Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ACROSS = new AlgorithmSettingEnum(++_index, "Maximum Outer Gap Length Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ACROSS = new AlgorithmSettingEnum(++_index, "Minimum Outer Gap Thickness Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MAXIMUM_INNER_PERCENT_OF_OUTER_GAP_ALONG = new AlgorithmSettingEnum(++_index, "Maximum Inner % of Outer Gap Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MAXIMUM_OUTER_GAP_LENGTH_ALONG = new AlgorithmSettingEnum(++_index, "Maximum Outer Gap Length Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MINIMUM_OUTER_GAP_THICKNESS_ALONG = new AlgorithmSettingEnum(++_index, "Minimum Outer Gap Thickness Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ACROSS = new AlgorithmSettingEnum(++_index, "Maximum Inner Gap Length Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_MAXIMUM_INNER_GAP_LENGTH_ALONG = new AlgorithmSettingEnum(++_index, "Maximum Inner Gap Length Along");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_ALLOWED_GAP_FAILURES = new AlgorithmSettingEnum(++_index, "Allowed Gap Failures");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ACROSS = new AlgorithmSettingEnum(++_index, "Filled Gap Thickness Across");
  public static final AlgorithmSettingEnum EXPOSED_PAD_OPEN_FILLED_GAP_THICKNESS_ALONG = new AlgorithmSettingEnum(++_index, "Filled Gap Thickness Along");

  // EXPOSED PAD : INSUFFICIENT
  public static final AlgorithmSettingEnum EXPOSED_PAD_INSUFFICIENT_MAX_VOID_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Void Percent");

  // EXPOSED PAD : VOIDING
  public static final AlgorithmSettingEnum EXPOSED_PAD_VOIDING_MAXIMUM_VOID_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Void Percent");
  public static final AlgorithmSettingEnum EXPOSED_PAD_VOIDING_SOLDER_THICKNESS = new AlgorithmSettingEnum(++_index, "Solder Thickness");
  public static final AlgorithmSettingEnum EXPOSED_PAD_VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE = new AlgorithmSettingEnum(++_index, "Maximum Gray Level Difference");
  public static final AlgorithmSettingEnum EXPOSED_PAD_VOIDING_MINIMUM_VOID_AREA = new AlgorithmSettingEnum(++_index, "Minimum Void Area");
  public static final AlgorithmSettingEnum EXPOSED_PAD_VOIDING_LEVEL_OF_NOISE_REDUCTION = new AlgorithmSettingEnum(++_index, "Level of Noise Reduction");
  public static final AlgorithmSettingEnum EXPOSED_PAD_VOIDING_VOIDING_TECHNIQUE = new AlgorithmSettingEnum(++_index, "Voiding Technique");
  public static final AlgorithmSettingEnum EXPOSED_PAD_INDIVIDUAL_VOIDING_MAXIMUM_VOID_PERCENT = new AlgorithmSettingEnum(++_index, "Maximum Individual Void Percent");
  
  // USER DEFINED SLICEHEIGHT SETTINGS SHARED BY MULTIPLE JOINT TYPES
  public static final AlgorithmSettingEnum USER_DEFINED_PAD_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Pad Slice Offset");
  public static final AlgorithmSettingEnum USER_DEFINED_BGA_LOWERPAD_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Lower Pad Slice Offset");
  public static final AlgorithmSettingEnum USER_DEFINED_PACKAGE_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Package Slice Offset");
  public static final AlgorithmSettingEnum USER_DEFINED_MIDBALL_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Midball Slice Offset");
  // Swee Yee Wong - enabled constant distance between pad and package for same component
  public static final AlgorithmSettingEnum GRID_ARRAY_MEASUREMENT_DEFINE_OFFSET_FOR_PAD_AND_PACKAGE = new AlgorithmSettingEnum(++_index, "Define Offset for Pad and Package");
  public static final AlgorithmSettingEnum USER_DEFINED_MIDBALL_TO_EDGE_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Midball To Edge Offset");

  public static final AlgorithmSettingEnum USER_DEFINED_PCAP_SLUG_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Slug Slice Offset");
  public static final AlgorithmSettingEnum USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Opaque Chip Slice Offset");
  public static final AlgorithmSettingEnum USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Clear Chip Slice Offset");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Barrel Slice Height");

  // PREDICTIVE SLICEHEIGHT SETTING SHARED BY MULTIPLE JOINT TYPES
  public static final AlgorithmSettingEnum PREDICTIVE_SLICEHEIGHT = new AlgorithmSettingEnum(++_index, "Predictive Slice Height");
  // Slice height working unit
  public static final AlgorithmSettingEnum SLICEHEIGHT_WORKING_UNIT = new AlgorithmSettingEnum(++_index, "Slice Height Working Unit");
  public static final AlgorithmSettingEnum USER_DEFINED_NUMBER_OF_SLICE = new AlgorithmSettingEnum(++_index, "User Defined Barrel Slice");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness");
  public static final AlgorithmSettingEnum THROUGHHOLE_COMPONENT_SIDE_SHORT_SLICEHEIGHT_IN_THICKNESS = new AlgorithmSettingEnum(++_index, "Component Side Short Slice Height Thickness");
  public static final AlgorithmSettingEnum THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS = new AlgorithmSettingEnum(++_index, "Pin Side Slice Height Thickness");
  //THROUGHHOLE_PIN_SIDE_SLICEHEIGHT_IN_THICKNESS
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_2 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 2");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_2 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 2");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_3 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 3");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_3 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 3");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_4 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 4");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_4 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 4");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_5 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 5");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_5 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 5");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_6 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 6");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_6 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 6");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_7 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 7");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_7= new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 7");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_8 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 8");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_8 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 8");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_9 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 9");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_9 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 9");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_SLICE_10 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height - Slice 10");
  public static final AlgorithmSettingEnum THROUGHHOLE_BARREL_SLICEHEIGHT_IN_THICKNESS_10 = new AlgorithmSettingEnum(++_index, "Barrel Slice Height Thickness - Slice 10");

  // USER DEFINED SLICEHEIGHT SETTINGS FOR AUTOFOCUS MID-BOARD SHARED BY MULTIPLE JOINT TYPES
  public static final AlgorithmSettingEnum USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET = new AlgorithmSettingEnum(++_index, "AutoFocus Search Range MidBoard Offset");

  // USER DEFINED GRAYLEVEL MINIMUM & MAXIMUM
  public static final AlgorithmSettingEnum USER_DEFINED_GRAYLEVEL_UPDATE = new AlgorithmSettingEnum(++_index, "Update Graylevel");
  public static final AlgorithmSettingEnum USER_DEFINED_GRAYLEVEL_MINIMUM = new AlgorithmSettingEnum(++_index, "Graylevel Minimum");
  public static final AlgorithmSettingEnum USER_DEFINED_GRAYLEVEL_MAXIMUM = new AlgorithmSettingEnum(++_index, "Graylevel Maximum");

  public static final AlgorithmSettingEnum USER_DEFINED_REFERENCES_FROM_PLANE = new AlgorithmSettingEnum(++_index, "Reference from Plane Offset");
  
  public static final AlgorithmSettingEnum USER_DEFINED_FOCUS_CONFIRMATION = new AlgorithmSettingEnum(++_index, "Focus Confirmation");
  public static final AlgorithmSettingEnum ADVANCED_GULLWING_USER_DEFINED_NUMBER_OF_SLICE = new AlgorithmSettingEnum(++_index, "User Defined Slice");

  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_BOXFILTER_ENABLE = new AlgorithmSettingEnum(++_index, "Background Filter");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR = new AlgorithmSettingEnum(++_index, "Filter Iteration");
  
  //Siew Yeng - XCR2388
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_BOXFILTER_WIDTH = new AlgorithmSettingEnum(++_index, "Filter Width");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_BOXFILTER_LENGTH = new AlgorithmSettingEnum(++_index, "Filter Length");
  
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_RESIZE_ENABLE = new AlgorithmSettingEnum(++_index, "Resize");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_RESIZE_SCALE = new AlgorithmSettingEnum(++_index, "Resize - Scale");
  
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_CLAHE_ENABLE = new AlgorithmSettingEnum(++_index, "CLAHE");
  
  //Siew Yeng - XCR2388
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_CLAHE_BLOCK_SIZE = new AlgorithmSettingEnum(++_index, "Block Size");

  //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE = new AlgorithmSettingEnum(++_index, "BandPass Filter");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_LARGESCALE = new AlgorithmSettingEnum(++_index, "Large Scale Filter");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE = new AlgorithmSettingEnum(++_index, "Small Scale Filter");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUPPRESSSTRIPES = new AlgorithmSettingEnum(++_index, "Suppress Stripes");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_TOLERANCEOFDIRECTION = new AlgorithmSettingEnum(++_index, "Tolerance Of Direction");
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SATURATEVALUE = new AlgorithmSettingEnum(++_index, "Saturate Value");  
  //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END
  
  //Siew Yeng - XCR-2389 - Retinex Filter
  public static final AlgorithmSettingEnum IMAGE_ENHANCEMENT_RFILTER_ENABLE = new AlgorithmSettingEnum(++_index, "R Filter");
  
  // Added by Khang Wah, 2013-09-11. user defined initial wavelet level
  public static final AlgorithmSettingEnum USER_DEFINED_WAVELET_LEVEL = new AlgorithmSettingEnum(++_index, "Search Speed");

  // Added by Wei Chin, 2014-02-10. user defined background sensitivity
  public static final AlgorithmSettingEnum BACKGROUND_SENSITIVITY_VALUE = new AlgorithmSettingEnum(++_index, "Background Sensitivity Thresholds");
  
  // Added by Lee Herng, 2014-08-13. Search Range - Low Limit
  public static final AlgorithmSettingEnum PSP_SEARCH_RANGE_LOW_LIMIT = new AlgorithmSettingEnum(++_index, "PSP Search Range - Low Limit");
  
  // Added by Lee Herng, 2014-08-13. Search Range - High Limit
  public static final AlgorithmSettingEnum PSP_SEARCH_RANGE_HIGH_LIMIT = new AlgorithmSettingEnum(++_index, "PSP Search Range - High Limit");
  
  // Added by Lee Herng, 2015-03-27. PSP Local Search
  public static final AlgorithmSettingEnum PSP_LOCAL_SEARCH = new AlgorithmSettingEnum(++_index, "PSP Local Search");
  
  // Added by Lee Herng, 2015-08-10. PSP Z-Offset
  public static final AlgorithmSettingEnum PSP_Z_OFFSET = new AlgorithmSettingEnum(++_index, "PSP Z Offset");

  // Added by Wei Chin
  public static final AlgorithmSettingEnum SAVE_DIAGNOSTIC_IMAGES = new AlgorithmSettingEnum(++_index, "Save Diagnostic Image");
  
  // Added by Siew Yeng - XCR-2683
  public static final AlgorithmSettingEnum SAVE_ENHANCED_IMAGE =  new AlgorithmSettingEnum(++_index, "Save Enhanced Image");
  
  //Added by Siew Yeng - XCR-3094
  public static final AlgorithmSettingEnum STITCH_COMPONENT_IMAGE_AT_VVTS = new AlgorithmSettingEnum(++_index, "Stitch Component Image at VVTS(Component Failure Only)");
  
  //Added by Kee Chin Seong
  public static final AlgorithmSettingEnum SHARED_VOIDING_DETECTION_SENSITIVITY =  new AlgorithmSettingEnum(++_index, "Maximum Percentage Different From Nominal Solder");
  public static final AlgorithmSettingEnum SHARED_VOIDING_REGION_REFERENCE_POSITION =  new AlgorithmSettingEnum(++_index, "Reference Position");
  public static final AlgorithmSettingEnum SHARED_VOIDING_INNER_EDGE_POSITION =  new AlgorithmSettingEnum(++_index, "Voiding Inner edge Position");
  public static final AlgorithmSettingEnum SHARED_VOIDING_OUTER_EDGE_POSITION =  new AlgorithmSettingEnum(++_index, "Voiding Outer edge Position");
  public static final AlgorithmSettingEnum SHARED_VOIDING_GREY_LEVEL_NOMINAL =  new AlgorithmSettingEnum(++_index, "Nominal Solder Signal In GreyLevel");

  //Khaw Chek Hau - XCR3601: Auto distribute the pressfit region by direction of artifact
  public static final AlgorithmSettingEnum ARTIFACT_BASED_REGION_SEPERATION_DIRECTION = new AlgorithmSettingEnum(++_index, "Artifact Based Region Seperation Direction");

  private final String _name;

  /**
   * @author Sunit Bhalla
   */
  private AlgorithmSettingEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
   _name = name.intern();
   _nameToAlgorithmSettingEnumMap.put(_name, this);
  }

  /**
   * @author Sunit Bhalla
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Sunit Bhalla
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Sunit Bhalla
   */
  public static AlgorithmSettingEnum getAlgorithmSettingEnum(final String jointTypeName)
  {
    final AlgorithmSettingEnum AlgorithmSettingEnum = _nameToAlgorithmSettingEnumMap.get(jointTypeName);
    Assert.expect(AlgorithmSettingEnum != null);
    return AlgorithmSettingEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public static boolean doesAlgorithmSettingEnumExist(final String jointTypeName)
  {
    return _nameToAlgorithmSettingEnumMap.containsKey(jointTypeName);
  }

  /**
   * @author Sunit Bhalla
   */
  public static List<AlgorithmSettingEnum> getAllAlgorithmSettingEnums()
  {
    return new ArrayList<AlgorithmSettingEnum>(_nameToAlgorithmSettingEnumMap.values());
  }

  /**
   * @author Peter Esbensen
   */
  public int compareTo(Object object)
  {
    final AlgorithmSettingEnum algorithmSettingEnum = (AlgorithmSettingEnum)object;

    return algorithmSettingEnum.toString().compareTo(toString());
  }

}
