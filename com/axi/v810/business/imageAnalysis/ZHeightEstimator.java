package com.axi.v810.business.imageAnalysis;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.chip.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
public class ZHeightEstimator
{
  private static ZHeightEstimator _instance;
  // all the points in these maps should be SystemFiducialCoordinates.
  private static Map<Point2D, Pair<ReconstructionRegion,Integer>> _topSidePoints = new HashMap<Point2D, Pair<ReconstructionRegion,Integer>>(); //Siew Yeng - XCR-3781
  private static Map<Point2D, Pair<ReconstructionRegion,Integer>> _bottomSidePoints = new HashMap<Point2D, Pair<ReconstructionRegion,Integer>>();

  private static Map<Point2D, Point2D> _topSideToNearestBottomSidePointMap = new HashMap<Point2D, Point2D>();
  private static Map<Point2D, Point2D> _bottomSideToNearestTopSidePointMap = new HashMap<Point2D, Point2D>();

  private static Config _config = Config.getInstance();
  private static final boolean _LOG_FOCUS_CONFIRMATION_DATA = _config.getBooleanValue(SoftwareConfigEnum.LOG_FOCUS_CONFIRMATION_DATA);
  private static final boolean _ENABLE_SURFACE_MODEL_INFO_LOG = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_SURFACE_MODEL_INFO_LOG); //Siew Yeng - XCR-3781

  private static boolean _boardThicknessEstimateIsValid = false;
  private static float _estimatedBoardThickness = 0.0f;
  private static float _cadBoardThickness = 0.0f;
  
  //Siew Yeng - XCR-3781 - PshSettings
  private static Map<Component, List<Pair<ReconstructionRegion,Integer>>> _componentToRegionAndZHeightMap = new HashMap<Component, List<Pair<ReconstructionRegion,Integer>>>();
  private SurfaceModelInfoLogger _surfaceModelLogger = null;
  
  /**
   * @author Peter Esbensen
   */
  public synchronized static ZHeightEstimator getInstance()
  {
    if (_instance == null)
      _instance = new ZHeightEstimator();

    return _instance;
  }

  /**
   * @author Peter Esbensen
   */
  public void resetForNewInspection(int cadPanelThicknessInNanometers, String projectName)
  {
    _topSidePoints.clear();
    _bottomSidePoints.clear();
    _topSideToNearestBottomSidePointMap.clear();
    _bottomSideToNearestTopSidePointMap.clear();
    _componentToRegionAndZHeightMap.clear(); //Siew Yeng - XCR-3781 - for selective component(PSH)
    _boardThicknessEstimateIsValid = true;
    _estimatedBoardThickness = _cadBoardThickness = (float)cadPanelThicknessInNanometers;
    
    //Siew Yeng - XCR-3781 - PshSettings
    if(_ENABLE_SURFACE_MODEL_INFO_LOG)
    {
      if(_surfaceModelLogger == null)
        _surfaceModelLogger = SurfaceModelInfoLogger.getInstance();
      
      _surfaceModelLogger.open(projectName);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForPadSlice(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    // PAD is the default
    SliceNameEnum padSliceNameEnum = SliceNameEnum.PAD;

    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();

    // if this is a cap, then we need to figure out if we should use the clear or opaque slice
    if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
    {
      if (ChipMeasurementAlgorithm.testAsOpaque(jointInspectionData))
      {
        padSliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      }
      else
      {
        padSliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      }
    }
    // resistors always use the clear slice
    if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
    {
      padSliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
    }
    // for throughhole, let's use the pin side slice
    // Note that this is on the "wrong: side, so will require special handling!
    if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE) ||
        jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) //Siew Yeng - XCR-3318
    {
      padSliceNameEnum = SliceNameEnum.THROUGHHOLE_PIN_SIDE;
      //padSliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
    }
    // for CSP, we have to use the midball and actually, it should be close enough
    if (jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE))
    {
      padSliceNameEnum = SliceNameEnum.MIDBALL;
    }
    // For pressfit, use the component side slice
    if (jointTypeEnum.equals(JointTypeEnum.PRESSFIT))
    {
      padSliceNameEnum = SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE;
    }
    return padSliceNameEnum;
  }

  /**
   * @author Roy Williams
   */
  public synchronized void addDataPointForTesting(boolean isTopSidePoint,
                                                  Point2D point2D,
                                                  Integer padZHeight)
  {
    if (isTopSidePoint)
      _topSidePoints.put(point2D, new Pair(new ReconstructionRegion(), padZHeight)); //Siew Yeng - XCR-3781
    else
      _bottomSidePoints.put(point2D, new Pair(new ReconstructionRegion(), padZHeight));
  }


  /**
   * @author Peter Esbensen
   * @author Siew Yeng - XCR-3781 - added parameters pshSettings and isFocusConfirmation
   * @param pshSettings - this will decide to add point into top and bottom points list
   * @param isFocusConfirmation - for debug purpose
   */
  public synchronized void addDataPoint(List<JointInspectionData> jointInspectionDataList,
                                        ReconstructedImages reconstructedImages,
                                        IntegerRef actualPadZHeight,
                                        PshSettings pshSettings,
                                        boolean isFocusConfirmation)
  {
    Assert.expect(jointInspectionDataList != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(actualPadZHeight != null);

    ReconstructionRegion region = reconstructedImages.getReconstructionRegion();
    
    //Siew Yeng - XCR-3781
    if(pshSettings.doesRegionPassPshSettingsFilter(region) == false)
      return;
    
    boolean allJointsPassed = true;
    boolean isTopSide = false;
    boolean isPTH = false;
    int padZHeight = 0;
    boolean determinedPadSlice = false;

    final boolean WE_CARE_IF_THE_IMAGE_PASSED = true;
    
    if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_ZHEIGHT_ESTIMATOR_DEBUG))
    {
      System.out.println("  ZHeightEstimator: addDataPoint: " + reconstructedImages.getReconstructionRegion().toString() +
                          "; Board " + reconstructedImages.getReconstructionRegion().getBoard().getName() + 
                          "; Component " + jointInspectionDataList.get(0).getComponent().getReferenceDesignator());
    }    
    
    JointTypeEnum jointTypeEnum = reconstructedImages.getReconstructionRegion().getJointTypeEnum();
    for (JointInspectionData jointInspectionData : jointInspectionDataList)
    {
      if (determinedPadSlice == false)
      {
        // Check to see if thie joint type should be used to build the limited surface model
        JointTypeEnum jointType = jointInspectionData.getJointTypeEnum();
        if (jointType.isUseToBuildLimitedSurfaceModel() == false)
        {
          return;
        }
        SliceNameEnum padSliceNameEnum = getSliceNameEnumForPadSlice(jointInspectionData);
        isTopSide = jointInspectionData.isTopSideSurfaceModel();

        boolean foundPadSlice = false;
        for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
        {
          if (reconstructedSlice.getSliceNameEnum().equals(padSliceNameEnum))
          {
            padZHeight = reconstructedSlice.getHeightInNanometers();
            foundPadSlice = true;
            actualPadZHeight.setValue(padZHeight);
            jointTypeEnum =  jointType;
          }
        }
        Assert.expect(foundPadSlice, "no pad slice for " + jointInspectionData.getFullyQualifiedPadName());
        determinedPadSlice = true;
      }

      if (WE_CARE_IF_THE_IMAGE_PASSED)
      {
        // check if the joint failed or its component failed
        // this code is kind of dorky but I'm trying to isolate the source of a null pointer exception
        JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
        boolean jointPassed = jointInspectionResult.passed();
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
        boolean componentPassed = componentInspectionResult.passed();
        boolean jointAndComponentPassed = jointPassed && componentPassed;

        if (jointAndComponentPassed == false)
        {
          allJointsPassed = false;
          break;
        }
      }
    }

    Assert.expect(determinedPadSlice);

    //Siew Yeng - XCR-3781 - handle selective component (PSH)
    if(pshSettings.isEnabledSelectiveComponent() && pshSettings.isNeighborExist(region.getComponent().getComponentType()))
    {
      if(_componentToRegionAndZHeightMap.containsKey(region.getComponent()) == false)
        _componentToRegionAndZHeightMap.put(region.getComponent(), new ArrayList<Pair<ReconstructionRegion,Integer>>());
      
      //Siew Yeng - XCR-3849 - fix selective neighbour have two results for the same region
      boolean exist = false;
      for(Pair<ReconstructionRegion, Integer> pair : _componentToRegionAndZHeightMap.get(region.getComponent()))
      {
        if(pair.getFirst().equals(region))
        {
          pair.setSecond(padZHeight);
          exist = true;
          break;
        }
      }
      
      if(exist == false)
        _componentToRegionAndZHeightMap.get(region.getComponent()).add(new Pair(region, padZHeight));
    }
    
    if ((WE_CARE_IF_THE_IMAGE_PASSED == false) || (allJointsPassed))
    {
      Point2D point2D = getReconstructedImagesSystemFiducialRectangleCenterPoint2D(reconstructedImages);
      
      //Siew Yeng - XCR-3781 - PSH debug ONLY
      if(_ENABLE_SURFACE_MODEL_INFO_LOG)
      {
        //Siew Yeng - XCR-3866 - should log component (board) side instead of surface model side
        _surfaceModelLogger.logAddPointInfo(region.isTopSide(), region, jointTypeEnum, point2D, padZHeight, isFocusConfirmation);
      }
      
      if (isTopSide)
      {
        if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_ZHEIGHT_ESTIMATOR_DEBUG))
        {
          System.out.println("  ZHeightEstimator: addDataPoint: TopSide: Start adding (" + point2D.getX() + "," + point2D.getY() + "), Z-Height = " + padZHeight);
        }
        
        _topSidePoints.put(point2D, new Pair(region,padZHeight));
        /***** JMH following is for debug plotting of original and corrected points wrt true surface
        System.out.println("JMHSurface," + Math.round(point2D.getX()) + "," + Math.round(point2D.getY()) + "," +
                           String.valueOf(padZHeight) + ",1");
        *****/

        // update the nearest neighbor map
        Point2D nearestBottomSidePoint = null;
        double closestDistance = Double.MAX_VALUE;
        for (Point2D bottomSidePoint : _bottomSidePoints.keySet())
        {
          // see if this is the closest bottom side point
          double distance = point2D.distance(bottomSidePoint);
          if (distance < closestDistance)
          {
            nearestBottomSidePoint = bottomSidePoint;
            closestDistance = distance;
          }
          // while we are looking at this bottom side point, see if this topside point is its closest neighbor
          Assert.expect(_bottomSideToNearestTopSidePointMap.containsKey(bottomSidePoint)); // everything in _bottomSidePoints should be in the map
          Point2D existingClosestNeighborToBottomSidePoint = _bottomSideToNearestTopSidePointMap.get(bottomSidePoint);
          // if this point is closer, or if the previous nearest neighbor doesn't exist (is null), then set it
          if ((existingClosestNeighborToBottomSidePoint == null) ||
              (distance < existingClosestNeighborToBottomSidePoint.distance(bottomSidePoint)))
          {
            _bottomSideToNearestTopSidePointMap.put(bottomSidePoint, point2D);
          }
        }
        // ok we found the nearest bottom side point, add it to the map (if we didn't find it, it will just be NULL)
        _topSideToNearestBottomSidePointMap.put(point2D, nearestBottomSidePoint);
        if (nearestBottomSidePoint != null)
          _boardThicknessEstimateIsValid = false;
      }
      else
      {
        if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.ENABLE_ZHEIGHT_ESTIMATOR_DEBUG))
        {
          System.out.println("  ZHeightEstimator: addDataPoint: BottomSide: Start adding (" + point2D.getX() + "," + point2D.getY() + "), Z-Height = " + padZHeight);
        }
        
        _bottomSidePoints.put(point2D, new Pair(region,padZHeight));
        /***** JMH following is for debug plotting of original and corrected points wrt true surface
        System.out.println("JMHSurface," + Math.round(point2D.getX()) + "," + Math.round(point2D.getY()) + "," +
                           String.valueOf(padZHeight) + ",0");
        *****/

        // update the nearest neighbor map
        Point2D nearestTopSidePoint = null;
        double closestDistance = Double.MAX_VALUE;
        for (Point2D topSidePoint : _topSidePoints.keySet())
        {
          // see if this is the closest top side point
          double distance = point2D.distance(topSidePoint);
          if (distance < closestDistance)
          {
            nearestTopSidePoint = topSidePoint;
            closestDistance = distance;
          }
          // while we are looking at this top side point, see if this bottomside point is its closest neighbor
          Assert.expect(_topSideToNearestBottomSidePointMap.containsKey(topSidePoint)); // everything in _topSidePoints should be in the map
          Point2D existingClosestNeighborToTopSidePoint = _topSideToNearestBottomSidePointMap.get(topSidePoint);
          // if this point is closer, or if the previous nearest neighbor doesn't exist (is null), then set it
          if ((existingClosestNeighborToTopSidePoint == null) ||
              (distance < existingClosestNeighborToTopSidePoint.distance(topSidePoint)))
          {
            _topSideToNearestBottomSidePointMap.put(topSidePoint, point2D);
          }
        }
        // ok we found the nearest top side point, add it to the map (if we didn't find it, it will just be NULL)
        _bottomSideToNearestTopSidePointMap.put(point2D, nearestTopSidePoint);
        if (nearestTopSidePoint != null)
          _boardThicknessEstimateIsValid = false;
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized int getPadSliceZHeight(JointInspectionData jointInspectionData,
      ReconstructedImages reconstructedImages)
  {
    int padZHeight = 0;

    SliceNameEnum padSliceNameEnum = getSliceNameEnumForPadSlice(jointInspectionData);

    boolean foundPadSlice = false;
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      if (reconstructedSlice.getSliceNameEnum().equals(padSliceNameEnum))
      {
        padZHeight = reconstructedSlice.getHeightInNanometers();
        foundPadSlice = true;
      }
    }
    Assert.expect(foundPadSlice);

    return padZHeight;
  }


  /**
   * For the given reconstructedImages, return the center coordinate relative to the system fiducial.
   *
   * @author Peter Esbensen
   */
  private Point2D getReconstructedImagesSystemFiducialRectangleCenterPoint2D(ReconstructedImages reconstructedImages)
  {
    Assert.expect(reconstructedImages != null);
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    PanelRectangle panelRectangle = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    SystemFiducialRectangle systemFiducialRectangle = MechanicalConversions.convertPanelRectangleToSystemFiducialRectangle(panelRectangle,
                                                                                                                           reconstructionRegion.getManualAlignmentMatrix());
    Point2D point2D = new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY());
    return point2D;
  }

  /**
   * @author Peter Esbensen
  public synchronized void addDataPoint(SystemFiducialRectangle systemFiducialRectangle,
                                        int zHeightInNanometers,
                                        boolean topSide)
  {
    Assert.expect(systemFiducialRectangle != null);

    Point2D point2D = new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY());
    if (topSide)
      _topSidePoints.put(point2D, zHeightInNanometers);
    else
      _bottomSidePoints.put(point2D, zHeightInNanometers);
  }
  */

  /**
   * @author Peter Esbensen
   */
  private static float[] convertIntegerCollectionToFloatArray(Collection<Integer> integers)
  {
    Assert.expect(integers.size() > 0);
    float[] floatArray = new float[ integers.size() ];
    int index = 0;
    for (Integer integerObject : integers)
    {
      floatArray[index] = integerObject;
      ++index;
    }
    return floatArray;
  }

  /**
   * This is only for FocusConfirmation - Siew Yeng - XCR-3781
   * added parameter region for debug purpose
   * @author Peter Esbensen
   */
  public synchronized int getBestEstimatedZHeight(SystemFiducialRectangle systemFiducialRectangle,
                                              boolean topside,
                                              BooleanRef estimateIsValid,
                                              ReconstructionRegion region)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);

    final int DISTANCE_THRESHOLD_IN_MILS = 750;
    final int MINIMUM_POINTS_NEEDED_FOR_ESTIMATE = 1;

    return getEstimatedZHeight( new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
                                topside,
                                DISTANCE_THRESHOLD_IN_MILS,
                                MINIMUM_POINTS_NEEDED_FOR_ESTIMATE,
                                estimateIsValid,
                                region,
                                false); //Siew Yeng - XCR-3848
  }
  
  /**
   * @author Peter Esbensen
   * @author Siew Yeng - call this only for SurfaceModeling (PSH)
   * @param pshSettings - for Surface Model settings
   * @param region - for debug purpose
   */
  public synchronized int getBestEstimatedZHeight(SystemFiducialRectangle systemFiducialRectangle,
                                              boolean topside,
                                              BooleanRef estimateIsValid,
                                              PshSettings pshSettings,
                                              ReconstructionRegion region)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);

    final int DISTANCE_THRESHOLD_IN_MILS = 750;
    final int MINIMUM_POINTS_NEEDED_FOR_ESTIMATE = 1;

    return getEstimatedZHeight( new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
                                topside,
                                DISTANCE_THRESHOLD_IN_MILS,
                                MINIMUM_POINTS_NEEDED_FOR_ESTIMATE,
                                estimateIsValid,
                                pshSettings,
                                region);
  }

  /**
   * @author Peter Esbensen
   * This is only for FocusConfirmation - Siew Yeng - XCR-3781
   * added parameter region for debug purpose
   */
  public synchronized int getReasonablyEstimatedZHeight(SystemFiducialRectangle systemFiducialRectangle,
                                                        boolean topside,
                                                        BooleanRef estimateIsValid,
                                                        ReconstructionRegion region)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);

    final int DISTANCE_THRESHOLD_IN_MILS = 1500;
    final int MINIMUM_POINTS_NEEDED_FOR_ESTIMATE = 3;

    return getEstimatedZHeight( new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
                                topside,
                                DISTANCE_THRESHOLD_IN_MILS,
                                MINIMUM_POINTS_NEEDED_FOR_ESTIMATE,
                                estimateIsValid,
                                region,
                                false); //Siew Yeng - XCR-3848
  }
  
  /**
   * @author Peter Esbensen
   * @author Siew Yeng - call this only for SurfaceModeling (PSH)
   * @param pshSettings - for Surface Model settings
   * @param region - for debug purpose
   */
  public synchronized int getReasonablyEstimatedZHeight(SystemFiducialRectangle systemFiducialRectangle,
                                                        boolean topside,
                                                        BooleanRef estimateIsValid,
                                                        PshSettings pshSettings,
                                                        ReconstructionRegion region)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);

    final int DISTANCE_THRESHOLD_IN_MILS = 1500;
    final int MINIMUM_POINTS_NEEDED_FOR_ESTIMATE = 3;

    return getEstimatedZHeight( new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
                                topside,
                                DISTANCE_THRESHOLD_IN_MILS,
                                MINIMUM_POINTS_NEEDED_FOR_ESTIMATE,
                                estimateIsValid,
                                pshSettings,
                                region);
  }

  /**
   * @author Peter Esbensen
   * NOT CURRENTLY USED
  public synchronized int getEstimatedZHeightWithNoRestrictions(SystemFiducialRectangle systemFiducialRectangle,
                                                                boolean topside,
                                                                BooleanRef estimateIsValid)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);

    final int DISTANCE_THRESHOLD_IN_MILS = 1500;
    final int MINIMUM_POINTS_NEEDED_FOR_ESTIMATE = 3;

    return getEstimatedZHeight( new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
                                topside,
                                DISTANCE_THRESHOLD_IN_MILS,
                                MINIMUM_POINTS_NEEDED_FOR_ESTIMATE,
                                estimateIsValid );
  }
   */

  /**
   * This can be call from FocusConfirmation and SurfaceModeling
   * @author Peter Esbensen
   * @author Siew Yeng XCR-3781 - added parameter region and doSurfaceModeling for debugging purpose
   * @param doSurfaceModeling - pass true if call from SurfaceModel, pass false if call from FocusConfirmation (XCR-3848)
   */
  public synchronized int getEstimatedZHeight(Point2D point2D,
                                              boolean topside,
                                              int thresholdForNeighborsInMils,
                                              int numberOfNeighborsRequired,
                                              BooleanRef estimateIsValid,
                                              ReconstructionRegion region,
                                              boolean doSurfaceModeling)
  {
    Assert.expect(point2D != null);
    Assert.expect(estimateIsValid != null);

    estimateIsValid.setValue(false);

    int numTopSidePoints = _topSidePoints.size();
    int numBottomSidePoints = _bottomSidePoints.size();
    
    int finalZHeight = -1;
    //if ((topside && (numTopSidePoints > 0)) ||
    //    ((topside == false) && (numBottomSidePoints > 0)))
    //Siew Yeng - XCR-2151 - Incorrect Z-Height on Pin Side Slice for PTH When Using PSH on Single-Sided Board
    // - Allow to estimate z-height when either top or bottom side have points - handle single sided board
    if((numTopSidePoints > 0) || (numBottomSidePoints > 0))
    {
      Map<Point2D, Integer> pointsToUseForEstimate = getNearbyPoints(point2D, thresholdForNeighborsInMils, topside);
      Collection<Integer> zHeightCollection = new ArrayList<Integer>();
      zHeightCollection.addAll(pointsToUseForEstimate.values());

      //Siew Yeng - XCR-3781 - PSH debug ONLY
      if(_ENABLE_SURFACE_MODEL_INFO_LOG && doSurfaceModeling) //XCR-3848
      {
        for(Map.Entry<Point2D, Integer> mapEntry : pointsToUseForEstimate.entrySet())
        {
          ReconstructionRegion neighbourRegion;
          if(topside)
            neighbourRegion = _topSidePoints.get(mapEntry.getKey()).getFirst();
          else
            neighbourRegion = _bottomSidePoints.get(mapEntry.getKey()).getFirst();

          _surfaceModelLogger.logNeighborInfo(topside, neighbourRegion, thresholdForNeighborsInMils, mapEntry.getKey(), mapEntry.getValue());
        }
      }
      // if we have points on both sides of the board, then we can use the opposite side points in conjunction with a board thickness estimate
      //if ((numTopSidePoints > 0) && (numBottomSidePoints > 0))
      //{
      //Siew Yeng - XCR-2151
      // - Always check opposite side points to estimate z-height
      Map<Point2D, Integer> oppositeSidePointsToUseForEstimate = getNearbyPoints(point2D, thresholdForNeighborsInMils, !topside);
      
      for (Integer zHeight : oppositeSidePointsToUseForEstimate.values())
      {
        if (topside)
        {
          zHeightCollection.add(new Integer(zHeight.intValue() + Math.round(getEstimatedBoardThickness())));
        }
        else
        {
          zHeightCollection.add(new Integer(zHeight.intValue() - Math.round(getEstimatedBoardThickness())));
        }
      }
      
      //Siew Yeng - XCR-3781 - PSH debug ONLY
      if(_ENABLE_SURFACE_MODEL_INFO_LOG && doSurfaceModeling) //XCR-3848
      {
        for(Map.Entry<Point2D, Integer> mapEntry : oppositeSidePointsToUseForEstimate.entrySet())
        {
          ReconstructionRegion neighbourRegion;
          int zHeight;
          if(topside)
          {
            neighbourRegion = _bottomSidePoints.get(mapEntry.getKey()).getFirst();
            zHeight = mapEntry.getValue()+ Math.round(getEstimatedBoardThickness());
          }
          else
          {
            neighbourRegion = _topSidePoints.get(mapEntry.getKey()).getFirst();
            zHeight = mapEntry.getValue() - Math.round(getEstimatedBoardThickness());
          }

          _surfaceModelLogger.logNeighborInfo(!topside, neighbourRegion, thresholdForNeighborsInMils, mapEntry.getKey(), zHeight);
        }
      }
      //}

      if (zHeightCollection.size() >= numberOfNeighborsRequired)
      {
        float[] zHeightArray = convertIntegerCollectionToFloatArray(zHeightCollection);
        float medianZHeight = StatisticsUtil.median(zHeightArray);
        
        //Siew Yeng - XCR-3883 - PTH pin side is at the opposite side of the board
        if(region.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE))
        {
          if(topside)
            medianZHeight = medianZHeight - Math.round(getEstimatedBoardThickness());
          else
            medianZHeight = medianZHeight + Math.round(getEstimatedBoardThickness());
        }
        
        estimateIsValid.setValue(true);

        if (_LOG_FOCUS_CONFIRMATION_DATA)
        {
          if ((_topSidePoints.size() > 0) && (_bottomSidePoints.size() > 0))
            System.out.println("point numPoints boardThickness " + Math.round(point2D.getX()) + " " + Math.round(point2D.getY()) + " " +
                               zHeightCollection.size() + " " + Math.round(getEstimatedBoardThickness()));
        }

        //Siew Yeng - XCR-3781
        //return Math.round(medianZHeight);
        finalZHeight = Math.round(medianZHeight);
      }
    }
    
    //Siew Yeng - XCR-3781 - PSH DEBUG ONLY
    if(_ENABLE_SURFACE_MODEL_INFO_LOG && doSurfaceModeling) //XCR-3848
    {
      //Siew Yeng - XCR-3866 - should log component (board) side instead of surface model side
      _surfaceModelLogger.logEstimatedZHeightInfo(region.isTopSide(), region, thresholdForNeighborsInMils, point2D, 
                                                  Math.round(getEstimatedBoardThickness()), Math.round(_cadBoardThickness), finalZHeight);
    }
    
    return finalZHeight;
  }
  
  /**
   * Estimate zheight method with pshSettings. Call this method for SurfaceModeling only (PSH)
   * @author Siew Yeng - XCR-3781
   */
  public synchronized int getEstimatedZHeight(Point2D point2D,
                                              boolean topside,
                                              int thresholdForNeighborsInMils,
                                              int numberOfNeighborsRequired,
                                              BooleanRef estimateIsValid,
                                              PshSettings pshSettings,
                                              ReconstructionRegion region)
  {
    Assert.expect(point2D != null);
    Assert.expect(estimateIsValid != null);
    
    if(pshSettings.isEnableSelectiveBoardSide() == false ||
       (pshSettings.isEnableSelectiveBoardSide() && pshSettings.getPshSettingsBoardSideEnum().equals(PshSettingsBoardSideEnum.BOTH)))
    {
      return getEstimatedZHeight(point2D, topside, thresholdForNeighborsInMils, numberOfNeighborsRequired, estimateIsValid, region, true);//XCR-3848
    }
    
    estimateIsValid.setValue(false);

    int numTopSidePoints = _topSidePoints.size();
    int numBottomSidePoints = _bottomSidePoints.size();
    
    Collection<Integer> zHeightCollection = new ArrayList<Integer>();
    
    PshSettingsBoardSideEnum pshBoardSideEnum = pshSettings.getPshSettingsBoardSideEnum();
    if(pshBoardSideEnum.equals(PshSettingsBoardSideEnum.SAME_SIDE_WITH_COMPONENT))
    {
      if((topside && numTopSidePoints > 0) ||
         (topside == false && numBottomSidePoints > 0))
      {
        zHeightCollection.addAll(getNearbyPointZHeightList(point2D, thresholdForNeighborsInMils, topside, topside));
      }
    }
    else if(pshBoardSideEnum.equals(PshSettingsBoardSideEnum.TOP))
    {
      if(numTopSidePoints > 0)
      {
        zHeightCollection.addAll(getNearbyPointZHeightList(point2D, thresholdForNeighborsInMils, topside, true));
      }
    }
    else if(pshBoardSideEnum.equals(PshSettingsBoardSideEnum.BOTTOM))
    {
      if(numBottomSidePoints > 0)
      {
        zHeightCollection.addAll(getNearbyPointZHeightList(point2D, thresholdForNeighborsInMils, topside, false));
      }
    }
    
    int zHeight = -1;
    if (zHeightCollection.size() > 0 && zHeightCollection.size() >= numberOfNeighborsRequired)
    {
      float[] zHeightArray = convertIntegerCollectionToFloatArray(zHeightCollection);
      float medianZHeight = StatisticsUtil.median(zHeightArray);
      
      //Siew Yeng - XCR-3883 - PTH pin side is at the opposite side of the board
      if(region.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE))
      {
        if(topside)
          medianZHeight = medianZHeight - Math.round(getEstimatedBoardThickness());
        else
          medianZHeight = medianZHeight + Math.round(getEstimatedBoardThickness());
      }
      
      estimateIsValid.setValue(true);

      if (_LOG_FOCUS_CONFIRMATION_DATA)
      {
        if ((_topSidePoints.size() > 0) || (_bottomSidePoints.size() > 0))
          System.out.println("point numPoints boardThickness zHeight" + Math.round(point2D.getX()) + " " + Math.round(point2D.getY()) + " " +
                             zHeightCollection.size() + " " + Math.round(getEstimatedBoardThickness()) + " " + Math.round(medianZHeight));
      }
      //return Math.round(medianZHeight);
      zHeight = Math.round(medianZHeight);
    }
    
    if(_ENABLE_SURFACE_MODEL_INFO_LOG)
    {
      //Siew Yeng - XCR-3866 - should log component (board) side instead of surface model side
      _surfaceModelLogger.logSelectiveBoardSideEstimatedZHeightInfo(region.isTopSide(), region, thresholdForNeighborsInMils, point2D, 
                                                  Math.round(getEstimatedBoardThickness()), Math.round(_cadBoardThickness), zHeight);
    }
    
    return zHeight;
  }
  
  /**
   * Only for SurfaceModeling (PSH)
   * @author John Heumann
   * @author Siew Yeng - XCR-3781
   * @param pshSettings - for Surface Model settings
   * @param region - for debug purpose
   */
  public synchronized int getEstimatedZHeight(SystemFiducialRectangle systemFiducialRectangle,
                                                        boolean isTopside,
                                                        int maxNeighborDistanceInMils,
                                                        int minNumberOfNeighbors,
                                                        BooleanRef estimateIsValid,
                                                        PshSettings pshSettings,
                                                        ReconstructionRegion region)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);
    Assert.expect(maxNeighborDistanceInMils >= 0);
    Assert.expect(minNumberOfNeighbors >= 1);

    return getEstimatedZHeight( new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
                                isTopside,
                                maxNeighborDistanceInMils,
                                minNumberOfNeighbors,
                                estimateIsValid,
                                pshSettings,
                                region);
  }

  /**
   * @author Peter Esbensen
   * @author Chnee Khang Wah, 2013-08-19, Real Time PSH, need to synchroniced this function as this function now will be call
   * before _topSidePoints and _bottomSidePoints is completely filled.
   */
  private synchronized static Map<Point2D, Integer> getNearbyPoints(Point2D point,
                                                       int distanceThresholdInMils,
                                                       boolean topside)
  {
    Assert.expect(point != null);

    Map<Point2D, Integer> points = new HashMap<Point2D,Integer>();
    double distanceThresholdInNanometers = MathUtil.convertMilsToNanoMeters(distanceThresholdInMils);
    if (topside)
    {
      //Siew Yeng - XCR-3781
      for (Map.Entry<Point2D, Pair<ReconstructionRegion, Integer>> mapEntry : _topSidePoints.entrySet())
      {
        double distance = point.distance(mapEntry.getKey());
        if ((distance < distanceThresholdInNanometers) && (MathUtil.fuzzyEquals(0.0, distance) == false))
        {
          points.put(mapEntry.getKey(), mapEntry.getValue().getSecond());
        }
      }
    }
    else // bottomside
    {
      for (Map.Entry<Point2D, Pair<ReconstructionRegion, Integer>> mapEntry : _bottomSidePoints.entrySet())
      {
        double distance = point.distance(mapEntry.getKey());
        if ((distance < distanceThresholdInNanometers) && (MathUtil.fuzzyEquals(0.0, distance) == false))
        {
          points.put(mapEntry.getKey(), mapEntry.getValue().getSecond());
        }
      }
    }
    return points;
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized float getEstimatedBoardThickness()
  {
    if (_topSidePoints.size() == 0 || _bottomSidePoints.size() == 0)
      return _cadBoardThickness;

    if (_boardThicknessEstimateIsValid)
      return _estimatedBoardThickness;

    //Siew Yeng - XCR-3781
    Collection<Pair<ReconstructionRegion,Integer>> topSideZHeightIntegers= _topSidePoints.values();
    float[] topsideZHeights = new float[ topSideZHeightIntegers.size() ];
    Collection<Pair<ReconstructionRegion,Integer>> bottomSideZHeightIntegers = _bottomSidePoints.values();
    float[] bottomSideZHeights = new float[bottomSideZHeightIntegers.size() ];
    int index = 0;
    for (Pair<ReconstructionRegion,Integer> zHeight : topSideZHeightIntegers)
    {
      topsideZHeights[index] = zHeight.getSecond();
      ++index;
    }
    index = 0;
    for (Pair<ReconstructionRegion,Integer> zHeight : bottomSideZHeightIntegers)
    {
      bottomSideZHeights[index] = zHeight.getSecond();
      ++index;
    }

    List<Float> boardThicknessEstimates = new LinkedList<Float>();
    Map<Point2D, Point2D> topToBottomSidePairingsAlreadyVisited = new HashMap<Point2D, Point2D>();
    for (Map.Entry<Point2D, Point2D> mapEntry : _topSideToNearestBottomSidePointMap.entrySet())
    {
      Point2D nearestBottomSidePoint = mapEntry.getValue();
      if (nearestBottomSidePoint != null)
      {
        Point2D topSidePoint = mapEntry.getKey();
        double distance = topSidePoint.distance(nearestBottomSidePoint);
        if (distance <= 25400.0) {   // only use opposite side points within 1 inch
          Integer topSideZHeight = _topSidePoints.get(topSidePoint).getSecond();
          Assert.expect(topSideZHeight != null);
          Integer bottomSideZHeight = _bottomSidePoints.get(nearestBottomSidePoint).getSecond();
          float boardThicknessEstimate = topSideZHeight - bottomSideZHeight;
          boardThicknessEstimates.add(boardThicknessEstimate);
          topToBottomSidePairingsAlreadyVisited.put(topSidePoint, nearestBottomSidePoint);
        }
      }
    }
    for (Map.Entry<Point2D, Point2D> mapEntry : _bottomSideToNearestTopSidePointMap.entrySet())
    {
      Point2D nearestTopSidePoint = mapEntry.getValue();
      if (nearestTopSidePoint != null)
      {
        Point2D bottomSidePoint = mapEntry.getKey();
        Point2D alreadyVisited = topToBottomSidePairingsAlreadyVisited.get(nearestTopSidePoint);
        if (alreadyVisited != bottomSidePoint)
        {
          double distance = bottomSidePoint.distance(nearestTopSidePoint);
          if (distance <= 25400.0) {   // only use opposite side points within 1 inch
            Integer topSideZHeight = _topSidePoints.get(nearestTopSidePoint).getSecond();
            Assert.expect(topSideZHeight != null);
            Integer bottomSideZHeight = _bottomSidePoints.get(bottomSidePoint).getSecond();
            float boardThicknessEstimate = topSideZHeight - bottomSideZHeight;
            boardThicknessEstimates.add(boardThicknessEstimate);
          }
        }
      }
    }

    //    float estimatedBoardThickness = StatisticsUtil.median(topsideZHeights) - StatisticsUtil.median(bottomSideZHeights);

    _boardThicknessEstimateIsValid = true;
    _estimatedBoardThickness = _cadBoardThickness;
    if (_topSidePoints.size() >= 3 && _bottomSidePoints.size() >= 3)
    {
      int n = boardThicknessEstimates.size();
      if (n > 5)
      {
        float[] boardThicknessEstimatesArray = ArrayUtil.convertFloatListToFloatArray(boardThicknessEstimates);
        float dynamicThicknessEstimate = StatisticsUtil.median(boardThicknessEstimatesArray);
        _estimatedBoardThickness =
          (5.0f * _cadBoardThickness + n * dynamicThicknessEstimate) / (5 + n);
      }
    }

    return _estimatedBoardThickness;
  }

  /**
   * Following 3 routines use quadratic surface fitting rather than a median to predict z height.
   * They are not currently used, but should be preserved in case we decide to at some point.
   * Note that a cleaner implementation would be to combine getEstimatedZHeight and getQuadraticallyEstimatedZHeight
   * into a single routine which would automatically choose the best method. (For example, median will frequently
   * be better at extrapolation, while quadratic will be able to follow curvature during interpolation.
   * This suggests that one might want to use quadratic if the point to be predicted lies with the convex hull
   * of the neighboring points. However, it's unclear if the benefit would justify the expense of computing the
   * convex hull.
   */

  /**
   * @author Peter Esbensen
   * @author John Heumann
   */
  /***
  public synchronized int getBestQuadraticallyEstimatedZHeight(SystemFiducialRectangle systemFiducialRectangle,
                                                               boolean topside,
                                                               BooleanRef estimateIsValid)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);

    final int DISTANCE_THRESHOLD_IN_MILS = 750;
    final int MINIMUM_POINTS_NEEDED_FOR_ESTIMATE = 5;

    return getQuadraticallyEstimatedZHeight(
      new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
      topside,
      DISTANCE_THRESHOLD_IN_MILS,
      MINIMUM_POINTS_NEEDED_FOR_ESTIMATE,
      estimateIsValid);
  }
  ***/

  /**
   * @author Peter Esbensen
   * @author John Heumann
   */
  /***
  public synchronized int getReasonablyQuadraticallyEstimatedZHeight(SystemFiducialRectangle systemFiducialRectangle,
                                                                     boolean topside,
                                                                     BooleanRef estimateIsValid)
  {
    Assert.expect(systemFiducialRectangle != null);
    Assert.expect(estimateIsValid != null);

    final int DISTANCE_THRESHOLD_IN_MILS = 1500;
    final int MINIMUM_POINTS_NEEDED_FOR_ESTIMATE = 5;

    return getQuadraticallyEstimatedZHeight(
      new Point2D.Double(systemFiducialRectangle.getCenterX(), systemFiducialRectangle.getCenterY()),
      topside,
      DISTANCE_THRESHOLD_IN_MILS,
      MINIMUM_POINTS_NEEDED_FOR_ESTIMATE,
      estimateIsValid);
  }
  ***/

  /**
    * @author Peter Esbensen
    * @author John Heumann
    */
   /***
   public synchronized int getQuadraticallyEstimatedZHeight(Point2D point2D,
                                                            boolean topside,
                                                            int thresholdForNeighborsInMils,
                                                            int numberOfNeighborsRequired,
                                                            BooleanRef estimateIsValid)
   {
     Assert.expect(point2D != null);
     Assert.expect(estimateIsValid != null);

     estimateIsValid.setValue(false);

     int numTopSidePoints = _topSidePoints.size();
     int numBottomSidePoints = _bottomSidePoints.size();
     if ((topside && (numTopSidePoints > 0)) ||
         ((topside == false) && (numBottomSidePoints > 0)))
     {
       Map<Point2D, Integer> pointsToUseForEstimate = getNearbyPoints(point2D, thresholdForNeighborsInMils, topside);
       Map<Point2D, Integer> oppositeSidePointsToUseForEstimate = null;

       // if we have points on both sides of the board, then we can use the opposite side points in conjunction with a board thickness estimate
       if ((numTopSidePoints > 0) && (numBottomSidePoints > 0))
         oppositeSidePointsToUseForEstimate = getNearbyPoints(point2D, thresholdForNeighborsInMils, !topside);

       int n = pointsToUseForEstimate.size();
       if (oppositeSidePointsToUseForEstimate != null)
         n += oppositeSidePointsToUseForEstimate.size();

       double x[] = new double[n];
       double y[] = new double[n];
       double z[] = new double[n];
       int i = 0;

       Set<Map.Entry<Point2D, Integer>> entries = pointsToUseForEstimate.entrySet();
       for (Map.Entry<Point2D, Integer> entry : entries)
       {
         x[i] = entry.getKey().getX();
         y[i] = entry.getKey().getY();
         z[i++] = entry.getValue();
       }

       if (oppositeSidePointsToUseForEstimate != null)
       {
         entries = oppositeSidePointsToUseForEstimate.entrySet();
         for (Map.Entry<Point2D, Integer> entry : entries)
         {
           x[i] = entry.getKey().getX();
           y[i] = entry.getKey().getY();
           z[i] = entry.getValue();
           if (topside)
             z[i++] += getEstimatedBoardThickness();
           else
             z[i++] -= getEstimatedBoardThickness();
         }
       }
       if (n >= numberOfNeighborsRequired)
       {
         try
         {
           // Note that they type of fit specified below determines a minimum value for numberOfNeighborsRequired.
           // For BILINEAR, the minimum is 4. For LINEAR_X_QUADRATIC_Y it is 5. For QUADRATIC it is 6.
           QuadraticSurfaceEstimator qse =
               new QuadraticSurfaceEstimator(x, y, z, QuadraticSurfaceEstimatorTypeEnum.LINEAR_X_QUADRATIC_Y);
           double predictedZHeight = qse.getEstimatedHeight(point2D.getX(), point2D.getY());

           estimateIsValid.setValue(true);

           if (_LOG_FOCUS_CONFIRMATION_DATA)
           {
             if ((_topSidePoints.size() > 0) && (_bottomSidePoints.size() > 0))
               System.out.println("point numPoints boardThickness " + Math.round(point2D.getX()) + " " +
                                  Math.round(point2D.getY()) + " " + n + " " + Math.round(getEstimatedBoardThickness()));
           }

           return (int)(Math.round(predictedZHeight));
         }
         catch (Throwable ex)
         {
           // silently do nothing
         }
       }
     }

     return -1;
   }
   ***/
  
  /**
   * Selective component (PSH)
   * @author Siew Yeng
   * @param topside component board side
   */
  public synchronized int getSelectiveNeighboursEstimatedZHeight(PshSettings pshSettings,
                                                                    ReconstructionRegion region,
                                                                    boolean topside,
                                                                    BooleanRef estimateIsValid)
  {
    Assert.expect(estimateIsValid != null);

    estimateIsValid.setValue(false);
    Collection<Integer> zHeightCollection = new ArrayList<Integer>();

    for(Component neighbour : pshSettings.getSelectiveNeighbors(region.getComponent()))
    {
      if(_componentToRegionAndZHeightMap.containsKey(neighbour))
      {
        boolean neighbourIsTopSide = neighbour.isTopSide();
        
        for(Pair<ReconstructionRegion,Integer> regionAndzHeightPair : _componentToRegionAndZHeightMap.get(neighbour))
        {
          int zHeight = regionAndzHeightPair.getSecond();
          
          if(topside)
          {
            if(neighbourIsTopSide == false)
              zHeight = regionAndzHeightPair.getSecond() + Math.round(getEstimatedBoardThickness());
          }
          else
          {
            if(neighbourIsTopSide)
              zHeight = regionAndzHeightPair.getSecond() - Math.round(getEstimatedBoardThickness());
          }

          zHeightCollection.add(zHeight);

          if(_ENABLE_SURFACE_MODEL_INFO_LOG)
          {
            _surfaceModelLogger.logSelectiveNeighborInfo(neighbourIsTopSide, regionAndzHeightPair.getFirst(), zHeight);
          }
        }
      }
    }
    
    int zHeight = -1;
    if (zHeightCollection.size() > 0)
    {
      float[] zHeightArray = convertIntegerCollectionToFloatArray(zHeightCollection);
      float medianZHeight = StatisticsUtil.median(zHeightArray);
      
      //Siew Yeng - XCR-3883 - PTH pin side is at the opposite side of the board
      if(region.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE))
      {
        if(topside)
          medianZHeight = medianZHeight - Math.round(getEstimatedBoardThickness());
        else
          medianZHeight = medianZHeight + Math.round(getEstimatedBoardThickness());
      }
      
      estimateIsValid.setValue(true);
      
      zHeight = Math.round(medianZHeight);
    }

    if(_ENABLE_SURFACE_MODEL_INFO_LOG)
    {
      //Siew Yeng - XCR-3866 - should log component (board) side instead of surface model side
      _surfaceModelLogger.logSelectiveNeigborEstimatedZHeightInfo(region.isTopSide(), region, Math.round(getEstimatedBoardThickness()), 
                                                                  Math.round(_cadBoardThickness), zHeight);
    }
    
    return zHeight;
  }
  
  /**
   * Selective board side (PSH)
   * @author Siew Yeng
   */
  private synchronized List<Integer> getNearbyPointZHeightList(Point2D point,
                                       int distanceThresholdInMils,
                                       boolean topside,
                                       boolean neighborIsTopSide)
  {
    Map<Point2D, Integer> pointsToUseForEstimate = getNearbyPoints(point, distanceThresholdInMils, neighborIsTopSide);
    List<Integer> zheightList = new ArrayList();
    
    if(topside == neighborIsTopSide)
    {
      zheightList.addAll(pointsToUseForEstimate.values());
    }
    else
    {
      for(Map.Entry<Point2D, Integer> mapEntry : pointsToUseForEstimate.entrySet())
      {
        int zHeight = mapEntry.getValue();

        if(topside && neighborIsTopSide == false)
          zheightList.add(zHeight + Math.round(getEstimatedBoardThickness()));
        else if(topside == false && neighborIsTopSide)
          zheightList.add(zHeight - Math.round(getEstimatedBoardThickness()));
      }
    }
    
    if(_ENABLE_SURFACE_MODEL_INFO_LOG)
    {
      for(Map.Entry<Point2D, Integer> mapEntry : pointsToUseForEstimate.entrySet())
      {
        int finalZHeight;
        
        if(topside && neighborIsTopSide == false)
          finalZHeight = mapEntry.getValue() + Math.round(getEstimatedBoardThickness());
        else if(topside == false && neighborIsTopSide)
          finalZHeight = mapEntry.getValue() - Math.round(getEstimatedBoardThickness());
        else
          finalZHeight = mapEntry.getValue();
        
        ReconstructionRegion region;
        
        if(neighborIsTopSide)
          region = _topSidePoints.get(mapEntry.getKey()).getFirst();
        else
          region = _bottomSidePoints.get(mapEntry.getKey()).getFirst();
        
        _surfaceModelLogger.logNeighborInfo(neighborIsTopSide, region, distanceThresholdInMils, point, finalZHeight);
      }
    }
            
    return zheightList;
  }
}
