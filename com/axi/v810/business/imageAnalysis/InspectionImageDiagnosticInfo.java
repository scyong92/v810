package com.axi.v810.business.imageAnalysis;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Patrick Lacz
 */
public class InspectionImageDiagnosticInfo extends DiagnosticInfo
{
  Map<SliceNameEnum, Image> _images = null;
  int _reconstructionRegionId;
  SliceNameEnum _selectedSlice = null;

  /**
   * @author Patrick Lacz
   */
  public InspectionImageDiagnosticInfo(ReconstructedImages reconstructedImages, SliceNameEnum sliceToDisplay, Algorithm algorithm)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceToDisplay != null);

    _selectedSlice = sliceToDisplay;

    _images = new HashMap<SliceNameEnum, Image>();

    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
    {
      //added by sheng chuan because short only use image without enhance during inspection. should not post enhance image.
      if(algorithm instanceof SharedShortAlgorithm)
        _images.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getOrthogonalImageWithoutEnhanced()));
      else
        _images.put(reconstructedSlice.getSliceNameEnum(), new Image(reconstructedSlice.getOrthogonalImage()));
    }

    ImageEnhancement.autoEnhanceContrastInPlace(ImageManager.getInstance().excludeUnnecessaryImageFromEnhancement(_images).values());

    Assert.expect(_images.containsKey(sliceToDisplay));
  }

  /**
   * @author Patrick Lacz
   */
  public Image getImage()
  {
    return _images.get(_selectedSlice);
  }

  /**
   * @author Patrick Lacz
   */
  public void incrementReferenceCount()
  {
    for (Image image : _images.values())
      image.incrementReferenceCount();
  }

  /**
   * @author Patrick Lacz
   */
  public void decrementReferenceCount()
  {
    for (Image image : _images.values())
      image.decrementReferenceCount();
  }
}
