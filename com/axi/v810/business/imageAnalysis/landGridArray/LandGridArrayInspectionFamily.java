package com.axi.v810.business.imageAnalysis.landGridArray;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for land grid arrays and quad flat noleads (QFNs).
 *
 * @author Peter Esbensen
 */
public class LandGridArrayInspectionFamily extends InspectionFamily
{
  /**
   * @author Peter Esbensen
   */
  public LandGridArrayInspectionFamily()
  {
    super(InspectionFamilyEnum.LAND_GRID_ARRAY);
    Algorithm landGridArrayMeasurementAlgorithm = new LandGridArrayMeasurementAlgorithm(InspectionFamilyEnum.LAND_GRID_ARRAY);
    addAlgorithm(landGridArrayMeasurementAlgorithm);
    Algorithm landGridArrayShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.LAND_GRID_ARRAY);
    addAlgorithm(landGridArrayShortAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    return Collections.singletonList(SliceNameEnum.PAD);
  }

  /**
   * Returns the applicable default SliceNameEnum for a LandGridArray
   *
   * @author Matt Wharton
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // We always use the traditional pad slice for land grid arrays.
    return SliceNameEnum.PAD;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return getDefaultPadSliceNameEnum(subtype);
  }

  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return false;
  }

}
