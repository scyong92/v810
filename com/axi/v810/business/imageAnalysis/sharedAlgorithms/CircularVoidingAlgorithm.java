package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.XrayTesterException;
import com.virtualLive.virtualLive.util.StringLocalizer;
import java.awt.geom.*;
import java.text.DecimalFormat;

/**
 *
 * @author chin-seong.kee
 */
public class CircularVoidingAlgorithm
{
    private static InspectionFamily _inspectionFamily;
    private static Algorithm _algorithm;

    public static class VoidDefectDispositionEnum extends com.axi.util.Enum
    {
      private static int _index = 0;
      
      public static final VoidDefectDispositionEnum DEFINITE_VOID = new VoidDefectDispositionEnum(_index++);
      public static final VoidDefectDispositionEnum QUESTIONABLE_VOID = new VoidDefectDispositionEnum(_index++);
      public static final VoidDefectDispositionEnum EXONERATED_VOID = new VoidDefectDispositionEnum(_index++);
      
      /**
       * @author Matt Wharton
       */
      private VoidDefectDispositionEnum(int id)
      {
        super(id);
      }
    }
    
    /*
     * @author Kee Chin Seong - Creating new algorithm settings for circular voiding.
     */
    public static Collection<AlgorithmSetting> createSharedVoidingAlgorithmSettings(InspectionFamily inspectionFamily, 
                                                                                Algorithm algorithm, JointTypeEnum jointTypeEnum,
                                                                                SliceNameEnum sliceNameEnum,
                                                                                int displayOrder, int currentVersion)
    {
       _inspectionFamily = inspectionFamily;
       _algorithm = algorithm;
       
       ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();
        /*
        * @author Kee Chin Seong - new Circular Voding Algorithm
        */
       AlgorithmSetting voidGreyLevelNominal = new AlgorithmSetting(
           AlgorithmSettingEnum.SHARED_VOIDING_GREY_LEVEL_NOMINAL,
           displayOrder++,
           1.0f, // default value
           0.0f, // minimum value
           300.0f, // maximum value
           MeasurementUnitsEnum.NONE,
           "HTML_DESC_LARGEPAD_VOIDING_(GREY_LEVEL_NOMINAL)_KEY", // description URL key
           "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(GREY_LEVEL_NOMINAL)_KEY", // desailed description URL key
           "IMG_DESC_LARGEPAD_VOIDING_(GREY_LEVEL_NOMINAL)_KEY", // image description URL key
           AlgorithmSettingTypeEnum.STANDARD,
           currentVersion);   
       settingList.add(voidGreyLevelNominal);

       inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                             voidGreyLevelNominal,
                                                             sliceNameEnum,
                                                             MeasurementEnum.SHARED_VOIDING_DEFINITE_VOIDING_GREY_LEVEL);


        /*
        * @author Kee Chin Seong - new Circular Voding Algorithm
        */
       ArrayList<String> allowableRegionReferencePositionValues = new ArrayList<String>(Arrays.asList("Located Position",
                                                                                                      "CAD Position"));
       AlgorithmSetting regionReferencePositionSetting = new AlgorithmSetting(
           AlgorithmSettingEnum.SHARED_VOIDING_REGION_REFERENCE_POSITION,
           displayOrder++,
           "Located Position",
           allowableRegionReferencePositionValues,
           MeasurementUnitsEnum.NONE,
           "HTML_DESC_LARGEPAD_VOIDING_(REGION_REFERENCE_POSITION)_KEY",
           "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(REGION_REFERENCE_POSITION)_KEY",
           "IMG_DESC_LARGEPAD_VOIDING_(REGION_REFERENCE_POSITION)_KEY",
           AlgorithmSettingTypeEnum.ADDITIONAL,
           currentVersion);
       settingList.add(regionReferencePositionSetting); 

        /*
        * @author Kee Chin Seong - new Circular Voding Algorithm
        */
       AlgorithmSetting greyLevelDifferenceFromNominal = new AlgorithmSetting(
           AlgorithmSettingEnum.SHARED_VOIDING_DETECTION_SENSITIVITY,
           displayOrder++,
           10.0f, // default value
           0.0f, // minimum value
           100.0f, // maximum value
           MeasurementUnitsEnum.PERCENT,
           "HTML_DESC_LARGEPAD_VOIDING_(GREY_LEVEL_SENSITIVITY_DETECTION)_KEY",
           "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(GREY_LEVEL_SENSITIVITY_DETECTION)_KEY",
           "IMG_DESC_LARGEPAD_VOIDING_(GREY_LEVEL_SENSITIVITY_DETECTION)_KEY",
           AlgorithmSettingTypeEnum.STANDARD,
           currentVersion);
       settingList.add(greyLevelDifferenceFromNominal);
       inspectionFamily.mapAlgorithmSettingToMeasurementEnum(jointTypeEnum,
                                                             greyLevelDifferenceFromNominal,
                                                             sliceNameEnum,
                                                             MeasurementEnum.SHARED_VOIDING_DIFFERENCE_FROM_NOMINAL_PERCENTAGE);

       // Region inner edge distance.
       AlgorithmSetting innerEdgeLocationSetting = new AlgorithmSetting(
           AlgorithmSettingEnum.SHARED_VOIDING_INNER_EDGE_POSITION,
           displayOrder++,
           15.f,
           0f,
           200f,
           MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
           "HTML_DESC_LARGEPAD_VOIDING_(REGION_INNER_EDGE)_KEY",
           "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(REGION_INNER_EDGE)_KEY",
           "IMG_DESC_LARGEPAD_VOIDING_(REGION_INNER_EDGE)_KEY",
           AlgorithmSettingTypeEnum.STANDARD,
           currentVersion);
       settingList.add(innerEdgeLocationSetting);

       // Region outer edge distance.
       AlgorithmSetting outerEdgeLocationSetting = new AlgorithmSetting(
           AlgorithmSettingEnum.SHARED_VOIDING_OUTER_EDGE_POSITION,
           displayOrder++,
           75.f,
           0f,
           //100f,
           200f,
           MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
           "HTML_DESC_LARGEPAD_VOIDING_(REGION_OUTER_EDGE)_KEY",
           "HTML_DETAILED_DESC_LARGEPAD_VOIDING_(REGION_OUTER_EDGE)_KEY",
           "IMG_DESC_LARGEPAD_VOIDING_(REGION_OUTER_EDGE)_KEY",
           AlgorithmSettingTypeEnum.STANDARD,
           currentVersion);
       settingList.add(outerEdgeLocationSetting);
       
       return settingList;
    }
    
  /*
   * @Author Kee Chin Seong
   */
  private static ProfileDiagnosticInfo createVoidingProfileDiagnosticInfo(
      JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum,
      float[] greyLevelProfile,
      Map<Pair<Integer, Integer>, VoidDefectDispositionEnum> voidZoneToDispositionMap)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(greyLevelProfile != null);
    Assert.expect(voidZoneToDispositionMap != null);

    // Create a mapping identifying each profile bin as background or some kind of defect bin (definite, questionable,
    // or exonerated).
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer,Integer>,MeasurementRegionEnum>();
    // All the bins are initially marked as background.
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, greyLevelProfile.length),
                                                  MeasurementRegionEnum.VOID_BACKGROUND_REGION);
    // Mark all the void defect zones accordingly.
    for (Map.Entry<Pair<Integer, Integer>, VoidDefectDispositionEnum> mapEntry : voidZoneToDispositionMap.entrySet())
    {
      Pair<Integer, Integer> voidZoneBins = mapEntry.getKey();
      int startBin = voidZoneBins.getFirst();
      Assert.expect((startBin >= 0) && (startBin < greyLevelProfile.length));
      int endBin = voidZoneBins.getSecond();
      Assert.expect((endBin >= 0) && (endBin < greyLevelProfile.length));

      MeasurementRegionEnum defectRegionType = null;
      VoidDefectDispositionEnum voidDisposition = mapEntry.getValue();
      if (voidDisposition.equals(VoidDefectDispositionEnum.DEFINITE_VOID))
      {
        defectRegionType = MeasurementRegionEnum.DEFINITE_VOID_DEFECT_REGION;
      }
      else if (voidDisposition.equals(VoidDefectDispositionEnum.QUESTIONABLE_VOID))
      {
        defectRegionType = MeasurementRegionEnum.QUESTIONABLE_VOID_DEFECT_REGION;
      }
      else if (voidDisposition.equals(VoidDefectDispositionEnum.EXONERATED_VOID))
      {
        defectRegionType = MeasurementRegionEnum.EXONERATED_VOID_DEFECT_REGION;
      }
      else
      {
        // Shouldn't get here!
        Assert.expect(false, "Unexpected voiding defect disposition : " + voidDisposition);
      }

      // Add the voiding zone bins to our map.
      if (startBin <= endBin)
      {
        // Void is NOT a "wrap around" void.  Just add the subrange to the map.
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(startBin, endBin + 1), defectRegionType);
      }
      else
      {
        // Void is a "wrap around" void.
        // We need to add two subranges to the map: one from startBin to profile end and another from
        // profile start to endBin.
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(startBin, greyLevelProfile.length),
                                                      defectRegionType);
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, endBin + 1), defectRegionType);
      }
    }
    
    // We want the profile to scale from -50% to 200% of the "minimum void thickness" setting.
    float profileMin = StatisticsUtil.minimum(greyLevelProfile, 0, greyLevelProfile.length - 1);
    float profileMax = StatisticsUtil.maximum(greyLevelProfile, 0, greyLevelProfile.length - 1);

    LocalizedString voidProfileLabelLocalizedString = new LocalizedString("ALGDIAG_VOID_PROFILE_LABEL_KEY", null);
    ProfileDiagnosticInfo voidProfileDiagnosticInfo =
        ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                         ProfileTypeEnum.BACKGROUND_PROFILE,
                                                                         voidProfileLabelLocalizedString,
                                                                         profileMin,
                                                                         profileMax,
                                                                         greyLevelProfile,
                                                                         profileSubrangeToMeasurementRegionEnumMap,
                                                                         MeasurementUnitsEnum.NONE);


    return voidProfileDiagnosticInfo;
  }
  
  
  /*
   * @author Kee Chin Seong - new Circular Voding Algorithm
  */
  private static Map<Pair<Integer, Integer>, Pair<Float, Float>> analyzeDeltaGreyLevelProfileForVoid(Image image,
                                                                                     ReconstructionRegion inspectionRegion,
                                                                                     JointInspectionData jointInspectionData,
                                                                                     SliceNameEnum sliceNameEnum,
                                                                                     float[] greyLevelCircularProfile,
                                                                                     final float minimumVoidThicknessInMillis)
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(greyLevelCircularProfile != null);

    // Get the minimum void thickness and length thresholds.
    final float MILLIS_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel();
    
    //following void's length wrap up 
    final float minimumVoidLengthInMillis = 0.057f;
    
    boolean isDetectedAsDefect = false;   

    Map<Pair<Integer, Integer>, Pair<Float, Float>> candidateVoidZoneToLengthAndThicknessMap = new HashMap<Pair<Integer, Integer>, Pair<Float, Float>>();

    boolean measuringPotentialVoid = false;
    Pair<Integer, Integer> voidZoneAtStartBin = null;
    Pair<Integer, Integer> voidZoneAtEndBin = null;
    int startIndexOfCurrentVoid = 0;
    int endIndexOfCurrentVoid = 0;
    for (int i = 0; i < greyLevelCircularProfile.length; ++i)
    {
      if (MathUtil.fuzzyGreaterThanOrEquals(greyLevelCircularProfile[i], minimumVoidThicknessInMillis))
      {
        // If _algorithm is the first bin of _algorithm potential void, update the starting index marker.
        if (measuringPotentialVoid == false)
        {
          startIndexOfCurrentVoid = i;

          // Mark that we're now analyzing a potential void.
          measuringPotentialVoid = true;
        }

        // If we're at the end of the profile, we need to mark _algorithm candidate void
        // region for analysis as a potential wrap-around void.
        if (i == greyLevelCircularProfile.length - 1)
        {
          endIndexOfCurrentVoid = i;
          voidZoneAtEndBin = new Pair<Integer, Integer>(startIndexOfCurrentVoid, endIndexOfCurrentVoid);
          measuringPotentialVoid = false;
        }
      }
      else if (measuringPotentialVoid)
      {
        // Current bin does not exceed the void minimum thickness threshold.  Update the end index marker.
        // The end index is inclusive.
        endIndexOfCurrentVoid = i;

        // If the current candidate void zone began at the start bin, we need to mark _algorithm candidate void
        // region for analysis as a potential wrap-around void.
        if (startIndexOfCurrentVoid == 0)
        {
          voidZoneAtStartBin = new Pair<Integer, Integer>(startIndexOfCurrentVoid, endIndexOfCurrentVoid);
          measuringPotentialVoid = false;
        }
      }

      // If we've reached the end of a candidate void and the void doesn't start on the first bin or end on the
      // last bin (we need to correctly handle voids which "wrap around" the end of the profile back to the start),
      // evaluate it to see if it exceeds the minimum thickness threshold.
      if ((measuringPotentialVoid)
          && (endIndexOfCurrentVoid > startIndexOfCurrentVoid))
      {
        // Is _algorithm void wide enough to call?
        final int voidLengthInPixels = endIndexOfCurrentVoid - startIndexOfCurrentVoid + 1;
        final float lengthOfPotentialVoidInMillis = voidLengthInPixels * MILLIS_PER_PIXEL;
   
        // We have a void.
        //add the maximum delta thickness within the range which causing the void to final result. 
        final float minDeltaThicknessValueInMillis = StatisticsUtil.minimum(greyLevelCircularProfile, startIndexOfCurrentVoid, endIndexOfCurrentVoid);

    
        // Add _algorithm candidate void zone to our list.
        candidateVoidZoneToLengthAndThicknessMap.put(new Pair<Integer, Integer>(startIndexOfCurrentVoid, endIndexOfCurrentVoid),
                                                       new Pair<Float, Float>(lengthOfPotentialVoidInMillis, minDeltaThicknessValueInMillis));
        isDetectedAsDefect = true;

        measuringPotentialVoid = false;

      }
    }

    // Check for "wrap around" voids.
    if ((voidZoneAtStartBin != null) || (voidZoneAtEndBin != null))
    {
      int wrapAroundVoidStartBin = -1;
      int wrapAroundVoidEndBin = -1;
      int wrapAroundVoidLengthInPixels = 0;

      if ((voidZoneAtStartBin != null) && (voidZoneAtEndBin != null))
      {
        wrapAroundVoidStartBin = voidZoneAtEndBin.getFirst();
        wrapAroundVoidEndBin = voidZoneAtStartBin.getSecond();
        wrapAroundVoidLengthInPixels = (greyLevelCircularProfile.length - wrapAroundVoidStartBin) + wrapAroundVoidEndBin + 1;
      }
      else if (voidZoneAtStartBin != null)
      {
        wrapAroundVoidStartBin = voidZoneAtStartBin.getFirst();
        wrapAroundVoidEndBin = voidZoneAtStartBin.getSecond();
        wrapAroundVoidLengthInPixels = wrapAroundVoidEndBin - wrapAroundVoidStartBin + 1;
      }
      else if (voidZoneAtEndBin != null)
      {
        wrapAroundVoidStartBin = voidZoneAtEndBin.getFirst();
        wrapAroundVoidEndBin = voidZoneAtEndBin.getSecond();
        wrapAroundVoidLengthInPixels = wrapAroundVoidEndBin - wrapAroundVoidStartBin + 1;
      }
      else
      {
        // Shouldn't ever occur (logical impossibility).
        Assert.expect(false, "Both start and end void regions are null!");
      }

      Assert.expect(wrapAroundVoidStartBin != -1);
      Assert.expect(wrapAroundVoidEndBin != -1);
      Assert.expect(wrapAroundVoidLengthInPixels > 0);

      // Is the wrap-around void wide enough to call?
      final float lengthOfPotentialWrapAroundVoidInMillis = wrapAroundVoidLengthInPixels * MILLIS_PER_PIXEL;
      
      if (lengthOfPotentialWrapAroundVoidInMillis >= minimumVoidLengthInMillis)
      {
        // We have a wrap-around void.
        // Add _algorithm candidate void zone to our list.
        float firstDeltaThicknessValueInMillis = greyLevelCircularProfile[greyLevelCircularProfile.length - 1];

          firstDeltaThicknessValueInMillis = StatisticsUtil.minimum(greyLevelCircularProfile, wrapAroundVoidStartBin, (greyLevelCircularProfile.length - 1));
        
        float secondDeltaThicknessValueInMillis = greyLevelCircularProfile[0];
        
        secondDeltaThicknessValueInMillis = StatisticsUtil.minimum(greyLevelCircularProfile, 0, (greyLevelCircularProfile.length - 1));
        
        final float maxDeltaThicknessValueInMillis = Math.min(firstDeltaThicknessValueInMillis, secondDeltaThicknessValueInMillis);
        candidateVoidZoneToLengthAndThicknessMap.put(new Pair<Integer, Integer>(wrapAroundVoidStartBin, wrapAroundVoidEndBin),
                                         new Pair<Float, Float>(lengthOfPotentialWrapAroundVoidInMillis, maxDeltaThicknessValueInMillis));
        isDetectedAsDefect = true;
      }
    }
    
    if(isDetectedAsDefect == false)
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      Pad pad = jointInspectionData.getPad();
      final float maxThickness = ArrayUtil.max(greyLevelCircularProfile);
      JointMeasurement voidGreyLevelMeasurement = new JointMeasurement(_algorithm,
                                                        MeasurementEnum.SHARED_VOIDING_DEFINITE_VOIDING_GREY_LEVEL,
                                                        MeasurementUnitsEnum.NONE,
                                                        pad,
                                                        sliceNameEnum,
                                                        maxThickness);
      jointInspectionResult.addMeasurement(voidGreyLevelMeasurement);
    }
    
    return candidateVoidZoneToLengthAndThicknessMap;
  }
   
 /*
  * @author Kee Chin Seong - new Circular Voding Algorithm
  */
  private static java.awt.Shape getShapeForVoidDefectInRingRegion(float[] ringDeltaThicknessProfile,
                                                            Pair<RegionOfInterest, RegionOfInterest> ringRegionPair,
                                                            int defectStartBin,
                                                            int defectEndBin)
  {
    Assert.expect(ringDeltaThicknessProfile != null);
    Assert.expect(ringRegionPair != null);
    Assert.expect((defectStartBin >= 0) && (defectStartBin < ringDeltaThicknessProfile.length));
    Assert.expect((defectEndBin >= 0) && (defectEndBin < ringDeltaThicknessProfile.length));

    RegionOfInterest outerRoi = ringRegionPair.getFirst();
    Assert.expect(outerRoi != null);
    RegionOfInterest innerRoi = ringRegionPair.getSecond();
    Assert.expect(innerRoi != null);

    double degreesPerBin = 360.0 / (double)ringDeltaThicknessProfile.length;
    double arcStartingAngle = defectStartBin * degreesPerBin;
    double arcAngleExtent = MathUtil.getDegreesWithin0To359((defectEndBin - defectStartBin) * degreesPerBin);

    // Create the outer arc shape.
    int outerRoiCenterX = outerRoi.getCenterX();
    int outerRoiCenterY = outerRoi.getCenterY();
    float outerRoiDiameter = (outerRoi.getHeight() + outerRoi.getWidth()) / 2f;
    float outerRoiRadius = outerRoiDiameter / 2f;
    Arc2D outerArc = new Arc2D.Float();
    outerArc.setArcByCenter(
      outerRoiCenterX,
      outerRoiCenterY,
      outerRoiRadius,
      arcStartingAngle,
      arcAngleExtent,
      Arc2D.OPEN);

    // Create the inner arc shape.
    int innerRoiCenterX = innerRoi.getCenterX();
    int innerRoiCenterY = innerRoi.getCenterY();
    float innerRoiDiameter = (innerRoi.getHeight() + innerRoi.getWidth()) / 2f;
    float innerRoiRadius = innerRoiDiameter / 2f;
    Arc2D innerArc = new Arc2D.Float();
    innerArc.setArcByCenter(
      innerRoiCenterX,
      innerRoiCenterY,
      innerRoiRadius,
      arcStartingAngle,
      arcAngleExtent,
      Arc2D.OPEN);

    // Create an aggregate shape that contains the outer and inner arcs and line segments that
    // connect them.
    GeneralPath voidDefectRegion = new GeneralPath(outerArc);
    voidDefectRegion.append(innerArc, false);
    Point2D outerArcStart = outerArc.getStartPoint();
    Point2D innerArcStart = innerArc.getStartPoint();
    voidDefectRegion.moveTo((float)outerArcStart.getX(), (float)outerArcStart.getY());
    voidDefectRegion.lineTo((float)innerArcStart.getX(), (float)innerArcStart.getY());
    Point2D outerArcEnd = outerArc.getEndPoint();
    Point2D innerArcEnd = innerArc.getEndPoint();
    voidDefectRegion.moveTo((float)outerArcEnd.getX(), (float)outerArcEnd.getY());
    voidDefectRegion.lineTo((float)innerArcEnd.getX(), (float)innerArcEnd.getY());

    return voidDefectRegion;
  }
  
 /*
  * @author Kee Chin Seong
 */
 private static void checkPotentialVoidZonesUsingLightestImage(Image image,
                                                               ReconstructionRegion inspectionRegion,
                                                               Subtype subtype,
                                                               JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum,
                                                               Map<Pair<Integer, Integer>, VoidDefectDispositionEnum> voidZoneToDispositionMap,
                                                               RegionOfInterest thresholdRegion) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(voidZoneToDispositionMap != null);
//
    Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair = null;
    try
    {
      ringRegionAndProfilePair  =  measureRingRegionBackgroundGrayLevelProfile(image,
                                                                               inspectionRegion,
                                                                               sliceNameEnum,
                                                                               jointInspectionData);
    }
    catch (Exception ex)
    {
       LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
              "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[] {inspectionRegion});
       AlgorithmUtil.raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);
          //jointPassed.setValue(false);
       return;
    }
    float graylevelProfile[] = ringRegionAndProfilePair.getSecond();

    Assert.expect(graylevelProfile != null, "Gray level profile is null!");

    final float greyLevelNominal= (float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_VOIDING_GREY_LEVEL_NOMINAL);
    final float greyLevelDiffFromNominal = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_VOIDING_DETECTION_SENSITIVITY);
    
//    // For each candidate void, make sure there is actually a void there by examining the lightest image.
    final float MINIMUM_GREY_LEVEL = greyLevelNominal + (greyLevelNominal * greyLevelDiffFromNominal / 100);
    
    for (Map.Entry<Pair<Integer, Integer>, VoidDefectDispositionEnum> mapEntry : voidZoneToDispositionMap.entrySet())
    {
      Pair<Integer, Integer> voidZone = mapEntry.getKey();
      int voidZoneStartBin = voidZone.getFirst();
      int voidZoneEndBin = voidZone.getSecond();

      // Does the void zone "wrap around"?
      if (voidZoneEndBin < voidZoneStartBin)
      {
        voidZoneEndBin += graylevelProfile.length;
      }

      // Count the number of dark bins in the profile.
      int numberOfBinsInVoidZone = (voidZoneEndBin - voidZoneStartBin) + 1;
      int numberOfDarkBinsInVoidZone = 0;
      for (int i = voidZoneStartBin; i <= voidZoneEndBin; ++i)
      {
        if (graylevelProfile[i % graylevelProfile.length] < MINIMUM_GREY_LEVEL)
        {
          ++numberOfDarkBinsInVoidZone;
        }
      }

      // Calculate the fraction of dark bins in the Void zone.
      float fractionOfDarkBinsInVoidZone = (float)numberOfDarkBinsInVoidZone / (float)numberOfBinsInVoidZone;

      final float MAX_ALLOWABLE_FRACTION_OF_DARK_BINS_IN_VOID_ZONE = 0.3f;
      if (MathUtil.fuzzyLessThan(fractionOfDarkBinsInVoidZone, MAX_ALLOWABLE_FRACTION_OF_DARK_BINS_IN_VOID_ZONE))
      {
        mapEntry.setValue(VoidDefectDispositionEnum.DEFINITE_VOID);   
      }
      else
      {
        mapEntry.setValue(VoidDefectDispositionEnum.EXONERATED_VOID);
      }
    }
  }

  
   /*
   * @author Kee Chin Seong - RF connector Voiding
   * 
   */
  protected static RegionOfInterest getReferenceRoi(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    RegionOfInterest referenceRoi = null;

    Subtype subtype = jointInspectionData.getSubtype();
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
    String referencePositionSetting =
        (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_VOIDING_REGION_REFERENCE_POSITION);
    if (referencePositionSetting.equals("Located Position"))
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        referenceRoi = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData);
      }
      else
      {
        referenceRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
      }
    }
    else if (referencePositionSetting.equals("CAD Position"))
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        referenceRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest();
      }
      else
      {
        referenceRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels();
      }
    }
    else
    {
      Assert.expect(false, "Unexpected reference position setting: " + referencePositionSetting);
    }

    Assert.expect(referenceRoi != null);
    return referenceRoi;
  }
  
  /*
   * @Author Kee Chin Seong
   */
  private static Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> measureRingRegionBackgroundGrayLevelProfile(
      Image image,
      ReconstructionRegion inspectionRegion,
      SliceNameEnum sliceNameEnum,
      JointInspectionData jointInspectionData) throws JointCannotBeInspectedBusinessException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the IPD.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels(MagnificationEnum.getCurrentNorminal());

    // Get the reference ROI.
    RegionOfInterest referenceRoi = getReferenceRoi(jointInspectionData);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.SHARED_VOIDING_INNER_EDGE_POSITION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.SHARED_VOIDING_OUTER_EDGE_POSITION) / 100.f;

    if(regionOuterEdgeAsFractionOfInterPadDistance <= regionInnerEdgeAsFractionOfInterPadDistance)
    {
      throw new JointCannotBeInspectedBusinessException(new LocalizedString("ALGDIAG_INNER_AND_OUTER_REGION_ARE_SAME_WARNING_KEY", null).toString());
    }

    int profileInnerEdgeOffset = (int)Math.ceil(interPadDistanceInPixels * regionInnerEdgeAsFractionOfInterPadDistance);
    int profileOuterEdgeOffset = (int)Math.ceil(interPadDistanceInPixels * regionOuterEdgeAsFractionOfInterPadDistance);
    int locatedJointMinX = referenceRoi.getMinX();
    int locatedJointMinY = referenceRoi.getMinY();
    int locatedJointWidth = referenceRoi.getWidth();
    int locatedJointHeight = referenceRoi.getHeight();
    int locatedJointOrientation = referenceRoi.getOrientationInDegrees();

    // Make sure that there's at least 2 pixels between the inner and outer ROIs.
    if ( profileOuterEdgeOffset == profileInnerEdgeOffset || 
        (profileOuterEdgeOffset-profileInnerEdgeOffset) < 4)
    {
      profileOuterEdgeOffset+=2;
      profileInnerEdgeOffset-=2;
      if (profileInnerEdgeOffset < 0)
      {
        profileOuterEdgeOffset+=2;
        profileInnerEdgeOffset=0;
      }
    }
    
    // Create the outer ring ROI.
    RegionOfInterest outerRoi = new RegionOfInterest(
        locatedJointMinX - profileOuterEdgeOffset,
        locatedJointMinY - profileOuterEdgeOffset,
        locatedJointWidth + (profileOuterEdgeOffset * 2),
        locatedJointHeight + (profileOuterEdgeOffset * 2),
        locatedJointOrientation,
        RegionShapeEnum.OBROUND);

    if (AlgorithmUtil.regionCanFitInImage(outerRoi, image) == false)
    {
      throw new JointCannotBeInspectedBusinessException("");
    }

    // Make sure the outer ring fits within the image.
    if (AlgorithmUtil.checkRegionBoundaries(outerRoi, image, inspectionRegion, "Void Background Outer Ring ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, outerRoi);
    }

    // Create the inner ring ROI.
    RegionOfInterest innerRoi = new RegionOfInterest(
        locatedJointMinX - profileInnerEdgeOffset,
        locatedJointMinY - profileInnerEdgeOffset,
        locatedJointWidth + (profileInnerEdgeOffset * 2),
        locatedJointHeight + (profileInnerEdgeOffset * 2),
        locatedJointOrientation,
        RegionShapeEnum.OBROUND);
    // Make sure the inner ring fits within the image.
    if (AlgorithmUtil.checkRegionBoundaries(innerRoi, image, inspectionRegion, "Void Background Inner Ring ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, innerRoi);

      // Make sure the inner ring is as least one pixel smaller than the outer.
      innerRoi.setWidthKeepingSameCenter(Math.min(innerRoi.getWidth(), (outerRoi.getWidth() - 1)));
      innerRoi.setHeightKeepingSameCenter(Math.min(innerRoi.getHeight(), (outerRoi.getHeight() - 1)));
    }

    // Measure the ring region gray level profile.
    float[] ringProfile = ImageFeatureExtraction.ringProfile(image, outerRoi, innerRoi);

    Pair<RegionOfInterest, RegionOfInterest> ringRegionPair =
      new Pair<RegionOfInterest, RegionOfInterest>(outerRoi, innerRoi);
    Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair =
      new Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]>(ringRegionPair, ringProfile);

    return ringRegionAndProfilePair;
  }
  
   /*
   * @author Kee Chin Seong - new Circular Voding Algorithm
  */
  public static void classifyJointsCircularVoid(ReconstructedImages reconstructedImages,
                                         List<JointInspectionData> jointInspectionDataObjects,
                                         SliceNameEnum sliceNameEnum) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice slice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    for(JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
        //reinitialize the boolean 
        BooleanRef jointPassed = new BooleanRef(true);
        
        Subtype subtype = jointInspectionData.getSubtype();

        // started the diagnostic 
        AlgorithmUtil.postStartOfJointDiagnostics(_algorithm, jointInspectionData, reconstructionRegion, slice, false);
        AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, _algorithm);
        // Measure the smoothed background thickness profile.
        Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair;
        try
        {
          ringRegionAndProfilePair = measureRingRegionBackgroundGrayLevelProfile(reconstructedImages.getReconstructedSliceIgnoreNullImages(sliceNameEnum).getImage(),
                                                                                 reconstructionRegion,
                                                                                 sliceNameEnum,
                                                                                 jointInspectionData);
        }
        catch (JointCannotBeInspectedBusinessException jEx)
        {
          if(jEx.getMessage().isEmpty())
          {
            LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
                "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[] {reconstructionRegion});
            AlgorithmUtil.raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);
          }
          else
          {
            AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_INNER_AND_OUTER_REGION_ARE_SAME_WARNING_KEY", null));  
          }
          jointPassed.setValue(false);
          
          return;
        }

        Pair<RegionOfInterest, RegionOfInterest> ringRegionPair = ringRegionAndProfilePair.getFirst();
        float[] ringProfile = ringRegionAndProfilePair.getSecond();
        ringRegionAndProfilePair = null;
        
        // Mapping of all potential void defect zones to defect disposition.
        Map<Pair<Integer, Integer>, VoidDefectDispositionEnum> voidZoneToDispositionMap =
                           new HashMap<Pair<Integer, Integer>, VoidDefectDispositionEnum>();
        
        // Create the "ring region" shape.
        RegionOfInterest outerRoi = ringRegionPair.getFirst();
        RegionOfInterest innerRoi = ringRegionPair.getSecond();
        GeneralPath ringVoidRegion = new GeneralPath(outerRoi.getShape());
        ringVoidRegion.append(innerRoi.getShape(), false);

        // Mark the start bin for the ring profile.  This is always at the zero degree location and the
        // profile goes counter-clockwise.
        Line2D profileStartLineSegment = new Line2D.Float(innerRoi.getMaxX(),
                                                          innerRoi.getCenterY(),
                                                          outerRoi.getMaxX(),
                                                          outerRoi.getCenterY());
        MeasurementRegionDiagnosticInfo profileStartLineSegmentDiagInfo =
            new MeasurementRegionDiagnosticInfo(profileStartLineSegment, MeasurementRegionEnum.VOID_PROFILE_START_BIN_REGION);

        // Keep track of an aggregate shape for all of the voiding defect regions.
        GeneralPath definiteVoidDefectsAggregateShape = new GeneralPath();
        GeneralPath questionableVoidDefectsAggregateShape = new GeneralPath();
        GeneralPath exoneratedVoidDefectsAggregateShape = new GeneralPath();

        final float greyLevelThresHold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_VOIDING_GREY_LEVEL_NOMINAL);
        
        // Check the profile for potential Voiding.   
        Map<Pair<Integer, Integer>, Pair<Float, Float>> candidateVoidZonesToLengthAndThicknessMap =
            CircularVoidingAlgorithm.analyzeDeltaGreyLevelProfileForVoid(reconstructedImages.getReconstructedSliceIgnoreNullImages(sliceNameEnum).getImage(),
                                                  reconstructionRegion,
                                                  jointInspectionData,
                                                  sliceNameEnum,
                                                  ringProfile,
                                                  greyLevelThresHold); 

        // Based on our intiial list of potential voiding defects, build up a map of the voiding range to its defect
        // disposition.  The defect disposition will start out as QUESTIONABLE_VOID.
        // Also, if diags are on, keep a map of all the voiding region shapes.  We need _algorithm later to mark the indicted zones.
        Map<Pair<Integer, Integer>, java.awt.Shape> voidZoneToShapeMap = new HashMap<Pair<Integer, Integer>, java.awt.Shape>();

        for (Pair<Integer, Integer> candidateVoidZone : candidateVoidZonesToLengthAndThicknessMap.keySet())
        {
          int candidateVoidStartBin = candidateVoidZone.getFirst();
          int candidateVoidEndBin = candidateVoidZone.getSecond();
          
          final Pair<Float, Float> voidDataInMillis = candidateVoidZonesToLengthAndThicknessMap.get(candidateVoidZone); 
          float voidDefectInMillis = voidDataInMillis.getSecond();
          
          voidZoneToDispositionMap.put(candidateVoidZone, VoidDefectDispositionEnum.QUESTIONABLE_VOID);
          
          float voidingDifferenceFromNominalPercent = Math.abs(voidDefectInMillis - greyLevelThresHold) / greyLevelThresHold * 100;
            
          JointMeasurement voidDifferenceFromNominalMeasurement = new JointMeasurement(_algorithm,
                                                                                         MeasurementEnum.SHARED_VOIDING_DIFFERENCE_FROM_NOMINAL_PERCENTAGE,
                                                                                         MeasurementUnitsEnum.VOID_DIFFERENCE_FROM_NOMINAL_GREY_LEVEL,
                                                                                         jointInspectionData.getPad(), sliceNameEnum, voidingDifferenceFromNominalPercent);
            
          jointInspectionData.getJointInspectionResult().addMeasurement(voidDifferenceFromNominalMeasurement);

          if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), _algorithm))
          {
            java.awt.Shape candidateVoidShape = CircularVoidingAlgorithm.getShapeForVoidDefectInRingRegion(ringProfile,
                                                                                    ringRegionPair,
                                                                                    candidateVoidStartBin,
                                                                                    candidateVoidEndBin);
            voidZoneToShapeMap.put(candidateVoidZone, candidateVoidShape);
          }
        }
        
        
        //Reconfirm the potential void by using the sensitivity to adjust the nominal
        checkPotentialVoidZonesUsingLightestImage(reconstructedImages.getReconstructedSliceIgnoreNullImages(sliceNameEnum).getImage(),
                                                  reconstructionRegion,
                                                  subtype,
                                                  jointInspectionData,
                                                  sliceNameEnum,
                                                  voidZoneToDispositionMap,
                                                  getReferenceRoi(jointInspectionData));

        // Iterate thru our map and find any entries that are real void and indict them.
        // Also, label all questionable and exonerated void so we can show them in the diagnostics.
       for (Map.Entry<Pair<Integer, Integer>, VoidDefectDispositionEnum> mapEntry : voidZoneToDispositionMap.entrySet())
       {
          Pair<Integer, Integer> candidateVoidZone = mapEntry.getKey();
          VoidDefectDispositionEnum voidDefectDisposition = mapEntry.getValue();

          final Pair<Float, Float> voidDataInMillis = candidateVoidZonesToLengthAndThicknessMap.get(candidateVoidZone); 
          float voidDefectInMillis = voidDataInMillis.getSecond();
          
          if (voidDefectDisposition.equals(VoidDefectDispositionEnum.DEFINITE_VOID))
          {
            // Indict the void - instead of void length, we also display the void thickness here.
            JointInspectionResult jointResult = jointInspectionData.getJointInspectionResult();
            JointMeasurement voidMeasurement = new JointMeasurement(_algorithm,
              MeasurementEnum.SHARED_VOIDING_DEFINITE_VOIDING_GREY_LEVEL,
              MeasurementUnitsEnum.NONE,
              jointInspectionData.getPad(), sliceNameEnum, Float.parseFloat(new DecimalFormat("###.##").format(voidDefectInMillis)));

            jointResult.addMeasurement(voidMeasurement);
            
            JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
            _algorithm, sliceNameEnum);
            voidPercentIndictment.addFailingMeasurement(voidMeasurement);
            jointResult.addIndictment(voidPercentIndictment);

            if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), _algorithm))
            {
              // Add the void region shape to our aggregate definite void defects shape.
              java.awt.Shape voidDefectShape = voidZoneToShapeMap.get(candidateVoidZone);
              Assert.expect(voidDefectShape != null);
              definiteVoidDefectsAggregateShape.append(voidDefectShape, false);
              
              AlgorithmUtil.postMeasurementTextualDiagnostics(_algorithm, reconstructionRegion, jointInspectionData, voidMeasurement);
              //AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(_algorithm, jointInspectionData, reconstructionRegion, sliceNameEnum, false);
            }

            jointPassed.setValue(false);
          }
          else if (voidDefectDisposition.equals(VoidDefectDispositionEnum.QUESTIONABLE_VOID))
          {
            if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), _algorithm))
            {
              // Add the void region shape to our aggregate questionable void defects shape.
              java.awt.Shape voidDefectShape = voidZoneToShapeMap.get(candidateVoidZone);
              Assert.expect(voidDefectShape != null);
              questionableVoidDefectsAggregateShape.append(voidDefectShape, false);
            }
          }
          else if (voidDefectDisposition.equals(VoidDefectDispositionEnum.EXONERATED_VOID))
          {
            JointInspectionResult jointResult = jointInspectionData.getJointInspectionResult();
            JointMeasurement voidMeasurement = new JointMeasurement(_algorithm,
                                                                    MeasurementEnum.SHARED_VOIDING_EXONERATED_VOIDING_GREY_LEVEL,
                                                                    MeasurementUnitsEnum.EXONERATED_VOIDING_GREY_LEVEL_VOIDED,
                                                                    jointInspectionData.getPad(), sliceNameEnum, voidDefectInMillis);

            jointResult.addMeasurement(voidMeasurement);
            if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), _algorithm))
            {
              // Add the voiding region shape to our aggregate exonerated voiding defects shape.
              java.awt.Shape voidDefectShape = voidZoneToShapeMap.get(candidateVoidZone);
              Assert.expect(voidDefectShape != null);
              exoneratedVoidDefectsAggregateShape.append(voidDefectShape, false);
            }
          }
          else
          {
            // Shouldn't get here!
            Assert.expect(false, "Unexpected voiding defect disposition : " + voidDefectDisposition);
          }
        }
        
        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), _algorithm))
        {
          //AlgorithmUtil.postMeasurementTextualDiagnostics(_algorithm, reconstructionRegion, jointInspectionData, voidPercentMeasurement);
  
          MeasurementRegionDiagnosticInfo ringVoidRegionDiagInfo                = new MeasurementRegionDiagnosticInfo(ringVoidRegion,
                                                                                  MeasurementRegionEnum.VOID_BACKGROUND_REGION);
          MeasurementRegionDiagnosticInfo definiteVoidDefectRegionDiagInfo      = new MeasurementRegionDiagnosticInfo(definiteVoidDefectsAggregateShape,
                                                                                  MeasurementRegionEnum.DEFINITE_VOID_DEFECT_REGION);
          
          MeasurementRegionDiagnosticInfo questionableVoidDefectRegionDiagInfo  = new MeasurementRegionDiagnosticInfo(questionableVoidDefectsAggregateShape,
                                                                                  MeasurementRegionEnum.QUESTIONABLE_VOID_DEFECT_REGION);
          MeasurementRegionDiagnosticInfo exoneratedVoidDefectRegionDiagInfo    = new MeasurementRegionDiagnosticInfo(exoneratedVoidDefectsAggregateShape,
                                                                                  MeasurementRegionEnum.EXONERATED_VOID_DEFECT_REGION);
          
          ProfileDiagnosticInfo ringVoidProfileDiagInfo = CircularVoidingAlgorithm.createVoidingProfileDiagnosticInfo(jointInspectionData,
                                                                                                     sliceNameEnum,
                                                                                                     ringProfile,
                                                                                                     voidZoneToDispositionMap);

          AlgorithmDiagnostics.getInstance().postDiagnostics(reconstructionRegion,
                                                             sliceNameEnum,
                                                             jointInspectionData,
                                                             _algorithm,
                                                             false,
                                                             ringVoidRegionDiagInfo,
                                                             profileStartLineSegmentDiagInfo,
                                                             definiteVoidDefectRegionDiagInfo,
                                                             questionableVoidDefectRegionDiagInfo,
                                                             //exoneratedVoidDefectRegionDiagInfo,
                                                             ringVoidProfileDiagInfo);
          
          AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(_algorithm, jointInspectionData, reconstructionRegion, sliceNameEnum, jointPassed.getValue());
        }
    }
  }
  
  /*
   * @author Kee Chin Seong - Learning algorithm settings for the circle profile
   */
  public static void learnAlgorithmSettings(Subtype subtype,
                                            ManagedOfflineImageSet typicalBoardImages,
                                            ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    ManagedOfflineImageSetIterator reconstructedImagesIterator = typicalBoardImages.iterator();
    ReconstructedImages reconstructedImages;
    List righProfileArray = new ArrayList();
    
    while ((reconstructedImages = reconstructedImagesIterator.getNext()) != null)
    {
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair;
        try
        {
          ringRegionAndProfilePair = measureRingRegionBackgroundGrayLevelProfile(reconstructedImages.getReconstructedSliceIgnoreNullImages(SliceNameEnum.PAD).getImage(),
                                                                                 inspectionRegion,
                                                                                 SliceNameEnum.PAD,
                                                                                 jointInspectionData);
        }
        catch (Exception ex)
        {
          LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
              "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[] {inspectionRegion});
          AlgorithmUtil.raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);
          return;
        }

        float[] ringProfile = ringRegionAndProfilePair.getSecond();
        righProfileArray.addAll(Arrays.asList(ringProfile));
      }
      reconstructedImagesIterator.finishedWithCurrentRegion();
    }
    
    //Calculate proper right size
    int ringProfileArrayLength = 0;
    for(Object p : righProfileArray)
    {
      float[] a = (float[])p;

      for(int i = 0; i < a.length; i ++)
      {
          ringProfileArrayLength ++;
      } 
    }
    //Combining the arrays into 1 to get the mdedian
    float[] tempRingProfile = (float[])righProfileArray.get(0);
    float[] overAllRingProfile = new float[tempRingProfile.length * ringProfileArrayLength];
    
    int startingArrayPoint = 0;
    for(Object p : righProfileArray)
    {
      float[] a = (float[])p;

      for(int i = 0; i < a.length; i ++)
      {
        overAllRingProfile[i + startingArrayPoint] = a[i];
      }
      
      startingArrayPoint = startingArrayPoint + a.length - 1;
    }
    subtype.setLearnedValue(subtype.getJointTypeEnum(), _algorithm, AlgorithmSettingEnum.SHARED_VOIDING_GREY_LEVEL_NOMINAL, StatisticsUtil.median(overAllRingProfile));
  }
}
