package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.panelDesc.JointTypeEnum;

/**
 * Implementation of the Short algorithm for rectangular short regions.
 *
 * @author Patrick Lacz
 * @author Matt Wharton
 */
public class RectangularShortAlgorithm extends SharedShortAlgorithm
{
  private static String _ON = "On";
  private static String _OFF = "Off";
  private static ArrayList<JointTypeEnum> _JOINT_TYPES_ALLOWING_HIGH_SHORT_DETECTION = new ArrayList<JointTypeEnum>(
    Arrays.asList(JointTypeEnum.GULLWING));

  private static String _TRUE = "True";
  private static String _FALSE = "False";
  
  //Lim, Lay Ngor - XCR1743:Benchmark - START
  private static int _NUMBER_OF_PIXEL_PER_SEGMENT = 15;
  //Set _SOLDER_BALL_INSPECTION_TIME_STAMP to true to record the solder ball inspection time stamp if needed
  public static final boolean _SOLDER_BALL_INSPECTION_TIME_STAMP = false;
  TimerUtil _solderBallInspectionTime;
  //Lim, Lay Ngor - XCR1743:Benchmark - END

  /**
   * @author Patrick Lacz
   */
  public RectangularShortAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(inspectionFamilyEnum);
    
    //Lim, Lay Ngor - XCR1743:Benchmark 
    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
      _solderBallInspectionTime = new TimerUtil(); 
      _solderBallInspectionTime.reset();
    }
  }

  /**
   * Initializes the AlgorithmSettings for this Algorithm.
   *
   * @author Matt Wharton
   * @author Cheah, Lee Herng
   */
  protected void addAlgorithmSettings()
  {
    final int algorithmVersion = 1;

    // PE:  this high displayOrder number is a hack to get the Short slice-related settings to appear in our desired
    // order in the Slice Setup tab.  We want the Short slice-related settings to come at the end.
    int displayOrder = 1000;

    //Siew Yeng - XCR-3318 - Oval PTH
    Collection<AlgorithmSetting> throughHoleSharedShortAlgorithmSettings = createThroughHoleSharedShortAlgorithmSettings(displayOrder, algorithmVersion);    
    displayOrder += throughHoleSharedShortAlgorithmSettings.size();
    
    // Global minimum thickness setting.
    AlgorithmSetting globalMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(GLOBAL_MIN_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(GLOBAL_MIN_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(GLOBAL_MIN_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    addAlgorithmSetting(globalMinimumThicknessSetting);
    
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
    // Global minimum length setting.
    AlgorithmSetting globalMinimumLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH,
        displayOrder++,
        _MINIMUM_SHORT_LENGTH_DEFAULT_VALUE_IN_MILLIS, //previously is fix to 3 pixels, fix to 3 pixels to support old recipe so that same result will retain.
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),//please ensure this is large enough...
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(GLOBAL_MIN_LENGTH)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(GLOBAL_MIN_LENGTH)_KEY",
        "IMG_DESC_SHARED_SHORT_(GLOBAL_MIN_LENGTH)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    addAlgorithmSetting(globalMinimumLengthSetting);    

    // Region inner edge distance.
    AlgorithmSetting innerEdgeLocationSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION,
        displayOrder++,
        15.f,
        0f,
        100f,
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_SHARED_SHORT_(REGION_INNER_EDGE)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(REGION_INNER_EDGE)_KEY",
        "IMG_DESC_SHARED_SHORT_(REGION_INNER_EDGE)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    addAlgorithmSetting(innerEdgeLocationSetting);

    // Region outer edge distance.
    AlgorithmSetting outerEdgeLocationSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION,
        displayOrder++,
        75.f,
        0f,
        //100f,
        150f,
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_SHARED_SHORT_(REGION_OUTER_EDGE)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(REGION_OUTER_EDGE)_KEY",
        "IMG_DESC_SHARED_SHORT_(REGION_OUTER_EDGE)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    addAlgorithmSetting(outerEdgeLocationSetting);

    // Region reference position (CAD or Located position).
    ArrayList<String> allowableRegionReferencePositionValues = new ArrayList<String>(Arrays.asList("Located Position",
                                                                                                   "CAD Position"));
    AlgorithmSetting regionReferencePositionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_REFERENCE_POSITION,
        displayOrder++,
        "Located Position",
        allowableRegionReferencePositionValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(REGION_REFERENCE_POSITION)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(REGION_REFERENCE_POSITION)_KEY",
        "IMG_DESC_SHARED_SHORT_(REGION_REFERENCE_POSITION)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(regionReferencePositionSetting);

    // Detect high shorts (Gullwing only).
    ArrayList<String> highShortSliceSettings = new ArrayList<String>(Arrays.asList(_OFF, _ON));
    AlgorithmSetting highShortDetectionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_DETECTION,
        displayOrder++,
        _OFF,
        highShortSliceSettings,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(HIGH_SHORT_DETECTION)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(HIGH_SHORT_DETECTION)_KEY",
        "IMG_DESC_SHARED_SHORT_(HIGH_SHORT_DETECTION)_KEY",
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        algorithmVersion);
    // we add this setting below

    // Detect high shorts (Gullwing only).
    AlgorithmSetting highShortSliceheightSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(10.0f), // default value
        MathUtil.convertMilsToMillimeters(-10.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(HIGH_SHORT_SLICEHEIGHT)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(HIGH_SHORT_SLICEHEIGHT)_KEY",
        "IMG_DESC_SHARED_SHORT_(HIGH_SHORT_SLICEHEIGHT)_KEY",
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        algorithmVersion);
    // we add this setting below

    // filter the High Short settings appropriately and then add them
    List<JointTypeEnum> jointTypeList = JointTypeEnum.getAllJointTypeEnums();
    for (JointTypeEnum jointTypeEnum : jointTypeList)
    {
      if (allowHighShortDetection(jointTypeEnum) == false)
      {
        highShortDetectionSetting.filter(jointTypeEnum);
        highShortSliceheightSetting.filter(jointTypeEnum);
      }
    }
    addAlgorithmSetting(highShortDetectionSetting);
    addAlgorithmSetting(highShortSliceheightSetting);

    // Enable Test Region Head.
    ArrayList<String> enableTestRegionHeadValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableTestRegionHeadSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_HEAD,
        displayOrder++,
        "True",
        enableTestRegionHeadValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_HEAD)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_HEAD)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_HEAD)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(enableTestRegionHeadSetting);

    // Enable Test Region Right.
    ArrayList<String> enableTestRegionRightValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableTestRegionRightSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_RIGHT,
        displayOrder++,
        "True",
        enableTestRegionRightValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_RIGHT)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_RIGHT)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_RIGHT)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(enableTestRegionRightSetting);

    // Enable Test Region Tail.
    ArrayList<String> enableTestRegionTailValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableTestRegionTailSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_TAIL,
        displayOrder++,
        "True",
        enableTestRegionTailValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_TAIL)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_TAIL)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_TAIL)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(enableTestRegionTailSetting);

    // Enable Test Region Left.
    ArrayList<String> enableTestRegionLeftValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableTestRegionLeftSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_LEFT,
        displayOrder++,
        "True",
        enableTestRegionLeftValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_LEFT)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_LEFT)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_LEFT)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(enableTestRegionLeftSetting);
    
    //Siew Yeng - XCR-3318 - Enable Test Region 4 Corners(For Oval PTH).
    ArrayList<String> enableTestRegion4CornersValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableTestRegion4CornersSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS,
        displayOrder++,
        _TRUE,
        enableTestRegion4CornersValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_4_CORNERS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_4_CORNERS)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_4_CORNERS)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(enableTestRegion4CornersSetting);
    
    //Siew Yeng - Enable Test Region Inner Corner(For Oval PTH).
    ArrayList<String> enableTestRegionInnerCornerValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableTestRegionInnerCornerSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER,
        displayOrder++,
        _FALSE,
        enableTestRegionInnerCornerValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_INNER_CORNER)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_INNER_CORNER)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_INNER_CORNER)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(enableTestRegionInnerCornerSetting);

    // High Short minimum thickness setting.
    AlgorithmSetting highShortMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    addAlgorithmSetting(highShortMinimumThicknessSetting);

    // Minimum pixels of Short ROI Region.
    AlgorithmSetting minimumPixelsShortROIRegionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION,
        displayOrder++,
        1,
        1,
        10,
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_SHARED_SHORT_(MINIMUM_PIXELS_SHORT_ROI_REGION)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(MINIMUM_PIXELS_SHORT_ROI_REGION)_KEY",
        "IMG_DESC_SHARED_SHORT_(MINIMUM_PIXELS_SHORT_ROI_REGION)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    addAlgorithmSetting(minimumPixelsShortROIRegionSetting);

    //Lim, Lay Ngor - XCR1743:Benchmark - START
    // Enable Test Region Center(Clear Chip Only).
    ArrayList<String> enableTestRegionCenterValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableTestRegionCenterSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_CENTER,
        displayOrder++,
        "False", 
        enableTestRegionCenterValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_CENTER)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_CENTER)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_TEST_REGION_CENTER)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    // we add this setting below    
    
    // Enable Multiple ROI Center Region(Clear Chip Only).
    ArrayList<String> enableMultiROICenterRegionValues = new ArrayList<String>(Arrays.asList(_TRUE,
                                                                                       _FALSE));
    AlgorithmSetting enableMultiROICenterRegionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_ENABLE_MULTI_ROI_CENTER_REGION,
        displayOrder++,
        "False", 
        enableMultiROICenterRegionValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(ENABLE_MULTI_ROI_CENTER_REGION)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_MULTI_ROI_CENTER_REGION)_KEY",
        "IMG_DESC_SHARED_SHORT_(ENABLE_MULTI_ROI_CENTER_REGION)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    // we add this setting below        
    
    // Minimum center solder ball thickness setting between two pads.(Clear Chip Only)
    AlgorithmSetting minimumCenterSolderBallThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(0.6f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(MINIMUM_CENTER_SOLDER_BALL_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(MINIMUM_CENTER_SOLDER_BALL_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(MINIMUM_CENTER_SOLDER_BALL_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    // we add this setting below    
    
    // Center Effective Height
    AlgorithmSetting centerEffectiveLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_LENGTH,
        displayOrder++,
        100.f,
        1f,
        300f,
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_SHARED_SHORT_(CENTER_EFFECTIVE_LENGTH)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(CENTER_EFFECTIVE_LENGTH)_KEY",
        "IMG_DESC_SHARED_SHORT_(CENTER_EFFECTIVE_LENGTH)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    // we add this setting below    
    
    // Center Effective Width(Clear Chip Only)
    AlgorithmSetting centerEffectiveWidthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_WIDTH,
        displayOrder++,
        100.f,
        1f,
        200f,
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_SHARED_SHORT_(CENTER_EFFECTIVE_WIDTH)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(CENTER_EFFECTIVE_WIDTH)_KEY",
        "IMG_DESC_SHARED_SHORT_(CENTER_EFFECTIVE_WIDTH)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    
    //Siew Yeng - XCR-3318 - Oval PTH
    Collection<AlgorithmSetting> brokenPinAlgorithmSettings = BrokenPinAlgorithm.createBrokenPinAlgorithmSettings(displayOrder, algorithmVersion);    
    displayOrder += brokenPinAlgorithmSettings.size();   
    
    // we add this setting below
    
    // filter the Center Solder Ball Short settings appropriately and then add them
    //if can pls merge with high short above to reduce coding
    List<JointTypeEnum> jointTypeList2 = JointTypeEnum.getAllJointTypeEnums();
    for (JointTypeEnum jointTypeEnum : jointTypeList2)
    {
      if (allowCenterSolderBallShortDetection(jointTypeEnum) == false)
      {
        enableTestRegionCenterSetting.filter(jointTypeEnum);
        enableMultiROICenterRegionSetting.filter(jointTypeEnum);
        minimumCenterSolderBallThicknessSetting.filter(jointTypeEnum);
        centerEffectiveLengthSetting.filter(jointTypeEnum);
        centerEffectiveWidthSetting.filter(jointTypeEnum);
      }
      
      //Siew Yeng - XCR-3318 - Oval PTH
      if(jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE) == false)
      {
        for (AlgorithmSetting algoSetting : throughHoleSharedShortAlgorithmSettings)
        {            
          algoSetting.filter(jointTypeEnum);
        }    
        
        for (AlgorithmSetting algoSetting : brokenPinAlgorithmSettings)
        {            
          algoSetting.filter(jointTypeEnum);
        }
        
        enableTestRegion4CornersSetting.filter(jointTypeEnum);
        enableTestRegionInnerCornerSetting.filter(jointTypeEnum);
      }
    }
    for (AlgorithmSetting algoSetting : throughHoleSharedShortAlgorithmSettings)
    {            
      addAlgorithmSetting(algoSetting);
    }   
    
    for (AlgorithmSetting algoSetting : brokenPinAlgorithmSettings)
    {            
      addAlgorithmSetting(algoSetting);
    } 
    
    addAlgorithmSetting(enableTestRegionCenterSetting);   
    addAlgorithmSetting(enableMultiROICenterRegionSetting);
    addAlgorithmSetting(minimumCenterSolderBallThicknessSetting); 
    addAlgorithmSetting(centerEffectiveLengthSetting);
    addAlgorithmSetting(centerEffectiveWidthSetting);
    //Siew Yeng - Oval PTH
    addAlgorithmSetting(enableTestRegion4CornersSetting);
    addAlgorithmSetting(enableTestRegionInnerCornerSetting);
	//Lim, Lay Ngor - XCR1743:Benchmark - END
  }

  /**
   * @author Matt Wharton
   * @author Lim, Seng Yew
   */
  protected float getMinimumShortThicknessInMMThreshold(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    float minimumShortThicknessInMM = 0f;

    //Siew Yeng - XCR-3318 - Oval PTH (Copy from CircularShortAlgorithm)
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS);
      }
      else if (sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_PIN_SIDE))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS);
      }
      else if (sliceNameEnum.equals(SliceNameEnum.HIGH_SHORT))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS);
      }
      else if (sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_PROTRUSION))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS);

      }
      //Broken Pin
      else if (sliceNameEnum.equals(SliceNameEnum.CAMERA_0))
      {
         minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS);
      }
      else
      {
        Assert.expect(false, "Illegal slice for Oval PTH Short!");
      }
    }
    else
    {
      if ( sliceNameEnum.equals(SliceNameEnum.HIGH_SHORT) )
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS);
      else
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS);
    }
    return minimumShortThicknessInMM;
  }

  /**
   * @author Matt Wharton
   */
  protected Collection<SliceNameEnum> getSlicesToInspect(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Collection<SliceNameEnum> slicesToInspect =
        new LinkedList<SliceNameEnum>(getInspectionFamily().getShortInspectionSlices(subtype));

    // Depending on the joint type, we need to further filter out slices based on an algorithm setting.
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    //Siew Yeng XCR-3318 - Oval PTH (Copy from CircularShortAlgorithm)
    if (jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE)) 
    {
      String inspectionSlice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE);
      if (inspectionSlice.equals(_COMPONENT_SLICE))
      {
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
        slicesToInspect.remove(SliceNameEnum.CAMERA_0);//Broken Pin
      }
      else if (inspectionSlice.equals(_PIN_SLICE))
      {
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
        slicesToInspect.remove(SliceNameEnum.CAMERA_0);//Broken Pin
      }
      else if (inspectionSlice.equals(_BOTH_SLICES))
      {
        slicesToInspect.remove(SliceNameEnum.CAMERA_0);//Broken Pin
        // leave both slices in there
      }
      //Broken Pin
      else if (inspectionSlice.equals(_PROJECTION_SLICE_ONLY))
      {
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
      }
      else if (inspectionSlice.equals(_PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES) || inspectionSlice.equals("All"))
      {
          // leave all slices in there
      }
      else
      {
        Assert.expect(false);  // unexpected slice
      }
    }
    
    if (allowHighShortDetection(jointTypeEnum))
    {
      String highShortDetectionSetting = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_DETECTION);
      //Siew Yeng - XCR-3318 - Oval PTH (Copy from CircularShortAlgorithm)
      if (jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE))
        highShortDetectionSetting = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION);
      
      Assert.expect(slicesToInspect.contains(SliceNameEnum.HIGH_SHORT)^
                    slicesToInspect.contains(SliceNameEnum.THROUGHHOLE_PROTRUSION));
      if (highShortDetectionSetting.equals(_ON))
      {
        // do nothing, leave the high short slice in there
      }
      else if (highShortDetectionSetting.equals(_OFF))
      {
        slicesToInspect.remove(SliceNameEnum.HIGH_SHORT);
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_PROTRUSION);
      }
      else
      {
        Assert.expect(false); // unexpected slice
      }
    }
    else
    {
      // make sure nobody is using this slice when they're not supposed to
      Assert.expect(slicesToInspect.contains(SliceNameEnum.HIGH_SHORT) == false);
      Assert.expect(slicesToInspect.contains(SliceNameEnum.THROUGHHOLE_PROTRUSION) == false);
    }

    return slicesToInspect;
  }

  /**
   * @author Lim, Lay Ngor - Broken Pin
   */
  protected void detectBrokenPinBaseOnShape(ReconstructedImages reconstructedImages,
                                  SliceNameEnum sliceNameEnum,
                                  List<JointInspectionData> jointInspectionDataObjects,
//                                  Algorithm algorithm,
                                  BooleanRef slicePassed) throws XrayTesterException
  {
    slicePassed.setValue(true);//not supported yet
  }

  /**
   * @author Matt Wharton
   * @author Seng-Yew Lim - Added disabled short profile region handling and display.
   * @author Lim, Lay Ngor - Added solder ball detection between two pads
   */
  protected void detectShortsOnJointBasedOnShape(Image image,
                                                 ReconstructionRegion inspectionRegion,
                                                 Subtype subtype,
                                                 JointInspectionData jointInspectionData,
                                                 SliceNameEnum sliceNameEnum,
                                                 BooleanRef jointPassed,
                                                 final float MILIMETER_PER_PIXEL) throws XrayTesterException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointPassed != null);

    // Get the minimum short thickness threshold.
    final float minimumShortThicknessInMillis = getMinimumShortThicknessInMMThreshold(subtype, sliceNameEnum);
    final float minimumShortLengthInMillis = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH);
    final String enableTestRegionHeadSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_HEAD);      
    final String enableTestRegionRightSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_RIGHT);      
    final String enableTestRegionTailSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_TAIL);      
    final String enableTestRegionLeftSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_LEFT);
    final boolean enableTestRegionHead = enableTestRegionHeadSetting.equals(_TRUE);     
    final boolean enableTestRegionRight = enableTestRegionRightSetting.equals(_TRUE);     
    final boolean enableTestRegionTail = enableTestRegionTailSetting.equals(_TRUE);     
    final boolean enableTestRegionLeft = enableTestRegionLeftSetting.equals(_TRUE);  
    
    //Siew Yeng - XCR-3318 - Oval PTH
    boolean enableTestRegion4Corners = true; 
    boolean enableTestRegionInnerCorner = false; 
    
    if(jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE))
    {
      enableTestRegion4Corners = Boolean.parseBoolean((String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS)); 
      if(enableTestRegion4Corners == false)
        enableTestRegionInnerCorner = Boolean.parseBoolean((String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER)); 
    }
    
    //Lim, Lay Ngor - XCR1743:Benchmark - START
    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
      _solderBallInspectionTime.reset();
//      System.out.println("detectShortsOnJointBasedOnShape: section 1 start");
      _solderBallInspectionTime.start();
    }
    
    final float minimumCenterSolderBallThicknessInMillis =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS);
    final String enableTestRegionCenterSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_CENTER);      
    final boolean enableTestRegionCenter = enableTestRegionCenterSetting.equals(_TRUE);     
    
    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
      _solderBallInspectionTime.stop();
//      System.out.println("detectShortsOnJointBasedOnShape: section 1 stop");
//      System.out.println("Solderball ElapseTime in function detectShortsOnJointBasedOnShape section 1: " + _solderBallInspectionTime.getElapsedTimeInMillis());
    }
    //Lim, Lay Ngor - XCR1743:Benchmark - END

    // Measure the background profiles around each side of the joint.
    Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> profileSideMap = measureRectangularRegionBackgroundGrayLevelProfiles(
      image,
      inspectionRegion,
      sliceNameEnum,
      jointInspectionData);

    // Get the head ROI gray level profile.
    Pair<RegionOfInterest, float[]> headRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.HEAD_PROFILE);
    RegionOfInterest headRoi = headRegionAndProfilePair.getFirst();
    float[] headProfile = headRegionAndProfilePair.getSecond();

    // Get the tail ROI gray level profile.
    Pair<RegionOfInterest, float[]> tailRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.TAIL_PROFILE);
    RegionOfInterest tailRoi = tailRegionAndProfilePair.getFirst();
    float[] tailProfile = tailRegionAndProfilePair.getSecond();

    // Get the left ROI gray level profile
    Pair<RegionOfInterest, float[]> leftRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.LEFT_PROFILE);
    RegionOfInterest leftRoi = leftRegionAndProfilePair.getFirst();
    float[] leftProfile = leftRegionAndProfilePair.getSecond();

    // Get the right ROI gray level profile.
    Pair<RegionOfInterest, float[]> rightRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.RIGHT_PROFILE);
    RegionOfInterest rightRoi = rightRegionAndProfilePair.getFirst();
    float[] rightProfile = rightRegionAndProfilePair.getSecond();

    //Siew Yeng - XCR-3318 - Oval PTH
//    GeneralPath rectangularShortRegion = new GeneralPath(outerProfileRoi.getShape());
    GeneralPath rectangularShortRegion = new GeneralPath();
    if(enableTestRegion4Corners)
    {
      // Create the all encompassing inner and outer profile rectangular regions.
      RegionOfInterest outerProfileRoi = new RegionOfInterest(headRoi);
      outerProfileRoi.add(tailRoi);
      outerProfileRoi.add(leftRoi);
      outerProfileRoi.add(rightRoi);
      RegionOfInterest innerProfileRoi = new RegionOfInterest(outerProfileRoi);
      int distanceBetweenInnerAndOuterRegions = headRoi.getLengthAcross();
      innerProfileRoi.setWidthKeepingSameCenter(outerProfileRoi.getWidth() - (2 * distanceBetweenInnerAndOuterRegions));
      innerProfileRoi.setHeightKeepingSameCenter(outerProfileRoi.getHeight() - (2 * distanceBetweenInnerAndOuterRegions));
      
      // Create an aggregate shape with the outer and inner regions.
      rectangularShortRegion.append(outerProfileRoi.getShape(), false);
      rectangularShortRegion.append(innerProfileRoi.getShape(), false);
    }
    else
    {
      rectangularShortRegion.append(headRoi.getShape(), false);
      rectangularShortRegion.append(tailRoi.getShape(), false);
      rectangularShortRegion.append(leftRoi.getShape(), false);
      rectangularShortRegion.append(rightRoi.getShape(), false);      
    }

    // Mark the start bin on the head roi and the end bin on the left roi.  This
    // marks the absolute beginning and end of the profile region.
    Line2D profileStartLineSegment = getLineSegmentForBinInCombinedRectangularRegion(headRoi, 0, false);
    MeasurementRegionDiagnosticInfo profileStartLineSegmentDiagInfo =
        new MeasurementRegionDiagnosticInfo(profileStartLineSegment, MeasurementRegionEnum.SHORT_PROFILE_START_BIN_REGION);
    Line2D profileEndLineSegment = getLineSegmentForBinInCombinedRectangularRegion(leftRoi, leftRoi.getLengthAlong() - 1, true);
    MeasurementRegionDiagnosticInfo profileEndLineSegmentDiagInfo =
        new MeasurementRegionDiagnosticInfo(profileEndLineSegment, MeasurementRegionEnum.SHORT_PROFILE_END_BIN_REGION);

    // Combine the 4 side gray level profiles into a single aggregate gray level profile.
    // This always starts at the 'head' and goes counter-clockwise (head, right, tail, left).
    float[] combinedRectangularGrayLevelProfile = ArrayUtil.combineArrays(new float[][]{
                                                                          headProfile,
                                                                          rightProfile,
                                                                          tailProfile,
                                                                          leftProfile });
    
    //Siew Yeng - XCR-3318
    int innerCornerProfileSize = 0;
    if(enableTestRegionInnerCorner)
    {
      //Siew Yeng - include inner corner
      // Get the bottom right ROI gray level profile.
      Pair<RegionOfInterest, float[]> bottomRightInnerCornerRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.BOTTOM_RIGHT_INNER_CORNER_PROFILE);
      RegionOfInterest bottomRightInnerCornerRoi = bottomRightInnerCornerRegionAndProfilePair.getFirst();
      float[] bottomRightInnerCornerProfile = bottomRightInnerCornerRegionAndProfilePair.getSecond();
      
      // Get the top right ROI gray level profile.
      Pair<RegionOfInterest, float[]> topRightInnerCornerRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.TOP_RIGHT_INNER_CORNER_PROFILE);
      RegionOfInterest topRightInnerCornerRoi = topRightInnerCornerRegionAndProfilePair.getFirst();
      float[] topRightInnerCornerProfile = topRightInnerCornerRegionAndProfilePair.getSecond();

      // Get the top left ROI gray level profile.
      Pair<RegionOfInterest, float[]> topLeftInnerCornerRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.TOP_LEFT_INNER_CORNER_PROFILE);
      RegionOfInterest topLeftInnerCornerRoi = topLeftInnerCornerRegionAndProfilePair.getFirst();
      float[] topLeftInnerCornerProfile = topLeftInnerCornerRegionAndProfilePair.getSecond();
      
      // Get the bottom left ROI gray level profile
      Pair<RegionOfInterest, float[]> bottomLeftInnerCornerRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.BOTTOM_LEFT_INNER_CORNER_PROFILE);
      RegionOfInterest bottomLeftInnerCornerRoi = bottomLeftInnerCornerRegionAndProfilePair.getFirst();
      float[] bottomLeftInnerCornerProfile = bottomLeftInnerCornerRegionAndProfilePair.getSecond();
      
      rectangularShortRegion.append(topLeftInnerCornerRoi.getShape(), false);
      rectangularShortRegion.append(topRightInnerCornerRoi.getShape(), false);
      rectangularShortRegion.append(bottomLeftInnerCornerRoi.getShape(), false);
      rectangularShortRegion.append(bottomRightInnerCornerRoi.getShape(), false);     
      
      combinedRectangularGrayLevelProfile = ArrayUtil.combineArrays(new float[][]{combinedRectangularGrayLevelProfile,
                                                                                  bottomRightInnerCornerProfile,
                                                                                  topRightInnerCornerProfile,
                                                                                  topLeftInnerCornerProfile,
                                                                                  bottomLeftInnerCornerProfile});
      //Profile size is same for all inner corner
      innerCornerProfileSize = bottomRightInnerCornerProfile.length;
    }
    
    // Convert the gray level profile into a thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float[] combinedRectangularThicknessProfile =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(combinedRectangularGrayLevelProfile,
                                                                             _GRAY_LEVEL_BACKGROUND_PERCENTILE,
                                                                             thicknessTable,
                                                                             backgroundSensitivityOffset);
    // Smooth the thickness profile.
    combinedRectangularThicknessProfile = ProfileUtil.getSmoothedProfile(combinedRectangularThicknessProfile,
                                                                         _SMOOTHING_KERNEL_LENGTH);
      
    //Lim, Lay Ngor - XCR1743:Benchmark START
    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
//      System.out.println("detectShortsOnJointBasedOnShape: section 2 start");
      _solderBallInspectionTime.start();
    }
    
    //center solder ball detection
    // Get the center ROI gray level profile.    
    //final int centerProfileStart = headProfile.length + rightProfile.length + tailProfile.length + leftProfile.length;
    final int centerProfileStart = combinedRectangularGrayLevelProfile.length;
    float[] combinedRectangularAndMultiROICenterGrayLevelProfile = combinedRectangularGrayLevelProfile;
    float[] combinedRectangularAndMultiROICenterThicknessProfile = combinedRectangularThicknessProfile;
    float[] combineMultiROICenterProfile = null; //Will use this null to check for center ROI availability at below.
    Pair<RegionOfInterest, float[]> firstCenterRegionAndProfilePair = profileSideMap.get(ShortProfileSideEnum.CENTER_PROFILE_1);
    if (firstCenterRegionAndProfilePair != null) //check for availability to avoid crash
    {     
      int id = ShortProfileSideEnum.CENTER_PROFILE_1.getId();
      combineMultiROICenterProfile = firstCenterRegionAndProfilePair.getSecond();
      //Now start from 2nd section of center ROI until the last available section of center ROI
      for(int i=0; i<ShortProfileSideEnum.getMaxSupportedCenterProfileEnum(); ++i)
      {
        Pair<RegionOfInterest, float[]> singleSectionCenterProfile = profileSideMap.get(ShortProfileSideEnum.getEnum(++id));
        if(singleSectionCenterProfile == null)
          break; //reach last section of center roi profile that we have, so just stop here
        else
        {
          combineMultiROICenterProfile = ArrayUtil.combineArrays(new float[][]{
            combineMultiROICenterProfile, singleSectionCenterProfile.getSecond() });               
        }      
      }

      combinedRectangularAndMultiROICenterGrayLevelProfile = ArrayUtil.combineArrays(new float[][]{
                                                                          combinedRectangularGrayLevelProfile, //head to tail only
                                                                          combineMultiROICenterProfile}); 
      
      // Convert the gray level profile into a thickness profile and overwrite the original value
      combinedRectangularAndMultiROICenterThicknessProfile =
          AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(
                                                                               combinedRectangularAndMultiROICenterGrayLevelProfile,
                                                                               _GRAY_LEVEL_BACKGROUND_PERCENTILE,
                                                                               thicknessTable,
                                                                               backgroundSensitivityOffset);
      // Smooth the thickness profile.
      combinedRectangularAndMultiROICenterThicknessProfile = ProfileUtil.getSmoothedProfile(combinedRectangularAndMultiROICenterThicknessProfile,
                                                                           _SMOOTHING_KERNEL_LENGTH);
      
      //Overwrite again the original rectangular thickness profile that without center ROI.
      combinedRectangularThicknessProfile = ArrayUtil.copy(combinedRectangularAndMultiROICenterThicknessProfile, 0, centerProfileStart); 
    }
    else
    {
      //Case with NO center ROI! Just set the non center ROI Thickness Profile to the variable for later use.
      combinedRectangularAndMultiROICenterThicknessProfile = combinedRectangularThicknessProfile;
    }

    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
      _solderBallInspectionTime.stop();    
//      System.out.println("detectShortsOnJointBasedOnShape: section 2 stop");
//      System.out.println("Solderball ElapseTime in function detectShortsOnJointBasedOnShape section 2: " + _solderBallInspectionTime.getElapsedTimeInMillis());
    }
    //Lim, Lay Ngor - XCR1743:Benchmark END
    
    // Retrieve the short learning for this joint.
    BooleanRef learnedDataAvailable = new BooleanRef(true);
    //Lim, Lay Ngor - already add center profile to short learning.
    ShortProfileLearning shortLearning = getShortProfileLearning(image,
                                                                 inspectionRegion,
                                                                 jointInspectionData,
                                                                 sliceNameEnum,
                                                                 learnedDataAvailable);

    //Lim, Lay Ngor - XCR1743:Benchmark START
    //Modify the learn process for solder ball detection.
    // Get the learned thickness profile.  If we don't have any learning, just create an all zeroes learned profile.
    float[] learnedThicknessWithCenterProfile = null;
    boolean detectedOldLearnDataOnly = false;
    if (learnedDataAvailable.getValue())
    {
      learnedThicknessWithCenterProfile = shortLearning.getAverageLearnedThicknessProfile();

      // Make sure that the learned profile sizes match the measured profile.
      //Lim, Lay Ngor - beware that when we remove center profile from combinedRectangularThicknessProfile
      //the length certainly not the same as learnedThicknessProfile      
      if (combinedRectangularAndMultiROICenterThicknessProfile.length != learnedThicknessWithCenterProfile.length)
      {
        //Found learn profile don't have center profile - possible is previous non-center-solder-ball detection learn data.
        if (enableTestRegionCenter == false)
        {
          //If the setting is False, we will just check is the length of learn profile without center profile 
          //is the same with the current profile that also without center profile.
          if (combinedRectangularThicknessProfile.length != learnedThicknessWithCenterProfile.length)
          {
            raiseProfileSizeMismatchWarning(jointInspectionData, sliceNameEnum);
            return;
          }
          else
          {
            //Found learn data without center profile
            detectedOldLearnDataOnly = true;
          }
        }
        else //user insist enable Center Test Region, we will skip checking for old learn data
        {
          raiseProfileSizeMismatchWarning(jointInspectionData, sliceNameEnum);
          return;
        }
      }
    }
    else
    {
      //Lim, Lay Ngor - beware that when we remove center profile from combinedRectangularThicknessProfile
      //the length certainly not the same as learnedThicknessProfile, pls combine the centerProfile here!    
      //learnedThicknessProfile = new float[combinedRectangularAndCenterGrayLevelProfile.length];
//      if (enableTestRegionCenter == false)       
//        learnedThicknessWithCenterProfile = new float[combinedRectangularThicknessProfile.length];
//      else
      learnedThicknessWithCenterProfile = new float[combinedRectangularAndMultiROICenterThicknessProfile.length];
      Arrays.fill(learnedThicknessWithCenterProfile, 0f);
    }

    //Becareful, ArrayUtil.copy endIndex is not including in the copy.
    float[] learnedThicknessProfile = ArrayUtil.copy(learnedThicknessWithCenterProfile, 0, centerProfileStart);
    if (combinedRectangularThicknessProfile.length != learnedThicknessProfile.length)
    {
      raiseProfileSizeMismatchWarning(jointInspectionData, sliceNameEnum);
      return;
    }
    //Lim, Lay Ngor - XCR1743:Benchmark END

    // Take the difference of the measured and learned thickness profiles - without center profile
    float[] deltaThicknessProfile = ArrayUtil.subtractArrays(combinedRectangularThicknessProfile, learnedThicknessProfile);

    //Lim, Lay Ngor - XCR2610 - skip display of measurement if disable all 
    //Siew Yeng - XCR-3318 - move from bottom
    // All region profile start and end
    int headProfileStart  = 0;                   int headProfileEnd  = headProfile.length - 1;
    int rightProfileStart = headProfileEnd + 1;  int rightProfileEnd = rightProfileStart + rightProfile.length - 1;
    int tailProfileStart  = rightProfileEnd + 1; int tailProfileEnd  = tailProfileStart + tailProfile.length - 1;
    int leftProfileStart  = tailProfileEnd + 1;  int leftProfileEnd  = leftProfileStart + leftProfile.length - 1;
    if(enableTestRegionHead || enableTestRegionRight || enableTestRegionTail || enableTestRegionLeft)
    {
      // Check the delta thickness profile for potential shorts.
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
      //Siew Yeng - XCR-3318
      //if enableTestRegion4Corners is true, wrap around true
      //if enableTestRegion4Corners is false, wrap around false(ignore corner)
      Map<Pair<Integer, Integer>, Pair<Float, Float>> candidateShortZonesToLengthAndThicknessMap =
                                                     analyzeDeltaThicknessProfileWithThicknessForShorts(image,
                                                                                                        inspectionRegion,
                                                                                                        jointInspectionData,
                                                                                                        sliceNameEnum,
                                                                                                        deltaThicknessProfile,
                                                                                                        MILIMETER_PER_PIXEL,
                                                                                                        enableTestRegion4Corners);  
      if(enableTestRegion4Corners)
      {
        //normal case with wrap around
        candidateShortZonesToLengthAndThicknessMap = disableTestRegionBasedOnAlgorithmSettings (subtype,
                                                                                   candidateShortZonesToLengthAndThicknessMap,
                                                                                   headProfile.length,
                                                                                   rightProfile.length,
                                                                                   tailProfile.length,
                                                                                   leftProfile.length,
                                                                                   MILIMETER_PER_PIXEL);
      }
      else
      {     
        //Siew Yeng - handle short without wrap around
        List<Integer> profilesSize = new ArrayList(Arrays.asList(headProfile.length, rightProfile.length,
                                                                tailProfile.length, leftProfile.length,
                                                                innerCornerProfileSize, innerCornerProfileSize,
                                                                innerCornerProfileSize, innerCornerProfileSize));
        List<Pair<Integer,Integer>> disableRegion = new ArrayList();
        if(enableTestRegionHead == false)
          disableRegion.add(new Pair<Integer, Integer>(headProfileStart, headProfileEnd));
        if(enableTestRegionRight == false)
          disableRegion.add(new Pair<Integer, Integer>(rightProfileStart, rightProfileEnd));
        if(enableTestRegionTail == false)
          disableRegion.add(new Pair<Integer, Integer>(tailProfileStart, tailProfileEnd));
        if(enableTestRegionLeft == false)
          disableRegion.add(new Pair<Integer, Integer>(leftProfileStart, leftProfileEnd));
        
        candidateShortZonesToLengthAndThicknessMap = disableTestRegionBasedOnAlgorithmSettingsWithoutWrapAround(subtype,
                                                                                   candidateShortZonesToLengthAndThicknessMap,
                                                                                   profilesSize,
                                                                                   disableRegion,
                                                                                   (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH),
                                                                                   MILIMETER_PER_PIXEL);
      }
      
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END

    // Based on our intiial list of potential short defects, build up a map of the short range to its defect
    // disposition.  The defect disposition will start out as QUESTIONABLE_SHORT.
    // Also, if diags are on, keep a map of all the short region shapes.  We need this later to mark the indicted zones.
    Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> shortZoneToDispositionMap =
        new HashMap<Pair<Integer, Integer>, ShortDefectDispositionEnum>();
    Map<Pair<Integer, Integer>, java.awt.Shape> shortZoneToShapeMap = new HashMap<Pair<Integer, Integer>, java.awt.Shape>();
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
    for (Pair<Integer, Integer> candidateShortZone : candidateShortZonesToLengthAndThicknessMap.keySet())
    {
      int candidateShortStartBin = candidateShortZone.getFirst();
      int candidateShortEndBin = candidateShortZone.getSecond();
      shortZoneToDispositionMap.put(candidateShortZone, ShortDefectDispositionEnum.QUESTIONABLE_SHORT);

      if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
      {
        java.awt.Shape candidateShortShape = getShapeForShortDefectInCombinedRectangularRegion(deltaThicknessProfile,
                                                                                               profileSideMap,
                                                                                               candidateShortStartBin,
                                                                                               candidateShortEndBin);
        shortZoneToShapeMap.put(candidateShortZone, candidateShortShape);
      }
    }

    // Verify that the shorts we found are true shorts.
    validateShortIndictments(image,
                             inspectionRegion,
                             subtype,
                             jointInspectionData,
                             sliceNameEnum,
                             combinedRectangularThicknessProfile,
                             shortZoneToDispositionMap,
                             false);//Lim, Lay Ngor - XCR1743:Benchmark

    // Keep track of an aggregate shape for all of the short defect regions.
    GeneralPath definiteShortDefectsAggregateShape = new GeneralPath();
    GeneralPath questionableShortDefectsAggregateShape = new GeneralPath();
    GeneralPath exoneratedShortDefectsAggregateShape = new GeneralPath();

    // Iterate thru our map and find any entries that are real shorts and indict them.
    // Also, label all questionable and exonerated shorts so we can show them in the diagnostics.
    for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : shortZoneToDispositionMap.entrySet())
    {
      Pair<Integer, Integer> candidateShortZone = mapEntry.getKey();
      ShortDefectDispositionEnum shortDefectDisposition = mapEntry.getValue();

      if (shortDefectDisposition.equals(ShortDefectDispositionEnum.DEFINITE_SHORT))
      {
        // Indict the short - instead of short length, we also display the short thickness here.
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
        final Pair<Float, Float> shortDataInMillis = candidateShortZonesToLengthAndThicknessMap.get(candidateShortZone);
        final float shortLengthInMillis = shortDataInMillis.getFirst(); 
        final float shortDeltaThicknessInMillis = shortDataInMillis.getSecond();
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END
     
        //indict short length
        indictShort(image,
                    inspectionRegion,
                    sliceNameEnum,
                    jointInspectionData,
                    shortLengthInMillis,
                    minimumShortLengthInMillis,
                    MeasurementEnum.SHORT_LENGTH);

        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
        //indict short thickness
        indictShort(image,
                    inspectionRegion,
                    sliceNameEnum,
                    jointInspectionData,
                    shortDeltaThicknessInMillis,//Lim, Lay Ngor - XCR1780 change to thickness
                    minimumShortThicknessInMillis,
                    MeasurementEnum.SHORT_THICKNESS);
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END

        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
        {
          // Add the short region shape to our aggregate definite short defects shape.
          java.awt.Shape shortDefectShape = shortZoneToShapeMap.get(candidateShortZone);
          Assert.expect(shortDefectShape != null);
          definiteShortDefectsAggregateShape.append(shortDefectShape, false);
        }

        jointPassed.setValue(false);
      }
      else if (shortDefectDisposition.equals(ShortDefectDispositionEnum.QUESTIONABLE_SHORT))
      {
        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
        {
          // Add the short region shape to our aggregate questionable short defects shape.
          java.awt.Shape shortDefectShape = shortZoneToShapeMap.get(candidateShortZone);
          Assert.expect(shortDefectShape != null);
          questionableShortDefectsAggregateShape.append(shortDefectShape, false);
        }
      }
      else if (shortDefectDisposition.equals(ShortDefectDispositionEnum.EXONERATED_SHORT))
      {
        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
        {
          // Add the short region shape to our aggregate exonerated short defects shape.
          java.awt.Shape shortDefectShape = shortZoneToShapeMap.get(candidateShortZone);
          Assert.expect(shortDefectShape != null);
          exoneratedShortDefectsAggregateShape.append(shortDefectShape, false);
        }
      }
      else
      {
        // Shouldn't get here!
        Assert.expect(false, "Unexpected short defect disposition : " + shortDefectDisposition);
      }
    }

    // If we didn't ANY indictments (confirmed or exonerated), we should double check to make sure we didn't get
    // any huge shorts which span the whole region.
    //Lim, Lay Ngor - This LARGE SHORT does not include solder ball area.
    //Lim, Lay Ngor - due to the learning start from this version is included the center profile, and therefore causing
    //shortLearning variable included centerProfile and hard to separate out the centerProfile from shortLearning.
    //We decided to detect the LARGE SHORT by not using the shortLearning.getAverageLearnedThicknessProfile() value.
    if (shortZoneToDispositionMap.isEmpty())
    {
      if (learnedDataAvailable.getValue())
      {
        //Lim, Lay Ngor - use combinedRectangularGrayLevelProfile and learnedThicknessProfile which did not include the 
        //center solder ball profile for LARGE SHORT detection.
        //Lim, Lay Ngor - If we want to include the solder ball profile in to LARGE SHORT detection, direct use 
        //combinedRectangularAndMultiROICenterGrayLevelProfile and shortLearning.getAverageLearnedThicknessProfile() value.
        //Note: shortLearning not necessary included the centerProfile, its depends on the learn data version 
        //that we use.
        //Please becareful as shortLearning size(with or without centerProfile) shall tally with measuredGrayLevelProfile size!
        combinedRectangularGrayLevelProfile = ArrayUtil.copy(combinedRectangularAndMultiROICenterGrayLevelProfile, 0, centerProfileStart);
        //XCR2111 - Revert back
        //Lim, Lay Ngor - Open comment below if want to include the center solder ball consideration.
        //Note: the flow may not applicable if solder ball consideration is apply here.
        float potentialLargeShortThicknessInMillis = measureLargeShortAcrossEntireRegion(combinedRectangularAndMultiROICenterGrayLevelProfile,
                                                                                         shortLearning,subtype,
                                                                                         backgroundSensitivityOffset);
        if (MathUtil.fuzzyGreaterThan(potentialLargeShortThicknessInMillis, _LARGE_SHORT_MAX_THICKNESS_IN_MILLIS))
        {
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
          //Large short only detected by thickness factor, so the indictment of short length is removed.  
          indictShort(image,
            inspectionRegion,
            sliceNameEnum,
            jointInspectionData,
            potentialLargeShortThicknessInMillis,//Lim, Lay Ngor - XCR1780 change to thickness
            _LARGE_SHORT_MAX_THICKNESS_IN_MILLIS,
            MeasurementEnum.SHORT_THICKNESS);
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - EMD     
        
          // Indicate that the joint is failing.
          jointPassed.setValue(false);

          if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
          {
            // Update the definte short regions aggregate shape.
            definiteShortDefectsAggregateShape.append(rectangularShortRegion, false);
          }
        }
      }
    }

    // Post the diagnostics for the short region (and defect zones).
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      MeasurementRegionDiagnosticInfo rectangularShortRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(rectangularShortRegion,
                                              MeasurementRegionEnum.SHORT_BACKGROUND_REGION);
      MeasurementRegionDiagnosticInfo definiteShortDefectRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(definiteShortDefectsAggregateShape,
                                              MeasurementRegionEnum.DEFINITE_SHORT_DEFECT_REGION);
/*      MeasurementRegionDiagnosticInfo questionableShortDefectRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(questionableShortDefectsAggregateShape,
                                              MeasurementRegionEnum.QUESTIONABLE_SHORT_DEFECT_REGION);
      MeasurementRegionDiagnosticInfo exoneratedShortDefectRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(exoneratedShortDefectsAggregateShape,
                                              MeasurementRegionEnum.EXONERATED_SHORT_DEFECT_REGION);*/

      // Prepare test region disable info
      Map<Pair<Integer, Integer>, MeasurementRegionEnum> shortTestRegionDisableInfo =
          new LinkedHashMap<Pair<Integer,Integer>,MeasurementRegionEnum>();
      // Prepare disable short region display in joint image
      GeneralPath disableShortRegionsAggregateShape = new GeneralPath();
      List<Integer> rangeDisableRegion = new ArrayList<Integer>();
      if (enableTestRegionHead == false)
      {
        rangeDisableRegion.add(headProfileStart);
        rangeDisableRegion.add(headProfileEnd);
        shortTestRegionDisableInfo.put(new Pair<Integer, Integer>(headProfileStart, headProfileEnd+1) , MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);
      }
      if (enableTestRegionRight == false)
      {
        if (rangeDisableRegion.contains(headProfileEnd))
          rangeDisableRegion.remove(rangeDisableRegion.indexOf(headProfileEnd));
        else
          rangeDisableRegion.add(rightProfileStart);
        rangeDisableRegion.add(rightProfileEnd);
        shortTestRegionDisableInfo.put(new Pair<Integer, Integer>(rightProfileStart, rightProfileEnd+1) , MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);
      }
      if (enableTestRegionTail == false)
      {
        if (rangeDisableRegion.contains(rightProfileEnd))
          rangeDisableRegion.remove(rangeDisableRegion.indexOf(rightProfileEnd));
        else
          rangeDisableRegion.add(tailProfileStart);
        rangeDisableRegion.add(tailProfileEnd);
        shortTestRegionDisableInfo.put(new Pair<Integer, Integer>(tailProfileStart, tailProfileEnd+1) , MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);
      }
      if (enableTestRegionLeft == false)
      {
        if (rangeDisableRegion.contains(tailProfileEnd))
          rangeDisableRegion.remove(rangeDisableRegion.indexOf(tailProfileEnd));
        else
          rangeDisableRegion.add(leftProfileStart);
        rangeDisableRegion.add(leftProfileEnd);
        shortTestRegionDisableInfo.put(new Pair<Integer, Integer>(leftProfileStart, leftProfileEnd+1) , MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);
      }
      Assert.expect((rangeDisableRegion.size()%2) == 0);
      // Handle wraparound case
      if ( (enableTestRegionHead == false) && (enableTestRegionLeft == false) )
      {
        // Make sure it is not a big wraparound
        if ( (enableTestRegionRight == true) || (enableTestRegionTail == true ) )
        {
          rangeDisableRegion.remove(rangeDisableRegion.size()-1);
          rangeDisableRegion.remove(0);
          rangeDisableRegion.add(0, rangeDisableRegion.get(rangeDisableRegion.size()-1));
          rangeDisableRegion.remove(rangeDisableRegion.size()-1);
        }
      }
      for (int regionIndex = 0; regionIndex < rangeDisableRegion.size()/2; ++regionIndex)
      {
        java.awt.Shape disableShortRegionShape = getShapeForShortDefectInCombinedRectangularRegion(deltaThicknessProfile,
                                                                                                   profileSideMap,
                                                                                                   rangeDisableRegion.get(regionIndex*2),
                                                                                                   rangeDisableRegion.get(regionIndex*2+1));
        disableShortRegionsAggregateShape.append(disableShortRegionShape, false);
      }
      MeasurementRegionDiagnosticInfo disableShortRegionDiagInfo =
      new MeasurementRegionDiagnosticInfo(disableShortRegionsAggregateShape,
                                          MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);

      ProfileDiagnosticInfo rectangularShortProfileDiagInfo = createShortProfileDiagnosticInfoWithDisableInfo(jointInspectionData,
                                                                                               sliceNameEnum,
                                                                                               deltaThicknessProfile,
                                                                                               shortZoneToDispositionMap,
                                                                                               shortTestRegionDisableInfo,
                                                                                               false);//Lim, Lay Ngor - XCR1743:Benchmark

      _diagnostics.postDiagnostics(inspectionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   this,
                                   false,
                                   rectangularShortRegionDiagInfo,
                                   profileStartLineSegmentDiagInfo,
                                   profileEndLineSegmentDiagInfo,
                                   definiteShortDefectRegionDiagInfo,
//                                   questionableShortDefectRegionDiagInfo,
//                                   exoneratedShortDefectRegionDiagInfo,
                                   rectangularShortProfileDiagInfo,
                                   disableShortRegionDiagInfo);
    } //Lim, Lay Ngor - XCR2610
      
      //Measurement data for passing component
      if(jointPassed.getValue() == true)
      {
        calculateAndRecordPassMeasurementForShort(jointInspectionData, sliceNameEnum, deltaThicknessProfile,
          headProfile.length, rightProfile.length, tailProfile.length, leftProfile.length);      
      }
    }

    //Lim, Lay Ngor - XCR1743:Benchmark - START
    if(enableTestRegionCenter == false)
      return;//skip inspection if it is false
    
    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
//      System.out.println("detectShortsOnJointBasedOnShape: section 3 start");
      _solderBallInspectionTime.start();
    }
    
    //start center solder ball detection!
    if (enableTestRegionCenter && combineMultiROICenterProfile == null)
    {
      raiseEmptyCenterROIWarning(jointInspectionData, sliceNameEnum);
      return;
    }

//    if (enableTestRegionCenter && combineMultiROICenterProfile != null) 
    if (detectedOldLearnDataOnly == false && combineMultiROICenterProfile != null) //if wanna use this, pls ensure all test case especially for Leadless & old learn profile able to run.
    {
      boolean solderBallPassed = true;
      final int multiCenterProfileEnd  = centerProfileStart + combineMultiROICenterProfile.length - 1;       
      //Warning, ArrayUtil.copy endIndex is not including in the copy.
      final float[] centerRectangularThicknessProfile = ArrayUtil.copy(combinedRectangularAndMultiROICenterThicknessProfile, centerProfileStart, multiCenterProfileEnd + 1);

      //To get center learn profile only
      final float[] learnedCenterThicknessProfile = ArrayUtil.copy(learnedThicknessWithCenterProfile, centerProfileStart, multiCenterProfileEnd + 1);
      if(centerRectangularThicknessProfile.length != learnedCenterThicknessProfile.length)
      {
          raiseProfileSizeMismatchWarning(jointInspectionData, sliceNameEnum);
          return;
      }
              
      // Take the difference of the measured and learned center thickness profiles
      // Delta center thickness is define as "Short Thickness" in GUI.
      final float[] deltaCenterThicknessProfile = ArrayUtil.subtractArrays(centerRectangularThicknessProfile, learnedCenterThicknessProfile);
      
      List<Integer> profilesSize = new ArrayList();
      Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> multiROIProfileCenterMap =
        new HashMap<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>>();
      int id = ShortProfileSideEnum.CENTER_PROFILE_1.getId();
      List<GeneralPath> singleSectionPathList = new ArrayList<GeneralPath>();
      RegionOfInterest combinedMultiCenterRoi = null; // just use for display the disable region.
      //Now start from 1st section of center roi, loop until the last available section of center roi
      for(int i=0; i<ShortProfileSideEnum.getMaxSupportedCenterProfileEnum(); ++i)
      {
        Pair<RegionOfInterest, float[]> singleSectionCenterProfile = profileSideMap.get(ShortProfileSideEnum.getEnum(id));
        if(singleSectionCenterProfile == null)
          break; //reach the last section of center roi profile that we have, so just stop here
        else
        {
          profilesSize.add(singleSectionCenterProfile.getSecond().length);
          multiROIProfileCenterMap.put(ShortProfileSideEnum.getEnum(id), singleSectionCenterProfile);

          //One by one add the multi section center ROI for later display
          singleSectionPathList.add(new GeneralPath(singleSectionCenterProfile.getFirst().getShape()));
          //Create a new RegionOfInterest and add each section of ROI for display the center ROI in whole.
          //This is for disable region used only with another new GeneralPath to display the single center ROI. 
          if(i==0)
            combinedMultiCenterRoi = new RegionOfInterest(singleSectionCenterProfile.getFirst());
          else
            combinedMultiCenterRoi.add(singleSectionCenterProfile.getFirst());
        }
        ++id;
      }
      
      //Siew Yeng - XCR-3318
      //To get the map with Delta Thickness info
      Map<Pair<Integer, Integer>, Pair<Float, Float>> candidateCenterShortZonesToLengthAndThicknessMap =
        analyzeDeltaThicknessProfileWithThicknessForShorts(image,
        inspectionRegion,
        jointInspectionData,
        sliceNameEnum,
        deltaCenterThicknessProfile,
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS),
        _MINIMUM_SHORT_LENGTH_DEFAULT_VALUE_IN_MILLIS,
        MILIMETER_PER_PIXEL,
        false);
      
      candidateCenterShortZonesToLengthAndThicknessMap = 
              disableTestRegionBasedOnAlgorithmSettingsWithoutWrapAround(subtype,
                                                                         candidateCenterShortZonesToLengthAndThicknessMap,
                                                                         profilesSize,
                                                                         new ArrayList(), //Siew Yeng - put empty list since all roi is enabled
                                                                         _MINIMUM_SHORT_LENGTH_DEFAULT_VALUE_IN_MILLIS,
                                                                         MILIMETER_PER_PIXEL);
//      candidateCenterShortZonesToLengthAndThicknessMap = disableCenterTestRegionBasedOnAlgorithmSettings(subtype,
//        candidateCenterShortZonesToLengthAndThicknessMap,
//        combineMultiROICenterProfile.length,
//        MILIMETER_PER_PIXEL);

      if (combinedMultiCenterRoi == null)
      {
        raiseEmptyCenterROIWarning(jointInspectionData, sliceNameEnum);
        return;
      }     

      Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> centerShortZoneToDispositionMap =
        new HashMap<Pair<Integer, Integer>, ShortDefectDispositionEnum>();
      Map<Pair<Integer, Integer>, java.awt.Shape> centerShortZoneToShapeMap = new HashMap<Pair<Integer, Integer>, java.awt.Shape>();
      for (Pair<Integer, Integer> candidateCenterShortZone : candidateCenterShortZonesToLengthAndThicknessMap.keySet())      
      {
        final int candidateCenterShortStartBin = candidateCenterShortZone.getFirst();
        final int candidateCenterShortEndBin = candidateCenterShortZone.getSecond();
        centerShortZoneToDispositionMap.put(candidateCenterShortZone, ShortDefectDispositionEnum.QUESTIONABLE_SHORT);

        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
        {
          java.awt.Shape candidateCenterShortShape = getShapeForShortDefectInCenterRectangularRegion(deltaCenterThicknessProfile,
            multiROIProfileCenterMap,
            candidateCenterShortStartBin,
            candidateCenterShortEndBin);
          centerShortZoneToShapeMap.put(candidateCenterShortZone, candidateCenterShortShape);
        }
      }

      //Display the detected true short here
      validateShortIndictments(image,
        inspectionRegion,
        subtype,
        jointInspectionData,
        sliceNameEnum,
        centerRectangularThicknessProfile,
        centerShortZoneToDispositionMap,
        true);  

      // Keep track of an aggregate shape for all of the short defect regions.
      GeneralPath definiteCenterShortDefectsAggregateShape = new GeneralPath();
      GeneralPath questionableCenterShortDefectsAggregateShape = new GeneralPath();
      GeneralPath exoneratedCenterShortDefectsAggregateShape = new GeneralPath();

      //Define each detected center short(solder ball) disposition
      for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : centerShortZoneToDispositionMap.entrySet())
      {
        Pair<Integer, Integer> candidateCenterShortZone = mapEntry.getKey();
        ShortDefectDispositionEnum centerShortDefectDisposition = mapEntry.getValue();

        if (centerShortDefectDisposition.equals(ShortDefectDispositionEnum.DEFINITE_SHORT))
        {
          // Indict the short - instead of short length, we display the short thickness here.
          final Pair<Float, Float> shortDataInMillis = candidateCenterShortZonesToLengthAndThicknessMap.get(candidateCenterShortZone);
//          final float shortLengthInMillis = shortDataInMillis.getFirst(); //this is just detected short length of each section of center ROI
          final float shortDeltaThicknessInMillis = shortDataInMillis.getSecond();
          indictShort(image,
            inspectionRegion,
            sliceNameEnum,
            jointInspectionData,
            shortDeltaThicknessInMillis, //instead of show the length of the short(meaningless), we show the thickness to assist user fine tune the threshold
            minimumCenterSolderBallThicknessInMillis,
            MeasurementEnum.SOLDER_BALL_SHORT_DELTA_THICKNESS);

          if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
          {
            // Add the short region shape to our aggregate definite short defects shape.
            java.awt.Shape shortCenterDefectShape = centerShortZoneToShapeMap.get(candidateCenterShortZone);
            Assert.expect(shortCenterDefectShape != null);
            definiteCenterShortDefectsAggregateShape.append(shortCenterDefectShape, false);
          }

          jointPassed.setValue(false);
          solderBallPassed = false;
        }
        else if (centerShortDefectDisposition.equals(ShortDefectDispositionEnum.QUESTIONABLE_SHORT))
        {
          if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
          {
            // Add the short region shape to our aggregate questionable short defects shape.
            java.awt.Shape shortCenterDefectShape = centerShortZoneToShapeMap.get(candidateCenterShortZone);
            Assert.expect(shortCenterDefectShape != null);
            questionableCenterShortDefectsAggregateShape.append(shortCenterDefectShape, false);
          }
        }
        else if (centerShortDefectDisposition.equals(ShortDefectDispositionEnum.EXONERATED_SHORT))
        {
          if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
          {
            // Add the short region shape to our aggregate exonerated short defects shape.
            java.awt.Shape shortCenterDefectShape = centerShortZoneToShapeMap.get(candidateCenterShortZone);
            Assert.expect(shortCenterDefectShape != null);
            exoneratedCenterShortDefectsAggregateShape.append(shortCenterDefectShape, false);
          }
        }
        else
        {
          // Shouldn't get here!
          Assert.expect(false, "Unexpected short defect disposition : " + centerShortDefectDisposition);
        }
      }
      
      //Lim, Lay Ngor - We will not detect Large short for solder ball profile.
      //Lim, Lay Ngor - XCR2610
      //Measurement data for passing component
      if(solderBallPassed == true)
      {
        calculateAndRecordPassMeasurementForCenterShort(jointInspectionData, sliceNameEnum, deltaCenterThicknessProfile);
      }
      
      // Post the diagnostics for the short region (and defect zones).
      if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
      {                       
        MeasurementRegionDiagnosticInfo definiteCenterShortDefectRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(definiteCenterShortDefectsAggregateShape,
          MeasurementRegionEnum.DEFINITE_SHORT_DEFECT_REGION);
        /*      MeasurementRegionDiagnosticInfo questionableCenterShortDefectRegionDiagInfo =
         new MeasurementRegionDiagnosticInfo(questionableCenterShortDefectsAggregateShape,
         MeasurementRegionEnum.QUESTIONABLE_SHORT_DEFECT_REGION);
         MeasurementRegionDiagnosticInfo exoneratedCenterShortDefectRegionDiagInfo =
         new MeasurementRegionDiagnosticInfo(exoneratedCenterShortDefectsAggregateShape,
         MeasurementRegionEnum.EXONERATED_SHORT_DEFECT_REGION);*/
        
        //We will never enter here because due to we need to do revision handling for previous learn data that did not have 
        //center roi information, we had restrict the whole center detection portion must fullfill 
        //enableTestRegionCenter = true at the begining of the scope. The below code can be use when
        //future we don't need to restrict for enableTestRegionCenter = true at the begining of the scope.
        GeneralPath disableCenterShortRegionsAggregateShape = new GeneralPath();
        Map<Pair<Integer, Integer>, MeasurementRegionEnum> centerShortTestRegionDisableInfo =
          new LinkedHashMap<Pair<Integer, Integer>, MeasurementRegionEnum>();                     
        if (enableTestRegionCenter == false)
        {
          centerShortTestRegionDisableInfo.put(new Pair<Integer, Integer>(0, combineMultiROICenterProfile.length - 1), 
            MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);
        
         disableCenterShortRegionsAggregateShape.append(combinedMultiCenterRoi.getShape(), false);
        }
       
        MeasurementRegionDiagnosticInfo disableShortCenterRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(disableCenterShortRegionsAggregateShape,
          MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);
        //End not possible enter portion

        ProfileDiagnosticInfo rectangularCenterShortProfileDiagInfo = createShortProfileDiagnosticInfoWithDisableInfo(jointInspectionData,
          sliceNameEnum,
          deltaCenterThicknessProfile,
          centerShortZoneToDispositionMap,
          centerShortTestRegionDisableInfo,
          true);

        //One by one display the multi section center ROI at GUI
        for(GeneralPath singleSectionPath : singleSectionPathList)
        {         
          MeasurementRegionDiagnosticInfo singleSectionDiagInfo =
            new MeasurementRegionDiagnosticInfo(singleSectionPath,
            MeasurementRegionEnum.SHORT_BACKGROUND_REGION);
          
          _diagnostics.postDiagnostics(inspectionRegion,
            sliceNameEnum,
            jointInspectionData,
            this,
            false,
            singleSectionDiagInfo);
        }               
        
        _diagnostics.postDiagnostics(inspectionRegion,
          sliceNameEnum,
          jointInspectionData,
          this,
          false,
          definiteCenterShortDefectRegionDiagInfo,
          // questionableCenterShortDefectRegionDiagInfo,
          // exoneratedCenterShortDefectRegionDiagInfo,                                   
          rectangularCenterShortProfileDiagInfo,
          disableShortCenterRegionDiagInfo);
      }
    }

    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
      _solderBallInspectionTime.stop();
//      System.out.println("detectShortsOnJointBasedOnShape: section 3 stop: ");      
//      System.out.println("Solderball ElapseTime in function detectShortsOnJointBasedOnShape section 3: " + _solderBallInspectionTime.getElapsedTimeInMillis());
      long totalSolderBallInspectionTime = _solderBallInspectionTime.getElapsedTimeInMillis();
      System.out.println("Total Solder Ball Inspection time: " + totalSolderBallInspectionTime);
    }
	//Lim, Lay Ngor - XCR1743:Benchmark - END
  }

  /**
   * Calculate and record passing measurement if short test region is enable.
   * @author Lim, Lay Ngor
   */
  private void calculateAndRecordPassMeasurementForShort(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum, float[] deltaThicknessProfile,
    int headProfileSize, int rightProfileSize, int tailProfileSize, int leftProfileSize)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(deltaThicknessProfile != null);
    
    Subtype subtype = jointInspectionData.getSubtype();
    // Disable test region according to user settings
    //LNLim Note: if it is disable, we will still keep the short delta thickness data we set above.      
    String enableTestRegionHeadSetting = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_HEAD);
    String enableTestRegionRightSetting = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_RIGHT);
    String enableTestRegionTailSetting = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_TAIL);
    String enableTestRegionLeftSetting = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_LEFT);

    if(enableTestRegionHeadSetting.equals(_FALSE) && enableTestRegionRightSetting.equals(_FALSE) &&
      enableTestRegionTailSetting.equals(_FALSE) && enableTestRegionLeftSetting.equals(_FALSE))
    {
      return;//Not to record any pass measurement since all region is disable.
    }
    
    // All region start and end
    int headProfileStart = 0;                     int headProfileEnd = headProfileSize - 1;
    int rightProfileStart = headProfileEnd + 1;   int rightProfileEnd = rightProfileStart + rightProfileSize - 1;
    int tailProfileStart = rightProfileEnd + 1;   int tailProfileEnd = tailProfileStart + tailProfileSize - 1;
    int leftProfileStart = tailProfileEnd + 1;    int leftProfileEnd = leftProfileStart + leftProfileSize - 1;

    float maxThickness = ArrayUtil.min(deltaThicknessProfile);//initialize as minimum value first
    if(enableTestRegionHeadSetting.equals(_TRUE) && enableTestRegionRightSetting.equals(_TRUE) &&
      enableTestRegionTailSetting.equals(_TRUE) && enableTestRegionLeftSetting.equals(_TRUE))
    {
      maxThickness = ArrayUtil.max(deltaThicknessProfile);//check for full profile maximum value if all region is enable
    }    
    else
    {
      if (enableTestRegionHeadSetting.equals(_TRUE))
      {
        float[] headArray = ArrayUtil.copy(deltaThicknessProfile, headProfileStart, headProfileEnd + 1);
        maxThickness = ArrayUtil.max(headArray);//direct replace maxThickness for the first range
      }

      if (enableTestRegionRightSetting.equals(_TRUE))
      {
        float[] rightArray = ArrayUtil.copy(deltaThicknessProfile, rightProfileStart, rightProfileEnd + 1);
        maxThickness = Math.max(maxThickness, ArrayUtil.max(rightArray));//incase head region have max value
      }

      if (enableTestRegionTailSetting.equals(_TRUE))
      {
        float[] tailArray = ArrayUtil.copy(deltaThicknessProfile, tailProfileStart, tailProfileEnd + 1);
        maxThickness = Math.max(maxThickness, ArrayUtil.max(tailArray));
      }

      if (enableTestRegionLeftSetting.equals(_TRUE))
      {
        float[] leftArray = ArrayUtil.copy(deltaThicknessProfile, leftProfileStart, leftProfileEnd + 1);
        maxThickness = Math.max(maxThickness, ArrayUtil.max(leftArray));
      }
    }

    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
    //Show max thickness as pass value if not detected as defect under this pad
    //Lim, Lay Ngor - XCR2167 - show measurement as component type if it is under category isSpecialTwoPinComponent().
    recordPassedMeasurement(sliceNameEnum, jointInspectionData, maxThickness, MeasurementEnum.SHORT_THICKNESS);
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END      
  }
  
  /**
   * Calculate and record passing measurement if center short test region is enable.
   * 
   * @author Lim, Lay Ngor
   */
  private void calculateAndRecordPassMeasurementForCenterShort(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum, float[] deltaCenterThicknessProfile)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(deltaCenterThicknessProfile != null);
    
    Subtype subtype = jointInspectionData.getSubtype();
    // Disable test region according to user settings
    //LNLim Note: if it is disable, we will still keep the short delta thickness data we set above.      
    String enableTestRegionCenterSetting = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_CENTER);
    if(enableTestRegionCenterSetting.equals(_TRUE))
    {    
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
      //Show max thickness as pass value if not detected as defect under this pad
      final float maxThickness = ArrayUtil.max(deltaCenterThicknessProfile);
      //Lim, Lay Ngor - XCR2167 - show measurement as component type if it is under category isSpecialTwoPinComponent().
      recordPassedMeasurement(sliceNameEnum, jointInspectionData, maxThickness, MeasurementEnum.SOLDER_BALL_SHORT_DELTA_THICKNESS);
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END      
    }
    //Else, not to record any pass measurement since region is disable.
  }  
  
  /**
   * This routine is used to take out disabled region based on short algorithm settings.
   * All regions are enabled by default.
   *
   * Map<Pair<Integer, Integer>, Float>candidateShortZonesToLengthMap
   *
   * Map<Pair<  X,          Y>,      Z>
   *            ^           ^
   *            |     Z     |
   *            |<--------->|
   * ----------------------------------------------------------------------
   * |      HEAD      |      RIGHT      |      TAIL      |      LEFT      |
   * ----------------------------------------------------------------------
   * All possible region location:
   *
   *        |---|
   *        |-----------------|
   *        |-----------------------------------|
   *        |----------------------------------------------------|
   *                          |---|
   *                          |-----------------|
   *                          |----------------------------------|
   *                                            |---|
   *                                            |----------------|
   *                                                         |---|
   * Wrap-around cases:
   * Strategy to handle these is to split them and then combine again afterwards.
   * -----|                                                      |---------
   * -------------------------|                                  |---------
   * -----|                                     |--------------------------
   * -----|                   |--------------------------------------------
   * -------------------------|                 |--------------------------
   * -------------------------------------------|                |---------
   * -----|  |-------------------------------------------------------------
   * ---------------------|  |---------------------------------------------
   * ----------------------------------------|  |--------------------------
   * ---------------------------------------------------------|  |---------
   *
   * @author Lim, Seng Yew
   *
   * Be careful that in Arrays.fill(array a, int fromIndex, int toIndex, value val)
   * fromIndex is included but toIndex is excluded. (refer JBuilder Help as below)
   *    Assigns the specified boolean value to each element of the specified range of the specified array of booleans.
   *    The range to be filled extends from index fromIndex, inclusive, to index toIndex, exclusive.
   *    (If fromIndex==toIndex, the range to be filled is empty.)
   *
   * The algorithm in this routine is inspired by Peter Esbensen.
   * * @author Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness
   *
   */
    /** PE - I recommend considering an alternate approach to disabling sections of the profile.  I think it could
     * be done more simply, with less chance of error in the coding.
     */
    private Map<Pair<Integer, Integer>, Pair<Float, Float>> disableTestRegionBasedOnAlgorithmSettings(Subtype subtype,
                                                                                          Map<Pair<Integer, Integer>, Pair<Float, Float>>candidateShortZonesToLengthAndThicknessMap,
                                                                                          int headProfileSize,
                                                                                          int rightProfileSize,
                                                                                          int tailProfileSize,
                                                                                          int leftProfileSize,
                                                                                          final float MILIMETER_PER_PIXEL)
    {
      Assert.expect(subtype != null);

      // create an array to keep track of where we have shorts
      int totalProfileSize = headProfileSize + rightProfileSize + tailProfileSize + leftProfileSize;
      boolean[] candidateShortArray = new boolean[ totalProfileSize ];
      
      // initialize it all to false (no shorts)
      Arrays.fill(candidateShortArray, false);

      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
      float[] candidateShortDeltaThickness = new float[totalProfileSize];
      Arrays.fill(candidateShortDeltaThickness, 0.f);      
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END

      // now go through the candidate short zones and update the short array we just created
      // basically we are converting from a list of zones to a simple array
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
      for (Pair<Integer, Integer> shortZone : candidateShortZonesToLengthAndThicknessMap.keySet())
      {
        int startBinIndex = shortZone.getFirst();
        int endBinIndex = shortZone.getSecond();
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
        Pair<Float, Float> shortDataInMillis = candidateShortZonesToLengthAndThicknessMap.get(shortZone);

        // Handle wraparound case
        if ( startBinIndex > endBinIndex )
        {
          Arrays.fill(candidateShortArray, startBinIndex, totalProfileSize, true);
          Arrays.fill(candidateShortArray, 0, endBinIndex + 1, true);
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
          //Fill in the maximum delta thickness for the whole range of this short
          Arrays.fill(candidateShortDeltaThickness, startBinIndex, totalProfileSize, shortDataInMillis.getSecond());
          Arrays.fill(candidateShortDeltaThickness, 0, endBinIndex + 1, shortDataInMillis.getSecond());
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END          
        }
        else
        {
          Arrays.fill(candidateShortArray, startBinIndex, endBinIndex + 1, true);
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
          //Fill in the maximum delta thickness for the whole range of this short
          Arrays.fill(candidateShortDeltaThickness, startBinIndex, endBinIndex+1, shortDataInMillis.getSecond());          
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END
        }
      }

      // All region start and end
      int headProfileStart  = 0;                   int headProfileEnd  = headProfileSize - 1;
      int rightProfileStart = headProfileEnd + 1;  int rightProfileEnd = rightProfileStart + rightProfileSize - 1;
      int tailProfileStart  = rightProfileEnd + 1; int tailProfileEnd  = tailProfileStart + tailProfileSize - 1;
      int leftProfileStart  = tailProfileEnd + 1;  int leftProfileEnd  = leftProfileStart + leftProfileSize - 1;

      // Disable test region according to user settings
      //LNLim Note: if it is disable, we will still keep the short delta thickness data we set above.      
      String enableTestRegionHeadSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_HEAD);
      if (enableTestRegionHeadSetting.equals(_FALSE))
      {
        Arrays.fill(candidateShortArray, headProfileStart, headProfileEnd + 1, false);
      }
      String enableTestRegionRightSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_RIGHT);
      if (enableTestRegionRightSetting.equals(_FALSE))
      {
        Arrays.fill(candidateShortArray, rightProfileStart, rightProfileEnd + 1, false);
      }
      String enableTestRegionTailSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_TAIL);
      if (enableTestRegionTailSetting.equals(_FALSE))
      {
        Arrays.fill(candidateShortArray, tailProfileStart, tailProfileEnd + 1, false);
      }
      String enableTestRegionLeftSetting  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_LEFT);
      if (enableTestRegionLeftSetting.equals(_FALSE))
      {
        Arrays.fill(candidateShortArray, leftProfileStart, leftProfileEnd + 1, false);
      }

      // This is the new object to return.
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
      Map<Pair<Integer, Integer>, Pair<Float, Float>> newCandidateShortZonesToLengthAndThicknessMap =
        new HashMap<Pair<Integer, Integer>, Pair<Float, Float>>();

      // Collecting info on wraparound case
      int locationOfWraparoundShortStart = -1;
      if (candidateShortArray[0] && candidateShortArray[candidateShortArray.length-1])
      {
        // search backwards for the start of the wraparound short.  also handle the case where the entire profile is one giant wraparound short
        locationOfWraparoundShortStart = candidateShortArray.length-1;
        while ( --locationOfWraparoundShortStart != 0 )
        {
          if ( candidateShortArray[locationOfWraparoundShortStart] == false )
          {
            break;
          }
        }
        if (locationOfWraparoundShortStart == 0)
        {
          // If the program comes to here, it means it's a giant wraparound short
          float shortLengthInMillis = candidateShortArray.length * MILIMETER_PER_PIXEL;
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
          //Get maximum delta thickness within the range again here base on previous result.
          final float finalCandidateShortDeltaThicknessInMillis = StatisticsUtil.maximum(candidateShortDeltaThickness, 0, (candidateShortArray.length - 1));          
          newCandidateShortZonesToLengthAndThicknessMap.put(new Pair<Integer, Integer>(0, (candidateShortArray.length - 1)), 
            new Pair<Float, Float>(shortLengthInMillis, finalCandidateShortDeltaThicknessInMillis));
          return newCandidateShortZonesToLengthAndThicknessMap;
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END
        }
        --locationOfWraparoundShortStart;
      }

      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
      final float minimumShortLengthInMillis = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH);
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END 

      // now that we've found the start of any potential wraparound short, we can just go through the rest of the profile
      // now convert the candidate short array back into the Map we need to return
      boolean inCandidateShort = false;
      int candidateShortStart = -1;
      for (int i = 0; i < candidateShortArray.length; ++i)
      {
        if ((candidateShortArray[i] == true) && (inCandidateShort == false))
        {
          inCandidateShort = true;
          candidateShortStart = i;
          continue;
        }
        if ((candidateShortArray[i] == false) && (inCandidateShort == true))
        {
          // we've reach the end of a potential short
          inCandidateShort = false;
          if ( locationOfWraparoundShortStart > -1 )
          {
            // handle wraparound case
            final int shortLengthInPixels = (i - candidateShortStart) + (candidateShortArray.length - locationOfWraparoundShortStart);
            final float shortLengthInMillis = shortLengthInPixels * MILIMETER_PER_PIXEL;            
            //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length              
            if (shortLengthInMillis >= minimumShortLengthInMillis)
            {
              //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
              //Get maximum delta thickness within the range again here base on previous result.
              final float fistCandidateShortDeltaThicknessInMillis = StatisticsUtil.maximum(candidateShortDeltaThickness, candidateShortStart, i);
              final float secondCandidateShortDeltaThicknessInMillis = StatisticsUtil.maximum(candidateShortDeltaThickness, locationOfWraparoundShortStart, candidateShortArray.length);
              final float finalCandidateShortDeltaThicknessInMillis = Math.max(fistCandidateShortDeltaThicknessInMillis, secondCandidateShortDeltaThicknessInMillis);

              newCandidateShortZonesToLengthAndThicknessMap.put(
                new Pair<Integer, Integer>(locationOfWraparoundShortStart, (i - 1)), //short range in profile
                new Pair<Float, Float>(shortLengthInMillis, finalCandidateShortDeltaThicknessInMillis)); //short info/data  
              //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END
            }
            locationOfWraparoundShortStart = -2;
          }
          else
          {
            // add it to our final map
            final int shortLengthInPixels = i - candidateShortStart;
            final float shortLengthInMillis = shortLengthInPixels * MILIMETER_PER_PIXEL;            
            //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
            if (shortLengthInMillis >= minimumShortLengthInMillis)            
            {
              //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
              //Get maximum delta thickness within the range again here base on previous result.
              final float finalCandidateShortDeltaThicknessInMillis = StatisticsUtil.maximum(candidateShortDeltaThickness, candidateShortStart, (i - 1));              
              
              newCandidateShortZonesToLengthAndThicknessMap.put(
                new Pair<Integer, Integer>(candidateShortStart, (i - 1)), //short range in profile
                new Pair<Float, Float>(shortLengthInMillis, finalCandidateShortDeltaThicknessInMillis)); //short info/data        
              //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END              
            }
          }
          candidateShortStart = -1;
        }
      }

      // handle short in left region where it ends at the very last bin of the profile.
      // If wraparound is detected and executed before(locationOfWraparoundShortStart == -2), should not run this portion.
      if ( (candidateShortStart != -1) && (locationOfWraparoundShortStart != -2) )
      {
        final int shortLengthInPixels = (candidateShortArray.length - candidateShortStart);
        final float shortLengthInMillis = shortLengthInPixels * MILIMETER_PER_PIXEL;        
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
        if (shortLengthInMillis >= minimumShortLengthInMillis)
        {
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
          //Get maximum delta thickness within the range again here base on previous result.
          final float finalCandidateShortDeltaThicknessInMillis = StatisticsUtil.maximum(candidateShortDeltaThickness, candidateShortStart, (candidateShortArray.length - 1));

          newCandidateShortZonesToLengthAndThicknessMap.put(
            new Pair<Integer, Integer>(candidateShortStart, (candidateShortArray.length - 1)), //short range in profile
            new Pair<Float, Float>(shortLengthInMillis, finalCandidateShortDeltaThicknessInMillis)); //short info/data        
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END             
        }
        candidateShortStart = -1;
      }

      return newCandidateShortZonesToLengthAndThicknessMap;
  }
    
  /**
   * Code reference from function disableTestRegionBasedOnAlgorithmSettings.
   * But this  function is used by Center Region for solder ball detection
   * OR region that does not need wrap around case.
   * Besides, this function had add in the short thickness(use maximum from the group of short candidates)
   * to the return short data information.
   * Warning: Arrays.fill "toIndex" is not including in the fill.
   * 
   * @param subtype
   * @param candidateShortZonesToLengthMap
   * @param profileSizeList - a list of all the shortSideProfile size. This is important to know end index of each profile
   * @param disableProfilesStartBinAndEndBin - list of starting and ending bin of disabled test region
   * @return 
   * @author Lim, Lay Ngor
   * @author Siew Yeng - XCR-3318 - rename function name and added parameter profileSizeList and disableProfilesStartBinAndEndBin
   */
  private Map<Pair<Integer, Integer>, Pair<Float, Float>> disableTestRegionBasedOnAlgorithmSettingsWithoutWrapAround(Subtype subtype,
                                                                                        Map<Pair<Integer, Integer>, Pair<Float, Float>>candidateShortZonesToLengthAndThicknessMap,
                                                                                        List<Integer> profileSizeList,
                                                                                        List<Pair<Integer,Integer>> disableProfilesStartBinAndEndBin,
                                                                                        float minimumShortLengthInMillis,
                                                                                        final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(subtype != null);
    Assert.expect(profileSizeList.isEmpty() == false);

    //calculate end index of each sideProfile
    int totalProfileSize = 0;
    List<Integer> profileEndBin = new ArrayList();
    for(int profileSize : profileSizeList)
    {
      totalProfileSize += profileSize;
      profileEndBin.add(totalProfileSize - 1);
    }

    // create an array to keep track of where we have shorts
    boolean[] candidateShortArray = new boolean[ totalProfileSize ];
    float[] candidateShortDeltaThickness = new float[totalProfileSize];

    // initialize it all to false (no shorts)
    Arrays.fill(candidateShortArray, false);
    Arrays.fill(candidateShortDeltaThickness, 0.f);

    // now go through the candidate short zones and update the short array we just created
    // basically we are converting from a list of zones to a simple array
    for (Pair<Integer, Integer> shortZone : candidateShortZonesToLengthAndThicknessMap.keySet())
    {
      int startBinIndex = shortZone.getFirst();
      int endBinIndex = shortZone.getSecond();
      Pair<Float, Float> shortDataInMillis = candidateShortZonesToLengthAndThicknessMap.get(shortZone);

      Arrays.fill(candidateShortArray, startBinIndex, endBinIndex + 1, true);
      //Fill in the maximum delta thickness for the whole range of this short
      Arrays.fill(candidateShortDeltaThickness, startBinIndex, endBinIndex+1, shortDataInMillis.getSecond());          
    }

    if(disableProfilesStartBinAndEndBin.isEmpty() == false)
    {
      for(Pair<Integer,Integer> startBinAndEndBin : disableProfilesStartBinAndEndBin)
      {
        // "toIndex" is not including in the fill.
        Arrays.fill(candidateShortArray, startBinAndEndBin.getFirst(), startBinAndEndBin.getSecond() + 1, false);
        //Note: if it is disable, we will still keep the short delta thickness data we set above.
      }
    }
    
    // This is the new object to return.
    Map<Pair<Integer, Integer>, Pair<Float, Float>> newCandidateShortZonesToLengthAndThicknessMap =
      new HashMap<Pair<Integer, Integer>, Pair<Float, Float>>();

    //Lim, Lay Ngor : remove all wrap around short defects case at below.
    //Can refer to function disableTestRegionBasedOnAlgorithmSettings for wrap around method but not so applicable for
    //wrap defect for multi center roi.

    // now that we've found the start of any potential short, we can just go through the rest of the profile
    // now convert the candidate short array back into the Map we need to return
    boolean inCandidateShort = false;
    boolean reachEndProfile = false;
    int candidateShortStart = -1;
    int candidateShortEnd = -1;
    for (int i = 0; i < candidateShortArray.length; ++i)
    {
      if ((candidateShortArray[i] == true) && (inCandidateShort == false))
      {
        inCandidateShort = true;
        candidateShortStart = i;
      }

      if(inCandidateShort == true && profileEndBin.contains(i))
      {
        reachEndProfile = true;
      }

      if (reachEndProfile || ((candidateShortArray[i] == false) && (inCandidateShort == true)))
      {
        // we've reach the end of a potential short
        inCandidateShort = false;

        if(reachEndProfile)
        {
          //set end of potential short when the current index is end index of either one of the sideProfile
          candidateShortEnd = i;
          reachEndProfile = false;
        }
        else
        {
          candidateShortEnd = i - 1;
        }

        // add it to our final map
        final int shortLengthInPixels = candidateShortEnd - candidateShortStart + 1;
        final float shortLengthInMillis = shortLengthInPixels * MILIMETER_PER_PIXEL;
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
        //For solder ball detection, we will remain the hard-coded 3 pixel length checking 
        //because we consider the solder ball length is very small.
        if(shortLengthInMillis >= minimumShortLengthInMillis)
        {
          //Get maximum delta thickness within the range again here base on previous result.
          final float finalCandidateShortDeltaThicknessInMillis = StatisticsUtil.maximum(candidateShortDeltaThickness, candidateShortStart, candidateShortEnd);
          newCandidateShortZonesToLengthAndThicknessMap.put(
            new Pair<Integer, Integer>(candidateShortStart, candidateShortEnd), //short range in profile
            new Pair<Float, Float>(shortLengthInMillis, finalCandidateShortDeltaThicknessInMillis)); //short info/data
        }

        candidateShortStart = -1;
        candidateShortEnd = -1;
      }
    }

    return newCandidateShortZonesToLengthAndThicknessMap;
}

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  protected float[] createGrayLevelProfileForLightestImage(Image lightestImage,
                                                           ReconstructionRegion inspectionRegion,
                                                           SliceNameEnum sliceNameEnum,
                                                           JointInspectionData jointInspectionData)
  {
    Assert.expect(lightestImage != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> profileSideMap =
        measureRectangularRegionBackgroundGrayLevelProfiles(lightestImage,
                                                            inspectionRegion,
                                                            sliceNameEnum,
                                                            jointInspectionData);

    float[] headProfile = profileSideMap.get(ShortProfileSideEnum.HEAD_PROFILE).getSecond();
    float[] tailProfile = profileSideMap.get(ShortProfileSideEnum.TAIL_PROFILE).getSecond();
    float[] leftProfile = profileSideMap.get(ShortProfileSideEnum.LEFT_PROFILE).getSecond();
    float[] rightProfile = profileSideMap.get(ShortProfileSideEnum.RIGHT_PROFILE).getSecond();

    float graylevelProfile[] = ArrayUtil.combineArrays(new float[][]
                                               {
                                               headProfile,
                                               rightProfile,
                                               tailProfile,
                                               leftProfile});
    return graylevelProfile;
  }

  /**
   * Measures the four background profiles for a rectangular region (HEAD, TAIL, LEFT, RIGHT).
   * Profiles are measured in terms of gray levels.
   *
   * @author Matt Wharton
   */
  private Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> measureRectangularRegionBackgroundGrayLevelProfiles(
      Image image,
      ReconstructionRegion inspectionRegion,
      SliceNameEnum sliceNameEnum,
      JointInspectionData jointInspectionData)
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the IPD.
    int interPadDistance = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels(MagnificationEnum.getCurrentNorminal());

    // Get the reference ROI. - LNLim: component size
    RegionOfInterest referenceRoi = getReferenceRoi(jointInspectionData);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;
    final int minimumPixelsShortROIRegion = (Integer)subtype.getAlgorithmSettingValue(
            AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_PIXELS_SHORT_ROI_REGION);

    Assert.expect(regionOuterEdgeAsFractionOfInterPadDistance > regionInnerEdgeAsFractionOfInterPadDistance);

    int profileInnerEdgeOffset = (int)Math.ceil(interPadDistance * regionInnerEdgeAsFractionOfInterPadDistance);
    int profileOuterEdgeOffset = (int)Math.ceil(interPadDistance * regionOuterEdgeAsFractionOfInterPadDistance);
    int profileCenterOffset = (int)Math.ceil((float)(profileInnerEdgeOffset + profileOuterEdgeOffset) / 2f);
    int profileWidth = Math.max(minimumPixelsShortROIRegion, profileOuterEdgeOffset - profileInnerEdgeOffset);
    int profileExtension = (profileCenterOffset * 2) + profileWidth;

    int locatedJointMinX = referenceRoi.getMinX();
    int locatedJointMinY = referenceRoi.getMinY();
    int locatedJointMaxX = referenceRoi.getMaxX();
    int locatedJointMaxY = referenceRoi.getMaxY();
    int locatedJointLengthAcross = referenceRoi.getLengthAcross();
    int locatedJointLengthAlong = referenceRoi.getLengthAlong();
    int locatedJointOrientationInDegrees = referenceRoi.getOrientationInDegrees();
    
    //Siew Yeng - XCR-3318 - Oval PTH
    boolean enableTestRegion4Corners = true;
    boolean enableTestRegionInnerCorner = false;
    
    if(jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE))
    {
      //to do: this should apply to all joint type in future
      //starting point of the short region should be the next pixel of the outer most pixel of locatedJoint
      //when we set locatedJointMaxX as getMaxX() & locatedJointMaxY as getMaxY(), 
      //locatedJointMaxX & locatedJointMaxY are still within the locatedJoint region, +1 to get next pixel outside locatedJoint region.
      locatedJointMaxX += 1;
      locatedJointMaxY += 1;
      
      enableTestRegion4Corners = Boolean.parseBoolean((String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_4_CORNERS));
      
      if(enableTestRegion4Corners == false)
        enableTestRegionInnerCorner = Boolean.parseBoolean((String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_TEST_REGION_INNER_CORNER));
    }

    // Head ROI dimensions.
    int headRoiMinX = 0;
    int headRoiMinY = 0;
    int headRoiWidth = 0;
    int headRoiHeight = 0;
    int headRoiOrientation = 0;

    // Tail ROI dimensions.
    int tailRoiMinX = 0;
    int tailRoiMinY = 0;
    int tailRoiWidth = 0;
    int tailRoiHeight = 0;
    int tailRoiOrientation = 0;

    // Left ROI dimensions.
    int leftRoiMinX = 0;
    int leftRoiMinY = 0;
    int leftRoiWidth = 0;
    int leftRoiHeight = 0;
    int leftRoiOrientation = 0;

    // Right ROI dimensions.
    int rightRoiMinX = 0;
    int rightRoiMinY = 0;
    int rightRoiWidth = 0;
    int rightRoiHeight = 0;
    int rightRoiOrientation = 0;
    
    //Siew Yeng - XCR-3318
    // Top Left inner corner ROI dimensions.
    int topLeftInnerCornerRoiMinX = 0;
    int topLeftInnerCornerRoiMinY = 0;
    int topLeftInnerCornerRoiWidth = 0;
    int topLeftInnerCornerRoiHeight = 0;
    int topLeftInnerCornerRoiOrientation = 0;

    // Top Right inner corner ROI dimensions.
    int topRightInnerCornerRoiMinX = 0;
    int topRightInnerCornerRoiMinY = 0;
    int topRightInnerCornerRoiWidth = 0;
    int topRightInnerCornerRoiHeight = 0;
    int topRightInnerCornerRoiOrientation = 0;
    
    // Bottom Left inner corner ROI dimensions.
    int bottomLeftInnerCornerRoiMinX = 0;
    int bottomLeftInnerCornerRoiMinY = 0;
    int bottomLeftInnerCornerRoiWidth = 0;
    int bottomLeftInnerCornerRoiHeight = 0;
    int bottomLeftInnerCornerRoiOrientation = 0;

    // Bottom Right inner corner ROI dimensions.
    int bottomRightInnerCornerRoiMinX = 0;
    int bottomRightInnerCornerRoiMinY = 0;
    int bottomRightInnerCornerRoiWidth = 0;
    int bottomRightInnerCornerRoiHeight = 0;
    int bottomRightInnerCornerRoiOrientation = 0;

    switch (MathUtil.getDegreesWithin0To359(locatedJointOrientationInDegrees))
    {
      case 0:
        // Calculate the Head ROI dimensions.
        headRoiMinX = locatedJointMaxX + profileInnerEdgeOffset;
        headRoiMinY = locatedJointMinY - profileInnerEdgeOffset;
        headRoiWidth = profileWidth;
        headRoiHeight = locatedJointLengthAcross + (profileInnerEdgeOffset * 2);
        headRoiOrientation = 90;

        // Calculate the Tail ROI dimensions.
        tailRoiMinX = locatedJointMinX - profileOuterEdgeOffset;
        tailRoiMinY = locatedJointMinY - profileInnerEdgeOffset;
        tailRoiWidth = profileWidth;
        tailRoiHeight = locatedJointLengthAcross + (profileInnerEdgeOffset * 2);
        tailRoiOrientation = 270;

        // Calculate the Left ROI dimensions.
        leftRoiMinX = (int)Math.ceil(locatedJointMinX - (profileExtension / 2.f));
        leftRoiMinY = locatedJointMaxY + profileInnerEdgeOffset;
        leftRoiWidth = locatedJointLengthAlong + profileExtension;
        leftRoiHeight = profileWidth;
        leftRoiOrientation = 0;

        // Calculate the Right ROI dimensions.
        rightRoiMinX = (int)Math.ceil(locatedJointMinX - (profileExtension / 2.f));
        rightRoiMinY = locatedJointMinY - profileOuterEdgeOffset;
        rightRoiWidth = locatedJointLengthAlong + profileExtension;
        rightRoiHeight = profileWidth;
        rightRoiOrientation = 180;
        
        //Siew Yeng - XCR-3318
        if(enableTestRegionInnerCorner)
        {
          topLeftInnerCornerRoiMinX = leftRoiMinX + profileWidth;
          topLeftInnerCornerRoiMinY = leftRoiMinY - profileWidth;
          topLeftInnerCornerRoiWidth = profileWidth;
          topLeftInnerCornerRoiHeight = profileWidth;
          topLeftInnerCornerRoiOrientation = 0;
          
          bottomLeftInnerCornerRoiMinX = leftRoiMinX + leftRoiWidth - (2* profileWidth);
          bottomLeftInnerCornerRoiMinY = leftRoiMinY - profileWidth;
          bottomLeftInnerCornerRoiWidth = profileWidth;
          bottomLeftInnerCornerRoiHeight = profileWidth;
          bottomLeftInnerCornerRoiOrientation = 0;

          topRightInnerCornerRoiMinX = rightRoiMinX + profileWidth;
          topRightInnerCornerRoiMinY = rightRoiMinY + profileWidth;
          topRightInnerCornerRoiWidth = profileWidth;
          topRightInnerCornerRoiHeight = profileWidth;
          topRightInnerCornerRoiOrientation = 180;
          
          bottomRightInnerCornerRoiMinX = rightRoiMinX + rightRoiWidth - (2*profileWidth);
          bottomRightInnerCornerRoiMinY = rightRoiMinY + profileWidth;
          bottomRightInnerCornerRoiWidth = profileWidth;
          bottomRightInnerCornerRoiHeight = profileWidth;
          bottomRightInnerCornerRoiOrientation = 180;
        }

        break;

      case 90:
        // Calculate the Head ROI dimensions.
        headRoiMinX = locatedJointMinX - profileInnerEdgeOffset;
        headRoiMinY = locatedJointMinY - profileOuterEdgeOffset;
        headRoiWidth = locatedJointLengthAcross + (2 * profileInnerEdgeOffset);
        headRoiHeight = profileWidth;
        headRoiOrientation = 180;

        // Calculate the Tail ROI dimensions.
        tailRoiMinX = locatedJointMinX - profileInnerEdgeOffset;
        tailRoiMinY = locatedJointMaxY + profileInnerEdgeOffset;
        tailRoiWidth = locatedJointLengthAcross + (2 * profileInnerEdgeOffset);
        tailRoiHeight = profileWidth;
        tailRoiOrientation = 0;

        // Calculate the Left ROI dimensions.
        leftRoiMinX = locatedJointMaxX + profileInnerEdgeOffset;
        leftRoiMinY = (int)Math.ceil(locatedJointMinY - (profileExtension / 2.f));
        leftRoiWidth = profileWidth;
        leftRoiHeight = locatedJointLengthAlong + profileExtension;
        leftRoiOrientation = 90;

        // Calculate the Right ROI dimensions.
        rightRoiMinX = locatedJointMinX - profileOuterEdgeOffset;
        rightRoiMinY = (int)Math.ceil(locatedJointMinY - (profileExtension / 2.f));
        rightRoiWidth = profileWidth;
        rightRoiHeight = locatedJointLengthAlong + profileExtension;
        rightRoiOrientation = 270;
        
        //Siew Yeng - XCR-3318
        if(enableTestRegionInnerCorner)
        {
          topLeftInnerCornerRoiMinX = leftRoiMinX - profileWidth;
          topLeftInnerCornerRoiMinY = leftRoiMinY + leftRoiHeight - (2*profileWidth);
          topLeftInnerCornerRoiWidth = profileWidth;
          topLeftInnerCornerRoiHeight = profileWidth;
          topLeftInnerCornerRoiOrientation = 90;
          
          bottomLeftInnerCornerRoiMinX = leftRoiMinX - profileWidth;
          bottomLeftInnerCornerRoiMinY = leftRoiMinY + profileWidth;
          bottomLeftInnerCornerRoiWidth = profileWidth;
          bottomLeftInnerCornerRoiHeight = profileWidth;
          bottomLeftInnerCornerRoiOrientation = 90;

          topRightInnerCornerRoiMinX = rightRoiMinX + profileWidth;
          topRightInnerCornerRoiMinY = rightRoiMinY + rightRoiHeight - (2*profileWidth);
          topRightInnerCornerRoiWidth = profileWidth;
          topRightInnerCornerRoiHeight = profileWidth;
          topRightInnerCornerRoiOrientation = 270;
          
          bottomRightInnerCornerRoiMinX = rightRoiMinX + profileWidth;
          bottomRightInnerCornerRoiMinY = rightRoiMinY + profileWidth;
          bottomRightInnerCornerRoiWidth = profileWidth;
          bottomRightInnerCornerRoiHeight = profileWidth;
          bottomRightInnerCornerRoiOrientation = 270;
        }

        break;

      case 180:
        // Calculate the Head ROI dimensions.
        headRoiMinX = locatedJointMinX - profileOuterEdgeOffset;
        headRoiMinY = locatedJointMinY - profileInnerEdgeOffset;
        headRoiWidth = profileWidth;
        headRoiHeight = locatedJointLengthAcross + (2 * profileInnerEdgeOffset);
        headRoiOrientation = 270;

        // Calculate the Tail ROI dimensions.
        tailRoiMinX = locatedJointMaxX + profileInnerEdgeOffset;
        tailRoiMinY = locatedJointMinY - profileInnerEdgeOffset;
        tailRoiWidth = profileWidth;
        tailRoiHeight = locatedJointLengthAcross + (2 * profileInnerEdgeOffset);
        tailRoiOrientation = 90;

        // Calculate the Left ROI dimensions.
        leftRoiMinX = (int)Math.ceil(locatedJointMinX - (profileExtension / 2.f));
        leftRoiMinY = locatedJointMinY - profileOuterEdgeOffset;
        leftRoiWidth = locatedJointLengthAlong + profileExtension;
        leftRoiHeight = profileWidth;
        leftRoiOrientation = 180;

        // Calculate the Right ROI dimensions.
        rightRoiMinX = (int)Math.ceil(locatedJointMinX - (profileExtension / 2.f));
        rightRoiMinY = locatedJointMaxY + profileInnerEdgeOffset;
        rightRoiWidth = locatedJointLengthAlong + profileExtension;
        rightRoiHeight = profileWidth;
        rightRoiOrientation = 0;
        
        //Siew Yeng - XCR-3318
        if(enableTestRegionInnerCorner)
        {
          topLeftInnerCornerRoiMinX = leftRoiMinX + leftRoiWidth - (2* profileWidth);
          topLeftInnerCornerRoiMinY = leftRoiMinY + profileWidth;
          topLeftInnerCornerRoiWidth = profileWidth;
          topLeftInnerCornerRoiHeight = profileWidth;
          topLeftInnerCornerRoiOrientation = 180;
          
          bottomLeftInnerCornerRoiMinX = leftRoiMinX + profileWidth;
          bottomLeftInnerCornerRoiMinY = leftRoiMinY + profileWidth;
          bottomLeftInnerCornerRoiWidth = profileWidth;
          bottomLeftInnerCornerRoiHeight = profileWidth;
          bottomLeftInnerCornerRoiOrientation = 180;

          topRightInnerCornerRoiMinX = rightRoiMinX + rightRoiWidth - (2*profileWidth);
          topRightInnerCornerRoiMinY = rightRoiMinY - profileWidth;
          topRightInnerCornerRoiWidth = profileWidth;
          topRightInnerCornerRoiHeight = profileWidth;
          topRightInnerCornerRoiOrientation = 0;
          
          bottomRightInnerCornerRoiMinX = rightRoiMinX + profileWidth;
          bottomRightInnerCornerRoiMinY = rightRoiMinY - profileWidth;
          bottomRightInnerCornerRoiWidth = profileWidth;
          bottomRightInnerCornerRoiHeight = profileWidth;
          bottomRightInnerCornerRoiOrientation = 0;
        }

        break;

      case 270:
        // Calculate the Head ROI dimensions.
        headRoiMinX = locatedJointMinX - profileInnerEdgeOffset;
        headRoiMinY = locatedJointMaxY + profileInnerEdgeOffset;
        headRoiWidth = locatedJointLengthAcross + (2 * profileInnerEdgeOffset);
        headRoiHeight = profileWidth;
        headRoiOrientation = 0;

        // Calculate the Tail ROI dimensions.
        tailRoiMinX = locatedJointMinX - profileInnerEdgeOffset;
        tailRoiMinY = locatedJointMinY - profileOuterEdgeOffset;
        tailRoiWidth = locatedJointLengthAcross + (2* profileInnerEdgeOffset);
        tailRoiHeight = profileWidth;
        tailRoiOrientation = 180;

        // Calculate the Left ROI dimensions.
        leftRoiMinX = locatedJointMinX - profileOuterEdgeOffset;
        leftRoiMinY = (int)Math.ceil(locatedJointMinY - (profileExtension / 2.f));
        leftRoiWidth = profileWidth;
        leftRoiHeight = locatedJointLengthAlong + profileExtension;
        leftRoiOrientation = 270;

        // Calculate the Right ROI dimensions.
        rightRoiMinX = locatedJointMaxX + profileInnerEdgeOffset;
        rightRoiMinY = (int)Math.ceil(locatedJointMinY - (profileExtension / 2.f));
        rightRoiWidth = profileWidth;
        rightRoiHeight = locatedJointLengthAlong + profileExtension;
        rightRoiOrientation = 90;

        //Siew Yeng - XCR-3318
        if(enableTestRegionInnerCorner)
        {
          topLeftInnerCornerRoiMinX = leftRoiMinX + profileWidth;
          topLeftInnerCornerRoiMinY = leftRoiMinY + profileWidth;
          topLeftInnerCornerRoiWidth = profileWidth;
          topLeftInnerCornerRoiHeight = profileWidth;
          topLeftInnerCornerRoiOrientation = 270;
          
          bottomLeftInnerCornerRoiMinX = leftRoiMinX + profileWidth;
          bottomLeftInnerCornerRoiMinY = leftRoiMinY + leftRoiHeight - (2*profileWidth);
          bottomLeftInnerCornerRoiWidth = profileWidth;
          bottomLeftInnerCornerRoiHeight = profileWidth;
          bottomLeftInnerCornerRoiOrientation = 270;

          topRightInnerCornerRoiMinX = rightRoiMinX - profileWidth;
          topRightInnerCornerRoiMinY = rightRoiMinY + profileWidth;
          topRightInnerCornerRoiWidth = profileWidth;
          topRightInnerCornerRoiHeight = profileWidth;
          topRightInnerCornerRoiOrientation = 90;
          
          bottomRightInnerCornerRoiMinX = rightRoiMinX - profileWidth;
          bottomRightInnerCornerRoiMinY = rightRoiMinY + rightRoiHeight - (2*profileWidth);
          bottomRightInnerCornerRoiWidth = profileWidth;
          bottomRightInnerCornerRoiHeight = profileWidth;
          bottomRightInnerCornerRoiOrientation = 90;
        }
        
        break;

      default:
        Assert.expect(false, "Encountered non-cardinal joint orientation");
        break;
    }

    // Create the profile regions.
    RegionOfInterest headRoi = new RegionOfInterest(
        headRoiMinX,
        headRoiMinY,
        headRoiWidth,
        headRoiHeight,
        headRoiOrientation,
        RegionShapeEnum.RECTANGULAR);
    RegionOfInterest tailRoi = new RegionOfInterest(
        tailRoiMinX,
        tailRoiMinY,
        tailRoiWidth,
        tailRoiHeight,
        tailRoiOrientation,
        RegionShapeEnum.RECTANGULAR);
    RegionOfInterest leftRoi = new RegionOfInterest(
        leftRoiMinX,
        leftRoiMinY,
        leftRoiWidth,
        leftRoiHeight,
        leftRoiOrientation,
        RegionShapeEnum.RECTANGULAR);
    RegionOfInterest rightRoi = new RegionOfInterest(
        rightRoiMinX,
        rightRoiMinY,
        rightRoiWidth,
        rightRoiHeight,
        rightRoiOrientation,
        RegionShapeEnum.RECTANGULAR);
    
    //Siew Yeng - XCR-3318
    if(enableTestRegion4Corners == false)
    {
      //remove 4 corners
      if(locatedJointOrientationInDegrees == 90 || locatedJointOrientationInDegrees == 270)
      {
        leftRoi.setHeightKeepingSameCenter(leftRoi.getHeight() -(profileWidth*2));
        rightRoi.setHeightKeepingSameCenter(rightRoi.getHeight() -(profileWidth*2));
      }
      else
      {
        leftRoi.setWidthKeepingSameCenter(leftRoi.getWidth()-(profileWidth*2));
        rightRoi.setWidthKeepingSameCenter(rightRoi.getWidth()-(profileWidth*2));
      }
    }

    if (_MDW_DEBUG)
    {
      // DEBUG - MDW - plot the side regions.
      GeneralPath sideRegionShapes = new GeneralPath();
      sideRegionShapes.append(headRoi.getShape(), false);
      sideRegionShapes.append(tailRoi.getShape(), false);
      sideRegionShapes.append(leftRoi.getShape(), false);
      sideRegionShapes.append(rightRoi.getShape(), false);
      MeasurementRegionDiagnosticInfo sideRegionsDiagInfo = new MeasurementRegionDiagnosticInfo(sideRegionShapes,
                                                                                                MeasurementRegionEnum.
                                                                                                SHORT_BACKGROUND_REGION);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   this, false,
                                   sideRegionsDiagInfo);
    }

    // Ensure our ROIs are constrained to the image boundaries.
    if (AlgorithmUtil.checkRegionBoundaries(headRoi, image, inspectionRegion, "Head Short Background ROI") == false)
    {
      // Shift the ROI back into the image.
      // trancate it before process - Wei Chin Fixed for large view
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, headRoi);
    }
    if (AlgorithmUtil.checkRegionBoundaries(tailRoi, image, inspectionRegion, "Tail Short Background ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, tailRoi);
    }
    if (AlgorithmUtil.checkRegionBoundaries(leftRoi, image, inspectionRegion, "Left Short Background ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, leftRoi);
    }
    if (AlgorithmUtil.checkRegionBoundaries(rightRoi, image, inspectionRegion, "Right Short Background ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, rightRoi);
    }

    // Measure the profiles.
    float[] headProfile = ImageFeatureExtraction.profile(image, headRoi);
    float[] tailProfile = ImageFeatureExtraction.profile(image, tailRoi);
    float[] leftProfile = ImageFeatureExtraction.profile(image, leftRoi);
    float[] rightProfile = ImageFeatureExtraction.profile(image, rightRoi);

    // Sanity check to make sure we got non-null profiles.
    Assert.expect(headProfile != null, "Head profile is null!");
    Assert.expect(tailProfile != null, "Tail profile is null!");
    Assert.expect(leftProfile != null, "Left profile is null!");
    Assert.expect(rightProfile != null, "Right profile is null!");

    // Create the map.
    Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> profileSideMap =
        new HashMap<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>>();
    profileSideMap.put(ShortProfileSideEnum.HEAD_PROFILE,
                       new Pair<RegionOfInterest, float[]>(headRoi, headProfile));
    profileSideMap.put(ShortProfileSideEnum.TAIL_PROFILE,
                       new Pair<RegionOfInterest, float[]>(tailRoi, tailProfile));
    profileSideMap.put(ShortProfileSideEnum.LEFT_PROFILE,
                       new Pair<RegionOfInterest, float[]>(leftRoi, leftProfile));
    profileSideMap.put(ShortProfileSideEnum.RIGHT_PROFILE,
                       new Pair<RegionOfInterest, float[]>(rightRoi, rightProfile));
    
    //Siew Yeng - XCR-3318
    if(enableTestRegionInnerCorner)
    {
      RegionOfInterest topLeftInnerCornerRoi = new RegionOfInterest(
          topLeftInnerCornerRoiMinX,
          topLeftInnerCornerRoiMinY,
          topLeftInnerCornerRoiWidth,
          topLeftInnerCornerRoiHeight,
          topLeftInnerCornerRoiOrientation,
          RegionShapeEnum.RECTANGULAR);
      RegionOfInterest topRightInnerCornerRoi = new RegionOfInterest(
          topRightInnerCornerRoiMinX,
          topRightInnerCornerRoiMinY,
          topRightInnerCornerRoiWidth,
          topRightInnerCornerRoiHeight,
          topRightInnerCornerRoiOrientation,
          RegionShapeEnum.RECTANGULAR);
      RegionOfInterest bottomLeftInnerCornerRoi = new RegionOfInterest(
          bottomLeftInnerCornerRoiMinX,
          bottomLeftInnerCornerRoiMinY,
          bottomLeftInnerCornerRoiWidth,
          bottomLeftInnerCornerRoiHeight,
          bottomLeftInnerCornerRoiOrientation,
          RegionShapeEnum.RECTANGULAR);
      RegionOfInterest bottomRightInnerCornerRoi = new RegionOfInterest(
          bottomRightInnerCornerRoiMinX,
          bottomRightInnerCornerRoiMinY,
          bottomRightInnerCornerRoiWidth,
          bottomRightInnerCornerRoiHeight,
          bottomRightInnerCornerRoiOrientation,
          RegionShapeEnum.RECTANGULAR);

      // Ensure our ROIs are constrained to the image boundaries.
      if (AlgorithmUtil.checkRegionBoundaries(topLeftInnerCornerRoi, image, inspectionRegion, "Top Left Inner Corner Short Background ROI") == false)
      {
        // Shift the ROI back into the image.
        // trancate it before process - Wei Chin Fixed for large view
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, topLeftInnerCornerRoi);
      }
      if (AlgorithmUtil.checkRegionBoundaries(topRightInnerCornerRoi, image, inspectionRegion, "Top Right Inner Corner Short Background ROI") == false)
      {
        // Shift the ROI back into the image.
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, topRightInnerCornerRoi);
      }
      if (AlgorithmUtil.checkRegionBoundaries(bottomLeftInnerCornerRoi, image, inspectionRegion, "Bottom Left Inner Corner Short Background ROI") == false)
      {
        // Shift the ROI back into the image.
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, bottomLeftInnerCornerRoi);
      }
      if (AlgorithmUtil.checkRegionBoundaries(bottomRightInnerCornerRoi, image, inspectionRegion, "Bottom Right Inner Corner Short Background ROI") == false)
      {
        // Shift the ROI back into the image.
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, bottomRightInnerCornerRoi);
      }

      // Measure the profiles.
      float[] topLeftInnerCornerProfile = ImageFeatureExtraction.profile(image, topLeftInnerCornerRoi);
      float[] topRightInnerCornerProfile = ImageFeatureExtraction.profile(image, topRightInnerCornerRoi);
      float[] bottomLeftInnerCornerProfile = ImageFeatureExtraction.profile(image, bottomLeftInnerCornerRoi);
      float[] bottomRightInnerCornerProfile = ImageFeatureExtraction.profile(image, bottomRightInnerCornerRoi);

      profileSideMap.put(ShortProfileSideEnum.TOP_LEFT_INNER_CORNER_PROFILE,
                         new Pair<RegionOfInterest, float[]>(topLeftInnerCornerRoi, topLeftInnerCornerProfile));
      profileSideMap.put(ShortProfileSideEnum.TOP_RIGHT_INNER_CORNER_PROFILE,
                         new Pair<RegionOfInterest, float[]>(topRightInnerCornerRoi, topRightInnerCornerProfile));
      profileSideMap.put(ShortProfileSideEnum.BOTTOM_LEFT_INNER_CORNER_PROFILE,
                         new Pair<RegionOfInterest, float[]>(bottomLeftInnerCornerRoi, bottomLeftInnerCornerProfile));
      profileSideMap.put(ShortProfileSideEnum.BOTTOM_RIGHT_INNER_CORNER_PROFILE,
                         new Pair<RegionOfInterest, float[]>(bottomRightInnerCornerRoi, bottomRightInnerCornerProfile));
    }

	//Lim, Lay Ngor - XCR1743:Benchmark - START
    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
//      System.out.println("measureRectangularRegionBackgroundGrayLevelProfiles: start");
      _solderBallInspectionTime.start();
    }
    
    //Only component with 2 pads are allow for Center ROI SolderBall detection
    if (allowCenterSolderBallShortDetection( jointInspectionData.getJointTypeEnum()) == true)
    {
      int centerInterPadDistance = interPadDistance;
      Assert.expect(centerInterPadDistance>0);
	  //Change the assert to code below if interPadDistance always < 0.
//     if (interPadDistance <= 0)
//     {
//       System.out.println("jointInspectionData" + jointInspectionData.getJointTypeEnum());
//       centerInterPadDistance = 1;
//     }

      final float centerEffectiveLengthPercentage =
        (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_LENGTH) / 100.f;
      final int centerEffectiveLength = (int) Math.ceil(centerInterPadDistance * centerEffectiveLengthPercentage);
      final float centerEffectiveWidthPercentage =
        (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_CENTER_EFFECTIVE_WIDTH) / 100.f;
      final int centerEffectiveWidth = (int) Math.ceil(locatedJointLengthAcross * centerEffectiveWidthPercentage);
      final int locatedJointCenterX = referenceRoi.getCenterX();
      final int locatedJointCenterY = referenceRoi.getCenterY();
     
      String enableMultiRoiCenterRegionString  = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_MULTI_ROI_CENTER_REGION);
      boolean enableMultiRoiCenterRegion = enableMultiRoiCenterRegionString.equals(_TRUE);
      
      RegionOfInterest[] multiCenterRoiList = getMultiCenterRoiList(centerEffectiveWidth,
                                                                                                  centerEffectiveLength,
                                                                                                  locatedJointCenterX,
                                                                                                  locatedJointCenterY,
                                                                                                  enableMultiRoiCenterRegion,
                                                                                                  centerInterPadDistance,
                                                                                                  locatedJointLengthAcross,
                                                                                                  locatedJointOrientationInDegrees);
      
        if (_MDW_DEBUG)
        {
          // DEBUG - MDW - plot the center region only.
          GeneralPath centerRegionShapes = new GeneralPath();
          for (int i = 0; i < multiCenterRoiList.length; ++i)
          {
            centerRegionShapes.append(multiCenterRoiList[i].getShape(), false);
          }

          MeasurementRegionDiagnosticInfo centerRegionsDiagInfo = new MeasurementRegionDiagnosticInfo(centerRegionShapes,
                                                                                                    MeasurementRegionEnum.
                                                                                                    SHORT_BACKGROUND_REGION);
          _diagnostics.postDiagnostics(inspectionRegion,
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this, false,
                                       centerRegionsDiagInfo);
        }      
       
      int id = ShortProfileSideEnum.CENTER_PROFILE_1.getId();
      for (int i = 0; i < multiCenterRoiList.length; ++i)
      {
        if (AlgorithmUtil.checkRegionBoundaries(multiCenterRoiList[i], image, inspectionRegion, "Center Short Background ROI") == false)
        {
          // Shift the ROI back into the image.
          AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, multiCenterRoiList[i]);
        }
        //reprint for verification
//        System.out.println("centerRoiList[" + i + "]:" + multiCenterRoiList[i]);

        float[] singleSectionCenterProfile = ImageFeatureExtraction.profile(image, multiCenterRoiList[i]);
        Assert.expect(singleSectionCenterProfile != null, "One of the multiple ROI center profile is null!");
      
        //Shall we combine the profile or separately send to outside & let outside combine???
        //This is to append all multiple roi profile to one single original short profile.
        //start from CENTER_PROFILE_1 and keep add until finish all sepate roi finish
        profileSideMap.put(ShortProfileSideEnum.getEnum(id),
          new Pair<RegionOfInterest, float[]>(multiCenterRoiList[i], singleSectionCenterProfile));     
        ++id;
      }
    }
    
    if(_SOLDER_BALL_INSPECTION_TIME_STAMP)
    {
      _solderBallInspectionTime.stop();    
//      System.out.println("measureRectangularRegionBackgroundGrayLevelProfiles: stop");
//      System.out.println("Solderball ElapseTime in function measureRectangularRegionBackgroundGrayLevelProfiles: " + _solderBallInspectionTime.getElapsedTimeInMillis());
    }
	//Lim, Lay Ngor - XCR1743:Benchmark - END

    return profileSideMap;
  }

  /**
   *  Solder Ball Detection on Multiple ROI on Center Region between two pads.
   *  Only component with 2 pads are allow for Center ROI SolderBall detection.
   * @author Lim, Lay Ngor - XCR1743:Benchmark
   */
  RegionOfInterest[] getMultiCenterRoiList(
      final int centerEffectiveWidth,
      final int centerEffectiveLength,
      final int locatedJointCenterX,
      final int locatedJointCenterY,
      final boolean enableMultiRoiCenterRegion,
      final int centerInterPadDistance, 
      final int locatedJointLengthAcross, 
      final int locatedJointOrientationInDegrees
    )
  {
    Assert.expect(centerInterPadDistance > 0);
    
    // Center ROI dimensions.
    int centerRoiMinX = 0;
    int centerRoiMinY = 0;
    int centerRoiWidth = 0;
    int centerRoiHeight = 0;
    int centerRoiOrientation = 0;

    int numberOfCenterProfileSegment = 1;
    if (enableMultiRoiCenterRegion)
    {
      //number of array is determine by interpad size
      if (_NUMBER_OF_PIXEL_PER_SEGMENT < centerEffectiveLength)
        numberOfCenterProfileSegment = (int) Math.ceil(centerEffectiveLength / _NUMBER_OF_PIXEL_PER_SEGMENT);
      //if _NUMBER_OF_PIXEL_PER_SEGMENT > centerEffectiveLength, only one segment needed
    }
    
    final int sizePerSegment = (int) Math.ceil(centerEffectiveLength / numberOfCenterProfileSegment);
    RegionOfInterest[] multiCenterRoiList = new RegionOfInterest[numberOfCenterProfileSegment];

    switch (MathUtil.getDegreesWithin0To359(locatedJointOrientationInDegrees))
    {
      case 0:
      {
        // Calculate the Center ROI dimensions.
        centerRoiMinX = (int) Math.ceil(locatedJointCenterX - centerEffectiveLength / 2.f);
        centerRoiMinY = (int) Math.ceil(locatedJointCenterY - centerEffectiveWidth / 2.f);//locatedJointMinY;
        centerRoiWidth = centerEffectiveLength;//interPadDistance;
        centerRoiHeight = centerEffectiveWidth;//locatedJointLengthAcross;
        centerRoiOrientation = 90;
        multiCenterRoiList = getMultiCenterRoi(numberOfCenterProfileSegment, sizePerSegment, centerRoiMinX, centerRoiMinY,
          centerRoiWidth, centerRoiHeight, centerRoiOrientation, 0);
//        System.out.println("case 0");
        break;
      }
      case 90:
      {
        centerRoiMinX = (int) Math.ceil(locatedJointCenterX - centerEffectiveWidth / 2.f);//locatedJointMinX;
        centerRoiMinY = (int) Math.ceil(locatedJointCenterY - centerEffectiveLength / 2.f);
        centerRoiWidth = centerEffectiveWidth;//locatedJointLengthAcross;
        centerRoiHeight = centerEffectiveLength;//interPadDistance;
        centerRoiOrientation = 180;
        multiCenterRoiList = getMultiCenterRoi(numberOfCenterProfileSegment, sizePerSegment, centerRoiMinX, centerRoiMinY,
          centerRoiWidth, centerRoiHeight, centerRoiOrientation, 90);
//        System.out.println("case 90");
        break;
      }
      case 180:
      {
        centerRoiMinX = (int) Math.ceil(locatedJointCenterX - centerEffectiveLength / 2.f);
        centerRoiMinY = (int) Math.ceil(locatedJointCenterY - centerEffectiveWidth / 2.f);//locatedJointMinY;
        centerRoiWidth = centerEffectiveLength;//interPadDistance;
        centerRoiHeight = centerEffectiveWidth;//locatedJointLengthAcross;
        centerRoiOrientation = 270;
        multiCenterRoiList = getMultiCenterRoi(numberOfCenterProfileSegment, sizePerSegment, centerRoiMinX, centerRoiMinY,
          centerRoiWidth, centerRoiHeight, centerRoiOrientation, 180);
//        System.out.println("case 180");
        break;
      }
      case 270:
      {
        centerRoiMinX = (int) Math.ceil(locatedJointCenterX - centerEffectiveWidth / 2.f);//locatedJointMinX;
        centerRoiMinY = (int) Math.ceil(locatedJointCenterY - centerEffectiveLength / 2.f);
        centerRoiWidth = centerEffectiveWidth;//locatedJointLengthAcross;
        centerRoiHeight = centerEffectiveLength;//interPadDistance;
        centerRoiOrientation = 0;
        multiCenterRoiList = getMultiCenterRoi(numberOfCenterProfileSegment, sizePerSegment, centerRoiMinX, centerRoiMinY,
          centerRoiWidth, centerRoiHeight, centerRoiOrientation, 270);
//        System.out.println("case 270");
        break;
      }
      default:
        Assert.expect(false, "Encountered non-cardinal joint orientation");
        break;
    }

    //just for double confirm and debugging purpose
    //can delete after multi roi stable
//    RegionOfInterest overallCenterRoi = new RegionOfInterest(centerRoiMinX, centerRoiMinY,
//      centerRoiWidth, centerRoiHeight, centerRoiOrientation, RegionShapeEnum.RECTANGULAR);
//    Assert.expect(overallCenterRoi != null);
//    System.out.println("LNLim - Benchmark: centerRoi2:" + overallCenterRoi);
    //end just for debugging purpose

    return multiCenterRoiList;
  }
  
  /**
   * Separate the center ROI to multiple section.
   * @author Lim, Lay Ngor - XCR1743:Benchmark
   */
  RegionOfInterest [] getMultiCenterRoi(int numberOfCenterProfileSegment, int sizePerSegment
    , int centerRoiMinX, int centerRoiMinY, int centerRoiWidth, int centerRoiHeight
    , int centerRoiOrientation, int locatedJointOrientation)
  {
    RegionOfInterest[] centerRoiList = new RegionOfInterest[numberOfCenterProfileSegment];
    int totalSizeBeforeLastSegment = 0;
    if(locatedJointOrientation == 0 || locatedJointOrientation == 180)
    {
      for (int i = 0; i < numberOfCenterProfileSegment; ++i)
      {
        int newWidth = 0;
        int newMinX = 0;
        if (i != numberOfCenterProfileSegment - 1)
        {
          newWidth = sizePerSegment;
          if (i == 0)
            newMinX = centerRoiMinX;
          else
            newMinX = centerRoiMinX + totalSizeBeforeLastSegment;

          totalSizeBeforeLastSegment += sizePerSegment;
        }
        else
        {
          //Last Segment
          newWidth = centerRoiWidth - totalSizeBeforeLastSegment;
          newMinX = centerRoiMinX + totalSizeBeforeLastSegment;
        }

        centerRoiList[i] = new RegionOfInterest(newMinX, centerRoiMinY, newWidth, centerRoiHeight, centerRoiOrientation, RegionShapeEnum.RECTANGULAR);      
        Assert.expect(centerRoiList[i] != null);
      }
    }
    else if(locatedJointOrientation == 90 || locatedJointOrientation == 270)
    {
      for (int i = 0; i < numberOfCenterProfileSegment; ++i)
      {
        int newHeight = 0;
        int newMinY = 0;
        if (i != numberOfCenterProfileSegment - 1)
        {
          newHeight = sizePerSegment;
          if (i == 0)
            newMinY = centerRoiMinY;
          else
            newMinY = centerRoiMinY + totalSizeBeforeLastSegment;

          totalSizeBeforeLastSegment += sizePerSegment;
        }
        else
        {
          //Last Segment
          newHeight = centerRoiHeight - totalSizeBeforeLastSegment;
          newMinY = centerRoiMinY + totalSizeBeforeLastSegment;
        }

        centerRoiList[i] = new RegionOfInterest(centerRoiMinX, newMinY, centerRoiWidth, newHeight, centerRoiOrientation, RegionShapeEnum.RECTANGULAR);      
        Assert.expect(centerRoiList[i] != null);
      }      
      //System.out.println("LN Debug: centerRoiList[" + i + "]:" + centerRoiList[i]);
    }

    return centerRoiList;
  }

  /**
   * Code refer from function getShapeForShortDefectInCombinedRectangularRegion.
   * @author Lim, Lay Ngor - XCR1743:Benchmark
   */
  private java.awt.Shape getShapeForShortDefectInCenterRectangularRegion(
      float[] centerDeltaThicknessProfile,
      Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> centerProfileToRoiAndProfileMap,
      int defectStartBin,
      int defectEndBin)
  {
    Assert.expect(centerDeltaThicknessProfile != null);
    Assert.expect(centerProfileToRoiAndProfileMap != null);
    Assert.expect(defectStartBin >= 0);
    Assert.expect(defectStartBin < centerDeltaThicknessProfile.length);
    Assert.expect(defectEndBin >= 0);
    Assert.expect(defectEndBin < centerDeltaThicknessProfile.length);

    // Next, let's figure out which section in center ROI the start and end bins belong to (and the offsets into those section bins).
    Pair<ShortProfileSideEnum, Integer> startBinProfileSideEnumAndOffset = getShortProfileSideAndOffsetForBinInCenterRectRegion(
      centerDeltaThicknessProfile,
      centerProfileToRoiAndProfileMap,
      defectStartBin);
    ShortProfileSideEnum startBinProfileSideEnum = startBinProfileSideEnumAndOffset.getFirst();
    int startBinOffsetIntoSideRoi = startBinProfileSideEnumAndOffset.getSecond();
    Pair<ShortProfileSideEnum, Integer> endBinProfileSideEnumAndOffset = getShortProfileSideAndOffsetForBinInCenterRectRegion(
      centerDeltaThicknessProfile,
      centerProfileToRoiAndProfileMap,
      defectEndBin);
    ShortProfileSideEnum endBinProfileSideEnum = endBinProfileSideEnumAndOffset.getFirst();
    int endBinOffsetIntoSideRoi = endBinProfileSideEnumAndOffset.getSecond();

    // Now create line segments for the start and end bins.
    RegionOfInterest startBinSideProfileRoi = centerProfileToRoiAndProfileMap.get(startBinProfileSideEnum).getFirst();
    Line2D startBinLineSegment = getLineSegmentForBinInCombinedRectangularRegion(startBinSideProfileRoi,
                                                                                 startBinOffsetIntoSideRoi,
                                                                                 false);
    RegionOfInterest endBinSideProfileRoi = centerProfileToRoiAndProfileMap.get(endBinProfileSideEnum).getFirst();
    Line2D endBinLineSegment = getLineSegmentForBinInCombinedRectangularRegion(endBinSideProfileRoi,
                                                                               endBinOffsetIntoSideRoi,
                                                                               true);

    // Now we need to create a shape for the defect region...this should be interesting...
    GeneralPath defectRegionShape = new GeneralPath();
    // Add the line segment for the start bin.
    defectRegionShape.append(startBinLineSegment, false);
    // Add the line segment for the end bin.
    defectRegionShape.append(endBinLineSegment, false);
    // Move to the outside point on the start bin line segment.
    defectRegionShape.moveTo((float)startBinLineSegment.getX2(), (float)startBinLineSegment.getY2());
    // Determine if we have a region that "wraps around" multiple regions.
    boolean regionWrapsAround = true;
    if (startBinProfileSideEnum.equals(endBinProfileSideEnum))
    {
      // The start and end bins are on the same side.  This probably means that the short is contained on one side,
      // but we need to make sure we don't have some huge region that wraps around most of the joint.
      switch (MathUtil.getDegreesWithin0To359(startBinSideProfileRoi.getOrientationInDegrees()))
      {
        case 0:
          if (startBinLineSegment.getX1() < endBinLineSegment.getX1())
            regionWrapsAround = false;
       
          break;

        case 90:
          if (startBinLineSegment.getY1() > endBinLineSegment.getY1())
            regionWrapsAround = false;

          break;

        case 180:
          if (startBinLineSegment.getX1() > endBinLineSegment.getX1())
            regionWrapsAround = false;

          break;

        case 270:
          if (startBinLineSegment.getY1() < endBinLineSegment.getY1())
            regionWrapsAround = false;

          break;

        default:
          // Shouldn't occur.
          Assert.expect(false, "Encountered non-cardinal orientation!");

          break;
      }
    }

    // If the region doesn't wrap around, we're just going to create a simple rectangle.
    if (regionWrapsAround == false)
    {
      defectRegionShape.lineTo((float)endBinLineSegment.getX2(), (float)endBinLineSegment.getY2());
      defectRegionShape.moveTo((float)endBinLineSegment.getX1(), (float)endBinLineSegment.getY1());
      defectRegionShape.lineTo((float)startBinLineSegment.getX1(), (float)startBinLineSegment.getY1());
    }
    else
    {
      //We expect the start and end bin is in the same section of center ROI.
      //And we also expect no regionWrapsAround=true case here.      
      System.out.println("Should not have wrap around case for center defects.");
    }
    //Lim, Lay Ngor : skip wrap around case for center short defects.
    //Can refer to function getShapeForShortDefectInCombinedRectangularRegion for wrap around method but not so applicable for
    //wrap defect for multi center roi.

    return defectRegionShape;
  }

  /**
   * @author Matt Wharton
   * @edited Siew Yeng - always +1 when call ROI getMaxX() & getMaxY() to cover the whole region.
   */
  private java.awt.Shape getShapeForShortDefectInCombinedRectangularRegion(
      float[] combinedDeltaThicknessProfile,
      Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> sideProfileToRoiAndProfileMap,
      int defectStartBin,
      int defectEndBin)
  {
    Assert.expect(combinedDeltaThicknessProfile != null);
    Assert.expect(sideProfileToRoiAndProfileMap != null);
    Assert.expect(defectStartBin >= 0);
    Assert.expect(defectStartBin < combinedDeltaThicknessProfile.length);
    Assert.expect(defectEndBin >= 0);
    Assert.expect(defectEndBin < combinedDeltaThicknessProfile.length);

    // Next, let's figure out what side ROIs the start and end bins belong to (and the offsets into those side bins).
    Pair<ShortProfileSideEnum, Integer> startBinProfileSideEnumAndOffset = getShortProfileSideAndOffsetForBinInCombinedRectRegion(
      combinedDeltaThicknessProfile,
      sideProfileToRoiAndProfileMap,
      defectStartBin);
    ShortProfileSideEnum startBinProfileSideEnum = startBinProfileSideEnumAndOffset.getFirst();
    int startBinOffsetIntoSideRoi = startBinProfileSideEnumAndOffset.getSecond();
    Pair<ShortProfileSideEnum, Integer> endBinProfileSideEnumAndOffset = getShortProfileSideAndOffsetForBinInCombinedRectRegion(
      combinedDeltaThicknessProfile,
      sideProfileToRoiAndProfileMap,
      defectEndBin);
    ShortProfileSideEnum endBinProfileSideEnum = endBinProfileSideEnumAndOffset.getFirst();
    int endBinOffsetIntoSideRoi = endBinProfileSideEnumAndOffset.getSecond();

    // Now create line segments for the start and end bins.
    RegionOfInterest startBinSideProfileRoi = sideProfileToRoiAndProfileMap.get(startBinProfileSideEnum).getFirst();
    Line2D startBinLineSegment = getLineSegmentForBinInCombinedRectangularRegion(startBinSideProfileRoi,
                                                                                 startBinOffsetIntoSideRoi,
                                                                                 false);
    RegionOfInterest endBinSideProfileRoi = sideProfileToRoiAndProfileMap.get(endBinProfileSideEnum).getFirst();
    Line2D endBinLineSegment = getLineSegmentForBinInCombinedRectangularRegion(endBinSideProfileRoi,
                                                                               endBinOffsetIntoSideRoi,
                                                                               true);

    // Now we need to create a shape for the defect region...this should be interesting...
    GeneralPath defectRegionShape = new GeneralPath();
    // Add the line segment for the start bin.
    defectRegionShape.append(startBinLineSegment, false);
    // Add the line segment for the end bin.
    defectRegionShape.append(endBinLineSegment, false);
    // Move to the outside point on the start bin line segment.
    defectRegionShape.moveTo((float)startBinLineSegment.getX2(), (float)startBinLineSegment.getY2());
    // Determine if we have a region that "wraps around" multiple regions.
    boolean regionWrapsAround = true;
    if (startBinProfileSideEnum.equals(endBinProfileSideEnum))
    {
      // The start and end bins are on the same side.  This probably means that the short is contained on one side,
      // but we need to make sure we don't have some huge region that wraps around most of the joint.
      switch (MathUtil.getDegreesWithin0To359(startBinSideProfileRoi.getOrientationInDegrees()))
      {
        case 0:
          if (startBinLineSegment.getX1() < endBinLineSegment.getX1())
          {
            regionWrapsAround = false;
          }

          break;

        case 90:
          if (startBinLineSegment.getY1() > endBinLineSegment.getY1())
          {
            regionWrapsAround = false;
          }

          break;

        case 180:
          if (startBinLineSegment.getX1() > endBinLineSegment.getX1())
          {
            regionWrapsAround = false;
          }

          break;

        case 270:
          if (startBinLineSegment.getY1() < endBinLineSegment.getY1())
          {
            regionWrapsAround = false;
          }

          break;

        default:
          // Shouldn't occur.
          Assert.expect(false, "Encountered non-cardinal orientation!");

          break;
      }
    }    

    // If the region doesn't wrap around, we're just going to create a simple rectangle.
    if (regionWrapsAround == false)
    {
      defectRegionShape.lineTo((float)endBinLineSegment.getX2(), (float)endBinLineSegment.getY2());
      defectRegionShape.moveTo((float)endBinLineSegment.getX1(), (float)endBinLineSegment.getY1());
      defectRegionShape.lineTo((float)startBinLineSegment.getX1(), (float)startBinLineSegment.getY1());
    }
    // Otherwise, we need to trace around the combined short region.
    else
    {      
      //Siew Yeng profile side other than head,tail,left,right shouldn't enter here!!
      // Trace around the outside of the combined profile region from the start bin to the end bin (counter-clockwise).
      ShortProfileSideEnum currentSide = startBinProfileSideEnum;
      while (currentSide.equals(endBinProfileSideEnum) == false)
      {
        RegionOfInterest currentSideRoi = sideProfileToRoiAndProfileMap.get(currentSide).getFirst();

        // If we're on the head or tail side, we need to also draw across the width of the left or right side.
        int offsetToOuterRegionBorder = 0;
        if (currentSide.equals(ShortProfileSideEnum.HEAD_PROFILE) ||
            currentSide.equals(ShortProfileSideEnum.TAIL_PROFILE))
        {
          offsetToOuterRegionBorder = currentSideRoi.getLengthAcross();
        }

        // Draw the line segment across the current profile region outer side.
        switch (MathUtil.getDegreesWithin0To359(currentSideRoi.getOrientationInDegrees()))
        {
          case 0:
            defectRegionShape.lineTo(currentSideRoi.getMaxX() + 1 + offsetToOuterRegionBorder, currentSideRoi.getMaxY() + 1);

            break;

          case 90:
            defectRegionShape.lineTo(currentSideRoi.getMaxX() + 1, currentSideRoi.getMinY() - offsetToOuterRegionBorder);

            break;

          case 180:
            defectRegionShape.lineTo(currentSideRoi.getMinX() - offsetToOuterRegionBorder, currentSideRoi.getMinY());

            break;

          case 270:
            defectRegionShape.lineTo(currentSideRoi.getMinX(), currentSideRoi.getMaxY() + 1 + offsetToOuterRegionBorder);

            break;

          default:
            // Shouldn't occur.
            Assert.expect(false, "Encountered non-cardinal rotation!");

            break;
        }

        // Move to the next side.
        if (currentSide.equals(ShortProfileSideEnum.HEAD_PROFILE))
        {
          currentSide = ShortProfileSideEnum.RIGHT_PROFILE;
        }
        else if (currentSide.equals(ShortProfileSideEnum.RIGHT_PROFILE))
        {
          currentSide = ShortProfileSideEnum.TAIL_PROFILE;
        }
        else if (currentSide.equals(ShortProfileSideEnum.TAIL_PROFILE))
        {
          currentSide = ShortProfileSideEnum.LEFT_PROFILE;
        }
        else if (currentSide.equals(ShortProfileSideEnum.LEFT_PROFILE))
        {
          currentSide = ShortProfileSideEnum.HEAD_PROFILE;
        }
        else
        {
          // Shouldn't be here.
          Assert.expect(false, "Unexpected ShortProfileSideEnum");
        }
      }
      // Draw the line segment to the outside point of the end bin line segment.
      defectRegionShape.lineTo((float)endBinLineSegment.getX2(), (float)endBinLineSegment.getY2());

      // Trace around the inside of the combined profile region from the end bin to the start bin (clockwise).
      defectRegionShape.moveTo((float)endBinLineSegment.getX1(), (float)endBinLineSegment.getY1());
      while (currentSide.equals(startBinProfileSideEnum) == false)
      {
        RegionOfInterest currentSideRoi = sideProfileToRoiAndProfileMap.get(currentSide).getFirst();

        // If we're on the left or right side, we don't want to draw across the width of the head or tail side.
        int offsetToOuterRegionBorder = 0;
        if (currentSide.equals(ShortProfileSideEnum.LEFT_PROFILE) ||
            currentSide.equals(ShortProfileSideEnum.RIGHT_PROFILE))
        {
          offsetToOuterRegionBorder = currentSideRoi.getLengthAcross();
        }

        // Draw the line segment across the current profile region inner side.
        switch (MathUtil.getDegreesWithin0To359(currentSideRoi.getOrientationInDegrees()))
        {
          case 0:
            defectRegionShape.lineTo(currentSideRoi.getMinX() + offsetToOuterRegionBorder, currentSideRoi.getMinY());

            break;

          case 90:
            defectRegionShape.lineTo(currentSideRoi.getMinX(), currentSideRoi.getMaxY() + 1 - offsetToOuterRegionBorder);

            break;

          case 180:
            defectRegionShape.lineTo(currentSideRoi.getMaxX() + 1 - offsetToOuterRegionBorder, currentSideRoi.getMaxY() + 1);

            break;

          case 270:
            defectRegionShape.lineTo(currentSideRoi.getMaxX() + 1, currentSideRoi.getMinY() + offsetToOuterRegionBorder);

            break;

          default:
            // Shouldn't occur.
            Assert.expect(false, "Encountered non-cardinal rotation!");

            break;
        }

        // Move to the next side.
        if (currentSide.equals(ShortProfileSideEnum.HEAD_PROFILE))
        {
          currentSide = ShortProfileSideEnum.LEFT_PROFILE;
        }
        else if (currentSide.equals(ShortProfileSideEnum.RIGHT_PROFILE))
        {
          currentSide = ShortProfileSideEnum.HEAD_PROFILE;
        }
        else if (currentSide.equals(ShortProfileSideEnum.TAIL_PROFILE))
        {
          currentSide = ShortProfileSideEnum.RIGHT_PROFILE;
        }
        else if (currentSide.equals(ShortProfileSideEnum.LEFT_PROFILE))
        {
          currentSide = ShortProfileSideEnum.TAIL_PROFILE;
        }
        else
        {
          // Shouldn't be here.
          Assert.expect(false, "Unexpected ShortProfileSideEnum");
        }
      }
      // Draw the line segment to the inside point of the start bin line segment.
      defectRegionShape.lineTo((float)startBinLineSegment.getX1(), (float)startBinLineSegment.getY1());
    }

    return defectRegionShape;
  }


  /**
   * @edited by Siew Yeng - XCR-3318
   * Let say we have a ROI 5x5 where there are short start from pixel-1 to pixel-3.
   * 
   * offsetIntoSideProfileRoi(start) : 1
   * offsetIntoSideProfileRoi(end)   : 3
   * 
   *  Pixel                                Line
   *  index                                index 
   *            ---------------------   <-   0
   *    0 ->    |   |   |   |   |   |        
   *            ---------------------   <-   1
   *    1 ->    | x | x | x | x | x |
   *            ---------------------   <-   2
   *    2 ->    | x | x | x | x | x |
   *            ---------------------   <-   3
   *    3 ->    | x | x | x | x | x |
   *            ---------------------   <-   4
   *    4 ->    |   |   |   |   |   |
   *            ---------------------   <-   5
   * 
   *  Starting line: offsetIntoSideProfileRoi    ------> Line 1
   *  Ending line  : offsetIntoSideProfileRoi + 1 -----> Line 4
   *            
   *  ALWAYS +1 for END LINE to cover up the short area correctly.
   * 
   *  Pixel                                Line
   *  index                                index 
   *            ---------------------   <-   0
   *    0 ->    |   |   |   |   |   |        
   *            =====================   <-   1
   *    1 ->   || x | x | x | x | x ||
   *            ---------------------   <-   2
   *    2 ->   || x | x | x | x | x ||
   *            ---------------------   <-   3
   *    3 ->   || x | x | x | x | x ||
   *            =====================   <-   4
   *    4 ->    |   |   |   |   |   |
   *            ---------------------   <-   5
   * 
   * ROI.getMinX() -> 0     ROI.getMaxX() -> 4
   * ROI.getMinY() -> 0     ROI.getMaxY() -> 4
   * ROI maxX & maxY MUST always +1 as well to cover up the whole region !!
   * 
   * Given the specified side profile ROI and the offset into the bin, calculates a line segment for that
   * bin.
   * @param isEndBin - Set TRUE if looking for END line of short. Set FALSE if looking for START line of short
   * @author Matt Wharton
   */
  private Line2D getLineSegmentForBinInCombinedRectangularRegion(RegionOfInterest sideProfileRoi,
                                                                 int offsetIntoSideProfileRoi,
                                                                 boolean isEndBin)
  {
    Assert.expect(sideProfileRoi != null);
    Assert.expect(offsetIntoSideProfileRoi <= sideProfileRoi.getLengthAlong());
    
    if(isEndBin)
      offsetIntoSideProfileRoi += 1;

    int x1 = 0;
    int y1 = 0;
    int x2 = 0;
    int y2 = 0;
    switch (MathUtil.getDegreesWithin0To359(sideProfileRoi.getOrientationInDegrees()))
    {
      case 0:
        x1 = sideProfileRoi.getMinX() + offsetIntoSideProfileRoi;
        y1 = sideProfileRoi.getMinY();
        x2 = x1;
        y2 = sideProfileRoi.getMaxY() + 1;

        break;

      case 90:
        x1 = sideProfileRoi.getMinX();
        y1 = sideProfileRoi.getMaxY() + 1 - offsetIntoSideProfileRoi;
        x2 = sideProfileRoi.getMaxX() + 1;
        y2 = y1;

        break;

      case 180:
        x1 = sideProfileRoi.getMaxX() + 1 - offsetIntoSideProfileRoi;
        y1 = sideProfileRoi.getMaxY() + 1;
        x2 = x1;
        y2 = sideProfileRoi.getMinY();

        break;

      case 270:
        x1 = sideProfileRoi.getMaxX() + 1;
        y1 = sideProfileRoi.getMinY() + offsetIntoSideProfileRoi;
        x2 = sideProfileRoi.getMinX();
        y2 = y1;

        break;

      default:
        // Shouldn't occur.
        Assert.expect(false, "Encountered non-cardinal orientation!");

        break;
    }

    // Create the line segment for the bin.
    Line2D lineSegmentForBin = new Line2D.Float(x1, y1, x2, y2);

    return lineSegmentForBin;
  }


  /**
   * @author Matt Wharton
   */
  private Pair<ShortProfileSideEnum, Integer> getShortProfileSideAndOffsetForBinInCombinedRectRegion(
      float[] combinedProfile,
      Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> sideProfileToRoiAndProfileMap,
      int binNumberInCombinedProfile)
  {
    Assert.expect(combinedProfile != null);
    Assert.expect(sideProfileToRoiAndProfileMap != null);
    Assert.expect((binNumberInCombinedProfile >= 0) && (binNumberInCombinedProfile < combinedProfile.length));

    // Extract the side ROIs.
    RegionOfInterest headProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.HEAD_PROFILE).getFirst();
    RegionOfInterest tailProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.TAIL_PROFILE).getFirst();
    RegionOfInterest leftProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.LEFT_PROFILE).getFirst();
    RegionOfInterest rightProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.RIGHT_PROFILE).getFirst();

    int headProfileRoiLengthAlong = headProfileRoi.getLengthAlong();
    int tailProfileRoiLengthAlong = tailProfileRoi.getLengthAlong();
    int leftProfileRoiLengthAlong = leftProfileRoi.getLengthAlong();
    int rightProfileRoiLengthAlong = rightProfileRoi.getLengthAlong();
    
    //Siew Yeng - XCR-3318
    int bottomRightCornerProfileRoiLengthAlong = 0;
    int topRightCornerProfileRoiLengthAlong = 0;
    int topLeftCornerProfileRoiLengthAlong = 0;
    int bottomLeftCornerProfileRoiLengthAlong = 0;
    
    if(isEnableTestRegionInnerCorner(sideProfileToRoiAndProfileMap))
    {
      RegionOfInterest bottomRightCornerProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.BOTTOM_RIGHT_INNER_CORNER_PROFILE).getFirst();
      RegionOfInterest topRightCornerProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.TOP_RIGHT_INNER_CORNER_PROFILE).getFirst();
      RegionOfInterest topLeftCornerProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.TOP_LEFT_INNER_CORNER_PROFILE).getFirst();
      RegionOfInterest bottomLeftCornerProfileRoi = sideProfileToRoiAndProfileMap.get(ShortProfileSideEnum.BOTTOM_LEFT_INNER_CORNER_PROFILE).getFirst();
      
      bottomRightCornerProfileRoiLengthAlong = bottomRightCornerProfileRoi.getLengthAlong();
      topRightCornerProfileRoiLengthAlong = topRightCornerProfileRoi.getLengthAlong();
      topLeftCornerProfileRoiLengthAlong = topLeftCornerProfileRoi.getLengthAlong();
      bottomLeftCornerProfileRoiLengthAlong = bottomLeftCornerProfileRoi.getLengthAlong();
    }

    // Figure out what side ROI the bin belongs to (assumes counter-clockwise order of 'head', 'right', 'tail', 'left').
    ShortProfileSideEnum shortProfileSideEnumForBin = null;
    int offsetIntoSideBin = -1;

    // Is it in the head ROI?
    if ((binNumberInCombinedProfile >= 0) && (binNumberInCombinedProfile < headProfileRoiLengthAlong))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.HEAD_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile;
    }
    // Is it in the right ROI?
    else if ((binNumberInCombinedProfile >= headProfileRoiLengthAlong) &&
             (binNumberInCombinedProfile < (headProfileRoiLengthAlong + rightProfileRoiLengthAlong)))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.RIGHT_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile - headProfileRoiLengthAlong;
    }
    // Is it in the tail ROI?
    else if ((binNumberInCombinedProfile >= (headProfileRoiLengthAlong + rightProfileRoiLengthAlong)) &&
             (binNumberInCombinedProfile < (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong)))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.TAIL_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile - (headProfileRoiLengthAlong + rightProfileRoiLengthAlong);
    }
    // Is it in the left ROI?
    else if ((binNumberInCombinedProfile >= (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong)) &&
             (binNumberInCombinedProfile < (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong)))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.LEFT_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile -
                          (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong);
    }
    // Is it in the bottom right inner corner ROI?
    else if (bottomRightCornerProfileRoiLengthAlong != 0 &&
             (binNumberInCombinedProfile >= (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong)) &&
             (binNumberInCombinedProfile < (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                                            bottomRightCornerProfileRoiLengthAlong)))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.BOTTOM_RIGHT_INNER_CORNER_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile -
                          (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong);
    }
    // Is it in the top right inner corner ROI?
    else if (topRightCornerProfileRoiLengthAlong != 0 &&
             (binNumberInCombinedProfile >= (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                                            bottomRightCornerProfileRoiLengthAlong)) &&
             (binNumberInCombinedProfile < (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                                            bottomRightCornerProfileRoiLengthAlong + topRightCornerProfileRoiLengthAlong)))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.TOP_RIGHT_INNER_CORNER_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile - 
                          (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                           bottomRightCornerProfileRoiLengthAlong);
    }
    // Is it in the top left inner corner ROI
    else if (topLeftCornerProfileRoiLengthAlong != 0 &&
             (binNumberInCombinedProfile >= (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                                            bottomRightCornerProfileRoiLengthAlong + topRightCornerProfileRoiLengthAlong)) &&
             (binNumberInCombinedProfile < (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                                            bottomRightCornerProfileRoiLengthAlong + topRightCornerProfileRoiLengthAlong + 
                                            topLeftCornerProfileRoiLengthAlong)))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.TOP_LEFT_INNER_CORNER_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile - 
                          (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                           bottomRightCornerProfileRoiLengthAlong + topRightCornerProfileRoiLengthAlong);
    }
    // Is it in the bottom left inner corner ROI?
    else if (bottomLeftCornerProfileRoiLengthAlong != 0 &&
             (binNumberInCombinedProfile >= (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                                            bottomRightCornerProfileRoiLengthAlong + topRightCornerProfileRoiLengthAlong + topLeftCornerProfileRoiLengthAlong)) &&
             (binNumberInCombinedProfile < (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                                            bottomRightCornerProfileRoiLengthAlong + topRightCornerProfileRoiLengthAlong + 
                                            topLeftCornerProfileRoiLengthAlong + bottomLeftCornerProfileRoiLengthAlong)))
    {
      shortProfileSideEnumForBin = ShortProfileSideEnum.BOTTOM_LEFT_INNER_CORNER_PROFILE;
      offsetIntoSideBin = binNumberInCombinedProfile -
                          (headProfileRoiLengthAlong + rightProfileRoiLengthAlong + tailProfileRoiLengthAlong + leftProfileRoiLengthAlong + 
                           bottomRightCornerProfileRoiLengthAlong + topRightCornerProfileRoiLengthAlong + topLeftCornerProfileRoiLengthAlong);
    }

    Assert.expect(shortProfileSideEnumForBin != null,
                  "Couldn't find appropriate side for bin: " + binNumberInCombinedProfile);
    Assert.expect(offsetIntoSideBin != -1, "Couldn't calculate side bin offset for bin: " + binNumberInCombinedProfile);

    return new Pair<ShortProfileSideEnum, Integer>(shortProfileSideEnumForBin, offsetIntoSideBin);
  }

  /**
   * Refer to function getShortProfileSideAndOffsetForBinInCombinedRectRegion.
   * Support multiple ROI for center region - solder ball detection used.
   * @author Lim, Lay Ngor - XCR1743:Benchmark
   */
  private Pair<ShortProfileSideEnum, Integer> getShortProfileSideAndOffsetForBinInCenterRectRegion(
      float[] multiROICenterProfile, //just for checking only
      Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> centerEnumToRoiAndProfileMap,
      int binNumberInCenterProfile)
  {
    Assert.expect(multiROICenterProfile != null);
    Assert.expect(centerEnumToRoiAndProfileMap != null);
    Assert.expect((binNumberInCenterProfile >= 0) && (binNumberInCenterProfile < multiROICenterProfile.length));

    // Figure out which center ROI the bin belongs to (according to enum id).
    ShortProfileSideEnum shortProfileCenterEnumForMultiROICenterBin = null;
    int offsetIntoMultiROICenterBin = -1;    
    int id = ShortProfileSideEnum.CENTER_PROFILE_1.getId();
    int previousSectionCenterRoiLengthAlong = 0;
    //Now start from 1st roi until the last available roi
    for (int i = 0; i < ShortProfileSideEnum.getMaxSupportedCenterProfileEnum(); ++i)
    {
      Pair<RegionOfInterest, float[]> singleSectionCenterProfileMap = centerEnumToRoiAndProfileMap.get(ShortProfileSideEnum.getEnum(id));
      if (singleSectionCenterProfileMap == null)
        break; //available center profile end
      else
      {
        // One by one extract each section of center ROI.
        final RegionOfInterest singleSectionCenterProfileRoi = singleSectionCenterProfileMap.getFirst();
        final int singleSectionCenterProfileRoiLengthAlong = previousSectionCenterRoiLengthAlong + singleSectionCenterProfileRoi.getLengthAlong();

        // Is it in this center ROI?
        if ((binNumberInCenterProfile >= previousSectionCenterRoiLengthAlong) && 
          (binNumberInCenterProfile < singleSectionCenterProfileRoiLengthAlong))
        {
          shortProfileCenterEnumForMultiROICenterBin = ShortProfileSideEnum.getEnum(id);
          offsetIntoMultiROICenterBin = binNumberInCenterProfile - previousSectionCenterRoiLengthAlong;
          break; //reach the section of center roi profile that we want, so just stop here
        }
        
        previousSectionCenterRoiLengthAlong = singleSectionCenterProfileRoiLengthAlong;
        ++id;
      }
    }     
    
    Assert.expect(shortProfileCenterEnumForMultiROICenterBin != null,
                  "Couldn't find appropriate side for bin: " + binNumberInCenterProfile);
    Assert.expect(offsetIntoMultiROICenterBin != -1, "Couldn't calculate side bin offset for bin: " + binNumberInCenterProfile);  
    
    return new Pair<ShortProfileSideEnum, Integer>(shortProfileCenterEnumForMultiROICenterBin, offsetIntoMultiROICenterBin);    
  }  

  /**
   * @author Matthew Wharton
   * @author Patrick Lacz
   */
  protected float[] measureThicknessProfileForLearning(Image image,
                                                       ReconstructionRegion inspectionRegion,
                                                       SliceNameEnum sliceNameEnum,
                                                       JointInspectionData jointInspectionData,
                                                       ShortProfileLearning shortLearning) throws DatastoreException

  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(shortLearning != null);

    Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>>
        profileSideMap = measureRectangularRegionBackgroundGrayLevelProfiles(image,
                                                                             inspectionRegion,
                                                                             sliceNameEnum,
                                                                             jointInspectionData);

    // Get the head ROI gray level profile.
    Pair<RegionOfInterest, float[]> headRegionAndProfilePair =
        profileSideMap.get(ShortProfileSideEnum.HEAD_PROFILE);
    float[] headProfile = headRegionAndProfilePair.getSecond();

    // Get the tail ROI gray level profile.
    Pair<RegionOfInterest, float[]> tailRegionAndProfilePair =
        profileSideMap.get(ShortProfileSideEnum.TAIL_PROFILE);
    float[] tailProfile = tailRegionAndProfilePair.getSecond();

    // Get the left ROI gray level profile.
    Pair<RegionOfInterest, float[]> leftRegionAndProfilePair =
        profileSideMap.get(ShortProfileSideEnum.LEFT_PROFILE);
    float[] leftProfile = leftRegionAndProfilePair.getSecond();

    // Get the right ROI gray level profile.
    Pair<RegionOfInterest, float[]> rightRegionAndProfilePair =
        profileSideMap.get(ShortProfileSideEnum.RIGHT_PROFILE);
    float[] rightProfile = rightRegionAndProfilePair.getSecond();

    // Create a combined gray level profile.
    float[] combinedGrayLevelProfile = ArrayUtil.combineArrays(new float[][]
                                                               {headProfile, rightProfile, tailProfile, leftProfile});
    
    //Siew Yeng - XCR-3318
    if(isEnableTestRegionInnerCorner(profileSideMap))
    {
      // Get the head ROI gray level profile.
      Pair<RegionOfInterest, float[]> bottomRightInnerCornerRegionAndProfilePair =
          profileSideMap.get(ShortProfileSideEnum.BOTTOM_RIGHT_INNER_CORNER_PROFILE);
      float[] bottomRightInnerCornerProfile = bottomRightInnerCornerRegionAndProfilePair.getSecond();

      // Get the tail ROI gray level profile.
      Pair<RegionOfInterest, float[]> topRightInnerCornerRegionAndProfilePair =
          profileSideMap.get(ShortProfileSideEnum.TOP_RIGHT_INNER_CORNER_PROFILE);
      float[] topRightInnerCornerProfile = topRightInnerCornerRegionAndProfilePair.getSecond();

      // Get the left ROI gray level profile.
      Pair<RegionOfInterest, float[]> topLeftInnerCornerRegionAndProfilePair =
          profileSideMap.get(ShortProfileSideEnum.TOP_LEFT_INNER_CORNER_PROFILE);
      float[] topLeftInnerCornerProfile = topLeftInnerCornerRegionAndProfilePair.getSecond();

      // Get the right ROI gray level profile.
      Pair<RegionOfInterest, float[]> bottomLeftInnerCornerRegionAndProfilePair =
          profileSideMap.get(ShortProfileSideEnum.BOTTOM_LEFT_INNER_CORNER_PROFILE);
      float[] bottomLeftInnerCornerProfile = bottomLeftInnerCornerRegionAndProfilePair.getSecond();
      
      //append inner corners profile at the end of the profile
      combinedGrayLevelProfile = ArrayUtil.combineArrays(new float[][]
                                                               {combinedGrayLevelProfile, 
                                                                bottomRightInnerCornerProfile,
                                                                topRightInnerCornerProfile,
                                                                topLeftInnerCornerProfile,
                                                                bottomLeftInnerCornerProfile});
    }

	//XCR2111: Revert back-put learn at correct place
    // Learn the average gray level for the short region. - without center region
    float averageGrayLevel = StatisticsUtil.mean(combinedGrayLevelProfile);
    shortLearning.setAverageRegionGrayLevel(averageGrayLevel);
    
      //Lim, Lay Ngor - XCR1743:Benchmark - START
      // Get each center ROI gray level profile and add it behind the short profile.
      int id = ShortProfileSideEnum.CENTER_PROFILE_1.getId();
      for(int i=0; i<ShortProfileSideEnum.getMaxSupportedCenterProfileEnum(); ++i)
      {
        Pair<RegionOfInterest, float[]> singleSectionCenterProfile = profileSideMap.get(ShortProfileSideEnum.getEnum(id));
        if(singleSectionCenterProfile == null)
          break; //reach the last center roi profile that we have, so just stop here
        else
        {
          combinedGrayLevelProfile = ArrayUtil.combineArrays(new float[][]
                                                               {combinedGrayLevelProfile, singleSectionCenterProfile.getSecond()});                    
        }
        ++id;
      }
     //Lim, Lay Ngor - XCR1743:Benchmark - END

    // Convert the combined gray level profile to a thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    float thicknessProfile[] =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(combinedGrayLevelProfile,
                                                                             _GRAY_LEVEL_BACKGROUND_PERCENTILE,
                                                                             thicknessTable,
                                                                             backgroundSensitivityOffset);
    // Smooth the thickness profile.
    thicknessProfile = ProfileUtil.getSmoothedProfile(thicknessProfile, _SMOOTHING_KERNEL_LENGTH);

    return thicknessProfile;
  }
  
  /**
   * XCR-3318
   * @author Siew Yeng
   */
  protected void learnShortRegionPlacementSettings(Subtype subtype)
  {
    Assert.expect(subtype != null);

    //Copy from CircularShortAlgorithm
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE))
    {
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION, 25.f);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION, 80.f);
    }
  }
  
  /**
   * XCR-3318
   * @author Siew Yeng 
   */
  private boolean isEnableTestRegionInnerCorner(Map<ShortProfileSideEnum, Pair<RegionOfInterest, float[]>> sideProfileToRoiAndProfileMap)
  {
    Assert.expect(sideProfileToRoiAndProfileMap != null);
    
    //Check if inner corner profile exist
    return sideProfileToRoiAndProfileMap.containsKey(ShortProfileSideEnum.BOTTOM_RIGHT_INNER_CORNER_PROFILE);
  }
}
