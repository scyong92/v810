package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.util.image.Filter;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;


/**
 *
 * @author wei-chin.chong
 */
public class ImageProcessingAlgorithm
{
  private static final String _TRUE = "True";
  private static final String _FALSE = "False";
  
  private static final String _NONE = "None";
  private static final String _HORIZONTAL = "Horizontal";
  private static final String _VERTICAL = "Vertical";
  private static final String _BOTH = "Both";  
  
  private static final String _THICKCAL_COMPENSATION = "ThickCal Compensation";
  private static final String _DIFFERENCE = "Difference";
  /**
   * The below parameter is used to ensure originalImage that pass into each image enhancer implementation
   * in this class(e.g. applyCLAHE, applyFFTBandPassFilter & etc) is called decrementReferenceCount().
 The checking to determine whether we need to called originalImage.decrementReferenceCount() 
 is depends on the sequence of each enhancer implementation that called from function 
 enhanceImageBeforeInspection() in class ReconstructedImages.java. 
 The 1st  function(which is applyResize) don't need to called originalImage.decrementReferenceCount().
 While the 2nd function(which is applyCLAHE) need to check for parameter  "_RESIZE_PROPERTY"
 before calling originalImage.decrementReferenceCount().
 And the subsequence function need to check for all previous parameter before calling 
 originalImage.decrementReferenceCount().
 e.g. applyFFTBandPassFilter which is currently the last implementation at function enhanceImageBeforeInspection()
 need to check all previous parameter which is "_RESIZE_PROPERTY", "_CLAHE_PROPERTY",
 and "_REMOVE_BACKGROUND_PROPERTY" before calling originalImage.decrementReferenceCount().
   */
  private static final String _RESIZE_PROPERTY = "resize";
  private static final String _MOTION_BLUR_PROPERTY = "motionBlur";
  private static final String _SHADING_REMOVAL_PROPERTY = "shadingRemoval";
  private static final String _CLAHE_PROPERTY = "equalizer";
  private static final String _REMOVE_BACKGROUND_PROPERTY = "backgroundRemover";  
  private static final String _FFT_BANDPASSFILTER_PROPERTY = "BandPassFilter";
  private static final String _RFILTER_PROPERTY = "RFilter";
  private static TimerUtil _timerUtil = new TimerUtil();
  private static boolean _logTimestamp = false;
  
  /**
   * Create the algorithm settings required by the boxFilter algorithm.
   * 
   * <pre>
   * for (AlgorithmSetting algSetting : Locator.createAlgorithmSettings(displayOrder, currentVersion))
   *   addAlgorithmSetting(algSetting);
   * </pre>
   *
   * @author Wei Chin
   */
  public static Collection<AlgorithmSetting> createBoxFilterAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    // The "Enable Box Filter " settings define the area that the filter will be
    // applied to. 
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableBoxFilter = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE,
        displayOrder++,
        _FALSE,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(ENABLE_BOX_FILTER)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(ENABLE_BOX_FILTER)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(ENABLE_BOX_FILTER)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(enableBoxFilter);
    
    AlgorithmSetting boxFilterIterator = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR,
        displayOrder++,
        1, // default value
        1, // minimum value
        10, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_ITERATOR)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_ITERATOR)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_ITERATOR)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(boxFilterIterator);
    
    AlgorithmSetting boxFilterWidth = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_WIDTH,
        displayOrder++,
        0, // default value
        0, // minimum value
        1000, // maximum value
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_WIDTH)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_WIDTH)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_WIDTH)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(boxFilterWidth);
    
    AlgorithmSetting boxFilterLength = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_LENGTH,
        displayOrder++,
        0, // default value
        0, // minimum value
        1000, // maximum value
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_LENGTH)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_LENGTH)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(BOX_FILTER_LENGTH)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(boxFilterLength);

    return settingList;
  }
  
  /**
   * Create the algorithm settings required by the boxFilter algorithm.
   * 
   * <pre>
   * for (AlgorithmSetting algSetting : Locator.createAlgorithmSettings(displayOrder, currentVersion))
   *   addAlgorithmSetting(algSetting);
   * </pre>
   *
   * @author Wei Chin
   */
  public static Collection<AlgorithmSetting> createResizeAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    // The "Enable Box Filter " settings define the area that the filter will be
    // applied to. 
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableResize = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE,
        displayOrder++,
        _FALSE,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(ENABLE_RESIZE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(ENABLE_RESIZE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(ENABLE_RESIZE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(enableResize);
    
    AlgorithmSetting resizeScale = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE,
        displayOrder++,
        2, // default value
        1, // minimum value
        4, // maximum value
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_IMAGE_ENHANCEMENT_(RESIZE_SCALE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(RESIZE_SCALE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(RESIZE_SCALE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(resizeScale);

    return settingList;
  }
  
/**
   * Create the algorithm settings required by the boxFilter algorithm.
   * 
   * <pre>
   * for (AlgorithmSetting algSetting : Locator.createAlgorithmSettings(displayOrder, currentVersion))
   *   addAlgorithmSetting(algSetting);
   * </pre>
   *
   * @author Wei Chin
   */
  public static Collection<AlgorithmSetting> createCLAHEAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    // The "Enable Box Filter " settings define the area that the filter will be
    // applied to. 
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableCLAHE = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE,
        displayOrder++,
        _FALSE,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(ENABLE_CLAHE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(ENABLE_CLAHE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(ENABLE_CLAHE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(enableCLAHE);
    
     AlgorithmSetting blockSize = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_BLOCK_SIZE,
        displayOrder++,
        0, // default value
        0, // minimum value
        1000, // maximum value
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_IMAGE_ENHANCEMENT_(CLAHE_BLOCK_SIZE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(CLAHE_BLOCK_SIZE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(CLAHE_BLOCK_SIZE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
     
    settingList.add(blockSize);

    return settingList;
  }
  
  /**
   * XCR1723: Create the algorithm settings required by the FFT BandPassFilter algorithm.
   * 
   * <pre>
   * for (AlgorithmSetting algSetting : Locator.createAlgorithmSettings(displayOrder, currentVersion))
   *   addAlgorithmSetting(algSetting);
   * </pre>
   *
   * @author Lim, Lay Ngor
   */
  public static Collection<AlgorithmSetting> createFFTBandPassFilterAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    // The "Enable enableFFTBandPass Filter " settings define the area that the filter will be applied to. 
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableFFTBandPassFilter = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE,
        displayOrder++,
        _FALSE,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(ENABLE_FFT_BANDPASS_FILTER)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(ENABLE_FFT_BANDPASS_FILTER)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(ENABLE_FFT_BANDPASS_FILTER)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(enableFFTBandPassFilter);
    
    AlgorithmSetting FFTBandPassFilterLargeScale = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_LARGESCALE,
        displayOrder++,
        40, // default value
        1, // minimum value
        1000, // maximum value
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_LARGE_SCALE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_LARGE_SCALE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_LARGE_SCALE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(FFTBandPassFilterLargeScale);
    
    AlgorithmSetting FFTBandPassFilterSmallScale = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE,
        displayOrder++,
        3, // default value
        0, // minimum value
        100, // maximum value - the band pass small scale is depends on the size of image background noise. 
             //                 Therefore, it could be larger than 100 if the image is huge.
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SMALL_SCALE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SMALL_SCALE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SMALL_SCALE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(FFTBandPassFilterSmallScale);
      
    ArrayList<String> suppressStripesOptions = new ArrayList<String>(Arrays.asList(
      _NONE, _HORIZONTAL, _VERTICAL));
    AlgorithmSetting FFTBandPassFilterSuppressStripes = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUPPRESSSTRIPES,
        displayOrder++,
        _NONE, // default value
        suppressStripesOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SUPPRESS_STRIPES)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SUPPRESS_STRIPES)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SUPPRESS_STRIPES)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(FFTBandPassFilterSuppressStripes);    

    AlgorithmSetting FFTBandPassFilterToleranceOfDirection = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_TOLERANCEOFDIRECTION,
        displayOrder++,
        5, // default value
        0, // minimum value
        100, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_TOLERANCE_OF_DIRECTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_TOLERANCE_OF_DIRECTION)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_TOLERANCE_OF_DIRECTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(FFTBandPassFilterToleranceOfDirection);
    
    AlgorithmSetting FFTBandPassFilterSaturateValue = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SATURATEVALUE,
        displayOrder++,
        5, // default value
        0, // minimum value
        100, // maximum value
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SATURATE_VALUE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SATURATE_VALUE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(FFT_BANDPASS_FILTER_SATURATE_VALUE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(FFTBandPassFilterSaturateValue);    
    
    return settingList;
  }
  
  /**
   * @author Siew Yeng
   */
  public static Collection<AlgorithmSetting> createRFilterAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    // The "Enable R Filter " settings define the area that the filter will be
    // applied to. 
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableResize = new AlgorithmSetting(
        AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RFILTER_ENABLE,
        displayOrder++,
        _FALSE,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(ENABLE_RFILTER)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(ENABLE_RFILTER)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(ENABLE_RFILTER)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(enableResize);
    
    return settingList;
  }

  /**
   * Motion Blur Algorithm Settings
   * @author Kok Chun, Tan
   */
  public static Collection<AlgorithmSetting> createMotionBlurAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableMotionBlur = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE,
      displayOrder++,
      _FALSE,
      trueFalseOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(ENABLE_MOTION_BLUR)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(ENABLE_MOTION_BLUR)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(ENABLE_MOTION_BLUR)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(enableMotionBlur);

    ArrayList<String> directionOptions = new ArrayList<String>(Arrays.asList(_HORIZONTAL, _VERTICAL, _BOTH));
    AlgorithmSetting direction = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_DIRECTION,
      displayOrder++,
      _VERTICAL, // default value
      directionOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(MOTION_BLUR_DIRECTION)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(MOTION_BLUR_DIRECTION)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(MOTION_BLUR_DIRECTION)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(direction);

    AlgorithmSetting scale = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_MASK_SCALE,
      displayOrder++,
      60, // default value
      4, // minimum value
      200, // maximum value
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(MASK_SCALE)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(MASK_SCALE)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(MASK_SCALE)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(scale);

    AlgorithmSetting offset = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_GRAYLEVEL_OFFSET,
      displayOrder++,
      80, // default value
      1, // minimum value
      254, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_IMAGE_ENHANCEMENT_(GRAYLEVEL_OFFSET)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(GRAYLEVEL_OFFSET)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(GRAYLEVEL_OFFSET)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(offset);

    AlgorithmSetting factor = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ITERATION,
      displayOrder++,
      1, // default value
      1, // minimum value
      50, // maximum value
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(MOTION_BLUR_ITERATION)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(MOTION_BLUR_ITERATION)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(MOTION_BLUR_ITERATION)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(factor);

    return settingList;
  }
  
  /**
   * Shading Removal Algorithm Settings
   * @author Kok Chun, Tan
   */
  public static Collection<AlgorithmSetting> createShadingRemovalAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableShadingRemoval = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE,
      displayOrder++,
      _FALSE,
      trueFalseOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(ENABLE_SHADING_REMOVAL)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(ENABLE_SHADING_REMOVAL)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(ENABLE_SHADING_REMOVAL)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(enableShadingRemoval);

    ArrayList<String> directionOptions = new ArrayList<String>(Arrays.asList(_HORIZONTAL, _VERTICAL, _BOTH));
    AlgorithmSetting direction = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DIRECTION,
      displayOrder++,
      _VERTICAL, // default value
      directionOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_DIRECTION)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_DIRECTION)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_DIRECTION)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(direction);

    AlgorithmSetting scale = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_BLUR_DISTANCE,
      displayOrder++,
      30, // default value
      4, // minimum value
      200, // maximum value
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_BLUR_DISTANCE)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_BLUR_DISTANCE)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_BLUR_DISTANCE)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(scale);

    AlgorithmSetting offset = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_KEEP_OUT_DISTANCE,
      displayOrder++,
      10, // default value
      0, // minimum value
      200, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_KEEP_OUT_DISTANCE)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_KEEP_OUT_DISTANCE)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_KEEP_OUT_DISTANCE)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(offset);
    
    ArrayList<String> filterTechiniqueOptions = new ArrayList<String>(Arrays.asList(_THICKCAL_COMPENSATION, _DIFFERENCE));
    AlgorithmSetting backgroundFilterMethod = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_FILTERING_TECHNIQUE,
      displayOrder++,
      _THICKCAL_COMPENSATION,
      filterTechiniqueOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_FILTERING_TECHNIQUE)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_FILTERING_TECHNIQUE)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_FILTERING_TECHNIQUE)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(backgroundFilterMethod);
    
    AlgorithmSetting desiredBackground = new AlgorithmSetting(
      AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DESIRED_BACKGROUND,
      displayOrder++,
      255, // default value
      1, // minimum value
      255, // maximum value
      MeasurementUnitsEnum.GRAYLEVEL,
      "HTML_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_DESIRED_BACKGROUND)_KEY", // description URL key
      "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_DESIRED_BACKGROUND)_KEY", // desailed description URL key
      "IMG_DESC_IMAGE_ENHANCEMENT_(SHADING_REMOVAL_DESIRED_BACKGROUND)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(desiredBackground);

    return settingList;
  }
  
  /**
   * @param subtype
   * @return 
   * @author Wei Chin
   */
  public static boolean useBoxFilter(Subtype subtype)
  {
    boolean useBoxFilter = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ENABLE);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          useBoxFilter = false;
        else if (value.equals(_TRUE))
          useBoxFilter = true;
        else
          Assert.expect(false, "Unexpected Box filter value.");
    }
    return useBoxFilter;
  }
  
  /**
   * @param subtype
   * @return 
   * @author Wei Chin
   */
  public static boolean useResize(Subtype subtype)
  {
    boolean useResize = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_ENABLE);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          useResize = false;
        else if (value.equals(_TRUE))
          useResize = true;
        else
          Assert.expect(false, "Unexpected Resize value.");
    }
    return useResize;
  }

  /**
   * @param subtype
   * @return 
   * @author Wei Chin
   */
  public static boolean useCLAHE(Subtype subtype)
  {
    boolean useCLAHE = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_ENABLE);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          useCLAHE = false;
        else if (value.equals(_TRUE))
          useCLAHE = true;
        else
          Assert.expect(false, "Unexpected CLAHE value.");
    }
    return useCLAHE;
  }
  
  /**
   * @param subtype
   * @return 
   * @author Lim, Lay Ngor
   */
  public static boolean useFFTBandPassFilter(Subtype subtype)
  {
    boolean useFFTBandPassFilter = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_ENABLE);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          useFFTBandPassFilter = false;
        else if (value.equals(_TRUE))
          useFFTBandPassFilter = true;
        else
          Assert.expect(false, "Unexpected Band Pass filter value.");
    }
    return useFFTBandPassFilter;
  }    
  
  /**
   * @author Siew Yeng
   */
  public static boolean useRFilter(Subtype subtype)
  {
    boolean useRFilter = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RFILTER_ENABLE))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RFILTER_ENABLE);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          useRFilter = false;
        else if (value.equals(_TRUE))
          useRFilter = true;
        else
          Assert.expect(false, "Unexpected R filter value.");
    }
    return useRFilter;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static boolean useMotionBlur(Subtype subtype)
  {
    boolean useMotionBlur = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE))
    {
      java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ENABLE);
      Assert.expect(value instanceof String);
      if (value.equals(_FALSE))
        useMotionBlur = false;
      else if (value.equals(_TRUE))
        useMotionBlur = true;
      else
        Assert.expect(false, "Unexpected Motion Blur value.");
    }
    return useMotionBlur;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static boolean useShadingRemoval(Subtype subtype)
  {
    boolean useShadingRemoval = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE))
    {
      java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_ENABLE);
      Assert.expect(value instanceof String);
      if (value.equals(_FALSE))
        useShadingRemoval = false;
      else if (value.equals(_TRUE))
        useShadingRemoval = true;
      else
        Assert.expect(false, "Unexpected Shading Removal value.");
    }
    return useShadingRemoval;
  }
  
  /**
   * @param subtype
   * @return 
   * 
   * @author Wei Chin
   */
  public static Image removeBackgroundByBoxFilter(Subtype subtype, Image originalImage, double width, double height)
  {
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
    
    //Siew Yeng - XCR-2170 - fix box filter always get different box size
//    double height = subtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getHeight();
//    double width = subtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getWidth();

    double NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();    
    if(subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      NANOMETERS_PER_PIXEL = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    
    int scaleFactor = AlgorithmUtil.getResizeFactor(subtype);
    final double PIXELS_PER_NANOMETER = scaleFactor * 1.0 / NANOMETERS_PER_PIXEL;
    
    //Siew Yeng - XCR-2388 - Image Enhancer - Add threshold for Clahe and Background Filter
    int boxWidth = (Integer)  subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_WIDTH);
    int boxLength = (Integer)  subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_LENGTH);
    
    //Siew Yeng - use back the auto calculate width and height if threshold set is equal to zero.
    //Siew Yeng - XCR-2576 - limit the maximum width to image width
    if(boxWidth > 0)
      width = Math.min(boxWidth, originalImage.getWidth() - 1);
    else
      width = width * PIXELS_PER_NANOMETER;
    
    //Siew Yeng - limit the maximum length to image height
    if(boxLength > 0)
      height = Math.min(boxLength, originalImage.getHeight() - 1);
    else
      height = height * PIXELS_PER_NANOMETER;

    int iterator = (Integer)  subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_BOXFILTER_ITERATOR);   
//    Image desImage = Image.createCopy(originalImage, RegionOfInterest.createRegionFromImage(originalImage));    
    Image desImage = new Image(
      originalImage.getWidth(),
      originalImage.getHeight());
    desImage = ImageEnhancer.removeArtifactByBoxFilter(desImage, originalImage, (int)width, (int)height, iterator);
    if (desImage.hasImageDescription() == false)
      desImage.setImageDescription(new ImageDescription(""));
    desImage.getImageDescription().setParameter(_REMOVE_BACKGROUND_PROPERTY, _TRUE);

    //Please refer comment at PROPERTY_KEY declaration at above for how to implement the below checking.
    if (originalImage.hasImageDescription()
      &&  (originalImage.getImageDescription().hasParameter(_RESIZE_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_MOTION_BLUR_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_SHADING_REMOVAL_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_RFILTER_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_CLAHE_PROPERTY)))
    {
      originalImage.decrementReferenceCount();
    }
    
    if(_logTimestamp)
    {
      _timerUtil.stop();
      System.out.println("Timestamp for Remove Background : " + _timerUtil.getElapsedTimeInMillis());
    }
    return desImage;
  }
  
/**
   * @param subtype
   * @return 
   *  
   * @author Wei Chin
   */
  public static Image resizeImage(Subtype subtype, Image originalImage)
  {
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
    
    int width = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
    int height = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
    RegionOfInterest roi = RegionOfInterest.createRegionFromImage(originalImage);
    roi.scaleFromCenterXY(width, height);
    Image desImage = Image.createContiguousFloatImage(roi.getWidth(), roi.getHeight());
    desImage = ImageEnhancer.resizeLinear32bitsImage(desImage, originalImage, height, width);
    if (desImage.hasImageDescription() == false)
      desImage.setImageDescription(new ImageDescription(""));
    desImage.getImageDescription().setParameter(_RESIZE_PROPERTY, _TRUE);

    if(_logTimestamp)
    {
      _timerUtil.stop();    
      System.out.println("Timestamp for Resize : " + _timerUtil.getElapsedTimeInMillis());
    }
    return desImage;
  }
  
  /**
   * @param subtype
   * @param roi
   * @return 
   * 
   * @author Wei CHin
   */
  public static RegionOfInterest getResizeROIIfEnableResize(Subtype subtype, RegionOfInterest roi)
  {
    if (ImageProcessingAlgorithm.useResize(subtype))
    {
      int width = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
      int height = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
      roi.scaleFromCenterAlongAcross(width, height);
    }
    return roi;
  }

 /**
   * @param subtype
   * @return 
   * 
   * @author Wei Chin
   */
  public static Image applyCLAHE(Subtype subtype, Image originalImage, double blockSize)
  {
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
//    Image desImage = Image.createCopy(originalImage, RegionOfInterest.createRegionFromImage(originalImage));
    Image desImage = new Image(
      originalImage.getWidth(),
      originalImage.getHeight());

    //Siew Yeng - XCR2170 - fix CLAHE always get different block size
//    double height = subtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getHeight();
//    double width = subtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getWidth();
//        
//    if (height > width)
//      blockSize = width;
//    else
//      blockSize = height;
    
    double NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
    
    if(subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
      NANOMETERS_PER_PIXEL = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    
    int scaleFactor = AlgorithmUtil.getResizeFactor(subtype);;
    final double PIXELS_PER_NANOMETER = scaleFactor * 1.0 / NANOMETERS_PER_PIXEL;
    
    //Siew Yeng - XCR-2388 - Image Enhancer - Add threshold for Clahe and Background Filter
    int blockSizeThreshold = (Integer)  subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_CLAHE_BLOCK_SIZE);
    
    //Siew Yeng - use back the auto calculate width and height if threshold set is equal to zero.
    //Siew Yeng - XCR-2576 - limit the block size to image width/height
    if(blockSizeThreshold > 0)
    {
      int maxLimit = Math.min(originalImage.getWidth() - 1, originalImage.getHeight() - 1);
      blockSize = Math.min(blockSizeThreshold, maxLimit);
    }
    else
      blockSize = blockSize * PIXELS_PER_NANOMETER;

    desImage = ImageEnhancer.contrastLimitAdapHisEqual(desImage, originalImage, (int)blockSize, 256, 3, true);
    if (desImage.hasImageDescription() == false)
      desImage.setImageDescription(new ImageDescription(""));
    desImage.getImageDescription().setParameter(_CLAHE_PROPERTY, _TRUE);

    //Please refer comment at PROPERTY_KEY declaration at above for how to implement the below checking.
    if (originalImage.hasImageDescription()
      && (originalImage.getImageDescription().hasParameter(_RESIZE_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_MOTION_BLUR_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_SHADING_REMOVAL_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_RFILTER_PROPERTY)))
    {
      originalImage.decrementReferenceCount();
    }
    
    if(_logTimestamp)
    {
      _timerUtil.stop();
      System.out.println("Timestamp for CLAHE : " + _timerUtil.getElapsedTimeInMillis());
    }
    
    return desImage;
  }
  
 /**
   * @param subtype
   * @return 
   * 
   * @author Lim, Lay Ngor
   */
  public static Image applyFFTBandPassFilter(Subtype subtype, Image originalImage)
  {
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
    Image desImage = Image.createCopy(originalImage, RegionOfInterest.createRegionFromImage(originalImage));
//    Image desImage = new Image(
//      originalImage.getWidth(),
//      originalImage.getHeight());
//    double height = subtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getHeight();
//    double width = subtype.getPads().get(0).getShapeRelativeToPanelInNanoMeters().getBounds().getWidth();
//    double blockSize;        
//    if (height > width)
//      blockSize = width;
//    else
//      blockSize = height;
//    
//    double NANOMETERS_PER_PIXEL = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
//    
//    if(subtype.getSubtypeAdvanceSettings().getMagnificationType().equals(MagnificationTypeEnum.HIGH))
//      NANOMETERS_PER_PIXEL = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
//    
//    int scaleFactor = 1;
//    if(useResize(subtype))
//      scaleFactor = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE);
//    final double PIXELS_PER_NANOMETER = scaleFactor * 1.0 / NANOMETERS_PER_PIXEL;
//    
//    blockSize = blockSize * PIXELS_PER_NANOMETER;
    
    //Lim, Lay Ngor - actually the largeScaleInPixel should be set as blockSize + small tolerance value
    //But due to this feature is new, and user wish to control the parameter, therefore we just export the GUI.
    final int largeScaleInPixel = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_LARGESCALE);
    final int smallScaleInPixel = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SMALLSCALE);
    final int toleranceOfDirection = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_TOLERANCEOFDIRECTION);
    final int saturateValue = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SATURATEVALUE);
    
    FilterDirectionModeEnum suppressStripes = FilterDirectionModeEnum.None;
    java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_FFTBANDPASSFILTER_SUPPRESSSTRIPES);
    Assert.expect(value instanceof String);
    if(value.equals(_NONE))
      suppressStripes = FilterDirectionModeEnum.None;
    else if(value.equals(_HORIZONTAL))
      suppressStripes = FilterDirectionModeEnum.Horizontal;
    else if (value.equals(_VERTICAL))
      suppressStripes = FilterDirectionModeEnum.Vertical;
    else
      Assert.expect(false, "Unexpected Suppress Stripes value.");
        
    desImage = ImageEnhancer.FFTBandPassFilter(desImage, originalImage, largeScaleInPixel, smallScaleInPixel, 
      toleranceOfDirection, suppressStripes, true, saturateValue, InterpolationModeEnum.Linear);
    
    if (desImage.hasImageDescription() == false)
      desImage.setImageDescription(new ImageDescription(""));
    desImage.getImageDescription().setParameter(_FFT_BANDPASSFILTER_PROPERTY, _TRUE);

    //Please refer comment at PROPERTY_KEY declaration at above for how to implement the below checking.
    if (originalImage.hasImageDescription()
      && (originalImage.getImageDescription().hasParameter(_RESIZE_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_MOTION_BLUR_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_SHADING_REMOVAL_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_RFILTER_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_CLAHE_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_REMOVE_BACKGROUND_PROPERTY)))
    {
      originalImage.decrementReferenceCount();
    }
    
    if(_logTimestamp)
    {
      _timerUtil.stop();
      System.out.println("Timestamp for FFTBandPassFilter : " + _timerUtil.getElapsedTimeInMillis());
    }
    
    return desImage;
  }
  
  /**
   * @author Siew Yeng
   */
  public static Image applyRFilter(Subtype subtype, Image originalImage)
  {
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
    Image desImage = Image.createCopy(originalImage, RegionOfInterest.createRegionFromImage(originalImage));
    
    desImage = ImageEnhancer.RFilter(desImage, originalImage);
    
    if (desImage.hasImageDescription() == false)
      desImage.setImageDescription(new ImageDescription(""));
    
    desImage.getImageDescription().setParameter(_RFILTER_PROPERTY, _TRUE);

    //Please refer comment at PROPERTY_KEY declaration at above for how to implement the below checking.
    if (originalImage.hasImageDescription()&&
        (originalImage.getImageDescription().hasParameter(_RESIZE_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_MOTION_BLUR_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_SHADING_REMOVAL_PROPERTY)))
    {
      originalImage.decrementReferenceCount();
    }
    
    if(_logTimestamp)
    {
      _timerUtil.stop();
      System.out.println("Timestamp for RFilter : " + _timerUtil.getElapsedTimeInMillis());
    }
    
    return desImage;
  }

  /**
   * @author Yong Sheng Chuan
   */
  public static Image applyMotionBlur(Subtype subtype, Image originalImage)
  {
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
    final int scale = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_MASK_SCALE);
    final int offset = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_GRAYLEVEL_OFFSET);
    final int iteration = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_ITERATION);
    Image desImage = Image.createCopy(originalImage, RegionOfInterest.createRegionFromImage(originalImage));

    FilterDirectionModeEnum direction = FilterDirectionModeEnum.Horizontal;
    java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_MOTION_BLUR_DIRECTION);
    Assert.expect(value instanceof String);
    if(value.equals(_HORIZONTAL))
      direction = FilterDirectionModeEnum.Horizontal;
    else if (value.equals(_VERTICAL))
      direction = FilterDirectionModeEnum.Vertical;
    else if (value.equals(_BOTH))
      direction = FilterDirectionModeEnum.Both;
    else
      Assert.expect(false, "Unexpected motion blur direction value.");
    
    desImage = ImageEnhancer.MotionBlur(desImage, originalImage, scale, offset, direction, iteration);

    if (desImage.hasImageDescription() == false)
      desImage.setImageDescription(new ImageDescription(""));

    desImage.getImageDescription().setParameter(_MOTION_BLUR_PROPERTY, _TRUE);

    //Please refer comment at PROPERTY_KEY declaration at above for how to implement the below checking.
    if(originalImage.hasImageDescription() && 
       (originalImage.getImageDescription().hasParameter(_RESIZE_PROPERTY)))
    {
      originalImage.decrementReferenceCount();
    }

    if(_logTimestamp)
    {
      _timerUtil.stop();
      System.out.println("Timestamp for Motion Blur : " + _timerUtil.getElapsedTimeInMillis());
    }

    return desImage;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public static Image applyShadingRemoval(Subtype subtype, Image originalImage) throws DatastoreException
  {
    if(_logTimestamp)
    {
      _timerUtil.reset();
      _timerUtil.start();
    }
    final int blurDistance = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_BLUR_DISTANCE);
    final int keepOutDistance = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_KEEP_OUT_DISTANCE);
    final int desiredBackground = (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DESIRED_BACKGROUND);
    final java.io.Serializable filteringTechnique = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_FILTERING_TECHNIQUE);    
    Image desImage = Image.createCopy(originalImage, RegionOfInterest.createRegionFromImage(originalImage));

    FilterDirectionModeEnum direction = FilterDirectionModeEnum.Horizontal;
    java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_SHADING_REMOVAL_DIRECTION);
    Assert.expect(value instanceof String);
    if(value.equals(_HORIZONTAL))
      direction = FilterDirectionModeEnum.Horizontal;
    else if (value.equals(_VERTICAL))
      direction = FilterDirectionModeEnum.Vertical;
    else if (value.equals(_BOTH))
      direction = FilterDirectionModeEnum.Both;
    else
      Assert.expect(false, "Unexpected shading removal direction value.");
    
    desImage = ImageEnhancer.ShadingRemoval(desImage, originalImage, blurDistance, keepOutDistance, direction);
    
    float[] originalImageArray = Image.createArrayFromImage(originalImage, RegionOfInterest.createRegionFromImage(originalImage));
    float[] backgroundImageArray = Image.createArrayFromImage(desImage, RegionOfInterest.createRegionFromImage(desImage));
    float[] deltaGrayImageArray = ArrayUtil.subtractArrays(backgroundImageArray, originalImageArray);
    
    Assert.expect(deltaGrayImageArray.length == backgroundImageArray.length);
    
    Assert.expect(filteringTechnique instanceof String);
    float[] imageThicknessArray = new float[deltaGrayImageArray.length];
    float[] estimatedDeltaGrayArray = new float[deltaGrayImageArray.length];
    if (filteringTechnique.equals(_THICKCAL_COMPENSATION))
    {
      SolderThickness solderThickness = AlgorithmUtil.getSolderThickness(subtype);
      for (int i = 0; i < deltaGrayImageArray.length; i++)
      {
        float deltaGray = deltaGrayImageArray[i];
        float background = backgroundImageArray[i];
        if (deltaGray < 0)
        {
          deltaGray = 0.0f;
        }
        if (background < 0)
        {
          background = 0.0f;
        }

        imageThicknessArray[i] = solderThickness.getThicknessInMils(background, deltaGray);
        estimatedDeltaGrayArray[i] = solderThickness.getDeltaGrayLevel(desiredBackground, imageThicknessArray[i]);
      }

      // add desire background and do invert
      estimatedDeltaGrayArray = ArrayUtil.addArrayByConstant(estimatedDeltaGrayArray, (float) desiredBackground);
      estimatedDeltaGrayArray = ArrayUtil.subtractArrayFromConstant(estimatedDeltaGrayArray, 255);
    }
    else
    {
      estimatedDeltaGrayArray = ArrayUtil.subtractArrayFromConstant(deltaGrayImageArray, 255);
    }
    
    // stretch based on first region min max
    estimatedDeltaGrayArray = stretchImage(estimatedDeltaGrayArray);

    desImage.decrementReferenceCount();
    desImage = Image.createFloatImageFromArray(originalImage.getWidth(), originalImage.getHeight(), estimatedDeltaGrayArray);
    
    if (desImage.hasImageDescription() == false)
      desImage.setImageDescription(new ImageDescription(""));

    desImage.getImageDescription().setParameter(_SHADING_REMOVAL_PROPERTY, _TRUE);

    //Please refer comment at PROPERTY_KEY declaration at above for how to implement the below checking.
    if (originalImage.hasImageDescription() && 
        (originalImage.getImageDescription().hasParameter(_RESIZE_PROPERTY)
      || originalImage.getImageDescription().hasParameter(_MOTION_BLUR_PROPERTY)))
    {
      originalImage.decrementReferenceCount();
    }

    if(_logTimestamp)
    {
      _timerUtil.stop();
      System.out.println("Timestamp for Shading Removal : " + _timerUtil.getElapsedTimeInMillis());
    }

    return desImage;
  }
  
  /**
   * @param image
   * @return 
   * 
   * @author Wei Chin
   */
  public static boolean doesImageEnhanced(Image image)
  {
    Assert.expect(image != null);

    if (image.hasImageDescription()
      && image.getImageDescription().hasParameter(_REMOVE_BACKGROUND_PROPERTY)
      && image.getImageDescription().getParameter(_REMOVE_BACKGROUND_PROPERTY).equals(_TRUE))
    
      return true;
    else
      
      return false;
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @return 
   * 
   * @author Wei Chin
   */
  public static Collection <AlgorithmSetting> createGrayLevelEnhancementAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();
        // Added by Seng Yew on 22-Apr-2011
    ArrayList<String> updateGraylevelOption = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting updateGraylevel = new AlgorithmSetting(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE,
        displayOrder++,
        _FALSE, // default
        updateGraylevelOption, // possible values
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_UPDATE)_KEY", // desc
        "HTML_DETAILED_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_UPDATE)_KEY", // detailed desc
        "IMG_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_UPDATE)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
        currentVersion);
    settingList.add(updateGraylevel);

    // Added by Seng Yew on 22-Apr-2011
    AlgorithmSetting graylevelMinimum = new AlgorithmSetting(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM,
        displayOrder++,
        0.0f, // default
        0.0f, // min
        255.0f, // max
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_MINIMUM)_KEY", // desc
        "HTML_DETAILED_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_MINIMUM)_KEY", // detailed desc
        "IMG_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_MINIMUM)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
        currentVersion);
    settingList.add(graylevelMinimum);    

    // Added by Seng Yew on 22-Apr-2011
    AlgorithmSetting graylevelMaximum = new AlgorithmSetting(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM,
        displayOrder++,
        255.0f, // default
        0.0f, // min
        255.0f, // max
        MeasurementUnitsEnum.GRAYLEVEL,
        "HTML_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_MAXIMUM)_KEY", // desc
        "HTML_DETAILED_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_MAXIMUM)_KEY", // detailed desc
        "IMG_DESC_USER_DEFINED_(USER_DEFINED_GRAYLEVEL_MAXIMUM)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
        currentVersion);
    settingList.add(graylevelMaximum);   
    
    return settingList;
  }
  
  /**
   * @author Wei Chin
   */
  public static Collection<AlgorithmSettingEnum> getLearnedGrayLevelAlgorithmSettingEnums()
  {
    Collection<AlgorithmSettingEnum> learnedAlgorithmSettingEnums = new TreeSet<AlgorithmSettingEnum>();
    learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM);
    learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM);
    
    return learnedAlgorithmSettingEnums;
  }
  
  /**
   * @author Wei CHin
   */
  public static void updateNominals(Subtype subtype, ManagedOfflineImageSet typicalBoardImages, ManagedOfflineImageSet unloadedBoardImages, Algorithm currentAlgorithm) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);
    Assert.expect(currentAlgorithm != null);

    // Added by Seng Yew on 22-Apr-2011
    // Image normalization by subtype feature - only applicable if algorithm setttings "Update Graylevel", "Graylevel Minimum" & "Graylevel Maximum" are defined.

    if (currentAlgorithm.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE) && 
        currentAlgorithm.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM) &&
        currentAlgorithm.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM))
    {
      if (typicalBoardImages.size() == 0)
        return;

      String graylevelUpdateOption = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE);
      boolean updateGraylevel = graylevelUpdateOption.equalsIgnoreCase(_TRUE);

      if (updateGraylevel)
      {
        float minV = 255.0f;
        float maxV = 0.0f;
        ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
        ReconstructedImages reconstructedImages;
        while ((reconstructedImages = imagesIterator.getNext()) != null)
        {
          for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
          {
            float minInImage = Statistics.minValue(reconstructedSlice.getImage());
            float maxInImage = Statistics.maxValue(reconstructedSlice.getImage());
            if (minV > minInImage)
              minV = minInImage;
            if (maxV < maxInImage)
              maxV = maxInImage;
          }
          imagesIterator.finishedWithCurrentRegion();
        }
        subtype.setLearnedValue(subtype.getJointTypeEnum(), currentAlgorithm, AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM, minV);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), currentAlgorithm, AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM, maxV+1);
      }
    }
  }
  
  /**
   * @param subtype
   * @return 
   * @author Wei Chin
   */
  public static boolean useUserDefinedGrayLevel(Subtype subtype)
  {
    String graylevelUpdateOption = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_UPDATE);
    boolean updateGraylevel = graylevelUpdateOption.equalsIgnoreCase(_TRUE);
    
    return updateGraylevel;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static Image applyUserDefinedGrayLevel(Subtype subtype, Image originalImage)
  {
    Image newImage = Image.createCopy(originalImage, RegionOfInterest.createRegionFromImage(originalImage));
    float minimumGraylevel = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM);
    float maximumGraylevel = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM);
    
    Filter.normalizeWithMinMaxValue(newImage, minimumGraylevel, maximumGraylevel);
    return newImage;
  }
  
  /**
   * @return 
   * @author Wei Chin
   */
  public static void applyOriginalImageWithUserDefinedGrayLevel(Subtype subtype, Image originalImage)
  {
    float minimumGraylevel = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM);
    float maximumGraylevel = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM);
    
    Filter.normalizeWithMinMaxValue(originalImage, minimumGraylevel, maximumGraylevel);
  }
  
  /**
   * @param displayOrder
   * @param currentVersion
   * @return 
   * 
   * @author Wei Chin
   */
  public static Collection<AlgorithmSetting> createBackgroundSensitivitySettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList();
    
    AlgorithmSetting backgroundSensitivityThreshold = new AlgorithmSetting(
        AlgorithmSettingEnum.BACKGROUND_SENSITIVITY_VALUE,
        displayOrder++,
        0, // default value
        0, // minimum value
        254, // maximum value
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_IMAGE_ENHANCEMENT_(BACKGROUND_SENSITIVITY_VALUE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(BACKGROUND_SENSITIVITY_VALUE)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(BACKGROUND_SENSITIVITY_VALUE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(backgroundSensitivityThreshold);

    return settingList;
  }

  /**
   * Set to save the Diagnostic image for production
   * @author Wei Chin
   */
  public static AlgorithmSetting createSaveDiagnoticVoidImageAlgorithmSetting(int displayOrder, int currentVersion)
  {
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableSaveDiagnosticImage = new AlgorithmSetting(
        AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES,
        displayOrder++,
        _FALSE,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(SAVE_VOID_DIAGNOSTIC_IMAGES)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(SAVE_VOID_DIAGNOSTIC_IMAGES)_KEY", // desailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(SAVE_VOID_DIAGNOSTIC_IMAGES)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    
    return enableSaveDiagnosticImage;
  }
  
  /**
   * @param subtype
   * @return 
   * @author Wei Chin
   */
  public static boolean needToSaveVoidDiagnosticImage(Subtype subtype)
  {
    boolean needToSaveVoidDiagnosticImage = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          needToSaveVoidDiagnosticImage = false;
        else if (value.equals(_TRUE))
          needToSaveVoidDiagnosticImage = true;
        else
          Assert.expect(false, "Unexpected Save Void Diagnostic Image value.");
    }
    return needToSaveVoidDiagnosticImage;
  }
  
  /**
   * Set to save the Enhanced image for production
   * @author Siew Yeng
   */
  public static AlgorithmSetting createSaveEnhancedImageAlgorithmSetting(int displayOrder, int currentVersion)
  {
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_FALSE, _TRUE));
    AlgorithmSetting enableSaveDiagnosticImage = new AlgorithmSetting(
        AlgorithmSettingEnum.SAVE_ENHANCED_IMAGE,
        displayOrder++,
        _FALSE,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_IMAGE_ENHANCEMENT_(SAVE_ENHANCED_IMAGE)_KEY", // description URL key
        "HTML_DETAILED_DESC_IMAGE_ENHANCEMENT_(SAVE_ENHANCED_IMAGE)_KEY", // detailed description URL key
        "IMG_DESC_IMAGE_ENHANCEMENT_(SAVE_ENHANCED_IMAGE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    
    return enableSaveDiagnosticImage;
  }

  /**
   * @param subtype
   * @author Siew Yeng
   */
  public static boolean needToSaveEnhancedImage(Subtype subtype)
  {
    boolean needToSaveEnahncedImage = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.SAVE_ENHANCED_IMAGE))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SAVE_ENHANCED_IMAGE);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          needToSaveEnahncedImage = false;
        else if (value.equals(_TRUE))
          needToSaveEnahncedImage = true;
        else
          Assert.expect(false, "Unexpected Save Enhanced Image value.");
    }
    return needToSaveEnahncedImage;
  }
  
  /**
   * Add enhanced image into restructedImages as enhanced image slice
   * @author Siew Yeng
   */
  public static void addEnhancedImageIntoReconstructedImages(ReconstructedImages reconstructedImages, SliceNameEnum sliceNameEnum)
  {
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    
    if(reconstructedSlice.hasEnhancedImageForInspection())
    {
      SliceNameEnum enhancedImageSliceName = EnumToUniqueIDLookup.getInstance().getEnhancedImageSliceNameEnum(sliceNameEnum);
      //if there is rotation in the slice, we only want the enhance image but without the rotation feature.
      if(reconstructedSlice.hasEnhancedImageWithoutRotationForVVTS())
        reconstructedImages.addImage(reconstructedSlice.getEnhancedImageWithoutRotation(), enhancedImageSliceName);
      else
         reconstructedImages.addImage(reconstructedSlice.getOrthogonalImage(), enhancedImageSliceName);
    }
  }
  
  /**
   * @param subtype
   * @return 
   * @author Lim, Lay Ngor - BrokenPin
   */
  public static boolean needToSaveForeignInclusionDiagnosticImage(Subtype subtype)
  {
    boolean needToSaveForeignInclusionDiagnosticImage = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES))
    {
        java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SAVE_DIAGNOSTIC_IMAGES);
        Assert.expect(value instanceof String);
        if (value.equals(_FALSE))
          needToSaveForeignInclusionDiagnosticImage = false;
        else if (value.equals(_TRUE))
          needToSaveForeignInclusionDiagnosticImage = true;
        else
          Assert.expect(false, "Unexpected Save Void Diagnostic Image value.");
    }
    return needToSaveForeignInclusionDiagnosticImage;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public static float[] stretchImage(float[] imageArray)
  {
    Assert.expect(imageArray != null);
    float min = ArrayUtil.min(imageArray);
    float max = ArrayUtil.max(imageArray);
    float range = max - min;
    if (range == 0)
      return imageArray;
    
    float[] newImage = new float[imageArray.length];
    for (int i = 0; i < newImage.length; ++i)
    {
      newImage[i] = (255*(imageArray[i] - min)) / range;
    }

    return newImage;
  }
}