package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import com.axi.util.Pair;
import java.util.List;
import java.util.ArrayList;

/**
 * This class contains information about a region labeled by the clusterContiguousRegions routine. It is a
 * helper class for ExpectedImageVoidingAlgorithm.
 *
 * This was ported from 5DX code written by Patrick Lacz.
 *
 * A pixelList was added to streamline region operations.  The 5DX code would do an iterative
 * search around the initial x,y values to find other values to convert. That was slow.
 *
 * @author George Booth
 */
public class LabelRegion
{
  private int _regionId = 0;
  private int _numberOfPixels = 0;
  private List<Pair<Integer, Integer>> _pixelList;

  /**
   * @author George Booth
   */
  public LabelRegion(int newRegionId, int newRootX, int newRootY)
  {
    _regionId = newRegionId;
    _numberOfPixels = 1;
    _pixelList = new ArrayList<Pair<Integer,Integer>>();
    _pixelList.add(new Pair<Integer,Integer>(newRootX, newRootY));
  }

  /**
   * @author George Booth
   */
  public void addPixel(int x, int y)
  {
    ++_numberOfPixels;
    _pixelList.add(new Pair<Integer,Integer>(x, y));
  }

  /**
   * @author George Booth
   */
  public int getNumberOfPixels()
  {
    return _numberOfPixels;
  }

  /**
   * @author George Booth
   */
  public int getRegionId()
  {
    return _regionId;
  }

  /**
   * @author George Booth
   */
  public List<Pair<Integer, Integer>> getRegionPixelList()
  {
    return _pixelList;
  }

}
