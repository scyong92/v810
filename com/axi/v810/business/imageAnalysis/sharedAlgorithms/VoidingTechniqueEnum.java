package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import com.axi.util.*;

/**
 * @author Rex Shang
 */
public class VoidingTechniqueEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static VoidingTechniqueEnum MAXIMUM_THICKNESS = new VoidingTechniqueEnum(++_index);
  public static VoidingTechniqueEnum MINIMUM_THICKNESS = new VoidingTechniqueEnum(++_index);
  public static VoidingTechniqueEnum THRESHOLD_MASK = new VoidingTechniqueEnum(++_index);

  /**
   * @author Rex Shang
   */
  private VoidingTechniqueEnum(int id)
  {
    super(id);
  }

}
