package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;


/**
 * This algorithm is an adaptation of the 5dx 8.4.1 FET Voiding algorithm by Kay Lannen. In the 5dx code it is called the
 * 'Pool' Voiding algorithm.  I chose to name it the flood algorithm because I think it is slightly more descriptive.
 *
 * The algorithm is fairly straight forward:
 * <ul>
 * <li>The pixels are visited in order, from brightest to darkest.
 * <li>If a pixel, when visited, borders a 'dry'/boundary pixel, it is labelled as dry - water can drain out of the ROI.
 * <li>Otherwise we create or join a 'pool' depending on our neighbors.
 * <li>The pools are examined and the small ones discarded.
 * </ul>
 *
 * This class is designed to be inheirited from by the different algorithm families. By default, it will use the same settings on all
 * the inspectable slices for whatever joint type it is applied to.  If a family wants to provide per-slice settings, it should
 * inheirit and overwrite the appropriate methods.
 *
 * @todo PWL : This algorithm isn't finished, but it's pretty close. It would be a good replacement for the Large Pad Voiding algorithm.
 *
 * @author Patrick Lacz
 */
public class FloodVoidingAlgorithm extends Algorithm
{

  protected final int _ALGORITHM_VERSION = 1;

  private final float _DRY_PIXEL_INTENSITY = 0.f;
  private final float _VOID_PIXEL_INTENSITY = 200.f;

  /**
   * @author Patrick Lacz
   */
  public FloodVoidingAlgorithm(InspectionFamilyEnum inspectionFamilyEnum, InspectionFamily inspectionFamily)
  {
    super(AlgorithmEnum.VOIDING, inspectionFamilyEnum);

    createAlgorithmSettings(inspectionFamily);
    addMeasurementEnums();
  }

  /**
   * @author Patrick Lacz
   */
  protected void createAlgorithmSettings(InspectionFamily inspectionFamily)
  {
    int displayOrder = 0;

    // Noise Reduction
    // How much to try to compensate for noise.
    // 0 = no noise reduction
    // 1 = apply blur
    // 2+ = apply blur & NR-1 cycles of opening (dilate/erode cycles) to the void pixels image.
    // The opening will remove smaller features from the image and generally make the regions appear
    // more blocky than without NR.
    //  I expect that this value will normally be 0 to 2.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION,
        displayOrder++,
        2, // default value
        0, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));
  }

  /**
   * Override this method if you have changed the algorithm settings for this class (such as providing per-slice settings).
   * @author Patrick Lacz
   */
  protected int getNoiseReductionSettingForSlice(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    return ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION)).intValue();
  }

  /**
   * Override this method if you have changed the measurement enums for this class (such as providing per-slice settings).
   * @author Patrick Lacz
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);

    if (Config.isComponentLevelClassificationEnabled())
    {
      _componentMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    }
    else
    {
      _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT);
    }
  }

  /**
   * Override this method to change which slices this algorithm runs on (and in which order). By default, it runs on all the slices in
   * the reconstruction region.
   *
   * @author Patrick Lacz
   */
  protected List<SliceNameEnum> selectInspectedSlices(Subtype subtype, ReconstructedImages reconstructedImages)
  {
    Assert.expect(subtype != null);
    Assert.expect(reconstructedImages != null);

    List<SliceNameEnum> sliceNamesToInspect = new LinkedList<SliceNameEnum>();
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getInspectedReconstructedSlices())
      sliceNamesToInspect.add(reconstructedSlice.getSliceNameEnum());

    return sliceNamesToInspect;
  }

  /**
   * @author Patrick Lacz
   * @author Jack Hwee
   */
  public void classifyJoints(ReconstructedImages reconstructedImages, List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    if (jointInspectionDataObjects.isEmpty())
    {
      // nothing to inspect
      return;
    }
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    for (SliceNameEnum sliceNameEnum : selectInspectedSlices(subtype, reconstructedImages))
    {
      int noiseReduction = getNoiseReductionSettingForSlice(subtype, sliceNameEnum);
      float voidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD);
      float individualVoidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD);
      MeasurementEnum percentMeasurementEnum = MeasurementEnum.LARGE_PAD_VOIDING_PERCENT;
        MeasurementEnum individualpercentMeasurementEnum = MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT;

      measureVoidingOnSlice(reconstructedImages,
                            sliceNameEnum,
                            jointInspectionDataObjects,
                            subtype,
                            noiseReduction,
                            voidAreaFailThreshold,
                            individualVoidAreaFailThreshold,
                            percentMeasurementEnum,
                            individualpercentMeasurementEnum);
    }
  }

  /**
   * @author Patrick Lacz
   * @author Jack Hwee
   */
  protected void measureVoidingOnSlice(ReconstructedImages reconstructedImages,
                                       SliceNameEnum sliceNameEnum,
                                       List<JointInspectionData> jointInspectionDataObjects,
                                      Subtype subtype,
                                      int noiseReduction,
                                      float voidAreaFailThreshold,
                                      float individualVoidAreaFailThreshold,
                                      MeasurementEnum percentMeasurementEnum,
                                      MeasurementEnum individualpercentMeasurementEnum)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(subtype != null);
    Assert.expect(noiseReduction >= 0 && noiseReduction <= 5);

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, jointInspectionData.getInspectionRegion(), reconstructedSlice, false);

      RegionOfInterest locatedJointRegionInSliceImage = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

//      locatedJointRegionInSliceImage.setWidthKeepingSameCenter(locatedJointRegionInSliceImage.getWidth()+4);
//      locatedJointRegionInSliceImage.setHeightKeepingSameCenter(locatedJointRegionInSliceImage.getHeight()+4);

      Image jointImage = reconstructedSlice.getOrthogonalImage();
      jointImage.incrementReferenceCount();
      RegionOfInterest jointRoi = locatedJointRegionInSliceImage;
      if (noiseReduction >= 1)
      {
        // blur the image
        Image blurredJointImage = Filter.convolveLowpass(jointImage, jointRoi);
        jointImage.decrementReferenceCount();
        jointImage = blurredJointImage;
        jointRoi = RegionOfInterest.createRegionFromImage(jointImage);
      }

      Image dryPixelsImage = labelDryPixels(jointImage, jointRoi);

      if (noiseReduction >= 2)
      {
        int openingSize = (noiseReduction - 1) * 2;

        Image openedDryPixelsImage = Filter.opening(dryPixelsImage, RegionOfInterest.createRegionFromImage(dryPixelsImage), openingSize);
        dryPixelsImage.decrementReferenceCount();
        dryPixelsImage = openedDryPixelsImage;
      }

      //Jack Hwee- individual void
   //   BufferedImage  dryPixelsBufferedImage = dryPixelsImage.getBufferedImage();

      // count the number of "void" pixels
      int numberOfVoidPixels = Threshold.countPixelsInRange(dryPixelsImage, _DRY_PIXEL_INTENSITY+0.5f, _VOID_PIXEL_INTENSITY);

      // Store the percent voiding
      int numberOfPixelsTested = jointRoi.getWidth()*jointRoi.getHeight();
      float voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;

      // Jack Hwee- individual void
       int numberOfIndividualVoidPixels = Threshold.individualVoid (dryPixelsImage,individualVoidAreaFailThreshold, numberOfPixelsTested);
       float individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / numberOfPixelsTested;

      JointInspectionResult jointResult = jointInspectionData.getJointInspectionResult();
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
          percentMeasurementEnum,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          jointInspectionData.getPad(), sliceNameEnum, voidPercent);
      jointResult.addMeasurement(voidPercentMeasurement);

      //Jack Hwee- individual void
       JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
          individualpercentMeasurementEnum,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          jointInspectionData.getPad(), sliceNameEnum, individualVoidPercent);
      jointResult.addMeasurement(individualVoidPercentMeasurement);

      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, jointInspectionData, voidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     new OverlayImageDiagnosticInfo(dryPixelsImage, locatedJointRegionInSliceImage));

         //Jack Hwee- individual void
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, jointInspectionData, individualVoidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     false,
                                     new OverlayImageDiagnosticInfo(dryPixelsImage, jointRoi));
      }
      jointImage.decrementReferenceCount();
      dryPixelsImage.decrementReferenceCount();

      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > voidAreaFailThreshold);
       boolean individualJointFailed = (individualVoidPercent > individualVoidAreaFailThreshold);

      if (jointFailed)
      {
        JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
            this, sliceNameEnum);
        voidPercentIndictment.addFailingMeasurement(voidPercentMeasurement);
        jointResult.addIndictment(voidPercentIndictment);
      }

       if (individualJointFailed)
      {
        JointIndictment individualVoidPercentIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING,
            this, sliceNameEnum);
        individualVoidPercentIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        jointResult.addIndictment(individualVoidPercentIndictment);
      }

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, jointInspectionData, reconstructionRegion, sliceNameEnum, !jointFailed);
       AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, jointInspectionData, reconstructionRegion, sliceNameEnum, !individualJointFailed);
    }
  }

  /**
   * @author Patrick Lacz
   */
  private Image labelDryPixels(Image sliceImage, RegionOfInterest jointRoiInSlice)
  {
    Assert.expect(sliceImage != null);
    Assert.expect(jointRoiInSlice != null);

    final float INTENSITY_HIGHER_THAN_NORMAL_VALUES = 300.f;

    Image jointImagePlusBoundary = new Image(jointRoiInSlice.getWidth() + 2, jointRoiInSlice.getHeight() + 2);
    Paint.fillImage(jointImagePlusBoundary, INTENSITY_HIGHER_THAN_NORMAL_VALUES);

    RegionOfInterest jointRoi = new RegionOfInterest(jointRoiInSlice);
    jointRoi.setMinXY(1, 1);

    Image jointImageRaw = Image.createCopy(sliceImage, jointRoiInSlice);
    Image jointImageBlurred = Filter.convolveLowpass(jointImageRaw);

    Transform.copyImageIntoImage(jointImageBlurred, RegionOfInterest.createRegionFromImage(jointImageBlurred), jointImagePlusBoundary, jointRoi);
    jointImageBlurred.decrementReferenceCount();
    // keep the jointImageRaw around so we can use it as the return value (avoiding some fragmentation)

    // perform a flood fill along the edge of the jointRoi. We've set the boundary so that the flood fill should hit every pixel along the boundary,
    // and thus every pixel along the border of the image.
    Paint.floodFillGradient(jointImagePlusBoundary,
                            RegionOfInterest.createRegionFromImage(jointImagePlusBoundary),
                            0, 0,
                            _DRY_PIXEL_INTENSITY,
                            2.f * INTENSITY_HIGHER_THAN_NORMAL_VALUES, 0.25f);

    Transform.copyImageIntoImage(jointImagePlusBoundary, jointRoi, jointImageRaw, RegionOfInterest.createRegionFromImage(jointImageRaw));
    jointImagePlusBoundary.decrementReferenceCount();
    return jointImageRaw;
  }

  /**
   * @author Patrick Lacz
   */
  private boolean isPixelDry(Image dryPixelImage, int x, int y)
  {
    Assert.expect(dryPixelImage != null);
    if (x < 0 || x >= dryPixelImage.getWidth() || y < 0 || y >= dryPixelImage.getHeight())
      return true;

    float value = dryPixelImage.getPixelValue(x, y);
    if (value == _DRY_PIXEL_INTENSITY)
      return true;
    return false;
  }

  /**
   * For this algorithm, we want to visit all the potentially voiding pixels in order from most likely to least likely.
   * This is from lightest to darkest. On the 5dx, this was from lowest intensity to highest, on genesis, this is from highest to lowest.
   *
   * We don't want to do work for the pixels outside of the region, so don't add the 'dry' pixels.
   *
   * maskImage may be null, if you don't want to eliminate the 'dry' pixels.
   * @author Patrick Lacz
   */
  private List<Pair<Float, ImageCoordinate>> createListOfPixelsSortedByIntensity(Image intensitySourceImage,
                                                                                   RegionOfInterest sourceRoi,
                                                                                   Image maskImage)
  {
    Assert.expect(intensitySourceImage != null);
    Assert.expect(sourceRoi != null);
    if (maskImage != null)
    {
      Assert.expect(sourceRoi.getWidth() == maskImage.getWidth());
      Assert.expect(sourceRoi.getHeight() == maskImage.getWidth());

    }
    int width = sourceRoi.getWidth();
    int height = sourceRoi.getHeight();

    int sourceRoiMinX = sourceRoi.getMinX();
    int sourceRoiMinY = sourceRoi.getMinY();

    List<Pair<Float, ImageCoordinate>> pixelList = new ArrayList<Pair<Float,ImageCoordinate>>(width*height);

    for (int x = 0 ; x < width ; ++x)
    {
      for (int y = 0 ; y < height ; ++y)
      {
        boolean addThisPixel = true;
        if (maskImage != null)
          addThisPixel = isPixelDry(maskImage, x, y);

        if (addThisPixel == true)
        {
          float intensity = intensitySourceImage.getPixelValue(sourceRoiMinX + x, sourceRoiMinY + y);
          ImageCoordinate imageCoordinate = new ImageCoordinate(x, y);
          pixelList.add(new Pair<Float, ImageCoordinate>(intensity, imageCoordinate));
        }
      }
    }

    Collections.sort(pixelList, Collections.reverseOrder(new PairComparator<Float, ImageCoordinate>()));

    return pixelList;
  }

 

}
