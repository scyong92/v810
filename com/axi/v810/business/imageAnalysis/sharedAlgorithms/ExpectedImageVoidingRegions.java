package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;

/**
 *
 * <p>Title: ExpectedImageVoidingRegions</p>
 *
 * <p>Description: This class is a helper class for the ExpectedImageVoidingAlgorithm. It is used to
 * group regions of void pixels for the purpose of eliminating very small voids. </p>
 *
 * <p>It is called from the method removeVoidsSmallerThanNPixels()</p>
 *
 * <p>This is based on 5DX code written by Patrick Lacz and Peter Esbensen.</p>
 *
 * <p>Copyright: Copyright (c) 2008</p>
 *
 * <p>Company: Agilent Technogies</p>
 *
 * @author George Booth
 * @version 1.0
 */
public class ExpectedImageVoidingRegions
{
  private static final float ROUNDING_EPSILON = 0.001f;

  public ExpectedImageVoidingRegions()
  {
    // do nothing
  }

  /**
   * Move all the pixels in the destroyed region into the target region.
   * This modifies the buffer and the target region object.
   *
   * @author George Booth
   */
  static void convertRegion(float[] pixelArray,
                            int arrayWidth,
                            LabelRegion destroyedRegion,
                            LabelRegion targetRegion)
  {
    Assert.expect(pixelArray != null);
    Assert.expect(destroyedRegion != null);
    Assert.expect(targetRegion != null);
    Assert.expect(destroyedRegion.getRegionId() != targetRegion.getRegionId());

    // move pixels from destroyedRegion to targetRegion and update the image pixels with the new region
    float targetRegionId = (float)targetRegion.getRegionId();
    List<Pair<Integer, Integer>> destroyedPixelList = destroyedRegion.getRegionPixelList();
    for (Pair<Integer, Integer> pixel : destroyedPixelList)
    {
      int x = pixel.getFirst();
      int y = pixel.getSecond();
      targetRegion.addPixel(x, y);
      pixelArray[y * arrayWidth + x] = targetRegionId;
    }
  }

  /**
   * Set all the pixels in a region to not be part of any region.
   * This modifies the buffer.
   *
   * @author George Booth
   */
  static void clearRegion(float[] pixelArray,
                          int arrayWidth,
                          LabelRegion destroyedRegion)
  {
    Assert.expect(pixelArray != null);
    Assert.expect(destroyedRegion != null);

    // update the image pixels to show they aren't in a region
    List<Pair<Integer, Integer>> destroyedPixelList = destroyedRegion.getRegionPixelList();
    for (Pair<Integer, Integer> pixel : destroyedPixelList)
    {
      int x = pixel.getFirst();
      int y = pixel.getSecond();
      pixelArray[y * arrayWidth + x] = 0.0f;
    }
  }

  /**
   * Labels contiguous groups of pixels in the buffer greather than the clusterAboveThreshold.
   * Only looks at the 4-connected neighborhood to determine contiguity.
   * Pixels not in a region are assigned to 0.0, other pixels are assigned to their region id (cast to a float).

   * Returns the number of regions found and modifies the outRegionMap appropriately.
   *
   * This was ported from 5DX code written by Patrick Lacz.
   *
   * @author George Booth
   */
  static int clusterContiguousRegions(float[] pixelArray,
                                      RegionOfInterest region,
                                      float clusterAboveThreshold,
                                      Map<Integer, LabelRegion> outRegionMap)
  {
    Assert.expect(pixelArray != null);
    Assert.expect(region != null);
    Assert.expect(outRegionMap != null);

    int arrayWidth = region.getLengthAlong();
    int arrayHeight = region.getLengthAcross();
    int pixelArraySize = arrayWidth * arrayHeight;
    int minX = 0;
    int maxX = arrayWidth - 1;
    int minY = 0;
    int maxY = arrayHeight - 1;
    Assert.expect(pixelArray.length == pixelArraySize);

    outRegionMap.clear();

    final int NOT_IN_A_REGION = 0;
    int highestRegionId = 0;

    int pixelIndex = 0;
    for (int y = minY; y <= maxY; ++y)
    {
      for (int x = minX; x <= maxX; ++x)
      {
        float thisPixelValue = pixelArray[pixelIndex];
        if (thisPixelValue <= clusterAboveThreshold)
        {
          pixelArray[pixelIndex] = 0.0f;
          pixelIndex++;
          continue;
        }

        int leftRegionId = NOT_IN_A_REGION;
        if (x > minX)
        {
          // back one in x dimension
          leftRegionId = (int)(pixelArray[pixelIndex - 1] + ROUNDING_EPSILON);
        }
        int upRegionId = NOT_IN_A_REGION;
        if (y > minY)
        {
          // back one in y dimension
          upRegionId = (int)(pixelArray[pixelIndex - arrayWidth] + ROUNDING_EPSILON);
        }

        int assignRegionId = NOT_IN_A_REGION;
        if (leftRegionId == NOT_IN_A_REGION && upRegionId == NOT_IN_A_REGION)
        {
          // create a new region - neither neighbor is in a region
          assignRegionId = ++highestRegionId;
          outRegionMap.put(assignRegionId, new LabelRegion(assignRegionId, x, y));
        }
        else if (leftRegionId == upRegionId)
        {
          // regions agree, just add the pixel to that common region
          assignRegionId = leftRegionId;

          LabelRegion labelRegion = outRegionMap.get(assignRegionId);
          Assert.expect(labelRegion != null);
          labelRegion.addPixel(x, y);
        }
        else if (leftRegionId == NOT_IN_A_REGION || upRegionId == NOT_IN_A_REGION)
        {
          // only one of the regions is assigned - use that region.
          if (leftRegionId == NOT_IN_A_REGION)
            assignRegionId = upRegionId;
          if (upRegionId == NOT_IN_A_REGION)
            assignRegionId = leftRegionId;

          LabelRegion labelRegion = outRegionMap.get(assignRegionId);
          Assert.expect(labelRegion != null);
          labelRegion.addPixel(x, y);
        }
        else
        {
          // both regions have regions assigned to them - and they don't agree.
          // merge them and get rid of one of the regions.

          pixelArray[pixelIndex] = 0.0f; // assign so that there's no chance it will be overwritten.

          // add the pixel to that up region
          assignRegionId = upRegionId;
          LabelRegion assignRegion = outRegionMap.get(assignRegionId);
          Assert.expect(assignRegion != null);
          assignRegion.addPixel(x,y);

          // we purposefully are getting rid of the left region and assigning it to the up region.
          // with the order we are visiting the pixels, this assures us that we don't erronously assign
          // any unvisited pixels to the right of the current pixel.
          int destroyRegionId = leftRegionId;
          LabelRegion destroyRegion = outRegionMap.get(destroyRegionId);
          convertRegion(pixelArray, arrayWidth, destroyRegion, assignRegion);
          outRegionMap.remove(destroyRegionId);
        }

        // Write the region into the buffer.
        pixelArray[pixelIndex] = assignRegionId;

        pixelIndex++;
      } // end x loop
    } // end y loop

    return outRegionMap.size();
  }

}


