package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.chip.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Shared short algorithm.
 *
 * @author Matt Wharton
 */
public abstract class SharedShortAlgorithm extends Algorithm
{
  // Keeps track of lightest images.
  protected static Map<ReconstructionRegion, ReconstructedImages> _reconstructionRegionToLightestImagesMap =
      Collections.synchronizedMap(new HashMap<ReconstructionRegion,ReconstructedImages>());

  // Keeps track of which joints we've issued "no learning available" warnings for.
  protected static Collection<Pair<JointInspectionData, SliceNameEnum>> _jointsAndSlicesWarnedOn =
      Collections.synchronizedList(new LinkedList<Pair<JointInspectionData, SliceNameEnum>>());

  // Maximum allowable thickness when checking for "whole region" shorts.
  protected static final float _LARGE_SHORT_MAX_THICKNESS_IN_MILLIS = MathUtil.convertMilsToMillimeters(6f);

  protected static final float _GRAY_LEVEL_BACKGROUND_PERCENTILE = 0.6f;

  // Default smoothing kernel size.
  protected static final int _SMOOTHING_KERNEL_LENGTH = 3;

  private static final boolean _LOG_ALGORITHM_PROFILE_DATA =
      Config.getInstance().getBooleanValue(SoftwareConfigEnum.LOG_ALGORITHM_PROFILE_DATA);

  // Keeps track of applicable default minimum thickness settings for various joint types.
  private static Map<JointTypeEnum, Float> _jointTypeEnumToDefaultMinimumShortThicknessInMillisSettings;

  // Keeps track of applicable default minimum high short thickness settings for various joint types.
  private static Map<JointTypeEnum, Float> _jointTypeEnumToDefaultMinimumHighShortThicknessInMillisSettings;

  // Internal debugging flags.
  protected static final boolean _MDW_DEBUG = false;
  protected static final boolean _DEVELOPER_DEBUG = Config.isDeveloperDebugModeOn();

  private static ArrayList<JointTypeEnum> _JOINT_TYPES_ALLOWING_HIGH_SHORT_DETECTION = new ArrayList<JointTypeEnum>(
      Arrays.asList(JointTypeEnum.CGA,
                    JointTypeEnum.THROUGH_HOLE,
                    JointTypeEnum.OVAL_THROUGH_HOLE, //Siew Yeng - XCR-3318 - Oval PTH
                    JointTypeEnum.GULLWING));

  //Lim, Lay Ngor - XCR1743:Benchmark - START  
  private static ArrayList<JointTypeEnum> _JOINT_TYPES_ALLOWING_CENTER_SOLDER_BALL_SHORT_DETECTION = 
    new ArrayList<JointTypeEnum>(
      Arrays.asList(JointTypeEnum.RESISTOR
                    //, JointTypeEnum.CAPACITOR //beware of opaque capcitor
    ));
  //Lim, Lay Ngor - XCR1743:Benchmark - END

  //Lim, Lay Ngor - XCR2100:Broken Pin - START
    private static ArrayList<JointTypeEnum> _JOINT_TYPES_ALLOWING_BROKEN_PIN_SHORT_DETECTION = 
    new ArrayList<JointTypeEnum>(
      Arrays.asList(JointTypeEnum.THROUGH_HOLE,
                    JointTypeEnum.OVAL_THROUGH_HOLE // Siew Yeng - XCR-3318 - Oval PTH
    ));
   //Lim, Lay Ngor - XCR2100:Broken Pin - END
    
  //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
  //Change the hard-coded minimum short length default value from 3 pixels to a fix 0.057 float value so that
  //when it is convert to pixel while actual algo checking, it will follows the magnification setting.
  static protected final float _MINIMUM_SHORT_LENGTH_DEFAULT_VALUE_IN_MILLIS = 0.057f;
  // We'll call any short which spans for at least 3 pixels. (old var name is "minimumShortWidthInPixels")
//  static protected final int MINIMUM_SHORT_WIDTH_IN_PIXELS = 3;
  //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END
  
  //Siew Yeng - XCR-3318 - move from CircularShortAlgorithm
  protected static String _PIN_SLICE = "Pin";
  protected static String _COMPONENT_SLICE = "Component";
  protected static String _BOTH_SLICES = "Both";
  protected static String _PROJECTION_SLICE_ONLY = "2.5D Only"; //Broken Pin
  protected static String _PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES = "Pin, Component, 2.5D"; //Broken Pin
  // lam
  protected static String _ON = "On";
  protected static String _OFF = "Off";

  /**
   * Internal enumeration used to identify the different sides we take background profiles of.
   *
   * @author Matt Wharton
   */
  protected static class ShortProfileSideEnum extends com.axi.util.Enum
  {
    private static int _index = 0;

    public static final ShortProfileSideEnum RING_PROFILE = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum HEAD_PROFILE = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum TAIL_PROFILE = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum LEFT_PROFILE = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum RIGHT_PROFILE = new ShortProfileSideEnum(_index++);
    //Siew Yeng - XCR-3318 - Oval PTH
    public static final ShortProfileSideEnum TOP_RIGHT_INNER_CORNER_PROFILE = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum BOTTOM_RIGHT_INNER_CORNER_PROFILE = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum TOP_LEFT_INNER_CORNER_PROFILE = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum BOTTOM_LEFT_INNER_CORNER_PROFILE = new ShortProfileSideEnum(_index++);

	//Lim, Lay Ngor - XCR1743:Benchmark
    private static final Map<Integer, ShortProfileSideEnum> IdToEnum = new HashMap<Integer, ShortProfileSideEnum>();
    //Multiple center ROI - simply prepare maximum 8 separated profile
    //Please update IdToEnum.put() and getMaxSupportedCenterProfileEnum() when add in new CenterProfile enum
    public static final ShortProfileSideEnum CENTER_PROFILE_1 = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum CENTER_PROFILE_2 = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum CENTER_PROFILE_3 = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum CENTER_PROFILE_4 = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum CENTER_PROFILE_5 = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum CENTER_PROFILE_6 = new ShortProfileSideEnum(_index++);
    public static final ShortProfileSideEnum CENTER_PROFILE_7 = new ShortProfileSideEnum(_index++);    
    public static final ShortProfileSideEnum CENTER_PROFILE_8 = new ShortProfileSideEnum(_index++);        
    
    static 
    {
      IdToEnum.put(RING_PROFILE.getId(), RING_PROFILE);
      IdToEnum.put(HEAD_PROFILE.getId(), HEAD_PROFILE);
      IdToEnum.put(TAIL_PROFILE.getId(), TAIL_PROFILE);
      IdToEnum.put(LEFT_PROFILE.getId(), LEFT_PROFILE);
      IdToEnum.put(RIGHT_PROFILE.getId(), RIGHT_PROFILE);
      //Siew Yeng - XCR-3318 - Oval PTH
      IdToEnum.put(TOP_RIGHT_INNER_CORNER_PROFILE.getId(), TOP_RIGHT_INNER_CORNER_PROFILE);
      IdToEnum.put(BOTTOM_RIGHT_INNER_CORNER_PROFILE.getId(), BOTTOM_RIGHT_INNER_CORNER_PROFILE);
      IdToEnum.put(TOP_LEFT_INNER_CORNER_PROFILE.getId(), TOP_LEFT_INNER_CORNER_PROFILE);
      IdToEnum.put(BOTTOM_LEFT_INNER_CORNER_PROFILE.getId(), BOTTOM_LEFT_INNER_CORNER_PROFILE);
      IdToEnum.put(CENTER_PROFILE_1.getId(), CENTER_PROFILE_1);
      IdToEnum.put(CENTER_PROFILE_2.getId(), CENTER_PROFILE_2);
      IdToEnum.put(CENTER_PROFILE_3.getId(), CENTER_PROFILE_3);
      IdToEnum.put(CENTER_PROFILE_4.getId(), CENTER_PROFILE_4);
      IdToEnum.put(CENTER_PROFILE_5.getId(), CENTER_PROFILE_5);
      IdToEnum.put(CENTER_PROFILE_6.getId(), CENTER_PROFILE_6);
      IdToEnum.put(CENTER_PROFILE_7.getId(), CENTER_PROFILE_7);
      IdToEnum.put(CENTER_PROFILE_8.getId(), CENTER_PROFILE_8);      
    }

    /**
     * @author Matt Wharton
     */
    private ShortProfileSideEnum(int id)
    {
      super(id);
    }

    /**
	 * Get maximum supported center ROI(multiple center roi use).
     * @author Lim, Lay Ngor - XCR1743:Benchmark
     */
    public static int getMaxSupportedCenterProfileEnum()
    {
      return (CENTER_PROFILE_8.getId() - CENTER_PROFILE_1.getId() + 1) ;
    }
    
    /**
	 * Return the profile enum id.
     * @author Lim, Lay Ngor - XCR1743:Benchmark
     */
    public static ShortProfileSideEnum getEnum(int id)
    {
      return IdToEnum.get(id);
    }
  }

  /**
   * Returns true if the joint type supports high short detection.  For those joint types, it is still up to the
   * user whether or not to actually test for high shorts, so even if this returns true, you still need to check the
   * user setting.
   *
   * @author Peter Esbensen
   */
  public static boolean allowHighShortDetection(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    if (_JOINT_TYPES_ALLOWING_HIGH_SHORT_DETECTION.contains(jointTypeEnum))
      return true;
    return false;
  }

  /**
   * Returns true if the joint type supports center solder ball short detection.  For those joint types, it is still up to the
   * user whether or not to actually test for high shorts, so even if this returns true, you still need to check the
   * user setting.
   *
   * @author Lim, Lay Ngor - XCR1743:Benchmark
   */
  public static boolean allowCenterSolderBallShortDetection(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    if (_JOINT_TYPES_ALLOWING_CENTER_SOLDER_BALL_SHORT_DETECTION.contains(jointTypeEnum))
      return true;
    return false;
  }

  /**
   * Returns true if the joint type supports Broken Pin short detection.  For those joint types, it is still up to the
   * user whether or not to actually test for broken pin shorts, so even if this returns true, you still need to check the
   * user setting.
   *
   * @author Lim, Lay Ngor - XCR2100 - Broken Pin
   */
  public static boolean allowBrokenPinShortDetection(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    if (_JOINT_TYPES_ALLOWING_BROKEN_PIN_SHORT_DETECTION.contains(jointTypeEnum))
      return true;
    return false;
  }

  /**
   * Internal enumeration used to identify the disposition of potential short defects
   * (DEFINITE_SHORT, QUESTIONABLE_SHORT, EXONERATED_SHORT).
   *
   * @author Matt Wharton
   */
  protected static class ShortDefectDispositionEnum extends com.axi.util.Enum
  {
    private static int _index = 0;

    public static final ShortDefectDispositionEnum DEFINITE_SHORT = new ShortDefectDispositionEnum(_index++);
    public static final ShortDefectDispositionEnum QUESTIONABLE_SHORT = new ShortDefectDispositionEnum(_index++);
    public static final ShortDefectDispositionEnum EXONERATED_SHORT = new ShortDefectDispositionEnum(_index++);

    /**
     * @author Matt Wharton
     */
    private ShortDefectDispositionEnum(int id)
    {
      super(id);
    }
  }

  /**
   * @author Matt Wharton
   */
  static
  {
    initializeJointTypeToDefaultMinimumShortThicknessSettingsMap();
    initializeJointTypeToDefaultHighShortThicknessSettingsMap();
  }

  /**
   * @author Matt Wharton
   */
  private static void initializeJointTypeToDefaultMinimumShortThicknessSettingsMap()
  {
    Map<JointTypeEnum, Float> temporaryJointTypeToMinimumThicknessSettingsMap = new HashMap<JointTypeEnum, Float>();
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.CAPACITOR, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.CGA, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.CHIP_SCALE_PACKAGE, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.COLLAPSABLE_BGA, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.GULLWING, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.JLEAD, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.LEADLESS, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.NON_COLLAPSABLE_BGA, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.POLARIZED_CAP, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.PRESSFIT, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.RESISTOR, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.RF_SHIELD, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.SINGLE_PAD, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.LGA, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.SMALL_OUTLINE_LEADLESS, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.SURFACE_MOUNT_CONNECTOR, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.THROUGH_HOLE, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.OVAL_THROUGH_HOLE, MathUtil.convertMilsToMillimeters(6f));//Siew yeng - XCR-3318
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.EXPOSED_PAD, MathUtil.convertMilsToMillimeters(4f));

    // Create a read-only copy of the map.
    _jointTypeEnumToDefaultMinimumShortThicknessInMillisSettings =
        Collections.unmodifiableMap(temporaryJointTypeToMinimumThicknessSettingsMap);
  }

  /**
   * @author Poh Kheng
   */
  private static void initializeJointTypeToDefaultHighShortThicknessSettingsMap()
  {
    Map<JointTypeEnum, Float> temporaryJointTypeToMinimumThicknessSettingsMap = new HashMap<JointTypeEnum, Float>();
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.CAPACITOR, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.CGA, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.CHIP_SCALE_PACKAGE, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.COLLAPSABLE_BGA, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.GULLWING, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.JLEAD, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.LEADLESS, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.NON_COLLAPSABLE_BGA, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.POLARIZED_CAP, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.PRESSFIT, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.QUAD_FLAT_PACK_NOLEAD, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.RESISTOR, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.RF_SHIELD, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.SINGLE_PAD, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.LGA, MathUtil.convertMilsToMillimeters(4f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.SMALL_OUTLINE_LEADLESS, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.SURFACE_MOUNT_CONNECTOR, MathUtil.convertMilsToMillimeters(2f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.THROUGH_HOLE, MathUtil.convertMilsToMillimeters(6f));
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.OVAL_THROUGH_HOLE, MathUtil.convertMilsToMillimeters(6f)); //Siew Yeng - XCR-3318
    temporaryJointTypeToMinimumThicknessSettingsMap.put(JointTypeEnum.EXPOSED_PAD, MathUtil.convertMilsToMillimeters(4f));

    // Create a read-only copy of the map.
    _jointTypeEnumToDefaultMinimumHighShortThicknessInMillisSettings =
        Collections.unmodifiableMap(temporaryJointTypeToMinimumThicknessSettingsMap);
  }


  /**
   * @author Matt Wharton
   */
  protected static boolean isLightestImageTestingEnabled()
  {
    Config config = Config.getInstance();

    boolean enableLightestImageTestForShortDefects =
        config.getBooleanValue(SoftwareConfigEnum.ENABLE_LIGHTEST_IMAGE_TEST_FOR_SHORT_DEFECTS);

    return enableLightestImageTestForShortDefects;
  }

  /**
   * @param inspectionFamilyEnum the inspection family enum this algorithm is affliated with.
   * @author Matt Wharton
   */
  public SharedShortAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.SHORT, inspectionFamilyEnum);

    // Initialize the algorithm settings.
    addAlgorithmSettings();

    addMeasurementEnums();
  }

  /**
   * Initializes the AlgorithmSettings for this Algorithm.
   *
   * @author Matt Wharton
   */
  protected abstract void addAlgorithmSettings();

  /**
   * @author Peter Esbensen
   * @author Rex Shang
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.SHORT_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.SHORT_LENGTH);
	//Lim, Lay Ngor - XCR1743:Benchmark
    _jointMeasurementEnums.add(MeasurementEnum.SOLDER_BALL_SHORT_DELTA_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.SHORT_BROKEN_PIN_AREA_SIZE); //Broken Pin
    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());

    _componentMeasurementEnums.add(MeasurementEnum.SHORT_THICKNESS);
    _componentMeasurementEnums.add(MeasurementEnum.SHORT_LENGTH);
	//Lim, Lay Ngor - XCR1743:Benchmark
    _componentMeasurementEnums.add(MeasurementEnum.SOLDER_BALL_SHORT_DELTA_THICKNESS);
    //Lim, Lay Ngor - XCR????: Broken Pin
    _componentMeasurementEnums.add(MeasurementEnum.SHORT_BROKEN_PIN_AREA_SIZE);    
    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());
  }

  /**
   * @author Matt Wharton
   */
  protected abstract Collection<SliceNameEnum> getSlicesToInspect(Subtype subtype);

  /**
   * @author Matt Wharton
   */
  protected RegionOfInterest getReferenceRoi(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    RegionOfInterest referenceRoi = null;

    Subtype subtype = jointInspectionData.getSubtype();
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
    String referencePositionSetting =
        (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_REGION_REFERENCE_POSITION);
    if (referencePositionSetting.equals("Located Position"))
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        referenceRoi = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData, AlgorithmUtil.choseSliceToPerformLocatorForJoint(jointInspectionData), false);
      }
      else
      {
        referenceRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData, AlgorithmUtil.choseSliceToPerformLocatorForJoint(jointInspectionData), false);
      }
    }
    else if (referencePositionSetting.equals("CAD Position"))
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        referenceRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest(false);
      }
      else
      {
        referenceRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels(false);
      }
    }
    else
    {
      Assert.expect(false, "Unexpected reference position setting: " + referencePositionSetting);
    }

    Assert.expect(referenceRoi != null);
    return referenceRoi;
  }

  /**
   * @author Matt Wharton
   */
  protected abstract float getMinimumShortThicknessInMMThreshold(Subtype subtype, SliceNameEnum sliceNameEnum);

  /**
   * Determine the capacitor slice: OPAQUE, CLEAR or PAD.
   * Return detected slice from Chip measurement algorithm for short inspection.
   * 
   * @author Lim, Lay Ngor
   */
  SliceNameEnum capacitorSlice(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    boolean testAsOpaque = ChipMeasurementAlgorithm.testAsOpaque(jointInspectionData);
    SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
    if (testAsOpaque)
      sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
    
    if (jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP))
      sliceNameEnum = SliceNameEnum.PAD;

    return sliceNameEnum;
  }
  
  /**
   * Main entry point for the short algorithm.
   *
   * @author Matt Wharton
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.isEmpty() == false);

    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();

    // Free any references to the lightest image for the reconstruction region in question.
    ReconstructedImages lightestReconstructedImages = _reconstructionRegionToLightestImagesMap.remove(inspectionRegion);
    if (lightestReconstructedImages != null)
    {
      lightestReconstructedImages.decrementReferenceCount();
      lightestReconstructedImages = null;
    }

    // Get the subtype.
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);
    Subtype subtype = jointInspectionDataObjects.iterator().next().getSubtype();
    
    //short will never apply resize feature as it will not have any affect by sheng chuan
    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel();
    // Run on the applicable slices for the inspection family.
    Collection<SliceNameEnum> shortInspectionSlices = getSlicesToInspect(subtype);
    //Lim Lay Ngor - XCR-2492 Will use the default inspection slice at the beginning 
    //of inspection because we do not have jointInspectionData yet.
    // XCR-2681, Resize feature bug on BGA ball diameter calculation
    SliceNameEnum sliceNameEnumForLocator = AlgorithmUtil.choseSliceToPerformLocatorForJoint(jointInspectionDataObjects);
    if (ImageProcessingAlgorithm.useResize(subtype) && (Integer) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.IMAGE_ENHANCEMENT_RESIZE_SCALE) > 1)
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(subtype.getJointTypeEnum()))
      {
        for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
        {
          Locator.locateRectangularComponent(reconstructedImages,
            sliceNameEnumForLocator,
            componentInspectionData,
            this,
            false);
        }
      }
      else
      {
        Locator.locateJoints(reconstructedImages, sliceNameEnumForLocator, jointInspectionDataObjects, this, false);
      }
    }
    
    for (SliceNameEnum sliceNameEnum : shortInspectionSlices)
    {
      //Lim, Lay Ngor - Start Broken Pin
      //Jack Hwee - broken pin
      if(reconstructedImages.hasReconstructedSlice(sliceNameEnum) == false)
        continue;
        
      //Very bad implementation because the loop is for slice and jointinspection, not component!      
      if (allowBrokenPinShortDetection(subtype.getJointTypeEnum()) == true
        && sliceNameEnum == SliceNameEnum.CAMERA_0)
      {
        AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);
        if (BrokenPinAlgorithm.enableBrokenPin(subtype))
        {
          detectBrokenPinOnSliceAndPostDiagnosticResult(reconstructedImages, subtype, sliceNameEnum,
            jointInspectionDataObjects, MILIMETER_PER_PIXEL);
    
          continue; //don't need to run normal short
        }
        // will run normal short for Jack algo if it is Disable BrokenPin.
      }
      //Lim, Lay Ngor - End Broken Pin

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        //Lim, Lay Ngor - XCR-2492 make sure correct slice is used for capacitor.
        if (subtype.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR))
        {
           SliceNameEnum newSliceNameEnum = capacitorSlice(jointInspectionData);
            if (newSliceNameEnum != sliceNameEnum)
              continue;
        }
        
        ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
        Image imageToInspect = reconstructedSlice.getOrthogonalImageWithoutEnhanced();
        //need to relocate joint because X Y position will be wrong if previously using resize image.
        
        AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);      
        
        if (ImageAnalysis.isSpecialTwoPinComponent(jointInspectionData.getJointTypeEnum()))
        {
          if (jointInspectionData != jointInspectionData.getComponentInspectionData().getPadOneJointInspectionData())
          {
            // only test pad one of the special two pin components (res, chip, pcap)
            continue;
          }
        }

        AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, inspectionRegion, reconstructedSlice, false);

        BooleanRef jointPassed = new BooleanRef(true);
        detectShortsOnJoint(imageToInspect, inspectionRegion, subtype, jointInspectionData, sliceNameEnum, jointPassed, MILIMETER_PER_PIXEL);

        if (ImageAnalysis.isSpecialTwoPinComponent(jointInspectionData.getJointTypeEnum()))
        {
          AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                      jointInspectionData.getComponentInspectionData(),
                                                                      inspectionRegion,
                                                                      sliceNameEnum,
                                                                      jointPassed.getValue());

        }
        else
        {
          AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
                                                                  jointInspectionData,
                                                                  inspectionRegion,
                                                                  sliceNameEnum,
                                                                  jointPassed.getValue());
        }

        // Remove the joint/slice pair from our list of joint/slice pairs we've warned about "no learning available" on.
        Pair<JointInspectionData, SliceNameEnum> jointAndSlicePair =
            new Pair<JointInspectionData, SliceNameEnum>(jointInspectionData, sliceNameEnum);
        _jointsAndSlicesWarnedOn.remove(jointAndSlicePair);
      }
    }

    // Free any references to the lightest image for the reconstruction region in question.
    lightestReconstructedImages = _reconstructionRegionToLightestImagesMap.remove(inspectionRegion);
    if (lightestReconstructedImages != null)
    {
      lightestReconstructedImages.decrementReferenceCount();
      lightestReconstructedImages = null;
    }

    // CR1018 Memory Leak fix by LeeHerng - Clear off any reference to AlgorithmShortsLearningReaderWriter
    // and AlgorithmExpectedImageLearningReaderWriter
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
        Pad pad = jointInspectionData.getPad();
        PadSettings padSettings = pad.getPadSettings();
        padSettings.clearAlgorithmLearningReference();
    }
  }

  /**
   * @author Matt Wharton
   */
  private void detectShortsOnJoint(Image image,
                                   ReconstructionRegion inspectionRegion,
                                   Subtype subtype,
                                   JointInspectionData jointInspectionData,
                                   SliceNameEnum sliceNameEnum,
                                   BooleanRef jointPassed,
                                   final float MILIMETER_PER_PIXEL) throws XrayTesterException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;

    // Give a warning if we don't have any learned data for the joint.
    if (subtype.isJointSpecificDataLearned() == false)
    {
      // Issue a warning.
      raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);
    }

    // @todo: mdw - it would be uber-cool if we could actually validate these thresholds at the tuner level.
    if (regionInnerEdgeAsFractionOfInterPadDistance >= regionOuterEdgeAsFractionOfInterPadDistance)
    {
      // Raise a warning to the user that we can't run under these circumstances.
      LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
        "ALGDIAG_SHORT_OUTER_EDGE_LOCATION_LESS_THEN_INNER_EDGE_LOCATION_WARNING_KEY",
        new Object[] { jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName() });
      AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);

      return;
    }

    detectShortsOnJointBasedOnShape(image,
                        inspectionRegion,
                        subtype,
                        jointInspectionData,
                        sliceNameEnum,
                        jointPassed,
                        MILIMETER_PER_PIXEL);
  }

  /**
   * @author Matt Wharton
   */
  protected abstract void detectShortsOnJointBasedOnShape(Image image,
                                     ReconstructionRegion inspectionRegion,
                                     Subtype subtype,
                                     JointInspectionData jointInspectionData,
                                     SliceNameEnum sliceNameEnum,
                                     BooleanRef jointPassed,
                                     final float MILIMETER_PER_PIXEL) throws XrayTesterException;

  /**
   * @author Lim, Lay Ngor - Broken Pin
   */
  private void detectBrokenPinOnSliceAndPostDiagnosticResult(ReconstructedImages reconstructedImages,
                                  Subtype subtype,
                                  SliceNameEnum sliceNameEnum,
                                  List<JointInspectionData> jointInspectionDataObjects,
                                  final float MILIMETER_PER_PIXEL) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(sliceNameEnum != null);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;

//    // Give a warning if we don't have any learned data for the joint.
//    if (subtype.isJointSpecificDataLearned() == false)
//    {
//      // Issue a warning.
//      raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);
//    }
//
    // @todo: mdw - it would be uber-cool if we could actually validate these thresholds at the tuner level.
    if (regionInnerEdgeAsFractionOfInterPadDistance >= regionOuterEdgeAsFractionOfInterPadDistance)
    {
      // Raise a warning to the user that we can't run under these circumstances.
      LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
        "ALGDIAG_SHORT_OUTER_EDGE_LOCATION_LESS_THEN_INNER_EDGE_LOCATION_WARNING_KEY",
        new Object[] { jointInspectionDataObjects.get(0).getFullyQualifiedPadName(), sliceNameEnum.getName() });
      AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);

      return;
    }

    //Put here just because we wanna get the component information - please remove this to avoid unecessary warning
//  for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
//  {
//     Locator.locateComponent(reconstructedImages,
//              sliceNameEnum,
//              componentInspectionData,
//              this,
//              false);
//   }
    
    BooleanRef slicePassed = new BooleanRef(true);    
    detectBrokenPinBaseOnShape(reconstructedImages, 
            sliceNameEnum,
            jointInspectionDataObjects, 
//            this, 
            slicePassed); 
    
    // Get the inspection region.
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    Image imageToInspect = reconstructedSlice.getOrthogonalImageWithoutEnhanced();    
    
    //Suppose we should post pass/fail component, but after post it seems not so suitable cause 
    //we cant know which portion of joint is having this error(we only know the component name
    //So now we directly post all joints failed. Besides, to post component level pass/fail, 
    //we need to update component Locator here separately!!!!
    JointInspectionData jointInspectionData = jointInspectionDataObjects.get(0);
    AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this,
      jointInspectionData,
      inspectionRegion,
      sliceNameEnum,
      slicePassed.getValue());
    
    for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      // Attach the indictment to the component's results.
      ComponentInspectionResult componentInspectionResult = jointInspectionData.getComponentInspectionData().getComponentInspectionResult();

      //Simply set location just for indictment use?
      componentInspectionResult.addMeasurement(new ComponentMeasurement(this,
        subtype,
        MeasurementEnum.LOCATOR_X_LOCATION,
        MeasurementUnitsEnum.PIXELS,
        componentInspectionData.getComponent(),
        sliceNameEnum,
        imageToInspect.getCenterCoordinate().getX()));

      componentInspectionResult.addMeasurement(new ComponentMeasurement(this,
        subtype,
        MeasurementEnum.LOCATOR_Y_LOCATION,
        MeasurementUnitsEnum.PIXELS,
        componentInspectionData.getComponent(),
        sliceNameEnum,
        imageToInspect.getCenterCoordinate().getY()));

      AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
        componentInspectionData,
        inspectionRegion,
        sliceNameEnum,
        slicePassed.getValue());
    }         
  }
  
  
  /**
   * @author Lim, Lay Ngor - Broken Pin
   */
  protected abstract void detectBrokenPinBaseOnShape(ReconstructedImages reconstructedImages,
                                  SliceNameEnum sliceNameEnum,
                                  List<JointInspectionData> jointInspectionDataObjects,
//                                  Algorithm algorithm,
                                  BooleanRef slicePassed) throws XrayTesterException;
  
  /**
   * Checks the specified measured gray level profile for a huge short.  This is done by using
   * the average gray level in the measured profile as a foreground and the learned average gray
   * level as a background and creating an average thickness.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - Factored out the calculation from this function for RectangularShortAlgo
   * use. This code is remain for CircularShortAlgo use.
   */
  protected float measureLargeShortAcrossEntireRegion(float[] measuredGrayLevelProfile,
                                                      ShortProfileLearning shortLearning,
                                                      Subtype subtype,
                                                      int backgroundSensitivityOffset) throws DatastoreException
  {
    Assert.expect(measuredGrayLevelProfile != null);
    Assert.expect(shortLearning != null);

    // Get the measured average gray level.
    float measuredAverageGrayLevel = StatisticsUtil.mean(measuredGrayLevelProfile);

    // Get the learned average gray level for the region.
    float learnedAverageGrayLevel = shortLearning.getAverageRegionGrayLevel();

	//XCR2111 - revert back
    float deltaGray = Math.max(0, learnedAverageGrayLevel - measuredAverageGrayLevel);

    learnedAverageGrayLevel = Math.min(learnedAverageGrayLevel, 255);
    learnedAverageGrayLevel = Math.max(learnedAverageGrayLevel, 0);
    float averageThicknessInMillis = AlgorithmUtil.getSolderThickness(subtype).getThicknessInMillimeters(learnedAverageGrayLevel, deltaGray, backgroundSensitivityOffset);

    return averageThicknessInMillis;
  }

  /**
   * Creates a ProfileDiagnosticInfo for the specified short profile which also identifies defect zones in the
   * profile.
   *
   * @author Matt Wharton
   */
  protected ProfileDiagnosticInfo createShortProfileDiagnosticInfo(
      JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum,
      float[] deltaThicknessProfile,
      Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> shortZoneToDispositionMap)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(deltaThicknessProfile != null);
    Assert.expect(shortZoneToDispositionMap != null);

    // Create a mapping identifying each profile bin as background or some kind of defect bin (definite, questionable,
    // or exonerated).
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer,Integer>,MeasurementRegionEnum>();
    // All the bins are initially marked as background.
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, deltaThicknessProfile.length),
                                                  MeasurementRegionEnum.SHORT_BACKGROUND_REGION);
    // Mark all the short defect zones accordingly.
    for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : shortZoneToDispositionMap.entrySet())
    {
      Pair<Integer, Integer> shortZoneBins = mapEntry.getKey();
      int startBin = shortZoneBins.getFirst();
      Assert.expect((startBin >= 0) && (startBin < deltaThicknessProfile.length));
      int endBin = shortZoneBins.getSecond();
      Assert.expect((endBin >= 0) && (endBin < deltaThicknessProfile.length));

      MeasurementRegionEnum defectRegionType = null;
      ShortDefectDispositionEnum shortDisposition = mapEntry.getValue();
      if (shortDisposition.equals(ShortDefectDispositionEnum.DEFINITE_SHORT))
      {
        defectRegionType = MeasurementRegionEnum.DEFINITE_SHORT_DEFECT_REGION;
      }
      else if (shortDisposition.equals(ShortDefectDispositionEnum.QUESTIONABLE_SHORT))
      {
        defectRegionType = MeasurementRegionEnum.QUESTIONABLE_SHORT_DEFECT_REGION;
      }
      else if (shortDisposition.equals(ShortDefectDispositionEnum.EXONERATED_SHORT))
      {
        defectRegionType = MeasurementRegionEnum.EXONERATED_SHORT_DEFECT_REGION;
      }
      else
      {
        // Shouldn't get here!
        Assert.expect(false, "Unexpected short defect disposition : " + shortDisposition);
      }

      // Add the short zone bins to our map.
      if (startBin <= endBin)
      {
        // Short is NOT a "wrap around" short.  Just add the subrange to the map.
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(startBin, endBin + 1), defectRegionType);
      }
      else
      {
        // Short is a "wrap around" short.
        // We need to add two subranges to the map: one from startBin to profile end and another from
        // profile start to endBin.
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(startBin, deltaThicknessProfile.length),
                                                      defectRegionType);
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, endBin + 1), defectRegionType);
      }
    }

    // Get the "maximum short thickness" setting value.
    Subtype subtype = jointInspectionData.getSubtype();
    final float MINIMUM_SHORT_THICKNESS_IN_MILLIS = getMinimumShortThicknessInMMThreshold(subtype, sliceNameEnum);

    // We want the profile to scale from -50% to 200% of the "minimum short thickness" setting.
    float profileMin = MINIMUM_SHORT_THICKNESS_IN_MILLIS * -0.5f;
    float profileMax = MINIMUM_SHORT_THICKNESS_IN_MILLIS * 2f;

    LocalizedString shortProfileLabelLocalizedString = new LocalizedString("ALGDIAG_SHORT_PROFILE_LABEL_KEY", null);
    ProfileDiagnosticInfo shortProfileDiagnosticInfo =
        ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                         ProfileTypeEnum.BACKGROUND_PROFILE,
                                                                         shortProfileLabelLocalizedString,
                                                                         profileMin,
                                                                         profileMax,
                                                                         deltaThicknessProfile,
                                                                         profileSubrangeToMeasurementRegionEnumMap,
                                                                         MeasurementUnitsEnum.MILLIMETERS);


    return shortProfileDiagnosticInfo;
  }

  /**
   * Creates a ProfileDiagnosticInfo for the specified short profile which also identifies defect zones in the
   * profile.
   *
   * @author Lim, Seng Yew - Plus disable info.
   * @author Lim, Lay Ngor - XCR1743:Benchmark-Add handling for solder ball detection on center area 
   */
  protected ProfileDiagnosticInfo createShortProfileDiagnosticInfoWithDisableInfo(
      JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum,
      float[] deltaThicknessProfile,
      Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> shortZoneToDispositionMap,
      Map<Pair<Integer, Integer>, MeasurementRegionEnum> shortTestRegionDisableInfo,
      boolean isCenterDetection) //Lim, Lay Ngor - XCR1743:Benchmark
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(deltaThicknessProfile != null);
    Assert.expect(shortZoneToDispositionMap != null);

    // Create a mapping identifying each profile bin as background or some kind of defect bin (definite, questionable,
    // or exonerated).
    Map<Pair<Integer, Integer>, MeasurementRegionEnum> profileSubrangeToMeasurementRegionEnumMap =
        new LinkedHashMap<Pair<Integer,Integer>,MeasurementRegionEnum>();
    // All the bins are initially marked as background.
    profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, deltaThicknessProfile.length),
                                                  MeasurementRegionEnum.SHORT_BACKGROUND_REGION);

    // Mark all the short defect zones accordingly.
    for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : shortZoneToDispositionMap.entrySet())
    {
      Pair<Integer, Integer> shortZoneBins = mapEntry.getKey();
      int startBin = shortZoneBins.getFirst();
      Assert.expect((startBin >= 0) && (startBin < deltaThicknessProfile.length));
      int endBin = shortZoneBins.getSecond();
      Assert.expect((endBin >= 0) && (endBin < deltaThicknessProfile.length));

      MeasurementRegionEnum defectRegionType = null;
      ShortDefectDispositionEnum shortDisposition = mapEntry.getValue();
      if (shortDisposition.equals(ShortDefectDispositionEnum.DEFINITE_SHORT))
      {
        defectRegionType = MeasurementRegionEnum.DEFINITE_SHORT_DEFECT_REGION;
      }
      else if (shortDisposition.equals(ShortDefectDispositionEnum.QUESTIONABLE_SHORT))
      {
        defectRegionType = MeasurementRegionEnum.QUESTIONABLE_SHORT_DEFECT_REGION;
      }
      else if (shortDisposition.equals(ShortDefectDispositionEnum.EXONERATED_SHORT))
      {
        defectRegionType = MeasurementRegionEnum.EXONERATED_SHORT_DEFECT_REGION;
      }
      else
      {
        // Shouldn't get here!
        Assert.expect(false, "Unexpected short defect disposition : " + shortDisposition);
      }

      // Add the short zone bins to our map.
      if (startBin <= endBin)
      {
        // Short is NOT a "wrap around" short.  Just add the subrange to the map.
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(startBin, endBin + 1), defectRegionType);
      }
      else
      {
        // Short is a "wrap around" short.
        // We need to add two subranges to the map: one from startBin to profile end and another from
        // profile start to endBin.
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(startBin, deltaThicknessProfile.length),
                                                      defectRegionType);
        profileSubrangeToMeasurementRegionEnumMap.put(new Pair<Integer, Integer>(0, endBin + 1), defectRegionType);
      }
    }

    // Add test region disable information
    for (Map.Entry<Pair<Integer, Integer>, MeasurementRegionEnum> mapEntry : shortTestRegionDisableInfo.entrySet())
      profileSubrangeToMeasurementRegionEnumMap.put(mapEntry.getKey(), MeasurementRegionEnum.SHORT_PROFILE_DISABLE_REGION);

    // Get the "maximum short thickness" setting value.
    Subtype subtype = jointInspectionData.getSubtype();
    
    //Lim, Lay Ngor - XCR1743:Benchmark - START
    float MINIMUM_SHORT_THICKNESS_IN_MILLIS = getMinimumShortThicknessInMMThreshold(subtype, sliceNameEnum);
    if(isCenterDetection)
      MINIMUM_SHORT_THICKNESS_IN_MILLIS = 
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS);
	//Lim, Lay Ngor - XCR1743:Benchmark - END

    // We want the profile to scale from -50% to 200% of the "minimum short thickness" setting.
    float profileMin = MINIMUM_SHORT_THICKNESS_IN_MILLIS * -0.5f;
    float profileMax = MINIMUM_SHORT_THICKNESS_IN_MILLIS * 2f;

    LocalizedString shortProfileLabelLocalizedString = new LocalizedString("ALGDIAG_SHORT_PROFILE_LABEL_KEY", null);
	//Lim, Lay Ngor - XCR1743:Benchmark
    if(isCenterDetection)
      shortProfileLabelLocalizedString = new LocalizedString("ALGDIAG_CENTER_SOLDER_BALL_SHORT_PROFILE_LABEL_KEY", null);

    ProfileDiagnosticInfo shortProfileDiagnosticInfo =
        ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getPad().getName(),
                                                                         ProfileTypeEnum.BACKGROUND_PROFILE,
                                                                         shortProfileLabelLocalizedString,
                                                                         profileMin,
                                                                         profileMax,
                                                                         deltaThicknessProfile,
                                                                         profileSubrangeToMeasurementRegionEnumMap,
                                                                         MeasurementUnitsEnum.MILLIMETERS);


    return shortProfileDiagnosticInfo;
  }
  
  /**
   * Analyzes the specified delta thickness profile for potential shorts as per the minimum thickness/length
   * threshold.
   * Return the maximum delta thickness value within each detected short region to the map.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness
   * @author Siew Yeng - added parameter wrapAround
   */
  protected Map<Pair<Integer, Integer>, Pair<Float, Float>> analyzeDeltaThicknessProfileWithThicknessForShorts(Image image,
                                                                                     ReconstructionRegion inspectionRegion,
                                                                                     JointInspectionData jointInspectionData,
                                                                                     SliceNameEnum sliceNameEnum,
                                                                                     float[] deltaThicknessProfile,
                                                                                     final float MILIMETER_PER_PIXEL,
                                                                                     boolean wrapAround)
  {
    
    return analyzeDeltaThicknessProfileWithThicknessForShorts(image,
                                                              inspectionRegion,
                                                              jointInspectionData,
                                                              sliceNameEnum,
                                                              deltaThicknessProfile,
                                                              getMinimumShortThicknessInMMThreshold(jointInspectionData.getSubtype(), sliceNameEnum),
                                                              (Float)jointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH),
                                                              MILIMETER_PER_PIXEL,
                                                              wrapAround);
  }

  /**
   * Analyzes the specified delta thickness profile for potential shorts as per the minimum thickness/length
   * threshold.
   * Return the maximum delta thickness value within each detected short region to the map.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness
   * @author Siew Yeng - added parameter minimumShortThicknessInMillis & minimumShortLengthInMillis
   */
  protected Map<Pair<Integer, Integer>, Pair<Float, Float>> analyzeDeltaThicknessProfileWithThicknessForShorts(Image image,
                                                                                     ReconstructionRegion inspectionRegion,
                                                                                     JointInspectionData jointInspectionData,
                                                                                     SliceNameEnum sliceNameEnum,
                                                                                     float[] deltaThicknessProfile,
                                                                                     float minimumShortThicknessInMillis,
                                                                                     float minimumShortLengthInMillis,
                                                                                     final float MILIMETER_PER_PIXEL,
                                                                                     boolean wrapAround)
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(deltaThicknessProfile != null);

    // If applicable, log the delta thickness profile.
    if (_LOG_ALGORITHM_PROFILE_DATA)
    {
      CSVFileWriterAxi shortProfileDataWriter = InspectionEngine.getInstance().getShortProfileDataWriter();
      String uniquePadName = jointInspectionData.getFullyQualifiedPadName();
      for (int i = 0; i < deltaThicknessProfile.length; ++i)
      {
        float thicknessProfileValueInMils = MathUtil.convertMillimetersToMils(deltaThicknessProfile[i]);
        shortProfileDataWriter.writeDataLine(Arrays.asList(uniquePadName,
                                                           sliceNameEnum.getName(),
                                                           String.valueOf(i),
                                                           String.valueOf(thicknessProfileValueInMils)));
      }
    }

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the minimum short thickness and length thresholds.
//    final float minimumShortThicknessInMillis = getMinimumShortThicknessInMMThreshold(subtype, sliceNameEnum);
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
//    final float minimumShortLengthInMillis = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH);
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END         

    Map<Pair<Integer, Integer>, Pair<Float, Float>> candidateShortZoneToLengthAndThicknessMap = new HashMap<Pair<Integer, Integer>, Pair<Float, Float>>();

    boolean measuringPotentialShort = false;
    Pair<Integer, Integer> shortZoneAtStartBin = null;
    Pair<Integer, Integer> shortZoneAtEndBin = null;
    int startIndexOfCurrentShort = 0;
    int endIndexOfCurrentShort = 0;
    for (int i = 0; i < deltaThicknessProfile.length; ++i)
    {
      if (MathUtil.fuzzyGreaterThanOrEquals(deltaThicknessProfile[i], minimumShortThicknessInMillis))
      {
        // If this is the first bin of this potential short, update the starting index marker.
        if (measuringPotentialShort == false)
        {
          startIndexOfCurrentShort = i;

          // Mark that we're now analyzing a potential short.
          measuringPotentialShort = true;
        }

        // If we're at the end of the profile, we need to mark this candidate short
        // region for analysis as a potential wrap-around short.
        if (i == deltaThicknessProfile.length - 1)
        {
          endIndexOfCurrentShort = i;
          if(wrapAround)
          {
            shortZoneAtEndBin = new Pair<Integer, Integer>(startIndexOfCurrentShort, endIndexOfCurrentShort);
            measuringPotentialShort = false;
          }
        }
      }
      else if (measuringPotentialShort)
      {
        // Current bin does not exceed the short minimum thickness threshold.  Update the end index marker.
        // The end index is inclusive.
        endIndexOfCurrentShort = i - 1;
        
        if(wrapAround)
        {
          // If the current candidate short zone began at the start bin, we need to mark this candidate short
          // region for analysis as a potential wrap-around short.
          if (startIndexOfCurrentShort == 0)
          {
            shortZoneAtStartBin = new Pair<Integer, Integer>(startIndexOfCurrentShort, endIndexOfCurrentShort);
            measuringPotentialShort = false;
          }
        }
      }

      // If we've reached the end of a candidate short and the short doesn't start on the first bin or end on the
      // last bin (we need to correctly handle shorts which "wrap around" the end of the profile back to the start),
      // evaluate it to see if it exceeds the minimum thickness threshold.
      if ((measuringPotentialShort)
          && (endIndexOfCurrentShort > startIndexOfCurrentShort))
      {
        // Is this short wide enough to call?
        final int shortLengthInPixels = endIndexOfCurrentShort - startIndexOfCurrentShort + 1;
        final float lengthOfPotentialShortInMillis = shortLengthInPixels * MILIMETER_PER_PIXEL;
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
        if (lengthOfPotentialShortInMillis >= minimumShortLengthInMillis)
        {
          // We have a short.
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
          //add the maximum delta thickness within the range which causing the short to final result. 
          final float maxDeltaThicknessValueInMillis = StatisticsUtil.maximum(deltaThicknessProfile, startIndexOfCurrentShort, endIndexOfCurrentShort);

          // Add this candidate short zone to our list.
          candidateShortZoneToLengthAndThicknessMap.put(new Pair<Integer, Integer>(startIndexOfCurrentShort, endIndexOfCurrentShort),
                                                       new Pair<Float, Float>(lengthOfPotentialShortInMillis, maxDeltaThicknessValueInMillis));
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END

          measuringPotentialShort = false;
        }
      }
    }

    // Check for "wrap around" shorts.
    if ((shortZoneAtStartBin != null) || (shortZoneAtEndBin != null))
    {
      int wrapAroundShortStartBin = -1;
      int wrapAroundShortEndBin = -1;
      int wrapAroundShortLengthInPixels = 0;

      if ((shortZoneAtStartBin != null) && (shortZoneAtEndBin != null))
      {
        wrapAroundShortStartBin = shortZoneAtEndBin.getFirst();
        wrapAroundShortEndBin = shortZoneAtStartBin.getSecond();
        wrapAroundShortLengthInPixels = (deltaThicknessProfile.length - wrapAroundShortStartBin) + wrapAroundShortEndBin + 1;
      }
      else if (shortZoneAtStartBin != null)
      {
        wrapAroundShortStartBin = shortZoneAtStartBin.getFirst();
        wrapAroundShortEndBin = shortZoneAtStartBin.getSecond();
        wrapAroundShortLengthInPixels = wrapAroundShortEndBin - wrapAroundShortStartBin + 1;
      }
      else if (shortZoneAtEndBin != null)
      {
        wrapAroundShortStartBin = shortZoneAtEndBin.getFirst();
        wrapAroundShortEndBin = shortZoneAtEndBin.getSecond();
        wrapAroundShortLengthInPixels = wrapAroundShortEndBin - wrapAroundShortStartBin + 1;
      }
      else
      {
        // Shouldn't ever occur (logical impossibility).
        Assert.expect(false, "Both start and end short regions are null!");
      }

      Assert.expect(wrapAroundShortStartBin != -1);
      Assert.expect(wrapAroundShortEndBin != -1);
      Assert.expect(wrapAroundShortLengthInPixels > 0);

      // Is the wrap-around short wide enough to call?
      final float lengthOfPotentialWrapAroundShortInMillis = wrapAroundShortLengthInPixels * MILIMETER_PER_PIXEL;
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
      if (lengthOfPotentialWrapAroundShortInMillis >= minimumShortLengthInMillis)
      {
        // We have a wrap-around short.
        // Add this candidate short zone to our list.
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
        float firstDeltaThicknessValueInMillis = deltaThicknessProfile[deltaThicknessProfile.length - 1];
//        if(wrapAroundShortStartBin != (deltaThicknessProfile.length - 1))
          firstDeltaThicknessValueInMillis = StatisticsUtil.maximum(deltaThicknessProfile, wrapAroundShortStartBin, (deltaThicknessProfile.length - 1));
        
        float secondDeltaThicknessValueInMillis = deltaThicknessProfile[0];
//        if(wrapAroundShortEndBin != 0)
          secondDeltaThicknessValueInMillis = StatisticsUtil.maximum(deltaThicknessProfile, 0, wrapAroundShortEndBin);
        
        final float maxDeltaThicknessValueInMillis = Math.max(firstDeltaThicknessValueInMillis, secondDeltaThicknessValueInMillis);
        candidateShortZoneToLengthAndThicknessMap.put(new Pair<Integer, Integer>(wrapAroundShortStartBin, wrapAroundShortEndBin),
                                         new Pair<Float, Float>(lengthOfPotentialWrapAroundShortInMillis, maxDeltaThicknessValueInMillis));
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END
      }
    }
    
    return candidateShortZoneToLengthAndThicknessMap;
  }
  
  /**
   * Validates that the candidate shorts in the specified Map are, in fact, true shorts.
   *
   * This is accomplished with a two stage process where we initially verify if there's a thickness peak
   * that may have been shifted from the learning.  If there is no defined peak in the candidate short zone, we
   * conclude that there is no short.  If there is a short and it exceeds the "minimum short thickness" algorithm
   * setting, we conclude that there is a short.  If there is a defined peak, but it doesn't exceed the "minimum
   * short thickness" setting, we consider the test inconclusive and move on the "lightest image" test.
   *
   * If we still can't decide if there's a short based on the 'shifted peaks' test, we request a lightest image
   * and threshold it.  If the number of 'dark' pixels in the candidate short zone on the thresholded
   * lightest image exceeds some percentage, we conclude that there's a real short there, otherwise there is no
   * short.
   *
   * @author Matt Wharton
   */
  protected void validateShortIndictments(Image image,
                                        ReconstructionRegion inspectionRegion,
                                        Subtype subtype,
                                        JointInspectionData jointInspectionData,
                                        SliceNameEnum sliceNameEnum,
                                        float[] thicknessProfile,
                                        Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> shortZoneToDispositionMap,
                                        boolean isCenterDetect) throws XrayTesterException //Lim, Lay Ngor - XCR1743:Benchmark
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(thicknessProfile != null);
    Assert.expect(shortZoneToDispositionMap != null);

    // Determine if we need to request a lightest image.  We only need to request lightest if there are actual
    // candidate Short defects.
    boolean needLightestImage = isLightestImageTestingEnabled() && (shortZoneToDispositionMap.isEmpty() == false);

    if (needLightestImage)
    {
      // Do we already have the lightest images cached?
      ReconstructedImages lightestReconstructedImages = _reconstructionRegionToLightestImagesMap.get(inspectionRegion);
      if (lightestReconstructedImages == null)
      {
        // Request the lightest images for the region and cache.
        BooleanRef lightestImagesAvailable = new BooleanRef(true);
        lightestReconstructedImages = ImageAnalysis.getLightestImagesForRegion(inspectionRegion, lightestImagesAvailable);
        if (lightestImagesAvailable.getValue() == true)
        {
          _reconstructionRegionToLightestImagesMap.put(inspectionRegion, lightestReconstructedImages);
        }
      }

      // Do we have the lightest images available?
      if (lightestReconstructedImages != null)
      {
        Assert.expect(lightestReconstructedImages.getReconstructionRegion() == inspectionRegion,
                      "Lightest image reconstruction region DOES NOT match requested reconstruction region!");

        // We've got the lightest images.  Analyze them.
        Image lightestImageForSlice = lightestReconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImageWithoutEnhanced();
        checkPotentialShortZonesUsingLightestImage(lightestImageForSlice,
                                                   inspectionRegion,
                                                   subtype,
                                                   jointInspectionData,
                                                   sliceNameEnum,
                                                   shortZoneToDispositionMap,
                                                   isCenterDetect); //Lim, Lay Ngor - XCR1743:Benchmark
      }
      else
      {
        // We couldn't get the lightest image.  All Short calls will remain.
      }
    }

    // At this point, any lingering "questionable" shorts should be marked
    // as "definite" shorts.  We'd rather err on the side of caution than risk
    // an escape.
    for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : shortZoneToDispositionMap.entrySet())
    {
      ShortDefectDispositionEnum currentDisposition = mapEntry.getValue();
      if (currentDisposition.equals(ShortDefectDispositionEnum.QUESTIONABLE_SHORT))
      {
        mapEntry.setValue(ShortDefectDispositionEnum.DEFINITE_SHORT);
      }
    }
  }

//  /**
//   * If we still can't decide if there's a short based on the 'shifted peaks' test, we request a lightest image
//   * and threshold it.  If the number of 'dark' pixels in the candidate short zone on the thresholded
//   * lightest image exceeds some percentage, we conclude that there's a real short there, otherwise there is no
//   * short.
//   *
//   * @author Matt Wharton
//   */
//  private void checkPotentialShortZonesUsingLightestImage(
//      Image lightestImage,
//      ReconstructionRegion inspectionRegion,
//      Subtype subtype,
//      JointInspectionData jointInspectionData,
//      SliceNameEnum sliceNameEnum,
//      Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> shortZoneToDispositionMap,
//      RegionOfInterest thresholdRegion) throws DatastoreException
//  {
//    Assert.expect(lightestImage != null);
//    Assert.expect(subtype != null);
//    Assert.expect(jointInspectionData != null);
//    Assert.expect(shortZoneToDispositionMap != null);
//
//    // Create a defensive copy of the lightest image.
//    Image lightestImageCopy = new Image(lightestImage);
//
//    // Threshold our lightest image.
//    final float THRESHOLD_PERCENTILE = 0.4f;
//    float thresholdValue = Statistics.getPercentile(lightestImageCopy, thresholdRegion, THRESHOLD_PERCENTILE);
//    Threshold.threshold(lightestImageCopy, thresholdRegion, thresholdValue, 0f, thresholdValue, 255f);
//
////    ImageDebugWindow.displayAndBlock(lightestImageCopy, "Thresholded Lightest Image - " + jointInspectionData.getFullyQualifiedPadName());
//
//    // Measure a gray level profile in the short region.
//    float[] grayLevelProfile = createGrayLevelProfileForLightestImage(lightestImageCopy,
//                                                                      inspectionRegion,
//                                                                      sliceNameEnum,
//                                                                      jointInspectionData);
//
//    Assert.expect(grayLevelProfile != null, "Gray level profile is null!");
//
//    lightestImageCopy.decrementReferenceCount();
//
//    // For each candidate Short, make sure there is actually a Short there by examining the lightest image.
//    final float MINIMUM_LIGHT_VALUE = 64.f;
//    for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : shortZoneToDispositionMap.entrySet())
//    {
//      Pair<Integer, Integer> shortZone = mapEntry.getKey();
//      int shortZoneStartBin = shortZone.getFirst();
//      int shortZoneEndBin = shortZone.getSecond();
//
//      // Does the short zone "wrap around"?
//      if (shortZoneEndBin < shortZoneStartBin)
//      {
//        shortZoneEndBin += grayLevelProfile.length;
//      }
//
//      // Count the number of dark bins in the profile.
//      int numberOfBinsInShortZone = (shortZoneEndBin - shortZoneStartBin) + 1;
//      int numberOfDarkBinsInShortZone = 0;
//      for (int i = shortZoneStartBin; i <= shortZoneEndBin; ++i)
//      {
//        if (grayLevelProfile[i % grayLevelProfile.length] < MINIMUM_LIGHT_VALUE)
//        {
//          ++numberOfDarkBinsInShortZone;
//        }
//      }
//
//      // Calculate the fraction of dark bins in the short zone.
//      float fractionOfDarkBinsInShortZone = (float)numberOfDarkBinsInShortZone / (float)numberOfBinsInShortZone;
//
//      // If the fraction exceeds some threshold (say 20%), we'll say the region is a true short, otherwise
//      // it's not.
//      final float MAX_ALLOWABLE_FRACTION_OF_DARK_BINS_IN_SHORT_ZONE = 0.3f;
//      if (MathUtil.fuzzyGreaterThan(fractionOfDarkBinsInShortZone, MAX_ALLOWABLE_FRACTION_OF_DARK_BINS_IN_SHORT_ZONE))
//      {
//        mapEntry.setValue(ShortDefectDispositionEnum.DEFINITE_SHORT);
//      }
//      else
//      {
//        mapEntry.setValue(ShortDefectDispositionEnum.EXONERATED_SHORT);
//      }
//    }
//  }

  /**
   * Retake the background thickness profile on the lightest image and make sure that a certain fraction of the bins
   * in each candidate short zone exceed max([defect threshold] - 1 mil, 1 mil).
   *
   * @author Matt Wharton
   */
  private void checkPotentialShortZonesUsingLightestImage(Image lightestImage,
                                                          ReconstructionRegion inspectionRegion,
                                                          Subtype subtype,
                                                          JointInspectionData jointInspectionData,
                                                          SliceNameEnum sliceNameEnum,
                                                          Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> shortZoneToDispositionMap,
                                                          boolean isCenterDetection) throws DatastoreException//Lim, Lay Ngor - XCR1743:Benchmark
  {
    Assert.expect(lightestImage != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(shortZoneToDispositionMap != null);

//    ImageDebugWindow.displayAndBlock(lightestImage, "Lightest Image - " + jointInspectionData.getFullyQualifiedPadName());

    // Measure a gray level profile in the short region.
    float[] grayLevelProfile;
    try
    {
      grayLevelProfile = createGrayLevelProfileForLightestImage(lightestImage,
                                                                inspectionRegion,
                                                                sliceNameEnum,
                                                                jointInspectionData);
    }
    catch (JointCannotBeInspectedBusinessException jEx)
    {
      LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
          "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[] {inspectionRegion});
      AlgorithmUtil.raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);

      return;
    }

    // Convert to a thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float[] thicknessProfile =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfile,
                                                                             _GRAY_LEVEL_BACKGROUND_PERCENTILE,
                                                                             thicknessTable,
                                                                             backgroundSensitivityOffset);

    float minimumShortThicknessInMillis = getMinimumShortThicknessInMMThreshold(subtype, sliceNameEnum);

	//Lim, Lay Ngor - XCR1743:Benchmark
    if(isCenterDetection)
      minimumShortThicknessInMillis = 
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_MINIMUM_CENTER_SOLDER_BALL_THICKNESS);

    minimumShortThicknessInMillis = Math.max(minimumShortThicknessInMillis - MathUtil.convertMilsToMillimeters(1f),
                                             MathUtil.convertMilsToMillimeters(1f));
    for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : shortZoneToDispositionMap.entrySet())
    {
      Pair<Integer, Integer> shortZone = mapEntry.getKey();
      int shortZoneStartBin = shortZone.getFirst();
      int shortZoneEndBin = shortZone.getSecond();

      // Does the short zone "wrap around"?
      if (shortZoneEndBin < shortZoneStartBin)
      {
        shortZoneEndBin += grayLevelProfile.length;
      }

      int numberOfBinsInShortZone = (shortZoneEndBin - shortZoneStartBin) + 1;
      int numberOfExceedingThresholdInShortZone = 0;
      for (int i = shortZoneStartBin; i <= shortZoneEndBin; ++i)
      {
        if (thicknessProfile[i % thicknessProfile.length] >= minimumShortThicknessInMillis)
        {
          ++numberOfExceedingThresholdInShortZone;
        }
      }

      // Calculate the fraction of bins exceeding the threshold in the short zone.
      float fractionOfBinsExceedingThresholdInShortZone =
          (float)numberOfExceedingThresholdInShortZone / (float)numberOfBinsInShortZone;

      // If a sufficient fraction of the bins in the "hot zone" exceed the defect threshold, we'll let the Short call
      // stand; otherwise, we'll exonerate it.
      final float MINIMUM_FRACTION_OF_BINS_EXCEEDING_THRESHOLD = 0.25f;
      if (fractionOfBinsExceedingThresholdInShortZone >= MINIMUM_FRACTION_OF_BINS_EXCEEDING_THRESHOLD)
      {
        mapEntry.setValue(ShortDefectDispositionEnum.DEFINITE_SHORT);
      }
      else
      {
        mapEntry.setValue(ShortDefectDispositionEnum.EXONERATED_SHORT);
      }
    }
  }

  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  protected abstract float[] createGrayLevelProfileForLightestImage(Image lightestImage,
                                                                    ReconstructionRegion inspectionRegion,
                                                                    SliceNameEnum sliceNameEnum,
                                                                    JointInspectionData jointInspectionData) throws JointCannotBeInspectedBusinessException;

  /**
   * Helper method to create an Indictment object for the specified JointMeasurement object.
   *
   * @author Matt Wharton
   */
  private JointIndictment createShortIndictment(JointMeasurement shortThicknessMeasurement,
                                                SliceNameEnum sliceNameEnum)
  {
    Assert.expect(shortThicknessMeasurement != null);
    Assert.expect(sliceNameEnum != null);

    JointIndictment shortIndictment = new JointIndictment(IndictmentEnum.SHORT,
        this, sliceNameEnum);
    shortIndictment.addFailingMeasurement(shortThicknessMeasurement);

    return shortIndictment;
  }


  /**
   * Helper method to create an short length or short thickness component measurement object.
   * LLN: Modify to direct set the MeasurementEnum type from caller.
   * @author Rex Shang
   * @author Lim, Lay Ngor - XCR1780
   */
  private ComponentMeasurement createShortComponentMeasurement(Pad pad,
                                                                    SliceNameEnum sliceNameEnum,
                                                                    MeasurementEnum measurementEnum,
                                                                    float shortDataInMilis)
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementEnum != null);

    ComponentMeasurement shortMeasurement = new ComponentMeasurement(
        this,
        pad.getSubtype(),
        measurementEnum,
        MeasurementUnitsEnum.MILLIMETERS,
        pad.getComponent(),
        sliceNameEnum,
        shortDataInMilis);

    return shortMeasurement;
  }

  /**
   * Helper method to create an short length component indictment object.
   * @author Rex Shang
   */
  private ComponentIndictment createShortComponentIndictment(Pad pad,
                                                             SliceNameEnum sliceNameEnum,
                                                             ComponentMeasurement shortThicknessMeasurement)
  {
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(shortThicknessMeasurement != null);

    ComponentIndictment shortComponentIndictment = new ComponentIndictment(IndictmentEnum.SHORT, this, sliceNameEnum, pad.getSubtype(), pad.getJointTypeEnum());

    shortComponentIndictment.addFailingMeasurement(shortThicknessMeasurement);

    return shortComponentIndictment;
  }

  /**
   * Helper method to create a failing short measurement containing the specified short thickness value
   * and tie that measurement to an indictment.  The indictment is then added to the joint's results.
   * LLN: Modify to direct set the MeasurementEnum type from caller.
   *
   * @author Rex Shang
   * @author Matt Wharton
   * @author Lim, Lay Ngor - XCR1780
   */
  protected void indictShort(Image image,
                             ReconstructionRegion inspectionRegion,
                             SliceNameEnum sliceNameEnum,
                             JointInspectionData jointInspectionData,
                             float shortDataInMillis,
                             float minimumShortDataThresholdInMillis,
                             MeasurementEnum measurementEnum)
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(measurementEnum != null); //LNLim: pls check how it crash

    Pad pad = jointInspectionData.getPad();

    if (ImageAnalysis.isSpecialTwoPinComponent(jointInspectionData.getJointTypeEnum()))
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        // Create the measurement.
        //Lim, Lay Ngor - XCR1780
        ComponentMeasurement shortComponentMeasurement = createShortComponentMeasurement(pad, sliceNameEnum, 
          measurementEnum, shortDataInMillis);

        // Create an indictment that's tied to this failing measurement.
        ComponentIndictment shortComponentIndictment = createShortComponentIndictment(pad, sliceNameEnum, shortComponentMeasurement);

        // Attach the indictment to the component's results.
        ComponentInspectionResult componentInspectionResult = jointInspectionData.getComponentInspectionData().getComponentInspectionResult();
        componentInspectionResult.addMeasurement(shortComponentMeasurement);
        componentInspectionResult.addIndictment(shortComponentIndictment);

        // Post joint failed diagnostics.
        AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                            jointInspectionData.getComponentInspectionData(),
                                                            inspectionRegion,
                                                            sliceNameEnum,
                                                            shortComponentMeasurement,
                                                            minimumShortDataThresholdInMillis);//Lim, Lay Ngor - XCR1780
      }
      else
      {
        JointMeasurement shortMeasurement = new JointMeasurement(
            this,
            measurementEnum,
            MeasurementUnitsEnum.MILLIMETERS,
            pad,
            sliceNameEnum,
            shortDataInMillis);
        
        // Create an indictment that's tied to this failing measurement.
        JointIndictment shortIndictment = createShortIndictment(shortMeasurement, sliceNameEnum);

        // Attach the indictment to the joint's results.
        JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
        jointInspectionResult.addMeasurement(shortMeasurement);
        jointInspectionResult.addIndictment(shortIndictment);
        
        // Post joint failed diagnostics.
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        jointInspectionData,
                                                        inspectionRegion,
                                                        sliceNameEnum,
                                                        shortMeasurement,
                                                        minimumShortDataThresholdInMillis);//Lim, Lay Ngor - XCR1780
      }
    }
    else
    {
      // Create the measurement.
      //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length     
      //Lim, Lay Ngor - XCR2167
      JointMeasurement shortMeasurement = new JointMeasurement(
        this,
        measurementEnum,
        MeasurementUnitsEnum.MILLIMETERS,
        pad,
        sliceNameEnum,
        shortDataInMillis);
    
      // Create an indictment that's tied to this failing measurement.
      JointIndictment shortIndictment = createShortIndictment(shortMeasurement, sliceNameEnum);

      // Attach the indictment to the joint's results.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addMeasurement(shortMeasurement);
      jointInspectionResult.addIndictment(shortIndictment);

      // Post joint failed diagnostics.
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      inspectionRegion,
                                                      sliceNameEnum,
                                                      shortMeasurement,
                                                      minimumShortDataThresholdInMillis);//Lim, Lay Ngor - XCR1780
    }
  }

 /**
   * Helper method to create a passing short measurement containing the specified short thickness value
   * and tie that measurement to an indictment.  The indictment is then added to the joint's results.
   *
   * @author Lim, Lay Ngor - XCR1780, XCR2167
   */
  protected void recordPassedMeasurement(SliceNameEnum sliceNameEnum,
                             JointInspectionData jointInspectionData,
                             float maxThicknessDetectedInMillis,
                             MeasurementEnum measurementEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(measurementEnum != null); //LNLim: pls check how it crash

    if (ImageAnalysis.isSpecialTwoPinComponent(jointInspectionData.getJointTypeEnum()))
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();      
        Component component = jointInspectionData.getComponent();
        ComponentMeasurement thicknessMeasurement = new ComponentMeasurement(this,
                                                          jointInspectionData.getSubtype(),
                                                          measurementEnum,
                                                          MeasurementUnitsEnum.MILLIMETERS,
                                                          component,
                                                          sliceNameEnum,
                                                          maxThicknessDetectedInMillis);
        componentInspectionResult.addMeasurement(thicknessMeasurement);
      }
      else
      {
        JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
        JointMeasurement thicknessMeasurement = new JointMeasurement(this,
                                                          measurementEnum,
                                                          MeasurementUnitsEnum.MILLIMETERS,
                                                          jointInspectionData.getPad(),
                                                          sliceNameEnum,
                                                          maxThicknessDetectedInMillis);
        jointInspectionResult.addMeasurement(thicknessMeasurement);
      }
    }
    else
    {
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      JointMeasurement thicknessMeasurement = new JointMeasurement(this,
                                                        measurementEnum,
                                                        MeasurementUnitsEnum.MILLIMETERS,
                                                        jointInspectionData.getPad(),
                                                        sliceNameEnum,
                                                        maxThicknessDetectedInMillis);
      jointInspectionResult.addMeasurement(thicknessMeasurement);
    }
  }
  
  /**
   * Retrieves the learned profiles for a rectangular joint region.  Issues a warning if
   * the learning is not available.
   *
   * @author Matt Wharton
   */
  protected ShortProfileLearning getShortProfileLearning(Image image,
                                                         ReconstructionRegion inspectionRegion,
                                                         JointInspectionData jointInspectionData,
                                                         SliceNameEnum sliceNameEnum,
                                                         BooleanRef learnedDataAvailable) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(learnedDataAvailable != null);

    learnedDataAvailable.setValue(true);
    Pad pad = jointInspectionData.getPad();
    PadSettings padSettings = pad.getPadSettings();

    // Now, make sure we have data.
    ShortProfileLearning shortProfileLearning = null;
    if (padSettings.hasShortProfileLearning(sliceNameEnum))
    {
      shortProfileLearning = padSettings.getShortProfileLearning(sliceNameEnum);
      if (shortProfileLearning.hasAverageLearnedThicknessProfile() == false)
      {
        // Issue warning.
        raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);

        learnedDataAvailable.setValue(false);
      }
    }
    else
    {
      // Issue warning.
      raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);

      learnedDataAvailable.setValue(false);
    }

    return shortProfileLearning;
  }

  /**
   * Raises an algorithm warning that no learning is available for the given joint and slice.
   * Keeps track of whether or not the warning has already been issued and only issues the warning
   * once per joint/slice combination.
   *
   * @author Matt Wharton
   */
  protected void raiseNoLearningAvailableWarningIfNeeded(JointInspectionData jointInspectionData,
                                                         SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    Pair<JointInspectionData, SliceNameEnum> jointAndSlicePair =
        new Pair<JointInspectionData, SliceNameEnum>(jointInspectionData, sliceNameEnum);
    if (_jointsAndSlicesWarnedOn.contains(jointAndSlicePair) == false)
    {
      LocalizedString warningText = new LocalizedString("ALGDIAG_SHORT_NO_LEARNING_AVAILABLE_WARNING_KEY",
                                                        new Object[]
                                                        {jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName()});
      AlgorithmUtil.raiseAlgorithmWarning(warningText);

      _jointsAndSlicesWarnedOn.add(jointAndSlicePair);
    }
  }

  /**
   * Raises an algorithm warning that there's a size difference between the learned and measured profiles.
   *
   * @author Matt Wharton
   */
  protected void raiseProfileSizeMismatchWarning(JointInspectionData jointInspectionData,
                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    LocalizedString warningText = new LocalizedString("ALGDIAG_SHORT_PROFILE_SIZE_MISMATCH_WARNING_KEY",
      new Object[] { jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName() });
    AlgorithmUtil.raiseAlgorithmWarning(warningText);
  }

  /**
   * Raises an algorithm warning that there's no ROI found.
   *
   * @author Lim, Lay Ngor - XCR1743:Benchmark
   */
  protected void raiseEmptyCenterROIWarning(JointInspectionData jointInspectionData,
                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    LocalizedString warningText = new LocalizedString("ALGDIAG_EMPTY_ROI_WARNING_KEY",
      new Object[] { jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName() });
    AlgorithmUtil.raiseAlgorithmWarning(warningText);
  }

  /**
   * Entry point for learning joint specific data.
   *
   * @author Matt Wharton
   */
  public void learnJointSpecificData(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Learn the Short background profiles.
    learnShortBackgroundThicknessProfiles(subtype, typicalBoardImages);
  }
  
   /*
   * @author Kee Chin Seong
   *  - Applying all boards' short profile by using first board only.
   */
  public void learnJointSpecificData(Subtype subtype, Board board, 
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(board != null);

    // Learn the Short background profiles.
    learnShortBackgroundThicknessProfiles(subtype, board, typicalBoardImages);
  }
  
   /*
   * @author Kee Chin Seong
   *  - Applying all boards' short profile by using first board only.
   */
  private void learnShortBackgroundThicknessProfiles(Subtype subtype, Board board,
                                                     ManagedOfflineImageSet typicalBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;

    // @todo: mdw - it would be uber-cool if we could actually validate these thresholds at the tuner level.
    if (regionInnerEdgeAsFractionOfInterPadDistance >= regionOuterEdgeAsFractionOfInterPadDistance)
    {
      // Raise a warning to the user that we can't run under these circumstances.
      LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
        "ALGDIAG_SHORT_OUTER_EDGE_LOCATION_LESS_THEN_INNER_EDGE_LOCATION_DURING_LEARNING_WARNING_KEY",
        new Object[] { subtype.getJointTypeEnum().getName(), subtype.getShortName() });
      AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);

      return;
    }

    /** @todo: mdw - does this need to be here? */
    // Cause all previous test results to be reset
    subtype.getPanel().getProject().getTestProgram().startNewInspectionRun();

    ManagedOfflineImageSetIterator reconstructedImagesIterator = typicalBoardImages.iterator();

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = reconstructedImagesIterator.getNext()) != null)
    {
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // Make sure each joint type matches the first one in the region.
        Assert.expect(jointTypeEnum.equals(jointInspectionData.getSubtype().getJointTypeEnum()));
      }

      // Just learn on the applicable slices for the family.
      for (SliceNameEnum sliceNameEnum : getInspectionFamily().getShortInspectionSlices(subtype))
      {
        ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
        Image image = reconstructedPadSlice.getOrthogonalImageWithoutEnhanced();

        // Measure the background profile(s) for each joint.
        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          // Get the PadSettings for this joint's pad.
          Pad pad = jointInspectionData.getPad();
          PadSettings padSettings = pad.getPadSettings();

          // Do we already have some saved learned data for this joint and slice?
          ShortProfileLearning shortLearning;
          if (padSettings.hasShortProfileLearning(sliceNameEnum))
            shortLearning = padSettings.getShortProfileLearning(sliceNameEnum);
          else
            shortLearning = new ShortProfileLearning();

          try
          {
            // XCR-3150 System crash when learn short to all board
            // If there's a mismatch in learned profile lengths, reset the learned profile data.
            if (shortLearning.hasAverageLearnedThicknessProfile())
            {
              if (shortLearning.getAverageLearnedThicknessProfile().length != padSettings.getLearnedShortProfileLearning(board, sliceNameEnum).getAverageLearnedThicknessProfile().length)
              {
                shortLearning.clearAverageLearnedThicknessProfile();
              }
            }
            // Update the learned average profile with the profile we just measured.
            shortLearning.addLearnedThicknessProfile(padSettings.getLearnedShortProfileLearning(board, sliceNameEnum).getAverageLearnedThicknessProfile());
            shortLearning.setAverageRegionGrayLevel(padSettings.getLearnedShortProfileLearning(board, sliceNameEnum).getAverageRegionGrayLevel());
            shortLearning.setPad(pad);
            shortLearning.setSliceNameEnum(sliceNameEnum);
            
            // Save the learned profile. 
            padSettings.setShortProfileLearning(shortLearning);
          }
          catch (DatastoreException ex)
          {
            // do nothing
          }
        }
      }

      reconstructedImagesIterator.finishedWithCurrentRegion();
    }
  }

  /**
   * Learns the short background thickness profiles for the specified subtype and images.
   *
   * @author Matt Wharton
   */
  private void learnShortBackgroundThicknessProfiles(Subtype subtype,
                                                     ManagedOfflineImageSet typicalBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;

    // @todo: mdw - it would be uber-cool if we could actually validate these thresholds at the tuner level.
    if (regionInnerEdgeAsFractionOfInterPadDistance >= regionOuterEdgeAsFractionOfInterPadDistance)
    {
      // Raise a warning to the user that we can't run under these circumstances.
      LocalizedString outerEdgeLessThanInnerEdgeWarningText = new LocalizedString(
        "ALGDIAG_SHORT_OUTER_EDGE_LOCATION_LESS_THEN_INNER_EDGE_LOCATION_DURING_LEARNING_WARNING_KEY",
        new Object[] { subtype.getJointTypeEnum().getName(), subtype.getShortName() });
      AlgorithmUtil.raiseAlgorithmWarning(outerEdgeLessThanInnerEdgeWarningText);

      return;
    }

    /** @todo: mdw - does this need to be here? */
    // Cause all previous test results to be reset
    subtype.getPanel().getProject().getTestProgram().startNewInspectionRun();

    ManagedOfflineImageSetIterator reconstructedImagesIterator = typicalBoardImages.iterator();

    ReconstructedImages reconstructedImages;
    while ((reconstructedImages = reconstructedImagesIterator.getNext()) != null)
    {
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = inspectionRegion.getInspectableJointInspectionDataList(subtype);

      JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        // Make sure each joint type matches the first one in the region.
        Assert.expect(jointTypeEnum.equals(jointInspectionData.getSubtype().getJointTypeEnum()));
      }

      // Call Locator at the applicable pad slice.
      InspectionFamily inspectionFamily = getInspectionFamily();
      SliceNameEnum padSliceNameEnumForCurrentJointType = inspectionFamily.getSliceNameEnumForLocator(subtype);
      if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
        {
          Locator.locateRectangularComponent(reconstructedImages,
                                             padSliceNameEnumForCurrentJointType,
                                             componentInspectionData,
                                             this,
                                             true);
        }
      }
      else
      {
        Locator.locateJoints(reconstructedImages, padSliceNameEnumForCurrentJointType, jointInspectionDataObjects, this, true);
      }

      // Just learn on the applicable slices for the family.
      for (SliceNameEnum sliceNameEnum : getInspectionFamily().getShortInspectionSlices(subtype))
      {
        //Jack Hwee - broken pin
        if(reconstructedImages.hasReconstructedSlice(sliceNameEnum) == false)
          continue;
        
        ReconstructedSlice reconstructedPadSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
        Image image = reconstructedPadSlice.getOrthogonalImageWithoutEnhanced();

        //Lim, Lay Ngor - Learn Broken Pin - shall we through circular?
        if (allowBrokenPinShortDetection(subtype.getJointTypeEnum()) == true
          //subtype.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE)
          && sliceNameEnum == SliceNameEnum.CAMERA_0)
        {
          if (BrokenPinAlgorithm.enableBrokenPin(subtype))
            BrokenPinAlgorithm.learnBrokenPinBorder(reconstructedImages, sliceNameEnum, jointInspectionDataObjects, this); 
        } 
        
        // Measure the background profile(s) for each joint.
        for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
        {
          // Get the PadSettings for this joint's pad.
          Pad pad = jointInspectionData.getPad();
          PadSettings padSettings = pad.getPadSettings();

          // Do we already have some saved learned data for this joint and slice?
          ShortProfileLearning shortLearning;
          if (padSettings.hasShortProfileLearning(sliceNameEnum))
            shortLearning = padSettings.getShortProfileLearning(sliceNameEnum);
          else
            shortLearning = new ShortProfileLearning();

          try
          {
            float[] thicknessProfile = measureThicknessProfileForLearning(image,
                                                                          inspectionRegion,
                                                                          sliceNameEnum,
                                                                          jointInspectionData,
                                                                          shortLearning);

            // If there's a mismatch in learned profile lengths, reset the learned profile data.
            if (shortLearning.hasAverageLearnedThicknessProfile())
            {
              if (thicknessProfile.length != shortLearning.getAverageLearnedThicknessProfile().length)
              {
                shortLearning.clearAverageLearnedThicknessProfile();
              }
            }

            // Update the learned average profile with the profile we just measured.
            shortLearning.addLearnedThicknessProfile(thicknessProfile);
            shortLearning.setPad(pad);
            shortLearning.setSliceNameEnum(sliceNameEnum);

            // Save the learned profile.
            padSettings.setShortProfileLearning(shortLearning);
 
          }
          catch (JointCannotBeInspectedBusinessException jEx)
          {
            LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
                "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[] {inspectionRegion});
            AlgorithmUtil.raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);
          }
        }
      }

      reconstructedImagesIterator.finishedWithCurrentRegion();
    }
  }

  /**
   * @author Matthew Wharton
   * @author Patrick Lacz
   */
  protected abstract float[] measureThicknessProfileForLearning(Image image,
                                                                ReconstructionRegion inspectionRegion,
                                                                SliceNameEnum sliceNameEnum,
                                                                JointInspectionData jointInspectionData,
                                                                ShortProfileLearning shortLearning) throws DatastoreException, JointCannotBeInspectedBusinessException;

  /**
   * Deletes all of the learned Short profiles for the specified subtype.
   *
   * @author Matt Wharton
   */
  public void deleteLearnedJointSpecificData(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    // Delete the short profile learning for all pads in the subtype.
    Collection<Pad> subtypePads = subtype.getPads();
    for (Pad pad : subtypePads)
    {
      deleteLearnedJointSpecificData(pad);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public void deleteLearnedJointSpecificData(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    PadSettings padSettings = pad.getPadSettings();
    if (padSettings.hasShortProfileLearning())
      padSettings.deleteShortProfileLearning();

    //Lim, Lay Ngor - Start Broken Pin
    if (allowBrokenPinShortDetection(pad.getJointTypeEnum()) == true
      //subtype.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE)
      )
//      && sliceNameEnum == SliceNameEnum.CAMERA_0)
    {
      //Add broken pin ??
      if (padSettings.hasBrokenPinLearning())
        padSettings.deleteBrokenPinLearning();
    }
    //Lim, Lay Ngor - Start Broken Pin
  }

  /**
   * Learns appropriate short region placement settings based on the joint type.
   *
   * @author Matt Wharton
   */
  protected void learnShortRegionPlacementSettings(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // By default, we'll just leave the settings at their defaults...
  }

  /**
   * "Learns" an appropriate minimum short thickness setting based on the joint type.
   *
   * @author Matt Wharton
   */
  private void learnShortMinimumThicknessSettings(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_jointTypeEnumToDefaultMinimumShortThicknessInMillisSettings != null);

    // If we don't have a default in our map, just leave the setting at it's default value.
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (_jointTypeEnumToDefaultMinimumShortThicknessInMillisSettings.containsKey(jointTypeEnum))
    {
      float defaultMinimumShortThicknessSettingInMillis =
          _jointTypeEnumToDefaultMinimumShortThicknessInMillisSettings.get(jointTypeEnum);

      if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE) || 
          jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE))//Siew Yeng - XCR-3318 - Oval PTH
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS,
                                defaultMinimumShortThicknessSettingInMillis);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS,
                                defaultMinimumShortThicknessSettingInMillis);
      }
      else
      {
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS,
                                defaultMinimumShortThicknessSettingInMillis);
      }
    }
  }

  /**
   * "Learns" an appropriate minimum high short thickness setting based on the joint type.
   *
   * @author Poh Kheng
   */
  private void learnHighShortMinimumThicknessSettings(Subtype subtype)
  {
    Assert.expect(subtype != null);
    Assert.expect(_jointTypeEnumToDefaultMinimumHighShortThicknessInMillisSettings != null);

    // If we don't have a default in our map, just leave the setting at it's default value.
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (_jointTypeEnumToDefaultMinimumHighShortThicknessInMillisSettings.containsKey(jointTypeEnum))
    {
      float defaultMinimumHighShortThicknessSettingInMillis =
          _jointTypeEnumToDefaultMinimumHighShortThicknessInMillisSettings.get(jointTypeEnum);

        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS,
                                defaultMinimumHighShortThicknessSettingInMillis);
    }
  }

  /**
   * Entry point for learning Short algorithm settings.
   *
   * @author Matt Wharton
   * @author Poh Kheng
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages)
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Learn the region placement settings.
    learnShortRegionPlacementSettings(subtype);

    // Learn the minimum short thickness defect settings.
    learnShortMinimumThicknessSettings(subtype);

    // Learn the minimum high short thickness defect settings.
    learnHighShortMinimumThicknessSettings(subtype);
  }

  /**
   * Restores all learned Short algorithm settings to their defaults.
   *
   * @author Matt Wharton
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Reset the inner/outer edge locations.
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION);

    //Ngie Xing
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS);

    // Reset the defect threshold.
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE) || 
        jointTypeEnum.equals(JointTypeEnum.OVAL_THROUGH_HOLE))//Siew Yeng - XCR-3318 - Oval PTH
    {
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS);
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS);
    }
    else
    {
      subtype.setSettingToDefaultValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS);
    }
  }
  
  /**
   * Shared short algoritm settings for ThroughHole and Oval ThroughHole (Move from CircularShortAlgorithm)
   * @author Siew Yeng
   */
  protected Collection<AlgorithmSetting> createThroughHoleSharedShortAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    // Slices to analyze (PTH only).
    ArrayList<String> allowableThroughHoleSlices = new ArrayList<String>(Arrays.asList(_PIN_SLICE, _COMPONENT_SLICE, _BOTH_SLICES, _PROJECTION_SLICE_ONLY, _PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES)); //Broken Pin
    AlgorithmSetting inspectionSliceSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE,
        displayOrder++,
        _BOTH_SLICES,
        allowableThroughHoleSlices,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(INSPECTION_SLICE)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(INSPECTION_SLICE)_KEY",
        "IMG_DESC_SHARED_SHORT_(INSPECTION_SLICE)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    settingList.add(inspectionSliceSetting);
    
    // Component side slice minimum thickness setting (PTH only).
    AlgorithmSetting componentSideSliceMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(COMPONENT_SIDE_MIN_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(COMPONENT_SIDE_MIN_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(COMPONENT_SIDE_MIN_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    settingList.add(componentSideSliceMinimumThicknessSetting);

    // Pin side slice minimum thickness setting (PTH only).
    AlgorithmSetting pinSideSliceMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(PIN_SIDE_MIN_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(PIN_SIDE_MIN_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(PIN_SIDE_MIN_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    settingList.add(pinSideSliceMinimumThicknessSetting);

    ArrayList<String> highShortSliceSettings = new ArrayList<String>(Arrays.asList(_OFF, _ON));
    // Detect high shorts (PTH only)
    AlgorithmSetting protrusionHighShortDetectionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION,
        displayOrder++,
        _OFF,
        highShortSliceSettings,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_DETECTION)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_DETECTION)_KEY",
        "IMG_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_DETECTION)_KEY",
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    
    settingList.add(protrusionHighShortDetectionSetting); 
    
    // Protrusion Slice High short minimum thickness setting  (PTH only)
    AlgorithmSetting protrusionHighShortSliceMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    
    settingList.add(protrusionHighShortSliceMinimumThicknessSetting); 
    
    // 2D Slice High short minimum thickness setting  (PTH only)
    AlgorithmSetting projectionShortSliceMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    
    settingList.add(projectionShortSliceMinimumThicknessSetting); 
    
    return settingList;
  }
}

