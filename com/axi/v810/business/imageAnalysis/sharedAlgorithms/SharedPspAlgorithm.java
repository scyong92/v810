package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;

/**
 * This is a common class for PSP-related algorithms. 
 * 
 * @author Cheah Lee Herng
 */
public class SharedPspAlgorithm
{
  private static final String _ON   = "On";
  private static final String _OFF  = "Off";
  
  /**
   * This setting is to enable/disable PSP Local Search feature.
   * PSP Local Search will use localized PSP Auto-Focus to search for correct slice.
   * By disabling it, system will use PSP surface map result instead.
   * 
   * @author Cheah Lee Herng
   */
  public static AlgorithmSetting createPspLocalSearchAlgorithmSettings(int displayOrder, int currentVersion)
  {    
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList(_ON, _OFF));
    return new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_LOCAL_SEARCH,
        displayOrder++,
        _ON,
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_PSP_(PSP_LOCAL_SEARCH)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_LOCAL_SEARCH)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_LOCAL_SEARCH)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
        currentVersion);
  }
  
  /**
   * This setting is to control the lower limit of PSP search range.
   * This setting will only take effect when AlgorithmSettingEnum.PSP_LOCAL_SEARCH is set to ON.
   * 
   * @author Cheah Lee Herng
   */
  public static AlgorithmSetting createPspSearchRangeLowLimitAlgorithmSettings(int displayOrder, int currentVersion)
  {
    return new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_SEARCH_RANGE_LOW_LIMIT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(-20.0f), // default value
        MathUtil.convertMilsToMillimeters(-100.0f), // minimum value
        MathUtil.convertMilsToMillimeters(-2.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_SEARCH_RANGE_LOW_LIMIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
        currentVersion);
  }
  
  /**
   * This setting is to control the high limit of PSP search range.
   * This setting will only take effect when AlgorithmSettingEnum.PSP_LOCAL_SEARCH is set to ON.
   * 
   * @author Cheah Lee Herng
   */
  public static AlgorithmSetting createPspSearchRangeHighLimitAlgorithmSettings(int displayOrder, int currentVersion)
  {
    return new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_SEARCH_RANGE_HIGH_LIMIT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(20.0f), // default value
        MathUtil.convertMilsToMillimeters(2.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
        currentVersion);
  }
  
  /**
   * This setting is to control the Z-Offset of PSP search range.
   * This setting will only take effect when AlgorithmSettingEnum.PSP_LOCAL_SEARCH is set to ON.
   * 
   * @author Cheah Lee Herng
   * @param displayOrder
   * @param currentVersion
   * @return 
   */
  public static AlgorithmSetting createPspZOffsetAlgorithmSettings(int displayOrder, int currentVersion)
  {
    return new AlgorithmSetting(
        AlgorithmSettingEnum.PSP_Z_OFFSET,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-150.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // description URL Key
        "HTML_DETAILED_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // detailed description URL Key
        "IMG_DESC_PSP_(PSP_SEARCH_RANGE_HIGH_LIMIT)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
        currentVersion);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static boolean isEnabledPspLocalSearch(Subtype subtype)
  {
    Assert.expect(subtype != null);
    
    boolean isEnabledPspLocalSearch = true;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.PSP_LOCAL_SEARCH))
    {
      java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.PSP_LOCAL_SEARCH);
      Assert.expect(value instanceof String);
      if (value.equals(_ON))
        isEnabledPspLocalSearch = true;
      else if (value.equals(_OFF))
        isEnabledPspLocalSearch = false;
      else
        Assert.expect(false, "Unexpected Psp Local Search value.");
    }
    return isEnabledPspLocalSearch;
  }
}
