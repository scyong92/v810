package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.util.image.Filter;
import com.axi.util.image.Image;
import com.axi.util.image.Paint;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import static com.axi.v810.business.imageAnalysis.sharedAlgorithms.SharedShortAlgorithm._jointsAndSlicesWarnedOn;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

import ij.*;
import ij.process.*;
import java.awt.geom.*;
import java.io.*;
import java.util.*;
import java.util.List;
import javax.imageio.*;

public class BrokenPinAlgorithm
{
  private static final boolean _IS_LARGE_VIEW = false;
  private static final boolean _ENABLE_BORDER_FILTER = true;//to reduce the border OR - may introduce border escapy
  private static final boolean _IS_DEBUG = false;
  private final static String _DISABLE = "Disable";
  private final static String _PTH_AREA = "PTH Area Broken Pin";
  private final static String _NONE_PTH_AREA = "None PTH Area Broken Pin";
  private final static ArrayList<String> SHORT_BROKEN_PIN_CHOICES = new ArrayList<String>(Arrays.asList(_DISABLE, _PTH_AREA, _NONE_PTH_AREA));

  private static Algorithm _algorithm;
  private static final AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();//possible bug??

  //Basically only support PTH for broken pin
  public static Collection<AlgorithmSetting> createBrokenPinAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    AlgorithmSetting enableBrokenPinDetection = new AlgorithmSetting(
      AlgorithmSettingEnum.SHARED_SHORT_ENABLE_BROKEN_PIN,
      displayOrder++,
      _DISABLE,
      SHORT_BROKEN_PIN_CHOICES,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_SHARED_SHORT_(ENABLE_BROKEN_PIN)_KEY", // description URL key
      "HTML_DETAILED_DESC_SHARED_SHORT_(ENABLE_BROKEN_PIN)_KEY", // desailed description URL key
      "IMG_DESC_SHARED_SHORT_(ENABLE_BROKEN_PIN)_KEY", // image description URL key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    settingList.add(enableBrokenPinDetection);

    AlgorithmSetting shortBrokenPinAreaSizeThreshold = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_AREA_SIZE,
        displayOrder++,
        150.0f, // default value
        4.0f, // minimum value
        1000.0f, // maximum value
        MeasurementUnitsEnum.PIXELS,
        "HTML_DESC_SHARED_SHORT_(BROKEN_PIN_AREA_SIZE)_KEY", // description URL key
        "HTML_DETAILED_DESC_SHARED_SHORT_(BROKEN_PIN_AREA_SIZE)_KEY", // desailed description URL key
        "IMG_DESC_SHARED_SHORT_(BROKEN_PIN_AREA_SIZE)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(shortBrokenPinAreaSizeThreshold);
    
    AlgorithmSetting shortBrokenPinThresholdRatio = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_THRESHOLD_RATIO,
        displayOrder++,
        0.38f, // default value
        0.f, // minimum value
        1.f, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(BROKEN_PIN_THRESHOLD_RATIO)_KEY", // description URL key
        "HTML_DETAILED_DESC_SHARED_SHORT_(BROKEN_PIN_THRESHOLD_RATIO)_KEY", // desailed description URL key
        "IMG_DESC_SHARED_SHORT_(BROKEN_PIN_THRESHOLD_RATIO)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    settingList.add(shortBrokenPinThresholdRatio);    
    
    //Save diagnostic images
    settingList.add(ImageProcessingAlgorithm.createSaveDiagnoticVoidImageAlgorithmSetting(displayOrder, currentVersion));
    
    return settingList;
  }

  /**
   * @param subtype
   * @return
   * @author Lim, Lay Ngor
   */
  public static boolean enableBrokenPin(Subtype subtype)
  {
    boolean enableBrokenPin = false;
    if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_BROKEN_PIN))
    {
      java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_BROKEN_PIN);
      Assert.expect(value instanceof String);
      if(value.equals(_DISABLE))
        enableBrokenPin = false;
      else if (value.equals(_PTH_AREA) || value.equals(_NONE_PTH_AREA))
        enableBrokenPin = true;
      else
        Assert.expect(false, "Unexpected Broken Pin value.");
    }
    return enableBrokenPin;
  }

  /**
   * @author Lim, Lay Ngor - inspection call
   */
  public static void detectBrokenPinBaseOnShape(ReconstructedImages reconstructedImages,
    SliceNameEnum sliceNameEnum,
    List<JointInspectionData> jointInspectionDataObjects,
    Algorithm algorithm,
    BooleanRef slicePassed) throws XrayTesterException
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(algorithm != null);
    
    Map<RegionOfInterest, Integer> detectedBrokenPinList = detectObjectsAtBackground(
      reconstructedImages,sliceNameEnum, jointInspectionDataObjects, algorithm, false);
    
    //detectedBrokenPinList = null means failed to perform detection, we shall prompt warning & show joint pass.
    //detectedBrokenPinList.size() = 0 means NO broken pin detected, joint PASSED.
    if(detectedBrokenPinList == null)
    {
      //exception catched inside, inspection failed to proceed.
      //shall prompth warning
    }
    else if (detectedBrokenPinList.size() > 0)// Broken Pins detected
    {
      slicePassed.setValue(false);
      ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
      JointInspectionData firstJointInspectionData = jointInspectionDataObjects.get(0);
      final int brokenPinAreaSizeThreshold = Math.round((float) firstJointInspectionData.getSubtype()
        .getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_AREA_SIZE));
      
      //Sort the broken pin defect list so that the smallest defect will show at measurement tab.
      List<Integer> sortedDefectsTotalPixels = new ArrayList<Integer>(detectedBrokenPinList.values()); 
      Collections.sort(sortedDefectsTotalPixels);
      for(Integer defectTotalPixels : sortedDefectsTotalPixels)
      {
        // Create the measurement.              
        JointMeasurement brokenPinJointMeasurement = new JointMeasurement(_algorithm,
          MeasurementEnum.SHORT_BROKEN_PIN_AREA_SIZE, MeasurementUnitsEnum.PIXELS,
          firstJointInspectionData.getPad(), SliceNameEnum.FOREIGN_INCLUSION, defectTotalPixels);          

        // Create an indictment that's tied to this failing measurement.
        JointIndictment brokenPinIndictment = new JointIndictment(IndictmentEnum.SHORT,
            _algorithm, SliceNameEnum.FOREIGN_INCLUSION);
        brokenPinIndictment.addFailingMeasurement(brokenPinJointMeasurement);
        //Note: No passing measurement to be post because it is detecting extra items. 

        // Attach the indictment to the joint's results.
        JointInspectionResult jointInspectionResult = firstJointInspectionData.getJointInspectionResult();
        jointInspectionResult.addMeasurement(brokenPinJointMeasurement);
        jointInspectionResult.addIndictment(brokenPinIndictment);

        // Post joint failed diagnostics.
        AlgorithmUtil.postFailingJointTextualDiagnostic(_algorithm, firstJointInspectionData,
          inspectionRegion, SliceNameEnum.FOREIGN_INCLUSION, brokenPinJointMeasurement, brokenPinAreaSizeThreshold);      
      }
    }
  }

  /**
   * Inspect base on sliceEnum.
   * @author Lim, Lay Ngor
   */
  public static Map<RegionOfInterest, Integer> detectObjectsAtBackground(ReconstructedImages reconstructedImages,   
    SliceNameEnum sliceNameEnum,
    List<JointInspectionData> jointInspectionDataObjects,
    Algorithm algorithm,
    final boolean isLearning) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(jointInspectionDataObjects.isEmpty() == false);

    _algorithm = algorithm;
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();
    ReconstructionRegion inspectionRegion = reconstructedImages.getReconstructionRegion();
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
   
    final java.io.Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_ENABLE_BROKEN_PIN);
    final boolean isDummyPTH = value.equals(_NONE_PTH_AREA);

    //Normalised the image.
    Image image2D = new Image(reconstructedSlice.getOrthogonalImage());//new Image(reconstructedSlice.getImage());
    final RegionOfInterest image2DRoi = RegionOfInterest.createRegionFromImage(image2D);        
    ImageEnhancement.autoEnhanceContrastInPlace(image2D);
    //define: background(anything which is not broken pin) as white pixels, 
    //foreground(broken pin) as black pixel, modify PTH object become background.          
    final int notSmoothNormaliseImageHistogram[] = Threshold.histogram(image2D, image2DRoi, 256);
    final int normaliseImageHistogram[] = ProfileUtil.getSmoothedProfile(notSmoothNormaliseImageHistogram, 5);
    int normalisedImageThreshold = Threshold.getAutoThreshold(normaliseImageHistogram);
    //backgroundPeakIndex = background threshold value that have most pixels in image.
    //from normaliseImageThreshold to last index, 255
    int backgroundPeakIndex = StatisticsUtil.maximumIndex(normaliseImageHistogram, normalisedImageThreshold, normaliseImageHistogram.length-1);
    int backgroundPeakValue = StatisticsUtil.maximum(normaliseImageHistogram, normalisedImageThreshold, normaliseImageHistogram.length-1);
    //Foreground only use by dummyPTH here or non-dummyPTH for emptyPTH case
    int foregroundPeakIndex = StatisticsUtil.maximumIndex(normaliseImageHistogram, 0, normalisedImageThreshold); 
      
    if(isDummyPTH)
    {
      //wrong background peak due to noise, less than 20% of total pixels size
      if(backgroundPeakIndex > 200 && (backgroundPeakValue/(image2D.getWidth()*image2D.getHeight())) < 0.2f)
      {
        backgroundPeakIndex = Math.min((normaliseImageHistogram.length - 1), backgroundPeakIndex);
        backgroundPeakIndex = StatisticsUtil.maximumIndex(normaliseImageHistogram, normalisedImageThreshold, backgroundPeakIndex);
        backgroundPeakValue = StatisticsUtil.maximum(normaliseImageHistogram, normalisedImageThreshold, backgroundPeakIndex);
        //double check failed possible wrong foreground
        //modify from >200 to 195
        if(backgroundPeakIndex >= 193 && (backgroundPeakValue/(image2D.getWidth()*image2D.getHeight())) < 0.2f)
        {
          backgroundPeakIndex = Math.min((normaliseImageHistogram.length - 1), backgroundPeakIndex);
          foregroundPeakIndex = Math.min((normaliseImageHistogram.length - 1), foregroundPeakIndex);
          foregroundPeakIndex = StatisticsUtil.maximumIndex(normaliseImageHistogram, 0, foregroundPeakIndex);
          backgroundPeakIndex = StatisticsUtil.maximumIndex(normaliseImageHistogram, foregroundPeakIndex, backgroundPeakIndex);
        }
      }
    
      //modify from 20 to 26
      if(backgroundPeakIndex - foregroundPeakIndex <= 26)//background and foreground too near means no black object
      {
        float[] offsetHistogramProfile = new float[normaliseImageHistogram.length];        
        int[] offsetHistogram = new int[normaliseImageHistogram.length];
        for(int i=0; i<normaliseImageHistogram.length ; ++i)
        {
          offsetHistogram[i] = Math.max(normaliseImageHistogram[i] - 100, 0);//offset the hitogram by 100
          offsetHistogramProfile[i] = offsetHistogram[i];
        }        
        final float[] firstDeriProfile = ProfileUtil.createDerivativeProfile(offsetHistogramProfile, 3);        
        int count = 0;
        for(int i=0; i<firstDeriProfile.length; ++i)
        {
          if(firstDeriProfile[i] > 0)
          {
            ++count;
            if (count > 10)
            {
              normalisedImageThreshold = i - 5;
              break;
            }
          }
          else 
            count = Math.max(--count, 0);
        }       
      }
    }
    
    if(_IS_DEBUG)
    {
//    saveImage(image2D, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "normalize" +".png");      
      System.out.println("background:" + backgroundPeakIndex + " vs foreground:" + foregroundPeakIndex);
      System.out.println("normalisedImageThreshold:" + normalisedImageThreshold);    
    }
       
    int brokenPinAreaSizeThreshold = Math.round((float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_AREA_SIZE));    
    if (isLearning)
      brokenPinAreaSizeThreshold = 60;//fix to filter 80 pixels of noise allow only for learning
    
    // Precompute a circular mask to get rid of the pixels in the corners that we aren't interested in.
    Image combineDiagnosticVoidImage = null;
    
    int tempCounterForDebug = 0;//debug use
    List<RegionOfInterest> outerRoiForEmptyPTH = new LinkedList<RegionOfInterest>();
    Map<RegionOfInterest, Integer> brokenPinsDetected = new HashMap<RegionOfInterest, Integer>();
    //mask all detected joint 
    List<MeasurementRegionDiagnosticInfo> ringShortRegionDiagInfoList = new LinkedList<MeasurementRegionDiagnosticInfo>();
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {   
      ++tempCounterForDebug;
      // Measure the smoothed background thickness profile.
      Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair;
      try
      {
        boolean postDiagnostics = true;
        if (isLearning)
          postDiagnostics = false;
          
        RegionOfInterest roi = jointInspectionData.getOrthogonalRegionOfInterestInPixels(false);
        roi.setWidthKeepingSameCenter(roi.getWidth()/2);
        roi.setHeightKeepingSameCenter(roi.getHeight()/2);
        final float innerRoiMeanValue = Statistics.mean(image2D, roi);        
        // Get the reference ROI. - Lim, Lay Ngor add sliceNameEnum for camera 0 locator
        RegionOfInterest referenceRoi;        //we may need the locator information for learning too, please pass algorithm to learning
        if(innerRoiMeanValue < 125.f && !isDummyPTH)// && !isLearning)
          referenceRoi = Locator.locateSingleJoint(reconstructedImages, sliceNameEnum, jointInspectionData, _algorithm, postDiagnostics);       
        else 
          referenceRoi = getReferenceRoi(jointInspectionData, sliceNameEnum);
        
        ringRegionAndProfilePair = measureRingRegionBackgroundGrayLevelProfile(
          image2D, inspectionRegion, sliceNameEnum, jointInspectionData, referenceRoi);
      }
      catch (JointCannotBeInspectedBusinessException jEx)
      {
        LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
          "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[]
          { inspectionRegion });
        
        AlgorithmUtil.raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);
        image2D.decrementReferenceCount();
        return null;
      }

      final Pair<RegionOfInterest, RegionOfInterest> ringRegionPair = ringRegionAndProfilePair.getFirst();
//      final float[] ringProfile = ringRegionAndProfilePair.getSecond();
//      float median = StatisticsUtil.medianAllowModification(ringProfile);
//      final float max = StatisticsUtil.maximum(ringProfile, 0, ringProfile.length);
//      float backgroundGrayLevel = (median + max) / 2.0f;
      final float backgroundGrayLevel = 190.f;//direct set to white cause not use for threshold d

      // Create the "ring region" shape.
      final RegionOfInterest outerRoi = ringRegionPair.getFirst();
      final RegionOfInterest innerRoi = ringRegionPair.getSecond();
      if (!isLearning)//learn removed   
      { 
        //Post diagnostics information for inner & outer roi.
        GeneralPath ringShortRegion = new GeneralPath(outerRoi.getShape());
        ringShortRegion.append(innerRoi.getShape(), false);
        // Post the diagnostics for the short region (and defect zones).
        if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), _algorithm))
        {
          MeasurementRegionDiagnosticInfo ringShortRegionDiagInfo
            = new MeasurementRegionDiagnosticInfo(ringShortRegion, MeasurementRegionEnum.SHORT_BACKGROUND_REGION);
          ringShortRegionDiagInfoList.add(ringShortRegionDiagInfo);
//          _diagnostics.postDiagnostics(inspectionRegion,
//            sliceNameEnum, jointInspectionData, _algorithm, false, ringShortRegionDiagInfo);
        }
      }
      
      if(!isDummyPTH)//dummy PTH only use learn data to avoid false call
      {
        //Get the grayvalue of innerROI, this is to determine is the innerROI contain PTH pin or not.    
        final float innerRoiMeanValue = Statistics.mean(image2D, innerRoi);
        if(_IS_DEBUG)
          System.out.println("innerRoiMeanValue:" + innerRoiMeanValue + " vs normalisedImageThreshold:" + normalisedImageThreshold);

        if(innerRoiMeanValue <= normalisedImageThreshold)//Mask the CAD
          fillObjectToImageByPixel(outerRoi, image2D, 128.f, backgroundGrayLevel);     
        else//Empty PTH, innerRoiMeanValue > normalisedImageThreshold
        {
          //Empty PTH means no PTH pin inside this joint(innerROI is brighter than threshold)
          //Need to check for Broken Pin, skip masking the CAD if it is inspection, 
          outerRoiForEmptyPTH.add(outerRoi);//keep the outerROIList for learn masking exclusion use.

          if(_IS_DEBUG)        
            System.out.println("skip mask the CAD if it is inspection");

          if(isLearning)
            //Direct masked the CAD if it is learning although it is empty PTH - skip learn this area
            fillObjectToImageByPixel(outerRoi, image2D, 128.f, backgroundGrayLevel);
          else
          {
            //Detect broken pin for empty PTH inner ROI here
            Image innerImage = Image.createCopy(image2D, innerRoi);
            final int innerImageHistogram[] = Threshold.histogram(innerImage, RegionOfInterest.createRegionFromImage(innerImage), 256);
            final int innerImageThreshold = Threshold.getAutoThreshold(innerImageHistogram);      
            final float fillValue = Math.max(innerRoiMeanValue, backgroundGrayLevel);
            //Mask the inner ROI surrounding ring for background inspection use - avoid detection of PTH ring
            fillROIFrameToImageByPixel(innerRoi, image2D, innerImageThreshold, fillValue, 5, 4, inspectionRegion);

            RegionOfInterest innerImageRoi = RegionOfInterest.createRegionFromRegionBorder(10, 10, innerImage.getWidth() - 10,
              innerImage.getHeight() - 10, 0, RegionShapeEnum.OBROUND);
            final float innerImageMaxValue = Statistics.maxValue(innerImage);
            //Masked the inner ROI image except for the PTH hole - avoid detection of PTH ring
            fillImageByPixelExcludeObject(innerImageRoi, innerImage, innerImageMaxValue, innerImageMaxValue);
            if(_IS_DEBUG)
              saveImage(innerImage, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "_innerImage_" + tempCounterForDebug + ".png");    

            Threshold.threshold(innerImage, innerImageThreshold, 0, innerImageThreshold, 255);//threshold the filled_image2D
            if(_IS_DEBUG)
              saveImage(innerImage, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "_innerImageT_" + tempCounterForDebug + ".png");    

            Map<RegionOfInterest, Integer> innerRingBrokenPins = brokenPinDetectionByBlobMethod(reconstructedImages, innerImage, 
              jointInspectionDataObjects.get(0), sliceNameEnum, inspectionRegion, brokenPinAreaSizeThreshold, true, 
              null, isDummyPTH);
            if(innerRingBrokenPins != null && innerRingBrokenPins.size() > 0)
            {
              //showing the dignostic information and add the defect to global list
              for(RegionOfInterest roi  : innerRingBrokenPins.keySet())
              {                        
                //Prepare masking image to pixel count the potential defect
                Image defectObjectImage = new Image(roi.getWidth(), roi.getHeight());
                Paint.fillImage(defectObjectImage, 0.f);//reset image      
                RegionOfInterest defectObjectImageRoi = RegionOfInterest.createRegionFromImage(image2D);
                defectObjectImageRoi.setRect(roi.getMinX() + innerRoi.getMinX(), 
                  roi.getMinY() + innerRoi.getMinY(), roi.getWidth(), roi.getHeight());                            
                
                // Ensure our ROIs are constrained to the image boundaries.
                if (AlgorithmUtil.checkRegionBoundaries(defectObjectImageRoi, image2D, inspectionRegion, "Broken Pin Joint ROI") == false)
                {
                  //Means learnRectRoi hit image boundary, shift the ROI back into the image.
                  //truncate it before process - Wei Chin Fixed for large view
                  AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image2D, defectObjectImageRoi);
                }

                //Put the defects detected in Inner ROI into global list with adjusted defect ROI location
                brokenPinsDetected.put(defectObjectImageRoi, innerRingBrokenPins.get(roi));

                Image defectObjectImageRoiSource = Image.createCopy(image2D, defectObjectImageRoi);
                Threshold.threshold(defectObjectImageRoiSource, innerImageThreshold, 0, innerImageThreshold, 255);//threshold the filled_image2D
                fillImageByPixelBaseOnOriginalImage(defectObjectImageRoiSource, defectObjectImage, 125.f);       
                //Mask the detected defect only
                fillObjectToImageByPixel(defectObjectImageRoi, image2D, innerImageThreshold, fillValue);
                if (!isLearning)//learn removed   
                {            
                  _diagnostics.postDiagnostics(inspectionRegion, sliceNameEnum, subtype, _algorithm, false, true,
                    new OverlayImageDiagnosticInfo(defectObjectImage, defectObjectImageRoi, "Broken Pin"));
                }
                
                //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
                if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype))
                  combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, combineDiagnosticVoidImage, defectObjectImage, defectObjectImageRoi, sliceNameEnum);             
                
                defectObjectImage.decrementReferenceCount();//testing?
                defectObjectImageRoiSource.decrementReferenceCount();
              }
            }
            innerImage.decrementReferenceCount();
          }//!isLearning
        }
      }//if isDumyPTH do nothing
      
      if(_IS_DEBUG)
        saveImage(image2D, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "_fillCAD_" + tempCounterForDebug + ".png");       
    }//end jointInspectionData loop
    
    if(!isLearning)
    {
      _diagnostics.postDiagnostics(inspectionRegion, sliceNameEnum, jointInspectionDataObjects.get(0), _algorithm, false,
        ringShortRegionDiagInfoList.toArray(new DiagnosticInfo[ringShortRegionDiagInfoList.size()]));
    }
    
    List<Image> combineDiagnosticVoidImageList = new LinkedList<Image>();
    if(combineDiagnosticVoidImage!=null)
      combineDiagnosticVoidImageList.add(combineDiagnosticVoidImage);
    
    //Get learned object list and mask the object to reduce false call for inspection
    //Mainly is to target for masking the boundary objects.
    //Optimize by just mask the rectList for first jointinspctionData only since they are all the same.
    float backgroundGrayLevel = 180.f;
    float thresholdToFill = 150.f;//128.0f
    if(isDummyPTH)
    {
      thresholdToFill = backgroundPeakIndex;//fill if <
      backgroundGrayLevel = Math.max(backgroundPeakIndex, 220);
    }
    
    if (!isLearning)
    {
      maskLearnDataToInspectionImage(jointInspectionDataObjects.get(0), sliceNameEnum, image2D, inspectionRegion, 
        outerRoiForEmptyPTH, thresholdToFill, backgroundGrayLevel, isDummyPTH);
      if(_IS_DEBUG)
        saveImage(image2D, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "fillLearn_" + 
          jointInspectionDataObjects.get(0).getPad().getName() + ".png");
    }
    
    final float brokenPinThresholdRatio = (float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_BROKEN_PIN_THRESHOLD_RATIO);        
    final int adjustedThreshold = adjustThresholdValue(isDummyPTH, isLearning, outerRoiForEmptyPTH.size() == jointInspectionDataObjects.size(),
    backgroundPeakIndex, foregroundPeakIndex, normalisedImageThreshold, brokenPinThresholdRatio);
    Threshold.threshold(image2D, adjustedThreshold, 0, adjustedThreshold, 255);//threshold the filled_image2D
    if(_IS_DEBUG)
      saveImage(image2D, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + jointInspectionDataObjects.get(0).getPad().getName() + 
        "_threshold_" + adjustedThreshold +".png");
        
    if(_ENABLE_BORDER_FILTER && !isLearning)
    {
      final int borderMaskingSize = 2;
      maskImageBorder(image2D, borderMaskingSize, backgroundPeakIndex);//change 255 to backgroundPeakIndex
    }
    
    //Reduce small noise and combine them together if near
    Image imageMorpho = image2D;
    image2D = Filter.closing(imageMorpho, image2DRoi, 2);
    imageMorpho.decrementReferenceCount();    
    if(_IS_DEBUG)
      saveImage(image2D, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + jointInspectionDataObjects.get(0).getPad().getName() + 
        "_closing_" + adjustedThreshold +".png");    
    
    if(!isLearning)
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, algorithm);
    
    //Detect extra object(broken pin) in image
    Map<RegionOfInterest, Integer> brokenPinDetectedAtBackground = brokenPinDetectionByBlobMethod(reconstructedImages, image2D, 
      jointInspectionDataObjects.get(0), sliceNameEnum, inspectionRegion, brokenPinAreaSizeThreshold, isLearning,
      combineDiagnosticVoidImageList, isDummyPTH);
    
    //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
    if(combineDiagnosticVoidImageList.size() > 0)
    {
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImageList.get(combineDiagnosticVoidImageList.size()-1), sliceNameEnum);
      combineDiagnosticVoidImageList.get(combineDiagnosticVoidImageList.size()-1).decrementReferenceCount();
      combineDiagnosticVoidImageList.clear();
    }          
    
    if(brokenPinDetectedAtBackground!= null && brokenPinDetectedAtBackground.size() > 0)
      brokenPinsDetected.putAll(brokenPinDetectedAtBackground);

    image2D.decrementReferenceCount();
    return brokenPinsDetected;
  }

  /**
   * Adjust the threshold value so that the defect able to threshold as foreground.
   * Broken pin defects gray value is found usually drop in between foreground(PTH) and background gray value.
   * @author Lim, Lay Ngor
   */  
  public static int adjustThresholdValue(final boolean isDummyPTH, final boolean isLearning, final boolean isAllEmptyPTHCAD,
    final float backgroundPeakIndex, final float foregroundPeakIndex, final int normalisedImageThreshold, 
    final float brokenPinThresholdRatio)
  {
    Assert.expect(backgroundPeakIndex >= 0 || backgroundPeakIndex < 256);
    Assert.expect(foregroundPeakIndex >= 0 || foregroundPeakIndex < 256);
    Assert.expect(normalisedImageThreshold >= 0 || normalisedImageThreshold < 256);
    
    //final int adjustedThreshold = (int) ((backgroundPeakIndex + normalisedImageThreshold) * 0.38f);
    int adjustedThreshold = 0;
    //Assume the ring profile can represent the background gray value?
    //numberOfEmptyPTHs == jointInspectionDataObjects.size() = isAllEmptyPTHCAD = all joints in CAD is empty PTH
    if(isAllEmptyPTHCAD && !isDummyPTH && !_IS_LARGE_VIEW)
    {
      //assume backgroundPeakIndex is actually empty PTH grayvalue(innerRoiMeanValue), 
      //should shift the background to foreground hill instead, add 0.008 to make it less sensitive to noise
      final float adjustedRatio = brokenPinThresholdRatio/2.0f;
      adjustedThreshold = (int)(foregroundPeakIndex/2.f + foregroundPeakIndex*adjustedRatio);
      //Learning threshold may be need to be less sensitive - so that no escape - means lesser than inspection threshold
      if(_IS_DEBUG)
        System.out.println("empty PTH: adjustedThreshold" + adjustedThreshold);
    }
    else//for dummyPTH & largeView
    { //Learning threshold is less sensitive - so that no escape??
      if(isDummyPTH && (backgroundPeakIndex - foregroundPeakIndex <= 26))
      {
//        adjustedThreshold = normalisedImageThreshold;
        adjustedThreshold = (int)(normalisedImageThreshold + 127*(brokenPinThresholdRatio - 0.5));
      }
      else
      {
//        if(isLearning)//Learning threshold is 1% more sensitive
//          adjustedThreshold = (int) ((backgroundPeakIndex + normalisedImageThreshold) * (brokenPinThresholdRatio + 0.01)); 
//        else
          adjustedThreshold = (int) ((backgroundPeakIndex + normalisedImageThreshold) * brokenPinThresholdRatio);
      }
      if(_IS_DEBUG)
        System.out.println("normal: adjustedThreshold" + adjustedThreshold);      
    }
      
    //clipping
    adjustedThreshold = Math.min(adjustedThreshold, 255);
    adjustedThreshold = Math.max(adjustedThreshold, 0); 
    
    return adjustedThreshold;
  }

  /**
   * @author Lim, Lay Ngor
   */
  public static void maskImageBorder(Image image, int borderMaskingSize, int maskGrayValue)
  {
    Assert.expect(image != null);
    Assert.expect(borderMaskingSize > 0);
    Assert.expect(maskGrayValue >= 0 || maskGrayValue <= 255);
    Assert.expect(image.getWidth() > borderMaskingSize);
    Assert.expect(image.getHeight() > borderMaskingSize);

    final java.awt.Rectangle top = new java.awt.Rectangle(0, 0, image.getWidth(), borderMaskingSize);
    final java.awt.Rectangle bottom = new java.awt.Rectangle(0, image.getHeight() - borderMaskingSize, image.getWidth(), borderMaskingSize);
    final java.awt.Rectangle left = new java.awt.Rectangle(0, 0, borderMaskingSize, image.getHeight());
    final java.awt.Rectangle right = new java.awt.Rectangle(image.getWidth() - borderMaskingSize, 0, borderMaskingSize, image.getHeight());
    Paint.fillRegion(image, top, maskGrayValue);
    Paint.fillRegion(image, bottom, maskGrayValue);
    Paint.fillRegion(image, left, maskGrayValue);
    Paint.fillRegion(image, right, maskGrayValue);   
  }
  
  /**
   * Mask the all learn rectangle to inspection image.
   * @author Lim, Lay Ngor
   */
  public static void maskLearnDataToInspectionImage(
    JointInspectionData firstJointInspectionData,
    SliceNameEnum sliceNameEnum,
    Image image2D,
    ReconstructionRegion inspectionRegion,
    List<RegionOfInterest> emptyPTHOuterRoiList,
    float thresholdToFill,
    float backgroundGrayLevel,
    boolean isDummyPTH) throws DatastoreException 
  {
    Assert.expect(firstJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(image2D != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(emptyPTHOuterRoiList != null);
    
    final int profileEdgeOffset = (isDummyPTH) ? 4 : 2;
    List<java.awt.Rectangle> rectList = getLearnBrokenPinBorderObject(firstJointInspectionData, sliceNameEnum);
    if (rectList != null && rectList.size() > 0)
    {
      Image black = new Image(image2D.getWidth(), image2D.getHeight());//failed to move in if case
      if(_IS_DEBUG)
        Paint.fillImage(black, 0.f);

      final float inputThresholdToFill = thresholdToFill;
      List<MeasurementRegionDiagnosticInfo> rectRegionDiagInfoList = new LinkedList<MeasurementRegionDiagnosticInfo>();
      for (java.awt.Rectangle rect : rectList)
      {
        //Adjust the rect larger a bit - hardcoded
        RegionOfInterest learnRectRoi = RegionOfInterest.createRegionFromImage(image2D);  
        RegionOfInterest learnRectRoiOri = new RegionOfInterest(learnRectRoi);
        learnRectRoi.setRect(rect.x - profileEdgeOffset, rect.y  - profileEdgeOffset, 
         rect.width + (2 * profileEdgeOffset), rect.height + (2 * profileEdgeOffset));   
        learnRectRoiOri.setRect(rect.x, rect.y, rect.width, rect.height);
        // Ensure our ROIs are constrained to the image boundaries.
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image2D, learnRectRoi);
        AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image2D, learnRectRoiOri);
        
        //Shows all learn ROI
        MeasurementRegionDiagnosticInfo rectRegionDiagInfo
          = new MeasurementRegionDiagnosticInfo(learnRectRoi, MeasurementRegionEnum.LEARN_REGION);
        rectRegionDiagInfoList.add(rectRegionDiagInfo);

        if(isDummyPTH && rect.width > 5 && rect.height > 5)//only perform if it is PTH area and large learn rect.
        {
          //circle detection
          final int notSmoothNormaliseImageHistogram2[] = Threshold.histogram(image2D, learnRectRoiOri, 256);
          final int normaliseImageHistogram2[] = ProfileUtil.getSmoothedProfile(notSmoothNormaliseImageHistogram2, 5);
          int newThresholdToFill2 = Threshold.getAutoThreshold(normaliseImageHistogram2);
          Image newImage2 = Image.createCopy(image2D, RegionOfInterest.createRegionFromImage(image2D));
          Threshold.threshold(newImage2, newThresholdToFill2, 0, newThresholdToFill2, 255);

          RegionOfInterest reDetectRoi = new RegionOfInterest(learnRectRoiOri);
          Image newImage3 = Image.createCopy(image2D, learnRectRoiOri);
          Threshold.threshold(newImage3, newThresholdToFill2, 0, newThresholdToFill2, 255);
          List<java.awt.Shape> inspectShapeList = BlobAnalyzer.getInstance().blobDetection(newImage3);
          newImage3.decrementReferenceCount();
          for (java.awt.Shape element : inspectShapeList)
          {
            reDetectRoi.setRect(learnRectRoiOri.getMinX() + element.getBounds().x, learnRectRoiOri.getMinY() + element.getBounds().y,
              element.getBounds().width, element.getBounds().height);
            AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image2D, reDetectRoi);
            if(element.getBounds().width > 1 && element.getBounds().height > 1)
              break;//assume only one detected
          }

          final boolean isDebug = false;
          boolean isCircle = false;
          //boundary detection
          float meanTopLeft = 0, meanBottomLeft = 0, meanTopRight = 0, meanBottomRight = 0;
          final int imageHalfHeight = reDetectRoi.getHeight() / 2;
          final int imageHalfWidth = reDetectRoi.getWidth() / 2;
          final int imageQuarterHeight = reDetectRoi.getHeight() / 4;
          final int imageQuarterWidth = reDetectRoi.getWidth() / 4;
          if (learnRectRoiOri.getMaxX() + 2 > image2D.getWidth())
          {
            if(learnRectRoiOri.getMinY() -2 < 0 && learnRectRoiOri.getMaxY() + 2 > image2D.getHeight()
              && Math.abs(reDetectRoi.getWidth() - reDetectRoi.getHeight()) < 25)
            {
              meanTopLeft = triangleAreaMeanValueWithoutRotation(newImage2, reDetectRoi.getMinX(), reDetectRoi.getMinY(),
                Math.max(reDetectRoi.getMinX(), reDetectRoi.getMinX() + imageHalfWidth), Math.max(reDetectRoi.getMinY(), reDetectRoi.getMinY() + imageQuarterHeight), 1, isDebug);
              meanBottomLeft = triangleAreaMeanValueWithoutRotation(newImage2, reDetectRoi.getMinX(), Math.max(0, reDetectRoi.getMaxY() - imageQuarterHeight),
                Math.max(reDetectRoi.getMinX(), reDetectRoi.getMinX() + imageHalfWidth), reDetectRoi.getMaxY(), 2, isDebug);
              if (meanTopLeft > 125 && meanBottomLeft > 125)
                isCircle = true;
            }
          }
          else if (learnRectRoiOri.getMinX() -2 < 0)
          {
            if(learnRectRoiOri.getMinY() -2 < 0 && learnRectRoiOri.getMaxY() + 2 > image2D.getHeight()
              && Math.abs(reDetectRoi.getWidth() - reDetectRoi.getHeight()) < 25)
            {
              meanTopRight = triangleAreaMeanValueWithoutRotation(newImage2, reDetectRoi.getMaxX() - imageHalfWidth, reDetectRoi.getMinY(),
                reDetectRoi.getMaxX(), Math.max(reDetectRoi.getMinY(), reDetectRoi.getMinY() + imageQuarterHeight), 3, isDebug);
              meanBottomRight = triangleAreaMeanValueWithoutRotation(newImage2, Math.max(0, reDetectRoi.getMaxX() - imageHalfWidth),
                reDetectRoi.getMaxY() - imageQuarterHeight, reDetectRoi.getMaxX(), reDetectRoi.getMaxY(), 4, isDebug);
              if (meanTopRight > 125 && meanBottomRight > 125)
                isCircle = true;
            }
          }
          else if (learnRectRoiOri.getMinY() -2 < 0)
          {
            if(learnRectRoiOri.getMinX() -2 < 0 && learnRectRoiOri.getMaxX() + 2 > image2D.getWidth()
              && Math.abs(reDetectRoi.getWidth() - reDetectRoi.getHeight()) < 25)
            {
              meanBottomLeft = triangleAreaMeanValueWithoutRotation(newImage2, reDetectRoi.getMinX(), Math.max(0, reDetectRoi.getMaxY() - imageHalfHeight),
                Math.max(reDetectRoi.getMinX(), reDetectRoi.getMinX() + imageQuarterWidth), reDetectRoi.getMaxY(), 2, isDebug);
              meanBottomRight = triangleAreaMeanValueWithoutRotation(newImage2, Math.max(0, reDetectRoi.getMaxX() - imageQuarterWidth),
                reDetectRoi.getMaxY() - imageHalfHeight, reDetectRoi.getMaxX(), reDetectRoi.getMaxY(), 4, isDebug);
              if (meanBottomLeft > 125 && meanBottomRight > 125)
                isCircle = true;
            }
          }
          else if (learnRectRoiOri.getMaxY() + 2 > image2D.getHeight())
          {
            if(learnRectRoiOri.getMinX() -2 < 0 && learnRectRoiOri.getMaxX() + 2 > image2D.getWidth()
              && Math.abs(reDetectRoi.getWidth() - reDetectRoi.getHeight()) < 25)
            {
              meanTopLeft = triangleAreaMeanValueWithoutRotation(newImage2, reDetectRoi.getMinX(), reDetectRoi.getMinY(),
                Math.max(reDetectRoi.getMinX(), reDetectRoi.getMinX() + imageQuarterWidth), Math.max(reDetectRoi.getMinY(), reDetectRoi.getMinY() + imageHalfHeight), 1, isDebug);
              meanTopRight = triangleAreaMeanValueWithoutRotation(newImage2, Math.max(0, reDetectRoi.getMaxX() - imageQuarterWidth), reDetectRoi.getMinY(),
                reDetectRoi.getMaxX(), Math.max(reDetectRoi.getMinY(), reDetectRoi.getMinY() + imageHalfHeight), 3, isDebug);
              if (meanTopLeft > 125 && meanTopRight > 125)
                isCircle = true;
            }
          }
          else
          {
            if(Math.abs(reDetectRoi.getWidth() - reDetectRoi.getHeight()) < 25)
            {
              meanTopLeft = triangleAreaMeanValueWithoutRotation(newImage2, reDetectRoi.getMinX(), reDetectRoi.getMinY(),
                Math.max(reDetectRoi.getMinX(), reDetectRoi.getMinX() + imageQuarterWidth), Math.max(reDetectRoi.getMinY(), reDetectRoi.getMinY() + imageQuarterHeight), 1, isDebug);
              meanBottomLeft = triangleAreaMeanValueWithoutRotation(newImage2, reDetectRoi.getMinX(), Math.max(0, reDetectRoi.getMaxY() - imageQuarterHeight),
                Math.max(reDetectRoi.getMinX(), reDetectRoi.getMinX() + imageQuarterWidth), reDetectRoi.getMaxY(), 2, isDebug);
              meanTopRight = triangleAreaMeanValueWithoutRotation(newImage2, Math.max(0, reDetectRoi.getMaxX() - imageQuarterWidth), reDetectRoi.getMinY(),
                reDetectRoi.getMaxX(), Math.max(reDetectRoi.getMinY(), reDetectRoi.getMinY() + imageQuarterHeight), 3, isDebug);
              meanBottomRight = triangleAreaMeanValueWithoutRotation(newImage2, Math.max(0, reDetectRoi.getMaxX() - imageQuarterWidth),
                Math.max(0, reDetectRoi.getMaxY() - imageQuarterHeight), reDetectRoi.getMaxX(), reDetectRoi.getMaxY(), 4, isDebug);

              int strongCircleEdge = 0;
              if (meanTopLeft > 125) ++strongCircleEdge;
              if (meanBottomLeft > 125) ++strongCircleEdge;
              if (meanTopRight > 125) ++strongCircleEdge;
              if (meanBottomRight > 125) ++strongCircleEdge;
              if (strongCircleEdge >= 2)
                isCircle = true;
            }
          }

          final float roiMeanValue = Statistics.mean(image2D, learnRectRoiOri);
          if(_IS_DEBUG)        
          {
            saveImage(newImage2, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "learnrectimage2D" + ".png"); 
                      System.out.println(isCircle + "," + meanTopRight + "," + meanBottomRight);              
            System.out.println("roiMeanValue" + roiMeanValue);
            System.out.println("isCircle" + isCircle);
            saveImage(newImage2, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "circleDetect" + ".png");
          }
          newImage2.decrementReferenceCount();            

            if (roiMeanValue < 145.f && isCircle)//inside is dark means is circle 
          {
            final int notSmoothNormaliseImageHistogram[] = Threshold.histogram(image2D, learnRectRoi, 256);
            final int normaliseImageHistogram[] = ProfileUtil.getSmoothedProfile(notSmoothNormaliseImageHistogram, 5);
            int adjustValue = 2;
            float roiMinimumValue = Statistics.minValue(image2D, learnRectRoi);
            for (int i = 0; i < normaliseImageHistogram.length; i++)
            {
              if (normaliseImageHistogram[i] > 0)
              {
                roiMinimumValue = i;
                break;
              }
            }

            if (_IS_DEBUG)
              System.out.println(roiMeanValue + ", " + roiMinimumValue);

            if (roiMinimumValue > 40)
              adjustValue = 28;

            int newThresholdToFill = Threshold.getAutoThreshold(normaliseImageHistogram) + adjustValue;
            thresholdToFill = newThresholdToFill;//Math.min(inputThresholdToFill, newThresholdToFill);
            if (backgroundGrayLevel < thresholdToFill)
              backgroundGrayLevel = backgroundGrayLevel + 10;
          }
          else
            thresholdToFill = inputThresholdToFill +50;
        }
        
        //Filter both boundary and internal learn objects 
        fillObjectToImageByPixelExcludeEmptyPTH(learnRectRoi, image2D, thresholdToFill, backgroundGrayLevel, emptyPTHOuterRoiList);//backgroundGrayLevel        
        if(_IS_DEBUG)
        {
          _diagnostics.postDiagnostics(inspectionRegion, sliceNameEnum, firstJointInspectionData.getSubtype(), _algorithm, false, true,
            rectRegionDiagInfo);          
          fillObjectToImageByPixelExcludeEmptyPTH(learnRectRoi, black, thresholdToFill, backgroundGrayLevel, emptyPTHOuterRoiList);//backgroundGrayLevel
        }
      }
      
        _diagnostics.postDiagnostics(inspectionRegion, sliceNameEnum, firstJointInspectionData.getSubtype(), _algorithm, false, true,
          rectRegionDiagInfoList.toArray(new DiagnosticInfo[rectRegionDiagInfoList.size()]));      
      if(_IS_DEBUG)
        saveImage(black, "D:/09ABI/04Algo/06 Broken Pin/5Actual/debug/" + "blackExcludeEmptyPTH_" + ".png");

      black.decrementReferenceCount();
    }  
  }
 
/**
 * 
 */
  public static float triangleAreaMeanValueWithoutRotation(Image image, int startX, int startY, int endX, int endY, int side, boolean isDebug)
  {
    Assert.expect(image!=null);
    if(isDebug) System.out.println(startX + "," + startY + "," + endX + "," + endY);
    Assert.expect(startX >= 0 && startY >= 0);
    Assert.expect(endX < image.getWidth() && endY < image.getHeight());
    Assert.expect(endX >= startX && endY >=startY);
    
    float totalGrayValue = 0;
    int counter = 0;
    if(side == 1) //topleft
    {
      for(int i=startY; i <endY; ++i)
      {
        for(int j=startX; j<endX; ++j)
        {
          totalGrayValue += image.getPixelValue(j, i);
          if(isDebug)
            image.setPixelValue(j, i, 125);
          ++counter;
        }
        endX = endX-1;
        if(endX < 0) break;
      }
    }
    else if(side == 2) //bottomleft
    {
      for(int i=endY; i >startY; --i)
      {
        for(int j=startX; j<endX; ++j)
        {
          totalGrayValue += image.getPixelValue(j, i);
          if(isDebug)
            image.setPixelValue(j, i, 125);
          ++counter;
        }
        endX = endX-1;
        if(endX < 0) break;
      }      
    }
    else if(side == 3)//topRight
    {
      for(int i=startY; i <endY; ++i)
      {
        for(int j=endX; j>startX; --j)
        {
          totalGrayValue += image.getPixelValue(j, i);
          if(isDebug)
            image.setPixelValue(j, i, 125);
          ++counter;
        }
        startX = startX+1;
        if(startX >= image.getWidth()) break;
      }      
    }
    else if(side == 4)
    {
      for(int i=endY; i >startY; --i)
      {
        for(int j=endX; j>startX; --j)
        {
          totalGrayValue += image.getPixelValue(j, i);
          if(isDebug)
            image.setPixelValue(j, i, 125);
          ++counter;
        }
        startX = startX+1;
        if(startX >= image.getWidth()) break;
      }       
    }
    else
      Assert.expect(false);
    
    if(counter == 0)
      return 0;
    return (totalGrayValue/(float)counter);
  }

  /**
   * If return is empty shape means inspection pass.
   * @author Lim, Lay Ngor
   */   
  public static Map<RegionOfInterest, Integer> brokenPinDetectionByBlobMethod(
    ReconstructedImages reconstructedImages,    
    final Image image2D,
    JointInspectionData firstJointInspectionData,
    SliceNameEnum sliceNameEnum,
    ReconstructionRegion inspectionRegion,
    final int brokenPinAreaSizeThreshold,
    final boolean isLearning,
    List<Image> combineDiagnosticVoidImageList,
    final boolean isDummyPTH)
  {
    Assert.expect(image2D != null);
    Assert.expect(firstJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(brokenPinAreaSizeThreshold > 1);//Minimum Broken Pin Area Size
    if(!isLearning)
      Assert.expect(_algorithm != null);
       
    Subtype subtype = firstJointInspectionData.getSubtype();
    
    // Precompute a circular mask to get rid of the pixels in the corners that we aren't interested in.
    Image combineDiagnosticVoidImage = null; 
    if(combineDiagnosticVoidImageList!=null && combineDiagnosticVoidImageList.size() > 0)
      combineDiagnosticVoidImage = combineDiagnosticVoidImageList.get(combineDiagnosticVoidImageList.size()-1);
    
    //Detect extra object(broken pin) in image
    List<java.awt.Shape> inspectShapeList = BlobAnalyzer.getInstance().blobDetection(image2D);
    Map<RegionOfInterest, Integer> brokenPinToTotalDefectPixels = new HashMap<RegionOfInterest, Integer>();
    for (java.awt.Shape element : inspectShapeList)
    { 
      final int elementArea = element.getBounds().width * element.getBounds().height;
      if (elementArea > brokenPinAreaSizeThreshold)//fast filter small box defect
      {
        //Prepare masking image to pixel count the potential defect
        Image defectObjectImage = new Image(element.getBounds().width, element.getBounds().height);
        Paint.fillImage(defectObjectImage, 0.f);//reset image      
        RegionOfInterest defectObjectImageRoi = RegionOfInterest.createRegionFromImage(image2D);
        defectObjectImageRoi.setRect(element.getBounds().getMinX(), element.getBounds().getMinY(),
          element.getBounds().getWidth(), element.getBounds().getHeight());
        Image defectObjectImageRoiSource = Image.createCopy(image2D, defectObjectImageRoi);
        //please optimise use real large source image and dump roi into it
        final int totalPixel = fillImageByPixelBaseOnOriginalImage(defectObjectImageRoiSource, defectObjectImage, 125.f);
        if (totalPixel > brokenPinAreaSizeThreshold) //filter small defect base on pixel
        {
          //defect reconfirmation
          boolean isBrokenPin = true;//if it is not dummyPTH, we don't need to confirm it is background 

          //only perform isObject if it is dummy PTH
          if(isDummyPTH && !isLearning)
          {
            //Determine it is background noise or real BrokenPin using profile compare - START
            Image originalImageForPeakDetection = Image.createCopy(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage(), defectObjectImageRoi);
            isBrokenPin = isObject(originalImageForPeakDetection);
            originalImageForPeakDetection.decrementReferenceCount();
          }
          
          if (!isLearning && isDummyPTH && isBrokenPin == false)
          {     
            //check is it we are so lucky to get a fully vertical or horizontal broken pin
            //match top and bottom sides only
            RegionOfInterest imageRoi = RegionOfInterest.createRegionFromImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage());
            final int offset = 10;
            final int offset2 = 4;
            if(element.getBounds().width > element.getBounds().height + 30)//if width and height about the same, use width(center pin)
            {
              RegionOfInterest templateLeftRoi = new RegionOfInterest(defectObjectImageRoi);
              RegionOfInterest templateRightRoi = new RegionOfInterest(defectObjectImageRoi);                  
              final double startY = Math.max(0, element.getBounds().getMinY() - offset2);
              double width = Math.min(offset, imageRoi.getWidth());
              double height = element.getBounds().height;
              if((startY + element.getBounds().height + (offset2 * 2)) < imageRoi.getHeight())
                height = Math.min(element.getBounds().height + (offset2 * 2), imageRoi.getHeight());            
              templateLeftRoi.setRect(element.getBounds().getMinX(), startY, width, height);
              templateRightRoi.setRect(element.getBounds().getMaxX() - offset, startY, width, height); 
              
              if (element.getBounds().getMinX() - offset > 5
                && (templateLeftRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage())))//top side comparison
              {            
                double startX2 = Math.max(element.getBounds().getMinX() - offset, 0);
                RegionOfInterest compareRoi = new RegionOfInterest(defectObjectImageRoi);
                compareRoi.setRect(startX2, startY, width, height);
                if (templateLeftRoi.getOrientationInDegrees() == 0 || templateLeftRoi.getOrientationInDegrees() == 180)
                {
                  templateLeftRoi.setOrientationInDegrees(90);
                  compareRoi.setOrientationInDegrees(90);
                }
                
                //temporary handling - please make it better.
                if(compareRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage()))                
                  isBrokenPin = reverifyBrokenPinOrBackground(reconstructedImages, firstJointInspectionData, inspectionRegion,
                    sliceNameEnum, templateLeftRoi, compareRoi, isLearning, defectObjectImageRoi);
                else
                  isBrokenPin = true;
              }
              else if (element.getBounds().getMaxY() + height < imageRoi.getMaxY()
                && (templateRightRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage())))//bottom side comparison
              {                  
                double startX2 = element.getBounds().getMaxX();
                RegionOfInterest compareRoi = new RegionOfInterest(defectObjectImageRoi);
                compareRoi.setRect(startX2, startY, width, height);
                if (templateRightRoi.getOrientationInDegrees() == 0 || templateRightRoi.getOrientationInDegrees() == 180)
                {
                  templateRightRoi.setOrientationInDegrees(90);
                  compareRoi.setOrientationInDegrees(90);
                }

                //temporary handling - please make it better.
                if(compareRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage()))
                  isBrokenPin = reverifyBrokenPinOrBackground(reconstructedImages, firstJointInspectionData, inspectionRegion,
                    sliceNameEnum, templateRightRoi, compareRoi, isLearning, defectObjectImageRoi);
                else
                  isBrokenPin = true;
              }
              else
                isBrokenPin = true;//if cannot compare make it false call?
            }
            else
            {
              RegionOfInterest templateTopRoi = new RegionOfInterest(defectObjectImageRoi);
              RegionOfInterest templateBottomRoi = new RegionOfInterest(defectObjectImageRoi);              
              double startX = Math.max(element.getBounds().getMinX() - offset2, 0);
              double width = element.getBounds().width;
              if ((startX + element.getBounds().width + (offset2 * 2)) < imageRoi.getWidth())
              {
                width = Math.min(element.getBounds().width + (offset2 * 2), imageRoi.getWidth());
              }
              double height = Math.min(offset, imageRoi.getHeight());
              templateTopRoi.setRect(startX, element.getBounds().getMinY(), width, height);
              templateBottomRoi.setRect(startX, element.getBounds().getMaxY() - offset, width, height);

              if (element.getBounds().getMinY() - offset > 5
                && (templateTopRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage())))//top side comparison
              {
                double startY = Math.max(element.getBounds().getMinY() - offset, 0);
                RegionOfInterest compareRoi = new RegionOfInterest(defectObjectImageRoi);
                compareRoi.setRect(startX, startY, width, height);
                //temporary handling - please make it better.
                if(compareRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage()))                  
                  isBrokenPin = reverifyBrokenPinOrBackground(reconstructedImages, firstJointInspectionData, inspectionRegion,
                    sliceNameEnum, templateTopRoi, compareRoi, isLearning, defectObjectImageRoi);
                else
                  isBrokenPin = true;
              }
              else if (element.getBounds().getMaxY() + height < imageRoi.getMaxY()
                && (templateBottomRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage())))//bottom side comparison
              {
                double startY = element.getBounds().getMaxY();
                RegionOfInterest compareRoi = new RegionOfInterest(defectObjectImageRoi);
                compareRoi.setRect(startX, startY, width, height);

                //temporary handling - please make it better.
                if(compareRoi.fitsWithinImage(reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage()))
                  isBrokenPin = reverifyBrokenPinOrBackground(reconstructedImages, firstJointInspectionData, inspectionRegion,
                    sliceNameEnum, templateBottomRoi, compareRoi, isLearning, defectObjectImageRoi);
                else 
                  isBrokenPin = true;
              }
              else
              {
                isBrokenPin = true;//if cannot compare make it false call?
              }
            }
          }
          //Determine it is background noise or real BrokenPin using profile compare - END

          if(isBrokenPin)
          {
            //defect detected
            brokenPinToTotalDefectPixels.put(defectObjectImageRoi, totalPixel);
            if (!isLearning)//show detected defects at short diagnostic
              _diagnostics.postDiagnostics(inspectionRegion, sliceNameEnum, subtype, _algorithm, false, true,
                new OverlayImageDiagnosticInfo(defectObjectImage, defectObjectImageRoi, "Broken Pin"));

            //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
            if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype))
              combineDiagnosticVoidImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, combineDiagnosticVoidImage, defectObjectImage, defectObjectImageRoi, sliceNameEnum);             
          }
        }
        defectObjectImage.decrementReferenceCount();
        defectObjectImageRoiSource.decrementReferenceCount();
      }
    }
   
    if(ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(subtype) && combineDiagnosticVoidImage!=null
      && combineDiagnosticVoidImageList!=null)
    {
      if(combineDiagnosticVoidImageList.size()>0)
      {
        combineDiagnosticVoidImageList.get(combineDiagnosticVoidImageList.size() - 1);
        combineDiagnosticVoidImageList.clear();
      }
      
      combineDiagnosticVoidImageList.add(combineDiagnosticVoidImage);
    }
//    //Wei Chin - XCR-2387 - Save Diagnostic Slice Image
//    if(combineDiagnosticVoidImage != null)
//    {
//      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineDiagnosticVoidImage, sliceNameEnum);
//      combineDiagnosticVoidImage.decrementReferenceCount();
//    }   
    
    return brokenPinToTotalDefectPixels;
  }
  
// Returns true if two rectangles (l1, r1) and (l2, r2) overlap
  static boolean doOverlap(java.awt.Point l1, java.awt.Point r1, java.awt.Point l2, java.awt.Point r2)
  {
    // If one rectangle is on left side of other
    if (l1.x > r2.x || l2.x > r1.x)
      return false;

    // If one rectangle is above other
    if (l1.y < r2.y || l2.y < r1.y)
      return false;

    return true;
  }

  static boolean reverifyBrokenPinOrBackground(ReconstructedImages reconstructedImages, 
    JointInspectionData jointInspectionData, ReconstructionRegion inspectionRegion,
    SliceNameEnum sliceNameEnum, RegionOfInterest templateRoi, 
    RegionOfInterest compareRoi, boolean isLearning, RegionOfInterest defectObjectImageRoi)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);    
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(templateRoi != null);
    Assert.expect(compareRoi != null);
    Assert.expect(defectObjectImageRoi != null);
        
    boolean isBrokenPin = false;    
    boolean isOverlap = false;
    Subtype subtype = jointInspectionData.getSubtype();
    float[] grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(
      reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage(), templateRoi);
    float[] grayLevelProfileAcrossComponent2 = ImageFeatureExtraction.profile(
      reconstructedImages.getReconstructedSlice(sliceNameEnum).getOrthogonalImage(), compareRoi);
    float[] result = ArrayUtil.subtractArrays(grayLevelProfileAcrossComponent, grayLevelProfileAcrossComponent2);

    if (!isLearning)//only for debugging
    {
      MeasurementRegionDiagnosticInfo templateRegionDiagInfo
        = new MeasurementRegionDiagnosticInfo(templateRoi, MeasurementRegionEnum.SHORT_BACKGROUND_REGION);
      MeasurementRegionDiagnosticInfo compareRegionDiagInfo
        = new MeasurementRegionDiagnosticInfo(compareRoi, MeasurementRegionEnum.SHORT_BACKGROUND_REGION);
      MeasurementRegionDiagnosticInfo defectInfo
        = new MeasurementRegionDiagnosticInfo(defectObjectImageRoi, MeasurementRegionEnum.EXONERATED_SHORT_DEFECT_REGION);
      _diagnostics.postDiagnostics(inspectionRegion, sliceNameEnum, subtype, _algorithm, false, true,
        templateRegionDiagInfo, compareRegionDiagInfo, defectInfo);
    }

    for (int i = 0; i < result.length; ++i)
    {
      if (result[i] > 30 || result[i] < -30)//very different
      {
        int counter = 0;
        for (int j = i; j < result.length; ++j)
        {
          ++counter;
          if (counter > 5)//continuous 5 times different profile
          {
            isBrokenPin = true;
            break;
          }
          ++i;
        }
      }
    }
    
    if(!isBrokenPin)//reconfirm to avoid escape
    {
      try
      {
        List<java.awt.Rectangle> rectList = getLearnBrokenPinBorderObject(jointInspectionData, sliceNameEnum);   
        if (rectList != null && rectList.size() > 0)
        {
          for (java.awt.Rectangle rect : rectList)
          { 
    //      Rectangle.intersect(rect, compareRoi.getRectangle2D(), rect);
            java.awt.Point l1 = new java.awt.Point((int) rect.getMinX(), (int) rect.getMinY());
            java.awt.Point l2 = new java.awt.Point((int) compareRoi.getMinX(), (int) compareRoi.getMinY());
            java.awt.Point r1 = new java.awt.Point((int) rect.getMaxX(), (int) rect.getMaxY());
            java.awt.Point r2 = new java.awt.Point((int) compareRoi.getMaxX(), (int) compareRoi.getMaxY());
            if (doOverlap(l1, r1, l2, r2))
            {
              isOverlap = true;
              break;//stop the loop
            }
          }
        }
      }
      catch (DatastoreException ex)
      {
        System.out.println(ex);
      }
    }
    
    if (isBrokenPin == false && isOverlap == true)
    {
      //report if it is long to avoid escape
      double ratio;
      if(defectObjectImageRoi.getWidth() > defectObjectImageRoi.getHeight())
        ratio = defectObjectImageRoi.getWidth() / defectObjectImageRoi.getHeight();
      else
        ratio = defectObjectImageRoi.getHeight() / defectObjectImageRoi.getWidth();
      
      if (ratio > 3)
        isBrokenPin = true;
    }
    
    return isBrokenPin;
  }
  
  /*
   *  @author Lim, Lay Ngor
   */
  static boolean isObject(Image image)
  {
    Assert.expect(image != null);
    
    final int notSmoothNormaliseImageHistogram[] = Threshold.histogram(image, RegionOfInterest.createRegionFromImage(image), 256);
    final int normaliseImageHistogram[] = ProfileUtil.getSmoothedProfile(notSmoothNormaliseImageHistogram, 5);

    double[] histo = new double[normaliseImageHistogram.length];
    for (int i = 0; i < normaliseImageHistogram.length; ++i)
    {
      histo[i] = (double) normaliseImageHistogram[i];
    }

    final int totalPeaks[] = findMaxima(histo, 5, true);
    if (totalPeaks.length > 1)//more than a peak means have clear background and foreground.
    {
      if(totalPeaks.length == 2)//if there are two peaks, we need to ensure those peak have big different
      {
        final int different = Math.abs(totalPeaks[0] - totalPeaks[1]);
        //small difference shows less contrast, possible not object
        return (different > 35);
      }
      //consider more than 2 peaks is defects.
      return true;
    }
    //less than a peak means background
    return false;
  }  
  
    /**
    * Calculates peak positions of 1D array N.Vischer, 13-sep-2013
    *
    * @param xx Array containing peaks.
    * @param tolerance Depth of a qualified valley must exceed tolerance.
    * Tolerance must be >= 0. Flat tops are marked at their centers.
    * @param  excludeOnEdges If 'true', a peak is only
    * accepted if it is separated by two qualified valleys. If 'false', a peak
    * is also accepted if separated by one qualified valley and by a border.
    * @return Positions of peaks, sorted with decreasing amplitude
    */
  public static int[] findMaxima(double[] xx, double tolerance, boolean excludeOnEdges)
  {
    boolean includeEdge = !excludeOnEdges;
    int len = xx.length;
    if (len < 2)
      return new int[0];
    
    if (tolerance < 0)
      tolerance = 0;

    int[] maxPositions = new int[len];
    double max = xx[0];
    double min = xx[0];
    int maxPos = 0;
    int lastMaxPos = -1;
    boolean leftValleyFound = includeEdge;
    int maxCount = 0;
    for (int jj = 1; jj < len; jj++)
    {
      double val = xx[jj];
      if (val > min + tolerance)
        leftValleyFound = true;

      if (val > max && leftValleyFound)
      {
        max = val;
        maxPos = jj;
      }
      
      if (leftValleyFound)
        lastMaxPos = maxPos;

      if (val < max - tolerance && leftValleyFound)
      {
        maxPositions[maxCount] = maxPos;
        maxCount++;
        leftValleyFound = false;
        min = val;
        max = val;
      }
      
      if (val < min)
      {
        min = val;
        if (!leftValleyFound)
          max = val;
      }
    }
    if (includeEdge)
    {
      if (maxCount > 0 && maxPositions[maxCount - 1] != lastMaxPos)
        maxPositions[maxCount++] = lastMaxPos;

      if (maxCount == 0 && max - min >= tolerance)
        maxPositions[maxCount++] = lastMaxPos;
    }
    
    int[] cropped = new int[maxCount];
    System.arraycopy(maxPositions, 0, cropped, 0, maxCount);
    maxPositions = cropped;
    double[] maxValues = new double[maxCount];
    for (int jj = 0; jj < maxCount; jj++)
    {
      int pos = maxPositions[jj];
      double midPos = pos;
      while (pos < len - 1 && xx[pos] == xx[pos + 1])
      {
        midPos += 0.5;
        pos++;
      }
      maxPositions[jj] = (int) midPos;
      maxValues[jj] = xx[maxPositions[jj]];
    }
//        int[] rankPositions = Tools.rank(maxValues);
    int[] rankPositions = rank(maxValues);
    int[] returnArr = new int[maxCount];
    for (int jj = 0; jj < maxCount; jj++)
    {
      int pos = maxPositions[rankPositions[jj]];
      returnArr[maxCount - jj - 1] = pos;//use descending order
    }
    return returnArr;
  }
    
  /**
   * Returns a sorted list of indices of the specified double array. Modified
   * from: http://stackoverflow.com/questions/951848 by N.Vischer.
   */
  public static int[] rank(double[] values)
  {
    int n = values.length;
    final Integer[] indexes = new Integer[n];
    final Double[] data = new Double[n];
    for (int i = 0; i < n; i++)
    {
      indexes[i] = new Integer(i);
      data[i] = new Double(values[i]);
    }
    
    Arrays.sort(indexes, new Comparator<Integer>()
    {
      public int compare(final Integer o1, final Integer o2)
      {
        return data[o1].compareTo(data[o2]);
      }
    });
    
    int[] indexes2 = new int[n];
    for (int i = 0; i < n; i++)
    {
      indexes2[i] = indexes[i].intValue();
    }
    
    return indexes2;
  }

/**
 * 
 * @author Lim, Lay Ngor
 */  
  public static void learnBrokenPinBorder(ReconstructedImages reconstructedImages, SliceNameEnum sliceNameEnum,
    List<JointInspectionData> jointInspectionDataObjects, Algorithm algorithm)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionDataObjects != null);

    try
    {
      Map<RegionOfInterest, Integer> detectedBlackObjects = detectObjectsAtBackground(reconstructedImages,
        sliceNameEnum, jointInspectionDataObjects, algorithm, true);

      if(detectedBlackObjects != null && detectedBlackObjects.size() > 0)
      {
        List<java.awt.Rectangle> rectList = new ArrayList<java.awt.Rectangle>();        
        for(RegionOfInterest ele : detectedBlackObjects.keySet())
        {
          java.awt.Rectangle rect = new java.awt.Rectangle(ele.getMinX(), ele.getMinY(), ele.getWidth(), ele.getHeight());
          rectList.add(rect);
        }

        if (rectList.size() > 0)
        {
          //just loop so that all pad have this info, suppose it was link to slice only
          //This means each jointInspectionData in jointInspectionDataObjects having same rectList.
          //Having same learn rect for all jointInspectionData.
          for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
          {
            learnBrokenPinBorderObject(jointInspectionData, sliceNameEnum, rectList);
          }
        }
      }
    }
    catch (XrayTesterException ex)
    {
      System.out.println("failed");
    }
  }

  /**
   *
   */
  public static void learnBrokenPinBorderObject(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum, List<java.awt.Rectangle> rectList) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(rectList != null);

    //Preparing data to be save while learn
    int[] learnBrokenPinBorderObject = new int[rectList.size() * 4];
    int i = 0;
    for (java.awt.Rectangle rect : rectList)
    {
      learnBrokenPinBorderObject[i++] = (int) rect.getMinX();
      learnBrokenPinBorderObject[i++] = (int) rect.getMinY();
      learnBrokenPinBorderObject[i++] = (int) rect.getWidth();
      learnBrokenPinBorderObject[i++] = (int) rect.getHeight();
    }

    // Get the PadSettings for this joint's pad.
    Pad pad = jointInspectionData.getPad();
    PadSettings padSettings = pad.getPadSettings();

    // Do we already have some saved learned data for this joint and slice?
    BrokenPinLearning brokenPinLearning;
    if (padSettings.hasBrokenPinLearning(sliceNameEnum))
      brokenPinLearning = padSettings.getBrokenPinLearning(sliceNameEnum);
    else
      brokenPinLearning = new BrokenPinLearning();

    brokenPinLearning.setLearnBorderObjectRect(learnBrokenPinBorderObject);
    brokenPinLearning.setPad(pad);
    brokenPinLearning.setSliceNameEnum(sliceNameEnum);
    padSettings.setBrokenPinLearning(brokenPinLearning);
  }

  public static List<java.awt.Rectangle> getLearnBrokenPinBorderObject(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    Pad pad = jointInspectionData.getPad();
    PadSettings padSettings = pad.getPadSettings();

    // Now, make sure we have data.
    BrokenPinLearning brokenPinLearning;
    if (padSettings.hasBrokenPinLearning(sliceNameEnum))
      brokenPinLearning = padSettings.getBrokenPinLearning(sliceNameEnum);
    else
    {
      // No learn data! Issue warning.
      raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);
//      learnedDataAvailable.setValue(false);
      return null;
    }

    List<java.awt.Rectangle> rectList = new ArrayList<java.awt.Rectangle>();
    final int rectInfoList[] = brokenPinLearning.getLearnBorderObjectRect();
    if (rectInfoList.length % 4 == 0)
    {
      //push learn data one by one to rect
      for (int count = 0; count < rectInfoList.length;)
      {
        java.awt.Rectangle rect = new java.awt.Rectangle();
        if (count % 4 == 0)
          rect.x = rectInfoList[count++];
        if (count % 4 == 1)
          rect.y = rectInfoList[count++];
        if (count % 4 == 2)
          rect.width = rectInfoList[count++];
        if (count % 4 == 3)
        {
          rect.height = rectInfoList[count++];
          rectList.add(rect);
        }
      }
    }
    else //else the means data is corrupted cause we learn rect information everytime.
    {
      // Issue warning.
      raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);
//      learnedDataAvailable.setValue(false);
      return null;
    }

    return rectList;
  }

  /**
   * Copy and modify from Circular Short!!! Measures a background "ring profile" for the
   * specified JointInspectionData.
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - BrokenPin
   */
  private static Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> measureRingRegionBackgroundGrayLevelProfile(
    Image image,
    ReconstructionRegion inspectionRegion,
    SliceNameEnum sliceNameEnum,
    JointInspectionData jointInspectionData,
    RegionOfInterest referenceRoi)throws JointCannotBeInspectedBusinessException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the IPD.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels(MagnificationEnum.getCurrentNorminal());

//    // Get the reference ROI. - Lim, Lay Ngor add sliceNameEnum for camera 0 locator
//    RegionOfInterest referenceRoi = getReferenceRoi(jointInspectionData, sliceNameEnum);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float) subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float) subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;

    Assert.expect(regionOuterEdgeAsFractionOfInterPadDistance > regionInnerEdgeAsFractionOfInterPadDistance);

    int profileInnerEdgeOffset = (int) Math.ceil(interPadDistanceInPixels * regionInnerEdgeAsFractionOfInterPadDistance);
    int profileOuterEdgeOffset = (int) Math.ceil(interPadDistanceInPixels * regionOuterEdgeAsFractionOfInterPadDistance);
    int locatedJointMinX = referenceRoi.getMinX();
    int locatedJointMinY = referenceRoi.getMinY();
    int locatedJointWidth = referenceRoi.getWidth();
    int locatedJointHeight = referenceRoi.getHeight();
    int locatedJointOrientation = referenceRoi.getOrientationInDegrees();

    // Make sure that there's at least 2 pixels between the inner and outer ROIs.
    if (profileOuterEdgeOffset == profileInnerEdgeOffset
      || (profileOuterEdgeOffset - profileInnerEdgeOffset) < 4)
    {
      profileOuterEdgeOffset += 2;
      profileInnerEdgeOffset -= 2;
      if (profileInnerEdgeOffset < 0)
      {
        profileOuterEdgeOffset += 2;
        profileInnerEdgeOffset = 0;
      }
    }

    // Create the outer ring ROI.
    RegionOfInterest outerRoi = new RegionOfInterest(
      locatedJointMinX - profileOuterEdgeOffset,
      locatedJointMinY - profileOuterEdgeOffset,
      locatedJointWidth + (profileOuterEdgeOffset * 2),
      locatedJointHeight + (profileOuterEdgeOffset * 2),
      locatedJointOrientation,
      RegionShapeEnum.OBROUND);

    if (AlgorithmUtil.regionCanFitInImage(outerRoi, image) == false)
      throw new JointCannotBeInspectedBusinessException("");

    // Make sure the outer ring fits within the image.
    if (AlgorithmUtil.checkRegionBoundaries(outerRoi, image, inspectionRegion, "Short Background Outer Ring ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, outerRoi);
    }

    // Create the inner ring ROI.
    RegionOfInterest innerRoi = new RegionOfInterest(
      locatedJointMinX - profileInnerEdgeOffset,
      locatedJointMinY - profileInnerEdgeOffset,
      locatedJointWidth + (profileInnerEdgeOffset * 2),
      locatedJointHeight + (profileInnerEdgeOffset * 2),
      locatedJointOrientation,
      RegionShapeEnum.OBROUND);
    // Make sure the inner ring fits within the image.
    if (AlgorithmUtil.checkRegionBoundaries(innerRoi, image, inspectionRegion, "Short Background Inner Ring ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, innerRoi);

      // Make sure the inner ring is as least one pixel smaller than the outer.
      innerRoi.setWidthKeepingSameCenter(Math.min(innerRoi.getWidth(), (outerRoi.getWidth() - 1)));
      innerRoi.setHeightKeepingSameCenter(Math.min(innerRoi.getHeight(), (outerRoi.getHeight() - 1)));
    }

    // Measure the ring region gray level profile.
    float[] ringProfile = ImageFeatureExtraction.ringProfile(image, outerRoi, innerRoi);

    Pair<RegionOfInterest, RegionOfInterest> ringRegionPair
      = new Pair<RegionOfInterest, RegionOfInterest>(outerRoi, innerRoi);
    Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair
      = new Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]>(ringRegionPair, ringProfile);

    return ringRegionAndProfilePair;
  }

  /**
   * copy from circularShortAlgo!!!
   *
   * @author Matt Wharton
   * @author Lim, Lay Ngor - add sliceNameEnum so that able to locate camera 0 slice
   */
  protected static RegionOfInterest getReferenceRoi(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);

    RegionOfInterest referenceRoi = null;
    Subtype subtype = jointInspectionData.getSubtype();
    JointTypeEnum jointTypeEnum = jointInspectionData.getJointTypeEnum();
    String referencePositionSetting
      = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_REGION_REFERENCE_POSITION);
    if (referencePositionSetting.equals("Located Position"))
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        referenceRoi = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData, sliceNameEnum, false);
      }
      else
        referenceRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData, sliceNameEnum, false);
    }
    else if (referencePositionSetting.equals("CAD Position"))
    {
      if (ImageAnalysis.isSpecialTwoPinComponent(jointTypeEnum))
      {
        ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
        referenceRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest(false);
      }
      else
        referenceRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels(false);
    }
    else
      Assert.expect(false, "Unexpected reference position setting: " + referencePositionSetting);

    Assert.expect(referenceRoi != null);
    return referenceRoi;
  }

  private static int fillImageByPixelBaseOnOriginalImage(Image source, Image dest, float fillValue)
  {
    Assert.expect(source != null);
    Assert.expect(dest != null);
    
    int counter = 0;
    for (int x = 0; x < (int) source.getWidth(); ++x)
    {
      for (int y = 0; y < (int) source.getHeight(); ++y)
      {
        //fill the pixel-shape into image
        if (source.getPixelValue(x, y) < 128)
        {
          dest.setPixelValue(x, y, fillValue);
          ++counter;
        }
      }
    }

    return counter;
  }

  /**
   * Fill the pixel to fillValue if the image pixel value is less than thresholdToFill(non-inclusive).
   * Return total dark pixels detected.
   * @author Lim, Lay Ngor
   */
  private static void fillObjectToImageByPixel(RegionOfInterest roi, Image dest, float thresholdToFill, float fillValue)
  {
    Assert.expect(roi != null);
    Assert.expect(dest != null);
    
    for (int x = (int) roi.getMinX(); x < (int) roi.getMinX() + roi.getWidth(); ++x)
    {
      for (int y = (int) roi.getMinY(); y < (int) roi.getMinY() + roi.getHeight(); ++y)
      {
        ImageCoordinate pixelLocation = new ImageCoordinate(x, y);
        if (roi.containsPixel(pixelLocation)//if its inside the ellipse
          && dest.getPixelValue(x, y) < thresholdToFill)//and if it is darker than thresholdToFill
        {
          dest.setPixelValue(x, y, fillValue);
        }
      }
    }
  }
  
    /**
   * Fill the pixel to fillValue if the image pixel value is less than thresholdToFill(non-inclusive).
   * Return total dark pixels detected.
   * @author Lim, Lay Ngor
   */
  private static void fillImageByPixelExcludeObject(RegionOfInterest roi, Image dest, float thresholdToFill, float fillValue)
  {
    Assert.expect(roi != null);
    Assert.expect(dest != null);
    
    for (int x = 0; x < dest.getWidth(); ++x)
    {
      for (int y = 0; y < dest.getHeight(); ++y)
      {    
        ImageCoordinate pixelLocation = new ImageCoordinate(x, y);
        if (!roi.containsPixel(pixelLocation)//if its not inside the ellipse
          && dest.getPixelValue(x, y) < thresholdToFill)//and if it is darker than thresholdToFill
        {
          dest.setPixelValue(x, y, fillValue);
        }
      }
    }
  }
  
  /**
   * Fill the pixel to fillValue if the image pixel value is less than thresholdToFill(non-inclusive).
   * Exclude to fill the pass in ROI list.
   * Return total dark pixels detected.
   * @author Lim, Lay Ngor - slow... double loop
   */  
  private static void fillObjectToImageByPixelExcludeEmptyPTH(RegionOfInterest roi, Image dest, float thresholdToFill,
    float fillValue, List<RegionOfInterest> excludeROIList)
  {
    Assert.expect(roi != null);
    Assert.expect(dest != null);
    Assert.expect(excludeROIList != null);
    
    for (int x = (int) roi.getMinX(); x < (int) roi.getMinX() + roi.getWidth(); ++x)
    {
      for (int y = (int) roi.getMinY(); y < (int) roi.getMinY() + roi.getHeight(); ++y)
      {
        ImageCoordinate pixelLocation = new ImageCoordinate(x, y);
        if (roi.containsPixel(pixelLocation)//if its inside the ellipse
          && dest.getPixelValue(x, y) < thresholdToFill)//and if it is darker than thresholdToFill
        {
          boolean isInExcludeRoi = false;
          for(RegionOfInterest excludeROI : excludeROIList)
          {
            if(excludeROI.containsPixel(pixelLocation))//and it is not at empty PTH ROI
              isInExcludeRoi = true;
          }

          if(!isInExcludeRoi)
            dest.setPixelValue(x, y, fillValue);
        }
      }
    }
  }  

  /**
   * Fill the pixel to fillValue if the image pixel value is less than thresholdToFill(non-inclusive)
   * and its is surrounding the roi base on toleranceFromRoi parameter.
   * Return total dark pixels detected.
   * @author Lim, Lay Ngor
   */
  private static int fillROIFrameToImageByPixel(RegionOfInterest roi, Image dest, float thresholdToFill,
    float fillValue, int roiToleranceToCenter, int roiToleranceToOutside, ReconstructionRegion inspectionRegion)
  {
    Assert.expect(roi != null);
    Assert.expect(dest != null);
    Assert.expect(inspectionRegion != null);
    
    RegionOfInterest maskToCenterRoi = new RegionOfInterest(roi);
    maskToCenterRoi.setRect(roi.getMinX() + roiToleranceToCenter, roi.getMinY() + roiToleranceToCenter,
      roi.getWidth() - (roiToleranceToCenter*2), roi.getHeight() - (roiToleranceToCenter*2)); 
    RegionOfInterest maskToOutsideRoi = new RegionOfInterest(roi);
    maskToOutsideRoi.setRect(roi.getMinX() - roiToleranceToOutside, roi.getMinY() - roiToleranceToOutside,
      roi.getWidth() + (roiToleranceToOutside*2), roi.getHeight() + (roiToleranceToOutside*2));    
    
    // Make sure the outer ring fits within the image.
    if (AlgorithmUtil.checkRegionBoundaries(maskToOutsideRoi, dest, inspectionRegion, "Short Background Outer Ring ROI") == false)
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(dest, maskToOutsideRoi);
    
    int totalDarkPixels = 0;
    for (int x = (int) maskToOutsideRoi.getMinX(); x < (int) maskToOutsideRoi.getMinX() + maskToOutsideRoi.getWidth(); ++x)
    {
      for (int y = (int) maskToOutsideRoi.getMinY(); y < (int) maskToOutsideRoi.getMinY() + maskToOutsideRoi.getHeight(); ++y)
      {
        ImageCoordinate pixelLocation = new ImageCoordinate(x, y);
        if (!maskToCenterRoi.containsPixel(pixelLocation) && //if its outside the inner Ring ellipse
          maskToOutsideRoi.containsPixel(pixelLocation)//if its inside the outer Ring ellipse
          && dest.getPixelValue(x, y) < thresholdToFill)//and if it is darker than thresholdToFill
        {
          ++totalDarkPixels;
          dest.setPixelValue(x, y, fillValue);
        }
      }
    }
    return totalDarkPixels;
  }
  
//  /**
//   * Please develop the 8 bit blob detection code.
//   * Now we convert the 8 bit image to RGB image in order to use the current blobFinder.
//   * @param image
//   * @return 
//   * @author Lim, Lay Ngor - XCR2100 - BrokenPin
//   */
//  private static java.util.List<java.awt.Shape> blobDetection(Image image)
//  {
//    Assert.expect(image != null);
//    
//    java.awt.image.BufferedImage bufferImage = image.getBufferedImage();
//    ImagePlus imagePlus = new ImagePlus("", bufferImage);
//
//    //convert to RGB
//    ImageConverter ic = new ImageConverter(imagePlus);
//    ic.convertToRGB();
//    ByteArrayOutputStream byar = new ByteArrayOutputStream();
//    try
//    {
//      ImageIO.write(imagePlus.getBufferedImage(), "jpg", byar);
//      byar.flush();
//    }
//    catch (IOException ex)
//    {
//    }
//
//    byte[] b = byar.toByteArray();
//    InputStream in = new ByteArrayInputStream(b);
//    java.awt.image.BufferedImage bImageFromConvert = null;
//    try
//    {
//      bImageFromConvert = ImageIO.read(in);
//    }
//    catch (IOException ex)
//    {
//    }
//
//    java.util.List<java.awt.Shape> sourceShapeList = new LinkedList<java.awt.Shape>();
//    //Lim, Lay Ngor - temporary created function for BrokenPin use.
//    sourceShapeList = BlobAnalyzer.getInstance().analyze(bImageFromConvert, true);
////    System.out.println("sourceShapeList" + sourceShapeList.size());
//    
//    imagePlus.flush();
//    return sourceShapeList;
//  }

  /**
   *
   * @author Lim, Lay Ngor - temp for debug use
   */
  private static void saveImage(Image image, String filePath)
  {
    try
    {
      ImageIoUtil.saveImage(image, filePath);
    }
    catch (CouldNotCreateFileException ex)
    {
      System.out.println("Failed to save");
    }
  }

  /**
   * Copy from shared short!! Raises an algorithm warning that no learning is
   * available for the given joint and slice. Keeps track of whether or not the
   * warning has already been issued and only issues the warning once per
   * joint/slice combination.
   *
   * @author Matt Wharton
   */
  protected static void raiseNoLearningAvailableWarningIfNeeded(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    Pair<JointInspectionData, SliceNameEnum> jointAndSlicePair
      = new Pair<JointInspectionData, SliceNameEnum>(jointInspectionData, sliceNameEnum);
    if (_jointsAndSlicesWarnedOn.contains(jointAndSlicePair) == false)
    {
      LocalizedString warningText = new LocalizedString("ALGDIAG_SHORT_NO_LEARNING_AVAILABLE_WARNING_KEY",
        new Object[]
        {
          jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName()
        });
      AlgorithmUtil.raiseAlgorithmWarning(warningText);

      _jointsAndSlicesWarnedOn.add(jointAndSlicePair);
    }
  }
}
