package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.awt.geom.*;
import java.io.*;
import java.util.*;
import java.awt.image.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.business.imageAnalysis.exposedPad.ExposedPadMeasurementAlgorithm;


/**
 * This algorithm is an adaptation of the 5dx 8.5+ FET Voiding algorithm by Patrick Lacz.
 *
 * The algorithm is not so straight forward:
 * <ol>
 * <li>Create a background for the image by
 * <ul>
 * <li>Creating a "single Value background" with an average value for each pixel
 * <br>- or - <br>
 * <li>creating an "interpolated background" pixel by pixel:
 * <ul>
 * <li>Create an Across profile with a value for each row of pixels.
 *
 * <li>For each row, create a row-width ROI above and below that row and determine the lightest (background)
 * and darkest (foreground) pixels in the ROI. The Across profile value for that row is heuristically determined
 * from the two values.
 *
 * <li>Create an Along profile with a value for each column of pixels.
 *
 * <li>For each column, create a column-heigth ROI before ands after that column and determine the lightest
 * (background) and darkest (foreground) pixels in the ROI. The Along profile value for that column is heuristically
 * determined from the two values.
 *
 * <li>Create the 2-dimensional background by heuristically combining the corresponding Across and Along values
 * for each background pixel.
 * </ul>
 * </ul>
 *
 * <li>Create a solder thickness image using the Inspection image for the joint being inspected and the
 * background. Any pixels below the Solder Threshold are replaced by a background gray level. Convert this image to
 * gray levels.
 *
 * <li>Retrieve the learned Expected image representing the solder area above the Solder Threshold for a theoretical
 * "prefect joint".  Any pixels below the threshold are replaced by a value of 0 (black).  Convert this image to
 * gray levels.
 *
 * <li>Compare the Inspected and Expected images pixel by pixel.  Any pixels with a black value in the Expected
 * image are not potential void areas (no solder expected there).  Inspected image pixels lighter than a
 * corresponding non-black Expected pixel by more than the Maximum Gray Level Difference are considered potential
 * void pixels.
 *
 * <li>Collect potential void pixels into contiguous areas. If a contiguous void area has more than Minimum Void Area
 * pixels, that area is considered a void.
 *
 * <li>Determine the percent of voiding by dividing the number of pixels in all void areas by the number of non-black
 * pixels in the Expected image (not the number of pixels in the entire joint).
 * </ol>
 *
 * The creation of Expected images follows the same method.  The solder thickness image of the joint being
 * inspected is saved as the Expected image.  Multiple images for the same joint on different boards can be
 * combined to produce Expected images with few or no voids. This is done by selecting pixel by pixel the darkest
 * (most solder) pixel from each of the images. In theory, multiple joints will not have voids in the same places.
 *
 * <p>This class is designed to be inheirited from by the different algorithm families. By default, it will use the
 * same settings on all the inspectable slices for whatever joint type it is applied to.  If a family wants to
 * provide per-slice settings, it should inherit and overwrite the appropriate methods.
 *
 * @author George Booth
 */
public class ExpectedImageVoidingAlgorithm extends Algorithm
{
  private final boolean _backgroundDebug = false;

  protected final int _ALGORITHM_VERSION = 1;

  private final int _smoothingKernelLength = 6;

  private final float MILS_PER_MILLIMETER = 39.3700787F;

  // value of pixels highlighted in Interactive Learning
  private final float HIGHLIGHT_PIXEL_VALUE = 200.0f;

  // this value is to set the background to 1 if that InterpolatedBackground is not calculated due to search are out of range
  protected final int _DEFAULT_BACKGROUND = 1;

  // flags for creating a thickness image.
  // FOR_INSPECTION will use a backgropund value for pixels below the Solder Threshold.
  // FOR_DIAGNOSTICS will use a transparent (0.0f) value for pixels below the Solder Threshold.
  private final boolean FOR_INSPECTION = false;
  private final boolean FOR_DIAGNOSTICS = true;

  // for interactive learning
  private final InspectionEventObservable _inspectionEventObservable = InspectionEventObservable.getInstance();

  private BooleanLock _waitingForInteractiveLearningUserInput = new BooleanLock(false);
  private boolean _interactiveLearning = false;
  private boolean _acceptCurrentImage = false;

  // Keeps track of which joints we've issued "no learning available" warnings for.
  protected static Collection<Pair<JointInspectionData, SliceNameEnum>> _jointsAndSlicesWarnedOn =
      Collections.synchronizedList(new LinkedList<Pair<JointInspectionData, SliceNameEnum>>());

  // Developer Debug flag
  private boolean _devDebug  = Config.getInstance().getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE);

  /**
   * @author George Booth
   */
  public ExpectedImageVoidingAlgorithm(InspectionFamily exposedPadInspectionFamily, InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(AlgorithmEnum.VOIDING, inspectionFamilyEnum);

    createAlgorithmSettings(exposedPadInspectionFamily);
    addMeasurementEnums();
  }

  /**
   * Override this method if you have changed the algorithm settings for this class.
   * @author George Booth
   */
  protected void createAlgorithmSettings(InspectionFamily exposedPadInspectionFamily)
  {
    int displayOrder = 0;

    // Noise Reduction
    // How much to try to compensate for noise.
    // 0 = no noise reduction
    // 1 = apply blur
    // 2+ = apply blur & NR-1 cycles of opening (dilate/erode cycles) to the void pixels image.
    // The opening will remove smaller features from the image and generally make the regions appear
    // more blocky than without NR.
    //  I expect that this value will normally be 0 to 2.
    addAlgorithmSetting(new AlgorithmSetting(
        AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION,
        displayOrder++,
        2, // default value
        0, // minimum value
        5, // maximum value
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // description URL key
        "HTML_DETAILED_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // desailed description URL key
        "IMG_DESC_GRIDARRAY_VOIDING_(NOISE_REDUCTION)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        _ALGORITHM_VERSION));
  }

  /**
   * Override this method if you have changed the algorithm settings for this class (such as providing per-slice settings).
   * @author George Booth
   */
  protected int getNoiseReductionSettingForSlice(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    return ((Number)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_VOIDING_NOISE_REDUCTION)).intValue();
  }

  /**
   * Override this method if you have changed the measurement enums for this class (such as providing per-slice settings).
   * @author George Booth
   */
  protected void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.LOCATOR_X_LOCATION);
    _jointMeasurementEnums.add(MeasurementEnum.LOCATOR_Y_LOCATION);
  }

  /**
   * Override this method to change which slices this algorithm runs on (and in which order). By default, it runs on
   * all the slices in the reconstruction region.
   *
   * @author George Booth
   */
  protected List<SliceNameEnum> selectInspectedSlices(Subtype subtype, ReconstructedImages reconstructedImages)
  {
    Assert.expect(subtype != null);
    Assert.expect(reconstructedImages != null);

    List<SliceNameEnum> sliceNamesToInspect = new LinkedList<SliceNameEnum>();
    for (ReconstructedSlice reconstructedSlice : reconstructedImages.getInspectedReconstructedSlices())
      sliceNamesToInspect.add(reconstructedSlice.getSliceNameEnum());

    return sliceNamesToInspect;
  }

  /**
   * Deletes all of the learned expected images for the specified subtype.
   *
   * @author George Booth
   */
  public void deleteLearnedExpectedImageData(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    // Delete the short profile learning for all pads in the subtype.
    Collection<Pad> subtypePads = subtype.getPads();
    for (Pad pad : subtypePads)
    {
      deleteLearnedExpectedImageData(pad);
    }
  }

  /**
   * @author George Booth
   */
  public void deleteLearnedExpectedImageData(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    PadSettings padSettings = pad.getPadSettings();
    if (padSettings.hasExpectedImageLearning())
      padSettings.deleteExpectedImageLearning();
  }


  /**
   * Learns the expected images for the specified subtype.
   *
   * @author George Booth
   */
  public boolean learnExpectedImageData(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    _acceptCurrentImage = true;  // for non-interactive learning
    boolean imageLearned = false;
    VoidingTechniqueEnum voidingTechniqueEnum = getVoidingTechnique(subtype);
    final float SOLDER_THICKNESS_THRESHOLD =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS);

    ManagedOfflineImageSet imageSetToUse = typicalBoardImages;
    if (imageSetToUse.getReconstructionRegions().isEmpty())
      imageSetToUse = unloadedBoardImages;

    // Exposed Pad only runs on the pad slice.
    SliceNameEnum sliceNameEnum = getInspectionFamily().getDefaultPadSliceNameEnum(subtype);

    ManagedOfflineImageSetIterator imagesIterator = imageSetToUse.iterator();
    ReconstructedImages reconstructedImages;

    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ImageSetData currentImageSetData = imagesIterator.getCurrentImageSetData();
      ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

      // Get all the joints in the region which are of the applicable subtype.
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataObjects = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      // Run Locator.
      Locator.locateJoints(reconstructedImages, sliceNameEnum, jointInspectionDataObjects, this, true);

      for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
      {
        String subtypeName = jointInspectionData.getSubtype().getShortName();
        String jointName = jointInspectionData.getFullyQualifiedPadName();
        jointName = currentImageSetData.getUserDescription() + "_" + jointName.replace(' ', '_');

        RegionOfInterest locatedJointRegionInSliceImage = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

        Image rawInspectedImage = reconstructedSlice.getOrthogonalImage();

        // Enhance the contrast if needed
        if (ExposedPadMeasurementAlgorithm.isContrastEnhancementEnabled(subtype))
        {
          AlgorithmUtil.enhanceContrastByEqualizingHistogram(rawInspectedImage);
        }

        RegionOfInterest rawInspectedImageRoi = locatedJointRegionInSliceImage;

/* This was done on the 5DX.  However, the Voiding Minimum Void Area and Maximum Gray Level Difference settings
   have the effect of reducing noise, so this step is not needed. If this step is needed in the future, note that
   convolveLowpass() creates a new image at 0,0.  It needs to be shifted back to the correct ROI.

        if (noiseReduction >= 1)
        {
          // blur the image
          Image blurredJointImage = Filter.convolveLowpass(rawInspectedImage, rawInspectedImageRoi);
          rawInspectedImage.decrementReferenceCount();
          rawInspectedImage = blurredJointImage;
          rawInspectedImageRoi = RegionOfInterest.createRegionFromImage(rawInspectedImage);
        }
*/
        // rotate a working image so it is normalized to 0 degrees
        // "pointer math" is used on the image arrays and requires Y to be the vertical dimension
        int rawInspectionImageRotation = rawInspectedImageRoi.getOrientationInDegrees();
        Pair<Image, RegionOfInterest> rotatedImagePair = createNormalizedImage(rawInspectedImage,
                                                                               rawInspectedImageRoi,
                                                                               rawInspectionImageRotation);
        Image inspectedImage = rotatedImagePair.getFirst();
        RegionOfInterest inspectedImageRoi = rotatedImagePair.getSecond();

        // create a background image from the inspection image
        float[] backgroundImagePixelArray =
           createBackgroundImage(jointInspectionData,
                                 sliceNameEnum,
                                 inspectedImage,
                                 inspectedImageRoi,
                                 jointName);

        if (_backgroundDebug)
          displayDebugImageArray(backgroundImagePixelArray, inspectedImageRoi, "Background");

        // create an inspected image pixel array from the inspection image
        float[] inspectedImagePixelArray = Image.createArrayFromImage(inspectedImage, inspectedImageRoi);

        // Get the PadSettings for this joint's pad.
        Pad pad = jointInspectionData.getPad();
        PadSettings padSettings = pad.getPadSettings();

        int imageWidth = inspectedImageRoi.getLengthAlong();
        int imageHeight = inspectedImageRoi.getLengthAcross();

        // Do we already have some saved learned data for this joint and slice?
        ExpectedImageLearning expectedImageLearning;
        if (padSettings.hasExpectedImageLearning(sliceNameEnum))
          expectedImageLearning = padSettings.getExpectedImageLearning(sliceNameEnum);
        else
          expectedImageLearning = new ExpectedImageLearning(imageWidth, imageHeight);

/** @todo GLB check for matching previous values */
        expectedImageLearning.setVoidingTechniqueEnum(voidingTechniqueEnum);
        expectedImageLearning.setSolderThicknessThreshold(SOLDER_THICKNESS_THRESHOLD);

        // create the expected thickness pixel array from the inspected image
        float[] expectedThicknessPixelArray =
                     createExpectedThicknessPixelArray(inspectedImagePixelArray,
                                                       backgroundImagePixelArray,
                                                       inspectedImageRoi,
                                                       subtype);

        // show the image we are processing
//        displayDebugThicknessPixelArray(expectedThicknessPixelArray, backgroundImagePixelArray, inspectedImageRoi, jointName + " current image");
        SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
        if (_devDebug)
        {
          // save what we learned as an image in a viewable png file (c:/temp/!<imageSetComment>_<jointName>_learnedImage.png)
          saveThicknessPixelArrayAsPngFile(expectedThicknessPixelArray, backgroundImagePixelArray,
                                           inspectedImageRoi, jointName + "_learnedImage", thicknessTable);
        }

        // If there's a mismatch in learned array lengths, reset the learned data
        if (expectedImageLearning.hasCompositeExpectedImageArray())
        {
          if (expectedThicknessPixelArray.length != expectedImageLearning.getCompositeImageArray().length)
          {
            expectedImageLearning.clearCompositeExpectedImageArray(imageWidth, imageHeight);
          }
        }

        if (_interactiveLearning)
        {
          // show the result of merging with current learned data

          // current image
          float[] imagePixelArray1 = convertThicknessArrayToImageArray(
                       expectedThicknessPixelArray, backgroundImagePixelArray, inspectedImageRoi, thicknessTable);
          Image image1 = Image.createFloatImageFromArray(imageWidth, imageHeight, imagePixelArray1);
          enhanceLearningImage(image1, 100.0f);

          // get a temporary version of the new composite expected image with new pixels flagged
          float[] updatedExpectedThicknessPixelArray = expectedImageLearning.createExpectedImageArray(
                       imageWidth, imageHeight, expectedThicknessPixelArray);
          float[] imagePixelArray2 = convertThicknessArrayToImageArray(
                       updatedExpectedThicknessPixelArray, backgroundImagePixelArray, inspectedImageRoi, thicknessTable);
          Image image2 = Image.createFloatImageFromArray(imageWidth, imageHeight, imagePixelArray2);
          enhanceLearningImage(image2, 100.0f);

          InteractiveLearningExposedPadInspectionEvent inspectionEvent =
            new InteractiveLearningExposedPadInspectionEvent(InspectionEventEnum.INTERACTIVE_LEARNING_EXPOSED_PAD_USER_INFO, this);

          List<Image> imageList = new ArrayList<Image>();
          imageList.add(image1);
          imageList.add(image2);
          inspectionEvent.setImageData(imageList);

          List<String> stringList = new ArrayList<String>();
          stringList.add(subtype.getShortName());
          String imageSetName = currentImageSetData.getImageSetName();
          String imageSetUserComment = currentImageSetData.getUserDescription();
          if (imageSetUserComment.equals("") == false)
          {
            imageSetName += " \"" + imageSetUserComment + "\"";
          }
          stringList.add(imageSetName);
          String boardName = jointInspectionData.getBoard().getName();
          stringList.add(boardName);
          String thisJointName = jointInspectionData.getPad().getComponent().getReferenceDesignator() + "-" +
                             jointInspectionData.getPad().getPadType().getName();
          stringList.add(thisJointName);
          inspectionEvent.setStringData(stringList);

          _inspectionEventObservable.sendEventInfo(inspectionEvent);

          // user will view images and accept or reject the current image
          waitForUserInput();
        }

        // Update the learned composite image with the new expected image if:
        // 1. learning was not interactive (_acceptCurrentImage is always true)
        // 2. learning was interactive and user accepted the image (_acceptCurrentImage is true)
        // 3. If interactive learning was cancelled, _acceptCurrentImage is set false.
        //    Do not add any more expected images if learning was cancelled.
        if (_acceptCurrentImage)
        {
          // Update the learned composite image with the new expected image
          expectedImageLearning.addLearnedExpectedImagePixelArray(imageWidth, imageHeight, expectedThicknessPixelArray);
          expectedImageLearning.setPad(pad);
          expectedImageLearning.setSliceNameEnum(sliceNameEnum);

          padSettings.setExpectedImageLearning(expectedImageLearning);
          imageLearned = true;
        }


        inspectedImage.decrementReferenceCount();
      }
      imagesIterator.finishedWithCurrentRegion();
    }
    return imageLearned;
  }

  /**
   * Enhance the learning image intensity so thet the pixels are bright enough
   *
   * @author George Booth
   */
  private void enhanceLearningImage(Image image, float maxIntensity)
  {
    Assert.expect(image != null);

    // find the maximum intensity
    float maxValue = 0.0f;
    for (int y = 0; y < image.getHeight(); y++)
    {
      for (int x = 0; x < image.getWidth(); x++)
      {
        float pixel = image.getPixelValue(x, y);
        if (pixel != HIGHLIGHT_PIXEL_VALUE && pixel > maxValue)
        {
          maxValue = pixel;
        }
      }
    }
    // adjust all pixels except highlighted pixels so brightest is maxIntensity
    float scaleFactor = maxIntensity / maxValue;
    for (int y = 0; y < image.getHeight(); y++)
    {
      for (int x = 0; x < image.getWidth(); x++)
      {
        float pixel = image.getPixelValue(x, y);
        if (pixel != HIGHLIGHT_PIXEL_VALUE && pixel > 0.0)
        {
          image.setPixelValue(x, y, pixel * scaleFactor);
        }
      }
    }
  }


  /**
   * @author George A. David
   */
  private void waitForUserInput()
  {
    _waitingForInteractiveLearningUserInput.setValue(true);
    try
    {
      _waitingForInteractiveLearningUserInput.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      // Do nothing ...
    }
  }

  /**
   * For interactive learning, accept the current image
   * @author George Booth
   */
  public void setPostiveUserInput()
  {
    _acceptCurrentImage = true;
    _waitingForInteractiveLearningUserInput.setValue(false);
  }

  /**
   * For interactive learning, reject the current image
   * @author George Booth
   */
  public void setNegativeUserInput()
  {
    _acceptCurrentImage = false;
    _waitingForInteractiveLearningUserInput.setValue(false);
  }

  /**
   * For interactive learning, enable interactive learning
   * @author George Booth
   */
  public void enableInteractiveLearning(boolean enabled)
  {
    _interactiveLearning = enabled;
  }

  /**
   * For interactive learning, cancel learning
   * @author George Booth
   */
  public void cancelInteractiveLearning()
  {
    _acceptCurrentImage = false;
    _interactiveLearning = false;
    _waitingForInteractiveLearningUserInput.setValue(false);
  }

  /**
   * Create a new expected thickness array.
   *
   * @author George Booth
   */
  private float[] createExpectedThicknessPixelArray(float[] inspectionImagePixelArray,
                                                    float[] backgroundImagePixelArray,
                                                    RegionOfInterest expectedImageRoi,
                                                    Subtype subtype) throws DatastoreException
  {
    Assert.expect(inspectionImagePixelArray != null);
    Assert.expect(backgroundImagePixelArray != null);
    Assert.expect(expectedImageRoi != null);
    int imageWidth = expectedImageRoi.getLengthAlong();
    int imageHeight = expectedImageRoi.getLengthAcross();
    int imagePixelArraySize = imageWidth * imageHeight;

    Assert.expect(inspectionImagePixelArray.length == imagePixelArraySize);
    Assert.expect(backgroundImagePixelArray.length == imagePixelArraySize);

    final float SOLDER_THICKNESS_THRESHOLD =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS);
    // initialize a new expected gray level pixel array
    float[] expectedThicknessPixelArray = new float[imagePixelArraySize];

    boolean print = false; int minX = 30; int maxX = 35; int minY = 10; int maxY = 15;

    int pixelIndex = 0;
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    for (int y = 0; y < imageHeight; y++)
    {
      for (int x = 0; x < imageWidth; x++)
      {
//        pixelIndex = y * imageWidth + x;
        float backgroundPixelValue = backgroundImagePixelArray[pixelIndex];
        float inspectionPixelValue = inspectionImagePixelArray[pixelIndex];
        float deltaGrayLevel = backgroundPixelValue - inspectionPixelValue;
        float newThicknessInMillimeters = 0.0f;

        if (deltaGrayLevel > 0.0f)
        {
          int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
          newThicknessInMillimeters = thicknessTable.getThicknessInMillimeters(backgroundPixelValue, deltaGrayLevel,backgroundSensitivityOffset);
        }
        if (newThicknessInMillimeters < SOLDER_THICKNESS_THRESHOLD)
        {
          newThicknessInMillimeters = 0.0f;
        }
        else
        {
          if (print && x >= minX && x <= maxX & y >= minY & y <= maxY)
            System.out.println("  x = " + x + ", y = " + y + ", background = " + backgroundPixelValue +
                               ", inspection = " + inspectionPixelValue +
                               ", deltaGray = " + deltaGrayLevel +
                               ", thickness = " + newThicknessInMillimeters +
                               " (" + newThicknessInMillimeters * MILS_PER_MILLIMETER + " mils)");
        }

        expectedThicknessPixelArray[pixelIndex] = newThicknessInMillimeters;

        pixelIndex++;
      }
    }

    return expectedThicknessPixelArray;
  }

  /**
   * Create a new thickness array highlighting background pixels that match the average background value
   *
   * @author George Booth
   */
  private float[] createHighlightedBackgroundPixelArray(float[] solderImagePixelArray,
                                                        RegionOfInterest expectedImageRoi,
                                                        float averageBackgroundValue)
  {
    Assert.expect(solderImagePixelArray != null);
    Assert.expect(expectedImageRoi != null);
    int imageWidth = expectedImageRoi.getLengthAlong();
    int imageHeight = expectedImageRoi.getLengthAcross();
    int imagePixelArraySize = imageWidth * imageHeight;
    Assert.expect(solderImagePixelArray.length == imagePixelArraySize);

    // initialize a new gray level pixel array
    float[] backgroundPixelArray = new float[imagePixelArraySize];
    float backgroundValue = Math.round(averageBackgroundValue);

    int pixelIndex = 0;
    for (int y = 0; y < imageHeight; y++)
    {
      for (int x = 0; x < imageWidth; x++)
      {
//        pixelIndex = y * imageWidth + x;
        // solder pixel values are intger values
        float solderPixelValue = solderImagePixelArray[pixelIndex];
        if (solderPixelValue == backgroundValue)
        {
          backgroundPixelArray[pixelIndex] = 255.0f;
        }
        else
        {
          backgroundPixelArray[pixelIndex] = 0.0f;
        }
        pixelIndex++;
      }
    }

    return backgroundPixelArray;
  }

  /**
   * Create a new inspected thickness array.
   *
   * @author George Booth
   */
  private float[] createInspectedThicknessPixelArray(float[] inspectionImagePixelArray,
                                                    float[] backgroundImagePixelArray,
                                                    RegionOfInterest expectedImageRoi,
                                                    boolean forDiagnostics,
                                                    Subtype subtype) throws DatastoreException
  {
    Assert.expect(inspectionImagePixelArray != null);
    Assert.expect(backgroundImagePixelArray != null);
    Assert.expect(expectedImageRoi != null);
    int imageWidth = expectedImageRoi.getLengthAlong();
    int imageHeight = expectedImageRoi.getLengthAcross();
    int imagePixelArraySize = imageWidth * imageHeight;
    Assert.expect(inspectionImagePixelArray.length == imagePixelArraySize);
    Assert.expect(backgroundImagePixelArray.length == imagePixelArraySize);

    final float SOLDER_THICKNESS_THRESHOLD =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_SOLDER_THICKNESS);

    // initialize a new expected gray level pixel array
    float[] expectedThicknessPixelArray = new float[imagePixelArraySize];

    // create a "background" thickness value to be used when the image thickness is below the solder threshold
    int middleX = imageWidth / 2;
    int middleY = imageHeight / 2;
    int middleIndex = middleY * imageWidth + middleX;
    float middleBackgroundPixelValue = backgroundImagePixelArray[middleIndex];
    float middleDeltaGrayLevel = 1.0f;
    // tweak for regression test pathological image sets
    if (middleDeltaGrayLevel > middleBackgroundPixelValue)
    {
      middleDeltaGrayLevel = middleBackgroundPixelValue;
    }

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float middleBackgroundThicknessInMillimeters =
      thicknessTable.getThicknessInMillimeters(middleBackgroundPixelValue, middleDeltaGrayLevel, backgroundSensitivityOffset);

    boolean print = false; int minX = 30; int maxX = 35; int minY = 10; int maxY = 15;

    int pixelIndex = 0;
    for (int y = 0; y < imageHeight; y++)
    {
      for (int x = 0; x < imageWidth; x++)
      {
//        pixelIndex = y * imageWidth + x;
        float backgroundPixelValue = backgroundImagePixelArray[pixelIndex];
        float inspectionPixelValue = inspectionImagePixelArray[pixelIndex];
        float deltaGrayLevel = backgroundPixelValue - inspectionPixelValue;
        float newThicknessInMillimeters = 0.0f;
        if (deltaGrayLevel > 0.0f)
        {
          newThicknessInMillimeters = thicknessTable.getThicknessInMillimeters(backgroundPixelValue, deltaGrayLevel, backgroundSensitivityOffset);
        }
        if (newThicknessInMillimeters < SOLDER_THICKNESS_THRESHOLD)
        {
          if (forDiagnostics)
          {
            // create a "transparent" pixel for the diagnostics overlay image
            newThicknessInMillimeters = 0.0f;
          }
          else
          {
            // force a value that will be a void
            newThicknessInMillimeters = middleBackgroundThicknessInMillimeters;
          }
        }
        if (print && x >= minX && x <= maxX & y >= minY & y <= maxY)
          System.out.println("  x = " + x + ", y = " + y + ", background = " + backgroundPixelValue +
                             ", inspection = " + inspectionPixelValue +
                             ", deltaGray = " + deltaGrayLevel +
                             ", thickness = " + newThicknessInMillimeters +
                             " (" + newThicknessInMillimeters * MILS_PER_MILLIMETER + " mils)");

        expectedThicknessPixelArray[pixelIndex] = newThicknessInMillimeters;

        pixelIndex++;
      }
    }

    return expectedThicknessPixelArray;
  }

  /**
   * @author George Booth
   */
  public void classifyJoints(ReconstructedImages reconstructedImages, List<JointInspectionData> jointInspectionDataObjects) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);

    if (jointInspectionDataObjects.isEmpty())
    {
      // nothing to inspect
      return;
    }

    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    for (SliceNameEnum sliceNameEnum : selectInspectedSlices(subtype, reconstructedImages))
    {
      int noiseReduction = getNoiseReductionSettingForSlice(subtype, sliceNameEnum);
      float voidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_FAIL_JOINT_PERCENT_PAD);
   //   float individualVoidAreaFailThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_INDIVIDUAL_VOIDING_FAIL_JOINT_PERCENT_PAD);
      MeasurementEnum percentMeasurementEnum = MeasurementEnum.LARGE_PAD_VOIDING_PERCENT;
    //   MeasurementEnum individualPercentMeasurementEnum = MeasurementEnum.LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT;
      measureVoidingOnSlice(reconstructedImages,
                            sliceNameEnum,
                            jointInspectionDataObjects,
                            subtype,
                            noiseReduction,
                            voidAreaFailThreshold,
                          //  individualVoidAreaFailThreshold,
                            percentMeasurementEnum,
                            MILIMETER_PER_PIXEL);
                          //  individualPercentMeasurementEnum);
    }
  }

  /**
   * @author George Booth
   */
   protected void measureVoidingOnSlice(ReconstructedImages reconstructedImages,
                                       SliceNameEnum sliceNameEnum,
                                       List<JointInspectionData> jointInspectionDataObjects,
                                       Subtype subtype,
                                       int noiseReduction,
                                       float voidAreaFailThreshold,
                                    //   float individualVoidAreaFailThreshold,
                                       MeasurementEnum percentMeasurementEnum,
                                       final float MILIMETER_PER_PIXEL) throws XrayTesterException
                                    //   MeasurementEnum IndividualVoidPercentMeasurementEnum) throws XrayTesterException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(subtype != null);
    Assert.expect(noiseReduction >= 0 && noiseReduction <= 5);

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);
    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      AlgorithmUtil.postStartOfJointDiagnostics(this, jointInspectionData, jointInspectionData.getInspectionRegion(), reconstructedSlice, false);

      String subtypeName = jointInspectionData.getSubtype().getShortName();
      String jointName = jointInspectionData.getFullyQualifiedPadName();
      jointName = jointName.replace(' ', '_');
//      System.out.print("jointName = \"" + jointName + "\", ");

      RegionOfInterest locatedJointRegionInSliceImage = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);

      Image rawInspectedImage = reconstructedSlice.getOrthogonalImage();
      rawInspectedImage.incrementReferenceCount();

      RegionOfInterest rawInspectedImageRoi = locatedJointRegionInSliceImage;

      /* This was done on the 5DX.  However, the Voiding Minimum Void Area and Maximum Gray Level Difference settings
         have the effect of reducing noise, so this step is not needed. If this step is needed in the future, note that
         convolveLowpass() creates a new image at 0,0.  It needs to be shifted back to the correct ROI.

              if (noiseReduction >= 1)
              {
                // blur the image
                Image blurredJointImage = Filter.convolveLowpass(rawInspectedImage, rawInspectedImageRoi);
                rawInspectedImage.decrementReferenceCount();
                rawInspectedImage = blurredJointImage;
                rawInspectedImageRoi = RegionOfInterest.createRegionFromImage(rawInspectedImage);
              }
      */

      // rotate a working image so it is normalized to 0 degrees
      // "pointer math" used on the image arrays expects Y to be the vertical dimension
      int rawInspectionImageRotation = rawInspectedImageRoi.getOrientationInDegrees();
      Pair<Image, RegionOfInterest> rotatedImagePair = createNormalizedImage(rawInspectedImage,
                                                                             rawInspectedImageRoi,
                                                                             rawInspectionImageRotation);
      Image inspectedImage = rotatedImagePair.getFirst();
      RegionOfInterest inspectedImageRoi = rotatedImagePair.getSecond();

      float[] solderImagePixelArray = Image.createArrayFromImage(inspectedImage, inspectedImageRoi);

      // create a background image from the inspection image
      float[] backgroundImagePixelArray =
          createBackgroundImage(jointInspectionData,
                                sliceNameEnum,
                                inspectedImage,
                                inspectedImageRoi,
                                jointName);
      if (_backgroundDebug)
        displayDebugImageArray(backgroundImagePixelArray, inspectedImageRoi, "backgroundImage");

      // Retrieve the expected image learning for this joint.
      BooleanRef learnedDataAvailable = new BooleanRef(true);
      float[] expectedImageThicknessArray = new float[solderImagePixelArray.length];
      float numberOfPixelsTested = 0.0f;
     
      Arrays.fill(expectedImageThicknessArray, 0.0f);

      ExpectedImageLearning expectedImageLearning = getExpectedImageLearning(jointInspectionData,
                                                                             sliceNameEnum,
                                                                             learnedDataAvailable);
      if (learnedDataAvailable.getValue())
      {
        expectedImageThicknessArray = expectedImageLearning.getCompositeImageArray();

        numberOfPixelsTested = expectedImageLearning.getNumberOfTestedPixels();

        // Make sure that the learned profile sizes match the measured profile.
        if (solderImagePixelArray.length != expectedImageThicknessArray.length)
        {
          raiseImageSizeMismatchWarning(jointInspectionData, sliceNameEnum);
          return;
        }
      }

      Image backgroundPixelsDiagnosticImage = null;
      Image solderAreaDiagnosticImage = null;
      Image expectedDiagnosticImage = null;
      Image voidingDiagnosticImage = null;

      // if diagnostics are enabled and not using standard backgrounds, display the background region(s)
      // and highlight the pixels that match the background value
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this) &&
          ExposedPadMeasurementAlgorithm.isStandardBackgroundEnabled(jointInspectionData.getSubtype()) == false)
      {
        // create the background pixel array from the inspected image using the average background value
        float averageBackgroundValue = getAverageBackgroundValue(jointInspectionData,
                                                                 sliceNameEnum,
                                                                 rawInspectedImage);
        float[] backgroundPixelsImagePixelArray =
          createHighlightedBackgroundPixelArray(solderImagePixelArray,
                                                inspectedImageRoi,
                                                averageBackgroundValue);

        // create a diagnostic image that is "un-normalized" to match the inspection image
        backgroundPixelsDiagnosticImage = preparePixelArrayForDiagnosticPosting(backgroundPixelsImagePixelArray,
                                                                                inspectedImageRoi,
                                                                                rawInspectionImageRotation);
        // get the background regions that are enabled
        Pair<RegionOfInterest, RegionOfInterest> backgroundRois =
            ExposedPadMeasurementAlgorithm.getAverageBackgroundRois(rawInspectedImage,
                                                                    jointInspectionData);

        RegionOfInterest backgroundProfile1Roi = backgroundRois.getFirst();
        RegionOfInterest backgroundProfile2Roi = backgroundRois.getSecond();

        // There is always 1 background ROI
        MeasurementRegionDiagnosticInfo backgroundProfile1RoiDiagInfo =
           new MeasurementRegionDiagnosticInfo(backgroundProfile1Roi,
                                               MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

        if (backgroundProfile2Roi == null)
        {
          // just post the one background region
          _diagnostics.postDiagnostics(reconstructionRegion,
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       true, // false,
                                       backgroundProfile1RoiDiagInfo,
                                       new OverlayBackgroundImageDiagnosticInfo(backgroundPixelsDiagnosticImage, locatedJointRegionInSliceImage));
        }
        else
        {
          // post both background regions
          MeasurementRegionDiagnosticInfo backgroundProfile2RoiDiagInfo =
              new MeasurementRegionDiagnosticInfo(backgroundProfile2Roi,
                                                  MeasurementRegionEnum.EXPOSED_PAD_BACKGROUND_REGION);

          _diagnostics.postDiagnostics(reconstructionRegion,
                                       sliceNameEnum,
                                       jointInspectionData,
                                       this,
                                       true, // false,
                                       backgroundProfile1RoiDiagInfo,
                                       backgroundProfile2RoiDiagInfo,
                                       new OverlayBackgroundImageDiagnosticInfo(backgroundPixelsDiagnosticImage, locatedJointRegionInSliceImage));
        }
      }

      // if diagnostics are enabled, display what the solder area of the
      // joint looks like to help tune the Solder Thickness threshold
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
      {
        // create the inspected thickness pixel array from the inspected image using the solder threshold
        float[] diagnosticThicknessPixelArray =
          createInspectedThicknessPixelArray(solderImagePixelArray,
                                             backgroundImagePixelArray,
                                             inspectedImageRoi,
                                             FOR_DIAGNOSTICS,
                                             subtype);

        // convert thickness to gray levels
        float[] solderAreaImagePixelArray = convertThicknessArrayToImageArray(diagnosticThicknessPixelArray,
                                                                              backgroundImagePixelArray,
                                                                              inspectedImageRoi,
                                                                              thicknessTable);

        // create a diagnostic image that is "un-normalized" to match the inspection image
        solderAreaDiagnosticImage = preparePixelArrayForDiagnosticPosting(solderAreaImagePixelArray,
                                                                                inspectedImageRoi,
                                                                                rawInspectionImageRotation);
        // adjust the pixel intensity
        enhanceDiagnosticImage(solderAreaDiagnosticImage, 150.0f);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     true, // false,
                                     new OverlaySolderImageDiagnosticInfo(solderAreaDiagnosticImage, locatedJointRegionInSliceImage));
      }

      // if diagnostics are enabled, display what the expected image of the joint looks like
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this) && learnedDataAvailable.getValue())
      {
        // convert thickness to gray levels
        float[] expectedImagePixelArray = convertThicknessArrayToImageArray(expectedImageThicknessArray,
                                                                          backgroundImagePixelArray,
                                                                          inspectedImageRoi,
                                                                          thicknessTable);

        // create a diagnostic image that is "un-normalized" to match the inspection image
        expectedDiagnosticImage = preparePixelArrayForDiagnosticPosting(expectedImagePixelArray,
                                                                              inspectedImageRoi,
                                                                              rawInspectionImageRotation);

        // adjust the pixel intensity
        enhanceDiagnosticImage(expectedDiagnosticImage, 170.0f);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     true, // false,
                                     new OverlayExpectedImageDiagnosticInfo(expectedDiagnosticImage, locatedJointRegionInSliceImage));
      }

      float numberOfVoidPixels = 0.0f;
   //   int numberOfIndividualVoidPixels = 0;
      float[] voidingImagePixelArray = new float[solderImagePixelArray.length];
    //  float BufferedImageNumberOfPixelsTested = 0;

        //Jack Hwee-individual void
   /*   Image solderImagePixelImage = Image.createFloatImageFromArray(inspectedImageRoi.getLengthAlong(), inspectedImageRoi.getLengthAcross(), solderImagePixelArray);
      BufferedImage  inspectedBufferedImage = solderImagePixelImage.getBufferedImage();
      BufferedImageNumberOfPixelsTested = inspectedImageRoi.getLengthAlong() * inspectedImageRoi.getLengthAcross();  */

      if (learnedDataAvailable.getValue())
      {
        // get voiding pixels
        voidingImagePixelArray = findVoidingPixels(subtype,
                                                   solderImagePixelArray,
                                                   inspectedImageRoi,
                                                   expectedImageThicknessArray,
                                                   backgroundImagePixelArray,
                                                   inspectedImageRoi.getOrientationInDegrees(),
                                                   jointName);

        // ignore clusters of voided pixels below a certain area
        final float MILLIMETERS_AREA_THRESHOLD =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MINIMUM_VOID_AREA);

        float pixelsPerMillimeter = 1.0f / MILIMETER_PER_PIXEL;
        float squarePixelsPerSquareMillimeter = pixelsPerMillimeter * pixelsPerMillimeter;
        int pixelAreaThreshold = (int)(MILLIMETERS_AREA_THRESHOLD * squarePixelsPerSquareMillimeter);

        removeVoidsSmallerThanNPixels(voidingImagePixelArray, inspectedImageRoi, pixelAreaThreshold);

        // determine the amount of voiding
        Pair<float[], Integer> updatedVoidingPixelsPair = getVoidingPixels(voidingImagePixelArray, inspectedImageRoi);
        voidingImagePixelArray = updatedVoidingPixelsPair.getFirst();
        numberOfVoidPixels = (float)updatedVoidingPixelsPair.getSecond();

         //Jack Hwee-individual void
   //    numberOfIndividualVoidPixels = Threshold.individualVoid (inspectedBufferedImage,individualVoidAreaFailThreshold, BufferedImageNumberOfPixelsTested);
      }
     
      // Store the percent voiding
      float voidPercent = 0.0f;
      float individualVoidPercent = 0.0f;
      if (learnedDataAvailable.getValue())
      {
        voidPercent = 100.f * (float)numberOfVoidPixels / numberOfPixelsTested;
    //    individualVoidPercent = 100.f * (float)numberOfIndividualVoidPixels / BufferedImageNumberOfPixelsTested;
      }
      JointInspectionResult jointResult = jointInspectionData.getJointInspectionResult();
      JointMeasurement voidPercentMeasurement = new JointMeasurement(this,
                                                                     percentMeasurementEnum,
                                                                     MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
                                                                     jointInspectionData.getPad(), sliceNameEnum, voidPercent);
      jointResult.addMeasurement(voidPercentMeasurement);

        // //Jack Hwee-individual void
    /*   JointMeasurement individualVoidPercentMeasurement = new JointMeasurement(this,
                                                                     IndividualVoidPercentMeasurementEnum,
                                                                     MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
                                                                     jointInspectionData.getPad(), sliceNameEnum, individualVoidPercent);
      jointResult.addMeasurement(individualVoidPercentMeasurement);  */

      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this) && learnedDataAvailable.getValue())
      {
        AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, jointInspectionData, voidPercentMeasurement);


        // create a diagnostic image that is "un-normalized" to match the inspection image
        voidingDiagnosticImage = preparePixelArrayForDiagnosticPosting(voidingImagePixelArray,
                                                                             inspectedImageRoi,
                                                                             rawInspectionImageRotation);

        // remove the voided pixels from the expected image for visual clarity
        subtractVoidedPixels(voidingDiagnosticImage, expectedDiagnosticImage);



        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     true, // false,
                                     new OverlayExpectedImageDiagnosticInfo(expectedDiagnosticImage, locatedJointRegionInSliceImage),
                                     new OverlayImageDiagnosticInfo(voidingDiagnosticImage, locatedJointRegionInSliceImage));

      /*     AlgorithmUtil.postMeasurementTextualDiagnostics(this, reconstructionRegion, jointInspectionData, individualVoidPercentMeasurement);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     jointInspectionData,
                                     this,
                                     true, // false,
                                     new OverlayExpectedImageDiagnosticInfo(expectedDiagnosticImage, locatedJointRegionInSliceImage),
                                     new OverlayImageDiagnosticInfo(voidingDiagnosticImage, locatedJointRegionInSliceImage));   */
      }

      if (rawInspectedImage != null)
      {
        rawInspectedImage.decrementReferenceCount();
      }
      if (inspectedImage != null)
      {
        inspectedImage.decrementReferenceCount();
      }
      if (backgroundPixelsDiagnosticImage != null)
      {
        backgroundPixelsDiagnosticImage.decrementReferenceCount();
      }
      if (solderAreaDiagnosticImage != null)
      {
        solderAreaDiagnosticImage.decrementReferenceCount();
      }
      if (expectedDiagnosticImage != null)
      {
        expectedDiagnosticImage.decrementReferenceCount();
      }
      if (voidingDiagnosticImage != null)
      {
        voidingDiagnosticImage.decrementReferenceCount();
      }
    /*  if (solderImagePixelImage != null)
      {
        solderImagePixelImage.decrementReferenceCount();
      }  */

      // Indict the joint if it exceeds the joint-voiding threshold
      boolean jointFailed = (voidPercent > voidAreaFailThreshold);
    //   boolean individualJointFailed = (individualVoidPercent > individualVoidAreaFailThreshold);
      if (jointFailed)
      {
        JointIndictment voidPercentIndictment = new JointIndictment(IndictmentEnum.VOIDING,
                                                                    this, sliceNameEnum);
        voidPercentIndictment.addFailingMeasurement(voidPercentMeasurement);
        jointResult.addIndictment(voidPercentIndictment);
      }

 /*       if (individualJointFailed)
      {
        JointIndictment individualVoidPercentIndictment = new JointIndictment(IndictmentEnum.INDIVIDUAL_VOIDING,
            this, sliceNameEnum);
        individualVoidPercentIndictment.addFailingMeasurement(individualVoidPercentMeasurement);
        jointResult.addIndictment(individualVoidPercentIndictment);
      }  */

      AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, jointInspectionData, reconstructionRegion, sliceNameEnum, !jointFailed);
   //   AlgorithmUtil.postJointPassingOrFailingRegionDiagnostic(this, jointInspectionData, reconstructionRegion, sliceNameEnum, !individualJointFailed);

      // Remove the joint/slice pair from our list of joint/slice pairs we've warned about "no learning available" on.
      Pair<JointInspectionData, SliceNameEnum> jointAndSlicePair =
      new Pair<JointInspectionData, SliceNameEnum>(jointInspectionData, sliceNameEnum);
      _jointsAndSlicesWarnedOn.remove(jointAndSlicePair);
    }
  }

  /**
   * Create an image from the pixel array and un-roate it
   *
   * @author George Booth
   */
  private Image preparePixelArrayForDiagnosticPosting(float[] diagnosticImagePixelArray,
                                                       RegionOfInterest inspectedImageRoi,
                                                       int rawInspectionImageRotation)
  {
    Image rotatedDiagnosticImage = Image.createFloatImageFromArray(inspectedImageRoi.getLengthAlong(),
                                                              inspectedImageRoi.getLengthAcross(),
                                                              diagnosticImagePixelArray);

    // unrotate a working image so it matches the inspection image
    int reverseRotation = 0;
    if (rawInspectionImageRotation == 0)
      reverseRotation = 0;
    else if (rawInspectionImageRotation == 90)
      reverseRotation = 270;
    else if (rawInspectionImageRotation == 180)
      reverseRotation = 180;
    else if (rawInspectionImageRotation == 270)
      reverseRotation = 90;
    else
      Assert.expect(false);

    Pair<Image, RegionOfInterest> voidingImagePair = createNormalizedImage(rotatedDiagnosticImage,
                                                                           inspectedImageRoi,
                                                                           reverseRotation);
    Image voidingImage = voidingImagePair.getFirst();
    rotatedDiagnosticImage.decrementReferenceCount();

    return voidingImage;
  }

  /**
   * Make voided pixels in the expected image transparent
   *
   * @author George Booth
   */
  private void subtractVoidedPixels(Image voidingDiagnosticImage, Image expectedDiagnosticImage)
  {
    Assert.expect(voidingDiagnosticImage != null);
    Assert.expect(expectedDiagnosticImage != null);

    for (int y = 0; y < voidingDiagnosticImage.getHeight(); y++)
    {
      for (int x = 0; x < voidingDiagnosticImage.getWidth(); x++)
      {
        float pixel = voidingDiagnosticImage.getPixelValue(x, y);
        if (pixel > 0.0f)
        {
          expectedDiagnosticImage.setPixelValue(x, y, 0.0f);
        }
      }
    }
  }

  /**
   * Enhance the diagnostic image intensity so thet the overlaid pixels are bright enough
   *
   * @author George Booth
   */
  private void enhanceDiagnosticImage(Image image, float maxIntensity)
  {
    Assert.expect(image != null);

    // find the maximum intensity
    float maxValue = 0.0f;
    for (int y = 0; y < image.getHeight(); y++)
    {
      for (int x = 0; x < image.getWidth(); x++)
      {
        float pixel = image.getPixelValue(x, y);
        if (pixel > maxValue)
        {
          maxValue = pixel;
        }
      }
    }
    // adjust all pixels so brightest is maxIntensity
    float scaleFactor = maxIntensity / maxValue;
    for (int y = 0; y < image.getHeight(); y++)
    {
      for (int x = 0; x < image.getWidth(); x++)
      {
        float pixel = image.getPixelValue(x, y);
        if (pixel > 0.0)
        {
          image.setPixelValue(x, y, pixel * scaleFactor);
        }
      }
    }
  }

  /**
   * Rotate the image such that it is normalized to 0 degress. This is required so that
   * "pointer math" will process the image properly.
   *
   * @author George Booth
   */
  private Pair<Image, RegionOfInterest> createNormalizedImage(Image inspectionImage,
                                                              RegionOfInterest inspectionImageRoi,
                                                              int degreeRotation)
  {
    // build the transformation
    AffineTransform rotationTransform = AffineTransform.getRotateInstance( Math.toRadians(degreeRotation));
    AffineTransform correctionTransform = new AffineTransform();

    int imageMaxX = inspectionImage.getWidth() - 1;
    int imageMaxY = inspectionImage.getHeight() - 1;

    RegionOfInterest baseRoi = RegionOfInterest.createRegionFromImage(inspectionImage);
    RegionOfInterest tempResultRoi = new RegionOfInterest(baseRoi);

    Transform.boundTransformedRegionOfInterest(baseRoi, rotationTransform, tempResultRoi, correctionTransform);

    // we need to keep the region's rotated dimensions so that we know how big of an image to create.
    int correctedImageWidth = tempResultRoi.getWidth();
    int correctedImageHeight = tempResultRoi.getHeight();

    Assert.expect(correctedImageWidth > 0);
    Assert.expect(correctedImageHeight > 0);

    RegionOfInterest resultRoi = new RegionOfInterest(0, 0, correctedImageWidth, correctedImageHeight, 0, RegionShapeEnum.RECTANGULAR);

    Image correctedImage = Transform.applyAffineTransform(inspectionImage,
                                                          correctionTransform,
                                                          resultRoi,
                                                          0.0f);

    // adjust the ROI so it is normalized to 0 degress
    RegionOfInterest correctedRoi = new RegionOfInterest(inspectionImageRoi);
    int minX = inspectionImageRoi.getMinX();
    int maxX = inspectionImageRoi.getMaxX();
    int width = inspectionImageRoi.getWidth();
    int minY = inspectionImageRoi.getMinY();
    int maxY = inspectionImageRoi.getMaxY();
    int height = inspectionImageRoi.getHeight();
    if (degreeRotation == 0)
    {
      // we're good to go
    }
    else if (degreeRotation == 90)
    {
      // rotate 90 degrees clockwise
      correctedRoi = new RegionOfInterest(imageMaxY - maxY, minX, height, width, 0, RegionShapeEnum.RECTANGULAR);
    }
    else if (degreeRotation == 180)
    {
      // rotate 180 degrees clockwise
      correctedRoi = new RegionOfInterest(imageMaxX - maxX, imageMaxY - maxY, width, height, 0, RegionShapeEnum.RECTANGULAR);
    }
    else if (degreeRotation == 270)
    {
      // rotate 270 degrees clockwise
      correctedRoi = new RegionOfInterest(minY, imageMaxX - maxX, height, width, 0, RegionShapeEnum.RECTANGULAR);
    }
    else
    {
      Assert.expect(false);
    }

    return new Pair<Image,RegionOfInterest>(correctedImage, correctedRoi);
  }

  /**
   * Retrieves the learned expected image for a joint region.  Issues a warning if
   * the learning is not available.
   *
   * @author Matt Wharton
   * @author George Booth
   */
  protected ExpectedImageLearning getExpectedImageLearning(JointInspectionData jointInspectionData,
                                                           SliceNameEnum sliceNameEnum,
                                                           BooleanRef learnedDataAvailable) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(learnedDataAvailable != null);

    learnedDataAvailable.setValue(true);
    Pad pad = jointInspectionData.getPad();
    PadSettings padSettings = pad.getPadSettings();

    // Now, make sure we have data.
    ExpectedImageLearning expectedImageLearning = null;
    if (padSettings.hasExpectedImageLearning(sliceNameEnum))
    {
      expectedImageLearning = padSettings.getExpectedImageLearning(sliceNameEnum);
      if (expectedImageLearning.hasCompositeExpectedImageArray() == false)
      {
        // Issue warning.
        raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);

        learnedDataAvailable.setValue(false);
      }
    }
    else
    {
      // Issue warning.
      raiseNoLearningAvailableWarningIfNeeded(jointInspectionData, sliceNameEnum);

      learnedDataAvailable.setValue(false);
    }

    return expectedImageLearning;
  }

  /**
   * Raises an algorithm warning that no learning is available for the given joint and slice.
   * Keeps track of whether or not the warning has already been issued and only issues the warning
   * once per joint/slice combination.
   *
   * @author Matt Wharton
   * @author George Booth
   */
  protected void raiseNoLearningAvailableWarningIfNeeded(JointInspectionData jointInspectionData,
                                                         SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    Pair<JointInspectionData, SliceNameEnum> jointAndSlicePair =
        new Pair<JointInspectionData, SliceNameEnum>(jointInspectionData, sliceNameEnum);
    if (_jointsAndSlicesWarnedOn.contains(jointAndSlicePair) == false)
    {
      LocalizedString warningText = new LocalizedString("ALGDIAG_EXPECTED_IMAGE_NO_LEARNING_AVAILABLE_WARNING_KEY",
                                                        new Object[]
                                                        {jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName()});
      AlgorithmUtil.raiseAlgorithmWarning(warningText);

      _jointsAndSlicesWarnedOn.add(jointAndSlicePair);
    }
  }

  /**
   * Raises an algorithm warning that there's a size difference between the learned and measured images.
   *
   * @author Matt Wharton
   * @author George Booth
   */
  protected void raiseImageSizeMismatchWarning(JointInspectionData jointInspectionData,
                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    LocalizedString warningText = new LocalizedString("ALGDIAG_EXPECTED_IMAGE_SIZE_MISMATCH_WARNING_KEY",
      new Object[] { jointInspectionData.getFullyQualifiedPadName(), sliceNameEnum.getName() });
    AlgorithmUtil.raiseAlgorithmWarning(warningText);
  }


//--------------------------------------------------------
// Ported from 5DX
//--------------------------------------------------------

  /**
   * Creates a background image pixel array for use with the inspection image
   *
   * @author George Booth
   */
  private float[] createBackgroundImage(JointInspectionData jointInspectionData,
                                        SliceNameEnum sliceNameEnum,
                                        Image image,
                                        RegionOfInterest padBorderRoi,
                                        String jointName) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(image != null);
    Assert.expect(padBorderRoi != null);

    if (ExposedPadMeasurementAlgorithm.isStandardBackgroundEnabled(jointInspectionData.getSubtype()))
    {
//      System.out.println("Using interpolated background for voiding");
      // create an interpolated background from the image
      return createInterpolatedBackgroundImage(jointInspectionData,
                                               sliceNameEnum,
                                               image,
                                               padBorderRoi,
                                               jointName);
    }
    else
    {
      float averageBackgroundValue = getAverageBackgroundValue(jointInspectionData,
                                                               sliceNameEnum,
                                                               image);
      // use the average background value for the entire background
      return createSingleValueBackgroundImage(padBorderRoi, averageBackgroundValue);
    }
  }

  /**
   * Gets the average background value
   *
   * @author George Booth
   */
  private float getAverageBackgroundValue(JointInspectionData jointInspectionData,
                                        SliceNameEnum sliceNameEnum,
                                        Image image)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(image != null);

    float averageBackgroundValue;
    if (ExposedPadMeasurementAlgorithm.hasAverageBackgroundValueMeasurement(jointInspectionData, sliceNameEnum))
    {
//        System.out.println("Using existing average background for voiding");
      JointMeasurement averageBackgroundValueMeasurement =
          ExposedPadMeasurementAlgorithm.getAverageBackgroundValueMeasurement(jointInspectionData, sliceNameEnum);
      averageBackgroundValue = averageBackgroundValueMeasurement.getValue();
    }
    else
    {
//        System.out.println("Creating average background for voiding");
      averageBackgroundValue =
          ExposedPadMeasurementAlgorithm.getAverageBackgroundValue(image, jointInspectionData);
    }
//    System.out.println("  averageBackgroundValue = " + averageBackgroundValue);
    return averageBackgroundValue;
  }

  /**
   * Creates a background image pixel array from a single background value as determined in
   * ExposedPadMeasurementAlgorithm
   *
   * @author George Booth
   */
  private float[] createSingleValueBackgroundImage(RegionOfInterest padBorderRoi,
                                                   float averageBackgroundValue)
  {
    Assert.expect(padBorderRoi != null);
    Assert.expect(averageBackgroundValue > 0.0f);

    // create background image pixel array
    int backgroundWidth = padBorderRoi.getWidth();
    int backgroundHeight = padBorderRoi.getHeight();
    float[] singleValueBackgroundImagePixelArray = new float[backgroundWidth * backgroundHeight];
    int pixelIndex = 0;
    for (int y = 0; y < backgroundHeight; y++)
    {
      for (int x = 0; x < backgroundWidth; x++)
      {
        singleValueBackgroundImagePixelArray[pixelIndex] = averageBackgroundValue;
        pixelIndex++;
      }
    }

    return singleValueBackgroundImagePixelArray;
  }

  /**
   * Creates an interpolated background image pixel array from the inspection image
   *
   * This was ported from 5DX FET/HSSOP code written by Patrick Lacz
   *
   * @author George Booth
   */
  private float[] createInterpolatedBackgroundImage(JointInspectionData jointInspectionData,
                                                    SliceNameEnum sliceNameEnum,
                                                    Image image,
                                                    RegionOfInterest padBorderRoi,
                                                    String jointName) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    Subtype subtype = jointInspectionData.getSubtype();
    final float MAXIMUM_EXPECTED_THICKNESS =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_MEASUREMENT_MAXIMUM_EXPECTED_THICKNESS);

    // define some borders around pad; the % may need to be adjusted if the image is large
    int fractionOfHeight = (int)Math.round((float)padBorderRoi.getLengthAcross() / 20.0f);
    int fractionOfWidth = (int)Math.round((float)padBorderRoi.getLengthAlong() / 20.0f);;

    // create a rectangular row search area 6 pixels high along the pad with a border
    RegionOfInterest rowSearchAreaRoi = new RegionOfInterest(padBorderRoi);
    rowSearchAreaRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);
    int rowSearchWidthWithBorder = padBorderRoi.getLengthAlong() + (fractionOfWidth * 2);
    // make sure rowSearchAreaRoi with border is within image width
    int roiCenterAlong = (int)rowSearchAreaRoi.getCenterAlong();
    int rowSearchHalfWidth = rowSearchWidthWithBorder / 2;
    int imageWidth = image.getWidth();
    if (roiCenterAlong - rowSearchHalfWidth <= 0 ||
        roiCenterAlong + rowSearchHalfWidth >= imageWidth)
    {
      int roiMinX = (int)(rowSearchAreaRoi.getMinCoordinateAlong());
      int roiMaxX = roiMinX + (int)(rowSearchAreaRoi.getLengthAlong());
      int leftBorderWidth = roiMinX;
      int rightBorderWidth = imageWidth - roiMaxX;
      int smallestBorder = Math.min(leftBorderWidth, rightBorderWidth) - 1;
      rowSearchWidthWithBorder = padBorderRoi.getLengthAlong() + 2 * smallestBorder;
    }
    rowSearchAreaRoi.setWidthKeepingSameCenter(rowSearchWidthWithBorder);
    rowSearchAreaRoi.setHeightKeepingSameCenter(6);

    // create an across background by sliding the row search area down the pad
    float[] acrossBackground = new float[padBorderRoi.getLengthAcross()];
    int rowSearchCenterX = (int)rowSearchAreaRoi.getCenterAlong();
    int yIndex = (int)padBorderRoi.getMinCoordinateAcross();

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    for (int i = 0; i < acrossBackground.length; i++)
    {
      rowSearchAreaRoi.setCenterXY(rowSearchCenterX, yIndex++);

      // make sure ROI is within image (smoothing will compensate for any missing row values)
      if (rowSearchAreaRoi.fitsWithinImage(image))
      {
        float lightestValueInRow = Statistics.maxValue(image, rowSearchAreaRoi);
        float darkestValueInRow = Statistics.minValue(image, rowSearchAreaRoi);

        float background = lightestValueInRow; // we assume background is light because there�s no solder there
        float foreground = darkestValueInRow; // we assume foreground is dark because that�s where the solder is
        float deltaGray = background - foreground; // delta gray is just the difference between background and foreground
        int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
        float thicknessUsingBackground = thicknessTable.getThicknessInMillimeters(background, deltaGray, backgroundSensitivityOffset);

        // if the thickness exceeds the user set max thickness, reduce it
        while (thicknessUsingBackground > MAXIMUM_EXPECTED_THICKNESS && background > 5.0f)
        {
          background -= 1.0f; // one gray level at a time
          deltaGray = background - foreground;
          thicknessUsingBackground = thicknessTable.getThicknessInMillimeters(background, deltaGray, backgroundSensitivityOffset);
//          System.out.println("  adjusted across background = " + background + ", deltaGray = " + deltaGray +
//                             ", thickness = " + thicknessUsingBackground * MILS_PER_MILLIMETER + " (mils)");
        }

        acrossBackground[i] = background;
      } else {
        acrossBackground[i] = _DEFAULT_BACKGROUND;
      }
    }

    // smooth it
    float[] smoothedAcrossBackground = ProfileUtil.getSmoothedProfile(acrossBackground, _smoothingKernelLength);

    if (_backgroundDebug) writeProfileToCSVFile("c:/temp/!acrossBackground1.csv",
                          "acrossBackground", acrossBackground, 1.0f,
                          "smoothedAcrossBackground", smoothedAcrossBackground, 1.0f);
    for (int i = 0; i < smoothedAcrossBackground.length; i++)
      acrossBackground[i] = smoothedAcrossBackground[i];


    // create a rectangular row search area 6 pixels width across the pad
    RegionOfInterest columnSearchAreaRoi = new RegionOfInterest(padBorderRoi);
    columnSearchAreaRoi.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);
    int columnSearchWidthWithBorder = padBorderRoi.getLengthAcross() + (fractionOfHeight * 2);
    // make sure columnSearchAreaRoi with border is within image height
    int roiCenterAcross = (int)columnSearchAreaRoi.getCenterAcross();
    int columnSearchHalfWidth = columnSearchWidthWithBorder / 2;
    int imageHeight = image.getHeight();
    if (roiCenterAcross - columnSearchHalfWidth <= 0 ||
        roiCenterAcross + columnSearchHalfWidth >= imageHeight)
    {
      int roiMinY = (int)(columnSearchAreaRoi.getMinCoordinateAcross());
      int roiMaxY = roiMinY + (int)(columnSearchAreaRoi.getLengthAcross());
      int topBorderWidth = roiMinY;
      int bottomBorderWidth = imageHeight - roiMaxY;
      int smallestBorder = Math.min(topBorderWidth, bottomBorderWidth) - 1;
      columnSearchWidthWithBorder = padBorderRoi.getLengthAcross() + 2 * smallestBorder;
    }
    columnSearchAreaRoi.setHeightKeepingSameCenter(columnSearchWidthWithBorder);
    columnSearchAreaRoi.setWidthKeepingSameCenter(6);

    // create an along background by sliding the column search area across the pad
    float[] alongBackground = new float[padBorderRoi.getLengthAlong()];
    int columnSearchCenterY = (int)columnSearchAreaRoi.getCenterAcross();
    int xIndex = (int)padBorderRoi.getMinCoordinateAlong();
    for (int i = 0; i < alongBackground.length; i++)
    {
      columnSearchAreaRoi.setCenterXY(xIndex++, columnSearchCenterY);

      // make sure ROI is within image (smoothing will compensate for any missing column values)
      if (columnSearchAreaRoi.fitsWithinImage(image))
      {
        float lightestValueInRow = Statistics.maxValue(image, columnSearchAreaRoi);
        float darkestValueInRow = Statistics.minValue(image, columnSearchAreaRoi);

        float background = lightestValueInRow; // we assume background is light because there�s no solder there
        float foreground = darkestValueInRow; // we assume foreground is dark because that�s where the solder is
        float deltaGray = background - foreground; // delta gray is just the difference between background and foreground
        int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
        float thicknessUsingBackground = thicknessTable.getThicknessInMillimeters(background, deltaGray, backgroundSensitivityOffset);

        // if the thickness exceeds the user set max thickness, reduce it
        while (thicknessUsingBackground > MAXIMUM_EXPECTED_THICKNESS && background > 5.0f)
        {
          background -= 1.0f; // one gray level at a time
          deltaGray = background - foreground;
          thicknessUsingBackground = thicknessTable.getThicknessInMillimeters(background, deltaGray, backgroundSensitivityOffset);
//          System.out.println("  adjusted along background = " + background + ", deltaGray = " + deltaGray +
//                             ", thickness = " + thicknessUsingBackground * MILS_PER_MILLIMETER + " (mils)");
        }

        alongBackground[i] = background;
      } else {
        alongBackground[i] = _DEFAULT_BACKGROUND;
     }
    }

    float[] smoothedAlongBackground = ProfileUtil.getSmoothedProfile(alongBackground, _smoothingKernelLength);

    if (_backgroundDebug) writeProfileToCSVFile("c:/temp/!alongBackground1.csv",
                          "alongBackground", alongBackground, 1.0f,
                          "smoothedAlongBackground", smoothedAlongBackground, 1.0f);
    for (int i = 0; i < smoothedAlongBackground.length; i++)
      alongBackground[i] = smoothedAlongBackground[i];

    /* A note from Patrick Patrick Lacz:
       This is something of a hack, but could be presented to the user.
       Try to exploit symmatry in the joint in creating graylevels.
       We would want this to be optional because sometimes the joint isn't symmetrical..
       or perhaps the background isn't.
     */
//    float decisionBetweenMinAndMax = 0.75f;
    float decisionBetweenMinAndMax = 0.5f;
    boolean exploitSymmatryAcross = true;
    if (exploitSymmatryAcross == true)
    {
      int distanceAcross = smoothedAcrossBackground.length;
      for (int i = 0; i < distanceAcross / 2; i++)
      {
        float left = smoothedAcrossBackground[i];
        float right = smoothedAcrossBackground[distanceAcross - 1 - i];

        float minValue = Math.min(left, right);
        float maxValue = Math.max(left, right);
        float symValue = Math.min(maxValue, minValue);

        //float symValue = decisionBetweenMinAndMax * maxValue + (1.0 - decisionBetweenMinAndMax) * minValue;
//        if (left > symValue)
          smoothedAcrossBackground[i] = symValue;
//        if (right > symValue)
          smoothedAcrossBackground[distanceAcross - 1 - i] = symValue;
      }
    }
//  not done in 5DX - not sure why
    boolean exploitSymmatryAlong = false;
    if (exploitSymmatryAlong == true)
    {
      int distanceAlong = smoothedAlongBackground.length;
      for (int i = 0; i < distanceAlong / 2; i++)
      {
        float left = smoothedAlongBackground[i];
        float right = smoothedAlongBackground[distanceAlong - 1 - i];

        float minValue = Math.min(left, right);
        float maxValue = Math.max(left, right);
        float symValue = Math.min(maxValue, minValue);

        //float symValue = decisionBetweenMinAndMax * maxValue + (1.0 - decisionBetweenMinAndMax) * minValue;
//        if (left < symValue)
          smoothedAlongBackground[i] = symValue;
//        if (right < symValue)
          smoothedAlongBackground[distanceAlong - 1 - i] = symValue;
      }
    }

    if (_backgroundDebug) writeProfileToCSVFile("c:/temp/!acrossBackground2.csv",
                          "acrossBackground", acrossBackground, 1.0f,
                          "smoothedAcrossBackground", smoothedAcrossBackground, 1.0f);

    if (_backgroundDebug) writeProfileToCSVFile("c:/temp/!alongBackground2.csv",
                          "alongBackground", alongBackground, 1.0f,
                          "smoothedAlongBackground", smoothedAlongBackground, 1.0f);

    // create background image pixel array
    int backgroundWidth = smoothedAlongBackground.length;
    int backgroundHeight = smoothedAcrossBackground.length;
    float[] interpolatedBackgroundImagePixelArray = new float[smoothedAcrossBackground.length * smoothedAlongBackground.length];
    for (int y = 0; y < backgroundHeight; y++)
    {
      for (int x = 0; x < backgroundWidth; x++)
      {
        float alongValue = smoothedAlongBackground[x];
        float acrossValue = smoothedAcrossBackground[y];

        float minValue = Math.min(alongValue, acrossValue);
        float maxValue = Math.max(alongValue, acrossValue);
        float pixelValue = (decisionBetweenMinAndMax * maxValue + (1.0f - decisionBetweenMinAndMax) * minValue);
        interpolatedBackgroundImagePixelArray[y * backgroundWidth + x] = pixelValue;
      }
    }

    return interpolatedBackgroundImagePixelArray;
  }

  /**
   * Find all the voiding pixels.
   *
   * @author George Booth
   */
  private float[] findVoidingPixels(Subtype subtype,
                                    float[] inspectedImagePixelArray,
                                    RegionOfInterest inspectedRegion,
                                    float[] expectedThicknessPixelArray,
                                    float[] backgroundImagePixelArray,
                                    int orientation,
                                    String jointName) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(inspectedImagePixelArray != null);
    Assert.expect(expectedThicknessPixelArray != null);
    Assert.expect(backgroundImagePixelArray != null);
    Assert.expect(inspectedRegion != null);
    int imageWidth = inspectedRegion.getLengthAlong();
    int imageHeight = inspectedRegion.getLengthAcross();
    int imagePixelArraySize = imageWidth *imageHeight;
    Assert.expect(inspectedImagePixelArray.length == imagePixelArraySize);
    Assert.expect(expectedThicknessPixelArray.length == imagePixelArraySize);
    Assert.expect(backgroundImagePixelArray.length == imagePixelArraySize);

    // make an ROI in the expected image that covers the area in the passed in region.
    RegionOfInterest expectedRoi = new RegionOfInterest(inspectedRegion);

    // The expected image is in mils thickness. Convert it to gray levels to compare with the inspected image.
    // We use the background image and the solder table to do this.
    float[] expectedImagePixelArray = createExpectedImagePixelArray(expectedThicknessPixelArray,
                                                                    backgroundImagePixelArray,
                                                                    expectedRoi,
                                                                    subtype);
//    displayDebugImageArray(expectedImagePixelArray, inspectedRegion, "expectedImage");
//    displayDebugImageArray(inspectedImagePixelArray, inspectedRegion, "inspectedImage");
    if (_devDebug)
    {
      // save the expected image in a viewable png file (c:/temp/!<jointName>_expectedImage.png)
       Image tempImage1 = Image.createFloatImageFromArray(imageWidth, imageHeight, expectedImagePixelArray);
       saveImageAsPngFile(tempImage1, "c:/temp/!" + jointName + "_expectedImage.png");
       tempImage1.decrementReferenceCount();

       // save the inspected image in a viewable png file (c:/temp/!<jointName>_inspectedImage.png)
       Image tempImage2 = Image.createFloatImageFromArray(imageWidth, imageHeight, inspectedImagePixelArray);
       saveImageAsPngFile(tempImage2, "c:/temp/!" + jointName + "_inspectedImage.png");
       tempImage2.decrementReferenceCount();
    }

    float[] resultImagePixelArray = new float[imagePixelArraySize];

    final float VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE = 0.05f +
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE);

    int numVoidedPixels = 0;
    int numSolderPixels = 0;
    int numExpectedPixels = 0;
    int pixelIndex = 0;
    for (int y = 0; y < imageHeight; y++)
    {
      for (int x = 0; x < imageWidth; x++)
      {
//        pixelIndex = y * imageWidth + x;
        float expectedGraylevel = expectedImagePixelArray[pixelIndex];
        float inspectionGraylevel = inspectedImagePixelArray[pixelIndex];

        // don't check pixels that we don't expect to have have solder
        if ((int)(expectedGraylevel) == 0)
        {
          resultImagePixelArray[pixelIndex] = 0.0f;
          pixelIndex++;
          continue;
        }

        numExpectedPixels++;
        boolean isVoiding = false;

        // use "GRAYLEVEL DIFFERENCE"
        float deltaGraylevel = inspectionGraylevel - expectedGraylevel;
        isVoiding = (deltaGraylevel > VOIDING_MAXIMUM_GRAY_LEVEL_DIFFERENCE);

/* other techiques tried in the 5DX

        // use "MISSING THICKNESS"
        float deltaGraylevel = expectedGraylevel - inspectionGraylevel;
        float thicknessInMils = 0.f;
        if (deltaGraylevel > 0.f)
           thicknessInMils = _thicknessTable.getThicknessInMillimeters(inspectionGraylevel, deltaGraylevel);
        isVoiding = (thicknessInMils > VOIDING_THRESHOLD);

        // use "PERCENT OF INVERSE GRAYLEVEL"
        float differenceFromExpected = 0.01f * VOIDING_THRESHOLD * (255.f - expectedGraylevel);
        float thresholdGraylevel = expectedGraylevel - differenceFromExpected;
        isVoiding = (inspectionGraylevel < thresholdGraylevel);
*/
        if (isVoiding)
        {
          resultImagePixelArray[pixelIndex] = 255.0f;
        }
        else
        {
          resultImagePixelArray[pixelIndex] = 0.0f;
        }
        pixelIndex++;
      }
    }

    return resultImagePixelArray;
  }

  /**
   * Do noise reduction, requires a previous findVoidingPixels call.
   * This is conducted by counting the pixels in each contiguous (4-connected) pixel region. If there
   * are < the number of pixels passed in, we throw it away.
   *
   * @author George Booth
   */
  private void removeVoidsSmallerThanNPixels(float[] resultImagePixelArray,
                                             RegionOfInterest expectedImageRoi,
                                             int smallVoidsThreshold)
  {
    Assert.expect(resultImagePixelArray != null);
    Assert.expect(expectedImageRoi != null);
    int imageWidth = expectedImageRoi.getLengthAlong();
    int imageHeight = expectedImageRoi.getLengthAcross();
    int imagePixelArraySize = imageWidth * imageHeight;
    Assert.expect(resultImagePixelArray.length == imagePixelArraySize);

    Map<Integer, LabelRegion> labelRegionMap = new HashMap<Integer, LabelRegion>();
    int numberOfRegions = ExpectedImageVoidingRegions.clusterContiguousRegions(resultImagePixelArray, expectedImageRoi, 1.0f, labelRegionMap);

    Set regionEntries = labelRegionMap.entrySet();
    Iterator regionIter = regionEntries.iterator();
    while (regionIter.hasNext())
    {
      Map.Entry entry = (Map.Entry)regionIter.next();
      LabelRegion labelRegion = (LabelRegion)entry.getValue();
      if (labelRegion.getNumberOfPixels() < smallVoidsThreshold)
      {
        ExpectedImageVoidingRegions.clearRegion(resultImagePixelArray, imageWidth, labelRegion);
        numberOfRegions--;
      }
    }
  }

  /**
   * Gather all of the 'on' pixels from the result image and return them in (x,y) pair format.
   * Requires a previous findVoidingPixels call.
   *
   * @author George Booth
   */
  private Pair<float[], Integer> getVoidingPixels(float[] resultImagePixelArray,
                                                  RegionOfInterest expectedRoi)
  {
    Assert.expect(resultImagePixelArray != null);
    Assert.expect(expectedRoi != null);
    int imageWidth = expectedRoi.getLengthAlong();
    int imageHeight = expectedRoi.getLengthAcross();

    int numVoidingPixels = 0;
    int pixelIndex = 0;
    for (int y = 0; y < imageHeight; y++)
    {
      for (int x = 0; x < imageWidth; x++)
      {
//        pixelIndex = y * imageWidth + x;
        float voidingPixelValue = resultImagePixelArray[pixelIndex];

        if (voidingPixelValue >= 1.f)
        {
          numVoidingPixels++;
          // The voiding region analysis has altered the result image with small values (dark pixels)
          // Update voided pixels to full intensity
          resultImagePixelArray[pixelIndex] = 255.0f;
        }
        pixelIndex++;
      }
    }

    return new Pair<float[], Integer>(resultImagePixelArray, numVoidingPixels);
  }

  /**
   * "Maximum Thickness", "Minimum Thickness", "Threshold Mask"
   * @author George Booth
   */
  private VoidingTechniqueEnum getVoidingTechnique(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // CR32282 not validated.  Use Minimum Thickness as default.
    if (true)
      return VoidingTechniqueEnum.MINIMUM_THICKNESS;

    String voidingTechniqueSetting =
      (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.EXPOSED_PAD_VOIDING_VOIDING_TECHNIQUE);

    if (voidingTechniqueSetting.equals("Maximum Thickness"))
    {
      return VoidingTechniqueEnum.MAXIMUM_THICKNESS;
    }
    else if (voidingTechniqueSetting.equals("Minimum Thickness"))
    {
      return VoidingTechniqueEnum.MINIMUM_THICKNESS;
    }
    else if (voidingTechniqueSetting.equals("Threshold Mask"))
    {
      return VoidingTechniqueEnum.THRESHOLD_MASK;
    }
    else
    {
      // Shouldn't get here!
      Assert.expect(false, "Unexpected Voiding Technique setting: " + voidingTechniqueSetting);
      return null;
    }
  }


  /**
   * Create a gray level thickness array that represents the solder thickness in gray levels for each pixel
   *
   * @author George Booth
   */
  private float[] createExpectedImagePixelArray(float[] expectedThicknessPixelArray,
                                                float[] backgroundImagePixelArray,
                                                RegionOfInterest expectedImageRoi,
                                                Subtype subtype) throws DatastoreException
  {
    Assert.expect(expectedThicknessPixelArray != null);
    Assert.expect(backgroundImagePixelArray != null);
    Assert.expect(expectedImageRoi != null);
    Assert.expect(subtype != null);

    int imageWidth = expectedImageRoi.getLengthAlong();
    int imageHeight = expectedImageRoi.getLengthAcross();
    int imagePixelArraySize = imageWidth * imageHeight;
    Assert.expect(expectedThicknessPixelArray.length == imagePixelArraySize);
    Assert.expect(backgroundImagePixelArray.length == imagePixelArraySize);

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);

    boolean print = false; int minX = 30; int maxX = 35; int minY = 10; int maxY = 15;

    float[] expectedImagePixelArray = new float[imagePixelArraySize];
    int pixelIndex = 0;
    for (int y = 0; y < imageHeight; y++)
    {
      for (int x = 0; x < imageWidth; x++)
      {
//        pixelIndex = y * imageWidth + x;
        float background = backgroundImagePixelArray[pixelIndex];
        float thicknessInMillimeters = expectedThicknessPixelArray[pixelIndex];
        float thicknessInMils = thicknessInMillimeters * MILS_PER_MILLIMETER;
        float deltaGrayLevel = 0.0f;
        if (thicknessInMils > 0.0f)
        {
          // tweak for pathological image sets (all black)
          if (background < 0.5f)
          {
            background = 1.0f;
          }
          deltaGrayLevel = thicknessTable.getDeltaGrayLevel(background, thicknessInMils);
        }

        if (thicknessInMillimeters > 0.0f)
        {
          expectedImagePixelArray[pixelIndex] = background - deltaGrayLevel;
        }
        else
        {
          expectedImagePixelArray[pixelIndex] = 0.0f;
        }
        if (print && x >= minX && x <= maxX & y >= minY & y <= maxY)
          System.out.println("  x = " + x + ", y = " + y + ", background = " + background +
                             ", thickness = " + thicknessInMillimeters +
                             " (" + thicknessInMils + " mils)" +
                             ", deltaGray = " + deltaGrayLevel +
                             ", imagePixel = " + expectedImagePixelArray[pixelIndex]);
        pixelIndex++;
      }
    }

    return expectedImagePixelArray;
  }

  /**
   * Convert a thickness pixel array to an image pixel array
   *
   * @author George Booth
   */
  private float[] convertThicknessArrayToImageArray(float[] thicknessPixelArray,
                                                    float[] backgroundPixelArray,
                                                    RegionOfInterest imageRoi,
                                                    SolderThickness thicknessTable)
  {
    Assert.expect(thicknessPixelArray != null);
    Assert.expect(backgroundPixelArray != null);
    Assert.expect(imageRoi != null);
    Assert.expect(thicknessTable != null);

    int imageWidth = imageRoi.getLengthAlong();
    int imageHeight = imageRoi.getLengthAcross();
    int imagePixelArraySize = imageWidth * imageHeight;
    Assert.expect(thicknessPixelArray.length == imagePixelArraySize);

    float contributorPixel = ExpectedImageLearning.getContributorPixelValue();

    boolean print = false; int minX = 30; int maxX = 35; int minY = 10; int maxY = 15;

    float[] imagePixelArray = new float[imagePixelArraySize];
    int pixelIndex = 0;
    for (int y = 0; y < imageHeight; y++)
    {
      for (int x = 0; x < imageWidth; x++)
      {
//        pixelIndex = y * imageWidth + x;
        float background = backgroundPixelArray[pixelIndex];
        float thicknessInMillimeters = thicknessPixelArray[pixelIndex];
        if (thicknessInMillimeters == contributorPixel)
        {
          // special for interactive learning - highlight new pixel for this image
          imagePixelArray[pixelIndex] = HIGHLIGHT_PIXEL_VALUE;
        }
        else
        {
          float thicknessInMils = thicknessInMillimeters * MILS_PER_MILLIMETER;
          float deltaGrayLevel = 0.0f;
          if (thicknessInMils > 0.0f)
          {
            deltaGrayLevel = thicknessTable.getDeltaGrayLevel(background, thicknessInMils);
          }
          float imagePixel = 0.0f;
          if (deltaGrayLevel > 0.0f)
          {
            imagePixel = background - deltaGrayLevel;

            if (print && x >= minX && x <= maxX & y >= minY & y <= maxY)
              System.out.println("  x = " + x + ", y = " + y + ", background = " + background +
                                 ", thickness = " + thicknessInMillimeters +
                                 " (" + thicknessInMils + " mils)" +
                                 ", deltaGray = " + deltaGrayLevel + ", imagePixel = " + imagePixel);

            imagePixelArray[pixelIndex] = imagePixel;

          }
        }
        pixelIndex++;
      }
    }
    return imagePixelArray;
  }

/*
 **************************************************************************
 ************** Debug methods *********************************************
 **************************************************************************
 */

  /**
   * Display the image pixel array as image in a pop-up window (debug only)
   *
   * @author George Booth
   */
  private void displayDebugImageArray(float[] imagePixelArray,
                                      RegionOfInterest imageRoi,
                                      String title)
  {
    Assert.expect(imagePixelArray != null);
    Assert.expect(imageRoi != null);
    int imageWidth = imageRoi.getLengthAlong();
    int imageHeight = imageRoi.getLengthAcross();
    int imagePixelArraySize = imageWidth * imageHeight;
    Assert.expect(imagePixelArray.length == imagePixelArraySize);

    Image image = Image.createFloatImageFromArray(imageWidth, imageHeight, imagePixelArray);
    ImageDebugWindow.displayAndBlock(image, title);
    image.decrementReferenceCount();
  }

  /**
   * Display the gray level thickness pixel array as image in a pop-up window (debug only)
   *
   * @author George Booth
   */
  private void displayDebugThicknessPixelArray(float[] thicknessPixelArray,
                                               float[] backgroundPixelArray,
                                               RegionOfInterest imageRoi,
                                               String title,
                                               SolderThickness thicknessTable)
  {
    Assert.expect(imageRoi != null);
    Assert.expect(title != null);

    int imageWidth = imageRoi.getLengthAlong();
    int imageHeight = imageRoi.getLengthAcross();

    float[] imagePixelArray = convertThicknessArrayToImageArray(thicknessPixelArray, backgroundPixelArray, imageRoi, thicknessTable);

    Image image = Image.createFloatImageFromArray(imageWidth, imageHeight, imagePixelArray);
    ImageDebugWindow.displayAndBlock(image, title);
    image.decrementReferenceCount();
  }

  /**
   * Save the gray level thickness pixel array as image in a png file in c:/temp/<title>.png (debug only)
   *
   * @author George Booth
   */
  private void saveThicknessPixelArrayAsPngFile(float[] thicknessPixelArray,
                                                float[] backgroundPixelArray,
                                                RegionOfInterest imageRoi,
                                                String title,
                                                SolderThickness thicknessTable)
  {
    Assert.expect(imageRoi != null);
    Assert.expect(title != null);
    Assert.expect(thicknessTable != null);

    int imageWidth = imageRoi.getLengthAlong();
    int imageHeight = imageRoi.getLengthAcross();

    float[] imagePixelArray = convertThicknessArrayToImageArray(thicknessPixelArray, backgroundPixelArray, imageRoi, thicknessTable);

    Image image = Image.createFloatImageFromArray(imageWidth, imageHeight, imagePixelArray);
    saveImageAsPngFile(image, "c:/temp/!" + title + ".png");
    image.decrementReferenceCount();
  }

  /**
   * Save an image to the disk temp dir.
   *
   * @author George Booth
   */
  private void saveImageAsPngFile(Image image, String imagePath)
  {
    try
    {
      ImageIoUtil.savePngImage(image, imagePath);
    }
    catch(CouldNotCreateFileException fe)
    {
      System.out.println("COULD NOT CREATE EXPECTED IMAGE FILE");
    }
  }

  private void writeProfileToCSVFile(String filePath,
                                     String profileName1, float[] profile1, float scaleFactor1,
                                     String profileName2, float[] profile2, float scaleFactor2)
  {
    String delimiter = ", ";

    // do not append
    try
    {
      FileWriterUtil fileWriter = new FileWriterUtil(filePath, false);
      fileWriter.open();

      String title = "Profile";

      // report title and date
      Date date = new Date();
      if (title != null)
      {
        fileWriter.writeln(title + delimiter + date);
      }
      else
      {
        fileWriter.writeln(delimiter + delimiter + date);
      }
      fileWriter.writeln();

      fileWriter.writeln(profileName1);
      fileWriter.writeln(profileName2);
      fileWriter.writeln();

      // column header line
      StringBuffer headerLine = new StringBuffer();
      headerLine.append("index, value1, value2");
      fileWriter.writeln(headerLine.toString());

      // profile data
      int len = profile1.length;
      if (profile2.length > len)
        len = profile2.length;
      float last1 = profile1[0];
      float last2 = profile2[0];
      for (int i = 0; i < profile1.length; i++)
      {
        StringBuffer reportLine = new StringBuffer();
        reportLine.append(i + delimiter);

        if (i < profile1.length)
        {
          last1 = profile1[i] * scaleFactor1;
          reportLine.append(last1 + delimiter);
        }
        else
          reportLine.append(last1 + delimiter);

        if (i < profile2.length)
        {
          last2 = profile2[i] * scaleFactor2;
          reportLine.append(last2 + delimiter);
        }
        else
          reportLine.append(last2);

        fileWriter.writeln(reportLine.toString());
      }

      fileWriter.close();
    }
    catch (IOException ioe)
    {
      ioe.printStackTrace();
    }
  }

}
