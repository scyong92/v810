package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;

import ij.*;
import ij.process.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.largePad.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * The Locator 'algorithm' is a set of routines used by the Measurement algorithms of each family.
 * The results from the locator are stored as measurements; use the get* routines to retrieve the values.
 *
 * <ul>
 * <li><pre>createAlgorithmSettings</pre> should be called from the constructor of each Measurement algorithm
 * and add all the results to the algorithm's settings.
 *
 * <li><pre>learn</pre> should be called from the <pre>learn</pre> method of the Algorithm.
 *
 * <li><pre>locateJoints</pre> should be called from the <pre>classifyJoints</pre> method of the Algorithm. In general, the
 * slice should be the Pad slice.
 * </ul>
 *
 * @author Patrick Lacz
 */
public class Locator
{
  protected static AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();

  // When the search window size is learned, it is currently computed by looking at this fraction of IPD of all the
  // concerned joints.
  // Another alternative would be to use IPD, but to the subtract off some fraction of the pad size.
  private final static float _LEARN_SEARCH_WINDOW_AT_FRACTION_OF_IPD = 0.4f;
  private static ImageCoordinate _imageCoordinateForMasking;
  private static ImageCoordinate _offsetForMasking;
  private static Double _maskImageRotationDegree = -1.0; 
  
  private static final String _TRUE = "True";
  private static final String _FALSE = "False";  

  /**
   * @author Patrick Lacz
   */
   public Locator()
   {
     // do nothing, the methods here are statically defined.
   }

   /**
    * Gathers the measured Region of Interest from the pad slice.
    * Use this routine to get a new RegionOfInterest centered at the joint location.
    * The size of the region is defined by the cad information.
    *
    * @author Patrick Lacz
    */
   public static RegionOfInterest getRegionOfInterestAtMeasuredLocation(JointInspectionData jointInspectionData)
   {
    Assert.expect(jointInspectionData != null);
    
    return getRegionOfInterestAtMeasuredLocation(jointInspectionData, AlgorithmUtil.choseSliceToPerformLocatorForJoint(jointInspectionData), true);
  }
   
   /**
    * Gathers the measured Region of Interest from the pad slice.
    * Use this routine to get a new RegionOfInterest centered at the joint location.
    * The size of the region is defined by the cad information.
    *
    * @author Patrick Lacz
    * @author Lim, Lay Ngor - support ableToPerformResize key.
    */
   public static RegionOfInterest getRegionOfInterestAtMeasuredLocation(JointInspectionData jointInspectionData, boolean ableToPerformResize)
   {
    Assert.expect(jointInspectionData != null);
    
    return getRegionOfInterestAtMeasuredLocation(jointInspectionData, AlgorithmUtil.choseSliceToPerformLocatorForJoint(jointInspectionData), ableToPerformResize);
   }   
   
   /**
    * Gathers the measured Region of Interest from the pad slice.
    * Use this routine to get a new RegionOfInterest centered at the joint location.
    * The size of the region is defined by the cad information.
    *
    * @author Patrick Lacz
    * @author Lim, Lay Ngor - Broken Pin
    * @edit by sheng chuan
    * XCR-2681, Resize feature bug on BGA ball diameter calculation
    * Duplicate from above function - need to evaluate function usage
    */
   public static RegionOfInterest getRegionOfInterestAtMeasuredLocation(JointInspectionData jointInspectionData,SliceNameEnum sliceNameEnum, boolean ableToPerformResize)
   {
    Assert.expect(jointInspectionData != null);

    RegionOfInterest jointRoi = jointInspectionData.getOrthogonalRegionOfInterestInPixels(ableToPerformResize);
    if (jointInspectionData.getPad().isThroughHolePad())
      jointRoi = AlgorithmUtil.getRegionOfInterestOfThroughholeBarrel(jointInspectionData, ableToPerformResize);

    if (hasJointLocationMeasurements(jointInspectionData, sliceNameEnum))
    {
      ImageCoordinate jointCenter = null;
      // the measured location lies at the center of the joint.
      if(ableToPerformResize || jointInspectionData.getJointInspectionResult().hasOrigImageLocation() == false)
        jointCenter = getMeasuredLocation(jointInspectionData, sliceNameEnum);
      else
        jointCenter = jointInspectionData.getJointInspectionResult().getOrigImageLocation();
      jointRoi.setCenterXY(jointCenter.getX(), jointCenter.getY());
    }

    return jointRoi;
  }
   
   /**
    * Gathers the measured Region of Interest from the pad slice.
    * Use this routine to get a new RegionOfInterest centered at the component location.
    * The size of the region is defined by the cad information.
    *
    * @author Patrick Lacz
    */
   public static RegionOfInterest getRegionOfInterestAtMeasuredLocation(ComponentInspectionData componentInspectionData)
   {
     Assert.expect(componentInspectionData != null);

     List<JointInspectionData> jointInspectionData = new ArrayList<JointInspectionData>();
     jointInspectionData.addAll(componentInspectionData.getInspectableJointInspectionDataSet());
     
     return getRegionOfInterestAtMeasuredLocation(componentInspectionData, AlgorithmUtil.choseSliceToPerformLocatorForJoint(jointInspectionData), true);
   }
   
   
   /**
    * Gathers the measured Region of Interest from the pad slice.
    * Use this routine to get a new RegionOfInterest centered at the component location.
    * The size of the region is defined by the cad information.
    *
    * @author Patrick Lacz
    * @author Lim, Lay Ngor - Broken Pin
    * @ edit by sheng chuan
    * XCR-2681, Resize feature bug on BGA ball diameter calculation
    * Duplicate from above, need to evaluate this function usage again.
    */
   public static RegionOfInterest getRegionOfInterestAtMeasuredLocation(ComponentInspectionData componentInspectionData,
     SliceNameEnum sliceNameEnum, boolean ableToPerformResize)
   {
     Assert.expect(componentInspectionData != null);
     Assert.expect(sliceNameEnum != null);

     // create a copy to avoid modifying the data
     RegionOfInterest componentRoi = new RegionOfInterest(componentInspectionData.getOrthogonalComponentRegionOfInterest(ableToPerformResize));
     // the measured location lies at the center of the joint.
     ImageCoordinate componentCenter;
     if(ableToPerformResize || componentInspectionData.getComponentInspectionResult().hasOrigImageLocation() == false)
       componentCenter = getMeasuredLocation(componentInspectionData, sliceNameEnum);
     else
       componentCenter = componentInspectionData.getComponentInspectionResult().getOrigImageLocation();
     componentRoi.setCenterXY(componentCenter.getX(), componentCenter.getY());
     return componentRoi;
   }

  /**
   * Use this routine to get the measured center of the joint.
   *
   * @author Patrick Lacz
   */
  public static ImageCoordinate getMeasuredLocation(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointMeasurement xMeasurement = getXLocationMeasurement(jointInspectionData, sliceNameEnum);
    JointMeasurement yMeasurement = getYLocationMeasurement(jointInspectionData, sliceNameEnum);

    return new ImageCoordinate(Math.round(xMeasurement.getValue()), Math.round(yMeasurement.getValue()));
  }

  /**
   * Use this routine to get the measured center of a component.
   *
   * @author Peter Esbensen
   */
  public static ImageCoordinate getMeasuredLocation(ComponentInspectionData componentInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    ComponentMeasurement xMeasurement = getXLocationMeasurement(componentInspectionData, sliceNameEnum);
    ComponentMeasurement yMeasurement = getYLocationMeasurement(componentInspectionData, sliceNameEnum);

    return new ImageCoordinate(Math.round(xMeasurement.getValue()), Math.round(yMeasurement.getValue()));
  }

  /**
     * Use this routine to get the X coordinate of the component's center.
     * In general, it is easier to use getMeasuredLocation instead of this routine.
     *
     * @author Peter Esbensen
     */
    public static ComponentMeasurement getXLocationMeasurement(
      ComponentInspectionData componentInspectionData,
        SliceNameEnum sliceNameEnum)
    {
      Assert.expect(componentInspectionData != null);
      Assert.expect(sliceNameEnum != null);

      ComponentInspectionResult result = componentInspectionData.getComponentInspectionResult();
      return result.getComponentMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_X_LOCATION);
    }

    /**
     * Use this routine to get the Y coordinate of the component's center.
     * In general, it is easier to use getMeasuredLocation instead of this routine.
     *
     * @author Peter Esbensen
     */
    public static ComponentMeasurement getYLocationMeasurement(
      ComponentInspectionData componentInspectionData,
        SliceNameEnum sliceNameEnum)
    {
      Assert.expect(componentInspectionData != null);
      Assert.expect(sliceNameEnum != null);

      ComponentInspectionResult result = componentInspectionData.getComponentInspectionResult();
      return result.getComponentMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_Y_LOCATION);
  }

  /**
   * Use this routine to get the X coordinate of the joint's center.
   * In general, it is easier to use getMeasuredLocation instead of this routine.
   *
   * @author Patrick Lacz
   */
  public static JointMeasurement getXLocationMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionResult result = jointInspectionData.getJointInspectionResult();
    Assert.expect(result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_X_LOCATION));
    return result.getJointMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_X_LOCATION);
  }

  /**
   * Use this routine to get the Y coordinate of the joint's center.
   * In general, it is easier to use getMeasuredLocation instead of this routine.
   *
   * @author Patrick Lacz
   */
  public static JointMeasurement getYLocationMeasurement(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionResult result = jointInspectionData.getJointInspectionResult();
    Assert.expect(result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_Y_LOCATION));
    return result.getJointMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_Y_LOCATION);
  }

  /**
   * @author Patrick Lacz
   */
  public static boolean hasJointLocationMeasurements(JointInspectionData jointInspectionData, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    JointInspectionResult result = jointInspectionData.getJointInspectionResult();

    return result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_X_LOCATION) &&
        result.hasJointMeasurement(sliceNameEnum, MeasurementEnum.LOCATOR_Y_LOCATION);
  }

  /**
   * Create the algorithm settings required by the locator algorithm.
   *
   * There are not separate algorithm settings for circular or throughhole joints since it was
   * viewed that in general we can not guarantee that a family will always have a circular
   * shape (eg, BGA's may be elliptical).
   *
   * The other reason for this decision was consistency across all algorithm families.
   *
   * Use the following code in the constructor of the Algorithm to add all of the algorithm settings for Locator.
   * <pre>
   * for (AlgorithmSetting algSetting : Locator.createAlgorithmSettings(displayOrder, currentVersion))
   *   addAlgorithmSetting(algSetting);
   * </pre>
   *
   * @author Patrick Lacz
   */
  public static Collection<AlgorithmSetting> createAlgorithmSettings(int displayOrder, int currentVersion)
  {
    ArrayList<AlgorithmSetting> settingList = new ArrayList<AlgorithmSetting>();

    // The "Search Window Across/Along" settings define the area that the filter will be
    // applied to. This includes the 'border' defined below, so the located joint will
    // never be plush against the side of this window.
    settingList.add( new AlgorithmSetting(
      AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS, // setting enum
      displayOrder++, // display order,
      120.f, // default value
      100.f, // minimum value
      400.f, // maximum value
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS,
      "HTML_DESC_SHARED_LOCATOR_(SEARCH_WINDOW_ACROSS)_KEY", // description URL Key
      "HTML_DETAILED_DESC_SHARED_LOCATOR_(SEARCH_WINDOW_ACROSS)_KEY", // detailed description URL Key
      "IMG_DESC_SHARED_LOCATOR_(SEARCH_WINDOW_ACROSS)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion));

    settingList.add(new AlgorithmSetting(
        AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG, // setting enum
        displayOrder++, // display order,
        120.f, // default value
        100.f, // minimum value
        400.f, // maximum value
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_SHARED_LOCATOR_(SEARCH_WINDOW_ALONG)_KEY", // description URL Key
        "HTML_DETAILED_DESC_SHARED_LOCATOR_(SEARCH_WINDOW_ALONG)_KEY", // detailed description URL Key
        "IMG_DESC_SHARED_LOCATOR_(SEARCH_WINDOW_ALONG)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion));

    // The "Effective Width/Height" settings define the size of the rectangle filter used.
    settingList.add( new AlgorithmSetting(
      AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH, // setting enum
      displayOrder++, // display order,
      100.f, // default value
      10.f, // minimum value
      300.f, // maximum value
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS, // units
      "HTML_DESC_SHARED_LOCATOR_(EFFECTIVE_WIDTH)_KEY",  // description URL Key
      "HTML_DETAILED_DESC_SHARED_LOCATOR_(EFFECTIVE_WIDTH)_KEY", // detailed description URL Key
      "IMG_DESC_SHARED_LOCATOR_(EFFECTIVE_WIDTH)_KEY",// image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion));

    settingList.add( new AlgorithmSetting(
      AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH, // setting enum
      displayOrder++, // display order,
      100.f, // default value
      10.f, // minimum value
      300.f, // maximum value
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG, // units
      "HTML_DESC_SHARED_LOCATOR_(EFFECTIVE_LENGTH)_KEY", // description URL Key
      "HTML_DETAILED_DESC_SHARED_LOCATOR_(EFFECTIVE_LENGTH)_KEY", // detailed description URL Key
      "IMG_DESC_SHARED_LOCATOR_(EFFECTIVE_LENGTH)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion));

    // The "Effective Shift Across/Along" setting define a correction that may be applied from the
    // located position. This is used when if Effective Length/Width has been used to lock onto a
    // feature that is not at the center of the joint.
    settingList.add( new AlgorithmSetting(
      AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ACROSS, // setting enum
      displayOrder++, // display order,
      0.0f, // default value
      -100.f, // minimum value
      100.f, // maximum value
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ACROSS, // units
      "HTML_DESC_SHARED_LOCATOR_(EFFECTIVE_SHIFT_ACROSS)_KEY", // description URL Key
      "HTML_DETAILED_DESC_SHARED_LOCATOR_(EFFECTIVE_SHIFT_ACROSS)_KEY", // detailed description URL Key
      "IMG_DESC_SHARED_LOCATOR_(EFFECTIVE_SHIFT_ACROSS)_KEY",  // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion));

    settingList.add( new AlgorithmSetting(
      AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ALONG, // setting enum
      displayOrder++, // display order,
      0.0f, // default value
      -100.f, // minimum value
      100.f, // maximum value
      MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG, // units
      "HTML_DESC_SHARED_LOCATOR_(EFFECTIVE_SHIFT_ALONG)_KEY", // description URL Key
      "HTML_DETAILED_DESC_SHARED_LOCATOR_(EFFECTIVE_SHIFT_ALONG)_KEY", // detailed description URL Key
      "IMG_DESC_SHARED_LOCATOR_(EFFECTIVE_SHIFT_ALONG)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion));



    // The "Border Size" setting defines the size of the border around the joint to use
    // in the algorithm. This region will attempt to match 'bright' pixels while inside
    // the rectangle, the filter matches 'dark' regions.
    settingList.add(new AlgorithmSetting(
        AlgorithmSettingEnum.LOCATE_BORDER_WIDTH, // setting enum
        displayOrder++, // display order,
        5.f, // default value
        .01f, // minimum value
        25.0f, // maximum value
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_SHARED_LOCATOR_(BORDER_WIDTH)_KEY", // description URL Key
        "HTML_DETAILED_DESC_SHARED_LOCATOR_(BORDER_WIDTH)_KEY", // detailed description URL Key
        "IMG_DESC_SHARED_LOCATOR_(BORDER_WIDTH)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.HIDDEN,
        currentVersion));

    // If the located position varies by more than the "Snapback Distance" from the
    // average location for an inspection region, we move the located position back
    // to the average location. We assume that the locations matched for these joints are wrong.
    AlgorithmSetting locateSnapbackDistance = new AlgorithmSetting(
        AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(3.0f), // default value
        MathUtil.convertMilsToMillimeters(0.0f), // minimum value
        MathUtil.convertMilsToMillimeters(100.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_LOCATOR_(SNAPBACK_DISTANCE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_SHARED_LOCATOR_(SNAPBACK_DISTANCE)_KEY", // detailed description URL Key
        "IMG_DESC_SHARED_LOCATOR_(SNAPBACK_DISTANCE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    locateSnapbackDistance.filter(JointTypeEnum.CAPACITOR);
    locateSnapbackDistance.filter(JointTypeEnum.RESISTOR);
    locateSnapbackDistance.filter(JointTypeEnum.POLARIZED_CAP);
    // Wei Chin (Tall Cap)
//    locateSnapbackDistance.filter(JointTypeEnum.TALL_CAPACITOR);
    settingList.add(locateSnapbackDistance);

    return settingList;
  }

  /**
   * @author Peter Esbensen
   */
  public static List<MeasurementEnum> getJointMeasurementEnums()
  {
    List<MeasurementEnum> measurementEnums = new ArrayList<MeasurementEnum>();
    measurementEnums.add(MeasurementEnum.LOCATOR_X_LOCATION);
    measurementEnums.add(MeasurementEnum.LOCATOR_Y_LOCATION);
    //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
    measurementEnums.add(MeasurementEnum.IMAGE_RESIZE_SCALE);
    return measurementEnums;
  }

  /**
   * @author Peter Esbensen
   */
  public static List<MeasurementEnum> getComponentMeasurementEnums()
  {
    List<MeasurementEnum> measurementEnums = new ArrayList<MeasurementEnum>();
    measurementEnums.add(MeasurementEnum.LOCATOR_X_LOCATION);
    measurementEnums.add(MeasurementEnum.LOCATOR_Y_LOCATION);
    //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
    measurementEnums.add(MeasurementEnum.IMAGE_RESIZE_SCALE);
    return measurementEnums;
  }

  /**
   * Search the region around where the pad is expected to be for the best match.
   *
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  protected static RegionOfInterest locateRegion(
      JointInspectionData joint,
      Image image,
      RegionOfInterest region,
      ReconstructionRegion inspectRegionInt,
      SliceNameEnum sliceNameEnum,
      Algorithm callingAlgorithm,
      List<DiagnosticInfo> listOfDiagnosticsToDisplay)
  {
    Assert.expect(joint != null);
    Assert.expect(image != null);
    Assert.expect(region != null);
    Assert.expect(inspectRegionInt != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    RegionOfInterest effectiveRegion = new RegionOfInterest(region);
    RegionOfInterest searchRegion = new RegionOfInterest(region);

    boolean isAbleToResize = AlgorithmUtil.isAlgorithmAbleToPerformResize(callingAlgorithm);
    // Get Algorithm Setting values

    // Scale the search region and effective region.
    Subtype subtype = joint.getSubtype();
    float effectiveSizeAlong = (Float)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH)) / 100.f;
    float effectiveSizeAcross = (Float)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH)) / 100.f;
    effectiveRegion.scaleFromCenterAlongAcross(effectiveSizeAlong, effectiveSizeAcross);

    float searchRegionAlong = (Float)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG)) / 100.f;
    float searchRegionAcross = (Float)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS)) / 100.f;

    //Lim, Lay Ngor - Broken Pin handling - make the search region larger and size larger if it is for Broken Pin detection.
    if(sliceNameEnum == SliceNameEnum.CAMERA_0 && 
      ((subtype.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE) || (subtype.getJointTypeEnum().equals(JointTypeEnum.OVAL_THROUGH_HOLE))) //Siew Yeng - XCR-3318 - Oval PTH
        && subtype.getEnabledAlgorithmEnums().contains(AlgorithmEnum.SHORT)))
    {
      //?? Want this auto handling??
      effectiveSizeAlong = effectiveSizeAlong + 0.2f;
      effectiveSizeAcross = effectiveSizeAcross + 0.2f;
      searchRegionAlong = searchRegionAlong + 0.2f;
      searchRegionAcross = searchRegionAcross + 0.2f;
    }

    if (searchRegionAlong < effectiveSizeAlong)
    {
      // post warning that we are resizing the search region.
      MeasurementUnitsEnum effectiveSizeAlongUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH);
      MeasurementUnitsEnum searchRegionAlongUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG);
      AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_LOCATOR_EFFECTIVE_REGION_LARGER_THAN_SEARCH_REGION_WARNING_KEY",
          new Object[]{subtype.getShortName(),
          joint.getFullyQualifiedPadName(),
          AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH.getName(),
          MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(effectiveSizeAlongUnitsEnum, Float.toString(100*effectiveSizeAlong)),
          AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG.getName(),
          MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(searchRegionAlongUnitsEnum, Float.toString(100*searchRegionAlong))
      }));
      searchRegionAlong = effectiveSizeAlong;
    }
    if (searchRegionAcross < effectiveSizeAcross)
    {
      // post warning that we are resizing the search region.
      MeasurementUnitsEnum effectiveSizeAcrossUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH);
      MeasurementUnitsEnum searchRegionAcrossUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS);

      AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_LOCATOR_EFFECTIVE_REGION_LARGER_THAN_SEARCH_REGION_WARNING_KEY",
          new Object[]{subtype.getShortName(),
          joint.getFullyQualifiedPadName(),
          AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH.getName(),
          MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(effectiveSizeAcrossUnitsEnum, Float.toString(100*effectiveSizeAcross)),
          AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS.getName(),
          MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(searchRegionAcrossUnitsEnum, Float.toString(100*searchRegionAcross))
      }));

      searchRegionAcross = effectiveSizeAcross;
    }
    searchRegion.scaleFromCenterAlongAcross(searchRegionAlong, searchRegionAcross);

    // Compute the border width from a % of Inter-Pad Distance.
    // One sanity check - the border shouldn't be more than 15 pixels, but 6-8 would be normal.
    int interPadDistanceInPixels = joint.getInterPadDistanceInPixels(isAbleToResize);
    
    float borderWidth = (Float)(joint.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_BORDER_WIDTH)) / 100.f;
    borderWidth *= interPadDistanceInPixels;
    int border = (int)borderWidth;
    border = Math.min(border, 15);
    border = Math.max(border, 3);

    int borderX = border;
    int borderY = border;

    // we may want to modify these in each axis, although for now each axis has the same value.
    searchRegion.setRegionShapeEnum(RegionShapeEnum.RECTANGULAR);
    RegionOfInterest searchRegionWithBorder = new RegionOfInterest(searchRegion);
    searchRegionWithBorder.setWidthKeepingSameCenter(searchRegion.getWidth() + 2*borderX);
    searchRegionWithBorder.setHeightKeepingSameCenter(searchRegion.getHeight() + 2*borderY);
    RegionOfInterest searchRegionWithBorderInImage = RegionOfInterest.createRegionFromIntersection(searchRegionWithBorder, image);

    int effectiveRegionWidth = effectiveRegion.getWidth();
    int effectiveRegionHeight = effectiveRegion.getHeight();

    if (searchRegionWithBorderInImage.equals(searchRegionWithBorder) == false)
    {
      // Post warning about the search region being too large.
      AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_LOCATOR_SEARCH_REGION_TOO_LARGE_WARNING_KEY",
          new Object[]{
          joint.getFullyQualifiedPadName()
          }));

      // if we can resolve this just by reducing the borders, do so.
      int effectiveBorderX = (searchRegionWithBorderInImage.getWidth() - effectiveRegion.getWidth())/2;
      int effectiveBorderY = (searchRegionWithBorderInImage.getHeight() - effectiveRegion.getHeight())/2;
      borderX = Math.min(borderX, Math.max(effectiveBorderX, 0));
      borderY = Math.min(borderY, Math.max(effectiveBorderY, 0));

      // create a search region to show on the screen.
      searchRegion = new RegionOfInterest(searchRegionWithBorderInImage);
      searchRegion.setWidthKeepingSameCenter(searchRegionWithBorderInImage.getWidth() - 2*borderX);
      searchRegion.setHeightKeepingSameCenter(searchRegionWithBorderInImage.getHeight() - 2*borderY);

      if (borderX == 0 || borderY == 0)
      {
        float goodEffectiveLengthAcross = (100.f*searchRegion.getLengthAcross()) / region.getLengthAcross();
        float goodEffectiveLengthAlong = (100.f*searchRegion.getLengthAlong()) / region.getLengthAlong();

        MeasurementUnitsEnum effectiveSizeAcrossUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH);
        MeasurementUnitsEnum effectiveSizeAlongUnitsEnum = subtype.getAlgorithmSettingUnitsEnum(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH);

        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_LOCATOR_EFFECTIVE_REGION_TOO_LARGE_FOR_IMAGE_WARNING_KEY",
            new Object[]{joint.getFullyQualifiedPadName(),
            AlgorithmSettingEnum.LOCATE_EFFECTIVE_WIDTH.getName(),
            MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(effectiveSizeAcrossUnitsEnum, Float.toString(goodEffectiveLengthAcross)),
            MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(effectiveSizeAcrossUnitsEnum, Float.toString(100*effectiveSizeAcross)),
            AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH.getName(),
            MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(effectiveSizeAlongUnitsEnum, Float.toString(goodEffectiveLengthAlong)),
            MeasurementUnitsEnum.labelValueWithUnitsIfNecessary(effectiveSizeAlongUnitsEnum, Float.toString(100*effectiveSizeAlong))
        }));
      }

      effectiveRegionWidth = Math.min(searchRegion.getWidth(), effectiveRegionWidth);
      effectiveRegionHeight = Math.min(searchRegion.getHeight(), effectiveRegionHeight);
    }


    listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(searchRegion, MeasurementRegionEnum.LOCATE_SEARCH_REGION));

    ImageCoordinate matchedLocation;

    boolean usingMasking = AlgorithmUtil.isSubtypeUsingMaskingMethod(subtype);

    // avoid the case where the search region is exactly than the size of the search window.
    if (borderX == 0 && borderY == 0 && usingMasking == false)
    {
      // given the search region and the joint size, there is only one point that the locator could choose
      // use that point, don't actually execute the filter.
      matchedLocation = new ImageCoordinate(searchRegion.getMinX(), searchRegion.getMinY());
    } 
    else if (usingMasking)
    {
      MaskImage createdMaskImage =  MaskImage.getInstance();
      
      //Siew Yeng - XCR-2843 - masking rotation
      double originalRotation = createdMaskImage.getSubtypeMaskImageOriginalRotation(subtype.getLongName());
      double rotation = joint.getComponent().getDegreesRotationAfterAllRotations();
      
      //if current joint rotation is not the same with the original mask image rotation, 
      // rotate the mask then add into mask image mapping
      //Siew Yeng - XCR-3708 - production looping crash due to single pad masking
      if(rotation != originalRotation && 
        (createdMaskImage.getSubtypeMaskImage(subtype.getLongName(), rotation) == null || 
         createdMaskImage.getCropMaskImage(subtype.getLongName(), rotation) == null))
      {   
        //Siew Yeng - XCR-3545 - masking rotation locator offset
        double correctedRotation = joint.getInspectionRegion().getRotationCorrectionInDegrees();//handle custom rotation
        handleMaskRotation(createdMaskImage, subtype, image.getWidth(), image.getHeight(), originalRotation, rotation, correctedRotation);
      }
      
      if (subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_LOCATOR_METHOD).toString().equalsIgnoreCase(LargePadVoidingAlgorithm._MASKING_FIXED_METHOD))
      {
        ImageCoordinate offset = createdMaskImage.getOffsetValueForMask(subtype.getLongName(), rotation);
        matchedLocation = offset;
        setMatchedLocationForMask(matchedLocation, offset.getX(), offset.getY());  
        
        matchedLocation = Filter.locateRectangle(
        image, searchRegionWithBorderInImage, effectiveRegionWidth, effectiveRegionHeight,
        borderX, borderY, true);
      }
      else
      {
        ImageCoordinate matchedLocationForMask = new ImageCoordinate(0,0);
 
        DoubleRef matchQuality = new DoubleRef(1.00f);

        java.awt.image.BufferedImage maskImage = createdMaskImage.getCropMaskImage(subtype.getLongName(), rotation);
        
        int minX = 0;
        int minY = 0;

        ImageCoordinate offset = createdMaskImage.getOffsetValueForMask(subtype.getLongName(), rotation);

        if (offset != null)
        {
          if (inspectRegionInt.getPads().size() > 1)
          {
            minX = searchRegion.getMinX();
            minY = searchRegion.getMinY();         
          }
          else
          {
            minX = offset.getX();
            minY = offset.getY();
          }
        }

        Image maskImageForTemplateMatching = Image.createFloatImageFromBufferedImage(maskImage);

        int offsetX = 0;
        int offsetY = 0;
//        remove this if / else condition as the if condition is always true, _maskImageRotationDegree is always reset to -1 before get in here
//        if (joint.getComponent().getDegreesRotationRelativeToBoard() != _maskImageRotationDegree)
//        {
        if (RegionOfInterest.createRegionFromImage(maskImageForTemplateMatching).fitsWithinImage(image))
        {
          if (inspectRegionInt.getPads().size() > 1)
            matchedLocationForMask.setLocation(searchRegion.getMinX(), searchRegion.getMinY());         
          else
            matchedLocationForMask = Filter.matchTemplate(image, maskImageForTemplateMatching, RegionOfInterest.createRegionFromImage(image), MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING, matchQuality);
        }
        else
        {
          matchedLocationForMask.setLocation(0, 0);
        }
           
        offsetX = Math.abs(matchedLocationForMask.getX() - minX);
        offsetY = Math.abs(matchedLocationForMask.getY() - minY);

          //old masking rotation method - not working
//          int degree = (int)Math.abs(joint.getComponent().getDegreesRotationRelativeToBoard());
//
//          java.awt.image.BufferedImage oriMaskBufferedImage = createdMaskImage.getSubtypeMaskImage(subtype.getLongName());
//          Image oriMaskImage = Image.createFloatImageFromBufferedImage(oriMaskBufferedImage);
//
//          if ((offsetX > 25 || offsetY > 25) && degree != 0)
//          {                  
//            //doesn't matter since mask has been symmetrized
//            if (degree == 90 || degree == 270)
//              degree = 90;
//
//            if (degree == 180)
//              degree = 0;
//
//            Image rotatedMaskImageForTemplateMatching = rotateMask(maskImageForTemplateMatching, degree);
//            Image rotatedMaskImage = rotateMask(oriMaskImage, degree);
//
//            if (RegionOfInterest.createRegionFromImage(maskImageForTemplateMatching).fitsWithinImage(image))            
//              matchedLocationForMask = Filter.matchTemplate(image, rotatedMaskImageForTemplateMatching, RegionOfInterest.createRegionFromImage(image), MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING, matchQuality);           
//
//            minX = offset.getX();
//            minY = offset.getY();
//            setOffsetValueForMask(minX, minY);
//            offsetX = Math.abs(matchedLocationForMask.getX() - minX);
//            offsetY = Math.abs(matchedLocationForMask.getY() - minY);
//
//            //if matching still fail after rotate, use back original mask image
//            if (offsetX > 30 || offsetY > 30)
//            {      
//              createdMaskImage.removeSubtypeMaskImage(subtype.getLongName());
//              createdMaskImage.addSubtypeMaskImage(subtype.getLongName(), oriMaskImage.getBufferedImage());
//            } 
//            else
//            {
//              createdMaskImage.removeSubtypeMaskImage(subtype.getLongName());
//              createdMaskImage.addSubtypeMaskImage(subtype.getLongName(), rotatedMaskImage.getBufferedImage());
//            }            
//            rotatedMaskImage.decrementReferenceCount();
//            rotatedMaskImageForTemplateMatching.decrementReferenceCount();                      
//          }
//          else
//          {
//            createdMaskImage.removeSubtypeMaskImage(subtype.getLongName());
//            createdMaskImage.addSubtypeMaskImage(subtype.getLongName(), oriMaskImage.getBufferedImage());
//          }    
//          oriMaskImage.decrementReferenceCount();
//          oriMaskBufferedImage.flush();
//        }     
//        else
//        {
//          if (RegionOfInterest.createRegionFromImage(maskImageForTemplateMatching).fitsWithinImage(image))      
//            matchedLocationForMask = Filter.matchTemplate(image, maskImageForTemplateMatching, RegionOfInterest.createRegionFromImage(image), MatchTemplateTechniqueEnum.CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING, matchQuality);
//
//          offsetX = Math.abs(matchedLocationForMask.getX() - minX);
//          offsetY = Math.abs(matchedLocationForMask.getY() - minY);
//
//          matchedLocationForMask.setLocation(searchRegion.getMinX(), searchRegion.getMinY());
//        }
            
        // if offset is below certain value means that the matching is success, means the mask image rotation is match with component rotation
        if (offsetX < 10 && offsetY < 10)
        {        
          if (inspectRegionInt.getPads().size() > 1)
          {
            matchedLocationForMask.setLocation(searchRegion.getMinX(), searchRegion.getMinY());
          }
//          _maskImageRotationDegree = joint.getComponent().getDegreesRotationRelativeToBoard();         
        }
        // if offset is above certain value means that the matching is failed hence rather use the exact mask image location than the matching location
        else if (offsetX > 30 || offsetY > 30)
        {          
          if (inspectRegionInt.getPads().size() > 1 == false)
          {
//            java.awt.geom.Point2D xEdge = null;
//            java.awt.geom.Point2D yEdge = null;
//    
//            xEdge = (ImageFeatureExtraction.findHorizontalEdgeForMask(maskImageForTemplateMatching));
//            yEdge = (ImageFeatureExtraction.findVerticalEdgeForMask(maskImageForTemplateMatching));
//        
//            matchedLocationForMask.setLocation((int)yEdge.getX(), (int)xEdge.getX());

            matchedLocationForMask.setLocation(offset.getX(), offset.getY());
            
          }
          else
          {
            matchedLocationForMask.setLocation(searchRegion.getMinX(), searchRegion.getMinY());
          }
        }
                 
        if ((matchedLocationForMask.getX() == 0 || matchedLocationForMask.getY() == 0) && borderX != 0 && borderY != 0)
        {
          matchedLocationForMask.setLocation(searchRegion.getMinX(), searchRegion.getMinY());
          setMatchedLocationForMask(matchedLocationForMask, minX, minY);
        }
        else
          setMatchedLocationForMask(matchedLocationForMask, minX, minY);
         
        maskImage.flush();
        maskImageForTemplateMatching.decrementReferenceCount();

        //matchedLocation = matchedLocationForMask;
        
        matchedLocation = Filter.locateRectangle(
        image, searchRegionWithBorderInImage, effectiveRegionWidth, effectiveRegionHeight,
        borderX, borderY, true);
      }
    }
    else 
    {
      matchedLocation = Filter.locateRectangle(
      image, searchRegionWithBorderInImage, effectiveRegionWidth, effectiveRegionHeight,
      borderX, borderY, true);
    }

    // move the matched location to center on the search region for each direction
    if (borderX == 0)
      matchedLocation.setX(matchedLocation.getX() - (effectiveRegion.getWidth() - searchRegion.getWidth()) / 2);
    if (borderY == 0)
      matchedLocation.setY(matchedLocation.getY() - (effectiveRegion.getHeight() - searchRegion.getHeight()) / 2);

    RegionOfInterest returnRegion = new RegionOfInterest(region);
  
    effectiveRegion.setMinXY(matchedLocation.getX(), matchedLocation.getY());

    // Correct the located position.
    float effectiveShiftAlong = (Float)(joint.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ALONG)) / 100.f;
    float effectiveShiftAcross = (Float)(joint.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_EFFECTIVE_SHIFT_ACROSS)) / 100.f;

    effectiveRegion.translateAlongAcross((int)(effectiveShiftAlong*region.getLengthAlong()),
        (int)(effectiveShiftAcross*region.getLengthAcross()));

    returnRegion.setCenterXY(effectiveRegion.getCenterX(),effectiveRegion.getCenterY());

    // we have to check these boundaries after switching to the pad-sized region. The effective region could be smaller. In any case,
    // it is this region that is actually used. Let's make certain it fits in the image.

    if (returnRegion.getMinX() < 0 || returnRegion.getMinY() < 0)
    {
      returnRegion.translateXY(Math.max( -returnRegion.getMinX(), 0),
                           Math.max( -returnRegion.getMinY(), 0));
    }
    if (returnRegion.getMaxX() >= image.getWidth() || returnRegion.getMaxY() >= image.getHeight())
    {
      returnRegion.translateXY(Math.min(image.getWidth() - returnRegion.getMaxX() - 1, 0),
                               Math.min(image.getHeight() - returnRegion.getMaxY() - 1, 0));
    }
    listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(effectiveRegion, MeasurementRegionEnum.LOCATE_MATCH_REGION));
 
    return returnRegion;
 
  }

  /**
   * Locate a rectangular component (for example, a capacitor or resistor)
   *
   * @author Peter Esbensen
   */
  static public void locateRectangularComponent(ReconstructedImages reconstructedImages,
                                                SliceNameEnum sliceToUseForLocate,
                                                ComponentInspectionData componentInspectionData,
                                                Algorithm callingAlgorithm,
                                                boolean postDiagnostics)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceToUseForLocate);

    // Locate all the joints, and compute the average deviation.
    Image padImage;
    // XCR-2681, Resize feature bug on BGA ball diameter calculation
    boolean ableToPerformResize = AlgorithmUtil.isAlgorithmAbleToPerformResize(callingAlgorithm);
    if(ableToPerformResize)
      padImage = reconstructedSlice.getOrthogonalImage();
    else
      padImage = reconstructedSlice.getOrthogonalImageWithoutEnhanced();
      
    ReconstructionRegion inspectRegionInt = reconstructedImages.getReconstructionRegion();

    RegionOfInterest componentRegionOfInterest = componentInspectionData.getOrthogonalComponentRegionOfInterest(ableToPerformResize);
    // find the component
    RegionOfInterest expectedComponentLocation = componentRegionOfInterest;

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();

    List<DiagnosticInfo> listOfDiagnosticsToDisplay = new LinkedList<DiagnosticInfo>();

    // show the cad locations for these two pin devices
    {
      JointInspectionData padOneInspectionData = componentInspectionData.getPadOneJointInspectionData();
      MeasurementRegionDiagnosticInfo padOneDiagnosticInfo = new MeasurementRegionDiagnosticInfo(
          padOneInspectionData.getOrthogonalRegionOfInterestInPixels(ableToPerformResize), MeasurementRegionEnum.CAD_LOCATION_REGION);
      padOneDiagnosticInfo.setLabel(padOneInspectionData.getPad().getName());
      listOfDiagnosticsToDisplay.add(padOneDiagnosticInfo);

      JointInspectionData padTwoInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
      MeasurementRegionDiagnosticInfo padTwoDiagnosticInfo = new MeasurementRegionDiagnosticInfo(
          padTwoInspectionData.getOrthogonalRegionOfInterestInPixels(ableToPerformResize), MeasurementRegionEnum.CAD_LOCATION_REGION);
      padTwoDiagnosticInfo.setLabel(padTwoInspectionData.getPad().getName());
      listOfDiagnosticsToDisplay.add(padTwoDiagnosticInfo);

      MeasurementRegionDiagnosticInfo componentDiagnosticInfo = new MeasurementRegionDiagnosticInfo(expectedComponentLocation,
                                                                                                    MeasurementRegionEnum.CAD_LOCATION_REGION);
      componentDiagnosticInfo.setLabel(componentInspectionData.getComponent().getReferenceDesignator());
      listOfDiagnosticsToDisplay.add(componentDiagnosticInfo);
    }

    if (postDiagnostics)
    {
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages,
                                                sliceToUseForLocate,
                                                subtype,
                                                callingAlgorithm,
                                                listOfDiagnosticsToDisplay);
    }
    listOfDiagnosticsToDisplay.clear();

    // execute the locating routine.
    JointInspectionData joint = componentInspectionData.getPadOneJointInspectionData();
    RegionOfInterest locatedComponent = locateRegion(joint, padImage, expectedComponentLocation,
                        inspectRegionInt, sliceToUseForLocate, callingAlgorithm, listOfDiagnosticsToDisplay);

    // Display the diagnostics generated in the 'locateRegion' calls.
    if (postDiagnostics && (listOfDiagnosticsToDisplay.isEmpty() == false))
    {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
    }

    // draw the final regions and record the measurements
    ComponentInspectionResult result = componentInspectionData.getComponentInspectionResult();
    result.addMeasurement(new ComponentMeasurement(callingAlgorithm,
                                                   componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                                   MeasurementEnum.LOCATOR_X_LOCATION,
                                                   MeasurementUnitsEnum.PIXELS,
                                                   componentInspectionData.getComponent(),
                                                   sliceToUseForLocate,
                                                   locatedComponent.getCenterX()));
    result.addMeasurement(new ComponentMeasurement(callingAlgorithm,
                                                   componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                                   MeasurementEnum.LOCATOR_Y_LOCATION,
                                                   MeasurementUnitsEnum.PIXELS,
                                                   componentInspectionData.getComponent(),
                                                   sliceToUseForLocate,
                                                   locatedComponent.getCenterY()));
    
    //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
    result.addMeasurement(new ComponentMeasurement(callingAlgorithm,
                                                   componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                                   MeasurementEnum.IMAGE_RESIZE_SCALE,
                                                   MeasurementUnitsEnum.NONE,
                                                   componentInspectionData.getComponent(),
                                                   sliceToUseForLocate,
                                                   (float) AlgorithmUtil.getResizeFactor(subtype)));

    if(ableToPerformResize == false)
    {
      //XCR-3014, now save the location to inspection result to reuse in short
      result.setOrigImageLocation(new ImageCoordinate(locatedComponent.getCenterX(), locatedComponent.getCenterY()));
    }
    if (postDiagnostics)
    {
      MeasurementRegionDiagnosticInfo regionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(locatedComponent, MeasurementRegionEnum.PAD_REGION);

      //Post pad location
      _diagnostics.postDiagnostics(inspectRegionInt,
                                   sliceToUseForLocate,
                                   componentInspectionData.getPadOneJointInspectionData(),
                                   callingAlgorithm, false,
                                   regionDiagnosticInfo);
    }
  }

  /**
   * Match locations for all the indicated joints. If any vary more than the snap back setting from the average deviation,
   * move them to the average deviated position.
   *
   * @author Patrick Lacz
   * @author Matt Wharton
   * @author Lim, Lay Ngor - add parameter to enable or disable postDiagnostic.
   */
  static public void locateJoints(ReconstructedImages reconstructedImages,
                                  SliceNameEnum sliceToUseForLocate,
                                  List<JointInspectionData> jointInspectionDataObjects, Algorithm callingAlgorithm,
                                  boolean postDiagnostics)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    List<DiagnosticInfo> listOfDiagnosticsToDisplay = new ArrayList<DiagnosticInfo>();
    List<DiagnosticInfo> cadDiagnosticInfoList = new LinkedList<DiagnosticInfo>();

    int numberOfJoints = jointInspectionDataObjects.size();
    List<RegionOfInterest> resultLocations = new ArrayList<RegionOfInterest>(numberOfJoints);
    List<ImageCoordinate> deviationList = new ArrayList<ImageCoordinate>(numberOfJoints);

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceToUseForLocate);

    // Locate all the joints, and compute the average deviation.
    // XCR-2681, Resize feature bug on BGA ball diameter calculation
    boolean ableToPerformResize = AlgorithmUtil.isAlgorithmAbleToPerformResize(callingAlgorithm);
    Image surfaceImage;
    if(ableToPerformResize)
      surfaceImage = reconstructedSlice.getOrthogonalImage();
    else
      surfaceImage = reconstructedSlice.getOrthogonalImageWithoutEnhanced();
      
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    ReconstructionRegion inspectRegionInt = reconstructedImages.getReconstructionRegion();
    MeasurementRegionEnum measurementRegionForLocationRegion = MeasurementRegionEnum.PAD_REGION;
    
    //Siew Yeng - XCR-2843 - masking rotation
    boolean usingMasking = AlgorithmUtil.isSubtypeUsingMaskingMethod(subtype);

    if (usingMasking)
    {
      createCropMaskImage(subtype, null, surfaceImage);
    }
    
    //Siew Yeng - XCR-3545 - subregion mask image not rotating
    if(subtype.hasSubSubtypes())
    {
      for(Subtype subSubtype : subtype.getSubSubtypes())
      {
        //Siew Yeng - skip subregion if it is disabled
        if(subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION).equals(_FALSE))
          continue;
        
        boolean subSubtypeUsingMasking = AlgorithmUtil.isSubtypeUsingMaskingMethod(subSubtype);

        if(subSubtypeUsingMasking)
        {
          createCropMaskImage(subSubtype, subtype, surfaceImage);
        }
        else
        {
          // if parent subtype use masking, but sub-subtype does not have mask image, auto use parent's mask
          if(usingMasking)
          {
            double rotation = MaskImage.getInstance().getSubtypeMaskImageOriginalRotation(subtype.getLongName());
            
            //Siew Yeng - XCR-3708 - production looping crash due to single pad masking
            MaskImage.getInstance().setSubtypeMaskImageOriginalRotation(subSubtype.getLongName(), rotation);
            MaskImage.getInstance().addSubtypeMaskImage(subSubtype.getLongName(), rotation, MaskImage.getInstance().getSubtypeMaskImage(subtype.getLongName(), rotation));
            MaskImage.getInstance().addCropMaskSubtypeImage(subSubtype.getLongName(), rotation, MaskImage.getInstance().getCropMaskImage(subtype.getLongName(), rotation));
          }
        }
      }
    }
    
    // Iterate over all the joints to find the located position.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // find the joint
      RegionOfInterest expectedJointLocation = jointInspectionData.getOrthogonalRegionOfInterestInPixels(ableToPerformResize);

      // For Throughhole joints, use the barrel as the 'joint'.
      LandPatternPad landPatternPad = jointInspectionData.getPad().getLandPatternPad();
      if (landPatternPad.isThroughHolePad())
      {
        expectedJointLocation = AlgorithmUtil.getRegionOfInterestOfThroughholeBarrel(jointInspectionData, ableToPerformResize);
        measurementRegionForLocationRegion = MeasurementRegionEnum.BARREL_REGION;
      }

      MeasurementRegionDiagnosticInfo cadDiagnostic = new MeasurementRegionDiagnosticInfo(expectedJointLocation, MeasurementRegionEnum.CAD_LOCATION_REGION);
      cadDiagnostic.setLabel(jointInspectionData.getPad().getName());
      cadDiagnosticInfoList.add(cadDiagnostic);

      // execute the locating routine.
      RegionOfInterest locatedJoint = Locator.locateRegion(
          jointInspectionData,
          surfaceImage,
          expectedJointLocation,
          inspectRegionInt,
          sliceToUseForLocate,
          callingAlgorithm,
          listOfDiagnosticsToDisplay);
      resultLocations.add(locatedJoint);

      // keep track of the deviation from the expected location
      ImageCoordinate deviation = new ImageCoordinate(locatedJoint.getMinX()-expectedJointLocation.getMinX(),
                        locatedJoint.getMinY()-expectedJointLocation.getMinY());
      deviationList.add(deviation);     

      if (usingMasking)
      {
       // if (deviation.getX() > 5.0 || deviation.getY() > 5.0 ||deviation.getX() < -5.0 || deviation.getY() < -5.0)
        applySnapBackForMaskImage(locatedJoint, jointInspectionData, subtype);
      }   
     
      //Siew Yeng - XCR-2764
      if(subtype.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtype.getSubSubtypes())
        {
          //Siew Yeng - XCR-3708 - skip subregion if it is disabled
          if(subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_VOIDING_ENABLE_SUBREGION).equals(_FALSE))
            continue;
          
          //boolean subSubtypeUsingMasking = Boolean.parseBoolean((String)subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES));
          boolean subSubtypeUsingMasking = AlgorithmUtil.isSubtypeUsingMaskingMethod(subSubtype);

          if(subSubtypeUsingMasking)
          {
            //Siew Yeng - XCR-3653 - subregion masking not working if parent subtype does not use masking
            double originalRotation = MaskImage.getInstance().getSubtypeMaskImageOriginalRotation(subSubtype.getLongName());
            double rotation = jointInspectionData.getComponent().getDegreesRotationAfterAllRotations();
            
            //Siew Yeng - XCR-3708 - production looping crash due to single pad masking
            if(rotation != originalRotation && 
              (MaskImage.getInstance().getSubtypeMaskImage(subSubtype.getLongName(), rotation) == null || 
               MaskImage.getInstance().getCropMaskImage(subSubtype.getLongName(), rotation) == null))
            {
              double correctedRotation = jointInspectionData.getInspectionRegion().getRotationCorrectionInDegrees();//handle custom rotation
              //XCR-3545 - subregion masking not rotating
              handleMaskRotation(MaskImage.getInstance(), subSubtype, surfaceImage.getWidth(), surfaceImage.getHeight(), originalRotation, rotation, correctedRotation);
            }
            
            //if parent subtype using masking, apply snapback to subSubtype also
            if(usingMasking)
              applySnapBackForMaskImage(locatedJoint, jointInspectionData, subSubtype);
          }
          else
          {
            // if parent subtype use masking, but sub-subtype does not have mask image, auto use parent's mask
            if(usingMasking)
            {
              //Siew Yeng - XCR-3653 - subregion should follow parent subtype masking if subregion does not use masking
              double rotation = jointInspectionData.getComponent().getDegreesRotationAfterAllRotations();
              
              if(MaskImage.getInstance().getSubtypeMaskImage(subSubtype.getLongName(), rotation) == null)
              {
                MaskImage.getInstance().addSubtypeMaskImage(subSubtype.getLongName(), rotation, MaskImage.getInstance().getSubtypeMaskImage(subtype.getLongName(), rotation));
                MaskImage.getInstance().addCropMaskSubtypeImage(subSubtype.getLongName(), rotation, MaskImage.getInstance().getCropMaskImage(subtype.getLongName(), rotation));
              }
              //Siew Yeng - XCR-3708 - production looping crash due to single pad masking
              MaskImage.getInstance().addMaskImageXCoordinate(jointInspectionData, subSubtype.getLongName(), MaskImage.getInstance().getSubtypeMaskImageXCoordinate(jointInspectionData, subtype.getLongName()));
              MaskImage.getInstance().addMaskImageYCoordinate(jointInspectionData, subSubtype.getLongName(), MaskImage.getInstance().getSubtypeMaskImageYCoordinate(jointInspectionData, subtype.getLongName()));
              MaskImage.getInstance().addMisalignedMaskSubtypeImage(jointInspectionData, subtype.getLongName(), MaskImage.getInstance().getMisalignedSubtypeMaskImage(jointInspectionData, subtype.getLongName()));
            }
          }
        }
      }
    }
   
    if(postDiagnostics)
    {
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages,
                                                sliceToUseForLocate,
                                                subtype,
                                                callingAlgorithm,
                                                cadDiagnosticInfoList);
    }

    // Display the diagnostics generated in the 'locateRegion' calls.
    if (postDiagnostics && listOfDiagnosticsToDisplay.isEmpty() == false)
    {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
      listOfDiagnosticsToDisplay.clear();
    }

    // determine which joints to 'snap back' to the consensus location.

    // build an array that StatisticsUtil can handle
    float deviationArray[][] = new float[numberOfJoints][2];
    int j = 0;
    for (ImageCoordinate deviation : deviationList)
    {
      deviationArray[j][0] = deviation.getX();
      deviationArray[j++][1] = deviation.getY();
    }
    
    float snapBackDistance = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE);
    float millimetersPerPixel = MathUtil.convertNanometersToMillimeters( MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel() );
    snapBackDistance /= millimetersPerPixel;

    Set<Integer> goodJoints = StatisticsUtil.convergeOnValidSet(deviationArray, StatisticsNormEnum.EUCLIDIAN, (float)snapBackDistance);

    if (usingMasking == false)
      applySnapback(resultLocations, deviationArray, goodJoints);
    
    // draw the final regions and record the measurements
    int i = 0;
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult result = jointInspectionData.getJointInspectionResult();
      Assert.expect(i < resultLocations.size());
      RegionOfInterest jointRoi = resultLocations.get(i++);
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_X_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterX()));
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_Y_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterY()));
      
      //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.IMAGE_RESIZE_SCALE,
                                                 MeasurementUnitsEnum.NONE,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 (float) AlgorithmUtil.getResizeFactor(subtype)));
      
      if(ableToPerformResize == false)
      {
        //XCR-3014, now save the location to inspection result to reuse in short
        result.setOrigImageLocation(new ImageCoordinate(jointRoi.getCenterX(), jointRoi.getCenterY()));
      }
      
      listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(jointRoi, measurementRegionForLocationRegion));
    }

    // display all the dignostics generated in the loop above - the final measured locations of the joints.
    if (postDiagnostics && listOfDiagnosticsToDisplay.isEmpty() == false)
    {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
    }
  }
  
  /**
   * Match locations for all the indicated joints. If any vary more than the snap back setting from the average deviation,
   * move them to the average deviated position.
   *
   * @author Patrick Lacz
   * @author Matt Wharton
   * @author Lim, Lay Ngor - duplicate locateJoints, now only locate single joint.
   */
 /* static public RegionOfInterest locateSingleJoint(ReconstructedImages reconstructedImages,
                                  SliceNameEnum sliceToUseForLocate,
                                  JointInspectionData jointInspectionData, Algorithm callingAlgorithm,
                                  boolean postDiagnostics)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    List<DiagnosticInfo> listOfDiagnosticsToDisplay = new ArrayList<DiagnosticInfo>();
    List<DiagnosticInfo> cadDiagnosticInfoList = new LinkedList<DiagnosticInfo>();

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceToUseForLocate);

    // Locate all the joints, and compute the average deviation.
    Image surfaceImage = reconstructedSlice.getOrthogonalImage();
    Subtype subtype = jointInspectionData.getSubtype();

    ReconstructionRegion inspectRegionInt = reconstructedImages.getReconstructionRegion();
    MeasurementRegionEnum measurementRegionForLocationRegion = MeasurementRegionEnum.PAD_REGION;
    
    String usingMasking = _FALSE;
    if ((subtype.getJointTypeEnum().equals(JointTypeEnum.SINGLE_PAD)) && subtype.getEnabledAlgorithmEnums().contains(AlgorithmEnum.VOIDING))
    {
      usingMasking = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES);
    }

    if (usingMasking.equals(_TRUE))
    {
      String maskImagePath = Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()) + java.io.File.separator + "mask#" + subtype.getShortName() + ".png";

      if (FileUtil.exists(maskImagePath))
      {
        usingMasking = (String) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES);
      }
      else
      {
        LocalizedString warningText = new LocalizedString("ALGDIAG_MASKING_IMAGE_NOT_AVAILABLE_KEY",
          new Object[]
          {
            subtype.getShortName()
          });

        AlgorithmUtil.raiseAlgorithmWarning(warningText);

        usingMasking = _FALSE;
      }
    }

    if (usingMasking.equals(_TRUE))
    {
      createCropMaskImage(subtype);
      _maskImageRotationDegree = -1.0;
    }
    
    // find the joint located position.
    RegionOfInterest expectedJointLocation = jointInspectionData.getOrthogonalRegionOfInterestInPixels();

    // For Throughhole joints, use the barrel as the 'joint'.
    LandPatternPad landPatternPad = jointInspectionData.getPad().getLandPatternPad();
    if (landPatternPad.isThroughHolePad())
    {
      expectedJointLocation = AlgorithmUtil.getRegionOfInterestOfThroughholeBarrel(jointInspectionData);
      measurementRegionForLocationRegion = MeasurementRegionEnum.BARREL_REGION;
    }

    MeasurementRegionDiagnosticInfo cadDiagnostic = new MeasurementRegionDiagnosticInfo(expectedJointLocation, MeasurementRegionEnum.CAD_LOCATION_REGION);
    cadDiagnostic.setLabel(jointInspectionData.getPad().getName());
    cadDiagnosticInfoList.add(cadDiagnostic);

    // execute the locating routine.
    RegionOfInterest locatedJoint = Locator.locateRegion(
      jointInspectionData,
      surfaceImage,
      expectedJointLocation,
      inspectRegionInt,
      sliceToUseForLocate,
      callingAlgorithm,
      listOfDiagnosticsToDisplay);

    // keep track of the deviation from the expected location
    ImageCoordinate deviation = new ImageCoordinate(locatedJoint.getMinX() - expectedJointLocation.getMinX(),
      locatedJoint.getMinY() - expectedJointLocation.getMinY());

    if (usingMasking.equals(_TRUE))
    {
      // if (deviation.getX() > 5.0 || deviation.getY() > 5.0 ||deviation.getX() < -5.0 || deviation.getY() < -5.0)
      applySnapBackForMaskImage(locatedJoint, jointInspectionData);
    }    

    
   if(postDiagnostics)
   {
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages,
                                                sliceToUseForLocate,
                                                subtype,
                                                callingAlgorithm,
                                                cadDiagnosticInfoList);
   }

   // Display the diagnostics generated in the 'locateRegion' calls.
   if (postDiagnostics && listOfDiagnosticsToDisplay.isEmpty() == false)
   {
        _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                     callingAlgorithm,
                                     false, false,
                                     listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
        listOfDiagnosticsToDisplay.clear();
   }

    // determine which joints to 'snap back' to the consensus location.

    // build an array that StatisticsUtil can handle
    float deviationArray[] = new float[2];
      deviationArray[0] = deviation.getX();
      deviationArray[1] = deviation.getY();
    
    float snapBackDistance = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE);
    float millimetersPerPixel = MathUtil.convertNanometersToMillimeters( MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel() );
    snapBackDistance /= millimetersPerPixel;

    Set<Integer> goodJoints = StatisticsUtil.convergeOnValidSet(deviationArray, StatisticsNormEnum.EUCLIDIAN, (float)snapBackDistance);

//    if (usingMasking.equals(_FALSE))
//       applySnapback(resultLocations, deviationArray, goodJoints);
    
      JointInspectionResult result = jointInspectionData.getJointInspectionResult();
      RegionOfInterest jointRoi = locatedJoint;
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_X_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterX()));
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_Y_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterY()));

      listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(jointRoi, measurementRegionForLocationRegion));

    // display all the dignostics generated in the loop above - the final measured locations of the joints.
    if (postDiagnostics && listOfDiagnosticsToDisplay.isEmpty() == false)
    {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
    }
    return locatedJoint;
  }  
  */
  
   /**
   * Match locations for all the indicated joints. If any vary more than the snap back setting from the average deviation,
   * move them to the average deviated position.
   *
   * @author Patrick Lacz
   * @author Matt Wharton
   * @author Lim, Lay Ngor - duplicate locateJoints, now only locate single joint.
   */
  static public RegionOfInterest locateSingleJoint(ReconstructedImages reconstructedImages,
                                  SliceNameEnum sliceToUseForLocate,
                                  JointInspectionData jointInspectionData, Algorithm callingAlgorithm,
                                  boolean postDiagnostics)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    List<DiagnosticInfo> listOfDiagnosticsToDisplay = new ArrayList<DiagnosticInfo>();
    List<DiagnosticInfo> cadDiagnosticInfoList = new LinkedList<DiagnosticInfo>();

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceToUseForLocate);

    // Locate all the joints, and compute the average deviation.
    // XCR-2681, Resize feature bug on BGA ball diameter calculation
    boolean ableToPerformResize = AlgorithmUtil.isAlgorithmAbleToPerformResize(callingAlgorithm);
    // Locate all the joints, and compute the average deviation.
    Image surfaceImage = null;
    
    if(ableToPerformResize)
      surfaceImage = reconstructedSlice.getOrthogonalImage();
    else
      surfaceImage = reconstructedSlice.getOrthogonalImageWithoutEnhanced();
        
    Subtype subtype = jointInspectionData.getSubtype();

    ReconstructionRegion inspectRegionInt = reconstructedImages.getReconstructionRegion();
    MeasurementRegionEnum measurementRegionForLocationRegion = MeasurementRegionEnum.PAD_REGION;
    
    //Siew Yeng - XCR-2843 - masking rotation
    boolean usingMasking = AlgorithmUtil.isSubtypeUsingMaskingMethod(subtype);
    
    // find the joint located position.
    RegionOfInterest expectedJointLocation = jointInspectionData.getOrthogonalRegionOfInterestInPixels(ableToPerformResize);

    // For Throughhole joints, use the barrel as the 'joint'.
    LandPatternPad landPatternPad = jointInspectionData.getPad().getLandPatternPad();
    if (landPatternPad.isThroughHolePad())
    {
      expectedJointLocation = AlgorithmUtil.getRegionOfInterestOfThroughholeBarrel(jointInspectionData, ableToPerformResize);
      measurementRegionForLocationRegion = MeasurementRegionEnum.BARREL_REGION;
    }

    MeasurementRegionDiagnosticInfo cadDiagnostic = new MeasurementRegionDiagnosticInfo(expectedJointLocation, MeasurementRegionEnum.CAD_LOCATION_REGION);
    cadDiagnostic.setLabel(jointInspectionData.getPad().getName());
    cadDiagnosticInfoList.add(cadDiagnostic);

    // execute the locating routine.
    RegionOfInterest locatedJoint = Locator.locateRegion(
      jointInspectionData,
      surfaceImage,
      expectedJointLocation,
      inspectRegionInt,
      sliceToUseForLocate,
      callingAlgorithm,
      listOfDiagnosticsToDisplay);

      // keep track of the deviation from the expected location
      ImageCoordinate deviation = new ImageCoordinate(locatedJoint.getMinX()-expectedJointLocation.getMinX(),
                        locatedJoint.getMinY()-expectedJointLocation.getMinY());

      if (usingMasking)
      {
       // if (deviation.getX() > 5.0 || deviation.getY() > 5.0 ||deviation.getX() < -5.0 || deviation.getY() < -5.0)
        applySnapBackForMaskImage(locatedJoint, jointInspectionData, subtype);
      }   
     
      //Siew Yeng - XCR-2764
      if(subtype.hasSubSubtypes())
      {
        for(Subtype subSubtype : subtype.getSubSubtypes())
        {
          //boolean subSubtypeUsingMasking = Boolean.parseBoolean((String)subSubtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LARGE_PAD_USING_MASKING_CHOICES));
          boolean subSubtypeUsingMasking = AlgorithmUtil.isSubtypeUsingMaskingMethod(subSubtype);

          if(subSubtypeUsingMasking)
          {
            createCropMaskImage(subSubtype, subtype, surfaceImage);

            //if parent subtype using masking, apply snapback to subSubtype also
            if(usingMasking)
              applySnapBackForMaskImage(locatedJoint, jointInspectionData, subSubtype);
          }
          else
          {
            // if parent subtype use masking, but sub-subtype does not have mask image, auto use parent's mask
            if(usingMasking)
            {
              double rotation = MaskImage.getInstance().getSubtypeMaskImageOriginalRotation(subtype.getLongName());

              MaskImage.getInstance().addSubtypeMaskImage(subSubtype.getLongName(), rotation, MaskImage.getInstance().getSubtypeMaskImage(subtype.getLongName(), rotation));
              MaskImage.getInstance().addCropMaskSubtypeImage(subSubtype.getLongName(), rotation, MaskImage.getInstance().getCropMaskImage(subtype.getLongName(), rotation));
              MaskImage.getInstance().addMaskImageXCoordinate(jointInspectionData, subSubtype.getLongName(), MaskImage.getInstance().getSubtypeMaskImageXCoordinate(jointInspectionData, subtype.getLongName()));
              MaskImage.getInstance().addMaskImageYCoordinate(jointInspectionData, subSubtype.getLongName(), MaskImage.getInstance().getSubtypeMaskImageYCoordinate(jointInspectionData, subtype.getLongName()));
            }
          }
        }
      }
    
   
   if(postDiagnostics)
   {
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages,
                                                sliceToUseForLocate,
                                                subtype,
                                                callingAlgorithm,
                                                cadDiagnosticInfoList);
   }

   // Display the diagnostics generated in the 'locateRegion' calls.
   if (postDiagnostics && listOfDiagnosticsToDisplay.isEmpty() == false)
   {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
      listOfDiagnosticsToDisplay.clear();
   }

    // determine which joints to 'snap back' to the consensus location.

    // build an array that StatisticsUtil can handle
    float deviationArray[] = new float[2];
      deviationArray[0] = deviation.getX();
      deviationArray[1] = deviation.getY();

    float snapBackDistance = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE);
    float millimetersPerPixel = MathUtil.convertNanometersToMillimeters( MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel() );
    snapBackDistance /= millimetersPerPixel;

    Set<Integer> goodJoints = StatisticsUtil.convergeOnValidSet(deviationArray, StatisticsNormEnum.EUCLIDIAN, (float)snapBackDistance);

//    if (usingMasking.equals(_FALSE))
//       applySnapback(resultLocations, deviationArray, goodJoints);
    
      JointInspectionResult result = jointInspectionData.getJointInspectionResult();
      RegionOfInterest jointRoi = locatedJoint;
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_X_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterX()));
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_Y_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterY()));
      
      //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.IMAGE_RESIZE_SCALE,
                                                 MeasurementUnitsEnum.NONE,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 (float) AlgorithmUtil.getResizeFactor(subtype)));

      listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(jointRoi, measurementRegionForLocationRegion));

    // display all the dignostics generated in the loop above - the final measured locations of the joints.
    if (postDiagnostics && listOfDiagnosticsToDisplay.isEmpty() == false)
    {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
    }
    return locatedJoint;
  } 

  /**
   * @author Patrick Lacz
   */
  protected static void applySnapback(List<RegionOfInterest> resultLocations,
      float[][] deviationArray, Set<Integer> goodJoints)
  {
    Assert.expect(resultLocations != null);
    Assert.expect(deviationArray != null);
    Assert.expect(goodJoints != null);
    Assert.expect(resultLocations.size() == deviationArray.length);

    if (deviationArray.length <= 1)
        return;

    int medianDx, medianDy;
    if (goodJoints.size() > 1)
    {
      // Compute the median deviation of the good joints.

      float deviationArrayOfGoodJoints[][] = new float[goodJoints.size()][2];
      int i = 0;
      for (Integer joint : goodJoints)
      {
        deviationArrayOfGoodJoints[i][0] = deviationArray[joint][0];
        deviationArrayOfGoodJoints[i][1] = deviationArray[joint][1];
        ++i;
      }

      float medianDeviationOfGoodJoints[] = StatisticsUtil.median(deviationArrayOfGoodJoints);
      medianDx = (int)Math.round(medianDeviationOfGoodJoints[0]);
      medianDy = (int)Math.round(medianDeviationOfGoodJoints[1]);
      Assert.expect(medianDeviationOfGoodJoints.length == 2);
    }
    else
    {
      float medianDeviationOfAllJoints[] = StatisticsUtil.median(deviationArray);
      medianDx = (int)Math.round(medianDeviationOfAllJoints[0]);
      medianDy = (int)Math.round(medianDeviationOfAllJoints[1]);
      Assert.expect(medianDeviationOfAllJoints.length == 2);
    }

    // apply snapback to all the bad joints
    for (int i = 0; i < resultLocations.size(); ++i)
    {
      RegionOfInterest resultLocation = resultLocations.get(i);

      boolean isGoodJoint = goodJoints.contains(i);
      if (!isGoodJoint)
      {
        resultLocation.translateXY( (int)Math.round(-deviationArray[i][0]), (int)Math.round(-deviationArray[i][1]));
        resultLocation.translateXY(medianDx, medianDy);
      }
    }
  }

  /**
   * Computes a reasonable value for Search Window.
   *
   * @author Patrick Lacz
   */
  static public void learn(Subtype subtype,
                           Algorithm algorithm,
                           ManagedOfflineImageSet typicalBoardImages,
                           ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(algorithm != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    ManagedOfflineImageSet imagesToUse = typicalBoardImages;
    if (imagesToUse.getReconstructionRegions().isEmpty())
      imagesToUse = unloadedBoardImages;
    if (imagesToUse.getReconstructionRegions().isEmpty())
      return; // cannot learn.
    ReconstructionRegion region = imagesToUse.getReconstructionRegions().iterator().next();

    // XCR-2681, Resize feature bug on BGA ball diameter calculation
    boolean ableToPerformResize = AlgorithmUtil.isAlgorithmAbleToPerformResize(algorithm);
    float minSizeAcross = Float.MAX_VALUE;
    float minSizeAlong = Float.MAX_VALUE;

    for (JointInspectionData jointInspectionData : region.getInspectableJointInspectionDataList(subtype))
    {
      // @todo : what to do about rotated joints? this function requries that the rotated images have been generated (perhaps that should be changed)
      // RegionOfInterest roi = reconstructedImagesToUse.getRegionOfInterestForJoint(jointInspectionData);
      RegionOfInterest roi = jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(ableToPerformResize);

      // For Throughhole joints, use the barrel as the 'joint'.
      LandPatternPad landPatternPad = jointInspectionData.getPad().getLandPatternPad();
      if (landPatternPad.isThroughHolePad())
      {
        roi = AlgorithmUtil.getRegionOfInterestOfThroughholeBarrel(jointInspectionData, ableToPerformResize);
      }

      int pixelsAcross = roi.getLengthAcross();
      int pixelsAlong = roi.getLengthAlong();

      boolean isSpecialTwoPinDevice = InspectionFamily.isSpecialTwoPinDevice(subtype.getInspectionFamily().getInspectionFamilyEnum());
      if (isSpecialTwoPinDevice)
      {
        RegionOfInterest componentRoi = jointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest(ableToPerformResize);
        pixelsAcross = componentRoi.getLengthAcross();
        pixelsAlong = componentRoi.getLengthAlong();
      }

      // This equation gives us twice a fraction of IPD, but that doesn't include the
      // area of the pad itself, hence the + 1.f at the end of each equation.
      // XCR-3152 Search Along Measurement Value Smaller Than Effective Length After Initial Learning
      int algorithmIPDInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels(ableToPerformResize);
      float totalWidthInPixels = 2.f * _LEARN_SEARCH_WINDOW_AT_FRACTION_OF_IPD * algorithmIPDInPixels;
      float relativeSizeAcross = totalWidthInPixels / pixelsAcross + 1.f;
      float relativeSizeAlong = totalWidthInPixels / pixelsAlong + 1.f;

      // Figure out the search distance across
      float standardSearchDistanceAcrossInPixels = relativeSizeAcross * pixelsAcross;

      // make sure it can search at least 5 mils on each side to handle alignment slop
      final float ALIGNMENT_SLOP_IN_MILS = 10f;
      int alignmentSlopInNanometers = (int)MathUtil.convertMilsToNanoMeters(ALIGNMENT_SLOP_IN_MILS);
      int nanometersPerPixel;
      if(ableToPerformResize)
        nanometersPerPixel = (int)AlgorithmUtil.getNanometersPerPixel(subtype);
      else
        nanometersPerPixel = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
      int alignmentSlopInPixels = MathUtil.convertNanoMetersToPixelsUsingRound(alignmentSlopInNanometers, nanometersPerPixel);
      float searchDistanceAcrossToHandleAlignmentSlopInPixels = Math.max(standardSearchDistanceAcrossInPixels,
                                                                         alignmentSlopInPixels + pixelsAcross);

      // Now, make sure the final value isn't more than 90 percent of the width across plus twice the IPD to avoid locking onto the wrong pad
      // (Only limit it if it ISN'T a special two-pin device . . . those often have wonky pitches)
      if (isSpecialTwoPinDevice == false)
      {
        float interPadDistanceInPixels = jointInspectionData.getInterPadDistanceInPixels(ableToPerformResize);
        searchDistanceAcrossToHandleAlignmentSlopInPixels = Math.min( searchDistanceAcrossToHandleAlignmentSlopInPixels,
                                                                      (pixelsAcross + (2 * interPadDistanceInPixels)) * 0.90f );
      }
      relativeSizeAcross = searchDistanceAcrossToHandleAlignmentSlopInPixels / pixelsAcross;

      if (minSizeAcross > relativeSizeAcross)
        minSizeAcross = relativeSizeAcross;
      if (minSizeAlong > relativeSizeAlong)
        minSizeAlong = relativeSizeAlong;
    }

    if (subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.GULLWING) ||
        subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.ADVANCED_GULLWING) ||
        subtype.getInspectionFamilyEnum().equals(InspectionFamilyEnum.QUAD_FLAT_NO_LEAD))
    {
      // Make sure the search window along settings doesn't get set above 120%.
      final float MAX_ALLOWABLE_SEARCH_WINDOW_ALONG_SIZE_FRACTION = 1.2f;
      minSizeAlong = Math.min(minSizeAlong, MAX_ALLOWABLE_SEARCH_WINDOW_ALONG_SIZE_FRACTION);
    }
        
    subtype.setLearnedValue(subtype.getJointTypeEnum(), algorithm, AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS, 100.f * minSizeAcross);
    subtype.setLearnedValue(subtype.getJointTypeEnum(), algorithm, AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG, 100.f * minSizeAlong);
  }

  /**
   * @author Patrick Lacz
   */
  static public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Assert.expect(subtype != null);

    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS);
    subtype.setSettingToDefaultValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG);
  }
  
   /**
   * @author Jack Hwee
   */
  static private void setMatchedLocationForMask(ImageCoordinate imageCoordinate, int xOffset, int yOffset)
  {
    Assert.expect(imageCoordinate != null);
    Assert.expect(xOffset >= 0);
    Assert.expect(yOffset >= 0);
    
    int newX = (imageCoordinate.getX() - xOffset - 1);
    int newY = (imageCoordinate.getY() - yOffset - 1);

    _imageCoordinateForMasking = new ImageCoordinate(0,0);
    _imageCoordinateForMasking.setLocation(newX, newY);
  }
  
  /**
   * @author Jack Hwee
   */
  static private ImageCoordinate getMatchedLocationForMask()
  {
    return _imageCoordinateForMasking;
  }
  
  /**
   * Siew Yeng - XCR-3545 - move this function to MaskImage.java
   * @author Jack Hwee
   */
//  static public void setOffsetValueForMask(int x, int y)
//  {
//    _offsetForMasking = new ImageCoordinate(0,0);
//    _offsetForMasking.setLocation(x, y);
//  }
  
  /**
   * Siew Yeng - XCR-3545 - move this function to MaskImage.java
   * @author Jack Hwee
   */
//  static public ImageCoordinate getOffsetValueForMask()
//  {
//    return _offsetForMasking;
//  }
  
  /**
   * @author Jack Hwee
   * @edited by Siew Yeng - pass in subtype to support sub-subtype feature
   */
  protected static void applySnapBackForMaskImage(RegionOfInterest locatedLocation, JointInspectionData jointInspectionData, Subtype subtype)
  {
    MaskImage createMaskImage = MaskImage.getInstance();
    java.awt.image.BufferedImage maskImage = createMaskImage.getSubtypeMaskImage(subtype.getLongName(), jointInspectionData.getComponent().getDegreesRotationAfterAllRotations());

    if(maskImage != null)
    {
      int x = getMatchedLocationForMask().getX();
      int y = getMatchedLocationForMask().getY();

      int width = maskImage.getWidth();
      int height = maskImage.getHeight();

      float[] white = new float[width * height];
      Arrays.fill(white, 255.0f);

      Image newMaskImage = Image.createFloatImageFromArray(width, height, white);

      Image maskImageForResize = Image.createFloatImageFromBufferedImage(maskImage);
      Transform.copyImageIntoImageWithResize(newMaskImage, maskImageForResize, x, y, width, height);

      Threshold.threshold(newMaskImage, 0.0f, 0.0f, 1.0f, 255.0f);

      java.awt.geom.Point2D xEdge = (ImageFeatureExtraction.findHorizontalEdgeForMask(newMaskImage));
      java.awt.geom.Point2D yEdge = (ImageFeatureExtraction.findVerticalEdgeForMask(newMaskImage));

      ImageCoordinate maskCoordinateX = new ImageCoordinate(0, 0);
      ImageCoordinate maskCoordinateY = new ImageCoordinate(0, 0);

      maskCoordinateX.setLocation((int) yEdge.getY(), (int) yEdge.getX());
      maskCoordinateY.setLocation((int) xEdge.getY(), (int) xEdge.getX());
      
      createMaskImage.addMaskImageXCoordinate(jointInspectionData, subtype.getLongName(), maskCoordinateX);
      createMaskImage.addMaskImageYCoordinate(jointInspectionData, subtype.getLongName(), maskCoordinateY);
      createMaskImage.addMisalignedMaskSubtypeImage(jointInspectionData, subtype.getLongName(), newMaskImage.getBufferedImage());

      newMaskImage.decrementReferenceCount();

      maskImage.flush();
      maskImageForResize.decrementReferenceCount();
    }
  }
  
  /**
   * @author Jack Hwee
   * @edited by Siew Yeng - add parameter parentSubtype to support sub-subtype feature
   *                      - add parameter sliceImage to check mask image size with slice image
   */
  protected static void createCropMaskImage(Subtype subtype, Subtype parentSubtype, Image sliceImage)
  {   
    //Siew Yeng - XCR-2843 - masking rotation
    List<String> maskImagePathList = FileUtil.listFiles(Directory.getAlgorithmLearningDir(subtype.getPanel().getProject().getName()), 
                                                        FileName.getMaskImagePatternString(subtype.getShortName()));
   
    if (maskImagePathList.isEmpty() == false)
    {
      //each subtype/subSubtype will only have have one mask, always get the 1st and only one from the list
      String maskImagePath = maskImagePathList.get(0);
      String rotationString = "";
      
      try
      {
        //get image rotation from mask image name
        rotationString = maskImagePath.substring(maskImagePath.lastIndexOf(FileName.getMaskImageRotationSeparator()) + 1, maskImagePath.length() - FileName.getMaskImageExtension().length());
        double rotation = Double.parseDouble(rotationString);
        
        MaskImage createdMaskImage = MaskImage.getInstance();
        //Siew Yeng - if crop mask already exist skip this step
        if(createdMaskImage.getCropMaskImage(subtype.getLongName(), rotation) != null)
          return;
        
        //store the mask image rotation from image name into map
        createdMaskImage.setSubtypeMaskImageOriginalRotation(subtype.getLongName(), rotation);
        
        convertToGrayMaskImageIfNecessary(maskImagePath);

        Image maskImage = XrayImageIoUtil.loadImage(maskImagePath);
        
        //Siew Yeng - XCR-2824 - check mask image size
        if(maskImage.getWidth() != sliceImage.getWidth() || maskImage.getHeight() != sliceImage.getHeight())
        {
          Image resizedMaskImage = resizeMask(maskImage, sliceImage.getWidth(), sliceImage.getHeight());
          
          maskImage.decrementReferenceCount();
          maskImage = resizedMaskImage;
        }
 
        //Siew Yeng - XCR-2764
        //this is to make sure subregion masking does not exceed parent subtype masking - crop area exceed parent masking
        if(parentSubtype != null && createdMaskImage.getSubtypeMaskImage(parentSubtype.getLongName(), rotation) != null)
        {
          Image parent = Image.createFloatImageFromBufferedImage(createdMaskImage.getSubtypeMaskImage(parentSubtype.getLongName(), rotation));
          Arithmetic.orImages(parent, RegionOfInterest.createRegionFromImage(parent), 
                              maskImage, RegionOfInterest.createRegionFromImage(maskImage), 
                              maskImage, RegionOfInterest.createRegionFromImage(maskImage));
          parent.decrementReferenceCount();
        }
        
        createdMaskImage.addSubtypeMaskImage(subtype.getLongName(), rotation, maskImage.getBufferedImage());
        
//        java.awt.geom.Point2D xEdge = (ImageFeatureExtraction.findHorizontalEdgeForMask(maskImage));
//        java.awt.geom.Point2D yEdge = (ImageFeatureExtraction.findVerticalEdgeForMask(maskImage));
//
//        if (xEdge.getY() - xEdge.getX() < 2)
//        {
//          xEdge.setLocation(xEdge.getY(), maskImage.getHeight());
//        }
//        
//        if (yEdge.getY() - yEdge.getX() < 2)
//        {
//          yEdge.setLocation(0, yEdge.getY());
//        }
//
//        ImageCoordinate x = new ImageCoordinate(0, 0);
//        ImageCoordinate y = new ImageCoordinate(0, 0);
//
//        x.setLocation((int) yEdge.getY(), (int) yEdge.getX());
//        y.setLocation((int) xEdge.getY(), (int) xEdge.getX());
//        
//        int width = x.getX() - x.getY();
//        int height = y.getX() - y.getY();
//
//        if (width == 0 || height == 0)
//        {
//          width = maskImage.getWidth();
//          height = maskImage.getHeight();
//
//          x.setLocation(0, height);
//          y.setLocation(width, 0);                
//        }
        //set offset value for mask
        RegionOfInterest cropRegion = getCropRegionFromMaskImage(maskImage);

        createdMaskImage.setOffsetValueForMask(subtype.getLongName(), rotation, cropRegion.getMinX(), cropRegion.getMinY());

        if (createdMaskImage.getCropMaskImage(subtype.getLongName(), rotation) == null)
        {
          float[] white = new float[cropRegion.getWidth() * cropRegion.getHeight()];
          Arrays.fill(white, 255.0f);

          Image newMaskImage = Image.createFloatImageFromArray(cropRegion.getWidth(), cropRegion.getHeight(), white);

//          RegionOfInterest cropRegion = RegionOfInterest.createLineRegionFromRegionBorder(x.getY(), y.getY(), x.getX(), y.getX(), 0, RegionShapeEnum.RECTANGULAR);

          Transform.copyImageIntoImage(maskImage, cropRegion, newMaskImage, RegionOfInterest.createRegionFromImage(newMaskImage));

          createdMaskImage.addCropMaskSubtypeImage(subtype.getLongName(), rotation, newMaskImage.getBufferedImage());
        
          newMaskImage.decrementReferenceCount();
        }
        maskImage.decrementReferenceCount();
        
      }
      catch(NumberFormatException nfe)
      {
        NumberFormatException numFormatException = new NumberFormatException(rotationString);
        numFormatException.initCause(nfe);
        numFormatException.printStackTrace();
      }
      catch (DatastoreException ex)
      {
        DatastoreException dex = new CannotReadDatastoreException(maskImagePath);
        dex.initCause(ex);
        dex.printStackTrace();
      }
    }
  }
  
  /**
   * @author Jack Hwee
   */
  static private void convertToGrayMaskImageIfNecessary(String path)
  {
    ImagePlus maskImage = ij.IJ.openImage(path);

    if (maskImage.getType() == ImagePlus.GRAY8)
    {
      maskImage.flush();
      maskImage.close();
    }
    else
    {
      ImageConverter img = new ImageConverter(maskImage);
      img.convertToGray8();
      ij.IJ.save(maskImage, path);

      maskImage.flush();
      maskImage.close();
    }
  }
  
  /**
   * Rotates a mask by degreeRotation.
   * @author Jack Hwee
   */
  private static Image rotateMask(Image maskImage, double degreeRotation)
  {
    // build the transformation
    Assert.expect(maskImage != null);
    java.awt.geom.AffineTransform rotationTransform = java.awt.geom.AffineTransform.getRotateInstance(-Math.toRadians(degreeRotation));
    java.awt.geom.AffineTransform correctionTransform = new java.awt.geom.AffineTransform();

    RegionOfInterest baseRoi = RegionOfInterest.createRegionFromImage(maskImage);
    RegionOfInterest tempResultRoi = new RegionOfInterest(baseRoi);

    Transform.boundTransformedRegionOfInterest(baseRoi, rotationTransform, tempResultRoi, correctionTransform);

    // we need to keep the region's rotated dimensions so that we know how big of an image to create.
    int correctedImageWidth = tempResultRoi.getWidth();
    int correctedImageHeight = tempResultRoi.getHeight();

    Assert.expect(correctedImageWidth > 0);
    Assert.expect(correctedImageHeight > 0);

    RegionOfInterest resultRoi = new RegionOfInterest(0, 0, correctedImageWidth, correctedImageHeight, 0, RegionShapeEnum.RECTANGULAR);

    Image correctedImage = Transform.applyAffineTransform(maskImage,
      correctionTransform,
      resultRoi,
      255.0f);//Siew Yeng - background color for mask image should be white
    return correctedImage;
  }
  
  /**
   * Resize mask image with white background
   * @author Siew Yeng
   */
  private static Image resizeMask(Image maskImage, int width, int height)
  {
    RegionOfInterest roi = RegionOfInterest.createRegionFromImage(maskImage);
    roi.setWidthKeepingSameCenter(width);
    roi.setHeightKeepingSameCenter(height);

    RegionOfInterest correctedRoi = new RegionOfInterest(0, 0, width, height, 0, RegionShapeEnum.RECTANGULAR);

    java.awt.geom.AffineTransform affineTransform = java.awt.geom.AffineTransform.getTranslateInstance(-roi.getMinX(), -roi.getMinY());
    Image resizedMaskImage = Transform.applyAffineTransform(maskImage, affineTransform ,correctedRoi, 255.f);
    
    return resizedMaskImage;
  }
  
  /**
   * @author Siew Yeng
   */
  public static RegionOfInterest getCropRegionFromMaskImage(Image maskImage)
  {
    java.awt.geom.Point2D xEdge = (ImageFeatureExtraction.findHorizontalEdgeForMask(maskImage));
    java.awt.geom.Point2D yEdge = (ImageFeatureExtraction.findVerticalEdgeForMask(maskImage));

    if (xEdge.getY() - xEdge.getX() < 2)
    {
      xEdge.setLocation(xEdge.getY(), maskImage.getHeight());
    }

    if (yEdge.getY() - yEdge.getX() < 2)
    {
      yEdge.setLocation(0, yEdge.getY());
    }

    ImageCoordinate x = new ImageCoordinate(0, 0);
    ImageCoordinate y = new ImageCoordinate(0, 0);

    x.setLocation((int) yEdge.getY(), (int) yEdge.getX());
    y.setLocation((int) xEdge.getY(), (int) xEdge.getX());

    int width = x.getX() - x.getY();
    int height = y.getX() - y.getY();

    if (width == 0 || height == 0)
    {
      width = maskImage.getWidth();
      height = maskImage.getHeight();

      //Siew Yeng - XCR-3319
      x.setLocation(0, width);
      y.setLocation(height, 0);                
    }

    return RegionOfInterest.createLineRegionFromRegionBorder(x.getY(), y.getY(), x.getX(), y.getX(), 0, RegionShapeEnum.RECTANGULAR);
  }
  
  /**
   * @param subtype Subtype of mask image
   * @param imageWidth Slice image width
   * @param imageHeight Slice image height
   * @param maskImageRotation original mask image rotation
   * @param currentJointRotation current joint rotation(relative to panel)
   * @param customRotation to handle custom rotation(eg 45, 135,...)
   * @author Siew Yeng
   */
  private static void handleMaskRotation(MaskImage createdMaskImage,
                                        Subtype subtype, 
                                        int imageWidth, int imageHeight, 
                                        double maskImageRotation, double currentJointRotation, double customRotation)
  {
    Image oriMask = Image.createFloatImageFromBufferedImage(createdMaskImage.getSubtypeMaskImage(subtype.getLongName(), maskImageRotation));
    Image oriCropMask = Image.createFloatImageFromBufferedImage(createdMaskImage.getCropMaskImage(subtype.getLongName(), maskImageRotation));
    //rotate the mask
    Image rotatedMaskImage = rotateMask(oriMask, currentJointRotation - maskImageRotation + customRotation);
    Image rotatedCropMaskImage = rotateMask(oriCropMask, currentJointRotation - maskImageRotation + customRotation);

    //resize mask image to handle custom rotation
    if(rotatedMaskImage.getWidth() != imageWidth || rotatedMaskImage.getHeight() != imageHeight)
    {
      Image resizedMaskImage = resizeMask(rotatedMaskImage, imageWidth, imageHeight);

      rotatedMaskImage.decrementReferenceCount();
      rotatedMaskImage = resizedMaskImage;
    }
    
    //Siew Yeng - XCR-3553
    //set new offset value after rotation
    RegionOfInterest cropRegion = getCropRegionFromMaskImage(rotatedMaskImage);
    createdMaskImage.setOffsetValueForMask(subtype.getLongName(), currentJointRotation, cropRegion.getMinX(), cropRegion.getMinY());
    
    //add rotated mask into map
    createdMaskImage.addSubtypeMaskImage(subtype.getLongName(), currentJointRotation, rotatedMaskImage.getBufferedImage());
    createdMaskImage.addCropMaskSubtypeImage(subtype.getLongName(), currentJointRotation, rotatedCropMaskImage.getBufferedImage());

    oriMask.decrementReferenceCount();
    oriCropMask.decrementReferenceCount();
    rotatedMaskImage.decrementReferenceCount();
    rotatedCropMaskImage.decrementReferenceCount();
  }
}
