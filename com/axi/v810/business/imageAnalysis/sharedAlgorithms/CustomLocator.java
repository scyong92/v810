package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.util.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import java.awt.geom.AffineTransform;
import com.axi.v810.util.StringLocalizer;

 /**
This class can be used for custom locating algorithms when the standard
"look for black rectangle" locator is inadequate.The branching point is at customLocateRegion
Currently there is one locator implementation for exposed pad type joints which
is implemented in locateRegionbyLearnedVoidImage.

@author Lam Wai Beng
*/
public class CustomLocator extends Locator
{
  //private static boolean _locatordebug = false;
  // locate method, add new ones when required
  private static String _STANDARD ="Standard";
  private static String _LEARNEDVOIDIMAGE="Learned Image";

  private static int _BORDER = 10;
  private static double _MATCHQUALITY=0.5;

  public CustomLocator()
  {
    // do nothing
  }
  /**
  *    Adds new user locator type and user settings here.
  *    @author Lam Wai Beng
  */
  public static Collection<AlgorithmSetting> createAlgorithmSettings(int displayOrder, int currentVersion)
  {
    // Calls supperclass method first
    Collection<AlgorithmSetting> settingList=Locator.createAlgorithmSettings(displayOrder,currentVersion);
    displayOrder=displayOrder + settingList.size();

    ArrayList<String> allowableLocateMethods =
      new ArrayList<String>(Arrays.asList(_STANDARD, _LEARNEDVOIDIMAGE));

    AlgorithmSetting locateMethodSetting= new AlgorithmSetting(
       AlgorithmSettingEnum.LOCATE_METHOD, // setting enum
       displayOrder++, // display order,
       _STANDARD, // default value
       allowableLocateMethods,
       MeasurementUnitsEnum.NONE,
       "HTML_DESC_SHARED_LOCATOR_(LOCATE_METHOD)_KEY", // description URL Key
       "HTML_DETAILED_DESC_SHARED_LOCATOR_(LOCATE_METHOD)_KEY", // detailed description URL Key
       "IMG_DESC_SHARED_LOCATOR_(LOCATE_METHOD)_KEY", // image description URL Key
       AlgorithmSettingTypeEnum.STANDARD,
       currentVersion);
    // Append other customized settings here for custom locator method
    settingList.add(locateMethodSetting);
    return settingList;
  }

  /**
   * This is an exact copy of Locator.locatejoints except it calls
   * customLocateRegion.
   * Note: Unfortunately static methods cannot be overidden so the need to do this.
   * @author Lam Wai Beng
   */
  static public void customLocateJoints(ReconstructedImages reconstructedImages,
                                  SliceNameEnum sliceToUseForLocate,
                                  List<JointInspectionData> jointInspectionDataObjects,
                                  Algorithm callingAlgorithm)throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    List<DiagnosticInfo> listOfDiagnosticsToDisplay = new ArrayList<DiagnosticInfo>();
    List<DiagnosticInfo> cadDiagnosticInfoList = new LinkedList<DiagnosticInfo>();

    int numberOfJoints = jointInspectionDataObjects.size();
    List<RegionOfInterest> resultLocations = new ArrayList<RegionOfInterest>(numberOfJoints);
    List<ImageCoordinate> deviationList = new ArrayList<ImageCoordinate>(numberOfJoints);

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceToUseForLocate);
    boolean ableToPerformResize = AlgorithmUtil.isAlgorithmAbleToPerformResize(callingAlgorithm);
    // Locate all the joints, and compute the average deviation.
    Image surfaceImage = null;
    if(ableToPerformResize)
      surfaceImage = reconstructedSlice.getOrthogonalImage();
    else
      surfaceImage = reconstructedSlice.getOrthogonalImageWithoutEnhanced();
    
    Subtype subtype = jointInspectionDataObjects.get(0).getSubtype();

    ReconstructionRegion inspectRegionInt = reconstructedImages.getReconstructionRegion();
    MeasurementRegionEnum measurementRegionForLocationRegion = MeasurementRegionEnum.PAD_REGION;

    // Iterate over all the joints to find the located position.
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      // find the joint
      RegionOfInterest expectedJointLocation = jointInspectionData.getOrthogonalRegionOfInterestInPixels(ableToPerformResize);

      // For Throughhole joints, use the barrel as the 'joint'.
      LandPatternPad landPatternPad = jointInspectionData.getPad().getLandPatternPad();
      if (landPatternPad.isThroughHolePad())
      {
        expectedJointLocation = AlgorithmUtil.getRegionOfInterestOfThroughholeBarrel(jointInspectionData, ableToPerformResize);
        measurementRegionForLocationRegion = MeasurementRegionEnum.BARREL_REGION;
      }

      MeasurementRegionDiagnosticInfo cadDiagnostic = new MeasurementRegionDiagnosticInfo(expectedJointLocation, MeasurementRegionEnum.CAD_LOCATION_REGION);
      cadDiagnostic.setLabel(jointInspectionData.getPad().getName());
      cadDiagnosticInfoList.add(cadDiagnostic);

      // execute the locating routine from Custom Locator
      RegionOfInterest locatedJoint = customLocateRegion(
          jointInspectionData,
          surfaceImage,
          expectedJointLocation,
          inspectRegionInt,
          sliceToUseForLocate,
          callingAlgorithm,
          listOfDiagnosticsToDisplay);
      resultLocations.add(locatedJoint);

      // keep track of the deviation from the expected location
      ImageCoordinate deviation = new ImageCoordinate(locatedJoint.getMinX()-expectedJointLocation.getMinX(),
                        locatedJoint.getMinY()-expectedJointLocation.getMinY());
      deviationList.add(deviation);
    }

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages,
                                              sliceToUseForLocate,
                                              subtype,
                                              callingAlgorithm,
                                              cadDiagnosticInfoList);

    // Display the diagnostics generated in the 'locateRegion' calls.
    if (listOfDiagnosticsToDisplay.isEmpty() == false)
    {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
      listOfDiagnosticsToDisplay.clear();
    }

    // determine which joints to 'snap back' to the consensus location.

    // build an array that StatisticsUtil can handle
    float deviationArray[][] = new float[numberOfJoints][2];
    int j = 0;
    for (ImageCoordinate deviation : deviationList)
    {
      deviationArray[j][0] = deviation.getX();
      deviationArray[j++][1] = deviation.getY();
    }

    float snapBackDistance = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SNAPBACK_DISTANCE);
    float millimetersPerPixel = MathUtil.convertNanometersToMillimeters( MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel() );
    snapBackDistance /= millimetersPerPixel;

    Set<Integer> goodJoints = StatisticsUtil.convergeOnValidSet(deviationArray, StatisticsNormEnum.EUCLIDIAN, (float)snapBackDistance);

    applySnapback(resultLocations, deviationArray, goodJoints);

    // draw the final regions and record the measurements
    int i = 0;
    for (JointInspectionData jointInspectionData : jointInspectionDataObjects)
    {
      JointInspectionResult result = jointInspectionData.getJointInspectionResult();
      Assert.expect(i < resultLocations.size());
      RegionOfInterest jointRoi = resultLocations.get(i++);
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_X_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterX()));
      result.addMeasurement(new JointMeasurement(callingAlgorithm,
                                                 MeasurementEnum.LOCATOR_Y_LOCATION,
                                                 MeasurementUnitsEnum.PIXELS,
                                                 jointInspectionData.getPad(),
                                                 sliceToUseForLocate,
                                                 jointRoi.getCenterY()));

      listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(jointRoi, measurementRegionForLocationRegion));
    }

    // display all the dignostics generated in the loop above - the final measured locations of the joints.
    if (listOfDiagnosticsToDisplay.isEmpty() == false)
    {
      _diagnostics.postDiagnostics(inspectRegionInt, sliceToUseForLocate, subtype,
                                   callingAlgorithm,
                                   false, false,
                                   listOfDiagnosticsToDisplay.toArray(new DiagnosticInfo[listOfDiagnosticsToDisplay.size()]));
    }
  }

  /**
   This is the jumping point where the various locator methods may be executed
   based on the locate Method algorithm setting.
   * @author Lam Wai Beng
   */
  protected static RegionOfInterest customLocateRegion(
      JointInspectionData joint,
      Image image,
      RegionOfInterest region,
      ReconstructionRegion inspectRegionInt,
      SliceNameEnum sliceNameEnum,
      Algorithm callingAlgorithm,
      List<DiagnosticInfo> listOfDiagnosticsToDisplay) throws DatastoreException
  {

    Assert.expect(joint != null);
    Assert.expect(image != null);
    Assert.expect(region != null);
    Assert.expect(inspectRegionInt != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    Subtype subtype = joint.getSubtype();
    String locatormethod=(String)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_METHOD));
    RegionOfInterest result=null;
    if ( locatormethod.compareTo(_LEARNEDVOIDIMAGE) == 0 )
    {
       result = locateRegionbyLearnedVoidImage(joint, image, region, inspectRegionInt, sliceNameEnum, callingAlgorithm,
                                                listOfDiagnosticsToDisplay);
    }
    else // default standard locator
    {
       result = Locator.locateRegion(joint, image, region, inspectRegionInt, sliceNameEnum, callingAlgorithm,
                                      listOfDiagnosticsToDisplay);
    }
    return result;

  }
  /**
   This function implements the custom locating method for Bosch exposed pads.
   Composite thickness arrays are stored by the exposed pad voiding algorithm during the
   learning phase. These array are retrieved and used to create a template for correlation
   with the image. Note that the array is stored in 0 deg orientation
   Currently the function works with joints of 0,90,180 and 270 degreee orientation only.

   @author Lam Wai Beng
  */
  private static RegionOfInterest locateRegionbyLearnedVoidImage(
        JointInspectionData joint,
             Image image,
             RegionOfInterest region,
             ReconstructionRegion inspectRegionInt,
             SliceNameEnum sliceNameEnum,
             Algorithm callingAlgorithm,
      List<DiagnosticInfo> listOfDiagnosticsToDisplay) throws DatastoreException
  {
    Assert.expect(joint != null);
    Assert.expect(image != null);
    Assert.expect(region != null);
    Assert.expect(inspectRegionInt != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(callingAlgorithm != null);
    Assert.expect(_diagnostics != null);

    Pad thisPad = joint.getPad();
    PadSettings thisPadSetting = thisPad.getPadSettings();
    // verify learned composite image is available else use default locator
    if (!thisPadSetting.hasExpectedImageLearning(sliceNameEnum))
    {
       AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_CUSTOM_LOCATOR_REVERT_TO_STANDARD_LOCATOR_NODATA_KEY",
            new Object[]{
                joint.getFullyQualifiedPadName() }));

      return locateRegion(joint, image, region, inspectRegionInt, sliceNameEnum, callingAlgorithm,
                          listOfDiagnosticsToDisplay);
    }
    // get the learned solder thickness array of the pad
    ExpectedImageLearning  imageLearned = thisPadSetting.getExpectedImageLearning(sliceNameEnum);
    if (!imageLearned.hasCompositeExpectedImageArray())
    {
      AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_CUSTOM_LOCATOR_REVERT_TO_STANDARD_LOCATOR_NODATA_KEY",
            new Object[]{
                joint.getFullyQualifiedPadName() }));
      return locateRegion(joint, image, region, inspectRegionInt, sliceNameEnum, callingAlgorithm,
                          listOfDiagnosticsToDisplay);
    }
    // search locate parameters
    Subtype subtype = joint.getSubtype();
    float searchRegionAlong = (Float)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ALONG)) / 100.f;
    float searchRegionAcross = (Float)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.LOCATE_SEARCH_WINDOW_ACROSS)) / 100.f;
    int searchRegionAlongDist=(int)(searchRegionAlong*region.getLengthAlong());
    int searchRegionAcrossDist=(int)(searchRegionAcross*region.getLengthAcross());
    // check that requested search region is bigger than mask size else no point using the mask!
    // if mask is too big use standard locator and post warning.
    int distanceAcrossOffset = searchRegionAcrossDist-imageLearned.getImageHeight();
    int distanceAlongOffset  = searchRegionAlongDist-imageLearned.getImageWidth() ;
    if ( distanceAcrossOffset < 0 || distanceAlongOffset < 0  )
    {
        // System.out.println("Search region too small compared to mask");
        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_CUSTOM_LOCATOR_SEARCH_AREA_SMALL_KEY",
            new Object[]{
                joint.getFullyQualifiedPadName()}));

        // use standard locator
        return locateRegion(joint, image, region, inspectRegionInt, sliceNameEnum, callingAlgorithm,
                          listOfDiagnosticsToDisplay);
    }
    // create the user requested search region, ensure it is not bigger than image(minus border)
    // the border can be used to limit the maximum search area as the correlation is an expensive
    // operation.
    RegionOfInterest imageWithBorderRoi = RegionOfInterest.createRegionFromImage(image);
    imageWithBorderRoi.setWidthKeepingSameCenter(imageWithBorderRoi.getWidth()- 2*_BORDER);
    imageWithBorderRoi.setHeightKeepingSameCenter(imageWithBorderRoi.getHeight()- 2*_BORDER);
    RegionOfInterest requestedSearchRoi = new RegionOfInterest(region);
    requestedSearchRoi.scaleFromCenterAlongAcross(searchRegionAlong,searchRegionAcross);
    imageWithBorderRoi.setOrientationInDegrees(requestedSearchRoi.getOrientationInDegrees());
    RegionOfInterest searchRoi = RegionOfInterest.createRegionFromIntersection(imageWithBorderRoi,requestedSearchRoi) ;
    // check whether search region has been clipped and issue warning
    boolean clipSearchDistance = false;
    if ( searchRoi.getLengthAcross() < requestedSearchRoi.getLengthAcross() ) clipSearchDistance=true;
    if ( searchRoi.getLengthAlong() < requestedSearchRoi.getLengthAlong() ) clipSearchDistance=true;
    if (clipSearchDistance)
    {
      AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_LOCATOR_SEARCH_REGION_TOO_LARGE_WARNING_KEY",
            new Object[]{
                joint.getFullyQualifiedPadName() }));
      }
    listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(searchRoi, MeasurementRegionEnum.LOCATE_SEARCH_REGION));
    // get learned thickness pixel array
    float[] mask = imageLearned.getCompositeImageArray();
    int maskWidth=imageLearned.getImageWidth();
    int maskHeight=imageLearned.getImageHeight();
    Image maskImage=convertLearnedImageToMask(mask,maskWidth,maskHeight);
    // check component orientation first and rotate mask accordingly before template matching
    int absEpadRotation = Math.abs(region.getOrientationInDegrees()) ;
    // prepare to correlate
    DoubleRef quality=new DoubleRef(_MATCHQUALITY);
    ImageCoordinate matchResultTopLeftCoord;
    int centerXofMatchRect=0;
    int centerYofMatchRect=0;

    if ( absEpadRotation == 0 || absEpadRotation == 180 )
    {
      matchResultTopLeftCoord=Filter.matchTemplate(image,maskImage,searchRoi,
                             MatchTemplateTechniqueEnum.CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING,quality);
      centerXofMatchRect = matchResultTopLeftCoord.getX()+ maskWidth/2;
      centerYofMatchRect = matchResultTopLeftCoord.getY()+ maskHeight/2;
    }
    else if ( absEpadRotation == 90 || absEpadRotation == 270 )
    {
      // rotate mask 90 or 270, doesn't matter since mask has been symmetrized
      // warning : code in this section has note been tested yet
      Image rotatedMaskImage=rotateMask(maskImage,90);

      matchResultTopLeftCoord= Filter.matchTemplate( image,rotatedMaskImage,searchRoi,
                             MatchTemplateTechniqueEnum.CROSS_CORRELATION_NO_PIXEL_VALUE_LEVELING,quality);
      centerXofMatchRect = matchResultTopLeftCoord.getX()+ maskHeight/2; // mask has been rotated 90, use height
      centerYofMatchRect = matchResultTopLeftCoord.getY()+ maskWidth/2;
      rotatedMaskImage.decrementReferenceCount();

    }
    else // not suppose to happen
    {
      Assert.expect(false);
    }
    RegionOfInterest returnRoi = new RegionOfInterest(region);
    returnRoi.setCenterXY(centerXofMatchRect, centerYofMatchRect);
    // effective region width and height are not used
    returnRoi.setLengthAlong(maskWidth);
    returnRoi.setLengthAcross(maskHeight);

    //  must ensure that the return region fits in the image, following the convention in standard locator
    //  note that parameters effective shift along and across is not used since the same value cannot apply to
    //  both locators
    if (returnRoi.getMinX() < 0 || returnRoi.getMinY() < 0)
    {
      returnRoi.translateXY(Math.max( -returnRoi.getMinX(), 0),
                               Math.max( -returnRoi.getMinY(), 0));
    }
    if (returnRoi.getMaxX() >= image.getWidth() || returnRoi.getMaxY() >= image.getHeight())
    {
      returnRoi.translateXY(Math.min(image.getWidth() - returnRoi.getMaxX() - 1, 0),
                               Math.min(image.getHeight() - returnRoi.getMaxY() - 1, 0));
    }

    listOfDiagnosticsToDisplay.add(new MeasurementRegionDiagnosticInfo(returnRoi, MeasurementRegionEnum.LOCATE_MATCH_REGION));

    maskImage.decrementReferenceCount();

    return returnRoi;

  }
  /**
  * Rotates a mask by degreeRotation.
  * @author Lam Wai Beng ( adapted from George Booth's code)
  */
  private static Image rotateMask(Image inspectionImage, int degreeRotation)
  {
      // build the transformation
      Assert.expect(inspectionImage!=null);
      AffineTransform rotationTransform = AffineTransform.getRotateInstance( Math.toRadians(degreeRotation));
      AffineTransform correctionTransform = new AffineTransform();

      int imageMaxX = inspectionImage.getWidth() - 1;
      int imageMaxY = inspectionImage.getHeight() - 1;

      RegionOfInterest baseRoi = RegionOfInterest.createRegionFromImage(inspectionImage);
      RegionOfInterest tempResultRoi = new RegionOfInterest(baseRoi);

      Transform.boundTransformedRegionOfInterest(baseRoi, rotationTransform, tempResultRoi, correctionTransform);

      // we need to keep the region's rotated dimensions so that we know how big of an image to create.
      int correctedImageWidth = tempResultRoi.getWidth();
      int correctedImageHeight = tempResultRoi.getHeight();

      Assert.expect(correctedImageWidth > 0);
      Assert.expect(correctedImageHeight > 0);

      RegionOfInterest resultRoi = new RegionOfInterest(0, 0, correctedImageWidth, correctedImageHeight, 0, RegionShapeEnum.RECTANGULAR);

      Image correctedImage = Transform.applyAffineTransform(inspectionImage,
                                                            correctionTransform,
                                                            resultRoi,
                                                            0.0f);
      return correctedImage;

  }
  /**
   *   Converts Solder thickness Array to a Image mask/template required for correlation.
   *   Array values are remapped with zero thickness mapped to brightest
   *   and max array value mapped to darkest.
   *   Four corner averaging is used to symmetrize the mask.
   *
   *    @author Lam Wai Beng
   */
  private static Image convertLearnedImageToMask(float[] compositeThicknessArray, int maskWidth, int maskHeight)
  {
    Assert.expect(compositeThicknessArray!=null);
    Assert.expect( maskWidth*maskHeight==compositeThicknessArray.length );

    int arrayIndex=0;
    int maxColToProcess = (maskWidth-1)/2 ;
    int maxRowToProcess = (maskHeight-1)/2;

    final float MAXPIXELAMPLITUDE=255.0f;
    float maxPixelThickness = ArrayUtil.max(compositeThicknessArray);
    final float numberOfPixelsAveraged=4.0f;
    // Perform the 4 corner averaging
    int posTopLeft=0,posTopRight=0,posBotLeft=0,posBotRight=0;
    int maxColArrayPos=maskWidth-1;
    int maxRowArrayPos=maskHeight-1;
    float pixelAverageValue;

    for ( int row=0;row<=maxRowToProcess;row++)
    {
      for (int col = 0; col<= maxColToProcess; col++)
      {
        // pixel position mapped from array position.
        posTopLeft = row * maskWidth + col;
        posTopRight = row * maskWidth + maxColArrayPos - col;
        posBotLeft = (maxRowArrayPos - row) * maskWidth + col;
        posBotRight = (maxRowArrayPos - row) * maskWidth + maxColArrayPos - col;

        // four corner averaging
        pixelAverageValue=(compositeThicknessArray[posTopLeft]+compositeThicknessArray[posTopRight] +
          compositeThicknessArray[posBotLeft]+compositeThicknessArray[posBotRight])/numberOfPixelsAveraged;

        // map thickness to pixel intensities, max thickess=zero instensity, zero thciness=max intensity
        pixelAverageValue= -(MAXPIXELAMPLITUDE/maxPixelThickness)*pixelAverageValue + MAXPIXELAMPLITUDE;

        compositeThicknessArray[posTopLeft] = pixelAverageValue;
        compositeThicknessArray[posTopRight] = pixelAverageValue;
        compositeThicknessArray[posBotLeft] = pixelAverageValue;
        compositeThicknessArray[posBotRight] = pixelAverageValue;
      }
    }
    Image maskImage=Image.createFloatImageFromArray(maskWidth, maskHeight, compositeThicknessArray);
    return maskImage;
  }
} // end CustomLocator class **********************




















