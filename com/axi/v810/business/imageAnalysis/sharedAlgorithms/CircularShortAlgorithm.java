package com.axi.v810.business.imageAnalysis.sharedAlgorithms;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*; //Broken Pin
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.Config;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Implementation of the Short algorithm for circular short regions.
 *
 * @author Patrick Lacz
 * @author Matt Wharton
 */
public class CircularShortAlgorithm extends SharedShortAlgorithm
{
  //Siew Yeng - XCR-3318 - move to SharedShortAlgorithm
//  private static String _PIN_SLICE = "Pin";
//  private static String _COMPONENT_SLICE = "Component";
//  private static String _BOTH_SLICES = "Both";
//  private static String _PROJECTION_SLICE_ONLY = "2.5D Only"; //Broken Pin
//  private static String _PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES = "Pin, Component, 2.5D"; //Broken Pin
//  // lam
//  private static String _ON = "On";
//  private static String _OFF = "Off";

  /**
   * @author Patrick Lacz
   */
  public CircularShortAlgorithm(InspectionFamilyEnum inspectionFamilyEnum)
  {
    super(inspectionFamilyEnum);
  }

  /**
   * Initializes the AlgorithmSettings for this Algorithm.
   *
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  protected void addAlgorithmSettings()
  {
    final int algorithmVersion = 1;

    // PE:  this high displayOrder number is a hack to get the Short slice-related settings to appear in our desired
    // order in the Slice Setup tab.  We want the Short slice-related settings to come at the end.
    int displayOrder = 1000;

    List<JointTypeEnum> jointTypeList = JointTypeEnum.getAllJointTypeEnums();
    
    //Siew Yeng - XCR-3318 - Oval PTH
    Collection<AlgorithmSetting> throughHoleSharedShortAlgorithmSettings = createThroughHoleSharedShortAlgorithmSettings(displayOrder, algorithmVersion);    
    displayOrder += throughHoleSharedShortAlgorithmSettings.size();    
    
    //Siew Yeng - XCR-3318 - move to SharedShortAlgorithm
    // Slices to analyze (PTH only).
//    ArrayList<String> allowableThroughHoleSlices = new ArrayList<String>(Arrays.asList(_PIN_SLICE, _COMPONENT_SLICE, _BOTH_SLICES, _PROJECTION_SLICE_ONLY, _PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES)); //Broken Pin
//    AlgorithmSetting inspectionSliceSetting = new AlgorithmSetting(
//        AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE,
//        displayOrder++,
//        _BOTH_SLICES,
//        allowableThroughHoleSlices,
//        MeasurementUnitsEnum.NONE,
//        "HTML_DESC_SHARED_SHORT_(INSPECTION_SLICE)_KEY",
//        "HTML_DETAILED_DESC_SHARED_SHORT_(INSPECTION_SLICE)_KEY",
//        "IMG_DESC_SHARED_SHORT_(INSPECTION_SLICE)_KEY",
//        AlgorithmSettingTypeEnum.STANDARD,
//        algorithmVersion);

    // Detect high shorts (CGA only).
    ArrayList<String> highShortSliceSettings = new ArrayList<String>(Arrays.asList(_OFF, _ON));
    AlgorithmSetting highShortDetectionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_DETECTION,
        displayOrder++,
        _OFF,
        highShortSliceSettings,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(HIGH_SHORT_DETECTION)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(HIGH_SHORT_DETECTION)_KEY",
        "IMG_DESC_SHARED_SHORT_(HIGH_SHORT_DETECTION)_KEY",
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        algorithmVersion);

    //Siew Yeng - XCR-3318 - move to SharedShortAlgorithm
    // Detect high shorts (PTH only)
//    AlgorithmSetting protrusionHighShortDetectionSetting = new AlgorithmSetting(
//        AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION,
//        displayOrder++,
//        _OFF,
//        highShortSliceSettings,
//        MeasurementUnitsEnum.NONE,
//        "HTML_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_DETECTION)_KEY",
//        "HTML_DETAILED_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_DETECTION)_KEY",
//        "IMG_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_DETECTION)_KEY",
//        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
//        algorithmVersion);

    // Detect high shorts (CGA only).
    AlgorithmSetting highShortSliceheightSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_SLICEHEIGHT,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(10.0f), // default value
        MathUtil.convertMilsToMillimeters(-10.0f), // minimum value
        MathUtil.convertMilsToMillimeters(150.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(HIGH_SHORT_SLICEHEIGHT)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(HIGH_SHORT_SLICEHEIGHT)_KEY",
        "IMG_DESC_SHARED_SHORT_(HIGH_SHORT_SLICEHEIGHT)_KEY",
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        algorithmVersion);

    // High short minimum thickness setting
    AlgorithmSetting highShortSliceMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);

    //Siew Yeng - XCR-3318 - move to SharedShortAlgorithm
    // Protrusion Slice High short minimum thickness setting  (PTH only)
//    AlgorithmSetting protrusionHighShortSliceMinimumThicknessSetting = new AlgorithmSetting(
//        AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS,
//        displayOrder++,
//        MathUtil.convertMilsToMillimeters(4f),
//        MathUtil.convertMilsToMillimeters(0f),
//        MathUtil.convertMilsToMillimeters(25.5f),
//        MeasurementUnitsEnum.MILLIMETERS,
//        "HTML_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
//        "HTML_DETAILED_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
//        "IMG_DESC_SHARED_SHORT_(PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
//        AlgorithmSettingTypeEnum.ADDITIONAL,
//        algorithmVersion);
    
	//Broken Pin
     // 2D Slice High short minimum thickness setting  (PTH only)
//    AlgorithmSetting projectionShortSliceMinimumThicknessSetting = new AlgorithmSetting(
//        AlgorithmSettingEnum.SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS,
//        displayOrder++,
//        MathUtil.convertMilsToMillimeters(4f),
//        MathUtil.convertMilsToMillimeters(0f),
//        MathUtil.convertMilsToMillimeters(25.5f),
//        MeasurementUnitsEnum.MILLIMETERS,
//        "HTML_DESC_SHARED_SHORT_(PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
//        "HTML_DETAILED_DESC_SHARED_SHORT_(PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
//        "IMG_DESC_SHARED_SHORT_(PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS)_KEY",
//        AlgorithmSettingTypeEnum.ADDITIONAL,
//        algorithmVersion);

    // Global minimum thickness setting.
    AlgorithmSetting globalMinimumThicknessSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS,
        displayOrder++,
        MathUtil.convertMilsToMillimeters(4f),
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(GLOBAL_MIN_THICKNESS)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(GLOBAL_MIN_THICKNESS)_KEY",
        "IMG_DESC_SHARED_SHORT_(GLOBAL_MIN_THICKNESS)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);
    
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
    // Global minimum length setting.
    AlgorithmSetting globalMinimumLengthSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH,
        displayOrder++,
        _MINIMUM_SHORT_LENGTH_DEFAULT_VALUE_IN_MILLIS, //previously is fix to 3 pixels, fix to 3 pixels to support old recipe so that same result will retain.
        MathUtil.convertMilsToMillimeters(0f),
        MathUtil.convertMilsToMillimeters(25.5f),
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_SHARED_SHORT_(GLOBAL_MIN_LENGTH)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(GLOBAL_MIN_LENGTH)_KEY",
        "IMG_DESC_SHARED_SHORT_(GLOBAL_MIN_LENGTH)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);       

    //Siew Yeng - XCR-3318 - move to SharedShortAlgorithm
    // Component side slice minimum thickness setting (PTH only).
//    AlgorithmSetting componentSideSliceMinimumThicknessSetting = new AlgorithmSetting(
//        AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS,
//        displayOrder++,
//        MathUtil.convertMilsToMillimeters(4f),
//        MathUtil.convertMilsToMillimeters(0f),
//        MathUtil.convertMilsToMillimeters(25.5f),
//        MeasurementUnitsEnum.MILLIMETERS,
//        "HTML_DESC_SHARED_SHORT_(COMPONENT_SIDE_MIN_THICKNESS)_KEY",
//        "HTML_DETAILED_DESC_SHARED_SHORT_(COMPONENT_SIDE_MIN_THICKNESS)_KEY",
//        "IMG_DESC_SHARED_SHORT_(COMPONENT_SIDE_MIN_THICKNESS)_KEY",
//        AlgorithmSettingTypeEnum.STANDARD,
//        algorithmVersion);

    // Pin side slice minimum thickness setting (PTH only).
//    AlgorithmSetting pinSideSliceMinimumThicknessSetting = new AlgorithmSetting(
//        AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS,
//        displayOrder++,
//        MathUtil.convertMilsToMillimeters(4f),
//        MathUtil.convertMilsToMillimeters(0f),
//        MathUtil.convertMilsToMillimeters(25.5f),
//        MeasurementUnitsEnum.MILLIMETERS,
//        "HTML_DESC_SHARED_SHORT_(PIN_SIDE_MIN_THICKNESS)_KEY",
//        "HTML_DETAILED_DESC_SHARED_SHORT_(PIN_SIDE_MIN_THICKNESS)_KEY",
//        "IMG_DESC_SHARED_SHORT_(PIN_SIDE_MIN_THICKNESS)_KEY",
//        AlgorithmSettingTypeEnum.STANDARD,
//        algorithmVersion);

    // Region inner edge distance.
    AlgorithmSetting innerEdgeLocationSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION,
        displayOrder++,
        15.f,
        0f,
        200f,
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_SHARED_SHORT_(REGION_INNER_EDGE)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(REGION_INNER_EDGE)_KEY",
        "IMG_DESC_SHARED_SHORT_(REGION_INNER_EDGE)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);

    // Region outer edge distance.
    AlgorithmSetting outerEdgeLocationSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION,
        displayOrder++,
        75.f,
        0f,
        //100f,
        200f,
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_SHARED_SHORT_(REGION_OUTER_EDGE)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(REGION_OUTER_EDGE)_KEY",
        "IMG_DESC_SHARED_SHORT_(REGION_OUTER_EDGE)_KEY",
        AlgorithmSettingTypeEnum.STANDARD,
        algorithmVersion);

    // Region reference position (CAD or Located position).
    ArrayList<String> allowableRegionReferencePositionValues = new ArrayList<String>(Arrays.asList("Located Position",
                                                                                                   "CAD Position"));
    AlgorithmSetting regionReferencePositionSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_REFERENCE_POSITION,
        displayOrder++,
        "Located Position",
        allowableRegionReferencePositionValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_SHARED_SHORT_(REGION_REFERENCE_POSITION)_KEY",
        "HTML_DETAILED_DESC_SHARED_SHORT_(REGION_REFERENCE_POSITION)_KEY",
        "IMG_DESC_SHARED_SHORT_(REGION_REFERENCE_POSITION)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        algorithmVersion);
    //lam
    ArrayList<String>
        checkAllSlicesForShortDropList = new ArrayList<String>(Arrays.asList(_ON, _OFF));
    AlgorithmSetting checkAllSlicesForShort = new AlgorithmSetting(
      AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_CHECK_ALL_SLICES_FOR_SHORT ,
      displayOrder++,
      _OFF,
      checkAllSlicesForShortDropList,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(CHECK_ALL_SLICES_FOR_SHORT)_KEY", // desc
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(CHECK_ALL_SLICES_FOR_SHORT)_KEY", // detailed desc
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(CHECK_ALL_SLICES_FOR_SHORT)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      algorithmVersion);

	//Broken Pin
    Collection<AlgorithmSetting> brokenPinAlgorithmSettings = BrokenPinAlgorithm.createBrokenPinAlgorithmSettings(displayOrder, algorithmVersion);    
    displayOrder += brokenPinAlgorithmSettings.size();    
    
    for (JointTypeEnum jointTypeEnum : jointTypeList)
    {
      if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE))
      {
        // filter stuff that does NOT apply to Through Hole
        globalMinimumThicknessSetting.filter(jointTypeEnum);
        globalMinimumLengthSetting.filter(jointTypeEnum);//Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
        highShortDetectionSetting.filter(jointTypeEnum);
        highShortSliceheightSetting.filter(jointTypeEnum);
        highShortSliceMinimumThicknessSetting.filter(jointTypeEnum);
      }
      else
      {
        // filter stuff that ONLY applies to Through Hole
        for (AlgorithmSetting algSetting : throughHoleSharedShortAlgorithmSettings)
        {            
          algSetting.filter(jointTypeEnum);
        }    
        //Siew Yeng - XCR-3318 - move to SharedShortAlgorithm
//        inspectionSliceSetting.filter(jointTypeEnum);
//        pinSideSliceMinimumThicknessSetting.filter(jointTypeEnum);
//        componentSideSliceMinimumThicknessSetting.filter(jointTypeEnum);
//        protrusionHighShortSliceMinimumThicknessSetting.filter(jointTypeEnum);
//        projectionShortSliceMinimumThicknessSetting.filter(jointTypeEnum);//Broken Pin
//        protrusionHighShortDetectionSetting.filter(jointTypeEnum);
      }
      //lam
      if ( !( jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
                               jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA) ||
                               jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR ) ))
      {
        // check all slices only applies to the colapsible, non-colapse and variable height BGA
        checkAllSlicesForShort.filter(jointTypeEnum);
      }

      if (allowHighShortDetection(jointTypeEnum) == false)
      {
        // filter High Short related stuff
        highShortDetectionSetting.filter(jointTypeEnum);
        highShortSliceheightSetting.filter(jointTypeEnum);
        highShortSliceMinimumThicknessSetting.filter(jointTypeEnum);
      }
      
      //Lim, Lay Ngor - Broken Pin
      if(allowBrokenPinShortDetection(jointTypeEnum) == false)
      {
        for (AlgorithmSetting algSetting : brokenPinAlgorithmSettings)
        {            
          algSetting.filter(jointTypeEnum);
        }        
      }
    }

    //Siew Yeng - XCR-3318 - Oval PTH
    for (AlgorithmSetting algSetting : throughHoleSharedShortAlgorithmSettings)
    {            
      addAlgorithmSetting(algSetting);
    } 
//    addAlgorithmSetting(inspectionSliceSetting);
    addAlgorithmSetting(highShortDetectionSetting);
    addAlgorithmSetting(highShortSliceheightSetting);
    addAlgorithmSetting(highShortSliceMinimumThicknessSetting);
    addAlgorithmSetting(globalMinimumThicknessSetting);
    addAlgorithmSetting(globalMinimumLengthSetting);//Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
//    addAlgorithmSetting(componentSideSliceMinimumThicknessSetting);
//    addAlgorithmSetting(pinSideSliceMinimumThicknessSetting);
    addAlgorithmSetting(innerEdgeLocationSetting);
    addAlgorithmSetting(outerEdgeLocationSetting);
    addAlgorithmSetting(regionReferencePositionSetting);
//    addAlgorithmSetting(protrusionHighShortDetectionSetting);
//    addAlgorithmSetting(protrusionHighShortSliceMinimumThicknessSetting);
    //Broken Pin
//    addAlgorithmSetting(projectionShortSliceMinimumThicknessSetting);
    for (AlgorithmSetting algSetting : brokenPinAlgorithmSettings)
    {
      addAlgorithmSetting(algSetting);
    }        
    addAlgorithmSetting(checkAllSlicesForShort); //lam

  }

  /**
   * @author Matt Wharton
   */
  protected float getMinimumShortThicknessInMMThreshold(Subtype subtype, SliceNameEnum sliceNameEnum)
  {
    Assert.expect(subtype != null);
    Assert.expect(sliceNameEnum != null);

    float minimumShortThicknessInMM = 0f;

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE))
    {
      if (sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_COMPONENT_SIDE_SLICE_MINIMUM_SHORT_THICKNESS);
      }
      else if (sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_PIN_SIDE))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PIN_SIDE_SLICE_MINIMUM_SHORT_THICKNESS);
      }
      else if (sliceNameEnum.equals(SliceNameEnum.HIGH_SHORT))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_MINIMUM_SHORT_THICKNESS);
      }
      else if (sliceNameEnum.equals(SliceNameEnum.THROUGHHOLE_PROTRUSION))
      {
        minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_MINIMUM_SHORT_THICKNESS);

      }
      //Broken Pin
      else if (sliceNameEnum.equals(SliceNameEnum.CAMERA_0))
      {
         minimumShortThicknessInMM =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PROJECTION_SHORT_MINIMUM_SHORT_THICKNESS);
      }
      else
      {
        Assert.expect(false, "Illegal slice for PTH Short!");
      }
    }
    else
    {
      minimumShortThicknessInMM = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_THICKNESS);
    }

    return minimumShortThicknessInMM;
  }

  /**
   * @author Matt Wharton
   * @author Peter Esbensen
   */
  protected Collection<SliceNameEnum> getSlicesToInspect(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Collection<SliceNameEnum> slicesToInspect =
        new LinkedList<SliceNameEnum>(getInspectionFamily().getShortInspectionSlices(subtype));

    // Depending on the joint type, we need to further filter out slices based on the applicable algorithm setting.
    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE))
    {
      String inspectionSlice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_INSPECTION_SLICE);
      if (inspectionSlice.equals(_COMPONENT_SLICE))
      {
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
        slicesToInspect.remove(SliceNameEnum.CAMERA_0);//Broken Pin
      }
      else if (inspectionSlice.equals(_PIN_SLICE))
      {
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
        slicesToInspect.remove(SliceNameEnum.CAMERA_0);//Broken Pin
      }
      else if (inspectionSlice.equals(_BOTH_SLICES))
      {
        slicesToInspect.remove(SliceNameEnum.CAMERA_0);//Broken Pin
        // leave both slices in there
      }
      //Broken Pin
      else if (inspectionSlice.equals(_PROJECTION_SLICE_ONLY))
      {
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_PIN_SIDE);
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_COMPONENT_SIDE);
      }
      else if (inspectionSlice.equals(_PIN_AND_COMPONENT_INCLUDING_PROJECTION_SLICES) || inspectionSlice.equals("All"))
      {
          // leave all slices in there
      }
      else
      {
        Assert.expect(false);  // unexpected slice
      }
    }

    if (allowHighShortDetection(jointTypeEnum))
    {
      // adjust the sliceToInspection collection and do some sanity checks on it

      String highShortDetectionSetting = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_HIGH_SHORT_DETECTION);
      if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE))
        highShortDetectionSetting = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION);

      // based on the current state of the algorithms, the slice list better contain either High Short (for CGA)
      // or Protrusion (for PTH) -- but not both
      Assert.expect(slicesToInspect.contains(SliceNameEnum.HIGH_SHORT) ^
                    slicesToInspect.contains(SliceNameEnum.THROUGHHOLE_PROTRUSION));

      if (highShortDetectionSetting.equals(_ON))
      {
        // do nothing, leave the high short slice in there
      }
      else if (highShortDetectionSetting.equals(_OFF))
      {
        slicesToInspect.remove(SliceNameEnum.HIGH_SHORT);
        slicesToInspect.remove(SliceNameEnum.THROUGHHOLE_PROTRUSION);
      }
      else
      {
        Assert.expect(false);  // unexpected slice
      }
    }
    else
    {
      // make sure nobody is using this slice when they're not supposed to
      Assert.expect(slicesToInspect.contains(SliceNameEnum.HIGH_SHORT) == false);
      Assert.expect(slicesToInspect.contains(SliceNameEnum.THROUGHHOLE_PROTRUSION) == false);
    }

    return slicesToInspect;
  }

  /**
   * @author Lim, Lay Ngor - Broken Pin
   */
  protected void detectBrokenPinBaseOnShape(ReconstructedImages reconstructedImages,
                                  SliceNameEnum sliceNameEnum,
                                  List<JointInspectionData> jointInspectionDataObjects,
//                                  Algorithm algorithm,
                                  BooleanRef slicePassed) throws XrayTesterException
  {
//    slicePassed.setValue(false);
    
    BrokenPinAlgorithm.detectBrokenPinBaseOnShape(reconstructedImages, sliceNameEnum,
            jointInspectionDataObjects, this, slicePassed);    
  } 
  
  /**
   * @author Matt Wharton
   */
  protected void detectShortsOnJointBasedOnShape(Image image,
                                                 ReconstructionRegion inspectionRegion,
                                                 Subtype subtype,
                                                 JointInspectionData jointInspectionData,
                                                 SliceNameEnum sliceNameEnum,
                                                 BooleanRef jointPassed, 
                                                 final float MILIMETER_PER_PIXEL) throws XrayTesterException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointPassed != null);

    // Measure the smoothed background thickness profile.
    Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair;
    try
    {
      ringRegionAndProfilePair = measureRingRegionBackgroundGrayLevelProfile(image,
                                                                             inspectionRegion,
                                                                             sliceNameEnum,
                                                                             jointInspectionData);
    }
    catch (JointCannotBeInspectedBusinessException jEx)
    {
      LocalizedString regionDoesNotFitWithinImageWarningText = new LocalizedString(
          "ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_WARNING_KEY", new Object[] {inspectionRegion});
      AlgorithmUtil.raiseAlgorithmWarning(regionDoesNotFitWithinImageWarningText);
      jointPassed.setValue(false);
      return;
    }

    Pair<RegionOfInterest, RegionOfInterest> ringRegionPair = ringRegionAndProfilePair.getFirst();
    float[] ringProfile = ringRegionAndProfilePair.getSecond();
    ringRegionAndProfilePair = null;

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float[] ringThicknessProfile =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(ringProfile,
                                                                             _GRAY_LEVEL_BACKGROUND_PERCENTILE,
                                                                             thicknessTable, backgroundSensitivityOffset);
    ringThicknessProfile = ProfileUtil.getSmoothedProfile(ringThicknessProfile, _SMOOTHING_KERNEL_LENGTH);
    Assert.expect(ringThicknessProfile != null, "Ring thickness profile is null!");

    // Retrieve the short learning for this joint.
    BooleanRef learnedDataAvailable = new BooleanRef(true);
    ShortProfileLearning shortLearning = getShortProfileLearning(image,
                                                                 inspectionRegion,
                                                                 jointInspectionData,
                                                                 sliceNameEnum,
                                                                 learnedDataAvailable);

    // Get the learned thickness profile.  If we don't have any learning, just create an all zeroes learned profile.
    float[] ringDeltaThicknessProfile = getDeltaThicknessProfile(jointInspectionData,
                                                                 sliceNameEnum,
                                                                 ringThicknessProfile,
                                                                 learnedDataAvailable,
                                                                 shortLearning);

    // Create the "ring region" shape.
    RegionOfInterest outerRoi = ringRegionPair.getFirst();
    RegionOfInterest innerRoi = ringRegionPair.getSecond();
    GeneralPath ringShortRegion = new GeneralPath(outerRoi.getShape());
    ringShortRegion.append(innerRoi.getShape(), false);

    // Mark the start bin for the ring profile.  This is always at the zero degree location and the
    // profile goes counter-clockwise.
    Line2D profileStartLineSegment = new Line2D.Float(innerRoi.getMaxX(),
                                                      innerRoi.getCenterY(),
                                                      outerRoi.getMaxX(),
                                                      outerRoi.getCenterY());
    MeasurementRegionDiagnosticInfo profileStartLineSegmentDiagInfo =
        new MeasurementRegionDiagnosticInfo(profileStartLineSegment, MeasurementRegionEnum.SHORT_PROFILE_START_BIN_REGION);

    // Mapping of all potential short defect zones to defect disposition.
    Map<Pair<Integer, Integer>, ShortDefectDispositionEnum> shortZoneToDispositionMap =
        new HashMap<Pair<Integer, Integer>, ShortDefectDispositionEnum>();

    // Keep track of an aggregate shape for all of the short defect regions.
    GeneralPath definiteShortDefectsAggregateShape = new GeneralPath();
    GeneralPath questionableShortDefectsAggregateShape = new GeneralPath();
    GeneralPath exoneratedShortDefectsAggregateShape = new GeneralPath();

    final float minimumShortThicknessInMillis = getMinimumShortThicknessInMMThreshold(subtype, sliceNameEnum);
    final float minimumShortLengthInMillis = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_GLOBAL_MINIMUM_SHORT_LENGTH);
        
    // Check the delta thickness profile for potential shorts.
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START    
    Map<Pair<Integer, Integer>, Pair<Float, Float>> candidateShortZonesToLengthAndThicknessMap =
        analyzeDeltaThicknessProfileWithThicknessForShorts(image,
                                              inspectionRegion,
                                              jointInspectionData,
                                              sliceNameEnum,
                                              ringDeltaThicknessProfile,
                                              MILIMETER_PER_PIXEL,
                                              true); //Siew Yeng - XCR-3318 - Oval PTH   
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END

    // Based on our intiial list of potential short defects, build up a map of the short range to its defect
    // disposition.  The defect disposition will start out as QUESTIONABLE_SHORT.
    // Also, if diags are on, keep a map of all the short region shapes.  We need this later to mark the indicted zones.
    Map<Pair<Integer, Integer>, java.awt.Shape> shortZoneToShapeMap = new HashMap<Pair<Integer, Integer>, java.awt.Shape>();
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length
    for (Pair<Integer, Integer> candidateShortZone : candidateShortZonesToLengthAndThicknessMap.keySet())
    {
      int candidateShortStartBin = candidateShortZone.getFirst();
      int candidateShortEndBin = candidateShortZone.getSecond();
      shortZoneToDispositionMap.put(candidateShortZone, ShortDefectDispositionEnum.QUESTIONABLE_SHORT);

      if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
      {
        java.awt.Shape candidateShortShape = getShapeForShortDefectInRingRegion(ringDeltaThicknessProfile,
                                                                                ringRegionPair,
                                                                                candidateShortStartBin,
                                                                                candidateShortEndBin);
        shortZoneToShapeMap.put(candidateShortZone, candidateShortShape);
      }
    }

    // Verify that the shorts we found are true shorts.
    validateShortIndictments(image,
                             inspectionRegion,
                             subtype,
                             jointInspectionData,
                             sliceNameEnum,
                             ringThicknessProfile,
                             shortZoneToDispositionMap,
                             false);//Lim, Lay Ngor - XCR1743:Benchmark

    // Iterate thru our map and find any entries that are real shorts and indict them.
    // Also, label all questionable and exonerated shorts so we can show them in the diagnostics.
    for (Map.Entry<Pair<Integer, Integer>, ShortDefectDispositionEnum> mapEntry : shortZoneToDispositionMap.entrySet())
    {
      Pair<Integer, Integer> candidateShortZone = mapEntry.getKey();
      ShortDefectDispositionEnum shortDefectDisposition = mapEntry.getValue();

      if (shortDefectDisposition.equals(ShortDefectDispositionEnum.DEFINITE_SHORT))
      {
        // Indict the short - instead of short length, we also display the short thickness here.
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
        final Pair<Float, Float> shortDataInMillis = candidateShortZonesToLengthAndThicknessMap.get(candidateShortZone);
        final float shortLengthInMillis = shortDataInMillis.getFirst(); 
        final float shortDeltaThicknessInMillis = shortDataInMillis.getSecond();
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END        
        
        //indict short length
        indictShort(image,
                    inspectionRegion,
                    sliceNameEnum,
                    jointInspectionData,
                    shortLengthInMillis,
                    minimumShortLengthInMillis,
                    MeasurementEnum.SHORT_LENGTH);
        
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
        //indict short thickness at analyzeDeltaThicknessProfileWithThicknessForShorts()
        indictShort(image,
                    inspectionRegion,
                    sliceNameEnum,
                    jointInspectionData,
                    shortDeltaThicknessInMillis,//Lim, Lay Ngor - XCR1780 change to thickness
                    minimumShortThicknessInMillis,
                    MeasurementEnum.SHORT_THICKNESS);        
        //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - EMD
        
        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
        {
          // Add the short region shape to our aggregate definite short defects shape.
          java.awt.Shape shortDefectShape = shortZoneToShapeMap.get(candidateShortZone);
          Assert.expect(shortDefectShape != null);
          definiteShortDefectsAggregateShape.append(shortDefectShape, false);
        }

        jointPassed.setValue(false);
      }
      else if (shortDefectDisposition.equals(ShortDefectDispositionEnum.QUESTIONABLE_SHORT))
      {
        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
        {
          // Add the short region shape to our aggregate questionable short defects shape.
          java.awt.Shape shortDefectShape = shortZoneToShapeMap.get(candidateShortZone);
          Assert.expect(shortDefectShape != null);
          questionableShortDefectsAggregateShape.append(shortDefectShape, false);
        }
      }
      else if (shortDefectDisposition.equals(ShortDefectDispositionEnum.EXONERATED_SHORT))
      {
        if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
        {
          // Add the short region shape to our aggregate exonerated short defects shape.
          java.awt.Shape shortDefectShape = shortZoneToShapeMap.get(candidateShortZone);
          Assert.expect(shortDefectShape != null);
          exoneratedShortDefectsAggregateShape.append(shortDefectShape, false);
        }
      }
      else
      {
        // Shouldn't get here!
        Assert.expect(false, "Unexpected short defect disposition : " + shortDefectDisposition);
      }
    }

    // If we didn't have ANY indictments (confirmed or exonerated), let's do a final check to make sure
    // we don't have a huge short which takes up the whole region.
    if (shortZoneToDispositionMap.isEmpty())
    {
      if (learnedDataAvailable.getValue())
      {
        float potentialLargeShortThickness = measureLargeShortAcrossEntireRegion(ringProfile,
                                                                                 shortLearning,
                                                                                 subtype,
                                                                                 backgroundSensitivityOffset);

        // If the ring region has a huge short, just indict the whole thing.
        if (MathUtil.fuzzyGreaterThan(potentialLargeShortThickness, _LARGE_SHORT_MAX_THICKNESS_IN_MILLIS))
        {
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
          //Large short only detected by thickness factor, so the indictment of short length is removed.          
          indictShort(image,
            inspectionRegion,
            sliceNameEnum,
            jointInspectionData,
            potentialLargeShortThickness,//Lim, Lay Ngor - XCR1780 change to thickness
            _LARGE_SHORT_MAX_THICKNESS_IN_MILLIS,
            MeasurementEnum.SHORT_THICKNESS);
          //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - EMD          

          // Indicate that the joint failed.
          jointPassed.setValue(false);

          if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
          {
            // Make the short defect ROI equal to the whole short region.
            definiteShortDefectsAggregateShape = ringShortRegion;
          }
        }
      }
    }

    //Lim, Lay Ngor - XCR2610
    //Measurement data for passing component
    if(jointPassed.getValue() == true)
        calculateAndRecordPassMeasurementForCircularShort(jointInspectionData, sliceNameEnum, ringDeltaThicknessProfile);
        
    // Post the diagnostics for the short region (and defect zones).
    if (ImageAnalysis.areDiagnosticsEnabled(jointInspectionData.getJointTypeEnum(), this))
    {
      MeasurementRegionDiagnosticInfo ringShortRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(ringShortRegion,
                                              MeasurementRegionEnum.SHORT_BACKGROUND_REGION);
      MeasurementRegionDiagnosticInfo definiteShortDefectRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(definiteShortDefectsAggregateShape,
                                              MeasurementRegionEnum.DEFINITE_SHORT_DEFECT_REGION);
      MeasurementRegionDiagnosticInfo questionableShortDefectRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(questionableShortDefectsAggregateShape,
                                              MeasurementRegionEnum.QUESTIONABLE_SHORT_DEFECT_REGION);
      MeasurementRegionDiagnosticInfo exoneratedShortDefectRegionDiagInfo =
          new MeasurementRegionDiagnosticInfo(exoneratedShortDefectsAggregateShape,
                                              MeasurementRegionEnum.EXONERATED_SHORT_DEFECT_REGION);
      ProfileDiagnosticInfo rectangularShortProfileDiagInfo = createShortProfileDiagnosticInfo(jointInspectionData,
                                                                                               sliceNameEnum,
                                                                                               ringDeltaThicknessProfile,
                                                                                               shortZoneToDispositionMap);
      _diagnostics.postDiagnostics(inspectionRegion,
                                   sliceNameEnum,
                                   jointInspectionData,
                                   this,
                                   false,
                                   ringShortRegionDiagInfo,
                                   profileStartLineSegmentDiagInfo,
                                   definiteShortDefectRegionDiagInfo,
                                   questionableShortDefectRegionDiagInfo,
                                   exoneratedShortDefectRegionDiagInfo,
                                   rectangularShortProfileDiagInfo);
    }
  }

  /**
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  private float[] getDeltaThicknessProfile(JointInspectionData jointInspectionData,
                                           SliceNameEnum sliceNameEnum,
                                           float[] ringThicknessProfile,
                                           BooleanRef learnedDataAvailable,
                                           ShortProfileLearning shortLearning)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(ringThicknessProfile != null);
    Assert.expect(learnedDataAvailable != null);

    if (learnedDataAvailable.getValue())
    {
      Assert.expect(shortLearning != null);
      float[] learnedThicknessProfile = shortLearning.getAverageLearnedThicknessProfile();

      // Make sure that the learned profile sizes match the measured profile.
      if (ringThicknessProfile.length == learnedThicknessProfile.length)
      {
        return ArrayUtil.subtractArrays(ringThicknessProfile, learnedThicknessProfile);
      }
      else
      {
        raiseProfileSizeMismatchWarning(jointInspectionData, sliceNameEnum);
      }
    }

    return ringThicknessProfile;
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  protected float[] createGrayLevelProfileForLightestImage(Image lightestImage,
                                                           ReconstructionRegion inspectionRegion,
                                                           SliceNameEnum sliceNameEnum,
                                                           JointInspectionData jointInspectionData) throws JointCannotBeInspectedBusinessException
  {
    Assert.expect(lightestImage != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair =
        measureRingRegionBackgroundGrayLevelProfile(lightestImage,
                                                    inspectionRegion,
                                                    sliceNameEnum,
                                                    jointInspectionData);

    float graylevelProfile[] = ringRegionAndProfilePair.getSecond();

    return graylevelProfile;

  }

  /**
   * @author Matt Wharton
   * @author Peter Esbensen
   */
  RegionOfInterest getOuterRingRegionOfInterest(JointInspectionData jointInspectionData)

  {
    Assert.expect(jointInspectionData != null);

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the IPD.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels(MagnificationEnum.getCurrentNorminal());

    // Get the reference ROI.
    RegionOfInterest referenceRoi = getReferenceRoi(jointInspectionData);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
         AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
     final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
         AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;

     Assert.expect(regionOuterEdgeAsFractionOfInterPadDistance > regionInnerEdgeAsFractionOfInterPadDistance);

     int profileInnerEdgeOffset = (int)Math.ceil(interPadDistanceInPixels * regionInnerEdgeAsFractionOfInterPadDistance);
     int profileOuterEdgeOffset = (int)Math.ceil(interPadDistanceInPixels * regionOuterEdgeAsFractionOfInterPadDistance);
     int locatedJointMinX = referenceRoi.getMinX();
     int locatedJointMinY = referenceRoi.getMinY();
     int locatedJointWidth = referenceRoi.getWidth();
     int locatedJointHeight = referenceRoi.getHeight();
     int locatedJointOrientation = referenceRoi.getOrientationInDegrees();

     // Make sure that there's at least a pixel between the inner and outer ROIs.
     if (profileOuterEdgeOffset == profileInnerEdgeOffset)
     {
       profileOuterEdgeOffset++;
     }

     // Create the outer ring ROI.
     RegionOfInterest outerRoi = new RegionOfInterest(
         locatedJointMinX - profileOuterEdgeOffset,
         locatedJointMinY - profileOuterEdgeOffset,
         locatedJointWidth + (profileOuterEdgeOffset * 2),
         locatedJointHeight + (profileOuterEdgeOffset * 2),
         locatedJointOrientation,
         RegionShapeEnum.OBROUND);

     return outerRoi;
  }

  /**
   * Measures a background "ring profile" for the specified JointInspectionData.
   *
   * @author Matt Wharton
   */
  //LN: ori is private
  //Broken Pin
  public Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> measureRingRegionBackgroundGrayLevelProfile(
      Image image,
      ReconstructionRegion inspectionRegion,
      SliceNameEnum sliceNameEnum,
      JointInspectionData jointInspectionData) throws JointCannotBeInspectedBusinessException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);

    // Get the subtype.
    Subtype subtype = jointInspectionData.getSubtype();

    // Get the IPD.
    int interPadDistanceInPixels = jointInspectionData.getAlgorithmLimitedInterPadDistanceInPixels(MagnificationEnum.getCurrentNorminal());

    // Get the reference ROI.
    RegionOfInterest referenceRoi = getReferenceRoi(jointInspectionData);

    final float regionInnerEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION) / 100.f;
    final float regionOuterEdgeAsFractionOfInterPadDistance = (Float)subtype.getAlgorithmSettingValue(
        AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION) / 100.f;

    Assert.expect(regionOuterEdgeAsFractionOfInterPadDistance > regionInnerEdgeAsFractionOfInterPadDistance);

    int profileInnerEdgeOffset = (int)Math.ceil(interPadDistanceInPixels * regionInnerEdgeAsFractionOfInterPadDistance);
    int profileOuterEdgeOffset = (int)Math.ceil(interPadDistanceInPixels * regionOuterEdgeAsFractionOfInterPadDistance);
    int locatedJointMinX = referenceRoi.getMinX();
    int locatedJointMinY = referenceRoi.getMinY();
    int locatedJointWidth = referenceRoi.getWidth();
    int locatedJointHeight = referenceRoi.getHeight();
    int locatedJointOrientation = referenceRoi.getOrientationInDegrees();

    // Make sure that there's at least 2 pixels between the inner and outer ROIs.
    if ( profileOuterEdgeOffset == profileInnerEdgeOffset || 
        (profileOuterEdgeOffset-profileInnerEdgeOffset) < 4)
    {
      profileOuterEdgeOffset+=2;
      profileInnerEdgeOffset-=2;
      if (profileInnerEdgeOffset < 0)
      {
        profileOuterEdgeOffset+=2;
        profileInnerEdgeOffset=0;
      }
    }

    // Create the outer ring ROI.
    RegionOfInterest outerRoi = new RegionOfInterest(
        locatedJointMinX - profileOuterEdgeOffset,
        locatedJointMinY - profileOuterEdgeOffset,
        locatedJointWidth + (profileOuterEdgeOffset * 2),
        locatedJointHeight + (profileOuterEdgeOffset * 2),
        locatedJointOrientation,
        RegionShapeEnum.OBROUND);

    if (AlgorithmUtil.regionCanFitInImage(outerRoi, image) == false)
    {
      throw new JointCannotBeInspectedBusinessException("");
    }

    // Make sure the outer ring fits within the image.
    if (AlgorithmUtil.checkRegionBoundaries(outerRoi, image, inspectionRegion, "Short Background Outer Ring ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, outerRoi);
    }

    // Create the inner ring ROI.
    RegionOfInterest innerRoi = new RegionOfInterest(
        locatedJointMinX - profileInnerEdgeOffset,
        locatedJointMinY - profileInnerEdgeOffset,
        locatedJointWidth + (profileInnerEdgeOffset * 2),
        locatedJointHeight + (profileInnerEdgeOffset * 2),
        locatedJointOrientation,
        RegionShapeEnum.OBROUND);
    // Make sure the inner ring fits within the image.
    if (AlgorithmUtil.checkRegionBoundaries(innerRoi, image, inspectionRegion, "Short Background Inner Ring ROI") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageAndTruncateIfNecessary(image, innerRoi);

      // Make sure the inner ring is as least one pixel smaller than the outer.
      innerRoi.setWidthKeepingSameCenter(Math.min(innerRoi.getWidth(), (outerRoi.getWidth() - 1)));
      innerRoi.setHeightKeepingSameCenter(Math.min(innerRoi.getHeight(), (outerRoi.getHeight() - 1)));
    }

    // Measure the ring region gray level profile.
    float[] ringProfile = ImageFeatureExtraction.ringProfile(image, outerRoi, innerRoi);

    Pair<RegionOfInterest, RegionOfInterest> ringRegionPair =
      new Pair<RegionOfInterest, RegionOfInterest>(outerRoi, innerRoi);
    Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair =
      new Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]>(ringRegionPair, ringProfile);

    return ringRegionAndProfilePair;
  }

  /**
   * @author Matt Wharton
   */
  private java.awt.Shape getShapeForShortDefectInRingRegion(float[] ringDeltaThicknessProfile,
                                                            Pair<RegionOfInterest, RegionOfInterest> ringRegionPair,
                                                            int defectStartBin,
                                                            int defectEndBin)
  {
    Assert.expect(ringDeltaThicknessProfile != null);
    Assert.expect(ringRegionPair != null);
    Assert.expect((defectStartBin >= 0) && (defectStartBin < ringDeltaThicknessProfile.length));
    Assert.expect((defectEndBin >= 0) && (defectEndBin < ringDeltaThicknessProfile.length));

    RegionOfInterest outerRoi = ringRegionPair.getFirst();
    Assert.expect(outerRoi != null);
    RegionOfInterest innerRoi = ringRegionPair.getSecond();
    Assert.expect(innerRoi != null);

    double degreesPerBin = 360.0 / (double)ringDeltaThicknessProfile.length;
    double arcStartingAngle = defectStartBin * degreesPerBin;
    double arcAngleExtent = MathUtil.getDegreesWithin0To359((defectEndBin - defectStartBin) * degreesPerBin);

    // Create the outer arc shape.
    int outerRoiCenterX = outerRoi.getCenterX();
    int outerRoiCenterY = outerRoi.getCenterY();
    float outerRoiDiameter = (outerRoi.getHeight() + outerRoi.getWidth()) / 2f;
    float outerRoiRadius = outerRoiDiameter / 2f;
    Arc2D outerArc = new Arc2D.Float();
    outerArc.setArcByCenter(
      outerRoiCenterX,
      outerRoiCenterY,
      outerRoiRadius,
      arcStartingAngle,
      arcAngleExtent,
      Arc2D.OPEN);

    // Create the inner arc shape.
    int innerRoiCenterX = innerRoi.getCenterX();
    int innerRoiCenterY = innerRoi.getCenterY();
    float innerRoiDiameter = (innerRoi.getHeight() + innerRoi.getWidth()) / 2f;
    float innerRoiRadius = innerRoiDiameter / 2f;
    Arc2D innerArc = new Arc2D.Float();
    innerArc.setArcByCenter(
      innerRoiCenterX,
      innerRoiCenterY,
      innerRoiRadius,
      arcStartingAngle,
      arcAngleExtent,
      Arc2D.OPEN);

    // Create an aggregate shape that contains the outer and inner arcs and line segments that
    // connect them.
    GeneralPath shortDefectRegion = new GeneralPath(outerArc);
    shortDefectRegion.append(innerArc, false);
    Point2D outerArcStart = outerArc.getStartPoint();
    Point2D innerArcStart = innerArc.getStartPoint();
    shortDefectRegion.moveTo((float)outerArcStart.getX(), (float)outerArcStart.getY());
    shortDefectRegion.lineTo((float)innerArcStart.getX(), (float)innerArcStart.getY());
    Point2D outerArcEnd = outerArc.getEndPoint();
    Point2D innerArcEnd = innerArc.getEndPoint();
    shortDefectRegion.moveTo((float)outerArcEnd.getX(), (float)outerArcEnd.getY());
    shortDefectRegion.lineTo((float)innerArcEnd.getX(), (float)innerArcEnd.getY());

    return shortDefectRegion;
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  protected float[] measureThicknessProfileForLearning(Image image,
                                                       ReconstructionRegion inspectionRegion,
                                                       SliceNameEnum sliceNameEnum,
                                                       JointInspectionData jointInspectionData,
                                                       ShortProfileLearning shortLearning) throws DatastoreException, JointCannotBeInspectedBusinessException
  {
    Assert.expect(image != null);
    Assert.expect(inspectionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(shortLearning != null);

    Pair<Pair<RegionOfInterest, RegionOfInterest>, float[]> ringRegionAndProfilePair =
        measureRingRegionBackgroundGrayLevelProfile(image, inspectionRegion, sliceNameEnum, jointInspectionData);
    float[] ringProfile = ringRegionAndProfilePair.getSecond();

    // Learn the average gray level for the ring region.
    float averageRingGrayLevel = StatisticsUtil.mean(ringProfile);
    shortLearning.setAverageRegionGrayLevel(averageRingGrayLevel);

    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(jointInspectionData.getSubtype());
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(jointInspectionData.getSubtype());
    // Compute the smoothed ring thickness profile.
    float thicknessProfile[] =
        AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(ringProfile,
                                                                             _GRAY_LEVEL_BACKGROUND_PERCENTILE,
                                                                             thicknessTable,
                                                                             backgroundSensitivityOffset);
    thicknessProfile = ProfileUtil.getSmoothedProfile(thicknessProfile, _SMOOTHING_KERNEL_LENGTH);
    return thicknessProfile;
  }

  /**
   * Learns appropriate short region placement settings based on the joint type.
   *
   * @author Matt Wharton
   */
  protected void learnShortRegionPlacementSettings(Subtype subtype)
  {
    Assert.expect(subtype != null);

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.CGA) ||
        jointTypeEnum.equals(JointTypeEnum.CHIP_SCALE_PACKAGE) ||
        jointTypeEnum.equals(JointTypeEnum.COLLAPSABLE_BGA) ||
        jointTypeEnum.equals(JointTypeEnum.VARIABLE_HEIGHT_BGA_CONNECTOR) ||
        jointTypeEnum.equals(JointTypeEnum.NON_COLLAPSABLE_BGA))
    {
      // Find the minimum pitch and IPD.
      int minPitchInNanoMeters = Integer.MAX_VALUE;
      int minInterPadDistanceInNanoMeters = Integer.MAX_VALUE;
      Collection<Pad> padsInSubtype = subtype.getPads();
      
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - START
      //Ngie Xing
      float[] pitchResult = new float[2];
      float[] interPadDistanceResult = new float[2];
      boolean isIntelligentLearningDataAvailable = false;
      if(padsInSubtype.size() >= 2 && Config.isIntelligentLearning())
      {
        isIntelligentLearningDataAvailable = true;
        float[] padsPitch = new float[padsInSubtype.size()];
        float[] interPadDistance = new float[padsInSubtype.size()];
        int i = 0;
        for (Pad pad : padsInSubtype)
        {
          padsPitch[i] = pad.getPitchInNanoMeters();
          interPadDistance[i] = pad.getInterPadDistanceInNanoMeters();
          ++i;
        }

        pitchResult = StatisticsUtil.getOutliersOuterFences(padsPitch);
        interPadDistanceResult = StatisticsUtil.getOutliersOuterFences(interPadDistance);
      }
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - END
      
      for (Pad pad : padsInSubtype)
      {
        int pitchInNanoMeters = pad.getPitchInNanoMeters();
        
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier - START
        if (isIntelligentLearningDataAvailable && Config.isIntelligentLearning())
        {
          if (pitchInNanoMeters >= pitchResult[0] && pitchInNanoMeters <= pitchResult[1])
          {
            //Skip the below original code if it is outlier
            if (pitchInNanoMeters < minPitchInNanoMeters)
            {
              minPitchInNanoMeters = pitchInNanoMeters;
            }
          }
        }
        else
        {
          if (pitchInNanoMeters < minPitchInNanoMeters)
          {
            minPitchInNanoMeters = pitchInNanoMeters;
          }
        }
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier - END        
          
        int interPadDistanceInNanoMeters = pad.getInterPadDistanceInNanoMeters();
        
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier - START
        if (isIntelligentLearningDataAvailable && Config.isIntelligentLearning())
        {
          if (interPadDistanceInNanoMeters < interPadDistanceResult[0] || interPadDistanceInNanoMeters > interPadDistanceResult[1])
            continue;//Skip the below original code if it is outlier
          //Lim, Lay Ngor - XCR-2027 Exclude Outlier - END
        }
        
        if (interPadDistanceInNanoMeters < minInterPadDistanceInNanoMeters)
        {
          minInterPadDistanceInNanoMeters = interPadDistanceInNanoMeters;
        }
      }

      // Get the nominal diamaters for all the inspected slices and take the largest one.
      Collection<Float> nominalDiameters = new HashSet<Float>();
      InspectionFamily inspectionFamily = getInspectionFamily();
      Collection<SliceNameEnum> inspectionSlices = inspectionFamily.getOrderedInspectionSlices(subtype);
      if (inspectionSlices.contains(SliceNameEnum.PAD))
      {
        float padSliceNominalDiameterInMillis =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PAD);
        nominalDiameters.add(padSliceNominalDiameterInMillis);
      }
      if (inspectionSlices.contains(SliceNameEnum.MIDBALL))
      {
        float midballSliceNominalDiameterInMillis =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_MIDBALL);
        nominalDiameters.add(midballSliceNominalDiameterInMillis);
      }
      if (inspectionSlices.contains(SliceNameEnum.PACKAGE))
      {
        float packageSliceNominalDiameterInMillis =
            (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.GRID_ARRAY_MEASUREMENT_NOMINAL_DIAMETER_PACKAGE);
        nominalDiameters.add(packageSliceNominalDiameterInMillis);
      }
      Assert.expect(nominalDiameters.isEmpty() == false);
      float maxNominalDiameterInMillis = Collections.max(nominalDiameters);
      int maxNominalDiameterInNanoMeters = (int)Math.round(MathUtil.convertMillimetersToNanometers(maxNominalDiameterInMillis));

      // Calculate the inner and outer edge offsets based on the pitch and max nominal diameter.
      int pitchAndNominalDiameterDifference = Math.max(minPitchInNanoMeters - maxNominalDiameterInNanoMeters, 0);
      final double PITCH_AND_NOMINAL_DIAMETER_DIFFERENCE_FRACTION = 0.15d;
      double pitchAndDiameterOffset = (double)pitchAndNominalDiameterDifference * PITCH_AND_NOMINAL_DIAMETER_DIFFERENCE_FRACTION;
      double innerEdgeOffsetInNanoMeters = pitchAndDiameterOffset;
      double outerEdgeOffsetInNanoMeters = minPitchInNanoMeters - maxNominalDiameterInNanoMeters - pitchAndDiameterOffset;
      float innerEdgeOffsetAsPercentageOfIpd = (float)(innerEdgeOffsetInNanoMeters / minInterPadDistanceInNanoMeters) * 100.f;
      float outerEdgeOffsetAsPercentageOfIpd = (float)(outerEdgeOffsetInNanoMeters / minInterPadDistanceInNanoMeters) * 100.f;

      // Do some sanity checks to make sure the inner edge isn't beyond the outer or some such nonsense.
      if (innerEdgeOffsetAsPercentageOfIpd >= outerEdgeOffsetAsPercentageOfIpd)
      {
        innerEdgeOffsetAsPercentageOfIpd = 25.f;
        outerEdgeOffsetAsPercentageOfIpd = 80.f;
      }

      // Set learned values.
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION, innerEdgeOffsetAsPercentageOfIpd);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION, outerEdgeOffsetAsPercentageOfIpd);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.THROUGH_HOLE) || jointTypeEnum.equals(JointTypeEnum.PRESSFIT))
    {
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_REGION_INNER_EDGE_LOCATION, 25.f);
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.SHARED_SHORT_REGION_OUTER_EDGE_LOCATION, 80.f);
    }
    else
    {
      // Do nothing...leave the settings at their defaults.
    }
  }

  /**
   * This is just a utility routine to help the ThroughHole Inspection Family know which slices it needs to inspect.
   *
   * @author Peter Esbensen
   */
  static public boolean hasProtrusionSlice(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // you shouldn't be calling this except from ThroughHole
    Assert.expect(subtype.getJointTypeEnum().equals(JointTypeEnum.THROUGH_HOLE));

    String pinDetectionSliceChoice = (String)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.SHARED_SHORT_PROTRUSION_HIGH_SHORT_DETECTION);

    if (pinDetectionSliceChoice.equals(_ON))
    {
      return true;
    }

    return false;
  }
  
  /**
   * Calculate and record passing measurement if circular short test region is enable.
   * 
   * @author Lim, Lay Ngor
   */
  private void calculateAndRecordPassMeasurementForCircularShort(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum, float[] deltaCircularThicknessProfile)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(deltaCircularThicknessProfile != null);
    
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - START
    //Show max thickness as pass value if not detected as defect under this pad
    final float maxThickness = ArrayUtil.max(deltaCircularThicknessProfile);
    //Lim, Lay Ngor - XCR2167 - show measurement as component type if it is under category isSpecialTwoPinComponent().
    recordPassedMeasurement(sliceNameEnum, jointInspectionData, maxThickness, MeasurementEnum.SHORT_THICKNESS);
    //Lim, Lay Ngor - XCR1780 - FoxConn Short Thickness & Length - END      
  }   
}
