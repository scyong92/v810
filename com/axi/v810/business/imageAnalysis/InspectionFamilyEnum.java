package com.axi.v810.business.imageAnalysis;

import com.axi.util.*;

/**
 * @author Peter Esbensen
 */
public class InspectionFamilyEnum extends com.axi.util.Enum
{
  private static int _index = 0;
  public static final InspectionFamilyEnum GRID_ARRAY = new InspectionFamilyEnum(_index++, "Grid Array");
  public static final InspectionFamilyEnum CHIP = new InspectionFamilyEnum(_index++, "Chip");
  public static final InspectionFamilyEnum GULLWING = new InspectionFamilyEnum(_index++, "Gullwing");
  public static final InspectionFamilyEnum ADVANCED_GULLWING = new InspectionFamilyEnum(_index++, "Advanced Gullwing");
  public static final InspectionFamilyEnum THROUGHHOLE = new InspectionFamilyEnum(_index++, "Through Hole");
  public static final InspectionFamilyEnum LAND_GRID_ARRAY = new InspectionFamilyEnum(_index++, "Land Grid Array");
  public static final InspectionFamilyEnum LARGE_PAD = new InspectionFamilyEnum(_index++, "Large Pad");
  public static final InspectionFamilyEnum CALIBRATION = new InspectionFamilyEnum(_index++, "Calibration");  // not currently used . . . was used for developing thickness tables
  public static final InspectionFamilyEnum POLARIZED_CAP = new InspectionFamilyEnum(_index++, "Polarized Cap");
  public static final InspectionFamilyEnum PRESSFIT = new InspectionFamilyEnum(_index++, "Pressfit");
  public static final InspectionFamilyEnum QUAD_FLAT_NO_LEAD = new InspectionFamilyEnum(_index++, "QFN");
  public static final InspectionFamilyEnum EXPOSED_PAD = new InspectionFamilyEnum(_index++, "Exposed Pad");
  public static final InspectionFamilyEnum OVAL_THROUGHHOLE = new InspectionFamilyEnum(_index++, "Oval Through Hole"); //Siew Yeng - XCR-3318 - Oval PTH

  private String _name;

  /**
   * @author Bill Darbie
   */
  public InspectionFamilyEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name.intern();
  }

  /**
   * @author Bill Darbie
   */
  public String getName()
  {
    return _name;
  }
}
