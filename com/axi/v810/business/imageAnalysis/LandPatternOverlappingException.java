package com.axi.v810.business.imageAnalysis;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 *
 * @author khang-shian.sham
 */
public class LandPatternOverlappingException extends BusinessException
{

  /**
   * @author khang-shian.sham
   */
  public LandPatternOverlappingException(LocalizedString localizedString)
  {
    super(localizedString);
    Assert.expect(localizedString != null);
  }
}
