package com.axi.v810.business.imageAnalysis;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author Sunit Bhalla
 */
public class Test_StatisticalTuningEngineFull extends AlgorithmUnitTest
{
  private static final String _BGA_SUBTYPE_1 = "bg00001_CollapsableBGA";
  private static final String _BGA_SUBTYPE_2 = "bg00002_CollapsableBGA";

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_StatisticalTuningEngineFull());
  }

  /**
   * @author Sunit Bhalla
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    boolean printDebugInformation = false;

    boolean runFullTests = true;

    Test_StatisticalTuningEngine testStatisticalTuningEngine = new Test_StatisticalTuningEngine();

    try
    {

      TimerUtil wholeTestTimer = new TimerUtil();
      wholeTestTimer.start();

      TimerUtil timer = new TimerUtil();

      timer.start();
      Project project =  getProject("FAMILIES_ALL_RLV");
      timer.stop();
      if (printDebugInformation)
        System.out.println("The project created: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      testStatisticalTuningEngine.deleteAllLearnedData(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("deleteOldDatabaseData done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      testStatisticalTuningEngine.testLearnSubtypeAndDeleteLearnedData(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testLearnSubtype done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testStatisticalTuningEngine.testNonApplicableImageRun(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testNonApplicableImageRun done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testStatisticalTuningEngine.testLearnJointType(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("learnJointType done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

//      timer.start();
//      testStatisticalTuningEngine.testLearnPanel(project);
//      timer.stop();
//      if (printDebugInformation)
//        System.out.println("learnPanel done: " + timer.getElapsedTimeInMillis() / 60000.0);
//      timer.reset();

      timer.start();
      if (runFullTests)
        testStatisticalTuningEngine.testStatusWindowInformation(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testStatusWindow done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testStatisticalTuningEngine.testIncrementalLearning(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testIncrementalLearning done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testStatisticalTuningEngine.testMultipleImageRuns(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testMultipleImageRuns done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      if (runFullTests)
        testStatisticalTuningEngine.testOnlyUnloadedBoards(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("unloadedBoards done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      timer.start();
      testStatisticalTuningEngine.testLearnSubtypesAsserts(project);
      timer.stop();
      if (printDebugInformation)
        System.out.println("testAsserts done: " + timer.getElapsedTimeInMillis() / 60000.0);
      timer.reset();

      wholeTestTimer.stop();

      if (printDebugInformation)
        System.out.println("Time for test (min): " + wholeTestTimer.getElapsedTimeInMillis() / 60000.0);

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }


}
