package com.axi.v810.business.imageAnalysis.chip;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import java.util.*;

/**
 * Extracting some of the clutter from ChipMeasurementAlgorithm into utility methods.
 *
 * @author Patrick Lacz
 */
public class ChipAlgorithmUtil
{
  /**
   * @author Patrick Lacz
   */
  public ChipAlgorithmUtil()
  {
    // do nothing
    // this class has only static methods.
  }

  static private final double _PROFILE_EXTENSION = 0.15;
  static private final int _PROFILE_SMOOTHING_STEP_SIZE = 7;

  /**
   * @author Peter Esbensen
   */
  public static float measureBodyThicknessInMillimeters(float[] componentThicknessProfile,
                                                        JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        boolean isLearningData)//Lim, Lay Ngor - XCR-2027 Exclude Outlier
  {
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);

    // take the average thickness in between the pads
    RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest componentRoi = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

    int leftIndexOfComponentNotIncludingPadTwo = getPadTwoRightSideIndex(componentThicknessProfile, padTwoJointInspectionData, padTwoRoi, componentRoi);
    int rightIndexOfComponentNotIncludingPadOne = getPadOneLeftSideIndex(componentThicknessProfile, padOneJointInspectionData, padOneRoi, componentRoi);
    int lengthOfComponentNotIncludingPads = rightIndexOfComponentNotIncludingPadOne -
                                            leftIndexOfComponentNotIncludingPadTwo;
    int thicknessMeasurementRegionLength = Math.max(3, lengthOfComponentNotIncludingPads / 2);

    // the thickness region is centered between the two pads.
    int leftIndexOfThicknessRegion = leftIndexOfComponentNotIncludingPadTwo +
                                     lengthOfComponentNotIncludingPads / 2 -
                                     thicknessMeasurementRegionLength / 2;
    int rightIndexOfThicknessRegion = leftIndexOfThicknessRegion + thicknessMeasurementRegionLength;

    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    float averageThicknessInMils = AlgorithmUtil.meanWithExcludeOutlierDecision(componentThicknessProfile, leftIndexOfThicknessRegion, rightIndexOfThicknessRegion, isLearningData);

    return MathUtil.convertMilsToMillimeters(averageThicknessInMils);
  }

  /**
   * Pad One is on the right side of the profile.  So you basically use the right side of profile, adjusted for the
   * PROFILE_EXTENSION.
   *
   * @author Peter Esbensen
   */
  public static int getPadOneRightSideIndex(float[] componentProfile,
                                            JointInspectionData padOneJointInspectionData,
                                            RegionOfInterest componentRegion)
  {
    Assert.expect(componentProfile != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(componentRegion != null);

    int componentLengthInCad = componentRegion.getLengthAlong();

    return componentProfile.length - (int)Math.round(componentLengthInCad * _PROFILE_EXTENSION * 0.5);
  }

  /**
   * Pad One is on the right side of the profile.
   *
   * @author Peter Esbensen
   */
  public static int getPadOneLeftSideIndex(float[] componentProfile,
                                           JointInspectionData padOneJointInspectionData,
                                           RegionOfInterest padRegion,
                                           RegionOfInterest componentRegion)
  {
    Assert.expect(componentProfile != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padRegion != null);
    Assert.expect(componentRegion != null);

    int padOneLeftSideIndex = getPadOneRightSideIndex(componentProfile, padOneJointInspectionData, componentRegion)
                              - padRegion.getLengthAlong();
    return padOneLeftSideIndex;
  }

  /**
   * Pad Two is on the left side of the profile.  So we basically use the start of the profile, adjusted for the
   * PROFILE_EXTENSION.
   *
   * @author Peter Esbensen
   */
  public static int getPadTwoLeftSideIndex(JointInspectionData padTwoJointInspectionData,
                                           RegionOfInterest componentRegion)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentRegion != null);

    int componentLengthInCad = componentRegion.getLengthAlong();

    return (int)Math.round(componentLengthInCad * _PROFILE_EXTENSION * 0.5);
  }

  /**
   * Pad Two is on the left side of the profile.
   *
   * @author Peter Esbensen
   */
  public static int getPadTwoRightSideIndex(float[] componentProfile,
                                            JointInspectionData padTwoJointInspectionData,
                                            RegionOfInterest padRegion,
                                            RegionOfInterest componentRegion)
  {
    Assert.expect(componentProfile != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(padRegion != null);
    Assert.expect(componentRegion != null);

    int padTwoRightSideIndex = getPadTwoLeftSideIndex(padTwoJointInspectionData, componentRegion) +
                               padRegion.getLengthAlong();

    return padTwoRightSideIndex;
  }

  /**
   * @author Peter Esbensen
   */
  public static int forceIndexOntoPadOne(JointInspectionData padOneJointInspectionData,
                                         float[] profile, int index)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(profile != null);

    RegionOfInterest componentRegion = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();
    RegionOfInterest padRegion = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    int padOneLeftSide = ChipAlgorithmUtil.getPadOneLeftSideIndex(profile, padOneJointInspectionData, padRegion, componentRegion);
    int padOneRightSide = ChipAlgorithmUtil.getPadOneRightSideIndex(profile, padOneJointInspectionData, componentRegion);
    return Math.max(padOneLeftSide, Math.min(padOneRightSide - 1, index));
  }

  /**
   * @author Peter Esbensen
   */
  public static int forceIndexOntoPadTwo(JointInspectionData padTwoJointInspectionData, float[] profile, int index)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(profile != null);

    RegionOfInterest componentRegion = padTwoJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();
    RegionOfInterest padRegion = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    int padTwoLeftSide = ChipAlgorithmUtil.getPadTwoLeftSideIndex(padTwoJointInspectionData, componentRegion);
    int padTwoRightSide = ChipAlgorithmUtil.getPadTwoRightSideIndex(profile, padTwoJointInspectionData, padRegion, componentRegion);
    return Math.max(padTwoLeftSide, Math.min(padTwoRightSide - 1, index));
  }


  /**
   * @author Peter Esbensen
   */
  public static float getPadOneSolderVolume(JointInspectionData padOneJointInspectionData, float[] profile)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(profile != null);
    int padOneLengthInPixels = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().
                               getLengthAlong();

    return ArrayUtil.sum(profile, 0, padOneLengthInPixels);
  }

  /**
   * @author Peter Esbensen
   */
  public static float getPadTwoSolderVolume(JointInspectionData padTwoJointInspectionData, float[] profile)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(profile != null);
    int padTwoLengthInPixels = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().
                               getLengthAlong();
    return ArrayUtil.sum(profile, profile.length - 1 - padTwoLengthInPixels, profile.length);
  }


  /**
   * @author Peter Esbensen
   */
  public static boolean landPatternIsTestable(ComponentInspectionData componentInspectionData)
  {
    Assert.expect(componentInspectionData != null);

    RegionOfInterest padOneRegionOfInterest = componentInspectionData.getPadOneJointInspectionData().getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padTwoRegionOfInterest = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getOrthogonalRegionOfInterestInPixels();

    return (padOneRegionOfInterest.intersects(padTwoRegionOfInterest) == false);
  }


  /**
   * @author Peter Esbensen
   */
  public static float[] getComponentProfile(ComponentInspectionData componentInspectionData,
                                            ReconstructionRegion reconstructionRegion,
                                            ReconstructedSlice reconstructedSlice,
                                            RegionOfInterest componentRegionOfInterest,
                                            Algorithm callingAlgorithm,
                                            boolean postDiagnostics)
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);

    Image image = reconstructedSlice.getOrthogonalImage();

    RegionOfInterest adjustedRegionOfInterest = new RegionOfInterest(componentRegionOfInterest);

    adjustedRegionOfInterest.scaleFromCenterAlongAcross(1.0, 0.5);

    float[] componentProfile = ImageFeatureExtraction.profile(image, adjustedRegionOfInterest);

    if (postDiagnostics)
    {
      if (ImageAnalysis.areDiagnosticsEnabled(componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum(), callingAlgorithm))
      {
        MeasurementRegionDiagnosticInfo componentDiagnosticRegion =
            new MeasurementRegionDiagnosticInfo(adjustedRegionOfInterest,
                                                MeasurementRegionEnum.COMPONENT_PROFILE_REGION);

        AlgorithmDiagnostics.getInstance().postDiagnostics(reconstructionRegion,
                                                           reconstructedSlice.getSliceNameEnum(),
                                                           componentInspectionData.getPadOneJointInspectionData(),
                                                           callingAlgorithm,
                                                           false,
                                                           componentDiagnosticRegion);
      }
    }
    return componentProfile;
  }

  /**
   * @author Peter Esbensen
   */
  public static float[] getComponentThicknessProfile(ReconstructedSlice reconstructedSlice,
                                                     ReconstructionRegion reconstructionRegion,
                                                     RegionOfInterest componentRegionOfInterest,
                                                     JointInspectionData padOneJointInspectionData,
                                                     Algorithm callingAlgorithm,
                                                     boolean postDiagnostics) throws DatastoreException
  {
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(callingAlgorithm != null);

    Image image = reconstructedSlice.getOrthogonalImage();
    RegionOfInterest componentProfileRegionOfInterest = new RegionOfInterest(componentRegionOfInterest);
    // Ensure the component ROI is constrained to the image boundaries.
    if (AlgorithmUtil.checkRegionBoundaries(componentProfileRegionOfInterest, image, reconstructionRegion, "component region of interest") == false)
    {
      // Shift the ROI back into the image.
      AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, componentProfileRegionOfInterest);
    }

    // get a profile over the component
    float[] componentProfile = ChipAlgorithmUtil.getComponentProfile(padOneJointInspectionData.getComponentInspectionData(),
                                                                     reconstructionRegion, reconstructedSlice,
                                                                     componentProfileRegionOfInterest, callingAlgorithm, postDiagnostics);

    // get an average of the two background profiles
    Subtype subtype = padOneJointInspectionData.getSubtype();
    float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f *
         (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT);

    float[] backgroundProfile = AlgorithmUtil.getComponentBasedBackgroundProfile(reconstructionRegion,
                                                                                 reconstructedSlice,
                                                                                 componentProfileRegionOfInterest,
                                                                                 padOneJointInspectionData,
                                                                                 backgroundRegionShiftAsFractionOfInterPadDistance,
                                                                                 callingAlgorithm, postDiagnostics);

    // compute component thickness profile
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityThreshold = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float[] componentThicknessProfile = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMils(componentProfile, backgroundProfile, thicknessTable, backgroundSensitivityThreshold);

    return componentThicknessProfile;
  }

  /**
   * @author Peter Esbensen
   */
  public static float measurePadOneFilletGap(JointInspectionData padOneJointInspectionData,
                                             float[] componentDerivativeProfile,
                                             float filletLocationSubPixelIndex)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(componentDerivativeProfile != null);
    int profileLength = componentDerivativeProfile.length;
    int filletLocationIndex = Math.round(filletLocationSubPixelIndex);
    Assert.expect((filletLocationIndex >= 0.0) && (filletLocationIndex < profileLength));

    int oneThirdPadLengthInPixels = Math.round((padOneJointInspectionData.getPad().getLengthInNanoMeters() / 3) /
                                               MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel());
    int indexToStopSearch = Math.min(profileLength - 1, filletLocationIndex + oneThirdPadLengthInPixels);

    return ArrayUtil.max(componentDerivativeProfile, filletLocationIndex, indexToStopSearch + 1);
  }

  /**
   * @author Peter Esbensen
   */
  public static float measurePadTwoFilletGap(JointInspectionData padTwoJointInspectionData,
                                             float[] componentDerivativeProfile,
                                             float filletLocationSubPixelIndex)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentDerivativeProfile != null);
    int filletLocationIndex = Math.round(filletLocationSubPixelIndex);
    Assert.expect((filletLocationIndex >= 0) && (filletLocationIndex < componentDerivativeProfile.length));

    int oneThirdPadLengthInPixels = (padTwoJointInspectionData.getPad().getLengthInNanoMeters() / 3) /
                                    MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    int indexToStartSearch = Math.max(0, filletLocationIndex - oneThirdPadLengthInPixels);

    return ArrayUtil.min(componentDerivativeProfile, indexToStartSearch, filletLocationIndex + 1);
  }

  /**
   * @author Peter Esbensen
   */
  public static float measureComponentShiftAcrossInDegrees(int filletOneLocationIndex,
                                                        int filletTwoLocationIndex,
                                                        RegionOfInterest componentRegionOfInterest,
                                                        ReconstructedSlice reconstructedSlice,
                                                        ReconstructionRegion reconstructionRegion,
                                                        JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        BooleanRef measurementIsValid)
  {
    Assert.expect((filletOneLocationIndex >= 0) && (filletOneLocationIndex < componentRegionOfInterest.getLengthAlong()));
    Assert.expect((filletTwoLocationIndex >= 0) && (filletTwoLocationIndex < componentRegionOfInterest.getLengthAlong()));
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(measurementIsValid != null);

    Image image = reconstructedSlice.getOrthogonalImage();

    int centerAlongIndex = (int)Math.round(
        componentRegionOfInterest.getCenterAlong() - componentRegionOfInterest.getMinCoordinateAlong());
    int filletOneTranslationAlong = filletOneLocationIndex - centerAlongIndex;
    int filletTwoTranslationAlong = filletTwoLocationIndex - centerAlongIndex;

    int halfPadLengthInNanos = padOneJointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    // set up regions for each fillet
    RegionOfInterest filletOneRegion = new RegionOfInterest(componentRegionOfInterest);
    filletOneRegion.translateAlongAcross(filletOneTranslationAlong, 0);
    filletOneRegion.setLengthAlong(halfPadLengthInPixels);

    RegionOfInterest filletTwoRegion = new RegionOfInterest(componentRegionOfInterest);
    filletTwoRegion.translateAlongAcross(filletTwoTranslationAlong, 0);
    filletTwoRegion.setLengthAlong(halfPadLengthInPixels);

    int orientationOfProfiles = MathUtil.getDegreesWithin0To359(filletOneRegion.getOrientationInDegrees() + 90);
    filletOneRegion.setOrientationInDegrees(orientationOfProfiles);
    filletTwoRegion.setOrientationInDegrees(orientationOfProfiles);

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, filletTwoRegion);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, filletOneRegion);

    float[] filletOneProfile = ImageFeatureExtraction.profile(image, filletOneRegion);

    float[] filletTwoProfile = ImageFeatureExtraction.profile(image, filletTwoRegion);

    // get center of mass for each profile
    float filletOneCentroid = StatisticsUtil.getCentroid(filletOneProfile);
    float filletTwoCentroid = StatisticsUtil.getCentroid(filletTwoProfile);
    if ((Float.isNaN(filletOneCentroid)) || (Float.isNaN(filletTwoCentroid)))
    {
      measurementIsValid.setValue(false);
      return 0.0f;
    }

    double distanceAcrossBetweenPadCentroidsInPixels = Math.abs(filletOneCentroid - filletTwoCentroid);
    double distanceAlongBetweenPadCentroidsInPixels = Math.abs(filletOneRegion.getCenterAcross() - filletTwoRegion.getCenterAcross()); // these regions are rotated 90 degrees, so we use across not along
    double rotationInRadians = Math.atan(distanceAcrossBetweenPadCentroidsInPixels / distanceAlongBetweenPadCentroidsInPixels);
    double rotationInDegrees = Math.toDegrees(rotationInRadians);
    measurementIsValid.setValue(true);
    return (float)rotationInDegrees;
  }

  // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
  /**
   * @author Anthony Fong
   */
  public static float measureComponentMisalignmentAlongInMils(int filletOneLocationIndex,
                                                        int filletTwoLocationIndex,
                                                        RegionOfInterest componentRegionOfInterest,
                                                        ReconstructedSlice reconstructedSlice,
                                                        ReconstructionRegion reconstructionRegion,
                                                        JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        BooleanRef measurementIsValid)
  {
    Assert.expect((filletOneLocationIndex >= 0) && (filletOneLocationIndex < componentRegionOfInterest.getLengthAlong()));
    Assert.expect((filletTwoLocationIndex >= 0) && (filletTwoLocationIndex < componentRegionOfInterest.getLengthAlong()));
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(measurementIsValid != null);

    Image image = reconstructedSlice.getOrthogonalImage();

    int centerAlongIndex = (int)Math.round(
        componentRegionOfInterest.getCenterAlong() - componentRegionOfInterest.getMinCoordinateAlong());
    int filletOneTranslationAlong = filletOneLocationIndex - centerAlongIndex;
    int filletTwoTranslationAlong = filletTwoLocationIndex - centerAlongIndex;

    int halfPadLengthInNanos = padOneJointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    // set up regions for each fillet
    RegionOfInterest filletOneRegion = new RegionOfInterest(componentRegionOfInterest);
    filletOneRegion.translateAlongAcross(filletOneTranslationAlong, 0);
    filletOneRegion.setLengthAlong(halfPadLengthInPixels);

    RegionOfInterest filletTwoRegion = new RegionOfInterest(componentRegionOfInterest);
    filletTwoRegion.translateAlongAcross(filletTwoTranslationAlong, 0);
    filletTwoRegion.setLengthAlong(halfPadLengthInPixels);

    int orientationOfProfiles = MathUtil.getDegreesWithin0To359(filletOneRegion.getOrientationInDegrees() + 90);
    filletOneRegion.setOrientationInDegrees(orientationOfProfiles);
    filletTwoRegion.setOrientationInDegrees(orientationOfProfiles);

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, filletTwoRegion);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, filletOneRegion);

    float[] filletOneProfile = ImageFeatureExtraction.profile(image, filletOneRegion);

    float[] filletTwoProfile = ImageFeatureExtraction.profile(image, filletTwoRegion);

    // get center of mass for each profile
    float filletOneCentroid = StatisticsUtil.getCentroid(filletOneProfile);
    float filletTwoCentroid = StatisticsUtil.getCentroid(filletTwoProfile);
    if ((Float.isNaN(filletOneCentroid)) || (Float.isNaN(filletTwoCentroid)))
    {
      measurementIsValid.setValue(false);
      return 0.0f;
    }


//    MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(filletOneRegion, MeasurementRegionEnum.COMPONENT_REGION);


//    AlgorithmDiagnostics _diagnostics = AlgorithmDiagnostics.getInstance();

//    _diagnostics.postDiagnostics(reconstructionRegion,
//                                 reconstructedSlice.getSliceNameEnum(),
//                                 padOneJointInspectionData.getSubtype(),
//                                 null, false, true,
//                                 componentRegionDiagnosticInfo);


    double distanceAcrossBetweenPadCentroidsInPixels = Math.abs(filletOneCentroid - filletTwoCentroid);
    double distanceAlongBetweenPadCentroidsInPixels = Math.abs(filletOneRegion.getCenterAcross() - filletTwoRegion.getCenterAcross()); // these regions are rotated 90 degrees, so we use across not along
    double rotationInRadians = Math.atan(distanceAcrossBetweenPadCentroidsInPixels / distanceAlongBetweenPadCentroidsInPixels);
    double rotationInDegrees = Math.toDegrees(rotationInRadians);
    measurementIsValid.setValue(true);
    return (float)rotationInDegrees;
  }

  // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
  /**
   * @author Anthony Fong
   */
  public static float measureComponentMisalignmentAcrossInMils(int filletOneLocationIndex,
                                                        int filletTwoLocationIndex,
                                                        RegionOfInterest componentRegionOfInterest,
                                                        ReconstructedSlice reconstructedSlice,
                                                        ReconstructionRegion reconstructionRegion,
                                                        JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        BooleanRef measurementIsValid)
  {
      /*
    Assert.expect((filletOneLocationIndex >= 0) && (filletOneLocationIndex < componentRegionOfInterest.getCenterAcross()));
    Assert.expect((filletTwoLocationIndex >= 0) && (filletTwoLocationIndex < componentRegionOfInterest.getCenterAcross()));
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(measurementIsValid != null);

    Image image = reconstructedSlice.getOrthogonalImage();

    int centerAcrossIndex = (int)Math.round(
        componentRegionOfInterest.getCenterAcross() - componentRegionOfInterest.getMinCoordinateAcross());
    int filletOneTranslationAcross = filletOneLocationIndex - centerAcrossIndex;
    int filletTwoTranslationAcross = filletTwoLocationIndex - centerAcrossIndex;

    int halfPadLengthInNanos = padOneJointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.NOMINAL.getNanoMetersPerPixel();

    // set up regions for each fillet
    RegionOfInterest filletOneRegion = new RegionOfInterest(componentRegionOfInterest);
    filletOneRegion.translateAlongAcross(filletOneTranslationAcross, 0);
    filletOneRegion.setLengthAlong(halfPadLengthInPixels);

    RegionOfInterest filletTwoRegion = new RegionOfInterest(componentRegionOfInterest);
    filletTwoRegion.translateAlongAcross(filletTwoTranslationAcross, 0);
    filletTwoRegion.setLengthAlong(halfPadLengthInPixels);

    int orientationOfProfiles = MathUtil.getDegreesWithin0To359(filletOneRegion.getOrientationInDegrees() + 90);
    filletOneRegion.setOrientationInDegrees(orientationOfProfiles);
    filletTwoRegion.setOrientationInDegrees(orientationOfProfiles);

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, filletTwoRegion);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, filletOneRegion);

    float[] filletOneProfile = ImageFeatureExtraction.profile(image, filletOneRegion);

    float[] filletTwoProfile = ImageFeatureExtraction.profile(image, filletTwoRegion);

    // get center of mass for each profile
    float filletOneCentroid = StatisticsUtil.getCentroid(filletOneProfile);
    float filletTwoCentroid = StatisticsUtil.getCentroid(filletTwoProfile);
    if ((Float.isNaN(filletOneCentroid)) || (Float.isNaN(filletTwoCentroid)))
    {
      measurementIsValid.setValue(false);
      return 0.0f;
    }

    double distanceAcrossBetweenPadCentroidsInPixels = Math.abs(filletOneCentroid - filletTwoCentroid);
    double distanceAlongBetweenPadCentroidsInPixels = Math.abs(filletOneRegion.getCenterAcross() - filletTwoRegion.getCenterAcross()); // these regions are rotated 90 degrees, so we use across not along
    double rotationInRadians = Math.atan(distanceAcrossBetweenPadCentroidsInPixels / distanceAlongBetweenPadCentroidsInPixels);
    double rotationInDegrees = Math.toDegrees(rotationInRadians);
    measurementIsValid.setValue(true);
    return (float)rotationInDegrees;
       */

       return 0;
  }

  /**
   * @author Peter Esbensen
   */
  public static float measureFilletThicknessInMils(JointInspectionData jointInspectionData,
                                                   float[] componentThicknessProfile,
                                                   int filletIndex,
                                                   boolean isLearningData) //Lim, Lay Ngor - XCR-2027 Exclude Outlier
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);

    int halfPadLengthInNanos = jointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    int leftIndex = Math.max(0, filletIndex - halfPadLengthInPixels);
    int rightIndex = Math.min(componentThicknessProfile.length - 1,
                              filletIndex + halfPadLengthInPixels);

    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    return AlgorithmUtil.meanWithExcludeOutlierDecision(componentThicknessProfile, leftIndex, rightIndex + 1, isLearningData);
  }

  /**
   * @author Seng-Yew Lim
   *
   * This is added on 16-Dec-2008, to enable customer to adjust the pad size used to measure fillet thickness on either ends.
   */
  public static float measurePartialFilletThicknessInMils(JointInspectionData jointInspectionData,
                                                   float[] componentThicknessProfile,
                                                   int filletIndex,
                                                   float percentageOfPad)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);

    int halfPadLengthInNanos = jointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    halfPadLengthInPixels = (int)( halfPadLengthInPixels * (percentageOfPad/100.0) );

    int leftIndex = Math.max(0, filletIndex - halfPadLengthInPixels);
    int rightIndex = Math.min(componentThicknessProfile.length - 1,
                              filletIndex + halfPadLengthInPixels);

    return StatisticsUtil.mean(componentThicknessProfile, leftIndex, rightIndex + 1);
  }

  /**
   * @author Seng-Yew Lim
   *
   * This is modified based on measurePartialFilletThicknessInMils on 26-Feb-2008,
   * to enable customer to adjust the pad size used to measure fillet thickness on either ends.
   */
  public static float measurePartialFilletThicknessInMilsToRight(JointInspectionData jointInspectionData,
                                                   float[] componentThicknessProfile,
                                                   int filletIndex,
                                                   float percentageOfPad,
                                                   boolean isLearningData)//Lim, Lay Ngor - XCR-2027 Exclude Outlier
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);

    int halfPadLengthInNanos = jointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    int partialPadLengthInPixels = (int)( halfPadLengthInPixels * 2 * (percentageOfPad/100.0) );

    int leftIndex = Math.max(0, filletIndex);
    int rightIndex = Math.min(componentThicknessProfile.length - 1,
                              filletIndex + partialPadLengthInPixels);

    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    return AlgorithmUtil.meanWithExcludeOutlierDecision(componentThicknessProfile, leftIndex, rightIndex + 1, isLearningData);
  }

  /**
   * @author Seng-Yew Lim
   *
   * This is modified based on measurePartialFilletThicknessInMils on 26-Feb-2008,
   * to enable customer to adjust the pad size used to measure fillet thickness on either ends.
   */
  public static float measurePartialFilletThicknessInMilsToLeft(JointInspectionData jointInspectionData,
                                                   float[] componentThicknessProfile,
                                                   int filletIndex,
                                                   float percentageOfPad,
                                                   boolean isLearningData)//Lim, Lay Ngor - XCR-2027 Exclude Outlier
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);

    int halfPadLengthInNanos = jointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    int partialPadLengthInPixels = (int)( halfPadLengthInPixels * 2 * (percentageOfPad/100.0) );

    int leftIndex = Math.max(0, filletIndex - partialPadLengthInPixels);
    int rightIndex = Math.min(componentThicknessProfile.length - 1,
                              filletIndex);

    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    return AlgorithmUtil.meanWithExcludeOutlierDecision(componentThicknessProfile, leftIndex, rightIndex + 1, isLearningData);
  }

  /**
   * @author Seng-Yew Lim
   *
   * This is added on 08-Jan-2009, to enable customer to adjust the pad size used to measure normalized fillet area on either ends.
   * ======================================================================================
   * | EXPERIMENT : Temporary hack to test if area calculation is better to detect insuff |
   * ======================================================================================
   */
  public static float measurePartialFilletAreaValue(JointInspectionData jointInspectionData,
                                                   float[] componentThicknessProfile,
                                                   int filletIndex,
                                                   float percentageOfPad)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);

    int halfPadLengthInNanos = jointInspectionData.getPad().getLengthInNanoMeters() / 2;
    int halfPadLengthInPixels = halfPadLengthInNanos / MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    halfPadLengthInPixels = (int)( halfPadLengthInPixels * (percentageOfPad/100.0) );

    int leftIndex = Math.max(0, filletIndex - halfPadLengthInPixels);
    int rightIndex = Math.min(componentThicknessProfile.length - 1,
                              filletIndex + halfPadLengthInPixels);

    float mean = StatisticsUtil.mean(componentThicknessProfile, leftIndex, rightIndex + 1);
    float normalizedAreaValue = mean * (rightIndex + 1 - leftIndex);
//    int indexMax = ProfileUtil.findMaxAbsoluteValue(componentThicknessProfile);
//    normalizedAreaValue /= componentThicknessProfile[indexMax]; // This does not represent the actual area.
    normalizedAreaValue /= 100;
    return normalizedAreaValue;
  }

  /**
   * @author Peter Esbensen
   */
  public static boolean regionFits(Image image,
                                   RegionOfInterest componentRegionOfInterest,
                                   ComponentInspectionData componentInspectionData)
  {
    Assert.expect(image != null);
    Assert.expect(componentRegionOfInterest != null);
    Assert.expect(componentInspectionData != null);

    // does the region fit in the image at all?
    RegionOfInterest imageRoi = RegionOfInterest.createRegionFromImage(image);
    int imageWidth = imageRoi.getWidth();
    int imageHeight = imageRoi.getHeight();
    int roiWidth = componentRegionOfInterest.getWidth();
    int roiHeight = componentRegionOfInterest.getHeight();
    if ((roiWidth > imageWidth) || (roiHeight > imageHeight))
      return false;
    return true;
  }
}
