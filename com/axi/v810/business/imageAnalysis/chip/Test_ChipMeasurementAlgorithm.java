package com.axi.v810.business.imageAnalysis.chip;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;

/**
 * @author Peter Esbensen
 */
class Test_ChipMeasurementAlgorithm extends AlgorithmUnitTest
{
  /**
   * @author Peter Esbensen
   */
  public Test_ChipMeasurementAlgorithm()
  {
    // do nothing
    super();
  }

  /**
   * Main test method.
   *
   * @author Peter Esbensen
   */
  public void test(BufferedReader in, PrintWriter out)
  {
    try
    {
      // Right now, these tests don't really verify any test results, they
      // just make sure that things don't crash.
      final String CHIP_SUBTYPE = "sm00002_Capacitor";
      final String REFDES_TO_TEST = "C234";
      final String PAD_TO_TEST = "1";

      final Project project = getProject("FAMILIES_ALL_RLV");
      final Panel panel = project.getPanel();
      final Subtype subtype = panel.getSubtype(CHIP_SUBTYPE);

      final Pad pad = project.getPanel().getBoards().get(0).getComponent(REFDES_TO_TEST).getPad(PAD_TO_TEST);

      final List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = getNormalImageSet(project);
      imageSetDataList.add(imageSetData);
      runSubtypeUnitTestInspection(project, subtype, imageSetDataList);

      imageSetData = createAndSelectBlackImageSet(project, pad, "testChipBlackImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      imageSetData = createAndSelectWhiteImageSet(project, pad, "testChipWhiteImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);

      imageSetData = createAndSelectPseudoRandomImageSet(project, pad, "testChipPseudoRandomImageSet");
      imageSetDataList.clear();
      imageSetDataList.add(imageSetData);
      runPadUnitTestInspection(project, pad, imageSetDataList);
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static void main(final String[] args)
  {
    UnitTest.execute(new Test_ChipMeasurementAlgorithm());
  }
}
