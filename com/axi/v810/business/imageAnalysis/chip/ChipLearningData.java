package com.axi.v810.business.imageAnalysis.chip;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.testProgram.*;
import com.axi.util.image.*;

/**
 * Holds all the information that is learned for a given Chip component.
 * This is a nested class that only exists inside of ChipMeasurementAlgorithm.
 *
 * @author Peter Esbensen
 */
class ChipLearningData
{
  private Map<SliceNameEnum, float[]> _sliceNameEnumToComponentBodyThicknessProfile =
      new HashMap<SliceNameEnum, float[]>();
  private Map<SliceNameEnum, float[]> _sliceNameEnumToComponentDerivativeProfile =
      new HashMap<SliceNameEnum, float[]>();
  private Map<SliceNameEnum, RegionOfInterest> _componentRegionOfInterest =
      new HashMap<SliceNameEnum, RegionOfInterest>();

  //Lim, Lay Ngor - keep the mean value
  private Map<SliceNameEnum, Float> _sliceNameEnumToComponentBodyThicknessMeanValue =
      new HashMap<SliceNameEnum, Float>();

  /**
   * @author Peter Esbensen
   */
  ChipLearningData()
  {
    // do nothing
  }

   /**
   * @author Anthony Fong
   */
  void setComponentRegionOfInterest(SliceNameEnum sliceNameEnum,
                                        RegionOfInterest componentRegionOfInterest)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentRegionOfInterest != null);
    RegionOfInterest previousEntry = _componentRegionOfInterest.put(sliceNameEnum,
                                                           componentRegionOfInterest);
    Assert.expect(previousEntry == null);
  }
  /**
   * @author Anthony Fong
   */
  RegionOfInterest getComponentRegionOfInterest(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_componentRegionOfInterest != null);
    return _componentRegionOfInterest.get(sliceNameEnum);
  }
  /**
   * @author Peter Esbensen
   */
  void setComponentBodyThicknessProfile(SliceNameEnum sliceNameEnum,
                                        float[] componentBodyThicknessProfile)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentBodyThicknessProfile != null);
    float[] previousEntry = _sliceNameEnumToComponentBodyThicknessProfile.put(sliceNameEnum,
                                                                              componentBodyThicknessProfile);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Peter Esbensen
   */
  float[] getComponentBodyThicknessProfile(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_sliceNameEnumToComponentBodyThicknessProfile != null);
    return _sliceNameEnumToComponentBodyThicknessProfile.get(sliceNameEnum);
  }

  /**
   * @author Peter Esbensen
   */
  void setComponentDerivativeProfile(SliceNameEnum sliceNameEnum,
                                     float[] componentDerivativeProfile)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentDerivativeProfile != null);
    float[] previousEntry = _sliceNameEnumToComponentDerivativeProfile.put(sliceNameEnum,
                                                                           componentDerivativeProfile);
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Peter Esbensen
   */
  float[] getComponentDerivativeProfile(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_sliceNameEnumToComponentDerivativeProfile != null);
    return _sliceNameEnumToComponentDerivativeProfile.get(sliceNameEnum);
  }

  /**
   * @author Lim, Lay Ngor - XCR-2027 Exclude Outlier
   */
  void setComponentBodyThicknessMeanValue(SliceNameEnum sliceNameEnum,
                                          float componentBodyThicknessMeanValue)
  {
    Assert.expect(sliceNameEnum != null);
    Float previousEntry = _sliceNameEnumToComponentBodyThicknessMeanValue.put(sliceNameEnum,
                                                                              new Float(componentBodyThicknessMeanValue));
    Assert.expect(previousEntry == null);
  }

  /**
   * @author Lim, Lay Ngor - XCR-2027 Exclude Outlier
   */
  float getComponentBodyThicknessMeanValue(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(_sliceNameEnumToComponentBodyThicknessMeanValue != null);
    
    return _sliceNameEnumToComponentBodyThicknessMeanValue.get(sliceNameEnum);
  }
  }
