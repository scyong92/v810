package com.axi.v810.business.imageAnalysis.chip;

import com.axi.util.*;
import com.axi.util.image.*;

/**
 * This is just a convenience class to hold indices that are useful in ChipMeasurementAlgorithm.
 *
 * @author Peter Esbensen
 */
class ClearChipProfileInfo
{
  private float[] _componentBodyThicknessProfile;
  private int _padOneFilletEdgeIndex = -1;
  private int _padTwoFilletEdgeIndex = -1;
  private int _padOneUpperFilletIndex = -1;
  private int _padOneLowerFilletIndex = -1;
  private int _padTwoUpperFilletIndex = -1;
  private int _padTwoLowerFilletIndex = -1;
  private int _padTwoLeftSideIndex = -1;
  private int _padTwoRightSideIndex = -1;
  private int _padOneLeftSideIndex = -1;
  private int _padOneRightSideIndex = -1;
  private int _padOneLeftSideOpenSignalSearchIndex = -1;
  private int _padOneRightSideOpenSignalSearchIndex = -1;
  private int _padTwoLeftSideOpenSignalSearchIndex = -1;
  private int _padTwoRightSideOpenSignalSearchIndex = -1;
  private float _filletOneThicknessInMils;
  private float _filletTwoThicknessInMils;
  private float _filletOneLowerRegionThicknessInMils;
  private float _filletTwoLowerRegionThicknessInMils;
  //Lim, Lay Ngor - Clear Tombstone - START
  private int _padOneCenterBinIndex = -1;
  private int _padTwoCenterBinIndex = -1;
  private float _padOneCenterThicknessInMils;
  private float _padTwoCenterThicknessInMils;
  private float _padOneCenterThicknessPercentOfUpperFilletThickness;
  private float _padTwoCenterThicknessPercentOfUpperFilletThickness; 
  private int _padOneLeftSideUpperFilletSearchIndex = -1;
  private int _padOneRightSideUpperFilletSearchIndex = -1;
  private int _padTwoLeftSideUpperFilletSearchIndex = -1;
  private int _padTwoRightSideUpperFilletSearchIndex = -1;  
  private float _padOneSolderAreaPercentage = 0.f;
  private float _padTwoSolderAreaPercentage = 0.f;
  //Lim, Lay Ngor - Clear Tombstone - END
  private float _componentBodyLengthInMillimeters;
  private RegionOfInterest _componentOrthogoonalRegionOfInterest;

  /**
   * @author Peter Esbensen
   */
  float[] getComponentBodyThicknessProfile()
  {
    Assert.expect(_componentBodyThicknessProfile != null);
    return _componentBodyThicknessProfile;
  }

  /**
   * @author Peter Esbensen
   */
  void setComponentBodyThicknessProfile(final float[] componentBodyThicknessProfile)
  {
    Assert.expect(componentBodyThicknessProfile != null);
    _componentBodyThicknessProfile = componentBodyThicknessProfile;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadOneFilletEdgeIndex()
  {
    Assert.expect(_padOneFilletEdgeIndex >= 0);
    return _padOneFilletEdgeIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadOneFilletEdgeIndex(final int padOneFilletEdgeIndex)
  {
    Assert.expect(padOneFilletEdgeIndex >= 0);
    _padOneFilletEdgeIndex = padOneFilletEdgeIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadTwoFilletEdgeIndex()
  {
    Assert.expect(_padTwoFilletEdgeIndex >= 0);
    return _padTwoFilletEdgeIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadTwoFilletEdgeIndex(final int padTwoFilletEdgeIndex)
  {
    Assert.expect(padTwoFilletEdgeIndex >= 0);
    _padTwoFilletEdgeIndex = padTwoFilletEdgeIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadOneUpperFilletIndex()
  {
    Assert.expect(_padOneUpperFilletIndex >= 0);
    return _padOneUpperFilletIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadOneUpperFilletIndex(final int padOneFilletIndex)
  {
    Assert.expect(padOneFilletIndex >= 0);
    _padOneUpperFilletIndex = padOneFilletIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadOneLowerFilletIndex()
  {
    Assert.expect(_padOneLowerFilletIndex >= 0);
    return _padOneLowerFilletIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadOneLowerFilletIndex(final int padOneLowerFilletIndex)
  {
    Assert.expect(padOneLowerFilletIndex >= 0);
    _padOneLowerFilletIndex = padOneLowerFilletIndex;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  int getPadOneCenterBinIndex()
  {
    Assert.expect(_padOneCenterBinIndex >= 0);
    return _padOneCenterBinIndex;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadOneCenterBinIndex(final int padOneCenterBinIndex)
  {
    Assert.expect(padOneCenterBinIndex >= 0);
    _padOneCenterBinIndex = padOneCenterBinIndex;
  }  
  
  /**
   * @author Peter Esbensen
   */
  int getPadTwoUpperFilletIndex()
  {
    Assert.expect(_padTwoUpperFilletIndex >= 0);
    return _padTwoUpperFilletIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadTwoUpperFilletIndex(final int padTwoUpperFilletIndex)
  {
    Assert.expect(padTwoUpperFilletIndex >= 0);
    _padTwoUpperFilletIndex = padTwoUpperFilletIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadTwoLowerFilletIndex()
  {
    Assert.expect(_padTwoLowerFilletIndex >= 0);
    return _padTwoLowerFilletIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadTwoLowerFilletIndex(final int padTwoLowerFilletIndex)
  {
    Assert.expect(padTwoLowerFilletIndex >= 0);
    _padTwoLowerFilletIndex = padTwoLowerFilletIndex;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  int getPadTwoCenterBinIndex()
  {
    Assert.expect(_padTwoCenterBinIndex >= 0);
    return _padTwoCenterBinIndex;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadTwoCenterBinIndex(final int padTwoCenterBinIndex)
  {
    Assert.expect(padTwoCenterBinIndex >= 0);
    _padTwoCenterBinIndex = padTwoCenterBinIndex;
  }  

  /**
   * @author Peter Esbensen
   */
  int getPadTwoLeftSideIndex()
  {
    Assert.expect(_padTwoLeftSideIndex >= 0);
    return _padTwoLeftSideIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadTwoLeftSideIndex(final int padTwoLeftSideIndex)
  {
    Assert.expect(padTwoLeftSideIndex >= 0);
    _padTwoLeftSideIndex = padTwoLeftSideIndex;
  }


  /**
   * @author Peter Esbensen
   */
  int getPadTwoRightSideIndex()
  {
    Assert.expect(_padTwoRightSideIndex >= 0);
    return _padTwoRightSideIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadTwoRightSideIndex(final int padTwoRightSideIndex)
  {
    Assert.expect(padTwoRightSideIndex >= 0);
    _padTwoRightSideIndex = padTwoRightSideIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadOneLeftSideIndex()
  {
    Assert.expect(_padOneLeftSideIndex >= 0);
    return _padOneLeftSideIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadOneLeftSideIndex(final int padOneLeftSideIndex)
  {
    Assert.expect(padOneLeftSideIndex >= 0);
    _padOneLeftSideIndex = padOneLeftSideIndex;
  }


  /**
   * @author Peter Esbensen
   */
  int getPadOneRightSideIndex()
  {
    Assert.expect(_padOneRightSideIndex >= 0);
    return _padOneRightSideIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadOneRightSideIndex(final int padOneRightSideIndex)
  {
    Assert.expect(padOneRightSideIndex >= 0);
    _padOneRightSideIndex = padOneRightSideIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadOneLeftSideOpenSignalSearchIndex()
  {
    Assert.expect(_padOneLeftSideOpenSignalSearchIndex >= 0);
    return _padOneLeftSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadOneLeftSideOpenSignalSearchIndex(final int padOneLeftSideOpenSignalSearchIndex)
  {
    Assert.expect(padOneLeftSideOpenSignalSearchIndex >= 0);
    _padOneLeftSideOpenSignalSearchIndex = padOneLeftSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadOneRightSideOpenSignalSearchIndex()
  {
    Assert.expect(_padOneRightSideOpenSignalSearchIndex >= 0);
    return _padOneRightSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadOneRightSideOpenSignalSearchIndex(final int padOneRightSideOpenSignalSearchIndex)
  {
    Assert.expect(padOneRightSideOpenSignalSearchIndex >= 0);
    _padOneRightSideOpenSignalSearchIndex = padOneRightSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadTwoLeftSideOpenSignalSearchIndex()
  {
    Assert.expect(_padTwoLeftSideOpenSignalSearchIndex >= 0);
    return _padTwoLeftSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadTwoLeftSideOpenSignalSearchIndex(final int padTwoLeftSideOpenSignalSearchIndex)
  {
    Assert.expect(padTwoLeftSideOpenSignalSearchIndex >= 0);
    _padTwoLeftSideOpenSignalSearchIndex = padTwoLeftSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  int getPadTwoRightSideOpenSignalSearchIndex()
  {
    Assert.expect(_padTwoRightSideOpenSignalSearchIndex >= 0);
    return _padTwoRightSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  void setPadTwoRightSideOpenSignalSearchIndex(final int padTwoRightSideOpenSignalSearchIndex)
  {
    Assert.expect(padTwoRightSideOpenSignalSearchIndex >= 0);
    _padTwoRightSideOpenSignalSearchIndex = padTwoRightSideOpenSignalSearchIndex;
  }

  /**
   * @author Peter Esbensen
   */
  float getFilletOneThicknessInMils()
  {
    return _filletOneThicknessInMils;
  }

  /**
   * @author Peter Esbensen
   */
  void setFilletOneThicknessInMils(final float filletOneThicknessInMils)
  {
    _filletOneThicknessInMils = filletOneThicknessInMils;
  }

  /**
   * @author Peter Esbensen
   */
  float getFilletTwoThicknessInMils()
  {
    return _filletTwoThicknessInMils;
  }

  /**
   * @author Peter Esbensen
   */
  void setFilletTwoThicknessInMils(final float filletTwoThicknessInMils)
  {
    _filletTwoThicknessInMils = filletTwoThicknessInMils;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  float getPadOneCenterThicknessInMils()
  {
    return _padOneCenterThicknessInMils;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadOneCenterThicknessInMils(final float padOneCenterThicknessInMils)
  {
    _padOneCenterThicknessInMils = padOneCenterThicknessInMils;
  } 

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  float getPadTwoCenterThicknessInMils()
  {
    return _padTwoCenterThicknessInMils;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadTwoCenterThicknessInMils(final float padTwoCenterThicknessInMils)
  {
    _padTwoCenterThicknessInMils = padTwoCenterThicknessInMils;
  } 
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  float getPadOneCenterThicknessPercentOfUpperFilletThickness()
  {
    return _padOneCenterThicknessPercentOfUpperFilletThickness;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadOneCenterThicknessPercentOfUpperFilletThickness(final float padOneCenterThicknessPercentOfUpperFilletThickness)
  {
    _padOneCenterThicknessPercentOfUpperFilletThickness = padOneCenterThicknessPercentOfUpperFilletThickness;
  }     
    
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  float getPadTwoCenterThicknessPercentOfUpperFilletThickness()
  {
    return _padTwoCenterThicknessPercentOfUpperFilletThickness;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadTwoCenterThicknessPercentOfUpperFilletThickness(final float padTwoCenterThicknessPercentOfUpperFilletThickness)
  {
    _padTwoCenterThicknessPercentOfUpperFilletThickness = padTwoCenterThicknessPercentOfUpperFilletThickness;
  }  
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  int getPadOneLeftSideUpperFilletSearchIndex()
  {
    Assert.expect(_padOneLeftSideUpperFilletSearchIndex >= 0);
    return _padOneLeftSideUpperFilletSearchIndex;
  }

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  void setPadOneLeftSideUpperFilletSearchIndex(final int padOneLeftSideUpperFilletSearchIndex)
  {
    Assert.expect(padOneLeftSideUpperFilletSearchIndex >= 0);
    _padOneLeftSideUpperFilletSearchIndex = padOneLeftSideUpperFilletSearchIndex;
  }

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  int getPadOneRightSideUpperFilletSearchIndex()
  {
    Assert.expect(_padOneRightSideUpperFilletSearchIndex >= 0);
    return _padOneRightSideUpperFilletSearchIndex;
  }

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  void setPadOneRightSideUpperFilletSearchIndex(final int padOneRightSideUpperFilletSearchIndex)
  {
    Assert.expect(padOneRightSideUpperFilletSearchIndex >= 0);
    _padOneRightSideUpperFilletSearchIndex = padOneRightSideUpperFilletSearchIndex;
  }

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  int getPadTwoLeftSideUpperFilletSearchIndex()
  {
    Assert.expect(_padTwoLeftSideUpperFilletSearchIndex >= 0);
    return _padTwoLeftSideUpperFilletSearchIndex;
  }

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  void setPadTwoLeftSideUpperFilletSearchIndex(final int padTwoLeftSideUpperFilletSearchIndex)
  {
    Assert.expect(padTwoLeftSideUpperFilletSearchIndex >= 0);
    _padTwoLeftSideUpperFilletSearchIndex = padTwoLeftSideUpperFilletSearchIndex;
  }

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  int getPadTwoRightSideUpperFilletSearchIndex()
  {
    Assert.expect(_padTwoRightSideUpperFilletSearchIndex >= 0);
    return _padTwoRightSideUpperFilletSearchIndex;
  }

  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  void setPadTwoRightSideUpperFilletSearchIndex(final int padTwoRightSideUpperFilletSearchIndex)
  {
    Assert.expect(padTwoRightSideUpperFilletSearchIndex >= 0);
    _padTwoRightSideUpperFilletSearchIndex = padTwoRightSideUpperFilletSearchIndex;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  float getPadOneSolderAreaPercentage()
  {
    return _padOneSolderAreaPercentage;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadOneSolderAreaPercentage(final float padOneSolderAreaPercentage)
  {
    _padOneSolderAreaPercentage = padOneSolderAreaPercentage;
  } 
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */
  float getPadTwoSolderAreaPercentage()
  {
    return _padTwoSolderAreaPercentage;
  }
  
  /**
   * @author Lim, Lay Ngor - Clear Tomstone
   */  
  void setPadTwoSolderAreaPercentage(final float padTwoSolderAreaPercentage)
  {
    _padTwoSolderAreaPercentage = padTwoSolderAreaPercentage;
  }      
  
  /**
   * @author Peter Esbensen
   */
  float getFilletOneLowerRegionThicknessInMils()
  {
    return _filletOneLowerRegionThicknessInMils;
  }

  /**
   * @author Peter Esbensen
   */
  void setFilletOneLowerRegionThicknessInMils(final float filletOneLowerRegionThicknessInMils)
  {
    _filletOneLowerRegionThicknessInMils = filletOneLowerRegionThicknessInMils;
  }

  /**
   * @author Peter Esbensen
   */
  float getFilletTwoLowerRegionThicknessInMils()
  {
    return _filletTwoLowerRegionThicknessInMils;
  }

  /**
   * @author Peter Esbensen
   */
  void setFilletTwoLowerRegionThicknessInMils(final float filletTwoLowerRegionThicknessInMils)
  {
    _filletTwoLowerRegionThicknessInMils = filletTwoLowerRegionThicknessInMils;
  }

  /**
   * @author Peter Esbensen
   */
  float getComponentBodyLengthInMillimeters()
  {
    return _componentBodyLengthInMillimeters;
  }

  /**
   * @author Peter Esbensen
   */
  void setComponentBodyLengthInMillimeters(final float componentBodyLengthInMillimeters)
  {
    _componentBodyLengthInMillimeters = componentBodyLengthInMillimeters;
  }

  /**
   * @author Peter Esbensen
   */
  void setComponentOrthogonalRegionOfInterest(final RegionOfInterest componentRegionOfInterest)
  {
    Assert.expect(componentRegionOfInterest != null);
    _componentOrthogoonalRegionOfInterest = componentRegionOfInterest;
  }

  /**
   * @author Peter Esbensen
   */
  RegionOfInterest getComponentOrthogonalRegionOfInterest()
  {
    Assert.expect(_componentOrthogoonalRegionOfInterest != null);
    return _componentOrthogoonalRegionOfInterest;
  }
}
