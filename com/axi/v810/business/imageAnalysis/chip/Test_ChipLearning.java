package com.axi.v810.business.imageAnalysis.chip;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.algorithmLearning.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
class Test_ChipLearning extends AlgorithmUnitTest
{
  private static final String _RESISTOR_SUBTYPE_TO_TEST = "sm00135_Resistor";
  private List<AlgorithmSettingEnum> _learnedSettings = new ArrayList<AlgorithmSettingEnum>();
  private Map<AlgorithmSettingEnum, Float> _learnedSettingToExpectedNormalLearnedValueMap = new HashMap<AlgorithmSettingEnum,Float>();
  private Map<AlgorithmSettingEnum, Float> _learnedSettingToExpectedBlackImageLearnedValueMap = new HashMap<AlgorithmSettingEnum,Float>();
  private Map<AlgorithmSettingEnum, Float> _learnedSettingToExpectedWhiteImageLearnedValueMap = new HashMap<AlgorithmSettingEnum,Float>();

  /**
   * @author Peter Esbensen
   */
  public Test_ChipLearning()
  {
    // do nothing
  }

  /**
   * @todo add in opaque chip if we ever get opaque chip images
   *
   * @author Peter Esbensen
   */
  private void setLearnedSettings()
  {
    Assert.expect(_learnedSettings != null);
    Assert.expect(_learnedSettingToExpectedNormalLearnedValueMap != null);
    Assert.expect(_learnedSettingToExpectedBlackImageLearnedValueMap != null);
    Assert.expect(_learnedSettingToExpectedWhiteImageLearnedValueMap != null);

    _learnedSettings.clear();
    InspectionFamily chipInspectionFamily = InspectionFamily.getInstance(InspectionFamilyEnum.CHIP);
    for (Algorithm algorithm : chipInspectionFamily.getAlgorithms())
    {
      _learnedSettings.addAll(algorithm.getLearnedAlgorithmSettingEnums());
    }

    _learnedSettingToExpectedNormalLearnedValueMap.clear();
    _learnedSettingToExpectedNormalLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH, 0.96f);
    _learnedSettingToExpectedNormalLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS, 0.0433f);
    _learnedSettingToExpectedNormalLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE, 2.54f);
    _learnedSettingToExpectedNormalLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET, 40.0f);

    _learnedSettingToExpectedBlackImageLearnedValueMap.clear();
    _learnedSettingToExpectedBlackImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH, 0.96f);
    _learnedSettingToExpectedBlackImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS, 0.0254f);
    _learnedSettingToExpectedBlackImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE, 2.54f);
    _learnedSettingToExpectedBlackImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET, 0.0f);

    _learnedSettingToExpectedWhiteImageLearnedValueMap.clear();
    _learnedSettingToExpectedWhiteImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH, 101.417f);
    _learnedSettingToExpectedWhiteImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS, 0.0f);
    _learnedSettingToExpectedWhiteImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE, 2.54f);
    _learnedSettingToExpectedWhiteImageLearnedValueMap.put(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET, 26.2f);
  }

  /**
   * @author Peter Esbensen
   */
  protected void testBlackImages(Project project,
                                 Subtype subtype) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "testChipBlackImageSet",
                                                                   POPULATED_BOARD);
    generateBlackImageSet(project, imageSetData);


    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // check the learned setting values
//    checkLearnedValuesAsPercentOfExpected(subtype, _learnedSettingToExpectedBlackImageLearnedValueMap, 2.0f);
  }

  /**
   * @author Peter Esbensen
   */
  protected void testWhiteImages(Project project,
                                 Subtype subtype) throws DatastoreException
  {
    Assert.expect(project != null);
    Assert.expect(subtype != null);

    // Create image run and learn
    List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
    ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                   subtype,
                                                                   "testChipWhiteImageSet",
                                                                   POPULATED_BOARD);
    generateWhiteImageSet(project, imageSetData);

    imageSetDataList.add(imageSetData);
    learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

    // check the learned setting values
//    checkLearnedValuesAsPercentOfExpected(subtype, _learnedSettingToExpectedWhiteImageLearnedValueMap, 2.0f);
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Assert.expect(is != null);
    Assert.expect(os != null);

    setLearnedSettings();

    Project project = getProject("FAMILIES_ALL_RLV");
    Panel panel = project.getPanel();

    try
    {
      deleteAllLearnedData(project);

      Subtype subtype = panel.getSubtype(_RESISTOR_SUBTYPE_TO_TEST);

      // Check that the settings are set to the default values
      verifyAllSettingsAreAtDefault(subtype, _learnedSettings);

      // do learning
      List<ImageSetData> imageSetDataList = new ArrayList<ImageSetData>();
      ImageSetData imageSetData = createImageSetAndSetProgramFilters(project,
                                                                     ALL_FAMILES_RLV_IMAGE_SET_CREATED_BY_MAKEFILE,
                                                                     POPULATED_BOARD);
      imageSetDataList.add(imageSetData);
      learnSubtypeUnitTest(project, subtype, imageSetDataList, _LEARN_SUBTYPES, _LEARN_SHORT);

      // make sure the settings are no longer at their defaults
      List<AlgorithmSettingEnum> learnedAlgorithmSettingEnums = new ArrayList<AlgorithmSettingEnum>(_learnedSettings);
      // remove the opaque nominal setting since we won't be learning that
      learnedAlgorithmSettingEnums.remove(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS);
      learnedAlgorithmSettingEnums.remove(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH);
      verifyAllSettingsAreNotAtDefault(subtype, learnedAlgorithmSettingEnums);

      // check the number of datapoints
      int numberOfDatapoints = getNumberOfMeasurements(subtype, SliceNameEnum.CLEAR_CHIP_PAD,
          MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS);
      String expectString = "Actual: " + numberOfDatapoints;
      Expect.expect((numberOfDatapoints == 6), expectString);

      // check the learned setting values
//      checkLearnedValuesAsPercentOfExpected(subtype, _learnedSettingToExpectedNormalLearnedValueMap, 2.0f);

      // do learning on black images
      testBlackImages(project, subtype);

      // do learning on white images
      /** @todo: mdw - figure out why we get short region gray level profiles slightly above 255 here */
//      testWhiteImages(project, subtype);

      // Delete subtype data; make sure values are back to their defaults
      AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = project.getAlgorithmSubtypeLearningReaderWriter();
      algorithmSubtypeLearning.open();
      subtype.deleteLearnedAlgorithmSettingsData();
      algorithmSubtypeLearning.close();

      // explicitly reset settings to defaults since deleting learned data in chip doesn't automatically reset them for us
      for (AlgorithmSettingEnum algorithmSettingEnum : InspectionFamily.getAlgorithm(InspectionFamilyEnum.CHIP, AlgorithmEnum.MEASUREMENT).getLearnedAlgorithmSettingEnums())
      {
        subtype.setSettingToDefaultValue(algorithmSettingEnum);
      }

      testLearningOnlyUnloadedBoards(project, subtype, _learnedSettings);

      verifyAllSettingsAreAtDefault(subtype, _learnedSettings);
      verifyChipDatabaseIsEmpty(subtype);

      deleteLearnedData(subtype);
    }
    catch (XrayTesterException xtex)
    {
      xtex.printStackTrace();
      Assert.expect(false);
    }

  }

  /**
   * @author Peter Esbensen
   */
  void verifyChipDatabaseIsEmpty(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    AlgorithmSubtypesLearningReaderWriter algorithmSubtypeLearning = subtype.getPanel().getProject().getAlgorithmSubtypeLearningReaderWriter();
    algorithmSubtypeLearning.open();

    SliceNameEnum slice = SliceNameEnum.CLEAR_CHIP_PAD;
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
                                                           true,
                                                           true).length == 0);
    Expect.expect(subtype.getLearnedJointMeasurementValues(slice,
                                                           MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH,
                                                           true,
                                                           true).length == 0);
    algorithmSubtypeLearning.close();
  }


  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ChipLearning());
  }

}
