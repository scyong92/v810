package com.axi.v810.business.imageAnalysis.chip;

import java.io.*;
import java.util.*;
import java.util.List;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.util.image.Filter;
import com.axi.util.image.Image;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelDesc.Component;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;


/**
 * Algorithm for measuring all capacitors and resistors.  Polarized capacitors are tested with the separate
 * Polarized Capacitor InspectionFamily.<br>
 * <b>NOTE:  These comments are best viewed in javadoc mode since I am linking
 * to pictures.  IF YOU HAVEN'T DONE SO, DO A MAKE DOCS IN THE XRAYTEST DIRECTORY TO SEE THE IMAGES.</b> <p>
 *
 * <h2>Algorithm Procedure</h2>
 *
 * <h3>Run Locator</h3>
 *
 * <h3>Take Body Thickness measurement</h3>
 * Using the �Pad_Clear� slice, take an average gray level reading in the middle
 * of the component.  Use the middle 50% of the area between the two pads.
 *
 * <h3>Decide whether this should be tested as clear or opaque</h3>
 *
 * If the body thickness is equal or greater to the
 * "Body Thickness Required for Opaque Test" threshold then do the �Opaque Only�
 * measurements below and skip the �Clear Only� measurements
 * (and vice versa if the thickness is too low).
 *
 * <h2>Opaque-Only Measurements</h2>
 *
 * <h3>Create background-corrected profile of entire chip</h3>
 * Take a profile along the chip.  Use two profiles on the side of the chip to
 * estimate the background and use that to remove background effect from the
 * chip profile.  In the following graphic, these profile regions are shown
 * in the dark blue color.
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-1.gif"></center>
 *
 * <h3>Locate Fillets</h3>
 * Convert the profile to a derivative profile.  These are centered on the
 * maximum derivative on the left and the minimum derivative no the right.  This
 * can be seen in the following graphic, ignoring the regions highlighted in
 * white.
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-2.gif"></center>
 *
 * <h3>Compute body length</h3>
 * This is simply the distance between the fillet locations from the previous
 * step.
 *
 * <h3>Measure fillet thicknesses</h3>
 * Measure the average thickness in the profile in the areas centered on the
 * fillet locations.  These regions are half of the CAD pad length.  In the
 * following graphic, they are shown in green and blue.
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-3.gif"></center>
 *
 * <h3>Measure any "gap edge"</h3>
 * Search the derivative profile in regions outside the fillet locations for any
 * slopes in the opposite direction of what we would expect.  These regions start
 * one-third of a pad length to the outside of the fillet regions.  These
 * regions are shown in white in the following graphic.  In this graphic there
 * are no slopes ("gap edges") found.  If there was, the measurement would just
 * be the maximum absolute value of the unexpected slope.  If no unexpected slope
 * is found, the gap edge measurement is set to zero.
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-2.gif"></center>
 *
 * <h3>Measure component position across</h3>
 * Take two profiles across the chip, just to the inside of the fillet regions.
 * Convert them to derivative profiles, like this:
 * Use the maximum derivative on the left and the minimum derivative on the right
 * as the locations of the component edges.
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-4.gif"></center>
 *
 * <h2>Clear-Only Measurements</h2>
 *
 * Use the �Pad_Clear� slice for all these measurements.
 *
 * <h3>Create background-corrected thickness profile of entire chip</h3>
 * Use the same procedure as described above in the Opaque section.
 *
 * <h3>Locate component body</h3>
 *
 * This is somewhat more complicated than the 5dx since people have complained about the quality
 * of the locations with the 5dx method.  This depends on a good Nominal Body Length setting.
 *
 * <ol>
 * <li>	Use the derivative profile to find two points that are exactly Nominal Body Length apart and have the steepest slopes.
 * This corresponds perfectly to the inside edge of the fillets.
 * <li>	If the joints are strange (or open), there won't be two points that are exactly Nominal Body Length apart that have
 * their slopes going down towards the middle of the component.  Draw yourself a picture to see this.  In this case, we
 * continue on to the legacy 5dx method next
 * <li>	Locate the edges using the Reflection technique, which looks for the best correlation between the two fillets (after
 * reflecting one so that they have the same orientation).
 * </ol>
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-5.gif"></center>
 *
 * <h3>Search/Locate the Fillet thickness regions</h3>
 *
 * There will be two fillet regions.  One, called the upper fillet region,
 * should be located at the peak of the fillet.  The other one, called the lower
 * fillet region, is placed to the side the upper region and is used to distinguish
 * between the fillet shapes of good and bad joints.  The Upper and Lower Fillet Locations
 * define the location of the upper and lower fillet region relative to the fillet edge, as found in the previous
 * Locate the Component Body step.
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-6.gif"></center>
 *
 * If the Open Signal Search Length setting is nonzero, then we search for Upper
 * and Lower Fillet locations that maximize their difference in thickness.
 * The Upper and Lower Fillet locations are restricted to be Lower Fillet Location
 * Offset in distance apart, but are allowed to slide from side-to-side to
 * maximize the thickness difference.
 *
 * <br><center><img src="./doc-files/ChipMeasurementAlgorithm-7.gif"></center>
 *
 * <p>
 * <h2>Other notes</h2>
 * In orientation zero, the pads should look like this.  This is how they appear in the profiles, with pad two on the
 * left and pad one on the right.
 *
 *<pre>
 *___________________________________
 *|           |         |           |
 *|           |         |           |
 *|    2      |         |     1     |
 *|           |         |           |
 *|___________|_________|___________|
 *</pre>
 *
 * @author Peter Esbensen
 */
public class ChipMeasurementAlgorithm extends Algorithm
{
  static private final int _PROFILE_SMOOTHING_STEP_SIZE = 7;
  static private final float _MILLIMETERS_THICKNESS_REQUIRED_TO_BE_CONSIDERED_AS_OPAQUE_DURING_LEARNING = 0.05f;
  static private final float _OPAQUE_CHIP_PROFILE_SCALE_FACTOR = 4.0f;
  static private final float _CLEAR_CHIP_PROFILE_SCALE_FACTOR = 2.0f;
  static private final double _PROFILE_EXTENSION = 0.15;


//  static private ClearChipProfileInfo _clearChipProfileInfo;
//  static private double _padOneUpperFilletPosition;
//  static private double _padTwoUpperFilletPosition;
  
  //ShengChuan - Clear Tombstone - Roundness Detection
  static private final int  _NOISE_AREA_DETECTED = 5;
  static private final int _NUMBER_OF_HISTOGRAM_BIN = 256;

  //private static final String _defaultSearchSpeed = Config.is64bitIrp() ? "slow" : "auto";
  
  // XCR-2859 Default Search Speed to Slow
  // Ee Jun Jiang
  // default search speed always is slow in 5.8
  protected static final String _defaultSearchSpeed = "slow";

  /**
   * @author Peter Esbensen
   */
  public ChipMeasurementAlgorithm(InspectionFamily chipInspectionFamily)
  {
    super(AlgorithmEnum.MEASUREMENT, InspectionFamilyEnum.CHIP);

    Assert.expect(chipInspectionFamily != null);
    Assert.expect(_learnedAlgorithmSettingEnums != null);
    _learnedAlgorithmSettingEnums.clear();

    int displayOrder = 1;
    int currentVersion = 1;

    Collection<AlgorithmSetting> locatorAlgorithmSettings = Locator.createAlgorithmSettings(displayOrder, currentVersion);

    // Add the shared locator settings.
    for (AlgorithmSetting algSetting : locatorAlgorithmSettings)
      addAlgorithmSetting(algSetting);

    displayOrder += locatorAlgorithmSettings.size();

    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    AlgorithmSetting learnedAlongOffsetOpaque = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE,
        displayOrder++,
        20.f, // default
        -100.f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    learnedAlongOffsetOpaque.filter(JointTypeEnum.RESISTOR);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              learnedAlongOffsetOpaque,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              learnedAlongOffsetOpaque,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              learnedAlongOffsetOpaque,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              learnedAlongOffsetOpaque,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);


    addAlgorithmSetting(learnedAlongOffsetOpaque);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE);

    AlgorithmSetting learnedAlongOffsetClear = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR,
        displayOrder++,
        20.f, // default
        -100.f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    //learnedAlongOffsetClear.filter(JointTypeEnum.RESISTOR);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              learnedAlongOffsetClear,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              learnedAlongOffsetClear,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              learnedAlongOffsetClear,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              learnedAlongOffsetClear,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    // Wei Chin (Tall Cap)
//    learnedAlongOffsetClear.filter(JointTypeEnum.TALL_CAPACITOR);
    addAlgorithmSetting(learnedAlongOffsetClear);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR);

    AlgorithmSetting learnedOffsetRotation = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION,
        displayOrder++,
        20.f, // default
        0.f, // min
        360.f, // max
        MeasurementUnitsEnum.DEGREES,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION)_KEY", // image file
        AlgorithmSettingTypeEnum.HIDDEN,
        currentVersion);
//     learnedOffsetRotation.filter(JointTypeEnum.RESISTOR);
   chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              learnedOffsetRotation,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);
   chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              learnedOffsetRotation,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);
   chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              learnedOffsetRotation,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);
   // Wei Chin (Tall Cap)
//   chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              learnedOffsetRotation,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);

    addAlgorithmSetting(learnedOffsetRotation);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION);

    AlgorithmSetting opaqueNominalBodyLength = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH,
        displayOrder++,
        1.524f, // default
        0.0254f, // min
        10.16f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    opaqueNominalBodyLength.filter(JointTypeEnum.RESISTOR);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              opaqueNominalBodyLength,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              opaqueNominalBodyLength,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH);

    addAlgorithmSetting(opaqueNominalBodyLength);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH);

    AlgorithmSetting clearNominalBodyLength = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH,
        displayOrder++,
        1.524f, // default
        0.0254f, // min
        10.16f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              clearNominalBodyLength,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              clearNominalBodyLength,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH);
    // Wei Chin (Tall Cap)
//    clearNominalBodyLength.filter(JointTypeEnum.TALL_CAPACITOR);
    addAlgorithmSetting(clearNominalBodyLength);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);

    AlgorithmSetting nominalOpaquePadThickness = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS,
        displayOrder++,
        0.127f, // default
        0.0254f, // min
        1.27f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_PAD_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_PAD_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_NOMINAL_PAD_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    nominalOpaquePadThickness.filter(JointTypeEnum.RESISTOR);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              nominalOpaquePadThickness,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              nominalOpaquePadThickness,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS);

    addAlgorithmSetting(nominalOpaquePadThickness);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS);

    AlgorithmSetting nominalClearPadThickness = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS,
        displayOrder++,
        0.1016f, // default
        0.0254f, // min
        1.27f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              nominalClearPadThickness,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              nominalClearPadThickness,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS);

    // Wei Chin (Tall Cap)
//    nominalClearPadThickness.filter(JointTypeEnum.TALL_CAPACITOR);
    addAlgorithmSetting(nominalClearPadThickness);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);

    AlgorithmSetting bodyThicknessRequiredToTestAsOpaque = new AlgorithmSetting(AlgorithmSettingEnum.
        CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE,
        displayOrder++,
        0.0f, // default
        0.0f, // min
        2.54f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    bodyThicknessRequiredToTestAsOpaque.filter(JointTypeEnum.RESISTOR);
    // Wei Chin (Tall Cap)
//    bodyThicknessRequiredToTestAsOpaque.filter(JointTypeEnum.TALL_CAPACITOR);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              bodyThicknessRequiredToTestAsOpaque,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS);

    addAlgorithmSetting(bodyThicknessRequiredToTestAsOpaque);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);
       
    //Lim, Lay Ngor - Clear Tombstone
    //Upper fillet/heel detection method
    ArrayList<String> upperFilletDetectionmMethodOptions = new ArrayList<String>(Arrays.asList("BestSignal", "Thickness"));
    AlgorithmSetting upperFilletDetectionMethodSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD,
      displayOrder++,
      "BestSignal", // default - BestSignal is previous method, Thickness is Gullwing heel detection method
      upperFilletDetectionmMethodOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD)_KEY", // desc
      "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD)_KEY", // detailed desc
      "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(upperFilletDetectionMethodSetting);
    //Lim, Lay Ngor - Clear Tombstone - End  
    
    AlgorithmSetting lowerFilletLocationOffset = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET,
        displayOrder++,
        20.f, // default
        -100.f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(lowerFilletLocationOffset);
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET);

    AlgorithmSetting openSignalSearchLength = new AlgorithmSetting(AlgorithmSettingEnum.
        CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH,
        displayOrder++,
        0.f, // default
        0.0f, // min
        500.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(openSignalSearchLength);

    AlgorithmSetting upperFilletLocationOffset = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET,
        displayOrder++,
        0f, // default
        -100f, // min
        100f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(upperFilletLocationOffset);
    //Ngie Xing, XCR-2152 - Reset Algorithm Settings button does not reset all algorithm settings
    _learnedAlgorithmSettingEnums.add(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET);
    
    //Lim, Lay Ngor - Clear Tombstone
    //duplicate Gullwing feature to chip - GULLWING_MEASUREMENT_HEEL_EDGE_SEARCH_THICKNESS_PERCENT 
    // Target heel edge thickess as a percent of max pad profile thickness.
    AlgorithmSetting heelEdgeSearchThicknessPercentSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT,
      displayOrder++,
      60.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG,
      "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // desc
      "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // detailed desc
      "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(heelEdgeSearchThicknessPercentSetting);
    //Gullwing have learning, for chip we skip learning.
    
    //duplicate Gullwing feature to chip - GULLWING_MEASUREMENT_HEEL_SEARCH_DISTANCE_FROM_EDGE 
    // Heel peak search distance from heel edge.
    AlgorithmSetting heelSearchDistanceFromEdgeSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE,
        displayOrder++,
        30.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(heelSearchDistanceFromEdgeSetting);    
    //Gullwing have learning, for chip we skip learning.  

    //duplicate Gullwing feature to chip - GULLWING_MEASUREMENT_CENTER_OFFSET 
    // Center offset from upper fillet peak (percent of upper to lower fillet length).
    AlgorithmSetting centerOffsetFromUpperFilletSetting = new AlgorithmSetting(
      AlgorithmSettingEnum.CHIP_MEASUREMENT_CENTER_OFFSET,
      displayOrder++,
      60.f, // default
      0.0f, // min
      100.f, // max
      MeasurementUnitsEnum.PERCENT_OF_FILLET_LENGTH,
      "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CENTER_OFFSET_FROM_UPPER_FILLET)_KEY", // desc
      "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CENTER_OFFSET_FROM_UPPER_FILLET)_KEY", // detailed desc
      "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_CENTER_OFFSET_FROM_UPPER_FILLET)_KEY", // image file
      AlgorithmSettingTypeEnum.STANDARD,
      currentVersion);
    addAlgorithmSetting(centerOffsetFromUpperFilletSetting);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              centerOffsetFromUpperFilletSetting,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              centerOffsetFromUpperFilletSetting,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
    //Lim, Lay Ngor - Clear Tombstone - end
    
    AlgorithmSetting backgroundRegionLocation = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT,
        displayOrder++,
        50f, // default
        0f, // min
        200f, // max
        MeasurementUnitsEnum.PERCENT_OF_INTERPAD_DISTANCE,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(backgroundRegionLocation);

    AlgorithmSetting bodyEdgePercent = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT,
        displayOrder++,
        50.f, // default
        0.0f, // min
        100.f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(bodyEdgePercent);
    bodyEdgePercent.filter(JointTypeEnum.RESISTOR);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting clearChipSliceheightSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(40.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(USER_DEFINED_CLEAR_CHIP_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    // Wei Chin (Tall Cap)
//    clearChipSliceheightSetting.filter(JointTypeEnum.TALL_CAPACITOR);
    addAlgorithmSetting(clearChipSliceheightSetting);

    /** @todo PE figure out default value */
    /** @todo PE figure out minimum and maximum values */
    // Note:  This sliceheight setting is not used by the algorithms at all.  It is just used by test generation to set the sliceheights.
    AlgorithmSetting opaqueChipSliceheightSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
        MathUtil.convertMilsToMillimeters(40.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(USER_DEFINED_OPAQUE_CHIP_SLICEHEIGHT)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    opaqueChipSliceheightSetting.filter(JointTypeEnum.RESISTOR);
    // Wei Chin (Tall Cap)
//    opaqueChipSliceheightSetting.filter(JointTypeEnum.TALL_CAPACITOR);
    addAlgorithmSetting(opaqueChipSliceheightSetting);

    // Wei Chin (Tall Cap)
//    AlgorithmSetting tallCapChipSliceheightSetting = new AlgorithmSetting(
//        AlgorithmSettingEnum.USER_DEFINED_PAD_SLICEHEIGHT, // setting enum
//        displayOrder++, // display order,
//        MathUtil.convertMilsToMillimeters(0.0f), // default value
//        MathUtil.convertMilsToMillimeters( -40.0f), // minimum value
//        MathUtil.convertMilsToMillimeters(40.0f), // maximum value
//        MeasurementUnitsEnum.MILLIMETERS,
//        "HTML_DESC_CHIP_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // desc
//        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // detailed desc
//        "IMG_DESC_CHIP_MEASUREMENT_(USER_DEFINED_PAD_SLICEHEIGHT)_KEY", // image file
//        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
//        currentVersion);
//    tallCapChipSliceheightSetting.filter(JointTypeEnum.RESISTOR);
//    tallCapChipSliceheightSetting.filter(JointTypeEnum.CAPACITOR);
//    addAlgorithmSetting(tallCapChipSliceheightSetting);

    // Wei Chin (Pin offset)
      AlgorithmSetting pinOffset = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_REFERENCES_FROM_PLANE, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters(-300.0f), // minimum value
        MathUtil.convertMilsToMillimeters(300.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // description URL Key
        "HTML_DETAILED_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // detailed description URL Key
        "IMG_DESC_USER_DEFINED_(REFERENCES_FROM_PLANE)_KEY", // image description URL Key
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(pinOffset);
    
    // Added by Lee Herng (4 Mar 2011)
    AlgorithmSetting autoFocusMidBoardOffsetSetting = new AlgorithmSetting(
        AlgorithmSettingEnum.USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET, // setting enum
        displayOrder++, // display order,
        MathUtil.convertMilsToMillimeters(0.0f), // default value
        MathUtil.convertMilsToMillimeters( -200.0f), // minimum value
        MathUtil.convertMilsToMillimeters(200.0f), // maximum value
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(USER_DEFINED_AUTOFOCUS_MIDBOARD_OFFSET)_KEY", // image file
        AlgorithmSettingTypeEnum.SLICE_HEIGHT,
        currentVersion);
    addAlgorithmSetting(autoFocusMidBoardOffsetSetting);
    
    ArrayList<String> focusConfirmationOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_FOCUS_CONFIRMATION,
      displayOrder++,
      "On",
      focusConfirmationOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_CHIP_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // description URL Key
      "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // detailed description URL Key
      "IMG_DESC_CHIP_MEASUREMENT_(USER_DEFINED_FOCUS_CONFIRMATION)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT,
      currentVersion));

    // Added by Khang Wah, 2013-09-10, user-define wavelet level
    ArrayList<String> allowableWaveletLevelValues = new ArrayList<String>(Arrays.asList("auto","fast","medium","slow"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.USER_DEFINED_WAVELET_LEVEL, // setting enum
      1999, // this is done to make sure this threshold is right before psh in the Slice Setup tab
      _defaultSearchSpeed, // default value
      allowableWaveletLevelValues, 
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // description URL Key
      "HTML_DETAILED_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // detailed description URL Key
      "IMG_DESC_GRIDARRAY_MEASUREMENT_(USER_DEFINED_WAVELET_LEVEL)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));
    
    // Added by Lee Herng, 2015-03-27, psp local search
    addAlgorithmSetting(SharedPspAlgorithm.createPspLocalSearchAlgorithmSettings(2000, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - low limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeLowLimitAlgorithmSettings(2001, currentVersion));
    
    // Added by Lee Herng, 2014-08-13, search range - high limit    
    addAlgorithmSetting(SharedPspAlgorithm.createPspSearchRangeHighLimitAlgorithmSettings(2002, currentVersion));
    
    // Added by Lee Herng, 2016-08-10, psp Z-offset
    addAlgorithmSetting(SharedPspAlgorithm.createPspZOffsetAlgorithmSettings(2003, currentVersion));
    
    ArrayList<String> predictiveSliceHeightOptions = new ArrayList<String>(Arrays.asList("Off", "On"));
    addAlgorithmSetting(new AlgorithmSetting(
      AlgorithmSettingEnum.PREDICTIVE_SLICEHEIGHT,
      2003, // this is done to make sure this threshold is displayed last in the Slice Setup tab
      "Off",
      predictiveSliceHeightOptions,
      MeasurementUnitsEnum.NONE,
      "HTML_DESC_CHIP_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // description URL Key
      "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // detailed description URL Key
      "IMG_DESC_CHIP_MEASUREMENT_(PREDICTIVE_SLICEHEIGHT)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.SLICE_HEIGHT_ADDITIONAL,
      currentVersion));

    AlgorithmSetting padPercentToMeasureFilletThickness = new AlgorithmSetting(AlgorithmSettingEnum.
        CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS,
        displayOrder++,
        100.0f, // default
        10.0f, // min
        120.f, // max
        MeasurementUnitsEnum.PERCENT_OF_PAD_LENGTH_ALONG,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(padPercentToMeasureFilletThickness);
    padPercentToMeasureFilletThickness.filter(JointTypeEnum.RESISTOR);

    ArrayList<String> edgeLocationTechniqueOptions = new ArrayList<String>(Arrays.asList("Max Slope", "Second Derivatives"));
    AlgorithmSetting edgeLocationTechniqueOpaque = new AlgorithmSetting(AlgorithmSettingEnum.
        CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE,
        displayOrder++,
        "Max Slope",
        edgeLocationTechniqueOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(edgeLocationTechniqueOpaque);
    edgeLocationTechniqueOpaque.filter(JointTypeEnum.RESISTOR);

    // CR32137 - Chong, Wei Chin
    ArrayList<String> updateNorminalFilletThicknessMethodOptions = new ArrayList<String>(Arrays.asList("Clear", "Opaque", "Both"));
    AlgorithmSetting updateNorminalFilletThicknessMethod = new AlgorithmSetting(AlgorithmSettingEnum.
        CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD,
        displayOrder++,
        "Opaque",
        updateNorminalFilletThicknessMethodOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(updateNorminalFilletThicknessMethod);
    updateNorminalFilletThicknessMethod.filter(JointTypeEnum.RESISTOR);
    // Wei Chin (Tall Cap)
//    updateNorminalFilletThicknessMethod.filter(JointTypeEnum.TALL_CAPACITOR);

    AlgorithmSetting profileOffsetAlong = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG,
        displayOrder++,
        0.0f, // default
        -200.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    //maximumComponentRotation.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(profileOffsetAlong);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              profileOffsetAlong,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              profileOffsetAlong,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG_IN_PERCENTAGE);

    
    //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
    AlgorithmSetting componentProfileLengthAcross = new AlgorithmSetting(
      AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE, // setting enum
      displayOrder++, // display order,
      100.f, // default value
      10.f, // minimum value
      300.f, // maximum value
      MeasurementUnitsEnum.PERCENT,
      "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE)_KEY", // description URL Key
      "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE)_KEY", // detailed description URL Key
      "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE)_KEY", // image description URL Key
      AlgorithmSettingTypeEnum.ADDITIONAL,
      currentVersion);
    addAlgorithmSetting(componentProfileLengthAcross);
    
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              componentProfileLengthAcross,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE_IN_PERCENTAGE);
    
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              componentProfileLengthAcross,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE_IN_PERCENTAGE);
    
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              componentProfileLengthAcross,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE_IN_PERCENTAGE);

    //Siew Yeng - XCR-3094
    ArrayList<String> trueFalseOptions = new ArrayList<String>(Arrays.asList("False", "True"));
    AlgorithmSetting enableSaveAllJointImageWhenFailComponent = new AlgorithmSetting(
        AlgorithmSettingEnum.STITCH_COMPONENT_IMAGE_AT_VVTS,
        displayOrder++,
        "False",
        trueFalseOptions,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // description URL key
        "HTML_DETAILED_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // desailed description URL key
        "IMG_DESC_MEASUREMENT_(STITCH_COMPONENT_IMAGE_AT_VVTS)_KEY", // image description URL key
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(enableSaveAllJointImageWhenFailComponent);
    
    addMeasurementEnums();
    
    // Move GrayLevelEnhancement to sharedAlgo. Wei Chin
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings = ImageProcessingAlgorithm.createGrayLevelEnhancementAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings.size();
    _learnedAlgorithmSettingEnums.addAll(ImageProcessingAlgorithm.getLearnedGrayLevelAlgorithmSettingEnums());
    
    // Add the shared Image Processing Algo settings. Resized (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings2 = ImageProcessingAlgorithm.createResizeAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings2)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings2.size();
    
    // Add the shared Image Processing Algo settings. CLAHE (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings3 = ImageProcessingAlgorithm.createCLAHEAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings3)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings3.size();
    
    // Add the shared Image Processing Algo settings. Background filter (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings4 = ImageProcessingAlgorithm.createBoxFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings4)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings4.size();
    
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - START
    // Add the shared Image Processing Algo settings. FFTBandPassFilter (Lay Ngor)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings5 = ImageProcessingAlgorithm.createFFTBandPassFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings5)
      addAlgorithmSetting(algSetting);    
    displayOrder += imageProcessingAlgorithmSettings5.size();
    //Lim, Lay Ngor - 28Oct2013 XCR1723: Add FFTBandPassFilter for pre-processing - END    
 
    // Add the background Sensitivity (Wei Chin)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings6 = ImageProcessingAlgorithm.createBackgroundSensitivitySettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings6)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings6.size();
    
    // Add the shared Image Processing Algo settings. R filter (Siew Yeng)
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings7 = ImageProcessingAlgorithm.createRFilterAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings7)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings7.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Motion Blur
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings8 = ImageProcessingAlgorithm.createMotionBlurAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings8)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings8.size();
    
    // Kok Chun, Tan
    // Add the shared Image Processing Algo settings. Shading Removal
    Collection<AlgorithmSetting> imageProcessingAlgorithmSettings9 = ImageProcessingAlgorithm.createShadingRemovalAlgorithmSettings(displayOrder, currentVersion);
    for (AlgorithmSetting algSetting : imageProcessingAlgorithmSettings9)
      addAlgorithmSetting(algSetting);
    displayOrder += imageProcessingAlgorithmSettings9.size();
    
    // Add the shared Image Processing Algo settings. Save Enhanced Image (Siew Yeng)
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveEnhancedImageAlgorithmSetting(displayOrder, currentVersion));
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_FILLET_GAP);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS);

    _jointMeasurementEnums.addAll(Locator.getJointMeasurementEnums());

    _componentMeasurementEnums.addAll(Locator.getComponentMeasurementEnums());

    if (Config.isComponentLevelClassificationEnabled())
    {
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);
      
      // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);
      _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);
    }
    else
    {
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH);    
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);
      
      // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);
      _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);
   }

    //Lim, Lay Ngor - Clear Tombstone
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_CENTER_FILLET_THICKNESS);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE);

    //ShengChuan - Clear Tombstone
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT);

    //Lim, Lay Ngor - Opaque Tombstone
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS);    
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH);
 
    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG_IN_PERCENTAGE);
    
    //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
    _componentMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE_IN_PERCENTAGE);
  }

  /**
   * @author Peter Esbensen
   */
  private void collectProfileMeasurementsForOpaqueChips(JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        float[] componentDerivativeProfile,
                                                        float[] componentThicknessProfile,
                                                        float pad1FilletIndex,
                                                        float pad2FilletIndex,
                                                        float nominalBodyLengthInMillimeters,
                                                        SliceNameEnum sliceNameEnum,
                                                        Image image,
                                                        ReconstructionRegion reconstructionRegion,
                                                        RegionOfInterest componentRegionOfInterest,
                                                        final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(componentDerivativeProfile != null);
    Assert.expect(pad1FilletIndex >= 0);
    Assert.expect(pad2FilletIndex >= 0);
    Assert.expect(image != null);
    Assert.expect(reconstructionRegion != null);

    JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();
    JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();
    ComponentInspectionData componentInspectionData = padOneJointInspectionData.getComponentInspectionData();
    ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();

    Pad padOne = padOneJointInspectionData.getPad();
    Pad padTwo = padTwoJointInspectionData.getPad();
    Component component = padOne.getComponent();
    Subtype subtype = padOneJointInspectionData.getSubtype();

    // compute the fillet length
    float componentBodyLengthInPixels = pad1FilletIndex - pad2FilletIndex;
    float componentBodyLengthInMillimeters = componentBodyLengthInPixels * MILIMETER_PER_PIXEL;

    float componentBodyLengthAsPercentOfNominal = 100.0f * componentBodyLengthInMillimeters / nominalBodyLengthInMillimeters;

    // check for "gaps" in fillets
    float padOneFilletGapValue = ChipAlgorithmUtil.measurePadOneFilletGap(padOneJointInspectionData,
                                                               componentDerivativeProfile, pad1FilletIndex);
    float padTwoFilletGapValue = ChipAlgorithmUtil.measurePadTwoFilletGap(padTwoJointInspectionData,
                                                               componentDerivativeProfile, pad2FilletIndex);

    // get the pad fillet thicknesses
    float percentageOfPad = (Float)padOneJointInspectionData.getSubtype().
                                                      getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS);

    String edgeLocationTechniqueOpaque = (String)padOneJointInspectionData.getSubtype().
                                                        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE);
    float pad1FilletThicknessInMils;
    float pad2FilletThicknessInMils;
    if (edgeLocationTechniqueOpaque.equals("Max Slope"))
    {
      pad1FilletThicknessInMils = ChipAlgorithmUtil.measureFilletThicknessInMils(padOneJointInspectionData, componentThicknessProfile,
                                                                 Math.round(pad1FilletIndex),
                                                                 false); //Lim, Lay Ngor - XCR-2027 Exclude Outlier
      pad2FilletThicknessInMils = ChipAlgorithmUtil.measureFilletThicknessInMils(padTwoJointInspectionData, componentThicknessProfile,
                                                                 Math.round(pad2FilletIndex),
                                                                 false);//Lim, Lay Ngor - XCR-2027 Exclude Outlier
    }
    else
    {
      pad1FilletThicknessInMils = ChipAlgorithmUtil.measurePartialFilletThicknessInMilsToRight(padOneJointInspectionData, componentThicknessProfile,
                                                                 Math.round(pad1FilletIndex), percentageOfPad,
                                                                 false);//Lim, Lay Ngor - XCR-2027 Exclude Outlier
      pad2FilletThicknessInMils = ChipAlgorithmUtil.measurePartialFilletThicknessInMilsToLeft(padTwoJointInspectionData, componentThicknessProfile,
                                                                 Math.round(pad2FilletIndex), percentageOfPad,
                                                                 false);//Lim, Lay Ngor - XCR-2027 Exclude Outlier
    }
    //LN: can optimise to be faster - redundant calculation
    float opaqueNominalFilletThicknessInMillimeters = (Float)padOneJointInspectionData.getSubtype().
                                                      getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS);
    float padOneFilletThicknessAsPercentOfNominal = MathUtil.convertMilsToMillimeters(pad1FilletThicknessInMils) / (float)opaqueNominalFilletThicknessInMillimeters;
    float padTwoFilletThicknessAsPercentOfNominal = MathUtil.convertMilsToMillimeters(pad2FilletThicknessInMils) / (float)opaqueNominalFilletThicknessInMillimeters;

    // compute opaque open signal
    float padThicknessRatio = 0.0f;
    if (padOneFilletThicknessAsPercentOfNominal < padTwoFilletThicknessAsPercentOfNominal)
    {
      padThicknessRatio = padOneFilletThicknessAsPercentOfNominal / padTwoFilletThicknessAsPercentOfNominal;
    }
    else
    {
      padThicknessRatio = padTwoFilletThicknessAsPercentOfNominal / padOneFilletThicknessAsPercentOfNominal;
    }
    boolean opaqueOpenSignalIsValid = true;
    float opaqueOpenSignal = componentBodyLengthAsPercentOfNominal / padThicknessRatio - 100.0f;
    // since this is a ratio we can end up with value like infinity.
    // The GUI measurement plotting folks don't know how to handle that, so they asked me to cap it
    final float _MAX_ALLOWABLE_OPAQUE_OPEN_SIGNAL = 1000.0f;
    opaqueOpenSignal = Math.min(opaqueOpenSignal, _MAX_ALLOWABLE_OPAQUE_OPEN_SIGNAL);
    if (Float.isNaN(padThicknessRatio))
    {
      opaqueOpenSignalIsValid = false;
      opaqueOpenSignal = 0.0f;
    }

    boolean opaqueComponentTiltRatioIsValid = true;
    float opaqueComponentTiltRatio;
    if (componentThicknessProfile[Math.round(pad1FilletIndex)] > componentThicknessProfile[Math.round(pad2FilletIndex)])
      opaqueComponentTiltRatio = componentThicknessProfile[Math.round(pad1FilletIndex)] / componentThicknessProfile[Math.round(pad2FilletIndex)];
    else
      opaqueComponentTiltRatio = componentThicknessProfile[Math.round(pad2FilletIndex)] / componentThicknessProfile[Math.round(pad1FilletIndex)];
    if (pad1FilletThicknessInMils > pad2FilletThicknessInMils)
      opaqueComponentTiltRatio = opaqueComponentTiltRatio/MathUtil.convertMillimetersToMils(componentBodyLengthInMillimeters) * pad1FilletThicknessInMils;
    else
      opaqueComponentTiltRatio = opaqueComponentTiltRatio/MathUtil.convertMillimetersToMils(componentBodyLengthInMillimeters) * pad2FilletThicknessInMils;
    // since this is a ratio we can end up with value like infinity.
    // The GUI measurement plotting folks don't know how to handle that, so they asked me to cap it
    final float _MAX_ALLOWABLE_OPAQUE_COMPONENT_TILT_RATIO = 5.0f;
    opaqueComponentTiltRatio = Math.min(opaqueComponentTiltRatio, _MAX_ALLOWABLE_OPAQUE_COMPONENT_TILT_RATIO);
    if (Float.isNaN(padThicknessRatio))
    {
      opaqueComponentTiltRatioIsValid = false;
      opaqueComponentTiltRatio = 5.0f;
    }

    // Variable Initialization
    ComponentMeasurement componentBodyLengthMeasurement = null;
    ComponentMeasurement componentBodyLengthPercentMeasurement = null;
    ComponentMeasurement opaqueOpenSignalMeasurement = null;
    ComponentMeasurement opaqueComponentTiltRatioMeasurement = null;
    ComponentMeasurement bodyWidthAcrossComponentMeasurement = null;
    
    JointMeasurement jointComponentBodyLengthMeasurement = null;
    JointMeasurement jointComponentBodyLengthPercentMeasurement = null;
    JointMeasurement jointOpaqueOpenSignalMeasurement = null;
    JointMeasurement jointOpaqueComponentTiltRatioMeasurement = null;
    JointMeasurement jointBodyWidthAcrossComponentMeasurement = null;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      // store component length - Component level
      componentBodyLengthMeasurement = new ComponentMeasurement(this,
                                                                subtype,
                                                                MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH,
                                                                MeasurementUnitsEnum.MILLIMETERS,
                                                                component,
                                                                sliceNameEnum,
                                                                componentBodyLengthInMillimeters);
      componentInspectionResult.addMeasurement(componentBodyLengthMeasurement);
      
      // store component length percent - Component level
      componentBodyLengthPercentMeasurement = new ComponentMeasurement(this,
                                                                       subtype,
                                                                       MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT,
                                                                       MeasurementUnitsEnum.PERCENT,
                                                                       component,
                                                                       sliceNameEnum,
                                                                       componentBodyLengthAsPercentOfNominal);
      componentInspectionResult.addMeasurement(componentBodyLengthPercentMeasurement);
      
      // store opaque open signal - Component level
      opaqueOpenSignalMeasurement = new ComponentMeasurement(this,
                                                             subtype,
                                                             MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL,
                                                             MeasurementUnitsEnum.NONE,
                                                             component,
                                                             sliceNameEnum,
                                                             opaqueOpenSignal,
                                                             opaqueOpenSignalIsValid);
      componentInspectionResult.addMeasurement(opaqueOpenSignalMeasurement);
      
      // store opaque component tilt ratio - Component level
      opaqueComponentTiltRatioMeasurement = new ComponentMeasurement(this,
                                                                     subtype,
                                                                     MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO,
                                                                     MeasurementUnitsEnum.NONE,
                                                                     component,
                                                                     sliceNameEnum,
                                                                     opaqueComponentTiltRatio,
                                                                     opaqueComponentTiltRatioIsValid);
      componentInspectionResult.addMeasurement(opaqueComponentTiltRatioMeasurement);
    }
    else
    {
      // store component length - Joint level
      jointComponentBodyLengthMeasurement = new JointMeasurement(this,
                                                                 MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH,
                                                                 MeasurementUnitsEnum.MILLIMETERS,
                                                                 padOne,
                                                                 sliceNameEnum,
                                                                 componentBodyLengthInMillimeters);
      padOneJointInspectionResult.addMeasurement(jointComponentBodyLengthMeasurement);
      
      // store component length percent - Joint level
      jointComponentBodyLengthPercentMeasurement = new JointMeasurement(this,
                                                                        MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT,
                                                                        MeasurementUnitsEnum.PERCENT,
                                                                        padOne,
                                                                        sliceNameEnum,
                                                                        componentBodyLengthAsPercentOfNominal);
      padOneJointInspectionResult.addMeasurement(jointComponentBodyLengthPercentMeasurement);
      
      // store opaque open signal - Joint level
      jointOpaqueOpenSignalMeasurement = new JointMeasurement(this,
                                                             MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL,
                                                             MeasurementUnitsEnum.NONE,
                                                             padOne,
                                                             sliceNameEnum,
                                                             opaqueOpenSignal,
                                                             opaqueOpenSignalIsValid);
      padOneJointInspectionResult.addMeasurement(jointOpaqueOpenSignalMeasurement);
      
      // store opaque component tilt ratio - Joint level
      jointOpaqueComponentTiltRatioMeasurement = new JointMeasurement(this,
                                                                      MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO,
                                                                      MeasurementUnitsEnum.NONE,
                                                                      padOne,
                                                                      sliceNameEnum,
                                                                      opaqueComponentTiltRatio,
                                                                      opaqueComponentTiltRatioIsValid);
      padOneJointInspectionResult.addMeasurement(jointOpaqueComponentTiltRatioMeasurement);
    }

    // store pad one fillet thickness
    JointMeasurement padOneFilletThicknessMeasurement = new JointMeasurement(this,
                                                                             MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS,
                                                                             MeasurementUnitsEnum.MILLIMETERS,
                                                                             padOne,
                                                                             sliceNameEnum,
                                                                             MathUtil.convertMilsToMillimeters(pad1FilletThicknessInMils));
    padOneJointInspectionResult.addMeasurement(padOneFilletThicknessMeasurement);

    // store pad two fillet thickness
    JointMeasurement padTwoFilletThicknessMeasurement = new JointMeasurement(this,
                                                                             MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS,
                                                                             MeasurementUnitsEnum.MILLIMETERS,
                                                                             padTwo,
                                                                             sliceNameEnum,
                                                                             MathUtil.convertMilsToMillimeters(pad2FilletThicknessInMils));
    padTwoJointInspectionResult.addMeasurement(padTwoFilletThicknessMeasurement);

    // store pad one fillet gap measurement
    JointMeasurement padOneFilletGapMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.CHIP_MEASUREMENT_FILLET_GAP,
                                                                       MeasurementUnitsEnum.NONE,
                                                                       padOne,
                                                                       sliceNameEnum,
                                                                       padOneFilletGapValue);
    padOneJointInspectionResult.addMeasurement(padOneFilletGapMeasurement);
    // store pad two fillet gap measurement
    JointMeasurement padTwoFilletGapMeasurement = new JointMeasurement(this,
                                                                       MeasurementEnum.CHIP_MEASUREMENT_FILLET_GAP,
                                                                       MeasurementUnitsEnum.NONE,
                                                                       padTwo,
                                                                       sliceNameEnum,
                                                                       padTwoFilletGapValue);
    padTwoJointInspectionResult.addMeasurement(padTwoFilletGapMeasurement);

    //LayNgor: Clear Tombstone
    RegionOfInterest adjustedRegionOfInterestClearTombstone = new RegionOfInterest(componentRegionOfInterest);
//    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
//    {
    //LayNgor: Clear Tombstone - Open up this cause I need the value later
      // Display a rectangular on joint image, indicating the location of fillet edges found.
      RegionOfInterest adjustedRegionOfInterest = new RegionOfInterest(componentRegionOfInterest);
      adjustedRegionOfInterest.setLengthAlong((int)(componentBodyLengthInPixels)+1);
      adjustedRegionOfInterest.translateAlongAcross((int)(pad2FilletIndex-(componentRegionOfInterest.getLengthAlong()-componentBodyLengthInPixels)/2.0),0);
      adjustedRegionOfInterest.scaleFromCenterAlongAcross(1.0, 0.5);
      adjustedRegionOfInterestClearTombstone = adjustedRegionOfInterest;//LayNgor: Clear Tombstone
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
    {
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo =
          new MeasurementRegionDiagnosticInfo(adjustedRegionOfInterest,
                                              MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
      AlgorithmDiagnostics.getInstance().postDiagnostics(reconstructionRegion,
                                                         sliceNameEnum,
                                                         componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                                         this, false, true,
                                                         componentRegionDiagnosticInfo);

      // some component measurements, then some joint measurements, then some more component measurements. Hence the three calls.
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (componentBodyLengthMeasurement != null && 
            componentBodyLengthPercentMeasurement != null)
        {
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      componentInspectionData,
                                                      componentBodyLengthMeasurement,
                                                      componentBodyLengthPercentMeasurement);
        }
      }
      else
      {
        if (jointComponentBodyLengthMeasurement != null && 
            jointComponentBodyLengthPercentMeasurement != null)
        {
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      jointComponentBodyLengthMeasurement,
                                                      jointComponentBodyLengthPercentMeasurement);
        }
      }
      
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      padOneFilletThicknessMeasurement,
                                                      padTwoFilletThicknessMeasurement,
                                                      padOneFilletGapMeasurement,
                                                      padTwoFilletGapMeasurement);
      
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (opaqueOpenSignalMeasurement != null &&
            opaqueComponentTiltRatioMeasurement != null)
        {
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      componentInspectionData,
                                                      opaqueOpenSignalMeasurement,
                                                      opaqueComponentTiltRatioMeasurement);
        }
      }
      else
      {
        if (jointOpaqueOpenSignalMeasurement != null &&
            opaqueComponentTiltRatioMeasurement != null)
        {
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      jointOpaqueOpenSignalMeasurement,
                                                      jointOpaqueComponentTiltRatioMeasurement);
        }
      }

      RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
      RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
      RegionOfInterest componentRoi = componentInspectionData.getOrthogonalComponentRegionOfInterest();

      int padTwoLeftSideIndex = ChipAlgorithmUtil.getPadTwoLeftSideIndex(padTwoJointInspectionData, componentRoi);
      int padTwoRightSideIndex = ChipAlgorithmUtil.getPadTwoRightSideIndex(componentThicknessProfile, padTwoJointInspectionData, padTwoRoi, componentRoi);
      int padOneLeftSideIndex = ChipAlgorithmUtil.getPadOneLeftSideIndex(componentThicknessProfile, padOneJointInspectionData, padOneRoi, componentRoi);
      int padOneRightSideIndex = ChipAlgorithmUtil.getPadOneRightSideIndex(componentThicknessProfile, padOneJointInspectionData, componentRoi);
      postOpaqueChipProfileDiagnostics(componentThicknessProfile,
                                       (int)pad1FilletIndex,
                                       (int)pad2FilletIndex,
                                       padTwoLeftSideIndex,
                                       padTwoRightSideIndex,
                                       padOneLeftSideIndex,
                                       padOneRightSideIndex,
                                       reconstructionRegion,
                                       sliceNameEnum,
                                       padOneJointInspectionData,
                                       MILIMETER_PER_PIXEL);
    }

    // measure and store bodyWidthAcross
    //Lim, Lay Ngor - OKI Opaque tombstone - targetSideFilletEdgeThicknessForCenterAcrossWidth is the threshold use for edge detection of the width across.
    //Get the targetSideFilletEdgeThicknessForCenterAcrossWidth for measureBodyEndWidthAcrossRatioForTombstone() use
    FloatRef targetSideFilletEdgeThicknessForCenterAcrossWidth = new FloatRef();
    float bodyWidthInMillimeters = measureBodyWidthAcross(componentInspectionData, componentRegionOfInterest, reconstructionRegion, sliceNameEnum, image, targetSideFilletEdgeThicknessForCenterAcrossWidth, MILIMETER_PER_PIXEL);    
    if (Config.isComponentLevelClassificationEnabled())
    {
      bodyWidthAcrossComponentMeasurement = new ComponentMeasurement(this,
                                                                     subtype,
                                                                     MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH,
                                                                     MeasurementUnitsEnum.MILLIMETERS,
                                                                     component,
                                                                     sliceNameEnum,
                                                                     bodyWidthInMillimeters,
                                                                     true);
      componentInspectionResult.addMeasurement(bodyWidthAcrossComponentMeasurement);
    }
    else
    {
      jointBodyWidthAcrossComponentMeasurement = new JointMeasurement(this,
                                                                      MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH,
                                                                      MeasurementUnitsEnum.MILLIMETERS,
                                                                      padOne,
                                                                      sliceNameEnum,
                                                                      bodyWidthInMillimeters,
                                                                      true);
      padOneJointInspectionResult.addMeasurement(jointBodyWidthAcrossComponentMeasurement);
    }
    
    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (bodyWidthAcrossComponentMeasurement != null)
        {
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      componentInspectionData,
                                                      bodyWidthAcrossComponentMeasurement);
        }
      }
      else
      {
        if (jointBodyWidthAcrossComponentMeasurement != null)
        {
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      jointBodyWidthAcrossComponentMeasurement);
        }
      }
    }

    //Lim, Lay Ngor - Opaque Tombstone - START
    //Lim, Lay Ngor - Opaque Tombstone - XCR3359 Enable Fillet Width Detection
    String opaqueChipEnableFilletWidthDetection = 
      (String)padOneJointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION);      
    if(opaqueChipEnableFilletWidthDetection.equalsIgnoreCase("true"))
    {
      // measure and store bodyWidthAcross
      //Get the targetSideFilletEdgeThicknessForCenterAcrossWidth for more accurate edge detection on End Width Across detection.
      FloatRef bodyWidthAcrossJointOneInMillimeters = new FloatRef();
      FloatRef bodyWidthAcrossJointTwoInMillimeters = new FloatRef();
      measureBodyEndWidthAcrossRatioForTombstone(componentInspectionData, componentRegionOfInterest,
        reconstructionRegion, sliceNameEnum, image, adjustedRegionOfInterestClearTombstone, bodyWidthAcrossJointOneInMillimeters, 
        bodyWidthAcrossJointTwoInMillimeters, targetSideFilletEdgeThicknessForCenterAcrossWidth.getValue());

      JointMeasurement bodyWidthAcrossJointOneMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH,
        MeasurementUnitsEnum.MILLIMETERS,
        padOne,
        sliceNameEnum,
        bodyWidthAcrossJointOneInMillimeters.getValue());
      padOneJointInspectionResult.addMeasurement(bodyWidthAcrossJointOneMeasurement);
      JointMeasurement bodyWidthAcrossJointTwoMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH,      
        MeasurementUnitsEnum.MILLIMETERS,
        padTwo,
        sliceNameEnum,
        bodyWidthAcrossJointTwoInMillimeters.getValue());
      padTwoJointInspectionResult.addMeasurement(bodyWidthAcrossJointTwoMeasurement);
      
      //Joint One
      if(bodyWidthAcrossJointOneMeasurement.getValue() == 0) //Warning for failed to detect joint across width
      {
        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_FAILED_TO_MEASURE_JOINT_FILLET_ACROSS_WIDTH_WARNING_KEY",
                                            new Object[]
                                            {padOneJointInspectionData.getPad().toString(), sliceNameEnum.getName()}));
      }
      
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))//Display joint number
      {   
        _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, subtype, this, false, false, 
          new TextualDiagnosticInfo(new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_MEASURING_PIN_ONE_KEY", null)));  
        //Display joint across width detected value
        AlgorithmUtil.postMeasurementTextualDiagnostics(this,
          reconstructionRegion,
          padOneJointInspectionData,
          bodyWidthAcrossJointOneMeasurement);
      }
      
      //Joint Two
      if(bodyWidthAcrossJointTwoMeasurement.getValue() == 0)//Warning for failed to detect joint across width
      {
        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_FAILED_TO_MEASURE_JOINT_FILLET_ACROSS_WIDTH_WARNING_KEY",
                                            new Object[]
                                            {padTwoJointInspectionData.getPad().toString(), sliceNameEnum.getName()}));
      }
      
      if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))//Display joint number
      {
        _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, subtype, this, false, false, 
          new TextualDiagnosticInfo(new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_MEASURING_PIN_TWO_KEY", null)));
        //Display joint across width detected value
        AlgorithmUtil.postMeasurementTextualDiagnostics(this,
          reconstructionRegion,
          padTwoJointInspectionData,
          bodyWidthAcrossJointTwoMeasurement);
      }
    }
    //Lim, Lay Ngor - Opaque Tombstone - END
  }

  /**
   * Opaque Tombstone.
   * @author Lim, Lay Ngor
   */
  private void measureBodyEndWidthAcrossRatioForTombstone(ComponentInspectionData componentInspectionData,
    RegionOfInterest componentRegionOfInterest,
    ReconstructionRegion reconstructionRegion,
    SliceNameEnum sliceNameEnum,
    Image image,
    RegionOfInterest adjustedRegionOfInterest,
    FloatRef bodyWidthAcrossJointOne,
    FloatRef bodyWidthAcrossJointTwo,
    float targetSideFilletEdgeThicknessForCenterAcrossWidth) throws DatastoreException
  {
    Assert.expect(componentRegionOfInterest != null);    
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(image != null);
    Assert.expect(adjustedRegionOfInterest != null);

    RegionOfInterest regionAcrossComponent = new RegionOfInterest(componentRegionOfInterest);
    int componentLength = componentRegionOfInterest.getLengthAlong();
    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    Subtype subtype = padOneJointInspectionData.getSubtype();
    int padOneLength = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    int padTwoLength = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    int areaBetweenPadsLength = componentLength - padOneLength - padTwoLength;
    float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f
      * (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT);
    regionAcrossComponent.setLengthAlong(Math.round(areaBetweenPadsLength * 0.5f));
    regionAcrossComponent.setLengthAcross(Math.round(regionAcrossComponent.getLengthAcross()
      + padOneJointInspectionData.getInterPadDistanceInPixels()
      * 2f * backgroundRegionShiftAsFractionOfInterPadDistance));

    int profileOrientation = MathUtil.getDegreesWithin0To359(regionAcrossComponent.getOrientationInDegrees() + 90);
    regionAcrossComponent.setOrientationInDegrees(profileOrientation);

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, regionAcrossComponent);

    RegionOfInterest padOneCADLocation = new RegionOfInterest(regionAcrossComponent);
    RegionOfInterest padTwoCADLocation = new RegionOfInterest(regionAcrossComponent);
    if (regionAcrossComponent.getOrientationInDegrees() == 0 || regionAcrossComponent.getOrientationInDegrees() == 180)
    {
      float diffOne = Math.abs(adjustedRegionOfInterest.getMinY() - padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinY());
      float diffTwo = Math.abs(adjustedRegionOfInterest.getMinY() - padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinY());
      if (diffOne < diffTwo)
      {
        padOneCADLocation.setMinXY(regionAcrossComponent.getMinX(), adjustedRegionOfInterest.getMinY());
        padTwoCADLocation.setMinXY(regionAcrossComponent.getMinX(), adjustedRegionOfInterest.getMaxY() - regionAcrossComponent.getHeight());
      }
      else
      {
        padTwoCADLocation.setMinXY(regionAcrossComponent.getMinX(), adjustedRegionOfInterest.getMinY());
        padOneCADLocation.setMinXY(regionAcrossComponent.getMinX(), adjustedRegionOfInterest.getMaxY() - regionAcrossComponent.getHeight());
      }
    }
    else
    {
      float diffOne = Math.abs(adjustedRegionOfInterest.getMinX() - padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinX());
      float diffTwo = Math.abs(adjustedRegionOfInterest.getMinX() - padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinX());
      if (diffOne < diffTwo)
      {
        padOneCADLocation.setMinXY(adjustedRegionOfInterest.getMinX(), regionAcrossComponent.getMinY());
        padTwoCADLocation.setMinXY(adjustedRegionOfInterest.getMaxX() - padTwoCADLocation.getWidth(), regionAcrossComponent.getMinY());
      }
      else
      {
        padTwoCADLocation.setMinXY(adjustedRegionOfInterest.getMinX(), regionAcrossComponent.getMinY());
        padOneCADLocation.setMinXY(adjustedRegionOfInterest.getMaxX() - padOneCADLocation.getWidth(), regionAcrossComponent.getMinY());
      }
    }

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, padOneCADLocation);
    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, padTwoCADLocation);

    final float bodyWidthInMillimetersOne = calculateBodyWidth(image, padOneCADLocation, subtype,
      padOneJointInspectionData, reconstructionRegion, sliceNameEnum, targetSideFilletEdgeThicknessForCenterAcrossWidth);
    final float bodyWidthInMillimetersTwo = calculateBodyWidth(image, padTwoCADLocation, subtype,
      padTwoJointInspectionData, reconstructionRegion, sliceNameEnum, targetSideFilletEdgeThicknessForCenterAcrossWidth);
    bodyWidthAcrossJointOne.setValue(bodyWidthInMillimetersOne);
    bodyWidthAcrossJointTwo.setValue(bodyWidthInMillimetersTwo);
  }

  /**
   * Calculate Component Body Width at different ROI - Special develop for
   * billboard(previously use center ROI) and tombstone detection. In previous
   * inspection(before 5Dec14), we using the center ROI(ROI 1) to detect
   * billboard through Component_Width_Across. Now, we add in another 2 ROIs,
   * which is ROI 2(for PAD 2) and ROI 3(for PAD 1) to detect the tombstone.
   * This two ROI will check upon Component_Width_Across_For_Tombstone.
   *
   *        _______          _______       _______ 
   *       |      |         |     |       |      | 
   *  /----|------|-----\   | /----|------|-----\|   /|\ 
   * |     |  ROI |     |   || ROI |      | ROI ||    |   Width Across 
   * |     |  1   |     |   || 2   |      | 3   ||    |   = Body Width 
   * \-----|------|----/    |\-----|------|----/ |   \|/
   *       |______|         |______|      |______|
   *
   * @author Lim, Lay Ngor - Opaque Tombstone
   */
  float calculateBodyWidth(Image image, RegionOfInterest roiToMeasureBodyWidth, Subtype subtype,
    JointInspectionData jointInspectionData, ReconstructionRegion reconstructionRegion, SliceNameEnum sliceNameEnum,
    float targetSideFilletEdgeThicknessForCenterAcrossWidth) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(roiToMeasureBodyWidth != null);
    Assert.expect(subtype != null);
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    final float[] grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(image, roiToMeasureBodyWidth);
    final float[] estimatedBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(grayLevelProfileAcrossComponent);

    // Create a thickness profile.
    final SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    final int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    final float[] thicknessProfileAcrossInMillimeters
      = AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent,
        estimatedBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

    //Removed the diagnostic because for center area(original) median accross is actually using component diagnostic,
    //but for tombstone joint 1 & 2 is actually using joint diagnostic
    MeasurementRegionDiagnosticInfo jointRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(roiToMeasureBodyWidth,
                                                                                                    MeasurementRegionEnum.FILLET_REGION);

//    final float TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS
//      = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT) / 100.f;
//    final float targetSideFilletEdgeThickness
//      = ArrayUtil.max(thicknessProfileAcrossInMillimeters) * TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS;
    //LLN to fix opaque tombstone bug!!! use targetSideFilletEdgeThicknessForCenterAcrossWidth for edge detection!
    final float leftSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(thicknessProfileAcrossInMillimeters, 0, thicknessProfileAcrossInMillimeters.length - 1, targetSideFilletEdgeThicknessForCenterAcrossWidth);
    final float rightSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(
      thicknessProfileAcrossInMillimeters, thicknessProfileAcrossInMillimeters.length - 1, 0, targetSideFilletEdgeThicknessForCenterAcrossWidth);

    boolean locationsAreValid = true;
    if (leftSideFilletIndex >= rightSideFilletIndex)
      locationsAreValid = false;

    MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[thicknessProfileAcrossInMillimeters.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.COMPONENT_PROFILE_REGION);
    if (locationsAreValid)
    {
      measurementRegionEnums[Math.round(leftSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;
      measurementRegionEnums[Math.round(rightSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;
    }

    LocalizedString profileAcrossLabelLocalizedString =
      new LocalizedString("ALGIAG_CHIP_MEASUREMENT_FILLET_PROFILE_ACROSS_KEY", null);
    ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo = ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(jointInspectionData.getComponent().getReferenceDesignator(),
                                                                                                                                ProfileTypeEnum.SOLDER_PROFILE,
                                                                                                                                profileAcrossLabelLocalizedString,
                                                                                                                                0.0f,
                                                                                                                                ArrayUtil.max(thicknessProfileAcrossInMillimeters) * 1.1f,
                                                                                                                                thicknessProfileAcrossInMillimeters,
                                                                                                                                measurementRegionEnums,
                                                                                                                                MeasurementUnitsEnum.MILLIMETERS);

    RegionOfInterest leftSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(roiToMeasureBodyWidth, Math.round(leftSideFilletIndex));
    MeasurementRegionDiagnosticInfo leftSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(leftSideEdgeRegion,
                                                                                                           MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
    RegionOfInterest rightSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(roiToMeasureBodyWidth, Math.round(rightSideFilletIndex));
    MeasurementRegionDiagnosticInfo rightSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(rightSideEdgeRegion,
                                                                                                            MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

    if(locationsAreValid)
    {
      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData.getSubtype(),
                                   this, false, true,
                                   jointRegionDiagnosticInfo,
                                   labeledPadThicknessProfileDiagInfo,
                                   leftSideEdgeRegionDiagnosticInfo,
                                   rightSideEdgeRegionDiagnosticInfo); 
    }
    else //Don't post the Outer Fillet Edge if Failed to find body width
    {
      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   jointInspectionData.getSubtype(),
                                   this, false, true,
                                   jointRegionDiagnosticInfo,
                                   labeledPadThicknessProfileDiagInfo); 
    }
    
    float bodyWidthInPixels = 0f;//If bodyWidthInPixels is 0, means failed to find body width
    if (locationsAreValid)
      bodyWidthInPixels = rightSideFilletIndex - leftSideFilletIndex;

    final float bodyWidthInMillimeters = MathUtil.convertNanometersToMillimeters(MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel() * bodyWidthInPixels);
    return bodyWidthInMillimeters;
  }

  /**
   * Clear Tombstone.
   * Image is enhance before detecting the dark solder area.
   * Large image for this function will be slow.
   * 
   * @param image
   * @param padCADLocation
   * @return 
   */
  int areaDetection(Image image, RegionOfInterest padCADLocation, boolean roundnessDetection)
  { 
    Assert.expect(image != null);
    Assert.expect(padCADLocation != null);

    ImageEnhancement.autoEnhanceContrastInPlace(image);
    
    RegionOfInterest thresholdROI = new RegionOfInterest(padCADLocation);
    final int widthAdjustment = 5;
    if(padCADLocation.getWidth() > 2 * widthAdjustment)
      thresholdROI.setWidthKeepingSameCenter(padCADLocation.getWidth() - 5);//best 4
//    thresholdROI.setWidthKeepingSameMinX(thresholdROI.getWidth() - 1);
    final int heightAdjustment = 7;
    if(padCADLocation.getHeight() > 2 * heightAdjustment)
      thresholdROI.setHeightKeepingSameCenter(padCADLocation.getHeight() - 7);// best 6
    
    final int notSmoothNormaliseImageHistogram[] = Threshold.histogram(image, thresholdROI, 256);
    int normalisedImageThreshold = Threshold.getAutoThreshold(notSmoothNormaliseImageHistogram);
    
    float max = Statistics.maxValue(image);
    float min = Statistics.minValue(image);
    float range = max - min;
    float learnRange = 50;//prefer learn up this value 15?
    //foreground is black = solder, 
    //smaller normalisedImageThreshold value means less sensitive, larger value is more senstive

    //this only used to detect roundness of pad
    if(roundnessDetection)
    {
      int reducedNormalisedThreshold = (int) Math.floor((range - learnRange) / 10 + 0.5f);
      normalisedImageThreshold = normalisedImageThreshold - reducedNormalisedThreshold;
    }
    
    if(range < learnRange && normalisedImageThreshold < 50)//dark background
//      normalisedImageThreshold = normalisedImageThreshold - 1;//reduce its sensitivity
      normalisedImageThreshold = normalisedImageThreshold + 1;//increase little its sensitivity
    
//    if(range < 10 && normalisedImageThreshold < 50)//dark background
//      normalisedImageThreshold = normalisedImageThreshold + 3;//increase little its sensitivity    
    
    if(range > 200)
      normalisedImageThreshold = normalisedImageThreshold + 5;//more sensitive
    
    return ImagePixelsExtraction.fillAndCalculateDarkPixelsInRoiOfImage(image, padCADLocation, normalisedImageThreshold, 60.f);
  }

  /**
   *
   * @author Lim, Lay Ngor - temp for debug use
   */
  private static void saveImage(Image image, String filePath)
  {
    try
    {
      ImageIoUtil.saveImage(image, filePath);
    }
    catch (CouldNotCreateFileException ex)
    {
      System.out.println("Failed to save");
    }
  }

  /**
   * @author Peter Esbensen
   * @author Lim, Lay Ngor - targetSideFilletEdgeThicknessForCenterAcrossWidth will be use in Opaque Tombstone 
   */
  private float measureBodyWidthAcross(ComponentInspectionData componentInspectionData,
                                       RegionOfInterest componentRegionOfInterest,
                                       ReconstructionRegion reconstructionRegion,
                                       SliceNameEnum sliceNameEnum,
                                       Image image,
                                       FloatRef targetSideFilletEdgeThicknessForCenterAcrossWidth,
                                       final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(targetSideFilletEdgeThicknessForCenterAcrossWidth != null);

    RegionOfInterest regionAcrossComponent = new RegionOfInterest(componentRegionOfInterest);
    int componentLength = componentRegionOfInterest.getLengthAlong();
    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    Subtype subtype = padOneJointInspectionData.getSubtype();
    int padOneLength = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    int padTwoLength = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    int areaBetweenPadsLength = componentLength - padOneLength - padTwoLength;
    float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f *
         (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT);
    regionAcrossComponent.setLengthAlong(Math.round(areaBetweenPadsLength * 0.5f));
    regionAcrossComponent.setLengthAcross(Math.round(regionAcrossComponent.getLengthAcross() +
                                                     padOneJointInspectionData.getInterPadDistanceInPixels() *
                                                     2f * backgroundRegionShiftAsFractionOfInterPadDistance));

    int profileOrientation = MathUtil.getDegreesWithin0To359(regionAcrossComponent.getOrientationInDegrees() + 90);
    regionAcrossComponent.setOrientationInDegrees(profileOrientation);

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, regionAcrossComponent);

    MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(regionAcrossComponent,
                                                                                                        MeasurementRegionEnum.COMPONENT_REGION);

    float[] grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(image, regionAcrossComponent);

    float[] estimatedBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(grayLevelProfileAcrossComponent);

    // Create a thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float[] thicknessProfileAcrossInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent, estimatedBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                 this, false, true,
                                 componentRegionDiagnosticInfo);

    // Find the fillet edges on each side of the across profile.
    final float TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT) / 100.f;

    float targetSideFilletEdgeThickness = ArrayUtil.max(thicknessProfileAcrossInMillimeters) * TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS;
    targetSideFilletEdgeThicknessForCenterAcrossWidth.setValue(targetSideFilletEdgeThickness);//Use in Opaque tombstone
    float leftSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(thicknessProfileAcrossInMillimeters, 0,
                                                                                         thicknessProfileAcrossInMillimeters.length - 1, targetSideFilletEdgeThickness);
    float rightSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(thicknessProfileAcrossInMillimeters,
                                                                                          thicknessProfileAcrossInMillimeters.length - 1, 0, targetSideFilletEdgeThickness);
    boolean locationsAreValid = true;
    if (leftSideFilletIndex >= rightSideFilletIndex)
      locationsAreValid = false;

    MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[thicknessProfileAcrossInMillimeters.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.COMPONENT_PROFILE_REGION);
    if (locationsAreValid)
    {
      measurementRegionEnums[Math.round(leftSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;
      measurementRegionEnums[Math.round(rightSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;
    }

    LocalizedString profileAcrossLabelLocalizedString =
      new LocalizedString("ALGIAG_CHIP_MEASUREMENT_PROFILE_ACROSS_KEY", null);
    ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo = ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(componentInspectionData.getComponent().getReferenceDesignator(),
                                                                                                                                ProfileTypeEnum.SOLDER_PROFILE,
                                                                                                                                profileAcrossLabelLocalizedString,
                                                                                                                                0.0f,
                                                                                                                                ArrayUtil.max(thicknessProfileAcrossInMillimeters) * 1.1f,
                                                                                                                                thicknessProfileAcrossInMillimeters,
                                                                                                                                measurementRegionEnums,
                                                                                                                                MeasurementUnitsEnum.MILLIMETERS);

    RegionOfInterest leftSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(leftSideFilletIndex));
    MeasurementRegionDiagnosticInfo leftSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(leftSideEdgeRegion,
                                                                                                           MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
    RegionOfInterest rightSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(rightSideFilletIndex));
    MeasurementRegionDiagnosticInfo rightSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(rightSideEdgeRegion,
                                                                                                            MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                 this, false, true,
                                 labeledPadThicknessProfileDiagInfo,
                                 leftSideEdgeRegionDiagnosticInfo,
                                 rightSideEdgeRegionDiagnosticInfo);


    float bodyWidthInPixels = 0f;
    if (locationsAreValid)
      bodyWidthInPixels = rightSideFilletIndex - leftSideFilletIndex;
    float bodyWidthInMillimeters = MILIMETER_PER_PIXEL * bodyWidthInPixels;
    return bodyWidthInMillimeters;
  }
  
  private float measureComponentMisalignmentAcrossInPercentage(ReconstructedImages reconstructedImages,
                                       ComponentInspectionData componentInspectionData,
                                       RegionOfInterest componentRegionOfInterest,
                                       ReconstructionRegion reconstructionRegion,
                                       SliceNameEnum sliceNameEnum,
                                       Image image, boolean testAsClear, FloatRef componentRotationInDegreesRef,
                                       ClearChipProfileInfo clearChipProfileInfo) throws DatastoreException
  {
    Assert.expect(image != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);

    RegionOfInterest regionAcrossComponent = new RegionOfInterest(componentRegionOfInterest);
    int componentLength = componentRegionOfInterest.getLengthAlong();
    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    Subtype subtype = padOneJointInspectionData.getSubtype();
    int padOneLength = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    int padTwoLength = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    int areaBetweenPadsLength = componentLength - padOneLength - padTwoLength;
    float backgroundRegionShiftAsFractionOfInterPadDistance = 0.01f *
         (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT);

   //if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.RESISTOR))
    {
      // clear out the locator diags
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);

    }

    float profileOffsetAlongInPercentage = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG);

    // get pad One Edge
    boolean normalComponoentOrientation = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinCoordinateAlong()>padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinCoordinateAlong();
    double regionAcrossComponentCenterAlong = componentRegionOfInterest.getCenterAlong();
    double regionAcrossComponentCenterAcross = componentRegionOfInterest.getCenterAcross();
    double regionAcrossComponentLengthAlong = componentRegionOfInterest.getLengthAlong();
    double regionAcrossComponentLengthAcross = componentRegionOfInterest.getLengthAcross();
    int regionAcrossComponentProfileOrientation =  regionAcrossComponent.getOrientationInDegrees();
    double pad1CenterCoordinateAlongProfile = 0;
    double pad2CenterCoordinateAlongProfile = 0;
    
    //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
    float componentProfileLengthAcrossInPercentage = (Float)(subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE)) / 100.f;

    double padOneUpperFilletPosition;
    double padTwoUpperFilletPosition;
    
    if(testAsClear)
    {
//        int padOneMaxCoordinateAlong = (int)padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMaxCoordinateAlong();
//        double regionCenterAlong = regionAcrossComponent.getCenterAlong();
//        double regionCenterAcross = regionAcrossComponent.getCenterAcross();
//        double regionLeft =  _clearChipProfileInfo.getPadOneLeftSideIndex();
//        double regionRight =  _clearChipProfileInfo.getPadOneRightSideIndex();
//        double padOneUpperFilletIndex = _clearChipProfileInfo.getPadOneUpperFilletIndex();
        
        if (regionAcrossComponentProfileOrientation==90)
        {
           padOneUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMaxCoordinateAlong() - clearChipProfileInfo.getPadOneUpperFilletIndex();
            regionAcrossComponent.setCenterXY( regionAcrossComponent.getCenterX(),(int)padOneUpperFilletPosition);
            regionAcrossComponent.translateAlongAcross(-(int)Math.round(padOneLength * profileOffsetAlongInPercentage/100.0),0);

        }
        else if (regionAcrossComponentProfileOrientation==270)
        {
            padOneUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMinCoordinateAlong() + clearChipProfileInfo.getPadOneUpperFilletIndex();
            regionAcrossComponent.setCenterXY( regionAcrossComponent.getCenterX(),(int)padOneUpperFilletPosition);
            regionAcrossComponent.translateAlongAcross(-(int)Math.round(padOneLength * profileOffsetAlongInPercentage/100.0),0);

        }else if (regionAcrossComponentProfileOrientation==180)
        {
           padOneUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMaxCoordinateAlong() - clearChipProfileInfo.getPadOneUpperFilletIndex();
            regionAcrossComponent.setCenterXY((int)padOneUpperFilletPosition, regionAcrossComponent.getCenterY());
            regionAcrossComponent.translateAlongAcross(-(int)Math.round(padOneLength * profileOffsetAlongInPercentage/100.0),0);

        }
        else if (regionAcrossComponentProfileOrientation==0)
        {
            padOneUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMinCoordinateAlong() + clearChipProfileInfo.getPadOneUpperFilletIndex();
            regionAcrossComponent.setCenterXY((int)padOneUpperFilletPosition, regionAcrossComponent.getCenterY());
            regionAcrossComponent.translateAlongAcross(-(int)Math.round(padOneLength * profileOffsetAlongInPercentage/100.0),0);
       }
        regionAcrossComponent.setLengthAlong(Math.round(padOneLength /4.0f));
        //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
        regionAcrossComponent.setLengthAcross(Math.round((regionAcrossComponent.getLengthAcross() +
                                                         padOneJointInspectionData.getInterPadDistanceInPixels() *
                                                         2f * backgroundRegionShiftAsFractionOfInterPadDistance) * componentProfileLengthAcrossInPercentage) );

        int profileOrientation = MathUtil.getDegreesWithin0To359(regionAcrossComponent.getOrientationInDegrees() + 90);
        regionAcrossComponent.setOrientationInDegrees(profileOrientation);
    }
    else
    {
        int padOneMaxCoordinateAlong = (int)padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMaxCoordinateAlong();
        
        if(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong()<padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong())
        {
            //regionAcrossComponent.translateAlongAcross((int)Math.round(areaBetweenPadsLength * 0.25f + areaBetweenPadsLength /10.0f),0);
            regionAcrossComponent.translateAlongAcross((int)Math.round(areaBetweenPadsLength * 0.25f),0);
        }
        else
        {
            //regionAcrossComponent.translateAlongAcross((int)Math.round(areaBetweenPadsLength * 0.25f + areaBetweenPadsLength /10.0f),0);
            regionAcrossComponent.translateAlongAcross((int)Math.round(areaBetweenPadsLength * 0.25f),0);
        }

        regionAcrossComponent.setLengthAlong(Math.round(areaBetweenPadsLength /5.0f));
        //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
        regionAcrossComponent.setLengthAcross(Math.round((regionAcrossComponent.getLengthAcross() +
                                                         padOneJointInspectionData.getInterPadDistanceInPixels() *
                                                         2f * backgroundRegionShiftAsFractionOfInterPadDistance) * componentProfileLengthAcrossInPercentage) );

        int profileOrientation = MathUtil.getDegreesWithin0To359(regionAcrossComponent.getOrientationInDegrees() + 90);
        regionAcrossComponent.setOrientationInDegrees(profileOrientation);
    }

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, regionAcrossComponent);

    MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(regionAcrossComponent,
                                                                                                        MeasurementRegionEnum.COMPONENT_REGION);

    float[] grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(image, regionAcrossComponent);

    float[] estimatedBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(grayLevelProfileAcrossComponent);
    pad1CenterCoordinateAlongProfile = regionAcrossComponent.getCenterAcross();

    // Create a thickness profile.
    SolderThickness thicknessTable = AlgorithmUtil.getSolderThickness(subtype);
    int backgroundSensitivityOffset = AlgorithmUtil.getBackgroundSensitivityThreshold(subtype);
    float[] thicknessProfileAcrossInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent, estimatedBackgroundProfile, thicknessTable, backgroundSensitivityOffset);

 
    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                 this, false, true,
                                 componentRegionDiagnosticInfo);

    // Find the fillet edges on each side of the across profile.
    final float TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT) / 100.f;

    float targetSideFilletEdgeThickness = ArrayUtil.max(thicknessProfileAcrossInMillimeters) * TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS;
    float leftSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(thicknessProfileAcrossInMillimeters, 0,
                                                                                         thicknessProfileAcrossInMillimeters.length - 1, targetSideFilletEdgeThickness);
    float rightSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(thicknessProfileAcrossInMillimeters,
                                                                                          thicknessProfileAcrossInMillimeters.length - 1, 0, targetSideFilletEdgeThickness);
    boolean locationsAreValid = true;
    if (leftSideFilletIndex >= rightSideFilletIndex)
      locationsAreValid = false;

    MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[thicknessProfileAcrossInMillimeters.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.COMPONENT_PROFILE_REGION);
    if (locationsAreValid)
    {
      measurementRegionEnums[Math.round(leftSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;
      measurementRegionEnums[Math.round(rightSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;

      measurementRegionEnums[Math.round((long)((leftSideFilletIndex +  rightSideFilletIndex)/2.0))] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;

    }

    int padOneCenterAcross = 0;
    int padTwoCenterAcross = 0;

    if (regionAcrossComponentProfileOrientation==180 || regionAcrossComponentProfileOrientation ==270)
    {
        padOneCenterAcross = (int)(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAcross()- regionAcrossComponent.getMinCoordinateAlong());
        padTwoCenterAcross = (int)(padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAcross()- regionAcrossComponent.getMinCoordinateAlong());
    }
    else
    {
        padOneCenterAcross = (int)-(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAcross()- regionAcrossComponent.getMaxCoordinateAlong());
        padTwoCenterAcross = (int)-(padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAcross()- regionAcrossComponent.getMaxCoordinateAlong());
    }

    //int centerAcrossIndex = (int)Math.round(
    //    regionAcrossComponent.getCenterAlong() - regionAcrossComponent.getMinCoordinateAlong());

    float padOneMisalignmentAcrossPercentage = 0f;
    float padOneLengthAcross=0.0f;
    float padOneLocatedCenterAcross =0.0f;
    if (locationsAreValid)
    {
      float padOneLocatedLengthAcross = (rightSideFilletIndex - leftSideFilletIndex);
      padOneLocatedCenterAcross = (rightSideFilletIndex + leftSideFilletIndex)/2.0f;
      padOneLengthAcross = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAcross();

      if(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong()<padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong())
      {
        padOneMisalignmentAcrossPercentage =-((padOneLocatedCenterAcross -padOneCenterAcross)/ padOneLengthAcross)*100.f;
      }
      else
      {
        padOneMisalignmentAcrossPercentage =((padOneLocatedCenterAcross -padOneCenterAcross)/ padOneLengthAcross)*100.f;
      }
    }
    //float bodyWidthInMillimeters = MathUtil.convertNanometersToMillimeters(MagnificationEnum.NOMINAL.getNanoMetersPerPixel() * bodyWidthInPixels);
    LocalizedString profileAcrossLabelLocalizedString =
      new LocalizedString("ALGIAG_CHIP_MEASUREMENT_PROFILE_ACROSS_KEY", null);
    ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo = ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(componentInspectionData.getComponent().getReferenceDesignator(),
                                                                                                                                ProfileTypeEnum.SOLDER_PROFILE,
                                                                                                                                profileAcrossLabelLocalizedString,
                                                                                                                                0.0f,
                                                                                                                                ArrayUtil.max(thicknessProfileAcrossInMillimeters) * 1.1f,
                                                                                                                                thicknessProfileAcrossInMillimeters,
                                                                                                                                measurementRegionEnums,
                                                                                                                                MeasurementUnitsEnum.MILLIMETERS);

    RegionOfInterest leftSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(leftSideFilletIndex));
    MeasurementRegionDiagnosticInfo leftSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(leftSideEdgeRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
    RegionOfInterest rightSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(rightSideFilletIndex));
    MeasurementRegionDiagnosticInfo rightSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(rightSideEdgeRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
    RegionOfInterest centerOfSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round((long)((leftSideFilletIndex +  rightSideFilletIndex)/2.0)));
    MeasurementRegionDiagnosticInfo centerOfSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(centerOfSideEdgeRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

    if ((Math.round(padOneCenterAcross) >= 0)&& (Math.round(padOneCenterAcross) < regionAcrossComponent.getLengthAlong()))
    {
        RegionOfInterest padOneCenterRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(padOneCenterAcross));
        MeasurementRegionDiagnosticInfo padOneCenterRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneCenterRegion, MeasurementRegionEnum.PAD_REGION);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                     this, false, true,
                                     labeledPadThicknessProfileDiagInfo,
                                     leftSideEdgeRegionDiagnosticInfo,
                                     rightSideEdgeRegionDiagnosticInfo,
                                     centerOfSideEdgeRegionDiagnosticInfo,
                                     padOneCenterRegionDiagnosticInfo);
    }
    else
    {
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                     this, false, true,
                                     labeledPadThicknessProfileDiagInfo,
                                     leftSideEdgeRegionDiagnosticInfo,
                                     centerOfSideEdgeRegionDiagnosticInfo,
                                     rightSideEdgeRegionDiagnosticInfo);
    }




    //get pad Two edge
    int profileOrientation = MathUtil.getDegreesWithin0To359(regionAcrossComponent.getOrientationInDegrees()-90);
    regionAcrossComponent.setOrientationInDegrees(profileOrientation);
//    2 --> 1  => 0                     
//
//    1
//    ^
//    |  => 90                          
//    2
//
//    1 --> 2  => 180                   
//
//    2
//    ^
//    |  => 270                         
//    1
    regionAcrossComponent.setOrientationInDegrees(regionAcrossComponentProfileOrientation);
    regionAcrossComponent.setLengthAlong((int)regionAcrossComponentLengthAlong);
    regionAcrossComponent.setLengthAcross((int)regionAcrossComponentLengthAcross);

     if(testAsClear)
     {
        int padTwoMinCoordinateAlong = (int)padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinCoordinateAlong();
        if (regionAcrossComponentProfileOrientation==0 || regionAcrossComponentProfileOrientation ==180)
        {
            regionAcrossComponent.setCenterXY((int)regionAcrossComponentCenterAlong,
                                              (int)regionAcrossComponentCenterAcross);
        }
        else
        {
             regionAcrossComponent.setCenterXY((int)regionAcrossComponentCenterAcross,
                                              (int)regionAcrossComponentCenterAlong);
        }

        if (regionAcrossComponentProfileOrientation==90)
        {
           padTwoUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMaxCoordinateAlong() - clearChipProfileInfo.getPadTwoUpperFilletIndex();
            regionAcrossComponent.setCenterXY( regionAcrossComponent.getCenterX(),(int)padTwoUpperFilletPosition);
            regionAcrossComponent.translateAlongAcross((int)Math.round(padTwoLength * profileOffsetAlongInPercentage/100.0),0);

        }
        else if (regionAcrossComponentProfileOrientation==270)
        {
            padTwoUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMinCoordinateAlong() + clearChipProfileInfo.getPadTwoUpperFilletIndex();
            regionAcrossComponent.setCenterXY( regionAcrossComponent.getCenterX(),(int)padTwoUpperFilletPosition);
            regionAcrossComponent.translateAlongAcross((int)Math.round(padTwoLength * profileOffsetAlongInPercentage/100.0),0);

        }else if (regionAcrossComponentProfileOrientation==180)
        {
           padTwoUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMaxCoordinateAlong() - clearChipProfileInfo.getPadTwoUpperFilletIndex();
            regionAcrossComponent.setCenterXY((int)padTwoUpperFilletPosition, regionAcrossComponent.getCenterY());
            regionAcrossComponent.translateAlongAcross((int)Math.round(padTwoLength * profileOffsetAlongInPercentage/100.0),0);

        }
        else if (regionAcrossComponentProfileOrientation==0)
        {
            padTwoUpperFilletPosition  =  clearChipProfileInfo.getComponentOrthogonalRegionOfInterest().getMinCoordinateAlong() + clearChipProfileInfo.getPadTwoUpperFilletIndex();
            regionAcrossComponent.setCenterXY((int)padTwoUpperFilletPosition, regionAcrossComponent.getCenterY());
            regionAcrossComponent.translateAlongAcross((int)Math.round(padTwoLength * profileOffsetAlongInPercentage/100.0),0);
        }

        regionAcrossComponent.setLengthAlong(Math.round(padOneLength /4.0f));
        //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
        regionAcrossComponent.setLengthAcross(Math.round((regionAcrossComponent.getLengthAcross() +
                                                         padOneJointInspectionData.getInterPadDistanceInPixels() *
                                                         2f * backgroundRegionShiftAsFractionOfInterPadDistance) * componentProfileLengthAcrossInPercentage));

        profileOrientation = MathUtil.getDegreesWithin0To359(regionAcrossComponent.getOrientationInDegrees()+90);
        regionAcrossComponent.setOrientationInDegrees(profileOrientation);

     }
     else
     {
        int padTwoMinCoordinateAlong = (int)padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinCoordinateAlong();
        if (regionAcrossComponentProfileOrientation==0 || regionAcrossComponentProfileOrientation ==180)
        {
            regionAcrossComponent.setCenterXY((int)regionAcrossComponentCenterAlong,
                                              (int)regionAcrossComponentCenterAcross);
        }
        else
        {
             regionAcrossComponent.setCenterXY((int)regionAcrossComponentCenterAcross,
                                              (int)regionAcrossComponentCenterAlong);
        }
        if(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong()<padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong())
        {
            //regionAcrossComponent.translateAlongAcross(-(int)Math.round(areaBetweenPadsLength * 0.25f + areaBetweenPadsLength /10.0f),0);
            regionAcrossComponent.translateAlongAcross(-(int)Math.round(areaBetweenPadsLength * 0.25f),0);
        }
        else
        {
            //regionAcrossComponent.translateAlongAcross(-(int)Math.round(areaBetweenPadsLength * 0.25f + areaBetweenPadsLength /10.0f),0);
            regionAcrossComponent.translateAlongAcross(-(int)Math.round(areaBetweenPadsLength * 0.25f),0);
        }

        regionAcrossComponent.setLengthAlong(Math.round(areaBetweenPadsLength /5.0f));
        //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
        regionAcrossComponent.setLengthAcross(Math.round((regionAcrossComponent.getLengthAcross() +
                                                         padOneJointInspectionData.getInterPadDistanceInPixels() *
                                                         2f * backgroundRegionShiftAsFractionOfInterPadDistance) * componentProfileLengthAcrossInPercentage) );

        profileOrientation = MathUtil.getDegreesWithin0To359(regionAcrossComponent.getOrientationInDegrees()+90);
        regionAcrossComponent.setOrientationInDegrees(profileOrientation);
     }


    pad2CenterCoordinateAlongProfile = regionAcrossComponent.getCenterAcross();

    AlgorithmUtil.truncateRegionSymmetricallyIntoImageIfNecessary(image, regionAcrossComponent);
    componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(regionAcrossComponent, MeasurementRegionEnum.COMPONENT_REGION);
    grayLevelProfileAcrossComponent = ImageFeatureExtraction.profile(image, regionAcrossComponent);
    estimatedBackgroundProfile = ProfileUtil.getBackgroundTrendProfile(grayLevelProfileAcrossComponent);

    // Create a thickness profile.
    thicknessProfileAcrossInMillimeters =
      AlgorithmUtil.convertGrayLevelProfileToThicknessProfileInMillimeters(grayLevelProfileAcrossComponent, estimatedBackgroundProfile, thicknessTable, backgroundSensitivityOffset);


    _diagnostics.postDiagnostics(reconstructionRegion,
                                 sliceNameEnum,
                                 componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getSubtype(),
                                 this, false, true,
                                 componentRegionDiagnosticInfo);

    // Find the fillet edges on each side of the across profile.
    final float padTwo_TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS =
      (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_SIDE_THICKNESS_PERCENT) / 100.f;

    targetSideFilletEdgeThickness = ArrayUtil.max(thicknessProfileAcrossInMillimeters) * padTwo_TARGET_ACROSS_EDGE_FRACTION_OF_MAX_THICKNESS;
    leftSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(thicknessProfileAcrossInMillimeters, 0,
                                                                                         thicknessProfileAcrossInMillimeters.length - 1, targetSideFilletEdgeThickness);
    rightSideFilletIndex = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(thicknessProfileAcrossInMillimeters,
                                                                                          thicknessProfileAcrossInMillimeters.length - 1, 0, targetSideFilletEdgeThickness);
    locationsAreValid = true;
    if (leftSideFilletIndex >= rightSideFilletIndex)
      locationsAreValid = false;

    measurementRegionEnums = new MeasurementRegionEnum[thicknessProfileAcrossInMillimeters.length];
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.COMPONENT_PROFILE_REGION);
    if (locationsAreValid)
    {
      measurementRegionEnums[Math.round(leftSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;
      measurementRegionEnums[Math.round(rightSideFilletIndex)] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;
      measurementRegionEnums[Math.round((long)((leftSideFilletIndex +  rightSideFilletIndex)/2.0))] = MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION;

    }

    profileAcrossLabelLocalizedString =
      new LocalizedString("ALGIAG_CHIP_MEASUREMENT_PROFILE_ACROSS_KEY", null);
    labeledPadThicknessProfileDiagInfo = ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(componentInspectionData.getComponent().getReferenceDesignator(),
                                                                                                                                ProfileTypeEnum.SOLDER_PROFILE,
                                                                                                                                profileAcrossLabelLocalizedString,
                                                                                                                                0.0f,
                                                                                                                                ArrayUtil.max(thicknessProfileAcrossInMillimeters) * 1.1f,
                                                                                                                                thicknessProfileAcrossInMillimeters,
                                                                                                                                measurementRegionEnums,
                                                                                                                                MeasurementUnitsEnum.MILLIMETERS);

    leftSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(leftSideFilletIndex));
    leftSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(leftSideEdgeRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
    rightSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(rightSideFilletIndex));
    rightSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(rightSideEdgeRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
    centerOfSideEdgeRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round((long)((leftSideFilletIndex +  rightSideFilletIndex)/2.0)));
    centerOfSideEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(centerOfSideEdgeRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

    float padTwoMisalignmentAcrossPercentage = 0f;
    float padTwoLocatedCenterAcross = 0f;
    if (locationsAreValid)
    {
      //padTwoMisalignmentAlongPercentage = ((rightSideFilletIndex - leftSideFilletIndex)/2.0f -padTwoCenterAcross)/( padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAcross());
      float padTwoLocatedLengthAcross = (rightSideFilletIndex - leftSideFilletIndex);
      padTwoLocatedCenterAcross = (rightSideFilletIndex + leftSideFilletIndex)/2.0f;
      float padTwoLengthAcross = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAcross();
      //padTwoMisalignmentAcrossPercentage =Math.abs((padTwoLocatedCenterAcross -padTwoCenterAcross)/ padTwoLengthAcross)*100.0f;
      
        if(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong()<padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong())
        {
            padTwoMisalignmentAcrossPercentage =-((padTwoLocatedCenterAcross -padTwoCenterAcross)/ padTwoLengthAcross)*100.0f;
        }
        else
        {
            padTwoMisalignmentAcrossPercentage =((padTwoLocatedCenterAcross -padTwoCenterAcross)/ padTwoLengthAcross)*100.0f;
        }

   }

    // get center of mass for each profile
    if ((Float.isNaN(padOneLocatedCenterAcross)) || (Float.isNaN(padTwoLocatedCenterAcross)))
    {
         componentRotationInDegreesRef.setValue(0.0f);   

    }
    else
    {
        double distanceAcrossBetweenPadCentroidsInPixels = Math.abs(padOneLocatedCenterAcross - padTwoLocatedCenterAcross);
        double distanceAlongBetweenPadCentroidsInPixels = Math.abs(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong() - padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong());
        double rotationInRadians = Math.atan(distanceAcrossBetweenPadCentroidsInPixels / distanceAlongBetweenPadCentroidsInPixels);
        double rotationInDegrees = Math.toDegrees(rotationInRadians);

        if(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong()<padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong())
        {
            if (padOneLocatedCenterAcross < padTwoLocatedCenterAcross)
            {
                componentRotationInDegreesRef.setValue(-(float)rotationInDegrees);
            }
            else
            {
                componentRotationInDegreesRef.setValue((float)rotationInDegrees);           
            }
        }
        else
        {
            if (padOneLocatedCenterAcross < padTwoLocatedCenterAcross)
            {
                componentRotationInDegreesRef.setValue(-(float)rotationInDegrees);
            }
            else
            {
                componentRotationInDegreesRef.setValue((float)rotationInDegrees);           
            }        
        }
    }

    
        RegionOfInterest orientationLineRegion = null;
        
        if ( regionAcrossComponentProfileOrientation==0)
        {
            if(padTwoLocatedCenterAcross > padOneLocatedCenterAcross)
            {
            orientationLineRegion = RegionOfInterest.createLineRegionFromRegionBorder((int)pad2CenterCoordinateAlongProfile,
                                                                                           (int)(regionAcrossComponent.getMaxCoordinateAlong() - padTwoLocatedCenterAcross + 1),
                                                                                           (int)pad1CenterCoordinateAlongProfile,
                                                                                           (int)(regionAcrossComponent.getMaxCoordinateAlong() - padOneLocatedCenterAcross + 1 ),
                                                                                           
                                                                                           regionAcrossComponentProfileOrientation,
                                                                                           RegionShapeEnum.LINE);
            }
            else
            {
            orientationLineRegion = RegionOfInterest.createLineRegionFromRegionBorder(
                                                                                           (int)pad1CenterCoordinateAlongProfile,
                                                                                           (int)(regionAcrossComponent.getMaxCoordinateAlong() - padOneLocatedCenterAcross + 1 ),
                                                                                           (int)pad2CenterCoordinateAlongProfile,
                                                                                           (int)(regionAcrossComponent.getMaxCoordinateAlong() - padTwoLocatedCenterAcross + 1),
                                                                                           regionAcrossComponentProfileOrientation,
                                                                                           RegionShapeEnum.LINE);
            }
        }
        else if ( regionAcrossComponentProfileOrientation == 180)
        {
             if(padTwoLocatedCenterAcross > padOneLocatedCenterAcross)
            {           
                 orientationLineRegion = RegionOfInterest.createLineRegionFromRegionBorder((int)pad1CenterCoordinateAlongProfile,
                                                                                           (int)(int)(regionAcrossComponent.getMinCoordinateAlong() + padOneLocatedCenterAcross),
                                                                                           (int)pad2CenterCoordinateAlongProfile,
                                                                                           (int)(int)(regionAcrossComponent.getMinCoordinateAlong() + padTwoLocatedCenterAcross),
                                                                                           
                                                                                           regionAcrossComponentProfileOrientation,
                                                                                           RegionShapeEnum.LINE);
             }else
             {
                 orientationLineRegion = RegionOfInterest.createLineRegionFromRegionBorder((int)pad2CenterCoordinateAlongProfile,
                                                                                           (int)(int)(regionAcrossComponent.getMinCoordinateAlong() + padTwoLocatedCenterAcross),
                                                                                           (int)pad1CenterCoordinateAlongProfile,
                                                                                           (int)(int)(regionAcrossComponent.getMinCoordinateAlong() + padOneLocatedCenterAcross),
                                                                                           regionAcrossComponentProfileOrientation,
                                                                                           RegionShapeEnum.LINE);
                 
             }
        }
        else if ( regionAcrossComponentProfileOrientation==90)
        {
            orientationLineRegion = RegionOfInterest.createLineRegionFromRegionBorder((int)(regionAcrossComponent.getMaxCoordinateAlong() - padOneLocatedCenterAcross + 1),
                                                                                           (int)pad1CenterCoordinateAlongProfile,
                                                                                           (int)(regionAcrossComponent.getMaxCoordinateAlong()-padTwoLocatedCenterAcross + 1),
                                                                                           (int)pad2CenterCoordinateAlongProfile,
                                                                                           regionAcrossComponentProfileOrientation,
                                                                                           RegionShapeEnum.LINE);
        }
        else if ( regionAcrossComponentProfileOrientation==270)
        {
            orientationLineRegion = RegionOfInterest.createLineRegionFromRegionBorder((int)(regionAcrossComponent.getMinCoordinateAlong() + padTwoLocatedCenterAcross),
                                                                                           (int)pad2CenterCoordinateAlongProfile,
                                                                                           (int)(regionAcrossComponent.getMinCoordinateAlong()+ padOneLocatedCenterAcross),
                                                                                           (int)pad1CenterCoordinateAlongProfile,
                                                                                           regionAcrossComponentProfileOrientation,
                                                                                           RegionShapeEnum.LINE);
        }


        MeasurementRegionDiagnosticInfo orientationLineRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(orientationLineRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

    if ((Math.round(padTwoCenterAcross) >= 0)&& (Math.round(padTwoCenterAcross) < regionAcrossComponent.getLengthAlong()))
    {
        RegionOfInterest padTwoCenterRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(regionAcrossComponent, Math.round(padTwoCenterAcross));
        MeasurementRegionDiagnosticInfo padTwoCenterRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoCenterRegion, MeasurementRegionEnum.PAD_REGION);

        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                     this, false, true,
                                     labeledPadThicknessProfileDiagInfo,
                                     leftSideEdgeRegionDiagnosticInfo,
                                     rightSideEdgeRegionDiagnosticInfo,
                                     centerOfSideEdgeRegionDiagnosticInfo,
                                     padTwoCenterRegionDiagnosticInfo,
                                     orientationLineRegionDiagnosticInfo);
    }
    else
    {
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                     this, false, true,
                                     labeledPadThicknessProfileDiagInfo,
                                     leftSideEdgeRegionDiagnosticInfo,
                                     centerOfSideEdgeRegionDiagnosticInfo,
                                     rightSideEdgeRegionDiagnosticInfo,
                                     orientationLineRegionDiagnosticInfo);
    }


    return Math.abs(padTwoMisalignmentAcrossPercentage)>Math.abs(padOneMisalignmentAcrossPercentage)? padTwoMisalignmentAcrossPercentage:padOneMisalignmentAcrossPercentage;

  }

   // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
   private float measureComponentMisalignmentAlongInPercentage(JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        float[] componentDerivativeProfile,
                                                        float[] componentThicknessProfile,
                                                        float pad1FilletIndex,
                                                        float pad2FilletIndex,
                                                        FloatRef pad1AlongOffsetInMils,
                                                        FloatRef pad2AlongOffsetInMils,
                                                        FloatRef pad1MisalignmentAlongInPercentage,
                                                        FloatRef pad2MisalignmentAlongInPercentage,
                                                        SliceNameEnum sliceNameEnum,
                                                        Image image,
                                                        ReconstructionRegion reconstructionRegion,
                                                        RegionOfInterest componentRegionOfInterest,
                                                        boolean testAsClear)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(componentDerivativeProfile != null);
    Assert.expect(pad1FilletIndex >= 0);
    Assert.expect(pad2FilletIndex >= 0);
    Assert.expect(image != null);
    Assert.expect(reconstructionRegion != null);

    ComponentInspectionData componentInspectionData = padOneJointInspectionData.getComponentInspectionData();
    //ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
    Subtype subtype = padOneJointInspectionData.getSubtype();
    float componentMisalignmentAlongInPercentage = 0.0f;
 
    float padOneFilletDistanceFromInnerEdgeAlong = 0.0f;
    float padTwoFilletDistanceFromInnerEdgeAlong = 0.0f;

    componentMisalignmentAlongInPercentage =   getPadFilletDistanceFromInnerEdgeAlong( padOneJointInspectionData,
                                                         padTwoJointInspectionData,
                                                         pad1FilletIndex,
                                                         pad2FilletIndex,
                                                         pad1AlongOffsetInMils,
                                                         pad2AlongOffsetInMils,
                                                         componentRegionOfInterest
                                                        );

    // Changed from mils to percentage requested by Jeremy P. - 27-Jan-2010 - by Lim, Seng Yew
    pad1AlongOffsetInMils.setValue((float)(100*pad1AlongOffsetInMils.getValue())/padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong());
    pad2AlongOffsetInMils.setValue((float)(100*pad2AlongOffsetInMils.getValue())/padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong());

    if (testAsClear)
    {
      float nominalAlongShiftClearInPercentage = getNominalAlongShiftClearInPercentage(subtype);
      padOneFilletDistanceFromInnerEdgeAlong = pad1AlongOffsetInMils.getValue()-nominalAlongShiftClearInPercentage;
      padTwoFilletDistanceFromInnerEdgeAlong = pad2AlongOffsetInMils.getValue()-nominalAlongShiftClearInPercentage;
    }
    else
    {
      float nominalAlongShiftOpaqueInPercentage = getNominalAlongShiftOpaqueInPercentage(subtype);
      padOneFilletDistanceFromInnerEdgeAlong = pad1AlongOffsetInMils.getValue()-nominalAlongShiftOpaqueInPercentage;
      padTwoFilletDistanceFromInnerEdgeAlong = pad2AlongOffsetInMils.getValue()-nominalAlongShiftOpaqueInPercentage;
    }
    pad1MisalignmentAlongInPercentage.setValue(padOneFilletDistanceFromInnerEdgeAlong);
    pad2MisalignmentAlongInPercentage.setValue(padTwoFilletDistanceFromInnerEdgeAlong);
    // CR33213 - Rework by Lim, Seng Yew on 27-Jan-2010
    // Change component shift along formula requested by Jeremy,
    // because original componentMisalignmentAlongInPercentage uses only pad 1 length in calculation
    componentMisalignmentAlongInPercentage = (padOneFilletDistanceFromInnerEdgeAlong - padTwoFilletDistanceFromInnerEdgeAlong) / 2;

    RegionOfInterest padOneRegionOfInterest = new RegionOfInterest(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels());
    RegionOfInterest padTwoRegionOfInterest = new RegionOfInterest(padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels());

    if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
    {
        RegionOfInterest padOneCenterRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(padOneRegionOfInterest, Math.round(0));
        RegionOfInterest padTwoCenterRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(padTwoRegionOfInterest, Math.round(0));

        MeasurementRegionDiagnosticInfo padOneCenterRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneCenterRegion,
                                                                                                                MeasurementRegionEnum.PAD_REGION);
        MeasurementRegionDiagnosticInfo padTwoCenterRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoCenterRegion,
                                                                                                                MeasurementRegionEnum.PAD_REGION);
       // highlight the pad one inner fillet region
      RegionOfInterest padOneUpperFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegionOfInterest, Math.round(pad1FilletIndex));

      MeasurementRegionDiagnosticInfo padOneFilletEdgeRegionDiagnosticInfo = null;
      if(testAsClear)
      {
        padOneFilletEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneUpperFilletRegion, MeasurementRegionEnum.INNER_FILLET_EDGE_REGION);
      }
      else
      {
        padOneFilletEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneUpperFilletRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
      }
      // highlight the pad two inner fillet region
      RegionOfInterest padTwoUpperFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegionOfInterest, Math.round(pad2FilletIndex));
      MeasurementRegionDiagnosticInfo padTwoFilletEdgeRegionDiagnosticInfo = null;
      if(testAsClear)
      {
        padTwoFilletEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoUpperFilletRegion, MeasurementRegionEnum.INNER_FILLET_EDGE_REGION);
      }
      else
      {
        padTwoFilletEdgeRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoUpperFilletRegion, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);
      }

      AlgorithmDiagnostics.getInstance().postDiagnostics(reconstructionRegion,
                                                         sliceNameEnum,
                                                         componentInspectionData.getPadOneJointInspectionData().getSubtype(),
                                                         this, false, true,
                                                         padOneCenterRegionDiagnosticInfo,
                                                         padTwoCenterRegionDiagnosticInfo,
                                                         padOneFilletEdgeRegionDiagnosticInfo,
                                                         padTwoFilletEdgeRegionDiagnosticInfo);
    }
    return componentMisalignmentAlongInPercentage;
  }

    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    private float getPadFilletDistanceFromInnerEdgeAlong(JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        float pad1FilletIndex,
                                                        float pad2FilletIndex,
                                                        FloatRef pad1AlongOffsetInMils,
                                                        FloatRef pad2AlongOffsetInMils,
                                                        RegionOfInterest componentRegionOfInterest
                                                        )
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(pad1FilletIndex >= 0);
    Assert.expect(pad2FilletIndex >= 0);
    Assert.expect(componentRegionOfInterest != null);

    float componentMisalignmentAlongInPercentage = 0.0f;
    float componentMisalignmentAlongInMils = 0.0f;
    double padOneFilletEdgeAlong = 0.0f;
    double padTwoFilletEdgeAlong = 0.0f;
    double padOneFilletDistanceFromInnerEdgeAlong = 0.0f;
    double padTwoFilletDistanceFromInnerEdgeAlong = 0.0f;
    int regionAcrossComponentProfileOrientation =  componentRegionOfInterest.getOrientationInDegrees();

    if(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong()<padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong())
    {
       padOneFilletEdgeAlong = componentRegionOfInterest.getMaxCoordinateAlong() - pad1FilletIndex;
       padTwoFilletEdgeAlong = componentRegionOfInterest.getMaxCoordinateAlong() - pad2FilletIndex;

       //                          F1 <------|        |------> F2
       padOneFilletDistanceFromInnerEdgeAlong = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMaxCoordinateAlong() - padOneFilletEdgeAlong;
       padTwoFilletDistanceFromInnerEdgeAlong = padTwoFilletEdgeAlong - padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinCoordinateAlong();

        if (regionAcrossComponentProfileOrientation == 90 || regionAcrossComponentProfileOrientation == 270)
        {
           if(padOneFilletDistanceFromInnerEdgeAlong > padTwoFilletDistanceFromInnerEdgeAlong)
           {
                componentMisalignmentAlongInMils=((float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           }
           else
           {
                componentMisalignmentAlongInMils=(-(float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           } 
        }
        else
        {
           if(padOneFilletDistanceFromInnerEdgeAlong > padTwoFilletDistanceFromInnerEdgeAlong)
           {
                componentMisalignmentAlongInMils=(-(float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           }
           else
           {
                componentMisalignmentAlongInMils=((float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           } 
        }
        // get component misalignment along in percentage
        componentMisalignmentAlongInPercentage =(float)(100*(componentMisalignmentAlongInMils)/2.0f)/padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    }
    else
    {
       //                          F2 <------|        |------> F1
       padOneFilletEdgeAlong =  pad1FilletIndex+componentRegionOfInterest.getMinCoordinateAlong();
       padTwoFilletEdgeAlong =  pad2FilletIndex+componentRegionOfInterest.getMinCoordinateAlong();

       padOneFilletDistanceFromInnerEdgeAlong = padOneFilletEdgeAlong - padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMinCoordinateAlong();
       padTwoFilletDistanceFromInnerEdgeAlong = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getMaxCoordinateAlong() - padTwoFilletEdgeAlong;

       if (regionAcrossComponentProfileOrientation == 90 || regionAcrossComponentProfileOrientation == 270)
       {
           if(padOneFilletDistanceFromInnerEdgeAlong > padTwoFilletDistanceFromInnerEdgeAlong)
           {
                componentMisalignmentAlongInMils=(-(float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           }
           else
           {
                componentMisalignmentAlongInMils=((float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           }      
       }
       else
       {
           if(padOneFilletDistanceFromInnerEdgeAlong > padTwoFilletDistanceFromInnerEdgeAlong)
           {
                componentMisalignmentAlongInMils=((float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           }
           else
           {
                componentMisalignmentAlongInMils=(-(float) Math.abs(padOneFilletDistanceFromInnerEdgeAlong-padTwoFilletDistanceFromInnerEdgeAlong));
           }
       }

        // get component misalignment along in percentage
        componentMisalignmentAlongInPercentage =(float)(100*(componentMisalignmentAlongInMils)/2.0f)/padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    }

    pad1AlongOffsetInMils.setValue((float)padOneFilletDistanceFromInnerEdgeAlong);
    pad2AlongOffsetInMils.setValue((float)padTwoFilletDistanceFromInnerEdgeAlong);

    return componentMisalignmentAlongInPercentage;
  }

  /**
   * @author Peter Esbensen
   */
  private ClearChipProfileInfo collectMeasurementsForClearChips(float[] componentThicknessProfile,
                                                RegionOfInterest componentRegion,
                                                JointInspectionData padOneJointInspectionData,
                                                JointInspectionData padTwoJointInspectionData,
                                                float[] componentDerivativeProfile,
                                                final float padOneFilletIndex,
                                                final float padTwoFilletIndex,
                                                SliceNameEnum sliceNameEnum,
                                                ReconstructedSlice reconstructedSlice,
                                                ReconstructionRegion reconstructionRegion,
                                                ReconstructedImages reconstructedImages,//LN - Clear Tombstone
                                                int nominalBodyLengthInPixels,
                                                final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(componentRegion != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(componentDerivativeProfile != null);
    Assert.expect(padOneFilletIndex >= 0);
    Assert.expect(padTwoFilletIndex >= 0);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(nominalBodyLengthInPixels > 0);

    ClearChipProfileInfo clearChipProfileInfo = new ClearChipProfileInfo();
    clearChipProfileInfo.setComponentBodyThicknessProfile(componentThicknessProfile);
    clearChipProfileInfo.setPadOneFilletEdgeIndex(Math.round(padOneFilletIndex));
    clearChipProfileInfo.setPadTwoFilletEdgeIndex(Math.round(padTwoFilletIndex));
    clearChipProfileInfo.setComponentOrthogonalRegionOfInterest(componentRegion);

    float upperFilletOffsetAsFractionOfBodyLength = (Float)padOneJointInspectionData.getSubtype().
        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET) * 0.01f;
    float lowerFilletOffsetAsFractionOfPadLength = (Float)padOneJointInspectionData.getSubtype().
        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET) * 0.01f;

    getClearChipFilletThicknesses(clearChipProfileInfo, padOneJointInspectionData,
                                  padTwoJointInspectionData, padOneFilletIndex, padTwoFilletIndex,
                                  upperFilletOffsetAsFractionOfBodyLength,
                                  lowerFilletOffsetAsFractionOfPadLength);

    RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest componentRoi = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

    clearChipProfileInfo.setPadOneLeftSideIndex(ChipAlgorithmUtil.getPadOneLeftSideIndex(componentThicknessProfile, padOneJointInspectionData, padOneRoi, componentRoi));
    clearChipProfileInfo.setPadOneRightSideIndex(ChipAlgorithmUtil.getPadOneRightSideIndex(componentThicknessProfile, padOneJointInspectionData, componentRoi));
    clearChipProfileInfo.setPadTwoRightSideIndex(ChipAlgorithmUtil.getPadTwoRightSideIndex(componentThicknessProfile, padTwoJointInspectionData, padTwoRoi, componentRoi));
    clearChipProfileInfo.setPadTwoLeftSideIndex(ChipAlgorithmUtil.getPadTwoLeftSideIndex(padTwoJointInspectionData, componentRoi));

    float bodyLengthInPixels = estimateClearBodyLengthInPixels(padOneJointInspectionData, componentThicknessProfile, componentDerivativeProfile);
    clearChipProfileInfo.setComponentBodyLengthInMillimeters(MILIMETER_PER_PIXEL* bodyLengthInPixels);

    postClearChipFilletProfileDiagnostics(clearChipProfileInfo, reconstructionRegion, sliceNameEnum, padOneJointInspectionData);
    
    //Lim, Lay Ngor - OKI tombstone - Area detection Start algorithm without posting
    String chipSecondLevelDetectionMethod = 
      (String)padOneJointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD);    
    if(chipSecondLevelDetectionMethod.equalsIgnoreCase("Disable"))
      ;//do nothing
    else if(chipSecondLevelDetectionMethod.equalsIgnoreCase("Solder Area"))
    {
      //update clearChipProfileInfo inside the function.
      getSolderAreaPercentageWithPostDiagnostics(padOneJointInspectionData, padTwoJointInspectionData, sliceNameEnum, reconstructedSlice, 
        reconstructedImages, clearChipProfileInfo);
    }
    //ShengChuan - Clear Tombstone Start
    else if(chipSecondLevelDetectionMethod.equalsIgnoreCase("Roundness Calculation"))
    {
      getRoundnessCalculationPercentageWithPostDiagnostics(padOneJointInspectionData, padTwoJointInspectionData, sliceNameEnum, 
        reconstructedImages, reconstructedSlice);    
    }
    else if (chipSecondLevelDetectionMethod.equalsIgnoreCase("Template Match"))
    {    
      if (padOneJointInspectionData.getPad().getComponent().hasExpectedImageTemplateLearning(padOneJointInspectionData.getPad(), padOneJointInspectionData.getPad().getComponent().getBoard(), sliceNameEnum, reconstructedSlice.getOrthogonalImage()))
      {
        getMatchPatternPercentage(padOneJointInspectionData, padTwoJointInspectionData, sliceNameEnum, reconstructedSlice);
      }
      else
      {
        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_HAVE_TEMPLATE_TO_MATCH_WARNING_KEY",
          new String[]
          {
            padOneJointInspectionData.getComponentInspectionData().getComponent().getReferenceDesignator()
          }));
      }
    }
    //ShengChuan - Clear Tombstone End
    else
      Assert.expect(false, "Chip Open Second Level Detection Selection Out of Range!");//error selection!
    //Lim, Lay Ngor - OKI tombstone - End Posting
    
    //Start Post diagnostic information
    storeAndPostClearChipMeasurements(clearChipProfileInfo, padOneJointInspectionData, padTwoJointInspectionData,
                                      reconstructionRegion, sliceNameEnum);

    //LayNgor - Move the posting of diagnostic information before second level detection for tombstone
//    postClearChipFilletProfileDiagnostics(clearChipProfileInfo, reconstructionRegion,
//                                          sliceNameEnum, padOneJointInspectionData);
  
    return clearChipProfileInfo;
//    _clearChipProfileInfo = clearChipProfileInfo;
    //_padOneUpperFilletPosition
  }

  /**
   * Calculate the solder area percentage and post the solder area diagnostic information.
   * Code re-factor. Move the solder area detection to a single function.
   * @author Lim, Lay Ngor
   */
  private void getSolderAreaPercentageWithPostDiagnostics(JointInspectionData padOneJointInspectionData, JointInspectionData padTwoJointInspectionData,
    SliceNameEnum sliceNameEnum, ReconstructedSlice reconstructedSlice, 
    ReconstructedImages reconstructedImages, ClearChipProfileInfo clearChipProfileInfo) 
  {
    Assert.expect(padOneJointInspectionData !=null);
    Assert.expect(padTwoJointInspectionData !=null);
    Assert.expect(sliceNameEnum !=null);
    Assert.expect(reconstructedImages !=null);
    Assert.expect(clearChipProfileInfo !=null);
    
    //find a way to optimise the speed of this algorithm
    //Smaller ROI will get better threshold here
    // Get the located joint ROI.
    RegionOfInterest padOneLocatorRoiForArea = Locator.getRegionOfInterestAtMeasuredLocation(padOneJointInspectionData);    
    RegionOfInterest padTwoLocatorRoiForArea = Locator.getRegionOfInterestAtMeasuredLocation(padTwoJointInspectionData);    
    Image image1 = Image.createCopy(reconstructedSlice.getOrthogonalImage(), padOneLocatorRoiForArea);
    Image image2 = Image.createCopy(reconstructedSlice.getOrthogonalImage(), padTwoLocatorRoiForArea);    

    final float weightage = 0.65f;
    clearChipProfileInfo.setPadOneSolderAreaPercentage(calculateSolderAreaPercentage(image1, padOneLocatorRoiForArea, weightage));
    clearChipProfileInfo.setPadTwoSolderAreaPercentage(calculateSolderAreaPercentage(image2, padTwoLocatorRoiForArea, weightage));
    //Lim, Lay Ngor - OKI tombstone - Area detection - End Algorithm

    //Lim, Lay Ngor - OKI tombstone - Start Posting
    //Area detection
    if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getJointTypeEnum(), this))
    {    
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, padOneJointInspectionData.getSubtype(), this);        
      MeasurementRegionDiagnosticInfo regionOneDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneLocatorRoiForArea, MeasurementRegionEnum.PAD_REGION);
      MeasurementRegionDiagnosticInfo regionTwoDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoLocatorRoiForArea, MeasurementRegionEnum.PAD_REGION);

      //For debugging use.
  //    RegionOfInterest threshold1ROI = new RegionOfInterest(padOneLocatorRoiForArea);
  //    threshold1ROI.setWidthKeepingSameCenter(padOneLocatorRoiForArea.getWidth() - 5);//best 4
  //    threshold1ROI.setHeightKeepingSameCenter(padOneLocatorRoiForArea.getHeight() - 7);// best 6
  //    RegionOfInterest threshold2ROI = new RegionOfInterest(padTwoLocatorRoiForArea);
  //    threshold2ROI.setWidthKeepingSameCenter(padTwoLocatorRoiForArea.getWidth() - 5);//best 4
  //    threshold2ROI.setHeightKeepingSameCenter(padTwoLocatorRoiForArea.getHeight() - 7);// best 6    
  //    MeasurementRegionDiagnosticInfo regionOneTDiagnosticInfo = new MeasurementRegionDiagnosticInfo(threshold1ROI, MeasurementRegionEnum.PIN_REGION);
  //    MeasurementRegionDiagnosticInfo regionTwoTDiagnosticInfo = new MeasurementRegionDiagnosticInfo(threshold2ROI, MeasurementRegionEnum.PIN_REGION);    
  //    _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, padOneJointInspectionData, this, false, regionOneTDiagnosticInfo);
  //    _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, padTwoJointInspectionData, this, false, regionTwoTDiagnosticInfo);

      _diagnostics.postDiagnostics(reconstructedImages.getReconstructionRegion(), sliceNameEnum, padOneJointInspectionData.getSubtype(),
        this, false, true, regionOneDiagnosticInfo, new OverlayImageDiagnosticInfo(image1, padOneLocatorRoiForArea, "SolderArea"),
          regionTwoDiagnosticInfo, new OverlayImageDiagnosticInfo(image2, padTwoLocatorRoiForArea, "SolderArea"));

      //Lim, Lay Ngor - OKI tombstone - Save Diagnostic Slice Image
      if (ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(padOneJointInspectionData.getSubtype()))
      {
        Image combineImage =  null;
        combineImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, combineImage, image1, padOneLocatorRoiForArea, sliceNameEnum);
        combineImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImages, combineImage, image2, padTwoLocatorRoiForArea, sliceNameEnum);    
        AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImages, combineImage, sliceNameEnum);
        combineImage.decrementReferenceCount();
      }
    }

    image1.decrementReferenceCount();
    image2.decrementReferenceCount();    
  }
  
  /**
   * @author Lim, Lay Ngor
   */
  private float calculateSolderAreaPercentage(Image image, RegionOfInterest padLocatorRoiForArea, float weightage)
  {
    Assert.expect(image != null);
    Assert.expect(padLocatorRoiForArea != null);
    
    int padOneTotalSize = (padLocatorRoiForArea.getWidth() * padLocatorRoiForArea.getHeight());
    padOneTotalSize = (int)(padOneTotalSize * weightage);
    Assert.expect(padOneTotalSize > 0);
    final int padOneSolderArea = areaDetection(image, RegionOfInterest.createRegionFromImage(image), true);
    return (100.f * padOneSolderArea / padOneTotalSize);
  }
  
  /**
   * @author Peter Esbensen
   */
  private int findBestOpenSignalIndex(ClearChipProfileInfo clearChipProfileInfo,
                                      int lowerFilletOffsetRelativeToUpperFilletInPixels,
                                      int centerOfSearchIndex,
                                      int searchWidth,
                                      boolean forPadOne)
  {
    Assert.expect(clearChipProfileInfo != null);
    Assert.expect(centerOfSearchIndex >= 0);
    float[] componentThicknessProfile = clearChipProfileInfo.getComponentBodyThicknessProfile();
    Assert.expect(centerOfSearchIndex < componentThicknessProfile.length);
    Assert.expect(searchWidth >= 0);

    int startIndex = Math.max(0, centerOfSearchIndex - searchWidth / 2);
    int endIndex = Math.min(componentThicknessProfile.length - 1, centerOfSearchIndex + searchWidth / 2);

    if (forPadOne)
    {
      clearChipProfileInfo.setPadOneLeftSideOpenSignalSearchIndex(startIndex);
      clearChipProfileInfo.setPadOneRightSideOpenSignalSearchIndex(endIndex);
    }
    else
    {
      clearChipProfileInfo.setPadTwoLeftSideOpenSignalSearchIndex(startIndex);
      clearChipProfileInfo.setPadTwoRightSideOpenSignalSearchIndex(endIndex);
    }

    float bestOpenSignal = Float.NEGATIVE_INFINITY;
    int bestOpenSignalIndex = startIndex;
    for (int i = startIndex; i < endIndex; ++i)
    {
      if (((i + lowerFilletOffsetRelativeToUpperFilletInPixels) >= componentThicknessProfile.length ) ||
          ((i + lowerFilletOffsetRelativeToUpperFilletInPixels) < 0))
        break; // don't search if we run out of room on the profile.
      float openSignal = componentThicknessProfile[i] - componentThicknessProfile[ i + lowerFilletOffsetRelativeToUpperFilletInPixels ];
      if (openSignal > bestOpenSignal)
      {
        bestOpenSignal = openSignal;
        bestOpenSignalIndex = i;
      }
    }
    return bestOpenSignalIndex;
  }

 /**
   * Clear Tombstone
   * @author Lim, Lay Ngor
   */
  int locateHeelPeakGullwingStyle(float[] padThicknessProfileUsingFullPadLengthAcross, 
    JointInspectionData jointInspectionData, boolean isPadOne,
    IntegerRef startUpperFilletEdge, IntegerRef endUpperFilletEdge)
  {
    Assert.expect(padThicknessProfileUsingFullPadLengthAcross != null);
    Assert.expect(jointInspectionData != null);

    Subtype subtype = jointInspectionData.getSubtype();
    // Locate the heel edge and peak.
    final float HEEL_EDGE_THICKNESS_FRACTION_OF_MAX  //= (Float) 60.f / 100.f;
      = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_EDGE_SEARCH_THICKNESS_PERCENT) / 100.f;
    
    // Get the located joint ROI.
    RegionOfInterest locatedJointRoi = Locator.getRegionOfInterestAtMeasuredLocation(jointInspectionData);
    int locatedJointRoiLengthAlong = locatedJointRoi.getLengthAlong();
    final float HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG //= (Float)80.f / 100.f; //15.f too small
      = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_SEARCH_DISTANCE_FROM_EDGE) / 100.f;
    
    int heelPeakSearchDistanceInPixels = (int) Math.ceil((float) locatedJointRoiLengthAlong
      * HEEL_PEAK_SEARCH_DISTANCE_FRACTION_OF_PAD_LENGTH_ALONG);
    
    //pad one need reverse search for chip
    float heelEdgeSubpixelBin = AlgorithmFeatureUtil.locateHeelEdgeUsingThickness(padThicknessProfileUsingFullPadLengthAcross,
      (float) HEEL_EDGE_THICKNESS_FRACTION_OF_MAX, isPadOne);
    
    float heelPeakSearchEndBin;    
    if(isPadOne)
    {
      heelPeakSearchEndBin = heelEdgeSubpixelBin;
      heelEdgeSubpixelBin = Math.min((int) heelPeakSearchEndBin - heelPeakSearchDistanceInPixels + 1,
        padThicknessProfileUsingFullPadLengthAcross.length - 1);
      heelEdgeSubpixelBin = Math.max(heelEdgeSubpixelBin, 0);
      if(heelPeakSearchEndBin < heelEdgeSubpixelBin) //Search distance zero handling
        heelPeakSearchEndBin = heelEdgeSubpixelBin;
    }
    else
    {
      heelPeakSearchEndBin = Math.min((int) heelEdgeSubpixelBin + heelPeakSearchDistanceInPixels + 1,
        padThicknessProfileUsingFullPadLengthAcross.length - 1);
    }
      
    int heelPeakBin = AlgorithmFeatureUtil.locateHeelPeak(padThicknessProfileUsingFullPadLengthAcross,
      (int) heelEdgeSubpixelBin, heelPeakSearchDistanceInPixels);
    
    startUpperFilletEdge.setValue((int)heelEdgeSubpixelBin);
    endUpperFilletEdge.setValue((int)heelPeakSearchEndBin);
    return heelPeakBin;
}
  
  /**
   * Measure the Upper and Lower fillet thicknesses.  Their locations are controlled by the upperFilletOffsetAsFractionOfPadLength
   * and lowerFilletOffsetAsFractionOfPadLength parameters.  Note that both of these parameters are relative to the
   * fillet edges, which are located at pad1FilletIndex and pad2FilletIndex.  The offsets move the locations inwards
   * towards the body of the component if they are positive and away from the body if they are negative.  If the user
   * has specified a search length, then those fillet locations will be shifted in unison until they find the best
   * open signal within that specified search area.
   *
   * @author Peter Esbensen
   */
  private void getClearChipFilletThicknesses(ClearChipProfileInfo clearChipProfileInfo,
                                             JointInspectionData padOneJointInspectionData,
                                             JointInspectionData padTwoJointInspectionData,
                                             float padOneFilletIndex,
                                             float padTwoFilletIndex,
                                             float upperFilletOffsetAsFractionOfPadLength,
                                             float lowerFilletOffsetFractionOfPadLength)
  {
    Assert.expect(clearChipProfileInfo != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);

    RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    final int padOneLengthInPixels = padOneRoi.getLengthAlong();
    final int padTwoLengthInPixels = padTwoRoi.getLengthAlong();

    float[] componentThicknessProfile = clearChipProfileInfo.getComponentBodyThicknessProfile();
    
    //Lim, Lay Ngor - Clear Tombstone - Gullwing like upper fillet
    final String upperFilletPeakDetectionMethod = (String)padOneJointInspectionData.getSubtype().
                                                        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD);
    
    //Gullwing Thickness method
    if(upperFilletPeakDetectionMethod.equals("Thickness"))
    {
      int middle = componentThicknessProfile.length/2;
      float[] padThicknessProfilePadOne = ArrayUtil.copy(componentThicknessProfile, middle + 1, componentThicknessProfile.length-1); 
      float[] padThicknessProfilePadTwo = ArrayUtil.copy(componentThicknessProfile, 0, middle);  
      
      //Only thickness method need this information
      IntegerRef startUpperFilletEdge = new IntegerRef();
      IntegerRef endUpperFilletEdge = new IntegerRef();
      int padOneUpperFilletIndex2 = middle + 1 + locateHeelPeakGullwingStyle(padThicknessProfilePadOne, padOneJointInspectionData, true,
        startUpperFilletEdge, endUpperFilletEdge);
      clearChipProfileInfo.setPadOneLeftSideUpperFilletSearchIndex(middle + 1 + startUpperFilletEdge.getValue());
      clearChipProfileInfo.setPadOneRightSideUpperFilletSearchIndex(middle + 1 + endUpperFilletEdge.getValue());
    
      int padTwoUpperFilletIndex2 = locateHeelPeakGullwingStyle(padThicknessProfilePadTwo, padTwoJointInspectionData, false, 
        startUpperFilletEdge, endUpperFilletEdge);
      clearChipProfileInfo.setPadTwoLeftSideUpperFilletSearchIndex(startUpperFilletEdge.getValue());
      clearChipProfileInfo.setPadTwoRightSideUpperFilletSearchIndex(endUpperFilletEdge.getValue());
    
      // store the indices and measurements into the Reference objects to return to the calling function
      clearChipProfileInfo.setPadOneUpperFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
                                                                          componentThicknessProfile,
                                                                          padOneUpperFilletIndex2));  
      clearChipProfileInfo.setPadTwoUpperFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadTwo(padTwoJointInspectionData,
                                                                          componentThicknessProfile,
                                                                          padTwoUpperFilletIndex2)); 
      
      int lowerFillerRelativeToUpperFilletInPixels = Math.round((lowerFilletOffsetFractionOfPadLength * padOneLengthInPixels));
      clearChipProfileInfo.setPadOneLowerFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
        componentThicknessProfile,
        clearChipProfileInfo.getPadOneUpperFilletIndex() - lowerFillerRelativeToUpperFilletInPixels));

      lowerFillerRelativeToUpperFilletInPixels = Math.round((lowerFilletOffsetFractionOfPadLength * padTwoLengthInPixels));
      clearChipProfileInfo.setPadTwoLowerFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadTwo(padTwoJointInspectionData,
        componentThicknessProfile,
        clearChipProfileInfo.getPadTwoUpperFilletIndex() + lowerFillerRelativeToUpperFilletInPixels));
    }
    else
    {
      //This is for Best Signal use only
      int upperFilletOffsetAlongInPixels = Math.round(padOneLengthInPixels * upperFilletOffsetAsFractionOfPadLength);
      int lowerFillerRelativeToUpperFilletInPixels = Math.round((lowerFilletOffsetFractionOfPadLength * padOneLengthInPixels) -
                                                                 upperFilletOffsetAlongInPixels);

      // compute open signal at fillet indices, then search around there to see if it improves
      int searchDistanceInPixels = Math.round((Float)(padOneJointInspectionData.getSubtype().getAlgorithmSettingValue(
          AlgorithmSettingEnum.CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)) * 0.01f * padOneLengthInPixels);
      
      final int padOneBestIndex = findBestOpenSignalIndex(clearChipProfileInfo,
        -1 * lowerFillerRelativeToUpperFilletInPixels,
        ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
          componentThicknessProfile,
          Math.round(padOneFilletIndex - upperFilletOffsetAlongInPixels)),
        searchDistanceInPixels, true); // true == for pad one

      //Pad Two calculation
      upperFilletOffsetAlongInPixels = Math.round(padTwoLengthInPixels * upperFilletOffsetAsFractionOfPadLength);
      lowerFillerRelativeToUpperFilletInPixels = Math.round((lowerFilletOffsetFractionOfPadLength * padTwoLengthInPixels) -
                                                             upperFilletOffsetAlongInPixels);

      // compute open signal at fillet indices, then search around there to see if it improves
      searchDistanceInPixels = Math.round((Float) (padTwoJointInspectionData.getSubtype().getAlgorithmSettingValue(
        AlgorithmSettingEnum.CHIP_MEASUREMENT_OPEN_SIGNAL_SEARCH_LENGTH)) * 0.01f * padTwoLengthInPixels);

      int padTwoBestIndex = findBestOpenSignalIndex(clearChipProfileInfo,
        lowerFillerRelativeToUpperFilletInPixels,
        ChipAlgorithmUtil.forceIndexOntoPadTwo(padTwoJointInspectionData,
          componentThicknessProfile,
          Math.round(padTwoFilletIndex + upperFilletOffsetAlongInPixels)),
        searchDistanceInPixels, false); // false == for pad two

      // store the indices and measurements into the Reference objects to return to the calling function
      clearChipProfileInfo.setPadOneUpperFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
        componentThicknessProfile,
        padOneBestIndex));
      clearChipProfileInfo.setPadTwoUpperFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadTwo(padTwoJointInspectionData,
        componentThicknessProfile,
        padTwoBestIndex));

      clearChipProfileInfo.setPadOneLowerFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
        componentThicknessProfile,
        clearChipProfileInfo.getPadOneUpperFilletIndex() - lowerFillerRelativeToUpperFilletInPixels));

      clearChipProfileInfo.setPadTwoLowerFilletIndex(ChipAlgorithmUtil.forceIndexOntoPadTwo(padTwoJointInspectionData,
        componentThicknessProfile,
        clearChipProfileInfo.getPadTwoUpperFilletIndex() + lowerFillerRelativeToUpperFilletInPixels));
    }
   
    //Lim, Lay Ngor - Clear Tombstone
    final int padOneUpperFilletIndex = clearChipProfileInfo.getPadOneUpperFilletIndex();
    clearChipProfileInfo.setFilletOneThicknessInMils(componentThicknessProfile[padOneUpperFilletIndex]);

    //Lim, Lay Ngor - Clear Tombstone
    final int padOneLowerFilletIndex = clearChipProfileInfo.getPadOneLowerFilletIndex();
    clearChipProfileInfo.setFilletOneLowerRegionThicknessInMils(componentThicknessProfile[padOneLowerFilletIndex]);

    //Lim, Lay Ngor - Clear Tombstone
    final int padTwoUpperFilletIndex = clearChipProfileInfo.getPadTwoUpperFilletIndex();
    clearChipProfileInfo.setFilletTwoThicknessInMils(componentThicknessProfile[padTwoUpperFilletIndex]);

    //Lim, Lay Ngor - Clear Tombstone
    final int padTwoLowerFilletIndex = clearChipProfileInfo.getPadTwoLowerFilletIndex();
    clearChipProfileInfo.setFilletTwoLowerRegionThicknessInMils(componentThicknessProfile[padTwoLowerFilletIndex]);

    //Lim, Lay Ngor - Clear Tombstone
    final int padOneUpperToLowerFilletLength = Math.abs(padOneLowerFilletIndex - padOneUpperFilletIndex);
    final float centerToUpperFilletThicknessPercentage = (Float) padOneJointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_CENTER_OFFSET);
//    final float centerToUpperFilletThicknessPercentage = 85.f;
    final int padOneCenterToHeelOffsetInPixel = (int) (padOneUpperToLowerFilletLength * centerToUpperFilletThicknessPercentage / 100.f);
    int padOneCenterBinIndex = padOneUpperFilletIndex + padOneCenterToHeelOffsetInPixel;
    if (padOneUpperFilletIndex > padOneLowerFilletIndex)
    {
      padOneCenterBinIndex = padOneUpperFilletIndex - padOneCenterToHeelOffsetInPixel;
    }
    clearChipProfileInfo.setPadOneCenterBinIndex(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
      componentThicknessProfile, padOneCenterBinIndex));

    // Center thickness is simply the thickness value at the determined center profile bin.
    float padOneCenterThicknessInMillimeters = componentThicknessProfile[padOneCenterBinIndex];
    // Clamp the center thickness to the smallest possible non-zero positive float.
    padOneCenterThicknessInMillimeters = Math.max(padOneCenterThicknessInMillimeters, Float.MIN_VALUE);
    clearChipProfileInfo.setPadOneCenterThicknessInMils(padOneCenterThicknessInMillimeters);
    // Compute the center:upper fillet ratio.
    float padOneCenterThicknessPercentOfUpperFilletThickness = 100.f;
    if (MathUtil.fuzzyGreaterThan(clearChipProfileInfo.getFilletOneThicknessInMils(), 0f))
    {
      padOneCenterThicknessPercentOfUpperFilletThickness = (padOneCenterThicknessInMillimeters / clearChipProfileInfo.getFilletOneThicknessInMils()) * 100.f;
    }
    clearChipProfileInfo.setPadOneCenterThicknessPercentOfUpperFilletThickness(padOneCenterThicknessPercentOfUpperFilletThickness);

    final int padTwoUpperToLowerFilletLength = Math.abs(padTwoLowerFilletIndex - padTwoUpperFilletIndex);
    final int padTwoCenterToHeelOffsetInPixel = (int) (padTwoUpperToLowerFilletLength * centerToUpperFilletThicknessPercentage / 100.f);
    int padTwoCenterBinIndex = padTwoUpperFilletIndex + padTwoCenterToHeelOffsetInPixel;
    if (padTwoUpperFilletIndex > padTwoLowerFilletIndex)
    {
      padTwoCenterBinIndex = padTwoUpperFilletIndex - padTwoCenterToHeelOffsetInPixel;
    }
    clearChipProfileInfo.setPadTwoCenterBinIndex(ChipAlgorithmUtil.forceIndexOntoPadTwo(padTwoJointInspectionData,
      componentThicknessProfile, padTwoCenterBinIndex));

    // Center thickness is simply the thickness value at the determined center profile bin.
    float padTwoCenterThicknessInMillimeters = componentThicknessProfile[padTwoCenterBinIndex];
    // Clamp the center thickness to the smallest possible non-zero positive float.
    padTwoCenterThicknessInMillimeters = Math.max(padTwoCenterThicknessInMillimeters, Float.MIN_VALUE);
    clearChipProfileInfo.setPadTwoCenterThicknessInMils(padTwoCenterThicknessInMillimeters);
    // Compute the center:upper fillet ratio.
    float padTwoCenterThicknessPercentOfUpperFilletThickness = 100.f;
    if (MathUtil.fuzzyGreaterThan(clearChipProfileInfo.getFilletTwoThicknessInMils(), 0f))
    {
      padTwoCenterThicknessPercentOfUpperFilletThickness = (padTwoCenterThicknessInMillimeters / clearChipProfileInfo.getFilletTwoThicknessInMils()) * 100.f;
    }
    clearChipProfileInfo.setPadTwoCenterThicknessPercentOfUpperFilletThickness(padTwoCenterThicknessPercentOfUpperFilletThickness);
  }

  /**
   * @author Peter Esbensen
   */
  private void storeAndPostClearChipMeasurements(ClearChipProfileInfo clearChipProfileInfo,
                                                 JointInspectionData padOneJointInspectionData,
                                                 JointInspectionData padTwoJointInspectionData,
                                                 ReconstructionRegion reconstructionRegion,
                                                 SliceNameEnum sliceNameEnum)
  {
    Assert.expect(clearChipProfileInfo != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();
    JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();
    Pad padOne = padOneJointInspectionData.getPad();
    Pad padTwo = padTwoJointInspectionData.getPad();

    AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_MEASURING_PIN_ONE_KEY", null),
        reconstructionRegion, sliceNameEnum, padOneJointInspectionData, this);
    // store pad one fillet thickness
    JointMeasurement padOneFilletThicknessMeasurement = new JointMeasurement(this,
                                                                             MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS,
                                                                             MeasurementUnitsEnum.MILLIMETERS,
                                                                             padOne,
                                                                             sliceNameEnum,
                                                                             MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletOneThicknessInMils()));
    padOneJointInspectionResult.addMeasurement(padOneFilletThicknessMeasurement);

    // store pad one lower fillet thickness
    JointMeasurement padOneLowerFilletThicknessMeasurement = new JointMeasurement(this,
                                                                                  MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS,
                                                                                  MeasurementUnitsEnum.MILLIMETERS,
                                                                                  padOne,
                                                                                  sliceNameEnum,
                                                                                  MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletOneLowerRegionThicknessInMils()));
    padOneJointInspectionResult.addMeasurement(padOneLowerFilletThicknessMeasurement);

    //Lim, Lay Ngor - Clear Tombstone - START
    // store pad one center bin thickness
    JointMeasurement padOneCenterThicknessMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_CENTER_FILLET_THICKNESS,
      MeasurementUnitsEnum.MILLIMETERS,
      padOne,
      sliceNameEnum,
      MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getPadOneCenterThicknessInMils()));
    padOneJointInspectionResult.addMeasurement(padOneCenterThicknessMeasurement);
    
    // Save the pad one center:upper fillet ratio measurement.
    JointMeasurement padOneCenterToHeelThicknessPercentMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT,
        MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
        padOne,
        sliceNameEnum,
        clearChipProfileInfo.getPadOneCenterThicknessPercentOfUpperFilletThickness());
    padOneJointInspectionResult.addMeasurement(padOneCenterToHeelThicknessPercentMeasurement);    

    String chipSecondLevelDetectionMethod = 
      (String)padOneJointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD);      
    if(chipSecondLevelDetectionMethod.equalsIgnoreCase("solder area"))
    {    
      JointMeasurement padOneSolderAreaPercentageMeasurement = new JointMeasurement(this,
          MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          padOne,
          sliceNameEnum,
          clearChipProfileInfo.getPadOneSolderAreaPercentage());
      padOneJointInspectionResult.addMeasurement(padOneSolderAreaPercentageMeasurement);
    }
    //Lim, Lay Ngor - Clear Tombstone - END
    
    AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_MEASURING_PIN_TWO_KEY", null),
                                         reconstructionRegion, sliceNameEnum, padOneJointInspectionData, this);
    // store pad two fillet thickness
    JointMeasurement padTwoFilletThicknessMeasurement = new JointMeasurement(this,
                                                                             MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS,
                                                                             MeasurementUnitsEnum.MILLIMETERS,
                                                                             padTwo,
                                                                             sliceNameEnum,
                                                                             MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletTwoThicknessInMils()));
    padTwoJointInspectionResult.addMeasurement(padTwoFilletThicknessMeasurement);


    // store pad two lower fillet thickness
    JointMeasurement padTwoLowerFilletThicknessMeasurement = new JointMeasurement(this,
                                                                                  MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS,
                                                                                  MeasurementUnitsEnum.MILLIMETERS,
                                                                                  padTwo,
                                                                                  sliceNameEnum,
                                                                                  MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletTwoLowerRegionThicknessInMils()));
    padTwoJointInspectionResult.addMeasurement(padTwoLowerFilletThicknessMeasurement);

    //Lim, Lay Ngor - Clear Tombstone - START
    // store pad two center bin thickness
    JointMeasurement padTwoCenterThicknessMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_CENTER_FILLET_THICKNESS,//GULLWING_CENTER_THICKNESS,
      MeasurementUnitsEnum.MILLIMETERS,
      padTwo,
      sliceNameEnum,
      MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getPadTwoCenterThicknessInMils()));
    padTwoJointInspectionResult.addMeasurement(padTwoCenterThicknessMeasurement);

    // Save the pad two center:upper fillet ratio measurement.
    JointMeasurement centerToHeelThicknessPercentMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT,
        MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
        padTwo,
        sliceNameEnum,
        clearChipProfileInfo.getPadTwoCenterThicknessPercentOfUpperFilletThickness());
    padTwoJointInspectionResult.addMeasurement(centerToHeelThicknessPercentMeasurement);
    
    if(chipSecondLevelDetectionMethod.equalsIgnoreCase("solder area"))
    {    
      JointMeasurement padTwoSolderAreaPercentageMeasurement = new JointMeasurement(this,
          MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE,
          MeasurementUnitsEnum.PERCENT_OF_AREA_VOIDED,
          padTwo,
          sliceNameEnum,
          clearChipProfileInfo.getPadTwoSolderAreaPercentage());
      padTwoJointInspectionResult.addMeasurement(padTwoSolderAreaPercentageMeasurement);
    }
    //Lim, Lay Ngor - Clear Tombstone - END
    
    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructionRegion,
                                                    padOneJointInspectionData,
                                                    padOneFilletThicknessMeasurement,
                                                    padOneLowerFilletThicknessMeasurement,
                                                    padOneCenterThicknessMeasurement,//Clear Tombstone
                                                    padTwoFilletThicknessMeasurement,
                                                    padTwoLowerFilletThicknessMeasurement,
                                                    padTwoCenterThicknessMeasurement,//Clear Tombstone
                                                    centerToHeelThicknessPercentMeasurement);//Clear Tombstone
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      // store pad component length - Component level
      ComponentInspectionData componentInspectionData = padOneJointInspectionData.getComponentInspectionData();
      ComponentMeasurement componentLengthInMillimetersMeasurement = new ComponentMeasurement(this, padOneJointInspectionData.getSubtype(),
                                                                                              MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH,
                                                                                              MeasurementUnitsEnum.MILLIMETERS,
                                                                                              componentInspectionData.getComponent(),
                                                                                              sliceNameEnum,
                                                                                              clearChipProfileInfo.getComponentBodyLengthInMillimeters());
      padOneJointInspectionData.getComponentInspectionData().getComponentInspectionResult().addMeasurement(componentLengthInMillimetersMeasurement);    
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      componentInspectionData,
                                                      componentLengthInMillimetersMeasurement);
    }
    else
    {
      // store pad component length - Component level      
      JointMeasurement jointComponentLengthInMillimetersMeasurement = new JointMeasurement(this,
                                                                                           MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH,
                                                                                           MeasurementUnitsEnum.MILLIMETERS,
                                                                                           padOneJointInspectionData.getPad(),
                                                                                           sliceNameEnum,
                                                                                           clearChipProfileInfo.getComponentBodyLengthInMillimeters());
      padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointComponentLengthInMillimetersMeasurement);    
      AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      jointComponentLengthInMillimetersMeasurement);
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void postOpaqueChipProfileDiagnostics(float[] componentBodyThicknessProfile,
                                                int filletOneEdgeIndex,
                                                int filletTwoEdgeIndex,
                                                int padTwoLeftSideIndex,
                                                int padTwoRightSideIndex,
                                                int padOneLeftSideIndex,
                                                int padOneRightSideIndex,
                                                ReconstructionRegion reconstructionRegion,
                                                SliceNameEnum sliceNameEnum,
                                                JointInspectionData padOneJointInspectionData,
                                                final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(filletTwoEdgeIndex >= 0);
    Assert.expect(padTwoRightSideIndex >= filletTwoEdgeIndex);
    Assert.expect(padOneLeftSideIndex >= padTwoRightSideIndex);

    if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getJointTypeEnum(), this))
    {
      int lengthOfProfile = componentBodyThicknessProfile.length;

      float percentageOfPad = (Float)padOneJointInspectionData.getSubtype().
                                                        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS);
      int halfPadLengthInNanos = padOneJointInspectionData.getPad().getLengthInNanoMeters() / 2;
      int halfPadLengthInPixels = (int)(MathUtil.convertNanometersToMillimeters(halfPadLengthInNanos) / MILIMETER_PER_PIXEL);
      int partialPadLengthInPixels = (int)( halfPadLengthInPixels * 2 * (percentageOfPad/100.0) );
      String edgeLocationTechniqueOpaque = (String)padOneJointInspectionData.getSubtype().
                                                        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE);
      int calculatedPadOneFilletThicknessLeftEdgeIndex;
      int calculatedPadOneFilletThicknessRightEdgeIndex;
      int calculatedPadTwoFilletThicknessLeftEdgeIndex;
      int calculatedPadTwoFilletThicknessRightEdgeIndex;
      if (edgeLocationTechniqueOpaque.equals("Max Slope"))
      {
        calculatedPadOneFilletThicknessLeftEdgeIndex = Math.max(0, filletOneEdgeIndex - halfPadLengthInPixels)+1;
        calculatedPadOneFilletThicknessRightEdgeIndex = Math.min(lengthOfProfile - 1, filletOneEdgeIndex + halfPadLengthInPixels);
        calculatedPadTwoFilletThicknessLeftEdgeIndex = Math.max(0, filletTwoEdgeIndex - halfPadLengthInPixels);
        calculatedPadTwoFilletThicknessRightEdgeIndex = Math.min(lengthOfProfile - 1, filletTwoEdgeIndex + halfPadLengthInPixels) - 1;
      }
      else
      {
        calculatedPadOneFilletThicknessLeftEdgeIndex = Math.max(0, filletOneEdgeIndex)+1;
        calculatedPadOneFilletThicknessRightEdgeIndex = Math.min(lengthOfProfile - 1, filletOneEdgeIndex + partialPadLengthInPixels);
        calculatedPadTwoFilletThicknessLeftEdgeIndex = Math.max(0, filletTwoEdgeIndex - partialPadLengthInPixels);
        calculatedPadTwoFilletThicknessRightEdgeIndex = Math.min(lengthOfProfile - 1, filletTwoEdgeIndex) - 1;
      }

      MeasurementRegionEnum[] measurementRegionEnums = new MeasurementRegionEnum[lengthOfProfile];

      // first set everything to background
      Arrays.fill(measurementRegionEnums, MeasurementRegionEnum.BACKGROUND_REGION);

      // now fill in the pad areas
      Arrays.fill(measurementRegionEnums, padTwoLeftSideIndex, padTwoRightSideIndex, MeasurementRegionEnum.PAD_REGION);
      Arrays.fill(measurementRegionEnums, padOneLeftSideIndex, padOneRightSideIndex,
                  MeasurementRegionEnum.PAD_REGION);

      // highlight the pad one fillet edge
      int filletOneLeftSide = Math.max(0, filletOneEdgeIndex - 1);
      int filletOneRightSide = Math.min(lengthOfProfile -(int) 1, filletOneEdgeIndex + 1);
      Arrays.fill(measurementRegionEnums, filletOneLeftSide, filletOneRightSide, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

      // highlight the pad two fillet edge
      int filletTwoLeftSide = Math.max(0, filletTwoEdgeIndex - 1);
      int filletTwoRightSide = Math.min(lengthOfProfile - 1, filletTwoEdgeIndex + 1);
      Arrays.fill(measurementRegionEnums, filletTwoLeftSide, filletTwoRightSide, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

      // use the nominal pad thickness to determine how to scale the profile
      Subtype subtype = padOneJointInspectionData.getSubtype();
      float opaqueNominalFilletThicknessInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS);
      float suggestedMaxVerticalScaleInMils = MathUtil.convertMillimetersToMils(opaqueNominalFilletThicknessInMillimeters * _OPAQUE_CHIP_PROFILE_SCALE_FACTOR);

      LocalizedString padThicknessProfileLabelLocalizedString =
          new LocalizedString("ALGIAG_CHIP_MEASUREMENT_OPAQUE_CHIP_EDGE_LOCATION_PROFILE_KEY", null);
      ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padThicknessProfileLabelLocalizedString,
                                                                           0.0f,
                                                                           suggestedMaxVerticalScaleInMils,
                                                                           componentBodyThicknessProfile,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.MILS);
      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   padOneJointInspectionData.getSubtype(),
                                   this, false, true,
                                   labeledPadThicknessProfileDiagInfo);

      // highlight the pad one measured fillet thickness region
      int filletThicknessOneLeftSide = Math.max(0, calculatedPadOneFilletThicknessLeftEdgeIndex - 1);
      int filletThicknessOneRightSide = Math.min(lengthOfProfile - 1, calculatedPadOneFilletThicknessRightEdgeIndex + 1);
      Arrays.fill(measurementRegionEnums, filletThicknessOneLeftSide, filletThicknessOneRightSide, MeasurementRegionEnum.MEASURED_FILLET_THICKNESS_REGION);

      // highlight the pad two measured fillet thickness region
      int filletThicknessTwoLeftSide = Math.max(0, calculatedPadTwoFilletThicknessLeftEdgeIndex - 1);
      int filletThicknessTwoRightSide = Math.min(lengthOfProfile - 1, calculatedPadTwoFilletThicknessRightEdgeIndex + 1);
      Arrays.fill(measurementRegionEnums, filletThicknessTwoLeftSide, filletThicknessTwoRightSide, MeasurementRegionEnum.MEASURED_FILLET_THICKNESS_REGION);

      // highlight the pad one fillet edge again
      Arrays.fill(measurementRegionEnums, filletOneLeftSide, filletOneRightSide, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

      // highlight the pad two fillet edge again
      Arrays.fill(measurementRegionEnums, filletTwoLeftSide, filletTwoRightSide, MeasurementRegionEnum.OUTER_FILLET_EDGE_REGION);

      ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo1 =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padThicknessProfileLabelLocalizedString,
                                                                           0.0f,
                                                                           suggestedMaxVerticalScaleInMils,
                                                                           componentBodyThicknessProfile,
                                                                           measurementRegionEnums,
                                                                           MeasurementUnitsEnum.MILS);
      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   padOneJointInspectionData.getSubtype(),
                                   this, false, true,
                                   labeledPadThicknessProfileDiagInfo1);

      // START - Added by Seng Yew
      float[] component1stDerivativeProfile = ProfileUtil.createDerivativeProfile(componentBodyThicknessProfile, _PROFILE_SMOOTHING_STEP_SIZE);
      if (edgeLocationTechniqueOpaque.equals("Max Slope"))
      {
        LocalizedString firstDerivativesProfileLabelLocalizedString =
            new LocalizedString("FIRST_DERIVATIVES_PROFILE_REGION_KEY", null);
        ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo2 =
            ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getFullyQualifiedPadName(),
                                                                             ProfileTypeEnum.SOLDER_PROFILE,
                                                                             firstDerivativesProfileLabelLocalizedString,
                                                                             ArrayUtil.min(component1stDerivativeProfile)*1.1f,
                                                                             ArrayUtil.max(component1stDerivativeProfile)*1.1f,
                                                                             component1stDerivativeProfile,
                                                                             measurementRegionEnums,
                                                                             MeasurementUnitsEnum.MILS);
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     padOneJointInspectionData.getSubtype(),
                                     this, false, false,
                                     labeledPadThicknessProfileDiagInfo2);
      }
      else
      {
        float[] component2ndDerivativeProfile = ProfileUtil.createDerivativeProfile(component1stDerivativeProfile, _PROFILE_SMOOTHING_STEP_SIZE);
        LocalizedString secondDerivativesProfileLabelLocalizedString =
            new LocalizedString("SECOND_DERIVATIVES_PROFILE_REGION_KEY", null);
        ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo3 =
            ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getFullyQualifiedPadName(),
                                                                             ProfileTypeEnum.SOLDER_PROFILE,
                                                                             secondDerivativesProfileLabelLocalizedString,
                                                                             ArrayUtil.min(component2ndDerivativeProfile)*1.1f,
                                                                             ArrayUtil.max(component2ndDerivativeProfile)*1.1f,
                                                                             component2ndDerivativeProfile,
                                                                             measurementRegionEnums,
                                                                             MeasurementUnitsEnum.MILS);
        _diagnostics.postDiagnostics(reconstructionRegion,
                                     sliceNameEnum,
                                     padOneJointInspectionData.getSubtype(),
                                     this, false, true,
                                     labeledPadThicknessProfileDiagInfo3);
      }
      // END - Added by Seng Yew
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void postClearChipFilletProfileDiagnostics(ClearChipProfileInfo clearChipProfileInfo,
                                                     ReconstructionRegion reconstructionRegion,
                                                     SliceNameEnum sliceNameEnum,
                                                     JointInspectionData padOneJointInspectionData)
  {
    Assert.expect(clearChipProfileInfo != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(padOneJointInspectionData != null);

    int padTwoUpperFilletIndex = clearChipProfileInfo.getPadTwoUpperFilletIndex();
    int padTwoRightSideIndex = clearChipProfileInfo.getPadTwoRightSideIndex();
    int padOneLeftSideIndex = clearChipProfileInfo.getPadOneLeftSideIndex();

    Assert.expect(padTwoUpperFilletIndex >= 0);
    Assert.expect(padTwoRightSideIndex >= padTwoUpperFilletIndex);
    Assert.expect(padOneLeftSideIndex >= padTwoRightSideIndex);

    if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getJointTypeEnum(), this))
    {
      float[] componentBodyThicknessProfile = clearChipProfileInfo.getComponentBodyThicknessProfile();
      int lengthOfProfile = componentBodyThicknessProfile.length;

      RegionOfInterest componentRegion = clearChipProfileInfo.getComponentOrthogonalRegionOfInterest();
      MeasurementRegionEnum[] backgroundMeasurementRegionEnums = new MeasurementRegionEnum[lengthOfProfile];

      // first set everything to background
      Arrays.fill(backgroundMeasurementRegionEnums, MeasurementRegionEnum.BACKGROUND_REGION);

      // now fill in the pad areas
      Arrays.fill(backgroundMeasurementRegionEnums, clearChipProfileInfo.getPadTwoLeftSideIndex(), padTwoRightSideIndex, MeasurementRegionEnum.PAD_REGION);
      Arrays.fill(backgroundMeasurementRegionEnums, padOneLeftSideIndex, clearChipProfileInfo.getPadOneRightSideIndex(),
                  MeasurementRegionEnum.PAD_REGION);


      //Lim, Lay Ngor - Clear Tombstone - Gullwing like upper fillet
      // display the heel peak search regions
      String upperFilletPeakDetectionMethod = (String) padOneJointInspectionData.getSubtype().
        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_DETECTION_METHOD);

      if (upperFilletPeakDetectionMethod.equals("Thickness"))
      {
        Arrays.fill(backgroundMeasurementRegionEnums, clearChipProfileInfo.getPadOneLeftSideUpperFilletSearchIndex(),
          clearChipProfileInfo.getPadOneRightSideUpperFilletSearchIndex(), MeasurementRegionEnum.GULLWING_HEEL_SEARCH_REGION);
        Arrays.fill(backgroundMeasurementRegionEnums, clearChipProfileInfo.getPadTwoLeftSideUpperFilletSearchIndex(),
          clearChipProfileInfo.getPadTwoRightSideUpperFilletSearchIndex(), MeasurementRegionEnum.GULLWING_HEEL_SEARCH_REGION);
      }
      else
      {
        //Only if it is "Best Signal" case!
        // display the search regions
        Arrays.fill(backgroundMeasurementRegionEnums, clearChipProfileInfo.getPadOneLeftSideOpenSignalSearchIndex(),
                    clearChipProfileInfo.getPadOneRightSideOpenSignalSearchIndex(), MeasurementRegionEnum.OPEN_SIGNAL_SEARCH_REGION);
        Arrays.fill(backgroundMeasurementRegionEnums, clearChipProfileInfo.getPadTwoLeftSideOpenSignalSearchIndex(),
                    clearChipProfileInfo.getPadTwoRightSideOpenSignalSearchIndex(), MeasurementRegionEnum.OPEN_SIGNAL_SEARCH_REGION);
      }
      //End Clear Tombstone
    
      MeasurementRegionEnumArrayManager arrayManager = new MeasurementRegionEnumArrayManager();
      arrayManager.setBackgroundArray(backgroundMeasurementRegionEnums);

      // highlight the pad one upper fillet region
      int padOneUpperFilletIndex = clearChipProfileInfo.getPadOneUpperFilletIndex();
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneUpperFilletIndex,
                                                                      MeasurementRegionEnum.UPPER_FILLET_REGION);
      RegionOfInterest padOneUpperFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padOneUpperFilletIndex);
      MeasurementRegionDiagnosticInfo padOneUpperFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneUpperFilletRegion,
                                                                                                                  MeasurementRegionEnum.UPPER_FILLET_REGION);

      // highlight the pad one lower fillet region
      int padOneLowerFilletIndex = clearChipProfileInfo.getPadOneLowerFilletIndex();
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneLowerFilletIndex,
                                                                      MeasurementRegionEnum.LOWER_FILLET_REGION);
      RegionOfInterest padOneLowerFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padOneLowerFilletIndex);
      MeasurementRegionDiagnosticInfo padOneLowerFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneLowerFilletRegion,
                                                                                                                  MeasurementRegionEnum.LOWER_FILLET_REGION);


      // highlight the pad two upper fillet region
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoUpperFilletIndex,
                                                                      MeasurementRegionEnum.UPPER_FILLET_REGION);
      RegionOfInterest padTwoUpperFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padTwoUpperFilletIndex);
      MeasurementRegionDiagnosticInfo padTwoUpperFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoUpperFilletRegion,
                                                                                                                  MeasurementRegionEnum.UPPER_FILLET_REGION);

      // highlight the pad two lower fillet region
      int padTwoLowerFilletIndex = clearChipProfileInfo.getPadTwoLowerFilletIndex();
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoLowerFilletIndex,
                                                                      MeasurementRegionEnum.LOWER_FILLET_REGION);
      RegionOfInterest padTwoLowerFilletRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padTwoLowerFilletIndex);
      MeasurementRegionDiagnosticInfo padTwoLowerFilletRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoLowerFilletRegion,
                                                                                                                  MeasurementRegionEnum.LOWER_FILLET_REGION);

      //Lim, Lay Ngor - Clear Tombstone
      // highlight the pad one center bin region
      final int padOneCenterBinIndex = clearChipProfileInfo.getPadOneCenterBinIndex();
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneCenterBinIndex,
        MeasurementRegionEnum.GULLWING_CENTER_REGION);
      RegionOfInterest padOneCenterRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padOneCenterBinIndex);
      MeasurementRegionDiagnosticInfo padOneCenterRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneCenterRegion,
        MeasurementRegionEnum.GULLWING_CENTER_REGION);

      // highlight the pad Two center bin region      
      final int padTwoCenterBinIndex = clearChipProfileInfo.getPadTwoCenterBinIndex();
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoCenterBinIndex,
        MeasurementRegionEnum.GULLWING_CENTER_REGION);
      RegionOfInterest padTwoCenterRegion = AlgorithmUtil.createRegionOfInterestToHighlightProfileFeature(componentRegion, padTwoCenterBinIndex);
      MeasurementRegionDiagnosticInfo padTwoCenterRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoCenterRegion,
        MeasurementRegionEnum.GULLWING_CENTER_REGION);
      //End clear tombstone

      // highlight the pad one fillet edge
      int padOneFilletEdgeIndex = clearChipProfileInfo.getPadOneFilletEdgeIndex();
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padOneFilletEdgeIndex,
                                                                   MeasurementRegionEnum.INNER_FILLET_EDGE_REGION);


      // highlight the pad two fillet edge
      int padTwoFilletEdgeIndex = clearChipProfileInfo.getPadTwoFilletEdgeIndex();
      arrayManager.addMeasurementRegionEnumWithoutHidingExistingEnums(padTwoFilletEdgeIndex,
                                                                   MeasurementRegionEnum.INNER_FILLET_EDGE_REGION);


      // use the nominal pad thickness to determine how to scale the profile
      Subtype subtype = padOneJointInspectionData.getSubtype();
      float clearNominalFilletThicknessInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);
      float suggestedMaxVerticalScaleInMils = MathUtil.convertMillimetersToMils(clearNominalFilletThicknessInMillimeters * _CLEAR_CHIP_PROFILE_SCALE_FACTOR);


      // the componentRegion is longer than the actual component.  Shrink it down to the located edges
      RegionOfInterest modifiedComponentRegion = AlgorithmUtil.getComponentRegionOfInterestBasedOnFilletEdges(componentRegion,
                                                                                                              padOneFilletEdgeIndex,
                                                                                                              padTwoFilletEdgeIndex);
      MeasurementRegionDiagnosticInfo componentRegionDiagnosticInfo = new MeasurementRegionDiagnosticInfo(modifiedComponentRegion,
                                                                                                          MeasurementRegionEnum.INNER_FILLET_EDGE_REGION);
      componentRegionDiagnosticInfo.setLabel(padOneJointInspectionData.getComponent().getReferenceDesignator());

      // create the actual profile diagnostic
      LocalizedString padThicknessProfileLabelLocalizedString =
          new LocalizedString("ALGIAG_CHIP_MEASUREMENT_CLEAR_CHIP_FILLET_REGIONS_PROFILE_KEY", null);
      ProfileDiagnosticInfo labeledPadThicknessProfileDiagInfo =
          ProfileDiagnosticInfo.createSingleDimensionProfileDiagnosticInfo(padOneJointInspectionData.getFullyQualifiedPadName(),
                                                                           ProfileTypeEnum.SOLDER_PROFILE,
                                                                           padThicknessProfileLabelLocalizedString,
                                                                           0.0f,
                                                                           suggestedMaxVerticalScaleInMils,
                                                                           componentBodyThicknessProfile,
                                                                           arrayManager.getFinalArray(),
                                                                           MeasurementUnitsEnum.MILS);

      // post it all
      _diagnostics.postDiagnostics(reconstructionRegion,
                                   sliceNameEnum,
                                   padOneJointInspectionData.getSubtype(),
                                   this, false, true,
                                   labeledPadThicknessProfileDiagInfo,
                                   componentRegionDiagnosticInfo,
                                   padOneUpperFilletRegionDiagnosticInfo,
                                   padOneLowerFilletRegionDiagnosticInfo,
                                   padOneCenterRegionDiagnosticInfo,//Clear Tombstone
                                   padTwoUpperFilletRegionDiagnosticInfo,
                                   padTwoLowerFilletRegionDiagnosticInfo,
                                   padTwoCenterRegionDiagnosticInfo);//Clear Tombstone
    }
  }

  /**
   * @author Peter Esbensen
   */
  private void measureFilletEdgeLocations(float[] componentDerivativeProfile,
                                          float[] componentBodyThicknessProfile,
                                          int nominalComponentBodyLengthInPixels,
                                          JointInspectionData padOneJointInspectionData,
                                          JointInspectionData padTwoJointInspectionData,
                                          boolean testAsClear,
                                          FloatRef padOneFilletEdgeIndex,
                                          FloatRef padTwoFilletEdgeIndex,
                                          final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(componentDerivativeProfile != null);
    Assert.expect(componentBodyThicknessProfile != null);
    Assert.expect(padOneFilletEdgeIndex != null);
    Assert.expect(padTwoFilletEdgeIndex != null);
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(nominalComponentBodyLengthInPixels >= 0);

    if (testAsClear == false)
    {
      locateOpaqueChipEdges (componentDerivativeProfile,
                            padOneJointInspectionData,
                            padTwoJointInspectionData,
                            padOneFilletEdgeIndex,
                            padTwoFilletEdgeIndex);
    }
    else // test as clear
    {
      locateClearChipEdges(padOneJointInspectionData, padTwoJointInspectionData,
                           componentBodyThicknessProfile, componentDerivativeProfile,
                           padOneFilletEdgeIndex, padTwoFilletEdgeIndex,
                           nominalComponentBodyLengthInPixels,
                           MILIMETER_PER_PIXEL);
    }
  }

  /**
   * Reflection is a location technique that I've brought over from the 5dx Resistor algorithm.  Basically, you are
   * trying to find the position along where pad one and pad two are most similar (after reflecting -- or reversing --
   * one of them).
   *
   * This technique tends to get bad results where one fillet is good and the other is missing solder because the
   * insufficient pad does not give you a good signal to lock on to.  To help with this, I have added an additional factor
   * to amplify the smaller joint to have the same volume as the bigger joint.  This seems to result in a more desirable
   * location.
   *
   * @author Peter Esbensen
   */
  private void locateClearChipEdgesUsingReflection(JointInspectionData padOneJointInspectionData,
                                                   JointInspectionData padTwoJointInspectionData,
                                                   float[] componentBodyThicknessProfile,
                                                   float[] componentDerivativeProfile,
                                                   FloatRef padOneFilletEdgeIndex,
                                                   FloatRef padTwoFilletEdgeIndex,
                                                   float nominalBodyLengthInPixels,
                                                   final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentBodyThicknessProfile != null);
    Assert.expect(padOneFilletEdgeIndex != null);
    Assert.expect(padTwoFilletEdgeIndex != null);

    float minimumSearchDiameterInMM = MathUtil.convertMilsToMillimeters(30);
    int searchDiameterInPixels = (int)Math.min(padOneJointInspectionData.getInterPadDistanceInPixels(),
                                          (minimumSearchDiameterInMM / MILIMETER_PER_PIXEL));
    // we can't search outside of the profile, so shrink the search distance if necessary
    int profileLength = componentBodyThicknessProfile.length;

    // Fixed CR31701 - NBS: Assert: ArrayIndexOutOfBoundsException: 164
    // Previously not considering (nominalBodyLengthInPixels + (2 * searchDiameterInPixels)) == profileLength
    // When this happens and searchDiameterInPixels is even number, the double for nested loop below will have ArrayIndexOutOfBoundsException.
    // by Seng-Yew Lim
    if ((nominalBodyLengthInPixels + (2 * searchDiameterInPixels)) > profileLength - 1)
      searchDiameterInPixels -= ((nominalBodyLengthInPixels + (2 * searchDiameterInPixels)) - (profileLength - 1));

    int searchRadiusInPixels = searchDiameterInPixels / 2;

    int padTwoBestGuessIndex = profileLength / 2 - (int)(nominalBodyLengthInPixels * 0.5f);
    int padOneBestGuessIndex = profileLength / 2 + (int)(nominalBodyLengthInPixels * 0.5f);

    float minResidual = Float.POSITIVE_INFINITY;
    int bestResidualIndexOffset = 0;

    for (int currentSearchLocationOffset = -searchRadiusInPixels; currentSearchLocationOffset <= searchRadiusInPixels; ++currentSearchLocationOffset)
    {
      float residual = 0.0f;
      float sumPadOneRegion = ArrayUtil.sum(componentBodyThicknessProfile,
                                            padOneBestGuessIndex + currentSearchLocationOffset - searchRadiusInPixels,
                                            padOneBestGuessIndex + currentSearchLocationOffset);
      float sumPadTwoRegion = ArrayUtil.sum(componentBodyThicknessProfile,
                                            padTwoBestGuessIndex + currentSearchLocationOffset,
                                            padTwoBestGuessIndex + currentSearchLocationOffset + searchRadiusInPixels);
      float scaleFactorToMakePadOneSameVolumeAsPadTwo = sumPadTwoRegion / sumPadOneRegion;

      for (int i = -searchRadiusInPixels; i < searchRadiusInPixels; ++i)
      {
        int padTwoIndex = padTwoBestGuessIndex + currentSearchLocationOffset + i;
        int padOneIndex = padOneBestGuessIndex + currentSearchLocationOffset - i;

        float slopeResidual = Math.abs(componentDerivativeProfile[padTwoIndex] +
                                       componentDerivativeProfile[padOneIndex]);

        float thicknessResidual = Math.abs(componentBodyThicknessProfile[padTwoIndex] -
                                           scaleFactorToMakePadOneSameVolumeAsPadTwo * componentBodyThicknessProfile[padOneIndex]);
        // it's possible that using the slope residual would help somewhat but I'm going to keep things simple
        //        residual += slopeResidual;  option b
        //        residual += slopeResidual + thicknessResidual;    option c
        residual += thicknessResidual;
      }
      if (residual < minResidual)
      {
        minResidual = residual;
        bestResidualIndexOffset = currentSearchLocationOffset;
      }
    }
    padOneFilletEdgeIndex.setValue(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData, componentBodyThicknessProfile,
                                                        padOneBestGuessIndex + bestResidualIndexOffset));
    padTwoFilletEdgeIndex.setValue(ChipAlgorithmUtil.forceIndexOntoPadTwo(padTwoJointInspectionData, componentBodyThicknessProfile,
                                                        padTwoBestGuessIndex + bestResidualIndexOffset));
  }

  /**
   * @author Peter Esbensen
   */
  private void postOpaqueTestDiags(ReconstructedImages reconstructedImages,
                                   SliceNameEnum sliceNameEnum,
                                   ComponentInspectionData componentInspectionData,
                                   float bodyThicknessRequired,
                                   ComponentMeasurement componentBodyThicknessMeasurement,
                                   LocalizedString testingAsClearOrOpaqueString)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(componentBodyThicknessMeasurement != null);
    Assert.expect(testingAsClearOrOpaqueString != null);

    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    Subtype subtype = padOneJointInspectionData.getSubtype();

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);
    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              padOneJointInspectionData,
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              false);

    LocalizedString checkingWhetherClearOrOpaqueString = new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_CHECKING_FOR_CLEAR_OR_OPAQUE_KEY", new Object[]
                                                                             {
                                                                             bodyThicknessRequired});

    TextualDiagnosticInfo checkingWhetherClearOrOpaqueTextDiagInfo = new TextualDiagnosticInfo(checkingWhetherClearOrOpaqueString);
    _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, subtype, this, false, false, checkingWhetherClearOrOpaqueTextDiagInfo);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructionRegion,
                                                    componentInspectionData,
                                                    componentBodyThicknessMeasurement);

    TextualDiagnosticInfo testingAsClearOrOpaqueTextDiagInfo = new TextualDiagnosticInfo(testingAsClearOrOpaqueString);
    _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, subtype, this, false, false, testingAsClearOrOpaqueTextDiagInfo);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private void postOpaqueTestDiags(ReconstructedImages reconstructedImages,
                                   SliceNameEnum sliceNameEnum,
                                   ComponentInspectionData componentInspectionData,
                                   float bodyThicknessRequired,
                                   JointMeasurement jointComponentBodyThicknessMeasurement,
                                   LocalizedString testingAsClearOrOpaqueString)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentInspectionData != null);
    Assert.expect(jointComponentBodyThicknessMeasurement != null);
    Assert.expect(testingAsClearOrOpaqueString != null);

    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    Subtype subtype = padOneJointInspectionData.getSubtype();

    AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);
    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              padOneJointInspectionData,
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              false);

    LocalizedString checkingWhetherClearOrOpaqueString = new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_CHECKING_FOR_CLEAR_OR_OPAQUE_KEY", new Object[]
                                                                             {
                                                                             bodyThicknessRequired});

    TextualDiagnosticInfo checkingWhetherClearOrOpaqueTextDiagInfo = new TextualDiagnosticInfo(checkingWhetherClearOrOpaqueString);
    _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, subtype, this, false, false, checkingWhetherClearOrOpaqueTextDiagInfo);

    AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                    reconstructionRegion,
                                                    padOneJointInspectionData,
                                                    jointComponentBodyThicknessMeasurement);

    TextualDiagnosticInfo testingAsClearOrOpaqueTextDiagInfo = new TextualDiagnosticInfo(testingAsClearOrOpaqueString);
    _diagnostics.postDiagnostics(reconstructionRegion, sliceNameEnum, subtype, this, false, false, testingAsClearOrOpaqueTextDiagInfo);
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
    for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      if (ChipAlgorithmUtil.landPatternIsTestable(componentInspectionData) == false)
      {
        AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_CHIP_WARNING_BAD_LAND_PATTERN_KEY",
                                                                new String[] { componentInspectionData.getComponent().getReferenceDesignator() }));
      }
      else
      {
        JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
        JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
        Subtype subtype = padOneJointInspectionData.getSubtype();

        final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
        // We start out with the Clear Chip Pad slice, even if we're not sure if we should be testing as opaque . . .
        SliceNameEnum sliceNameEnum;
//        if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//          sliceNameEnum = SliceNameEnum.PAD;
//        else
          sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;

        ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

        // run locator
        boolean postLocatorDiags = true;
        if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR))
          postLocatorDiags = false;  // let's hide the diags until we know whether we're using the clear or opaque slice

        Locator.locateRectangularComponent(reconstructedImages,
                                           sliceNameEnum,
                                           componentInspectionData, this, postLocatorDiags);
        RegionOfInterest componentRegionOfInterest = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData, sliceNameEnum, true);

        //Lim, Lay Ngor - OKI Tombstone.
        // Run Locator for clear slice only on all joint for Resistor case - clear chip heel search and area detection use
        Locator.locateJoints(reconstructedImages, SliceNameEnum.CLEAR_CHIP_PAD, jointInspectionDataObjects, this, false);
        
        // extend profile so that we can be sure to catch all of the fillets
        componentRegionOfInterest.scaleFromCenterAlongAcross(1.0 + _PROFILE_EXTENSION, 1.0);

        // Compute component thickness profile
        // For Caps, we want to suppress this first profile because it is just used for the body thickness measurement,
        // and showing that profile is actually more confusing to the user that just hiding it.  For Resistors, we
        // do want to show it because it is the main profile used for analysis.
        JointTypeEnum jointTypeEnum = padOneJointInspectionData.getJointTypeEnum();
        boolean postDiagnostics = false;
        if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
//            jointTypeEnum.equals(JointTypeEnum.TALL_CAPACITOR))
        {
          // clear out the locator diags
          AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImages, sliceNameEnum, subtype, this);
          // make sure the profile diags are posted
          postDiagnostics = true;
        }

        // does the region fit in the image at all?
        if (ChipAlgorithmUtil.regionFits(reconstructedSlice.getOrthogonalImage(), componentRegionOfInterest,
                                         componentInspectionData) == false)
        {
          AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_NOT_INSPECTING_WARNING_KEY",
                                                                  new String[]
                                                                  {componentInspectionData.getComponent().getReferenceDesignator()}));
          continue;
        }

        float[] componentThicknessProfile = ChipAlgorithmUtil.getComponentThicknessProfile(reconstructedSlice,
                                                                                           reconstructionRegion,
                                                                                           componentRegionOfInterest,
                                                                                           padOneJointInspectionData,
                                                                                           this, postDiagnostics);
        boolean testAsClear = true;

        // if this is a Capacitor, then determine if this should be tested as clear or as opaque
        if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
            // Wei Chin (Tall Cap)
//            || jointTypeEnum.equals(JointTypeEnum.TALL_CAPACITOR) )
        {
          //Lay Ngor - XCR-2027 Exclude Outlier
          float componentBodyThicknessInMillimeters = ChipAlgorithmUtil.measureBodyThicknessInMillimeters(componentThicknessProfile,
                                                                                                          padOneJointInspectionData, padTwoJointInspectionData,
                                                                                                          false);

          ComponentMeasurement componentBodyThicknessMeasurement  = null;
          JointMeasurement jointComponentBodyThicknessMeasurement = null;
          if (Config.isComponentLevelClassificationEnabled())
          {
            componentBodyThicknessMeasurement = new ComponentMeasurement(
                this, subtype,
                MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
                MeasurementUnitsEnum.MILLIMETERS,
                componentInspectionData.getComponent(),
                sliceNameEnum,
                componentBodyThicknessInMillimeters);
            componentInspectionData.getComponentInspectionResult().addMeasurement(componentBodyThicknessMeasurement);
          }
          else
          {
            jointComponentBodyThicknessMeasurement = new JointMeasurement(
                this,
                MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
                MeasurementUnitsEnum.MILLIMETERS,
                padOneJointInspectionData.getPad(),
                sliceNameEnum,
                componentBodyThicknessInMillimeters);
            padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointComponentBodyThicknessMeasurement);
          }

          float bodyThicknessRequiredToTestAsOpaqueInMillimeters = _MILLIMETERS_THICKNESS_REQUIRED_TO_BE_CONSIDERED_AS_OPAQUE_DURING_LEARNING;
//          if (subtype.
//              isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE)
//              == false)
          {
            bodyThicknessRequiredToTestAsOpaqueInMillimeters = (Float)subtype.
                                                               getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);
          }

          float bodyThicknessRequired = bodyThicknessRequiredToTestAsOpaqueInMillimeters;
          if (Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
            bodyThicknessRequired = MathUtil.convertMillimetersToMils(bodyThicknessRequiredToTestAsOpaqueInMillimeters);

          // do the clear/opaque test
          LocalizedString testingAsClearOrOpaqueString = new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_WILL_TEST_AS_CLEAR_KEY", null);
          if (testAsOpaque(padOneJointInspectionData))
          {
            testAsClear = false;
            sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
            // Wei Chin (Tall Cap)
//            if(padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//              sliceNameEnum = SliceNameEnum.PAD;
            reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

            //Khaw Chek Hau - XCR2278: Zero body thickness shown although can see opaque cap on image
            float[] componentThicknessProfileOpaque = ChipAlgorithmUtil.getComponentThicknessProfile(reconstructedSlice,
                                                                                       reconstructionRegion,
                                                                                       componentRegionOfInterest,
                                                                                       padOneJointInspectionData,
                                                                                       this, postDiagnostics);

            float componentBodyThicknessOpaqueInMillimeters = ChipAlgorithmUtil.measureBodyThicknessInMillimeters(componentThicknessProfileOpaque,
                                                                                                      padOneJointInspectionData, padTwoJointInspectionData,
                                                                                                      false);
            
            testingAsClearOrOpaqueString = new LocalizedString("ALGDIAG_CHIP_MEASUREMENT_WILL_TEST_AS_OPAQUE_KEY", null);

            if (Config.isComponentLevelClassificationEnabled())
            {
              //Khaw Chek Hau - XCR2494: Software crash when run test on capacitor subtype
              ComponentMeasurement componentBodyThicknessMeasurementForOpaque = new ComponentMeasurement(
                this, subtype,
                MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
                MeasurementUnitsEnum.MILLIMETERS,
                componentInspectionData.getComponent(),
                SliceNameEnum.OPAQUE_CHIP_PAD,
                componentBodyThicknessOpaqueInMillimeters);
              componentInspectionData.getComponentInspectionResult().addMeasurement(componentBodyThicknessMeasurementForOpaque);
            }
            else
            {
              JointMeasurement jointComponentBodyThicknessMeasurementForOpaque = new JointMeasurement(
                  this,
                  MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
                  MeasurementUnitsEnum.MILLIMETERS,
                  padOneJointInspectionData.getPad(),
                  SliceNameEnum.OPAQUE_CHIP_PAD,
                  componentBodyThicknessOpaqueInMillimeters);
              padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointComponentBodyThicknessMeasurementForOpaque);
            }
          }

          // rerun locator on the appropriate slice
          postLocatorDiags = true;
          Locator.locateRectangularComponent(reconstructedImages, sliceNameEnum, componentInspectionData, this, postLocatorDiags);
          componentRegionOfInterest = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData, sliceNameEnum, true);

          if (Config.isComponentLevelClassificationEnabled())
          {
            if (componentBodyThicknessMeasurement != null)
            {
              postOpaqueTestDiags(reconstructedImages, sliceNameEnum, componentInspectionData, bodyThicknessRequired,
                              componentBodyThicknessMeasurement, testingAsClearOrOpaqueString);
            }
          }
          else
          {
            if (jointComponentBodyThicknessMeasurement != null)
            {
              postOpaqueTestDiags(reconstructedImages, sliceNameEnum, componentInspectionData, bodyThicknessRequired,
                              jointComponentBodyThicknessMeasurement, testingAsClearOrOpaqueString);
            }
          }

          //Lim, Lay Ngor - OKI Tombstone.
          // Run Locator for all slice and joint on Capasitor case - clear chip heel search and area detection use
          Locator.locateJoints(reconstructedImages, SliceNameEnum.OPAQUE_CHIP_PAD, jointInspectionDataObjects, this, false);
          Locator.locateJoints(reconstructedImages, SliceNameEnum.CLEAR_CHIP_PAD, jointInspectionDataObjects, this, false);
            
          // OK, now redo the profile on the proper clear/opaque slice
          // (extend profile so that we can be sure to catch all of the fillets)
          componentRegionOfInterest.scaleFromCenterAlongAcross(1.0 + _PROFILE_EXTENSION, 1.0);
          // does the region fit in the image at all?
          if (ChipAlgorithmUtil.regionFits(reconstructedSlice.getOrthogonalImage(), componentRegionOfInterest,
                                           componentInspectionData) == false)
            continue;

          componentThicknessProfile = ChipAlgorithmUtil.getComponentThicknessProfile(reconstructedSlice,
                                                                                     reconstructionRegion,
                                                                                     componentRegionOfInterest,
                                                                                     padOneJointInspectionData, this, true);
        }

        // get a derivative profile
        float[] componentDerivativeProfile = ProfileUtil.createDerivativeProfile(componentThicknessProfile,
                                                                                 _PROFILE_SMOOTHING_STEP_SIZE);

        // get the nominal body length
        float nominalBodyLengthInMillimeters = getNominalBodyLengthInMillimeters(subtype, testAsClear);
        int nominalBodyLengthInPixels = (int)Math.round(nominalBodyLengthInMillimeters / MILIMETER_PER_PIXEL);

        // find the pad fillet locations
        FloatRef pad1FilletIndexRef = new FloatRef();
        FloatRef pad2FilletIndexRef = new FloatRef();
        measureFilletEdgeLocations(componentDerivativeProfile, componentThicknessProfile,
                               nominalBodyLengthInPixels, padOneJointInspectionData,
                               padTwoJointInspectionData, testAsClear,
                               pad1FilletIndexRef, pad2FilletIndexRef,
                               MILIMETER_PER_PIXEL);

        final float pad1FilletIndex = pad1FilletIndexRef.getValue();
        final float pad2FilletIndex = pad2FilletIndexRef.getValue();
        
        /*Not in used - Old Component Rotation Measurements Method (CR33213)
        // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong - 
        double minCoordinateAlongProfile = componentRegionOfInterest.getMinCoordinateAlong();
        double pad1FilletCoordinateAlongProfile = minCoordinateAlongProfile + pad1FilletIndex;
        double pad2FilletCoordinateAlongProfile = minCoordinateAlongProfile + pad2FilletIndex;
        double pad1CenterCoordinateAlongPad = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong();
        double pad2CenterCoordinateAlongPad = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getCenterAlong();
        double pad1FilletOffsetAlongFromCenter = Math.abs( pad1CenterCoordinateAlongPad-pad1FilletCoordinateAlongProfile);
        double pad2FilletOffsetAlongFromCenter = Math.abs( pad2CenterCoordinateAlongPad-pad2FilletCoordinateAlongProfile);

        if(pad1FilletCoordinateAlongProfile < pad2FilletCoordinateAlongProfile)
        {
            pad1FilletOffsetAlongFromCenter = Math.abs( pad1CenterCoordinateAlongPad-pad1FilletCoordinateAlongProfile);
            pad2FilletOffsetAlongFromCenter = Math.abs( pad2CenterCoordinateAlongPad-pad2FilletCoordinateAlongProfile);
        }
        else
        {
            pad1FilletOffsetAlongFromCenter = Math.abs( pad1CenterCoordinateAlongPad-pad2FilletCoordinateAlongProfile);
            pad2FilletOffsetAlongFromCenter = Math.abs( pad2CenterCoordinateAlongPad-pad1FilletCoordinateAlongProfile);
        }*/

        ClearChipProfileInfo clearChipProfileInfo = new ClearChipProfileInfo();
        if (testAsClear)
        {
          // get the measurements for clear chips
          clearChipProfileInfo = collectMeasurementsForClearChips(componentThicknessProfile, componentRegionOfInterest, padOneJointInspectionData,
                                           padTwoJointInspectionData, componentDerivativeProfile,
                                           pad1FilletIndex, pad2FilletIndex, sliceNameEnum,
                                           reconstructedSlice, reconstructionRegion, reconstructedImages,
                                           nominalBodyLengthInPixels,
                                           MILIMETER_PER_PIXEL);
        }
        else
        {
          // get the measurements for opaque chips
          //Lim, Lay Ngor - Opaque Tombstone - Step 1 calculate the width across inside this function          
          collectProfileMeasurementsForOpaqueChips(padOneJointInspectionData, padTwoJointInspectionData,
                                                   componentDerivativeProfile, componentThicknessProfile,
                                                   pad1FilletIndex, pad2FilletIndex,
                                                   nominalBodyLengthInMillimeters,
                                                   sliceNameEnum, reconstructedSlice.getOrthogonalImage(),
                                                   reconstructionRegion, componentRegionOfInterest, MILIMETER_PER_PIXEL);
  
          //Lim, Lay Ngor - Opaque Tombstone - Step 2 adjust edge and store the partial fillet thickness
          collectPartialFilletThicknessMeasurementsForOpaqueChips(padOneJointInspectionData, padTwoJointInspectionData,
                                                                  componentThicknessProfile,
                                                                  pad1FilletIndex, pad2FilletIndex,
                                                                  sliceNameEnum);
          //end Opaque tombstone
        }
        
        /* Not in used - Old Component Rotation Measurements Method (CR33213)
              // get component rotation in degrees
              BooleanRef shiftMeasurementIsValid = new BooleanRef();
              float componentRotationInDegrees = ChipAlgorithmUtil.measureComponentShiftAcrossInDegrees(
                  Math.round(pad1FilletIndex), Math.round(pad2FilletIndex),
                  componentRegionOfInterest, reconstructedSlice, reconstructionRegion,
                  padOneJointInspectionData, padTwoJointInspectionData, shiftMeasurementIsValid);

              // store component rotation in degrees
              ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
              Component component = padOneJointInspectionData.getPad().getComponent();
              ComponentMeasurement componentRotationInDegreesMeasurement = new ComponentMeasurement(this,
                                                                                                    subtype,
                                                                                                    MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION,
                                                                                                    MeasurementUnitsEnum.DEGREES,
                                                                                                    component,
                                                                                                    sliceNameEnum,
                                                                                                    componentRotationInDegrees,
                                                                                                    shiftMeasurementIsValid.getValue());
              componentInspectionResult.addMeasurement(componentRotationInDegreesMeasurement);
              AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                              reconstructionRegion,
                                                              componentInspectionData,
                                                              componentRotationInDegreesMeasurement);
        */

        // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
        // CR33213-Capacitor Misalignment (Jan 2010) by Lim, Seng Yew
        // get component misalignment across in percentage and rotation degrees
        FloatRef componentRotationInDegreesRef = new FloatRef();
        //testAsClear=true;
        float componentMisalignmentAcrossInPercentage = measureComponentMisalignmentAcrossInPercentage(reconstructedImages,
                                                                                    componentInspectionData,
                                                                                    componentRegionOfInterest,
                                                                                    reconstructionRegion,
                                                                                    sliceNameEnum,
                                                                                    reconstructedSlice.getOrthogonalImage(),
                                                                                    testAsClear,
                                                                                    componentRotationInDegreesRef,
                                                                                    clearChipProfileInfo);

        // get component rotation in degrees
        float componentRotationInDegrees = componentRotationInDegreesRef.getValue();

        // get component misalignment along in percentage
        ComponentMeasurement nominalRotationInDegreesMeasurement  = null;
        JointMeasurement jointNominalRotationInDegreesMeasurement = null;
        
        BooleanRef misalignmentAlongMeasurementIsValid = new BooleanRef();
        FloatRef pad1AlongOffsetInMils = new FloatRef();
        FloatRef pad2AlongOffsetInMils = new FloatRef();
        FloatRef pad1MisalignmentAlongInPercentage = new FloatRef();
        FloatRef pad2MisalignmentAlongInPercentage = new FloatRef();
        float nominalRotationInDegrees = 0.0f;
        float nominalPadHeightInPixels = 0.0f;
        
        float componentMisalignmentAlongInPercentage = measureComponentMisalignmentAlongInPercentage(padOneJointInspectionData, padTwoJointInspectionData,
                                                                                                           componentDerivativeProfile, componentThicknessProfile,
                                                                                                           pad1FilletIndex, pad2FilletIndex,
                                                                                                           pad1AlongOffsetInMils, pad2AlongOffsetInMils,
                                                                                                           pad1MisalignmentAlongInPercentage,pad2MisalignmentAlongInPercentage,
                                                                                                           sliceNameEnum, reconstructedSlice.getOrthogonalImage(),
                                                                                                           reconstructionRegion, componentRegionOfInterest,testAsClear);
        
        //Calculate Maximum Rotation Angle
        nominalPadHeightInPixels = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAcross();
        nominalRotationInDegrees =(float) Math.toDegrees(Math.atan( nominalPadHeightInPixels /(float) nominalBodyLengthInPixels ));
        
        if (Config.isComponentLevelClassificationEnabled())
        {
          // store component rotation in degrees - Component Level
          ComponentInspectionResult componentInspectionResult = componentInspectionData.getComponentInspectionResult();
          Component component = padOneJointInspectionData.getPad().getComponent();
          ComponentMeasurement componentRotationInDegreesMeasurement = new ComponentMeasurement(this,
                                                                                              subtype,
                                                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION,
                                                                                              MeasurementUnitsEnum.DEGREES,
                                                                                              component,
                                                                                              sliceNameEnum,
                                                                                              Math.abs(componentRotationInDegrees),
                                                                                              true);
          componentInspectionResult.addMeasurement(componentRotationInDegreesMeasurement);
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                        reconstructionRegion,
                                                        componentInspectionData,
                                                        componentRotationInDegreesMeasurement);
          
          // store component misalignment across in percentage - Component Level
          ComponentMeasurement componentMisalignmentAcrossInPercentageMeasurement = new ComponentMeasurement(this,
                                                                                          subtype,
                                                                                          MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS,
                                                                                          MeasurementUnitsEnum.PERCENT,//MeasurementUnitsEnum.MILS,
                                                                                          component,
                                                                                          sliceNameEnum,
                                                                                          Math.abs(componentMisalignmentAcrossInPercentage),
                                                                                          true);
          componentInspectionResult.addMeasurement(componentMisalignmentAcrossInPercentageMeasurement);

          // store component misalignment along in percentage - Component level
          ComponentMeasurement componentMisalignmentAlongInPercentageMeasurement = new ComponentMeasurement(this,
                                                                                                subtype,
                                                                                                MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE,
                                                                                                MeasurementUnitsEnum.PERCENT,
                                                                                                component,
                                                                                                sliceNameEnum,
                                                                                                Math.abs(componentMisalignmentAlongInPercentage),
                                                                                                misalignmentAlongMeasurementIsValid.getValue());
          componentInspectionResult.addMeasurement(componentMisalignmentAlongInPercentageMeasurement);

          // store component shift rotation in degree - Component level
          nominalRotationInDegreesMeasurement = new ComponentMeasurement(this,
                                                                         subtype,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES,
                                                                         MeasurementUnitsEnum.DEGREES,
                                                                         component,
                                                                         sliceNameEnum,
                                                                         nominalRotationInDegrees,
                                                                         misalignmentAlongMeasurementIsValid.getValue());
          componentInspectionResult.addMeasurement(nominalRotationInDegreesMeasurement);
        }
        else
        {
          // store component rotation in degrees - Joint Level          
          JointMeasurement jointComponentRotationInDegreesMeasurement = new JointMeasurement(this,
                                                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION,
                                                                                              MeasurementUnitsEnum.DEGREES,
                                                                                              padOneJointInspectionData.getPad(),
                                                                                              sliceNameEnum,
                                                                                              Math.abs(componentRotationInDegrees),
                                                                                              true);
          padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointComponentRotationInDegreesMeasurement);
          AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                        reconstructionRegion,
                                                        padOneJointInspectionData,
                                                        jointComponentRotationInDegreesMeasurement);
          
          // store component misalignment across in percentage - Joint Level
          JointMeasurement jointComponentMisalignmentAcrossInPercentageMeasurement = new JointMeasurement(this,
                                                                                          MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS,
                                                                                          MeasurementUnitsEnum.PERCENT,//MeasurementUnitsEnum.MILS,
                                                                                          padOneJointInspectionData.getPad(),
                                                                                          sliceNameEnum,
                                                                                          Math.abs(componentMisalignmentAcrossInPercentage),
                                                                                          true);
          padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointComponentMisalignmentAcrossInPercentageMeasurement);
          
          // store component misalignment along in percentage - Joint level
          JointMeasurement jointComponentMisalignmentAlongInPercentageMeasurement = new JointMeasurement(this,
                                                                                          MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE,
                                                                                          MeasurementUnitsEnum.PERCENT,
                                                                                          padOneJointInspectionData.getPad(),
                                                                                          sliceNameEnum,
                                                                                          Math.abs(componentMisalignmentAlongInPercentage),
                                                                                          misalignmentAlongMeasurementIsValid.getValue());
          padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointComponentMisalignmentAlongInPercentageMeasurement);

          // store component shift rotation in degree - Joint level
          jointNominalRotationInDegreesMeasurement = new JointMeasurement(this,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES,
                                                                         MeasurementUnitsEnum.DEGREES,
                                                                         padOneJointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         nominalRotationInDegrees,
                                                                         misalignmentAlongMeasurementIsValid.getValue());
          padOneJointInspectionData.getJointInspectionResult().addMeasurement(jointNominalRotationInDegreesMeasurement);
        }
        
        if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
        {
          AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",new Object[] {new String("    Actual Value "),new Float(componentRotationInDegrees),new String("degrees")}),
                                                   reconstructionRegion, sliceNameEnum, padOneJointInspectionData, this);
        }

        JointMeasurement pad1MisalignmentAlongInMilsMeasurement = new JointMeasurement(this,
                                                                           MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE,
                                                                           MeasurementUnitsEnum.PERCENT,
                                                                           padOneJointInspectionData.getPad(),
                                                                           sliceNameEnum,
                                                                           pad1AlongOffsetInMils.getValue());
        JointMeasurement pad1MisalignmentAlongDeltaInPrecentageMeasurement = new JointMeasurement(this,
                                                                           MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE,
                                                                           MeasurementUnitsEnum.PERCENT,
                                                                           padOneJointInspectionData.getPad(),
                                                                           sliceNameEnum,
                                                                           pad1MisalignmentAlongInPercentage.getValue());
        JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();
        padOneJointInspectionResult.addMeasurement(pad1MisalignmentAlongInMilsMeasurement);
        padOneJointInspectionResult.addMeasurement(pad1MisalignmentAlongDeltaInPrecentageMeasurement);

        JointMeasurement pad2MisalignmentAlongInMilsMeasurement = new JointMeasurement(this,
                                                                           MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE,
                                                                           MeasurementUnitsEnum.PERCENT,
                                                                           padTwoJointInspectionData.getPad(),
                                                                           sliceNameEnum,
                                                                           pad2AlongOffsetInMils.getValue());
        JointMeasurement pad2MisalignmentAlongDeltaInPercentageMeasurement = new JointMeasurement(this,
                                                                           MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE,
                                                                           MeasurementUnitsEnum.PERCENT,
                                                                           padTwoJointInspectionData.getPad(),
                                                                           sliceNameEnum,
                                                                           pad2MisalignmentAlongInPercentage.getValue());
        JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();
        padTwoJointInspectionResult.addMeasurement(pad2MisalignmentAlongInMilsMeasurement);
        padTwoJointInspectionResult.addMeasurement(pad2MisalignmentAlongDeltaInPercentageMeasurement);

        //Siew Yeng - XCR-2683 - add enhanced image
        if(ImageProcessingAlgorithm.needToSaveEnhancedImage(subtype))
        {
          ImageProcessingAlgorithm.addEnhancedImageIntoReconstructedImages(reconstructedImages, sliceNameEnum);
        }
        
        if (ImageAnalysis.areDiagnosticsEnabled(subtype.getJointTypeEnum(), this))
        {
          if (Config.isComponentLevelClassificationEnabled())
          {
            if (nominalRotationInDegreesMeasurement != null)
            {
              AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                            reconstructionRegion,
                                                            componentInspectionData,
                                                            nominalRotationInDegreesMeasurement);
            }
          }
          else
          {
            if (jointNominalRotationInDegreesMeasurement != null)
            {
              AlgorithmUtil.postMeasurementTextualDiagnostics(this,
                                                      reconstructionRegion,
                                                      padOneJointInspectionData,
                                                      jointNominalRotationInDegreesMeasurement);
            }
          }
            
          AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",new Object[] {new String("Pin 1 Along Offset"),new Float(Math.abs(pad1AlongOffsetInMils.getValue())),new String("% (" + pad1AlongOffsetInMils.getValue() + "%)")}),
                                                   reconstructionRegion, sliceNameEnum, padOneJointInspectionData, this);
          AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",new Object[] {new String("Pin 1 Misalignment Along Delta"),new Float(Math.abs(pad1MisalignmentAlongInPercentage.getValue())),new String("% (" + pad1MisalignmentAlongInPercentage.getValue() + "%)")}),
                                                   reconstructionRegion, sliceNameEnum, padOneJointInspectionData, this);
          AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",new Object[] {new String("Pin 2 Along Offset"),new Float(Math.abs(pad2AlongOffsetInMils.getValue())),new String("% (" + pad2AlongOffsetInMils.getValue() + "%)")}),
                                                   reconstructionRegion, sliceNameEnum, padTwoJointInspectionData, this);
          AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",new Object[] {new String("Pin 2 Misalignment Along Delta"),new Float(Math.abs(pad2MisalignmentAlongInPercentage.getValue())),new String("% (" + pad2MisalignmentAlongInPercentage.getValue() + "%)")}),
                                                   reconstructionRegion, sliceNameEnum, padTwoJointInspectionData, this);
          AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",new Object[] {new String("Component Shift Along"),new Float(Math.abs(componentMisalignmentAlongInPercentage)),new String("% (" + componentMisalignmentAlongInPercentage + "%)")}),
                                                   reconstructionRegion, sliceNameEnum, padTwoJointInspectionData, this);
          AlgorithmUtil.postTextualDiagnostics(new LocalizedString("ALGDIAG_MEASUREMENT_DIAGNOSTIC_KEY",new Object[] {new String("Component Shift Across"),new Float(Math.abs(componentMisalignmentAcrossInPercentage)),new String("% (" + componentMisalignmentAcrossInPercentage + "%)")}),
                                                   reconstructionRegion, sliceNameEnum, padTwoJointInspectionData, this);
         }
      }
    }
  }
  
  /**
   * Opaque Tombstone code re-factor - Store the partial fillet thickness to measurement.
   * The partial fillet thickness is located quarter from fillet edge from both side.
   *@author Lim, Lay Ngor  
   */
  private void collectPartialFilletThicknessMeasurementsForOpaqueChips(JointInspectionData padOneJointInspectionData,
                                                        JointInspectionData padTwoJointInspectionData,
                                                        float[] componentThicknessProfile,                                                        
                                                        float pad1FilletIndex,
                                                        float pad2FilletIndex,
                                                        SliceNameEnum sliceNameEnum)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentThicknessProfile != null);
    Assert.expect(sliceNameEnum != null);

    //not center but quarter
    double pad1PartialFilletIndex = pad1FilletIndex;
    double pad2PartialFilletIndex = pad2FilletIndex;
    double pad1Size = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
    double pad2Size = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();//Fix bug
    if (pad1FilletIndex > pad2FilletIndex)
    {
      pad1PartialFilletIndex = pad1FilletIndex - pad1Size / 4.f;
      pad2PartialFilletIndex = pad2FilletIndex + pad2Size / 4.f;
    }
    else
    {
      pad1PartialFilletIndex = pad1FilletIndex + pad1Size / 4.f;
      pad2PartialFilletIndex = pad2FilletIndex - pad2Size / 4.f;
    }
       
    float pad1PartialFilletThicknessInMils = ChipAlgorithmUtil.measureFilletThicknessInMils(padOneJointInspectionData, componentThicknessProfile,
      Math.round((float) pad1PartialFilletIndex), false);
    float pad2PartialFilletThicknessInMils = ChipAlgorithmUtil.measureFilletThicknessInMils(padTwoJointInspectionData, componentThicknessProfile,
      Math.round((float) pad2PartialFilletIndex), false);

    // store pad partial fillet bin thickness
    JointMeasurement padOneCenterThicknessMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS,
      MeasurementUnitsEnum.MILLIMETERS,
      padOneJointInspectionData.getPad(),
      sliceNameEnum,
      MathUtil.convertMilsToMillimeters(pad1PartialFilletThicknessInMils));
    padOneJointInspectionData.getJointInspectionResult().addMeasurement(padOneCenterThicknessMeasurement);

    JointMeasurement padTwoCenterThicknessMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS,
      MeasurementUnitsEnum.MILLIMETERS,
      padTwoJointInspectionData.getPad(),
      sliceNameEnum,
      MathUtil.convertMilsToMillimeters(pad2PartialFilletThicknessInMils));
    padTwoJointInspectionData.getJointInspectionResult().addMeasurement(padTwoCenterThicknessMeasurement);
  }
  
  /**
   * Clear Tombstone - perform a matching with the learnt component's joint
   * This feature will be hidden from GUI and only open for OKI use.
   * @author ShengChuan
   */
  private void  getMatchPatternPercentage(JointInspectionData padOneJointInspectionData, 
    JointInspectionData padTwoJointInspectionData,
    SliceNameEnum sliceNameEnum,
    ReconstructedSlice reconstructedSlice) throws DatastoreException
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(reconstructedSlice != null);
    
    RegionOfInterest padOneLocatorRoiForArea = Locator.getRegionOfInterestAtMeasuredLocation(padOneJointInspectionData);
    RegionOfInterest padTwoLocatorRoiForArea = Locator.getRegionOfInterestAtMeasuredLocation(padTwoJointInspectionData);

    Image componentPadOneImage = Image.createCopy(reconstructedSlice.getOrthogonalImage(), padOneLocatorRoiForArea);
    Image componentPadTwoImage = Image.createCopy(reconstructedSlice.getOrthogonalImage(), padTwoLocatorRoiForArea); 
    
    DoubleRef _padOneMatchQuality = new DoubleRef();
    DoubleRef _padTwoMatchQuality = new DoubleRef();

    ExpectedImageTemplateLearning expectedImageTemplate = padOneJointInspectionData.getPad().getComponent().getExpectedImageLearning(padOneJointInspectionData.getPad(), padOneJointInspectionData.getPad().getComponent().getBoard(), sliceNameEnum);

    Image padOneTemplateImage = Image.createCopy(expectedImageTemplate.getLearnedExpectedImage(), padOneLocatorRoiForArea);
    Image padTwoTemplateImage = Image.createCopy(expectedImageTemplate.getLearnedExpectedImage(), padTwoLocatorRoiForArea);

    Filter.matchTemplate(componentPadOneImage, padOneTemplateImage,
      MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
      _padOneMatchQuality);

    Filter.matchTemplate(componentPadTwoImage, padTwoTemplateImage,
      MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
      _padTwoMatchQuality);

    padOneTemplateImage.decrementReferenceCount();
    padTwoTemplateImage.decrementReferenceCount();

    expectedImageTemplate.getLearnedExpectedImage().decrementReferenceCount();

    JointMeasurement componentPadOneMatchingMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING,
      MeasurementUnitsEnum.PERCENT,
      padOneJointInspectionData.getPad(),
      sliceNameEnum,
      MathUtil.roundToPlaces((float)_padOneMatchQuality.getValue() * 100, 1));
    padOneJointInspectionData.getJointInspectionResult().addMeasurement(componentPadOneMatchingMeasurement);

    JointMeasurement componentPadTwoMatchingMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING,
      MeasurementUnitsEnum.PERCENT,
      padTwoJointInspectionData.getPad(),
      sliceNameEnum,
      MathUtil.roundToPlaces((float)_padTwoMatchQuality.getValue() * 100, 1));
    padTwoJointInspectionData.getJointInspectionResult().addMeasurement(componentPadTwoMatchingMeasurement);

    componentPadTwoImage.decrementReferenceCount();
    componentPadOneImage.decrementReferenceCount();   
  }
  
  /**
   * Clear tombstone roundness calculation.
   * author sheng-chuan.yong
   */
  private void getRoundnessCalculationPercentageWithPostDiagnostics(JointInspectionData padOneJointInspectionData, 
    JointInspectionData padTwoJointInspectionData,
    SliceNameEnum sliceNameEnum,
    ReconstructedImages reconstructedImage,
    ReconstructedSlice reconstructedSlice)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(reconstructedImage != null);
    Assert.expect(reconstructedSlice != null);

    RegionOfInterest padOneLocatorRoiForArea = Locator.getRegionOfInterestAtMeasuredLocation(padOneJointInspectionData);
    RegionOfInterest padTwoLocatorRoiForArea = Locator.getRegionOfInterestAtMeasuredLocation(padTwoJointInspectionData);
    Image componentPadOneImage = Image.createCopy(reconstructedSlice.getOrthogonalImage(), padOneLocatorRoiForArea);
    Image componentPadTwoImage = Image.createCopy(reconstructedSlice.getOrthogonalImage(), padTwoLocatorRoiForArea);
      
    double componentPadOneRoundness = getRoundnessValueOfComponent(componentPadOneImage, RegionOfInterest.createRegionFromImage(componentPadOneImage), padOneLocatorRoiForArea) * 100;
    double componentPadTwoRoundness = getRoundnessValueOfComponent(componentPadTwoImage, RegionOfInterest.createRegionFromImage(componentPadTwoImage), padTwoLocatorRoiForArea) * 100;

    JointMeasurement componentPadTwoRoundnessMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT,
      MeasurementUnitsEnum.PERCENT,
      padTwoJointInspectionData.getPad(),
      sliceNameEnum,
      (float) componentPadTwoRoundness);
    padTwoJointInspectionData.getJointInspectionResult().addMeasurement(componentPadTwoRoundnessMeasurement);

    JointMeasurement componentPadOneRoundnessMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT,
      MeasurementUnitsEnum.PERCENT,
      padOneJointInspectionData.getPad(),
      sliceNameEnum,
      (float) componentPadOneRoundness);
    padOneJointInspectionData.getJointInspectionResult().addMeasurement(componentPadOneRoundnessMeasurement);

    //Make it become overlay style
    Threshold.threshold(componentPadOneImage, 150, 60f, 150, 0f);
    Threshold.threshold(componentPadTwoImage, 150, 60f, 150, 0f);
    
    //Show the detected pad use for roundness calculation
    if (ImageAnalysis.areDiagnosticsEnabled(padOneJointInspectionData.getSubtype().getJointTypeEnum(), this))
    {    
      AlgorithmUtil.postStartOfSliceDiagnostics(reconstructedImage, sliceNameEnum, padOneJointInspectionData.getSubtype(), this);
      MeasurementRegionDiagnosticInfo regionOneDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padOneLocatorRoiForArea, MeasurementRegionEnum.PAD_REGION);
      MeasurementRegionDiagnosticInfo regionTwoDiagnosticInfo = new MeasurementRegionDiagnosticInfo(padTwoLocatorRoiForArea, MeasurementRegionEnum.PAD_REGION);

      _diagnostics.postDiagnostics(reconstructedImage.getReconstructionRegion(), sliceNameEnum, padOneJointInspectionData.getSubtype(),
          this, false, true, regionOneDiagnosticInfo, new OverlayImageDiagnosticInfo(componentPadOneImage, padOneLocatorRoiForArea, "Roundness"),
          regionTwoDiagnosticInfo, new OverlayImageDiagnosticInfo(componentPadTwoImage, padTwoLocatorRoiForArea, "Roundness"));
    }
    
    //XCR-3264, Diagnostic Image for Tombstone is not able to send to VVTS 
    if (ImageProcessingAlgorithm.needToSaveVoidDiagnosticImage(padOneJointInspectionData.getSubtype()))
    {
      Image combineImage = null;
      combineImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImage, combineImage, componentPadOneImage, padOneLocatorRoiForArea, sliceNameEnum);
      combineImage = AlgorithmUtil.combineDiagnosticImage(reconstructedImage, combineImage, componentPadTwoImage, padTwoLocatorRoiForArea, sliceNameEnum);
      AlgorithmUtil.addCombineDiagnosticImageIntoReconstructionImages(reconstructedImage, combineImage, sliceNameEnum);
      combineImage.decrementReferenceCount();
    }
    
    // XCR-3177 REFERENCE COUNT WARNING is added when run test if roundness calculation is enabled
    componentPadTwoImage.decrementReferenceCount();
    componentPadOneImage.decrementReferenceCount();
  }
  
  /**
   * Clear Tombstone - compute and obtain the roundness value of detected object in specified ROI
   * @author Shengchuan
   */
  private double getRoundnessValueOfComponent(Image image, RegionOfInterest imageRoi, RegionOfInterest padRoi)
  {
    Assert.expect(image != null);
    Assert.expect(imageRoi != null);
    Assert.expect(padRoi != null);
    
    //obtain the threshold image
    areaDetection(image, imageRoi, true);
    
    //convert the image into black and white
    int[] histogram = Threshold.histogram(image, imageRoi, _NUMBER_OF_HISTOGRAM_BIN);
    float threshold = Threshold.getAutoThreshold(histogram);
    Threshold.threshold(image, threshold, 255f, threshold, 0f);
    
    // find all shape detected
    List<java.awt.Shape> inspectShapeList = BlobAnalyzer.getInstance().blobDetection(image);
    int indexOfImageToBeTaken = 0;
    int largestImageDetected = 0;
    int iterationCount = 0;
    boolean noSuitableObjectDetected = true;
    
    //locate the largest shape
    for (java.awt.Shape element : inspectShapeList)
    {
      iterationCount++;
      //those image without height and width is ignored
      if(element.getBounds().width < _NOISE_AREA_DETECTED && element.getBounds().height < _NOISE_AREA_DETECTED)
        continue;
      
      if(element.getBounds().width * element.getBounds().height > largestImageDetected)
      {
        noSuitableObjectDetected = false;
        largestImageDetected = element.getBounds().width * element.getBounds().height;
        indexOfImageToBeTaken = iterationCount - 1;
      }
    }
    
    if(noSuitableObjectDetected)
      return 0;
    
    //perform roundness calculation in the largest shape image, provided by Dan D
    //image size is + 1 for both width and height because found that the blob function does not fit the size accurately
    java.awt.Shape element = inspectShapeList.get(indexOfImageToBeTaken);
    
    RegionOfInterest detectedRoi = new RegionOfInterest(element.getBounds().x, element.getBounds().y, element.getBounds().width + 1, element.getBounds().height + 1, 0, RegionShapeEnum.RECTANGULAR);
    Image detectedObjectImage = Image.createCopy(image, detectedRoi);
    
    //count the dark pixel value
    int actualArea = Threshold.countPixelsInRange(detectedObjectImage, 0, 50f);
    double detectedImageWidth = (double) (element.getBounds().width + 1) / 2;
    double detectedImageHeight = (double) (element.getBounds().height + 1) / 2;

    detectedObjectImage.decrementReferenceCount();
    double estimatedImageCircleArea = Math.PI * detectedImageWidth * detectedImageHeight;
    double error = Math.abs(actualArea - estimatedImageCircleArea);
    
    double expectedObjectAxisRatio = 1;
    double expectedObjectMaximumAxis = (double)Math.max(padRoi.getLengthAcross(), padRoi.getLengthAlong());
    double expectedObjectMinimumAxis = (double)Math.min(padRoi.getLengthAlong(), padRoi.getLengthAcross());
    
    if(expectedObjectMaximumAxis / expectedObjectMinimumAxis > 1.2)
    {
      expectedObjectAxisRatio = expectedObjectMaximumAxis / expectedObjectMinimumAxis;
    }
    
    double detectedObjectAxisRatio =  Math.min(detectedImageWidth, detectedImageHeight) / Math.max(detectedImageWidth, detectedImageHeight);
    
//    double e1xpectedObjectAxisRatio = Math.min(1,(Math.min(detectedImageWidth, detectedImageHeight) / Math.max(detectedImageWidth, detectedImageHeight) *
//      (double)Math.max(padRoi.getLengthAcross(), padRoi.getLengthAlong()) / Math.min(padRoi.getLengthAlong(), padRoi.getLengthAcross())));
    
    return Math.max(0.0, 1 - error / estimatedImageCircleArea) * Math.min(1,detectedObjectAxisRatio * expectedObjectAxisRatio);
  }
  
  /**
   * @author AnthonyFong
   */
  float getNominalAlongShiftClearInPercentage(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR);
  }
    /**
   * @author AnthonyFong
   */
  float getNominalAlongShiftOpaqueInPercentage(Subtype subtype)
  {
    Assert.expect(subtype != null);
    return (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE);
  }

  /**
   * @author AnthonyFong
   */
  float getNominalShiftRotationInDegrees(Subtype subtype,
                                          boolean testAsClear)
  {
    Assert.expect(subtype != null);
    if (testAsClear)
      return (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION);
    return (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION);
  }

  /**
   * @author Peter Esbensen
   */
  float getNominalBodyLengthInMillimeters(Subtype subtype,
                                          boolean testAsClear)
  {
    Assert.expect(subtype != null);
    if (testAsClear)
      return (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);
    return (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH);
  }

  /**
   * @author Peter Esbensen
   */
  private float learnEffectiveLength(Subtype subtype,
                                     Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataToLearnedDataSet)
      throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(padOneJointInspectionDataToLearnedDataSet != null);

    float estimatedBodyLengthsArray[] = new float[2 * padOneJointInspectionDataToLearnedDataSet.size()]; // estimate lengths for both clear and opaque
    JointInspectionData padOneJointInspectionData = null;
    int i = 0;
    // iterate through all the components and try to guess body length, do this for both the clear and opaque slices
    // so that it can handle mixed subtypes
    for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndChipLearningDataPair :
         padOneJointInspectionDataToLearnedDataSet)
    {
      padOneJointInspectionData = jointInspectionDataAndChipLearningDataPair.getFirst();
      subtype = padOneJointInspectionData.getSubtype();

      ChipLearningData ChipLearningData = jointInspectionDataAndChipLearningDataPair.getSecond();

      SliceNameEnum sliceNameEnum;
      float[] thicknessProfile;
      float maxThickness;
      float leftEdge;
      float rightEdge;
      float estimatedBodyLengthInPixels;

      // Wei Chin (Tall Cap)
//      if( subtype.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//      {
//        sliceNameEnum = SliceNameEnum.PAD;
//        thicknessProfile = ChipLearningData.getComponentBodyThicknessProfile(sliceNameEnum);
//
//        maxThickness = ArrayUtil.max(thicknessProfile);
//
//        leftEdge = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(thicknessProfile, 0, thicknessProfile.length / 2, maxThickness * 0.5f);
//        rightEdge = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(thicknessProfile, thicknessProfile.length - 1, 0, maxThickness * 0.5f);
//
//        estimatedBodyLengthInPixels = rightEdge - leftEdge;
//        estimatedBodyLengthsArray[i++] = estimatedBodyLengthInPixels;
//      }
//      else
//      {
        // clear slice
        sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
        thicknessProfile = ChipLearningData.getComponentBodyThicknessProfile(sliceNameEnum);

        maxThickness = ArrayUtil.max(thicknessProfile);

        leftEdge = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(thicknessProfile, 0, thicknessProfile.length / 2, maxThickness * 0.5f);
        rightEdge = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(thicknessProfile, thicknessProfile.length - 1, 0, maxThickness * 0.5f);

        estimatedBodyLengthInPixels = rightEdge - leftEdge;
        estimatedBodyLengthsArray[i++] = estimatedBodyLengthInPixels;
        // opaque slice
        sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
        thicknessProfile = ChipLearningData.getComponentBodyThicknessProfile(sliceNameEnum);

        maxThickness = ArrayUtil.max(thicknessProfile);

        leftEdge = ProfileUtil.findSubpixelEdgeLocationSearchingLeftToRight(thicknessProfile, 0, thicknessProfile.length / 2, maxThickness * 0.5f);
        rightEdge = ProfileUtil.findSubpixelEdgeLocationSearchingRightToLeft(thicknessProfile, thicknessProfile.length - 1, 0, maxThickness * 0.5f);

        estimatedBodyLengthInPixels = rightEdge - leftEdge;
        estimatedBodyLengthsArray[i++] = estimatedBodyLengthInPixels;
//      }
    }

    final float BODY_LENGTH_PERCENTILE = 0.95f; // this may include some opens unfortunately . . . if we ever are able to ask questions, this is a place to do it

    float expectedBodyLength = StatisticsUtil.percentile(estimatedBodyLengthsArray, BODY_LENGTH_PERCENTILE);
    ComponentInspectionData componentInspectionData = padOneJointInspectionData.getComponentInspectionData();
    int lengthInPixelsAccordingToCad = componentInspectionData.getOrthogonalComponentRegionOfInterest().getLengthAlong();

    float effectiveBodyLengthFraction = expectedBodyLength / (float)lengthInPixelsAccordingToCad;

    return effectiveBodyLengthFraction * 100.0f;
  }

  /**
   * Clear Tombstone - Learn template image, this feature will be hidden from GUI and only open for OKI use.
   * @param subtype
   * @param board
   * @param typicalBoardImages
   * @param unloadedBoardImages
   * @return
   * @throws DatastoreException
   * @author ShengChuan
   */
  public boolean learnImageTemplateData(Subtype subtype,
    Board board,
    ManagedOfflineImageSet typicalBoardImages,
    ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    ManagedOfflineImageSet imageSetToUse = typicalBoardImages;
    if (imageSetToUse.getReconstructionRegions().isEmpty())
    {
      imageSetToUse = unloadedBoardImages;
    }
    
    final SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;

    ManagedOfflineImageSetIterator imagesIterator = imageSetToUse.iterator();
    ReconstructedImages reconstructedImages;
    List<DiagnosticInfo> listOfDiagnosticsToDisplay = new LinkedList<DiagnosticInfo>();
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructedRegion = reconstructedImages.getReconstructionRegion();
      Component component = reconstructedRegion.getComponent();
      List<JointInspectionData> jointInspectionDataList = reconstructedRegion.getInspectableJointInspectionDataList(subtype);
      
      //** Lay Ngor temporary comment
//      RegionOfInterest locatedComponent = null;//LN This parameter not using at all !!

      for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList))
      {
        boolean needToReleaseImageReference = false;
        ExpectedImageTemplateLearning expectedImageTemplateLearning = null;
       
        ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

        Image padImage = reconstructedSlice.getOrthogonalImage();
        //** Lay Ngor temporary comment
//        RegionOfInterest componentRegionOfInterest = componentInspectionData.getOrthogonalComponentRegionOfInterest();

//        JointInspectionData joint = componentInspectionData.getPadOneJointInspectionData();
//        locatedComponent = Locator.locateRegion(joint, padImage, componentRegionOfInterest,
//          reconstructedRegion, subtype.getInspectionFamily().getSliceNameEnumForLocator(subtype), this, listOfDiagnosticsToDisplay);
        
        Pad padOne = componentInspectionData.getPadOneJointInspectionData().getPad();
        Component learnComponent = padOne.getComponent();
        
        if (board != null && learnComponent.hasExpectedImageTemplateLearning(padOne, board, sliceNameEnum, padImage))
        {
          expectedImageTemplateLearning = learnComponent.getExpectedImageLearning(padOne, board, sliceNameEnum);
          needToReleaseImageReference = true;
        }
        else
        {
          expectedImageTemplateLearning = new ExpectedImageTemplateLearning();
          String imageName = FileName.getInspectionImageName(reconstructedRegion.getName(), sliceNameEnum.getId());
          expectedImageTemplateLearning.setImageName(imageName);
          expectedImageTemplateLearning.setLearnedExpectedImage(padImage);
          needToReleaseImageReference = false;
        }
        
        expectedImageTemplateLearning.setSliceNameEnum(sliceNameEnum);
        expectedImageTemplateLearning.setPad(padOne);
        component.setExpectedImageLearning(expectedImageTemplateLearning);
        
        if(needToReleaseImageReference)
          expectedImageTemplateLearning.getLearnedExpectedImage().decrementReferenceCount();
       
        listOfDiagnosticsToDisplay.clear();
      }
      imagesIterator.finishedWithCurrentRegion();
    }
    return true;
  }

  /**
   * @author Peter Esbensen plagiarizing Sunit Bhalla
   */
  private void convertInspectionMeasurementsToLearningLists(ComponentInspectionData componentInspectionData,
                                                            List<Float> opaqueBodyLengthInMillimetersMeasurements,
                                                            List<Float> clearBodyLengthInMillimetersMeasurements,
                                                            List<Float> opaqueNominalPadThicknessInMillimetersMeasurements,
                                                            List<Float> clearNominalPadThicknessInMillimetersMeasurements,
                                                            List<Float> nominalPadShiftAlongOpaqueInPercentageMeasurements,
                                                            List<Float> nominalPadShiftAlongClearInPercentageMeasurements,
                                                            List<Float> nominalShiftRotationInDegreeMeasurements)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(opaqueBodyLengthInMillimetersMeasurements != null);
    Assert.expect(opaqueNominalPadThicknessInMillimetersMeasurements != null);
    Assert.expect(clearNominalPadThicknessInMillimetersMeasurements != null);
    Assert.expect(nominalPadShiftAlongOpaqueInPercentageMeasurements != null);
    Assert.expect(nominalPadShiftAlongClearInPercentageMeasurements != null);
    Assert.expect(nominalShiftRotationInDegreeMeasurements != null);

    boolean testAsOpaque = false;
    boolean testAsClear = false;
    // Wei Chin (Tall Cap)
//    if(componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//      testAsOpaque = true;
//    else
//    {
      testAsOpaque = isUpdateOpaque(componentInspectionData.getPadOneJointInspectionData().getSubtype());
      testAsClear = isUpdateClear(componentInspectionData.getPadOneJointInspectionData().getSubtype());
//    }

    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    SliceNameEnum sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;

    if (testAsOpaque)
    {
      // Wei Chin (Tall Cap)
//      if(padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//        sliceNameEnum = SliceNameEnum.PAD;
      // Make sure classifier did generate inspection results.  Otherwise, it would assert when getting measurement.
      if (padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS)
          && padTwoJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS)
          && (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH) ||
              padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH))
          && padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE)
          && padTwoJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE)
          && (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES) ||
              padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES)))
      {
        JointMeasurement padOneJointMeasurement = getOpaqueFilletThicknessInMillimetersMeasurement(padOneJointInspectionData, sliceNameEnum);
        JointMeasurement padTwoJointMeasurement = getOpaqueFilletThicknessInMillimetersMeasurement(padTwoJointInspectionData, sliceNameEnum);

        opaqueNominalPadThicknessInMillimetersMeasurements.add(padOneJointMeasurement.getValue());
        opaqueNominalPadThicknessInMillimetersMeasurements.add(padTwoJointMeasurement.getValue());

        if (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH))
        {
          ComponentMeasurement componentMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(componentInspectionData, sliceNameEnum, true);
          opaqueBodyLengthInMillimetersMeasurements.add(componentMeasurement.getValue());
        }
        else
        {
          JointMeasurement jointComponentMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(padOneJointInspectionData, sliceNameEnum, true);
          opaqueBodyLengthInMillimetersMeasurements.add(jointComponentMeasurement.getValue());
        }

        JointMeasurement joint1AlongOffsetInPercentage = getPin1AlongOffsetInPercentageMeasurement(componentInspectionData, sliceNameEnum);
        nominalPadShiftAlongOpaqueInPercentageMeasurements.add(joint1AlongOffsetInPercentage.getValue());

        JointMeasurement joint2AlongOffsetInPercentage = getPin2AlongOffsetInPercentageMeasurement(componentInspectionData, sliceNameEnum);
        nominalPadShiftAlongOpaqueInPercentageMeasurements.add(joint2AlongOffsetInPercentage.getValue());

        if (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES))
        {
          ComponentMeasurement shiftRotationMeasurement = getNominalRotationInDegreesMeasurement(componentInspectionData, sliceNameEnum);
          nominalShiftRotationInDegreeMeasurements.add(shiftRotationMeasurement.getValue());
        }
        else
        {
          JointMeasurement jointShiftRotationMeasurement = getNominalRotationInDegreesMeasurement(padOneJointInspectionData, sliceNameEnum);
          nominalShiftRotationInDegreeMeasurements.add(jointShiftRotationMeasurement.getValue());
        }
      }
    }
    if(testAsClear)
    {
      sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;

      // Make sure classifier did generate inspection results.  Otherwise, it would assert when getting measurement.
      if (padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS)
          && padTwoJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS)
          && (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH) ||
              padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH))
          && padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE)
          && padTwoJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE)
          && (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES) ||
              padOneJointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES)))
      {

        JointMeasurement padOneJointMeasurement = getClearFilletThicknessInMillimetersMeasurement(padOneJointInspectionData, sliceNameEnum);
        JointMeasurement padTwoJointMeasurement = getClearFilletThicknessInMillimetersMeasurement(padTwoJointInspectionData, sliceNameEnum);

        clearNominalPadThicknessInMillimetersMeasurements.add(padOneJointMeasurement.getValue());
        clearNominalPadThicknessInMillimetersMeasurements.add(padTwoJointMeasurement.getValue());

        if (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH))
        {
          ComponentMeasurement componentMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(componentInspectionData, sliceNameEnum, false);
          clearBodyLengthInMillimetersMeasurements.add(componentMeasurement.getValue());
        }
        else
        {
          JointMeasurement jointMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(padOneJointInspectionData, sliceNameEnum, false);
          clearBodyLengthInMillimetersMeasurements.add(jointMeasurement.getValue());
        }

        JointMeasurement joint1AlongOffsetInPercentage = getPin1AlongOffsetInPercentageMeasurement(componentInspectionData, sliceNameEnum);
        nominalPadShiftAlongClearInPercentageMeasurements.add(joint1AlongOffsetInPercentage.getValue());

        JointMeasurement joint2AlongOffsetInPercentage = getPin2AlongOffsetInPercentageMeasurement(componentInspectionData, sliceNameEnum);
        nominalPadShiftAlongClearInPercentageMeasurements.add(joint2AlongOffsetInPercentage.getValue());

        if (componentInspectionData.getComponentInspectionResult().hasComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES))
        {
          ComponentMeasurement shiftRotationMeasurement = getNominalRotationInDegreesMeasurement(componentInspectionData, sliceNameEnum);
          nominalShiftRotationInDegreeMeasurements.add(shiftRotationMeasurement.getValue()); 
        }
        else
        {
          JointMeasurement jointShiftRotationMeasurement = getNominalRotationInDegreesMeasurement(padOneJointInspectionData, sliceNameEnum);
          nominalShiftRotationInDegreeMeasurements.add(jointShiftRotationMeasurement.getValue());
        }
     }
    }
  }

  /**
   * Clear Tombstone
   * author sheng-chuan
   * @param subtype
   * @throws DatastoreException 
   */
  public void deleteLearnedImageTemplateData(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    // Delete the template learning for all pads in the subtype.
    Collection<Pad> subtypePads = subtype.getPads();
    for (Pad pad : subtypePads)
    {
      deleteLearnedImageTemplateData(pad);
    }
  }
  
  /**
   * Clear Tombstone
   * @author sheng-chuan
   */
  public void deleteLearnedImageTemplateData(Pad pad) throws DatastoreException
  {
    Assert.expect(pad != null);

    Component component = pad.getComponent();
    
    if (component.hasExpectedImageTemplateLearning(pad))
      component.deleteExpectedImageLearning(pad);
  }
  
  /**
   * @author Peter Esbensen
   */
  public void updateNominals(Subtype subtype,
                             ManagedOfflineImageSet typicalBoardImages,
                             ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    // Added by Seng Yew on 22-Apr-2011
    super.updateNominals(subtype,typicalBoardImages,unloadedBoardImages);

    if (typicalBoardImages.size() == 0)
      return;

    // Unfortunately, I need to do this process twice.  Since clear pad thickness is dependent on clear body length,
    // we have to go through things once to get the right body length and then a second time to get the right pad thicknesses.
    for (int i = 0; i < 2; ++i)
    {
      ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
      ReconstructedImages reconstructedImages;
      List<Float> opaqueBodyLengthInMillimetersMeasurements = new LinkedList<Float>();
      List<Float> clearBodyLengthInMillimetersMeasurements = new LinkedList<Float>();
      List<Float> opaqueNominalPadThicknessInMillimetersMeasurements = new LinkedList<Float>();
      List<Float> clearNominalPadThicknessInMillimetersMeasurements = new LinkedList<Float>();
      List<Float> nominalPadShiftAlongOpaqueInPencentageMeasurements = new LinkedList<Float>();
      List<Float> nominalPadShiftAlongClearInPencentageMeasurements = new LinkedList<Float>();
      List<Float> nominalShiftRotationInDegreeMeasurements = new LinkedList<Float>();

      while ((reconstructedImages = imagesIterator.getNext()) != null)
      {
        ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
        List<JointInspectionData> jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

        classifyJoints(reconstructedImages, jointInspectionDataList);

        Collection<ComponentInspectionData> componentInspectionDataList = AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList); 
        for (ComponentInspectionData componentInspectionData : componentInspectionDataList)
        {
          convertInspectionMeasurementsToLearningLists(componentInspectionData,
                                                       opaqueBodyLengthInMillimetersMeasurements,
                                                       clearBodyLengthInMillimetersMeasurements,
                                                       opaqueNominalPadThicknessInMillimetersMeasurements,
                                                       clearNominalPadThicknessInMillimetersMeasurements,
                                                       nominalPadShiftAlongOpaqueInPencentageMeasurements,
                                                       nominalPadShiftAlongClearInPencentageMeasurements,
                                                       nominalShiftRotationInDegreeMeasurements);
          componentInspectionData.getComponentInspectionResult().clearMeasurements();
          componentInspectionData.clearComponentInspectionResult();
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().clearMeasurements();
          componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getJointInspectionResult().clearMeasurements();
        }
        for (JointInspectionData jointInspectionData : jointInspectionDataList)
        {
          jointInspectionData.getJointInspectionResult().clearMeasurements();
          jointInspectionData.clearJointInspectionResult();
        }

        imagesIterator.finishedWithCurrentRegion();
        
        // XCR1481 by Lee Herng 6 Aug 2012 - Clear list
        if (componentInspectionDataList != null)
        {          
          componentInspectionDataList.clear();
          componentInspectionDataList = null;
        }
        
        if (jointInspectionDataList != null)
        {          
          jointInspectionDataList.clear();
          jointInspectionDataList = null;
        }
      }

      if (opaqueBodyLengthInMillimetersMeasurements.size() > 0)
      {
        float expectedOpaqueBodyLengthInMillimeters = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(opaqueBodyLengthInMillimetersMeasurements));
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH, expectedOpaqueBodyLengthInMillimeters);
      }

      if (subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH))
      {
        if (clearBodyLengthInMillimetersMeasurements.size() > 0)
        {
          float expectedClearBodyLengthInMillimeters = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(clearBodyLengthInMillimetersMeasurements));
          subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH, expectedClearBodyLengthInMillimeters);
        }
      }
      if (opaqueNominalPadThicknessInMillimetersMeasurements.size() > 0)
      {
        float expectedOpaquePadThicknessInMillimeters = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(opaqueNominalPadThicknessInMillimetersMeasurements));
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS, expectedOpaquePadThicknessInMillimeters);
      }
      if (clearNominalPadThicknessInMillimetersMeasurements.size() > 0)
      {
        float expectedClearPadThicknessInMillimeters = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(clearNominalPadThicknessInMillimetersMeasurements));
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS, expectedClearPadThicknessInMillimeters);
      }
      if (nominalPadShiftAlongOpaqueInPencentageMeasurements.size() > 0)
      {
        float expectedNominalPadShiftAlongOpaqueInPencentage = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(nominalPadShiftAlongOpaqueInPencentageMeasurements));
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE, expectedNominalPadShiftAlongOpaqueInPencentage);
      }
      if (nominalPadShiftAlongClearInPencentageMeasurements.size() > 0)
      {
        float expectedNominalPadShiftAlongClearInPencentage = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(nominalPadShiftAlongClearInPencentageMeasurements));
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR, expectedNominalPadShiftAlongClearInPencentage);
      }
      if (nominalShiftRotationInDegreeMeasurements.size() > 0)
      {
        float expectedNominalShiftRotationInDegree = StatisticsUtil.median(ArrayUtil.convertFloatListToFloatArray(nominalShiftRotationInDegreeMeasurements));
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION, expectedNominalShiftRotationInDegree);
      }

      // Added by Seng Yew on 22-Apr-2011
      // Need to be careful when call this. Have to make sure JointMeasurement & ComponentMeasurement instances created before this reset are not used anywhere.
      // JointInspectionResult::addMeasurement(...) & ComponentInspectionResult::addMeasurement(...) checks for duplicate _id when add.
      // After call this function, any new JointMeasurement & ComponentMeasurement instances created will start with zero again, and cause duplicate _id and cause crashes.
      subtype.getPanel().getProject().getTestProgram().startNewInspectionRun();  // make sure we clear out the measurements for the next run (WRONG USAGE PREVIOUSLY)
    }
  }

  /**
   * @author Peter Esbensen & Chong, Wei Chin (CR32137)
   * @author Anthony Fong & Lim, Seng Yew (CR33213)
   */
  public void learnAlgorithmSettings(Subtype subtype,
                                     ManagedOfflineImageSet typicalBoardImages,
                                     ManagedOfflineImageSet unloadedBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);
    Assert.expect(unloadedBoardImages != null);

    if (typicalBoardImages.size() == 0)
      return;

    final float MILIMETER_PER_PIXEL = AlgorithmUtil.getMillimetersPerPixel(subtype);
    Locator.learn(subtype, this, typicalBoardImages, unloadedBoardImages);

    // get all the profiles and put them into this set thing
    Set<Pair<JointInspectionData, ChipLearningData>> typicalPadOneJointInspectionDataAndLearnedDataSet =
      getPadOneJointInspectionDataAndLearnedDataSet(subtype, typicalBoardImages);

    int numberOfJointsLearnedOn = typicalPadOneJointInspectionDataAndLearnedDataSet.size() * 2; // two joints per component

    if (numberOfJointsLearnedOn > 0)
    {
      // figure out best Locator settings if this isn't already tuned
      // Close down this because it will affect out image.
      /*if (subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH))
      {
        //float effectiveLength = learnEffectiveLength(subtype, typicalPadOneJointInspectionDataAndLearnedDataSet);
        //subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.LOCATE_EFFECTIVE_LENGTH, effectiveLength);
      }*/

      // figure out best background locations
      //float bestBackgroundLocation = AlgorithmUtil.learnChipBackgroundRegionLocations(subtype, typicalBoardImages, this);
      //subtype.setLearnedValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BACKGROUND_REGION_SHIFT, bestBackgroundLocation);

      // ok since we fine tuned the background locations and locator, get the profiles again in case they are better now
      typicalPadOneJointInspectionDataAndLearnedDataSet.clear();
      typicalPadOneJointInspectionDataAndLearnedDataSet = getPadOneJointInspectionDataAndLearnedDataSet(subtype, typicalBoardImages);

      // get the unloaded panel profiles
      Set<Pair<JointInspectionData, ChipLearningData>> unloadedPadOneJointInspectionDataAndLearnedDataSet =
          getPadOneJointInspectionDataAndLearnedDataSet(subtype, unloadedBoardImages);

      boolean testAsOpaque = isUpdateOpaque(subtype);
      boolean testAsClear = isUpdateClear(subtype);


      // learn the body thickness
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier inside
      learnTestAsOpaqueAndOpaqueThicknessThreshold(typicalPadOneJointInspectionDataAndLearnedDataSet, subtype, testAsOpaque, testAsClear);

      // get separate sets for components that are assumed to be clear and for components that are assumed to be opaque
      Set<Pair<JointInspectionData, ChipLearningData>> clearPadOneJointInspectionDataAndLearnedDataSet = new HashSet<Pair<JointInspectionData, ChipLearningData>>();
      Set<Pair<JointInspectionData, ChipLearningData>> opaquePadOneJointInspectionDataAndLearnedDataSet = new HashSet<Pair<JointInspectionData, ChipLearningData>>();
      getClearChipPadOneJointInspectionDataAndLearnedDataSet(typicalPadOneJointInspectionDataAndLearnedDataSet,
                                                             clearPadOneJointInspectionDataAndLearnedDataSet,
                                                             opaquePadOneJointInspectionDataAndLearnedDataSet);
      typicalPadOneJointInspectionDataAndLearnedDataSet = null;

      // learn the expected body length - do it for assuming both clear and opaque so user can switch between the two
      filterJointsThatAppearToBeBad(clearPadOneJointInspectionDataAndLearnedDataSet);

      if (Config.isIntelligentLearning())
      {
        //Lim, Lay Ngor - XCR-2027 filter the outlier joint base on body thickness
        filterOutlierJointsBaseOnBodyThickness(clearPadOneJointInspectionDataAndLearnedDataSet, subtype);
        filterOutlierJointsBaseOnBodyThickness(opaquePadOneJointInspectionDataAndLearnedDataSet, subtype);
        //Lim, Lay Ngor - XCR-2027 exclude the body length, component length cannot exlude as not so applicable
      }
      learnClearAndOpaqueBodyLengthInPixels(clearPadOneJointInspectionDataAndLearnedDataSet,
                                            opaquePadOneJointInspectionDataAndLearnedDataSet,
                                            testAsOpaque,
                                            testAsClear,
                                            MILIMETER_PER_PIXEL);

      // learn the best offset for the lower fillet region
      // iterate through all joints in images and collect profiles
      //Lim, Lay Ngor - XCR-2027 Effort too big... and it already got filter bad and outlier joint above.
      learnFilletOffsets(clearPadOneJointInspectionDataAndLearnedDataSet,
                         unloadedPadOneJointInspectionDataAndLearnedDataSet,
                         subtype, MILIMETER_PER_PIXEL);

      //Lim, Lay Ngor - XCR-2027 - only partially exclude outliers
      //Effort too big... and it already got filter bad and outlier joint above.
      if (testAsClear)
      {
        learnClearFilletThicknesses(clearPadOneJointInspectionDataAndLearnedDataSet, subtype, MILIMETER_PER_PIXEL);
        learnPadsNominalShiftAlong(clearPadOneJointInspectionDataAndLearnedDataSet, subtype, true, MILIMETER_PER_PIXEL);
      }
      // Need to learn opaque for capacitor only
      if (subtype.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR))
      {
        if (testAsOpaque)
        {
          //Lim, Lay Ngor - XCR-2027 exclude the outliers.
          learnOpaqueFilletThickness(opaquePadOneJointInspectionDataAndLearnedDataSet, subtype, MILIMETER_PER_PIXEL);
          //Lim, Lay Ngor - XCR-2027 - only partially exclude outliers
          //Effort too big... and it already got filter bad and outlier joint above.
          learnPadsNominalShiftAlong(opaquePadOneJointInspectionDataAndLearnedDataSet, subtype, false, MILIMETER_PER_PIXEL);
        }
      }
      // Wei Chin (Tall Cap)
//      else if(subtype.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//      {
//        learnOpaqueFilletThickness(opaquePadOneJointInspectionDataAndLearnedDataSet, subtype);
//        learnPadsNominalShiftAlong(opaquePadOneJointInspectionDataAndLearnedDataSet, subtype, false);
//      }
      //if (testAsOpaque(padOneJointInspectionData))
    }

    final int MINIMUM_JOINTS_REQUIRED_FOR_REASONABLE_LEARNING = 12;
    if (numberOfJointsLearnedOn < MINIMUM_JOINTS_REQUIRED_FOR_REASONABLE_LEARNING)
      AlgorithmUtil.raiseInitialTuningLimitedDataWarning(subtype, numberOfJointsLearnedOn);
  }

  /**
   * @author Peter Esbensen
   */
  private void filterJointsThatAppearToBeBad(Set<Pair<JointInspectionData, ChipLearningData>> clearPadOneJointInspectionDataAndLearnedDataSet)
  {
    Assert.expect(clearPadOneJointInspectionDataAndLearnedDataSet != null);

    Set<Pair<JointInspectionData, ChipLearningData>> itemsToRemove = new HashSet<Pair<JointInspectionData, ChipLearningData>>();

    for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedData : clearPadOneJointInspectionDataAndLearnedDataSet)
    {
      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().getPadTwoJointInspectionDataOnTwoPinDevices();
      float[] componentBodyThicknessProfile = jointInspectionDataAndLearnedData.getSecond().getComponentBodyThicknessProfile(SliceNameEnum.CLEAR_CHIP_PAD);

      // if the skewness of either joint is too close to zero (too symmetric) then remove this pad from the data
      boolean padOneIsSkewed = isPadOneSkewed(padOneJointInspectionData, componentBodyThicknessProfile);
      boolean padTwoIsSkewed = isPadTwoSkewed(padTwoJointInspectionData, componentBodyThicknessProfile);
      if ((padOneIsSkewed == false) || (padTwoIsSkewed == false)) // ie - if too symmetric
      {
        itemsToRemove.add(jointInspectionDataAndLearnedData);
      }
    }
    clearPadOneJointInspectionDataAndLearnedDataSet.removeAll(itemsToRemove);
  }

  /**
   * Apply filter only if the learn data contains more than two items.
   * @author Lim, Lay Ngor - XCR-2027
   */
  private void filterOutlierJointsBaseOnBodyThickness(Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataAndLearnedDataSet,
    Subtype subtype) throws DatastoreException
  {
    Assert.expect(padOneJointInspectionDataAndLearnedDataSet != null);
    
    SliceNameEnum sliceNameEnum = getSliceNameEnumForPadSlice(subtype.getJointTypeEnum());
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier - This one already exclude outlier at body thickness for each component level
    float[] componentBodyThicknessesInMillimetersForSlice = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
      MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS, true, true);
    if(componentBodyThicknessesInMillimetersForSlice.length >= 2)//Minimum two data is needed for calculate outlier fences
    {
      float[] outlierOuterFences = StatisticsUtil.getOutliersOuterFences(componentBodyThicknessesInMillimetersForSlice);

      Set<Pair<JointInspectionData, ChipLearningData>> itemsToRemove = new HashSet<Pair<JointInspectionData, ChipLearningData>>();
      for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedData : padOneJointInspectionDataAndLearnedDataSet)
      {
        ChipLearningData ChipLearningData = jointInspectionDataAndLearnedData.getSecond();
        float componentBodyThicknessMeanValue = ChipLearningData.getComponentBodyThicknessMeanValue(sliceNameEnum);
        // if the componentBodyThicknessMeanValue is an outlier, then remove this pad from the data
        if (componentBodyThicknessMeanValue < outlierOuterFences[0] || componentBodyThicknessMeanValue > outlierOuterFences[1])
        {
          //is outlier
          itemsToRemove.add(jointInspectionDataAndLearnedData);
        }
      }
      padOneJointInspectionDataAndLearnedDataSet.removeAll(itemsToRemove);      
    }
  }

  /**
   * @author Peter Esbensen
   */
  private boolean isPadTwoSkewed(JointInspectionData padTwoJointInspectionData,
                                 float[] componentBodyThicknessProfile)
  {
    Assert.expect(componentBodyThicknessProfile != null);
    RegionOfInterest padTwoRegionOfInterest = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest componentRegionOfInterest = padTwoJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

    int padTwoStartIndex = ChipAlgorithmUtil.getPadTwoLeftSideIndex(padTwoJointInspectionData, componentRegionOfInterest);
    int padTwoEndIndex = ChipAlgorithmUtil.getPadTwoRightSideIndex(componentBodyThicknessProfile, padTwoJointInspectionData, padTwoRegionOfInterest, componentRegionOfInterest);

    float[] padTwoArray = ArrayUtil.copy(componentBodyThicknessProfile, padTwoStartIndex, padTwoEndIndex);

    return isPadProfileSkewed(padTwoArray);
  }

  /**
   * @author Peter Esbensen
   */
  private boolean isPadProfileSkewed(float[] profile)
  {
    Assert.expect(profile != null);

    float skewness = StatisticsUtil.getSkewness(profile, profile.length * 0.5f);
    double skewnessStandardError = Math.sqrt(6.0 / ArrayUtil.sum(profile));
    double normalizedSkewness = skewness / skewnessStandardError;

    if (Math.abs(normalizedSkewness) > 0.05)
    {
//      System.out.println(padTwoJointInspectionData.getFullyQualifiedPadName() + " normalizedSkew = " + normalizedSkewness);
      return true;
    }
//    System.out.println(padTwoJointInspectionData.getFullyQualifiedPadName() + " normalizedSkew = " + normalizedSkewness + " NOTSKEWED");
    return false;
  }

  /**
   * @author Peter Esbensen
   */
  private boolean isPadOneSkewed(JointInspectionData padOneJointInspectionData,
                                 float[] componentBodyThicknessProfile)
  {
    Assert.expect(componentBodyThicknessProfile != null);
    RegionOfInterest padOneRegionOfInterest = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest componentRegionOfInterest = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

    int padOneStartIndex = ChipAlgorithmUtil.getPadOneLeftSideIndex(componentBodyThicknessProfile, padOneJointInspectionData, padOneRegionOfInterest, componentRegionOfInterest);
    int padOneEndIndex = ChipAlgorithmUtil.getPadOneRightSideIndex(componentBodyThicknessProfile, padOneJointInspectionData, componentRegionOfInterest);

    float[] padOneArray = ArrayUtil.copy(componentBodyThicknessProfile, padOneStartIndex, padOneEndIndex);

    return isPadProfileSkewed(padOneArray);
  }

  /**
   * @author Peter Esbensen
   */
  private void learnClearFilletThicknesses(
      Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataAndLearnedDataSet,
      Subtype subtype, final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(padOneJointInspectionDataAndLearnedDataSet != null);
    Assert.expect(subtype != null);

    if (padOneJointInspectionDataAndLearnedDataSet.size() < 1)
      return;

    SliceNameEnum sliceNameEnum = null;

    Iterator<Pair<JointInspectionData, ChipLearningData>> setIt = padOneJointInspectionDataAndLearnedDataSet.iterator();

    while (setIt.hasNext())
    {
      Pair<JointInspectionData, ChipLearningData> setEntry = setIt.next();

      JointInspectionData padOneJointInspectionData = setEntry.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
                                                      getPadTwoJointInspectionDataOnTwoPinDevices();

      float[] componentBodyProfile = null;
      ChipLearningData ChipLearningData = setEntry.getSecond();

      if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP))
        sliceNameEnum = SliceNameEnum.PAD;
      else
        sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      componentBodyProfile = ChipLearningData.getComponentBodyThicknessProfile(sliceNameEnum);
      float[] componentDerivativeProfile = ChipLearningData.getComponentDerivativeProfile(sliceNameEnum);

      FloatRef padOneFilletEdgeIndexRef = new FloatRef();
      FloatRef padTwoFilletEdgeIndexRef = new FloatRef();
      float nominalComponentBodyLengthInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);
      int nominalBodyLengthInPixels = Math.round(nominalComponentBodyLengthInMillimeters / MILIMETER_PER_PIXEL);

      locateClearChipEdges(padOneJointInspectionData, padTwoJointInspectionData,
                           componentBodyProfile, componentDerivativeProfile,
                           padOneFilletEdgeIndexRef, padTwoFilletEdgeIndexRef,
                           nominalBodyLengthInPixels, MILIMETER_PER_PIXEL);

      ClearChipProfileInfo clearChipProfileInfo = new ClearChipProfileInfo();
      clearChipProfileInfo.setComponentBodyThicknessProfile(componentBodyProfile);

      float openSignalSearchLocationAsFractionOfBodyLength = (Float)padOneJointInspectionData.getSubtype().
                                                             getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET) * 0.01f;
      float openSignalSpacingAsFractionOfPadLength = (Float)padOneJointInspectionData.getSubtype().
                                                     getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET) * 0.01f;

      getClearChipFilletThicknesses(clearChipProfileInfo,
                                    padOneJointInspectionData,
                                    padTwoJointInspectionData,
                                    padOneFilletEdgeIndexRef.getValue(),
                                    padTwoFilletEdgeIndexRef.getValue(),
                                    openSignalSearchLocationAsFractionOfBodyLength,
                                    openSignalSpacingAsFractionOfPadLength);

      Pad padOne = padOneJointInspectionData.getPad();
      Pad padTwo = padTwoJointInspectionData.getPad();
      boolean goodJoint = true;
      boolean populatedBoard = true;
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOne,
                                                                         sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS,
                                                                         MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletOneThicknessInMils()), populatedBoard, goodJoint));
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOne,
                                                                         sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS,
                                                                         MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletOneLowerRegionThicknessInMils()), populatedBoard,
                                                                         goodJoint));
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padTwo,
                                                                         sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS,
                                                                         MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletTwoThicknessInMils()), populatedBoard, goodJoint));
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padTwo,
                                                                         sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS,
                                                                         MathUtil.convertMilsToMillimeters(clearChipProfileInfo.getFilletTwoLowerRegionThicknessInMils()), populatedBoard,
                                                                         goodJoint));
    }
    // get back all measurements from database
    float[] filletThicknesses = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS,
                                                                         true, // populatedBoard,
                                                                         true); //goodJoint);
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    float expectedFilletThickness = AlgorithmUtil.medianWithExcludeOutlierDecision(filletThicknesses, false);
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS,
                            expectedFilletThickness);
  }
  /**
   * @author Peter Esbensen
   */
  private void learnOpaqueFilletThickness(
      Set<Pair<JointInspectionData, ChipLearningData>> typicalPadOneJointInspectionDataAndLearnedDataSet,
      Subtype subtype, final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(typicalPadOneJointInspectionDataAndLearnedDataSet != null);
    Assert.expect(subtype != null);

    if (typicalPadOneJointInspectionDataAndLearnedDataSet.size() < 1)
      return;

    SliceNameEnum sliceNameEnum = null;

    Iterator<Pair<JointInspectionData, ChipLearningData>> setIt = typicalPadOneJointInspectionDataAndLearnedDataSet.iterator();

    while (setIt.hasNext())
    {
      Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedDataPair = setIt.next();

      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedDataPair.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
                                                      getPadTwoJointInspectionDataOnTwoPinDevices();

      float[] componentBodyProfile = null;
      float[] componentDerivativeProfile = null;
      ChipLearningData ChipLearningData = jointInspectionDataAndLearnedDataPair.getSecond();

      // Wei Chin (Tall Cap)
//      if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//        sliceNameEnum = SliceNameEnum.PAD;
//      else
        sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      componentBodyProfile = ChipLearningData.getComponentBodyThicknessProfile(sliceNameEnum);
      componentDerivativeProfile = ChipLearningData.getComponentDerivativeProfile(sliceNameEnum);

      FloatRef padOneFilletEdgeIndexRef = new FloatRef();
      FloatRef padTwoFilletEdgeIndexRef = new FloatRef();

      float nominalBodyLengthInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH);
      int nominalBodyLengthInPixels = Math.round((nominalBodyLengthInMillimeters) / MILIMETER_PER_PIXEL);

      measureFilletEdgeLocations(componentDerivativeProfile, componentBodyProfile,
                             nominalBodyLengthInPixels, padOneJointInspectionData,
                             padTwoJointInspectionData, false,
                             padOneFilletEdgeIndexRef, padTwoFilletEdgeIndexRef,
                             MILIMETER_PER_PIXEL);

      String edgeLocationTechniqueOpaque = (String)padOneJointInspectionData.getSubtype().
                                                        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE);
      float padOneFilletThicknessInMils;
      float padTwoFilletThicknessInMils;
      if (edgeLocationTechniqueOpaque.equals("Max Slope"))
      {
        padOneFilletThicknessInMils = ChipAlgorithmUtil.measureFilletThicknessInMils(padOneJointInspectionData, componentBodyProfile,
                                                                                     Math.round(padOneFilletEdgeIndexRef.getValue()),
                                                                                     true);//Lim, Lay Ngor - XCR-2027 Exclude Outlier
        padTwoFilletThicknessInMils = ChipAlgorithmUtil.measureFilletThicknessInMils(padTwoJointInspectionData, componentBodyProfile,
                                                                                   Math.round(padTwoFilletEdgeIndexRef.getValue()),
                                                                                   true);//Lim, Lay Ngor - XCR-2027 Exclude Outlier
      }
      else
      {
        float percentageOfPad = (Float)padOneJointInspectionData.getSubtype().
                                                          getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_PAD_PERCENT_TO_MEASURE_FILLET_THICKNESS);
        padOneFilletThicknessInMils = ChipAlgorithmUtil.measurePartialFilletThicknessInMilsToRight(padOneJointInspectionData, componentBodyProfile,
                                                                   Math.round(padOneFilletEdgeIndexRef.getValue()), percentageOfPad,
                                                                   true);//Lim, Lay Ngor - XCR-2027 Exclude Outlier
        padTwoFilletThicknessInMils = ChipAlgorithmUtil.measurePartialFilletThicknessInMilsToLeft(padTwoJointInspectionData, componentBodyProfile,
                                                                 Math.round(padTwoFilletEdgeIndexRef.getValue()), percentageOfPad,
                                                                 true);//Lim, Lay Ngor - XCR-2027 Exclude Outlier
      }


      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOneJointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS,
                                                                         MathUtil.convertMilsToMillimeters(padOneFilletThicknessInMils),
                                                                         true, // populatedBoard,
                                                                         true //goodJoint,
                                                                         ));

      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padTwoJointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS,
                                                                         MathUtil.convertMilsToMillimeters(padTwoFilletThicknessInMils),
                                                                         true, // populatedBoard,
                                                                         true //goodJoint
                                                                         ));
    }
    float expectedFilletThicknessInMillimeters = 0.0f;

    // get back all measurements from database
    float[] filletThicknesses = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS,
                                                                         true, // populatedBoard,
                                                                         true); //goodJoint);
                                                                         
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    expectedFilletThicknessInMillimeters = AlgorithmUtil.medianWithExcludeOutlierDecision(filletThicknesses, false);
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS,
                            expectedFilletThicknessInMillimeters);
  }


   /**
   * @author Anthony Fong
   */
  private void learnPadsNominalShiftAlong(
      Set<Pair<JointInspectionData, ChipLearningData>> typicalPadOneJointInspectionDataAndLearnedDataSet,
      Subtype subtype, boolean testAsClear, final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(typicalPadOneJointInspectionDataAndLearnedDataSet != null);
    Assert.expect(subtype != null);

    if (typicalPadOneJointInspectionDataAndLearnedDataSet.size() < 1)
      return;

    SliceNameEnum sliceNameEnum = null;

    Iterator<Pair<JointInspectionData, ChipLearningData>> setIt = typicalPadOneJointInspectionDataAndLearnedDataSet.iterator();

    while (setIt.hasNext())
    {
      Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedDataPair = setIt.next();

      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedDataPair.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
                                                      getPadTwoJointInspectionDataOnTwoPinDevices();

      float[] componentBodyProfile = null;
      float[] componentDerivativeProfile = null;

      ChipLearningData ChipLearningData = jointInspectionDataAndLearnedDataPair.getSecond();

      // Wei Chin (Tall Cap)
//      if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//        sliceNameEnum = SliceNameEnum.PAD;
//      else if(!testAsClear)
      if(!testAsClear)
      {
         sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
      }
      else
      {
         sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
      }

      componentBodyProfile = ChipLearningData.getComponentBodyThicknessProfile(sliceNameEnum);
      componentDerivativeProfile = ChipLearningData.getComponentDerivativeProfile(sliceNameEnum);
      RegionOfInterest componentRegionOfInterest = ChipLearningData.getComponentRegionOfInterest(sliceNameEnum);

      FloatRef padOneFilletEdgeIndexRef = new FloatRef();
      FloatRef padTwoFilletEdgeIndexRef = new FloatRef();

      // get the nominal body length
      float nominalBodyLengthInMillimeters = getNominalBodyLengthInMillimeters(subtype, testAsClear);
      int nominalBodyLengthInPixels = Math.round((nominalBodyLengthInMillimeters) / MILIMETER_PER_PIXEL);

      measureFilletEdgeLocations(componentDerivativeProfile, componentBodyProfile,
                             nominalBodyLengthInPixels, padOneJointInspectionData,
                             padTwoJointInspectionData, testAsClear,
                             padOneFilletEdgeIndexRef, padTwoFilletEdgeIndexRef,
                             MILIMETER_PER_PIXEL);

        float pad1FilletIndex = padOneFilletEdgeIndexRef.getValue();
        float pad2FilletIndex = padTwoFilletEdgeIndexRef.getValue();
        float pad1FilletOffsetAlongFromCenterInMils = 0.0f;
        float pad2FilletOffsetAlongFromCenterInMils = 0.0f;
//        double pad1FilletOffsetAlongFromCenter = 0.0f;
//        double pad2FilletOffsetAlongFromCenter = 0.0f;
        double nominalRotationInDegrees = 0.0f;
        double nominalPadHeightInPixels = 0.0f;

        FloatRef pad1AlongOffsetInMils = new FloatRef();
        FloatRef pad2AlongOffsetInMils = new FloatRef();

        nominalBodyLengthInPixels = componentRegionOfInterest.getLengthAlong();
        // Will not use returned value here
//        float componentMisalignmentAlongInPercentage =
        getPadFilletDistanceFromInnerEdgeAlong(padOneJointInspectionData,
                                               padTwoJointInspectionData,
                                               pad1FilletIndex,
                                               pad2FilletIndex,
                                               pad1AlongOffsetInMils,
                                               pad2AlongOffsetInMils,
                                               componentRegionOfInterest
                                              );
      // Changed from mils to percentage requested by Jeremy P. - 27-Jan-2010 - by Lim, Seng Yew
      pad1FilletOffsetAlongFromCenterInMils = (float)(100*pad1AlongOffsetInMils.getValue())/padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();
      pad2FilletOffsetAlongFromCenterInMils = (float)(100*pad2AlongOffsetInMils.getValue())/padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAlong();

      //Calculate Maximum Rotation Angle
      nominalPadHeightInPixels = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().getLengthAcross();
      nominalRotationInDegrees =  Math.toDegrees(Math.atan( nominalPadHeightInPixels /(float) nominalBodyLengthInPixels ));

      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOneJointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE,
                                                                         pad1FilletOffsetAlongFromCenterInMils,
                                                                         true, // populatedBoard,
                                                                         true //goodJoint,
                                                                         ));

      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padTwoJointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE,
                                                                         pad2FilletOffsetAlongFromCenterInMils,
                                                                         true, // populatedBoard,
                                                                         true //goodJoint
                                                                         ));
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOneJointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES,
                                                                         (float)nominalRotationInDegrees,
                                                                         true, // populatedBoard,
                                                                         true //goodJoint
                                                                         ));
    }
    float expectedpadFilletOffsetAlongInMils = 0.0f;
    float expectedShiftRotationInDegrees = 0.0f;

    // get back all measurements from database
    float[] pad1FilletOffsetAlong = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE,
                                                                         true, // populatedBoard,
                                                                         true); //goodJoint);
    float[] pad2FilletOffsetAlong = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE,
                                                                         true, // populatedBoard,
                                                                         true); //goodJoint);
    float[] ShiftRotation = subtype.getLearnedJointMeasurementValues(sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES,
                                                                         true, // populatedBoard,
                                                                         true); //goodJoint);

    final int newSize = pad1FilletOffsetAlong.length + pad2FilletOffsetAlong.length;
    float[] allPadFilletOffsetAlong = new float[newSize];
    System.arraycopy(pad1FilletOffsetAlong, 0, allPadFilletOffsetAlong, 0, pad1FilletOffsetAlong.length);
    System.arraycopy(pad2FilletOffsetAlong, 0, allPadFilletOffsetAlong, pad1FilletOffsetAlong.length, pad2FilletOffsetAlong.length);
//    System.out.print("testAsClear," + (testAsClear==true ? "true" : "false") + "\n");
//    System.out.print("allPadFilletOffsetAlong," + allPadFilletOffsetAlong.length + "\n");
//    for (int i = 0; i < allPadFilletOffsetAlong.length; ++i)
//    {
//      System.out.print(i + "," + allPadFilletOffsetAlong[i] + "\n");
//    }
    
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    expectedpadFilletOffsetAlongInMils = AlgorithmUtil.medianWithExcludeOutlierDecision(allPadFilletOffsetAlong, false);
    //Lim, Lay Ngor - XCR-2027 Exclude Outlier
    expectedShiftRotationInDegrees = AlgorithmUtil.medianWithExcludeOutlierDecision(ShiftRotation, false);
    if(testAsClear)
    {
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_CLEAR,
                              expectedpadFilletOffsetAlongInMils);
    }
    else
    {
      subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_ALONG_SHIFT_OPAQUE,
                              expectedpadFilletOffsetAlongInMils);
    }
    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION,
                            expectedShiftRotationInDegrees);
  }

  /**
   * @author Peter Esbensen
   */
  private static SliceNameEnum getSliceNameEnumForPadSlice(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    SliceNameEnum sliceNameEnum = null;
    if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
      sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
    else if (jointTypeEnum.equals(JointTypeEnum.POLARIZED_CAP))
            // Wei Chin (Tall Cap)
//             || jointTypeEnum.equals(JointTypeEnum.TALL_CAPACITOR))
      sliceNameEnum = SliceNameEnum.PAD;
    else if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
      sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
    else
      Assert.expect(false);
    return sliceNameEnum;
  }

  /**
   * @author Peter Esbensen
   */
  void getClearChipPadOneJointInspectionDataAndLearnedDataSet(
      Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataAndLearnedDataSet,
      Set<Pair<JointInspectionData, ChipLearningData>> clearChipPadOneJointInspectionDataAndLearnedDataSet,
      Set<Pair<JointInspectionData, ChipLearningData>> opaqueChipPadOneJointInspectionDataAndLearnedDataSet)
  {
    Assert.expect(padOneJointInspectionDataAndLearnedDataSet != null);

    // iterate through all the components
    for (Pair<JointInspectionData, ChipLearningData> padOneJointInspectionDataAndLearnedDataPair :
         padOneJointInspectionDataAndLearnedDataSet)
    {
      JointInspectionData padOneJointInspectionData = padOneJointInspectionDataAndLearnedDataPair.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().getPadTwoJointInspectionDataOnTwoPinDevices();
      ChipLearningData ChipLearningData = padOneJointInspectionDataAndLearnedDataPair.getSecond();

      float[] componentBodyProfile;
//      if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//      {
//        componentBodyProfile = ChipLearningData.getComponentBodyThicknessProfile(SliceNameEnum.PAD);
//      }
//      else
//      {
        componentBodyProfile = ChipLearningData.getComponentBodyThicknessProfile(SliceNameEnum.CLEAR_CHIP_PAD);
//      }
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier 
      //This value is use for checking is opaque or clear chip only, so don't need to exclude outlier....
      float bodyThicknessInMillimeters = ChipAlgorithmUtil.measureBodyThicknessInMillimeters(componentBodyProfile,
                                                                                             padOneJointInspectionData,
                                                                                             padTwoJointInspectionData,
                                                                                             false);

      Subtype subtype = padOneJointInspectionData.getSubtype();

//      float bodyThicknessRequiredToTestAsOpaqueInMillimeters = _MILLIMETERS_THICKNESS_REQUIRED_TO_BE_CONSIDERED_AS_OPAQUE_DURING_LEARNING;
//      if (subtype.
//          isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE)
//          == false)
//      {
//        bodyThicknessRequiredToTestAsOpaqueInMillimeters = (Float)subtype.
//                                                           getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);
//      }

      float bodyThicknessRequiredToTestAsOpaqueInMillimeters = (Float)subtype.
                                                                getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);

      // let's guess whether or not the component is clear or opaque based on the body thickness.  (except in the case of resistor, just do clear and ignore opaque)
      if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.RESISTOR) ||
          MathUtil.fuzzyLessThan(bodyThicknessInMillimeters, bodyThicknessRequiredToTestAsOpaqueInMillimeters)
          )
      {
//        if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//          opaqueChipPadOneJointInspectionDataAndLearnedDataSet.add(new Pair<JointInspectionData, ChipLearningData>(padOneJointInspectionData, ChipLearningData));
//        else
          clearChipPadOneJointInspectionDataAndLearnedDataSet.add(new Pair<JointInspectionData, ChipLearningData>(padOneJointInspectionData, ChipLearningData));
      }
      else
      {
        opaqueChipPadOneJointInspectionDataAndLearnedDataSet.add(new Pair<JointInspectionData, ChipLearningData>(padOneJointInspectionData, ChipLearningData));
      }
    }
  }

  /**
   * Find the open signal location and offset that gives the best separation between
   * good and bad.  Basically, we just do a brute force search to see which combination of upper and lower fillet
   * offsets give us the best results.  Note that, if the user has specified a search distance, that is used as well.
   *
   * @author Peter Esbensen
   */
  private void learnFilletOffsets(
      Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataToChipLearningDataSet,
      Set<Pair<JointInspectionData, ChipLearningData>> unloadedPadOneJointInspectionDataToChipLearningDataSet,
      Subtype subtype,
      final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(padOneJointInspectionDataToChipLearningDataSet != null);
    Assert.expect(unloadedPadOneJointInspectionDataToChipLearningDataSet != null);
    Assert.expect(subtype != null);

    if (padOneJointInspectionDataToChipLearningDataSet.size() < 1)
      return;

    float nominalComponentBodyLengthInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH);
    float nominalComponentBodyLengthInPixels = nominalComponentBodyLengthInMillimeters / MILIMETER_PER_PIXEL;

    float bestOpenSignalSeparation = -99999f;
    float bestUpperFilletOffsetAsFractionOfPad = 0.0f;
    float bestLowerFilletOffsetAsFractionOfPad = 0.0f;

    final float minUpperFilletOffsetnAsFractionOfPad = -0.5f;
    final float maxUpperFilletOffsetAsFractionOfPad = 0.5f;
    final float upperFilletSearchIncrementAsFractionOfPad = 0.025f;
    final float minLowerFilletOffsetAsFractionOfPad = -0.5f;
    final float maxLowerFilletOffsetAsFractionOfPad = 0.5f;
    final float lowerFilletOffsetSearchIncrementAsFractionOfPad = 0.025f;

    BooleanRef filletLocationsAreValid = new BooleanRef(true);

    // iterate over the various offsets to see which give us the best separation
    for (float upperFilletOffsetAsFractionOfPad = minUpperFilletOffsetnAsFractionOfPad; upperFilletOffsetAsFractionOfPad < maxUpperFilletOffsetAsFractionOfPad;
                                                  upperFilletOffsetAsFractionOfPad += upperFilletSearchIncrementAsFractionOfPad)
    {
      for (float lowerFilletOffsetAsFractionOfPad = minLowerFilletOffsetAsFractionOfPad; lowerFilletOffsetAsFractionOfPad < maxLowerFilletOffsetAsFractionOfPad; lowerFilletOffsetAsFractionOfPad += lowerFilletOffsetSearchIncrementAsFractionOfPad)
      {
        if (MathUtil.fuzzyEquals(lowerFilletOffsetAsFractionOfPad, upperFilletOffsetAsFractionOfPad, 0.01) == false)  // an difference of zero gives a meaningless open signal, so don't bother with it
        {
          if (lowerFilletOffsetAsFractionOfPad > upperFilletOffsetAsFractionOfPad) // Kathy has requested that the lower fillet always be placed to the inside of the upper fillet for ease-of-training, even if that doesn't give you the best separation(!)
          {
            filletLocationsAreValid.setValue(true);
            float estimatedOpenSignalSeparation = estimateOpenSignalSeparation(padOneJointInspectionDataToChipLearningDataSet,
                                                                               unloadedPadOneJointInspectionDataToChipLearningDataSet,
                                                                               nominalComponentBodyLengthInPixels,
                                                                               subtype, upperFilletOffsetAsFractionOfPad, lowerFilletOffsetAsFractionOfPad,
                                                                               filletLocationsAreValid, MILIMETER_PER_PIXEL);
            if (filletLocationsAreValid.getValue() == true)
            {
              if (estimatedOpenSignalSeparation > bestOpenSignalSeparation)
              {
                bestOpenSignalSeparation = estimatedOpenSignalSeparation;
                bestUpperFilletOffsetAsFractionOfPad = upperFilletOffsetAsFractionOfPad;
                bestLowerFilletOffsetAsFractionOfPad = lowerFilletOffsetAsFractionOfPad;
              }
            }
          }
        }
      }
    }

    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_LOWER_FILLET_LOCATION_OFFSET,
                            100.f * bestLowerFilletOffsetAsFractionOfPad);

    subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_UPPER_FILLET_LOCATION_OFFSET,
                            100.0f * bestUpperFilletOffsetAsFractionOfPad);
  }

  /**
   * @author Peter Esbensen
   */
  private void locateClearChipEdges(JointInspectionData padOneJointInspectionData,
                                    JointInspectionData padTwoJointInspectionData,
                                    float[] componentBodyThicknessProfile,
                                    float[] componentDerivativeProfile,
                                    FloatRef padOneFilletEdgeIndex,
                                    FloatRef padTwoFilletEdgeIndex,
                                    float nominalBodyLengthInPixels,
                                    final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentBodyThicknessProfile != null);
    Assert.expect(padOneFilletEdgeIndex != null);
    Assert.expect(padTwoFilletEdgeIndex != null);
    Assert.expect(componentDerivativeProfile != null);

    boolean foundEdges = locateClearChipEdgesUsingSteepestSlopes(padOneJointInspectionData,
                                                                 padTwoJointInspectionData,
                                                                 componentBodyThicknessProfile,
                                                                 componentDerivativeProfile,
                                                                 padOneFilletEdgeIndex,
                                                                 padTwoFilletEdgeIndex,
                                                                 nominalBodyLengthInPixels);
    if (foundEdges == false)
    {
      locateClearChipEdgesUsingReflection(padOneJointInspectionData,
                                          padTwoJointInspectionData,
                                          componentBodyThicknessProfile,
                                          componentDerivativeProfile,
                                          padOneFilletEdgeIndex,
                                          padTwoFilletEdgeIndex,
                                          nominalBodyLengthInPixels,
                                          MILIMETER_PER_PIXEL);
    }
  }

  /**
   * @return true if we are confident that we found the edges
   * @author Peter Esbensen
   */
  private boolean locateClearChipEdgesUsingSteepestSlopes(JointInspectionData padOneJointInspectionData,
                                                          JointInspectionData padTwoJointInspectionData,
                                                          float[] componentBodyThicknessProfile,
                                                          float[] componentDerivativeProfile,
                                                          FloatRef padOneFilletEdgeIndex,
                                                          FloatRef padTwoFilletEdgeIndex,
                                                          float nominalBodyLengthInPixels)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentBodyThicknessProfile != null);
    Assert.expect(padOneFilletEdgeIndex != null);
    Assert.expect(padTwoFilletEdgeIndex != null);

    RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest componentRoi = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

    // find the pad one peak - we will look to the left of this for the inside edge
    int padOneLeftSideIndex = ChipAlgorithmUtil.getPadOneLeftSideIndex(componentBodyThicknessProfile, padOneJointInspectionData, padOneRoi, componentRoi);
    int padOneRightSideIndex = ChipAlgorithmUtil.getPadOneRightSideIndex(componentBodyThicknessProfile, padOneJointInspectionData, componentRoi);
    float indexOfPadOneFilletPeak = ArrayUtil.maxIndex(componentBodyThicknessProfile,
                                                       padOneLeftSideIndex,
                                                       padOneRightSideIndex);

    // find the pad two peak - we will look to the right of this for the inside edge
    int padTwoLeftSideIndex = 0;
    int padTwoRightSideIndex = ChipAlgorithmUtil.getPadTwoRightSideIndex(componentBodyThicknessProfile, padTwoJointInspectionData, padTwoRoi, componentRoi);
    float indexOfPadTwoFilletPeak = ArrayUtil.maxIndex(componentBodyThicknessProfile,
                                                       padTwoLeftSideIndex,
                                                       padTwoRightSideIndex);

    int padTwoFilletPeakIndex = Math.round(indexOfPadTwoFilletPeak);

    int bestPadTwoLocation = 0;
    float steepestCombinedSlope = Float.MAX_VALUE;
    boolean foundValidLocation = false;
    for (int padTwoSearchIndex = padTwoFilletPeakIndex; padTwoSearchIndex < padTwoRightSideIndex - _PROFILE_SMOOTHING_STEP_SIZE; ++padTwoSearchIndex)
    {
      int padOneSearchIndex = Math.round(padTwoSearchIndex + nominalBodyLengthInPixels);
      if (padOneSearchIndex > indexOfPadOneFilletPeak)
        break;
      float padOneSlope = componentDerivativeProfile[padOneSearchIndex];
      float padTwoSlope = componentDerivativeProfile[padTwoSearchIndex];
      float combinedSlopeOfPadOneAndTwo = padTwoSlope - padOneSlope;
      boolean slopesGoingRightWay = (padOneSlope > 0.0f) && (padTwoSlope < 0.0f);
      if (slopesGoingRightWay && (combinedSlopeOfPadOneAndTwo < steepestCombinedSlope))
      {
        bestPadTwoLocation = padTwoSearchIndex;
        steepestCombinedSlope = combinedSlopeOfPadOneAndTwo;
        foundValidLocation = true;
      }
    }

    if (foundValidLocation == false)
      return false;

    padOneFilletEdgeIndex.setValue(bestPadTwoLocation + nominalBodyLengthInPixels);
    padTwoFilletEdgeIndex.setValue(bestPadTwoLocation);

    return true;
  }

  /**
   * @return false if the learned thickness locations would be placed too close to the edge
   *
   * @author Peter Esbensen
   */
  private void locateEdgesAndGetOpenSignals(JointInspectionData padOneJointInspectionData,
                                            JointInspectionData padTwoJointInspectionData,
                                            float[] componentBodyThicknessProfile,
                                            float[] componentDerivativeProfile,
                                            float nominalComponentBodyLengthInPixels,
                                            float upperFilletOffsetAsFractionOfPad,
                                            float lowerFilletOffsetAsFractionOfPad,
                                            List<Float> openSignalList,
                                            BooleanRef filletLocationsValid,
                                            final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentBodyThicknessProfile != null);

    FloatRef padOneFilletEdgeIndex = new FloatRef();
    FloatRef padTwoFilletEdgeIndex = new FloatRef();

    locateClearChipEdges(padOneJointInspectionData, padTwoJointInspectionData, componentBodyThicknessProfile,
                         componentDerivativeProfile, padOneFilletEdgeIndex, padTwoFilletEdgeIndex,
                         nominalComponentBodyLengthInPixels, MILIMETER_PER_PIXEL);

    RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest componentRoi = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

    ClearChipProfileInfo clearChipProfileInfo = new ClearChipProfileInfo();
    clearChipProfileInfo.setComponentBodyThicknessProfile(componentBodyThicknessProfile);
    clearChipProfileInfo.setPadOneFilletEdgeIndex((int)padOneFilletEdgeIndex.getValue());
    clearChipProfileInfo.setPadTwoFilletEdgeIndex((int)padTwoFilletEdgeIndex.getValue());
    clearChipProfileInfo.setPadTwoRightSideIndex(ChipAlgorithmUtil.getPadTwoRightSideIndex(componentBodyThicknessProfile, padTwoJointInspectionData, padTwoRoi, componentRoi));
    clearChipProfileInfo.setPadOneLeftSideIndex(ChipAlgorithmUtil.getPadOneLeftSideIndex(componentBodyThicknessProfile, padOneJointInspectionData, padOneRoi, componentRoi));
    clearChipProfileInfo.setPadTwoLeftSideIndex(ChipAlgorithmUtil.getPadTwoLeftSideIndex(padTwoJointInspectionData, padTwoRoi));
    clearChipProfileInfo.setPadOneRightSideIndex(ChipAlgorithmUtil.getPadOneRightSideIndex(componentBodyThicknessProfile, padOneJointInspectionData, padOneRoi));
    clearChipProfileInfo.setComponentOrthogonalRegionOfInterest(padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels());

    getClearChipFilletThicknesses(clearChipProfileInfo, padOneJointInspectionData,
                                  padTwoJointInspectionData,
                                  padOneFilletEdgeIndex.getValue(),
                                  padTwoFilletEdgeIndex.getValue(),
                                  upperFilletOffsetAsFractionOfPad, lowerFilletOffsetAsFractionOfPad);

    // if the fillet locations are too close to the pad edges then set invalid flag
    final float distanceFromEdgeAsFractionOfPadLengthToBeConsideredInvalid = 0.1f;
    if (isCloseToPadOneEdge(padOneJointInspectionData,
                            componentBodyThicknessProfile,
                            clearChipProfileInfo.getPadOneUpperFilletIndex(),
                            distanceFromEdgeAsFractionOfPadLengthToBeConsideredInvalid,
                            padOneRoi, componentRoi))
      filletLocationsValid.setValue(false);
    if (isCloseToPadOneEdge(padOneJointInspectionData,
                            componentBodyThicknessProfile,
                            clearChipProfileInfo.getPadOneLowerFilletIndex(),
                            distanceFromEdgeAsFractionOfPadLengthToBeConsideredInvalid,
                            padOneRoi, componentRoi))
      filletLocationsValid.setValue(false);
    if (isCloseToPadTwoEdge(padTwoJointInspectionData,
                            componentBodyThicknessProfile,
                            clearChipProfileInfo.getPadTwoUpperFilletIndex(),
                            distanceFromEdgeAsFractionOfPadLengthToBeConsideredInvalid,
                            padTwoRoi, componentRoi))
      filletLocationsValid.setValue(false);
    if (isCloseToPadTwoEdge(padTwoJointInspectionData,
                            componentBodyThicknessProfile,
                            clearChipProfileInfo.getPadTwoLowerFilletIndex(),
                            distanceFromEdgeAsFractionOfPadLengthToBeConsideredInvalid,
                            padTwoRoi, componentRoi))
      filletLocationsValid.setValue(false);

    float padOneOpenSignal = clearChipProfileInfo.getFilletOneThicknessInMils() - clearChipProfileInfo.getFilletOneLowerRegionThicknessInMils();
    float padTwoOpenSignal = clearChipProfileInfo.getFilletTwoThicknessInMils() - clearChipProfileInfo.getFilletTwoLowerRegionThicknessInMils();

    openSignalList.add(padOneOpenSignal);
    openSignalList.add(padTwoOpenSignal);
  }

  /**
   * Return true if the given index is too close to the pad's edges
   * @author Peter Esbensen
   */
  private boolean isCloseToPadOneEdge(JointInspectionData padOneJointInspectionData,
                                      float[] componentProfile,
                                      int index,
                                      float distanceFromEdgeAsFractionOfPadLength,
                                      RegionOfInterest padRegion,
                                      RegionOfInterest componentRegion)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(componentProfile != null);
    Assert.expect(distanceFromEdgeAsFractionOfPadLength >= 0.0f);
    Assert.expect(distanceFromEdgeAsFractionOfPadLength <= 1.0f);
    Assert.expect(padRegion != null);
    Assert.expect(componentRegion != null);

    int padOneLength = padRegion.getLengthAlong();
    int rightEdgeIndex = ChipAlgorithmUtil.getPadOneRightSideIndex(componentProfile, padOneJointInspectionData, componentRegion) -
                         (int)(padOneLength * distanceFromEdgeAsFractionOfPadLength);
    int leftEdgeIndex = ChipAlgorithmUtil.getPadOneLeftSideIndex(componentProfile, padOneJointInspectionData, padRegion, componentRegion) +
                        (int)(padOneLength * distanceFromEdgeAsFractionOfPadLength);

    if (index >= rightEdgeIndex)
      return true;
    if (index <= leftEdgeIndex)
      return true;
    return false;
  }

  /**
   * @author Peter Esbensen
   */
  private boolean isCloseToPadTwoEdge(JointInspectionData padTwoJointInspectionData,
                                      float[] componentProfile,
                                      int index,
                                      float distanceFromEdgeAsFractionOfPadLength,
                                      RegionOfInterest padRegion,
                                      RegionOfInterest componentRegion)
  {
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(componentProfile != null);
    Assert.expect(distanceFromEdgeAsFractionOfPadLength >= 0.0f);
    Assert.expect(distanceFromEdgeAsFractionOfPadLength <= 1.0f);
    Assert.expect(padRegion != null);
    Assert.expect(componentRegion != null);

    int padTwoLength = padRegion.getLengthAlong();
    int rightEdgeIndex = ChipAlgorithmUtil.getPadTwoRightSideIndex(componentProfile, padTwoJointInspectionData, padRegion, componentRegion) -
                         (int)(padTwoLength * distanceFromEdgeAsFractionOfPadLength);
    int leftEdgeIndex = ChipAlgorithmUtil.getPadTwoLeftSideIndex(padTwoJointInspectionData, componentRegion) +
                        (int)(padTwoLength * distanceFromEdgeAsFractionOfPadLength);

    if (index >= rightEdgeIndex)
      return true;
    if (index <= leftEdgeIndex)
      return true;
    return false;
  }

  /**
   * Estimate the open signal separation between typical joints and bad joints.  If the user has specified an unloaded
   * panel, then use that for the bad joint open signals.  Otherwise, simulate what they'd look like.
   *
   * @author Peter Esbensen
   */
  private float estimateOpenSignalSeparation(
      Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataAndLearnedDataSet,
      Set<Pair<JointInspectionData, ChipLearningData>> unloadedPadOneJointInspectionDataAndLearnedDataSet,
      float nominalComponentBodyLengthInPixels,
      Subtype subtype, float upperFilletLocationAsFractionOfPad, float lowerFilletOffsetAsFractionOfPad,
      BooleanRef filletLocationsAreValid,
      final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(padOneJointInspectionDataAndLearnedDataSet != null);
    Assert.expect(unloadedPadOneJointInspectionDataAndLearnedDataSet != null);
    Assert.expect(subtype != null);

    List<Float> typicalOpenSignals = new LinkedList<Float>();

    int[] filletLocationValidArray = new int[padOneJointInspectionDataAndLearnedDataSet.size()];
    int numberComponentsProcessed = 0;
    for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedData : padOneJointInspectionDataAndLearnedDataSet)
    {
      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().getPadTwoJointInspectionDataOnTwoPinDevices();
      float[] componentBodyThicknessProfile = jointInspectionDataAndLearnedData.getSecond().getComponentBodyThicknessProfile(SliceNameEnum.CLEAR_CHIP_PAD);
      float[] componentDerivativeProfile = jointInspectionDataAndLearnedData.getSecond().getComponentDerivativeProfile(SliceNameEnum.CLEAR_CHIP_PAD);

      BooleanRef currentFilletLocationsAreValid = new BooleanRef(true);
      locateEdgesAndGetOpenSignals(padOneJointInspectionData, padTwoJointInspectionData,
                                   componentBodyThicknessProfile, componentDerivativeProfile,
                                   nominalComponentBodyLengthInPixels, upperFilletLocationAsFractionOfPad, lowerFilletOffsetAsFractionOfPad,
                                   typicalOpenSignals, currentFilletLocationsAreValid, MILIMETER_PER_PIXEL);
      if (currentFilletLocationsAreValid.getValue() == false)
        filletLocationValidArray[numberComponentsProcessed] = 0;
      else
        filletLocationValidArray[numberComponentsProcessed] = 1;

      numberComponentsProcessed++;
    }
    float fractionOfComponentsWithValidFilletLocations = StatisticsUtil.mean(filletLocationValidArray);
    if (fractionOfComponentsWithValidFilletLocations < 0.9f)
    {
      filletLocationsAreValid.setValue(false);
      return 0.0f;
    }
    List<Float> unloadedOpenSignals = new LinkedList<Float>();

    if (unloadedPadOneJointInspectionDataAndLearnedDataSet.size() > 1)
    {
      for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedData :
           unloadedPadOneJointInspectionDataAndLearnedDataSet)
      {
        JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getFirst();
        JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
                                                        getPadTwoJointInspectionDataOnTwoPinDevices();
        float[] componentBodyThicknessProfile = jointInspectionDataAndLearnedData.getSecond().
                                                getComponentBodyThicknessProfile(SliceNameEnum.CLEAR_CHIP_PAD);

        float[] componentDerivativeProfile = jointInspectionDataAndLearnedData.getSecond().getComponentDerivativeProfile(SliceNameEnum.CLEAR_CHIP_PAD);

        Assert.expect(filletLocationsAreValid.getValue() == true); // we should have returned early otherwise
        locateEdgesAndGetOpenSignals(padOneJointInspectionData, padTwoJointInspectionData,
                                     componentBodyThicknessProfile, componentDerivativeProfile,
                                     nominalComponentBodyLengthInPixels, upperFilletLocationAsFractionOfPad,
                                     lowerFilletOffsetAsFractionOfPad, unloadedOpenSignals, filletLocationsAreValid, MILIMETER_PER_PIXEL);
        filletLocationsAreValid.setValue(true); // don't care if open joints put the fillet edges near the edge of the pad
      }
    }
    else // create fake open joints
    {
      Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedData = padOneJointInspectionDataAndLearnedDataSet.iterator().next();

      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedData.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
                                                      getPadTwoJointInspectionDataOnTwoPinDevices();
      float[] componentBodyThicknessProfile = createFakeOpenJointProfile(padOneJointInspectionData,
                                                                         padTwoJointInspectionData, jointInspectionDataAndLearnedData.getSecond().
                                                                         getComponentBodyThicknessProfile(SliceNameEnum.CLEAR_CHIP_PAD), MILIMETER_PER_PIXEL);
      float[] componentDerivativeProfile = ProfileUtil.createDerivativeProfile(componentBodyThicknessProfile, _PROFILE_SMOOTHING_STEP_SIZE);

      Assert.expect(filletLocationsAreValid.getValue() == true); // we should have returned early otherwise
      locateEdgesAndGetOpenSignals(padOneJointInspectionData, padTwoJointInspectionData,
                                   componentBodyThicknessProfile, componentDerivativeProfile, nominalComponentBodyLengthInPixels,
                                   upperFilletLocationAsFractionOfPad, lowerFilletOffsetAsFractionOfPad,
                                   unloadedOpenSignals, filletLocationsAreValid, MILIMETER_PER_PIXEL);
      filletLocationsAreValid.setValue(true); // don't care if open joints put the fillet edges near the edge of the pad
    }

    float[] typicalOpenSignalsArray = ArrayUtil.convertFloatListToFloatArray(typicalOpenSignals);
    float[] unloadedOpenSignalsArray = ArrayUtil.convertFloatListToFloatArray(unloadedOpenSignals);

    float[] quartileRangesForTypicalOpenSignals = StatisticsUtil.quartileRanges(typicalOpenSignalsArray);
    float[] quartileRangesForUnloadedJointOpenSignals = StatisticsUtil.quartileRanges(unloadedOpenSignalsArray);
    float expectedTypicalOpenSignal = quartileRangesForTypicalOpenSignals[2];
    float spreadOfTypicalOpenSignal = quartileRangesForTypicalOpenSignals[3] - quartileRangesForTypicalOpenSignals[1];
    float expectedUnloadedJointOpenSignal = quartileRangesForUnloadedJointOpenSignals[2];

    float spreadOfUnloadedJointOpenSignal = quartileRangesForUnloadedJointOpenSignals[3] - quartileRangesForUnloadedJointOpenSignals[1];
    if (unloadedOpenSignalsArray.length < 10)
      spreadOfUnloadedJointOpenSignal = 0.0f; // not enough data, we'll just use zero spread

    float separation = (expectedTypicalOpenSignal - expectedUnloadedJointOpenSignal) /
                       (spreadOfTypicalOpenSignal + spreadOfUnloadedJointOpenSignal);

    if (expectedTypicalOpenSignal < 0.0f)
      filletLocationsAreValid.setValue(false);

//    float tmp = separation;
//    if (filletLocationsAreValid.getValue() == false)
//      tmp = 0.0f;
//    System.out.println("location offset good expected spread bad expected spread separation " + locationAsFractionOfPad + " " + offsetAsFractionOfPad
//                       + " " + expectedTypicalOpenSignal + " " + spreadOfTypicalOpenSignal + " " + expectedUnloadedJointOpenSignal +
//                       " " + spreadOfUnloadedJointOpenSignal + " " + tmp);

    return separation;
  }

  /**
   * @author Peter Esbensen
   */
  private float[] createFakeOpenJointProfile(JointInspectionData padOneJointInspectionData,
                                             JointInspectionData padTwoJointInspectionData,
                                             float[] goodJointProfile,
                                             final float MILIMETER_PER_PIXEL)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padOneJointInspectionData != null);

    float[] fakeOpenJointProfile = new float[goodJointProfile.length];

    // get good solder volume
    float padOneSolderVolume = ChipAlgorithmUtil.getPadOneSolderVolume(padOneJointInspectionData, goodJointProfile);
    float padTwoSolderVolume = ChipAlgorithmUtil.getPadTwoSolderVolume(padTwoJointInspectionData, goodJointProfile);
    RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
    RegionOfInterest componentRoi = padOneJointInspectionData.getComponentInspectionData().getOrthogonalComponentRegionOfInterest();

    float MIL_PER_PIXEL = MathUtil.convertMillimetersToMils(MILIMETER_PER_PIXEL);
    int padOneLengthInPixels = padOneRoi.getLengthAlong();
    int padTwoLengthInPixels = padTwoRoi.getLengthAlong();
    float padOneLengthInMils = padOneLengthInPixels * MIL_PER_PIXEL;
    float padTwoLengthInMils = padTwoLengthInPixels * MIL_PER_PIXEL;

    float scaleFactor = (float)(0.5 * Math.pow((padOneLengthInMils * 0.5), 2.0) * Math.PI) /
                        padOneSolderVolume; // half the circle area divided by actual solder volume
    int padOneLeftSideIndex = ChipAlgorithmUtil.getPadOneLeftSideIndex(goodJointProfile, padOneJointInspectionData, padOneRoi, componentRoi);
    int padOneRightSideIndex = ChipAlgorithmUtil.getPadOneRightSideIndex(goodJointProfile, padOneJointInspectionData, padOneRoi);
    int centerOfPad = (padOneLeftSideIndex + padOneRightSideIndex) / 2;
    for (int i = padOneLeftSideIndex; i < padOneRightSideIndex; ++i)
    {
      float distanceFromCenterInMMs = (i - centerOfPad) * MIL_PER_PIXEL;
      float fakeHeight = (float)Math.pow(Math.pow(padOneLengthInMils / 2, 2.0) - Math.pow(distanceFromCenterInMMs, 2.0), 0.5);
      fakeHeight /= scaleFactor;
      fakeOpenJointProfile[i] = fakeHeight;
    }

    scaleFactor = (float)(0.5 * Math.pow((padOneLengthInMils * 0.5), 2.0) * Math.PI) /
                  padTwoSolderVolume; // half the circle area divided by actual solder volume
    int padTwoLeftSideIndex = ChipAlgorithmUtil.getPadTwoLeftSideIndex(padTwoJointInspectionData, padTwoRoi);
    int padTwoRightSideIndex = ChipAlgorithmUtil.getPadTwoRightSideIndex(goodJointProfile, padTwoJointInspectionData, padTwoRoi, componentRoi);
    centerOfPad = (padTwoLeftSideIndex + padTwoRightSideIndex) / 2;
    for (int i = padTwoLeftSideIndex; i < padTwoRightSideIndex; ++i)
    {
      float distanceFromCenterInMMs = (i - centerOfPad) * MIL_PER_PIXEL;
      float fakeHeight = (float)Math.pow(Math.pow(padTwoLengthInMils / 2, 2.0) - Math.pow(distanceFromCenterInMMs, 2.0), 0.5);
      fakeHeight /= scaleFactor;
      fakeOpenJointProfile[i] = fakeHeight;
    }
    return fakeOpenJointProfile;
  }

  /**
   * @author Peter Esbensen & Chong, Wei Chin (14 Aug 2009 CR32137)
   */
  private void learnTestAsOpaqueAndOpaqueThicknessThreshold(
      Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataAndLearnedDataSet,
      Subtype subtype,
      boolean testAsOpaque,
      boolean testAsClear) throws DatastoreException
  {
    Assert.expect(padOneJointInspectionDataAndLearnedDataSet != null);
    Assert.expect(subtype != null);

    boolean populatedBoard = true;
    boolean goodJoint = true;

    SliceNameEnum sliceNameEnum = getSliceNameEnumForPadSlice(subtype.getJointTypeEnum());

    // iterate through all the components and try to guess body thickness
    for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedDataPair :
         padOneJointInspectionDataAndLearnedDataSet)
    {
      JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedDataPair.getFirst();
      JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
                                                      getPadTwoJointInspectionDataOnTwoPinDevices();

      ChipLearningData ChipLearningData = jointInspectionDataAndLearnedDataPair.getSecond();
      float[] componentBodyProfile = ChipLearningData.getComponentBodyThicknessProfile(sliceNameEnum);
      Assert.expect(componentBodyProfile != null);
      //Lim, Lay Ngor - XCR-2027 Exclude Outlier - this is the whole body thickness of an component!!
      //unless we want to exclude solder ball thickness(sample case)
      float componentBodyThicknessInMillimeters = ChipAlgorithmUtil.measureBodyThicknessInMillimeters(componentBodyProfile,
                                                                                                      padOneJointInspectionData,
                                                                                                      padTwoJointInspectionData,
                                                                                                      true);//Lim, Lay Ngor - XCR-2027 Exclude Outlier

      if(Config.isIntelligentLearning())
        ChipLearningData.setComponentBodyThicknessMeanValue(sliceNameEnum, componentBodyThicknessInMillimeters);

      // Save the measurement to the database.
      subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOneJointInspectionData.getPad(),
                                                                         sliceNameEnum,
                                                                         MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
                                                                         componentBodyThicknessInMillimeters,
                                                                         populatedBoard,
                                                                         goodJoint));
    }

    // ok go ahead and set the "min thickness for opaque test" setting if the user hasn't set it already
    if (subtype.isAlgorithmSettingValueEqualToDefault(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE))
    {
      //  testAsOpaque = learnTestAsOpaque(subtype);

      if (testAsOpaque & testAsClear)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier - This one already exclude outlier at body thickness for each component level
        float [] componentBodyThicknessesInMillimetersForSlice =
              subtype.getLearnedJointMeasurementValues( sliceNameEnum,
                                                        MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
                                                        populatedBoard,
                                                        goodJoint);

        //Lim, Lay Ngor - XCR-2027 Exclude Outlier
        float expectedBodyThicknessInMillimeters = AlgorithmUtil.medianWithExcludeOutlierDecision(componentBodyThicknessesInMillimetersForSlice, false);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE,
                                expectedBodyThicknessInMillimeters);
      }
      else if (testAsClear == false)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier - don need to exclude because only take minimum value
        float lowLimitForBodyThicknessRequiredForOpaqueTestSetting = (Float)subtype.getAlgorithmSettingMinimumValue(
            AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE,
                                lowLimitForBodyThicknessRequiredForOpaqueTestSetting);
      }
      else if (testAsOpaque == false)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier - don need to exclude because only take maximum value
        float highLimitForBodyThicknessRequiredForOpaqueTestSetting = (Float)subtype.getAlgorithmSettingMaximumValue(
            AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE,
                                highLimitForBodyThicknessRequiredForOpaqueTestSetting);
      }
    }
  }

  /**
   * Wei Chin : not using anymore , modify by CR 32137 adding new threshold setting to control the learning as Opaque or Clear
   * @author Peter Esbensen
   */
  static boolean learnTestAsOpaque(Subtype subtype) throws DatastoreException
  {
    Assert.expect(subtype != null);

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    if (jointTypeEnum.equals(JointTypeEnum.POLARIZED_CAP))
      return true;
    if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
      return false;

    boolean populatedBoard = true;
    boolean goodJoint = true;
    float[] componentBodyThicknessesInMilsForSlice = subtype.getLearnedJointMeasurementValues(
        getSliceNameEnumForPadSlice(jointTypeEnum),
        MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
        populatedBoard,
        goodJoint);

    float expectedBodyThicknessInMils = StatisticsUtil.median(componentBodyThicknessesInMilsForSlice);
    return (expectedBodyThicknessInMils > _MILLIMETERS_THICKNESS_REQUIRED_TO_BE_CONSIDERED_AS_OPAQUE_DURING_LEARNING);
  }

  /**
   * @author Peter Esbensen
   */
  private void learnClearAndOpaqueBodyLengthInPixels(
      Set<Pair<JointInspectionData, ChipLearningData>> clearPadOneJointInspectionDataAndLearnedDataSet,
      Set<Pair<JointInspectionData, ChipLearningData>> opaquePadOneJointInspectionDataAndLearnedDataSet,
      boolean testAsOpaque,
      boolean testAsClear,
      final float MILIMETER_PER_PIXEL) throws DatastoreException
  {
    Assert.expect(clearPadOneJointInspectionDataAndLearnedDataSet != null);
    Assert.expect(opaquePadOneJointInspectionDataAndLearnedDataSet != null);

    Subtype subtype = null;
    boolean populatedBoard = true;
    boolean goodJoint = true;

    if (clearPadOneJointInspectionDataAndLearnedDataSet.size() > 0 && testAsClear)
    {
      // iterate through all the clear components and try to guess body length
      for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedDataPair :
           clearPadOneJointInspectionDataAndLearnedDataSet)
      {
        JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedDataPair.getFirst();
        ChipLearningData ChipLearningData = jointInspectionDataAndLearnedDataPair.getSecond();
        subtype = padOneJointInspectionData.getSubtype();

        float[] componentBodyProfile = ChipLearningData.getComponentBodyThicknessProfile(SliceNameEnum.CLEAR_CHIP_PAD);
        float[] derivativeProfile = ChipLearningData.getComponentDerivativeProfile(SliceNameEnum.CLEAR_CHIP_PAD);
        float estimatedBodyLengthInPixels = estimateClearBodyLengthInPixels(padOneJointInspectionData,
                                                                    componentBodyProfile,
                                                                    derivativeProfile);
        float estimatedClearBodyLengthInMils = (float)MathUtil.convertMillimetersToMils(estimatedBodyLengthInPixels *
                                                                                       MILIMETER_PER_PIXEL);
        subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOneJointInspectionData.getPad(),
                                                                           SliceNameEnum.CLEAR_CHIP_PAD,
                                                                           MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH,
                                                                           estimatedClearBodyLengthInMils,
                                                                           populatedBoard,
                                                                           (estimatedBodyLengthInPixels >= 0)));
      }
      // set the clear body length
      float expectedClearBodyLengthInMils = 0.0f;

      float[] componentBodyLengths = subtype.getLearnedJointMeasurementValues(
          SliceNameEnum.CLEAR_CHIP_PAD,
          MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH,
          populatedBoard,
          goodJoint);
      if (componentBodyLengths.length > 0)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier
        expectedClearBodyLengthInMils = AlgorithmUtil.medianWithExcludeOutlierDecision(componentBodyLengths, false);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_BODY_LENGTH,
                                MathUtil.convertMilsToMillimeters(expectedClearBodyLengthInMils));
      }

    }

    if (opaquePadOneJointInspectionDataAndLearnedDataSet.size() > 0 && testAsOpaque)
    {
      SliceNameEnum sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;

      // iterate through all the opaque components and try to guess body length
      for (Pair<JointInspectionData, ChipLearningData> jointInspectionDataAndLearnedDataPair :
           opaquePadOneJointInspectionDataAndLearnedDataSet)
      {
        JointInspectionData padOneJointInspectionData = jointInspectionDataAndLearnedDataPair.getFirst();
        JointInspectionData padTwoJointInspectionData = padOneJointInspectionData.getComponentInspectionData().
                                                        getPadTwoJointInspectionDataOnTwoPinDevices();
        ChipLearningData ChipLearningData = jointInspectionDataAndLearnedDataPair.getSecond();
        subtype = padOneJointInspectionData.getSubtype();

        // Wei Chin (Tall Cap)
//        if(subtype.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//          sliceNameEnum = SliceNameEnum.PAD;

        float[] derivativeProfile = ChipLearningData.getComponentDerivativeProfile(sliceNameEnum);
        float estimatedBodyLengthInPixels = estimateOpaqueBodyLength(derivativeProfile, padOneJointInspectionData, padTwoJointInspectionData);
        float estimatedBodyLengthInMils = (float)MathUtil.convertMillimetersToMils(estimatedBodyLengthInPixels *
                                                                                  MILIMETER_PER_PIXEL);
       
        subtype.addLearnedJointMeasurementData(new LearnedJointMeasurement(padOneJointInspectionData.getPad(),
                                                                           sliceNameEnum,
                                                                           MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH,
                                                                           estimatedBodyLengthInMils,
                                                                           populatedBoard,
                                                                           (estimatedBodyLengthInPixels >= 0)));

      }
      // set the opaque body length
      float expectedOpaqueBodyLengthInMils = 0.0f;

      float[] componentBodyLengths = subtype.getLearnedJointMeasurementValues(
          sliceNameEnum,
          MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH,
          populatedBoard,
          goodJoint);
      if (componentBodyLengths.length > 0)
      {
        //Lim, Lay Ngor - XCR-2027 Exclude Outlier
        expectedOpaqueBodyLengthInMils = AlgorithmUtil.medianWithExcludeOutlierDecision(componentBodyLengths, false);
        subtype.setLearnedValue(subtype.getJointTypeEnum(), this, AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_BODY_LENGTH,
                                MathUtil.convertMilsToMillimeters(expectedOpaqueBodyLengthInMils));
      }
    }
  }

  /**
   * Resistors have clearly defined inner edges on each pad.  So simply learn
   * the distance between those two sharp edges by assuming they are where
   * the slopes are steepest in those inside edge areas.
   *
   * @author Peter Esbensen
   */
  float estimateClearBodyLengthInPixels(JointInspectionData padOneJointInspectionData,
                                        float[] componentBodyProfile,
                                        float[] componentDerivativeProfile)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(componentBodyProfile != null);
    Assert.expect(componentDerivativeProfile != null);

    // find the pad one peak - we will look to the left of this for the inside edge
    int padOneLengthInPixels = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().
                               getLengthAlong();
    int padOneLeftSideIndex = componentBodyProfile.length - padOneLengthInPixels;
    int padOneRightSideIndex = componentBodyProfile.length;
    float indexOfPadOneFilletPeak = ArrayUtil.maxIndex(componentBodyProfile,
                                                       padOneLeftSideIndex,
                                                       padOneRightSideIndex);

    // find the pad two peak - we will look to the right of this for the inside edge
    int padTwoLengthInPixels = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels().
                               getLengthAlong();
    int padTwoLeftSideIndex = 0;
    int padTwoRightSideIndex = padTwoLengthInPixels;
    float indexOfPadTwoFilletPeak = ArrayUtil.maxIndex(componentBodyProfile,
                                                       padTwoLeftSideIndex,
                                                       padTwoRightSideIndex);

    // find the pad one fillet edge - this is the steepest rising slope
    int maxSearchIndexOfPadOneFilletEdge = Math.min(componentBodyProfile.length, Math.round(indexOfPadOneFilletPeak));
    int minSearchIndexOfPadOneFilletEdge = padOneLeftSideIndex + _PROFILE_SMOOTHING_STEP_SIZE;
    float indexOfPadOneFilletEdge = ArrayUtil.maxIndex(componentDerivativeProfile,
                                                       minSearchIndexOfPadOneFilletEdge, // start after the pad edge
                                                       Math.max(minSearchIndexOfPadOneFilletEdge, maxSearchIndexOfPadOneFilletEdge));

    // find the pad two fillet edge - this is the steepest falling slope
    int padTwoFilletPeakIndex = Math.round(indexOfPadTwoFilletPeak);
    float indexOfPadTwoFilletEdge = ArrayUtil.minIndex(componentDerivativeProfile,
                                                       padTwoFilletPeakIndex,
                                                       Math.max(padTwoFilletPeakIndex, padTwoRightSideIndex - _PROFILE_SMOOTHING_STEP_SIZE)); // stop before we find the pad edge

    // compute the body length
    Assert.expect(indexOfPadOneFilletEdge >= indexOfPadTwoFilletEdge);
    return (indexOfPadOneFilletEdge - indexOfPadTwoFilletEdge);
  }

  /**
   * @author Peter Esbensen
   */
  float estimateOpaqueBodyLength(float[] componentDerivativeProfile,
                                 JointInspectionData padOneJointInspectionData,
                                 JointInspectionData padTwoJointInspectionData)
  {
    Assert.expect(componentDerivativeProfile != null);

    FloatRef padOneFilletEdgeIndex = new FloatRef();
    FloatRef padTwoFilletEdgeIndex = new FloatRef();
    locateOpaqueChipEdges (componentDerivativeProfile,
                          padOneJointInspectionData,
                          padTwoJointInspectionData,
                          padOneFilletEdgeIndex,
                          padTwoFilletEdgeIndex);

    // find the distance between the edges as defined by steepest slope
    float indexOfPadOneFilletEdge = padOneFilletEdgeIndex.getValue();
    float indexOfPadTwoFilletEdge = padTwoFilletEdgeIndex.getValue();
    if (indexOfPadTwoFilletEdge < indexOfPadOneFilletEdge)
    {
      return (indexOfPadOneFilletEdge - indexOfPadTwoFilletEdge);
    }
    else
      return -1.0f;
  }

  /*
   *  @author Lim, Seng Yew
   */
  void locateOpaqueChipEdges (float[] componentDerivativeProfile,
                             JointInspectionData padOneJointInspectionData,
                             JointInspectionData padTwoJointInspectionData,
                             FloatRef padOneFilletEdgeIndex,
                             FloatRef padTwoFilletEdgeIndex)
  {
      RegionOfInterest padOneRoi = padOneJointInspectionData.getOrthogonalRegionOfInterestInPixels();
      int padOneLength = padOneRoi.getLengthAlong();
      RegionOfInterest padTwoRoi = padTwoJointInspectionData.getOrthogonalRegionOfInterestInPixels();
      int padTwoLength = padTwoRoi.getLengthAlong();

      String edgeLocationTechniqueOpaque = (String)padOneJointInspectionData.getSubtype().
                                                        getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_EDGE_LOCATION_TECHNIQUE_OPAQUE);
      if (edgeLocationTechniqueOpaque.equals("Max Slope"))
      {
        // With opaque chips, we just look for the steepest slopes on the outer parts of the component/fillets area
        padOneFilletEdgeIndex.setValue(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
                                                                              componentDerivativeProfile,
                                                                              Math.round(ArrayUtil.interpolatedMinIndex(componentDerivativeProfile,
                                                                                                                        componentDerivativeProfile.length - padOneLength,
                                                                                                                        componentDerivativeProfile.length))));

        padTwoFilletEdgeIndex.setValue(ChipAlgorithmUtil.forceIndexOntoPadTwo(padOneJointInspectionData,
                                                                              componentDerivativeProfile,
                                                                              Math.round(ArrayUtil.interpolatedMaxIndex(componentDerivativeProfile,
                                                                                                                        0, padTwoLength))));
      }
      else
      {
        // get 2nd derivative profile from componentDerivativeProfile
        // after Gaussain smoothing filtering using kernelRadius=10
        float[] componentGaussianSmoothedDerivativeProfile = ProfileUtil.getGaussainSmoothedProfile(componentDerivativeProfile,10);
        float[] component2ndDerivativeProfile = ProfileUtil.createDerivativeProfile(componentGaussianSmoothedDerivativeProfile,
                                                                                 _PROFILE_SMOOTHING_STEP_SIZE);
        padOneFilletEdgeIndex.setValue(ChipAlgorithmUtil.forceIndexOntoPadOne(padOneJointInspectionData,
                                                                              componentDerivativeProfile,
                                                                              Math.round(ArrayUtil.interpolatedMinIndex(component2ndDerivativeProfile,
                                                                                                                        componentDerivativeProfile.length - padOneLength,
                                                                                                                        componentDerivativeProfile.length))));

        padTwoFilletEdgeIndex.setValue(ChipAlgorithmUtil.forceIndexOntoPadTwo(padOneJointInspectionData,
                                                                              componentDerivativeProfile,
                                                                              Math.round(ArrayUtil.interpolatedMinIndex(component2ndDerivativeProfile,
                                                                                                                        0,padTwoLength))));
      }
  }

  /**
   * @author Peter Esbensen
   */
  private Set<Pair<JointInspectionData, ChipLearningData>> getPadOneJointInspectionDataAndLearnedDataSet(
      Subtype subtype,
      ManagedOfflineImageSet typicalBoardImages) throws DatastoreException
  {
    Assert.expect(subtype != null);
    Assert.expect(typicalBoardImages != null);

    final int NUMBER_OF_SAMPLES_REQUIRED = 100;

    Set<Pair<JointInspectionData, ChipLearningData>> padOneJointInspectionDataAndLearnedDataSet =
        new HashSet<Pair<JointInspectionData, ChipLearningData>>();

    // iterate through all joints in images and collect profiles
    ManagedOfflineImageSetIterator imagesIterator = typicalBoardImages.iterator();
    int numberOfImages = typicalBoardImages.size();
    int samplingFrequency = Math.max(1, numberOfImages / NUMBER_OF_SAMPLES_REQUIRED);
    imagesIterator.setSamplingFrequency(samplingFrequency);
    ReconstructedImages reconstructedImages = null;
    while ((reconstructedImages = imagesIterator.getNext()) != null)
    {
      ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();
      List<JointInspectionData> jointInspectionDataList = reconstructionRegion.getInspectableJointInspectionDataList(subtype);

      for (ComponentInspectionData componentInspectionData : AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataList))
      {
        boolean validComponent = true;
        if (ChipAlgorithmUtil.landPatternIsTestable(componentInspectionData) == false)
        {
          AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_CHIP_WARNING_BAD_LAND_PATTERN_KEY",
                                                                  new String[]
                                                                  {componentInspectionData.getComponent().getReferenceDesignator()}));
          validComponent = false;
        }
        else
        {
          JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();

          ChipLearningData ChipLearningData = new ChipLearningData();

          Locator.locateRectangularComponent(reconstructedImages,
                                             subtype.getInspectionFamily().getSliceNameEnumForLocator(subtype),
                                             componentInspectionData, this, false);

          for (ReconstructedSlice reconstructedSlice : reconstructedImages.getReconstructedSlices())
          {
            SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

            // get the component region
            RegionOfInterest componentRegionOfInterest = Locator.getRegionOfInterestAtMeasuredLocation(componentInspectionData);

            // extend profile so that we can be sure to catch all of the fillets
            componentRegionOfInterest.scaleFromCenterAlongAcross(1.0 + _PROFILE_EXTENSION, 1.0);

            // Ensure the component ROI is constrained to the image boundaries.
            Image image = reconstructedSlice.getOrthogonalImage();
            if (ChipAlgorithmUtil.regionFits(image, componentRegionOfInterest, componentInspectionData) == false)
            {
              // the region does not fit in the image at all

              AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_NOT_INSPECTING_WARNING_KEY",
                                                                      new String[]
                                                                      {componentInspectionData.getComponent().getReferenceDesignator()}));
              validComponent = false;
              continue;
            }


            if (AlgorithmUtil.checkRegionBoundaries(componentRegionOfInterest, image, reconstructionRegion, "component region of interest") == false)
            {
              // Shift the ROI back into the image.
              AlgorithmUtil.shiftRegionIntoImageIfNecessary(image, componentRegionOfInterest);
            }

            // does the region fit in the image at all?
            if (ChipAlgorithmUtil.regionFits(reconstructedSlice.getOrthogonalImage(), componentRegionOfInterest,
                                             componentInspectionData) == false)
            {
              AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_REGION_DOES_NOT_FIT_WITHIN_IMAGE_NOT_LEARNING_WARNING_KEY",
                                                                  new String[]
                                                                  {componentInspectionData.getComponent().getReferenceDesignator()}));
              continue;
            }

            float[] componentThicknessProfile = ChipAlgorithmUtil.getComponentThicknessProfile(reconstructedSlice,
                                                                                               reconstructionRegion,
                                                                                               componentRegionOfInterest,
                                                                                               padOneJointInspectionData,
                                                                                               this,
                                                                                               false);
            
            float[] componentDerivativeProfile = ProfileUtil.createDerivativeProfile(componentThicknessProfile, _PROFILE_SMOOTHING_STEP_SIZE);

            ChipLearningData.setComponentBodyThicknessProfile(sliceNameEnum, componentThicknessProfile);
            ChipLearningData.setComponentDerivativeProfile(sliceNameEnum, componentDerivativeProfile);
            ChipLearningData.setComponentRegionOfInterest(sliceNameEnum, componentRegionOfInterest);
          }
          if (validComponent)
          {
            Assert.expect(padOneJointInspectionData != null);
            padOneJointInspectionDataAndLearnedDataSet.add(
                new Pair<JointInspectionData, ChipLearningData>(padOneJointInspectionData, ChipLearningData));
          }
        }
      }
      imagesIterator.finishedWithCurrentRegion();
    }
    return padOneJointInspectionDataAndLearnedDataSet;
  }

  /**
   * @author Peter Esbensen
   */
  public void setLearnedSettingsToDefaults(Subtype subtype)
  {
    Locator.setLearnedSettingsToDefaults(subtype);

    for (AlgorithmSettingEnum algorithmSettingEnum : getLearnedAlgorithmSettingEnums())
    {
      subtype.setSettingToDefaultValue(algorithmSettingEnum);
    }
  }

  /**
   * @author Peter Esbensen
   */
  public static ComponentMeasurement getSharpnessAlongProfileMeasurement(final ComponentInspectionData componentInspectionData,
                                                       final SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    ComponentMeasurement measurement = componentInspectionData.getComponentInspectionResult().
                                       getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PROFILE_SHARPNESS_ALONG);
    return measurement;
  }

  /**
   * @author Peter Esbensen
   */
  public static ComponentMeasurement getOverallSharpnessMeasurement(final ComponentInspectionData componentInspectionData,
                                                  final SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    ComponentMeasurement measurement = componentInspectionData.getComponentInspectionResult().
                                       getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OVERALL_SHARPNESS);
    return measurement;
  }

  /**
   * @author Peter Esbensen
   */
  static ComponentMeasurement getBodyWidthMeasurement(final ComponentInspectionData componentInspectionData,
                                             final SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    ComponentMeasurement measurement = componentInspectionData.getComponentInspectionResult().
                                       getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH);
    return measurement;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  static JointMeasurement getBodyWidthMeasurement(final JointInspectionData jointInspectionData,
                                             final SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointMeasurement measurement = jointInspectionData.getJointInspectionResult().
                                       getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH);
    return measurement;
  }

  /**
   * @author Peter Esbensen
   */
  public static ComponentMeasurement getComponentBodyThicknessInMillimetersMeasurement(
      ComponentInspectionData componentInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    ComponentMeasurement measurement = componentInspectionData.getComponentInspectionResult().
                                       getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS);
    Assert.expect(measurement.getMeasurementUnitsEnum().equals(MeasurementUnitsEnum.MILLIMETERS));
    return measurement;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getComponentBodyThicknessInMillimetersMeasurement(
      JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointMeasurement measurement = jointInspectionData.getJointInspectionResult().
                                       getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS);
    Assert.expect(measurement.getMeasurementUnitsEnum().equals(MeasurementUnitsEnum.MILLIMETERS));
    return measurement;
  }

  /**
   * @author Peter Esbensen
   */
  public static ComponentMeasurement getComponentBodyLengthAsPercentOfNominalMeasurement(ComponentInspectionData componentInspectionData,
                                                                       SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getComponentBodyLengthAsPercentOfNominalMeasurement(JointInspectionData jointInspectionData,
                                                                        SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT);
  }

  /**
   * @author Peter Esbensen
   */
  public static ComponentMeasurement getComponentBodyLengthInMillimetersMeasurement(ComponentInspectionData componentInspectionData,
                                                                  SliceNameEnum sliceNameEnum,
                                                                  boolean isOpaque)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    MeasurementEnum measurementEnum = MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH;
    if (isOpaque)
      measurementEnum = MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH;
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, measurementEnum);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getComponentBodyLengthInMillimetersMeasurement(JointInspectionData jointInspectionData,
                                                                          SliceNameEnum sliceNameEnum,
                                                                          boolean isOpaque)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
     MeasurementEnum measurementEnum = MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH;
     if (isOpaque)
      measurementEnum = MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH;
     return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, measurementEnum);
  }

  /**
   * @author Peter Esbensen
   */
  public static ComponentMeasurement getOpaqueOpenSignalMeasurement(ComponentInspectionData componentJointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentJointInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getOpaqueOpenSignalMeasurement(JointInspectionData jointJointInspectionData,
                                                    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointJointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL);
  }

  /**
   * @author Peter Esbensen
   */
  public static JointMeasurement getLowerFilletThicknessMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS);
  }

  /**
   * @author Peter Esbensen
   */
  public static JointMeasurement getOpaqueFilletThicknessInMillimetersMeasurement(JointInspectionData jointInspectionData,
                                                                SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS);
  }

  /**
   * @author Peter Esbensen
   */
  public static JointMeasurement getClearFilletThicknessInMillimetersMeasurement(JointInspectionData jointInspectionData,
                                                               SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS);
  }

  /**
   * @author Peter Esbensen
   */
  public static JointMeasurement getFilletGapMeasurement(JointInspectionData jointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_FILLET_GAP);
  }

  // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
   /**
   * @author Anthony Fong
   */
  public static ComponentMeasurement getComponentMisalignmentAlongInPercentageMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getComponentMisalignmentAlongInPercentageMeasurement(JointInspectionData jointInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);
  }

  public static JointMeasurement getPin1AlongOffsetInPercentageMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    return padOneJointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE);
  }

  public static JointMeasurement getPin2AlongOffsetInPercentageMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    return padTwoJointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE);
  }

  public static JointMeasurement getPin1MisalignmentAlongDeltaInPercentageMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    return padOneJointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
  }

  public static JointMeasurement getPin2MisalignmentAlongDeltaInPercentageMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    return padTwoJointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
  }

  public static ComponentMeasurement getNominalRotationInDegreesMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getNominalRotationInDegreesMeasurement(JointInspectionData jointInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES);
  }

   // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
   /**
   * @author Anthony Fong
   */
  public static ComponentMeasurement getComponentMisalignmentAcrossInPercentageMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);
   }
  
  /**
   * @author Cheah Lee Herng
   */
  public static JointMeasurement getComponentMisalignmentAcrossInPercentageMeasurement(JointInspectionData jointInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);
  }

  /**
   * @author Peter Esbensen
   */
  public static ComponentMeasurement getComponentRotationInDegreesMeasurement(ComponentInspectionData componentInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getComponentRotationInDegreesMeasurement(JointInspectionData jointInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
  }

  /**
   * @author Lim, Seng-Yew on 13-03-2009 Friday the 13th
   */
  public static ComponentMeasurement getOpaqueComponentTiltRatioMeasurement(ComponentInspectionData componentJointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return componentJointInspectionData.getComponentInspectionResult().
        getComponentMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public static JointMeasurement getOpaqueComponentTiltRatioMeasurement(JointInspectionData jointJointInspectionData,
                                                  SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointJointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointJointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO);
  }

//Lim Lay Ngor - Tombstone start
  public static JointMeasurement getCenterToUpperFilletThicknessPercentMeasurement(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
  }
  
  public static JointMeasurement getPin1CenterToUpperFilletThicknessPercentMeasurement(ComponentInspectionData componentInspectionData,
    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
    return padOneJointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
  }

  public static JointMeasurement getPin2CenterToUpperFilletThicknessPercentMeasurement(ComponentInspectionData componentInspectionData,
    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(sliceNameEnum != null);

    JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();
    return padTwoJointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
  }
  
  public static JointMeasurement getPadSolderAreaPercentMeasurement(JointInspectionData jointInspectionData,
    SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE);
  }
//Lim Lay Ngor - Tombstone end

  /**
   * OKI tombstone
   * @author sheng-chuan
   */
  public static JointMeasurement getMatchingPercentagesMeasurement(JointInspectionData jointInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING);
   }
  
  /**
   * OKI tombstone
   * @author sheng-chuan
   */
  public static JointMeasurement getRoundnessPercentagesMeasurement(JointInspectionData jointInspectionData,
                                                                              SliceNameEnum sliceNameEnum)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    return jointInspectionData.getJointInspectionResult().getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT);
   }

  /**
   * @author Peter Esbensen
   */
  static public boolean testAsOpaque(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);

    // if this is a Capacitor, then determine if this should be tested as clear or as opaque
    if (jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR))
    {
      ComponentInspectionData componentInspectionData = jointInspectionData.getComponentInspectionData();
      if (measurementsExist(componentInspectionData))
      {
        ComponentMeasurement componentBodyThicknessMeasurementInMillimeters = null;
        JointMeasurement jointComponentBodyThicknessMeasurementInMilimeters = null;
        float bodyThicknessMeasurementInMilimetersFloatValue = 0.0f;
        
        if (Config.isComponentLevelClassificationEnabled())
        {
          componentBodyThicknessMeasurementInMillimeters = getComponentBodyThicknessInMillimetersMeasurement(componentInspectionData, 
                  SliceNameEnum.CLEAR_CHIP_PAD);
          bodyThicknessMeasurementInMilimetersFloatValue = componentBodyThicknessMeasurementInMillimeters.getValue();
        }
        else
        {
          jointComponentBodyThicknessMeasurementInMilimeters = getComponentBodyThicknessInMillimetersMeasurement(componentInspectionData.getPadOneJointInspectionData(), 
                  SliceNameEnum.CLEAR_CHIP_PAD);
          bodyThicknessMeasurementInMilimetersFloatValue = jointComponentBodyThicknessMeasurementInMilimeters.getValue();
        }        
        
        float bodyThicknessRequiredToTestAsOpaqueInMillimeters = (Float)jointInspectionData.getSubtype().
                                                                 getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);

        if (bodyThicknessMeasurementInMilimetersFloatValue >= bodyThicknessRequiredToTestAsOpaqueInMillimeters)
          return true;
      }
    }
    // Wei Chin (Tall Cap)
//    else if(jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//    {
//      return true;
//    }
    return false;
  }

  /**
   * @author Peter Esbensen
   */
  public static boolean learnedMeasurementsExist(Subtype subtype,
                                                 boolean populatedBoard) throws DatastoreException
  {
    Assert.expect(subtype != null);

    JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

    // see if the body thicknesses are there
    boolean goodJoint = true;
    float[] componentBodyThicknessesInMilsForSlice = subtype.getLearnedJointMeasurementValues(
        getSliceNameEnumForPadSlice(jointTypeEnum),
        MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS,
        populatedBoard,
        goodJoint);

    return (componentBodyThicknessesInMilsForSlice.length > 0);
  }

  /**
   * @author Peter Esbensen
   */
  public static boolean measurementsExist(ComponentInspectionData componentInspectionData)
  {
    Assert.expect(componentInspectionData != null);

    // see if at least one of the measurements is there.  For caps, look for the body thickness.  For resistors,
    // check for body length.
    JointTypeEnum jointTypeEnum = componentInspectionData.getPadOneJointInspectionData().getJointTypeEnum();
    SliceNameEnum sliceNameEnumToCheck = SliceNameEnum.CLEAR_CHIP_PAD;
    MeasurementEnum measurementEnumToCheck = MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS;
    if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
    {
      measurementEnumToCheck = MeasurementEnum.CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH;
    }
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      return (componentInspectionData.getComponentInspectionResult().
        hasComponentMeasurement(sliceNameEnumToCheck, measurementEnumToCheck));
    }
    else
    {
      return (componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().
             hasJointMeasurement(sliceNameEnumToCheck, measurementEnumToCheck));
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  private boolean isUpdateOpaque(Subtype subtype)
  {
    Assert.expect(subtype != null);

    // Resistor will not has the option to set this new threshold setting, It will always test as Clear
    if( subtype.getJointTypeEnum().equals(JointTypeEnum.RESISTOR) )
    {
      return false;
    }
    else
    {
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD);

      if(value instanceof String)
      {
        String updateMethod = (String)value;
        if(updateMethod.equals("Clear"))
          return false;
      }
      else
      {
        Assert.expect(false, "return illigal value!");
      }
      return true;
    }
  }

  /**
   * @author Chong, Wei Chin
   */
  private boolean isUpdateClear(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if( subtype.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR) )
    {
      Serializable value = subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_UPDATE_NORMINAL_FILLET_THICKNESS_METHOD);

      if(value instanceof String)
      {
        String updateMethod = (String)value;
        if(updateMethod.equals("Opaque"))
          return false;
      }
      else
      {
        Assert.expect(false, "return illigal value!");
      }
    }    
//    else if(subtype.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//    {
//      return false;
//    }
    return true;
  }
}
