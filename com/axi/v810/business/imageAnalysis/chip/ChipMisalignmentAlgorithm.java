package com.axi.v810.business.imageAnalysis.chip;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.config.*;

/**
 * The Misalignment algorithm checks opaque capacitors to see if they have been
 * shifted to far across their pads and checks polarized capacitors to make sure
 * they haven't been loaded backwards (reversed polarity).
 *
 * @author Peter Esbensen
 */
public class ChipMisalignmentAlgorithm extends Algorithm
{
  /**
   * @author Peter Esbensen
   */
  public ChipMisalignmentAlgorithm(InspectionFamily chipInspectionFamily)
  {
    super(AlgorithmEnum.MISALIGNMENT, InspectionFamilyEnum.CHIP);

    Assert.expect(chipInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
    // CR33213-Capacitor Misalignment (Jan 2010) by Lim, Seng Yew
    AlgorithmSetting maximumMisalignmentAlongDeltaOpaque = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE,
        displayOrder++,
        100.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE)_KEY", // detailed desc
        "IMG_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    maximumMisalignmentAlongDeltaOpaque.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(maximumMisalignmentAlongDeltaOpaque);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumMisalignmentAlongDeltaOpaque,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              maximumMisalignmentAlongDeltaOpaque,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);

    AlgorithmSetting maximumMisalignmentAlongDeltaClear = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR,
        displayOrder++,
        100.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR)_KEY", // detailed desc
        "IMG_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumMisalignmentAlongDeltaClear);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              maximumMisalignmentAlongDeltaClear,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumMisalignmentAlongDeltaClear,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE);

    AlgorithmSetting maximumComponentShiftAlong = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG,
        displayOrder++,
        100.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,//MeasurementUnitsEnum.MILS,
        "HTML_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG)_KEY", // detailed desc
        "IMG_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    //maximumComponentShiftAlong.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(maximumComponentShiftAlong);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumComponentShiftAlong,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              maximumComponentShiftAlong,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumComponentShiftAlong,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              maximumComponentShiftAlong,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE);

    AlgorithmSetting maximumComponentShiftAcross = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS,
        displayOrder++,
        50.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,//MeasurementUnitsEnum.MILS,
        "HTML_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS)_KEY", // detailed desc
        "IMG_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    //maximumComponentShiftAcross.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(maximumComponentShiftAcross);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumComponentShiftAcross,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              maximumComponentShiftAcross,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumComponentShiftAcross,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              maximumComponentShiftAcross,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS);

    AlgorithmSetting maximumComponentRotation = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION,
        displayOrder++,
        10.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.DEGREES,
        "HTML_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION)_KEY", // detailed desc
        "IMG_DESC_CHIP_MISALIGNMENT_(CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    //maximumComponentRotation.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(maximumComponentRotation);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumComponentRotation,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              maximumComponentRotation,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumComponentRotation,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              maximumComponentRotation,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_ROTATION);
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ComponentInspectionData componentInspectionData :
         AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      if (ChipMeasurementAlgorithm.measurementsExist(componentInspectionData))
      {
        JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();

        boolean jointPassed = true;
        boolean tested = false;
        SliceNameEnum sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;

        if (ChipMeasurementAlgorithm.testAsOpaque(padOneJointInspectionData))
        {  
            sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
            // Wei Chin (Tall Cap)
//            if(padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//              sliceNameEnum = SliceNameEnum.PAD;
            tested = true;
            jointPassed &= checkMaximumRotation(componentInspectionData, reconstructedImages, reconstructionRegion, sliceNameEnum);

            // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
            jointPassed &= checkChipMisalignmentAcross(componentInspectionData, reconstructedImages, reconstructionRegion, sliceNameEnum);
            jointPassed &= checkChipMisalignmentAlong(componentInspectionData, reconstructedImages, reconstructionRegion, sliceNameEnum);
        }
        else
        {
            sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
            // Wei Chin (Tall Cap
//            if(padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//              sliceNameEnum = SliceNameEnum.PAD;
            tested = true;
            jointPassed &= checkMaximumRotation(componentInspectionData, reconstructedImages, reconstructionRegion, sliceNameEnum);

            // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
            jointPassed &= checkChipMisalignmentAcross(componentInspectionData, reconstructedImages, reconstructionRegion, sliceNameEnum);
            jointPassed &= checkChipMisalignmentAlong(componentInspectionData, reconstructedImages, reconstructionRegion, sliceNameEnum);
        }

        if (tested)
          AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                      padOneJointInspectionData.getComponentInspectionData(),
                                                                      reconstructionRegion,
                                                                      sliceNameEnum,
                                                                      jointPassed);
      }
    }
  }

  /**
   * @author Peter Esbensen
   */
  private boolean checkMaximumRotation(ComponentInspectionData componentInspectionData,
                                       ReconstructedImages reconstructedImages,
                                       ReconstructionRegion reconstructionRegion,
                                       SliceNameEnum sliceNameEnum )
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();

    //SliceNameEnum sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              componentInspectionData.getPadOneJointInspectionData(),
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              true);

    ComponentMeasurement componentRotationInDegrees   = null;
    JointMeasurement jointComponentRotationInDegress  = null;
    float componentRotationInDegreesFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      componentRotationInDegrees = ChipMeasurementAlgorithm.getComponentRotationInDegreesMeasurement(
        componentInspectionData, sliceNameEnum);
      componentRotationInDegreesFloatValue = componentRotationInDegrees.getValue();
    }
    else
    {
      jointComponentRotationInDegress = ChipMeasurementAlgorithm.getComponentRotationInDegreesMeasurement(
        componentInspectionData.getPadOneJointInspectionData(), sliceNameEnum);
      componentRotationInDegreesFloatValue = jointComponentRotationInDegress.getValue();
    }

    float maximumComponentRotationInDegrees = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_ROTATION);

    if (Math.abs(componentRotationInDegreesFloatValue) > maximumComponentRotationInDegrees)
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (componentRotationInDegrees != null)
        {
          ComponentIndictment misalignedIndictment = new ComponentIndictment(IndictmentEnum.MISALIGNMENT,
                                                                             this,
                                                                             sliceNameEnum,
                                                                             subtype,
                                                                             subtype.getJointTypeEnum());
          misalignedIndictment.addFailingMeasurement(componentRotationInDegrees);
          componentInspectionData.getComponentInspectionResult().addIndictment(misalignedIndictment);
          AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                              componentInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              componentRotationInDegrees,
                                                              maximumComponentRotationInDegrees);
        }
      }
      else
      {
        if (jointComponentRotationInDegress != null)
        {
          JointIndictment misalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);
          misalignedIndictment.addFailingMeasurement(jointComponentRotationInDegress);          
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(misalignedIndictment);
          AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        jointComponentRotationInDegress,
                                                        maximumComponentRotationInDegrees);
        }
      }
      
      return false;
    }

    if (Config.isComponentLevelClassificationEnabled())
    {
      if (componentRotationInDegrees != null)
      {
        AlgorithmUtil.postPassingComponentTextualDiagnostic(this,
                                                        componentInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        componentRotationInDegrees,
                                                        maximumComponentRotationInDegrees);
      }
    }
    else
    {
      if (jointComponentRotationInDegress != null)
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadOneJointInspectionData(),
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      jointComponentRotationInDegress,
                                                      maximumComponentRotationInDegrees);
      }
    }
    return true;
  }

  // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
  /**
   * @author Anthony Fong
   */
  private boolean checkChipMisalignmentAlong(ComponentInspectionData componentInspectionData,
                                       ReconstructedImages reconstructedImages,
                                       ReconstructionRegion reconstructionRegion,
                                       SliceNameEnum sliceNameEnum )
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();

    //SliceNameEnum sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              componentInspectionData.getPadOneJointInspectionData(),
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              true);

    ComponentMeasurement componentMisalignmentAlongInPercentage   = null;
    JointMeasurement jointComponentMisalignmentAlongInPercentange = null;
    float componentMisalignmentAlongInPercentageFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      componentMisalignmentAlongInPercentage = ChipMeasurementAlgorithm.getComponentMisalignmentAlongInPercentageMeasurement(
        componentInspectionData, sliceNameEnum);
      componentMisalignmentAlongInPercentageFloatValue = componentMisalignmentAlongInPercentage.getValue();
    }
    else
    {
      jointComponentMisalignmentAlongInPercentange = ChipMeasurementAlgorithm.getComponentMisalignmentAlongInPercentageMeasurement(
        componentInspectionData.getPadOneJointInspectionData(), sliceNameEnum);
      componentMisalignmentAlongInPercentageFloatValue = jointComponentMisalignmentAlongInPercentange.getValue();
    }

    float maximumJointMisalignmentAlongDeltaInPercentage = 0;
    if (sliceNameEnum.equals(sliceNameEnum.OPAQUE_CHIP_PAD))
        // Wei Chin (Tall Cap)
//        || sliceNameEnum.equals(sliceNameEnum.PAD))
    {
      maximumJointMisalignmentAlongDeltaInPercentage = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_OPAQUE);
    }
    else
    {
      maximumJointMisalignmentAlongDeltaInPercentage = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_MISALIGNMENT_ALONG_DELTA_CLEAR);
    }
    JointMeasurement Joint1MisalignmentAlongInPercentage = ChipMeasurementAlgorithm.getPin1MisalignmentAlongDeltaInPercentageMeasurement(
      componentInspectionData, sliceNameEnum);
    JointMeasurement Joint2MisalignmentAlongInPercentage = ChipMeasurementAlgorithm.getPin2MisalignmentAlongDeltaInPercentageMeasurement(
      componentInspectionData, sliceNameEnum);
    boolean joint1Passed = true;
    boolean joint2Passed = true;
    //Joint 1 Result
    if (Math.abs(Joint1MisalignmentAlongInPercentage.getValue()) > maximumJointMisalignmentAlongDeltaInPercentage)
    {
      JointIndictment joint1MisalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT,
                                                                         this,
                                                                         sliceNameEnum);
      joint1MisalignedIndictment.addFailingMeasurement(Joint1MisalignmentAlongInPercentage);
      JointInspectionResult joint1InspectionResult = componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult();
  

      joint1InspectionResult.addIndictment(joint1MisalignedIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadOneJointInspectionData(),
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      Joint1MisalignmentAlongInPercentage,
                                                      maximumJointMisalignmentAlongDeltaInPercentage);
       joint1Passed &= false;
    }
    else
    {
      //JointIndictment joint1MisalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT,
      //                                                                   this,
      //                                                                   sliceNameEnum);
      //joint1MisalignedIndictment.addFailingMeasurement(Joint1MisalignmentAlongInPercentage);
      //JointInspectionResult joint1InspectionResult = componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult();


      //joint1InspectionResult.addIndictment(joint1MisalignedIndictment);
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadOneJointInspectionData(),
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      Joint1MisalignmentAlongInPercentage,
                                                      maximumJointMisalignmentAlongDeltaInPercentage);
       joint1Passed &= true;
    }

    //Joint 2 Result
    if (Math.abs(Joint2MisalignmentAlongInPercentage.getValue()) > maximumJointMisalignmentAlongDeltaInPercentage)
    {
      JointIndictment joint2MisalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT,
                                                                         this,
                                                                         sliceNameEnum);
      joint2MisalignedIndictment.addFailingMeasurement(Joint2MisalignmentAlongInPercentage);
      JointInspectionResult joint2InspectionResult = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getJointInspectionResult();


      joint2InspectionResult.addIndictment(joint2MisalignedIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices(),
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      Joint2MisalignmentAlongInPercentage,
                                                      maximumJointMisalignmentAlongDeltaInPercentage);
        joint2Passed &= false;
    }
    else
    {
//      JointIndictment joint2MisalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT,
//                                                                         this,
//                                                                         sliceNameEnum);
//      joint2MisalignedIndictment.addFailingMeasurement(Joint2MisalignmentAlongInPercentage);
//      JointInspectionResult joint2InspectionResult = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getJointInspectionResult();
//
//
//      joint2InspectionResult.addIndictment(joint2MisalignedIndictment);
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices(),
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      Joint2MisalignmentAlongInPercentage,
                                                      maximumJointMisalignmentAlongDeltaInPercentage);
       joint2Passed &= true;
    }
    //Component Result
    float maximumComponentMisalignmentAlongInPercentage = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ALONG);
    if ((Math.abs(componentMisalignmentAlongInPercentageFloatValue) > maximumComponentMisalignmentAlongInPercentage) || (joint1Passed == false && joint2Passed == false))
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (componentMisalignmentAlongInPercentage != null)
        {
          ComponentIndictment misalignedIndictment = new ComponentIndictment(IndictmentEnum.MISALIGNMENT,
                                                                             this,
                                                                             sliceNameEnum,
                                                                             subtype,
                                                                             subtype.getJointTypeEnum());
          misalignedIndictment.addFailingMeasurement(componentMisalignmentAlongInPercentage);
          componentInspectionData.getComponentInspectionResult().addIndictment(misalignedIndictment);
          AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                              componentInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              componentMisalignmentAlongInPercentage,
                                                              maximumComponentMisalignmentAlongInPercentage);
        }
      }
      else
      {
        if (jointComponentMisalignmentAlongInPercentange != null)
        {
          JointIndictment misalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);
          misalignedIndictment.addFailingMeasurement(jointComponentMisalignmentAlongInPercentange);          
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(misalignedIndictment);
          AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        jointComponentMisalignmentAlongInPercentange,
                                                        maximumComponentMisalignmentAlongInPercentage);
        }
      }
      return false;
    }
    else
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (componentMisalignmentAlongInPercentage != null)
        {
          AlgorithmUtil.postPassingComponentTextualDiagnostic(this,
                                                        componentInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        componentMisalignmentAlongInPercentage,
                                                        maximumComponentMisalignmentAlongInPercentage);
        }
      }
      else
      {
        if (jointComponentMisalignmentAlongInPercentange != null)
        {
          AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        jointComponentMisalignmentAlongInPercentange,
                                                        maximumComponentMisalignmentAlongInPercentage);
        }
      }
    }
    return true;
  }
  
 // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
   /**
   * @author Anthony Fong
   */
  private boolean checkChipMisalignmentAcross(ComponentInspectionData componentInspectionData,
                                       ReconstructedImages reconstructedImages,
                                       ReconstructionRegion reconstructionRegion,
                                       SliceNameEnum sliceNameEnum )
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();

    //SliceNameEnum sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              componentInspectionData.getPadOneJointInspectionData(),
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              true);

    //getPad2MisalignmentAlongInMilsMeasurement

    ComponentMeasurement componentMisalignmentAcrossInMils  = null;
    JointMeasurement jointComponentMisalignmentAcrossInMils = null;
    float componentMisalignmentAcrossInMilsFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      componentMisalignmentAcrossInMils = ChipMeasurementAlgorithm.getComponentMisalignmentAcrossInPercentageMeasurement(
              componentInspectionData, sliceNameEnum);
      componentMisalignmentAcrossInMilsFloatValue = componentMisalignmentAcrossInMils.getValue();
    }
    else
    {
      jointComponentMisalignmentAcrossInMils = ChipMeasurementAlgorithm.getComponentMisalignmentAcrossInPercentageMeasurement(
              componentInspectionData.getPadOneJointInspectionData(), sliceNameEnum);
      componentMisalignmentAcrossInMilsFloatValue = jointComponentMisalignmentAcrossInMils.getValue();
    }

    float maximumComponentMisalignmentAcrossInMils = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MISALIGNMENT_MAXIMUM_COMPONENT_SHIFT_ACROSS);

    if (Math.abs(componentMisalignmentAcrossInMilsFloatValue) > maximumComponentMisalignmentAcrossInMils)
    {
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (componentMisalignmentAcrossInMils != null)
        {
          ComponentIndictment misalignedIndictment = new ComponentIndictment(IndictmentEnum.MISALIGNMENT,
                                                                             this,
                                                                             sliceNameEnum,
                                                                             subtype,
                                                                             subtype.getJointTypeEnum());
          misalignedIndictment.addFailingMeasurement(componentMisalignmentAcrossInMils);
          componentInspectionData.getComponentInspectionResult().addIndictment(misalignedIndictment);
          AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                              componentInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              componentMisalignmentAcrossInMils,
                                                              maximumComponentMisalignmentAcrossInMils);
        }
      }
      else
      {
        if (jointComponentMisalignmentAcrossInMils != null)
        {
          JointIndictment misalignedIndictment = new JointIndictment(IndictmentEnum.MISALIGNMENT, this, sliceNameEnum);
          misalignedIndictment.addFailingMeasurement(jointComponentMisalignmentAcrossInMils);          
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(misalignedIndictment);
          AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        jointComponentMisalignmentAcrossInMils,
                                                        maximumComponentMisalignmentAcrossInMils);
        }
      }
      return false;
    }

    if (Config.isComponentLevelClassificationEnabled())
    {
      if (componentMisalignmentAcrossInMils != null)
      {
        AlgorithmUtil.postPassingComponentTextualDiagnostic(this,
                                                        componentInspectionData,
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        componentMisalignmentAcrossInMils,
                                                        maximumComponentMisalignmentAcrossInMils);
      }
    }
    else
    {
      if (jointComponentMisalignmentAcrossInMils != null)
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      componentInspectionData.getPadOneJointInspectionData(),
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      jointComponentMisalignmentAcrossInMils,
                                                      maximumComponentMisalignmentAcrossInMils);
      }
    }
    return true;
  }
}
