package com.axi.v810.business.imageAnalysis.chip;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * InspectionFamily for resistors, capacitors (including polarized caps).
 *
 * @author Peter Esbensen
 */
public class ChipInspectionFamily extends InspectionFamily
{

  /**
   * @author Peter Esbensen
   */
  public ChipInspectionFamily()
  {
    super(InspectionFamilyEnum.CHIP);
    Algorithm chipMeasurementAlgorithm = new ChipMeasurementAlgorithm(this);
    addAlgorithm(chipMeasurementAlgorithm);
    Algorithm chipShortAlgorithm = new RectangularShortAlgorithm(InspectionFamilyEnum.CHIP);
    addAlgorithm(chipShortAlgorithm);
    Algorithm chipOpenAlgorithm = new ChipOpenAlgorithm(this);
    addAlgorithm(chipOpenAlgorithm);
    Algorithm chipMisalignmentAlgorithm = new ChipMisalignmentAlgorithm(this);
    addAlgorithm(chipMisalignmentAlgorithm);
    Algorithm chipInsufficientAlgorithm = new ChipInsufficientAlgorithm(this);
    addAlgorithm(chipInsufficientAlgorithm);
    Algorithm chipExcessAlgorithm = new ChipExcessAlgorithm(this);
    addAlgorithm(chipExcessAlgorithm);
  }

  /**
   * @return a Collection of the slices inspected by this InspectionFamily for the specified subtype.
   * @author Matt Wharton
   */
  public Collection<SliceNameEnum> getOrderedInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);

    Collection<SliceNameEnum> inspectedSlices = new LinkedList<SliceNameEnum>();
    // Wei Chin (Tall Cap)
//    if(subtype.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//      inspectedSlices.add(SliceNameEnum.PAD);
//    else
//    {
      inspectedSlices.add(SliceNameEnum.CLEAR_CHIP_PAD);
      inspectedSlices.add(SliceNameEnum.OPAQUE_CHIP_PAD);
//    }

    return inspectedSlices;
  }

  /**
   * Returns the appropriate default SliceNameEnum for the Pad slice on the Chip inspection family given the joint
   * type of the specified JointInspectionData.
   *
   * @author Matt Wharton
   */
  public SliceNameEnum getDefaultPadSliceNameEnum(Subtype subtype)
  {
    Assert.expect(subtype != null);

    final JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();

    SliceNameEnum defaultPadSliceNameEnumForChip = null;
    // If the joint is a PCAP, return the standard pad slice.
    if (jointTypeEnum.equals(JointTypeEnum.POLARIZED_CAP))
        // Wei Chin (Tall Cap)
//        || jointTypeEnum.equals(JointTypeEnum.TALL_CAPACITOR))
    {
      defaultPadSliceNameEnumForChip = SliceNameEnum.PAD;
    }
    // For a RES, just use the clear pad slice
    else if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
    {
      defaultPadSliceNameEnumForChip = SliceNameEnum.CLEAR_CHIP_PAD;
    }
    // A CHIP could be either clear or opaque.  The measurement algorithm will determine the correct
    // slice to use.  Assume the clear pad slice.
    else if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
    {
      defaultPadSliceNameEnumForChip = SliceNameEnum.OPAQUE_CHIP_PAD;
    }
    else
    {
      // This joint type must be supported by the Chip inspection family.
      Assert.expect(false, "Chip inspection family does not support this joint type!");
    }
    
    return defaultPadSliceNameEnumForChip;
  }
  
  /**
   * XCR-2492 Short Thickness of a capacitor component shows Opaque chip and Clear chip slice
   * @author Cheah Lee Herng 
   */
  public Collection<SliceNameEnum> getShortInspectionSlices(Subtype subtype)
  {
    Assert.expect(subtype != null);
    
    final JointTypeEnum jointTypeEnum = subtype.getJointTypeEnum();
    
    Collection<SliceNameEnum> shortInspectionSlices = new ArrayList<SliceNameEnum>();
    
    if (jointTypeEnum.equals(JointTypeEnum.CAPACITOR))
    {
      shortInspectionSlices.add(SliceNameEnum.OPAQUE_CHIP_PAD);
      shortInspectionSlices.add(SliceNameEnum.CLEAR_CHIP_PAD);
    }
    else if (jointTypeEnum.equals(JointTypeEnum.RESISTOR))
    {
      shortInspectionSlices.add(SliceNameEnum.CLEAR_CHIP_PAD);
    }
    else
    {
      Assert.expect(false, "Chip inspection family does not support this joint type!");
    }
    return shortInspectionSlices;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnumForLocator(Subtype subtype)
  {
    Assert.expect(subtype != null);
//    if (subtype.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//        return SliceNameEnum.PAD;
    return SliceNameEnum.CLEAR_CHIP_PAD;
  }

  /**
   * @author Peter Esbensen
   */
  public boolean classifiesAtComponentOrMeasurementGroupLevel()
  {
    return true;
  }
}
