package com.axi.v810.business.imageAnalysis.chip;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;

/**
 * @author Lim, Seng Yew
 */
class ChipExcessAlgorithm extends Algorithm
{
  /**
   * @author Lim, Seng Yew
   */
  public ChipExcessAlgorithm(InspectionFamily chipInspectionFamily)
  {
    super(AlgorithmEnum.EXCESS, InspectionFamilyEnum.CHIP);

    Assert.expect(chipInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    AlgorithmSetting maximumFilletThickness = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS,
        displayOrder++,
        150.0f, // default
        0.0f, // min
        500.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_EXCESS_(CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_EXCESS_(CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_CHIP_EXCESS_(CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(maximumFilletThickness);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumFilletThickness,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
     //Aik Liang - XCR 2313:Double Click Work Incorrectly for Clear Capacitor's Minimum Fillet Thickness
     chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                               maximumFilletThickness,
                                                               SliceNameEnum.CLEAR_CHIP_PAD,
                                                               MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              maximumFilletThickness,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              maximumFilletThickness,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL);

    addMeasurementEnums();
  }

  /**
   * @author Lim, Seng Yew
   */
  private void addMeasurementEnums()
  {
    Assert.expect(_jointMeasurementEnums != null);

    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL);
  }

  /**
   * @author Lim, Seng Yew
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects)
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    for (ComponentInspectionData componentInspectionData :
         AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      if (ChipMeasurementAlgorithm.measurementsExist(componentInspectionData))
      {
        JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
        JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();

        checkFilletThickness(padOneJointInspectionData, reconstructedImages,
                             reconstructionRegion);
        checkFilletThickness(padTwoJointInspectionData, reconstructedImages,
                             reconstructionRegion);
      }
    }
  }

  /**
   * @author Lim, Seng Yew
   */
  private void checkFilletThickness(JointInspectionData jointInspectionData,
                                    ReconstructedImages reconstructedImages,
                                    ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedImages != null);
    Assert.expect(reconstructionRegion != null);

    boolean testAsOpaque = ChipMeasurementAlgorithm.testAsOpaque(jointInspectionData);
    SliceNameEnum sliceNameEnum = SliceNameEnum.CLEAR_CHIP_PAD;
    if (testAsOpaque)
      sliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
    if (jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP))
      sliceNameEnum = SliceNameEnum.PAD;
    // Wei Chin (Tall Cap)
//    if (jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
//      sliceNameEnum = SliceNameEnum.PAD;

    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

    AlgorithmUtil.postStartOfJointDiagnostics(this,
                                              jointInspectionData,
                                              reconstructionRegion,
                                              reconstructedSlice,
                                              true);

    Subtype subtype = jointInspectionData.getSubtype();

    JointMeasurement filletThicknessPercentOfNominalMeasurement = null;
    if (testAsOpaque)
      filletThicknessPercentOfNominalMeasurement = getFilletThicknessAsPercentOfOpaqueNominal(jointInspectionData, sliceNameEnum, subtype);
    else
      filletThicknessPercentOfNominalMeasurement = getFilletThicknessAsPercentOfClearNominal(jointInspectionData, sliceNameEnum, subtype);

    float maximumFilletThicknessThreshold = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.CHIP_EXCESS_MAXIMUM_FILLET_THICKNESS);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();

    boolean jointPassed = true;

    if (filletThicknessPercentOfNominalMeasurement.getValue() > maximumFilletThicknessThreshold)
    {
      jointPassed = false;
      JointIndictment jointIndictment = new JointIndictment(IndictmentEnum.EXCESS,
          this, sliceNameEnum);
      jointIndictment.addFailingMeasurement(filletThicknessPercentOfNominalMeasurement);
      jointInspectionResult.addIndictment(jointIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      maximumFilletThicknessThreshold);
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      filletThicknessPercentOfNominalMeasurement,
                                                      maximumFilletThicknessThreshold);
    }

    AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                jointInspectionData.getComponentInspectionData(),
                                                                reconstructionRegion,
                                                                sliceNameEnum,
                                                                jointPassed);
  }

  /**
   * @author Lim, Seng Yew
   */
  private JointMeasurement getFilletThicknessAsPercentOfOpaqueNominal(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum,
      Subtype subtype)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    JointMeasurement filletThicknessMeasurement = ChipMeasurementAlgorithm.getOpaqueFilletThicknessInMillimetersMeasurement(
        jointInspectionData, sliceNameEnum);

    float nominalFilletThicknessInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_OPAQUE_NOMINAL_PAD_THICKNESS);

    float filletThicknessPercentOfNominal = 100.0f * filletThicknessMeasurement.getValue() / nominalFilletThicknessInMillimeters;
    JointMeasurement filletThicknessPercentOfNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
        MeasurementUnitsEnum.PERCENT, jointInspectionData.getPad(),
        sliceNameEnum, filletThicknessPercentOfNominal, true);

    jointInspectionData.getJointInspectionResult().addMeasurement(filletThicknessPercentOfNominalMeasurement);
    return filletThicknessPercentOfNominalMeasurement;
  }

  /**
   * @author Lim, Seng Yew
   */
  private JointMeasurement getFilletThicknessAsPercentOfClearNominal(JointInspectionData jointInspectionData,
      SliceNameEnum sliceNameEnum,
      Subtype subtype)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);

    JointMeasurement filletThicknessMeasurement = ChipMeasurementAlgorithm.getClearFilletThicknessInMillimetersMeasurement(
        jointInspectionData, sliceNameEnum);

    float nominalFilletThicknessInMillimeters = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_CLEAR_NOMINAL_PAD_THICKNESS);

    float filletThicknessPercentOfNominal = 100.0f * filletThicknessMeasurement.getValue() / nominalFilletThicknessInMillimeters;
    JointMeasurement filletThicknessPercentOfNominalMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL,
        MeasurementUnitsEnum.PERCENT, jointInspectionData.getPad(),
        sliceNameEnum, filletThicknessPercentOfNominal, true);

    jointInspectionData.getJointInspectionResult().addMeasurement(filletThicknessPercentOfNominalMeasurement);
    return filletThicknessPercentOfNominalMeasurement;
  }
}
