package com.axi.v810.business.imageAnalysis.chip;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.datastore.*;
//import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;


/**
 * @author Peter Esbensen
 */
class ChipOpenAlgorithm extends Algorithm
{
  private static String _SOLDER_AREA_DETECTION = "Solder Area";
  private static String _ROUNDNESS_DETECTION = "Roundness Calculation";
  private static String _TEMPLATE_MATCH_DETECTION = "Template Match";
  private static String _DISABLED_SECONDARY_DETECTION = "Disable";
  /**
   * @author Peter Esbensen
   */
  public ChipOpenAlgorithm(InspectionFamily chipInspectionFamily)
  {
    super(AlgorithmEnum.OPEN, InspectionFamilyEnum.CHIP);

    Assert.expect(chipInspectionFamily != null);

    int displayOrder = 1;
    int currentVersion = 1;

    AlgorithmSetting minimumBodyLength = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_LENGTH,
        displayOrder++,
        90.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_LENGTH)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              minimumBodyLength,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              minimumBodyLength,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT);
    minimumBodyLength.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(minimumBodyLength);
    
    AlgorithmSetting maximumBodyLength = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_BODY_LENGTH,
        displayOrder++,
        120.0f, // default
        0.0f, // min
        300.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_BODY_LENGTH)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_BODY_LENGTH)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_BODY_LENGTH)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumBodyLength,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT);
    maximumBodyLength.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(maximumBodyLength);

    AlgorithmSetting opaqueOpenSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_OPAQUE_OPEN_SIGNAL,
        displayOrder++,
        100.0f, // default
        0.0f, // min
        1000.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_OPAQUE_OPEN_SIGNAL)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_OPAQUE_OPEN_SIGNAL)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_OPAQUE_OPEN_SIGNAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              opaqueOpenSignal,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              opaqueOpenSignal,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL);
    opaqueOpenSignal.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(opaqueOpenSignal);

    AlgorithmSetting clearOpenSignal = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_CLEAR_OPEN_SIGNAL,
        displayOrder++,
        0.0508f, // default
        -0.6350f, // min
        25.4f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_CLEAR_OPEN_SIGNAL)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_CLEAR_OPEN_SIGNAL)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_CLEAR_OPEN_SIGNAL)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    addAlgorithmSetting(clearOpenSignal);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              clearOpenSignal,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              clearOpenSignal,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL);

    AlgorithmSetting maximumFilletGapMeasurement = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_FILLET_GAP,
        displayOrder++,
        1.5f, // default
        0.0f, // min
        10.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_FILLET_GAP)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_FILLET_GAP)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_FILLET_GAP)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumFilletGapMeasurement,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_FILLET_GAP);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              maximumFilletGapMeasurement,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_FILLET_GAP);
    maximumFilletGapMeasurement.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(maximumFilletGapMeasurement);


    AlgorithmSetting opaqueMinimumBodyThickness = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_THICKNESS,
        displayOrder++,
        0.127f, // default
        0.0f, // min
        0.635f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              opaqueMinimumBodyThickness,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS);
    opaqueMinimumBodyThickness.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(opaqueMinimumBodyThickness);


    AlgorithmSetting clearMinimumBodyThickness = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS,
        displayOrder++,
        0.0f, // default
        0.0f, // min
        0.635f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              clearMinimumBodyThickness,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS);
    clearMinimumBodyThickness.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(clearMinimumBodyThickness);

    AlgorithmSetting minimumBodyWidth = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_WIDTH,
        displayOrder++,
        0.0f, // default
        0.0f, // min
        10.0f, // max
        MeasurementUnitsEnum.MILLIMETERS,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_WIDTH)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_WIDTH)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_BODY_WIDTH)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              minimumBodyWidth,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              minimumBodyWidth,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_BODY_WIDTH);
    minimumBodyWidth.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(minimumBodyWidth);
      
    //Lim, Lay Ngor - Opaque Tombstone - XCR3359 - Enable Fillet Width Detection
    ArrayList<String> enableDetectionValues = new ArrayList<String>(Arrays.asList("True", "False"));
    AlgorithmSetting enablOpaqueChipFilletWidthDetection = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION,
        displayOrder++,
        "False",
        enableDetectionValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION)_KEY",
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION)_KEY",
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    enablOpaqueChipFilletWidthDetection.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(enablOpaqueChipFilletWidthDetection);
    
    //Lim, Lay Ngor - Opaque Tombstone - Use to measure Left & Right Pad width ratio
    AlgorithmSetting minimumFilletWidthJointRatio = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO,
        displayOrder++,
        0.0f,//0.9f, // default
        0.0f, // min
        1.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              minimumFilletWidthJointRatio,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH_JOINT_RATIO);
    minimumFilletWidthJointRatio.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(minimumFilletWidthJointRatio);    
    
    //Lim, Lay Ngor - Opaque Tombstone - Use to measure Left & Right Pad width ratio
    AlgorithmSetting partialFilletThicknessPercentageFromBodyThickness= new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE,
        displayOrder++,
        90.0f, // default
        10.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              partialFilletThicknessPercentageFromBodyThickness,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS);
    partialFilletThicknessPercentageFromBodyThickness.filter(JointTypeEnum.RESISTOR);
    addAlgorithmSetting(partialFilletThicknessPercentageFromBodyThickness);        
    
    //Lim, Lay Ngor - Clear Tombstone - Plug from gullwing
    AlgorithmSetting maximumCenterToUpperFilletThicknessPercentage = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE,
        displayOrder++,
        200.0f,//85.0f, // default
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE)_KEY", // image file
        AlgorithmSettingTypeEnum.STANDARD,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumCenterToUpperFilletThicknessPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              maximumCenterToUpperFilletThicknessPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);    
    addAlgorithmSetting(maximumCenterToUpperFilletThicknessPercentage);         
    
    AlgorithmSetting opaqueComponentTiltRatioMax = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX,
        displayOrder++,
        5.0f, // default
        0.0f, // min
        5.0f, // max
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX)_KEY", // detailed desc
        "IMG_DESC_CHIP_MEASUREMENT_(CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              opaqueComponentTiltRatioMax,
                                                              SliceNameEnum.OPAQUE_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO);
    // Wei Chin (Tall Cap)
//    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.TALL_CAPACITOR,
//                                                              opaqueComponentTiltRatioMax,
//                                                              SliceNameEnum.PAD,
//                                                              MeasurementEnum.CHIP_MEASUREMENT_COMPONENT_TILT_RATIO);
    addAlgorithmSetting(opaqueComponentTiltRatioMax);
    opaqueComponentTiltRatioMax.filter(JointTypeEnum.RESISTOR);
    
    //ShengChuan - Clear Tombstone - Options for pattern match detection, we will hide this option unless it is OKI.
    ArrayList<String> allowableDetectionMethodValues = new ArrayList<String>(Arrays.asList(_DISABLED_SECONDARY_DETECTION, _ROUNDNESS_DETECTION, _SOLDER_AREA_DETECTION, _TEMPLATE_MATCH_DETECTION));
    //LayNgor rename
    AlgorithmSetting clearChipOpenDetectionMethod = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD,
        displayOrder++,
        "Disable",
        allowableDetectionMethodValues,
        MeasurementUnitsEnum.NONE,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_DETECTION_METHOD)_KEY",
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_DETECTION_METHOD)_KEY",
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_DETECTION_METHOD)_KEY",
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    addAlgorithmSetting(clearChipOpenDetectionMethod);
    
    //Lim, Lay Ngor - Clear Tombstone - Use to detect the solder area in percentage.
    AlgorithmSetting maximumSolderAreaPercentage = new AlgorithmSetting(
        AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE,
        displayOrder++,
        0.0f,// default 50.0, 0.0 means skip this checking
        0.0f, // min
        200.0f, // max
        MeasurementUnitsEnum.PERCENT_OF_HEEL_THICKNESS,
        "HTML_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              maximumSolderAreaPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE);
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              maximumSolderAreaPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE);    
    addAlgorithmSetting(maximumSolderAreaPercentage);    
    
    //ShengChuan - Clear Tombstone - Options for pattern match detection, we will hide this option unless it is OKI.
    AlgorithmSetting patternMatchingPercentage = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE,
        displayOrder++,
        100.00f, // default
        00.00f, // min
        100.0f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_OPEN_(CHIP_TEMPLATE_MATCHING_PERCENTAGE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_TEMPLATE_MATCHING_PERCENTAGE)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_TEMPLATE_MATCHING_PERCENTAGE)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              patternMatchingPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING);
    
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              patternMatchingPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING);
    
    addAlgorithmSetting(patternMatchingPercentage);   
    
    //ShengChuan - Clear Tombstone - Options for roundness detection.
    AlgorithmSetting roundnessCalculationPercentage = new AlgorithmSetting(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE,
        displayOrder++,
        00.00f, // default
        00.00f, // min
        100.000f, // max
        MeasurementUnitsEnum.PERCENT,
        "HTML_DESC_CHIP_OPEN_(CHIP_ROUNDNESS_CALCULATION_PERCENTAGE)_KEY", // desc
        "HTML_DETAILED_DESC_CHIP_OPEN_(CHIP_ROUNDNESS_CALCULATION_PERCENTAGE)_KEY", // detailed desc
        "IMG_DESC_CHIP_OPEN_(CHIP_ROUNDNESS_CALCULATION_PERCENTAGE)_KEY", // image file
        AlgorithmSettingTypeEnum.ADDITIONAL,
        currentVersion);
    
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.CAPACITOR,
                                                              roundnessCalculationPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT);
    
    chipInspectionFamily.mapAlgorithmSettingToMeasurementEnum(JointTypeEnum.RESISTOR,
                                                              roundnessCalculationPercentage,
                                                              SliceNameEnum.CLEAR_CHIP_PAD,
                                                              MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT);
    
    //Tombstone: Save diagnostic images
    addAlgorithmSetting(ImageProcessingAlgorithm.createSaveDiagnoticVoidImageAlgorithmSetting(displayOrder, currentVersion));
    
    addAlgorithmSetting(roundnessCalculationPercentage);   
    
    addMeasurementEnums();
  }

  /**
   * @author Peter Esbensen
   */
  private void addMeasurementEnums()
  {
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL);
    //Lim, Lay Ngor - Opaque Tombstone
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH_JOINT_RATIO);
    
    //ShengChuan - Clear Tombstone - Options for pattern match detection, we will hide this option unless it is OKI.
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT);
    _jointMeasurementEnums.add(MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING);
  }

  /**
   * @author Peter Esbensen
   */
  private SliceNameEnum getSliceNameEnumForOpen(JointInspectionData jointInspectionData,
                                                boolean testAsOpaque)
  {
    Assert.expect(jointInspectionData != null);
    if (testAsOpaque)
        // Wei Chin (Tall Cap)
//        && jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR) == false)
      return SliceNameEnum.OPAQUE_CHIP_PAD;
    else if (jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.POLARIZED_CAP)
             //Wei Chin (Tall Cap)
//             || jointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR)
            )
      return SliceNameEnum.PAD;
    else
      return SliceNameEnum.CLEAR_CHIP_PAD;
  }

  /**
   * @author Peter Esbensen
   */
  public void classifyJoints(ReconstructedImages reconstructedImages,
                             List<JointInspectionData> jointInspectionDataObjects) throws DatastoreException
  {
    Assert.expect(reconstructedImages != null);
    Assert.expect(jointInspectionDataObjects != null);
    assertAllJointInspectionDataObjectsAreSameSubtype(jointInspectionDataObjects);

    ReconstructionRegion reconstructionRegion = reconstructedImages.getReconstructionRegion();

    boolean firstJointInImage = true;

    for (ComponentInspectionData componentInspectionData :
         AlgorithmUtil.getComponentInspectionDataCollection(jointInspectionDataObjects))
    {
      if (ChipMeasurementAlgorithm.measurementsExist(componentInspectionData))
      {
        JointInspectionData padOneJointInspectionData = componentInspectionData.getPadOneJointInspectionData();
        JointInspectionData padTwoJointInspectionData = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices();

        boolean testAsOpaque = ChipMeasurementAlgorithm.testAsOpaque(padOneJointInspectionData);
        SliceNameEnum sliceNameEnum = getSliceNameEnumForOpen(padOneJointInspectionData, testAsOpaque);
        ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(sliceNameEnum);

        AlgorithmUtil.postStartOfJointDiagnostics(this,
                                                  padOneJointInspectionData,
                                                  reconstructionRegion,
                                                  reconstructedSlice,
                                                  firstJointInImage);
        firstJointInImage = false;

        boolean jointPassed = true;

        if (ChipMeasurementAlgorithm.testAsOpaque(padOneJointInspectionData))
        {
          jointPassed &= checkMinimumBodyLength(componentInspectionData, reconstructedSlice,
                                                reconstructionRegion);
          
          jointPassed &= checkMaximumBodyLength(componentInspectionData, reconstructedSlice,
                                                reconstructionRegion);

          jointPassed &= checkFilletGapMeasurements(padOneJointInspectionData, padTwoJointInspectionData,
                                                    reconstructedSlice, reconstructionRegion);

          jointPassed &= checkOpaqueOpenSignal(componentInspectionData, reconstructedSlice,
                                               reconstructionRegion);

          jointPassed &= checkOpaqueMinimumBodyThickness(componentInspectionData, reconstructedSlice,
                                                   reconstructionRegion);

          jointPassed &= checkMinimumBodyWidth(componentInspectionData, reconstructedSlice,
                                               reconstructionRegion);

          //Lim, Lay Ngor - Opaque Tombstone - XCR-3359 Enable Fillet Width Detection - START   
          String detectionMethod = (String)padOneJointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_ENABLE_OPAQUE_FILLET_WIDTH_DETECTION);
          if(detectionMethod.equalsIgnoreCase("true"))
          {
            jointPassed &= checkMinimumBodyWidthRatioForTombstone(componentInspectionData, reconstructedSlice,
                                               reconstructionRegion);
          }

          jointPassed &= checkOpaqueComponentTiltRatio(componentInspectionData, reconstructedSlice,
                                               reconstructionRegion);
        }
        else
        {
          //XCR-3461, component still fail even tombstone algo checking is pass
          String detectionMethod = (String)padOneJointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD);
          boolean jointOnePassed = true;
          boolean jointTwoPassed = true;
          jointOnePassed &= checkClearOpenSignal(padOneJointInspectionData, reconstructedSlice,
                                              reconstructionRegion);

          jointTwoPassed &= checkClearOpenSignal(padTwoJointInspectionData, reconstructedSlice,
                                              reconstructionRegion);

          //Lim, Lay Ngor - Clear Tombstone START
//            System.out.println("joint one");
          jointOnePassed &= checkClearCenterToUpperFilletThicknessPercent(padOneJointInspectionData, 
                                                                       reconstructedSlice);

//            System.out.println("joint two");
          jointTwoPassed &= checkClearCenterToUpperFilletThicknessPercent(padTwoJointInspectionData, 
                                                                       reconstructedSlice);
          
          if(detectionMethod.equalsIgnoreCase("disable") == false)
          {
            if (jointOnePassed == false)
              jointOnePassed |= checkSecondLevelDetectionMethod(padOneJointInspectionData,
                                                             reconstructedSlice, detectionMethod);
            if (jointTwoPassed == false)
              jointTwoPassed |= checkSecondLevelDetectionMethod(padTwoJointInspectionData, 
                                                             reconstructedSlice, detectionMethod);              
          }
          
          jointPassed = jointTwoPassed && jointOnePassed;
          // (CR30981) Both opaque and clear capacitors now require body thickness measurement
          // Resistors do not have a body thickness measurement so we need to check for joint type.
          if (padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.CAPACITOR))
            // Wei Chin (Tall Cap)
//            || padOneJointInspectionData.getJointTypeEnum().equals(JointTypeEnum.TALL_CAPACITOR))
          {
            jointPassed &= checkClearMinimumBodyThickness(componentInspectionData, reconstructedSlice,
                                                          reconstructionRegion);
          }
          //Lim, Lay Ngor - Clear Tombstone END
        }

        AlgorithmUtil.postComponentPassingOrFailingRegionDiagnostic(this,
                                                                    componentInspectionData,
                                                                    reconstructionRegion,
                                                                    sliceNameEnum,
                                                                    jointPassed);
      }
    }
  }
  
  /**
   * XCR-3461, component still fail even tombstone algo checking is pass
   * @author Yong Sheng Chuan
   */
  private boolean isSecondaryMeasurementResultPassed(JointInspectionData jointInspectionData, ReconstructedSlice reconstructedSlice)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    JointMeasurement secondaryJointMeasurement = null;
    Subtype subtype = jointInspectionData.getSubtype();
    boolean isSecondaryMeasurementResult = true;
    String detectionMethod = (String) jointInspectionData.getSubtype().getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_DETECTION_METHOD);
    //XCR-3477, System crash when run test
    if (detectionMethod.equalsIgnoreCase(_DISABLED_SECONDARY_DETECTION) == false)
    {
      if (detectionMethod.equalsIgnoreCase(_SOLDER_AREA_DETECTION) && 
          jointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE))
      {
        final float MAXIMUM_ACCEPTABLE_PAD_SOLDER_AREA_PERCENT = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE);
        secondaryJointMeasurement = ChipMeasurementAlgorithm.getPadSolderAreaPercentMeasurement(jointInspectionData, sliceNameEnum);
        if (MathUtil.fuzzyGreaterThan(secondaryJointMeasurement.getValue(), MAXIMUM_ACCEPTABLE_PAD_SOLDER_AREA_PERCENT))
          isSecondaryMeasurementResult = false;
      }
      else if (detectionMethod.equalsIgnoreCase(_ROUNDNESS_DETECTION) && 
               jointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_ROUNDNESS_PERCENT))
      {
        final float MAXIMUM_ACCEPTABLE_PAD_ROUNDNESS_PERCENT = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE);
        secondaryJointMeasurement = ChipMeasurementAlgorithm.getRoundnessPercentagesMeasurement(jointInspectionData, sliceNameEnum);
        if (MathUtil.fuzzyGreaterThan(secondaryJointMeasurement.getValue(), MAXIMUM_ACCEPTABLE_PAD_ROUNDNESS_PERCENT))
          isSecondaryMeasurementResult = false;
      }
      else if (detectionMethod.equalsIgnoreCase(_TEMPLATE_MATCH_DETECTION) && 
               jointInspectionData.getJointInspectionResult().hasJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING))
      {
        final float MINIMUM_ACCEPTABLE_PAD_TEMPLATE_MATCH_PERCENT = (Float) subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE);
        JointMeasurement padTemplateMatchPercentMeasurement = ChipMeasurementAlgorithm.getMatchingPercentagesMeasurement(jointInspectionData, sliceNameEnum);
        if (MathUtil.fuzzyLessThan(padTemplateMatchPercentMeasurement.getValue(), MINIMUM_ACCEPTABLE_PAD_TEMPLATE_MATCH_PERCENT))
          isSecondaryMeasurementResult = false;
      }
      else
        isSecondaryMeasurementResult = false;
    }
    else
    {
      isSecondaryMeasurementResult = false;
    }
    
    return isSecondaryMeasurementResult;
  }

  /*
   * @author Peter Esbensen
   */
  private boolean checkClearOpenSignal(JointInspectionData jointInspectionData,
                                    ReconstructedSlice reconstructedSlice,
                                    ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = jointInspectionData.getSubtype();

    JointMeasurement lowerFilletThicknessMeasurement = ChipMeasurementAlgorithm.getLowerFilletThicknessMeasurement(
        jointInspectionData, reconstructedSlice.getSliceNameEnum());
    JointMeasurement filletThicknessMeasurement = ChipMeasurementAlgorithm.getClearFilletThicknessInMillimetersMeasurement(
        jointInspectionData, reconstructedSlice.getSliceNameEnum());
    float clearOpenSignal = filletThicknessMeasurement.getValue() - lowerFilletThicknessMeasurement.getValue();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();
    JointMeasurement clearOpenSignalMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL,
        MeasurementUnitsEnum.MILLIMETERS, jointInspectionData.getPad(),
        sliceNameEnum, clearOpenSignal);

    JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
    jointInspectionResult.addMeasurement(clearOpenSignalMeasurement);

    float maximumClearOpenSignalThresholdInMillimeters = (Float)subtype.getAlgorithmSettingValue(
      AlgorithmSettingEnum.CHIP_OPEN_CLEAR_OPEN_SIGNAL);
    boolean usingMetricUnits = Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric();
    float openSignalThresholdInUsersUnits = maximumClearOpenSignalThresholdInMillimeters;
    if (usingMetricUnits == false)
    {
      // convert to Mils
      openSignalThresholdInUsersUnits = MathUtil.convertMillimetersToMils(maximumClearOpenSignalThresholdInMillimeters);
    }
    //XCR-3461, component still fail even tombstone algo checking is pass
    if ((clearOpenSignal < maximumClearOpenSignalThresholdInMillimeters && jointInspectionData.jointInspectionResultExists()) && 
        (isSecondaryMeasurementResultPassed(jointInspectionData, reconstructedSlice) == false))
    {
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
          this, sliceNameEnum);
      openIndictment.addFailingMeasurement(clearOpenSignalMeasurement);
      openIndictment.addRelatedMeasurement(filletThicknessMeasurement);
      openIndictment.addRelatedMeasurement(lowerFilletThicknessMeasurement);
      jointInspectionResult.addIndictment(openIndictment);

      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      clearOpenSignalMeasurement,
                                                      openSignalThresholdInUsersUnits);
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    reconstructionRegion,
                                                    clearOpenSignalMeasurement,
                                                    openSignalThresholdInUsersUnits);
    return true;
  }


  /*
   * @author Peter Esbensen
   */
  private boolean checkOpaqueOpenSignal(ComponentInspectionData componentInspectionData,
                                     ReconstructedSlice reconstructedSlice,
                                     ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    ComponentMeasurement opaqueOpenSignalMeasurement  = null;
    JointMeasurement jointOpaqueOpenSignalMeasurement = null;
    float opaqueOpenSignalFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      opaqueOpenSignalMeasurement = ChipMeasurementAlgorithm.getOpaqueOpenSignalMeasurement(
        componentInspectionData, reconstructedSlice.getSliceNameEnum());
      opaqueOpenSignalFloatValue = opaqueOpenSignalMeasurement.getValue();
    }
    else
    {
      jointOpaqueOpenSignalMeasurement = ChipMeasurementAlgorithm.getOpaqueOpenSignalMeasurement(
        componentInspectionData.getPadOneJointInspectionData(), reconstructedSlice.getSliceNameEnum());
      opaqueOpenSignalFloatValue = jointOpaqueOpenSignalMeasurement.getValue();
    }

    float maximumOpaqueOpenSignalThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        CHIP_OPEN_OPAQUE_OPEN_SIGNAL);

    if (opaqueOpenSignalFloatValue > maximumOpaqueOpenSignalThreshold)
    {
      postFailedDiagnosticResult(opaqueOpenSignalMeasurement, jointOpaqueOpenSignalMeasurement,
        componentInspectionData, reconstructionRegion, sliceNameEnum, maximumOpaqueOpenSignalThreshold, IndictmentEnum.OPEN);   
      return false;
    }

    postPassDiagnosticResult(opaqueOpenSignalMeasurement, jointOpaqueOpenSignalMeasurement,
      componentInspectionData, sliceNameEnum, reconstructionRegion, maximumOpaqueOpenSignalThreshold);
    return true;
  }


  /*
   * @author Peter Esbensen
   */
  private boolean checkMinimumBodyLength(ComponentInspectionData componentInspectionData,
                                         ReconstructedSlice reconstructedSlice,
                                         ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    ComponentMeasurement bodyLengthPercentOfNominalMeasurement  = null;
    JointMeasurement jointBodyLengthPercentOfNominalMeasurement = null;
    float bodyLengthPercentOfNominalFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      bodyLengthPercentOfNominalMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthAsPercentOfNominalMeasurement(
        componentInspectionData, reconstructedSlice.getSliceNameEnum());
      bodyLengthPercentOfNominalFloatValue = bodyLengthPercentOfNominalMeasurement.getValue();
    }
    else
    {
      jointBodyLengthPercentOfNominalMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthAsPercentOfNominalMeasurement(
        componentInspectionData.getPadOneJointInspectionData(), reconstructedSlice.getSliceNameEnum());
      bodyLengthPercentOfNominalFloatValue = jointBodyLengthPercentOfNominalMeasurement.getValue();
    }

    float minimumBodyLengthThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        CHIP_OPEN_MINIMUM_BODY_LENGTH);

    if (bodyLengthPercentOfNominalFloatValue < minimumBodyLengthThreshold)
    {
      //LN: Not refactor code because need to indict related measurement.
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (bodyLengthPercentOfNominalMeasurement != null)
        {
          ComponentMeasurement bodyLengthMillimetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(
            componentInspectionData, reconstructedSlice.getSliceNameEnum(), true);
          ComponentIndictment openIndictment = new ComponentIndictment(IndictmentEnum.OPEN,
                                                                       this,
                                                                       sliceNameEnum,
                                                                       subtype,
                                                                       subtype.getJointTypeEnum());
          openIndictment.addFailingMeasurement(bodyLengthPercentOfNominalMeasurement);
          openIndictment.addRelatedMeasurement(bodyLengthMillimetersMeasurement);
          componentInspectionData.getComponentInspectionResult().addIndictment(openIndictment);
          AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                              componentInspectionData,
                                                              reconstructionRegion,
                                                              sliceNameEnum,
                                                              bodyLengthPercentOfNominalMeasurement,
                                                              minimumBodyLengthThreshold);
        }
      }
      else
      {
        if (jointBodyLengthPercentOfNominalMeasurement != null)
        {
          JointMeasurement jointBodyLengthMillimetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(
            componentInspectionData.getPadOneJointInspectionData(), reconstructedSlice.getSliceNameEnum(), true);
          JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
          openIndictment.addFailingMeasurement(jointBodyLengthPercentOfNominalMeasurement);
          openIndictment.addRelatedMeasurement(jointBodyLengthMillimetersMeasurement);
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(openIndictment);
          AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        jointBodyLengthMillimetersMeasurement,
                                                        minimumBodyLengthThreshold);
        }
      }
      return false;
    }

    postPassDiagnosticResult(bodyLengthPercentOfNominalMeasurement, jointBodyLengthPercentOfNominalMeasurement,
      componentInspectionData, sliceNameEnum, reconstructionRegion, minimumBodyLengthThreshold);
    
    return true;
  }
  
  /*
   * @author Cheah Lee Herng
   */
  private boolean checkMaximumBodyLength(ComponentInspectionData componentInspectionData,
                                         ReconstructedSlice reconstructedSlice,
                                         ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    ComponentMeasurement bodyLengthPercentOfNominalMeasurement  = null;
    JointMeasurement jointBodyLengthPercentOfNominalMeasurement = null;
    float bodyLengthPercentOfNominalFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      bodyLengthPercentOfNominalMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthAsPercentOfNominalMeasurement(
        componentInspectionData, reconstructedSlice.getSliceNameEnum());
      bodyLengthPercentOfNominalFloatValue = bodyLengthPercentOfNominalMeasurement.getValue();
    }
    else
    {
      jointBodyLengthPercentOfNominalMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthAsPercentOfNominalMeasurement(
        componentInspectionData.getPadOneJointInspectionData(), reconstructedSlice.getSliceNameEnum());
      bodyLengthPercentOfNominalFloatValue = jointBodyLengthPercentOfNominalMeasurement.getValue();
    }

    float maximumBodyLengthThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        CHIP_OPEN_MAXIMUM_BODY_LENGTH);

    if (bodyLengthPercentOfNominalFloatValue > maximumBodyLengthThreshold)
    {
      //LN: Not refactor code because need to indict related measurement.
      if (Config.isComponentLevelClassificationEnabled())
      {
        if (bodyLengthPercentOfNominalMeasurement != null)
        {
          ComponentMeasurement bodyLengthMillimetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(
            componentInspectionData, reconstructedSlice.getSliceNameEnum(), true);
          ComponentIndictment openIndictment = new ComponentIndictment(IndictmentEnum.OPEN,
                                                                   this,
                                                                   sliceNameEnum,
                                                                   subtype,
                                                                   subtype.getJointTypeEnum());
          openIndictment.addFailingMeasurement(bodyLengthPercentOfNominalMeasurement);
          openIndictment.addRelatedMeasurement(bodyLengthMillimetersMeasurement);
          componentInspectionData.getComponentInspectionResult().addIndictment(openIndictment);
          AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                          componentInspectionData,
                                                          reconstructionRegion,
                                                          sliceNameEnum,
                                                          bodyLengthPercentOfNominalMeasurement,
                                                          maximumBodyLengthThreshold);
        }
      }
      else
      {
        if (jointBodyLengthPercentOfNominalMeasurement != null)
        {
          JointMeasurement jointBodyLengthMillimetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyLengthInMillimetersMeasurement(
            componentInspectionData.getPadOneJointInspectionData(), reconstructedSlice.getSliceNameEnum(), true);
          JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
          openIndictment.addFailingMeasurement(jointBodyLengthPercentOfNominalMeasurement);
          openIndictment.addRelatedMeasurement(jointBodyLengthMillimetersMeasurement);
          componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(openIndictment);
          AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        jointBodyLengthMillimetersMeasurement,
                                                        maximumBodyLengthThreshold);
        }
      }
      return false;
    }

    postPassDiagnosticResult(bodyLengthPercentOfNominalMeasurement, jointBodyLengthPercentOfNominalMeasurement,
      componentInspectionData, sliceNameEnum, reconstructionRegion, maximumBodyLengthThreshold);      

    return true;
  }

  /*
   * @author Peter Esbensen
   */
  private boolean checkMinimumBodyWidth(ComponentInspectionData componentInspectionData,
                                        final ReconstructedSlice reconstructedSlice,
                                        final ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    ComponentMeasurement bodyWidthMeasurement   = null;
    JointMeasurement jointBodyWidthMeasurement  = null;
    float bodyWidthMeasurementFloatValue = 0.0f;
            
    if (Config.isComponentLevelClassificationEnabled())
    {
      bodyWidthMeasurement = ChipMeasurementAlgorithm.getBodyWidthMeasurement(componentInspectionData, SliceNameEnum.OPAQUE_CHIP_PAD);
      bodyWidthMeasurementFloatValue = bodyWidthMeasurement.getValue();
    }
    else
    {
      jointBodyWidthMeasurement = ChipMeasurementAlgorithm.getBodyWidthMeasurement(componentInspectionData.getPadOneJointInspectionData(), 
                                                                                   SliceNameEnum.OPAQUE_CHIP_PAD);
      bodyWidthMeasurementFloatValue = jointBodyWidthMeasurement.getValue();
    }

    final float minimumBodyWidthThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_WIDTH);

    if (bodyWidthMeasurementFloatValue < minimumBodyWidthThreshold)
    {
      postFailedDiagnosticResult(bodyWidthMeasurement, jointBodyWidthMeasurement,
        componentInspectionData, reconstructionRegion, sliceNameEnum, minimumBodyWidthThreshold, IndictmentEnum.OPEN);       
      return false;
    }

    postPassDiagnosticResult(bodyWidthMeasurement, jointBodyWidthMeasurement,
      componentInspectionData, sliceNameEnum, reconstructionRegion, minimumBodyWidthThreshold);    
    return true;
  }

  /*
   * Duplicate checkMinimumBodyWidth to check left and right pad width size ratio for opaque tombstone detection.
   * @author Lim, Lay Ngor
   */
  private boolean checkMinimumBodyWidthRatioForTombstone(ComponentInspectionData componentInspectionData,
                                        final ReconstructedSlice reconstructedSlice,
                                        final ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    SliceNameEnum bodySliceNameEnum = SliceNameEnum.OPAQUE_CHIP_PAD;
    JointMeasurement bodyWidthMeasurementJointOne = componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().
      getJointMeasurement(bodySliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH);    
    JointMeasurement bodyWidthMeasurementJointTwo = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getJointInspectionResult().
      getJointMeasurement(bodySliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH);

    float minimumBodyThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_BODY_THICKNESS);
//    minimumBodyThickness = minimumBodyThickness * 0.9f;//tolerance to allow 90%
    final float minimumCenterThickness = minimumBodyThickness * (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_PARTIAL_FILLET_THICKNESS_PERCENTAGE) / 100.f;
        
    //Too loose unless they wanna use tighter fillet thickness
//    float minimumFilletThickness = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_INSUFFICIENT_MINIMUM_FILLET_THICKNESS);
    //Can be optimise for whole function coding
    JointMeasurement padOneFilletThicknessMeasurement = componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().
      getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS);
    JointMeasurement padTwoFilletThicknessMeasurement = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getJointInspectionResult().
      getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS);   
          
    final float minimumBodyWidthRatioThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_FILLET_WIDTH_JOINT_RATIO);    
    boolean jointOnePassed = true;  
    boolean jointTwoPassed = true;  
    float ratio = 0.f;
    final float algoSettingThreshold = minimumBodyWidthRatioThreshold;
    if(bodyWidthMeasurementJointTwo.getValue() > bodyWidthMeasurementJointOne.getValue())
    {
      if(bodyWidthMeasurementJointTwo.getValue() > 0)
      {
        ratio = bodyWidthMeasurementJointOne.getValue() / bodyWidthMeasurementJointTwo.getValue();//small value on top
        if(ratio < algoSettingThreshold && padOneFilletThicknessMeasurement.getValue() < minimumCenterThickness)//double checking
            jointOnePassed = false; //very different in joint width   
      }
      else
        jointTwoPassed = false;//joint two width zero, means no width detected.
    }
    else
    {
      if(bodyWidthMeasurementJointOne.getValue() > 0)
      {
        ratio = bodyWidthMeasurementJointTwo.getValue() / bodyWidthMeasurementJointOne.getValue();//small value on top
        if(ratio < algoSettingThreshold && padTwoFilletThicknessMeasurement.getValue() < minimumCenterThickness)//double checking
            jointTwoPassed = false; //very different in joint width             
      }
      else
        jointOnePassed = false;//joint two width zero, means no width detected.
    }
    
    //Indict joint level information - now pass one didnt show this 
    JointMeasurement filletWidthAcrossJointRatioJointOneMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH_JOINT_RATIO,
      MeasurementUnitsEnum.NONE,
      componentInspectionData.getPadOneJointInspectionData().getPad(),
      sliceNameEnum,
      ratio);
    JointInspectionResult jointOneInspectionResult = componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult();
    jointOneInspectionResult.addMeasurement(filletWidthAcrossJointRatioJointOneMeasurement);    
      
    if (jointOnePassed == false)
    {
      JointIndictment openJointIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
      openJointIndictment.addFailingMeasurement(filletWidthAcrossJointRatioJointOneMeasurement);
      openJointIndictment.addFailingMeasurement(padOneFilletThicknessMeasurement);
      openJointIndictment.addRelatedMeasurement(bodyWidthMeasurementJointOne);
      jointOneInspectionResult.addIndictment(openJointIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
        componentInspectionData.getPadOneJointInspectionData(),
        reconstructionRegion,
        sliceNameEnum,
        filletWidthAcrossJointRatioJointOneMeasurement,
        minimumBodyWidthRatioThreshold);
    }
    else
    {
      //In order to standardize, we post it as pass joint intead of pass component.
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
        componentInspectionData.getPadOneJointInspectionData(),
        sliceNameEnum,
        reconstructionRegion,
        filletWidthAcrossJointRatioJointOneMeasurement,
        minimumBodyWidthRatioThreshold);
    }

    JointMeasurement filletWidthAcrossJointRatioJointTwoMeasurement = new JointMeasurement(this,
      MeasurementEnum.CHIP_MEASUREMENT_FILLET_WIDTH_JOINT_RATIO,
      MeasurementUnitsEnum.NONE,
      componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getPad(),
      sliceNameEnum,
      ratio); 
    JointInspectionResult jointTwoInspectionResult = componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices().getJointInspectionResult();
    jointTwoInspectionResult.addMeasurement(filletWidthAcrossJointRatioJointTwoMeasurement);    
    if (jointTwoPassed == false)
    {
      JointIndictment openJointIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
      openJointIndictment.addFailingMeasurement(filletWidthAcrossJointRatioJointTwoMeasurement);
      openJointIndictment.addFailingMeasurement(padTwoFilletThicknessMeasurement);      
      openJointIndictment.addRelatedMeasurement(bodyWidthMeasurementJointTwo);
      jointTwoInspectionResult.addIndictment(openJointIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
        componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices(),
        reconstructionRegion,
        sliceNameEnum,
        filletWidthAcrossJointRatioJointTwoMeasurement,
        minimumBodyWidthRatioThreshold);
    }    
    else
    {
      //In order to standardize, we post it as pass joint intead of pass component.
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
        componentInspectionData.getPadTwoJointInspectionDataOnTwoPinDevices(),
        sliceNameEnum,
        reconstructionRegion,
        filletWidthAcrossJointRatioJointTwoMeasurement,
        minimumBodyWidthRatioThreshold);
    }

    return !(jointTwoPassed == false || jointTwoPassed == false);
  }

  /*
   * @author Peter Esbensen
   */
  private boolean checkOpaqueMinimumBodyThickness(ComponentInspectionData componentInspectionData,
                                         ReconstructedSlice reconstructedSlice,
                                         ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    //Khaw Chek Hau - XCR2567 : Opaque capacitor shouldn't select clear slice measurement when run test on capacitor subtype
    ComponentMeasurement thicknessInMillemetersMeasurement  = null;
    JointMeasurement jointThicknessInMillemetersMeasurement = null;
    float thicknessInMilimetersMeasurementFloatValue = 0.0f;
    if (Config.isComponentLevelClassificationEnabled())
    {
      thicknessInMillemetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyThicknessInMillimetersMeasurement(
                                                                                                    componentInspectionData,
                                                                                                    sliceNameEnum);
      thicknessInMilimetersMeasurementFloatValue = thicknessInMillemetersMeasurement.getValue();
    }
    else
    {
      jointThicknessInMillemetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyThicknessInMillimetersMeasurement(
                                                                                                    componentInspectionData.getPadOneJointInspectionData(),
                                                                                                    sliceNameEnum);
      thicknessInMilimetersMeasurementFloatValue = jointThicknessInMillemetersMeasurement.getValue();
    }

    float minimumBodyThicknessThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        CHIP_OPEN_MINIMUM_BODY_THICKNESS);

    if (thicknessInMilimetersMeasurementFloatValue < minimumBodyThicknessThreshold)
    {
      postFailedDiagnosticResult(thicknessInMillemetersMeasurement, jointThicknessInMillemetersMeasurement,
        componentInspectionData, reconstructionRegion, sliceNameEnum, minimumBodyThicknessThreshold, IndictmentEnum.MISSING);         
      return false;
    }

    postPassDiagnosticResult(thicknessInMillemetersMeasurement, jointThicknessInMillemetersMeasurement,
      componentInspectionData, sliceNameEnum, reconstructionRegion, minimumBodyThicknessThreshold);    
    return true;
  }

  /*
   * @author George Booth
   */
  private boolean checkClearMinimumBodyThickness(ComponentInspectionData componentInspectionData,
                                         ReconstructedSlice reconstructedSlice,
                                         ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    ComponentMeasurement thicknessInMillemetersMeasurement  = null;
    JointMeasurement jointThicknessInMillemetersMeasurement = null;
    float bodyThicknessRequiredToTestAsOpaqueInMillimetersFloatValue = 0.0f;
    
    if (Config.isComponentLevelClassificationEnabled())
    {
      thicknessInMillemetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyThicknessInMillimetersMeasurement(
        componentInspectionData,
        SliceNameEnum.CLEAR_CHIP_PAD);
      bodyThicknessRequiredToTestAsOpaqueInMillimetersFloatValue = thicknessInMillemetersMeasurement.getValue();
    }
    else
    {
      jointThicknessInMillemetersMeasurement = ChipMeasurementAlgorithm.getComponentBodyThicknessInMillimetersMeasurement(
        componentInspectionData.getPadOneJointInspectionData(),
        SliceNameEnum.CLEAR_CHIP_PAD);
      bodyThicknessRequiredToTestAsOpaqueInMillimetersFloatValue = jointThicknessInMillemetersMeasurement.getValue();
    }

    float minimumBodyThicknessThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        CHIP_OPEN_CLEAR_MINIMUM_BODY_THICKNESS);

    // check if clear min thickness is reasonable
    float bodyThicknessRequiredToTestAsOpaqueInMillimeters = (Float)subtype.
             getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_MEASUREMENT_BODY_THICKNESS_REQUIRED_TO_TEST_AS_OPAQUE);
    if (minimumBodyThicknessThreshold >= bodyThicknessRequiredToTestAsOpaqueInMillimeters)
    {
      AlgorithmUtil.raiseAlgorithmWarning(new LocalizedString("ALGDIAG_CHIP_WARNING_BAD_MINIMUM_CLEAR_THICKNESS_KEY",
                                                              new String[]
                                                              {componentInspectionData.getComponent().getReferenceDesignator()}));
    }

    if (bodyThicknessRequiredToTestAsOpaqueInMillimetersFloatValue < minimumBodyThicknessThreshold)
    {
      postFailedDiagnosticResult(thicknessInMillemetersMeasurement, jointThicknessInMillemetersMeasurement,
        componentInspectionData, reconstructionRegion, sliceNameEnum, minimumBodyThicknessThreshold, IndictmentEnum.MISSING);             
      return false;
    }

    postPassDiagnosticResult(thicknessInMillemetersMeasurement, jointThicknessInMillemetersMeasurement,
      componentInspectionData, sliceNameEnum, reconstructionRegion, minimumBodyThicknessThreshold);    
    return true;
  }

  /*
   * @author Peter Esbensen
   */
  private boolean checkFilletGapMeasurements(JointInspectionData padOneJointInspectionData,
                                          JointInspectionData padTwoJointInspectionData,
                                          ReconstructedSlice reconstructedSlice,
                                          ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(padOneJointInspectionData != null);
    Assert.expect(padTwoJointInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = padOneJointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    JointMeasurement padOneFilletGapMeasurement = ChipMeasurementAlgorithm.getFilletGapMeasurement(
        padOneJointInspectionData, reconstructedSlice.getSliceNameEnum());
    JointMeasurement padTwoFilletGapMeasurement = ChipMeasurementAlgorithm.getFilletGapMeasurement(
        padTwoJointInspectionData, reconstructedSlice.getSliceNameEnum());

    float maximumFilletGapMeasurementThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        CHIP_OPEN_MAXIMUM_FILLET_GAP);

    JointInspectionResult padOneJointInspectionResult = padOneJointInspectionData.getJointInspectionResult();
    JointInspectionResult padTwoJointInspectionResult = padTwoJointInspectionData.getJointInspectionResult();

    if (padOneFilletGapMeasurement.getValue() > maximumFilletGapMeasurementThreshold)
    {
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
          this, sliceNameEnum);
      openIndictment.addFailingMeasurement(padOneFilletGapMeasurement);
      padOneJointInspectionResult.addIndictment(openIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      padOneFilletGapMeasurement,
                                                      maximumFilletGapMeasurementThreshold);
      return false;
    }
    else
    {
      AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                      padOneJointInspectionData,
                                                      sliceNameEnum,
                                                      reconstructionRegion,
                                                      padOneFilletGapMeasurement,
                                                      maximumFilletGapMeasurementThreshold);
    }

    if (padTwoFilletGapMeasurement.getValue() > maximumFilletGapMeasurementThreshold)
    {
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN,
          this, sliceNameEnum);
      openIndictment.addFailingMeasurement(padTwoFilletGapMeasurement);
      padTwoJointInspectionResult.addIndictment(openIndictment);
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      padTwoJointInspectionData,
                                                      reconstructionRegion,
                                                      sliceNameEnum,
                                                      padTwoFilletGapMeasurement,
                                                      maximumFilletGapMeasurementThreshold);

      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    padTwoJointInspectionData,
                                                    sliceNameEnum,
                                                    reconstructionRegion,
                                                    padTwoFilletGapMeasurement,
                                                    maximumFilletGapMeasurementThreshold);
    return true;
  }

  /*
   * @author Lim, Seng-Yew on 13-03-2009 Friday the 13th
   * @
   */
  private boolean checkOpaqueComponentTiltRatio(ComponentInspectionData componentInspectionData,
                                     ReconstructedSlice reconstructedSlice,
                                     ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructedSlice != null);
    Assert.expect(reconstructionRegion != null);

    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();

    ComponentMeasurement opaqueComponentTiltRatioMeasurement  = null;
    JointMeasurement jointOpaqueComponentTiltRatioMeasurement = null;
    float opaqueComponentTiltRatioFloatValue = 0.0f;
            
    if (Config.isComponentLevelClassificationEnabled())
    {
      opaqueComponentTiltRatioMeasurement = ChipMeasurementAlgorithm.getOpaqueComponentTiltRatioMeasurement(
        componentInspectionData, reconstructedSlice.getSliceNameEnum());
      opaqueComponentTiltRatioFloatValue = opaqueComponentTiltRatioMeasurement.getValue();
    }
    else
    {
      jointOpaqueComponentTiltRatioMeasurement = ChipMeasurementAlgorithm.getOpaqueComponentTiltRatioMeasurement(
        componentInspectionData.getPadOneJointInspectionData(), reconstructedSlice.getSliceNameEnum());
      opaqueComponentTiltRatioFloatValue = jointOpaqueComponentTiltRatioMeasurement.getValue();
    }

    float maximumOpaqueComponentTiltRatioThreshold = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.
        CHIP_MEASUREMENT_COMPONENT_TILT_RATIO_MAX);

    if (opaqueComponentTiltRatioFloatValue > maximumOpaqueComponentTiltRatioThreshold)
    {
      postFailedDiagnosticResult(opaqueComponentTiltRatioMeasurement, jointOpaqueComponentTiltRatioMeasurement,
        componentInspectionData, reconstructionRegion, sliceNameEnum, maximumOpaqueComponentTiltRatioThreshold, IndictmentEnum.OPEN);                   
      return false;
    }

    postPassDiagnosticResult(opaqueComponentTiltRatioMeasurement, jointOpaqueComponentTiltRatioMeasurement, 
      componentInspectionData, sliceNameEnum, reconstructionRegion, maximumOpaqueComponentTiltRatioThreshold);
    return true;
  }

  /**
   * Post failed diagnostic results base on joint or component level from config setting.
   * @author Lim, Lay Ngor
   */
  void postFailedDiagnosticResult(ComponentMeasurement componentMeasurement, JointMeasurement jointMeasurement,
    ComponentInspectionData componentInspectionData, ReconstructionRegion reconstructionRegion,
    SliceNameEnum sliceNameEnum, float thresholdValue, IndictmentEnum indictmentEnum)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);
    
    Subtype subtype = componentInspectionData.getPadOneJointInspectionData().getSubtype();
    if (Config.isComponentLevelClassificationEnabled())
    {
      if (componentMeasurement != null)//Skip indictment instead of assert
      {
        ComponentIndictment openIndictment = new ComponentIndictment(indictmentEnum,
                                                                     this,
                                                                     sliceNameEnum,
                                                                     subtype,
                                                                     subtype.getJointTypeEnum());
        openIndictment.addFailingMeasurement(componentMeasurement);
        componentInspectionData.getComponentInspectionResult().addIndictment(openIndictment);
        AlgorithmUtil.postFailingComponentTextualDiagnostic(this,
                                                            componentInspectionData,
                                                            reconstructionRegion,
                                                            sliceNameEnum,
                                                            componentMeasurement,
                                                            thresholdValue);
      }
    }
    else
    {
      if (jointMeasurement != null)//Skip indictment instead of assert
      {
        JointIndictment openIndictment = new JointIndictment(indictmentEnum, this, sliceNameEnum);
        openIndictment.addFailingMeasurement(jointMeasurement);
        componentInspectionData.getPadOneJointInspectionData().getJointInspectionResult().addIndictment(openIndictment);
        AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        reconstructionRegion,
                                                        sliceNameEnum,
                                                        jointMeasurement,
                                                        thresholdValue);
      }
    }
  }
    
  /**
   * Post pass diagnostic results base on joint or component level from config setting.
   * @author Lim, Lay Ngor
   */
  void postPassDiagnosticResult(ComponentMeasurement componentMeasurement, JointMeasurement jointMeasurement,
    ComponentInspectionData componentInspectionData, SliceNameEnum sliceNameEnum, 
    ReconstructionRegion reconstructionRegion, float thresholdValue)
  {
    Assert.expect(componentInspectionData != null);
    Assert.expect(reconstructionRegion != null);
    Assert.expect(sliceNameEnum != null);    

    if (Config.isComponentLevelClassificationEnabled())
    {
      if (componentMeasurement != null)//Skip indictment instead of assert
      {
        AlgorithmUtil.postPassingComponentTextualDiagnostic(this,
                                                            componentInspectionData, 
                                                            sliceNameEnum,
                                                            reconstructionRegion,
                                                            componentMeasurement,
                                                            thresholdValue);
      }
    }
    else
    {
      if (jointMeasurement != null)//Skip indictment instead of assert
      {
        AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                        componentInspectionData.getPadOneJointInspectionData(),
                                                        sliceNameEnum,
                                                        reconstructionRegion,
                                                        jointMeasurement,
                                                        thresholdValue);
      }
    }
  }

  /**
   * Detects opens using the center:upperFillet thickness percent base on joint.
   * Gullwing Open detection: detectOpensUsingCenterToHeelThicknessPercent
   *
   * @author Lim, Lay Ngor - Clear Tombstone
   */
  private boolean checkClearCenterToUpperFilletThicknessPercent(JointInspectionData jointInspectionData,
                                     ReconstructedSlice reconstructedSlice)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);    

    Subtype subtype = jointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();    

    // Check to see if the center:heel thickness percent is within acceptable limits.
    final float MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT =
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENTAGE);
    JointMeasurement centerToUpperFilletThicknessPercentMeasurement =
        ChipMeasurementAlgorithm.getCenterToUpperFilletThicknessPercentMeasurement(jointInspectionData, sliceNameEnum);
    float centerToHeelThicknessPercent = centerToUpperFilletThicknessPercentMeasurement.getValue();
    //XCR-3461, component still fail even tombstone algo checking is pass
    if (MathUtil.fuzzyGreaterThan(centerToHeelThicknessPercent, MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT) && 
        (isSecondaryMeasurementResultPassed(jointInspectionData, reconstructedSlice) == false))
    {
      //Failed on Center to upper fillet thickness percent      
      // Center:upper fillet thickness ratio is outside of acceptable limits!
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      centerToUpperFilletThicknessPercentMeasurement,
                                                      MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
     // Create a new indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);

      JointMeasurement centerFilletThicknessJointMeasurement = jointInspectionData.getJointInspectionResult().
        getJointMeasurement(sliceNameEnum, MeasurementEnum.CHIP_MEASUREMENT_CENTER_FILLET_THICKNESS);    
      // Tie the applicable measurements to the indictment.
      openIndictment.addFailingMeasurement(centerToUpperFilletThicknessPercentMeasurement);
      openIndictment.addRelatedMeasurement(centerFilletThicknessJointMeasurement);

      // Add the indictment to the JointInspectionResult.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    centerToUpperFilletThicknessPercentMeasurement,
                                                    MAXIMUM_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT);
    return true;
  }
  
  /**
   * Perform Second Level Open Detection Checking which include Roundness, Solder Area & Template Matching.
   * @param jointInspectionData
   * @param reconstructedSlice
   * @param detectionMethod
   * @return 
   * @author Lim, Lay Ngor
   */
  private boolean checkSecondLevelDetectionMethod(JointInspectionData jointInspectionData,
                                                  ReconstructedSlice reconstructedSlice,
                                                  String detectionMethod) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);    

    //Perform double checking if either one is enable
    if(detectionMethod.equalsIgnoreCase(_SOLDER_AREA_DETECTION))
      return checkClearSolderAreaPercent(jointInspectionData, reconstructedSlice);
    else if(detectionMethod.equalsIgnoreCase(_ROUNDNESS_DETECTION))
      return checkClearRoundnessPercent(jointInspectionData, reconstructedSlice);
//      return checkMatchingOrRoundnessPercentage(jointInspectionData, reconstructedSlice);
    else if(detectionMethod.equalsIgnoreCase(_TEMPLATE_MATCH_DETECTION))
      return checkClearTemplateMatchingPercent(jointInspectionData, reconstructedSlice);
    else if(detectionMethod.equalsIgnoreCase(_DISABLED_SECONDARY_DETECTION))
      return false;//Not suppose to come to this line as we check this condition at the caller function.
    else//Not suppose to happen, another disable case is checking outside
      Assert.expect(false, "Failed to obtain correct detection method.");
      
     return false;
  }
  
  /**
   * Detects opens using the solder area percent base on joint. The more solder area means the rounder the joint.
   * The rounder the pad the chances of getting tombstone is higher.
   *
   * @author Lim, Lay Ngor - Clear Tombstone
   */
  private boolean checkClearSolderAreaPercent(JointInspectionData jointInspectionData,
                                              ReconstructedSlice reconstructedSlice)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);    

    Subtype subtype = jointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();    
    
    // Check to see if the center:heel thickness percent is within acceptable limits.
    final float MAXIMUM_ACCEPTABLE_PAD_SOLDER_AREA_PERCENT = //50.f;
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_SOLDER_AREA_PERCENTAGE);
    JointMeasurement padSolderAreaPercentMeasurement =
        ChipMeasurementAlgorithm.getPadSolderAreaPercentMeasurement(jointInspectionData, sliceNameEnum);
    float padSolderAreaPercent = padSolderAreaPercentMeasurement.getValue();
    if (MathUtil.fuzzyGreaterThan(padSolderAreaPercent, MAXIMUM_ACCEPTABLE_PAD_SOLDER_AREA_PERCENT))
    {
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      padSolderAreaPercentMeasurement,
                                                      MAXIMUM_ACCEPTABLE_PAD_SOLDER_AREA_PERCENT);

      // Create a new indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
  
      // Tie the applicable measurements to the indictment.
      openIndictment.addFailingMeasurement(padSolderAreaPercentMeasurement);

      // Add the indictment to the JointInspectionResult.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    padSolderAreaPercentMeasurement,
                                                    MAXIMUM_ACCEPTABLE_PAD_SOLDER_AREA_PERCENT);
    return true;
  }
  
 /**
   * Detects opens using the pad roundness percent base on joint.
   * The rounder the pad, the chances of getting of tombstone is higher.
   *
   * @author ShengChuan - Clear Tombstone Roundness
   * @author Lim, Lay Ngor - Code refactor
   */
  private boolean checkClearRoundnessPercent(JointInspectionData jointInspectionData,
                                              ReconstructedSlice reconstructedSlice)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);    

    Subtype subtype = jointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();          
      
    // Check to see if the center:heel thickness percent is within acceptable limits.
    final float MAXIMUM_ACCEPTABLE_PAD_ROUNDNESS_PERCENT = //50.f;
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MAXIMUM_ROUNDNESS_PERCENTAGE);
    JointMeasurement padRoundnessPercentMeasurement =
        ChipMeasurementAlgorithm.getRoundnessPercentagesMeasurement(jointInspectionData, sliceNameEnum);
    float padRoundnessPercent = padRoundnessPercentMeasurement.getValue();
    if (MathUtil.fuzzyGreaterThan(padRoundnessPercent, MAXIMUM_ACCEPTABLE_PAD_ROUNDNESS_PERCENT))
    {
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      padRoundnessPercentMeasurement,
                                                      MAXIMUM_ACCEPTABLE_PAD_ROUNDNESS_PERCENT);

      // Create a new indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
  
      // Tie the applicable measurements to the indictment.
      openIndictment.addFailingMeasurement(padRoundnessPercentMeasurement);

      // Add the indictment to the JointInspectionResult.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    padRoundnessPercentMeasurement,
                                                    MAXIMUM_ACCEPTABLE_PAD_ROUNDNESS_PERCENT);
    return true;
  }
  
  /**
   * Check the matching Percentage of joint whether it is identical to tombstone defect image or not.
   *
   * @author ShengChuan - Clear Tombstone TemplateMatching
   * @author Lim, Lay Ngor - Code refactor
   */
  private boolean checkClearTemplateMatchingPercent(JointInspectionData jointInspectionData,
                                              ReconstructedSlice reconstructedSlice) throws DatastoreException
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(reconstructedSlice != null);    

    Subtype subtype = jointInspectionData.getSubtype();
    SliceNameEnum sliceNameEnum = reconstructedSlice.getSliceNameEnum();          
    Pad pad = jointInspectionData.getPad();
    Component component = jointInspectionData.getPad().getComponent();
    //XCR-3126
    //no need to further check if no image learnts, warning on template absent is notified in measurement class
    if (subtype.isExpectedImageTemplateLearned() == false || component.hasExpectedImageTemplateLearning(pad, component.getBoard(), sliceNameEnum, reconstructedSlice.getOrthogonalImage()) == false)
      return true;
 
    // Check to see if the center:heel thickness percent is within acceptable limits.
    final float MINIMUM_ACCEPTABLE_PAD_TEMPLATE_MATCH_PERCENT = 
        (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.CHIP_OPEN_MINIMUM_MATCHING_PERCENTAGE);
        
    JointMeasurement padTemplateMatchPercentMeasurement =
        ChipMeasurementAlgorithm.getMatchingPercentagesMeasurement(jointInspectionData, sliceNameEnum);
    
        JointMeasurement matchingPercentageMeasurement = new JointMeasurement(this,
        MeasurementEnum.CHIP_MEASUREMENT_TEMPLATE_MATCHING,
        MeasurementUnitsEnum.PERCENT, jointInspectionData.getPad(),
        sliceNameEnum, padTemplateMatchPercentMeasurement.getValue());
    
    float padTemplateMatchPercent = padTemplateMatchPercentMeasurement.getValue();
    if (MathUtil.fuzzyLessThan(padTemplateMatchPercent, MINIMUM_ACCEPTABLE_PAD_TEMPLATE_MATCH_PERCENT))
    {
      AlgorithmUtil.postFailingJointTextualDiagnostic(this,
                                                      jointInspectionData,
                                                      jointInspectionData.getInspectionRegion(),
                                                      sliceNameEnum,
                                                      matchingPercentageMeasurement,//padTemplateMatchPercentMeasurement,
                                                      MINIMUM_ACCEPTABLE_PAD_TEMPLATE_MATCH_PERCENT);

      // Create a new indictment.
      JointIndictment openIndictment = new JointIndictment(IndictmentEnum.OPEN, this, sliceNameEnum);
  
      // Tie the applicable measurements to the indictment.
      openIndictment.addFailingMeasurement(padTemplateMatchPercentMeasurement);

      // Add the indictment to the JointInspectionResult.
      JointInspectionResult jointInspectionResult = jointInspectionData.getJointInspectionResult();
      jointInspectionResult.addIndictment(openIndictment);

      // Flag the joint as failing.
      return false;
    }

    AlgorithmUtil.postPassingJointTextualDiagnostic(this,
                                                    jointInspectionData,
                                                    sliceNameEnum,
                                                    jointInspectionData.getInspectionRegion(),
                                                    padTemplateMatchPercentMeasurement,
                                                    MINIMUM_ACCEPTABLE_PAD_TEMPLATE_MATCH_PERCENT);
    return true;
  }
}
