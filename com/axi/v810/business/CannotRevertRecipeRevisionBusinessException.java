/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.axi.v810.business;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 *
 * @author weng-jian.eoh
 */
public class CannotRevertRecipeRevisionBusinessException extends XrayTesterException
{
  public CannotRevertRecipeRevisionBusinessException(String saveRecipe,String revision)
  {
    super((new LocalizedString("DS_CANNOT_SAVE_PROJECT_IN_DIFF_REVISION_ERROR_KEY",new Object[]{saveRecipe,revision})));
  }
}
