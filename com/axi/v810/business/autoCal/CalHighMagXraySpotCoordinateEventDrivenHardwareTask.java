package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;


/**
 * Calibration (adjustment) to determine the stage location that puts
 * the system fiducial directly under the xray source. This measurement
 * corrects for most of the mechanical slop in the system.
 * @author Wei Chin
 */
public class CalHighMagXraySpotCoordinateEventDrivenHardwareTask extends EventDrivenHardwareTask
{
  private static CalHighMagXraySpotCoordinateEventDrivenHardwareTask _instance;

  // Printable name for this procedure
  private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_HIGH_MAG_XRAY_SPOT_NAME_KEY");

  private static Config _config = Config.getInstance();

  // This cal is called on the first event after the timeout occurs.
  private static final int _EVENT_FREQUENCY = 1;
  private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_EXECUTE_EVERY_TIMEOUT_SECONDS);

  // A template match quality metric below this will fail the cal (triangle not in image)
  // The range is from -1.0 to 1.0 where 1.0 is a perfect match
  private static final float _QUALITYTHRESHOLD = 0.89F;

  // Define the region to be imaged for this cal
  // X & Y are relative to the system fiducial
  
  private static final int _Y_OFFSET_IN_NANOMETERS = 0;
  private static final int _REGION_TO_IMAGE_X_NANOMETERS = -7200000;// - 100000;
  private static final int _REGION_TO_IMAGE_Y_NANOMETERS = -8480000 - _Y_OFFSET_IN_NANOMETERS;// - 11548000;

  // These variables are determined based on system values that are not known
  // at object instanciation time (MechanicalConversions). They are initialized in
  // the setUp method but are set to values that will fail here
  private static int _REGION_TO_IMAGE_WIDTH_NANOMETERS = -1;
  private static int _REGION_TO_IMAGE_HEIGHT_NANOMETERS = -1;

  // The template to match within the image is generated from these parameters
  private static int _templateBorderWidthPixels = 20;
  private static int _templateBorderHeightPixels = 20;
  private static int _templateSizeXPixels = XrayTester.getSystemFiducialWidthInNanometers()/
                                            MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
  private static int _templateSizeYPixels = XrayTester.getSystemFiducialHeightInNanometers()/
                                            MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
  private static Image _template =
      CalibrationSupportUtil.createTemplateImageOfSystemFiducial(_templateBorderWidthPixels,
      _templateBorderHeightPixels, _templateSizeXPixels, _templateSizeYPixels);

  private static final String _logDirectory = Directory.getXraySpotCalLogDir();
  private static final long _logTimeoutSeconds = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_LOG_RESULTS_EVERY_TIMEOUT_SECONDS);
  private static final long _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000L;
  private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_MAX_LOG_FILES_IN_DIRECTORY);

  // Internal index to identify results & limits
  private static int _index = 0;
  private static final int _FIDUCIAL_FOUND_RESULT_INDEX = _index++;
  private static final int _MATCH_QUALITY_RESULT_INDEX = _index++;
  private static final int _STAGE_X_RESULT_INDEX = _index++;
  private static final int _STAGE_Y_RESULT_INDEX = _index++;
  private static final int _MAX_RESULT_INDEX = _index++; // Must be last, used to size arrays

  private int _defaultXLocationNanometers = _config.getIntValue(HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_X_NANOMETERS);
  private int _defaultYLocationNanometers = _config.getIntValue(HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_Y_NANOMETERS);
  private List<VirtualProjection> _virtualProjections = new ArrayList<VirtualProjection>();
  private DoubleRef _matchQuality = new DoubleRef(0.0F);
  private boolean _fiducialFound = false;
  private Result[] _localResults;
  private Limit[] _localLimits;
  private ImageCoordinate _templateCoordinates;
  private ImageCoordinate _expectedLocation;
  private ImageCoordinate _actualLocation;
  private ImageCoordinate _offset;
  private int _oldXraySpotXPosition;
  private int _oldXraySpotYPosition;
  private int _newXraySpotXPosition;
  private int _newXraySpotYPosition;
  private DirectoryLoggerAxi _logger;
  private CalHighMagSystemFiducialEventDrivenHardwareTask _calSystemOffsets = CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance();

  /**
   * @author Eddie Williamson
   */
  static
  {
    Assert.expect(_MAX_RESULT_INDEX > _FIDUCIAL_FOUND_RESULT_INDEX);
    Assert.expect(_MAX_RESULT_INDEX > _MATCH_QUALITY_RESULT_INDEX);
    Assert.expect(_MAX_RESULT_INDEX > _STAGE_X_RESULT_INDEX);
    Assert.expect(_MAX_RESULT_INDEX > _STAGE_Y_RESULT_INDEX);
  }

  /**
   * @author Eddie Williamson
   */
  private CalHighMagXraySpotCoordinateEventDrivenHardwareTask()
  {
    super(_NAME_OF_CAL,
          _EVENT_FREQUENCY,
          _EXECUTE_EVERY_TIMEOUT_SECONDS,
          ProgressReporterEnum.XRAY_SPOT_ADJUSTMENT_TASK);

    _logger = new DirectoryLoggerAxi(_logDirectory, FileName.getHighMagXraySpotLogFileBasename(),
                                     FileName.getLogFileExtension(), _maxLogFilesInDirectory);

    _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

    //set the timestamp enum to save the last time this cal was run
    setTimestampEnum(HardwareCalibEnum.XRAY_SPOT_CALIBRATION_LAST_TIME_RUN);

    _isHighMagTask = true;
  }


  /**
   * @author Eddie Williamson
   */
  public static synchronized CalHighMagXraySpotCoordinateEventDrivenHardwareTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new CalHighMagXraySpotCoordinateEventDrivenHardwareTask();
    }
    return _instance;
  }

  /**
   * Logs detailed information about the cal and saves images when the timeout
   * is exceeded. The log information is formatted as comma separated values to
   * it can be imported into Excel for manual analysis.
   * @param forceLogging If true then override the timeout and log anyway.
   * @author Eddie Williamson
   */
  private void logInformation(Boolean forceLogging) throws DatastoreException
  {
    StringBuilder logString = new StringBuilder();
    boolean didWeLog = false;

    // Create header lines
    logString.append(_NAME_OF_CAL + "\n");
    logString.append(","); // offset first column
    logString.append("Camera,FiducialFound,MatchQuality,");
    logString.append("ExpectedCol,ExpectedRow,");
    // ProjectionRequest Header
    logString.append("Direction,RegionX(nm),RegionY(nm),RegionWidth(nm),RegionHeight(nm),");
    // ProjectionInfo Header
    logString.append("StageXCameraCenter(nm),StageYCameraCenter(nm),ImageColOnCamera,ImageRowOnCamera,");
    logString.append("ImageFileName,");
    if (_fiducialFound)
    {
      logString.append("ActualCol,ActualRow,");
      logString.append("OffsetCols,OffsetRows,");
      logString.append("NewLocationX(nm),NewLocationY(nm),");
      logString.append("OldLocationX(nm),OldLocationY(nm),");
      logString.append("MagnificationInfo,"); // Khang Wah, add mgnification info into CD&A log
    }
    logString.append("\n");


    for (VirtualProjection virtualProjection : _virtualProjections)
    {
      logString.append(","); // offset first column

      AbstractXrayCamera camera = virtualProjection.getCamera();
      logString.append(camera + "," + _fiducialFound + "," + _matchQuality.getValue() + ",");

      // Log expected location of fiducial
      logString.append(_expectedLocation.getX() + "," + _expectedLocation.getY() + ",");

      // Log the ProjectionRequest data
      logString.append(CalibrationSupportUtil.ProjectionRequestToCSVString(virtualProjection.getProjectionRequestThreadTask()) +
                       ",");

      // Log the ProjectionInfo data
      logString.append(CalibrationSupportUtil.ProjectionInfoToCSVString(virtualProjection.getProjectionRequestThreadTask().
          getProjectionInfo()) + ",");

      if (_fiducialFound)
      {
        // Log filename for reference, if logging we save the images also
        String filename = "cam_high_" + camera.getId() + "_" + _config.getSystemCurrentHighMagnificationForLogging() + ".png";
        logString.append(filename + ",");

        // Log actual fiducial location within the image
        logString.append(_actualLocation.getX() + "," + _actualLocation.getY() + ",");

        // Log offset from expected to actual
        logString.append(_offset.getX() + "," + _offset.getY() + ",");

        // Log new xray spot location
        logString.append(_newXraySpotXPosition + "," + _newXraySpotYPosition + ",");

        // Log old xray spot location
        logString.append(_oldXraySpotXPosition + "," + _oldXraySpotYPosition + ",");
        
        // Log Magnification info
        logString.append(_config.getSystemCurrentHighMagnificationForLogging()); // Khang Wah, add mgnification info into CD&A log
      }
      else
      {
        // Log the filename for the failed image, if logging we save the images also
        String filename = "cam_high_" + camera.getId() + "_FAILED_" + _config.getSystemCurrentHighMagnificationForLogging() + ".png";
        logString.append(filename + ",");
      }

      // terminate each line
      logString.append("\n");
    }


    if (forceLogging)
    {
      didWeLog = true;
      _logger.log(logString.toString());
    }
    else
    {
      didWeLog = _logger.logIfTimedOut(logString.toString(), _logTimeoutMilliSeconds);
    }


    if (didWeLog)
    {
      for (VirtualProjection virtualProjection : _virtualProjections)
      {
        // Save image to file, use the save filename that was logged above
        AbstractXrayCamera camera = virtualProjection.getCamera();
        Image image = virtualProjection.getImage();
        StringBuilder filename = new StringBuilder();
        //The file name for the saved file changes if the fiducial is not found
        if(_fiducialFound)
        {
          filename.append("cam_high_" + camera.getId() +  "_" + _config.getSystemCurrentHighMagnificationForLogging() + ".png");
        }
        else
        {
          filename.append("cam_high_" + camera.getId() + "_FAILED_" + _config.getSystemCurrentHighMagnificationForLogging() + ".png");
        }
        XrayImageIoUtil.savePngImage(image.getBufferedImage(), _logDirectory + File.separator + filename);
        XrayImageIoUtil.savePngImage(_template.getBufferedImage(), _logDirectory + File.separator + "template_high.png");
      }
    }
  }


  /**
   * @author Eddie Williamson
   */
  public void createResultsObject()
  {
    _localResults = new Result[_MAX_RESULT_INDEX];

    // Create container for the others
    _result = new Result(_NAME_OF_CAL);

    Result foundResult = new Result(new Boolean(false), getName());
    _result.addSubResult(foundResult);
    _localResults[_FIDUCIAL_FOUND_RESULT_INDEX] = foundResult;

    Result qualityResult = new Result(new Double(0.0), getName());
    _result.addSubResult(qualityResult);
    _localResults[_MATCH_QUALITY_RESULT_INDEX] = qualityResult;

    Result xResult = new Result(new Double(0.0), getName());
    _result.addSubResult(xResult);
    _localResults[_STAGE_X_RESULT_INDEX] = xResult;

    Result yResult = new Result(new Double(0.0), getName());
    _result.addSubResult(yResult);
    _localResults[_STAGE_Y_RESULT_INDEX] = yResult;
  }


  /**
   * @author Eddie Williamson
   */
  public void createLimitsObject() throws DatastoreException
  {
    _localLimits = new Limit[_MAX_RESULT_INDEX];

    // Create container for the others
    _limits = new Limit(null, null, null, _NAME_OF_CAL);

    // Fiducial found will be Boolean result
    Limit foundLimit = new Limit(null, null, null, "System fiducial found");
    _limits.addSubLimit(foundLimit);
    _localLimits[_FIDUCIAL_FOUND_RESULT_INDEX] = foundLimit;

    Limit qualityLimit = new Limit(new Double(_parsedLimits.getLowLimitDouble("qualityThreshold")),
                                   new Double(_parsedLimits.getHighLimitDouble("qualityThreshold")),
                                   "no units", "Template match quality");
    _limits.addSubLimit(qualityLimit);
    _localLimits[_MATCH_QUALITY_RESULT_INDEX] = qualityLimit;

    Limit xLimit = new Limit(new Double(_parsedLimits.getLowLimitDouble("stageX")),
                             new Double(_parsedLimits.getHighLimitDouble("stageX")),
                             "nanometers", "Stage X position");

    _limits.addSubLimit(xLimit);
    _localLimits[_STAGE_X_RESULT_INDEX] = xLimit;

    Limit yLimit = new Limit(new Double(_parsedLimits.getLowLimitDouble("stageY")),
                             new Double(_parsedLimits.getHighLimitDouble("stageY")),
                             "nanometers", "Stage Y position");
    _limits.addSubLimit(yLimit);
    _localLimits[_STAGE_Y_RESULT_INDEX] = yLimit;
  }


  private void storeNewCalibrationValues() throws DatastoreException
  {
    _config.setValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, _newXraySpotXPosition);
    _config.setValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, _newXraySpotYPosition);
  }


  /**
   * Used by regression test to check pass/fail status
   * @return true if all results pass compared to their limits
   * @author Eddie Williamson
   */
  boolean didTestPass()
  {
    return _limits.didTestPass(_result);
  }


  /**
   * This method sets up two variables that determine image width and height. They
   * cannot be set up at object instanciation time because they require hardware
   * information.
   *
   * @author Eddie Williamson
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {      
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to down position for high mag task.
        XrayActuator.getInstance().down(true);
      }
    }  
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
    
    _REGION_TO_IMAGE_WIDTH_NANOMETERS = XrayCameraArray.getCameraWidthInNanometersAtNominalMagnification();
    
    // This call CANNOT be made unless the hardware has been started up
    _REGION_TO_IMAGE_HEIGHT_NANOMETERS = MechanicalConversions.getMinimumStageTravelInNanometers();
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
  }


  /**
   * @author Eddie Williamson
   */
  public void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    } 
    // Free image memory, reset pointers.
    for (VirtualProjection virtualProjection : _virtualProjections)
      virtualProjection.clear();
    _virtualProjections.clear();
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    if (_matchQuality != null)
      _matchQuality = null;
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }


  /**
   * @return true if this procedure can run on this hardware platform
   * @author Eddie Williamson
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }


  /**
   * Method to create detailed failure information that will be available
   * to observers (in the Result object)
   *
   * @author Eddie Williamson
   */
  private void populateFailureInformation()
  {
    //If the task didn't fail, do nothing, otherwise...
    if (this.getLimit().didTestPass(this.getResult()) == false)
    {
      _result.setTaskStatusMessage(StringLocalizer.keyToString("ACD_MISSING_SYSTEM_FEATURE_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_RECOVER_FROM_XRAY_SPOT_SUGGESTED_ACTION_KEY"));
    }
  }

  /**
   * @author Eddie Williamson
   */
  public void executeTask() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
    //Swee Yee Wong - Camera debugging purpose
    debug("calHighMagXraySpot");
    ImageAcquisitionEngine imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
    Boolean forceLogging = false;
    _fiducialFound = false;
//    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_XRAYSPOT);
    _hardwareObservable.setEnabled(false);

    try
    {
    reportProgress(0);
    // Get current nominal xray spot coordinates from config
    _oldXraySpotXPosition = _config.getIntValue(HardwareCalibEnum.
                                                HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    _oldXraySpotYPosition = _config.getIntValue(HardwareCalibEnum.
                                                HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
    _config.setValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, new Integer(_defaultXLocationNanometers));
    _config.setValue(HardwareCalibEnum.HIGH_MAG_XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, new Integer(_defaultYLocationNanometers));
    // Tell the camera array to recalculate all of its rectangle geometries
    XrayCameraArray.getInstance().refresh();
    // Tell the IAE that constants used for obtaining ProjectionRequests and
    // reconstructedImages has been changed.
    ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer().calibrationConstantsWereReset();

    // Pixel size at the reference plane, Use nominal mag for X because mag cal might not have been run yet.
    // It's close enough for this cal.
    int xDirectionNanometersPerPixel =
      (int)Math.round(MechanicalConversions.getStepDirectionPixelSizeInNanometersAboveNominalPlane(0.0));
    int yDirectionNanometersPerPixel = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();

    // Create the list of projections required -- only one for this cal
    // Image acquisition wants a list of projections. These must be at least 1 inch long. The
    // virtual projection allows specifying the size I actually want. It then generates the
    // projection request for me and handles cropping the returned image to the size I originally
    // asked for.
    AbstractXrayCamera camera = CalibrationSupportUtil.getXraySpotCoordinateCamera();
    _virtualProjections = new ArrayList<VirtualProjection>();
    List<ProjectionRequestThreadTask> projectionRequests = new ArrayList<ProjectionRequestThreadTask>();
    VirtualProjection virtualProjection = new VirtualProjection(camera,
                                                                ScanPassDirectionEnum.FORWARD,
                                                                _REGION_TO_IMAGE_X_NANOMETERS,
                                                                _REGION_TO_IMAGE_Y_NANOMETERS,
                                                                _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                                                _REGION_TO_IMAGE_HEIGHT_NANOMETERS);
    _virtualProjections.add(virtualProjection);
    projectionRequests.add(virtualProjection.getProjectionRequestThreadTask());

    // Get the projections
    TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
    reportProgress(25);

    // Initialize the IAE before requesting projections
    imageAcquisitionEngine.initialize();
    imageAcquisitionEngine.acquireHighMagProjections(projectionRequests, testExecutionTimer, true);
    reportProgress(75);

    Image image = virtualProjection.getImage();

    // calculate the nominal location of the SF relative to the image origin
    _expectedLocation = new ImageCoordinate( -_REGION_TO_IMAGE_X_NANOMETERS / xDirectionNanometersPerPixel,
                                            (_REGION_TO_IMAGE_HEIGHT_NANOMETERS + _REGION_TO_IMAGE_Y_NANOMETERS) /
                                            yDirectionNanometersPerPixel);

    _matchQuality = new DoubleRef();  // Metric returned from template match
    _templateCoordinates = Filter.matchTemplate(image,
                                                _template,
                                                MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                _matchQuality);

    if (_matchQuality.getValue() > _QUALITYTHRESHOLD)
    {
      _fiducialFound = true;
      _localResults[_FIDUCIAL_FOUND_RESULT_INDEX].setResults(new Boolean(true));
      _localResults[_MATCH_QUALITY_RESULT_INDEX].setResults(new Double(_matchQuality.getValue()));

      // _templateCoordinates contains the location for the upper left corner of the template
      // we need the actual location of the fiducial within the template
      _actualLocation = new ImageCoordinate(_templateCoordinates.getX() + _templateSizeXPixels + _templateBorderWidthPixels,
                                            _templateCoordinates.getY() + _templateSizeYPixels + _templateBorderHeightPixels);

      // Calculate the offset from the actual fiducial location to the nominal
      // location oriented WRT machine coordinates but still in pixels.
      // Image Y and machine Y are of opposite polarity so the Y calculation is opposite the X calculation.
      _offset = new ImageCoordinate(_expectedLocation.getX() - _actualLocation.getX(),
                                    _actualLocation.getY() - _expectedLocation.getY());

      // Calculate new X,Y position in nanometers. Offset is still in pixels
      _newXraySpotXPosition = _defaultXLocationNanometers + _offset.getX() * xDirectionNanometersPerPixel;
      _newXraySpotYPosition = _defaultYLocationNanometers + _offset.getY() * yDirectionNanometersPerPixel;
      _localResults[_STAGE_X_RESULT_INDEX].setResults(new Double(_newXraySpotXPosition));
      _localResults[_STAGE_Y_RESULT_INDEX].setResults(new Double(_newXraySpotYPosition));
    }
    else
    {
      // match quality too low means the fiducial was missing from the image
      _fiducialFound = false;
      _localResults[_FIDUCIAL_FOUND_RESULT_INDEX].setResults(new Boolean(false));
      _localResults[_MATCH_QUALITY_RESULT_INDEX].setResults(new Double(_matchQuality.getValue()));
      // Don't need to set results for stage X/Y, they are already defaulted to a failing 0.0 value
    }

    if (_limits.didTestPass(_result))
    {
      storeNewCalibrationValues();

      // Tell the camera array to recalculate all of its rectangle geometries
      XrayCameraArray.getInstance().refresh();

      // Tell the IAE that constants used for obtaining ProjectionRequests and
      // reconstructedImages has been changed.
      ImageAcquisitionEngine.getInstance().getReconstructedImagesProducer().calibrationConstantsWereReset();

      // Running and passing this cal invalidates the SystemOffsets cal so it needs to run also
      _calSystemOffsets.setTaskToRunASAP();
    }
    else
    {
      // Force logging on failure so we can do post mortem
      forceLogging = true;
    }
    logInformation(forceLogging);
    reportProgress(100);
    populateFailureInformation();
  }
  finally
  {
    _hardwareObservable.setEnabled(true);
  }
//  _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_XRAYSPOT);
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
