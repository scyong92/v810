package com.axi.v810.business.autoCal;

import com.axi.util.Enum;
/**
 * Class to encapsulate the two possible trigger states for running event driven
 * hardware tasks. They can be triggered when an event/state START is seen. Or
 * they can be triggered when the event/state END is seen.
 *
 * @author Reid Hayhow
 */
public class EventTriggerStateEnum extends Enum
{
  private static int _index = -1;

  public static EventTriggerStateEnum TRIGGER_ON_START = new EventTriggerStateEnum(++_index);
  public static EventTriggerStateEnum TRIGGER_ON_END = new EventTriggerStateEnum(++_index);

  /**
   * @author Reid Hayhow
   */
  private EventTriggerStateEnum(int id)
  {
    super(id);
  }
}
