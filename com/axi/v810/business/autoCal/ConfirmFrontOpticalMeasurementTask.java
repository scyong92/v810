package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class ConfirmFrontOpticalMeasurementTask extends ConfirmOpticalMeasurementTask
{
  // Printable name for this procedure
  private static final String NAME = StringLocalizer.keyToString("CD_CONFIRM_FRONT_OPTICAL_MEASUREMENT_KEY");

  private static ConfirmFrontOpticalMeasurementTask _instance = null;
  
  /**
   * @author Cheah Lee Herng
   */
  private ConfirmFrontOpticalMeasurementTask()
  {
    super(NAME,
          Directory.getFrontOpticalMeasurementRepeatabilityConfirmationLogDir(),
          FileName.getFrontOpticalMeasurementRepeatabilityLogFileBasename(),
          ProgressReporterEnum.CONFIRM_FRONT_OPTICAL_MEASUREMENT_REPEATABILITY);
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  public static synchronized ConfirmFrontOpticalMeasurementTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmFrontOpticalMeasurementTask();
    }
    return _instance;
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  protected PspModuleEnum getPspModuleEnum()
  {
    return PspModuleEnum.FRONT;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalCameraIdEnum getOpticalCameraIdEnum()
  {
    return OpticalCameraIdEnum.OPTICAL_CAMERA_1;
  }
}
