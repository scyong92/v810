package com.axi.v810.business.autoCal;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CalOpticalSystemFiducialRotation extends EventDrivenHardwareTask implements OpticalLiveVideoInteractionInterface 
{
    private static CalOpticalSystemFiducialRotation _instance;
    private static Config _config = Config.getInstance();
    
    private PanelHandler _panelHandler = PanelHandler.getInstance();
    
    //Motion profile to use when moving stage, use the fastest
    protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
    
    private static OpticalLiveVideoObservable _opticalLiveVideoObservable = OpticalLiveVideoObservable.getInstance();
    
    // Printable name for this procedure
    private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_OPTICAL_SYSTEM_FIDUCIAL_ROTATION_NAME_KEY");
    
    // This cal is called on the first event after the timeout occurs.
    private static final int _EVENT_FREQUENCY = 1;
    private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.OPTICAL_SYSTEM_FIDUCIAL_ROTATION_EXECUTE_EVERY_TIMEOUT_SECONDS);
    
    // Logging support
    private static final String _logDirectory = Directory.getMagnificationCalLogDir();
    private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.OPTICAL_SYSTEM_FIDUCIAL_ROTATION_MAX_LOG_FILES_IN_DIRECTORY);
    
    //Log utility members
    private FileLoggerAxi _logger;
    private int _maxLogFileSizeInBytes = 1028;
    
    protected Map<OpticalCameraIdEnum, StagePositionMutable> _cameraToStagePositionMap = new HashMap<OpticalCameraIdEnum, StagePositionMutable>();
    private BooleanLock  _waitingForFirstOpticalCameraLocationInput = new BooleanLock(false);
    
    /**
     * @author Cheah Lee Herng
    */
    private CalOpticalSystemFiducialRotation()
    {
        super(_NAME_OF_CAL,
              _EVENT_FREQUENCY,
              _EXECUTE_EVERY_TIMEOUT_SECONDS,
              ProgressReporterEnum.OPTICAL_SYSTEM_FIDUCIAL_ROTATION_ADJUSTMENT_TASK);
        
        //Create the logger
        _logger = new FileLoggerAxi(FileName.getOpticalSystemFiducialRotationFullPath() +
                                    FileName.getLogFileExtension(),
                                    _maxLogFileSizeInBytes);

        _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

        setTimestampEnum(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_ROTATION_LAST_TIME_RUN);
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public static synchronized CalOpticalSystemFiducialRotation getInstance()
    {
        if (_instance == null)
        {
          _instance = new CalOpticalSystemFiducialRotation();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void executeTask() throws XrayTesterException 
    {
        boolean deferred = false;
        try
        {
            reportProgress(0);
        
            //Setup system fiducial stage position
            setupSystemFiducialStagePosition();
            
            reportProgress(20);
            
            // At least one optical camera must be installed to run system fiducial
            Assert.expect(OpticalCameraIdEnum.getAllEnums().size() > 0);
            
            PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);
            
            reportProgress(40);
            
            // Move stage to first optical camera
            moveStageBeforeExecute(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
            
            // Display Optical Live Video, starting with first optical camera Id
            _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                      OpticalCameraIdEnum.OPTICAL_CAMERA_1,
                                                      StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"), 
                                                      this, false, false, false));                                    
            
            // Wait for user to record current stage position
            waitForFirstOpticalCameraSystemFiducialInput();
        }
        catch (XrayTesterException ex)
        {
            _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
            deferred = true;
            return;
        }
        //Log any unexpected exceptions
        catch (Exception e)
        {
            Assert.logException(e);
        }
        finally
        {            
            //Restore stage (if needed)
            moveStageAfterExecute();
            
            //Make sure projector is off before leaving
            setProjectorAfterExecute();
        }
    }
    
    /**
     * Method for knowing if the task can run with the current panel state. For light
     * image tasks, this should return false if a panel is loaded and true if no
     * panel is loaded. This method is overridden for Dark tasks to always return
     * true. 
     */
    protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
    {
        // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
        //  add throws XrayTesterException
        //flip the logic of isPanelLoaded because light tasks can't run with a panel
        return (_panelHandler.isPanelLoaded() == false);
    }
    
    /**
     * Always make sure projector is off before leaving this task.
     * 
     * @author Cheah Lee Herng
     */
    protected void setProjectorAfterExecute() throws XrayTesterException
    {
      PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void waitForFirstOpticalCameraSystemFiducialInput()
    {
        _waitingForFirstOpticalCameraLocationInput.setValue(true);
        try
        {
          _waitingForFirstOpticalCameraLocationInput.waitUntilFalse();
        }
        catch (InterruptedException ex)
        {
          // Do nothing ...
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void runNextOpticalCamera()
    {
        _waitingForFirstOpticalCameraLocationInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void cancelLiveVideo()
    {
        _taskCancelled = true;
        _waitingForFirstOpticalCameraLocationInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void setSurfaceMapPoint(List<Point2D> points)
    {
      // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void setupSystemFiducialStagePosition()
    {
        Assert.expect(_cameraToStagePositionMap != null);
        
        // System Fiducial Stage Position Optical Camera 1
        StagePositionMutable systemFiducialCamera1StagePosition = new StagePositionMutable();
        systemFiducialCamera1StagePosition.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers());
        systemFiducialCamera1StagePosition.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers());
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_1, systemFiducialCamera1StagePosition);
        
        // System Fiducial Stage Position Optical Camera 2
        StagePositionMutable systemFiducialCamera2StagePosition = new StagePositionMutable();
        systemFiducialCamera2StagePosition.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers());
        systemFiducialCamera2StagePosition.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers());
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_2, systemFiducialCamera2StagePosition);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void moveStageBeforeExecute(OpticalCameraIdEnum opticalCameraIdEnum) throws XrayTesterException
    {
        Assert.expect(opticalCameraIdEnum != null);
        
        // panel needs to be repositioned left
        if (_panelHandler.isPanelLoaded())
          _panelHandler.movePanelToLeftSide();
        
        //Set up the stage to get it out of the way of the cameras
        StagePositionMutable stagePosition = _cameraToStagePositionMap.get(opticalCameraIdEnum);
        
        //Profile1 is fastest
        PanelPositioner.getInstance().setMotionProfile(new MotionProfile(_motionProfile));
        
        // Move stage to existing center position before performing CD&A
        PanelPositioner.getInstance().pointToPointMoveAllAxes(stagePosition);
    }
    
    /**
     * Method that controls stage motion after task execution.
     *
     * @author Cheah Lee Herng
    */
    protected void moveStageAfterExecute()
    {
        //Do nothing
    }

    protected void setUp() throws XrayTesterException 
    {
    }

    public boolean canRunOnThisHardware() 
    {
        return true;
    }

    protected void createLimitsObject() throws DatastoreException 
    {
    }

    protected void cleanUp() throws XrayTesterException 
    {
    }

    protected void createResultsObject() throws DatastoreException 
    {
    }
    
    /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setHardwareShutdownComplete()
  {
    // Do nothing
  }
}
