package com.axi.v810.business.autoCal;

import java.util.*;

/**
 * @author Anthony Fong
 */
public class BenchmarkJsonFile extends BenchmarkJsonFileSystemItem
{

  private long size;

  /**
   * @author Anthony Fong
   */
  public long getSize()
  {
    return size;
  }

  /**
   * @author Anthony Fong
   */
  public void setSize(long size)
  {
    this.size = size;
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFile(String name)
  {
    super(name, null, null, null);
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFile(String name, Date creationDate)
  {
    super(name, creationDate, null, null);
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFile(String name, Date creationDate, Boolean hidden)
  {
    super(name, creationDate, hidden, null);
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFile(String name, Date creationDate, Boolean hidden, List<Long> datas)
  {
    super(name, creationDate, hidden, datas);
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFile(String name, long size)
  {
    super(name, null, null, null);
    this.size = size;
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFile(String name, Boolean hidden)
  {
    super(name, null, hidden, null);
  }
}
