package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * A simple, time based task to log values from the HTube Xray source
 * to simplify the diagnostic process.
 *
 * @author Reid Hayhow
 * @author George Booth (leveraged from ConfirmLegacyXRaySource for the Hamamatsu Xray Source, aka HTube)
 */
public class ConfirmHTubeXRaySource extends EventDrivenHardwareTask
{
  private static ConfirmHTubeXRaySource _instance;

  private static Config _config = Config.getInstance();

  private FileLoggerAxi _logger;
  private int _maxLogFileSizeInBytes =
      _config.getIntValue(SoftwareConfigEnum.XRAY_SOURCE_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES);
  private HTubeXraySource _xraySourceInstance;

  private static String _name = StringLocalizer.keyToString("CD_CONFIRM_LEGACY_XRAY_SRC_KEY");

  private static final int _timeoutInSeconds =
      _config.getIntValue(SoftwareConfigEnum.XRAY_SOURCE_CONFIRM_TIMEOUT_IN_SECONDS);
  private static int _frequency =
      _config.getIntValue(HardwareCalibEnum.LEGACY_XRAY_SOURCE_CONFIRM_FREQUENCY);

  /**
   * @author Reid Hayhow
   * @author George Booth
   */
  public static synchronized ConfirmHTubeXRaySource getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmHTubeXRaySource();
    }
    return _instance;
  }
  /**
   * @author Reid Hayhow
   * @author George Booth
   */
  private ConfirmHTubeXRaySource()
  {
    super(_name,
          _frequency,
          _timeoutInSeconds,
          ProgressReporterEnum.CONFIRM_LEGACY_XRAY_SOURCE);

    _xraySourceInstance = (HTubeXraySource)AbstractXraySource.getInstance();

    //This confirmation and all sub-classed confirmations are automated
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    //Set the enum for the timestamp for this Confirmation
    setTimestampEnum(HardwareCalibEnum.CONFIRM_LEGACY_XRAY_SOURCE_LAST_TIME_RUN);
  }

  /**
   * By default all tests can run on any hardware configuration.
   *
   * @author Reid Hayhow
   * @author George Booth
   */
  public boolean canRunOnThisHardware()
  {
    //This test can run only if we are using the HTube XraySource
    return AbstractXraySource.getInstance() instanceof HTubeXraySource;
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * clean-up code that may be reused (for example executeTask and abortTask
   * may both use the same cleanUp code)
   * @author Reid Hayhow
   *
   */
  protected void cleanUp() throws XrayTesterException
  {
    _automatedTask =  false;
  }

  /**
   * Create a Limits object specific to the particular test being run.
   *
   * @author Reid Hayhow
   *
   */
  protected void createLimitsObject()
  {
    _limits = new Limit(null, null, null, _name);
  }

  /**
   * Create the results object for this test result. Assume the task did
   * not complete. If it does complete, it will be set to complete in
   * executeTask()
   *
   * @author Reid Hayhow
   *
   */
  protected void createResultsObject()
  {
    _result = new Result(new Boolean(false), _name);
    //Set the generic failure message and action here
    _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_XRAYS_NOT_ON_LOGGING_NOT_DONE_KEY"));
    _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_ENSURE_THE_XRAY_SOURCE_IS_ON_SUGGESTED_ACTION_KEY"));
  }

  /**
   * This method should contain the task code.
   *
   * @author Reid Hayhow
   * @author George Booth
   */
  protected void executeTask() throws XrayTesterException
  {
    // Report that this task is starting
    reportProgress(0);

    //Shouldn't get here if we are not using the HTube Xray Source,
    //because of canRunOnThisHardware test, but just in case
    Assert.expect(_xraySourceInstance instanceof HTubeXraySource);

    //We are only interested in logging values when Xrays are started
    //This should be the case by the time we get here, but double check
    if(_xraySourceInstance.isStartupRequired())
    {
      // Report that this task is done, even if not for the right reason
      reportProgress(100);

      // The Result is already set with this status, so just log the fact to the log file
      _logger.append(StringLocalizer.keyToString("CD_XRAYS_NOT_ON_LOGGING_NOT_DONE_KEY"));

      //The default result is already set to fail and has the correct message
      //so just exit out here
      return;
    }

    StringBuffer stringBuff = new StringBuffer();

    // Query each value from the xray source, log them and report progress,
    // assuming each value takes about the same time to query
    stringBuff.append(_xraySourceInstance.areXraysOn() +",");
    reportProgress(33);

    stringBuff.append(_xraySourceInstance.getAnodeVoltageInKiloVolts() + ",");
    reportProgress(66);

    stringBuff.append(_xraySourceInstance.getCathodeCurrentInMicroAmps() + ",");
    reportProgress(99);

//    stringBuff.append("0.0, "); // _xraySourceInstance.getFocusVoltageInVolts() + ",");
//    reportProgress(40);
//
//    stringBuff.append("0.0, "); // _xraySourceInstance.getFilamentVoltageInVolts() + ",");
//    reportProgress(50);
//
//    stringBuff.append("0.0, "); // _xraySourceInstance.getGridVoltageInVolts() + ",");
//    reportProgress(60);
//
//    stringBuff.append("0.0, "); // _xraySourceInstance.getTotalCurrentInMicroAmps() + ",");
//    reportProgress(70);
//
//    stringBuff.append("0.0, "); // _xraySourceInstance.getExpectedAnodeOnBleederMicroAmps() + ",");
//    reportProgress(80);
//
//    stringBuff.append("0.0, "); // _xraySourceInstance.getIsolatedSupplyVoltageInVolts() + ",");
//    reportProgress(90);

    // Log the values to the log file
    _logger.append(stringBuff.toString());

    //If we get here, the task completed as expected
    _result = new Result(new Boolean(true), _name);

    // Report that this task is done
    reportProgress(100);
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * set-up code that may be reused
   * @author Reid Hayhow
   *
   */
  protected void setUp() throws XrayTesterException
  {
    //If the _logger member is null then create the logfile and header
    //This should only happen once per instance of this confirmation
    // This is done to minimize the number of headers in the log file
    if(_logger == null)
    {
      createLogFileAndHeader();
    }
  }

  /**
   * Create the header for the logfile used by this task.
   *
   * @author Reid Hayhow
   */
  private void createLogFileAndHeader() throws DatastoreException
  {
    _logger = new FileLoggerAxi(FileName.getConfirmXraySourceFullPath() +
                                FileName.getLogFileExtension(),
                                _maxLogFileSizeInBytes);

    StringBuffer stringBuff = new StringBuffer();
    char micro = (char)181;
    stringBuff.append("XraysOn/Off, Anode (kV), Cathode (" + micro + "A)");
//        , Focus (V), Filament (V), Grid (V), Total (" + micro
//        + "A), Bleeder (" + micro + "A) , ISO Sup (V)");
    stringBuff.append(System.getProperty("line.separator"));

    _logger.append(stringBuff.toString());
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
