package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * Specializes the abstract ConfirmSystemMTF for the high magnification operation.
 *
 * @author Wei Chin: originally cut and pasted ConfirmSystemMTF to implement this class
 * @author Rick Gaudette: refactored ConfirmSystemMTF & ConfirmHighMagSystemMTF, created ConfirmLowMagSystemMTF to
 *         minimize the amount of repeated code.
 */
public class ConfirmHighMagSystemMTF extends ConfirmSystemMTF
{
  // Singleton class.
  private static ConfirmHighMagSystemMTF _instance;

  /**
   * @author Rick Gaudette
   */
  private ConfirmHighMagSystemMTF()
  {
    // TODO: RJG - the original author should fully i18n this string
    super("High Mag " + StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_MTF_KEY"),
          Directory.getHighMagBaseImageQualityConfirmationLogDir());

    // RJG: why use less projections for high mag?
    _NO_OF_PROJECTIONS = 4;
    _magnification = MagnificationEnum.H_NOMINAL;
    _differentInX = -1016000; // offset 40mils
    _differentInY = -1016000; // offset 40mils

    // RJG: Why is this 380 which is 1.9 x the low mag value
    _MIN_EDGE_WIDTH_IN_PIXELS = 380;

    // RJG: why scale by 1.5 when magnification ratio is 19/11 = 1.7272... ?
    // RJG: if 1.5 is appropriate then just multiply by 3 and divide by 2
    _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
        (int) Math.floor(
            _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS) * 1.5);
    _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
        (int) Math.floor(
            _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS) * 1.5);
    _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
        (int) Math.floor(
            _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS) * 1.5);

    AbstractXrayCamera xRayCamera = XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    _REGION_TO_IMAGE_WIDTH_NANOMETERS =
        xRayCamera.getNumberOfSensorPixelsUsed() * _magnification.getNanoMetersPerPixel();
    
    _isHighMagTask = true;
  }


  /**
   * A singleton class.
   *
   * @author John Dutton
   */
  public static synchronized ConfirmHighMagSystemMTF getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmHighMagSystemMTF();
    }
    return _instance;
  }
  
//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmSharpness will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
}
