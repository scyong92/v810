package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.HardwareTask;
import com.axi.v810.util.XrayTesterException;
import com.axi.v810.datastore.DatastoreException;
import com.axi.v810.datastore.config.Config;

/**
 * @author John Dutton
 */
public class Test_ConfirmSystemFiducial extends UnitTest
{
  private Config _config = Config.getInstance();
  HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  protected Test_ConfirmSystemFiducial()
  {
    super();

    // Won't run on xRayTest machine without loading config files.  Has something
    // to do with xRayTest.startup() expecting the config files to be loaded.
    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
      Assert.expect(false, "Can't load config files");
    }
  }

  /**
   * @author John Dutton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ConfirmSystemFiducial());
  }

  /**
   * @author John Dutton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // Create a test utils class and save original hardware calib file since last
    // timestamp will be written to it and the test harness expects no changes.
    TestUtils utils = new TestUtils();
    utils.backupHWCalib(getTestSourceDir());

    try
    {
      XrayTester.getInstance().startup();

      //------------------------------------------------------------------
      // Test Case #1: the pass case using good projections
      //------------------------------------------------------------------

      // Get an instance of the test to run
      ConfirmSystemFiducial confirm = ConfirmSystemFiducial.getInstance();
      confirm.setReadImageFromFile(true);

      //Execute the test
      runConfirmation(confirm);
    }
    catch (Exception e)
    {
      e.printStackTrace(os);
    }

    finally
    {
      //Use the utils to copy the HW calib back.
      utils.restoreHWCalib(getTestSourceDir());
    }
  }

  /**
   * @author John Dutton
   */
  private void runConfirmation(HardwareTask hwTask) throws XrayTesterException
  {
    final HardwareTask cal = hwTask;
    if (UnitTest.unitTesting() == false)
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws Exception
        {
          cal.executeNonInteractively();
        }
      });
    }
    else
    {
      cal.executeNonInteractively();
    }
  }


}
