##############################################################################
# FILE   : Makefile
#
# PURPOSE: To provide a standard Makefile template for use in SPT.
#          This template handles compiling Java files including
#          rmic and javah.  To use this template, copy it into
#          your directory and replace all XXXX with the appropriate
#          things as described in the comments.  In some cases you will
#          want to replace the XXXX with nothing.  Each java directory
#          should have one Makefile following this template in it.
#          This Makefile will create the following files in each directly:
#            - class files
#            - RMI stub class files (if needed)
#            - JNI .h files (if needed)
#            - JNI dll file (if needed)
#          This Makefile will not create a jar file.  There will be
#          one main Makefile under the xRayTest directory that will be
#          responsible for calling the other directory Makefiles.  It
#          will also create the application jar or jar files.  There
#          is no need to create individual directory level jar files
#
#          Responsibilities of the main Makefile:
#            - check that the proper environment variables are set
#              for the Makefiles to work
#            - check that software architecture is followed by looking
#              at .java imports
#            - call all directory Makefiles
#            - create the application jar file or files
#
#          Responsibilites of the directory Makefiles:
#            - create all .class, .h, and .dll files
#            - move appropriate files into the release directory
#
# NOTE   : This Makefile uses the following environment variables:
#          JAVACOPTS - options for the javac compiler
#          JAVAHOPTS - options for the javah compiler
#          RMICOPTS  - options for the rmic compiler
#          CL        - options for Microsoft visual c++ compiler
#          JAVA_HOME - location for java jdk (not jre)
#
# AUTHOR : Bill Darbie
##############################################################################
# make this work like a POSIX Makefile - standard is good
.POSIX:

# set what shell to use
SHELL=sh

##############################################################################
# define dependencies for a make
##############################################################################
all:

##############################################################################
# general Makefile setup
##############################################################################
MAKEFILE_VERSION="1.0"

##############################################################################
# set the current directory relative to the base java directory
# in other words com/axi/XXXX
##############################################################################
THIS_DIR=com/axi/v810/business/autoCal

##############################################################################
# set any flags you want to go to the c++ compile
##############################################################################
CC_COMMON_FLAGS=

CC_NODEBUG_FLAGS= \
  $(CC_COMMON_FLAGS) \
  $(CC_COMMON_NODEBUG_FLAGS)

CC_DEBUG_FLAGS= \
  $(CC_COMMON_FLAGS) \
  $(CC_COMMON_DEBUG_FLAGS)

##############################################################################
# set any -I options for the c++ compiler
##############################################################################
CC_INCLUDES= \
  $(CC_COMMON_INCLUDE)

##############################################################################
# set all the libraries to link to here
##############################################################################
CC_DEBUG_LINK_LIBS= \
  $(CURR_VIEW_DIR)/5dx/release/bin/cxsys.lib \
  $(CURR_VIEW_DIR)/5dx/release/bin/ipclib.lib \
  $(CURR_VIEW_DIR)/5dx/rel/5dx/rxx/bin/mtdUtil.lib \
  $(CURR_VIEW_DIR)/5dx/release/bin/sptAssert.lib

CC_NODEBUG_LINK_LIBS= \
  $(CURR_VIEW_DIR)/5dx/release/bin/cxsys.lib \
  $(CURR_VIEW_DIR)/5dx/release/bin/ipclib.lib \
  $(CURR_VIEW_DIR)/5dx/rel/5dx/rxx/bin/mtdUtil.lib \
  $(CURR_VIEW_DIR)/5dx/release/bin/sptAssert.lib

##############################################################################
##############################################################################
# list all .java files that have a main method in them that are NOT
# regression tests (test files go in JAVA_TEST_CLASS_FILES)
##############################################################################
JAVA_MAINS=

##############################################################################
# list .class files to be compiled for the customer here
# do not include unit or regression test files here or any files listed
# in JAVA_MAINS
# EXAMPLE: JAVA_CLASS_FILES=Bill.class
##############################################################################
JAVA_CLASS_FILES= \
  CalEventEnum.class \
  CalXraySpotCoordinateEventDrivenHardwareTask.class \
  CalHysteresisEventDrivenHardwareTask.class \
  CalSystemFiducialEventDrivenHardwareTask.class \
  CalSystemFiducialResultTypeEnum.class \
  CalibrationSupportUtil.class \
  AdjustmentsAndConfirmationsLogger.class \
  CalFringePatternUniformity.class \
  CalGrayscale.class \
  CalHighMagGrayscale.class \
  ConfirmCameraHighMagCalibration.class \
  ConfirmCameraHighMagDarkImageTask.class \
  ConfirmCameraHighMagGroupOneLightImageTask.class \
  ConfirmCameraHighMagGroupTwoLightImageTask.class \
  ConfirmCameraHighMagGroupThreeLightImageTask.class \
  ConfirmCameraHighMagLightAllGroups.class \
  ConfirmCameraHighMagLightImageWithNoPanel.class \
  ConfirmXRayCylinderTask.class \
  ConfirmXraySpotCoordinateForReverseDirectionTask.class \
  CalHighMagHysteresisEventDrivenHardwareTask.class \
  CalHighMagnification.class \
  CalHighMagSystemFiducialEventDrivenHardwareTask.class \
  CalHighMagXraySpotCoordinateEventDrivenHardwareTask.class \
  CalMagnification.class \
  CalOpticalCalibration.class \
  CalOpticalMagnification.class \
  CalOpticalSystemFiducial.class \
  CalOpticalSystemFiducialRotation.class \
  CalOpticalSystemOffsetAdjustment.class \
  CalXraySpotDeflection.class \
  ConfirmCameraCalibration.class \
  CameraCalibrationDataTypeEnum.class \
  CameraCalFileSaverThreadTask.class \
  CameraDarkPixelCalThreadTask.class \
  CameraDarkSegmentCalThreadTask.class \
  CameraImageDataTypeEnum.class \
  ConfirmAreaModeImages.class \
  ConfirmBenchmarkTask.class \
  BenchmarkJsonFile.class \
  BenchmarkJsonFileEnum.class \
  BenchmarkJsonFileSystemItem.class \
  BenchmarkJsonFileTimingLogger.class \
  BenchmarkJsonFolder.class \
  AreaModeImageResults.class \
  ConfirmCameraCalibrationImages.class \
  ConfirmHighMagCameraCalibrationImages.class \
  ConfirmCameraImageTask.class \
  ConfirmCameraDarkImageTask.class \
  ConfirmCameraLightImageWithNoPanel.class \
  ConfirmCameraGroupOneLightImageTask.class \
  ConfirmCameraGroupTwoLightImageTask.class \
  ConfirmCameraGroupThreeLightImageTask.class \
  ConfirmCameraLightAllGroups.class \
  ConfirmCommunicationTask.class \
  ConfirmCommunicationWithIRPs.class \
  ConfirmCommunicationWithRMS.class \
  ConfirmFrontOpticalMeasurementTask.class \
  ConfirmAllCommunication.class \
  ConfirmHighMagSystemMTF.class \
  ConfirmHighMagSystemFiducial.class \
  ConfirmHighMagSystemFiducialB.class \
  ConfirmHTubeXRaySource.class \
  ConfirmLegacyXRaySource.class \
  ConfirmLowMagSystemMTF.class \
  ConfirmPanelHandlingTask.class \
  ConfirmPanelPositioningTask.class \
  ConfirmRearOpticalMeasurementTask.class \
  ConfirmSystemMTF.class \
  ConfirmXRayCPMotorTask.class \
  ConfirmSystemFiducial.class \
  ConfirmSystemFiducialB.class \
  ConfirmMagnificationCoupon.class \
  ConfirmMagnificationCouponForHighMag.class \
  ConfirmOpticalCameraImageTask.class \
  ConfirmOpticalCameraLightImageTask.class \
  ConfirmOpticalMeasurementTask.class \
  ConfirmSharpness.class \
  ConfirmSharpnessEdgeEnum.class \
  ConfirmFullSystemMTF.class \
  ConfirmHighMagFullSystemMTF.class \
  ConfirmLowMagFullSystemMTF.class \
  ImageRetriever.class \
  ImageRetrieverTestProgramEnum.class \
  ImageRetrieverBusinessException.class \
  ImageSaverThreadTask.class \
  OpticalCameraImageDataTypeEnum.class \
  OpticalLiveVideoInteractionInterface.class \
  CameraImageRetreiver.class \
  EdgeMTF.class \
  EdgeMTFBusinessException.class \
  EventTriggerStateEnum.class \
  XrayHardwareAutomatedTaskList.class \
  XrayCameraCalibEnum.class \
  XrayCameraCalibThreadTask.class \
  XrayCameraEnableGrayscaleToRunTask.class \
  XrayCameraQualityMonitoring.class \
  XrayCameraQualityData.class \
  XrayCameraTriggerControlThreadTask.class \
  DiagnoseSharpness.class \
  SystemFiducialTemplateMatch.class \
  PanelInterferesWithAdjustmentAndConfirmationTaskException.class \
  PspModuleTestTypeEnum.class

##############################################################################
# list all .class files that need to be built for unit or regression testing
# here
# EXAMPLE: JAVA_TEST_CLASS_FILES=TestBill.java
##############################################################################
JAVA_TEST_CLASS_FILES= \
  Test_CalSystemFiducialEventDrivenHardwareTask.class \
  Test_CalXraySpotCoordinateEventDrivenHardwareTask1.class \
  Test_CalXraySpotCoordinateEventDrivenHardwareTask2.class \
  Test_CalXraySpotCoordinateEventDrivenHardwareTask3.class \
  Test_CalHysteresisEventDrivenHardwareTask.class \
  Test_CalMagnification.class \
  Test_CalXraySpotDeflection.class \
  Test_CalGrayscale.class \
  Test_ConfirmCameraCalibrationFile.class \
  Test_CameraDarkConfirmationTask.class \
  Test_CameraDarkConfirmationTaskTwo.class \
  Test_CameraLightConfirmationTask.class \
  Test_CameraLightConfirmationTaskTwo.class \
  Test_ConfirmCommunication.class \
  Test_ConfirmLegacyXRaySource.class \
  Test_ConfirmPanelHandlingTask.class \
  TestConfirmPanelPositioningTask.class \
  Test_ConfirmPanelPositioning.class \
  Test_ConfirmSystemMTF.class \
  Test_ConfirmSystemFiducial.class \
  Test_ConfirmSystemFiducialB.class \
  Test_ConfirmMagnificationCoupon.class \
  Test_EdgeMTF.class \
  TestUtils.class

##############################################################################
# list all _Stub.class files that need rmic to be run on here
# EXAMPLE: RMI_CLASS_FILES=Bill_Stub.class
##############################################################################
RMI_CLASS_FILES=

##############################################################################
# for each file listed in RMI_CLASS_FILES put a corresponding rule here that
# follows the form:
# Bill_Stub.class: Bill.class
#     $(BUILD_RMI_STUB)
# Be sure to put a tab before the $(BUILD_RMI_STUB)
##############################################################################

##############################################################################
# list all .h files that need javah run on them here
# EXAMPLE: JAVAH_FILES=com_axi_xRayTest_hardware_Bill.h
##############################################################################
JAVAH_FILES=
##############################################################################
# for each file listed in JAVAH_FILES put a corresponding rule here that
# follows the form:
# Bill.h: Bill.class
#     $(BUILD_JNI_HEADER)
# Be sure to put a tab before the $(BUILD_JNI_HEADER)
##############################################################################

##############################################################################
# list the name of the dll that needs to be created
# EXAMPLE: JNI_DLL_FILE=nativeInt.dll
##############################################################################
JNI_DLL_FILE=
all: $(JNI_DLL_FILE) release

##############################################################################
# list all the .cpp files that need to be compiled to create the JNI .dll file
# that you listed above
# EXAMPLE: JNI_CPPL_FILES=com_axi_mtd_Bill.cpp
##############################################################################
JNI_CPP_FILES=
##############################################################################
# create a rule for your dll file here
# It should follow the form:
# Bill.dll: $(JNI_CPP_FILES) $(JAVAH_FILES)
#     $(BUILD_JNI_DLL)
##############################################################################

##############################################################################
# define a rule to move any files from the current directory to the
# release directory.  The release directory should contain all files
# that the customer will need in the customer directory format.
# This includes files like .properties and .gif
# Do NOT copy .class files into the release directory
#
# REMEMBER TO FILL IN THE localClean TARGET TO REMOVE THE SAME FILES
#
# NOTE that the following macros are provided for you use:
#  $(RELEASE_DIR)
#  $(RELEASE_CONFIG_DIR)
#  $(RELEASE_PROPS_DIR)
#  $(RELEASE_IMAGES_DIR)
#  $(RELEASE_BIN)
##############################################################################
release: alwaysDoThis

##############################################################################
# do a remove on all files that you put into the release directory here
#  $(RELEASE_DIR)
#  $(RELEASE_CONFIG_DIR)
#  $(RELEASE_PROPS_DIR)
#  $(RELEASE_IMAGES_DIR)
#  $(RELEASE_BIN)
#  $(RELEASE_LIB)
##############################################################################
clean: localClean
localClean: alwaysDoThis

##############################################################################
# now include all the rules to build the files listed above
##############################################################################
include ../../javaCommonMakefile
