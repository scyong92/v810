package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * @author Reid Hayhow
 */
public class Test_CameraLightConfirmationTask extends UnitTest
{
  /**
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CameraLightConfirmationTask());
  }

  /**
   * @author Reid Hayhow
   */
  protected Test_CameraLightConfirmationTask()
  {
    super();
  }

  /**
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XrayCameraArray xRayCameraArray = null;
    TestUtils utils = new TestUtils();
    //NOTE:Currently this test doesn't need the TestUtils limits copy/restore
    //but it may eventually require it if the limits change enough.

    boolean testPassed = false;
    try
    {
      //Backup the hardware.calib since we are about to write a timestamp into it
      utils.backupHWCalib(getTestSourceDir());

      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getLightTestOneDir();
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);
      CalGrayscale.getInstance().executeUnconditionally();

      //---------------------First test group one---------------------
      //Get an instance of the test
      ConfirmCameraGroupOneLightImageTask groupOneLightConfirmation =  ConfirmCameraGroupOneLightImageTask.getInstance();

      //Run the test
      testPassed = groupOneLightConfirmation.executeUnconditionally();

      //Check to make sure start and stop dates are set and statii are passed
      utils.checkForStartStopDatesAndStatus(groupOneLightConfirmation.getResult(), false);

      //This test should pass, if not dump info into .actual file and assert
      if (testPassed == false)
      {
        utils.displayFailureData(groupOneLightConfirmation.getResult(), groupOneLightConfirmation.getLimit(), os);
        Expect.expect(false, "Light Image Group One Confirmation Failed");
      }

      //---------------------Now group two---------------------
      //Get an instance of the test
      ConfirmCameraGroupTwoLightImageTask groupTwoLightConfirmation =
          ConfirmCameraGroupTwoLightImageTask.getInstance();

      //Run the test
      testPassed = groupTwoLightConfirmation.executeUnconditionally();

      //Check to make sure start and stop dates are set and statii are passed
      utils.checkForStartStopDatesAndStatus(groupTwoLightConfirmation.getResult(), false);

      //This test should pass, if not dump info into .actual file and assert
      if (testPassed == false)
      {
        utils.displayFailureData(groupTwoLightConfirmation.getResult(), groupTwoLightConfirmation.getLimit(), os);
        Expect.expect(false, "Light Image Group Two Confirmation Failed");
      }

      //---------------------Now group three---------------------
      //Get an instance of the test
      ConfirmCameraGroupThreeLightImageTask groupThreeLightConfirmation =
          ConfirmCameraGroupThreeLightImageTask.getInstance();

      //Run the test
      testPassed = groupThreeLightConfirmation.executeUnconditionally();

      //Check to make sure start and stop dates are set and statii are passed
      utils.checkForStartStopDatesAndStatus(groupThreeLightConfirmation.getResult(), false);

      //This test should pass, if not dump info into .actual file and assert
      if (testPassed == false)
      {
        utils.displayFailureData(groupThreeLightConfirmation.getResult(), groupThreeLightConfirmation.getLimit(), os);
        Expect.expect(false, "Light Image Group Three Confirmation Failed");
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
    }
    catch (Exception e)
    {
      e.printStackTrace(os);
    }
    finally
    {
      xRayCameraArray.disableUnitTestMode();
      utils.restoreHWCalib(getTestSourceDir());
    }
  }
}
