package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * The CameraImageConfiramtionEventDrivenHardwareTask class is a super class that
 * both the Camera Light and Camera Dark confirmatino tasks can inherit from. It
 * provides common functionality for those classes, most importantly a common image
 * analysis function. The limits are specific to each confirmation (Dark or Light)
 * but the algorithm to get the result values is the same for both light and dark
 * images.
 *
 * This class also provides a common createResultsObject() method, since this is
 * common code that can be leveraged in a super class.
 *
 * @author Reid Hayhow
 */
public abstract class ConfirmCameraImageTask extends EventDrivenHardwareTask
{
  //Logging variables
  private DirectoryLoggerAxi _logger;
  protected String _logDirectory;
  private int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.IMAGE_CONFIRM_MAX_LOG_FILE_NUMBER);
  private static final int _logTimeoutSeconds = 1;
  private static final int _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000;

  //Static values for adjusting camera indexes
  private static final int FIRST_CAMERA_INDEX = 0;
  private static final int ADJUSTED_FIRST_CAMERA_INDEX = 14;

  //Members for saving and restoring the motion profile of the panel positioner
  protected PanelPositioner _panelPositioner = PanelPositioner.getInstance();
  private PanelHandler _panelHandler = PanelHandler.getInstance();
  private MotionProfile _panelPositionerSavedProfile;

  //Parallel Thread Task executor to handle saving failed images in parallel
  private ExecuteParallelThreadTasks<Boolean> _executor = new ExecuteParallelThreadTasks<Boolean>();
  private TreeSet<AbstractXrayCamera> _failingCameraSet = new TreeSet<AbstractXrayCamera>();

  //Place to store the localized name of the task (Light or Dark)
  private String _name;
  protected String _localizedLightOrDarkString;

  protected static Config _config = Config.getInstance();

  //This is the stated height needed for the images used for these confirmations
  //Package private so that other classes can access this value
  protected static final int IMAGE_PIXEL_HEIGHT = 1000;
//  protected static final int CAMERA_PIXEL_WIDTH = AbstractXrayCamera.getSensorWidthInPixels();
//  protected static final int USABLE_IMAGE_PIXEL_WIDTH = CAMERA_PIXEL_WIDTH - 4;

  protected static final boolean FORWARD_IMAGES = true;
  protected static final boolean BACKWARD_IMAGES = false;

  //Static references to the three sets of camera groups for use by sub-classes
  protected static Set<AbstractXrayCamera> _cameraGroupOne = new TreeSet<AbstractXrayCamera>();
  protected static Set<AbstractXrayCamera> _cameraGroupTwo = new TreeSet<AbstractXrayCamera>();
  protected static Set<AbstractXrayCamera> _cameraGroupThree = new TreeSet<AbstractXrayCamera>();
  protected static Set<AbstractXrayCamera> _cameras = new TreeSet<AbstractXrayCamera>();

  // Swee Yee Wong - the offset below is taken away since 254000 nanometers offset will not affect the result of CD&A
//  //static, final references to the X/Y offset from the Grayscale Cal position
//  protected static final int _OFFSET_FROM_GRAYSCALE_CAL_LOCATION_IN_NANOMETERS = 254000;

  //Member variable for camera set being used
  protected Set<AbstractXrayCamera> _camerasToGetImagesFromSet = _cameras;

  //The Limits and Results are mapped by direction, camera and then test type
  protected Map<ScanPassDirectionEnum, Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Result>>> _scanPassDirectionToCameraToTestTypeToResultMap = new HashMap<ScanPassDirectionEnum, Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Result>>>();
  protected Map<ScanPassDirectionEnum, Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Limit>>> _scanPassDirectionToCameraToTestTypeToLimitMap = new HashMap<ScanPassDirectionEnum, Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Limit>>>();

  //Xray source instance for all sub-classes to use
  protected static AbstractXraySource _xraySource = AbstractXraySource.getInstance();

  //X and Y stage position Calib enum for each task
  protected HardwareCalibEnum _xPosition;
  protected HardwareCalibEnum _yPosition;

  //Motion profile to use when moving stage, use the fastest
  protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;

  //Storage for the state of the xrays before the task was executed
  protected boolean _wereXraysOn = false;

  //In order to know how to reposition panel, we need to know which side of the
  //tester this test is running on
  private boolean _isRightSideTest = true;

  //In order to know how to reposition panel, we need to know which side of the
  //tester this test is running on
  private boolean _isHighMag = false;
  
  /**
   * @author Anthony Fong
   */
  protected ConfirmCameraImageTask(String name,
                                   int frequency,
                                   long timeoutInSeconds,
                                   String logDirectoryPathString,
                                   ProgressReporterEnum progressReporter,
                                   boolean highMag )
  {
    this(name, frequency, timeoutInSeconds,logDirectoryPathString,progressReporter);
    _isHighMag = highMag; //previously added by Anthony for CD&A task.
    
    //This is use for Confirmation And Adjustment passive mode - added by Siew Yeng 
    _isHighMagTask = highMag;
  }
 /**
   * @author Reid Hayhow
   */
  protected ConfirmCameraImageTask(String name,
                                   int frequency,
                                   long timeoutInSeconds,
                                   String logDirectoryPathString,
                                   ProgressReporterEnum progressReporter)
  {
    super(name, frequency, timeoutInSeconds, progressReporter);

    _isHighMag =false;
    //Save the name passed in from the sub-class to use creating the limits
    _name = name;

    //Can't log to nowhere
    Assert.expect(logDirectoryPathString != null);

    //Save this location for use later saving image copies
    _logDirectory = logDirectoryPathString;

    //Create the directory logger
    _logger = new DirectoryLoggerAxi(_logDirectory, FileName.getCameraImageLogFileBasename(),
                                     FileName.getLogFileExtension(), _maxLogFilesInDirectory);

    //This confirmation and all sub-classed confirmations are automated
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;
  }

  /**
   * The image analysis function. It takes the image and camera as input.
   * Then it calculates the various values and stores them in the appropriate
   * results. It needs, as imput, the camera and direction the image is from
   *
   * @author Reid Hayhow
   */
  protected void analyzeImage(Image imageToAnalyze,
                            ScanPassDirectionEnum directionEnum,
                            AbstractXrayCamera cameraImageIsFrom)
  {
    //Get the preCreated map of results
    Map<CameraImageDataTypeEnum, Result> mapOfResults = _scanPassDirectionToCameraToTestTypeToResultMap.get(directionEnum).get(cameraImageIsFrom);

    //Also get the limits so we can check them and know if this image passed or failed
    Map<CameraImageDataTypeEnum, Limit> mapOfLimits = _scanPassDirectionToCameraToTestTypeToLimitMap.get(directionEnum).get(cameraImageIsFrom);

    //Accumulator to know if this image passed or failed limit checking
    boolean passed = true;

    Assert.expect(mapOfResults != null);
    Assert.expect(mapOfLimits != null);
    Assert.expect(imageToAnalyze != null);

    // We can just get any camera so we can get the sensor attributes
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    //We are interested in the usable image, so specify the region of interest that
    //is the same size as the usable image
    int imageWidthInPixels = xRayCamera.getNumberOfSensorPixelsUsed();
    RegionOfInterest regionOfInterest = new RegionOfInterest(0,
                                                             0,
                                                             imageWidthInPixels,
                                                             IMAGE_PIXEL_HEIGHT,
                                                             0,
                                                             RegionShapeEnum.RECTANGULAR);

    //Get the vertical profile of this image. This averages all of the image

    if(regionOfInterest.fitsWithinImage(imageToAnalyze) == false)
    {
      System.out.println("regionOfInterest: w=" + regionOfInterest.getWidth() + " h=" + regionOfInterest.getHeight());
      System.out.println("imageToAnalyze : w=" + imageToAnalyze.getWidth() + " h=" + imageToAnalyze.getHeight());
      
      regionOfInterest = RegionOfInterest.createRegionFromImage(imageToAnalyze);
    }
    //columns into an array the width of the image
    float[] horizontalProfile = ImageFeatureExtraction.profile(imageToAnalyze, regionOfInterest);

    //Make sure the profile is the right size
    Assert.expect(horizontalProfile.length == imageWidthInPixels);

    //Check the Max and Min from the vertical array and store the results
    float maxHorizontalGrayInHorizontalProfile = ArrayUtil.max(horizontalProfile);
    float minGrayInHorizontalProfile = ArrayUtil.min(horizontalProfile);

    //Store the absolute value of the difference between the max and the min
    mapOfResults.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS).setResults(new Double(Math.abs(maxHorizontalGrayInHorizontalProfile -
                                                                                                 minGrayInHorizontalProfile)));

    //Check this result against the limits for it and accumulate the result
    passed &= mapOfLimits.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS).didTestPass(mapOfResults.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS));

    //Now, to get the horizontal array of data, we have to specify a rotated region of interest
    //specifying 90 degrees in the fifth parameter

    RegionOfInterest rotatedRegionOfInterest = new RegionOfInterest(0,
                                                                    0,
                                                                    imageWidthInPixels,
                                                                    IMAGE_PIXEL_HEIGHT,
                                                                    90,
                                                                    RegionShapeEnum.RECTANGULAR);

    //We should get back a horizontal array of averaged values from each row of the image
    float[] verticalProfile = ImageFeatureExtraction.profile(imageToAnalyze, rotatedRegionOfInterest);

    //Array should be the height we expect
    Assert.expect(verticalProfile.length == IMAGE_PIXEL_HEIGHT);

    //Check the Max and Min for the horizontal array
    float maxGrayInVerticalProfile = ArrayUtil.max(verticalProfile);
    float minGrayInVerticalProfile = ArrayUtil.min(verticalProfile);

    //Store the absolute value of the difference between the max and the min
    mapOfResults.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS).setResults(new Double(Math.abs(maxGrayInVerticalProfile -
                                                                                               minGrayInVerticalProfile)));

    //Check this result against the limits for it and accumulate the result
    passed &= mapOfLimits.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS).didTestPass(mapOfResults.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS));

    //Allocate storage for the mean and standard deviation we are about to get
    DoubleRef meanDoubleRef = new DoubleRef();
    DoubleRef stdDevDoubleRef = new DoubleRef();

    //Get the standard deviation and mean from the image and store them
    Statistics.meanStdDev(imageToAnalyze, meanDoubleRef, stdDevDoubleRef);

    mapOfResults.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY).setResults(new Double(meanDoubleRef.getValue()));
    mapOfResults.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).setResults(new Double(stdDevDoubleRef.getValue()));

    //Check these results against the limits for them and accumulate the results
    passed &= mapOfLimits.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY).didTestPass(mapOfResults.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY));
    passed &= mapOfLimits.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).didTestPass(mapOfResults.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION));

    //If the image didn't pass the limit checking *or* if the user specified
    //that we should save the images via an env variable, then save them in parallel
    if (passed == false || (_config.getBooleanValue(SoftwareConfigEnum.SAVE_ALL_CONFIRMATION_IMAGES) == true))
    {
      //Crop the image before saving it
      Image imageToSave = Image.createCopy(imageToAnalyze, regionOfInterest);

      //Create the thread task to save this specific image
      ImageSaverThreadTask saverThread = new ImageSaverThreadTask(imageToSave,
                                                                  _logDirectory + File.separator + "Camera_" + cameraImageIsFrom.getId() +
                                                                  "_" + _localizedLightOrDarkString + "_"
                                                                  + directionEnum.toString() + ".png");

      //Submit the thread task to the executor. We will check that all tasks
      //completed later
      _executor.submitThreadTask(saverThread);
      imageToSave.decrementReferenceCount();
    }
  }

  /**
   * Common function to gather images and analyze them. It needs to know the
   * direction the images should be taken in (currently a boolean but could be an
   * enum) and for creating an image it needs to know the direction the image
   * is being taken in and if the image is light or dark.
   *
   * @author Reid Hayhow
   */
  protected void getAndAnalyzeImageMap(ScanPassDirectionEnum directionEnum) throws XrayTesterException
  {
    //Do some basic checking on the camera sets before using them
    int totalNumberOfCameras = XrayCameraArray.getNumberOfCameras();

    //GroupOne and GroupTwo should include all cameras, as should the allCameras
    //set. Since allCameras = GroupOne + GroupTwo, we get a nice cross check as well
    if ((_cameraGroupOne.size() + _cameraGroupTwo.size() + _cameraGroupThree.size() != totalNumberOfCameras) || (_cameras.size() != totalNumberOfCameras))
    {
      throw new HardwareTaskExecutionException(ConfirmationExceptionEnum.INCORRECT_CAMERA_LIST_IN_HARDWARE_CALIB);
    }

    //Create an image retreiver to handle this request
    CameraImageRetreiver imageRetriever = new CameraImageRetreiver(_camerasToGetImagesFromSet, directionEnum);

    // Wrap getting the image map in a try block to guarantee that we purge the map even if something goes wrong
    try
    {
      //Get the images back from the retreiver
      //Map<AbstractXrayCamera, Image> cameraToResultImageMap = imageRetriever.getImageMap(true);
      Map<AbstractXrayCamera, Image> cameraToResultImageMap = null;
      
      if(_isHighMag)
      {
        cameraToResultImageMap = imageRetriever.getImageMap(false, isTurnOnXray());
      }
      else
      {
        cameraToResultImageMap = imageRetriever.getImageMap(true, isTurnOnXray());
      }

      //Since we requested images from a list of cameras, I assert we got
      //one for each in the list
      Assert.expect(cameraToResultImageMap.size() == _camerasToGetImagesFromSet.size());

      //Get the set of keys in the map of returned images
      Set<AbstractXrayCamera> cameraSet = cameraToResultImageMap.keySet();

      for (AbstractXrayCamera camera : cameraSet)
      {
        //I can do an even more specific check to make sure every
        //camera key in the resultImageMap is also in the list of cameras
        Assert.expect(_camerasToGetImagesFromSet.contains(camera));

        //Analyze the images and store the results
        analyzeImage(cameraToResultImageMap.get(camera), directionEnum, camera);
      }
    }
    finally
    {
      imageRetriever.clearResultImageMap();
    }
  }

  /**
   * Create the results object for this test result.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject() throws DatastoreException
  {
    _result = new Result(_name);

    _scanPassDirectionToCameraToTestTypeToResultMap.clear();

    readCameraListFromFile();

    //Populate it for forward and reverse directions
    populateResultMap(ScanPassDirectionEnum.FORWARD);
    populateResultMap(ScanPassDirectionEnum.REVERSE);
  }

  /**
   * Method to populate the result map for a specific camera direction.
   *
   * @author Reid Hayhow
   */
  private void populateResultMap(ScanPassDirectionEnum directionEnum)
  {
    //Get all of the localized strings we will need for the results
    //First, the the common building block keys
    String dirString = StringLocalizer.keyToString("ACD_FORWARD_KEY");

    if (directionEnum.equals(ScanPassDirectionEnum.REVERSE))
    {
      dirString = StringLocalizer.keyToString("ACD_BACKWARD_KEY");
    }

    String localizedCameraString = StringLocalizer.keyToString("ACD_CAMERA_KEY");

    //Then get the localized strings that are specific to this Confirmation
    String localizedHorizontalFlatness = StringLocalizer.keyToString("CD_HORIZONTAL_FLATNESS_KEY");
    String localizedVerticalFlatness = StringLocalizer.keyToString("CD_VERTICAL_FLATNESS_KEY");
    String localizedMeanGray = StringLocalizer.keyToString("CD_MEAN_GRAY_KEY");
    String localizedGrayStandardDeviation = StringLocalizer.keyToString("CD_GRAY_STANDARD_DEVIATION_KEY");

    Result directionResultContainer = new Result(dirString);
    _result.addSubResult(directionResultContainer);

    Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Result>> mapOfCamerasToTestResults = new TreeMap<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Result>>();

    _scanPassDirectionToCameraToTestTypeToResultMap.put(directionEnum, mapOfCamerasToTestResults);

    //Create a sub map of results for each camera
    for (AbstractXrayCamera camera : _camerasToGetImagesFromSet)
    {
      Map<CameraImageDataTypeEnum, Result> mapOfResultsToTests = new TreeMap<CameraImageDataTypeEnum, Result>();

      mapOfCamerasToTestResults.put(camera, mapOfResultsToTests);

      Result cameraResultContainer = new Result(localizedCameraString + camera.getId());
      directionResultContainer.addSubResult(cameraResultContainer);

      Result horizontalFlatnessResult = new Result(new Double(0.1), localizedHorizontalFlatness);
      cameraResultContainer.addSubResult(horizontalFlatnessResult);
      mapOfResultsToTests.put(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS, horizontalFlatnessResult);

      Result verticalFlatnessResult = new Result(new Double(0.1), localizedVerticalFlatness);
      cameraResultContainer.addSubResult(verticalFlatnessResult);
      mapOfResultsToTests.put(CameraImageDataTypeEnum.VERTICAL_FLATNESS, verticalFlatnessResult);

      Result meanPixelGrayResult = new Result(new Double(220.0), localizedMeanGray);
      cameraResultContainer.addSubResult(meanPixelGrayResult);
      mapOfResultsToTests.put(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY, meanPixelGrayResult);

      Result stdDevResult = new Result(new Double(6.0), localizedGrayStandardDeviation);
      cameraResultContainer.addSubResult(stdDevResult);
      mapOfResultsToTests.put(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION, stdDevResult);
    }
  }

  /**
   * Set up the limits for this specific task. Because we have the name of the
   * sub-class task, this can be handled by the super class. These values are
   * read out of the CustomerLimits.properties file.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject() throws DatastoreException
  {
    _limits = new Limit(null, null, null, _name);

    _scanPassDirectionToCameraToTestTypeToLimitMap.clear();

    readCameraListFromFile();

    //Create the limits maps for both directions
    populateLimitsMap(ScanPassDirectionEnum.FORWARD);
    populateLimitsMap(ScanPassDirectionEnum.REVERSE);
  }

  /**
   * Method to simplify populating the Limits map based on the camera direction
   *
   * @author Reid Hayhow
   */
  private void populateLimitsMap(ScanPassDirectionEnum directionEnum) throws DatastoreException
  {

    //Get all of the localized strings we will need for the limits
    //First, the the common building block keys
    String localizedCameraString = StringLocalizer.keyToString("ACD_CAMERA_KEY");
    String localizedGrayValue = StringLocalizer.keyToString("ACD_GRAY_VALUE_KEY");

    //Then get the localized strings that are specific to this Confirmation
    String localizedHorizontalFlatness =
        StringLocalizer.keyToString("CD_HORIZONTAL_FLATNESS_KEY");
    String localizedVerticalFlatness =
        StringLocalizer.keyToString("CD_VERTICAL_FLATNESS_KEY");
    String localizedMeanGray =
        StringLocalizer.keyToString("CD_MEAN_GRAY_KEY");
    String localizedGrayStandardDeviation =
        StringLocalizer.keyToString("CD_GRAY_STANDARD_DEVIATION_KEY");

    Double horizFlatnessHighLimit = _parsedLimits.getHighLimitDouble("HorizontalFlatness");
    Double horizFlatnessLowLimit = _parsedLimits.getLowLimitDouble("HorizontalFlatness");
    Double vertFlatnessHighLimit = _parsedLimits.getHighLimitDouble("VerticalFlatness");
    Double vertFlatnessLowLimit = _parsedLimits.getLowLimitDouble("VerticalFlatness");
    Double meanPixelHighLimit = _parsedLimits.getHighLimitDouble("MeanPixelGray");
    Double meanPixelLowLimit = _parsedLimits.getLowLimitDouble("MeanPixelGray");
    Double stdDevHighLimit = _parsedLimits.getHighLimitDouble("GrayStandardDeviation");
    Double stdDevLowLimit = _parsedLimits.getLowLimitDouble("GrayStandardDeviation");
    
    // Kok Chun, Tan - XCR-3153  - set different customer limit value for 23um and 19um
    if (_isHighMag == false && _config.getSystemCurrentLowMagnification().equalsIgnoreCase("M23"))
    {
      meanPixelHighLimit = _parsedLimits.getHighLimitDouble("MeanPixelGrayFor23um");
      meanPixelLowLimit = _parsedLimits.getLowLimitDouble("MeanPixelGrayFor23um");
    }

    String dirString = StringLocalizer.keyToString("ACD_FORWARD_KEY");

    if (directionEnum.equals(ScanPassDirectionEnum.REVERSE))
    {
      dirString = StringLocalizer.keyToString("ACD_BACKWARD_KEY");
    }

    Limit directionLimitContainer = new Limit(null, null, null,
                                              StringLocalizer.keyToString("ACD_DIRECTION_KEY")
                                              + dirString);
    _limits.addSubLimit(directionLimitContainer);

    Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Limit>> mapOfCamerasToTestLimits = new HashMap<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Limit>>();

    _scanPassDirectionToCameraToTestTypeToLimitMap.put(directionEnum, mapOfCamerasToTestLimits);

    for (AbstractXrayCamera camera : _camerasToGetImagesFromSet)
    {
      Limit cameraContainer = new Limit(null, null, null, localizedCameraString + camera.getId());
      directionLimitContainer.addSubLimit(cameraContainer);

      Map<CameraImageDataTypeEnum, Limit> mapOfTestsToLimits = new TreeMap<CameraImageDataTypeEnum, Limit>();
      mapOfCamerasToTestLimits.put(camera, mapOfTestsToLimits);


      Limit horizontalFlatness = new Limit(horizFlatnessLowLimit,
                                           horizFlatnessHighLimit,
                                           localizedGrayValue,
                                           localizedHorizontalFlatness);
      cameraContainer.addSubLimit(horizontalFlatness);
      mapOfTestsToLimits.put(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS, horizontalFlatness);

      Limit verticalFlatness = new Limit(vertFlatnessLowLimit,
                                         vertFlatnessHighLimit,
                                         localizedGrayValue,
                                         localizedVerticalFlatness);
      cameraContainer.addSubLimit(verticalFlatness);
      mapOfTestsToLimits.put(CameraImageDataTypeEnum.VERTICAL_FLATNESS, verticalFlatness);

      Limit meanPixelGray = new Limit(meanPixelLowLimit,
                                      meanPixelHighLimit,
                                      localizedGrayValue,
                                      localizedMeanGray);
      cameraContainer.addSubLimit(meanPixelGray);
      mapOfTestsToLimits.put(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY, meanPixelGray);

      Limit stdDev = new Limit(stdDevLowLimit,
                               stdDevHighLimit,
                               localizedGrayValue,
                               localizedGrayStandardDeviation);
      cameraContainer.addSubLimit(stdDev);
      mapOfTestsToLimits.put(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION, stdDev);
    }
  }

  /**
   * Common logging method. This provides a comma delimited log file that can
   * be used for debugging the results of this confirmation task.
   *
   * @author Reid Hayhow
   *
   */
  void logInformation(boolean forceLogging) throws DatastoreException
  {
    StringBuffer logData = new StringBuffer();

    logData.append("Direction, Camera Num, Horiz Flatness, Vert Flatness," +
                   "Mean Pixel Value, Pixel Std Dev\n");

    logDirectionSpecificInfo(ScanPassDirectionEnum.FORWARD, logData);
    logDirectionSpecificInfo(ScanPassDirectionEnum.REVERSE, logData);

    if (forceLogging)
    {
      _logger.log(logData.toString());
    }
    else
    {
      _logger.logIfTimedOut(logData.toString(), _logTimeoutMilliSeconds);
    }
  }

  /**
   * Wrap the common logging infrastructure, excluding direction information
   * in a single method.
   *
   * @author Reid Hayhow
   */
  private void logDirectionSpecificInfo(ScanPassDirectionEnum directionEnum,
                                        StringBuffer logData)
  {
    Assert.expect(directionEnum != null);
    Assert.expect(logData != null);

    List<AbstractXrayCamera> cameras = CalibrationSupportUtil.getSystemFiducialCameras();

    char directionChar = ' ';
    if (directionEnum.equals(ScanPassDirectionEnum.FORWARD))
    {
      directionChar = 'F';
    }
    else
    {
      directionChar = 'B';
    }

    //Get the result map and the limit map to analyze the failures
    Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Result>> mapOfCamerasToTests = _scanPassDirectionToCameraToTestTypeToResultMap.get(directionEnum);
    Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Limit>> mapOfTestLimits = _scanPassDirectionToCameraToTestTypeToLimitMap.get(directionEnum);

    if ((mapOfCamerasToTests != null) && (mapOfCamerasToTests.isEmpty() == false))
    {
      //Get the line separator character only once
      String lineSeparator = System.getProperty("line.separator");

      for (AbstractXrayCamera camera : cameras)
      {
        //Get the sub-maps for this specific camera containing the results and limits
        Map<CameraImageDataTypeEnum, Result> mapOfTestsToResults = mapOfCamerasToTests.get(camera);
        Map<CameraImageDataTypeEnum, Limit> mapOfLimits = mapOfTestLimits.get(camera);

        if (mapOfTestsToResults != null && !mapOfTestsToResults.isEmpty())
        {
          logData.append(directionChar + "," + camera.getId() + ",");

          //Log the horizontal flatness information
          if (mapOfLimits.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS).didTestPass(mapOfTestsToResults.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS)))
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS).getResultString() + ",");
          }
          //If the horizontal flatness failed, log detailed information
          else
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS).getResultString() +
                           " FAILED (limit = " +
                           mapOfLimits.get(CameraImageDataTypeEnum.HORIZONTAL_FLATNESS).toString() +
                           "),");
          }

          //Log the vertical flatness info
          if (mapOfLimits.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS).didTestPass(mapOfTestsToResults.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS)))
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS).getResultString() + ",");
          }
          //Again, be verbose if it failed
          else
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS).getResultString() +
                           " FAILED (limit = " +
                           mapOfLimits.get(CameraImageDataTypeEnum.VERTICAL_FLATNESS).toString() +
                           "),");
          }

          //Log the pixel gray values
          if (mapOfLimits.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY).didTestPass(mapOfTestsToResults.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY)))
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY).getResultString() + ",");
          }
          //Again, be verbose if it fails
          else
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY).getResultString() +
                           " FAILED (limit = " +
                           mapOfLimits.get(CameraImageDataTypeEnum.MEAN_PIXEL_GRAY).toString() +
                           "),");
          }

          //Finally, log the Pixel Gray standard deviation
          if (mapOfLimits.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).didTestPass(mapOfTestsToResults.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION)))
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).getResultString() + ",");
          }
          //Log verbosely if it fails
          else
          {
            logData.append(mapOfTestsToResults.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).getResultString() +
                           " FAILED (limit = " +
                           mapOfLimits.get(CameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).toString() +
                           "),");
          }
          //terminate the line with the appropriate character
          logData.append(lineSeparator);
        }
      }
    }
  }

  /**
   * Method heavily leveraged from Eddie's CalGrayscale list setup since we are
   * interested in the same list of cameras
   *
   * @author Reid Hayhow
   */
  private void readCameraListFromFile() throws DatastoreException
  {
    //Create the Set for all cameras
    _cameras.clear();

    //Create the set for camera group one, two an three. These calls automatically
    //add any camera encountered to the _allCameras Set
    createCameraList(_cameraGroupOne, HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_ONE_LIST_OF_CAMERAS);
    createCameraList(_cameraGroupTwo, HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_TWO_LIST_OF_CAMERAS);
    createCameraList(_cameraGroupThree, HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_THREE_LIST_OF_CAMERAS);
    _cameras.addAll(_cameraGroupOne);
    _cameras.addAll(_cameraGroupTwo);
    _cameras.addAll(_cameraGroupThree);
  }

  /**
   * Method to get a string from the config file, parse it and create a set of
   * cameras from that list.
   *
   * @author Reid Hayhow
   */
  private void createCameraList(Set<AbstractXrayCamera> cameraGroupSet, HardwareCalibEnum hwCalibEnumToParse) throws DatastoreException
  {
    Assert.expect(cameraGroupSet != null);
    Assert.expect(hwCalibEnumToParse != null);

    //If this method is called the set will be cleared
    cameraGroupSet.clear();

    //Get the value from the config file
    String cameraList = _config.getStringValue(hwCalibEnumToParse);

    //It is ',' delimited, so split it into pieces
    String[] splitCameraList = cameraList.split(",");

    //For every individual camera string, create an Integer and add it to the Set
    for (String cameraNumberString : splitCameraList)
    {
      //Allow the special case of "none" as a stand in for no cameras
      if(cameraNumberString.equalsIgnoreCase("none"))
      {
        continue;
      }
        // The string should be just a number but let's trim it to be
        // forgiving of extra spaces.

        //Storage for the parsed camera number
        int cameraId = 0;
        try
        {
          //try to parse the string value to get the camera number
          cameraId = StringUtil.convertStringToInt(cameraNumberString.trim());
        }
        //Handle a parse exception
        catch (BadFormatException ex)
        {
          //Display the enum key (aka name), file name, and the value that wasn't an int
          InvalidValueDatastoreException invalidValueException = new InvalidValueDatastoreException(hwCalibEnumToParse.getKey(),
                                                                                                    cameraNumberString.trim(),
                                                                                                    hwCalibEnumToParse.getFileName());
          invalidValueException.initCause(ex);
          throw invalidValueException;
        }
        //If we got here the number parsed and can be added to the list
        XrayCameraIdEnum idEnum = XrayCameraIdEnum.getEnum(new Integer(cameraId));
        cameraGroupSet.add(XrayCameraArray.getCamera(idEnum));
    }
  }

  /**
   * The execute method for the Camera Image Confirmation tests. It allows sub-classes
   * to move the stage and control xrays before and after acquiring images. It then
   * analyzes those images to make sure their gray values are within the specified limits.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {   
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
	//Swee Yee Wong - Camera debugging purpose
    debug("ConfirmCameraImageTask");
      boolean deferred = false;

      try
      {
        //Special case, if the camera list is empty, don't continue because lots of
        //downstream code doesn't like 0 length lists
        if (_camerasToGetImagesFromSet.isEmpty())
        {
          //Create a passing result
          String localizedTaskName = getName();
          _limits = new Limit(null, null, null, localizedTaskName);
          _result = new Result(new Boolean(true), localizedTaskName);
          return;
        }

        reportProgress(0);

        //Handle the fact that we cannot run light confirmations with a panel
        //in the system. Defer the task till later *but* don't cause the
        //task to fail. Display a meaningful message in the Service UI
        if (ableToRunWithCurrentPanelState() == false)
        {
          _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
          _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_PANEL_INTEFERRED_WITH_LIGHT_CONFIRM_KEY"));
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_REMOVE_THE_PANEL_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
          return;
        }

        //move the stage (if necessary) before executing the task
        moveStageBeforeExecute();

        //Save the state of xrays before fiddling with them
        _wereXraysOn = _xraySource.areXraysOn();

        //Let sub-classes determine if xrays should be on or off before the task runs
        setXraySourceBeforeExecute();

        //Turning on xrays will take time, so give a progress update
        reportProgress(10);

        //For speed, distribute the different requests for images to multiple threads
        //one per camera. These tasks can run in parallel. First get the images
        //from all cameras in the forward direction
        getAndAnalyzeImageMap(ScanPassDirectionEnum.FORWARD);

        //Report progress before fiddling with xrays
        reportProgress(40);

        //The call to the IAE to get the forward images will turn off xrays, give
        //tasks a chance to turn them back on (if they want)
        setXraySourceBeforeExecute();

        //Fiddling with xrays takes time, so report again
        reportProgress(60);

        //Now get the images in the backward direction
        getAndAnalyzeImageMap(ScanPassDirectionEnum.REVERSE);

        //Wait for any of the images that needed to be saved to actually be saved
        _executor.waitForTasksToComplete();

        //Report that this task is done
        reportProgress(100);

      }
      catch (XrayTesterException ex)
      {
        _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
        deferred = true;
        return;
      }
      //Log any unexpected exceptions
      catch (Exception e)
      {
        Assert.logException(e);
      }
      //Restore the system to the previous state
      finally
      {
        //For now force logging
        if(deferred == false)
          logInformation(_forceLogging);

        //Restore stage (if needed)
        moveStageAfterExecute();

        //Return xrays to initial state
        setXraySourceAfterExecute();

        //Create the detailed failure info for observers
        if(deferred == false)
          populateResultMessage();

      }
  }

  /**
   * Set the message in the Result object. This info is available to any observer
   * who wants to display detailed failure information.
   *
   * @author Reid Hayhow
   */
  private void populateResultMessage()
  {
    //Clear the treeSet before starting
    _failingCameraSet.clear();

    if (getLimit().didTestPass(getResult()) == false)
    {
      //if it did fail, get the set of direction keys (only two now)
      Set<ScanPassDirectionEnum> setOfDirections = _scanPassDirectionToCameraToTestTypeToResultMap.keySet();

      //For all directions, get the camera sepecific results map
      for (ScanPassDirectionEnum enumKey : setOfDirections)
      {
        Map<AbstractXrayCamera, Map<CameraImageDataTypeEnum, Result>> cameraResultMap = _scanPassDirectionToCameraToTestTypeToResultMap.get(enumKey);

        //Now get the set of cameras from this map
        Set<AbstractXrayCamera> cameraSet = cameraResultMap.keySet();

        //For each camera, we have to get each sub-task result
        for (AbstractXrayCamera camera : cameraSet)
        {
          Map<CameraImageDataTypeEnum, Result> specificCameraResultMap = cameraResultMap.get(camera);

          //Get the keys for the sub-task results
          Set<CameraImageDataTypeEnum> setOfResultKeys = specificCameraResultMap.keySet();

          //Finally ready to get the actual results themselves, whew!
          for (CameraImageDataTypeEnum resultKey : setOfResultKeys)
          {
            //Get the result for this direction->camera->sub-task
            Result resultForKey = specificCameraResultMap.get(resultKey);

            //If it failed, add the camera to the set. Sets ignore duplicates
            //so it is OK to add the same camera over and over and ...
            if (resultForKey.getStatus().equals(HardwareTaskStatusEnum.PASSED) == false)
            {
              _failingCameraSet.add(camera);
            }
          }
        }
      } //End of the for(ScanPassDirectionEnum enumKey...

      //Still in the if(didPass == false), so we know the task failed somewhere
      //so create the string buffer to display the failure info
      StringBuilder buffer = new StringBuilder();

      //Add the localized 'Camera' string
      buffer.append(StringLocalizer.keyToString("ACD_CAMERAS_PLURAL_KEY") + " ");

      //Add the set of cameras that failed. We don't care what the specific failure
      //was, at least for now
      for (AbstractXrayCamera camera : _failingCameraSet)
      {
        //Append the camera number to the list of failures
        buffer.append(String.valueOf(camera.getId()) + ",");
      }

      //Get rid of the last comma in the camera list
      buffer.deleteCharAt(buffer.length() - 1);

      //Add the localized FAILED string
      buffer.append(" " + StringLocalizer.keyToString("CD_FAILED_KEY"));

      //Add the suggestect action localized key
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_FAILED_SUGGESTED_ACTION_KEY"));

      //Set the message in the result passed to observers
      _result.setTaskStatusMessage(buffer.toString());
    } //end of if(getLimit()....
  }

  /**
   * Access to the set of failing cameras. Currently needed by Diagnostics so it
   * can 'see' into failure details that the Confirmation itself doesn't worry
   * about.
   *
   * @author Reid Hayhow
   */
  public Set<AbstractXrayCamera> getSetOfFailingCameras()
  {
    return _failingCameraSet;
  }

  /**
   * As part of the pre-condition for executing this type of task, we have to
   * save off the current motion profile set in the panel positioner so we can
   * restore it in cleanUp() after the task executes.
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to up position for low mag task.
        XrayActuator.getInstance().up(true);
      }
    }
    //Save off the active motion profile, just in case we end up moving the
    //stage and setting the profile to a point-to-point profile
    //_panelPositionerSavedProfile = _panelPositioner.getActiveMotionProfile();
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
      return;
    }
  }

  /**
   * By default all tests can run on any hardware configuration.
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * In order to clean up after the sub tasks of this type, we have to restore
   * the motion profile of the panel positioner. This is necessary because some
   * sub-classes move the stage.
   *
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
    //restore the saved profile before continuing
    //_panelPositioner.setMotionProfile(_panelPositionerSavedProfile);
    
    if (_scanPassDirectionToCameraToTestTypeToResultMap != null)
      _scanPassDirectionToCameraToTestTypeToResultMap.clear();
    
    if (_scanPassDirectionToCameraToTestTypeToLimitMap != null)
      _scanPassDirectionToCameraToTestTypeToLimitMap.clear();
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false; 
    }
  }

  /**
   * Method to handle xrays for Light Images before image capture. Turn them on
   * before executing.
   *
   * NOTE: This method is overridden for Dark Images because xrays need to be off
   * for the Dark Image test.
   *
   * @author Reid Hayhow
   */
  protected void setXraySourceBeforeExecute() throws XrayTesterException
  {
    //If they were not on, then remember this to restore it in the finally block
    if (_wereXraysOn == false)
    {
      _xraySource.on();
    }
  }

  /**
   * Method to handle xrays for Light Images after image capture. Turn them off
   * if they were off before the task. Otherwise leave them on.
   *
   * NOTE: This method is overridden for Dark Images to turn xrays back on after
   * execution, since execution will turn them off.
   *
   * @author Reid Hayhow
   */
  protected void setXraySourceAfterExecute() throws XrayTesterException
  {
    if (_wereXraysOn == false)
    {
      _xraySource.off();
    }
  }

  /**
   * Method to stage motion for Light Images before image capture. Move the stage
   * to a position that allows all the cameras in this specific group to be
   * fully exposed to xrays.
   *
   * NOTE: This method is overridden for Dark Images where stage motion is not needed
   *
   * @author Reid Hayhow
   */
  protected void moveStageBeforeExecute() throws XrayTesterException
  {
    //Reposition the panel if necessary
    //NOTE:This must be done before moving the stage to its final location
    //because the panel handler may move the stage to reposition the panel
    //(it needs to use a sensor to move the panel)
    repositionPanelIfNecessary();

    // Swee Yee Wong - the offset below is taken away since 254000 nanometers offset will not affect the result of CD&A
    
//    //Set up the stage to get it out of the way of the cameras
//    StagePositionMutable stagePosition = new StagePositionMutable();
//
//    //Stage position to get it out of the way of camera group one
//    //We also subtract out an offset to get a different stage position from the
//    //one used for the Grayscale adjustment
//    stagePosition.setXInNanometers(_config.getIntValue(_xPosition) -
//                                   _OFFSET_FROM_GRAYSCALE_CAL_LOCATION_IN_NANOMETERS);
//    stagePosition.setYInNanometers(_config.getIntValue(_yPosition) -
//                                   _OFFSET_FROM_GRAYSCALE_CAL_LOCATION_IN_NANOMETERS);

    if (PanelHandler.getInstance().moveStageToCameraClearPosition() == false)
    {
      //swee yee wong - XCR-3052	Wrong information of area mode task failed exception
      throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_name);
    }

  }

  /**
   * Method that controls stage motion after task execution.
   *
   * @author Reid Hayhow
   */
  protected void moveStageAfterExecute()
  {
    //Do nothing for now
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected boolean isTurnOnXray()
  {
    return true;
  }

  /**
   * Method to reposition the panel to the correct side of the tester
   * based on internal knowledge of which side of the tester the current camera
   * group is on.
   *
   * @author Reid Hayhow
   */
  private void repositionPanelIfNecessary() throws XrayTesterException
  {
    //Only matters if there is a panel in the system, so check that first
    if (_panelHandler.isPanelLoaded())
    {
      //Then only matters if the panel is too long to image clearly, so check that too
      if (_panelHandler.isPanelLongerThanPIPClearance())
      {
        //OK, the panel is too big, now decide if we move it right or left

        //If the camera group is on the right side of the tester, reposition the panel right
        if (_isRightSideTest == true)
        {
          // panel needs to be repositioned right
          _panelHandler.movePanelToRightSide();
        }
        //Otherwise, reposition the panel to the left
        else
        {
          // panel needs to be repositioned left
          _panelHandler.movePanelToLeftSide();
        }
      } //end of if(panel is longer than PIP clearence)
    } // end of if(panel is loaded)
  }

  /**
   * Set method to indicate the camera group is on the right side of the tester.
   * Needed to reposition the panel in the system to get the camera group 'clear'
   *
   * @author Reid Hayhow
   */
  protected void cameraGroupIsOnRightSideOfTester()
  {
    _isRightSideTest = true;
  }

  /**
   * Set method to indicate the camera group is on the left side of the tester.
   * Needed to reposition the panel in the system to get the camera group 'clear'
   *
   * @author Reid Hayhow
   */
  protected void cameraGroupIsOnLeftSideOfTester()
  {
    _isRightSideTest = false;
  }

  /**
   * Method for knowing if the task can run with the current panel state. For light
   * image tasks, this should return false if a panel is loaded and true if no
   * panel is loaded. This method is overridden for Dark tasks to always return
   * true.
   *
   * @author Reid Hayhow
   */
  protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
  {
    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    //  add throws XrayTesterException
    //flip the logic of isPanelLoaded because light tasks can't run with a panel
    return (_panelHandler.isPanelLoaded() == false);
  }
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
