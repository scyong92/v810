package com.axi.v810.business.autoCal;

import com.axi.util.Enum;

/**
 * Enum class to simplify evaluating Camera Image data values
 *
 * @author Reid Hayhow
 */
public class CameraImageDataTypeEnum extends Enum
{

  private static int index = 0;
  static final CameraImageDataTypeEnum HORIZONTAL_FLATNESS = new CameraImageDataTypeEnum(index++);
  static final CameraImageDataTypeEnum VERTICAL_FLATNESS = new CameraImageDataTypeEnum(index++);
  static final CameraImageDataTypeEnum PIXEL_GRAY_STANDARD_DEVIATION = new CameraImageDataTypeEnum(index++);
  static final CameraImageDataTypeEnum MEAN_PIXEL_GRAY = new CameraImageDataTypeEnum(index++);

  /**
   * CameraImageDataTypeEnum
   *
   * @author Reid Hayhow
   */
  protected CameraImageDataTypeEnum(int id)
  {
    super(id);
  }
}
