package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Reid Hayhow
 */
public class Test_CameraDarkConfirmationTaskTwo extends UnitTest
{
  //Create a test utils class
  private TestUtils _utils = new TestUtils();;
  /**
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CameraDarkConfirmationTaskTwo());
  }

  /**
   * @author Reid Hayhow
   */
  protected Test_CameraDarkConfirmationTaskTwo()
  {
    super();
  }

  /**
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XrayCameraArray xRayCameraArray = null;

    boolean testPassed = false;
    try
    {
      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getDarkTestTwoDir();
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);
      CalGrayscale.getInstance().executeUnconditionally();

      //Get an instance of the test to run
      ConfirmCameraDarkImageTask darkConfirmation = ConfirmCameraDarkImageTask.getInstance();

      //Execute the test
      testPassed = darkConfirmation.executeUnconditionally();

      //Check to make all start and stop dates are set and all statii are "passed"
      _utils.checkForStartStopDatesAndStatus(darkConfirmation.getResult(), true);

      //For this test, it should pass, if not, dump failure info into .actual
      //and assert
      if (testPassed == true)
      {
        Expect.expect(false, "Dark Image Confirmation Test should fail");
      }
      else
      {
        _utils.displayFailureData(darkConfirmation.getResult(), darkConfirmation.getLimit(), os);
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
      Assert.expect(false, ex.getMessage());
    }
    catch (Exception e)
    {
      e.printStackTrace(os);
      Assert.expect(false, e.getMessage());
    }
    finally
    {
      xRayCameraArray.disableUnitTestMode();
    }
  }

  /**
   * Copy the golden limits into place before starting the hardware. Also copy
   * the calib file (it will get clobbered).
   *
   * @author Reid Hayhow
   */
  protected void setupBeforeTest()
  {
    _utils.copyLimitsForRegressionTests(getTestSourceDir());
    _utils.backupHWCalib(getTestSourceDir());
  }

  /**
   * Restore the original limits into place when done. Also copy
   * the original calib file back.
   *
   * @author Reid Hayhow
   */
  protected void restoreAfterTest()
  {
    //Use the utils to copy the new limits back
    _utils.restoreLimitsAfterRegressionTests();
    _utils.restoreHWCalib(getTestSourceDir());
  }
}
