package com.axi.v810.business.autoCal;

import com.axi.v810.business.imageAcquisition.ImageAcquisitionEngine;
import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Confirmation to test camera light values. This confirmation gets a set of
 * images from the cameras and checks them for horizontal flatness, vertical
 * flatness, mean gray value and gray value standard deviation. These values
 * must fall within acceptable limits for the cameras to be functioning
 * correctly.
 *
 * This task is designed to be run when there is no panel in the system
 *
 * @author Reid Hayhow
 */
public class ConfirmCameraLightImageWithNoPanel extends ConfirmCameraImageTask
{
  private static final String NAME =
      StringLocalizer.keyToString("CD_CAMERA_LIGHT_CONFIRMATION_KEY");

  // Get frequency from hardware.calib
  private static final int FREQUENCY =
      _config.getIntValue(HardwareCalibEnum.CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY);

  // Get timeout from hardware.calib
  private static final long TIMEOUTINSECONDS =
      _config.getLongValue(SoftwareConfigEnum.CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_TIMEOUT_IN_SECONDS);

  private static ConfirmCameraLightImageWithNoPanel _instance = null;
  private static AbstractXraySource _xraySourceInstance = AbstractXraySource.getInstance();

  /**
   * Constructor for this confirmation task
   * @author Reid Hayhow
   */
  private ConfirmCameraLightImageWithNoPanel()
  {
    //Create it and specify the logging directory for the logger stored in the
    //super class
    super(NAME,
          FREQUENCY,
          TIMEOUTINSECONDS,
          Directory.getCameraLightConfirmationLogDir(),
          ProgressReporterEnum.LIGHT_CAMERA_IMAGE_QUALITY_TASK);

    //This confirmation is for camera group one
    _camerasToGetImagesFromSet = _cameras;

    //Find the correct stage position for this confirm to use from hardware.calib
    _xPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS;
    _yPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS;

    //The camera group for group one is on the right side of the tester, needed
    //to reposition panel before test
    cameraGroupIsOnRightSideOfTester();

    //This confirmation and all sub-classed confirmations are automated
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    //This is a light image task
    _localizedLightOrDarkString = StringLocalizer.keyToString("ACD_LIGHT_KEY");

    //Set the enum used to save the timestamp in the hardware.calib
    setTimestampEnum(HardwareCalibEnum.LIGHT_IMAGE_LAST_TIME_RUN);
  }

  /**
   * Instance access to this confirmation task
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCameraLightImageWithNoPanel getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCameraLightImageWithNoPanel();
    }
    return _instance;
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      //move x-rays cylinder or Z Axis up for high mag.
      XrayActuator.getInstance().up(true);
      // re-calculate magnification
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
      if(_xraySourceInstance instanceof HTubeXraySource)
      {
        HTubeXraySource htube = (HTubeXraySource)_xraySourceInstance;
        htube.on();
      }
      else
      {
        _xraySourceInstance.on();
      }
    }
    else
    {
      //For this grouping of all image light confirms, we want xrays ON to
      //optimize through put
      _xraySourceInstance.on();
    }
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
      return;
    }
  }

  /**
   *
   * @author Reid Hayhow
   *
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays cylinder are up for low mag.
        XrayActuator.getInstance().up(true);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(true,false);
      }

      // re-calculate magnification
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);

    }

    //Since we turned xrays ON in startUp, make sure to turn them off on exit
    _xraySourceInstance.off();
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;   
    }
  }


  /**
   * Method that controls stage motion after task execution.
   *
   * @author Reid Hayhow
   */
  protected void moveStageAfterExecute()
  {
      //This is a temporary solution for CD&A looping test fail (for variable mag system)
      //during Xray Spot confirmation  (after ConfirmCameraLightImageWithNoPanel)
      //where camera 0 always getting insuffucient trigger count  (trigger count=0)
      //This problem can fixed temporary by moving the XY stage to a transitional position.
      //A proper and long term solution is needed in future.
      try
      {
          if(LicenseManager.isVariableMagnificationEnabled())
          {
            //Set up the stage to get it out of the way of the cameras
            StagePositionMutable stagePosition = new StagePositionMutable();

            //Stage position to get it out of the way of camera group one
            //We also subtract out an offset to get a different stage position from the
            //one used for the Grayscale adjustment
            stagePosition.setXInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS));
            stagePosition.setYInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS));

            // Move stage to existing center position before performing CD&A
            // XCR-3457, Motion control require initialization error prompt if CDNA task timeout
            MotionProfile originalMotionProfile = _panelPositioner.getActiveMotionProfile();
            _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
            PanelPositioner.getInstance().pointToPointMoveAllAxes(stagePosition);
            _panelPositioner.setMotionProfile(originalMotionProfile);
//            try
//            {
//                Thread.sleep(2000);
//            }
//            catch (Exception ex)
//            {
//                // do nothing
//            }
          }
      }
      catch(XrayTesterException xtx)
      {
          //throw  xtx;
          //Do nothing for now
      }
     
  }
  
//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmCameraImageTask will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
//  }
}
