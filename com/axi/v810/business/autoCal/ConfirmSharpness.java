package com.axi.v810.business.autoCal;

import java.awt.image.*;
import java.io.*;
import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Base class for set of confirmations that measure the optical quality of a known
 * artifact in the system, such as the system fiducial or magnification coupon, by
 * measuring the MTF of its edges by using the FWHM of its line spread function
 * as a proxy for MTF.  It also tests panel positioning by measuring the
 * location of its edges.   MTF is an optical measurement
 * of how well an imaging system captures contrasts between black and white lines.  The
 * smaller the number, the better.
 *
 * @author John Dutton
 */
public abstract class ConfirmSharpness extends EventDrivenHardwareTask
{
  protected static Config _config = Config.getInstance();

  protected static final int _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED = _config.getIntValue(HardwareCalibEnum.CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED);
  private static final int _maxLogFileSizeInBytes = _config.getIntValue(SoftwareConfigEnum.SHARPNESS_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES);
  private static final Boolean _SAVE_DEBUG_IMAGES = _config.getBooleanValue(SoftwareConfigEnum.SAVE_ALL_CONFIRMATION_IMAGES);

  //Store state of xrays before task is executed, so can restore when finished.
  protected boolean _wereXraysOn = false;

  // A switch and location to save image files for debugging purposes.
  private boolean _saveDebugImages = false;
  private String _debugImagesDirectoryPath;

  // Logging variables.
  private String _logDirectoryFullPath;
  private String _logFileName;
  private FileLoggerAxi _logger;

  private int _progressPercentage = 0;

  // Result list.
  private List<Object> _localResultList = new ArrayList<Object>();

  protected int _edgeProfileOversample;
  protected double[] _edgeProfile;

  // For debugging purposes, read image from file.  This is a shortcut to avoid having to
  // setup images for camera simulators.
  private boolean _readImageFromFile = false;


  static
  {
    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException ex)
    {
      Assert.expect(false, "Can't load config files");
    }
  }

  /**
   * @author John Dutton
   */
  protected ConfirmSharpness(String nameOfCal,
                             int frequency,
                             long timeoutInSeconds,
                             String logDirectoryFullPath,
                             String logFileName,
                             ProgressReporterEnum progressReportEnum)
  {
    super(nameOfCal, frequency, timeoutInSeconds, progressReportEnum);
    _logDirectoryFullPath = logDirectoryFullPath;
    _logFileName = logFileName;
  }

  /**
   * @author John Dutton
   */
  protected void setUp() throws XrayTesterException
  {  
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to up position for low mag task.
        XrayActuator.getInstance().up(true);
      }
    }
    
    if (_SAVE_DEBUG_IMAGES)
    {
      EdgeMTF.setSaveDebugFiles(_logDirectoryFullPath);
      setSaveDebugImages(_logDirectoryFullPath);
    }

    // Remove results from prior execution if any
    _localResultList.clear();

    // Save the state of xrays before requesting images.
    _wereXraysOn = AbstractXraySource.getInstance().areXraysOn();
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
  }

  /**
   * @author John Dutton
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
    logResults();

    if (_wereXraysOn)
    {
      AbstractXraySource.getInstance().on();
    }
    else
    {
      AbstractXraySource.getInstance().off();
    }
    
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }

  /**
   * Where each subclass does it unique image processing.
   *
   * @author John Dutton
   */
  public abstract void doTheRealConfirmationWork() throws XrayTesterException;


  /**
   * From the superclass's perspective this method contains the task code.
   *
   * @author John Dutton
   */
  protected void executeTask() throws XrayTesterException
  {
	//Swee Yee Wong - Camera debugging purpose
    debug("ConfirmSharpness");
    doTheRealConfirmationWork();
  }

  /**
   * Used by regression test to check pass/fail status.
   *
   * @return true if all results pass compared to their limits
   * @author John Dutton
   */
  protected boolean didTestPass()
  {
    return _limits.didTestPass(_result);
  }

  /**
   * @author John Dutton
   */
  protected void setResultsToFail()
  {
    getResult().setResults(new Boolean(false));
  }

  /**
   * At this point, all confirmation can run on current hardware.
   *
   * @author John Dutton
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * Create the result object for this confirmation.
   *
   * @author John Dutton
   */
  public void createResultsObject()
  {
    // Initial result is pass (true).
    _result = new Result(new Boolean(true), super.getName());
  }

  /**
   *
   * @author John Dutton
   */
  public void createLimitsObject()
  {
    // Create dummy limit; the results is Boolean (pass/fail).
    _limits = new Limit(null, null, null, super.getName());
  }

  /**
   * Logs results about the confirmation.   The log information is formatted as comma
   * separated values so it can be imported into Excel for manual analysis.
   *
   * @author John Dutton
   */
  protected void logResults() throws DatastoreException
  {
    log(createLogString());
  }

  /**
   * @author John Dutton
   */
  private String createLogString()
  {
    String output = "";

    for (Object obj : _localResultList)
    {
      if (obj instanceof String)
      {
        output += String.format("%s,      ", obj);
      }
      else if (obj instanceof Double)
      {
        output += String.format("%.3f,      ", obj);
      }
      else
      {
        Assert.expect(false, "List contains unexpected object type");
      }
    }
    return output;
  }

  /**
   * Logs strings.
   *
   * @author John Dutton
   */
  private void log(String s) throws DatastoreException
  {
    if (_logger == null)
    {
      _logger = new FileLoggerAxi(_logDirectoryFullPath + File.separator +
                                  _logFileName + FileName.getLogFileExtension(),
                                  _maxLogFileSizeInBytes);
    }
    _logger.append(s);
  }

  /**
   * Used to name saved debug images to avoid collisions that would overwrite files.
   *
   * @author John Dutton
   */
  private String getCurrentTimeStamp()
  {
    return (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS")).format(Calendar.getInstance().getTime()) + "_";
  }

  /**
   * @author John Dutton
   */
  protected void saveIfSavingDebugImages(Image image, String nameToSaveUnder) throws DatastoreException

  {
    Assert.expect(image != null, "passing a null image for saving");
    if (saveDebugImages())
    {
      XrayImageIoUtil.savePngImage(image, _debugImagesDirectoryPath + File.separator +
                                   getCurrentTimeStamp() + nameToSaveUnder + ".png");
    }
  }

  /**
   * @author John Dutton
   */
  protected void saveImageOnConfirmationFailure(Image image, String nameToSaveUnder) throws DatastoreException

  {
    Assert.expect(image != null, "passing a null image for saving");
    XrayImageIoUtil.savePngImage(image, _logDirectoryFullPath + File.separator +
                                   getCurrentTimeStamp() + nameToSaveUnder + ".png");
  }


  /**
   * Saves reconstructed image as a png image for debugging and test
   * development purposes.
   *
   *
   * @author John Dutton
   */
  public void setSaveDebugImages(String debugFilesDirectoryPath) throws DatastoreException
  {
    Assert.expect(debugFilesDirectoryPath != null, "debugFilesDirectoryPath can't be null");
    _debugImagesDirectoryPath = debugFilesDirectoryPath;
    _saveDebugImages = true;

    // Handle case where dirName already exists.
    File file = new File(_debugImagesDirectoryPath);
    if (file.exists())
    {
      if (file.isDirectory() == false)
      {
        throw new FileFoundWhereDirectoryWasExpectedDatastoreException(_debugImagesDirectoryPath);
      }
      else
      {
        return;
      }
    }

    // dirName doesn't exist so create it.
    FileUtilAxi.mkdirs(_debugImagesDirectoryPath);
  }

   /**
    * Accessor method.
    *
    * @author John Dutton
    */
   boolean saveDebugImages()
   {
     return _saveDebugImages;
   }

  /**
   * Helper function for capturing lots of edge info.  Once design finalizes
   * as far as what edges and what result(s) to report, the function will
   * probably go away.
   * @author John Dutton
   */
  protected double calculateMTFAndAddResult(Image img,
                                            boolean isVertical,
                                            double lineSpreadFWHMPixelsLowLimit,
                                            double lineSpreadFWHMPixelsHighLimit,
                                            String edgeDescription)
      throws DatastoreException
  {
    Assert.expect(img != null, "image can't be null");

    double edgeAngle, edgeSpread, lineSpreadFWHM;
    try
    {
      EdgeMTF mtf = new EdgeMTF(img, isVertical);
      edgeAngle = mtf.getEdgeAngleInDegrees();
      edgeSpread = mtf.getEdgeSpreadRiseInPixels();
      lineSpreadFWHM = mtf.getLineSpreadFWHMInPixels();
      _edgeProfile = mtf.get_rawESF();
      _edgeProfileOversample = mtf.get_oversamplingFactor();
    }
    catch (EdgeMTFBusinessException ex)
    {
      // Something went wrong in the algorithm.  Most likely, something wasn't right with the image
      // that was analyzed.  Don't want to throw an exception about an algorithm error since
      // it wouldn't be meaningful to the user.  Instead, set the results so that the confirmation
      // fails.  Let the user see that.
      edgeAngle = -1.0;
      edgeSpread = -1.0;
      lineSpreadFWHM = -1.0;

      // Also, log the stack trace.
      StringBuffer sBuffer = new StringBuffer();
      StackTraceElement[] trace = ex.getStackTrace();
      for (int i = 0; i < trace.length; i++)
      {
        sBuffer.append(trace[i].toString());
      }
      log(sBuffer.toString());
    }

    if (!(lineSpreadFWHMPixelsLowLimit <= lineSpreadFWHM && lineSpreadFWHM <= lineSpreadFWHMPixelsHighLimit))
    {
      setResultsToFail();
    }

    _localResultList.add(edgeDescription);
    _localResultList.add(edgeAngle);
    _localResultList.add(edgeSpread);
    _localResultList.add(lineSpreadFWHM);

    return lineSpreadFWHM;
  }


  /**
   * Calculates the mean of the 8 horizontal edges as an overall, single
   * measure of sharpness.
   *
   * The code is very dependent on the order in the log file.  If this were to change
   * often, it would make sense to use a different data structure than a list, perhaps a
   * map so individual values could be named and accessed directly.
   *
   * @author John Dutton
   */
  protected void calculateMeanAndAddResult(int startingEdge, int noOfEdges, String label)
  {
    Iterator iter = _localResultList.iterator();
    int edgeCount = 0;
    Double edgeTotalFWHM = 0.0;
    while (iter.hasNext() && edgeCount < startingEdge + noOfEdges)
    {
      Double newValue = 0.0;
      // Ignore first 3 values in a group
      iter.next();
      iter.next();
      iter.next();
      // Is this an edge we want?
      if (edgeCount >= startingEdge)
      {
        // Get FWHM
        Object obj = iter.next();
        if (!(obj instanceof Double))
        {
          Assert.expect(false, "Expect 4th value to be a Double!");
        }
        else
        {
          newValue = (Double)obj;
        }
        edgeTotalFWHM += newValue;
      }
      else
      {
        iter.next();
      }
      edgeCount++;
    }

    Double edgeFWHMMean = edgeTotalFWHM / noOfEdges;
    _localResultList.add(label);
    _localResultList.add(edgeFWHMMean);
  }

  /**
   * @author George A. David
   */
  void addResult(double result, String description)
  {
    Assert.expect(description != null);

    _localResultList.add(description);
    _localResultList.add(result);
  }
  
  /**
   * @author Chnee Khang Wah
   */
  void addMagnificationInfo(MagnificationEnum magnification)
  {
    String mag;
    
    if (magnification == MagnificationEnum.NOMINAL) //low mag
    {
      mag = Config.getInstance().getSystemCurrentLowMagnificationForLogging();
    }
    else if(magnification == MagnificationEnum.H_NOMINAL)//high mag
    {
      mag = Config.getInstance().getSystemCurrentHighMagnificationForLogging();
    }
    else // shoud not reach here
    {
      mag = "unknown";
    }
    
    _localResultList.add("magnification");
    _localResultList.add(mag);
  }

  /**
   * To enable debugging by reading in test images.
   * @author John Dutton
   */
  protected Image readImageFromFile (String dirPath, String fileName)
  {
    Image image = null;
      BufferedImage bufferedImg = null;
      try
      {
        bufferedImg = ImageIoUtil.loadPngBufferedImage(dirPath + File.separator + fileName);
      }
      catch (IOException ex)
      {
        Assert.expect(false, "Can't open test image");
      }

      image = Image.createFloatImageFromBufferedImage(bufferedImg);
      return image;
  }

// Removed in Thunderbolt
  /**
   * Returns list with 4 images references that need to be decremented when done with them.
   *
   * Assumes the 4 images each fit completely within one camera segment and will assert
   * if not.  This means that the input image better be a projection across the full camera
   * array width.
   *
   * @author John Dutton
   */
//  public List<Image> get4ImagesOfHorizontalEdge(
//      Image projectionImg,
//      ImageCoordinate upperLeftPixel, ImageCoordinate upperRightPixel,
//      int edgeWidthPixels,
//      int edgeHeightPixels)
//  {
    // Calculate the mid-point along the top edge of the quadrilateral
//    int oneHalfPixelX = (upperLeftPixel.getX() + upperRightPixel.getX()) / 2;
//    int oneHalfPixelY = (upperLeftPixel.getY() + upperRightPixel.getY()) / 2;

    // Crop top line into 4 images across the camera's 4 CCD segments.

    // Do left center segment first.
//    int oneQuarterPixelX = (upperLeftPixel.getX() + oneHalfPixelX) / 2;
//    int oneQuarterPixelY = (upperLeftPixel.getY() + oneHalfPixelY) / 2;
//    int threeEightPixelX = (oneQuarterPixelX + oneHalfPixelX) / 2;
//    int threeEightPixelY = (oneQuarterPixelY + oneHalfPixelY) / 2;
//    int pixelX = threeEightPixelX - (edgeWidthPixels / 2);
//    int pixelY = threeEightPixelY - (edgeHeightPixels / 2);
//    Assert.expect(_CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED <= pixelX &&
//                  pixelX < _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 2,
//                  "cropped horizontal edge not fitting within one camera segment");
//    RegionOfInterest roi =
//        new RegionOfInterest(pixelX, pixelY, edgeWidthPixels,
//                             edgeHeightPixels, 0, RegionShapeEnum.RECTANGULAR);
//    Image croppedImgLeftCenter = Image.createCopy(projectionImg, roi);

    // Do right center.
//    int threeQuarterPixelX = (upperRightPixel.getX() + oneHalfPixelX) / 2;
//    int threeQuarterPixelY = (upperRightPixel.getY() + oneHalfPixelY) / 2;
//    int fiveEightPixelX = (threeQuarterPixelX + oneHalfPixelX) / 2;
//    int fiveEightPixelY = (threeQuarterPixelY + oneHalfPixelY) / 2;
//    pixelX = fiveEightPixelX - (edgeWidthPixels / 2);
//    pixelY = fiveEightPixelY - (edgeHeightPixels / 2);
//    Assert.expect(_CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 2 <= pixelX &&
//                  pixelX < _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 3,
//                  "cropped horizontal edge not fitting within one camera segment");
//    roi = new RegionOfInterest(pixelX, pixelY, edgeWidthPixels,
//                               edgeHeightPixels, 0, RegionShapeEnum.RECTANGULAR);
//    Image croppedImgRightCenter = Image.createCopy(projectionImg, roi);

    // Do far left.
//    int oneEightPixelX = (upperLeftPixel.getX() + oneQuarterPixelX) / 2;
//    int oneEightPixelY = (upperLeftPixel.getY() + oneQuarterPixelY) / 2;
//    pixelX = oneEightPixelX - (edgeWidthPixels / 2);
//    pixelY = oneEightPixelY - (edgeHeightPixels / 2);
//    Assert.expect(0 <= pixelX &&
//                  pixelX<_CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
//                  "cropped horizontal edge not fitting within one camera segment");
//    roi = new RegionOfInterest(pixelX, pixelY, edgeWidthPixels,
//                               edgeHeightPixels, 0, RegionShapeEnum.RECTANGULAR);
//    Image croppedImgFarLeft = Image.createCopy(projectionImg, roi);

    // Do far right.
//    int sevenEightPixelX = (upperRightPixel.getX() + threeQuarterPixelX) / 2;
//    int sevenEightPixelY = (upperRightPixel.getY() + threeQuarterPixelY) / 2;
//    pixelX = sevenEightPixelX - (edgeWidthPixels / 2);
//    pixelY = sevenEightPixelY - (edgeHeightPixels / 2);
//    Assert.expect(_CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 3 <= pixelX &&
//                  pixelX < _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 4,
//                  "cropped horizontal edge not fitting within one camera segment");
//    roi = new RegionOfInterest(pixelX, pixelY, edgeWidthPixels,
//                               edgeHeightPixels, 0, RegionShapeEnum.RECTANGULAR);
//    Image croppedImgFarRight = Image.createCopy(projectionImg, roi);

    // list takes ownership of the references we have created to these images.
//    List<Image> list = new ArrayList<Image>();
//    list.add(croppedImgFarLeft);
//    list.add(croppedImgLeftCenter);
//    list.add(croppedImgRightCenter);
//    list.add(croppedImgFarRight);
//    return list;
//  }

  /**
   * Returns list with 2 or 3 images whose references need to be decremented when done with them.
   *
   * @author John Dutton
   */
  public List<Image> getImagesOfHorizontalEdge(
      Image img,
      int numberOfImages,
      ImageCoordinate upperLeftPixel,
      ImageCoordinate upperRightPixel,
      int edgeWidthPixels, int edgeHeightPixels) throws HardwareTaskExecutionException
  {
    Assert.expect(numberOfImages == 2 || numberOfImages == 3);

    // Calculate the thirds along the edge of the quadrilateral
    int lengthOfAThird = (upperRightPixel.getX() - upperLeftPixel.getX()) / 3;
    int oneThirdPixelX = upperLeftPixel.getX() + lengthOfAThird;
    int twoThirdPixelX = oneThirdPixelX + lengthOfAThird;
    int heightOfAThird = (upperRightPixel.getY() - upperLeftPixel.getY()) / 3;
    int oneThirdPixelY = upperLeftPixel.getY() + heightOfAThird;
    int twoThirdPixelY = oneThirdPixelY + heightOfAThird;


    // Crop top line into 3 images.

    // Do left side first.
    int centerPixelX = (upperLeftPixel.getX() + oneThirdPixelX) / 2;
    int centerPixelY = (upperLeftPixel.getY() + oneThirdPixelY) / 2;
    int pixelX = centerPixelX - (edgeWidthPixels / 2);
    int pixelY = centerPixelY - (edgeHeightPixels / 2);
    Assert.expect(0 <= pixelX && pixelX + edgeWidthPixels <= img.getWidth(),
                  "Edge width is too great for creating a horizontal edge (or top and bottom points are too close to image left side or right side).");
    Assert.expect(0 <= pixelY && pixelY + edgeHeightPixels <= img.getHeight(),
                  "Edge height is too great for creating a horizontal edge (or top and bottom points are too close to image top side or bottom side).");
    RegionOfInterest roi =
        new RegionOfInterest(pixelX, pixelY, edgeWidthPixels,
                             edgeHeightPixels, 0, RegionShapeEnum.RECTANGULAR);
    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(roi, img);
    Image croppedImgLeft = Image.createCopy(img, roi);
    List<Image> list = new ArrayList<Image>();
    list.add(croppedImgLeft);

    // Do center.
    centerPixelX = (oneThirdPixelX + twoThirdPixelX) / 2;
    centerPixelY = (oneThirdPixelY + twoThirdPixelY) / 2;
    pixelX = centerPixelX - (edgeWidthPixels / 2);
    pixelY = centerPixelY - (edgeHeightPixels / 2);
    Assert.expect(0 <= pixelX && pixelX + edgeWidthPixels <= img.getWidth(),
                  "Edge width is too great for creating a horizontal edge (or top and bottom points are too close to image left side or right side).");
    Assert.expect(0 <= pixelY && pixelY + edgeHeightPixels <= img.getHeight(),
                  "Edge height is too great for creating a horizontal edge (or top and bottom points are too close to image top side or bottom side).");
    roi = new RegionOfInterest(pixelX, pixelY, edgeWidthPixels,
                               edgeHeightPixels, 0, RegionShapeEnum.RECTANGULAR);
    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(roi, img);
    Image croppedImgCenter = Image.createCopy(img, roi);
    list.add(croppedImgCenter);

    // Do right.
    if (numberOfImages == 3)
    {
      centerPixelX = (twoThirdPixelX + upperRightPixel.getX()) / 2;
      centerPixelY = (twoThirdPixelY + upperRightPixel.getY()) / 2;
      pixelX = centerPixelX - (edgeWidthPixels / 2);
      pixelY = centerPixelY - (edgeHeightPixels / 2);
      Assert.expect(0 <= pixelX && pixelX + edgeWidthPixels <= img.getWidth(),
                    "Edge width is too great for creating a horizontal edge (or top and bottom points are too close to image left side or right side).");
      Assert.expect(0 <= pixelY && pixelY + edgeHeightPixels <= img.getHeight(),
                    "Edge height is too great for creating a horizontal edge (or top and bottom points are too close to image top side or bottom side).");
      roi = new RegionOfInterest(pixelX, pixelY, edgeWidthPixels,
                                 edgeHeightPixels, 0, RegionShapeEnum.RECTANGULAR);
      //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
      checkIsRoiFitsWithinImage(roi, img);
      Image croppedImgRight = Image.createCopy(img, roi);
      list.add(croppedImgRight);
    }

    return list;
  }


  /**
   * Returns image reference that needs to be decremented when done with it.
   *
   * @author John Dutton
   */
  public Image getImageOfVerticalEdge(Image img, ImageCoordinate upperPixel,
                                      ImageCoordinate lowerPixel,
                                      int edgeWidth,
                                      int edgeHeight) throws HardwareTaskExecutionException
  {
    int midPixelX = (upperPixel.getX() + lowerPixel.getX()) / 2;
    int midPixelY = (upperPixel.getY() + lowerPixel.getY()) / 2;
    int pixelX = midPixelX - (edgeWidth / 2);
    int pixelY = midPixelY - (edgeHeight / 2);
    Assert.expect(0 <= pixelX && pixelX + edgeWidth <= img.getWidth(),
                  "Edge width is too great for creating a vertical edge (or top and bottom points are too close to image left side or right side).");
    Assert.expect(0 <= pixelY && pixelY + edgeHeight <= img.getHeight(),
                  "Edge height is too great for creating a vertical edge (or top and bottom points are too close to image top side or bottom side).");
    RegionOfInterest roi = new RegionOfInterest(pixelX, pixelY, edgeWidth,
                                                edgeHeight, 0, RegionShapeEnum.RECTANGULAR);
    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(roi, img);
    return (Image.createCopy(img, roi));
  }

  /**
   * Returns image reference that needs to be decremented when done with it.
   *
   * @author John Dutton
   */
  public Image getImageOfVerticalEdgeUpToRightSideOfImage(Image img, ImageCoordinate upperPixel,
                                                          ImageCoordinate lowerPixel,
                                                          int edgeWidth,
                                                          int edgeHeight) throws DatastoreException, HardwareTaskExecutionException
  {
    int midPixelX = (upperPixel.getX() + lowerPixel.getX()) / 2;
    int midPixelY = (upperPixel.getY() + lowerPixel.getY()) / 2;
    int pixelX = midPixelX - (edgeWidth / 2);
    int pixelY = midPixelY - (edgeHeight / 2);
    Assert.expect(0 <= pixelX && pixelX + edgeWidth <= img.getWidth(),
                  "Edge width is too great for creating a vertical edge (or top and bottom points are too close to image left side or right side).");
    Assert.expect(0 <= pixelY && pixelY + edgeHeight <= img.getHeight(),
                  "Edge height is too great for creating a vertical edge (or top and bottom points are too close to image top side or bottom side).");
    int adjustedEdgeWidth = img.getWidth() - pixelX;
    RegionOfInterest roi = new RegionOfInterest(pixelX, pixelY, adjustedEdgeWidth,
                                                edgeHeight, 0, RegionShapeEnum.RECTANGULAR);
    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(roi, img);
    Image verticalImage = Image.createCopy(img, roi);
    saveIfSavingDebugImages(verticalImage, "rightVerticalImage");

    return verticalImage;
}


  /**
   * @author John Dutton
   */
  public List<ImageCoordinate> getQuadImageCoordinates(List<IntCoordinate> listOfPoints,
      int farthestLeftVisibleXInNanometers,
      int lowestVisibleYInNanometers,
      int imageHeight,
      int xOffset,
      int yOffset) throws DatastoreException
  {
    List<ImageCoordinate> listOfImageCoordinates = new ArrayList<ImageCoordinate>();
    int pixelSizeXInNanometers = actualPixelSizeXInNanometers();
    int pixelSizeYInNanometers = actualPixelSizeYInNanometers();

    int distanceFromImageLeftSideToQuadUpperLeftX =
        Math.abs(listOfPoints.get(0).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadUpperLeftX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadUpperLeftX,
                                                    pixelSizeXInNanometers);
    int upperLeftPixelX = distanceInPixelsFromImageLeftSideToQuadUpperLeftX + xOffset;

    int distanceFromImageBottomToQuadUpperLeftY =
        Math.abs(listOfPoints.get(0).getY() - lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadUpperLeftY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadUpperLeftY,
                                                    pixelSizeYInNanometers);
    int upperLeftPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadUpperLeftY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(upperLeftPixelX, upperLeftPixelY));

    int distanceFromImageLeftSideToQuadUpperRightX =
        Math.abs(listOfPoints.get(1).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadUpperRightX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadUpperRightX,
                                                    pixelSizeXInNanometers);
    int upperRightPixelX = distanceInPixelsFromImageLeftSideToQuadUpperRightX + xOffset;

    int distanceFromImageBottomToQuadUpperRightY =
        Math.abs(listOfPoints.get(1).getY() -
                 lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadUpperRightY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadUpperRightY,
                                                    pixelSizeYInNanometers);
    int upperRightPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadUpperRightY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(upperRightPixelX, upperRightPixelY));

    int distanceFromImageLeftSideToQuadLowerRightX =
        Math.abs(listOfPoints.get(2).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadLowerRightX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadLowerRightX,
                                                    pixelSizeXInNanometers);
    int lowerRightPixelX = distanceInPixelsFromImageLeftSideToQuadLowerRightX + xOffset;

    int distanceFromImageBottomToQuadLowerRightY =
        Math.abs(listOfPoints.get(2).getY() - lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadLowerRightY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadLowerRightY,
                                                    pixelSizeYInNanometers);
    int lowerRightPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadLowerRightY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(lowerRightPixelX, lowerRightPixelY));

    int distanceFromImageLeftSideToQuadLowerLeftX =
        Math.abs(listOfPoints.get(3).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadLowerLeftX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadLowerLeftX,
                                                    pixelSizeXInNanometers);
    int lowerLeftPixelX = distanceInPixelsFromImageLeftSideToQuadLowerLeftX + xOffset;

    int distanceFromImageBottomToQuadLowerLeftY =
        Math.abs(listOfPoints.get(3).getY() - lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadLowerLeftY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadLowerLeftY,
                                                    pixelSizeYInNanometers);
    int lowerLeftPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadLowerLeftY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(lowerLeftPixelX, lowerLeftPixelY));

    return listOfImageCoordinates;
  }

  /**
   * @author Wei Chin
   */
  public List<ImageCoordinate> getQuadImageCoordinatesInHighMag(List<IntCoordinate> listOfPoints,
      int farthestLeftVisibleXInNanometers,
      int lowestVisibleYInNanometers,
      int imageHeight,
      int xOffset,
      int yOffset) throws DatastoreException
  {
    List<ImageCoordinate> listOfImageCoordinates = new ArrayList<ImageCoordinate>();
    int pixelSizeXInNanometers = actualPixelSizeXInNanometersInHighMag();
    int pixelSizeYInNanometers = actualPixelSizeYInNanometersInHighMag();

    int distanceFromImageLeftSideToQuadUpperLeftX =
        Math.abs(listOfPoints.get(0).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadUpperLeftX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadUpperLeftX,
                                                    pixelSizeXInNanometers);
    int upperLeftPixelX = distanceInPixelsFromImageLeftSideToQuadUpperLeftX + xOffset;

    int distanceFromImageBottomToQuadUpperLeftY =
        Math.abs(listOfPoints.get(0).getY() - lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadUpperLeftY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadUpperLeftY,
                                                    pixelSizeYInNanometers);
    int upperLeftPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadUpperLeftY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(upperLeftPixelX, upperLeftPixelY));

    int distanceFromImageLeftSideToQuadUpperRightX =
        Math.abs(listOfPoints.get(1).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadUpperRightX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadUpperRightX,
                                                    pixelSizeXInNanometers);
    int upperRightPixelX = distanceInPixelsFromImageLeftSideToQuadUpperRightX + xOffset;

    int distanceFromImageBottomToQuadUpperRightY =
        Math.abs(listOfPoints.get(1).getY() -
                 lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadUpperRightY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadUpperRightY,
                                                    pixelSizeYInNanometers);
    int upperRightPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadUpperRightY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(upperRightPixelX, upperRightPixelY));

    int distanceFromImageLeftSideToQuadLowerRightX =
        Math.abs(listOfPoints.get(2).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadLowerRightX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadLowerRightX,
                                                    pixelSizeXInNanometers);
    int lowerRightPixelX = distanceInPixelsFromImageLeftSideToQuadLowerRightX + xOffset;

    int distanceFromImageBottomToQuadLowerRightY =
        Math.abs(listOfPoints.get(2).getY() - lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadLowerRightY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadLowerRightY,
                                                    pixelSizeYInNanometers);
    int lowerRightPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadLowerRightY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(lowerRightPixelX, lowerRightPixelY));

    int distanceFromImageLeftSideToQuadLowerLeftX =
        Math.abs(listOfPoints.get(3).getX() - farthestLeftVisibleXInNanometers);
    int distanceInPixelsFromImageLeftSideToQuadLowerLeftX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageLeftSideToQuadLowerLeftX,
                                                    pixelSizeXInNanometers);
    int lowerLeftPixelX = distanceInPixelsFromImageLeftSideToQuadLowerLeftX + xOffset;

    int distanceFromImageBottomToQuadLowerLeftY =
        Math.abs(listOfPoints.get(3).getY() - lowestVisibleYInNanometers);
    int distanceInPixelsFromImageBottomToQuadLowerLeftY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(distanceFromImageBottomToQuadLowerLeftY,
                                                    pixelSizeYInNanometers);
    int lowerLeftPixelY = imageHeight - distanceInPixelsFromImageBottomToQuadLowerLeftY + yOffset;

    listOfImageCoordinates.add(new ImageCoordinate(lowerLeftPixelX, lowerLeftPixelY));

    return listOfImageCoordinates;
  }
// Removed in Thunderbolt
  /**
   * @author John Dutton
   */
//  public List<ImageCoordinate> getQuadImageCoordinates(List<IntCoordinate> listOfPoints,
//      int farthestRightVisibleXInNanometers,
//      int lowestVisibleYInNanometers,
//      int imageHeight) throws DatastoreException
//  {
//    return getQuadImageCoordinates(listOfPoints, farthestRightVisibleXInNanometers,
//                                   lowestVisibleYInNanometers,
//                                   imageHeight, 0, 0);
//  }

  /**
   * Returns pixel size width at the reference plane which depends on reference plan magnification
   * and physical pixel size width.
   *
   * @author John Dutton
   */
  public int actualPixelSizeXInNanometers() throws DatastoreException
  {
    Config config = Config.getInstance();
    config.loadIfNecessary();

    // Just create any camera so we can get sensor properties.
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    double actualMagnification = config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);

    int rv = (int)Math.round(xRayCamera.getSensorColumnPitchInNanometers() / actualMagnification);
    double altrv = MechanicalConversions.getStepDirectionPixelSizeInNanometersAboveNominalPlane(0.0);
    return (int)altrv;
  }
  
  /**
   * Returns pixel size width at the reference plane which depends on reference plan magnification
   * and physical pixel size width.
   *
   * @author Wei Chin
   */
  public int actualPixelSizeXInNanometersInHighMag() throws DatastoreException
  {
    Config config = Config.getInstance();
    config.loadIfNecessary();

    // Just create any camera so we can get sensor properties.
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    double actualMagnification = config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);

    int rv = (int)Math.round(xRayCamera.getSensorColumnPitchInNanometers() / actualMagnification);
    double altrv = MechanicalConversions.getStepDirectionPixelSizeInNanometersAboveNominalPlane(0.0);
    return (int)altrv;
  }

  /**
   * Returns pixel size height at reference plane which is fixed at nominal by the stage's
   * camera trigger rate at 16000 nm.
   *
   * @author John Dutton
   */
  public int actualPixelSizeYInNanometers() throws DatastoreException
  {
    Config config = Config.getInstance();
    config.loadIfNecessary();

    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();

    int rv = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    double altrv = MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel();
    return (int)altrv;
  }

  /**
   * Returns pixel size height at reference plane which is fixed at nominal by the stage's
   * camera trigger rate at 10000 nm.
   *
   * @author Wei Chin
   */
  public int actualPixelSizeYInNanometersInHighMag() throws DatastoreException
  {
    Config config = Config.getInstance();
    config.loadIfNecessary();

    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();

    int rv = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    double altrv = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
    return (int)altrv;
  }


  /**
   * @author John Dutton
   */
  boolean didConfirmationPass()
  {
    return getLimit().didTestPass(getResult());
  }

  /**
   * @author John Dutton
   */
  public void increaseAndReportProgressPercentage(int n)
  {
    _progressPercentage += n;
    Assert.expect(_progressPercentage <= 100, "progress dialog has gone past 100%");
    reportProgress(_progressPercentage);
  }

  /**
   * @author John Dutton
   */
  public void initializeAndReportProgressPercentage()
  {
    _progressPercentage = 0;
    reportProgress(_progressPercentage);
  }

  /**
   * @author Reid Hayhow
   */
  public void setProgressPercentageDoneAndReport(int newPercentageDone)
  {
    Assert.expect(newPercentageDone >= 0);
    Assert.expect(newPercentageDone <= 100);

    _progressPercentage = newPercentageDone;
    reportProgress(_progressPercentage);
  }

  /**
   * @author John Dutton
   */
  public void reportProgressPercentageDone()
  {
    reportProgress(100);
  }

  /**
   * Set reading image from file.  This is useful for debugging purposes when you have an image and
   * want to see how the code behaves with it.
   *
   * @author John Dutton
   */
  public void setReadImageFromFile(boolean value)
  {
    _readImageFromFile = value;
  }

  /**
   * @author John Dutton
   */
  public boolean readImageFromFileTrue()
  {
    return _readImageFromFile;
  }

  /**
   * For debugging purposes, textually prints pixel values in table format.
   * @author John Dutton
   */
  private void printImage(Image img)
  {
    Assert.expect(img != null, "image can't be null");
    for (int y = 0; y < 1; y++)
    {
      System.out.println();
      System.out.print("y = " + y + " ");

      for (int x = 0; x < img.getWidth(); x++)
      {
        float pixelValue = img.getPixelValue(x, y);
        System.out.print(pixelValue + " ");
      }
    }
  }


  public double[] get_edgeProfile()
  {
    return _edgeProfile;
  }

  
  /**
   * @author Anthony Fong - 
   *  Do not override this function in the derive class, 
   *   let this class (ConfirmSharpness) to take care of this validation
   */     
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void checkIsRoiFitsWithinImage(RegionOfInterest roi, Image image) throws HardwareTaskExecutionException
  {
    Assert.expect(roi != null);
    Assert.expect(image != null);
    
    if (roi.fitsWithinImage(image) == false)
    {
      // ROI cannot fit the images, so can't continue.   Fail confirmation and throw exception.
      setResultsToFail();
      throw
          new HardwareTaskExecutionException(
              ConfirmationExceptionEnum.CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH);
    }
  }
}
