package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.license.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import ij.ImagePlus;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;

/**
 *
 * @author Reid Hayhow
 */
public class ConfirmAreaModeImages extends EventDrivenHardwareTask
{
  // This is necessary because pixels on the right side get averaged with the white
  // pixels on the far right by the median filter
  private final int _NUMBER_OF_RIGHT_FILL_PIXELS = 1;

  // Max gray level allowed on the Genesis system
  private final double _MAX_GRAY_LEVEL = 255;

  // This is the number of pixels to search over
  private final int _SEARCH_WINDOW_SIZE = 8;

  // Number of bins or buckets to sort gray values into
  private final int _NUMBER_OF_HISTOGRAM_BINS = 32;

  // Progress is broken into two parts, 50% for light image and 50% for dark
  private final double _FIFTY_PERCENT_DONE = 0.5;

  private static String _name =
      StringLocalizer.keyToString("CD_CONFIRM_AREA_MODE_IMAGES_NAME_KEY");

  private static Config _config = Config.getInstance();

  static private int imageIndex;
  static private FileLoggerAxi _fileLogger;
  static private int _maxLogFileSizeInBytes =
      _config.getIntValue(SoftwareConfigEnum.XRAY_SOURCE_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES);

  //This task should always run
  private static int _frequency = _config.getIntValue(HardwareCalibEnum.AREA_MODE_IMAGE_CONFIRM_FREQUENCY);

  //There is no timeout for this task, it is manual
  private static long _timeoutInSeconds = _config.getIntValue(SoftwareConfigEnum.AREA_MODE_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS);

  //There should only be one logfile
  private int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.AREA_MODE_IMAGE_CONFIRM_MAX_NUMBER_OF_LOG_FILES);

  //Directory Logger member
  private DirectoryLoggerAxi _logger;

  //Motion profile to use when moving stage, use the fastest
  private PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;

  //Log directory where image files are saved
  private String _logDirectory = Directory.getConfirmAreaModeImageLogDir();

  //The list of cameras to capture snapshot images from
  private static List<AbstractXrayCamera> _cameras;

  //Parallel Thread Task executor to handle saving failed images in parallel
  private ExecuteParallelThreadTasks<Boolean> _executor = new ExecuteParallelThreadTasks<Boolean>();

  // The set of result object used to store results
  private Set<AreaModeImageResults> _setOfResults = new TreeSet<AreaModeImageResults>();

  // A set of failing cameras to use for logging
  private Set<AbstractXrayCamera> _setOfFailingCameras = new TreeSet<AbstractXrayCamera>();

  //Members for saving and restoring the motion profile of the panel positioner
  private PanelPositioner _panelPositioner = PanelPositioner.getInstance();
  private MotionProfile _panelPositionerSavedProfile;

  //Xray source instance for all sub-classes to use
  private AbstractXraySource _xraySource = AbstractXraySource.getInstance();

  // Grayscale instance to set cal to run ASAP
  private CalGrayscale _calGrayscale = CalGrayscale.getInstance();

  //Instance member
  private static ConfirmAreaModeImages _instance;

  // Members to store limit values
  private Integer _lightMeanPixelMaxValue;
  private Integer _lightMeanPixelMinValue;
  private Double _lightStandardDevMaxValue;
  private Double _lightStandardDevMinValue;
  private Double _lightVerticalProfileMinGrayValue;
  private Double _lightHorizontalProfileMinGrayValue;
  private Double _subtractedVerticalProfileMaxResponsivenessValue;
  private Double _subtractedVerticalMinResponsivenessValue;
  private Double _subtractedHorizontalProfileMaxResponsivenessValue;
  private Double _subtractedHorizontalProfileMinResponsivenessValue;

  private Double _edgeGradientMaxValue;  
  private Double _edgeGradientMinValue;
  private Double _edgeStandardDevMaxValue;
  private Double _edgeStandardDevMinValue;
  private Double _edgeQualityGradeMaxValue;
  private Double _edgeQualityGradeMinValue;
  private Double _betaAreaMaxValue;
  private Double _betaAreaMinValue;



  // Place to store the X/Y position of the stage before we move it. This allows
  // us to restore it to the right position
  private HardwareCalibEnum _xPosition;
  private HardwareCalibEnum _yPosition;

  // Create a new list of futures to track each task
  private List<Future<Boolean>> _listOfTasks;

  // Reuse the regions of interest
  private RegionOfInterest _verticalRegionOfInterest;
  private RegionOfInterest _horizontalRegionOfInterest;

  // Image dimension variables
  private int _imageHeightInPixels;
  private int _numberOfHorizontalPixels;
  private int _numberOfSegmentPixelsToExamine;

  // Use the following for offline testing of the Area Mode Image confirmation
  private boolean _useOfflineImages = false;
  //private String _lightImageLocationString = "c:\\temp\\areaModeTest\\camera_";
  private String _lightImageLocationString = "D:\\AreaMode\\AreaMode\\V810MC2\\5.0\\camera_";
  private String _lightImageLocationExtension = "_light.png";
  
  //private String _darkImageLocationString = "c:\\temp\\areaModeTest\\camera_";
  private String _darkImageLocationString = "D:\\AreaMode\\AreaMode\\V810MC2\\5.0\\camera_";
  private String _darkImageLocationExtension = "_dark.png";

  //private Image areaModeImage = null;
  //private Image areaModeImageCam0 = null;
  //private Image areaModeImageCurrent = null;
  private Image sobelImg = null;

  //XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
  private static boolean  _enableCameraMonitoringFeature = false;
  private static boolean  _enableCameraMonitoringTrigger = false;

  private static boolean  _disableXrayCamera0QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera1QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera2QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera3QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera4QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera5QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera6QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera7QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera8QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera9QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera10QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera11QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera12QualityMonitoringTrigger = true;
  private static boolean  _disableXrayCamera13QualityMonitoringTrigger = true;
  private static boolean[]  _disableXrayCamerasQualityMonitoringTrigger = new boolean[14];

  private static boolean  _enableXrayCameraQualityMonitoringGradientMean = false;
  private static boolean  _enableXrayCameraQualityMonitoringGradientStdDev = false;
  private static boolean  _enableXrayCameraQualityMonitoringGradientAboveMean = false;
  private static boolean  _enableXrayCameraQualityMonitoringGradientMax = false;
  private static boolean  _enableXrayCameraQualityMonitoringGradientBeta = false;

  public static final int XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MEAN_INDEX = 2;
  public static final int XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_STD_DEV_INDEX = 4;
  public static final int XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN_INDEX = 5;
  public static final int XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MAX_INDEX = 6;
  public static final int XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_BETA_INDEX = 7;
  private static boolean _isDeveloperSystemEnabled = false;
  
  /**
   * Class to encapsulate a confirmation that captures snap shot or area mode
   * images and performs analysis on them.
   *
   * @author Reid Hayhow
   */
  private ConfirmAreaModeImages()
  {
    super(_name, _frequency, _timeoutInSeconds, ProgressReporterEnum.CONFIRM_AREA_MODE_IMAGE);

    //Create the directory logger and make it use the CSV extension for simplicity
    _logger = new DirectoryLoggerAxi(Directory.getConfirmAreaModeImageLogDir(),
                                     FileName.getConfirmAreaModeImageLogFileName(),
                                     FileName.getCommaSeparatedValueFileExtension(),
                                     _maxLogFilesInDirectory);

    //This confirmation is automated
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    //Find the correct stage position for this confirm to use from hardware.calib
    _xPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS;
    _yPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS;

    //Add each camera to our internal list
    _cameras = XrayCameraArray.getCameras();

    //Set the timestamp enum for later use
    setTimestampEnum(HardwareCalibEnum.CONFIRM_AREA_MODE_IMAGE_LAST_TIME_RUN);

    // Create the two ROI's for all image sets
    // We can just get any camera so we can get the sensor attributes
    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);

    // We are interested in the usable image, so specify the region of interest that
    // is the same size as the usable image
	// Swee Yee Wong - XCR-3125 Support new camera upgrade firmware function
    _imageHeightInPixels = xRayCamera.getNumberOfSensorRowsWithOverHeadByte();
    _numberOfHorizontalPixels = xRayCamera.getNumberOfSensorPixelsUsed();

    // The final number to examine excludes the right fill pixels
    _numberOfSegmentPixelsToExamine = (_numberOfHorizontalPixels - _NUMBER_OF_RIGHT_FILL_PIXELS);

    // Now create the regions of interest that will be used for analyzing the area mode images
    _verticalRegionOfInterest = new RegionOfInterest(0,
                                                     0,
                                                     _numberOfSegmentPixelsToExamine,
                                                     _imageHeightInPixels,
                                                     0,
                                                     RegionShapeEnum.RECTANGULAR);

    // Now, to get the vertical array of data, we have to specify a rotated region of interest
    // specifying 90 degrees in the fifth parameter
    _horizontalRegionOfInterest = new RegionOfInterest(0,
                                                       0,
                                                       _numberOfSegmentPixelsToExamine,
                                                       _imageHeightInPixels,
                                                       90,
                                                       RegionShapeEnum.RECTANGULAR);
    _enableCameraMonitoringFeature = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_FEATURE);
    _enableCameraMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_TRIGGER);

    _disableXrayCamera0QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA0_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera1QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA1_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera2QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA2_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera3QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA3_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera4QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA4_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera5QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA5_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera6QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA6_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera7QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA7_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera8QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA8_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera9QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA9_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera10QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA10_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera11QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA11_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera12QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA12_QUALITY_MONITORING_TRIGGER);
    _disableXrayCamera13QualityMonitoringTrigger = _config.getBooleanValue(SoftwareConfigEnum.DISABLE_XRAY_CAMERA13_QUALITY_MONITORING_TRIGGER);

    _disableXrayCamerasQualityMonitoringTrigger[0] = _disableXrayCamera0QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[1] = _disableXrayCamera1QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[2] = _disableXrayCamera2QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[3] = _disableXrayCamera3QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[4] = _disableXrayCamera4QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[5] = _disableXrayCamera5QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[6] = _disableXrayCamera6QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[7] = _disableXrayCamera7QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[8] = _disableXrayCamera8QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[9] = _disableXrayCamera9QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[10] = _disableXrayCamera10QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[11] = _disableXrayCamera11QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[12] = _disableXrayCamera12QualityMonitoringTrigger;
    _disableXrayCamerasQualityMonitoringTrigger[13] = _disableXrayCamera13QualityMonitoringTrigger;

    _enableXrayCameraQualityMonitoringGradientMean = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MEAN);
    _enableXrayCameraQualityMonitoringGradientStdDev = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_STDDEV);
    _enableXrayCameraQualityMonitoringGradientAboveMean = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN);
    _enableXrayCameraQualityMonitoringGradientMax = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MAX);
    _enableXrayCameraQualityMonitoringGradientBeta = _config.getBooleanValue(SoftwareConfigEnum.ENABLE_XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_BETA);

    if (_enableCameraMonitoringFeature == true)
    {
      try
      {
        if (_fileLogger == null)
        {
          createCameraMonitoringLogFileAndHeader();
        }
        _isDeveloperSystemEnabled = _config.isDeveloperSystemEnabled() && LicenseManager.isDeveloperSystemEnabled();
      }
      catch (DatastoreException ex)
      {
        // do nothing
      }
      catch (BusinessException ex)
      {
        // do nothing
      }
    }
  }

  /**
   * Instance access to this Confirmation
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmAreaModeImages getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmAreaModeImages();
    }
    return _instance;
  }

  /**
   * This Confirmation can only run if the camera firmware allows area mode
   * images (AKA snap shot images).
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * This Confirmation is a simple run/didn't run task. But we do care about the
   * limits so we store them off internally to use later.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject() throws InvalidValueDatastoreException
  {
    //Boolean limits are all null
    _limits = new Limit(null, null, null, null);


    // Start with the limits for the light image, MIN, MAX and Standard Dev and profile MINs
    _lightMeanPixelMaxValue = _parsedLimits.getHighLimitInteger("lightMeanPixel");
    _lightMeanPixelMinValue = _parsedLimits.getLowLimitInteger("lightMeanPixel");

    _lightStandardDevMaxValue = _parsedLimits.getHighLimitDouble("lightStandardDeviation");
    _lightStandardDevMinValue = _parsedLimits.getLowLimitDouble("lightStandardDeviation");

    _lightHorizontalProfileMinGrayValue = _parsedLimits.getLowLimitDouble("lightHorizontalProfileMinGray");
    _lightVerticalProfileMinGrayValue = _parsedLimits.getLowLimitDouble("lightVerticalProfileMinGray");

    // Now get the responsiveness values (light image - dark image) for the profiles
    _subtractedVerticalProfileMaxResponsivenessValue = _parsedLimits.getHighLimitDouble("subtractedVerticalProfileResponsiveness");
    _subtractedVerticalMinResponsivenessValue = _parsedLimits.getLowLimitDouble("subtractedVerticalProfileResponsiveness");

    _subtractedHorizontalProfileMaxResponsivenessValue = _parsedLimits.getHighLimitDouble("subtractedHorizontalProfileResponsiveness");
    _subtractedHorizontalProfileMinResponsivenessValue = _parsedLimits.getLowLimitDouble("subtractedHorizontalProfileResponsiveness");

    _edgeGradientMaxValue = _parsedLimits.getHighLimitDouble("edgeGradient");
    _edgeGradientMinValue = _parsedLimits.getLowLimitDouble("edgeGradient");
    _edgeStandardDevMaxValue = _parsedLimits.getHighLimitDouble("edgeStandardDeviation");
    _edgeStandardDevMinValue = _parsedLimits.getLowLimitDouble("edgeStandardDeviation");
    _edgeQualityGradeMaxValue = _parsedLimits.getHighLimitDouble("edgeQualityGrade");
    _edgeQualityGradeMinValue = _parsedLimits.getLowLimitDouble("edgeQualityGrade");
    _betaAreaMaxValue= _parsedLimits.getHighLimitDouble("betaArea");
    _betaAreaMinValue= _parsedLimits.getLowLimitDouble("betaArea");

  }

  /**
   * This Confirmation uses a simple boolean pass/fail result. All the limit
   * checking is done internally and is used to set the boolean instead of passing
   * it up to the super class.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    // Assume the task passes to begin with. This means we have to set it to fail
    // if we encounter a problem.
    _result = new Result(new Boolean(true), _name);
  }

  /**
   * The core of this confirmation. Capture area mode images from all cameras
   * and analyze them.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
	//Swee Yee Wong - Camera debugging purpose
    debug("ConfirmAreaModeImages");
    // Make sure that synthetic triggers are on.  It is good/expected to leave
    // them on because they bathe cameras keeping them thermally warm.
    CameraTrigger cameraTrigger = null;

    if (!(_useOfflineImages == true && XrayTester.getInstance().isSimulationModeOn()))
    {
        cameraTrigger =CameraTrigger.getInstance();
        cameraTrigger.on();
    }

    // Clear out the set of failing cameras and set of results before executing
    _setOfFailingCameras.clear();
    _setOfResults.clear();

    // Prepare the list of Future's for a new list
    _listOfTasks = new ArrayList<Future<Boolean>>();

    // Report that we are starting this task
    reportProgress(0);

    //Handle a cancel gracefully. This will cause the task to fail
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }

    if (!(_useOfflineImages == true && XrayTester.getInstance().isSimulationModeOn()))
    {
        // Move the stage to a clear area
        moveStageBeforeCapturingImages();
    }

    // Area mode images purge the cal table, so grayscale has to run before other
    // images can be retrieved
    _calGrayscale.setTaskToRunASAP();

    // Capture the images with X-rays ON and analyze them. A copy of each image is stored
    // in the results object for later analysis with the dark images
    captureAndAnalyzeLightImages();

    // Capture the dark images, analyze them by comparing them with the stored light
    // images.
    captureDarkImagesAndAnalyzeSubtractedLightImage();

    // Prepare the result message that may be displayed to the user
    populateResultMessage();

    // Log data about this test
    logReport();

    if (_isDeveloperSystemEnabled == true)
    {
        //Camera Quality Monitoring and Analysis
        cameraQualityMonitoring();
    }

    // Report that we are done with this task
    reportProgress(100);

    // Area mode images purge the cal table, so grayscale has to run before other
    // images can be retrieved
    _calGrayscale.setTaskToRunASAP();
  }

  /**
   * Camera Quality Monitoring.
   * XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
   * @author Anthony Fong
   */
  private void cameraQualityMonitoring() throws XrayTesterException
  {
    if (_enableCameraMonitoringFeature == true &&
        _enableCameraMonitoringTrigger== true)
    {
        // Value used to store and report our progress to the progress observable
        //int percentDone = 0;
        int numberOfCameras = XrayCameraArray.getNumberOfCameras();
        int camerasAnalyzed = 0;
        if (!(_useOfflineImages == true && XrayTester.getInstance().isSimulationModeOn()))
        {

        }
        // Capture an image from each camera
        for (AbstractXrayCamera camera : _cameras)
        {
            camera.getUserGain();
            int cameraID = camera.getId();
            int monitoringSelection = 5;
            boolean alarm = false;
            //Gradient Mean = 2
            //Gradient StdDev = 4;
            //Gradient AboveMean = 5;
            //Gradient Max = 6;
            //Gradient Beta = 7;


           if( _disableXrayCamerasQualityMonitoringTrigger[cameraID] == false )
           {
               if(_enableXrayCameraQualityMonitoringGradientMean == true)
               {
                    monitoringSelection = XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MEAN_INDEX;
                    boolean alarmResult = new XrayCameraQualityMonitoring().generateXBarChart(String.valueOf(cameraID *10+monitoringSelection),true,false);
                    alarm = alarm || alarmResult;
               }
                              
               if(_enableXrayCameraQualityMonitoringGradientStdDev == true)
               {
                    monitoringSelection = XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_STD_DEV_INDEX;
                    boolean alarmResult = new XrayCameraQualityMonitoring().generateXBarChart(String.valueOf(cameraID *10+monitoringSelection),true,false);
                    alarm = alarm || alarmResult;
               }
                              
               if(_enableXrayCameraQualityMonitoringGradientAboveMean == true)
               {
                    monitoringSelection = XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN_INDEX;
                    boolean alarmResult = new XrayCameraQualityMonitoring().generateXBarChart(String.valueOf(cameraID *10+monitoringSelection),true,false);
                    alarm = alarm || alarmResult;
               }
                              
               if(_enableXrayCameraQualityMonitoringGradientMax == true)
               {
                    monitoringSelection = XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MAX_INDEX;
                    boolean alarmResult = new XrayCameraQualityMonitoring().generateXBarChart(String.valueOf(cameraID *10+monitoringSelection),true,false);
                    alarm = alarm || alarmResult;
               }
                              
               if(_enableXrayCameraQualityMonitoringGradientBeta == true)
               {
                    monitoringSelection = XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_BETA_INDEX;
                    boolean alarmResult = new XrayCameraQualityMonitoring().generateXBarChart(String.valueOf(cameraID *10+monitoringSelection),true,false);
                    alarm = alarm || alarmResult;
               }
           }

            //if(alarm==true) break;
            // Calculate how far along we are now and update the progress observable with our progress
            //percentDone = (int)((100 * ((double)camerasAnalyzed / numberOfCameras) * _FIFTY_PERCENT_DONE));
            //reportProgress(percentDone);
        } //end of for(AbstractXrayCamera camera...)
     }
  }



  /**
   *
   * This method wraps around the end of the image back to the front for the last few pixels
   *
   * @todo Does wrapping create inequality between the two ends of the image?
   *
   * @author Reid Hayhow
   */
  private boolean isProfileAverageBelowValueAcrossWindow(float[] profileToAnalyze,
                                                         int lowestAcceptableValue,
                                                         int windowSize,
                                                         DoubleRef lowestValueFound)
  {
    for (int currentPixel = 0; currentPixel < profileToAnalyze.length; currentPixel++)
    {
      // Determine the ending search index by adding the window size to to the current pixel
      // we want to analyze and then modulo it by the length of the profile
      int endSearchIndex = ((currentPixel + windowSize) % (profileToAnalyze.length));
      float sumOfPixels = 0;

      // if we are able to fit the search range into the array without wraping, do it!
      if (endSearchIndex >= currentPixel)
      {
        sumOfPixels = ArrayUtil.sum(profileToAnalyze, currentPixel, endSearchIndex);
      }
      // Need to sum the remaing pixels and wrap around to the front end
      else
      {
        sumOfPixels = ArrayUtil.sum(profileToAnalyze, currentPixel, (profileToAnalyze.length));

        sumOfPixels += ArrayUtil.sum(profileToAnalyze, 0, endSearchIndex);
      }

      // Now average the summed pixels
      float averagedValueOfPixels = sumOfPixels / windowSize;

      // Set the DoubleRef to a lower value if necessary
      if (lowestValueFound.getValue() > averagedValueOfPixels)
      {
        lowestValueFound.setValue(averagedValueOfPixels);
      }
    }

    // figure out of we fell below the lowest acceptable value or not
    if (lowestValueFound.getValue() >= lowestAcceptableValue)
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  /**
   * Capture light images from all cameras and analyze them.
   *
   * @author Reid Hayhow
   */
  private void captureAndAnalyzeLightImages() throws XrayTesterException
  {
    // Value used to store and report our progress to the progress observable
    int percentDone = 0;
    int numberOfCameras = XrayCameraArray.getNumberOfCameras();
    int camerasAnalyzed = 0;
    if (!(_useOfflineImages == true && XrayTester.getInstance().isSimulationModeOn()))
    {
    	// Assure xrays are on
    	_xraySource.on();

    }
    // Capture an image from each camera
    for (AbstractXrayCamera camera : _cameras)
    {
        // Create a result object and store it in the set
        AreaModeImageResults result = new AreaModeImageResults(camera);
        _setOfResults.add(result);
        if (_enableCameraMonitoringFeature == true)
        {
            // Reset sum data
            result.resetSumData(256);
        }
        int sampleCount = 1;
        //Sample Collection - Loop through 30 times to get average value
        for(int i=0; i<sampleCount; i++)
        {      
          // Get the snapshot from this camera
          Image areaModeImage = null;
          if (_useOfflineImages == true && XrayTester.getInstance().isSimulationModeOn())
          {
            
          }
          // Allow use of offline images for regression testing
          if (_useOfflineImages == true && XrayTester.getInstance().isSimulationModeOn())
          {
            //areaModeImage.decrementReferenceCount();
             areaModeImage = XrayImageIoUtil.loadPngImage(_lightImageLocationString + camera.getId() + _lightImageLocationExtension);
          }
          else
          {
             areaModeImage = camera.getAreaModeImage();  
          }



          // Set the light image in the result object for use later
          result.setLightImage(areaModeImage);

          // Analyze the image and get the result
          boolean imageIsOK = analyzeLightAreaModeImage(areaModeImage, result);

         if (_enableCameraMonitoringFeature == true)
         {
              // Sum Sample Data
              result.sumEdgeMean(result.getEdgeMean());
              result.sumEdgeStandardDeviation(result.getEdgeStandardDeviation());
              result.sumEdgeMaxGradient(result.getEdgeMaxGradient());
              result.sumEdgeFullHistogram(result.getEdgeFullHistogram());
         }
          
          // If the image is not OK, add this camera to the set of failing cameras
          if (imageIsOK == false)
          {
            _setOfFailingCameras.add(camera);
          }

          // Save the image for later analysis
          if(sampleCount == 1)
            saveImageAfterAnalysisIfNecessary(camera, areaModeImage,"_light", imageIsOK);
          else
            saveImageAfterAnalysisIfNecessary(camera, areaModeImage,"_" + i + "_light", imageIsOK);

          // We are done with the image, so decref it and null it out so JVM can GC it
          areaModeImage.decrementReferenceCount();
          areaModeImage = null;

        }//end of sample collection

        // Increment the number of cameras we have analyzed images to calcluate progress
        camerasAnalyzed++;

        // Calculate how far along we are now and update the progress observable with our progress
        percentDone = (int)((100 * ((double)camerasAnalyzed / numberOfCameras) * _FIFTY_PERCENT_DONE));
        reportProgress(percentDone);

         if (_enableCameraMonitoringFeature == true)
         {
            // Sum Sample Data
            result.setEdgeMean(result.getEdgeMeanAverage(sampleCount));
            result.setEdgeStandardDeviation(result.getEdgeStandardDeviationAverage(sampleCount));
            result.setEdgeMaxGradient(result.getEdgeMaxGradientAverage(sampleCount));
            result.setEdgeFullHistogram(result.getEdgeFullHistogramAverage(sampleCount));

            if(_isDeveloperSystemEnabled == true)
            {
                boolean imageAverageIsOK = true;
                imageAverageIsOK &= result.isEdgeGradientWithinLimits(_edgeGradientMinValue, _edgeGradientMaxValue);
                imageAverageIsOK &= result.isEdgeStandardDeviationWithinLimits(_edgeStandardDevMinValue, _edgeStandardDevMaxValue);
                imageAverageIsOK &= result.isBetaAreaWithinLimits(_betaAreaMinValue, _betaAreaMaxValue);
                // If the image is not OK, add this camera to the set of failing cameras
                if (imageAverageIsOK == false)
                {
                    _setOfFailingCameras.add(camera);
                }
            }
        }
 
    } //end of for(AbstractXrayCamera camera...)

    // Wait for the image saving tasks (if there are any) to complete
    try
    {
      _executor.waitForTasksToComplete();
    }
    // Handle a known exception by throwing it up the chain
    catch (XrayTesterException xrayTesterException)
    {
      throw xrayTesterException;
    }
    // Handle other exceptions
    catch (Exception otherException)
    {
      // Set the status of the result
      _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);

      // If the paralle thread task encountered an exception during execution
      // unwrap it
      if (otherException instanceof ExecutionException)
      {
        // Get the cause of the execution exception
        Throwable causeOfOtherException = otherException.getCause();

        //If it was an XrayTesterException, throw it along
        if (causeOfOtherException instanceof XrayTesterException)
        {
          throw (XrayTesterException)causeOfOtherException;
        }
        // Otherwise it was not expected, log it
        else
        {
          otherException.printStackTrace();
        }
      }
      // Not an execution exception, log it
      else
      {
        otherException.printStackTrace();
      }
    } // End of catch (ExecutionException ...)
  }

  /**
   * Actual analysis of the light image. Create horizontal profile and vertical profile
   * of the light image. Compare the lowest gray values against the threshold below which
   * the camera will not be calibratable. Also look at the mean gray value in the image
   * and the standard deviation of the gray values. Finally, create a histogram from the
   * image.
   *
   * @author Reid Hayhow
   */
  private boolean analyzeLightAreaModeImage(Image imageToAnalyze, AreaModeImageResults result)
  {
    // Start with the assumption that the image is OK, we will & in the true status
    // to catch failures
    boolean imageIsOK = true;

    // Get the horizontal profile of this image. This averages all of the image
    // columns into an array the width of the image
    float[] horizontalProfile = ImageFeatureExtraction.profile(imageToAnalyze, _verticalRegionOfInterest);

    // Create a double ref to get the lowest value from the profile window search
    DoubleRef horizontalProfileGrayValue = new DoubleRef(_MAX_GRAY_LEVEL);

    // Check the profile to see if any values are below the limit. The values are averaged across the search window
    // to help smooth out variation
    imageIsOK = isProfileAverageBelowValueAcrossWindow(horizontalProfile, _lightHorizontalProfileMinGrayValue.intValue(), _SEARCH_WINDOW_SIZE, horizontalProfileGrayValue);

    // Save the lowest value in the result object
    result.setLightHorizontalMinValue((float)horizontalProfileGrayValue.getValue());

    // columns into an array the width of the image
    float[] verticalProfile = ImageFeatureExtraction.profile(imageToAnalyze, _horizontalRegionOfInterest);

    // Create a double ref to get the lowest value from the profile window search
    DoubleRef verticalProfileLowestValue = new DoubleRef(_MAX_GRAY_LEVEL);

    // Check the profile to see if any values are below the limit. The values are averaged across the search window
    // to help smooth out variation
    imageIsOK = isProfileAverageBelowValueAcrossWindow(verticalProfile, _lightVerticalProfileMinGrayValue.intValue(), _SEARCH_WINDOW_SIZE, verticalProfileLowestValue);

    // Save the lowest value in the result object
    result.setLightVerticalMinValue((float)verticalProfileLowestValue.getValue());

    // Allocate storage for the mean and standard deviation we are about to get
    DoubleRef meanDoubleRef = new DoubleRef();
    DoubleRef stdDevDoubleRef = new DoubleRef();

    // Get the standard deviation and mean from the image and store them
    Statistics.meanStdDev(imageToAnalyze, meanDoubleRef, stdDevDoubleRef);

    // Store the standard deviation and mean value in the result object
    result.setLightStdDev(stdDevDoubleRef.getValue());
    result.setLightMeanGray(meanDoubleRef.getValue());

    // Create and store a histogram
    int[] histogram = Threshold.histogram(imageToAnalyze, _verticalRegionOfInterest, _NUMBER_OF_HISTOGRAM_BINS);
    result.setLightHistogram(histogram);

     if (_enableCameraMonitoringFeature == true)
     {
        //Check camera quality by rejecting camera with
        //(1)Maximum Gradient (Sobel(img)) > edge gradient spec_limit
        //(2)Standard Deviation (Sobel(img)) > edge std dev spec_limit

        // Create and store a histogram
        Image sourceImg = imageToAnalyze;
        ImagePlus edgeImg = new ImagePlus("",sourceImg.getBufferedImage());
        ImageProcessor  ip =new ByteProcessor(edgeImg.getImage());

        //Perform GaussianBlur to smooth out or filter noise
        //GaussianBlur gBlur =new GaussianBlur();
        //gBlur.blurGaussian(ip, 1.0, 1.0, 0.002);

        // Remove the 4 column of fake pixel at the end of image strip
        ip.setRoi(0, 0, sourceImg.getWidth()-4,sourceImg.getHeight());
        ip = ip.crop();
        //ip.resize(sourceImg.getWidth()-4,sourceImg.getHeight());
        ip.filter(ImageProcessor.FIND_EDGES);

        // Create sobel edge image and store a histogram
        // Do not use this Filter.convolveSobelCompass, it give different value
        //Image  sobelImg = Filter.convolveSobelCompass(sourceImg,verticalRegionOfInterest);
        sobelImg =  Image.createFloatImageFromBufferedImage(ip.getBufferedImage());
        //result.clearLightImage();

        //    Now create the regions of interest that will be used for analyzing the edge images
        RegionOfInterest edgeVerticalRegionOfInterest = new RegionOfInterest(0,
                                                         0,
                                                         _numberOfSegmentPixelsToExamine-4,
                                                         _imageHeightInPixels,
                                                         0,
                                                         RegionShapeEnum.RECTANGULAR);

        //int[] edgeHistogram = Threshold.histogram(sobelImg, edgeVerticalRegionOfInterest, _NUMBER_OF_HISTOGRAM_BINS);
        int[] edgeFullHistogram = Threshold.histogram(sobelImg, edgeVerticalRegionOfInterest, 256);

        // Get the standard deviation and mean from the sobel edge image and store them
        Statistics.meanStdDev(sobelImg, meanDoubleRef, stdDevDoubleRef);

        double badAreaThreshold = meanDoubleRef.getValue() + 3.0 *stdDevDoubleRef.getValue();
        double badArea = 0;
        for (int i = 0; i < edgeFullHistogram.length; i++)
        {
            if (i > badAreaThreshold)
            {
                badArea += edgeFullHistogram[i];
            }
        }
        badArea = 100 * badArea / (double)((sourceImg.getWidth()-4)*(sourceImg.getHeight()));
        // Store the edge standard deviation and mean value in the result object
        result.setEdgeStandardDeviation(stdDevDoubleRef.getValue());
        result.setEdgeMean(meanDoubleRef.getValue());
        //result.setEdgeHistogram(edgeHistogram);
        result.setBetaArea(badArea);

        //Calculate the edge max gradient
        //result.setEdgeMaxGradient(Statistics.maxValue(sobelImg));
        result.setEdgeFullHistogram(edgeFullHistogram);
        result.setEdgeMaxGradient( maxIndex(edgeFullHistogram,0,edgeFullHistogram.length));
        result.setEdgeAvgGradient( Average(edgeFullHistogram,0,edgeFullHistogram.length,(int)meanDoubleRef.getValue()));

        if(_isDeveloperSystemEnabled == true)
        {
            imageIsOK &= result.isEdgeGradientWithinLimits(_edgeGradientMinValue, _edgeGradientMaxValue);
            imageIsOK &= result.isEdgeStandardDeviationWithinLimits(_edgeStandardDevMinValue, _edgeStandardDevMaxValue);
            imageIsOK &= result.isBetaAreaWithinLimits(_betaAreaMinValue, _betaAreaMaxValue);
        }
        edgeImg.getImage().flush();

        // We are done with the image, so decref it and null it out so JVM can GC it
        sobelImg.decrementReferenceCount();
        sobelImg = null;
    }
    
    // Now that we have all the data, check it against the limits
    imageIsOK &= result.isLightMeanGrayValueWithinLimits(_lightMeanPixelMinValue, _lightMeanPixelMaxValue);
    imageIsOK &= result.isLightStandardDeviationWithinLimits(_lightStandardDevMinValue, _lightStandardDevMaxValue);
    imageIsOK &= result.isLightHorizontalMinValueWithinLimits(_lightHorizontalProfileMinGrayValue);
    imageIsOK &= result.isLightVerticalMinValueWithinLimits(_lightVerticalProfileMinGrayValue);

    return imageIsOK;
  }

  /**
   * This method captures dark images and compares them against the light images.
   *
   * @author Reid Hayhow
   */
  private void captureDarkImagesAndAnalyzeSubtractedLightImage() throws XrayTesterException
  {
    // Value used to store and report our progress to the progress observable
    int percentDone = 0;
    int numberOfCameras = XrayCameraArray.getNumberOfCameras();
    int camerasAnalyzed = 0;

    // Turn X-rays off in preparation for acquiring dark images
    _xraySource.off();

    // Capture a dark image from each camera
    for (AbstractXrayCamera camera : _cameras)
    {
      // Get the snapshot from this camera
      Image areaModeImage = null;

      // Support offline images for regression tests
      if (_useOfflineImages == true && XrayTester.getInstance().isSimulationModeOn())
      {
        //areaModeImage.decrementReferenceCount();
        //areaModeImage = null;
        areaModeImage = XrayImageIoUtil.loadPngImage(_darkImageLocationString + camera.getId() + _darkImageLocationExtension);
      }
      else
      {
         areaModeImage = camera.getAreaModeImage();
      }

      // Find the matching result object from the light image to store results in
      AreaModeImageResults result = null;
      for (AreaModeImageResults match : _setOfResults)
      {
        if (match.getCamera().equals(camera))
        {
          result = match;
          break;
        }
      }

      // Something went wrong if we didn't find a match!
      Assert.expect(result != null);

      // Compare the dark and light image by subtracting them
      boolean imageIsOK = compareLightAndDarkImages(camera, areaModeImage, result);

      // We are done with the light image, so free it up
      result.clearLightImage();

      // Save the dark image (if necessary) for later analysis
      saveImageAfterAnalysisIfNecessary(camera, areaModeImage, "_dark", imageIsOK);

      // We are done with the dark image, so decref it and free the handle
      areaModeImage.decrementReferenceCount();
      areaModeImage = null;

      // Increment the number of cameras we have analyzed images to calcluate progress
      camerasAnalyzed++;

      // Calculate how far along we are now and update the progress observable with our progress
      percentDone = (int)((100 * ((double)camerasAnalyzed / numberOfCameras) * .5)) + 50;
      reportProgress(percentDone);
    } //end of for(AbstractXrayCamera camera...)

    // Wait for the tasks to complete
    try
    {
      _executor.waitForTasksToComplete();
    }
    // Handle a known exception by throwing it up the chain
    catch (XrayTesterException xrayTesterException)
    {
      throw xrayTesterException;
    }
    // Handle other exceptions
    catch (Exception otherException)
    {
      // Set the status of the result
      _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);

      // If the paralle thread task encountered an exception during execution
      // unwrap it
      if (otherException instanceof ExecutionException)
      {
        // Get the cause of the execution exception
        Throwable causeOfOtherException = otherException.getCause();

        //If it was an XrayTesterException, throw it along
        if (causeOfOtherException instanceof XrayTesterException)
        {
          throw (XrayTesterException)causeOfOtherException;
        }
        // Otherwise it was not expected, log it
        else
        {
          otherException.printStackTrace();
        }
      }
      // Not an execution exception, log it
      else
      {
        otherException.printStackTrace();
      }
    } // End of catch (ExecutionException ...)
  }

  /**
   * This method is not used currently but could be useful. Results classes would
   * need to be created and populated.
   *
   * @author Reid Hayhow
   */
  private boolean analyzeDarkAreaModeImage(Image imageToAnalyze, AreaModeImageResults result)
  {
    boolean imageIsOK = true;

    // Get the horizontal profile of this image. This averages all of the image
    // columns into an array the width of the image
    float[] horizontalProfile = ImageFeatureExtraction.profile(imageToAnalyze, _verticalRegionOfInterest);

    // Check the Max and Min from the horizontal array and store the results
    float maxGrayInHorizontalProfile = ArrayUtil.max(horizontalProfile);
    float minGrayInHorizontalProfile = ArrayUtil.min(horizontalProfile);

    float grayHorizontalDeta = Math.abs(maxGrayInHorizontalProfile - minGrayInHorizontalProfile);

    // columns into an array the width of the image
    float[] verticalProfile = ImageFeatureExtraction.profile(imageToAnalyze, _horizontalRegionOfInterest);

    // Check the Max and Min from the vertical array and store the results
    float maxGrayInverticalProfile = ArrayUtil.max(verticalProfile);
    float minGrayInverticalProfile = ArrayUtil.min(verticalProfile);

    float grayVerticalDeta = Math.abs(maxGrayInverticalProfile - minGrayInverticalProfile);

    // Allocate storage for the mean and standard deviation we are about to get
    DoubleRef meanDoubleRef = new DoubleRef();
    DoubleRef stdDevDoubleRef = new DoubleRef();

    // Get the standard deviation and mean from the image and store them
    Statistics.meanStdDev(imageToAnalyze, meanDoubleRef, stdDevDoubleRef);

    // Create and store a histogram
    int[] histogram = Threshold.histogram(imageToAnalyze, _verticalRegionOfInterest, _NUMBER_OF_HISTOGRAM_BINS);

    return imageIsOK;
  }

  /**
   * @author Reid Hayhow
   */
  private boolean compareLightAndDarkImages(AbstractXrayCamera camera, Image darkImage, AreaModeImageResults result) throws DatastoreException
  {
    Assert.expect(darkImage != null);
    Assert.expect(result != null);
    Assert.expect(camera != null);

    // Checking on the isDirect method for the byte buffers is necessary for the
    // IPP code to work
    Assert.expect(darkImage.getByteBuffer().isDirect());

    Image lightImage = result.getLightImage();
    Assert.expect(lightImage.getByteBuffer().isDirect());

    // Assume the image is OK to start with, we will & in the real status
    Boolean isImageOK = true;

    // subtract the dark image from the light image to get 'responsiveness' image
    Image subtractedImage = Arithmetic.subtractImages(lightImage, darkImage);

    // Get the horizontal profile of this image. This averages all of the image
    // columns into an array the width of the image
    float[] horizontalProfile = ImageFeatureExtraction.profile(subtractedImage, _verticalRegionOfInterest);

    // Set the DoubleRef to a sentinal HIGH value. This guarantees we will catch
    // the lowest value in the search across the window
    DoubleRef horizontalProfileLowestValue = new DoubleRef(_MAX_GRAY_LEVEL);

    // Now search the horizontal profile to see if it has an average value across the search window
    // below our limit. If it does, the value will be returned in the DoubleRef
    isImageOK &= isProfileAverageBelowValueAcrossWindow(horizontalProfile,
                                                        _lightHorizontalProfileMinGrayValue.intValue(),
                                                        _SEARCH_WINDOW_SIZE,
                                                        horizontalProfileLowestValue);

    // Save the lowest averaged value from the subtracted horizontal profile
    result.setSubtractedHorizontalResponsiveness((float)horizontalProfileLowestValue.getValue());

    // columns into an array the width of the image
    float[] verticalProfile = ImageFeatureExtraction.profile(subtractedImage, _horizontalRegionOfInterest);

    // Set the DoubleRef to a sentinal HIGH value. This guarantees we will catch
    // the lowest value in the search across the window
    DoubleRef verticalProfileLowestValue = new DoubleRef(_MAX_GRAY_LEVEL);

    // Now search the vertical profile to see if it has an average value across the search window
    // below our limit. If it does, the value will be returned in the DoubleRef
    isImageOK &= isProfileAverageBelowValueAcrossWindow(verticalProfile,
                                                        _lightVerticalProfileMinGrayValue.intValue(),
                                                        _SEARCH_WINDOW_SIZE,
                                                        verticalProfileLowestValue);

    // Save the lowest averaged value from the subtracted vertical profile
    result.setSubtractedVerticalResponsiveness((float)verticalProfileLowestValue.getValue());

    // Save the subtracted image (if necessary)
    saveImageAfterAnalysisIfNecessary(camera, subtractedImage, "_subtracted", isImageOK);

    // We are done with the subtracted image, so decref it and free the handle
    subtractedImage.decrementReferenceCount();
    subtractedImage = null;

    return isImageOK;
  }

  /**
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    //Save off the active motion profile, just in case we end up moving the
    //stage and setting the profile to a point-to-point profile
    _panelPositionerSavedProfile = _panelPositioner.getActiveMotionProfile();

  }

 /**
   * Create the header for the logfile used by this task.
   *
   * @author Reid Hayhow
   */
  static private void createCameraMonitoringLogFileAndHeader() throws DatastoreException
  {
      //XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
     String fullPath =Directory.getConfirmAreaModeImageLogDir()+ File.separator+
                                     FileName.getConfirmCameraQualityLogFileName()+
                                     FileName.getCommaSeparatedValueFileExtension();
    _fileLogger = new FileLoggerAxi(fullPath,
                                    _maxLogFileSizeInBytes);


    // Because of the various types of information, I chose to build the header
    // up along side the data types. This makes it easier to keep the header in
    // synch with the output data
    StringBuilder header = new StringBuilder();
    for(int i=0; i<_cameras.size();i++)
    {
       header.append("C" + String.valueOf(i) + "-Gradient LLimit," +
                     "C" + String.valueOf(i) + "-Gradient Mean," +
                     "C" + String.valueOf(i) + "-Gradient ULimit," +
                     "C" + String.valueOf(i) + "-Gradient StdDev," +
                     "C" + String.valueOf(i) + "-Gradient Above Mean," +
                     "C" + String.valueOf(i) + "-Gradient Max," +
                     "C" + String.valueOf(i) + "-Gradient Beta," +
                     "C" + String.valueOf(i) + "-Grade," +
                     "C" + String.valueOf(i) + "-Result," +
                     "C" + String.valueOf(i) + "-Mac Address,");
    }
    File file =new File(fullPath);
    if(file.exists())
    {
        double bytes = file.length();
        if(_isDeveloperSystemEnabled)
          System.out.println("bytes : " + bytes);
    }
    else
    {
      if (_isDeveloperSystemEnabled)
        System.out.println("File does not exists!");
      _fileLogger.append(header.toString());
    }
  }

  /**
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    // restore the saved profile before continuing. If it had been a scan profile
    // and we set it to point-to-point without restoring they won't be happy
    _panelPositioner.setMotionProfile(_panelPositionerSavedProfile);

    // Default assumption is that xrays should be off most of the time
    _xraySource.off();
    
    _automatedTask = false;
  }


  /**
   * Method to create the log file for this task.
   *
   * @author Reid Hayhow
   */
  private void logReport() throws DatastoreException
  {
    // Because of the various types of information, I chose to build the header
    // up along side the data types. This makes it easier to keep the header in
    // synch with the output data
    StringBuilder header = new StringBuilder();

    // But we only want one header, so keep track of when it is complete :-)
    boolean buildHeader = true;

    // Use a different buffer for the actual data to be logged
    StringBuilder logDataBuffer = new StringBuilder();
    StringBuilder logQualityDataBuffer = new StringBuilder();
    String newLine = System.getProperty("line.separator");

    // Iterate over the set of results. AreaModeImageResults implements comparable
    // and sorts by the camera number for easy reading
    for (AreaModeImageResults result : _setOfResults)
    {
      // Log the camera id header and number once only
      if (buildHeader)
      {
        header.append("camera id,");
      }
      logDataBuffer.append(result.getCamera().getId() + ",");

      //*********** Log data for the subtracted image first. These measure responsiveness **************/

      // Log horizontal responsiveness header and value once only
      if (buildHeader)
      {
        header.append("horizontalResponsiveness,P/F,min limit, max limit,");
      }
      logDataBuffer.append(result.getSubtractedHorizontalResponsiveness() + ",");

      // Log if this test passed or failed and log limits for reference if it failed
      if (result.isSubtractedHorizontalResponsivenessWithinLimits(_subtractedHorizontalProfileMinResponsivenessValue, _subtractedHorizontalProfileMaxResponsivenessValue))
      {
        logDataBuffer.append("P,,,");
      }
      else
      {
        // Log the limits for reference if the camera failed
        logDataBuffer.append("F," + _subtractedHorizontalProfileMinResponsivenessValue + "," + _subtractedHorizontalProfileMaxResponsivenessValue + ",");
      }

      // Log the vertical responsiveness header and value once only
      if (buildHeader)
      {
        header.append("verticalResponsiveness,P/F,min limit, max limit,");
      }
      logDataBuffer.append(result.getSubtractedVerticalResponsiveness() + ",");

      // Log if this test passed or failed and log limits for reference if it failed
      if (result.isSubtractedVerticalResponsivenessWithinLimits(_subtractedVerticalMinResponsivenessValue, _subtractedVerticalProfileMaxResponsivenessValue))
      {
        logDataBuffer.append("P,,,");
      }
      else
      {
        // Log the limits for reference if the camera failed
        logDataBuffer.append("F," + _subtractedVerticalMinResponsivenessValue + "," + _subtractedVerticalProfileMaxResponsivenessValue + ",");
      }

      //*********** Then log data for the light image. This helps assure calibratability **************/

      // Log horizontal min gray header and value for the horizontal profile once only
      if (buildHeader)
      {
        header.append("horizontalProfileMinGray,P/F,min limit,");
      }
      logDataBuffer.append(result.getLightHorizontalMinValue() + ",");

      // Log if this test passed or failed and log limits for reference if it failed
      if (result.isLightHorizontalMinValueWithinLimits(_lightHorizontalProfileMinGrayValue))
      {
        logDataBuffer.append("P,,");
      }
      else
      {
        // Log the limits for reference if the camera failed
        logDataBuffer.append("F," + _lightHorizontalProfileMinGrayValue + ",");
      }

      // Log the vertical min gray header and value for the vertical profile once only
      if (buildHeader)
      {
        header.append("verticalProfileMinGray,P/F,min limit,");
      }
      logDataBuffer.append(result.getLightVerticalMinValue() + ",");

      // Log if this test passed or failed and log limits for reference if it failed
      if (result.isLightVerticalMinValueWithinLimits(_lightVerticalProfileMinGrayValue))
      {
        logDataBuffer.append("P,,");
      }
      else
      {
        // Log the limits for reference if the camera failed
        logDataBuffer.append("F," + _lightVerticalProfileMinGrayValue + ",");
      }

      // Log mean gray level header and value (for the whole light image) once only
      if (buildHeader)
      {
        header.append("meanGray,P/F,min limit,max limit,");
      }
      logDataBuffer.append(result.getLightMeanGray() + ",");

      // Log if this test passed or failed and log limits for reference if it failed
      if (result.isLightMeanGrayValueWithinLimits(_lightMeanPixelMinValue, _lightMeanPixelMaxValue))
      {
        logDataBuffer.append("P,,,");
      }
      else
      {
        // Log the limits for reference if the camera failed
        logDataBuffer.append("F," + _lightMeanPixelMinValue + "," + _lightMeanPixelMaxValue + ",");
      }

      // Log standard deviation header and value (for the whole light image) once only
      if (buildHeader)
      {
        header.append("standardDeviation,P/F,min limit,max limit,,");
      }
      logDataBuffer.append(result.getLightStandardDeviation() + ",");

      // Log if this test passed or failed and log limits for reference if it failed
      if (result.isLightStandardDeviationWithinLimits(_lightStandardDevMinValue, _lightStandardDevMaxValue))
      {
        // NOTE extra comma to offset for histogram values by a column in the spreadsheet
        logDataBuffer.append("P,,,,");
      }
      else
      {
        // Log the limits for reference if the camera failed
        // NOTE extra comma to offset for histogram values by a column in the spreadsheet
        logDataBuffer.append("F," + _lightStandardDevMinValue + "," + _lightStandardDevMaxValue + ",,");
      }

      //*********** Finally log histogram data for the light image. This is for forensic use only **************/

      // Log a histogram of the gray values for analysis along with a header once only
      if (buildHeader)
      {
        header.append("8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,256,");

        if (_enableCameraMonitoringFeature == false)
        {
           header.append(newLine);
        }
      }
      int[] histogram = result.getLightHistogram();
      result.setLightHistogram(histogram);

      for (int i = 0; i < histogram.length; i++)
      {
        logDataBuffer.append(histogram[i] + ",");
      }
     if (_enableCameraMonitoringFeature == true)
     {
          if (buildHeader)
          {
            header.append("Lower Limit,Mean,Upper Limit,Std Dev,Avg Gradient,Max Gradient,Beta,Grade,Quality Result,");
          }

          //Get 3 sigma Lower and Upper Limit foe Edge Image
          double edgeMeanValue = result.getEdgeMean();
          double edgeStandardDeviation = result.getEdgeStandardDeviation();
          double edgeLowerLimit = edgeMeanValue - 3*edgeStandardDeviation;
          double edgeUpperLimit = edgeMeanValue + 3*edgeStandardDeviation;
          int Grade = 1+(int)(edgeStandardDeviation /_edgeQualityGradeMinValue);
          logDataBuffer.append(edgeLowerLimit + ","+
                               edgeMeanValue + ","+
                               edgeUpperLimit + ","+
                               edgeStandardDeviation + ","+
                               result.getEdgeAvgGradient() + ","+
                               result.getEdgeMaxGradient() + ","+
                               result.getBetaArea()+ ","+
                               "G"+Grade+ ",");

          // Log if this test passed or failed for Camera Quality
          String finalResult = "";
          if (result.isEdgeGradientWithinLimits(_edgeGradientMinValue, _edgeGradientMaxValue) &&
              result.isEdgeStandardDeviationWithinLimits(_edgeStandardDevMinValue, _edgeStandardDevMaxValue) &&
              result.isBetaAreaWithinLimits(_betaAreaMinValue, _betaAreaMaxValue))
          {
                finalResult = "PASS";
          }
          else
          {
                finalResult = "FAIL";
          }
          logDataBuffer.append(finalResult + ",");

          // Log a Edge Histogram of the gray values for analysis along with a header once only
          if (buildHeader)
          {
            //header.append("8,16,24,32,40,48,56,64,72,80,88,96,104,112,120,128,136,144,152,160,168,176,184,192,200,208,216,224,232,240,248,256" + newLine);

            for (int i = 0; i < 256; i++)
            {
               header.append(String.valueOf(i) + ",");
            }
            header.append( newLine);
          }

          int[] edgeFullHistogram = result.getEdgeFullHistogram();
          for (int i = 0; i < edgeFullHistogram.length; i++)
          {
            logDataBuffer.append(edgeFullHistogram[i] + ",");
          }

                  // Write cam 0 to the log file
            //  if(result.getCamera().getId()==0)
            //  {
            //    _fileLogger.append(logDataBuffer.toString());
            //  }

           logQualityDataBuffer.append(//result.getCamera().getId() + ","+
                               edgeLowerLimit + ","+
                               edgeMeanValue + ","+
                               edgeUpperLimit + ","+
                               edgeStandardDeviation + ","+
                               result.getEdgeAvgGradient() + ","+
                               result.getEdgeMaxGradient() + ","+
                               result.getBetaArea()+ ","+
                               "G"+Grade+ ","+
                               finalResult+ ","+
                               result.getCamera().getServerMacAddress()+ ",");


        }
      // Set the log file up for data from the next camera
      logDataBuffer.append(newLine);
      buildHeader = false;
    }
    if (_enableCameraMonitoringFeature == true)
    {
        if(_fileLogger != null)
        {
            _fileLogger.append(logQualityDataBuffer.toString());
        }
    }

    // Write it all to the log file, header first and then data
    _logger.log(header.toString() + logDataBuffer.toString());
  }

  /**
   * Finds the maximum index in the specified int array between the specified starting index (inclusive) and
   * end index (exclusive).

   */
  public float maxIndex(int[] array, int startIndex, int endIndex)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    //Return the largest index where array[i] > 0
    for (int i =endIndex-1; i >= startIndex ; i--)
    {
      if (array[i] > 0)
      {
        return i;
      }
    }
    return 0;
  }

  public float Average(int[] array, int startIndex, int endIndex, int cutting)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(startIndex >= 0);
    Assert.expect(startIndex < array.length);
    Assert.expect(endIndex <= array.length);
    Assert.expect(endIndex >= startIndex);

    float count = 0;
    float sum = 0;

    //Return the average
    for (int i =endIndex-1; i >= startIndex ; i--)
    {
      if (i > cutting)
      {
        count = count + array[i];
        sum = sum +  (i-(cutting-1))*array[i];
       }
    }

    if(count==0)
        return cutting;
    else
        return sum/count;
  }


  /**
   * Build up the message that will be displayed to the user with details about
   * which cameras failed. If none failed there will not be a message.
   *
   * @author Reid Hayhow
   */
  private void populateResultMessage()
  {
    // If there were failures then we need to supply more information
    if (_setOfFailingCameras.size() > 0)
    {
      // Set the task to fail
      _result = new Result(new Boolean(false), _name);

      // Create the buffer we will use to build up a message
      StringBuilder buffer = new StringBuilder();

      //Add the set of cameras that failed. We don't care what the specific failure
      //was, at least for now
      for (AbstractXrayCamera camera : _setOfFailingCameras)
      {
        //Append the camera number to the list of failures
        buffer.append(String.valueOf(camera.getId()) + ",");
      }

      // Get rid of the last comma in the camera list
      buffer.deleteCharAt(buffer.length() - 1);

      // Add the list of failing cameras to an array of Objects to be paramerers to the localised string
      Object[] params =
          {buffer.toString()};

      // We will use a different localization string for one camera versus multiple cameras
      LocalizedString message = null;

      // Only one camera failed, so use the string "Area Mode Image Failed"
      if (_setOfFailingCameras.size() == 1)
      {
        message = new LocalizedString("CD_AREA_MODE_IMAGE_FAILED_KEY", params);
      }
      else
      {

        // Otherwise use a plural string "Area Mode Images Failed"
        message = new LocalizedString("CD_AREA_MODE_IMAGES_FAILED_KEY", params);
      }

      // Set the status and suggested action message with the localized strings
      _result.setTaskStatusMessage(StringLocalizer.keyToString(message));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_AREA_MODE_IMAGES_FAILED_SUGGESTED_ACTION_KEY"));
    }
  }

  /**
   * Method to thread off the work of saving a single image, naming it correctly, etc.
   *
   * NOTE: It does not wait to make sure the image has been saved. This is done after
   * all the images that may need to be saved have been threaded off and given a chance
   * to be written to disk.
   *
   * @author Reid Hayhow
   */
  private void saveImageAfterAnalysisIfNecessary(AbstractXrayCamera camera, 
                                                 Image imageToSave, 
                                                 String additionalInfo, 
                                                 boolean imageIsOK) throws DatastoreException
  {
    // Create the saver task variable
    ImageSaverThreadTask saverThread = null;

    // We will use different names depending on pass/fail
    if (imageIsOK == false)
    {
      String fullPathName = _logDirectory + 
                            File.separator +
                            "camera_FAILED_" +
                            camera.getId() +
                            additionalInfo +
                            ".png";
      
      //Create the thread task to save this specific image
      saverThread = new ImageSaverThreadTask(imageToSave, fullPathName);

      // Khang Wah, delete file before create a new one.
      if(FileUtilAxi.exists(fullPathName))
      {
        FileUtilAxi.delete(fullPathName);
      }
      
      //Submit the thread task to the executor. We will check that all tasks
      //completed later
      Future<Boolean> task = _executor.submitThreadTask(saverThread);

      //Add this task to the list of tasks so we can track it later
      _listOfTasks.add(task);

    }
    else
    {
      // The image is OK, only save it if the software.config enum specifies it should be saved
      if (_config.getBooleanValue(SoftwareConfigEnum.SAVE_ALL_CONFIRMATION_IMAGES) == true)
      {
        String fullPathName = _logDirectory + 
                              File.separator +
                              "camera_" +
                              camera.getId() +
                              additionalInfo +
                              ".png";
        
        // Khang Wah, delete file before create a new one.
        if(FileUtilAxi.exists(fullPathName))
        {
          FileUtilAxi.delete(fullPathName);
        }
      
        //Create the thread task to save this specific image
        saverThread = new ImageSaverThreadTask(imageToSave, fullPathName);

        //Submit the thread task to the executor. We will check that all tasks
        //completed later
        Future<Boolean> task = _executor.submitThreadTask(saverThread);

        //Add this task to the list of tasks so we can track it later
        _listOfTasks.add(task);
      }
    }
  }

  /**
   * Method to move the stage to a clear position before capturing the area mode
   * images.
   * @author Reid Hayhow
   */
  private void moveStageBeforeCapturingImages() throws XrayTesterException
  {
    if (PanelHandler.getInstance().moveStageToCameraClearPosition() == false)
    {
      //swee yee wong - XCR-3052	Wrong information of area mode task failed exception
      throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_name);
    }
  }

  /**
   * @author Reid Hayhow
   */
  protected void useOfflineImages()
  {
    _useOfflineImages = true;
  }

  /**
   * @author Reid Hayhow
   */
  protected void useLiveImages()
  {
    _useOfflineImages = false;
  }

  /**
   * @author Reid Hayhow
   */
  protected void setLightImageLocationString(String lightImageLocationString)
  {
    _lightImageLocationString = lightImageLocationString;
  }

  /**
   * @author Reid Hayhow
   */
  protected void setLightImageLocationExtension(String lightImageLocationExtension)
  {
    _lightImageLocationExtension = lightImageLocationExtension;
  }

  /**
   * @author Reid Hayhow
   */
  protected String getLightImageLocationString()
  {
    return _lightImageLocationString;
  }

  /**
   * @author Reid Hayhow
   */
  protected String getLightImageLocationExtension()
  {
    return _lightImageLocationExtension;
  }

  /**
   * @author Reid Hayhow
   */
  protected void setDarkImageLocationString(String darkImageLocationString)
  {
    _darkImageLocationString = darkImageLocationString;
  }

  /**
   * @author Reid Hayhow
   */
  protected String getDarkImageLocationString()
  {
    return _darkImageLocationString;
  }

  /**
   * @author Reid Hayhow
   */
  protected void setDarkImageLocationExtension(String darkImageLocationExtension)
  {
    _darkImageLocationExtension = darkImageLocationExtension;
  }

  /**
   * @author Reid Hayhow
   */
  protected String getDarkImageLocationExtension()
  {
    return _darkImageLocationExtension;
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
