package com.axi.v810.business.autoCal;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CalFringePatternUniformity extends EventDrivenHardwareTask implements OpticalLiveVideoInteractionInterface 
{
    private static final int GRAY_CARD_ADJUSTABLE_RAIL_WIDTH_IN_NANOMETERS = 131000000;
    
    private static CalFringePatternUniformity _instance;
    private static Config _config = Config.getInstance();
    
    private PanelHandler _panelHandler = PanelHandler.getInstance();
    
    //Motion profile to use when moving stage, use the fastest
    protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
    
    private static OpticalLiveVideoObservable _opticalLiveVideoObservable = OpticalLiveVideoObservable.getInstance();        
    
    // Printable name for this procedure
    private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_OPTICAL_FRINGE_PATTERN_UNIFORMITY_NAME_KEY");
    
    // This cal is called on the first event after the timeout occurs.
    private static final int _EVENT_FREQUENCY = 1;
    private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.OPTICAL_MAGNIFICATION_EXECUTE_EVERY_TIMEOUT_SECONDS);
    
    private static final String _logDirectory = Directory.getOpticalFringePatternUniformityCalLogDir();
    private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.OPTICAL_FRINGE_PATTERN_UNIFORMITY_MAX_LOG_FILES_IN_DIRECTORY);
    
    //Log utility members
    private FileLoggerAxi _logger;
    private int _maxLogFileSizeInBytes = 1028;
    
    protected Map<OpticalCameraIdEnum, StagePositionMutable> _cameraToStagePositionMap = new LinkedHashMap<OpticalCameraIdEnum, StagePositionMutable>();        
    private BooleanLock  _waitingForOpticalFringePatternUniformityInput = new BooleanLock(false);
    
    private Map<OpticalCameraIdEnum, DoubleRef> _cameraToIntensityRadioMap = new LinkedHashMap<OpticalCameraIdEnum, DoubleRef>();
    private Map<OpticalCameraIdEnum, DoubleRef> _cameraToGlobalIntensityMap = new LinkedHashMap<OpticalCameraIdEnum, DoubleRef>();
    
    /**
     * @author Cheah Lee Herng
    */
    public static synchronized CalFringePatternUniformity getInstance()
    {
        if (_instance == null)
        {
          _instance = new CalFringePatternUniformity();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private CalFringePatternUniformity()
    {
        super(_NAME_OF_CAL,
              _EVENT_FREQUENCY,
              _EXECUTE_EVERY_TIMEOUT_SECONDS,
              ProgressReporterEnum.OPTICAL_FRINGE_PATTERN_UNIFORMITY_ADJUSTMENT_TASK);
        
        //Create the logger
        _logger = new FileLoggerAxi(FileName.getOpticalFringePatternUniformityFullPath() +
                                    FileName.getLogFileExtension(),
                                    _maxLogFileSizeInBytes);

        _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

        setTimestampEnum(HardwareCalibEnum.REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN);
    }        

    /**
     * @author Cheah Lee Herng
     */
    protected void executeTask() throws XrayTesterException 
    {
        boolean deferred = false;
        try
        {
          reportProgress(0);

          //Handle the fact that we cannot run light confirmations with a panel
          //in the system. Defer the task till later *but* don't cause the
          //task to fail. Display a meaningful message in the Service UI
          if (ableToRunWithCurrentPanelState() == false)
          {
            _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
            _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_PANEL_INTEFERRED_WITH_LIGHT_CONFIRM_KEY"));
            _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_REMOVE_THE_PANEL_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
            return;
          }

          reportProgress(10);

          //Setup gray card stage position
          setupGrayCardStagePosition();

          reportProgress(20);

          // At least one optical camera must be installed to run system fiducial
          Assert.expect(OpticalCameraIdEnum.getAllEnums().size() > 0);

          // Prepare PSP for live optical view
          PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);

          reportProgress(30);

          // Move stage to first optical camera and set rail width in parallel
          moveStageToGrayCardPositionAndSetRailWidthInParallel(OpticalCameraIdEnum.OPTICAL_CAMERA_1);

          reportProgress(50);

          // Display Optical Live Video, starting with first optical camera Id
          _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                OpticalCameraIdEnum.OPTICAL_CAMERA_1,
                                                StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"), 
                                                this,
                                                true,
                                                false,
                                                false));                        

          // Wait for user to record current stage position
          waitForOpticalFringePatternUniformityInput();

          // Here we check if user cancel live video so that
          // we can exit the test gracefully
          checkAbortInProgress();

          // Record current fringe pattern uniformity
          recordCurrentFringePatternUniformity(OpticalCameraIdEnum.OPTICAL_CAMERA_1);

          // Record current fringe pattern global intensity
          recordCurrentFringePatternGlobalIntensity(OpticalCameraIdEnum.OPTICAL_CAMERA_1);

          // Send event to close live video dialog
          _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));

          // Stop optical live view
          PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);

          for(OpticalCameraIdEnum cameraId : OpticalCameraIdEnum.getAllEnums())
          {
            if ((cameraId == OpticalCameraIdEnum.OPTICAL_CAMERA_1) == false)
            {
              PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspModuleEnum.REAR, OpticalShiftNameEnum.WHITE_LIGHT);

              // Move stage to next optical camera
              moveStageBeforeExecute(cameraId);

              // Display Optical Live Video, starting with first optical camera Id
              _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                    cameraId,
                                                    StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"), 
                                                    this,
                                                    true, 
                                                    false,
                                                    false));

              // Wait for user to record current stage position
              waitForOpticalFringePatternUniformityInput();

              // Here we check if user cancel live video so that
              // we can exit the test gracefully
              checkAbortInProgress();

              // Record current fringe pattern uniformity
              recordCurrentFringePatternUniformity(cameraId);

              // Record current fringe pattern global intensity
              recordCurrentFringePatternGlobalIntensity(cameraId);

              // Send event to close live video dialog
              _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));

              // Shut down projector
              PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.REAR, OpticalShiftNameEnum.WHITE_LIGHT);
            }
          }

          //If we can reach here means the task passed
          _result = new Result(new Boolean(true), _NAME_OF_CAL);

          //Log the fact that the task passed. The _name value is already localized
          _logger.append(_NAME_OF_CAL + " - " + StringLocalizer.keyToString("CD_PASSED_KEY"));

          reportProgress(100);
        }
        catch (XrayTesterException ex)
        {
            _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
            deferred = true;
            return;
        }
        //Log any unexpected exceptions
        catch (Exception e)
        {
            Assert.logException(e);
        }
        finally
        {
            //Save record intensity ratio into hardware.calib
            saveCurrentIntensityRatioToConfig();
            
            //Save record global intensity into hardware.calib
            saveCurrentGlobalIntensityToConfig();
            
            PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.REAR, OpticalShiftNameEnum.WHITE_LIGHT);
            
            //Restore stage (if needed)
            moveStageAfterExecute();
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void setUp() throws XrayTesterException 
    {
        // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean canRunOnThisHardware() 
    {
        return true;
    }
    
    public void createLimitsObject() throws DatastoreException 
    {
        _limits = new Limit(null, null, null, null);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void createResultsObject() throws DatastoreException
    {
        _result = new Result(new Boolean(false), _NAME_OF_CAL);
    }
    
    /**
     * Method for knowing if the task can run with the current panel state. For light
     * image tasks, this should return false if a panel is loaded and true if no
     * panel is loaded. This method is overridden for Dark tasks to always return
     * true. 
     */
    protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
    {
        // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
        //  add throws XrayTesterException
        //flip the logic of isPanelLoaded because light tasks can't run with a panel
        return (_panelHandler.isPanelLoaded() == false);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void cleanUp() throws XrayTesterException 
    {
        if (_cameraToStagePositionMap != null)
            _cameraToStagePositionMap.clear();
        if (_cameraToIntensityRadioMap != null)
            _cameraToIntensityRadioMap.clear();
        if (_cameraToGlobalIntensityMap != null)
            _cameraToGlobalIntensityMap.clear();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void setupGrayCardStagePosition()
    {
        Assert.expect(_cameraToStagePositionMap != null);
        
        // Gray Card Stage Position 1
        StagePositionMutable grayCardCamera1StagePosition = new StagePositionMutable();
        grayCardCamera1StagePosition.setXInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS));
        grayCardCamera1StagePosition.setYInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS));
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_1, grayCardCamera1StagePosition);
        
        // Gray Card Stage Position 2
        StagePositionMutable grayCardCamera2StagePosition = new StagePositionMutable();
        grayCardCamera2StagePosition.setXInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS));
        grayCardCamera2StagePosition.setYInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS));
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_2, grayCardCamera2StagePosition);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void moveStageBeforeExecute(OpticalCameraIdEnum opticalCameraIdEnum) throws XrayTesterException
    {
        Assert.expect(opticalCameraIdEnum != null);
        
        //Set up the stage to get it out of the way of the cameras
        StagePositionMutable stagePosition = _cameraToStagePositionMap.get(opticalCameraIdEnum);
        
        //Profile1 is fastest
        PanelPositioner.getInstance().setMotionProfile(new MotionProfile(_motionProfile));
        
        // Move stage to existing center position before performing CD&A
        PanelPositioner.getInstance().pointToPointMoveAllAxes(stagePosition);
    }
    
    /**
     * Method that controls stage motion after task execution.
     *
     * @author Cheah Lee Herng
    */
    protected void moveStageAfterExecute()
    {
        //Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void waitForOpticalFringePatternUniformityInput()
    {
        _waitingForOpticalFringePatternUniformityInput.setValue(true);
        try
        {
          _waitingForOpticalFringePatternUniformityInput.waitUntilFalse();
        }
        catch (InterruptedException ex)
        {
          // Do nothing ...
        }
    }

    /**
     * @author Cheah Lee Herng
     */
    public void runNextOpticalCamera() 
    {
        _waitingForOpticalFringePatternUniformityInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void cancelLiveVideo()
    {
        _taskCancelled = true;
        _waitingForOpticalFringePatternUniformityInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void setSurfaceMapPoint(List<Point2D> points)
    {
      // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void recordCurrentFringePatternUniformity(OpticalCameraIdEnum cameraId) throws XrayTesterException
    {
        Assert.expect(_cameraToIntensityRadioMap != null);
        Assert.expect(cameraId != null);
        
        double intensityRatio = FringePattern.getIntensityRatio();        
        
        DoubleRef intensityRatioDoubleRef = _cameraToIntensityRadioMap.get(cameraId);
        Assert.expect(intensityRatioDoubleRef == null);
        
        _cameraToIntensityRadioMap.put(cameraId, new DoubleRef(intensityRatio));
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void recordCurrentFringePatternGlobalIntensity(OpticalCameraIdEnum cameraId) throws XrayTesterException
    {
        Assert.expect(_cameraToGlobalIntensityMap != null);
        Assert.expect(cameraId != null);
        
        double globalIntensity = FringePattern.getGlobalIntensity();        
        
        DoubleRef globalIntensityDoubleRef = _cameraToGlobalIntensityMap.get(cameraId);
        Assert.expect(globalIntensityDoubleRef == null);
        
        _cameraToGlobalIntensityMap.put(cameraId, new DoubleRef(globalIntensity));
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void saveCurrentIntensityRatioToConfig() throws XrayTesterException
    {
        Assert.expect(_cameraToIntensityRadioMap != null);
        
        for(Map.Entry<OpticalCameraIdEnum, DoubleRef> entry : _cameraToIntensityRadioMap.entrySet())
        {
            OpticalCameraIdEnum cameraIdEnum = entry.getKey();
            DoubleRef intensityRatio = entry.getValue();
            
            if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))         
                _config.setValue(PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_1_INTENSITY_RATIO, intensityRatio.getValue());
            else if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
                _config.setValue(PspCalibEnum.CAL_FRINGE_PATTERN_UNIFORMITY_CAMERA_2_INTENSITY_RATIO, intensityRatio.getValue());
            else
                Assert.expect(false, "Invalid optical camera Id");
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void saveCurrentGlobalIntensityToConfig() throws XrayTesterException
    {
        Assert.expect(_cameraToGlobalIntensityMap != null);
        
        for(Map.Entry<OpticalCameraIdEnum, DoubleRef> entry : _cameraToGlobalIntensityMap.entrySet())
        {
            OpticalCameraIdEnum cameraIdEnum = entry.getKey();
            DoubleRef globalIntensity = entry.getValue();
            
            System.out.println("CalFringePatternUniformity: Saving global intensity for camera " + cameraIdEnum.getId() + " with value = " + globalIntensity.getValue());
            
            if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))         
                _config.setValue(PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_1_GLOBAL_INTENSITY, globalIntensity.getValue());
            else if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
                _config.setValue(PspCalibEnum.CAL_FRINGE_PATTERN_CAMERA_2_GLOBAL_INTENSITY, globalIntensity.getValue());
            else
                Assert.expect(false, "Invalid optical camera Id");
        }
    }
    
    /*
     * @author Cheah Lee Herng
     */
    protected void checkAbortInProgress()
    {
        if (_taskCancelled == true)
        {
            reportProgress(100);
            _result.setStatus(HardwareTaskStatusEnum.CANCELED);
            return;
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void moveStageToGrayCardPositionAndSetRailWidthInParallel(OpticalCameraIdEnum cameraId) throws XrayTesterException
    {
        Assert.expect(cameraId != null);
        
        ExecuteParallelThreadTasks<Object> panelHandlerParallelTasks = new ExecuteParallelThreadTasks<Object>();
        
        ThreadTask<Object> task1 = new MoveStageToGrayCardPositionThreadTask(cameraId);
        ThreadTask<Object> task2 = new SetRailWidthThreadTask(GRAY_CARD_ADJUSTABLE_RAIL_WIDTH_IN_NANOMETERS);
        
        panelHandlerParallelTasks.submitThreadTask(task1);
        panelHandlerParallelTasks.submitThreadTask(task2);
        
        try
        {
          panelHandlerParallelTasks.waitForTasksToComplete();
        }
        catch (XrayTesterException xte)
        {              
          throw xte;
        }
        catch (Exception e)
        {  
          Assert.logException(e);
        }
    }
      
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setHardwareShutdownComplete()
  {
    // Do nothing
  }
}
