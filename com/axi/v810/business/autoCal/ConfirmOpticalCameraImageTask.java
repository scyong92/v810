package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public abstract class ConfirmOpticalCameraImageTask extends EventDrivenHardwareTask 
{
    private static final int MINIMUM_GAIN_FACTOR_STEP_SIZE = 11;
    private static final int GRAY_CARD_ADJUSTABLE_RAIL_WIDTH_IN_NANOMETERS = 131000000;
    
    //Logging variables
    private DirectoryLoggerAxi _logger;
    protected String _logDirectory;
    private int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.IMAGE_CONFIRM_MAX_LOG_FILE_NUMBER);
    private static final int _logTimeoutSeconds = 1;
    private static final int _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000;    
    
    //Members for saving and restoring the motion profile of the panel positioner
    private PanelPositioner _panelPositioner = PanelPositioner.getInstance();
    private PanelHandler _panelHandler = PanelHandler.getInstance();
    private MotionProfile _panelPositionerSavedProfile;   
    
    //Place to store the localized name of the task (Light or Dark)
    private String _name;
    protected String _localizedLightOrDarkString;
    
    protected static Config _config = Config.getInstance();
    
    private TreeSet<AbstractOpticalCamera> _failingCameraSet = new TreeSet<AbstractOpticalCamera>();
    
    //static, final references to the X/Y offset from the Grayscale Cal position
    protected static final int _OFFSET_FROM_GRAYSCALE_CAL_LOCATION_IN_NANOMETERS = 254000;
    
    protected static Set<AbstractOpticalCamera> _cameras = new TreeSet<AbstractOpticalCamera>();
    
    //Member variable for camera set being used
    protected Set<AbstractOpticalCamera> _camerasToGetImagesFromSet = _cameras;
    
    //The Limits and Results are mapped by camera and then test type
    protected Map<AbstractOpticalCamera, Map<OpticalCameraImageDataTypeEnum, Result>> _cameraToTestTypeToResultMap = new HashMap<AbstractOpticalCamera, Map<OpticalCameraImageDataTypeEnum, Result>>();
    protected Map<AbstractOpticalCamera, Map<OpticalCameraImageDataTypeEnum, Limit>> _cameraToTestTypeToLimitMap = new HashMap<AbstractOpticalCamera, Map<OpticalCameraImageDataTypeEnum, Limit>>();
    
    //Optical Camera Gain Factor are mapped by camera
    protected Map<AbstractOpticalCamera, DoubleRef> _cameraToGainFactor = new HashMap<AbstractOpticalCamera, DoubleRef>();
    
    //X and Y stage position Calib enum for each task
    protected HardwareCalibEnum _xPosition;
    protected HardwareCalibEnum _yPosition;
    
    protected Map<OpticalCameraIdEnum, StagePositionMutable> _cameraToStagePositionMap = new LinkedHashMap<OpticalCameraIdEnum, StagePositionMutable>();
    
    //Motion profile to use when moving stage, use the fastest
    protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
    
    /**
     * @author Cheah Lee Herng 
     */
    protected ConfirmOpticalCameraImageTask(String name,
                                           int frequency,
                                           long timeoutInSeconds,
                                           String logDirectoryPathString,
                                           ProgressReporterEnum progressReporter)
    {
        super(name, frequency, timeoutInSeconds, progressReporter);

        //Save the name passed in from the sub-class to use creating the limits
        _name = name;

        //Can't log to nowhere
        Assert.expect(logDirectoryPathString != null);

        //Save this location for use later saving image copies
        _logDirectory = logDirectoryPathString;

        //Create the directory logger
        _logger = new DirectoryLoggerAxi(_logDirectory, FileName.getOpticalCameraImageLogFileBasename(),
                                         FileName.getLogFileExtension(), _maxLogFilesInDirectory);

        //This confirmation and all sub-classed confirmations are automated
        _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void analyzeImage(Image imageToAnalyze, AbstractOpticalCamera cameraImageIsFrom)
    {
        //Get the preCreated map of results
        Map<OpticalCameraImageDataTypeEnum, Result> mapOfResults = _cameraToTestTypeToResultMap.get(cameraImageIsFrom);
        
        //Also get the limits so we can check them and know if this image passed or failed
        Map<OpticalCameraImageDataTypeEnum, Limit> mapOfLimits = _cameraToTestTypeToLimitMap.get(cameraImageIsFrom);

        Assert.expect(mapOfResults != null);
        Assert.expect(mapOfLimits != null);
        Assert.expect(imageToAnalyze != null);
        
        int imageWidthInPixels = imageToAnalyze.getWidth();
        int imageHeightInPixels = imageToAnalyze.getHeight();
        
        RegionOfInterest regionOfInterest = new RegionOfInterest(0,
                                                                 0,
                                                                 imageWidthInPixels,
                                                                 imageHeightInPixels,
                                                                 0,
                                                                 RegionShapeEnum.RECTANGULAR);
        
        //Allocate storage for the mean and standard deviation we are about to get
        DoubleRef meanDoubleRef = new DoubleRef();
        DoubleRef stdDevDoubleRef = new DoubleRef();
        
        //Calculate mean and standard deviation of gray level
        Statistics.meanStdDev(imageToAnalyze, regionOfInterest, meanDoubleRef, stdDevDoubleRef);
        
        mapOfResults.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY).setResults(new Double(meanDoubleRef.getValue()));
        mapOfResults.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).setResults(new Double(stdDevDoubleRef.getValue()));
        
        //Check these results against the limits for them and accumulate the results
        mapOfLimits.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY).didTestPass(mapOfResults.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY));
        mapOfLimits.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).didTestPass(mapOfResults.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION));
        
        //Calculate the maximum deviation (mean + 3 * sigma)
        double maximumDeviation = meanDoubleRef.getValue() + (3 * stdDevDoubleRef.getValue());
        Assert.expect(maximumDeviation > 0.0);
        
        //Calculate adjusted gain factor
        int calculatedGainFactor = (int)(Math.round(250.0 / maximumDeviation));
        
        //Multiply gain factor with default gain step size
        int finalGainFactor = calculatedGainFactor * MINIMUM_GAIN_FACTOR_STEP_SIZE;
        
        //Sanity check to make sure gain factor is within range
        finalGainFactor = Math.max(finalGainFactor, 0);
        finalGainFactor = Math.min(finalGainFactor, 100);
        
        System.out.println("ConfirmOpticalCameraImageTask: Camera " + cameraImageIsFrom.getId() + " : finalGainFactor = " + finalGainFactor);
        
        DoubleRef prevGainFactor = _cameraToGainFactor.get(cameraImageIsFrom);
        Assert.expect(prevGainFactor == null);
        
        _cameraToGainFactor.put(cameraImageIsFrom, new DoubleRef(finalGainFactor));
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void getAndAnalyzeImageMap() throws XrayTesterException
    {
        //Setup gray card stage position
        setupGrayCardStagePosition();
        
        reportProgress(40);
        
        //Do some basic checking on the camera sets before using them
        int totalNumberOfCameras = OpticalCameraArray.getNumberOfCameras();
        
        if (_cameras.size() != totalNumberOfCameras)
        {
            throw new HardwareTaskExecutionException(ConfirmationExceptionEnum.INCORRECT_OPTICAL_CAMERA_LIST);
        }
        
        // Progress steps
        int initialProgressPercentage = 40;
        int progressStepSize = 20;
        int progressPercentage = initialProgressPercentage;
        
        // Start capturing images for all optical cameras
        for(AbstractOpticalCamera opticalCamera : _camerasToGetImagesFromSet)
        {            
            // Get required stage position
            StagePositionMutable stagePosition = _cameraToStagePositionMap.get(opticalCamera.getOpticalCameraIdEnum());
            Assert.expect(stagePosition != null);
            
            acquireOpticalCameraImages(opticalCamera, stagePosition);
            
            Collection<String> opticalCameraImageFiles = FileUtilAxi.listAllFilesFullPathInDirectory(getOpticalCamerConfirmationLogImageDir(opticalCamera.getId()));
            
            // Start analyzing images
            for (String opticalCameraImageFile : opticalCameraImageFiles)
            {
                Image image = null;
                try
                {
                    // Load the saved image.
                    image = XrayImageIoUtil.loadPngImage(opticalCameraImageFile);                                

                    // Start analyze image
                    analyzeImage(image, opticalCamera);
                }
                finally
                {
                    if (image != null)
                    {
                        // De-reference image
                        image.decrementReferenceCount();
                        image = null;
                    }
                }                                                              
            }
            
            // Increment progress and display in ProgressBar
            progressPercentage += progressStepSize;
            reportProgress(progressPercentage);
        }
    }
    
    /**
   * Create the results object for this test result.
   *
   * @author Reid Hayhow
   */
    protected void createResultsObject() throws DatastoreException
    {
        _result = new Result(_name);

        _cameraToTestTypeToResultMap.clear();

        readCameraListFromFile();

        //Populate Result Map
        populateResultMap();
    }
    
    /**
     * Method to populate the result map for a specific camera direction.
     * 
     * @author Cheah Lee Herng
     */
    private void populateResultMap()
    {
        //Get all of the localized strings we will need for the results
        //First, the the common building block keys
        String localizedCameraString = StringLocalizer.keyToString("ACD_OPTICAL_CAMERA_KEY");
        
        String localizedMeanGray = StringLocalizer.keyToString("CD_MEAN_GRAY_KEY");
        String localizedGrayStandardDeviation = StringLocalizer.keyToString("CD_GRAY_STANDARD_DEVIATION_KEY");
        
        for(AbstractOpticalCamera opticalCamera : _camerasToGetImagesFromSet)
        {
            Map<OpticalCameraImageDataTypeEnum, Result> mapOfResultsToTests = new TreeMap<OpticalCameraImageDataTypeEnum, Result>();
            _cameraToTestTypeToResultMap.put(opticalCamera, mapOfResultsToTests);
            
            Result cameraResultContainer = new Result(localizedCameraString + opticalCamera.getId());
            _result.addSubResult(cameraResultContainer);
            
            Result meanPixelGrayResult = new Result(new Double(220.0), localizedMeanGray);
            cameraResultContainer.addSubResult(meanPixelGrayResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY, meanPixelGrayResult);
            
            Result stdDevResult = new Result(new Double(6.0), localizedGrayStandardDeviation);
            cameraResultContainer.addSubResult(stdDevResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION, stdDevResult);
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void createLimitsObject() throws DatastoreException
    {
        _limits = new Limit(null, null, null, _name);

        _cameraToTestTypeToLimitMap.clear();

        readCameraListFromFile();

        //Create the limits maps
        populateLimitsMap();
    }
    
    /**
     * Method to simplify populating the Limits map based on the camera direction
     * 
     * @param directionEnum
     * @throws DatastoreException 
     */
    private void populateLimitsMap() throws DatastoreException
    {
        String localizedCameraString = StringLocalizer.keyToString("ACD_OPTICAL_CAMERA_KEY");
        String localizedGrayValue = StringLocalizer.keyToString("ACD_GRAY_VALUE_KEY");
        
        String localizedMeanGray = StringLocalizer.keyToString("CD_MEAN_GRAY_KEY");
        String localizedGrayStandardDeviation = StringLocalizer.keyToString("CD_GRAY_STANDARD_DEVIATION_KEY");
        
        Double meanPixelHighLimit = _parsedLimits.getHighLimitDouble("MeanPixelGray");
        Double meanPixelLowLimit = _parsedLimits.getLowLimitDouble("MeanPixelGray");
        Double stdDevHighLimit = _parsedLimits.getHighLimitDouble("GrayStandardDeviation");
        Double stdDevLowLimit = _parsedLimits.getLowLimitDouble("GrayStandardDeviation");
        
        for(AbstractOpticalCamera opticalCamera : _camerasToGetImagesFromSet)
        {            
            Map<OpticalCameraImageDataTypeEnum, Limit> mapOfTestsToLimits = new TreeMap<OpticalCameraImageDataTypeEnum, Limit>();
            _cameraToTestTypeToLimitMap.put(opticalCamera, mapOfTestsToLimits);
            
            Limit cameraContainer = new Limit(null, null, null, localizedCameraString + opticalCamera.getId());
            _limits.addSubLimit(cameraContainer);
            
            Limit meanPixelGray = new Limit(meanPixelLowLimit,
                                      meanPixelHighLimit,
                                      localizedGrayValue,
                                      localizedMeanGray);
            cameraContainer.addSubLimit(meanPixelGray);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY, meanPixelGray);
            
            Limit stdDev = new Limit(stdDevLowLimit,
                               stdDevHighLimit,
                               localizedGrayValue,
                               localizedGrayStandardDeviation);
          cameraContainer.addSubLimit(stdDev);
          mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION, stdDev);
        }
    }
    
    /**
     * Method heavily leveraged from Eddie's CalGrayscale list setup since we are
     * interested in the same list of cameras 
     */
    private void readCameraListFromFile() throws DatastoreException
    {
        //Create the Set for all cameras
        _cameras.clear();
        
        _cameras.add(AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_1));
        _cameras.add(AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_2));
    }
    
    /**
     * @author Cheah Lee Herng
     */
    void logInformation(boolean forceLogging) throws DatastoreException
    {
        StringBuffer logData = new StringBuffer();

        logData.append("Camera Id, Mean Pixel Value, Pixel Std Dev\n\n");

        logSpecificInfo(logData);        

        if (forceLogging)
        {
          _logger.log(logData.toString());
        }
        else
        {
          _logger.logIfTimedOut(logData.toString(), _logTimeoutMilliSeconds);
        }
    }
    
    /**
     * Wrap the common logging infrastructure
     * 
     * @author Cheah Lee Herng 
     */
    private void logSpecificInfo(StringBuffer logData)
    {
        Assert.expect(logData != null);
       
        if ((_cameraToTestTypeToResultMap != null) && (_cameraToTestTypeToResultMap.isEmpty() == false))
        {
            //Get the line separator character only once
            String lineSeparator = System.getProperty("line.separator");
            
            for(AbstractOpticalCamera opticalCamera : _camerasToGetImagesFromSet)
            {
                //Get the sub-maps for this specific camera containing the results and limits
                Map<OpticalCameraImageDataTypeEnum, Result> mapOfTestsToResults = _cameraToTestTypeToResultMap.get(opticalCamera);
                Map<OpticalCameraImageDataTypeEnum, Limit> mapOfLimits = _cameraToTestTypeToLimitMap.get(opticalCamera);
                
                if (mapOfTestsToResults != null && !mapOfTestsToResults.isEmpty())
                {
                    logData.append(opticalCamera.getId() + ",");
                }
                
                //Log the pixel gray values
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.MEAN_PIXEL_GRAY).toString() +
                                   "),");
                }
                
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.PIXEL_GRAY_STANDARD_DEVIATION).toString() +
                                   "),");
                }
                
                //terminate the line with the appropriate character
                logData.append(lineSeparator);
            }
        }
    }
    
    /**
     * The execute method for the Optical Camera Image Confirmation tests. It allows sub-classes
     * to move the stage and control projectors before and after acquiring images. It then
     * analyzes those images to make sure their gray values are within the specified limits.
     * 
     * @author Cheah Lee Herng
     */
    protected void executeTask() throws XrayTesterException
    {
        boolean deferred = false;
        try
        {
            reportProgress(0);
            
            //Special case, if the camera list is empty, don't continue because lots of
            //downstream code doesn't like 0 length lists
            if (_camerasToGetImagesFromSet.isEmpty())
            {
              //Create a passing result
              String localizedTaskName = getName();
              _limits = new Limit(null, null, null, localizedTaskName);
              _result = new Result(new Boolean(true), localizedTaskName);
              return;
            }
            
            //Handle the fact that we cannot run optical light confirmations with a panel
            //in the system. Defer the task till later *but* don't cause the
            //task to fail. Display a meaningful message in the Service UI
            if (ableToRunWithCurrentPanelState() == false)
            {
              _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
              _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_PANEL_INTEFERRED_WITH_LIGHT_CONFIRM_KEY"));
              _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_REMOVE_THE_PANEL_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
              return;
            }
            
            reportProgress(10);
            
            setRailWidth();
            
            reportProgress(30);
            
            //Get images and analyze
            getAndAnalyzeImageMap();
            
            //Report that this task is done
            reportProgress(100);
        }
        catch (XrayTesterException ex)
        {
            _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
            deferred = true;
            return;
        }
        //Log any unexpected exceptions
        catch (Exception e)
        {
            Assert.logException(e);
        }
        finally
        {
            //Save gain factor into hardware.config
            //saveGainFactorToConfig();
            
            //For now force logging
            if(deferred == false)
              logInformation(_forceLogging);
            
            //Restore stage (if needed)
            moveStageAfterExecute();
            
            //Create the detailed failure info for observers
            if(deferred == false)
              populateResultMessage();
        }
    }
    
    /**
     * Set the message in the Result object. This info is available to any observer
     * who wants to display detailed failure information.
     *
     * @author Cheah Lee Herng
    */
    private void populateResultMessage()
    {
        //Clear the treeSet before starting
        _failingCameraSet.clear();

        if (getLimit().didTestPass(getResult()) == false)
        {
          //For each camera, we have to get each sub-task result
          for(AbstractOpticalCamera opticalCamera : _camerasToGetImagesFromSet)
          {
            Map<OpticalCameraImageDataTypeEnum, Result> specificCameraResultMap = _cameraToTestTypeToResultMap.get(opticalCamera);

            //Finally ready to get the actual results themselves, whew!
            for (Map.Entry<OpticalCameraImageDataTypeEnum, Result> resultKey : specificCameraResultMap.entrySet())
            {
              //Get the result for this direction->camera->sub-task
              Result resultForKey = resultKey.getValue();

              //If it failed, add the camera to the set. Sets ignore duplicates
              //so it is OK to add the same camera over and over and ...
              if (resultForKey.getStatus().equals(HardwareTaskStatusEnum.PASSED) == false)
              {
                _failingCameraSet.add(opticalCamera);
              }
            }
          }

          //Still in the if(didPass == false), so we know the task failed somewhere
          //so create the string buffer to display the failure info
          StringBuilder buffer = new StringBuilder();

          //Add the localized 'Optical Camera' string
          buffer.append(StringLocalizer.keyToString("ACD_OPTICAL_CAMERAS_PLURAL_KEY") + " ");

          //Add the set of cameras that failed. We don't care what the specific failure
          //was, at least for now
          for (AbstractOpticalCamera camera : _failingCameraSet)
          {
            //Append the camera number to the list of failures
            buffer.append(String.valueOf(camera.getId()) + ",");
          }

          //Get rid of the last comma in the camera list
          buffer.deleteCharAt(buffer.length() - 1);

          //Add the localized FAILED string
          buffer.append(" " + StringLocalizer.keyToString("CD_FAILED_KEY"));

          //Add the suggestect action localized key
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_OPTICAL_CAMERA_IMAGE_QUALITY_FAILED_SUGGESTED_ACTION_KEY"));

          //Set the message in the result passed to observers
          _result.setTaskStatusMessage(buffer.toString());
        } //end of if(getLimit()....
    }
    
    /**
     * Access to the set of failing cameras. Currently needed by Diagnostics so it
     * can 'see' into failure details that the Confirmation itself doesn't worry
     * about.
     *
     * @author Cheah Lee Herng
    */
    public Set<AbstractOpticalCamera> getSetOfFailingCameras()
    {
        return _failingCameraSet;
    }
    
    /**
     * Method for knowing if the task can run with the current panel state. For light
     * image tasks, this should return false if a panel is loaded and true if no
     * panel is loaded. This method is overridden for Dark tasks to always return
     * true. 
     */
    protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
    {
        // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
        //  add throws XrayTesterException
        //flip the logic of isPanelLoaded because light tasks can't run with a panel
        return (_panelHandler.isPanelLoaded() == false);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void setUp() throws XrayTesterException
    {
        //Save off the active motion profile, just in case we end up moving the
        //stage and setting the profile to a point-to-point profile
        _panelPositionerSavedProfile = _panelPositioner.getActiveMotionProfile();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean canRunOnThisHardware()
    {
        return true;
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void cleanUp() throws XrayTesterException
    {
        //restore the saved profile before continuing
        _panelPositioner.setMotionProfile(_panelPositionerSavedProfile);
        
        //clear maps
        if (_failingCameraSet != null)
            _failingCameraSet.clear();
        if (_cameras != null)
            _cameras.clear();
        if (_camerasToGetImagesFromSet != null)
            _camerasToGetImagesFromSet.clear();
        if (_cameraToStagePositionMap != null)
            _cameraToStagePositionMap.clear();
        if (_cameraToGainFactor != null)
            _cameraToGainFactor.clear();
    }
    
    /**
     * Method that controls stage motion after task execution.
     *
     * @author Cheah Lee Herng
    */
    protected void moveStageAfterExecute()
    {
        //Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected String getOpticalCameraImageFullPath(int cameraId, int cameraDeviceId, int sequenceNumber) throws XrayTesterException
    {
        return FileName.getOpticalCameraLightImageFullPath(cameraId, cameraDeviceId, sequenceNumber);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected String getOpticalCameraConfirmationLogDir()
    {
        return Directory.getOpticalCameraLightConfirmationLogDir();
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected String getOpticalCamerConfirmationLogImageDir(int cameraId)
    {
        return Directory.getOpticalCameraLightConfirmationLogImageDir(cameraId);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void setupGrayCardStagePosition()
    {
        Assert.expect(_cameraToStagePositionMap != null);
        
        // Gray Card Stage Position 1
        StagePositionMutable grayCardCamera1StagePosition = new StagePositionMutable();
        grayCardCamera1StagePosition.setXInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_X_NANOMETERS));
        grayCardCamera1StagePosition.setYInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_1_STAGE_POSITION_Y_NANOMETERS));
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_1, grayCardCamera1StagePosition);
        
        // Gray Card Stage Position 2
        StagePositionMutable grayCardCamera2StagePosition = new StagePositionMutable();
        grayCardCamera2StagePosition.setXInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_X_NANOMETERS));
        grayCardCamera2StagePosition.setYInNanometers(_config.getIntValue(PspCalibEnum.CONFIRM_OPTICAL_CAMERA_GRAY_CARD_CAMERA_2_STAGE_POSITION_Y_NANOMETERS));
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_2, grayCardCamera2StagePosition);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void saveGainFactorToConfig() throws XrayTesterException
    {
        Assert.expect(_cameraToGainFactor != null);
        
        for(Map.Entry<AbstractOpticalCamera, DoubleRef> entry : _cameraToGainFactor.entrySet())
        {
            AbstractOpticalCamera opticalCamera = entry.getKey();
            DoubleRef gainFactor = entry.getValue();
 
            OpticalCameraIdEnum cameraIdEnum = opticalCamera.getOpticalCameraIdEnum();
            
            if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
                _config.setValue(PspCalibEnum.OPTICAL_CAMERA_1_GAIN_FACTOR, gainFactor.getValue());
            else if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
                _config.setValue(PspCalibEnum.OPTICAL_CAMERA_2_GAIN_FACTOR, gainFactor.getValue());
            else
                Assert.expect(false, "Invalid optical camera Id");
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void acquireOpticalCameraImages(AbstractOpticalCamera opticalCamera, StagePosition stagePosition) throws XrayTesterException
    {
        Assert.expect(opticalCamera != null);
        Assert.expect(stagePosition != null);
               
        // Generate stage position name
        String stagePositionName = stagePosition.getXInNanometers() + "_" + stagePosition.getYInNanometers();
        String imageFullPath = getOpticalCameraImageFullPath(opticalCamera.getId(), opticalCamera.getOpticalCameraIdEnum().getDeviceId(), 1);
       
        // Consolidate info into OpticalPointToPointScan object
        OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
        opticalPointToPointScan.setOpticalCameraIdEnum(opticalCamera.getOpticalCameraIdEnum());
        opticalPointToPointScan.setRegionPositionName(stagePositionName);
        opticalPointToPointScan.setPointPositionName(imageFullPath);
        opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(stagePosition);
        opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
        opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());
        
        // Perform actual image acquisition
        PspEngine.getInstance().performSingleImageOperation(opticalPointToPointScan, OpticalShiftNameEnum.WHITE_LIGHT);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void setRailWidth() throws XrayTesterException
    {
        ExecuteParallelThreadTasks<Object> panelHandlerParallelTasks = new ExecuteParallelThreadTasks<Object>();
        
        ThreadTask<Object> task1 = new SetRailWidthThreadTask(GRAY_CARD_ADJUSTABLE_RAIL_WIDTH_IN_NANOMETERS);
        panelHandlerParallelTasks.submitThreadTask(task1);
        
        try
        {
          panelHandlerParallelTasks.waitForTasksToComplete();
        }
        catch (XrayTesterException xte)
        {              
          throw xte;
        }
        catch (Exception e)
        {  
          Assert.logException(e);
        }
    }
}
