package com.axi.v810.business.autoCal;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CalOpticalSystemFiducial extends EventDrivenHardwareTask implements OpticalLiveVideoInteractionInterface 
{    
    private static CalOpticalSystemFiducial _instance;
    private static Config _config = Config.getInstance();
    
    private PanelHandler _panelHandler = PanelHandler.getInstance();
    private XrayActuatorInt _xrayActuator = XrayActuator.getInstance();
    
    //Motion profile to use when moving stage, use the fastest
    protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
    
    private static OpticalLiveVideoObservable _opticalLiveVideoObservable = OpticalLiveVideoObservable.getInstance();
    
    private PanelPositioner _panelPositioner = PanelPositioner.getInstance();
    private MotionProfile _panelPositionerSavedProfile;
    
    // Printable name for this procedure
    private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_OPTICAL_SYSTEM_FIDUCIAL_NAME_KEY");
    
    // This cal is called on the first event after the timeout occurs.
    private static final int _EVENT_FREQUENCY = 1;
    private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.OPTICAL_SYSTEM_FIDUCIAL_EXECUTE_EVERY_TIMEOUT_SECONDS);
    
    //Log utility members
    private FileLoggerAxi _logger;
    private int _maxLogFileSizeInBytes = 1028;
    
    protected Map<OpticalCameraIdEnum, StagePositionMutable> _cameraToStagePositionMap = new LinkedHashMap<OpticalCameraIdEnum, StagePositionMutable>();
    private BooleanLock  _waitingForFirstOpticalCameraLocationInput = new BooleanLock(false);
    private BooleanLock _waitForHardware = new BooleanLock(false);
    private static PspSettingEnum _settingEnum = PspSettingEnum.getEnum(Config.getInstance().getIntValue(HardwareConfigEnum.PSP_SETTING)); 
    
    /**
     * @author Cheah Lee Herng
    */
    private CalOpticalSystemFiducial()
    {
        super(_NAME_OF_CAL,
              _EVENT_FREQUENCY,
              _EXECUTE_EVERY_TIMEOUT_SECONDS,
              ProgressReporterEnum.OPTICAL_SYSTEM_FIDUCIAL_ADJUSTMENT_TASK);
        
        //Create the logger
        _logger = new FileLoggerAxi(FileName.getOpticalSystemFiducialFullPath() +
                                    FileName.getLogFileExtension(),
                                    _maxLogFileSizeInBytes);

        _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

        setTimestampEnum(PspCalibEnum.CAL_SYSTEM_FIDUCIAL_LAST_TIME_RUN);
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public static synchronized CalOpticalSystemFiducial getInstance()
    {
        if (_instance == null)
        {
          _instance = new CalOpticalSystemFiducial();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void executeTask() throws XrayTesterException 
    {
        boolean deferred = false;
        try
        {
          reportProgress(10);
          
          // XCR-3314 Support PSP New Manual-Loaded Jig Plate
          if (PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_1) || 
              PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4))
          {
            loadOpticalCalibrationJig();
            //Handle the fact that we cannot run this task without a calibration
            //jig loaded into the system. Defer the task till later *but* don't cause the
            //task to fail. Display a meaningful message in the Service UI
            if (ableToRunWithCurrentPanelState() == false)
            {
              _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
              _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_NO_CALIBRATION_JIG_LOADED_KEY"));
              _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_LOAD_CALIBRATION_JIG_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
              return;
            }
          }
          else
          {
            // For On-Stage calibration jig, we should not have any panel loaded.
            if (ableToRunWithCurrentPanelState())
            {
              _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
              _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_PANEL_INTEFERRED_WITH_LIGHT_CONFIRM_KEY"));
              _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_REMOVE_THE_PANEL_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
              return;
            }
            
            PanelHandler.getInstance().setRailWidthInNanoMeters(PspConfiguration.getInstance().getOpticalSystemFiducialAdjustmentRailWidthInNanometers());
          }
          checkAbortInProgress();
          
          //Setup system fiducial stage position
          setupSystemFiducialStagePosition();
          reportProgress(20);

          if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH) || _settingEnum.equals(PspSettingEnum.SETTING_FRONT))
          {
            PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);
            
            reportProgress(30);

            // At least one optical camera must be installed to run system fiducial
            Assert.expect(OpticalCameraIdEnum.getAllEnums().size() > 0);

            // Move stage to first optical camera
            moveStageBeforeExecute(OpticalCameraIdEnum.OPTICAL_CAMERA_1);

            reportProgress(40);

            // Display Optical Live Video, starting with first optical camera Id
            _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO,
                                                  OpticalCameraIdEnum.OPTICAL_CAMERA_1,
                                                  StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"),
                                                  this,
                                                  false,
                                                  true,
                                                  false));

            // Wait for user to record current stage position
            waitForFirstOpticalCameraSystemFiducialInput();

          // Here we check if user cancel live video so that
            // we can exit the test gracefully
            checkAbortInProgress();

            if (_taskCancelled == false)
            {
              // Record current stage position for first camera
              recordCurrentStagePosition(OpticalCameraIdEnum.OPTICAL_CAMERA_1);
            }

            // Send event to close live video dialog
            _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));
            
            // We need to make sure PSP Hardware is taken care before start acquiring optical images      
            waitForHardware();

            // Off LED when we do not use it anymore
            PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);
          }

          if (_settingEnum.equals(PspSettingEnum.SETTING_BOTH) || _settingEnum.equals(PspSettingEnum.SETTING_REAR))
          {
            if (_taskCancelled == false)
            {
              PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspModuleEnum.REAR, OpticalShiftNameEnum.WHITE_LIGHT);;

              checkAbortInProgress();

              // Move stage to next optical camera
              moveStageBeforeExecute(OpticalCameraIdEnum.OPTICAL_CAMERA_2);

              checkAbortInProgress();

              // Display Optical Live Video, starting with first optical camera Id
              _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO,
                                                    OpticalCameraIdEnum.OPTICAL_CAMERA_2,
                                                    StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"),
                                                    this,
                                                    false,
                                                    true,
                                                    false));

              // Wait for user to record current stage position
              waitForFirstOpticalCameraSystemFiducialInput();

                // Here we check if user cancel live video so that
              // we can exit the test gracefully
              checkAbortInProgress();

              if (_taskCancelled == false)
              {
                // Record current stage position for next optical camera
                recordCurrentStagePosition(OpticalCameraIdEnum.OPTICAL_CAMERA_2);
              }

              // Send event to close live video dialog
              _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));
              
              // We need to make sure PSP Hardware is taken care before start acquiring optical images      
              waitForHardware();

              // Shut down projector
              PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.REAR, OpticalShiftNameEnum.WHITE_LIGHT);
            }
          }
          
          //If we can reach here means the task passed
          _result = new Result(new Boolean(true), _NAME_OF_CAL);

          //Log the fact that the task passed. The _name value is already localized
          _logger.append(_NAME_OF_CAL + " - " + StringLocalizer.keyToString("CD_PASSED_KEY"));

        reportProgress(100);            
      }
      catch (XrayTesterException xte)
      {
        reportProgress(100);
        _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);
        _result.setTaskStatusMessage(xte.getLocalizedMessageWithoutHeaderAndFooter());
        deferred = true;
        // The re-throw is needed to help the "catch (Exception e)" below.
        throw xte;
      }
      catch (Throwable t)
      {
        Assert.logException(t);
      }
      finally
      {          
        if (_taskCancelled == false)
        {
          //Save record stage position into hardware.calib
          saveCurrentStagePositionToConfig();
        }

        // XCR-3314 Support PSP New Manual-Loaded Jig Plate
        if (PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_1) ||
            PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4))
        {
          // Restore panel safety checking anyhow
          _panelHandler.unloadPanel();
        }

        //Restore stage (if needed)
        moveStageAfterExecute();    
      }
    }

    /**
     * @author Cheah Lee Herng 
     */
    protected void setUp() throws XrayTesterException 
    {
        //Save off the active motion profile, just in case we end up moving the
        //stage and setting the profile to a point-to-point profile
        _panelPositionerSavedProfile = _panelPositioner.getActiveMotionProfile();
    }

    /**
     * @author Cheah Lee Herng 
     */
    public boolean canRunOnThisHardware() 
    {
        return true;
    }

    /**
     * @author Cheah Lee Herng 
     */
    protected void createLimitsObject() throws DatastoreException 
    {
        _limits = new Limit(null, null, null, null);
    }

    /**
     * @author Cheah Lee Herng 
     */
    protected void cleanUp() throws XrayTesterException 
    {
        //restore the saved profile before continuing
        _panelPositioner.setMotionProfile(_panelPositionerSavedProfile);
        
        if (_cameraToStagePositionMap != null)
            _cameraToStagePositionMap.clear();
    }

    /**
     * @author Cheah Lee Herng 
     */
    protected void createResultsObject() throws DatastoreException 
    {
        _result = new Result(new Boolean(false), _NAME_OF_CAL);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
    {
        // We need to have calibration jig loaded so that we 
        // can run this task.
        return (_panelHandler.isPanelLoaded() == true);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void waitForFirstOpticalCameraSystemFiducialInput()
    {
        _waitingForFirstOpticalCameraLocationInput.setValue(true);
        try
        {
          _waitingForFirstOpticalCameraLocationInput.waitUntilFalse();
        }
        catch (InterruptedException ex)
        {
          // Do nothing ...
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void waitForHardware()
    {
      _waitForHardware.setValue(true);
      try
      {
        _waitForHardware.waitUntilFalse();
      }
      catch (InterruptedException ex)
      {
        // Do nothing ...
      }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void runNextOpticalCamera()
    {
        _waitingForFirstOpticalCameraLocationInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void cancelLiveVideo()
    {
        _taskCancelled = true;
        _waitingForFirstOpticalCameraLocationInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void setSurfaceMapPoint(List<Point2D> points)
    {
      // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void setupSystemFiducialStagePosition()
    {
        Assert.expect(_cameraToStagePositionMap != null);
        
        // Clear list before populate the latest value
        _cameraToStagePositionMap.clear();
        
        // System Fiducial Stage Position Optical Camera 1
        StagePositionMutable systemFiducialCamera1StagePosition = new StagePositionMutable();
        systemFiducialCamera1StagePosition.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers());
        systemFiducialCamera1StagePosition.setYInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers());
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_1, systemFiducialCamera1StagePosition);
        
        // System Fiducial Stage Position Optical Camera 2
        StagePositionMutable systemFiducialCamera2StagePosition = new StagePositionMutable();
        systemFiducialCamera2StagePosition.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers());
        systemFiducialCamera2StagePosition.setYInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers());
        _cameraToStagePositionMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_2, systemFiducialCamera2StagePosition);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void moveStageBeforeExecute(OpticalCameraIdEnum opticalCameraIdEnum) throws XrayTesterException
    {
        Assert.expect(opticalCameraIdEnum != null);
        
        // panel needs to be repositioned left
        if (_panelHandler.isPanelLoaded())
          _panelHandler.movePanelToLeftSide();
        
        //Set up the stage to get it out of the way of the cameras
        StagePositionMutable stagePosition = _cameraToStagePositionMap.get(opticalCameraIdEnum);
        
        //Profile1 is fastest
        PanelPositioner.getInstance().setMotionProfile(new MotionProfile(_motionProfile));
        
        // Move stage to existing center position before performing CD&A
        PanelPositioner.getInstance().pointToPointMoveAllAxes(stagePosition);
    }
    
    /**
     * Method that controls stage motion after task execution.
     *
     * @author Cheah Lee Herng
    */
    protected void moveStageAfterExecute()
    {
        //Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void recordCurrentStagePosition(OpticalCameraIdEnum cameraId) throws XrayTesterException
    {
        Assert.expect(_cameraToStagePositionMap != null);
        Assert.expect(cameraId != null);
        
        // Get current stage position
        int xPositionInNanometers = PanelPositioner.getInstance().getXaxisActualPositionInNanometers();
        int yPositionInNanometers = PanelPositioner.getInstance().getYaxisActualPositionInNanometers();
        
        StagePositionMutable stagePosition = _cameraToStagePositionMap.get(cameraId);
        Assert.expect(stagePosition != null);
        
        stagePosition.setXInNanometers(xPositionInNanometers);
        stagePosition.setYInNanometers(yPositionInNanometers);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void saveCurrentStagePositionToConfig() throws XrayTesterException
    {
        Assert.expect(_cameraToStagePositionMap != null);
        
        for(Map.Entry<OpticalCameraIdEnum, StagePositionMutable> entry : _cameraToStagePositionMap.entrySet())
        {
            OpticalCameraIdEnum cameraIdEnum = entry.getKey();
            StagePositionMutable stagePosition = entry.getValue();
            
            if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
            {
                _config.setValue(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXCalibEnum(), stagePosition.getXInNanometers());
                _config.setValue(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYCalibEnum(), stagePosition.getYInNanometers()); 
                
                System.out.println("CalOpticalSystemFiducial: FRONT: (" + stagePosition.getXInNanometers() + "," + stagePosition.getYInNanometers() + ")");
            }
            else if (cameraIdEnum.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
            {
                _config.setValue(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXCalibEnum(), stagePosition.getXInNanometers());
                _config.setValue(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYCalibEnum(), stagePosition.getYInNanometers()); 
                
                System.out.println("CalOpticalSystemFiducial: REAR: (" + stagePosition.getXInNanometers() + "," + stagePosition.getYInNanometers() + ")");
            }
            else
                Assert.expect(false, "Invalid optical camera Id");
        }
    }
    
    /*
     * @author Cheah Lee Herng
     */
    protected void checkAbortInProgress()
    {
        if (_taskCancelled == true)
        {
            reportProgress(100);
            _result.setStatus(HardwareTaskStatusEnum.CANCELED);
            return;
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void loadOpticalCalibrationJig() throws XrayTesterException
    {
        if (_panelHandler.isPanelLoaded() == false)
        {
          if (XrayActuator.isInstalled())
          {            
            if (XrayCylinderActuator.isInstalled() == true)
            {
              // Always move the cylinder to UP position
              _xrayActuator.up(false);
            }
            else if (XrayCPMotorActuator.isInstalled() == true)
            {
              // Always move the Z Axis to Home and Safe position
              _xrayActuator.home(false,false);
            }   
          }
          // comment by Wei Chin
          // should not check the safety sensor (due to it don't lower down the xray tube)
          // Please be aware if it need to run this board in high mag! 
          _panelHandler.loadPanel("Panel for Service Gui", 
                                  PspConfiguration.getInstance().getOpticalCalibrationJigWidthInNanometers(),
                                  PspConfiguration.getInstance().getOpticalCalibrationJigHeightInNanometers(),
                                  false, 0);
        }
    }
    
    /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setHardwareShutdownComplete()
  {
    _waitForHardware.setValue(false);
  }
}
