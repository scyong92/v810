package com.axi.v810.business.autoCal;

import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Greg Esparza
 */
public class XrayCameraEnableGrayscaleToRunTask extends EventDrivenHardwareTask

{
  private static final String _TASK_NAME = "X-ray Camera Enable Grayscale To Run task";
  private static final int _RUN_EVERY_TIME_EVENT_OCCURS = 1;
  private static final int _RUN_IMMEDIATELY_AFTER_EVENT_OCCURS = 0;

  /**
   * @author Greg Esparza
   */
  public XrayCameraEnableGrayscaleToRunTask()
  {
    super(_TASK_NAME,
          _RUN_EVERY_TIME_EVENT_OCCURS,
          _RUN_IMMEDIATELY_AFTER_EVENT_OCCURS,
          ProgressReporterEnum.XRAY_CAMERA_ENABLE_GRAYSCALE_TO_RUN_TASK);
  }

  /**
   * @author Greg Esparza
   */
  public void setUp() throws XrayTesterException
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public void executeTask() throws XrayTesterException
  {
    CalGrayscale.getInstance().setTaskToRunASAP();
    _result = Result.createPassingResult(getName());
  }

  /**
   * @author Greg Esparza
   */
  public void createResultsObject()
  {
    _result = Result.createFailingResult(getName());
  }

  /**
   * @author Greg Esparza
   */
  public void cleanUp()throws XrayTesterException
  {
    // do nothing
  }

  /**
   * @author Greg Esparza
   */
  public void createLimitsObject()
  {
    _limits = Limit.createShouldPassLimit();
  }

  /**
   * @author Greg Esparza
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
