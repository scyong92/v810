package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.ImageAcquisitionEngine;
import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;


/**
 * Confirmation to test the camera calibration file values. It communicates with
 * all cameras to get their individual calibration 'files.' The calibration files
 * contain the Channel Gain and Offset for all four channels in that camera. They
 * also contain the Pixel Gain and Offset values for every pixel in the camera.
 * After getting these values from each camera, this task analyzes them and compares
 * them to the appropriate limits. It also saves the calibration 'file' values
 * into a file.
 *
 * @author Reid Hayhow
 */
public class ConfirmCameraCalibration extends EventDrivenHardwareTask
{
  // Printable name for this procedure
  private static final String NAME =
      StringLocalizer.keyToString("CD_CAMERA_CAL_FILE_CONFIRMATION_KEY");

  private static Config _config = Config.getInstance();

  // Call execute() every time this procedure is triggered
  private static final int FREQUENCY =
      _config.getIntValue(HardwareCalibEnum.CAMERA_ADJUSTMENT_CONFIRM_FREQUENCY);

  private static final long TIMEOUTINSECONDS =
      _config.getIntValue(SoftwareConfigEnum.CAMERA_ADJUSTMENT_CONFIRM_TIMEOUT_IN_SECONDS);

  private static final String _logDirectory = Directory.getCameraAdjustmentConfirmationLogDir();

  private static final int _logTimeoutMilliSeconds =
      _config.getIntValue(SoftwareConfigEnum.CAMERA_ADJUSTMENT_CONFIRM_LOGGING_TIMEOUT);

  private static final int _maxLogFilesInDirectory =
      _config.getIntValue(SoftwareConfigEnum.CAMERA_ADJUSTMENT_CONFIRM_MAX_LOG_FILES_IN_DIRECTORY);

  private static ConfirmCameraCalibration _instance = null;

  // Internal Enum to identify results & limits

  // object variables
  private DirectoryLoggerAxi _logger = null;
  private Map<AbstractXrayCamera, Map<CameraCalibrationDataTypeEnum, Result>> _cameraToLocalResultsMap = new HashMap<AbstractXrayCamera, Map<CameraCalibrationDataTypeEnum, Result>>();
  private Map<AbstractXrayCamera, Map<CameraCalibrationDataTypeEnum, Limit>> _cameraToLocalLimitsMap = new HashMap<AbstractXrayCamera, Map<CameraCalibrationDataTypeEnum, Limit>>();
  private XrayTesterException _exception = null;

  //Static values for adjusting camera indexing
  private static final int FIRST_CAMERA_INDEX = 0;
  private static final int ADJUSTED_FIRST_CAMERA_INDEX = 14;

  //Tree to store the set of uncalibrated cameras in for error reporting
  private TreeSet<AbstractXrayCamera> _setOfUnCalibratedCameras = new TreeSet<AbstractXrayCamera>();
  private List<XrayCameraCalibrationData> _cameraCalDataList = new ArrayList<XrayCameraCalibrationData>();

  /**
   * Constructor, creates a logger for this confirmation
   *
   * @author Reid Hayhow
   */
  private ConfirmCameraCalibration()
  {
    super(NAME,
          FREQUENCY,
          TIMEOUTINSECONDS,
          ProgressReporterEnum.CONFIRM_CAMERA_ADJUSTMENT);

    _logger = new DirectoryLoggerAxi(_logDirectory,
                                     FileName.getCameraAdjustmentConfirmationlLogFileBasename(),
                                     FileName.getLogFileExtension(),
                                     _maxLogFilesInDirectory);

    //This confirmation is automated
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    //Set the hardware calib enum for logging timestamp
    setTimestampEnum(HardwareCalibEnum.CAMERA_CALIBRATION_CONFIRMATION_LAST_TIME_RUN);
  }

  /**
   * @return Instance object of this singleton class
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCameraCalibration getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCameraCalibration();
    }
    return _instance;
  }

  /**
   * By default all tests can run on any hardware configuration.
   *
   * @return true if this test is capable of being run on the currently defined hardware.
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * clean-up code that may be reused (for example executeTask and abortTask may
   * both use the same cleanUp code)
   *
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
    if (_cameraToLocalResultsMap != null)
      _cameraToLocalResultsMap.clear();
    
    if (_cameraToLocalLimitsMap != null)
      _cameraToLocalLimitsMap.clear();
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }

  /**
   * Create the results object(s) for this test result. The Results for this task
   * are grouped first by camera number and then by the type of data seen in the
   * cal file. The values used here are somewhat meaningless. The real results
   * will be used if there are measured values and if the camera was not calibrated
   * a boolean is substituted for the value(s) here.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    //Get all of the localized strings we will need for the results
    //First, the the common building block keys
    String localizedCameraString = StringLocalizer.keyToString("ACD_CAMERA_KEY");
    String localizedForward = StringLocalizer.keyToString("ACD_FORWARD_DIRECTION_KEY");
    String localizedBackward = StringLocalizer.keyToString("ACD_BACKWARD_DIRECTION_KEY");

    //Then get the keys specific to this Confirmation
    String localizedSegmentGainMax = StringLocalizer.keyToString("CD_SEGMENT_GAIN_MAX_KEY");
    String localizedSegmentOffsetMax = StringLocalizer.keyToString("CD_SEGMENT_OFFSET_MAX_KEY");
    String localizedPixelGainMax = StringLocalizer.keyToString("CD_PIXEL_GAIN_MAX_KEY");
    String localizedPixelOffsetMax = StringLocalizer.keyToString("CD_PIXEL_OFFSET_MAX_KEY");

    String localizedSegmentGainMin = StringLocalizer.keyToString("CD_SEGMENT_GAIN_MIN_KEY");
    String localizedSegmentOffsetMin = StringLocalizer.keyToString("CD_SEGMENT_OFFSET_MIN_KEY");
    String localizedPixelGainMin = StringLocalizer.keyToString("CD_PIXEL_GAIN_MIN_KEY");
    String localizedPixelOffsetMin = StringLocalizer.keyToString("CD_PIXEL_OFFSET_MIN_KEY");

    //Create the map to hold the map of results, indexed by camera
    _cameraToLocalResultsMap.clear();

    // Create container for the others
    _result = new Result(NAME);

    for (AbstractXrayCamera camera : XrayCameraArray.getCameras())
    {
      //Create the map of results, indexed by sub-test
      Map<CameraCalibrationDataTypeEnum, Result> subResultMap = new HashMap<CameraCalibrationDataTypeEnum, Result>();

      //Add the subResultMap for this camera to the local map
      _cameraToLocalResultsMap.put(camera, subResultMap);

      Result cameraResultsContainer = new Result(localizedCameraString + camera.getId());

      //First do the forward direction Max values
      Result segmentGainForwardMax = new Result(new Double(2.0),
                                                localizedSegmentGainMax +
                                                localizedForward);
      cameraResultsContainer.addSubResult(segmentGainForwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_FORWARD_MAX, segmentGainForwardMax);

      Result segmentOffsetForwardMax = new Result(new Double(150.0),
                                                  localizedSegmentOffsetMax +
                                                  localizedForward);
      cameraResultsContainer.addSubResult(segmentOffsetForwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_FORWARD_MAX, segmentOffsetForwardMax);

      Result pixelGainForwardMax = new Result(new Double(1.5),
                                              localizedPixelGainMax +
                                              localizedForward);
      cameraResultsContainer.addSubResult(pixelGainForwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_FORWARD_MAX, pixelGainForwardMax);

      Result pixelOffsetForwardMax = new Result(new Double( -8.0),
                                                localizedPixelOffsetMax +
                                                localizedForward);
      cameraResultsContainer.addSubResult(pixelOffsetForwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_FORWARD_MAX, pixelOffsetForwardMax);

      //Now the forward Min values
      Result segmentGainForwardMin = new Result(new Double(2.0),
                                                localizedSegmentGainMin +
                                                localizedForward);
      cameraResultsContainer.addSubResult(segmentGainForwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_FORWARD_MIN, segmentGainForwardMin);

      Result segmentOffsetForwardMin = new Result(new Double(150.0),
                                                  localizedSegmentOffsetMin +
                                                  localizedForward);
      cameraResultsContainer.addSubResult(segmentOffsetForwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_FORWARD_MIN, segmentOffsetForwardMin);

      Result pixelGainForwardMin = new Result(new Double(1.5),
                                              localizedPixelGainMin +
                                              localizedForward);
      cameraResultsContainer.addSubResult(pixelGainForwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_FORWARD_MIN, pixelGainForwardMin);

      Result pixelOffsetForwardMin = new Result(new Double( -8.0),
                                                localizedPixelOffsetMin +
                                                localizedForward);
      cameraResultsContainer.addSubResult(pixelOffsetForwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_FORWARD_MIN, pixelOffsetForwardMin);

      //Now the backward scan data, max first
      Result segmentGainBackwardMax = new Result(new Double(2.0),
                                                 localizedSegmentGainMax +
                                                 localizedBackward);
      cameraResultsContainer.addSubResult(segmentGainBackwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MAX, segmentGainBackwardMax);

      Result segmentOffsetBackwardMax = new Result(new Double(150.0),
          localizedSegmentOffsetMax +
          localizedBackward);
      cameraResultsContainer.addSubResult(segmentOffsetBackwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_REVERSE_MAX, segmentOffsetBackwardMax);

      Result pixelGainBackwardMax = new Result(new Double(1.5),
                                               localizedPixelGainMax +
                                               localizedBackward);
      cameraResultsContainer.addSubResult(pixelGainBackwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_REVERSE_MAX, pixelGainBackwardMax);

      Result pixelOffsetBackwardMax = new Result(new Double( -8.0),
                                                 localizedPixelOffsetMax +
                                                 localizedBackward);
      cameraResultsContainer.addSubResult(pixelOffsetBackwardMax);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_REVERSE_MAX, pixelOffsetBackwardMax);

      //Then the min data for the backward scan
      Result segmentGainBackwardMin = new Result(new Double(2.0),
                                                 localizedSegmentGainMin +
                                                 localizedBackward);
      cameraResultsContainer.addSubResult(segmentGainBackwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MIN, segmentGainBackwardMin);

      Result segmentOffsetBackwardMin = new Result(new Double(150.0),
          localizedSegmentOffsetMin +
          localizedBackward);
      cameraResultsContainer.addSubResult(segmentOffsetBackwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_REVERSE_MIN, segmentOffsetBackwardMin);

      Result pixelGainBackwardMin = new Result(new Double(1.5),
                                               localizedPixelGainMin +
                                               localizedBackward);
      cameraResultsContainer.addSubResult(pixelGainBackwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_REVERSE_MIN, pixelGainBackwardMin);

      Result pixelOffsetBackwardMin = new Result(new Double( -8.0),
                                                 localizedPixelOffsetMin +
                                                 localizedBackward);
      cameraResultsContainer.addSubResult(pixelOffsetBackwardMin);
      subResultMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_REVERSE_MIN, pixelOffsetBackwardMin);

      _result.addSubResult(cameraResultsContainer);
    }
  }


  /**
   * Function to read in and assign the various limits for this task. The limits
   * for this task are ordered by camera first, and then by the specific value that
   * is being measured
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject() throws DatastoreException
  {
    // Create container for the others
    _limits = new Limit(null, null, null, NAME);

    //Get all of the localized strings we will need for the limits
    //First, the the common building block keys
    String localizedCameraString = StringLocalizer.keyToString("ACD_CAMERA_KEY");
    String localizedVoltsOverVoltsString = StringLocalizer.keyToString("ACD_VOLTS_OVER_VOLTS_KEY");
    String localizedMiliVoltsString = StringLocalizer.keyToString("ACD_MILLI_VOLTS_KEY");
    String localizedForward = StringLocalizer.keyToString("ACD_FORWARD_DIRECTION_KEY");
    String localizedBackward = StringLocalizer.keyToString("ACD_BACKWARD_DIRECTION_KEY");

    //Then get the keys specific to this Confirmation
    String localizedSegmentGainMax = StringLocalizer.keyToString("CD_SEGMENT_GAIN_MAX_KEY");
    String localizedSegmentOffsetMax = StringLocalizer.keyToString("CD_SEGMENT_OFFSET_MAX_KEY");
    String localizedPixelGainMax = StringLocalizer.keyToString("CD_PIXEL_GAIN_MAX_KEY");
    String localizedPixelOffsetMax = StringLocalizer.keyToString("CD_PIXEL_OFFSET_MAX_KEY");

    String localizedSegmentGainMin = StringLocalizer.keyToString("CD_SEGMENT_GAIN_MIN_KEY");
    String localizedSegmentOffsetMin = StringLocalizer.keyToString("CD_SEGMENT_OFFSET_MIN_KEY");
    String localizedPixelGainMin = StringLocalizer.keyToString("CD_PIXEL_GAIN_MIN_KEY");
    String localizedPixelOffsetMin = StringLocalizer.keyToString("CD_PIXEL_OFFSET_MIN_KEY");

    //Create the local map to hold the map of camera Limits
    _cameraToLocalLimitsMap.clear();

    //Now create the limits for each camera.
    for (AbstractXrayCamera camera : XrayCameraArray.getCameras())
    {
	    //Swee Yee Wong - XCR-3361 Customer limit is not supported for new camera
      String boardMajorVersion = "";
      
      if(AxiTdiXrayCameraBoardVersionEnum.getEnum(camera.getCameraBoardVersion()).equals(AxiTdiXrayCameraBoardVersionEnum.BOARD_VERSION_A) == false)
        boardMajorVersion = "_CameraVersion" + AxiTdiXrayCameraBoardVersionEnum.getEnum(camera.getCameraBoardVersion()).toString();

      Double segmentGainLow = _parsedLimits.getLowLimitDouble("SegmentGain" + boardMajorVersion);
      Double segmentGainHigh = _parsedLimits.getHighLimitDouble("SegmentGain" + boardMajorVersion);
      Double segmentOffsetLow = _parsedLimits.getLowLimitDouble("SegmentOffset" + boardMajorVersion);
      Double segmentOffsetHigh = _parsedLimits.getHighLimitDouble("SegmentOffset" + boardMajorVersion);
      Double pixelGainLow = _parsedLimits.getLowLimitDouble("PixelGain" + boardMajorVersion);
      Double pixelGainHigh = _parsedLimits.getHighLimitDouble("PixelGain" + boardMajorVersion);
      Double pixelOffsetLow = _parsedLimits.getLowLimitDouble("PixelOffset" + boardMajorVersion);
      Double pixelOffsetHigh = _parsedLimits.getHighLimitDouble("PixelOffset" + boardMajorVersion);
    
      //Create the map of limits for each sub-test, indexed by sub-test number
      Map<CameraCalibrationDataTypeEnum, Limit> subLimitMap = new HashMap<CameraCalibrationDataTypeEnum, Limit>();

      //Add the map of sub-test limits for this camera to the map of all limits
      _cameraToLocalLimitsMap.put(camera, subLimitMap);

      //Create the result container for this camera
      //It should have no limits, no units, but should have a name
      Limit cameraResultsContainer = new Limit(null, null, null,
                                               localizedCameraString + camera.getId());

      //Create the forward Max limits for segment gain, segment offset
      //pixel gain and pixel offset, adding them to the limits list and
      //storing a reference that can be accessed by indexing for speed
      Limit segmentGainForwardMax = new Limit(segmentGainLow, segmentGainHigh,
                                              localizedVoltsOverVoltsString,
                                              localizedSegmentGainMax + localizedForward);
      cameraResultsContainer.addSubLimit(segmentGainForwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_FORWARD_MAX, segmentGainForwardMax);

      Limit segmentOffsetForwardMax = new Limit(segmentOffsetLow, segmentOffsetHigh,
                                                localizedMiliVoltsString,
                                                localizedSegmentOffsetMax + localizedForward);
      cameraResultsContainer.addSubLimit(segmentOffsetForwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_FORWARD_MAX, segmentOffsetForwardMax);

      Limit pixelGainForwardMax = new Limit(pixelGainLow, pixelGainHigh,
                                            localizedVoltsOverVoltsString,
                                            localizedPixelGainMax + localizedForward);
      cameraResultsContainer.addSubLimit(pixelGainForwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_FORWARD_MAX, pixelGainForwardMax);

      Limit pixelOffsetForwardMax = new Limit(pixelOffsetLow, pixelOffsetHigh,
                                              localizedMiliVoltsString,
                                              localizedPixelOffsetMax + localizedForward);
      cameraResultsContainer.addSubLimit(pixelOffsetForwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_FORWARD_MAX, pixelOffsetForwardMax);

      //Now create and store the forward Min limits
      Limit segmentGainForwardMin = new Limit(segmentGainLow, segmentGainHigh,
                                              localizedVoltsOverVoltsString,
                                              localizedSegmentGainMin + localizedForward);
      cameraResultsContainer.addSubLimit(segmentGainForwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_FORWARD_MIN, segmentGainForwardMin);

      Limit segmentOffsetForwardMin = new Limit(segmentOffsetLow, segmentOffsetHigh,
                                                localizedMiliVoltsString,
                                                localizedSegmentOffsetMin + localizedForward);
      cameraResultsContainer.addSubLimit(segmentOffsetForwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_FORWARD_MIN, segmentOffsetForwardMin);

      Limit pixelGainForwardMin = new Limit(pixelGainLow, pixelGainHigh,
                                            localizedVoltsOverVoltsString,
                                            localizedPixelGainMin + localizedForward);
      cameraResultsContainer.addSubLimit(pixelGainForwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_FORWARD_MIN, pixelGainForwardMin);

      Limit pixelOffsetForwardMin = new Limit(pixelOffsetLow, pixelOffsetHigh,
                                              localizedMiliVoltsString,
                                              localizedPixelOffsetMin + localizedForward);
      cameraResultsContainer.addSubLimit(pixelOffsetForwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_FORWARD_MIN, pixelOffsetForwardMin);

      //Backward limits, first the Max values
      Limit segmentGainBackwardMax = new Limit(segmentGainLow, segmentGainHigh,
                                               localizedVoltsOverVoltsString,
                                               localizedSegmentGainMax + localizedBackward);
      cameraResultsContainer.addSubLimit(segmentGainBackwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MAX, segmentGainBackwardMax);

      Limit segmentOffsetBackwardMax = new Limit(segmentOffsetLow, segmentOffsetHigh,
                                                 localizedMiliVoltsString,
                                                 localizedSegmentOffsetMax + localizedBackward);
      cameraResultsContainer.addSubLimit(segmentOffsetBackwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_REVERSE_MAX, segmentOffsetBackwardMax);

      Limit pixelGainBackwardMax = new Limit(pixelGainLow, pixelGainHigh,
                                             localizedVoltsOverVoltsString,
                                             localizedPixelGainMax + localizedBackward);
      cameraResultsContainer.addSubLimit(pixelGainBackwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_REVERSE_MAX, pixelGainBackwardMax);

      Limit pixelOffsetBackwardMax = new Limit(pixelOffsetLow, pixelOffsetHigh,
                                               localizedMiliVoltsString,
                                               localizedPixelOffsetMax + localizedBackward);
      cameraResultsContainer.addSubLimit(pixelOffsetBackwardMax);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_REVERSE_MAX, pixelOffsetBackwardMax);

      //Then the min limits for the backward scan direction
      Limit segmentGainBackwardMin = new Limit(segmentGainLow, segmentGainHigh,
                                               localizedVoltsOverVoltsString,
                                               localizedSegmentGainMin + localizedBackward);
      cameraResultsContainer.addSubLimit(segmentGainBackwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MIN, segmentGainBackwardMin);

      Limit segmentOffsetBackwardMin = new Limit(segmentOffsetLow, segmentOffsetHigh,
                                                 localizedMiliVoltsString,
                                                 localizedSegmentOffsetMin + localizedBackward);
      cameraResultsContainer.addSubLimit(segmentOffsetBackwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_REVERSE_MIN, segmentOffsetBackwardMin);

      Limit pixelGainBackwardMin = new Limit(pixelGainLow, pixelGainHigh,
                                             localizedVoltsOverVoltsString,
                                             localizedPixelGainMin + localizedBackward);
      cameraResultsContainer.addSubLimit(pixelGainBackwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_GAIN_REVERSE_MIN, pixelGainBackwardMin);

      Limit pixelOffsetBackwardMin = new Limit(pixelOffsetLow, pixelOffsetHigh,
                                               localizedMiliVoltsString,
                                               localizedPixelOffsetMin + localizedBackward);
      cameraResultsContainer.addSubLimit(pixelOffsetBackwardMin);
      subLimitMap.put(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_REVERSE_MIN, pixelOffsetBackwardMin);

      _limits.addSubLimit(cameraResultsContainer);
    }
  }

  /**
   * Execute the Confirmation. This function gets the calibration table from each
   * camera and passes it to an analysis function.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
	//Swee Yee Wong - Camera debugging purpose
    debug("ConfirmCameraCalibration");
    //reset the set of uncalibrated cameras since this may have changed
    _setOfUnCalibratedCameras.clear();
    _cameraCalDataList.clear();

    // Report that this task is starting
    int percentDone = 0;
    reportProgress(percentDone);

    // Save the number of cameras off for use reporting progress
    int numberOfCameras = XrayCameraArray.getCameras().size();

    for (AbstractXrayCamera camera : XrayCameraArray.getCameras())
    {
      //Create a list of calibration data to be used to save the data in parallel
      _cameraCalDataList.add(camera.getCalibrationData());

      //analyze the cal data from each camera
      analyzeCalTable(camera);

      // As we check each camera, update the progress observable based on the percent complete
      percentDone += 100/numberOfCameras;
      reportProgress(percentDone);
    }

    //Do the loging
    logCalibrationData();

    //Create the detailed message on any failures
    populateFailureInformation();

    // Report that we are done with this task
    reportProgress(100);
  }

  /**
   * Method to create detailed failure information that will be available
   * to observers (in the Result object)
   *
   * @author Reid Hayhow
   */
  private void populateFailureInformation() throws DatastoreException
  {
    //If the task didn't fail, do nothing, otherwise...
    if (this.getLimit().didTestPass(this.getResult()) == false)
    {
      // Use a string builder to create a single, detailed failure file
      StringBuilder failureDetails = new StringBuilder();

      // Add the header for the failure file
      failureDetails.append("Camera Details,Limit Name,Direction,High/Low Limit,Actual Value" + System.getProperty("line.separator"));

      //If the task failed, we need to figure out which camera failed
      TreeSet<AbstractXrayCamera> failingCameraSet = new TreeSet<AbstractXrayCamera>(new AlphaNumericComparator());

      //Get the set of cameras (should be all 14)
      Set<AbstractXrayCamera> cameraSet = _cameraToLocalResultsMap.keySet();

      //For each camera in the high level map, we need to look at the sub-test
      //results to see which one failed (if any)
      for (AbstractXrayCamera camera : cameraSet)
      {
        //Get the map for the sub-test results and limits
        Map<CameraCalibrationDataTypeEnum, Result> cameraResultsMap = _cameraToLocalResultsMap.get(camera);
        Map<CameraCalibrationDataTypeEnum, Limit> cameraLimitsMap = _cameraToLocalLimitsMap.get(camera);

        //Get the set of keys for this map of sub-test results
        Set<CameraCalibrationDataTypeEnum> setOfSubTestKeys = cameraResultsMap.keySet();

        //Now iterate over the set of keys to get the actual results, finally!
        for (CameraCalibrationDataTypeEnum subTestKey : setOfSubTestKeys)
        {
          //Get the result and limit
          Result result = cameraResultsMap.get(subTestKey);
          Limit limit = cameraLimitsMap.get(subTestKey);

          //If the sub-test failed, add the camera to the set of failing cameras
          //Sets are tolerant of duplicates, so don't worry about adding a camera
          //more than once
          if (result.getStatus().equals(HardwareTaskStatusEnum.PASSED) == false)
          {
            failingCameraSet.add(camera);

            // Handle it differently if the failure is because the camera was not calibrated.
            // The boolean result/limit is not very helpful, so replace it with a good message
            if(_setOfUnCalibratedCameras.contains(camera))
            {
              failureDetails.append(StringLocalizer.keyToString("ACD_CAMERA_KEY") + " " + camera.getId() + " ,,," +
                                    StringLocalizer.keyToString("CD_NOT_CALIBRATED_KEY") +
                                    System.getProperty("line.separator"));
            }
            else
            {
              // Add detailed failure information to the log file based on information from
              // the limit and result
              failureDetails.append(StringLocalizer.keyToString("ACD_CAMERA_KEY") + " " + camera.getId() + " " +
                                    StringLocalizer.keyToString("CD_FAILED_KEY") + "," +
                                    limit.getLimitName() + "," +
                                    limit.toString() + "," +
                                    result.getResultString() + System.getProperty("line.separator"));
            }
          }
        }
      }
      //Still in the if(didPass == false), so we know the task failed somewhere
      //so create the string buffer to display the failure info
      StringBuilder buffer = new StringBuilder();

      //display errors due to uncalibrated cameras first
      if (_setOfUnCalibratedCameras.size() > 0)
      {
        //Add the localized 'Camera' string
        buffer.append(StringLocalizer.keyToString("ACD_CAMERAS_PLURAL_KEY") + " ");

        //Add the set of cameras that were not calibrated
        for (AbstractXrayCamera camera : _setOfUnCalibratedCameras)
        {
          //Write the camera number to the buffer
          buffer.append(camera.getId() + ",");
        }

        //Get rid of the last comma in the camera list
        buffer.deleteCharAt(buffer.length() - 1);

        //Add the localized status message string
        buffer.append(" " + StringLocalizer.keyToString("CD_CAMERA_NOT_CALIBRATED_KEY"));

        //Add the suggested action localized key
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_NOT_CALIBRATED_SUGGESTED_ACTION_KEY"));

        //Remove any duplicates from the set of failing cameras. NOTE there may
        //be no duplicated, so I cannot assert/check on this call
        failingCameraSet.removeAll(_setOfUnCalibratedCameras);
      }

      //It is possible that the failures were only from uncalibrated cameras, so protect against
      //that possiblilty
      if (failingCameraSet.size() > 0)
      {
        //Add the localized 'Camera' string
        buffer.append(StringLocalizer.keyToString("ACD_CAMERAS_PLURAL_KEY") + " ");

        //Add the set of cameras that failed. We don't care what the specific failure
        //was, at least for now
        for (AbstractXrayCamera camera : failingCameraSet)
        {
          //Write the camera number to the buffer
          buffer.append(String.valueOf(camera.getId()) + ",");
        }

        //Get rid of the last comma in the camera list
        buffer.deleteCharAt(buffer.length() - 1);

        //Add the localized FAILED string
        buffer.append(" " + StringLocalizer.keyToString("CD_CAMERA_CALIBRATION_OUT_OF_RANGE_KEY"));

        //Add the suggestect action localized key
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_CALIBRATION_OUT_OF_RANGE_SUGGESTED_ACTION_KEY"));
      }

      //Set the message in the result passed to observers
      _result.setTaskStatusMessage(buffer.toString());
      _logger.log(buffer.toString());
      _logger.log(failureDetails.toString());
    }
  }

  /**
   * This function is the guts of this task. It calculates the min and max values
   * from each cal file, assigns them to the correct result object and stores a
   * local reference into the list that can be indexed by camera and data value
   *
   * @author Reid Hayhow
   *
   */
  private void analyzeCalTable(AbstractXrayCamera camera) throws XrayTesterException
  {
    Assert.expect(camera != null);

    // Handle the case where the camera is not calibrated
    if(camera.isCalibrated() == false)
    {
      setResultsAndLimitsForNonCalibratedCameras(camera);

      // Finally add this camera to the set of uncalibrated cameras for error reporting
      _setOfUnCalibratedCameras.add(camera);
    }
    else
    {
      XrayCameraCalibrationData calData = camera.getCalibrationData();

      Assert.expect(calData != null);

      //First, check to make sure the camera values are from calibrated cameras
      //If they are not, create a failing result for that specific uncalibrated
      //value.
      //NOTE:For now this code does an early return from these failures because
      //it is enough to know that the camera is not calibrated. There is no need
      //to know which part is not calibrated.

      //Get the max and min and store them
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MAX).setResults(calData.getMaximumSegmentReverseGain());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MIN).setResults(calData.getMinimumSegmentReverseGain());

      //Get the array of Channel Offsets, find the max and min and store them
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_REVERSE_MAX).setResults(calData.getMaximumSegmentReverseOffset());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_REVERSE_MIN).setResults(calData.getMinimumSegmentReverseOffset());

      //Get the array of pixel gain values, find the min and max and store the results
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_GAIN_REVERSE_MAX).setResults(calData.getMaximumPixelReverseGain());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_GAIN_REVERSE_MIN).setResults(calData.getMinimumPixelReverseGain());

      //Get the array of pixel offset values, find max/min and store the results
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_REVERSE_MAX).setResults(calData.getMaximumPixelReverseOffset());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_REVERSE_MIN).setResults(calData.getMinimumPixelReverseOffset());

      //Start with the Channel gain values, get min/max and store them
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_FORWARD_MAX).setResults(calData.getMaximumSegmentForwardGain());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_FORWARD_MIN).setResults(calData.getMinimumSegmentForwardGain());

      //Then the channel offset, find min/max and store
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_FORWARD_MAX).setResults(calData.getMaximumSegmentForwardOffset());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_OFFSET_FORWARD_MIN).setResults(calData.getMinimumSegmentForwardOffset());

      //Pixel gain next, find min/max and store the results
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_GAIN_FORWARD_MAX).setResults(calData.getMaximumPixelForwardGain());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_GAIN_FORWARD_MIN).setResults(calData.getMinimumPixelForwardGain());

      //Finally get the pixel offsets, find min/max and store the results
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_FORWARD_MAX).setResults(calData.getMaximumPixelForwardOffset());
      _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.PIXEL_OFFSET_FORWARD_MIN).setResults(calData.getMinimumPixelForwardOffset());
    }
  }

  /**
   * Method to handle fixing the Limits and Result for an uncalibrated camera. It
   * Replaces the Doubles used for actual limits and results with a Boolean value
   * that more accurately reflects the nature of this failure
   *
   * @author Reid Hayhow
   */
  private void setResultsAndLimitsForNonCalibratedCameras(AbstractXrayCamera camera)
  {
    // And replace the matching result with a boolean that fails
    _cameraToLocalResultsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MAX).setResults(new Boolean(false));

    // Now we have to clean up the limits, so find the entry in the local map
    Limit limitToBeReplaced = _cameraToLocalLimitsMap.get(camera).get(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MAX);

    // Now start to find the limit in the _limits list
    // get the high level container list of camera limits first, there is one per camera
    List<Limit> listOfLimitsForAllCameras = _limits.getLimitList();

    // Each camera has a container limit that has a list of limits, get the list container
    // for this specific camera

    List<AbstractXrayCamera> cameraList = XrayCameraArray.getInstance().getCameras();
    Limit limitsForThisCamera = listOfLimitsForAllCameras.get(cameraList.indexOf(camera));

    //Now get the list that was contained by the container
    List<Limit> cameraLimitList = limitsForThisCamera.getLimitList();

    //Finally, look-up the limit to be replaced in the list from the camera container
    int index = cameraLimitList.indexOf(limitToBeReplaced);

    //Create the new Limit. Since the result will be a Boolean, the 'contract'
    //with didPass() requires that the Limit be nulled out
    Limit replacement = Limit.createShouldPassLimit();

    //If we don't find it, there is a serious problem...
    if(index <= 0)
    {
      Assert.expect(false);
    }
    //Otherwise we found the limit to replace, so replace it
    else
    {
      //First remove the old entry from the list
      cameraLimitList.remove(index);

      //Then replace it with the new entry
      cameraLimitList.add(index, replacement);
    }

    //Finally, replace the map entry so we correctly find it if we look later
    _cameraToLocalLimitsMap.get(camera).put(CameraCalibrationDataTypeEnum.SEGMENT_GAIN_REVERSE_MAX, replacement);
  }

  /**
   * Required function, empty here since there is no set-up for this task
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {    

    if(LicenseManager.isVariableMagnificationEnabled())
    {    
      if(XrayActuator.isInstalled())
      {
        if (XrayCPMotorActuator.isInstalled() == true)
        {
          //move x-rays Z Axis to up position for low mag task.
          XrayActuator.getInstance().up(true);
        }
      }
      //bypass unnecessary task when in passive mode
      if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
      {
        disable();
        return;
      }
      XrayCameraArray.getInstance().loadLowMagGrayScale();
      // re-calculate magnification 
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    }
  }

  /**
   * Logging function to output the calibration file data to the log directory.
   * The format is identical to Eddie's so that we can 'leverage' his spreadsheet.
   *
   * @author Reid Hayhow
   */
  private void logCalibrationData() throws XrayTesterException
  {
    StringBuilder logString = new StringBuilder();
    ExecuteParallelThreadTasks<Boolean> executor = new ExecuteParallelThreadTasks<Boolean>();

    if (_exception != null)
    {
      logString.append(NAME + "\n");
      logString.append(_exception.getLocalizedMessage() + "\n");
      _logger.log(logString.toString());
    }
    else
    {
      for(final XrayCameraCalibrationData calData : _cameraCalDataList)
      {
        CameraCalFileSaverThreadTask fileSaver = new CameraCalFileSaverThreadTask(calData, _logDirectory, _logTimeoutMilliSeconds);
        executor.submitThreadTask(fileSaver);
      }
      try
      {
        executor.waitForTasksToComplete();
      }
      catch(XrayTesterException xRayEx)
      {
        throw xRayEx;
      }
      catch(Exception ex)
      {
        XrayTesterException convertedEx = new HardwareTaskExecutionException(ConfirmationExceptionEnum.PROBLEM_SAVING_CAL_FILE_DATA);
        convertedEx.initCause(ex);
        convertedEx.fillInStackTrace();
        throw convertedEx;
      }// end of catch
    }//end of if(exception seen) {} else
  }//end of logCalibrationData() method
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
