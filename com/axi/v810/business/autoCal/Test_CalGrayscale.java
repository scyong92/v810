package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
public class Test_CalGrayscale extends UnitTest
{
  /**
   * @author Eddie Williamson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    CalGrayscale cal = CalGrayscale.getInstance();
    try
    {
      TestUtils.initConfigForRegressionTest();
      PanelPositioner panelPositioner = PanelPositioner.getInstance();
      panelPositioner.startup();
      cal.executeNonInteractively();
    }
    catch (XrayTesterException ex)
    {
      System.out.println("OOPS!!");
      ex.printStackTrace();
      Expect.expect(false);
    }
    finally
    {
      TestUtils.restoreConfigAfterRegressionTest();
    }
  }

  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CalGrayscale());
  }
}
