package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.datastore.FileName;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
class XrayCameraCalibThreadTask extends ThreadTask<Object>
{
  private XrayCameraCalibEnum _xrayCameraCalibEnum;
  private boolean _pass;
  private StringBuilder _failMessage = new StringBuilder("");
  private AbstractXrayCamera _camera;
  private AdjustmentsAndConfirmationsLogger _adjustAndConfirmFileLogger = AdjustmentsAndConfirmationsLogger.getInstance();
  private XrayTesterException _exception;

  /**
   * @author Eddie Williamson
   */
  public XrayCameraCalibThreadTask(AbstractXrayCamera camera, XrayCameraCalibEnum xrayCameraCalibEnum)
  {
    super("XrayCameraCalibThreadTask: " + camera.getId() + " : " + xrayCameraCalibEnum.getName());
    Assert.expect(camera != null);
    Assert.expect(xrayCameraCalibEnum != null);

    _xrayCameraCalibEnum = xrayCameraCalibEnum;
    _pass = false;
    _camera = camera;
  }


  /**
   * @return pass/fail status
   * @author Eddie Williamson
   */
  public boolean getPass()
  {
    return _pass;
  }


  /**
   * @return String describing reason for failure.
   * @author Eddie Williamson
   */
  public String getFailMessage()
  {
    return _failMessage.toString();
  }

  /**
   *
   * @author Eddie Williamson
   */
  protected Object executeTask() throws XrayTesterException
  {
    try
    {
      if (_xrayCameraCalibEnum == XrayCameraCalibEnum.DO_DARK_SEGMENT_CAL)
      {
        _camera.runSensorSegmentOffsetCalibration();
      }
      else if (_xrayCameraCalibEnum == XrayCameraCalibEnum.DO_LIGHT_SEGMENT_CAL)
      {
        _camera.runSensorSegmentGainCalibration();
      }
      else if (_xrayCameraCalibEnum == XrayCameraCalibEnum.DO_DARK_PIXEL_CAL)
      {
        _camera.runSensorPixelOffsetCalibration();
      }
      else if (_xrayCameraCalibEnum == XrayCameraCalibEnum.DO_LIGHT_PIXEL_CAL)
      {
        _camera.runSensorPixelGainCalibration();
      }//Variable Mag Anthony August 2011
      else if (_xrayCameraCalibEnum == XrayCameraCalibEnum.SAVE_VARIABLE_MAG_HIGH_CAL)
      {
        _camera.runSaveVariableMagCalibrationConfiguration(FileName.getAgilentXrayCameraHighMagnificationFileConfigFileName());
      }
      else if (_xrayCameraCalibEnum == XrayCameraCalibEnum.LOAD_VARIABLE_MAG_HIGH_CAL)
      {
        _camera.runLoadVariableMagCalibrationConfiguration(FileName.getAgilentXrayCameraHighMagnificationFileConfigFileName());
      }
      else if (_xrayCameraCalibEnum == XrayCameraCalibEnum.SAVE_VARIABLE_MAG_LOW_CAL)
      {
        _camera.runSaveVariableMagCalibrationConfiguration(FileName.getAgilentXrayCameraLowMagnificationFileConfigFileName());
      }
      else if (_xrayCameraCalibEnum == XrayCameraCalibEnum.LOAD_VARIABLE_MAG_LOW_CAL)
      {
        _camera.runLoadVariableMagCalibrationConfiguration(FileName.getAgilentXrayCameraLowMagnificationFileConfigFileName());
      }
      else
      {
        Assert.expect(false, "Unexpected XrayCameraCalibEnum");
      }

      _pass = true;
    }
    catch (XrayTesterException xrte)
    {
      _failMessage.append(xrte.getMessage());
      _pass = false;
      _adjustAndConfirmFileLogger.append("Camera " + _camera.getId() + ": " + _failMessage);
      _exception = xrte;
      // Save the exception. The caller will query the pass status for each camera thread and throw if necessary
    }

    // parallel thread execution code requires we return an Object. Any will do.
    return this;
  }


  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // do nothing
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    // do nothing
  }

  /**
   * Get method to retreive any exception encountered during execution
   *
   * @author Reid Hayhow
   */
  public XrayTesterException getException()
  {
    return _exception;
  }
}







