package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.util.math.*;

/**
 * @author Eddie Williamson
 */
public class CalMagnification extends EventDrivenHardwareTask
{
  private static CalMagnification _instance;
  private static Config _config = Config.getInstance();

  // Printable name for this procedure
  private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_MAGNIFICATION_NAME_KEY");

  // This cal is called on the first event after the timeout occurs.
  private static final int _EVENT_FREQUENCY = 1;
  private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.MAGNIFICATION_EXECUTE_EVERY_TIMEOUT_SECONDS);

  // Logging support
  // NOTE: This shares config parms with system fiducial since these must be run together
  private static final String _logDirectory = Directory.getMagnificationCalLogDir();
  private static final long _logTimeoutSeconds = _config.getIntValue(SoftwareConfigEnum.MAGNIFICATION_LOG_RESULTS_EVERY_TIMEOUT_SECONDS);
  private static final long _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000L;
  private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.MAGNIFICATION_MAX_LOG_FILES_IN_DIRECTORY);

  private ExecuteParallelThreadTasks<Object> _magCalParallelThread = new ExecuteParallelThreadTasks<Object>();

  //--------------------------------------------------------------------------------------
  // NOTE: inside this class magnification is always used as an integer even though it is
  //       stored as a float in config. This is to make sure we never get caught with
  //       round off error and it is a convenient representation of the value to use as
  //       part of the filename when saving images. The only time the floating point
  //       value is used is when getting or setting the config value. These two functions
  //       are used to convert back and forth between the two representations of mag.
  //--------------------------------------------------------------------------------------
  /**
   * @author Eddie Williamson
   */
  private static int doubleAsIntMag(double mag)
  {
    // 6.25 -> 6250
    return (int)Math.round(mag * 1000.0);
  }

  /**
   * @author Eddie Williamson
   */
  private static double intMagAsDouble(int mag)
  {
    // 6250 -> 6.25
    return (mag / 1000.0);
  }

  // Object variables
  // 0 degrees for horizontal profile to find vertical edge
  private final int _VERTICAL = 0;

  private DirectoryLoggerAxi _logger;
  private boolean _areWeLogging = false;


  // Nominal +/- 0.3, steps of 0.001
  private double _searchRange = 0.150; // 2013-07-19, Chnee Khang Wah, Magnification Optimization
  private double _startingMag = MagnificationEnum.getNominalMagnificationAtReferencePlane() - _searchRange;
  private double _stoppingMag = MagnificationEnum.getNominalMagnificationAtReferencePlane() + _searchRange;
  private double _incrementMag = .001; // Thunderbolt uses double now. NOTE: Must be integer number of increments from start to stop


  //---------------------------------------------------------------------------
  // These regions are around the system fiducial
  // This is roughly a 30x110 pixel region starting at -15,+10 pixels from SF
  //---------------------------------------------------------------------------
  private static int _regionToImageXMils = -10;
  private static int _regionToImageYMils = 6;
  private static int _regionToImageWidthMils = 20;
  private static int _regionToImageHeightMils = 70;
  // This region is just a little bit larger to enclose the region above. This allows enough
  // room so as magnification is varied the pixel shifts will still be in the projections.
  private static int _regionToEncloseImageXMils = _regionToImageXMils - 25;
  private static int _regionToEncloseImageYMils = _regionToImageYMils - 25;
  private static int _regionToEncloseImageWidthMils = _regionToImageWidthMils + 50;
  private static int _regionToEncloseImageHeightMils = _regionToImageHeightMils + 50;

  private int _regionIdToAnalyze = -1;

  private ImageAcquisitionEngine _iae;

  private double _magToRestoreToConfig = 0;
  private double _nominalMag = 0;
  private double _magnification = -1;
  private double _newMagnification;
  private boolean _imagesWereValid;

  private TestProgram _testProgram;

  private double[] _sharpness;
  // Ying-Huan.Chu
  private double[] _sharpnessBeforeCurveFit;
  private float _gaussianFitMean;
  private float _gaussianFitSigma;
  private double _gaussianFitMin = 0;
  private double _gaussianFitMax = 0;
  private float _gaussianFitAmplitude = 0;
  private float _gaussianFitBackgroundLevel = 0;
  private float _gaussianFitChiSquaredValue = 0;
  private Result _parametricResult;
  private Result _booleanResult;

  // object initializer
  {
    Assert.expect(_startingMag <= _stoppingMag);
    // Insure integer number of increments from start to stop
    long deltaMag = (long)(_stoppingMag - _startingMag);
    Assert.expect(((deltaMag / _incrementMag) * _incrementMag) == deltaMag);

    _sharpness = new double[magIndex(_stoppingMag) + 1];
    _sharpnessBeforeCurveFit = new double[_sharpness.length];
  }

  /**
   * Converts magnification range (ie: 585 to 665) to 0-based array index
   * @author Eddie Williamson
   */
  private int magIndex(double mag)
  {
    return (int)((mag - _startingMag) / _incrementMag);
  }

  /**
   *
   * @author John Dutton
   */
  public static PanelRectangle getPanelRectangleToImage()
  {
    PanelRectangle panelRectangleToImage = new PanelRectangle(
        MathUtil.convertMilsToNanoMetersInteger(_regionToImageXMils),
        MathUtil.convertMilsToNanoMetersInteger(_regionToImageYMils),
        MathUtil.convertMilsToNanoMetersInteger(_regionToImageWidthMils),
        MathUtil.convertMilsToNanoMetersInteger(_regionToImageHeightMils));

    return panelRectangleToImage;
  }

  /**
   *
   * @author John Dutton
   */
  public static PanelRectangle getRectangleEnclosingAllRegions()
  {
    PanelRectangle rectangleEnclosingAllRegions = new PanelRectangle(
        MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageXMils),
        MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageYMils),
        MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageWidthMils),
        MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageHeightMils));

    return rectangleEnclosingAllRegions;
  }

  /**
   * @author Eddie Williamson
   */
  public static synchronized CalMagnification getInstance()
  {
    if (_instance == null)
    {
      _instance = new CalMagnification();
    }
    return _instance;
  }

  /**
   * @author Eddie Williamson
   */
  private CalMagnification()
  {
    super(_NAME_OF_CAL,
          _EVENT_FREQUENCY,
          _EXECUTE_EVERY_TIMEOUT_SECONDS,
          ProgressReporterEnum.MAGNIFICATION_ADJUSTMENT_TASK);
    _logger = new DirectoryLoggerAxi(_logDirectory, FileName.getMagnificationLogFileBasename(),
                                     FileName.getLogFileExtension(), _maxLogFilesInDirectory);

    _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

    setTimestampEnum(HardwareCalibEnum.REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN);
  }

  /**
   * @author Eddie Williamson
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * @author Eddie Williamson
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
        
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }

  /**
   * As of 8/16/06 the expected range of mag is from 6.189 to 6.312
   * The search range is specified near the top of this file _startingMag and _stoppingMag
   * Currently the limits are well outside this range (6.0-6.5) and will never fail parametrically.
   *
   * @author Eddie Williamson
   */
  protected void createLimitsObject() throws DatastoreException
  {
    // Create container for the others
    _limits = new Limit(null, null, null, _NAME_OF_CAL);
    Limit booleanLimit = new Limit(null, null, null, "Adjustment feature present");
    _limits.addSubLimit(booleanLimit);
    
    //Ngie Xing, S2EX will have its own limits for magnification
    if(XrayTester.isS2EXEnabled())
    {
      if(XrayTester.isLowMagnification())
      {
        String zAxisMotorPositionForLowMag = _config.getSystemCurrentLowMagnification();
        Limit parametricLimit= new Limit(new Double(_parsedLimits.getLowLimitDouble("magnification_" + zAxisMotorPositionForLowMag)),
                                   new Double(_parsedLimits.getHighLimitDouble("magnification_" + zAxisMotorPositionForLowMag)),
                                   "magnification", "Reference Plane Magnification");
        _limits.addSubLimit(parametricLimit);
      }
    }
    else
    {
      Limit parametricLimit= new Limit(new Double(_parsedLimits.getLowLimitDouble("magnification")),
                                 new Double(_parsedLimits.getHighLimitDouble("magnification")),
                                 "magnification", "Reference Plane Magnification");
      _limits.addSubLimit(parametricLimit);
    }
    
  }

  /**
   * @author Eddie Williamson
   */
  protected void createResultsObject()
  {
    // Create container for the others
    //result = 5.25 (throughput), 6.25(regular)
    double result = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL);
    _result = new Result(_NAME_OF_CAL);
    _booleanResult = new Result(new Boolean(true), getName());
    _parametricResult = new Result(new Double(result), getName());
    _result.addSubResult(_booleanResult);
    _result.addSubResult(_parametricResult);
  }

  /**
   * @author Eddie Williamson
   */
  protected void setUp() throws XrayTesterException
  {       
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to up position for low mag task.
        XrayActuator.getInstance().up(true);
      }
    }
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    // None needed
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
  }


  /**
   * Logs detailed information about the cal (and saves images) when the timeout
   * is exceeded. The log information is formatted as comma separated values to
   * it can be imported into Excel for manual analysis.
   * @param forceLogging If true then ignore the timeout and log anyway.
   * @author Eddie Williamson
   */
  private void logInformation() throws DatastoreException
  {
    StringBuilder logString = new StringBuilder();

    // Create header lines
    logString.append(_NAME_OF_CAL + "\n,\n");

    logString.append(",Magnification = " + _newMagnification + "\n");
    logString.append(",Gaussian Fit Mean = " + _gaussianFitMean + "\n");
    logString.append(",Gaussian Fit Sigma = " + _gaussianFitSigma + "\n");
    logString.append(",Gaussian Fit Chi Squared = " + _gaussianFitChiSquaredValue + "\n");
    logString.append(",Gaussian Fit Min = " + _gaussianFitMin + "\n");
    logString.append(",Gaussian Fit Max = " + _gaussianFitMax + "\n");
    logString.append(",Gaussian Fit Amplitude = " + _gaussianFitAmplitude + "\n");
    logString.append(",Gaussian Fit BackgroundLevel = " + _gaussianFitBackgroundLevel + "\n,\n");

    logString.append(",Mag vs Sharpness\n");
    logString.append(",,Mag,Sharpness,Gaussian,Error\n");
    for (_magnification = _startingMag; _magnification <= _stoppingMag; _magnification += _incrementMag)
    {
      double error = _sharpnessBeforeCurveFit[magIndex(_magnification)] - _sharpness[magIndex(_magnification)];
      logString.append(",," + _magnification + "," + _sharpnessBeforeCurveFit[magIndex(_magnification)] + "," + _sharpness[magIndex(_magnification)] + "," + error + "\n");
    }

    _logger.log(logString.toString());
    logString = null;
  }


  /**
   * Measure sharpness of the vertical edge in an image.
   * Uses Tracy's idea of using the standard deviation of the image after
   * using the sobel edge filter.
   * @author Eddie Williamson
   */
  private double measureVerticalEdgeSharpness(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);

    DoubleRef mean = new DoubleRef();
    DoubleRef stdev = new DoubleRef();

    Image sobelImage = Filter.convolveSobelVertical(image, roi);
    Statistics.meanStdDev(sobelImage, roi, mean, stdev);
    sobelImage.decrementReferenceCount();

    return stdev.getValue();
  }

  /**
   * @author Eddie Williamson
   */
  private void storeImageForLater(ReconstructedImages reconstructedImage)
  {
    Assert.expect(reconstructedImage != null);

    if (_areWeLogging)
    {
      int id = reconstructedImage.getReconstructionRegionId();
      Image image = reconstructedImage.getReconstructedSlice(SliceNameEnum.PAD).getImage();
      String filename = "img_" + _magnification + ".jpg";
      try
      {
        XrayImageIoUtil.saveJpegImage(image.getBufferedImage(), _logDirectory + File.separator + filename);
      }
      catch (DatastoreException ex)
      {
        ex.printStackTrace();
      }
    }
  }


  /**
   * @author Eddie Williamson
   */
  private boolean isValidImage(Image image)
  {
    Assert.expect(image != null);
    boolean valid;

    // 0 degrees for horizontal profile to find vertical edge
    int orientation = 0;

    // Search the entire image
    RegionOfInterest roi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
                                                                         image.getHeight() - 1, orientation, RegionShapeEnum.RECTANGULAR);
    // Use profile to average out noise
    float[] profile = ImageFeatureExtraction.profile(image, roi);

    // Image must go from light to dark moving from left to right
    // This is typically a difference of 60 graylevels, we'll use 30 for the limit
    if ((profile[0] - profile[image.getWidth() - 1]) > 30)
    {
      valid = true;
    }
    else
    {
      valid = false;
    }

    return valid;
  }

  /**
   * Find ReconstructionRegion of interest from TestProgram.
   * @author John Dutton
   */
  private ReconstructionRegion getReconstructedRegionToAnalyze()
  {
    ReconstructionRegion reconstructionRegionToAnalyze = null;
    Collection<ReconstructionRegion> reconstructionRegions =
      _testProgram.getAllTestSubPrograms().get(0).getVerificationRegions();
    for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
    {
      if (reconstructionRegion.getRegionId() == _regionIdToAnalyze)
      {
        reconstructionRegionToAnalyze = reconstructionRegion;
      }
    }
    Assert.expect(reconstructionRegionToAnalyze != null, "Can't find reconstruction region to analyze!");
    return reconstructionRegionToAnalyze;
  }

  /**
   * Handles interactions with ImageAcquisitionEngine for getting images.
   * @return Boolean is true if images were returned as expected from IAE, false otherwise.
   *
   * @author Eddie Williamson
   * @author John Dutton
   */
  private Boolean getReconstructedImagesFromIAE() throws XrayTesterException
  {
    BooleanRef noImagesReturned = new BooleanRef(false);
    ReconstructionRegion reconstructionRegionToAnalyze = getReconstructedRegionToAnalyze();

    try
    {
      // Get images off producer's queue (following typical use model), even though they won't be used.
      // Assumes 2 reconstruction regions were requested.  If this changes in the test program then the
      // following needs to change.  (I would deal with this dependency in a more robust manner if the
      // long-term intention wasn't to get rid of the use of test programs for acquiring coupon images.)
      ReconstructedImages flushReconstructedImage = _iae.getReconstructedImages(noImagesReturned);
      if (noImagesReturned.getValue() == false)
      {
        ReconstructionRegion regionOfImages = flushReconstructedImage.getReconstructionRegion();
        flushReconstructedImage.decrementReferenceCount();
        _iae.freeReconstructedImages(regionOfImages);
        flushReconstructedImage = _iae.getReconstructedImages(noImagesReturned);
        if (noImagesReturned.getValue() == false)
        {
          _iae.freeReconstructedImages(flushReconstructedImage.getReconstructionRegion());
          flushReconstructedImage.decrementReferenceCount();
        }
        else
        {
          return Boolean.FALSE;
        }
      }
      else
      {
        return Boolean.FALSE;
      }

      // Now do the guts of the adjustment, looping through all the magnifications and having the IRP
      // generate images based on the projection data that is still in the IRP.
      for (_magnification = _startingMag; _magnification <= _stoppingMag; _magnification += _incrementMag)
      {
        if (_iae.isAbortInProgress() == false)
        {
          // Set mag
          double newMag = _magnification;
          _config.setValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION, newMag);
          double highMag = Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION);
          _iae.sendCalibratedMagnifications(newMag, highMag);

          _iae.reacquireImages(reconstructionRegionToAnalyze);
          BooleanRef imagesReturned = new BooleanRef(true);
          ReconstructedImages reconstructedImage = _iae.waitForReReconstructedImages(reconstructionRegionToAnalyze, imagesReturned);
          if (imagesReturned.getValue() == false)
          {
            return Boolean.FALSE;
          }
          storeImageForLater(reconstructedImage);

          // calculate sharpness here
          Image image = reconstructedImage.getReconstructedSlice(SliceNameEnum.PAD).getImage();

          if (isValidImage(image))
          {
            RegionOfInterest vroi = RegionOfInterest.createRegionFromRegionBorder(0, 0, image.getWidth() - 1,
              image.getHeight() - 1, _VERTICAL, RegionShapeEnum.RECTANGULAR);

            _sharpness[magIndex(_magnification)] = measureVerticalEdgeSharpness(image, vroi);
          }
          else
          {
            // image is invalid, we need to fail the test
            // This will also break us out of the for loop
            //
            // DO NOT RETURN here. We need to clear out the images in the IAE
            // otherwise it will hang the test.
            _imagesWereValid = false;
          }

          reconstructedImage.decrementReferenceCount();

          reportProgress(20 + (int)(70 * ((_magnification - _startingMag)) / (_stoppingMag - _startingMag)));
        }
      } // End for loop through magnifications

      // Before going on we need to mark all RRs complete.
      // This frees memory and unblocks iae for the main thread
      Collection<ReconstructionRegion> reconstructionRegions =
        _testProgram.getAllTestSubPrograms().get(0).getVerificationRegions();
      for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
      {
        if (_iae.isAbortInProgress() == false)
        {
          _iae.reconstructionRegionComplete(reconstructionRegion, true);
        }
      }
    }
    catch (XrayTesterException xte)
    {
      // Need to stop IAE to stop the GetCouponImagesTask.
      _iae.abort(xte);
    }

    return Boolean.TRUE;
  }

  /**
   * @author Rex Shang
   */
  private class GetCouponImagesTask extends ThreadTask<Object>
  {
    public GetCouponImagesTask()
    {
      super("GetCouponImagesTask");
    }

    /**
     * @author Rex Shang
     */
    protected Boolean executeTask() throws XrayTesterException
    {
      Boolean gotImages = getReconstructedImagesFromIAE();
      return gotImages;
    }

    /**
     * @author Rex Shang
     */
    protected void clearCancel()
    {
      // Do nothing.
    }

    /**
     * @author Rex Shang
     */
    protected void cancel() throws XrayTesterException
    {
      // Do nothing.
    }
  }

  /**
   * @author Rex Shang
   */
  private class RequestCouponImagesTask extends ThreadTask<Object>
  {
    public RequestCouponImagesTask()
    {
      super("RequestCouponImagesTask");
    }

    /**
     * @author Rex Shang
     */
    protected Object executeTask() throws XrayTesterException
    {
      // Force half stepping of feature across the cameras. This maximizes the
      // error when the mag is not right which helps us find the right one.
      // Request an image from IAE.
      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
      _iae.acquireCouponImages(_testProgram,
                               testExecutionTimer,
                               2540000,
                               4445000,
                               0.45);

      return null;
    }

    /**
     * @author Rex Shang
     */
    protected void clearCancel()
    {
      // Do nothing.
    }

    /**
     * @author Rex Shang
     */
    protected void cancel() throws XrayTesterException
    {
      // Do nothing.
    }
  }

  /**
   * @author Eddie Williamson
   */
  protected void executeTask() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
	//Swee Yee Wong - Camera debugging purpose
    debug("CalMagnification");
    if (UnitTest.unitTesting() == true && _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true)
    {
      //Create a passing result
      String localizedTaskName = getName();
      _limits = new Limit(null, null, null, localizedTaskName);
      _result = new Result(new Boolean(true), localizedTaskName);
      return;
    }

    _areWeLogging = _logger.isLoggingNeeded(_logTimeoutMilliSeconds);

    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_MAGNIFICATION);
    _hardwareObservable.setEnabled(false);
    reportProgress(0);

    _imagesWereValid = true;

    // Save old mag to restore later
    _magToRestoreToConfig = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION);
    _nominalMag = _config.getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION_NOMINAL);


    try
    {
      // set _magnification for receiver thread
      _magnification = _nominalMag;
      _config.setValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION, _nominalMag);

      TimerUtil t2 = new TimerUtil();
      t2.start();

      ImageRetriever ir = ImageRetriever.getInstance();
      _testProgram = ir.getTestProgram(ImageRetrieverTestProgramEnum.CAL_MAGNIFICATION_IMAGE);
      _regionIdToAnalyze = ir.getRegionIdToAnalyze(); // Used in getImageFromIAE.

      t2.stop();
      CalibrationSupportUtil.debugPrint("[Mag] Time to create test program " + t2.getElapsedTimeInMillis());

      _iae = ImageAcquisitionEngine.getInstance();
      reportProgress(20);

      // Start a threaded task that returns an image from the IAE.
      _iae.initialize();

      ThreadTask<Object> getImagesTask = new GetCouponImagesTask();
      ThreadTask<Object> requestImagesTask = new RequestCouponImagesTask();

      _magCalParallelThread.submitThreadTask(getImagesTask);
      _magCalParallelThread.submitThreadTask(requestImagesTask);

      // Cleanup threaded task.
      try
      {
        _magCalParallelThread.waitForTasksToComplete();

        Boolean gotImages = (Boolean) getImagesTask.get();
        if (gotImages == null || gotImages.equals(Boolean.FALSE))
        {
          // Image wasn't returned from IAE.  Instead of forcing caller to deal with null return
          // values throw an exception.
          ImageRetrieverBusinessException irException = new ImageRetrieverBusinessException();
          throw irException;
        }
      }
      catch (XrayTesterException xte)
      {
        reportProgress(100);
        _result.setTaskStatusMessage(xte.getLocalizedMessageWithoutHeaderAndFooter());

        // The re-throw is needed to help the "catch (Exception e)" below.
        throw xte;
      }
      catch (Throwable t)
      {
        Assert.logException(t);
      }

      TimerUtil t1 = new TimerUtil();
      t1.start();
      reportProgress(90);

      _booleanResult.setResults(new Boolean(_imagesWereValid));
      if (_imagesWereValid)
      {
        // Successfully retrieved and analyzed all images. Now finish calculation of magnification
        boolean allVerticalSharpnessValid = true;
        double maxVerticalSharpness = 0.0;
        double  magForMaxVerticalSharpness = 0;
        for (_magnification = _startingMag; _magnification <= _stoppingMag; _magnification += _incrementMag)
        {
          if (_sharpness[magIndex(_magnification)] > maxVerticalSharpness)
          {
            magForMaxVerticalSharpness = _magnification;
            maxVerticalSharpness = _sharpness[magIndex(_magnification)];
          }
          if (_sharpness[magIndex(_magnification)] <= 0)
          {
            allVerticalSharpnessValid = false;
          }
        }
        if (allVerticalSharpnessValid)
        {
          double[] magnificationArray = new double[_sharpness.length];
          double[] curveFittedSharpness = new double[_sharpness.length];
          for (_magnification = _startingMag; _magnification <= _stoppingMag; _magnification += _incrementMag)
          {
            magnificationArray[magIndex(_magnification)] = _magnification;
          }
          // Perform Curve-Fitting
          curveFittedSharpness = GaussianModelCurveFitting.GaussianCurve(magnificationArray, _sharpness, magForMaxVerticalSharpness);
          // Copy the values to arrays
          for (int i = 0; i < _sharpness.length; ++i)
          {
            _sharpnessBeforeCurveFit[i] = _sharpness[i];
        }
          for (int i = 0; i < curveFittedSharpness.length; ++i)
          {
            _sharpness[i] = curveFittedSharpness[i];
          }
          // Calculate Gaussian Fit Mean and Gaussian Fit Sigma
          _gaussianFitMean = GaussianModelCurveFitting.getGaussianFitMean();
          _gaussianFitSigma = GaussianModelCurveFitting.getGaussianFitSigma();

          _gaussianFitMin = GaussianModelCurveFitting.getGaussianFitMin();
          _gaussianFitMax = GaussianModelCurveFitting.getGaussianFitMax();
          _gaussianFitAmplitude = GaussianModelCurveFitting.getGaussianFitAmplitude();
          _gaussianFitBackgroundLevel = GaussianModelCurveFitting.getGaussianFitBackgroundLevel();
          _gaussianFitChiSquaredValue = GaussianModelCurveFitting.getGaussianFitChiSquaredValue();

          // Calculate Gaussian Fit Maximum Sharpness 
          magForMaxVerticalSharpness = GaussianModelCurveFitting.getGaussianFitPeakX(magnificationArray, _sharpness);

        _newMagnification = magForMaxVerticalSharpness;
        }
        else
        {
          _gaussianFitMean = 0.0f;
          _gaussianFitSigma = 0.0f;
          _gaussianFitMin = 0.0f;
          _gaussianFitMax = 0.0f;
          _gaussianFitAmplitude = 0.0f;
          _gaussianFitBackgroundLevel = 0.0f;
          _gaussianFitChiSquaredValue = 0.0f;
          _newMagnification = magForMaxVerticalSharpness = 0.0f;
        }
        _parametricResult.setResults(new Double(magForMaxVerticalSharpness));
        CalibrationSupportUtil.debugPrint("[Mag] Measured magnification = " + _newMagnification);
        if (_limits.didTestPass(_result)&& allVerticalSharpnessValid)
        {
          // Passing result -- Set so this will be written into the cal file
          _magToRestoreToConfig = _newMagnification;
        }
        else
        {
          //No matter what we will log with parameters, so set this up before moving on
          Object[] parameters = new Object[]
          {
            _newMagnification
          };

          // Failing result
          _result.setTaskStatusMessage(StringLocalizer.keyToString(new LocalizedString("ACD_MAGNIFICATION_OUT_OF_EXPECTED_RANGE_KEY", parameters)));
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("ACD_MAGNIFICATION_FAILED_VALUE_OUT_OF_RANGE_SUGGESTED_ACTION_KEY"));
          _areWeLogging = true;
        }
      }
      else
      {
        CalibrationSupportUtil.debugPrint("[Mag] Invalid images -- missing correct edge");
        _result.setTaskStatusMessage(StringLocalizer.keyToString("ACD_MISSING_SYSTEM_FEATURE_KEY"));
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("ACD_MAGNIFICATION_FAILED_FEATURE_NOT_FOUND_SUGGESTED_ACTION_KEY"));
      }

      t1.stop();
      CalibrationSupportUtil.debugPrint("Final calculations took " + t1.getElapsedTimeInMillis() + " millis");
      if (_areWeLogging)
      {
        logInformation();
      }
    }
    finally
    {
      try
      {
        _config.setValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION, _magToRestoreToConfig);
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      reportProgress(100);
    }

    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_MAGNIFICATION);
  }

  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
