package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 *
 * @author Reid Hayhow
 */
public class Test_ConfirmLegacyXRaySource extends UnitTest
{
  /**
   * Test harness for the Legacy Xray Source time driven hardware task
   *
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    //Create test util used to backup/restore HW config
    TestUtils utils = new TestUtils();

    //backup HW config since fooling with Xrays may change it
    utils.backupHWConfig(getTestSourceDir());

    //Save off the hardware.calib because running tasks will change it by adding timestamps
    utils.backupHWCalib(getTestSourceDir());

    //Create an instance
    ConfirmLegacyXRaySource _confirm = ConfirmLegacyXRaySource.getInstance();

    boolean taskPassed = false;
    //Try to execute the task
    try
    {
      // Set the config enum to use the legacy xray source before starting up the tester
      Config.getInstance().setValue(HardwareConfigEnum.XRAY_SOURCE_TYPE, "legacy");

      // Assure that the tester is started up, otherwise tasks won't run!
      XrayTester.getInstance().setSimulationMode();
      XrayTester.getInstance().startup();

      //Get the xraySource instance
      AbstractXraySource xraySource = LegacyXraySource.getInstance();

      xraySource.abort();

      // Set an abort and run the confirmation, should fail
      taskPassed = _confirm.executeUnconditionally();

      Expect.expect(taskPassed);

      //Turn xrays off
      xraySource.off();

      //Execute the task, it should pass
      taskPassed = _confirm.executeUnconditionally();

      Expect.expect(taskPassed);

      //Turn xrays back on
      xraySource.on();

      //Execute the task, it should pass
      taskPassed = _confirm.executeUnconditionally();

      Expect.expect(taskPassed);

      //Even if we power off xrays, the task execution will start them back up
      xraySource.shutdown();

      //Execute the task, will NOT turn xrays back on, so task passes but is not run
      taskPassed = _confirm.executeUnconditionally();

      Expect.expect(taskPassed == false);
      Expect.expect(_confirm.getTaskStatus() == HardwareTaskStatusEnum.NOT_RUN);
    }
    //Didn't expect any exceptions, so fail if we see one
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
    }
    finally
    {
      //Restore the HW config and hardware.calib when done since values will have been changed
      utils.restoreHWConfig(getTestSourceDir());
      utils.restoreHWCalib(getTestSourceDir());
    }
  }

  /**
   *
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ConfirmLegacyXRaySource());
  }
}
