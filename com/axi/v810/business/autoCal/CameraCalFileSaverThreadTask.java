package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * This class provides a ThreadTask to allow camera cal file logging to be done
 * in parallel using the ExecuteParallelThreadTasks API.
 *
 * @author Reid Hayhow
 */
class CameraCalFileSaverThreadTask extends ThreadTask<Boolean>
{
  private String _logDirectory;
  private int _logTimeout;
  private XrayCameraCalibrationData _calData;
  /**
   *
   * @author Reid Hayhow
   */
  protected void cancel() throws Exception
  {
    //Do nothing
  }

  /**
   * @author Reid Hayhow
   *
   */
  protected void clearCancel()
  {
    //Do nothing
  }

  /**
   * @author Reid Hayhow
   *
   */
  protected Boolean executeTask() throws Exception
  {
    _calData.log(_logDirectory, _logTimeout);
    return true;
  }

  /**
   * @author Reid Hayhow
   */
  public CameraCalFileSaverThreadTask(XrayCameraCalibrationData calData,
                                      String logDirectory,
                                      int timeout)
  {
    super("Camera Cal File Saver Thread");
    _calData = calData;
    _logDirectory = logDirectory;
    _logTimeout = timeout;
  }
}
