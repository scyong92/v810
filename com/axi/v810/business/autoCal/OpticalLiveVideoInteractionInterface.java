package com.axi.v810.business.autoCal;

import java.awt.geom.*;
import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public interface OpticalLiveVideoInteractionInterface 
{
    public void runNextOpticalCamera();
    public void cancelLiveVideo();
    public void setSurfaceMapPoint(List<Point2D> points);
    public void setHardwareShutdownComplete();
}
