package com.axi.v810.business.autoCal;

import com.axi.v810.util.*;
import com.axi.v810.hardware.autoCal.*;

public class TestConfirmPanelPositioningTask extends ConfirmPanelPositioningTask
{
  private static TestConfirmPanelPositioningTask _testInstance;

  /**
   * Instance access for this class
   *
   * @author Reid Hayhow
   */
  public static synchronized TestConfirmPanelPositioningTask getInstance()
  {
    if(_testInstance == null)
    {
      _testInstance = new TestConfirmPanelPositioningTask();
    }
    return _testInstance;
  }

  public static TestConfirmPanelPositioningTask getTestInstance()
  {
    return _testInstance;
  }
  /*
   * repeatMotionProfilesTillTimeout
   */
  protected void runPointToPointMovesTillTimeout() throws XrayTesterException
  {
    int numberOfProfilesRun = 0;
    _listOfMotions.clear();

    while(numberOfProfilesRun < 10)
    {
      runRandomPointToPointMoves();
      numberOfProfilesRun++;
    }
  }

  protected void createLimitsObject()
  {
    //Get the limits from the limits file
    Long lowLimit = new Long(0);
    Long highLimit = new Long(Long.MAX_VALUE);

    //Set the limits object to the parsed limits from the file
    _limits = new Limit(lowLimit,
                        highLimit,
                        StringLocalizer.keyToString("ACD_TOTAL_DIFFERENCE_KEY"),
                        _name);
  }
}
