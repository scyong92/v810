package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Reid Hayhow
 *
 */
public class ConfirmPanelPositioningTask extends EventDrivenHardwareTask
{
  protected static String _name = StringLocalizer.keyToString("CD_CONFIRM_PANEL_POSITIONING_KEY");
  private static int _frequency = 1;
  private static long _timeoutInSeconds = 20000;
  private static ConfirmPanelPositioningTask _instance = null;

  //The config is needed to access the stored System Fiducial values
  private static Config _config = Config.getInstance();

  //Log utility members
  private FileLoggerAxi _logger;
  private int _maxLogFileSizeInBytes = 10280000;
  private Map<HardwareCalibEnum, Integer> _calibEnumToSavedValueMap = new LinkedHashMap<HardwareCalibEnum, Integer>();

  //Motion profile to use when moving stage, use the fastest
  protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;

  //How long to run the motion profile
  private static final int _timeToRunPointToPointMovesInMilliseconds = 1000 * 60;

  //List of the min values for Panel Positioning locations. I would initialize
  //it here and make it final, but the method from Panel Positioning isn't static
  private static int _minXInNanometers = -1;
  private static int _maxXInNanometers = -1;
  private static int _minYInNanometers = -1;
  private static int _maxYInNanometers = -1;

  //Map to store all the results in so we can dump them in the log file if
  //the confirmation fails
  private Map<HardwareCalibEnum, Integer> _resultValuesMap = new LinkedHashMap<HardwareCalibEnum, Integer>();

  //Protected because the Regression Test sub-class needs to access it
  protected List<Pair<Integer, Integer>> _listOfMotions = new ArrayList<Pair<Integer, Integer>>();
  private static final int MAX_POINT_TO_POINT_MOVES_TO_RUN = 50;
  private XrayTesterException _xrayTesterException = null;

  private static final int _ESTIMATED_TIME_TO_RUN_SYSTEM_OFFSET_ADJUSTMENT_IN_MILLIS = 12000;

  /**
   * Instance access for this class
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmPanelPositioningTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmPanelPositioningTask();
    }
    return _instance;
  }

  /**
   * Constuctor for this confirmation
   *
   * @author Reid Hayhow
   */
  protected ConfirmPanelPositioningTask()
  {
    super(_name,
          _frequency,
          _timeoutInSeconds,
          ProgressReporterEnum.CONFIRM_MOTION_REPEATABILITY);

    //Create the logger
    _logger = new FileLoggerAxi(FileName.getConfirmMotionRepeatabilityFullPath() +
                                FileName.getLogFileExtension(),
                                _maxLogFileSizeInBytes);

    //This confirmation is to be run manually only
    _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;

    //Set the enum used to save the timestamp for this confirmation
    setTimestampEnum(HardwareCalibEnum.CONFIRM_MOTION_REPEATABILITY_LAST_TIME_RUN);
  }

  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    //This will run on the first release of the Genesis SW
    return true;
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays Cylinder to Up and Safe position.
        XrayActuator.getInstance().up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
  }

  /**
   * Create a Limits object specific to the particular test being run.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject() throws DatastoreException
  {
    //Get the limits from the limits file, no sub-limit needed is indicated by ""
    Long lowLimit = _parsedLimits.getLowLimitLong("");
    Long highLimit = _parsedLimits.getHighLimitLong("");

    //Set the limits object to the parsed limits from the file
    _limits = new Limit(lowLimit,
                        highLimit,
                        StringLocalizer.keyToString("ACD_TOTAL_DIFFERENCE_KEY"),
                        _name);
  }

  /**
   * createResultsObject Create the results object for this test result.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    //Create a result that is guaranteed to fail
    _result = new Result(new Long( -1), _name);
  }

  /**
   * This is the method that actually executes the task. It saves off the values
   * for the System Fiducal Cal, runs predetermined 'extreme' point-to-point moves,
   * runs random point-to-point moves, runs the System Fiducial Cal again and
   * compares the saved values to the new values.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    //Initialize the exception member to null
    _xrayTesterException = null;

    // Report that this task is starting
    reportProgress(0);

    //Initialize the limits for the point-to-point moves in X
    _minXInNanometers = PanelPositioner.getInstance().getXaxisMinimumPositionLimitInNanometers();
    _maxXInNanometers = PanelPositioner.getInstance().getXaxisMaximumPositionLimitInNanometers();

    //Initialize the limits for the point-to-point moves in Y
    _minYInNanometers = PanelPositioner.getInstance().getYaxisMinimumPositionLimitInNanometers();
    _maxYInNanometers = PanelPositioner.getInstance().getYaxisMaximumPositionLimitInNanometers();

    //Save the system fiducial values from the HW config
    saveSystemFiducialValues();

    //Since we are going to move the stage, tell the cameras to ignore
    //triggers till we are done
    disableAllCameraTriggers();

    try
    {
      //Now actually run the point-to-point moves as long as specified
      runPointToPointMovesTillTimeout();
    }
    catch(XrayTesterException ex)
    {
      //Set the exception member for use in logging
      _xrayTesterException = ex;
      _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_PANEL_POSITIONING_REPEATABILITY_FAILED_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_EXCEPTION_IN_PANEL_POSITIONING_TASK_ACTION_KEY"));

      //Log the results of this confirmation
      logResults(Long.MAX_VALUE);
      throw _xrayTesterException;
    }
    finally
    {
      //OK, we are done with motions, so we can tell the cameras to respond
      //to triggers again
      enableAllCameraTriggers();
    }

    //Handle a cancel gracefully. This will cause the task to fail
    if (_taskCancelled == true)
    {
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }

    // Report that we are almost done, but still have a bit more to do
    reportProgress(85);

    //Get the System Fiducial cal instance so we can run it
    CalSystemFiducialEventDrivenHardwareTask systemFiducialCal =
        CalSystemFiducialEventDrivenHardwareTask.getInstance();

    //Run the System Fiducal Cal
    //NOTE:This is an execution WITHOUT updates. No results are sent up to
    //any observers. This is because we will know the result of the cal and
    //can handle any messages or reporting more accurately than the cal.
    boolean calPassed = systemFiducialCal.executeWithoutUpdate();

    //If the cal passes, then we can measure the differences between the first
    //System Fiducal cal and the one we just ran.
    if (calPassed)
    {
      //Get the sum of all the differences between the two System Fiducial cals
      long sumOfAllDifferences = compareSavedConfigValuesToNewConfigValues();

      //Save the value in a new Result object
      _result = new Result(new Long(sumOfAllDifferences), _name);

      //Log the results of this confirmation
      logResults(sumOfAllDifferences);
    }
    //If the cal didn't pass, then all bets are off. This confirmation certainly
    //has failed, but we can't tell how badly or why. Just report that the cal
    //failed and assure that this task fails.
    else
    {
      //Set the result to fail
      _result = new Result(new Long( -1), _name);

      //Log the results of this confirmation
      logResults(Long.MAX_VALUE);

      //Create a distinct message
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_FIDUCIAL_CAL_FAILED_IN_PANEL_POSITIONING_TASK_STATUS_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_FIDUCIAL_CAL_FAILED_IN_PANEL_POSITIONING_SUGGESTED_ACTION_KEY"));
    }

    // Report that this task is done
    reportProgress(100);
  }

  /**
   * Make the cameras respond to triggers again
   *
   * @author Reid Hayhow
   */
  private void enableAllCameraTriggers() throws XrayTesterException
  {
    enableCameraTriggers(true);
  }

  /**
   * Make the cameras ignore triggers while we move the stage
   *
   * @author Reid Hayhow
   */
  private void disableAllCameraTriggers() throws XrayTesterException
  {
    enableCameraTriggers(false);
  }

  /**
   * Consolidated method to loop over all cameras and change their response
   * to triggers. The boolean input determines if they will respond to triggers.
   *
   * @author Reid Hayhow
   */
  private void enableCameraTriggers(boolean triggersOn) throws XrayTesterException
  {
    List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();
    for (AbstractXrayCamera xRayCamera : xRayCameras)
      xRayCamera.enableTriggerDetection();
  }

  /**
   * Method to log results for this Confirmation. It logs results differentially
   * if the task passed or failed. Failures generate verbose logging, including
   * each System Fiducial Cal values delta and all motions that were run
   *
   * @author Reid Hayhow
   */
  private void logResults(long sumOfAllDifferences) throws DatastoreException
  {
    //No matter what we will log with parameters, so set this up before moving on
    Object[] parameters = new Object[]{sumOfAllDifferences};

    //If the task passed, then just log the fact that it passed
    if (_limits.didTestPass(_result))
    {
      //Create the localized string with parameters
      LocalizedString passingInfo = new LocalizedString("CD_RESULTS_FOR_PASSING_PANEL_POSITIONING_TEST_KEY", parameters);

      //Log the passing message as well as the sum of the differences calculated
      _logger.append(StringLocalizer.keyToString(passingInfo));

      //Also set the result status message to indicate pass
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_PASSED_KEY"));
    }
    //If the task passed, dump more detailed information
    else
    {
      //Create the localized string with parameters
      LocalizedString failureInfo = new LocalizedString("CD_RESULTS_FOR_FAILING_PANEL_POSITIONING_TEST_KEY", parameters);

      //Log a preamble that says the task failed and dumps the sum of differences
      _logger.append(StringLocalizer.keyToString(failureInfo));

      //Also set the result status message to indicate the failure
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_PANEL_POSITIONING_REPEATABILITY_FAILED_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_FAILING_PANEL_POSITIONING_SUGGESTED_ACTION_KEY"));
    }

    if(_xrayTesterException != null)
    {
      _logger.append(_xrayTesterException.getLocalizedMessage());
    }

    //Get a Map.Entry iterator. This allows me to iterate through each map entry
    //and get the key/value pairs in order. Because this map is a LinkedHashMap
    //the iterator will get them in the order I inserted them, thus making the
    //results MUCH more readable and logically grouped
    Iterator<Map.Entry<HardwareCalibEnum, Integer>> resultValuesMapEntryIterator =
        _resultValuesMap.entrySet().iterator();

    //Create a buffer to store the logfile data
    StringBuilder buffer = new StringBuilder();

    //I am going to use this lots, so make a local copy
    String lineSeparator = System.getProperty("line.separator");

    //Continue as long as we have more entries
    while (resultValuesMapEntryIterator.hasNext())
    {
      //Get the next entry in the map
      Map.Entry<HardwareCalibEnum, Integer> resultMapEntry = resultValuesMapEntryIterator.next();

      //Get the Key/Value pair for this entry
      HardwareCalibEnum calibEnum = resultMapEntry.getKey();
      Integer valueForCalibEnum = resultMapEntry.getValue();

      //Store the difference for this pair
      buffer.append(StringLocalizer.keyToString("ACD_TOTAL_DIFFERENCE_KEY") + " ");
      buffer.append(StringLocalizer.keyToString(calibEnum.getKey()) +
                    " = " +
                    valueForCalibEnum +
                    lineSeparator);
    }

    //Pad a line in the buffer
    buffer.append(lineSeparator);

    //Now generate the list of motions. First, get an iterator for the list of
    //motions. This list is generated as the motions are run, so it should be
    //in order
    ListIterator<Pair<Integer, Integer>> motionListIterator = _listOfMotions.listIterator();

    //Create the header for the motions in the logfile
    buffer.append(StringLocalizer.keyToString("ACD_MOTION_PROFILE_NUMBER_KEY") + "," +
                  StringLocalizer.keyToString("ACD_X_POSITION_VALUE_KEY") + "," +
                  StringLocalizer.keyToString("ACD_Y_POSITION_VALUE_KEY") + lineSeparator);

    //While there are still motions to log...
    while (motionListIterator.hasNext())
    {
      //Get the Pair of points for this motion
      Pair<Integer, Integer> motionPair = motionListIterator.next();

      //get the index for this motion so we can log it
      int motionNumber = motionListIterator.previousIndex() + 1;

      //Log the values for this motion
      buffer.append(motionNumber + "," +
                    motionPair.getFirst() + "," +
                    motionPair.getSecond() +
                    lineSeparator);
    }

    //Log the motion data to disk
    _logger.append(buffer.toString());
  }

  /**
   * High level method to run the point-to-point moves in this confirmation. It
   * runs the extreme point-to-point moves and then random point-to-point moves.
   * It also handles the timing/timeout for the moves as well as guaranteeing
   * that only a max number of motions are run.
   *
   * @author Reid Hayhow
   */
  protected void runPointToPointMovesTillTimeout() throws XrayTesterException
  {
    //Start the timer on the point-to-point moves
    long startTime = System.currentTimeMillis();
    long currentTime = startTime;
    double percentDone = 0;

    //Protect against a time warp by counting moves and stoping at a MAX
    int pointToPointMovesRun = 0;

    //Clear the list of point-to-point motions we have run since we are
    //starting over
    _listOfMotions.clear();

    //First, cover the extremes by going to the limits of Panel Positioning
    runExtremePointToPointMoves();

    // Calculate and report how far along we are with this task based on how long it should run
    // And reserve 20% for the system offset cal that has to run after this
    currentTime = System.currentTimeMillis();
    percentDone = 80 *((currentTime - startTime)/(double)_timeToRunPointToPointMovesInMilliseconds);

    // Report how much has been done
    reportProgress((int)percentDone);

    //Handle a cancel gracefully. This will cause the task to fail
    if (_taskCancelled == true)
    {
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }

    //Then run random point-to-point moves until timeout or too many moves
    while ((currentTime - startTime) < _timeToRunPointToPointMovesInMilliseconds
        && pointToPointMovesRun < MAX_POINT_TO_POINT_MOVES_TO_RUN )
    {
      //Run a single, random point-to-point move
      runRandomPointToPointMoves();

      //Update the timer
      currentTime = System.currentTimeMillis();

      // Calculate and report how far along we are with this task based on how long it should run
      // And reserve 20% for the system offset cal that has to run after this
      percentDone = 80 *((currentTime - startTime)/(double)_timeToRunPointToPointMovesInMilliseconds);

      // Report that we have made some progress
      reportProgress((int)percentDone);

      //Increment the fail-safe counter
      pointToPointMovesRun++;

      //Handle a cancel gracefully. This will cause the task to fail
      if (_taskCancelled == true)
      {
        _result.setStatus(HardwareTaskStatusEnum.CANCELED);
        return;
      }
    }
  }

  /**
   * Method to compare the System Fiducial Cal values saved before all of the
   * motions used by this confirmation against the values seen after all of the
   * motions. The results are saved in a map, using the HardwareCalibEnum as a
   * key. This method returns the sum of all differences encountered between the
   * saved values and the new values from System Fiducial Cal.
   *
   * @author Reid Hayhow
   */
  private long compareSavedConfigValuesToNewConfigValues()
  {
    //Storage for the acumulated value
    long acumulatedAbsoluteValueOfPixelOffsets = 0;

    //Purge stored results map each time
    _resultValuesMap.clear();

    //Get a Map.Entry iterator to the saved values. Since the values were saved
    //into this map in a defined order, this iterator preserves that order. This,
    //in turn, allows us to create the result map in a defined order.
    Iterator<Map.Entry<HardwareCalibEnum, Integer>>
        savedValueMapIterator = _calibEnumToSavedValueMap.entrySet().iterator();

    //iterate over each map entry
    while (savedValueMapIterator.hasNext())
    {
      //Get the entry at this map location
      Map.Entry<HardwareCalibEnum, Integer> savedMapEntry = savedValueMapIterator.next();

      //Get the Key for this entry
      HardwareCalibEnum calibEnum = savedMapEntry.getKey();

      //Get the value at the calibEnum key. This is the saved System Fiducial
      //Cal value
      Integer savedValue = savedMapEntry.getValue();

      //Now get the new System Fiducial cal value using the key
      int newValue = _config.getIntValue(calibEnum);

      //Calculate the difference
      int diffBetweenSavedAndNew = Math.abs(savedValue - newValue);

      //Store the difference in the result map at the key for the calib enum
      _resultValuesMap.put(calibEnum, diffBetweenSavedAndNew);

      //Also accumulate the total difference
      acumulatedAbsoluteValueOfPixelOffsets += diffBetweenSavedAndNew;
    }
    return acumulatedAbsoluteValueOfPixelOffsets;
  }

  /**
   * This method runs the stage through a predefined set of motions that
   * are at the limits of what the stage is ever expected to do.
   *
   * @author Reid Hayhow
   */
  private void runExtremePointToPointMoves() throws XrayTesterException
  {
    //Move the stage in an hourglass shape, starting with the two min values
    moveStage(_minXInNanometers, _minYInNanometers, _motionProfile);

    // Report progress each time. This is a rough guess of what percentage
    // of the whole test these moves takes cumulatively
    reportProgress(3);

    //Move to the two max values, diagonal across the cameras
    moveStage(_maxXInNanometers, _maxYInNanometers, _motionProfile);
    reportProgress(6);

    //Now move from Y max to Y min
    moveStage(_maxXInNanometers, _minYInNanometers, _motionProfile);
    reportProgress(9);

    //Now move from X max to X min
    moveStage(_minXInNanometers, _maxYInNanometers, _motionProfile);
    reportProgress(12);

    //Finally move back to X min, Y min
    moveStage(_minXInNanometers, _minYInNanometers, _motionProfile);
    reportProgress(15);
  }

  /**
   * Utilty method to simplify stage motion. Input the X/Y point to move to as
   * well as the motion profile (speed) to use. This method does the rest.
   *
   * @author Reid Hayhow
   */
  private void moveStage(int xPositionToMoveTo,
                         int yPositionToMoveTo,
                         PointToPointMotionProfileEnum profile) throws XrayTesterException
  {
    StagePositionMutable stagePosition = new StagePositionMutable();

    //Save this motion profile so we can dump it if the confirmation fails
    _listOfMotions.add(new Pair<Integer, Integer>(xPositionToMoveTo, yPositionToMoveTo));

    //Set the stage position we are moving to
    stagePosition.setXInNanometers(xPositionToMoveTo);
    stagePosition.setYInNanometers(yPositionToMoveTo);

    //Set the motion profile we will use
    PanelPositioner.getInstance().setMotionProfile(new MotionProfile(profile));

    //Move to the new location using the profile
    PanelPositioner.getInstance().pointToPointMoveAllAxes(stagePosition);
  }

  /**
   * Method to generate a pair of random X/Y points, set the stage to move
   * to those points and then move the stage to those point. It also stores
   * the pair of points in the list of motions.
   *
   * @author Reid Hayhow
   */
  protected void runRandomPointToPointMoves() throws XrayTesterException
  {
    Random random = new Random();

    //Get the range of motions that are acceptable. Random wants a 0 - MAX
    //value, and none of the min values can be assumed to be zero
    int randomXRange = _maxXInNanometers - _minXInNanometers;
    int randomYRange = _maxYInNanometers - _minYInNanometers;

    //Get the random number and scale it back up to be in the right range
    int xLocation = random.nextInt(randomXRange) + _minXInNanometers;
    int yLocation = random.nextInt(randomYRange) + _minYInNanometers;

    //Don't exceed the X location limits
    Assert.expect(xLocation <= _maxXInNanometers);
    Assert.expect(xLocation >= _minXInNanometers);

    //Don't exceed the Y location limits
    Assert.expect(yLocation <= _maxYInNanometers);
    Assert.expect(yLocation >= _minYInNanometers);

    //Everything is set-up and ready to go, so MOVE!
    moveStage(xLocation, yLocation, _motionProfile);
  }

  /**
   * method to save off all the config values for the System Fiducal cal before
   * we move the stage and get a new set of System Fiducial cal values.
   *
   * @author Reid Hayhow
   */
  private void saveSystemFiducialValues()
  {
    //NOTE:These values are added to a LinkedHashMap so the order of insertion
    //is preserved in the map. This order is then used to generate the results
    //that are stored to the logfile and provide the logical ordering there.

    //All values for Camera 0
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW);

    //All values for Camera 1
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW);

    //All values for Camera 2
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW);

    //All values for Camera 3
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW);

    //All values for Camera 4
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW);

    //All values for Camera 5
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW);

    //All values for Camera 6
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW);

    //All values for Camera 7
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW);

    //All values for Camera 8
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW);

    //All values for Camera 9
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW);

    //All values for Camera 10
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW);

    //All values for Camera 11
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW);

    //All values for Camera 12
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW);

    //All values for Camera 13
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN);
    addCalibValueToSavedMap(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW);
  }

  /**
   * Utility function to simplify adding values to the saved map of calibEnums
   *
   * @author Reid Hayhow
   */
  private void addCalibValueToSavedMap(HardwareCalibEnum hardwareCalibEnum)
  {
    _calibEnumToSavedValueMap.put(hardwareCalibEnum, _config.getIntValue(hardwareCalibEnum));
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * set-up code that may be reused
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays Cylinder to Up and Safe position.
        XrayActuator.getInstance().up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
