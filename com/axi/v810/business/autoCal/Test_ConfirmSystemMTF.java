package com.axi.v810.business.autoCal;

import java.awt.image.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author John Dutton
 */
public class Test_ConfirmSystemMTF extends UnitTest
{

  private Config _config = Config.getInstance();
  HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  /**
   * @author John Dutton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ConfirmSystemMTF());
  }

  /**
   * @author John Dutton
   */
  protected Test_ConfirmSystemMTF()
  {
    super();

    // Won't run on xRayTest machine without loading config files.  Has something
    // to do with xRayTest.startup() expecting the config files to be loaded.
    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
      Assert.expect(false, "Can't load config files");
    }
  }

  /**
   * @author John Dutton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // Create a test utils class and save original hardware calib file since last
    // timestamp will be written to it and the test harness expects no changes.
    TestUtils utils = new TestUtils();
    utils.backupHWCalib(getTestSourceDir());
    XrayCameraArray xRayCameraArray = null;

    try
    {
      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + File.separator + getClass().getSimpleName();
      xRayCameraArray = XrayCameraArray.getInstance();
      List<AbstractXrayCamera> xRayCameras = new ArrayList<AbstractXrayCamera>();
      AbstractXrayCamera xRayCamera = XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
      xRayCameras.add(xRayCamera);
      xRayCameraArray.enableUnitTestMode(xRayCameras, imageDataTestDirectory);
      CalGrayscale.getInstance().executeUnconditionally();

      //------------------------------------------------------------------
      // Test Case #1: the pass case using good projections
      //------------------------------------------------------------------

      // Get an instance of the test to run
      ConfirmLowMagSystemMTF confirm = ConfirmLowMagSystemMTF.getInstance();

      //Execute the test
      runConfirmation(confirm);

      //Check to make sure start and stop dates are set and status is passed
      checkForStartStopDatesAndStatus(confirm.getResult(), true);

      //For this test, it should pass, if not, dump failure info into .actual
      //and assert.
      if (confirm.didTestPass() == false)
      {
        displayFailureData(confirm.getResult(), confirm.getLimit(), os);
        Expect.expect(false, "System MTF Confirmation Test 1 Failed");
      }

      //------------------------------------------------------------------
      // Test Case #2: the fail case using poor quality projections (i.e. projections
      //               that were manually altered).
      //------------------------------------------------------------------

/*  Not going to fully implement test 2 until the confirmation has been verified
    and tweaked on working hardware.  The odds are too good that the test would need
    to be modified too.

      // Get an instance of the test to run
      confirm = ConfirmSystemMTF.getInstance();

      //Execute the test
      runConfirmation(confirm);

      //Check to make sure start and stop dates are set and statii are failed
      checkForStartStopDatesAndStatus(confirm.getResult(), false);

      //For this test, it should fail, if not, dump failure info into .actual
      //and assert.
      if (confirm.didTestPass() == true)
      {
        displayFailureData(confirm.getResult(), confirm.getLimit(), os);
        Expect.expect(false, "System MTF Confirmation Test 2 Failed");
      }
*/

    }
    catch (Exception e)
    {
      e.printStackTrace(os);
    }
    finally
    {
      if (xRayCameraArray != null)
        xRayCameraArray.disableUnitTestMode();

      if (utils != null)
        utils.restoreHWCalib(getTestSourceDir());
    }
  }

  /**
   * Convienence function to display any failure information.
   *
   * @author Reid Hayhow
   */
  private void displayFailureData(Result result, Limit limits, PrintWriter os)
  {
    List<Result> resultList = result.getResultList();
    List<Limit> limitsList = limits.getLimitList();
    Iterator<Limit> iter = limitsList.iterator();
    Assert.expect(resultList != null);
    if (resultList.isEmpty())
    {
      if (limits.didTestPass(result) == false)
      {
        os.println("result " + limits.getLimitName() + " failed!");
      }
    }
    else
    {
      for (Result subResult : resultList)
      {
        displayFailureData(subResult, iter.next(), os);
      }
    }
  }

  /**
   * Function to assure that the date and status have been set properly
   *
   * @author Reid Hayhow
   * @author John Dutton
   */
  private void checkForStartStopDatesAndStatus(Result result, boolean shouldPass)
  {
    List<Result> resultList = result.getResultList();

    //We are at a leaf node, check it
    if (resultList.isEmpty())
    {
      String passedString = null;
      if (shouldPass)
        passedString = StringLocalizer.keyToString("CD_PASSED_KEY");
      else
        passedString = StringLocalizer.keyToString("CD_FAILED_KEY");
      Assert.expect(result.getStartDate() != null);
      Assert.expect(result.getStopDate() != null);
      Assert.expect(result.getStatusString().equalsIgnoreCase(passedString));
    }
    else
    {
      for (Result subResult : resultList)
      {
        checkForStartStopDatesAndStatus(subResult, shouldPass);
      }
    }
  }

  /**
   * @author John Dutton
   */
  private void runConfirmation(HardwareTask hwTask) throws XrayTesterException
  {
    final HardwareTask cal = hwTask;
    if (UnitTest.unitTesting() == false)
    {
      _hardwareWorkerThread.invokeAndWait(new RunnableWithExceptions()
      {
        public void run() throws Exception
        {
          cal.executeNonInteractively();
        }
      });
    }
    else
    {
      cal.executeNonInteractively();
    }
  }

  /**
   * Routine used in development to convert the images output by ConfirmLowMagSystemMTF
   * into 1088 wide images that the camera simulators use and hence this regression
   * test can use.
   *
   * @author John Dutton
   */
  private void convertImagesFromTestMachineToCameraSimulatorImages() throws CouldNotReadFileException,
      FileDoesNotExistException,
      IOException,
      DatastoreException
  {
    String setName = "0";
    int count = 1;
    String testImagesDirectoryOut = getTestDataDir() + File.separator + "SystemMTFOutput";
    String testImagesDirectory = getTestDataDir() + File.separator + "SystemMTFInput";
    File dir = new File(testImagesDirectory);
    File[] fileList = dir.listFiles();
    for (File f : fileList)
    {
      BufferedImage bufferedImg = ImageIoUtil.loadPngBufferedImage(f.getCanonicalPath());
      Image img = Image.createFloatImageFromBufferedImage(bufferedImg);

      Image largerImg = new Image(1088, 1588);
      Paint.fillImage(largerImg, 255.f);
      for (int x = 0; x < 1084; x++)
        for (int y = 0; y < 1588; y++)
        {
          largerImg.setPixelValue(x, y, img.getPixelValue(x, y));
        }

      String countString = String.format("%2d", count);
      String name = "cam_" + setName + "_" + count + "_0_0_1087_1587.png";
      XrayImageIoUtil.savePngImage(largerImg.getBufferedImage(), testImagesDirectoryOut + File.separator + name);
      count++;

      countString = String.format("%2d", count);
      name = "cam_" + setName + "_" + count + "_0_0_1087_1587.png";
      XrayImageIoUtil.savePngImage(largerImg.getBufferedImage(), testImagesDirectoryOut + File.separator + name);
      count++;

      largerImg.decrementReferenceCount();
      img.decrementReferenceCount();
    }
  }
}
