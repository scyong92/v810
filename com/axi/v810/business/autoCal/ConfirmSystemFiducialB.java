package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Confirmation that measures the optical quality of the system fiducial B by measuring
 * the MTF of its diagonal edge by using the FWHM of its line spread function
 * as a proxy for MTF.  It also tests panel positioning by measuring the location
 * of its edges.   MTF is an optical measurement of how well an imaging system captures
 * contrasts between black and white lines.  The smaller the number, the better.
 *
 * System Fiducial B is on the moveable rail catercorner to the System Fiducial on
 * the fixed rail.
 *
 * @author John Dutton
 */
public class ConfirmSystemFiducialB extends ConfirmSharpness
{
  // Singleton class.
  private static ConfirmSystemFiducialB _instance;

  // Printable name for this confirmation.
  private static final String _NAME_OF_CAL =
      StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_FIDUCIAL_B_KEY");

  private static final int _FREQUENCY =
      _config.getIntValue(HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY);
  private static final long _TIMEOUTINSECONDS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_TIMEOUT_IN_SECONDS);

  private static final int _DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS =
      _config.getIntValue(HardwareCalibEnum.DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS);
  private static final int _DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS =
      _config.getIntValue(HardwareCalibEnum.DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS);

  private static final int _COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS =
      _config.getIntValue(HardwareCalibEnum.COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS);

  private static final int _REGION_TO_IMAGE_WIDTH_NANOMETERS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_REGION_TO_IMAGE_WIDTH_NANOMETERS);
  private static final int _REGION_TO_IMAGE_HEIGHT_NANOMETERS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_REGION_TO_IMAGE_HEIGHT_NANOMETERS);

  // Margins to use for requesting projections:
  private static final int _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS);
  private static final int _MARGIN_FOR_FOCUS_REGION_NANOMETERS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_MARGIN_FOR_FOCUS_REGION_NANOMETERS);

  private static final int _KEEP_Y_NANOMETERS_ABOVE_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_KEEP_Y_NANOMETERS_ABOVE_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH);


  private static final int _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS);
  private static final int _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_LEFT_WIDTH_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_LEFT_WIDTH_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_LEFT_HEIGHT_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_LEFT_HEIGHT_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS);

  // All below values should comes from SystemFiducialCoupon singleton, so that we can switch between normal and low mag easily in future.
   // System Fiducial Triangle on System Coupon is (1.27 mm x 2.54 mm x hypotenuse) right triangle.
//  private static final int _SYSFID_TRIANGLE_WIDTH_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS);
//  private static final int _SYSFID_TRIANGLE_HEIGHT_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS);

  // The following coordinates are based on the CAD drawing of the System Coupon.
//  private static final int _SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS);
//  private static final int _SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS =
//      _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS);

  // Use the following SW configurable value to determine bottom of system coupon to image.  This value
  // provides some extra margin so the lower edge of the quadrilaterial is easily seen.  Early  experiments
  // showed this to be necessary/wise.  The value is 1 mm greater than lowest visible dot on coupon according
  // to CAD drawing (19.85 mm + 1 mm or 19850000 nm + 1000000 nm = 20850000 nm).
  private static final int CONFIRM_SYSTEM_FIDUCIAL_B_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
     _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_B_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS);

  // The image is flipped in X & Y because of its orientation in the pocket.  Hence,
  // the lower right of the quadrilateral is the starting point and the order is counterclockwise
  // from there.
  private static List<IntCoordinate> _quadPointsInNanometers = new ArrayList<IntCoordinate>();

  // Once LP1 & LP2 range of motion error is resolved this can go away.
  private static final boolean _STAGE_MOTION_RANGE_OK_FOR_SYS_FID_B_CONFIRMATION =
    _config.getBooleanValue(HardwareConfigEnum.STAGE_MOTION_RANGE_OK_FOR_SYS_FID_B_CONFIRMATION);

  // Number of horizontal edges to average together
  private final int _noHorizontalEdges = 6;

  // Limits to compare to.
  private double _lineSpreadFWHMPixelsHighLimit;
  private double _lineSpreadFWHMPixelsLowLimit;

  private static SystemFiducialCoupon _systemCoupon = SystemFiducialCoupon.getInstance();

  /**
   * @author John Dutton
   */
  static
 {
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralLowerRightXinNanoMeters(),
                          _systemCoupon.getQuadrilateralLowerRightYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralLowerLeftXinNanoMeters(),
                          _systemCoupon.getQuadrilateralLowerLeftYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralUpperLeftXinNanoMeters(),
                          _systemCoupon.getQuadrilateralUpperLeftYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralUpperRightXinNanoMeters(),
                          _systemCoupon.getQuadrilateralUpperRightYinNanoMeters()));

   // Verify that region to image is not bigger than allowed.
   Assert.expect(_REGION_TO_IMAGE_WIDTH_NANOMETERS <= SetHardwareConstants.getMaxHardwareReconstructionRegionWidthInNanoMeters());
   Assert.expect(_REGION_TO_IMAGE_HEIGHT_NANOMETERS <= SetHardwareConstants.getMaxHardwareReconstructionRegionLengthInNanoMeters());
 }

 /**
   * @author John Dutton
   */
  private ConfirmSystemFiducialB()
  {
    super(_NAME_OF_CAL,
          _FREQUENCY,
          _TIMEOUTINSECONDS,
          Directory.getAdjustableRailImageQualityConfirmationLogDir(),
          FileName.getAdjustableRailImageQualityConfirmationLogFileName(),
          ProgressReporterEnum.CONFIRM_SYSTEM_FIDUCIAL_B_TASK);

    //This confirmation is to be run manually only
    _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;

    setTimestampEnum(HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_B_LAST_TIME_RUN);

  }

  /**
   * A singleton class.
   *
   * @author John Dutton
   */
  public static synchronized ConfirmSystemFiducialB getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmSystemFiducialB();
    }
    return _instance;
  }

  /**
   * If machine is LP1 or LP2 can't run this confirmation because of motion in x limitation.
   *
   * @author John Dutton
   */
  public boolean canRunOnThisHardware()
  {
    return _STAGE_MOTION_RANGE_OK_FOR_SYS_FID_B_CONFIRMATION;
  }

  /**
   * @author John Dutton
   */
  private static int getDistanceInXFromSysFidToLeftEdgeOfImageNanometers()
  {
    // The right angle point of the system fiducial is at x 12.66 mm in CAD drawing.
    // The farthest relevant point to the left is the upper right point on the quadrilateral
    // at x 5.42 mm.  The coupon and hence the CAD drawing is flipped in the coupon pocket
    // that is going to be imaged (i.e. flipped in X & Y).
    // So to have the left side of the image include the upper right point,
    // use the difference with the system fiducial to define the left side of the image,
    // along with a little margin.
    // With this definition for the image left side, the opposite upper point
    // on the quadrilateral will fit in the right side of the image with some margin remaining.

    int distanceBetweenSysFidToUpperRightNanometers =
        Math.abs(_systemCoupon.getQuadrilateralUpperRightXinNanometersInSystemFiducialCoordinates());
    return distanceBetweenSysFidToUpperRightNanometers +
           _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS;
  }

/**
 * @author John Dutton
 */
  private static int getDistanceInYFromSysFidToLowerRightNanometers()
  {
    // The right angle point of the system fiducial is at y 11.05 mm in CAD drawing.
    // The furthest dot above this point (marking visible region) is at y 19.85 mm.
    // (It is above because coupon flipped in Y.)
    // To have top of projection image be at the dot, use the difference to define the
    // top of the image and then derive bottom of image from image height.
    // (Note, sw config value for this confirmation uses a "custom" value with some margin for
    // the lower right visible dot.)
    int distanceInYFromSysFidToLowerRightNanometers =
        Math.abs(CONFIRM_SYSTEM_FIDUCIAL_B_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS -
                 _systemCoupon.getSystemFiducialPointYinNanoMeters());
    int heightBelowSysFidToImage = _REGION_TO_IMAGE_HEIGHT_NANOMETERS - distanceInYFromSysFidToLowerRightNanometers;
    Assert.expect(0 <= heightBelowSysFidToImage, "height of region to image is not great enough");
    return distanceInYFromSysFidToLowerRightNanometers;
  }

  /**
   * Calculates where the system fiducial is expected to be based on CAD values of the stage.
   * The tolerance stack will cause the actual to be different, hence a template match
   * will be performed to find the actual.
   *
   * @author John Dutton
   */
  private ImageCoordinate getPredictedSysFidPixel() throws DatastoreException
  {
    int actualPixelSizeXInNanometers = actualPixelSizeXInNanometers();
    int actualPixelSizeYInNanometers = actualPixelSizeYInNanometers();

    // System Fiducial location for use below with template match results.
    int predictedSysFidPixelX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(getDistanceInXFromSysFidToLeftEdgeOfImageNanometers()),
                                                    actualPixelSizeXInNanometers);

    // Since the coupon is flipped in Y, the distance from lower right is the same as from the top of
    // the image down to the System Fiducial point.
    int predictedSysFidPixelY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(getDistanceInYFromSysFidToLowerRightNanometers()),
                                                    actualPixelSizeYInNanometers);

    return new ImageCoordinate(predictedSysFidPixelX, predictedSysFidPixelY);
  }

  /**
   * @author John Dutton
   */
  private static int getRailWidth()
  {
    PanelHandler panelHandler = PanelHandler.getInstance();
    return (panelHandler.getMinimumPanelWidthInNanoMeters() +
            panelHandler.getMaximumPanelWidthInNanoMeters()) / 2;
  }



  /**
   * @author John Dutton
   */
  private static int getHeightBelowSysFidToImageNanometers()
  {
    int heightBelowSysFidToImageNanometers = _REGION_TO_IMAGE_HEIGHT_NANOMETERS - getDistanceInYFromSysFidToLowerRightNanometers();
    Assert.expect(0 <= heightBelowSysFidToImageNanometers, "height of region to image is not great enough");
    return heightBelowSysFidToImageNanometers;
  }




  /**
   * Calculates the region to be reconstructed, based on coordinates relative to the
   * system fiducial.
   *
   * @author John Dutton
   */
  public static PanelRectangle getImageRectangle()
  {
    int reconstructedRegionXInNanometers = -(_DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                             getDistanceInXFromSysFidToLeftEdgeOfImageNanometers());

    int reconstructedRegionYInNanometers = (_DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                            2 * _COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS +
                                            getRailWidth() -
                                            getHeightBelowSysFidToImageNanometers());


    PanelRectangle magCouponArea =
        new PanelRectangle(reconstructedRegionXInNanometers, reconstructedRegionYInNanometers,
                           _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                           _REGION_TO_IMAGE_HEIGHT_NANOMETERS);

    return magCouponArea;
  }

  /**
   * Calculates where to focus within the reconstruction region.  Coordinates are
   * relative to system fiducial.
   *
   * @author John Dutton
   */
  public static PanelRectangle getFocusRectangle()
  {
    // Need to focus on smaller region because of IRP limit.  Use area around system fid.
    int focusRegionXInNanometers = -(_DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                     _MARGIN_FOR_FOCUS_REGION_NANOMETERS);
    int focusRegionYInNanometers =  (_DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                    2 * _COUPON_INDENTATION_IN_Y_FROM_RAILS_NANOMETERS +
                                    getRailWidth() -
                                    _systemCoupon.getTriangleHeightInNanoMeters() -
                                    _MARGIN_FOR_FOCUS_REGION_NANOMETERS);
    int focusRegionWidthNanometers = _MARGIN_FOR_FOCUS_REGION_NANOMETERS +
                                     2 * _systemCoupon.getTriangleWidthInNanoMeters();
    int focusRegionHeightNanometers = _MARGIN_FOR_FOCUS_REGION_NANOMETERS +
                                      2 * _systemCoupon.getTriangleHeightInNanoMeters();

    // Verify focus region is no bigger than maximum supported on the IRP's.
    Assert.expect(focusRegionWidthNanometers <= SetHardwareConstants.getMaxFocusRegionWidthInNanoMeters());
    Assert.expect(focusRegionHeightNanometers <= SetHardwareConstants.getMaxFocusRegionLengthInNanoMeters());


    PanelRectangle focusRectangle =
        new PanelRectangle(focusRegionXInNanometers, focusRegionYInNanometers,
                           focusRegionWidthNanometers, focusRegionHeightNanometers);

    Assert.expect(getImageRectangle().contains(focusRectangle), "focus region is not contained within reconstructed image");

    return focusRectangle;
  }

  /**
   * Get reconstructed image, perform a template match and then hand image and
   * system fiducial location off for processing.
   *
   * @author John Dutton
   */
  public void doTheRealConfirmationWork() throws XrayTesterException
  {
    _lineSpreadFWHMPixelsHighLimit = _parsedLimits.getHighLimitDouble("LineSpreadFWHMPixels");
    _lineSpreadFWHMPixelsLowLimit = _parsedLimits.getLowLimitDouble("LineSpreadFWHMPixels");

    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    //Start the progress observable so users know what is happening
    initializeAndReportProgressPercentage();

    int actualPixelSizeXInNanometers = actualPixelSizeXInNanometers();
    int actualPixelSizeYInNanometers = actualPixelSizeYInNanometers();


    // Make sure panel is not loaded and adjust rail width.
    PanelHandler panelHandler = PanelHandler.getInstance();
    if (panelHandler.isPanelLoaded())
    {
      // Can't run with panel in system, fail confirmation and return.
      getResult().setResults(new Boolean(false));
      _result.setSuggestedUserActionMessage(getName() + " " +
                                            StringLocalizer.keyToString("CD_EXCEPTION_CONFIRM_SYSTEM_FIDUCIAL_B_CANNOT_HAVE_PANEL_IN_SYSTEM_KEY"));
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_UNABLE_TO_RUN_ADJUSTABLE_RAIL_IMAGE_CONFIRMATION_KEY"));
      return;
    }
    else
    {
      panelHandler.setRailWidthInNanoMeters(getRailWidth());
    }

    // Determine location of System Fiducial B.
    // Note, x & y for reconstruction images are relative to the system fiducial.
    //

    PanelRectangle imageRectangle =  getImageRectangle();
    PanelRectangle focusRectangle =  getFocusRectangle();

    increaseAndReportProgressPercentage(10);   // Show 10%

    // Get reconstructed image.
    Image image = null;
    if (readImageFromFileTrue())
      image = readImageFromFile("C:/temp", "testSysFidB.png");
    else
    {
      ImageRetriever ir = ImageRetriever.getInstance();
      // XCR-3353 Failed to run manual confirmation task on XXL system
      if(XrayTester.isXXLBased())
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.ADJUSTABLE_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                          MathUtil.convertMilsToNanoMeters(100),
                          MathUtil.convertMilsToNanoMeters(475));
      }
      else if(XrayTester.isS2EXEnabled())
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.ADJUSTABLE_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                          MathUtil.convertMilsToNanoMeters(100),
                          MathUtil.convertMilsToNanoMeters(375));
      }
      else
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.ADJUSTABLE_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                          MathUtil.convertMilsToNanoMeters(100),
                          MathUtil.convertMilsToNanoMeters(100));
      }
    }

    increaseAndReportProgressPercentage(40);   // Show 50%

    Assert.expect(image != null, "Didn't get an image so can't cut up edges.");
    saveIfSavingDebugImages(image, "reconstruction_img");

    // Save focus image
    if (saveDebugImages())
    {
      int focusRegionHeightInPixels = MathUtil.convertNanoMetersToPixelsUsingCeil(focusRectangle.getHeight(),
                                                                                  actualPixelSizeYInNanometers);
      int focusRegionWidthInPixels = MathUtil.convertNanoMetersToPixelsUsingCeil(focusRectangle.getWidth(),
                                                                                 actualPixelSizeXInNanometers);
      int focusRegionXInPixels =
          MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(imageRectangle.getMinX() -
                                                               focusRectangle.getMinX()),
                                                      actualPixelSizeXInNanometers);
      int distanceInPixelsFromImageBottomToBottomOfFocusRegion =
          MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(imageRectangle.getMinY() -
                                                               focusRectangle.getMinY()),
                                                      actualPixelSizeYInNanometers);
      int bottomOfFocusRegionInPixels = image.getHeight() - distanceInPixelsFromImageBottomToBottomOfFocusRegion;
      int focusRegionYInPixels = bottomOfFocusRegionInPixels - focusRegionHeightInPixels;
      RegionOfInterest focusROI =
          new RegionOfInterest(focusRegionXInPixels, focusRegionYInPixels, focusRegionWidthInPixels,
                               focusRegionHeightInPixels, 0, RegionShapeEnum.RECTANGULAR);

      //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
      checkIsRoiFitsWithinImage(focusROI, image);
      Image focusImg = Image.createCopy(image, focusROI);
      saveIfSavingDebugImages(focusImg, "focus_img");
      focusImg.decrementReferenceCount();
    }


    // Use template match to calculate offset from system fiducial point, i.e. by the code's
    // calculations it should be at x, y, but where is it actually in the image.  A lot of tolerances
    // stack up to determine this.
    int amountOfYToKeepNanometers = getHeightBelowSysFidToImageNanometers() +
                                    _KEEP_Y_NANOMETERS_ABOVE_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH;
    int amountOfYToKeepPixels =
          MathUtil.convertNanoMetersToPixelsUsingCeil(amountOfYToKeepNanometers,
                                                      actualPixelSizeYInNanometers);

    RegionOfInterest croppedROI =
        new RegionOfInterest(0, image.getHeight() - amountOfYToKeepPixels - 1,
                             image.getWidth(), amountOfYToKeepPixels,
                             0, RegionShapeEnum.RECTANGULAR);
    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(croppedROI, image);
    Image croppedReconstructedImage = Image.createCopy(image, croppedROI);
    ImageCoordinate sysFidPointLocation = SystemFiducialTemplateMatch.matchTemplate(croppedReconstructedImage, true, true);
    if (sysFidPointLocation.getX() == -1)
    {
      // Can't find system fiducial, so can't continue.   Fail confirmation and throw exception.
      setResultsToFail();
      throw
          new HardwareTaskExecutionException(
              ConfirmationExceptionEnum.CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH);
    }

    // Adjust Y because match was done in bottom half of reconstructed image.
    sysFidPointLocation.setY(sysFidPointLocation.getY() + (image.getHeight() - amountOfYToKeepPixels - 1));

    saveIfSavingDebugImages(croppedReconstructedImage, "cropped_reconstructed_img");
    croppedReconstructedImage.decrementReferenceCount();
    if (saveDebugImages())
    {
      Image template = SystemFiducialTemplateMatch.getTemplate(true, true);
      saveIfSavingDebugImages(template, "template_img");
      template.decrementReferenceCount();
    }

    increaseAndReportProgressPercentage(10);   // Show 60%

    ImageCoordinate predictedSysFidPixel = getPredictedSysFidPixel();
    processImage(image, _systemCoupon.getSystemFiducialPointYinNanoMeters() - getHeightBelowSysFidToImageNanometers(),
                 sysFidPointLocation.getX() - predictedSysFidPixel.getX(),
                 sysFidPointLocation.getY() - predictedSysFidPixel.getY());

    increaseAndReportProgressPercentage(30);   // Show 90%

    setResultMessages();

    if (didConfirmationPass() == false)
      saveImageOnConfirmationFailure(image, "reconstruction_img");

    image.decrementReferenceCount();

    addMagnificationInfo(MagnificationEnum.NOMINAL); // Khang Wah, add mgnification info into CD&A log
    reportProgressPercentageDone();
  }

  /**
   * @author John Dutton
   */
  void processImage(Image img, int farthestBottomVisibleYInNanometers, int sysFidPixelOffsetX,
                    int sysFidPixelOffsetY) throws DatastoreException, HardwareTaskExecutionException
  {
    // subtract margin in x because CAD x-values decrease going to the left
    int farthestLeftVisibleXInNanometers = _systemCoupon.getQuadrilateralUpperRightXinNanoMeters() - _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS;

    List<ImageCoordinate> listOfImageCoordinates =
        getQuadImageCoordinates(_quadPointsInNanometers,
                                farthestLeftVisibleXInNanometers,
                                farthestBottomVisibleYInNanometers,
                                img.getHeight(),
                                sysFidPixelOffsetX,
                                sysFidPixelOffsetY);

    // Process upper horizontal edge in 3 parts.
    ImageCoordinate upperLeftPixel = listOfImageCoordinates.get(0);

    // Need to synthesize upper right pixel since it was not captured in the actual image.
    // Use right side of actual image as X and use point-slope equation to calculate Y.
    double slopeUpperEdge = (_quadPointsInNanometers.get(1).getY() - _quadPointsInNanometers.get(0).getY()) /
                            (double)(_quadPointsInNanometers.get(1).getX() - _quadPointsInNanometers.get(0).getX());
    int farthestRightVisibleXInNanometers = farthestLeftVisibleXInNanometers + _REGION_TO_IMAGE_WIDTH_NANOMETERS;
    int synthesizedUpperRightYInNanometers = (int)
                                             (slopeUpperEdge * (farthestRightVisibleXInNanometers - _quadPointsInNanometers.get(0).getX())
                                              + _quadPointsInNanometers.get(0).getY());
    int distanceInPixelsYBetweenUpperLeftAndSynUpperRight =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(_quadPointsInNanometers.get(0).getY() -
        synthesizedUpperRightYInNanometers),
                                                    actualPixelSizeYInNanometers());
    int synthesizedUpperRightPixelY = upperLeftPixel.getY() + distanceInPixelsYBetweenUpperLeftAndSynUpperRight;
    int synthesizedUpperRightPixelX = img.getWidth() - 1;
    ImageCoordinate synthesizedUpperRightPixel =
        new ImageCoordinate(synthesizedUpperRightPixelX, synthesizedUpperRightPixelY);

    List<Image> listOfImages =
        getImagesOfHorizontalEdge(img, 3, upperLeftPixel, synthesizedUpperRightPixel,
                                  _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS, _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
    calculateMTFAndAddResult(listOfImages.get(0), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, left");
    calculateMTFAndAddResult(listOfImages.get(1), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, center");
    calculateMTFAndAddResult(listOfImages.get(2), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, right");

    for (Image image : listOfImages)
      image.decrementReferenceCount();

    // Process lower horizontal edge in 3 images.
    ImageCoordinate lowerLeftPixel = listOfImageCoordinates.get(3);

    // Need to synthesize lower right pixel since it was not captured in the actual image.
    // Use right side of actual image as X and use point-slope equation to calculate Y.
    double slopelLowerEdge = (_quadPointsInNanometers.get(2).getY() - _quadPointsInNanometers.get(3).getY()) /
                            (double)(_quadPointsInNanometers.get(2).getX() - _quadPointsInNanometers.get(3).getX());
    int synthesizedLowerRightYInNanometers = (int)
                                             (slopelLowerEdge * (farthestRightVisibleXInNanometers - _quadPointsInNanometers.get(3).getX())
                                              + _quadPointsInNanometers.get(3).getY());
    int distanceInPixelsYBetweenLowerLeftAndSynLowerRight =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(_quadPointsInNanometers.get(3).getY() -
        synthesizedLowerRightYInNanometers),
                                                    actualPixelSizeYInNanometers());
    int synthesizedLowerRightPixelY = lowerLeftPixel.getY() - distanceInPixelsYBetweenLowerLeftAndSynLowerRight;
    int synthesizedLowerRightPixelX = img.getWidth() - 1;
    ImageCoordinate synthesizedLowerRightPixel =
        new ImageCoordinate(synthesizedLowerRightPixelX, synthesizedLowerRightPixelY);

    listOfImages =
        getImagesOfHorizontalEdge(img, 3, lowerLeftPixel, synthesizedLowerRightPixel,
                                  _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS, _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
    calculateMTFAndAddResult(listOfImages.get(0), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "bottom line, left");
    calculateMTFAndAddResult(listOfImages.get(1), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "bottom line, center");
    calculateMTFAndAddResult(listOfImages.get(2), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "bottom line, right");

    for (Image image : listOfImages)
      image.decrementReferenceCount();


    // Process left vertical edge.
    Image verticalEdgeImg =
        getImageOfVerticalEdge(img, upperLeftPixel, lowerLeftPixel,
                               _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS,
                               _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS);
    calculateMTFAndAddResult(verticalEdgeImg, true, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "vertical edge, left");

    verticalEdgeImg.decrementReferenceCount();

    calculateMeanAndAddResult(0, _noHorizontalEdges, "horizontal edges mean");

  }

  /**
   * Set messages in the Result object. This info is available to any observer
   * who wants to display detailed failure information.
   *
   * @author John Dutton
   */
  private void setResultMessages()
  {
    // Set only if confirmation failed, otherwise let them default to empty strings.
    if(didConfirmationPass() == false)
    {
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_FIDUCIAL_B_FAILED_STATUS_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_FIDUCIAL_B_FAILED_ACTION_KEY"));
    }
  }

//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmSharpness will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
}
