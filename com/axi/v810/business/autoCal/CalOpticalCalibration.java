package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CalOpticalCalibration extends EventDrivenHardwareTask 
{    
    private static String _name =
      StringLocalizer.keyToString("CD_OPTICAL_CALIBRATION_ADJUSTMENT_NAME_KEY");
    
    //This task should always run
    private static int _frequency = 1;

    //There is no timeout for this task, it is manual
    private static long _timeoutInSeconds = 0;
    
    private PanelPositioner _panelPositioner = PanelPositioner.getInstance();
    private PanelHandler _panelHandler = PanelHandler.getInstance();
    private XrayActuatorInt _xrayActuator = XrayActuator.getInstance();
    private MotionProfile _panelPositionerSavedProfile;
    
    //Motion profile to use when moving stage, use the fastest
    protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
    
    //There should only be one logfile
    private int _maxLogFilesInDirectory = 1;

    //Directory Logger member
    private DirectoryLoggerAxi _logger;

    //Log directory where image files are saved
    private String _logDirectory = Directory.getXraySpotDeflectionCalLogDir();
    
    protected static Map<OpticalCameraIdEnum, AbstractOpticalCamera> _cameraIdToCamerasMap = new LinkedHashMap<OpticalCameraIdEnum, AbstractOpticalCamera>();
    protected Map<OpticalCameraIdEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>> _cameraToStagePositionMap = new LinkedHashMap<OpticalCameraIdEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>>();    
    protected static List<OpticalCalibrationProfileEnum> _opticalCalibrationProfiles = new ArrayList<OpticalCalibrationProfileEnum>();
    
    //Instance member
    private static CalOpticalCalibration _instance;
    
    private PspEngine _pspEngine = PspEngine.getInstance();
    
    /**
     * @author Cheah Lee Herng
     */
    private CalOpticalCalibration()
    {
        super(_name, _frequency, _timeoutInSeconds, ProgressReporterEnum.OPTICAL_CALIBRATION_ADJUSTMENT_TASK);
        
        //Create the directory logger
        _logger = new DirectoryLoggerAxi(_logDirectory,
                                         FileName.getXraySpotDeflectionLogFileBasename(),
                                         FileName.getLogFileExtension(),
                                         _maxLogFilesInDirectory);

        //This confirmation is automated
        _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;
        
        //Set the timestamp enum for later use
        setTimestampEnum(PspCalibEnum.OPTICAL_CALIBRATION_LAST_TIME_RUN);
    }
    
    /**
     * @author Cheah Lee Herng
    */
    public static synchronized CalOpticalCalibration getInstance()
    {
        if (_instance == null)
        {
          _instance = new CalOpticalCalibration();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public boolean canRunOnThisHardware() 
    {
        return true;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void cleanUp() throws XrayTesterException 
    {
        //restore the saved profile before continuing
        _panelPositioner.setMotionProfile(_panelPositionerSavedProfile);
        
        if (_cameraToStagePositionMap != null)
            _cameraToStagePositionMap.clear();
        if (_cameraIdToCamerasMap != null)
            _cameraIdToCamerasMap.clear();
        if (_opticalCalibrationProfiles != null)
            _opticalCalibrationProfiles.clear();
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void createLimitsObject() throws DatastoreException 
    {
        //Boolean limits are all null
        _limits = new Limit(null, null, null, null);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void createResultsObject() throws DatastoreException 
    {
        //Assume the task didn't run/pass to begin with
        _result = new Result(new Boolean(false), _name);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void setUp() throws XrayTesterException 
    {
        //Save off the active motion profile, just in case we end up moving the
        //stage and setting the profile to a point-to-point profile
        _panelPositionerSavedProfile = _panelPositioner.getActiveMotionProfile();
    }

    /**
     * @author Cheah Lee Herng
     */
    protected void executeTask() throws XrayTesterException 
    {                   
        try
        {
            // Report that we are starting this task
            reportProgress(5);
            
            if (PspConfiguration.getInstance().getOpticalCalibrationRailWidthInNanometers() > 0)
            {
              // For On-Stage calibration jig, we should not have any panel loaded.            
              if (ableToRunWithCurrentPanelState())
              {
                _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
                _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_PANEL_INTEFERRED_WITH_LIGHT_CONFIRM_KEY"));
                _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_REMOVE_THE_PANEL_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
                return;
              }
              
              PanelHandler.getInstance().setRailWidthInNanoMeters(PspConfiguration.getInstance().getOpticalCalibrationRailWidthInNanometers());
            }
            else
            {
              loadOpticalCalibrationJig();
              // Always move panel to left PIP
              if (_panelHandler.isPanelLoaded())
                _panelHandler.movePanelToLeftSide();
              //Handle the fact that we cannot run this task without a calibration
              //jig loaded into the system. Defer the task till later *but* don't cause the
              //task to fail. Display a meaningful message in the Service UI
              if (ableToRunWithCurrentPanelState() == false)
              {
                _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
                _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_NO_CALIBRATION_JIG_LOADED_KEY"));
                _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_LOAD_CALIBRATION_JIG_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
                return;
              }
            }
            
            //Handle a cancel gracefully. This will cause the task to fail
            if (_taskCancelled == true)
            {
                reportProgress(100);
                _result.setStatus(HardwareTaskStatusEnum.CANCELED);
                return;
            }
            
            //Setup camera list
            buildOpticalCameraList();

            if (_cameraIdToCamerasMap.isEmpty())
            {
              //Create a passing result
              String localizedTaskName = getName();
              _limits = new Limit(null, null, null, localizedTaskName);
              _result = new Result(new Boolean(true), localizedTaskName);
              return;
            }
                       
            reportProgress(40);
            
            // Call PspEngine to perform the actual calibration work
            _pspEngine.performCalibration();

            //If we get here, the task finished without exceptions and thus 'passed'
            _result = new Result(new Boolean(true), _name);

            // Report that we are done with this task
            reportProgress(100);
        }
        catch (XrayTesterException xte)
        {
          reportProgress(100);
          _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);
          _result.setTaskStatusMessage(xte.getLocalizedMessageWithoutHeaderAndFooter());
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_EXCEPTION_IN_OPTICAL_CALIBRATION_TASK_ACTION_KEY"));
          // The re-throw is needed to help the "catch (Exception e)" below.
          throw xte;
        }
        catch (Throwable t)
        {
          Assert.logException(t);
        }
        finally
        {           
          // Reload calibration data
          reloadCalibrationData();
          
          // XCR-3314 Support PSP New Manual-Loaded Jig Plate
          if(PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_1) ||
             PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4))
          {
            // Restore panel safety checking anyhow
            _panelHandler.unloadPanel();
          }
        }               
    }                
    
    /**
     * @author Cheah Lee Herng
     */
    private void buildOpticalCameraList()
    {
        _cameraIdToCamerasMap.clear();
        
        _cameraIdToCamerasMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_1, AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_1));
        _cameraIdToCamerasMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_2, AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_2));
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void reloadCalibrationData() throws XrayTesterException
    {
      Assert.expect(_cameraIdToCamerasMap != null);

      for(Map.Entry<OpticalCameraIdEnum, AbstractOpticalCamera> entry : _cameraIdToCamerasMap.entrySet())
      {
        AbstractOpticalCamera opticalCamera = entry.getValue();
        opticalCamera.loadCalibrationData(opticalCamera.getOpticalCalibrationDirectoryFullPath());        
      }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
    {
        // We need to have calibration jig loaded so that we 
        // can run this task.
        return (_panelHandler.isPanelLoaded() == true);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void loadOpticalCalibrationJig() throws XrayTesterException
    {
        if (_panelHandler.isPanelLoaded() == false)
        {
          if (XrayActuator.isInstalled())
          {
            if (XrayCylinderActuator.isInstalled() == true)
            {
              // Always move the cylinder to UP position
              _xrayActuator.up(false);
            }
            else if (XrayCPMotorActuator.isInstalled() == true)
            {
              // Always move the Z Axis to Home and Safe position
              _xrayActuator.home(false,false);
            }   
          }
          // comment by Wei Chin
          // should not check the safety sensor (due to it don't lower down the xray tube)
          // Please be aware if it need to run this board in high mag! 
          _panelHandler.loadPanel("Panel for Service Gui",
                                  PspConfiguration.getInstance().getOpticalCalibrationJigWidthInNanometers(),
                                  PspConfiguration.getInstance().getOpticalCalibrationJigHeightInNanometers(),
                                  false, 0);
        }
    }
    
    /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
