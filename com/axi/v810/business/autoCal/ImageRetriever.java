package com.axi.v810.business.autoCal;

import java.awt.geom.*;
import java.util.*;
import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Wrapper class that simplifies capturing an image by hiding threading and
 * the complexity of handling ImageAcquisitionEngine errors.  It also handles
 * TestProgram generation and when the generation happens.
 *
 * @author John Dutton
 */
public class ImageRetriever
{
  // Singleton class.
  private static ImageRetriever _instance;

  private ImageAcquisitionEngine _iae = ImageAcquisitionEngine.getInstance();

  private static Config _config = Config.getInstance();

  private boolean _beenInitialized = false;

  // This keeps track of the region of interest for CalMagnification.  It is generated during Test Program
  // generation.  Ideally, the ImageRetriever shouldn't need to know about this since it is tangential to
  // test programs and retrieving images.
  private int _regionIdToAnalyze    =  -1;

  // Test Program storage
  Map<ImageRetrieverTestProgramEnum, TestProgram> _mapTestPrograms =
      new HashMap<ImageRetrieverTestProgramEnum, TestProgram>();

  /**
   * @author John Dutton
   */
  private ImageRetriever()
  {
    // do nothing
  }

  /**
   * A singleton class.
   *
   * @author John Dutton
   */
  public static synchronized ImageRetriever getInstance()
  {
    if (_instance == null)
    {
      _instance = new ImageRetriever();
    }
    return _instance;
  }

  /**
   * Initialize the class with Test Programs.  Use this method to control when
   * this happens because Test Program generation causes project observable
   * events, which can negatively affect project state for an opened customer project.
   * Plan is to call this during startup before any project is loaded.
   *
   * ImageRetrieverTestProgramEnum is used to identify the TestPrograms.
   *
   * @author John Dutton
   */
  public void initTestPrograms()
  {
    _beenInitialized = true;

    TestProgram testProgram =
        createTestProgram(ConfirmMagnificationCoupon.getImageRectangle(),
                          ConfirmMagnificationCoupon.getFocusRectangle(), 
                          MagnificationTypeEnum.LOW);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.FIXED_RAIL_COUPON_IN_MAGNIFICATION_POCKET,
                         testProgram);
    
    testProgram =
        createTestProgram(ConfirmMagnificationCouponForHighMag.getImageRectangle(),
                          ConfirmMagnificationCouponForHighMag.getFocusRectangle(), 
                          MagnificationTypeEnum.HIGH);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.FIXED_RAIL_COUPON_IN_HIGH_MAGNIFICATION_POCKET,
                         testProgram);

    testProgram =
        createTestProgram(ConfirmSystemFiducial.getImageRectangle(),
                          ConfirmSystemFiducial.getFocusRectangle(),
                          MagnificationTypeEnum.LOW);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.FIXED_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                         testProgram);

    testProgram =
        createTestProgram(ConfirmSystemFiducialB.getImageRectangle(),
                          ConfirmSystemFiducialB.getFocusRectangle(),
                          MagnificationTypeEnum.LOW);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.ADJUSTABLE_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                         testProgram);
    
    testProgram =
        createTestProgramForCalMagnification(CalHighMagnification.getPanelRectangleToImage(),
                                             CalHighMagnification.getRectangleEnclosingAllRegions(), false);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.CAL_HIGH_MAGNIFICATION_IMAGE,
                         testProgram);
    
    testProgram =
        createTestProgramForCalMagnification(CalMagnification.getPanelRectangleToImage(),
                                             CalMagnification.getRectangleEnclosingAllRegions(), true);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.CAL_MAGNIFICATION_IMAGE,
                         testProgram);
    
    testProgram =
        createTestProgram(ConfirmHighMagSystemFiducial.getImageRectangle(),
                          ConfirmHighMagSystemFiducial.getFocusRectangle(),
                          MagnificationTypeEnum.HIGH);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.HIGH_MAG_FIXED_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                         testProgram);

    testProgram =
        createTestProgram(ConfirmHighMagSystemFiducialB.getImageRectangle(),
                          ConfirmHighMagSystemFiducialB.getFocusRectangle(),
                          MagnificationTypeEnum.HIGH);
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.HIGH_MAG_ADJUSTABLE_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                         testProgram);
    
    // Handle Optical System Offset Adjustment
    testProgram =
        createTestProgramForCalOpticalSystemOffsetAdjustment(CalOpticalSystemOffsetAdjustment.getPanelRectangleToImage(),
                                                             CalOpticalSystemOffsetAdjustment.getRectangleEnclosingAllRegions(),
                                                             true);
   
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.PSP_COUPON_IMAGE,
                         testProgram);
    
    testProgram =
        createTestProgramForXXLCalOpticalSystemOffsetAdjustment(CalOpticalSystemOffsetAdjustment.getPanelRectangleToImage(),
                                                                CalOpticalSystemOffsetAdjustment.getRectangleEnclosingAllRegions(),
                                                                true);
    
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.PSP_XXL_COUPON_IMAGE,
                         testProgram);
    
    testProgram =
        createTestProgramForS2EXCalOpticalSystemOffsetAdjustment(CalOpticalSystemOffsetAdjustment.getPanelRectangleToImage(),
                                                                CalOpticalSystemOffsetAdjustment.getRectangleEnclosingAllRegions(),
                                                                true);
   
    _mapTestPrograms.put(ImageRetrieverTestProgramEnum.PSP_S2EX_COUPON_IMAGE,
                         testProgram);
  }

  /**
   * Request an image based on the TestProgram associated with ImageRetrieverTestProgramEnum.
   * This association must already be setup in the initalize method of ImageRetrieverTestPrograms.
   * @author John Dutton
   */
  public Image getImage(ImageRetrieverTestProgramEnum imageRetrieverTestProgramEnum,
                        double zFocusHeightAboveNominalPlaneInNanometers,
                        double zFocusHeightBelowNominalPlaneInNanometers) throws XrayTesterException
  {
    Assert.expect(_beenInitialized, "initTestPrograms has not been called.");
    Assert.expect(imageRetrieverTestProgramEnum != null, "null imageRetrieverTestProgramEnum");
    Assert.expect(zFocusHeightAboveNominalPlaneInNanometers <=
                  XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers(), "slice is too high");
    Assert.expect(zFocusHeightBelowNominalPlaneInNanometers <=
                  Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()), "slice is too low");

    TestProgram testProgram = getTestProgram(imageRetrieverTestProgramEnum);
    Assert.expect(testProgram != null, "testProgram not found in map");

    return getImage(testProgram,
                    zFocusHeightAboveNominalPlaneInNanometers,
                    zFocusHeightBelowNominalPlaneInNanometers);
  }

  /**
   * Public so CalMagnification can have access.  CalMagnification does its own image retrieval.
   *
   * @author John Dutton
   */
  public TestProgram getTestProgram(ImageRetrieverTestProgramEnum imageRetrieverTestProgramEnum)
  {
    Assert.expect(_beenInitialized, "initTestPrograms has not been called.");
    Assert.expect(imageRetrieverTestProgramEnum != null, "null imageRetrieverTestProgramEnum");

    TestProgram testProgram = _mapTestPrograms.get(imageRetrieverTestProgramEnum);
    Assert.expect(testProgram != null, "testProgram not found in map");

    return testProgram;
  }

  /**
   * Interfaces with the Image Acquisition Engine to get a reconstructed
   * image of region specified in TestProgram.
   *
   *
   * @author John Dutton
   */
  private Image getImage(TestProgram testProgram,
                        double zFocusHeightAboveNominalPlaneInNanometers,
                        double zFocusHeightBelowNominalPlaneInNanometers)
      throws XrayTesterException
  {
    Image imageToReturn = null;

    // Start a threaded task that returns an image from the IAE.
    _iae.initialize();
    Callable<Image> getImageCallable = new getImageFromIAE();
    FutureTask<Image> getImageTask = new FutureTask<Image>(getImageCallable);
    Thread thread = new Thread(getImageTask);
    thread.start();

    // Request an image from IAE.
    try
    {
      requestImageFromIAE(testProgram,
                          zFocusHeightAboveNominalPlaneInNanometers,
                          zFocusHeightBelowNominalPlaneInNanometers);
    }
    catch (XrayTesterException ex1)
    {
      try
      {
        // Unblock the getImageTask so its thread and our thread won't be hung.
        abortImageAcquisitionFromIAE();
      }
      catch (XrayTesterException ex2)
      {
        // Not going to throw this second exception since first exception is more
        // interesting, but let's log it just in case it might be useful.
        ex2.printStackTrace();
      }
      throw ex1;
    }

    // Get image from threaded task.
    try
    {
      imageToReturn = getImageTask.get();
      if (imageToReturn == null)
      {
        // Image wasn't returned from IAE.  Instead of forcing caller to deal
        // with null return values throw an exception.
        ImageRetrieverBusinessException irException =
            new ImageRetrieverBusinessException();
        throw irException;
      }
    }
    catch (ExecutionException ex)
    {
      // Something went wrong in get.  Design doesn't allow throwing
      // non-XrayTesterExceptions, so wrap original exception.
      ImageRetrieverBusinessException irException =
          new ImageRetrieverBusinessException();
      irException.initCause(ex);
      throw irException;
    }
    catch (InterruptedException ex)
    {
      // Thread for getImageTask should never be interrupted, but it was.
      // Means there isn't an image to return, so throw an exception.
      ImageRetrieverBusinessException irException =
          new ImageRetrieverBusinessException();
      throw irException;
    }
    return imageToReturn;
  }

  /**
   * @author John Dutton
   */
  private void requestImageFromIAE(TestProgram testProgram,
                                   double zFocusHeightAboveNominalPlaneInNanometers,
                                   double zFocusHeightBelowNominalPlaneInNanometers)
      throws XrayTesterException
  {
    Assert.expect(testProgram.getVerificationRegions().size() == 1,
                  "not requesting the expected number of reconstructed images");

    TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
    _iae.acquireCouponImages(testProgram,
                             testExecutionTimer,
                             zFocusHeightAboveNominalPlaneInNanometers,
                             zFocusHeightBelowNominalPlaneInNanometers,
                             0);
  }

  /**
   * @author John Dutton
   */
  private void abortImageAcquisitionFromIAE() throws XrayTesterException
  {
    _iae.abort();
  }

  /**
   * Inner class that handles interactions with ImageAcquisitionEngine for
   * getting images.  Implements Callable so it can be wrapped in a FutureTask.
   *
   * @author John Dutton
   */
  private class getImageFromIAE implements Callable<Image>
  {
    public Image call() throws Exception
    {
      Image imageToReturn = null;
      BooleanRef noImagesReturned = new BooleanRef(false);

      ReconstructedImages reconstructedImage =
          _iae.getReconstructedImages(noImagesReturned);

      if (noImagesReturned.getValue())
      {
        // Valid situation that happens when IAE has been aborted, forcing the
        // getReconstructedImages to unblock.
        return null;
      }
      else
      {
        // Get the image from the slice and make sure there is only one slice.
        Assert.expect(reconstructedImage.getNumberOfSlices() == 1);
        ReconstructedSlice slice = reconstructedImage.getFirstSlice();
        imageToReturn = slice.getImage();
        imageToReturn.incrementReferenceCount();
        ReconstructionRegion reconstructionRegion = reconstructedImage.getReconstructionRegion();
        reconstructedImage.decrementReferenceCount();

        if (Config.isDeveloperDebugModeOn())
        {
          int sliceHeight = slice.getHeightInNanometers();
          int sliceHeightMils = (int)MathUtil.convertUnits(sliceHeight,
                                                           MathUtilEnum.NANOMETERS,
                                                           MathUtilEnum.MILS);
          System.out.println("SLICE HEIGHT for sharpness confirmation is " +
                             sliceHeight +
                             " nm which is " +
                             sliceHeightMils +
                             " mils.");
        }

        // Tell IAE we won't be asking for more images from reconstructedImage.
        // This frees projections from IRP's.
        if (_iae.isAbortInProgress() == false)
          _iae.reconstructionRegionComplete(reconstructionRegion, true);
        _iae.freeReconstructedImages(reconstructionRegion);
      }
      return imageToReturn;
    }
  }

  /**
   * Entry point for experimentation purposes only.  The proper, production code
   * approach is to set up test programs in ImageRetriverTestPrograms and then
   * call ImageRetriever to get the image.  The reason you don't want to use
   * this method for anything other than experimentation is because it creates
   * a TestProgram on the fly which will impact the currently loaded Project
   * through the ProjectObservable.
   *
   * @param reconstructedRegion rectangle to image in panel coordinates relative
   *   to the system fiducial.
   * @param focusRegion rectangle on reconstructedRegion to focus on in panel
   *   coordinates, also in panel coordinates relative to the system fiducial.
   * @author John Dutton
   */
 Image getImage(PanelRectangle reconstructedRegion,
                        PanelRectangle focusRegion,
                        double zFocusHeightAboveNominalPlaneInNanometers,
                        double zFocusHeightBelowNominalPlaneInNanometers)
      throws XrayTesterException
  {
    Assert.expect(reconstructedRegion != null && focusRegion != null, "null PanelRectangle");
    Assert.expect(zFocusHeightAboveNominalPlaneInNanometers <=
                  XrayTester.getMaximumSliceHeightFromNominalSliceInNanometers(), "slice is too high");
    Assert.expect(zFocusHeightBelowNominalPlaneInNanometers <=
                  Math.abs(XrayTester.getMinimumSliceHeightFromNominalSliceInNanometers()), "slice is too low");
    TestProgram testProgram = createTestProgram(reconstructedRegion, focusRegion, MagnificationTypeEnum.LOW);
    return getImage(testProgram,
                    zFocusHeightAboveNominalPlaneInNanometers,
                    zFocusHeightBelowNominalPlaneInNanometers);
  }

  /**
    * Creates a test program containing one verification region that images
    * the reconstructed region specified. Auto-focus is run using the SHARPEST
    * method.
    *
    * This method is protected instead of private so that ImageRetrieverBackEnd
    * can get images based on a reconstructedRegion and focusRegion for
    * experimentation purposes.  See ImageRetrieverBackEnd.
    *
    * @author Dave Ferguson
    * @author John Dutton
    */
   private TestProgram createTestProgram(PanelRectangle reconstructedRegion,
                                         PanelRectangle focusRegion, 
                                         MagnificationTypeEnum magnificationType)
   {
     Project dummyProject = new Project(true);

     dummyProject.setName("ImageRetrieverForConfirmation");

     dummyProject.getProjectState().disable();
     dummyProject.getProjectState().enable();

     Panel dummyPanel = new Panel();
     dummyPanel.setProject(dummyProject);
     dummyPanel.setWidthInNanoMeters(25400);
     dummyPanel.setLengthInNanoMeters(25400);
     if(XrayTester.isXXLBased())
     {
       dummyPanel.setThicknessInNanoMeters(14935200);
     }
     else if(XrayTester.isS2EXEnabled())
     {
       dummyPanel.setThicknessInNanoMeters(7000000);
     }
     else
       dummyPanel.setThicknessInNanoMeters(25400);
     PanelSettings dummyPanelSettings = new PanelSettings();
     dummyPanelSettings.setPanel(dummyPanel);
     dummyPanelSettings.setDegreesRotationRelativeToCad(0);
     dummyPanel.setPanelSettings(dummyPanelSettings);
     AffineTransform transform = new AffineTransform();
     dummyPanelSettings.setRightManualAlignmentTransform(transform);
     dummyProject.setPanel(dummyPanel);

     // Create here so can set test sub-program of reconstruction regions below.
     TestProgram program = new TestProgram();
     program.setProject(dummyProject);
     TestSubProgram tsp = new TestSubProgram(program);
     tsp.setMagnificationType(magnificationType);
     
     Collection<ReconstructionRegion> reconstructionRegions =
      new ArrayList<ReconstructionRegion>();

     // System Fiducial Reconstruction Region
     ReconstructionRegion rrSysFid = new ReconstructionRegion();
     rrSysFid.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
     rrSysFid.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
     rrSysFid.setTopSide(false);
     rrSysFid.setRegionRectangleRelativeToPanelInNanoMeters(reconstructedRegion);
     rrSysFid.setFocusPriority(FocusPriorityEnum.FIRST);
     rrSysFid.setTestSubProgram(tsp);

     FocusGroup fgSysFid = new FocusGroup();
     fgSysFid.setSingleSidedRegionOfBoard(true);
     fgSysFid.setRelativeZoffsetFromSurfaceInNanoMeters(0);
     FocusSearchParameters fspSysFid = new FocusSearchParameters();
     fspSysFid.setFocusProfileShape(FocusProfileShapeEnum.PEAK);
     FocusRegion frSysFid = new FocusRegion();
     frSysFid.setRectangleRelativeToPanelInNanoMeters(focusRegion);
     fspSysFid.setFocusRegion(frSysFid);
     fgSysFid.setFocusSearchParameters(fspSysFid);
     FocusInstruction fiSysFid = new FocusInstruction();
     fiSysFid.setFocusMethod(FocusMethodEnum.SHARPEST);
     Slice sliceSysFid = new Slice();
     sliceSysFid.setSliceName(SliceNameEnum.PAD);
     sliceSysFid.setCreateSlice(true);
     sliceSysFid.setFocusInstruction(fiSysFid);
     sliceSysFid.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
     fgSysFid.addSlice(sliceSysFid);
     rrSysFid.addFocusGroup(fgSysFid);
     reconstructionRegions.add(rrSysFid);

     // Add verification regions to bypass auto-alignment
     tsp.setVerificationRegions(reconstructionRegions);
     tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
     tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

     tsp.setVerificationRegionsBoundsInNanoMeters(reconstructedRegion);
     tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);
     PanelRectangle panelRectangle = tsp.getVerificationRegionsBoundsInNanoMeters();
     List<ProcessorStrip> processorStrips = ImagingChainProgramGenerator.createProcessorStripsForIrps(panelRectangle);
     tsp.setProcessorStripsForVerificationRegions(processorStrips);
     program.addTestSubProgram(tsp);

     Assert.expect(program != null, "null program returned");
     return program;
   }

   /**
    * Return region id of region of interest for CalMagnification.  It is generated during Test Program
    * generation.  Ideally, the ImageRetriever shouldn't need to know about this since it is tangential to
    * test programs and retrieving images.
    * @author John Dutton
    */
   public int getRegionIdToAnalyze()
   {
     return _regionIdToAnalyze;
   }

   /**
    * @author Dave Ferguson
    * @author Eddie Williamson
    */
   private TestProgram createTestProgramForCalMagnification(PanelRectangle panelRectangleToImage,
       PanelRectangle rectangleEnclosingAllRegions, boolean isLowMagnification)
   {
     ReconstructionRegion rrSysFid;
     FocusGroup fgSysFid;
     FocusSearchParameters fspSysFid;
     FocusRegion frSysFid;
     FocusInstruction fiSysFid;
     Slice sliceSysFid;
     
     ReconstructionRegion.resetRegionCounter();
     TestProgram program = new TestProgram();
     Project dummyProject = new Project(true);
     dummyProject.setName("ImageRetrieverForConfirmation");

     dummyProject.getProjectState().disable();
     dummyProject.getProjectState().enable();

     Panel dummyPanel = new Panel();
     dummyPanel.setProject(dummyProject);

     // A 1 mil cube
     dummyPanel.setWidthInNanoMeters(25400);
     dummyPanel.setLengthInNanoMeters(25400);
     dummyPanel.setThicknessInNanoMeters(25400);

     PanelSettings dummyPanelSettings = new PanelSettings();
     dummyPanelSettings.setPanel(dummyPanel);
     dummyPanelSettings.setDegreesRotationRelativeToCad(0);
     dummyPanel.setPanelSettings(dummyPanelSettings);
     AffineTransform transform = new AffineTransform();
     dummyPanelSettings.setRightManualAlignmentTransform(transform);
     dummyProject.setPanel(dummyPanel);
     TestSubProgram tsp = new TestSubProgram(program);

     Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();  
     
     if(isLowMagnification)
     {
       tsp.setMagnificationType(MagnificationTypeEnum.LOW);
       ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
     }
     else
     {
       tsp.setMagnificationType(MagnificationTypeEnum.HIGH);
       ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH); 
     }
     
     //---------------------------------------------------------------------
     // Create the reconstruction region large enough to contain all the area
     // needed to ensure the projection data is available for the smaller
     // regions as the mag is varied during the test.
     //---------------------------------------------------------------------
     rrSysFid = new ReconstructionRegion();
     rrSysFid.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
     rrSysFid.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
     rrSysFid.setTopSide(false);
     rrSysFid.setRegionRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
     rrSysFid.setFocusPriority(FocusPriorityEnum.FIRST);
     rrSysFid.setTestSubProgram(tsp);
     fgSysFid = new FocusGroup();
     fgSysFid.setSingleSidedRegionOfBoard(true);
     fgSysFid.setRelativeZoffsetFromSurfaceInNanoMeters(0);
     fspSysFid = new FocusSearchParameters();
     fspSysFid.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
     frSysFid = new FocusRegion();
     frSysFid.setRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
     fspSysFid.setFocusRegion(frSysFid);
     fgSysFid.setFocusSearchParameters(fspSysFid);
     fiSysFid = new FocusInstruction();
     fiSysFid.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
     fiSysFid.setZHeightInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(0));
     sliceSysFid = new Slice();
     sliceSysFid.setSliceName(SliceNameEnum.PAD);
     sliceSysFid.setCreateSlice(true);
     sliceSysFid.setFocusInstruction(fiSysFid);
     sliceSysFid.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
     fgSysFid.addSlice(sliceSysFid);
     rrSysFid.addFocusGroup(fgSysFid);
     reconstructionRegions.add(rrSysFid);
     //---------------------------------------------------------------------
     //    ++_numImagesRequested;  // Don't increment here. Usually only want the images below.
     // For the one case where we want this one also we add 1 to the loop count


     //---------------------------------------------------------------------
     // System Fiducial Reconstruction Region
     rrSysFid = new ReconstructionRegion();
     rrSysFid.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
     rrSysFid.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
     rrSysFid.setTopSide(false);
     _regionIdToAnalyze = rrSysFid.getRegionId();

     rrSysFid.setRegionRectangleRelativeToPanelInNanoMeters(panelRectangleToImage);
     rrSysFid.setFocusPriority(FocusPriorityEnum.FIRST);
     rrSysFid.setTestSubProgram(tsp);
     fgSysFid = new FocusGroup();
     fgSysFid.setSingleSidedRegionOfBoard(true);
     fgSysFid.setRelativeZoffsetFromSurfaceInNanoMeters(0);
     fspSysFid = new FocusSearchParameters();
     fspSysFid.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
     frSysFid = new FocusRegion();
     frSysFid.setRectangleRelativeToPanelInNanoMeters(panelRectangleToImage);
     fspSysFid.setFocusRegion(frSysFid);
     fgSysFid.setFocusSearchParameters(fspSysFid);
     fiSysFid = new FocusInstruction();
     fiSysFid.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);

     // Calculate z height for this rr/slice
     fiSysFid.setZHeightInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(0));

     sliceSysFid = new Slice();
     sliceSysFid.setSliceName(SliceNameEnum.PAD);
     sliceSysFid.setCreateSlice(true);
     sliceSysFid.setFocusInstruction(fiSysFid);
     sliceSysFid.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
     fgSysFid.addSlice(sliceSysFid);
     rrSysFid.addFocusGroup(fgSysFid);
     reconstructionRegions.add(rrSysFid);

     program.setProject(dummyProject);

     // Add verification regions to bypass auto-alignment
     tsp.setVerificationRegions(reconstructionRegions);
     tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
     tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

     // All reconstruction regions must be contained inside this rectangle
     tsp.setVerificationRegionsBoundsInNanoMeters(rectangleEnclosingAllRegions);
     tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);
     PanelRectangle panelRectangle = tsp.getVerificationRegionsBoundsInNanoMeters();
     List<ProcessorStrip> processorStrips = ImagingChainProgramGenerator.createProcessorStripsForIrps(panelRectangle);
     tsp.setProcessorStripsForVerificationRegions(processorStrips);
     program.addTestSubProgram(tsp);
     ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
     return program;
   }
   
  /**
   * @author Ying-Huan.Chu
   */
  private TestProgram createTestProgramForCalOpticalSystemOffsetAdjustment(PanelRectangle panelRectangleToImage,
                                                                           PanelRectangle rectangleEnclosingAllRegions, boolean isLowMagnification)
  {    
    FocusGroup fgSysFid;
    FocusSearchParameters fspSysFid;
    FocusRegion frSysFid;
    FocusInstruction fiSysFid;
    Slice sliceSysFid;

    ReconstructionRegion.resetRegionCounter();
    TestProgram program = new TestProgram();
    Project dummyProject = new Project(true);
    dummyProject.setName("ImageRetrieverForConfirmation");

    dummyProject.getProjectState().disable();
    dummyProject.getProjectState().enable();

    Panel dummyPanel = new Panel();
    dummyPanel.setProject(dummyProject);

    // A 1 mil cube
    dummyPanel.setWidthInNanoMeters(25400);
    dummyPanel.setLengthInNanoMeters(25400);
    dummyPanel.setThicknessInNanoMeters(25400);

    PanelSettings dummyPanelSettings = new PanelSettings();
    dummyPanelSettings.setPanel(dummyPanel);
    dummyPanelSettings.setDegreesRotationRelativeToCad(0);
    dummyPanel.setPanelSettings(dummyPanelSettings);
    AffineTransform transform = new AffineTransform();
    dummyPanelSettings.setRightManualAlignmentTransform(transform);
    dummyProject.setPanel(dummyPanel);
    TestSubProgram tsp = new TestSubProgram(program);

    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();  

    if(isLowMagnification)
    {
      tsp.setMagnificationType(MagnificationTypeEnum.LOW);
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    }
    else
    {
      tsp.setMagnificationType(MagnificationTypeEnum.HIGH);
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH); 
    }

    for(int zHeight=-100; zHeight < -70; ++zHeight)
    {
      ReconstructionRegion rrSysFid = new ReconstructionRegion();
      rrSysFid.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
      rrSysFid.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      rrSysFid.setTopSide(false);
      rrSysFid.setRegionRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
      rrSysFid.setFocusPriority(FocusPriorityEnum.FIRST);
      rrSysFid.setTestSubProgram(tsp);
      
      fgSysFid = new FocusGroup();
      fgSysFid.setSingleSidedRegionOfBoard(true);
      fgSysFid.setRelativeZoffsetFromSurfaceInNanoMeters(0);
      
      fspSysFid = new FocusSearchParameters();
      fspSysFid.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
      
      frSysFid = new FocusRegion();
      frSysFid.setRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
      fspSysFid.setFocusRegion(frSysFid);
      fgSysFid.setFocusSearchParameters(fspSysFid);
      
      fiSysFid = new FocusInstruction();
      fiSysFid.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
      fiSysFid.setZHeightInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(zHeight));
      
      sliceSysFid = new Slice();
      sliceSysFid.setSliceName(SliceNameEnum.PAD);
      sliceSysFid.setCreateSlice(true);
      sliceSysFid.setFocusInstruction(fiSysFid);
      sliceSysFid.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      
      fgSysFid.addSlice(sliceSysFid);
      rrSysFid.addFocusGroup(fgSysFid);
      reconstructionRegions.add(rrSysFid);
    }       

    program.setProject(dummyProject);
    
    // Add User-Gain
    // XCR-3769 Run optical system offset adjustment on XXL get dark image
    tsp.setVerificationUserGain(UserGainEnum.ONE);

    // Add verification regions to bypass auto-alignment
    tsp.setVerificationRegions(reconstructionRegions);
    tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
    tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

    // All reconstruction regions must be contained inside this rectangle
    tsp.setVerificationRegionsBoundsInNanoMeters(rectangleEnclosingAllRegions);
    tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);
    PanelRectangle panelRectangle = tsp.getVerificationRegionsBoundsInNanoMeters();
    List<ProcessorStrip> processorStrips = ImagingChainProgramGenerator.createProcessorStripsForIrps(panelRectangle);
    tsp.setProcessorStripsForVerificationRegions(processorStrips);
    program.addTestSubProgram(tsp);
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    return program;
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  private TestProgram createTestProgramForXXLCalOpticalSystemOffsetAdjustment(PanelRectangle panelRectangleToImage,
                                                                              PanelRectangle rectangleEnclosingAllRegions, boolean isLowMagnification)
  {
    FocusGroup fgSysFid;
    FocusSearchParameters fspSysFid;
    FocusRegion frSysFid;
    FocusInstruction fiSysFid;
    Slice sliceSysFid;

    ReconstructionRegion.resetRegionCounter();
    TestProgram program = new TestProgram();
    Project dummyProject = new Project(true);
    dummyProject.setName("ImageRetrieverForConfirmation");

    dummyProject.getProjectState().disable();
    dummyProject.getProjectState().enable();

    Panel dummyPanel = new Panel();
    dummyPanel.setProject(dummyProject);

    // A 1 mil cube
    dummyPanel.setWidthInNanoMeters(25400);
    dummyPanel.setLengthInNanoMeters(25400);
    dummyPanel.setThicknessInNanoMeters(25400);

    PanelSettings dummyPanelSettings = new PanelSettings();
    dummyPanelSettings.setPanel(dummyPanel);
    dummyPanelSettings.setDegreesRotationRelativeToCad(0);
    dummyPanel.setPanelSettings(dummyPanelSettings);
    AffineTransform transform = new AffineTransform();
    dummyPanelSettings.setRightManualAlignmentTransform(transform);
    dummyProject.setPanel(dummyPanel);
    TestSubProgram tsp = new TestSubProgram(program);

    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();  

    if(isLowMagnification)
    {
      tsp.setMagnificationType(MagnificationTypeEnum.LOW);
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    }
    else
    {
      tsp.setMagnificationType(MagnificationTypeEnum.HIGH);
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH); 
    }

    for(int zHeight=90; zHeight < 130; ++zHeight)
    {
      ReconstructionRegion rrSysFid = new ReconstructionRegion();
      rrSysFid.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
      rrSysFid.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      rrSysFid.setTopSide(false);
      rrSysFid.setRegionRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
      rrSysFid.setFocusPriority(FocusPriorityEnum.FIRST);
      rrSysFid.setTestSubProgram(tsp);
      
      fgSysFid = new FocusGroup();
      fgSysFid.setSingleSidedRegionOfBoard(true);
      fgSysFid.setRelativeZoffsetFromSurfaceInNanoMeters(0);
      
      fspSysFid = new FocusSearchParameters();
      fspSysFid.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
      
      frSysFid = new FocusRegion();
      frSysFid.setRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
      fspSysFid.setFocusRegion(frSysFid);
      fgSysFid.setFocusSearchParameters(fspSysFid);
      
      fiSysFid = new FocusInstruction();
      fiSysFid.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
      fiSysFid.setZHeightInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(zHeight));
      
      sliceSysFid = new Slice();
      sliceSysFid.setSliceName(SliceNameEnum.PAD);
      sliceSysFid.setCreateSlice(true);
      sliceSysFid.setFocusInstruction(fiSysFid);
      sliceSysFid.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      
      fgSysFid.addSlice(sliceSysFid);
      rrSysFid.addFocusGroup(fgSysFid);
      reconstructionRegions.add(rrSysFid);
    }       

    program.setProject(dummyProject);
    
    // Add User-Gain
    // XCR-3769 Run optical system offset adjustment on XXL get dark image
    tsp.setVerificationUserGain(UserGainEnum.SIX);

    // Add verification regions to bypass auto-alignment
    tsp.setVerificationRegions(reconstructionRegions);
    tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
    tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

    // All reconstruction regions must be contained inside this rectangle
    tsp.setVerificationRegionsBoundsInNanoMeters(rectangleEnclosingAllRegions);
    tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);
    PanelRectangle panelRectangle = tsp.getVerificationRegionsBoundsInNanoMeters();
    List<ProcessorStrip> processorStrips = ImagingChainProgramGenerator.createProcessorStripsForIrps(panelRectangle);
    tsp.setProcessorStripsForVerificationRegions(processorStrips);
    program.addTestSubProgram(tsp);
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    return program;
  }
  
  /**
   * @author Swee-Yee.Wong
   */
  private TestProgram createTestProgramForS2EXCalOpticalSystemOffsetAdjustment(PanelRectangle panelRectangleToImage,
                                                                           PanelRectangle rectangleEnclosingAllRegions, boolean isLowMagnification)
  {    
    FocusGroup fgSysFid;
    FocusSearchParameters fspSysFid;
    FocusRegion frSysFid;
    FocusInstruction fiSysFid;
    Slice sliceSysFid;

    ReconstructionRegion.resetRegionCounter();
    TestProgram program = new TestProgram();
    Project dummyProject = new Project(true);
    dummyProject.setName("ImageRetrieverForConfirmation");

    dummyProject.getProjectState().disable();
    dummyProject.getProjectState().enable();

    Panel dummyPanel = new Panel();
    dummyPanel.setProject(dummyProject);

    // A 1 mil cube
    dummyPanel.setWidthInNanoMeters(25400);
    dummyPanel.setLengthInNanoMeters(25400);
    dummyPanel.setThicknessInNanoMeters(25400);

    PanelSettings dummyPanelSettings = new PanelSettings();
    dummyPanelSettings.setPanel(dummyPanel);
    dummyPanelSettings.setDegreesRotationRelativeToCad(0);
    dummyPanel.setPanelSettings(dummyPanelSettings);
    AffineTransform transform = new AffineTransform();
    dummyPanelSettings.setRightManualAlignmentTransform(transform);
    dummyProject.setPanel(dummyPanel);
    TestSubProgram tsp = new TestSubProgram(program);

    Collection<ReconstructionRegion> reconstructionRegions = new ArrayList<ReconstructionRegion>();  

    if(isLowMagnification)
    {
      tsp.setMagnificationType(MagnificationTypeEnum.LOW);
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    }
    else
    {
      tsp.setMagnificationType(MagnificationTypeEnum.HIGH);
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH); 
    }

    for(int zHeight=20; zHeight < 50; ++zHeight)
    {
      ReconstructionRegion rrSysFid = new ReconstructionRegion();
      rrSysFid.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.VERIFICATION);
      rrSysFid.setReconstructionEngineId(ImageReconstructionEngineEnum.IRE0);
      rrSysFid.setTopSide(false);
      rrSysFid.setRegionRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
      rrSysFid.setFocusPriority(FocusPriorityEnum.FIRST);
      rrSysFid.setTestSubProgram(tsp);
      
      fgSysFid = new FocusGroup();
      fgSysFid.setSingleSidedRegionOfBoard(true);
      fgSysFid.setRelativeZoffsetFromSurfaceInNanoMeters(0);
      
      fspSysFid = new FocusSearchParameters();
      fspSysFid.setFocusProfileShape(FocusProfileShapeEnum.NO_SEARCH_REFERENCE_PLANE);
      
      frSysFid = new FocusRegion();
      frSysFid.setRectangleRelativeToPanelInNanoMeters(rectangleEnclosingAllRegions);
      fspSysFid.setFocusRegion(frSysFid);
      fgSysFid.setFocusSearchParameters(fspSysFid);
      
      fiSysFid = new FocusInstruction();
      fiSysFid.setFocusMethod(FocusMethodEnum.USE_Z_HEIGHT);
      fiSysFid.setZHeightInNanoMeters(MathUtil.convertMilsToNanoMetersInteger(zHeight));
      
      sliceSysFid = new Slice();
      sliceSysFid.setSliceName(SliceNameEnum.PAD);
      sliceSysFid.setCreateSlice(true);
      sliceSysFid.setFocusInstruction(fiSysFid);
      sliceSysFid.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
      
      fgSysFid.addSlice(sliceSysFid);
      rrSysFid.addFocusGroup(fgSysFid);
      reconstructionRegions.add(rrSysFid);
    }       

    program.setProject(dummyProject);
    
    // Add User-Gain
    // XCR-3769 Run optical system offset adjustment on XXL get dark image
    tsp.setVerificationUserGain(UserGainEnum.FIVE);

    // Add verification regions to bypass auto-alignment
    tsp.setVerificationRegions(reconstructionRegions);
    tsp.setInspectionRegions(new ArrayList<ReconstructionRegion>());
    tsp.setAlignmentRegions(new ArrayList<ReconstructionRegion>());

    // All reconstruction regions must be contained inside this rectangle
    tsp.setVerificationRegionsBoundsInNanoMeters(rectangleEnclosingAllRegions);
    tsp.setPanelLocationInSystem(PanelLocationInSystemEnum.RIGHT);
    PanelRectangle panelRectangle = tsp.getVerificationRegionsBoundsInNanoMeters();
    List<ProcessorStrip> processorStrips = ImagingChainProgramGenerator.createProcessorStripsForIrps(panelRectangle);
    tsp.setProcessorStripsForVerificationRegions(processorStrips);
    program.addTestSubProgram(tsp);
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    return program;
  }
}
