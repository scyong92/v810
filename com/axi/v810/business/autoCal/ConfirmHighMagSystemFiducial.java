package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.panelSettings.*;

/**
 * Confirmation that measures the optical quality of the system fiducial by measuring
 * the MTF of its diagonal edge by using the FWHM of its line spread function
 * as a proxy for MTF.  It also tests panel positioning by measuring the location
 * of its edges.   MTF is an optical measurement of how well an imaging system captures
 * contrasts between black and white lines.  The smaller the number, the better.
 *
 * @author Wei Chin
 */
public class ConfirmHighMagSystemFiducial extends ConfirmSharpness
{
  // Singleton class.
  private static ConfirmHighMagSystemFiducial _instance;

  // Printable name for this confirmation.
  private static final String _NAME_OF_CAL = "High Mag " + 
      StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_FIDUCIAL_KEY");

  private static final int _FREQUENCY =
      _config.getIntValue(HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY);
  private static final long _TIMEOUTINSECONDS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_TIMEOUT_IN_SECONDS);

  private static final int _REGION_TO_IMAGE_WIDTH_NANOMETERS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_REGION_TO_IMAGE_WIDTH_NANOMETERS);
  private static final int _REGION_TO_IMAGE_HEIGHT_NANOMETERS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_REGION_TO_IMAGE_HEIGHT_NANOMETERS);

  // Margins to use for requesting projections:
  private static final int _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS);
  private static final int _MARGIN_FOR_FOCUS_REGION_NANOMETERS =
    _config.getIntValue(SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_MARGIN_FOR_FOCUS_REGION_NANOMETERS);

  private static final int _KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
    _config.getIntValue(SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH);

  private static final int _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS);
  private static final int _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS);

  // The bottom of the system coupon is not imagable when it is in the mag coupon holder,
  // so limit how far down from sys fid to image.
  private static final int CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
     _config.getIntValue(SystemConfigEnum.CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS);

  private static List<IntCoordinate> _quadPointsInNanometers = new ArrayList<IntCoordinate>();

  // Once LP4 range of motion error is resolved this can go away.
  private static final boolean _STAGE_MOTION_RANGE_OK_FOR_SYS_FID_CONFIRMATION =
     _config.getBooleanValue(HardwareConfigEnum.STAGE_MOTION_RANGE_OK_FOR_SYS_FID_CONFIRMATION);

// Number of horizontal edges to average together
  private final int _noHorizontalEdges = 3;
  private final int _noVerticalEdges = 1;

  // Limits to compare to.
  private double _lineSpreadFWHMPixelsHighLimit;
  private double _lineSpreadFWHMPixelsLowLimit;

  private static SystemFiducialCoupon _systemCoupon = SystemFiducialCoupon.getInstance();

  /**
   * @author John Dutton
   */
  static
  {
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralUpperLeftXinNanoMeters(),
                          _systemCoupon.getQuadrilateralUpperLeftYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralUpperRightXinNanoMeters(),
                          _systemCoupon.getQuadrilateralUpperRightYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralLowerRightXinNanoMeters(),
                          _systemCoupon.getQuadrilateralLowerRightYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralLowerLeftXinNanoMeters(),
                          _systemCoupon.getQuadrilateralLowerLeftYinNanoMeters()));

    // Verify that region to image is not bigger than allowed.
    Assert.expect(_REGION_TO_IMAGE_WIDTH_NANOMETERS <= SetHardwareConstants.getMaxHardwareReconstructionRegionWidthInNanoMeters());
    Assert.expect(_REGION_TO_IMAGE_HEIGHT_NANOMETERS <= SetHardwareConstants.getMaxHardwareReconstructionRegionLengthInNanoMeters());
  }


  /**
   * @author Wei Chin
   */
  private ConfirmHighMagSystemFiducial()
  {
    super(_NAME_OF_CAL,
          _FREQUENCY,
          _TIMEOUTINSECONDS,
          Directory.getHighMagFixedRailImageQualityConfirmationLogDir(),
          FileName.getFixedRailImageQualityConfirmationlLogFileName(),
          ProgressReporterEnum.CONFIRM_SYSTEM_FIDUCIAL_TASK);

    //This confirmation is to be run manually only
    _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;

    setTimestampEnum(HardwareCalibEnum.CONFIRM_SYSTEM_FIDUCIAL_LAST_TIME_RUN);
    _isHighMagTask = true;
  }

  /**
   * A singleton class.
   *
   * @author Wei Chin
   */
  public static synchronized ConfirmHighMagSystemFiducial getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmHighMagSystemFiducial();
    }
    return _instance;
  }


  /**
   * Can't run this confirmation on LP4 because of range of motion issue.
   *
   * @author John Dutton
   */
  public boolean canRunOnThisHardware()
  {
    return _STAGE_MOTION_RANGE_OK_FOR_SYS_FID_CONFIRMATION;
  }

  /**
   * @author Wei Chin
   */
  private static int getDistanceInXFromSysFidToLeftEdgeOfImageNanometers()
  {
    // The right angle point of the system fiducial is at x 12.66 mm in CAD drawing.
    // A point to the left is the upper left point on the quadrilateral
    // at x 21.93 mm.  Have the left side of the reconstructed image start a little to
    // the right of this point by using _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS.
    // With this definition for the image left side, the right side of the
    // quadrilateral will fit in the image with plenty of margin so a vertical
    // edge can be measured.

    int distanceInXFromSysFidToUpperLeftNanometers =
        Math.abs(_systemCoupon.getQuadrilateralUpperLeftXinNanometersInSystemFiducialCoordinates());
    return (distanceInXFromSysFidToUpperLeftNanometers -
        _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS) /2;
  }

/**
 * @author John Dutton
 */
  private static int getDistanceInYFromSysFidToLowerRightNanometers()
  {
    // The right angle point of the system fiducial is at y 11.05 mm in CAD drawing.
    // The furthest dot below this point (marking visible region) is at y 19.85 mm.
    // To have bottom of reconstructed image be at the dot, use the difference to define the
    // bottom of the image.
    return (Math.abs(CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS -
                    _systemCoupon.getSystemFiducialPointYinNanoMeters()))/2;
  }

  /**
   * Calculates where the system fiducial is expected to be based on CAD values of the stage.
   * The tolerance stack will cause the actual to be different, hence a template match
   * will be performed to find the actual.
   *
   * @author John Dutton
   */
  private ImageCoordinate getPredictedSysFidPixel() throws DatastoreException
  {
    int actualPixelSizeXInNanometers = actualPixelSizeXInNanometersInHighMag();
    int actualPixelSizeYInNanometers = actualPixelSizeYInNanometersInHighMag();

    // System Fiducial location for use below with template match results.
    int predictedSysFidPixelX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(getDistanceInXFromSysFidToLeftEdgeOfImageNanometers()),
                                                    actualPixelSizeXInNanometers);
    int distanceInPixelsFromImageBottomToSysFidY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(getDistanceInYFromSysFidToLowerRightNanometers()),
                                                    actualPixelSizeYInNanometers);
    int imgHeight =
        MathUtil.convertNanoMetersToPixelsUsingCeil(_REGION_TO_IMAGE_HEIGHT_NANOMETERS,
                                                    actualPixelSizeYInNanometers);
    int predictedSysFidPixelY = imgHeight - distanceInPixelsFromImageBottomToSysFidY;
    return new ImageCoordinate(predictedSysFidPixelX, predictedSysFidPixelY);
  }


  /**
   * Calculates the region to be reconstructed, based on coordinates relative to the
   * system fiducial.
   *
   * @author John Dutton
   */
  public static PanelRectangle getImageRectangle()
  {
    int reconstructedRegionXInNanometers = -getDistanceInXFromSysFidToLeftEdgeOfImageNanometers();
    int reconstructedRegionYInNanometers = -getDistanceInYFromSysFidToLowerRightNanometers();

    PanelRectangle magCouponArea =
        new PanelRectangle(reconstructedRegionXInNanometers, reconstructedRegionYInNanometers,
                           _REGION_TO_IMAGE_WIDTH_NANOMETERS/2,
                           _REGION_TO_IMAGE_HEIGHT_NANOMETERS/2);

    return magCouponArea;
  }

  /**
   * Calculates where to focus within the reconstruction region.  Coordinates are
   * relative to system fiducial.
   *
   * @author John Dutton
   */
  public static PanelRectangle getFocusRectangle()
  {
    // Need to focus on smaller region because of IRP limit.  Use area around system fid.
    int focusRegionXInNanometers = -(_systemCoupon.getTriangleWidthInNanoMeters() +
                                      _MARGIN_FOR_FOCUS_REGION_NANOMETERS/2);
    int focusRegionYInNanometers = -(_MARGIN_FOR_FOCUS_REGION_NANOMETERS/2);

    int focusRegionWidthNanometers = _MARGIN_FOR_FOCUS_REGION_NANOMETERS/2 +
                                     2 * _systemCoupon.getTriangleWidthInNanoMeters();
    int focusRegionHeightNanometers = _MARGIN_FOR_FOCUS_REGION_NANOMETERS/2 +
                                      2 * _systemCoupon.getTriangleHeightInNanoMeters();

    // Verify focus region is no bigger than maximum supported on the IRP's.
    Assert.expect(focusRegionWidthNanometers <= SetHardwareConstants.getMaxFocusRegionWidthInNanoMeters());
    Assert.expect(focusRegionHeightNanometers <= SetHardwareConstants.getMaxFocusRegionLengthInNanoMeters());


    PanelRectangle focusRectangle =
        new PanelRectangle(focusRegionXInNanometers, focusRegionYInNanometers,
                           focusRegionWidthNanometers, focusRegionHeightNanometers);

    Assert.expect(getImageRectangle().contains(focusRectangle), "focus region is not contained within reconstructed image");

    return focusRectangle;
  }


  /**
   * Get reconstructed image, perform a template match and then hand image and
   * system fiducial location off for processing.
   *
   * @author John Dutton
   */
  public void doTheRealConfirmationWork() throws XrayTesterException
  {
    _lineSpreadFWHMPixelsHighLimit = _parsedLimits.getHighLimitDouble("LineSpreadFWHMPixels");
    _lineSpreadFWHMPixelsLowLimit = _parsedLimits.getLowLimitDouble("LineSpreadFWHMPixels");

    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
    //Start the progress observable so users know what is happening
    initializeAndReportProgressPercentage();

    int actualPixelSizeXInNanometers = actualPixelSizeXInNanometersInHighMag();
    int actualPixelSizeYInNanometers = actualPixelSizeYInNanometersInHighMag();

    PanelRectangle imageRectangle =  getImageRectangle();
    PanelRectangle focusRectangle =  getFocusRectangle();

    increaseAndReportProgressPercentage(10);   // Show 10%

    // Get reconstructed image.
    Image image = null;
    if (readImageFromFileTrue())
      image = readImageFromFile("C:/temp", "testSysFid.png");
    else
    {
      ImageRetriever ir = ImageRetriever.getInstance();
      // XCR-3353 Failed to run manual confirmation task on XXL system
      if(XrayTester.isXXLBased())
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.HIGH_MAG_FIXED_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                            MathUtil.convertMilsToNanoMeters(100),
                            MathUtil.convertMilsToNanoMeters(475));
      }
      else if(XrayTester.isS2EXEnabled())
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.HIGH_MAG_FIXED_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                            MathUtil.convertMilsToNanoMeters(100),
                            MathUtil.convertMilsToNanoMeters(375));
      }
      else
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.HIGH_MAG_FIXED_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET,
                            MathUtil.convertMilsToNanoMeters(25),
                            MathUtil.convertMilsToNanoMeters(170));
      }
    }
    increaseAndReportProgressPercentage(40);   // Show 50%

    Assert.expect(image != null, "Didn't get an image so can't cut up edges.");
    saveIfSavingDebugImages(image, "reconstruction_img");

    // Save focus image
    if (saveDebugImages())
    {
      int focusRegionHeightInPixels = MathUtil.convertNanoMetersToPixelsUsingCeil(focusRectangle.getHeight(),
          actualPixelSizeYInNanometers);
      int focusRegionWidthInPixels = MathUtil.convertNanoMetersToPixelsUsingCeil(focusRectangle.getWidth(),
          actualPixelSizeXInNanometers);
      int focusRegionXInPixels =
          MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(imageRectangle.getMinX() -
          focusRectangle.getMinX()),
                                                      actualPixelSizeXInNanometers);
      int distanceInPixelsFromImageBottomToBottomOfFocusRegion =
          MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(imageRectangle.getMinY() -
          focusRectangle.getMinY()),
                                                      actualPixelSizeYInNanometers);
      int bottomOfFocusRegionInPixels = image.getHeight() - distanceInPixelsFromImageBottomToBottomOfFocusRegion;
      int focusRegionYInPixels = bottomOfFocusRegionInPixels - focusRegionHeightInPixels;
      RegionOfInterest focusROI =
          new RegionOfInterest(focusRegionXInPixels, focusRegionYInPixels, focusRegionWidthInPixels,
                               focusRegionHeightInPixels, 0, RegionShapeEnum.RECTANGULAR);

      //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
      checkIsRoiFitsWithinImage(focusROI, image);
      Image focusImg = Image.createCopy(image, focusROI);
      saveIfSavingDebugImages(focusImg, "focus_img");
      focusImg.decrementReferenceCount();
    }


    // Use template match to calculate offset from system fiducial point, i.e. by the code's
    // calculations it should be at x, y, but where is it actually in the image.  A lot of tolerances
    // stack up to determine this.
    int chopYPixelsOffReconstructedImage = 0;
    RegionOfInterest croppedROI =
        new RegionOfInterest(0, 0, image.getWidth(), image.getHeight() - chopYPixelsOffReconstructedImage,
                             0, RegionShapeEnum.RECTANGULAR);
    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
      checkIsRoiFitsWithinImage(croppedROI, image);
    Image croppedReconstructedImage = Image.createCopy(image, croppedROI);
    ImageCoordinate sysFidPointLocation = SystemFiducialTemplateMatch.matchTemplateInHighMag(croppedReconstructedImage, false, false);
    if (sysFidPointLocation.getX() == -1)
    {
      // Can't find system fiducial, so can't continue.   Fail confirmation and throw exception.
      setResultsToFail();
      throw
          new HardwareTaskExecutionException(
              ConfirmationExceptionEnum.CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH);
    }
    saveIfSavingDebugImages(croppedReconstructedImage, "cropped_reconstructed_img");

    if (saveDebugImages())
    {
      Image template = SystemFiducialTemplateMatch.getTemplate(false, false);
      saveIfSavingDebugImages(template, "template_img");
      template.decrementReferenceCount();
    }

    increaseAndReportProgressPercentage(10);   // Show 60%

//    SystemTypeEnum systemType = XrayTester.getSystemType();
//    if (systemType.equals(SystemTypeEnum.STANDARD))
//    {
      ImageCoordinate predictedSysFidPixel = getPredictedSysFidPixel();
      processImage(image, sysFidPointLocation.getX() - predictedSysFidPixel.getX(),
                   sysFidPointLocation.getY() - predictedSysFidPixel.getY());
//    }
//    else if (systemType.equals(SystemTypeEnum.THROUGHPUT))
//    {
//      processRightTriangleImage(croppedReconstructedImage, sysFidPointLocation);
//    }
//    else
//      Assert.expect(false, "unkown system type: " + systemType.getName());

    croppedReconstructedImage.decrementReferenceCount();

    increaseAndReportProgressPercentage(30); // Show 90%

    setResultMessages();

    if (didConfirmationPass() == false)
      saveImageOnConfirmationFailure(image, "reconstruction_img");

    image.decrementReferenceCount();

    addMagnificationInfo(MagnificationEnum.H_NOMINAL); // Khang Wah, add mgnification info into CD&A log
    reportProgressPercentageDone();
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
  }

  /**
   * @author YingKat Choor
   * Use right triangle to preform MTF
   * cropped edge of the right triangle (slanted edge)
   * calculate MTF
   */
  void processRightTriangleImage(Image croppedReconstructedImage, ImageCoordinate sysFidPointLocation) throws DatastoreException, HardwareTaskExecutionException
  {
    //ROI size suggested by Tracy
    int croppedImageTopLeftXFromFiducialInPixel = 5*2;
    int croppedImageTopLeftYFromFiducialInPixel = 150*2;
    int slantedLineWidthInPixel = 46*2;
    int slantedLineHeightInPixel = 146*2;

    RegionOfInterest roiForTriangleslantedEdge = new RegionOfInterest(sysFidPointLocation.getX() - (croppedImageTopLeftXFromFiducialInPixel+slantedLineWidthInPixel),
                                                                      sysFidPointLocation.getY() - croppedImageTopLeftYFromFiducialInPixel,
                                                                      slantedLineWidthInPixel,
                                                                      slantedLineHeightInPixel, 0, RegionShapeEnum.RECTANGULAR);

    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(roiForTriangleslantedEdge, croppedReconstructedImage);
    Image croppedTriangleSlantedEdge = Image.createCopy(croppedReconstructedImage, roiForTriangleslantedEdge);
    saveIfSavingDebugImages(croppedTriangleSlantedEdge, "cropped_TriangleSlantedEdge_img_" + sysFidPointLocation.getX() + "_" + sysFidPointLocation.getY());

    calculateMTFAndAddResult(croppedTriangleSlantedEdge, true, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "Vertical edge, slanted");

    calculateMeanAndAddResult(0, _noVerticalEdges, "vertical edges mean");
  }

  /**
   * @author John Dutton
   */
  void processImage(Image img, int sysFidPixelOffsetX, int sysFidPixelOffsetY) throws DatastoreException, HardwareTaskExecutionException
  {
    int farthestLeftVisibleXInNanometers = _systemCoupon.getQuadrilateralUpperLeftXinNanoMeters() - _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS;
    int farthestBottomVisibleYInNanometers = CONFIRM_SYSTEM_FIDUCIAL_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS;
    List<ImageCoordinate> listOfImageCoordinates =
        getQuadImageCoordinates(_quadPointsInNanometers,
                                farthestLeftVisibleXInNanometers,
                                farthestBottomVisibleYInNanometers,
                                img.getHeight(),
                                sysFidPixelOffsetX,
                                sysFidPixelOffsetY);

    // Process upper horizontal edge in 3 images.

    ImageCoordinate upperRightPixel = listOfImageCoordinates.get(1);

    // Need to synthesize upper left pixel since it was not captured in the actual image.
    // Use left side of actual image as X and use point-slope equation to calculate Y.
    double slope = (_quadPointsInNanometers.get(1).getY() - _quadPointsInNanometers.get(0).getY())  /
                    (double) (_quadPointsInNanometers.get(1).getX() -  _quadPointsInNanometers.get(0).getX());
    int synthesizedUpperLeftYInNanometers = (int)
      (slope * (farthestLeftVisibleXInNanometers - _quadPointsInNanometers.get(1).getX())
        +  _quadPointsInNanometers.get(1).getY());
    int distanceInPixelsYBetweenUpperRightAndSynUpperLeft =
      MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(_quadPointsInNanometers.get(1).getY() -
                                                           synthesizedUpperLeftYInNanometers),
                                                    actualPixelSizeYInNanometersInHighMag());
    int synthesizedUpperLeftPixelY = upperRightPixel.getY() + distanceInPixelsYBetweenUpperRightAndSynUpperLeft;
    int synthesizedUpperLeftPixelX = 0;
    ImageCoordinate synthesizedUpperLeftPixel =
      new ImageCoordinate(synthesizedUpperLeftPixelX, synthesizedUpperLeftPixelY);


    List<Image> listOfImages =
        getImagesOfHorizontalEdge(img, 3, synthesizedUpperLeftPixel, upperRightPixel,
                                  _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS, _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
    calculateMTFAndAddResult(listOfImages.get(0), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, left");
    calculateMTFAndAddResult(listOfImages.get(1), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, center");
    calculateMTFAndAddResult(listOfImages.get(2), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, right");

    for (Image image : listOfImages)
      image.decrementReferenceCount();



    // Can't process lower horizontal edge in 3 images because can't always image it on all machines, would
    // get motion out of bounds error in Y on some machine.


    // Process right vertical edge.

    // Need to synthesize lower right pixel since it was not captured in the actual image and hence the
    // one in listOfImageCoordinates is not in the actual image.
    // Use bottom of actual image as Y and use point-slope equation to calculate X.
    double slopeLeftEdge = (_quadPointsInNanometers.get(2).getY() - _quadPointsInNanometers.get(1).getY()) /
                   (double)(_quadPointsInNanometers.get(2).getX() - _quadPointsInNanometers.get(1).getX());
    int synthesizedLowerRightXInNanometers =
       (int)(((farthestBottomVisibleYInNanometers - _quadPointsInNanometers.get(1).getY()) / slopeLeftEdge)
             + _quadPointsInNanometers.get(1).getX());
    int distanceInPixelsXBetweenUpperRightAndSynLowerRight =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(_quadPointsInNanometers.get(1).getX()  -
                                                             synthesizedLowerRightXInNanometers),
                                                    actualPixelSizeXInNanometers());
    int synthesizedLowerRightPixelX = upperRightPixel.getX() - distanceInPixelsXBetweenUpperRightAndSynLowerRight;
    int synthesizedLowerRightPixelY = img.getHeight() - 1;
    ImageCoordinate synthesizedLowerRightPixel =
        new ImageCoordinate(synthesizedLowerRightPixelX, synthesizedLowerRightPixelY);



    Image verticalEdgeImg =
        getImageOfVerticalEdge(img, upperRightPixel, synthesizedLowerRightPixel,
                               _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS,
                               _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS);
    calculateMTFAndAddResult(verticalEdgeImg, true, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "vertical edge, right");
    verticalEdgeImg.decrementReferenceCount();

    calculateMeanAndAddResult(0, _noHorizontalEdges, "horizontal edges mean");

  }

  /**
   * Set messages in the Result object. This info is available to any observer
   * who wants to display detailed failure information.
   *
   * @author John Dutton
   */
  private void setResultMessages()
  {
    // Set only if confirmation failed, otherwise let them default to empty strings.
    if(didConfirmationPass() == false)
    {
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_FIDUCIAL_FAILED_STATUS_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_FIDUCIAL_FAILED_ACTION_KEY"));
    }
  }

//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmSharpness will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
}