package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import java.text.*;

/**
 *
 * @author swee-yee.wong
 */
public class ConfirmHighMagCameraCalibrationImages extends EventDrivenHardwareTask
{
  private String _logDirectory;
  
//  private static String _name =
//      StringLocalizer.keyToString("CD_CONFIRM_AREA_MODE_IMAGES_NAME_KEY");
  
  private static String _name = StringLocalizer.keyToString("DS_HIGH_MAG_CONFIRM_CAMERA_CALIBRATION_IMAGES_NAME_KEY");

  private static Config _config = Config.getInstance();

  //There should only be one logfile
  private int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.AREA_MODE_IMAGE_CONFIRM_MAX_NUMBER_OF_LOG_FILES);

  //This task should always run
  private static int _frequency = _config.getIntValue(HardwareCalibEnum.AREA_MODE_IMAGE_CONFIRM_FREQUENCY);

  //There is no timeout for this task, it is manual
  private static long _timeoutInSeconds = _config.getIntValue(SoftwareConfigEnum.AREA_MODE_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS);
  
  //The list of cameras to capture snapshot images from
  protected static Set<AbstractXrayCamera> _cameras = new TreeSet<AbstractXrayCamera>();
  
  //Instance member
  private static ConfirmHighMagCameraCalibrationImages _instance;
  
  //Parallel Thread Task executor to handle saving failed images in parallel
  private ExecuteParallelThreadTasks<Boolean> _executor = new ExecuteParallelThreadTasks<Boolean>();
  
  private static final int IMAGE_PIXEL_HEIGHT = 1000;

//  // Grayscale instance to set cal to run ASAP
//  private CalGrayscale _calGrayscale = CalGrayscale.getInstance();
  
  private Map<AbstractXrayCamera, XrayCameraCalibThreadTask> _cameraToCameraCalibThreadMap = new HashMap<AbstractXrayCamera, XrayCameraCalibThreadTask>();
  
  private ExecuteParallelThreadTasks<Object> _cameraCalibThreads = null;
  private ExecuteParallelThreadTasks<Object> _cameraTriggerControlThreads = null;
  
  private Map<AbstractXrayCamera, Boolean> _cameraToPreviousEnableMap = new HashMap<AbstractXrayCamera, Boolean>();
  
  private static final int _LOG_FREQUENCY_IN_SECONDS =   _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_LOG_RESULTS_EVERY_TIMEOUT_SECONDS);
  private static final int _LOG_FREQUENCY_IN_MILLISECONDS = _LOG_FREQUENCY_IN_SECONDS;
  
  private static final int _MAX_LOG_FILES_IN_DIRECTORY = Config.getInstance().getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_MAX_LOG_FILES_IN_DIRECTORY);
  
  private static ImageAcquisitionEngine _imageAcquisitionEngine;
  
  //Xray source instance for all sub-classes to use
  protected static AbstractXraySource _xraySource = AbstractXraySource.getInstance();
  
  private final SimpleDateFormat _filenameSimpleDateFormat =
      new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS", new Locale("en", "US"));
  /**
   * Class to encapsulate a confirmation that captures snap shot or area mode
   * images and performs analysis on them.
   *
   * @author Reid Hayhow
   */
  private ConfirmHighMagCameraCalibrationImages()
  {
    super(_name, _frequency, _timeoutInSeconds, ProgressReporterEnum.CONFIRM_HIGH_MAG_CAMERA_CALIBRATION_IMAGES);
    
    String datePartOfFileName = _filenameSimpleDateFormat.format(Calendar.getInstance().getTime());
    _logDirectory = Directory.getHighMagConfirmCameraCalibrationImagesLogDir() + File.separator + datePartOfFileName;

    //This confirmation is automated
    _taskType = HardwareTaskTypeEnum.MANUAL_DIAGNOSTIC_TASK_TYPE;

    //Add each camera to our internal list
    createCamerasSet();

    //Set the timestamp enum for later use
    setTimestampEnum(HardwareCalibEnum.CONFIRM_AREA_MODE_IMAGE_LAST_TIME_RUN);
    
    _cameraCalibThreads = new ExecuteParallelThreadTasks<Object>();
    _cameraTriggerControlThreads = new ExecuteParallelThreadTasks<Object>();
    
    _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  }

  /**
   * Instance access to this Confirmation
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmHighMagCameraCalibrationImages getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmHighMagCameraCalibrationImages();
    }
    return _instance;
  }

  /**
   * This Confirmation can only run if the camera firmware allows area mode
   * images (AKA snap shot images).
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }
  
  /**
   * As part of the pre-condition for executing this type of task, we have to
   * save off the current motion profile set in the panel positioner so we can
   * restore it in cleanUp() after the task executes.
   *
   * @author Reid Hayhow
   */
  public void setUp() throws XrayTesterException
  {
    String datePartOfFileName = _filenameSimpleDateFormat.format(Calendar.getInstance().getTime());
    _logDirectory = Directory.getHighMagConfirmCameraCalibrationImagesLogDir() + File.separator + datePartOfFileName;
    if(XrayActuator.isInstalled())
    {
      //move x-rays Z Axis to up position for low mag task.
      XrayActuator.getInstance().home(false, false);
    }
    //Swee Yee- set calPoint for different Magnification
    XrayCameraArray.getInstance().loadCalPointForVariableMag(false);
    _xraySource.on();
    //Save off the active motion profile, just in case we end up moving the
    //stage and setting the profile to a point-to-point profile
    //_panelPositionerSavedProfile = _panelPositioner.getActiveMotionProfile();
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
      return;
    }
    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      PanelClamps.getInstance().close();
    }
  }
  
  /**
   * The execute method for the Camera Image Confirmation tests. It allows sub-classes
   * to move the stage and control xrays before and after acquiring images. It then
   * analyzes those images to make sure their gray values are within the specified limits.
   *
   * @author Reid Hayhow
   */
  public void executeTask() throws XrayTesterException
  {   
	//Swee Yee Wong - Camera debugging purpose
    debug("HighMagConfirmCameraCalibrationImageTask");

      try
      {
        //Special case, if the camera list is empty, don't continue because lots of
        //downstream code doesn't like 0 length lists
        if (_cameras.isEmpty() || PanelHandler.getInstance().isPanelLoaded())
        {
          System.out.println("ERROR: Failed to execute highMagConfirmCameraCalibrationImages due to either no camera is included in this task or panel is loaded.");
          //Create a passing result
          String localizedTaskName = getName();
          _limits = new Limit(null, null, null, localizedTaskName);
          _result = new Result(new Boolean(true), localizedTaskName);
          return;
        }
        for (AbstractXrayCamera camera : _cameras)
        {
          camera.disableSafeGuards();
        }
        

        reportProgress(0);

        //move the stage (if necessary) before executing the task
        moveStageBeforeExecute();

        //Turning on xrays will take time, so give a progress update
        reportProgress(10);
        
        clearCameraCalibration();
        
        captureUncalibratedDarkImages();
        
        reportProgress(20);
        captureUncalibratedLightImages();
        reportProgress(30);
        
        for (AbstractXrayCamera camera : _cameras)
        {
          camera.enableSafeGuards();
        }
        
        performCalibration();

        captureCalibratedDarkImages();
        
        reportProgress(85);
        captureCalibratedLightImages();
        
        reportProgress(90);
        
        //Wait for any of the images that needed to be saved to actually be saved
        _executor.waitForTasksToComplete();

        //Report that this task is done
        reportProgress(100);
        _result.setStatus(HardwareTaskStatusEnum.PASSED);

      }
      catch (XrayTesterException ex)
      {
        _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
        return;
      }
      //Log any unexpected exceptions
      catch (Exception e)
      {
        Assert.logException(e);
      }
      //Restore the system to the previous state
      finally
      {
//        logInformation(_forceLogging);
      }
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Create the results object for this test result.
   *
   * @author Reid Hayhow
   */
  public void createResultsObject() throws DatastoreException
  {
    _result = new Result(new Boolean(true), _name);
  }
  
  /**
   * In order to clean up after the sub tasks of this type, we have to restore
   * the motion profile of the panel positioner. This is necessary because some
   * sub-classes move the stage.
   *
   * @author Reid Hayhow
   */
  public void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    }
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false; 
    }
//    _calGrayscale.setTaskToRunASAP();
  }
  
  /**
   * Set up the limits for this specific task. Because we have the name of the
   * sub-class task, this can be handled by the super class. These values are
   * read out of the CustomerLimits.properties file.
   *
   * @author Reid Hayhow
   */
  public void createLimitsObject() throws DatastoreException
  {
    _limits = new Limit(null, null, null, null);
  }
  
  private void moveStageBeforeExecute() throws XrayTesterException
  {
    if (PanelHandler.getInstance().moveStageToCameraClearPosition() == false)
    {
      //swee yee wong - XCR-3052	Wrong information of area mode task failed exception
      throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_name);
    }
  }
  
  private void turnOffXraySource() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays cylinder down for high mag.    
        XrayActuator.getInstance().down(false);
      }
      else
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
      // re-calculate magnification 
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
      if(_xraySource instanceof HTubeXraySource)
      {
        HTubeXraySource htube = (HTubeXraySource)_xraySource;
        htube.turnXRaysPowerOffOnly();
      }
      else
      {
        _xraySource.off();
      }
    }
    _xraySource.off();
  }
  
  private void turnOnXraySource() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {        
      //move x-rays cylinder or Z Axis down for high mag.    
      XrayActuator.getInstance().down(false);
      
      // re-calculate magnification 
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
      if(_xraySource instanceof HTubeXraySource)
      {
        HTubeXraySource htube = (HTubeXraySource)_xraySource;
        htube.on();
      }
      else
      {
        _xraySource.on();
      }
    }
    //For this grouping of all image light confirms, we want xrays ON to
    //optimize through put
    _xraySource.on();
  }
  
  /**
   * Common function to gather images and analyze them. It needs to know the
   * direction the images should be taken in (currently a boolean but could be an
   * enum) and for creating an image it needs to know the direction the image
   * is being taken in and if the image is light or dark.
   *
   * @author Reid Hayhow
   */
  private void getAndSaveImages(ScanPassDirectionEnum directionEnum, String imagesTitle) throws XrayTesterException
  {
//    //Do some basic checking on the camera sets before using them
//    int totalNumberOfCameras = XrayCameraArray.getNumberOfCameras();
//    //GroupOne and GroupTwo should include all cameras, as should the allCameras
//    //set. Since allCameras = GroupOne + GroupTwo, we get a nice cross check as well
//    if (_cameras.size() != totalNumberOfCameras)
//    {
//      throw new HardwareTaskExecutionException(ConfirmationExceptionEnum.INCORRECT_CAMERA_LIST_IN_HARDWARE_CALIB);
//    }
    //Create an image retreiver to handle this request
    CameraImageRetreiver imageRetriever = new CameraImageRetreiver(_cameras, directionEnum);

    // Wrap getting the image map in a try block to guarantee that we purge the map even if something goes wrong
    try
    {
      //Get the images back from the retreiver
      //Map<AbstractXrayCamera, Image> cameraToResultImageMap = imageRetriever.getImageMap(true);
      Map<AbstractXrayCamera, Image> cameraToResultImageMap = null;
      cameraToResultImageMap = imageRetriever.getImageMap(false);

      //Since we requested images from a list of cameras, I assert we got
      //one for each in the list
      Assert.expect(cameraToResultImageMap.size() == _cameras.size());

      //Get the set of keys in the map of returned images
      Set<AbstractXrayCamera> cameraSet = cameraToResultImageMap.keySet();

      for (AbstractXrayCamera camera : cameraSet)
      {
        //I can do an even more specific check to make sure every
        //camera key in the resultImageMap is also in the list of cameras
        Assert.expect(_cameras.contains(camera));
        
        Image imageToAnalyze = cameraToResultImageMap.get(camera);
        
        // We can just get any camera so we can get the sensor attributes
        XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
        AbstractXrayCamera xRayCamera = xRayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
        //We are interested in the usable image, so specify the region of interest that
        //is the same size as the usable image
        int imageWidthInPixels = xRayCamera.getNumberOfSensorPixelsUsed();
        RegionOfInterest regionOfInterest = new RegionOfInterest(0,
                                                             0,
                                                             imageWidthInPixels,
                                                             IMAGE_PIXEL_HEIGHT,
                                                             0,
                                                             RegionShapeEnum.RECTANGULAR);

        //Get the vertical profile of this image. This averages all of the image

        if(regionOfInterest.fitsWithinImage(imageToAnalyze) == false)
        {
          regionOfInterest = RegionOfInterest.createRegionFromImage(imageToAnalyze);
        }

        //Crop the image before saving it
        Image imageToSave = Image.createCopy(imageToAnalyze, regionOfInterest);

        //Create the thread task to save this specific image
        ImageSaverThreadTask saverThread = new ImageSaverThreadTask(imageToSave,
                                                                    _logDirectory + File.separator + "Camera_" + camera.getId() +
                                                                    "_" + imagesTitle + "_"
                                                                    + directionEnum.toString() +".png");

        //Submit the thread task to the executor. We will check that all tasks
        //completed later
        _executor.submitThreadTask(saverThread);
        imageToSave.decrementReferenceCount();
      }
    }
    finally
    {
      imageRetriever.clearResultImageMap();
    }
  }
  
  /**
   * Method heavily leveraged from Eddie's CalGrayscale list setup since we are
   * interested in the same list of cameras
   *
   * @author Reid Hayhow
   */
  private void createCamerasSet()
  {
    _cameras.clear();
    
    for (int cameraId = 1; cameraId < 14; cameraId++)
    {
      //If we got here the number parsed and can be added to the list
      
      XrayCameraIdEnum idEnum = XrayCameraIdEnum.getEnum(new Integer(cameraId));
      _cameras.add(XrayCameraArray.getCamera(idEnum));
    }
  }
  
  private void captureCalibratedDarkImages() throws XrayTesterException
  {
    turnOffXraySource();
    getAndSaveImages(ScanPassDirectionEnum.FORWARD, "Calibrated_Dark");
    turnOffXraySource();
    //Now get the images in the backward direction
    getAndSaveImages(ScanPassDirectionEnum.REVERSE, "Calibrated_Dark");
  }
  
  private void captureCalibratedLightImages() throws XrayTesterException
  {
    turnOnXraySource();
    getAndSaveImages(ScanPassDirectionEnum.FORWARD, "Calibrated_Light");
    turnOnXraySource();
    //Now get the images in the backward direction
    getAndSaveImages(ScanPassDirectionEnum.REVERSE, "Calibrated_Light");
  }
  
  private void captureUncalibratedDarkImages() throws XrayTesterException
  {
    turnOffXraySource();

    getAndSaveImages(ScanPassDirectionEnum.FORWARD, "Uncalibrated_Dark");

    turnOffXraySource();

    //Now get the images in the backward direction
    getAndSaveImages(ScanPassDirectionEnum.REVERSE, "Uncalibrated_Dark");
  }
  
  private void captureUncalibratedLightImages() throws XrayTesterException
  {
    turnOnXraySource();

    getAndSaveImages(ScanPassDirectionEnum.FORWARD, "Uncalibrated_Light");

    turnOnXraySource();

    //Now get the images in the backward direction
    getAndSaveImages(ScanPassDirectionEnum.REVERSE, "Uncalibrated_Light");
  }
  
  private void clearCameraCalibration() throws XrayTesterException
  {
    for (AbstractXrayCamera camera : _cameras)
    {
      Image areaModeImage = camera.getAreaModeImage();  
    }
  }
  
  private void performCalibration() throws XrayTesterException
  {
    //Notify IAE that we are about to change programs and move the stage
    _imageAcquisitionEngine.getReconstructedImagesProducer().calibrationExecutionHasChangedCameraAndStagePrograms();
    
    turnOffXraySource();
    sleep(1000);
    performDarkSegmentCal();
    sleep(200);
    reportProgress(40);
    turnOnXraySource();
    performLightSegmentCal();
    sleep(200);
    reportProgress(60);
    turnOffXraySource();
    performDarkPixelCal();
    sleep(200);
    reportProgress(70);
    performSaveVariableMagHighCalConfig();
    sleep(200);
    performLoadVariableMagHighCalConfig();
    sleep(500);
    reportProgress(80);
    try
    {
      for (AbstractXrayCamera camera : _cameras)
      {
        camera.saveUncalibratedLogFiles(_logDirectory, 0, _MAX_LOG_FILES_IN_DIRECTORY);
      }
    }
    catch (XrayTesterException xe)
    {
      System.out.println("ERROR: Failed to receive CalibrationLog from camera.");
      System.out.println(xe.getMessage());
    }
    logInformation();
  }
  
  /**
   * @param cameras List of cameras on which to run this calibration step
   * @param xrayCameraCalibEnum Which calibration step to run
   * @author Eddie Williamson
   */
  private void performCalibStepOnCameras(Set<AbstractXrayCamera> cameras, XrayCameraCalibEnum xrayCameraCalibEnum) throws XrayTesterException
  {
    Assert.expect(cameras != null);
    Assert.expect(xrayCameraCalibEnum != null);

    CameraTrigger cameraTrigger = CameraTrigger.getInstance();
    try
    {
      sleep(1000);
      allCamerasIgnoreTriggers();
      sleep(1000);
      cameraTrigger.on();
      sleep(1000);

      startCameraCalibThreads(cameras, xrayCameraCalibEnum);
      waitForCameraCalibThreads(cameras, xrayCameraCalibEnum);

    }
    finally
    {
      // Make sure that synthetic triggers are on.  It is good/expected to leave
      // them on because they bathe cameras keeping them thermally warm.
      cameraTrigger.on();
      allCamerasRestorePreviousTriggerMode();
    }
  }
  
  /**
   * @param cameras List of cameras on which to run this calibration step
   * @param xrayCameraCalibEnum Which calibration step to run
   * @author Eddie Williamson
   */
  private void startCameraCalibThreads(Set<AbstractXrayCamera> cameras, XrayCameraCalibEnum xrayCameraCalibEnum)
  {
    _cameraToCameraCalibThreadMap.clear();

    // start threads for all cameras
    for (AbstractXrayCamera camera : cameras)
    {
      XrayCameraCalibThreadTask xrayCameraCalibThreadTask = new XrayCameraCalibThreadTask(camera, xrayCameraCalibEnum);
      _cameraToCameraCalibThreadMap.put(camera, xrayCameraCalibThreadTask);
      _cameraCalibThreads.submitThreadTask(xrayCameraCalibThreadTask);
    }
  }
  
  /**
   * @param cameras List of cameras on which to wait
   * @param xrayCameraCalibEnum Which calibration step was run
   * @author Eddie Williamson
   */
  private void waitForCameraCalibThreads(Set<AbstractXrayCamera> cameras, XrayCameraCalibEnum xrayCameraCalibEnum) throws XrayTesterException
  {
    try
    {
      _cameraCalibThreads.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      if (ex instanceof XrayTesterException)
      {
        throw (XrayTesterException)ex;
      }
      else
      {
        /** QUESTION: Should we do this even if it already was an XrayTesterException?
         * Adds Grayscale name to message. */
        // convert to XrayTesterException before throwing
        XrayTesterException convertedEx = new HardwareTaskExecutionException(this);
        convertedEx.initCause(ex);
        convertedEx.fillInStackTrace();
        throw convertedEx;
      }
    }

    for (AbstractXrayCamera camera : cameras)
    {
      if (_cameraToCameraCalibThreadMap.get(camera).getPass())
      {
        // do nothing
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.FAILED);
        throw _cameraToCameraCalibThreadMap.get(camera).getException();
      }
    }
  }
  
  /**
   * @author Eddie Williamson
   */
  private void allCamerasIgnoreTriggers() throws XrayTesterException
  {
    Map<AbstractXrayCamera, XrayCameraTriggerControlThreadTask> cameraToXrayCameraTriggerControlThreadTasksMap = new HashMap<AbstractXrayCamera, XrayCameraTriggerControlThreadTask>();

    // start threads for all cameras
    for (AbstractXrayCamera camera : _cameras)
    {
      XrayCameraTriggerControlThreadTask xrayCameraTriggerControlThreadTask = new XrayCameraTriggerControlThreadTask(camera, false);
      cameraToXrayCameraTriggerControlThreadTasksMap.put(camera, xrayCameraTriggerControlThreadTask);
      _cameraTriggerControlThreads.submitThreadTask(xrayCameraTriggerControlThreadTask);
    }

    try
    {
      _cameraTriggerControlThreads.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }

    for (AbstractXrayCamera camera : _cameras)
    {
      _cameraToPreviousEnableMap.put(camera, cameraToXrayCameraTriggerControlThreadTasksMap.get(camera).getPreviousEnable());
    }
  }

  /**
   * Restores the previous trigger mode that was saved when
   * allCamerasIgnoreTriggers() was called.
   * @author Eddie Williamson
   */
  private void allCamerasRestorePreviousTriggerMode() throws XrayTesterException
  {
    try
    {
      // start threads for all cameras
      for (AbstractXrayCamera camera : _cameras)
      {
        // This method can be called as a recovery (finally block) so the map may not have been populated
        // or may be only partially populated
        Boolean previousCameraTriggerState = _cameraToPreviousEnableMap.get(camera);

        // Only submit tasks for cameras that have a valid previous state
        if(previousCameraTriggerState != null)
        {
          XrayCameraTriggerControlThreadTask xrayCameraTriggerControlThreadTask = new XrayCameraTriggerControlThreadTask(camera, previousCameraTriggerState);
          _cameraTriggerControlThreads.submitThreadTask(xrayCameraTriggerControlThreadTask);
        }
      }

      _cameraTriggerControlThreads.waitForTasksToComplete();
    }
    catch (XrayTesterException ex)
    {
      throw ex;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }
  
  /**
  * Perform Camera Dark Segment Calibration
  * @author Farn Sern
  */
  private void performDarkSegmentCal() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_DARK_SEGMENT_CAL);
  }

  /**
  * Perform Camera Dark Segment Calibration
  * @author Anthony Fong
  */
  private void performLightSegmentCal() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_LIGHT_SEGMENT_CAL);
  }
 
  /**
  * Perform Camera Dark Pixel Calibration
  * @author Farn Sern
  */
  private void performDarkPixelCal() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_DARK_PIXEL_CAL);
  }
  
  /**
  * Perform Camera Save Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performSaveVariableMagHighCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.SAVE_VARIABLE_MAG_HIGH_CAL);
  }
  /**
  * Perform Camera Load Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performLoadVariableMagHighCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.LOAD_VARIABLE_MAG_HIGH_CAL);
  }
    /**
  * Perform Camera Save Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performSaveVariableMagLowCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.SAVE_VARIABLE_MAG_LOW_CAL);
  }
  /**
  * Perform Camera Load Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performLoadVariableMagLowCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.LOAD_VARIABLE_MAG_LOW_CAL);
  }
  
  private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }
  
  /**
   * Logs detailed information about the cal and saves images if needed. The
   * log information is formatted as comma separated values to it can be
   * imported into Excel for manual analysis.
   * @param forceLogging Force logging if true, else based on periodic timeout.
   * @author Eddie Williamson
   */
  private void logInformation() throws XrayTesterException
  {
//    int logFrequencyInMilliseconds = _LOG_FREQUENCY_IN_MILLISECONDS;
//
//    if (forceLogging)
      int logFrequencyInMilliseconds = 0;

    for (AbstractXrayCamera camera : _cameras)
    {
      XrayCameraCalibrationData calibrationData = camera.getCalibrationData();
        
      if(calibrationData.hasCalibrationData())
        calibrationData.log(_logDirectory, logFrequencyInMilliseconds);
      else
      {
        System.out.println("Camera: " + camera.getId() + " - No Calibration Data!");
      }
    }
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}

