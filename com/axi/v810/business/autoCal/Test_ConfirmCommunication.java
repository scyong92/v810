package com.axi.v810.business.autoCal;

import java.io.*;
import java.net.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

public class Test_ConfirmCommunication extends UnitTest
{
  private Config _config;
  private Map<ConfigEnum, Object> _mapOfOriginalIpAddresses = new HashMap<ConfigEnum, Object>();
  private InetAddress _localHost;

  /**
   * Main test class for the Communication Confirmations. Set up dummy IP
   * addresses, call the communication confirmation large group of tasks
   * and then restore the original IP addresses
   *
   * @author Reid Hayhow
   */
  public void test(BufferedReader inputStream, PrintWriter outputStream)
  {
    //Utils class allows backup/restore of config files
    TestUtils utils = new TestUtils();

    //Backup the HW config before fiddling with ip addresses
    utils.backupHWConfig(getTestSourceDir());

    //Save off the hardware.calib because running tasks will change it by adding timestamps
    utils.backupHWCalib(getTestSourceDir());

    //Setup the IP address for localhost
    try
    {
      _localHost = InetAddress.getByName("localhost");
    }
    catch (UnknownHostException ex)
    {
      ex.printStackTrace(outputStream);
      //No good if we can't get localhost
      Expect.expect(false);
    }

    //Get the config so we can copy old values and clobber them with dummy values
    _config = Config.getInstance();

    //Preserve the old IP addresses
    saveOldConfigValues();

    //Set the config with dummy values
    try
    {
      _config.setValues(createDummyIPAddressMap());
    }
    catch (DatastoreException ex1)
    {
      ex1.printStackTrace(outputStream);
      //No good, bail out
      Expect.expect(false);
    }

    // Assure that the tester is started up, otherwise tasks won't run!
    try
    {
      XrayTester.getInstance().setSimulationMode();
      XrayTester.getInstance().startup();

    }
    catch (XrayTesterException ex2)
    {
      Expect.expect(false, ex2.getLocalizedMessage());
    }

    boolean testPassed = false;
    //OK, now that everything is setup, get the confirmation
    ConfirmAllCommunication conf = ConfirmAllCommunication.getInstance();
    try
    {
      testPassed = conf.executeUnconditionally();
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(outputStream);
      Expect.expect(false);
    }

    Expect.expect(testPassed);
    if(!testPassed)
    {
      outputStream.append(conf.getResult().getTaskStatusMessage());
    }

    //Test the new exception for this Confirmation
    HardwareTaskExecutionException confirmException = new HardwareTaskExecutionException(ConfirmationExceptionEnum.PROBLEM_COMMUNICATING_WITH_REMOTE_SYSTEM, new Throwable());
    String localizedExceptionString = confirmException.getLocalizedMessage();
    String localizedExceptionStringDirect = StringLocalizer.keyToString(ConfirmationExceptionEnum.PROBLEM_COMMUNICATING_WITH_REMOTE_SYSTEM.getLocalizedString());

    Expect.expect(localizedExceptionString.contains(localizedExceptionStringDirect));

    //Restore the HW config since I changed it. The above changes leave whitespace :-(
    utils.restoreHWConfig(getTestSourceDir());

    //Also restore the hardware.calib since timestamps have been saved into it
    utils.restoreHWCalib(getTestSourceDir());
  }

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ConfirmCommunication());
  }

  /**
   * Create a map of dummy IP addresses so the test passes and goes quickly
   *
   * @author Reid Hayhow
   */
  private Map<ConfigEnum, Object> createDummyIPAddressMap()
  {
    //Create map to return
    Map<ConfigEnum, Object> mapOfDummyIpAddresses = new HashMap<ConfigEnum, Object>();

    //Get the set of keys from the map of saved config values
    Set<ConfigEnum> setOfKeys = _mapOfOriginalIpAddresses.keySet();

    //Get an iterator from the set of keys
    Iterator<ConfigEnum> setIterator = setOfKeys.iterator();

    //For each ConfigEnum key in the map of saved values, replace it with
    //localhost ip
    while(setIterator.hasNext())
    {
      mapOfDummyIpAddresses.put(setIterator.next(), _localHost.getHostAddress());
    }

    //return the map of dummy values
    return mapOfDummyIpAddresses;
  }

  /**
   * Preserve the old config values before clobbering them with dummy values so
   * we can restore them when done.
   *
   * @author Reid Hayhow
   */
  private void saveOldConfigValues()
  {
    //Camera IP addresses
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_14_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_1_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_2_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_3_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_4_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_5_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_6_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_7_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_8_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_9_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_10_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_11_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_12_IP_ADDRESS);
//    addConfigToMap(HardwareConfigEnum.AXI_TDI_CAMERA_13_IP_ADDRESS);

    //IRP addresses for ethernet cards 1 and 2
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_1);
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_1);
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_1);
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_1);
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_2);
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_2);
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_2);
    addConfigToMap(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_2);

    //Switch address
    addConfigToMap(HardwareConfigEnum.SWITCH_IP_ADDRESS);

  }

  /**
   * Shortcut to simplify and shorten adding map values
   *
   * @author Reid Hayhow
   */
  private void addConfigToMap(ConfigEnum enumValue)
  {
    _mapOfOriginalIpAddresses.put(enumValue, _config.getStringValue(enumValue));
  }
}
