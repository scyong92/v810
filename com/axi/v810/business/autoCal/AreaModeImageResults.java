package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.hardware.*;

/**
 * @author Reid Hayhow
 *
 * This class encapuslates all the results that are tested for in the Area Mode
 * Image Confirmation. It also provides a storage location for a light image from
 * the specific camera. It implements Comparable to allow for natural order
 * sorting and displaying in correct camera order.
 */
class AreaModeImageResults implements Comparable<AreaModeImageResults>
{
  private AbstractXrayCamera _camera;
  private Image _lightImage;
  private float _subtractedHorizontalResponsiveness;
  private float _subtractedVerticalResponsiveness;
  private double _lightStandardDeviation;
  private double _lightMeanGray;
  private int[] _lightHistogram;
  private float _lightHorizontalMinValue;
  private float _lightVerticalMinValue;

  private int[] _edgeHistogram;
  private int[] _edgeFullHistogram;
  private double _edgeMean;
  private double _edgeStandardDeviation;
  private double _edgeMaxGradient;
  private double _edgeAvgGradient;
  private double _betaArea;

  //private int[] _sumEdgeHistogram;
  private int[] _sumEdgeFullHistogram;
  private double _sumEdgeMean;
  private double _sumEdgeStandardDeviation;
  private double _sumEdgeMaxGradient;
  private double _sumEdgeAvgGradient;

  /**
   * @author Reid Hayhow
   */
  public AreaModeImageResults(AbstractXrayCamera camera)
  {
    Assert.expect(camera != null);

    _camera = camera;
  }

  /**
   * @author Reid Hayhow
   */
  protected AbstractXrayCamera getCamera()
  {
    Assert.expect(_camera != null);

    return _camera;
  }

  /**
   * @author Reid Hayhow
   *
   * In order to sort a set/list of AreaModeImageResults, implement comaparable
   * and use the underlying camera reference as the correct order for the results.
   */
  public int compareTo(AreaModeImageResults result)
  {
    Assert.expect(result != null);

    return getCamera().compareTo(result.getCamera());
  }

  /**
   * @author Reid Hayhow
   */
  public void setSubtractedHorizontalResponsiveness(float subtractedHorizontalResponsiveness)
  {
    _subtractedHorizontalResponsiveness = subtractedHorizontalResponsiveness;
  }

  /**
   * @author Reid Hayhow
   */
  public float getSubtractedHorizontalResponsiveness()
  {
    return _subtractedHorizontalResponsiveness;
  }

  /**
   * @author Reid Hayhow
   */
  public void setSubtractedVerticalResponsiveness(float subtractedVerticalResponsiveness)
  {
    _subtractedVerticalResponsiveness = subtractedVerticalResponsiveness;
  }

  /**
   * @author Reid Hayhow
   */
  public float getSubtractedVerticalResponsiveness()
  {
    return _subtractedVerticalResponsiveness;
  }

  /**
   * @author Reid Hayhow
   */
  public void setLightHorizontalMinValue(float grayHorizontalMinValue)
  {
    _lightHorizontalMinValue = grayHorizontalMinValue;
  }

  /**
   * @author Reid Hayhow
   */
  public float getLightHorizontalMinValue()
  {
    return _lightHorizontalMinValue;
  }

  /**
   * @author Reid Hayhow
   */
  public void setLightVerticalMinValue(float grayVerticalMinValue)
  {
    _lightVerticalMinValue = grayVerticalMinValue;
  }

  /**
   * @author Reid Hayhow
   */
  public float getLightVerticalMinValue()
  {
    return _lightVerticalMinValue;
  }

  /**
   * @author Reid Hayhow
   */
  public void setLightStdDev(double standardDev)
  {
    _lightStandardDeviation = standardDev;
  }

  /**
   * @author Reid Hayhow
   */
  public double getLightStandardDeviation()
  {
    return _lightStandardDeviation;
  }

    /**
   * @author Anthony Fong - CameraQuality
   */
  public void setEdgeMaxGradient(double edgeMaxGradient)
  {
    _edgeMaxGradient = edgeMaxGradient;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeMaxGradient()
  {
    return _edgeMaxGradient;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public void setEdgeAvgGradient(double edgeAvgGradient)
  {
    _edgeAvgGradient = edgeAvgGradient;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeAvgGradient()
  {
    return _edgeAvgGradient;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public void setEdgeMean(double edgeMean)
  {
    _edgeMean = edgeMean;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeMean()
  {
    return _edgeMean;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public void setEdgeStandardDeviation(double edgeStandardDev)
  {
    _edgeStandardDeviation = edgeStandardDev;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeStandardDeviation()
  {
    return _edgeStandardDeviation;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public void setBetaArea(double betaArea)
  {
    _betaArea = betaArea;
  }

  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getBetaArea()
  {
    return _betaArea;
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public void sumEdgeMean(double EdgeMean)
  {
    _sumEdgeMean += EdgeMean;
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeMeanAverage(int sampleDataCount)
  {
    return _sumEdgeMean / sampleDataCount;
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public void sumEdgeStandardDeviation(double EdgeStandardDeviation)
  {
    _sumEdgeStandardDeviation += EdgeStandardDeviation;
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeStandardDeviationAverage(int sampleDataCount)
  {
    return _sumEdgeStandardDeviation / sampleDataCount;
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public void sumEdgeMaxGradient(double EdgeMaxGradient)
  {
    _sumEdgeMaxGradient += EdgeMaxGradient;
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeMaxGradientAverage(int sampleDataCount)
  {
    return _sumEdgeMaxGradient / sampleDataCount;
  }

    /**
   * @author Anthony Fong - CameraQuality
   */
  public void sumEdgeAvgGradient(double EdgeAvgGradient)
  {
    _sumEdgeAvgGradient += EdgeAvgGradient;
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public double getEdgeAvgGradientAverage(int sampleDataCount)
  {
    return _sumEdgeAvgGradient / sampleDataCount;
  }

//  /**
//   * @author Anthony Fong - CameraQuality
//   */
//  public void sumEdgeHistogram(int[] EdgeHistogram)
//  {
//    for(int i=0; i<_sumEdgeHistogram.length; i++)
//    {
//        _sumEdgeHistogram[i] += EdgeHistogram[i];
//     }
//  }
//  /**
//   * @author Anthony Fong - CameraQuality
//   */
//  public int[] getEdgeHistogramAverage(int sampleDataCount)
//  {
//    int[] avgEdgeHistogram = new int[_sumEdgeHistogram.length];
//    for(int i=0; i<_sumEdgeHistogram.length; i++)
//    {
//        avgEdgeHistogram[i] = _sumEdgeHistogram[i]/sampleDataCount;
//    }
//    return avgEdgeHistogram;
//  }


  /**
   * @author Anthony Fong - CameraQuality
   */
  public void sumEdgeFullHistogram(int[] EdgeFullHistogram)
  {
    for(int i=0; i<_sumEdgeFullHistogram.length; i++)
    {
        _sumEdgeFullHistogram[i] += EdgeFullHistogram[i];
     }
  }
  /**
   * @author Anthony Fong - CameraQuality
   */
  public int[] getEdgeFullHistogramAverage(int sampleDataCount)
  {
    int[] avgEdgeFullHistogram = new int[_sumEdgeFullHistogram.length];
    for(int i=0; i<_sumEdgeFullHistogram.length; i++)
    {
        avgEdgeFullHistogram[i] = _sumEdgeFullHistogram[i]/sampleDataCount;
    }
    return avgEdgeFullHistogram;
  }
    /**
   * @author Anthony Fong - CameraQuality
   */
  public void resetSumData(int histogramDataCount)
  {
    _sumEdgeFullHistogram = new int[histogramDataCount];
    for(int i=0; i < histogramDataCount; i++)
    {
        _sumEdgeFullHistogram[i] = 0;
    }
    _sumEdgeMean = 0.0;
    _sumEdgeStandardDeviation = 0.0;
    _sumEdgeMaxGradient = 0.0;
  }

  /**
   * @author Reid Hayhow
   */
  public void setLightMeanGray(double meanGrayValue)
  {
    _lightMeanGray = meanGrayValue;
  }

  /**
   * @author Reid Hayhow
   */
  public double getLightMeanGray()
  {
    return _lightMeanGray;
  }

  /**
   * @author Reid Hayhow
   *
   * For now the min responsiveness value is not used for the compare, but it could
   * be used eventually so I have left it in the method.
   */
  public boolean isSubtractedHorizontalResponsivenessWithinLimits(Double _subtractedHorizontalProfileMinResponsivenessValue,
                                                                  Double _subtractedHorizontalProfileMaxResponsivenessValue)
  {
    Assert.expect(_subtractedHorizontalProfileMaxResponsivenessValue != null);
    Assert.expect(_subtractedHorizontalProfileMinResponsivenessValue != null);

    return _subtractedHorizontalResponsiveness <= _subtractedHorizontalProfileMaxResponsivenessValue.doubleValue();
  }

  /**
   * @author Reid Hayhow
   *
   * For now the min responsiveness value is not used for the compare, but it could
   * be used eventually so I have left it in the method.
   */
  public boolean isSubtractedVerticalResponsivenessWithinLimits(Double _subtractedVerticalProfileMinResponsivenessValue,
                                                                Double _subtractedVerticalProfileMaxResponsivenessValue)
  {
    Assert.expect(_subtractedVerticalProfileMaxResponsivenessValue != null);
    Assert.expect(_subtractedVerticalProfileMinResponsivenessValue != null);

    return _subtractedVerticalResponsiveness <= _subtractedVerticalProfileMaxResponsivenessValue.doubleValue();
  }

  /**
   * @author Reid Hayhow
   */
  public boolean isLightHorizontalMinValueWithinLimits(Double _horizontalProfileMinValue)
  {
    Assert.expect(_horizontalProfileMinValue != null);

    return _lightHorizontalMinValue >= _horizontalProfileMinValue.doubleValue();
  }

  /**
   * @author Reid Hayhow
   */
  public boolean isLightVerticalMinValueWithinLimits(Double _verticalProfileMinValue)
  {
    Assert.expect(_verticalProfileMinValue != null);

    return _lightVerticalMinValue >= _verticalProfileMinValue.doubleValue();
  }

  /**
   * @author Reid Hayhow
   */
  public boolean isLightMeanGrayValueWithinLimits(Integer _meanPixelMinValue, Integer _meanPixelMaxValue)
  {
    Assert.expect(_meanPixelMinValue != null);
    Assert.expect(_meanPixelMaxValue != null);

    return ((_lightMeanGray <= _meanPixelMaxValue.intValue()) && (_lightMeanGray >= _meanPixelMinValue.intValue()));
  }

  /**
   * @author Reid Hayhow
   */
  public boolean isLightStandardDeviationWithinLimits(Double _standardDevMinValue, Double _standardDevMaxValue)
  {
    Assert.expect(_standardDevMinValue != null);
    Assert.expect(_standardDevMaxValue != null);

    return _lightStandardDeviation <= _standardDevMaxValue.doubleValue();
  }


  /**
   * @author Anthony Fong
   */
  public boolean isEdgeGradientWithinLimits(Double gradientMinValue, Double gradientMaxValue)
  {
    Assert.expect(gradientMinValue != null);
    Assert.expect(gradientMaxValue != null);

    //boolean result =_edgeMaxGradient >= gradientMinValue.doubleValue() && _edgeMaxGradient <= gradientMaxValue.doubleValue();
    boolean result =_edgeAvgGradient >= gradientMinValue.doubleValue() && _edgeAvgGradient <= gradientMaxValue.doubleValue();

    if(result==false)
    {
      int a=0;
    }
    return result;
  }

  /**
   * @author Anthony Fong
   */
  public boolean isEdgeStandardDeviationWithinLimits(Double standardDevMinValue, Double standardDevMaxValue)
  {
    Assert.expect(standardDevMinValue != null);
    Assert.expect(standardDevMaxValue != null);

    boolean result =_edgeStandardDeviation >= standardDevMinValue.doubleValue() && _edgeStandardDeviation <= standardDevMaxValue.doubleValue();

    if(result==false)
    {
      int a=0;
    }
    return result;
  }
    /**
   * @author Anthony Fong
   */
  public boolean isBetaAreaWithinLimits(Double betaAreaMinValue, Double betaAreaMaxValue)
  {
    Assert.expect(betaAreaMinValue != null);
    Assert.expect(betaAreaMaxValue != null);

    boolean result =  _betaArea>=betaAreaMinValue.doubleValue() && _betaArea <= betaAreaMaxValue.doubleValue();

    if(result==false)
    {
      int a=0;
    }
    return result;
  }

  /**
   * @author Reid Hayhow
   *
   * Store the calculated histogram so it can be retreived later and logged to
   * a file.
   */
  public void setLightHistogram(int[] histogram)
  {
    _lightHistogram = histogram;
  }

  /**
   * @author Reid Hayhow
   */
  public int[] getLightHistogram()
  {
    return _lightHistogram;
  }
//
//  /**
//   * @author Anthony Fong
//   *
//   * Store the calculated histogram so it can be retreived later and logged to
//   * a file.
//   */
//  public void setEdgeHistogram(int[] histogram)
//  {
//    _edgeHistogram = histogram;
//  }
//
//  /**
//   * @author Anthony Fong
//   */
//  public int[] getEdgeHistogram()
//  {
//    return _edgeHistogram;
//  }
  /**
   * @author Anthony Fong
   *
   * Store the calculated histogram so it can be retreived later and logged to
   * a file.
   */
  public void setEdgeFullHistogram(int[] histogram)
  {
    _edgeFullHistogram = histogram;
  }

  /**
   * @author Anthony Fong
   */
  public int[] getEdgeFullHistogram()
  {
    return _edgeFullHistogram;
  }

  /**
   * @author Reid Hayhow
   */
  public void setLightImage(Image lightImage)
  {
    Assert.expect(lightImage != null);

    _lightImage = lightImage;

    // Because we have created a new reference to the image, increment the refcount
    lightImage.incrementReferenceCount();
  }

  /**
   * @author Reid Hayhow
   */
  public Image getLightImage()
  {
    Assert.expect(_lightImage != null);

    return _lightImage;
  }

  /**
   * Method to actively dispose of the stored image
   *
   * @author Reid Hayhow
   */
  public void clearLightImage()
  {
    Assert.expect(_lightImage != null);

    // Decref the image while we have a handle to it
    _lightImage.decrementReferenceCount();

    // Then null out the reference
    _lightImage = null;
  }
}
