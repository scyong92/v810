package com.axi.v810.business.autoCal;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;
import com.axi.util.RunnableWithExceptions;
import java.util.concurrent.*;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import java.io.File;
import java.util.*;

/**
 * This class is responsible for providing a confirmation task that exercises
 * the individual benchmark Test for Performance Timing Checking /Benchmarking.
 *
 * @author Anthony Fong
 */
public class ConfirmBenchmarkTask extends EventDrivenHardwareTask
{

  private static int _progressReporterEnumIndex = 0;
  private static int _frequency = 1;
  private static long _timeoutInSeconds = 15000;
  //private static ConfirmBenchmarkTask _instance = null;  
  private static Map<ConfirmBenchmarkEnum, ConfirmBenchmarkTask> _instances = new HashMap<ConfirmBenchmarkEnum, ConfirmBenchmarkTask>();
  private PanelHandler _panelHandler = PanelHandler.getInstance();
  private LeftOuterBarrier _leftOuterBarrier = LeftOuterBarrier.getInstance();
  // In order to get a full test of both sides (in flow through mode) we have to run two cycles of the test
  private static final int _TIMES_TO_RUN_BENCHMARK_HANDLER_TEST = 2;
  private static final int _ESTIMATED_TIME_TO_RUN_IN_MILLIS = 20000;
  //Executor service and two timeouts needed to allow us to abort this task
  private static ExecutorService _executor = new ScheduledThreadPoolExecutor(1);
  //This is the timeout applied for one iteration of this task. If it takes any
  //longer than the timeout, it will be aborted
  private static final int _EXECUTION_TIMEOUT_IN_SECONDS = 250;
  //Since we are dealing with hardware, there is also a timeout for the abort
  //command. If it takes longer than this timeout, the system is shutdown
  private static final int _ABORT_TIMEOUT_IN_SECONDS = 250;
  //private String _name = "";
  private ConfirmBenchmarkEnum _benchmarkEnum;
  private boolean _resultPass = false;
  private Object[] _localizedStringArguments;
  private XrayTesterException _xrayTesterException;
  private HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();
  private boolean _testComplete;

  /**
   * ConfirmBenchmarkTask constructor. Private for instance only access.
   *
   * @author Anthony Fong
   */
  private ConfirmBenchmarkTask(String name, int frequency, long timeoutInSeconds,
    Object[] localizedStringArguments,
    ConfirmBenchmarkEnum benchmarkEnum,
    int progressReporterEnumIndex)
  {
    super(name, frequency, timeoutInSeconds,
      new ProgressReporterEnum(progressReporterEnumIndex, "HW_CONFIRM_BENCHMARK_PROGRESS_REPORTER_KEY", new Object[]
    {
      name
    }));

    _localizedStringArguments = localizedStringArguments;

    //This confirmation is to be run manually only
    _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;

    //Set enum used to save the timestamp for this confirmation
    _isHighMagTask = false;
    _resultPass = false;
    _benchmarkEnum = benchmarkEnum;
    try
    {
      if (_benchmarkEnum == ConfirmBenchmarkEnum.LONG_PATH_SCANNING)
      {
        //Loading the Long Scan Path from Path Configuration File
        BenchmarkScanMoves.getInstance().loadScanPathConfigurationFile();
      }
    }
    catch (XrayTesterException ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
      _xrayTesterException = ex;
    }
  }

  /**
   * Instance access for this class
   *
   * @author Anthony Fong
   */
  public static synchronized ConfirmBenchmarkTask getInstance(ConfirmBenchmarkEnum benchmarkEnum)
  {

    if (_instances.containsKey(benchmarkEnum) == false)
    {
      String name = "";
      Object[] localizedStringArguments = null;
      if (benchmarkEnum == ConfirmBenchmarkEnum.LEFT_OUTER_BARRIER_OPEN_CLOSE)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_LEFT_OUTER_BARRIER_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.RIGHT_OUTER_BARRIER_OPEN_CLOSE)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_RIGHT_OUTER_BARRIER_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.INNER_BARRIER_OPEN_CLOSE)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_INNER_OUTER_BARRIER_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.LEFT_PIP_IN_OUT)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_LEFT_PIP_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.RIGHT_PIP_IN_OUT)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_RIGHT_PIP_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.PANEL_CLAMPS_OPEN_CLOSE)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_PANEL_CLAMPS_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.PANEL_HANDLER_RAIL_WIDTH_ADJUST)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_PANEL_HANDLER_RAIL_WIDTH_ADJUST_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.PANEL_POSITIONER_LOAD_UNLOAD)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_PANEL_POSITIONER_MOTION_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.XRAY_TUBE_ACTUATOR_UP_DOWN)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_XRAY_TUBE_ACTUATOR_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      else if (benchmarkEnum == ConfirmBenchmarkEnum.LONG_PATH_SCANNING)
      {
        localizedStringArguments = new Object[]
        {
          StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_LONG_PATH_SCANNING_KEY", null))
        };
        name = StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", localizedStringArguments));
      }
      _instances.put(benchmarkEnum, new ConfirmBenchmarkTask(name, _frequency, _timeoutInSeconds, localizedStringArguments, benchmarkEnum, --_progressReporterEnumIndex));
    }
    return _instances.get(benchmarkEnum);
  }

  /**
   * @author Anthony Fong
   */
  public ConfirmBenchmarkEnum getBenchmarkingEnum()
  {
    return _benchmarkEnum;
  }

  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   *
   * @author Anthony Fong
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * No clean-up needed for this task
   *
   * @author Anthony Fong
   */
  protected void cleanUp() throws XrayTesterException
  {
    if (XrayActuator.isInstalled())
    {
      //ensure is at homing position after Benchmark Task Execution
      XrayActuator.getInstance().home(false, false);
    }

    if (_benchmarkEnum == ConfirmBenchmarkEnum.PANEL_HANDLER_RAIL_WIDTH_ADJUST)
    {
      //Move XY Stage and AWA to initial condition
      _panelHandler.homeStageRails();
    }
    else if (_benchmarkEnum == ConfirmBenchmarkEnum.PANEL_POSITIONER_LOAD_UNLOAD)
    {
      _panelHandler.moveXYStageToLeftLoadPosition(false);
    }

  }

  /**
   * Create a simple Boolean result for this task. There are no limits, it just
   * passes or fails.
   *
   * @author Anthony Fong
   */
  protected void createLimitsObject()
  {
    _limits = new Limit(null, null, null, null);
  }

  /**
   * Create a failing result to begin with. Then, if the task executes as
   * expected I will create a passing result to override this one.
   *
   * @author Anthony Fong
   */
  protected void createResultsObject()
  {
    _result = new Result(new Boolean(false), getName());
  }

  /**
   * @author Anthony Fong
   */
  public void executeLeftOuterBarrierOpenCloseTask() throws XrayTesterException
  {
    try
    {
      if (LeftOuterBarrier.isInstalled())
      {
        LeftOuterBarrier.getInstance().open();
        Thread.sleep(100);
        LeftOuterBarrier.getInstance().close();
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executeRightOuterBarrierOpenCloseTask() throws XrayTesterException
  {
    try
    {
      if (RightOuterBarrier.isInstalled())
      {
        RightOuterBarrier.getInstance().open();
        Thread.sleep(100);
        RightOuterBarrier.getInstance().close();
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executeInnerBarrierOpenCloseTask() throws XrayTesterException
  {
    try
    {
      if (InnerBarrier.isInstalled())
      {
        InnerBarrier.getInstance().open();
        Thread.sleep(100);
        InnerBarrier.getInstance().close();
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }

    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executeLeftPIPExtendRetractTask() throws XrayTesterException
  {
    try
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        LeftPanelInPlaceSensor.getInstance().extend();
        Thread.sleep(100);
        LeftPanelInPlaceSensor.getInstance().retract();
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executeRightPIPExtendRetractTask() throws XrayTesterException
  {
    try
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        RightPanelInPlaceSensor.getInstance().extend();
        Thread.sleep(100);
        RightPanelInPlaceSensor.getInstance().retract();
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executePanelClampsOpenCloseTask() throws XrayTesterException
  {
    try
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        PanelClamps.getInstance().open();
        Thread.sleep(100);
        PanelClamps.getInstance().close();
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executePanelHandlerRailWidthAdjustTask() throws XrayTesterException
  {
    try
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        _panelHandler.setRailWidthInNanoMeters(_panelHandler.getMinimumRailWidthInNanoMeters());
        Thread.sleep(100);
        _panelHandler.setRailWidthInNanoMeters(_panelHandler.getMaximumRailWidthInNanoMeters());
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executePanelPositionerMoveLoadUnloadTask() throws XrayTesterException
  {
    try
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        PanelHandler.getInstance().moveXYStageToRightUnloadPosition(false);
        Thread.sleep(100);
        PanelHandler.getInstance().moveXYStageToLeftLoadPosition(false);
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executeXRayTubeActuatorUpDownTask() throws XrayTesterException
  {
    try
    {
      if ((_panelHandler.isPanelLoaded() == false) && (XrayActuator.isInstalled()))
      {
        //move down for high mag.
        XrayActuator.getInstance().down(false);
        Thread.sleep(100);
        //move up for low mag.
        XrayActuator.getInstance().up(false);
        Thread.sleep(100);
        //move to ready or homing position.
        XrayActuator.getInstance().home(false, false);
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
      }
    }
    catch (Exception ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
    }
  }

  /**
   * @author Anthony Fong
   */
  public void executeLongPathScanningTask() throws XrayTesterException
  {
    _xrayTesterException = null;
    _testComplete = false;
    //_resultPass = false;
    try
    {
      HardwareTaskStatusEnum taskStatus = BenchmarkScanMoves.getInstance().runScanPaths(1);
      _result.setStatus(taskStatus);
      if (_result.getStatus() == HardwareTaskStatusEnum.NOT_RUN)
      {
        Object[] localizedStringArguments = new Object[]
        {
           BenchmarkScanMoves.getInstance().getLongPathScanFilePathForCDNABenchmark()
        };
        _result.setTaskStatusMessage(StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_SCAN_PATH_FILE_NOT_EXIST_KEY", localizedStringArguments)));
        localizedStringArguments = new Object[]
        {
           Directory.getScantMoveDirectoryForCDNABenchmarking()
        };
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString(new LocalizedString("CD_CONFIRM_BENCHMARK_SCAN_PATH_FILE_NOT_EXIST_ACTION_MESSAGE_KEY", localizedStringArguments)));
        _resultPass = false;
      }      
      else if (_result.getStatus() == HardwareTaskStatusEnum.PASSED)
      {
        _resultPass = true;
      }      
      else if (_result.getStatus() == HardwareTaskStatusEnum.FAILED)
      {
        _resultPass = false;
      }
    }
    catch (XrayTesterException ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.FAILED);
      _xrayTesterException = ex;
    }
    finally
    {
      _testComplete = true;
    }
  }

  /**
   * The core of this task. Make sure there is a not panel loaded in setup() and
   * then call the benchmark test.
   *
   * @author Anthony Fong
   */
  protected void executeTask() throws XrayTesterException
  {    
    //We must be on the worker thread
    Assert.expect(HardwareWorkerThread.isHardwareWorkerThread());
    
    HardwareWorkerThread.getInstance().invokeAndWait(new RunnableWithExceptions()
    {
      public void run() throws XrayTesterException
      {
        delegateTask();
      }
    });
  }

  /**
   * @author Anthony Fong
   */
  protected void delegateTask() throws XrayTesterException
  {
    _resultPass = false;
    // Report that this task is starting
    reportProgress(0);

    // Report that a major task has started that I cannot provide updates for
    // Instead I provide an estimate of how long it will take
    _progressObservable.reportAtomicTaskStarted(_progressReporterEnum,
      _ESTIMATED_TIME_TO_RUN_IN_MILLIS * _TIMES_TO_RUN_BENCHMARK_HANDLER_TEST);

    //We need to run the benchmark test on a separate thread with a timeout, so
    //create a future task that will do this for us. It returns true when done
    FutureTask<Boolean> benchmarkTest = new FutureTask<Boolean>(new Callable<Boolean>()
    {
      public Boolean call() throws XrayTesterException
      {
        try
        {
          //if (isSimulationModeOn() && (isUnitTestModeOn() == false))
          {
            if (_benchmarkEnum == ConfirmBenchmarkEnum.LEFT_OUTER_BARRIER_OPEN_CLOSE)
            {
              executeLeftOuterBarrierOpenCloseTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.RIGHT_OUTER_BARRIER_OPEN_CLOSE)
            {
              executeRightOuterBarrierOpenCloseTask();
              //For Simulation Testing Purpose
              //throw createBenchmarkExecutionProblemException(new XrayTesterException(new LocalizedString("CD_CONFIRM_BENCHMARK_KEY", _localizedStringArguments)));   
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.INNER_BARRIER_OPEN_CLOSE)
            {
              executeInnerBarrierOpenCloseTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.LEFT_PIP_IN_OUT)
            {
              executeLeftPIPExtendRetractTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.RIGHT_PIP_IN_OUT)
            {
              executeRightPIPExtendRetractTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.PANEL_CLAMPS_OPEN_CLOSE)
            {
              executePanelClampsOpenCloseTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.PANEL_HANDLER_RAIL_WIDTH_ADJUST)
            {
              executePanelHandlerRailWidthAdjustTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.PANEL_POSITIONER_LOAD_UNLOAD)
            {
              executePanelPositionerMoveLoadUnloadTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.XRAY_TUBE_ACTUATOR_UP_DOWN)
            {
              executeXRayTubeActuatorUpDownTask();
            }
            else if (_benchmarkEnum == ConfirmBenchmarkEnum.LONG_PATH_SCANNING)
            {
              executeLongPathScanningTask();
            }
            _resultPass = true;
          }
        }
        catch (Exception ex)
        {
          _result.setStatus(HardwareTaskStatusEnum.FAILED);
          _resultPass = false;
        }

        return Boolean.TRUE;
      }
    });

    //Submit the future task to execute the benchmark test to the executor
    _executor.submit(benchmarkTest);

    //Run the benchmark test a selected number of times
    try
    {
      //Call the get method with the specified timeout, it will throw a
      //TimeoutExeception if the task takes any longer than the timeout
      //NOTE: get() throws an ExecutionException if the call() method throws
      //an exception, so this does not throw an XrayTesterException, see below
      benchmarkTest.get(_EXECUTION_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
    }
    //Catch a timeout exception, the task took longer than we expected
    catch (TimeoutException ex)
    {
      //Handle the timeout by aborting the test, FAILING the task and throwing
      //an exception
      handleBenchmarkTestTimeout(ex);
    }
    //The unloadLoad task threw an exception, probably an XrayTesterException
    //because there was a problem executing the test
    catch (ExecutionException ex)
    {
      //Tricky case, here we want to unwrap the ExecutionException and throw
      //the root cause if it was an XrayTesterException
      if (ex.getCause() instanceof XrayTesterException)
      {
        logExceptionInfoAndThrowException((XrayTesterException) ex.getCause());
      }
      //Otherwise wrap the exception and throw it
      else
      {
        throw createBenchmarkExecutionProblemException(ex);
      }

    }
    catch (InterruptedException ex)
    {
      //Shouldn't happen, but handle it if it does by wrapping and throwing
      throw createBenchmarkExecutionProblemException(ex);
    }
    finally
    {
      // OK, we have control back, so report that the task is done
      _progressObservable.reportAtomicTaskComplete(_progressReporterEnum);
    }

    //Benchmark Result - Do not update result if HardwareTaskStatusEnum.NOT_RUN
    if (_result.getStatus() != HardwareTaskStatusEnum.NOT_RUN)
    {
      _result = new Result(_resultPass, getName());
      //If the benchmark test doesn't throw any exceptions, the task reulst according to _resultPass
      if (_resultPass = true)
      {
        _result.setStatus(HardwareTaskStatusEnum.PASSED);
      }
      else
      {
        _result.setStatus(HardwareTaskStatusEnum.FAILED);
      }

    }
  }

  /**
   *
   * @author Anthony Fong
   */
  protected boolean isExecuteNeeded()
  {
    //For now this task should never run from the HardwareTaskEngine
    //so always return false
    return false;
  }

  /**
   *
   * @author Anthony Fong
   */
  protected void setUp() throws XrayTesterException
  {
    createResultsObject();
    _result.setStatus(HardwareTaskStatusEnum.NOT_RUN);
    _resultPass = false;
    if (XrayActuator.isInstalled())
    {
      //move x-rays Actuator home.
      XrayActuator.getInstance().home(false, false);
    }

    // Make sure a panel still needs to be unloaded
    if (_panelHandler.isPanelLoaded())
    {
      // use standby since we are waiting for the user to unload the panel from the machine
      _panelHandler.unloadPanel();
    }

    //Move XY Stage and AWA to initial condition
    if (_benchmarkEnum == ConfirmBenchmarkEnum.PANEL_HANDLER_RAIL_WIDTH_ADJUST)
    {
      _panelHandler.homeStageRails();
    }
    else if (_benchmarkEnum == ConfirmBenchmarkEnum.PANEL_POSITIONER_LOAD_UNLOAD)
    {
      _panelHandler.moveXYStageToLeftLoadPosition(false);
    }
    else if (_benchmarkEnum == ConfirmBenchmarkEnum.XRAY_TUBE_ACTUATOR_UP_DOWN)
    {
      if (XrayActuator.isInstalled())
      {
        //move to ready or homing position.
        XrayActuator.getInstance().home(false, false);
      }
    }
  }

  /**
   * Method to handle the situation where the benchmark test took longer than
   * expected. It tries to abort the test, logs failure information and handles
   * the possibility that the abort call fails as well.
   *
   * @author Anthony Fong
   */
  private void handleBenchmarkTestTimeout(TimeoutException ex) throws XrayTesterException
  {
    //The abort task needs to be run with a timeout, so create a future task
    //to call the benchmark Test abort. It returns true when done
    FutureTask<Boolean> abortTask = new FutureTask<Boolean>(new Callable<Boolean>()
    {
      public Boolean call() throws XrayTesterException
      {
        _leftOuterBarrier.abort();
        return Boolean.TRUE;
      }
    });

    //Submit the abort task to the executor to be run
    _executor.submit(abortTask);

    try
    {
      //Wait for the abort task to finish
      abortTask.get(_ABORT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);

      //If the abort completed, we still need to fail the task and throw an exception
      //so, create the new exception indicating the benchmark test execution
      //had a problem
      HardwareTaskExecutionException htex = createBenchmarkExecutionProblemException(ex);

      //log the data from the exception and then throw it
      logExceptionInfoAndThrowException(htex);
    }
    //It is *really* bad if the abort task doesn't complete, all of these
    //catch blocks will shutdown the tester and throw an exception that is
    //distinct from the benchmark Test execution problem exception thrown above
    catch (TimeoutException ex1)
    {
      handleBenchmarkAbortTimeout(ex1);
    }
    catch (ExecutionException ex1)
    {
      handleBenchmarkAbortTimeout(ex1);
    }
    catch (InterruptedException ex1)
    {
      handleBenchmarkAbortTimeout(ex1);
    }
  }

  /**
   * Method to handle the severe situation where the call to abort the benchmark
   * test does not complete before the timeout.
   *
   * @author Anthony Fong
   */
  private void handleBenchmarkAbortTimeout(Exception exception) throws XrayTesterException
  {
    //Shutdown the tester
    XrayTester.getInstance().shutdown();

    //Throw an exception indicating the situation
    throw createBenchmarkAbortProblemException(exception);
  }

  /**
   * Common method to wrap an exception as a HardwareTaskExecutionException
   * caused by a problem executing the benchmark test so it can be thrown
   * through the XrayTesterException path
   *
   * @author Anthony Fong
   */
  private HardwareTaskExecutionException createBenchmarkExecutionProblemException(Exception exception)
  {
    //Create the exception
    HardwareTaskExecutionException newException = new HardwareTaskExecutionException(
      ConfirmationExceptionEnum.PROBLEM_EXECUTING_BENCHMARK_TEST);
    //set the init cause and stack trace to preserve that data
    newException.initCause(exception);
    newException.setStackTrace(exception.getStackTrace());

    //Create the localized string with parameters      
    newException.getLocalizedString().setArguments(_localizedStringArguments);

    return newException;
  }

  /**
   * Common method to wrap an exception as a HardwareTaskExecutionException
   * caused by a problem ABORTING the benchmark test so it can be thrown through
   * the XrayTesterException path
   *
   * @author Anthony Fong
   */
  private HardwareTaskExecutionException createBenchmarkAbortProblemException(Exception exception)
  {
    //Create the exception
    HardwareTaskExecutionException newException = new HardwareTaskExecutionException(
      ConfirmationExceptionEnum.PROBLEM_ABORTING_BENCHMARK_TEST);

    //set the init cause and stack trace to preserve that data
    newException.initCause(exception);
    newException.setStackTrace(exception.getStackTrace());

    //Create the localized string with parameters       
    newException.getLocalizedString().setArguments(_localizedStringArguments);

    return newException;
  }

  /**
   * Centralized method to log information from an exception encountered during
   * the execution of this task. An exception always guarantees the task fails.
   *
   * @author Anthony Fong
   */
  private void logExceptionInfoAndThrowException(XrayTesterException exception) throws XrayTesterException
  {
    _resultPass = false;
    //Create the localized string with parameters
    exception.getLocalizedString().setArguments(_localizedStringArguments);
 
    if (false) //true = will log this Exception into benchmarking.log beside adjustAndConfirm.log
    {
      //Log the specific error information to a file as well
      String exceptionMessage = exception.getLocalizedMessage();

      //_loggerForBenchmark.append(exception.getLocalizedMessage());
      //Trim-off ** or __ from message become one line
      exceptionMessage = getName() + " - " + StringLocalizer.keyToString("CD_EXCEPTION_KEY") + exceptionMessage;
      exceptionMessage = exceptionMessage.replace("**", "");
      exceptionMessage = exceptionMessage.replace("__", "");
      exceptionMessage = exceptionMessage.replace('\r', ' ');
      exceptionMessage = exceptionMessage.replace('\n', ' ');

      _loggerForBenchmark.append(exceptionMessage);
    }

    //Also set the message in the result object passed to observable
    _result.setTaskStatusMessage(exception.getLocalizedMessage());
    _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CONFIRM_BENCHMARK_ACTION_MESSAGE_KEY"));
    throw exception;
  }

  /**
   * We want to bypass dependant tasks for benchmark Test Confirmation task
   * because we really do not want to execute the camera-related tasks on this
   * confirmation task.
   *
   * @author Anthony Fong
   */
  public boolean isByPassDependantTasks()
  {
    return true;
  }

  /**
   * @author Anthony Fong
   */
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}