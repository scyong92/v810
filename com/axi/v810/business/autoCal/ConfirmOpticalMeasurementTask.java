package com.axi.v810.business.autoCal;

import java.awt.geom.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * This class responsible for measuring PSP repeatability.
 */
public abstract class ConfirmOpticalMeasurementTask extends EventDrivenHardwareTask implements OpticalLiveVideoInteractionInterface
{
  private static final int TOTAL_PROCESSING_LOOP = 26;
    
  private static final String OPTICAL_MEASUREMENT_REGION_NAME = "measurement_region";
  private static final String OPTICAL_MEASUREMENT_OPTICAL_IMAGES_DIR = "ConfirmOpticalMeasurement";
  
  protected String _name;
  private static int _frequency = 1;
  private static long _timeoutInSeconds = 20000;
  
  //Motion profile to use when moving stage, use the fastest
  protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
  
  //The config is needed to access the stored System Fiducial values
  protected static Config _config = Config.getInstance();
  
  private PanelHandler _panelHandler = PanelHandler.getInstance();
  private XrayActuatorInt _xrayActuator = XrayActuator.getInstance();
  
  //Log utility members
  private DirectoryLoggerAxi _logger; 
  private int _maxLogFileSizeInBytes = 10280000;
    
  protected Map<PspModuleEnum, StagePositionMutable> _moduleEnumToStagePositionMap = new LinkedHashMap<PspModuleEnum, StagePositionMutable>();
  private BooleanLock  _waitingForUserInput = new BooleanLock(false);
  private BooleanLock _waitForHardware = new BooleanLock(false);
  
  private static OpticalLiveVideoObservable _opticalLiveVideoObservable = OpticalLiveVideoObservable.getInstance();
  
  protected PspModuleEnum _moduleEnum;
  protected StagePositionMutable _stagePosition;
  
  protected List<Point2D> _points = new ArrayList<Point2D>();
  
  protected OpticalPointToPointScan _opticalPointToPointScan;
  
  protected double _zHeightInNanometers = -1;
  protected boolean _zHeightValid = false;
  protected double _meanZHeightInNanometers = -1;
  protected double _stdDevZHeightInNanometers = -1;
  protected boolean _passedMeanZHeight = false;
  protected boolean _passedStdDevZHeight = false;
  
  protected double _background1ZHeightInNanometers = 0;
  protected double _background2ZHeightInNanometers = 0;
  protected double _background3ZHeightInNanometers = 0;
  protected double _background4ZHeightInNanometers = 0;
  
  protected double _foreground1ZHeightInNanometers = 0;
  protected double _foreground2ZHeightInNanometers = 0;
  
  protected Map<Integer,Pair<Boolean,Double>> _runIdToAvgZHeightInNanometersMap = new LinkedHashMap<Integer,Pair<Boolean,Double>>();
  protected Map<Integer, List<Double>> _runIdToBackgroundZHeightInNanometersMap = new LinkedHashMap<Integer, List<Double>>();
  protected Map<Integer, List<Double>> _runIdToForegroundZHeightInNanometersMap = new LinkedHashMap<Integer, List<Double>>();
  
  protected Map<PspModuleTestTypeEnum, Limit> _pspModuleTestTypeEnumToLimitMap = new LinkedHashMap<PspModuleTestTypeEnum, Limit>();
  protected Map<PspModuleTestTypeEnum, Result> _pspModuleTestTypeEnumToResultMap = new LinkedHashMap<PspModuleTestTypeEnum, Result>();
  
  /**
   * Constructor for this confirmation
   *
   * @author Cheah Lee Herng
   */
  protected ConfirmOpticalMeasurementTask(String name,
                                          String logDirectory,
                                          String logFileName,
                                          ProgressReporterEnum progressReporter)
  {
    super(name, _frequency, _timeoutInSeconds, progressReporter);
    
    //Save the name passed in from the sub-class to use creating the limits
    _name = name;
    
    //Create the logger    
    _logger = new DirectoryLoggerAxi(logDirectory, 
                                     logFileName,
                                     FileName.getLogFileExtension(),
                                     _maxLogFileSizeInBytes);

    //This confirmation is to be run manually only
    _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;

    //Set the enum used to save the timestamp for this confirmation
    setTimestampEnum(PspCalibEnum.CONFIRM_OPTICAL_MEASUREMENT_REPEATABILITY_LAST_TIME_RUN);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean canRunOnThisHardware()
  {    
    return true;
  }

  /**   
   * @author Cheah Lee Herng
   */
  protected void cleanUp() throws XrayTesterException
  {
    if (_moduleEnumToStagePositionMap != null)
      _moduleEnumToStagePositionMap.clear();
    
    if (_runIdToAvgZHeightInNanometersMap != null)
      _runIdToAvgZHeightInNanometersMap.clear();
    
    if (_runIdToBackgroundZHeightInNanometersMap != null)
      _runIdToBackgroundZHeightInNanometersMap.clear();
    
    if (_runIdToForegroundZHeightInNanometersMap != null)
      _runIdToForegroundZHeightInNanometersMap.clear();
    
    if (_opticalPointToPointScan != null)
    {
      _opticalPointToPointScan.clear();
      _opticalPointToPointScan = null;
    }
    
    if (_pspModuleTestTypeEnumToLimitMap != null)  
      _pspModuleTestTypeEnumToLimitMap.clear();   
    
    if (_pspModuleTestTypeEnumToResultMap != null) 
      _pspModuleTestTypeEnumToResultMap.clear();
    
    _moduleEnum = null;
    _stagePosition = null;
  }
  
  /**
   * Create a Limits object specific to the particular test being run.
   *
   * @author Cheah Lee Herng
   */
  protected void createLimitsObject() throws DatastoreException
  {
    // Create empty limit object
    _limits = new Limit(null, null, null, _name);
    
    // 500-Micron Mean Z-Height Limit
    Double meanZHeight500MicronInNanometersLowLimit  = _parsedLimits.getLowLimitDouble("meanZHeight500MicronInNanometers");
    Double meanZHeight500MicronInNanometersHighLimit = _parsedLimits.getHighLimitDouble("meanZHeight500MicronInNanometers");
    
    Limit meanZHeight500MicronInNanometersLimit = new Limit(meanZHeight500MicronInNanometersLowLimit, 
                                                            meanZHeight500MicronInNanometersHighLimit, 
                                                            "nanometers", 
                                                            "ConfirmOpticalMeasurement-500micron-Mean-Limit");
    
    // 500-Micron Std Dev Z-Height Limit
    Double stdDevZHeight500MicronInNanometersLowLimit  = _parsedLimits.getLowLimitDouble("stdDevZHeight500MicronInNanometers");
    Double stdDevZHeight500MicronInNanometersHighLimit = _parsedLimits.getHighLimitDouble("stdDevZHeight500MicronInNanometers");
        
    Limit stdDevZHeight500MicronInNanometersLimit = new Limit(stdDevZHeight500MicronInNanometersLowLimit, 
                                                              stdDevZHeight500MicronInNanometersHighLimit, 
                                                              "nanometers", 
                                                              "ConfirmOpticalMeasurement-500micron-StdDev-Limit");
    
    // 50-Micron Mean Z-Height Limit
    Double meanZHeight50MicronInNanometersLowLimit  = _parsedLimits.getLowLimitDouble("meanZHeight50MicronInNanometers");
    Double meanZHeight50MicronInNanometersHighLimit = _parsedLimits.getHighLimitDouble("meanZHeight50MicronInNanometers");
    
    Limit meanZHeight50MicronInNanometersLimit = new Limit(meanZHeight50MicronInNanometersLowLimit, 
                                                           meanZHeight50MicronInNanometersHighLimit, 
                                                           "nanometers", 
                                                           "ConfirmOpticalMeasurement-50micron-Mean-Limit");
    
    // 50-Micron Std Dev Z-Height Limit
    Double stdDevZHeight50MicronInNanometersLowLimit  = _parsedLimits.getLowLimitDouble("stdDevZHeight50MicronInNanometers");
    Double stdDevZHeight50MicronInNanometersHighLimit = _parsedLimits.getHighLimitDouble("stdDevZHeight50MicronInNanometers");
        
    Limit stdDevZHeight50MicronInNanometersLimit = new Limit(stdDevZHeight50MicronInNanometersLowLimit, 
                                                             stdDevZHeight50MicronInNanometersHighLimit, 
                                                             "nanometers", 
                                                             "ConfirmOpticalMeasurement-50micron-StdDev-Limit");
    
    // Populate TestType with Limit into map
    _pspModuleTestTypeEnumToLimitMap.put(PspModuleTestTypeEnum.MEAN_ZHEIGHT_500_MICRON_IN_NANOMETERS, meanZHeight500MicronInNanometersLimit);
    _pspModuleTestTypeEnumToLimitMap.put(PspModuleTestTypeEnum.MEAN_ZHEIGHT_50_MICRON_IN_NANOMETERS, meanZHeight50MicronInNanometersLimit);
    _pspModuleTestTypeEnumToLimitMap.put(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_500_MICRON_IN_NANOMETERS, stdDevZHeight500MicronInNanometersLimit);
    _pspModuleTestTypeEnumToLimitMap.put(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_50_MICRON_IN_NANOMETERS, stdDevZHeight50MicronInNanometersLimit);
  }
  
  /**
   * createResultsObject Create the results object for this test result.
   *
   * @author Cheah Lee Herng
   */
  protected void createResultsObject()
  {
    //Create a result that is guaranteed to fail
    _result = new Result(new Boolean(false), _name);
    
    Result meanZHeight500MicronInNanometersResult = new Result(new Double(0.0), "ConfirmOpticalMeasurement-500micron-Mean-Result");
    Result stdDevZHeight500MicronInNanometersResult = new Result(new Double(0.0), "ConfirmOpticalMeasurement-500micron-StdDev-Result");
    Result meanZHeight50MicronInNanometersResult = new Result(new Double(0.0), "ConfirmOpticalMeasurement-50micron-Mean-Result");
    Result stdDevZHeight50MicronInNanometersResult = new Result(new Double(0.0), "ConfirmOpticalMeasurement-50micron-StdDev-Result");
    
    // Populate TestType with Result into map
    _pspModuleTestTypeEnumToResultMap.put(PspModuleTestTypeEnum.MEAN_ZHEIGHT_500_MICRON_IN_NANOMETERS, meanZHeight500MicronInNanometersResult);
    _pspModuleTestTypeEnumToResultMap.put(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_500_MICRON_IN_NANOMETERS, stdDevZHeight500MicronInNanometersResult);
    _pspModuleTestTypeEnumToResultMap.put(PspModuleTestTypeEnum.MEAN_ZHEIGHT_50_MICRON_IN_NANOMETERS, meanZHeight50MicronInNanometersResult);
    _pspModuleTestTypeEnumToResultMap.put(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_50_MICRON_IN_NANOMETERS, stdDevZHeight50MicronInNanometersResult);
  }
  
  /**   
   * @author Cheah Lee Herng
   */
  protected void executeTask() throws XrayTesterException
  {
    try
    {
      // Report that this task is starting
      reportProgress(0);

      // XCR-3314 Support PSP New Manual-Loaded Jig Plate
      if (PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_1) || 
          PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4))
      {
        // XCR-3408 System crash when cancelling to run manual optical confirmation
        if (_panelHandler.isPanelLoaded())
          _panelHandler.unloadPanel();
        
        loadOpticalCalibrationJig();
        //Handle the fact that we cannot run this task without a calibration
        //jig loaded into the system. Defer the task till later *but* don't cause the
        //task to fail. Display a meaningful message in the Service UI
        if (ableToRunWithCurrentPanelState() == false)
        {
          _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
          _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_NO_CALIBRATION_JIG_LOADED_KEY"));
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_LOAD_CALIBRATION_JIG_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
          return;
        }
      }
      else
      {
        // We handle the fact that we cannot set the rail width with panel loaded in the machine.
        if (_panelHandler.isPanelLoaded() == false)
        {
          PanelHandler.getInstance().setRailWidthInNanoMeters(PspConfiguration.getInstance().getOpticalMeasurementConfirmationRailWidthInNanometers());
        }
      }
      checkAbortInProgress();
      
      // Increment progress bar - 20%
      reportProgress(20);
      
      //Setup system fiducial stage position
      setupSystemFiducialStagePosition();
      
      // Retrieve the correct Psp Module
      _moduleEnum = getPspModuleEnum();
      
      // Retrieve stage position
      _stagePosition = getStagePosition(_moduleEnum);

      PspImageAcquisitionEngine.getInstance().handleActualOpticalView(_moduleEnum, OpticalShiftNameEnum.WHITE_LIGHT);          

      // Increment progress bar - 30%
      reportProgress(30);     
      
      // Move stage to first optical camera
      moveStageBeforeExecute();

      // Increment progress bar - 40%
      reportProgress(40);

      // Display Optical Live Video, starting with first optical camera Id
      _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                                     getOpticalCameraIdEnum(),
                                                                     StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"),
                                                                     this,
                                                                     false,
                                                                     false,
                                                                     true));

      // Wait for user to mark location for repeatability measurement
      waitForUserInput();

      // Here we check if user cancel live video so that
      // we can exit the test gracefully
      checkAbortInProgress();
      
      if (_taskCancelled == false)
      {
        // Record current stage position for repeatability measurement
        recordCurrentStagePosition();
      }
      
      // Send event to close live video dialog
      _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));
      
      // Here we check if user cancel live video so that
      // we can exit the test gracefully
      checkAbortInProgress();
      
      // For repeatability measurement, we only interested in the raw Psp height value. We do not want offset value.
      PspEngine.getInstance().setReturnRawPspHeight(true);
      
      // For repeatability measurement, we also want to turn off filter outlier because we are taking more than 1 point including background and foreground
      PspEngine.getInstance().setFilterOutlier(false);
      
      // We need to make sure PSP Hardware is taken care before start acquiring optical images      
      waitForHardware();
      
      // Start run optical measurement repeatability
      for(int i=0; i < TOTAL_PROCESSING_LOOP; ++i)
      {
        // Here we check if user cancel live video so that
        // we can exit the test gracefully
        checkAbortInProgress();
        
        // Clean up OpticalPointToPointScan before proceed
        if (_opticalPointToPointScan != null)
        {
          _opticalPointToPointScan.clear();
          _opticalPointToPointScan = null;
        }
        
        // Generate OpticalPointToPointScan
        _opticalPointToPointScan = getOpticalPointToPointScan(_stagePosition);
        
        // Reset information
        _opticalPointToPointScan.reset();
        
        // Reset local variables
        _zHeightValid = false;
        _zHeightInNanometers = -1;
        
        // Start acquire optical image
        acquireOpticalImage();

        // Process z-height
        processZHeight();
        
        // Store result into map for later retrieval
        if (_runIdToAvgZHeightInNanometersMap.containsKey(i) == false && i != 0)
        {
          Pair<Boolean, Double> result = new Pair<Boolean, Double>(_zHeightValid, _zHeightInNanometers);
          _runIdToAvgZHeightInNanometersMap.put(i, result);
        }
        
        if (_runIdToBackgroundZHeightInNanometersMap.containsKey(i) == false && i != 0)
        {
          List<Double> backgroundZHeights = new ArrayList<Double>();
          backgroundZHeights.add(_background1ZHeightInNanometers);
          backgroundZHeights.add(_background2ZHeightInNanometers);
          backgroundZHeights.add(_background3ZHeightInNanometers);
          backgroundZHeights.add(_background4ZHeightInNanometers);
          
          _runIdToBackgroundZHeightInNanometersMap.put(i, backgroundZHeights);
        }
        
        if (_runIdToForegroundZHeightInNanometersMap.containsKey(i) == false && i != 0)
        {
          List<Double> foregroundZHeights = new ArrayList<Double>();
          foregroundZHeights.add(_foreground1ZHeightInNanometers);
          foregroundZHeights.add(_foreground2ZHeightInNanometers);
          
          _runIdToForegroundZHeightInNanometersMap.put(i, foregroundZHeights);
        }
        
        // Skip the first measurement. It may not be stable yet.
        if (i == 0)
          continue;
        
        // Increment progress bar
        reportProgress(40 + ((50 / (TOTAL_PROCESSING_LOOP - 1)) * i));
      }
      
      // Process end result
      calculateMeasurement();
      
      //Accumulator to know if this image passed or failed limit checking      
      boolean passedMeanZHeight500Micron    = false;
      boolean passedStdDevZHeight500Micron  = false;
      boolean passedMeanZHeight50Micron     = false;
      boolean passedStdDevZHeight50Micron   = false;
      
      // Check on 500-micron Mean Z-Height
      passedMeanZHeight500Micron = _pspModuleTestTypeEnumToLimitMap.get(PspModuleTestTypeEnum.MEAN_ZHEIGHT_500_MICRON_IN_NANOMETERS).didTestPass(
                                                  _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.MEAN_ZHEIGHT_500_MICRON_IN_NANOMETERS));
                  
      // Check on 500-micron Std Dev Z-Height
      passedStdDevZHeight500Micron = _pspModuleTestTypeEnumToLimitMap.get(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_500_MICRON_IN_NANOMETERS).didTestPass(
                                                  _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_500_MICRON_IN_NANOMETERS));
      
      // Check on 50-micron mean
      passedMeanZHeight50Micron = _pspModuleTestTypeEnumToLimitMap.get(PspModuleTestTypeEnum.MEAN_ZHEIGHT_50_MICRON_IN_NANOMETERS).didTestPass(
                                                  _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.MEAN_ZHEIGHT_50_MICRON_IN_NANOMETERS));

      // Check on 50-micron Std Dev Z-Height
      passedStdDevZHeight50Micron = _pspModuleTestTypeEnumToLimitMap.get(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_50_MICRON_IN_NANOMETERS).didTestPass(
                                                  _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_50_MICRON_IN_NANOMETERS));            
                  
      if (isTestPass(passedMeanZHeight500Micron,
                     passedStdDevZHeight500Micron, 
                     passedMeanZHeight50Micron, 
                     passedStdDevZHeight50Micron) == false)
      {
        Object[] parameters = null;
        LocalizedString localizedString = null;
        
        // Check if mean z-height is out of spec
        if (passedMeanZHeight500Micron)
        {
          _passedMeanZHeight = true;
          
          if (passedStdDevZHeight500Micron == false)
          {
            parameters = new Object[] { _stdDevZHeightInNanometers };
            localizedString = new LocalizedString("ACD_OPTICAL_MEASUREMENT_REPEATABILITY_STD_DEV_OUT_OF_EXPECTED_RANGE_KEY", parameters);
            
            _passedStdDevZHeight = false;
          }
          else
            _passedStdDevZHeight = true;
        }
        else if (passedMeanZHeight50Micron)
        {
          _passedMeanZHeight = true;
          
          if (passedStdDevZHeight50Micron == false)
          {
            parameters = new Object[] { _stdDevZHeightInNanometers };
            localizedString = new LocalizedString("ACD_OPTICAL_MEASUREMENT_REPEATABILITY_STD_DEV_OUT_OF_EXPECTED_RANGE_KEY", parameters);
            
            _passedStdDevZHeight = false;
          }
          else
            _passedStdDevZHeight = true;
        }
        else
        {
          parameters = new Object[] { _meanZHeightInNanometers };
          localizedString = new LocalizedString("ACD_OPTICAL_MEASUREMENT_REPEATABILITY_MEAN_OUT_OF_EXPECTED_RANGE_KEY", parameters);
          
          _passedMeanZHeight = false;
          
          if (passedMeanZHeight500Micron == false)
          {
            if (passedStdDevZHeight500Micron)
              _passedStdDevZHeight = true;
            else
              _passedStdDevZHeight = false;
          }
          else if (passedMeanZHeight50Micron == false)
          {
            if (passedStdDevZHeight50Micron)
              _passedStdDevZHeight = true;
            else
              _passedStdDevZHeight = false;
          }
        }
        
        // Failing result
        _result.setTaskStatusMessage(StringLocalizer.keyToString(localizedString));
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("ACD_OPTICAL_MEASUREMENT_REPEATABILITY_FAILED_VALUE_OUT_OF_RANGE_SUGGESTED_ACTION_KEY"));
      }
      else
      {
        _passedMeanZHeight = true;
        _passedStdDevZHeight = true;
        
        // Set the task to pass
        _result.setResults(new Boolean(true));
      }
      
      logInformation();
    }
    catch (XrayTesterException ex)
    {
      _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);
      _result.setTaskStatusMessage(ex.getLocalizedMessageWithoutHeaderAndFooter());
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_EXCEPTION_IN_OPTICAL_MEASUREMENT_REPEATABILITY_TASK_ACTION_KEY"));
      return;
    }
    //Log any unexpected exceptions
    catch (Exception e)
    {
      Assert.logException(e);
    }
    finally
    {
      // Reset returnRawPspHeight flag to false
      PspEngine.getInstance().setReturnRawPspHeight(false); 
      
      // Reset Filter Outlier flag to true
      PspEngine.getInstance().setFilterOutlier(true);
      
      // Update progress bar
      reportProgress(100);            
    }
  }

  /**   
   * @author Cheah Lee Herng 
   */
  protected void setUp() throws XrayTesterException
  {
    // Do nothing
  }

  /**   
   * @author Cheah Lee Herng 
   */
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
  
  /**
   * @author Cheah Lee Herng 
  */
  protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
  {
    // We need to have calibration jig loaded so that we can run this task.
    return (_panelHandler.isPanelLoaded() == true);
  }
  
  /**
   * @author Cheah Lee Herng
   */
  protected void waitForUserInput()
  {
    _waitingForUserInput.setValue(true);
    try
    {
      _waitingForUserInput.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      // Do nothing ...
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void waitForHardware()
  {
    _waitForHardware.setValue(true);
    try
    {
      _waitForHardware.waitUntilFalse();
    }
    catch (InterruptedException ex)
    {
      // Do nothing ...
    }
  }
  
  /*
   * @author Cheah Lee Herng
  */
  protected void checkAbortInProgress()
  {
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
  }
  
  /**
   * @author Cheah Lee Herng
  */
  protected void loadOpticalCalibrationJig() throws XrayTesterException
  {
    if (_panelHandler.isPanelLoaded() == false)
    {
      if (XrayActuator.isInstalled())
      {  
        if (XrayCylinderActuator.isInstalled() == true)
        {
          // Always move the cylinder to UP position
          _xrayActuator.up(false);
        }
        else if (XrayCPMotorActuator.isInstalled() == true)
        {
          // Always move the Z Axis to Home and Safe position
          _xrayActuator.home(false,false);
        }         
      }      
      _panelHandler.loadPanel("Panel for Service Gui", 
                              PspConfiguration.getInstance().getOpticalCalibrationJigWidthInNanometers(),
                              PspConfiguration.getInstance().getOpticalCalibrationJigHeightInNanometers(), 
                              false, 0);
    }
  }
  
  /**
   * @author Cheah Lee Herng
  */
  private void setupSystemFiducialStagePosition()
  {
    Assert.expect(_moduleEnumToStagePositionMap != null);

    // Clear list before populate the latest value
    _moduleEnumToStagePositionMap.clear();

    // System Fiducial Stage Position Optical Camera 1
    StagePositionMutable systemFiducialCamera1StagePosition = new StagePositionMutable();
    systemFiducialCamera1StagePosition.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers());
    systemFiducialCamera1StagePosition.setYInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers());
    _moduleEnumToStagePositionMap.put(PspModuleEnum.FRONT, systemFiducialCamera1StagePosition);

    // System Fiducial Stage Position Optical Camera 2
    StagePositionMutable systemFiducialCamera2StagePosition = new StagePositionMutable();
    systemFiducialCamera2StagePosition.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers());
    systemFiducialCamera2StagePosition.setYInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers());
    _moduleEnumToStagePositionMap.put(PspModuleEnum.REAR, systemFiducialCamera2StagePosition);
  }
  
  /**
   * @author Cheah Lee Herng
  */
  protected void moveStageBeforeExecute() throws XrayTesterException
  {
    // panel needs to be repositioned left
    if (_panelHandler.isPanelLoaded())
      _panelHandler.movePanelToLeftSide();
    
    //Profile1 is fastest
    PanelPositioner.getInstance().setMotionProfile(new MotionProfile(_motionProfile));

    // Move stage to existing center position before performing CD&A
    PanelPositioner.getInstance().pointToPointMoveAllAxes(_stagePosition);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void runNextOpticalCamera()
  {
    _waitingForUserInput.setValue(false);
  }

  /**
   * @author Cheah Lee Herng
   */
  public void cancelLiveVideo()
  {
    _taskCancelled = true;
    _waitingForUserInput.setValue(false);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void setSurfaceMapPoint(List<Point2D> points)
  {
    _points = points;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setHardwareShutdownComplete()
  {
    _waitForHardware.setValue(false);
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  private StagePositionMutable getStagePosition(PspModuleEnum moduleEnum)
  {
    Assert.expect(moduleEnum != null);
    Assert.expect(_moduleEnumToStagePositionMap != null); 
    
    return _moduleEnumToStagePositionMap.get(moduleEnum);
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private OpticalCameraIdEnum getOpticalCameraIdEnum(PspModuleEnum moduleEnum)
  {
    Assert.expect(moduleEnum != null);
    
    if (moduleEnum.equals(PspModuleEnum.FRONT))
      return OpticalCameraIdEnum.OPTICAL_CAMERA_1;
    else
      return OpticalCameraIdEnum.OPTICAL_CAMERA_2;
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  protected void acquireOpticalImage() throws XrayTesterException
  {    
    Assert.expect(_opticalPointToPointScan != null);        
    
    // Added by Lee Herng 16 July 2015 - Create date/time folder to store optical images
    long timeInMils = System.currentTimeMillis();
    String opticalImageSetName = Directory.getDirName(timeInMils);
    
    boolean isSavePspOpticalImages = _config.getBooleanValue(SoftwareConfigEnum.SAVE_PSP_INSPECTION_IMAGES);
    if (isSavePspOpticalImages)
    {
      String opticalImageSetDirectory = Directory.getOpticalImagesDir(OPTICAL_MEASUREMENT_OPTICAL_IMAGES_DIR, opticalImageSetName);
      if (FileUtilAxi.existsDirectory(opticalImageSetDirectory) == false)
      {
        FileUtilAxi.createDirectory(opticalImageSetDirectory);
      }
    }
    
    PspEngine.getInstance().performHeightMapOperation(_opticalPointToPointScan, OPTICAL_MEASUREMENT_OPTICAL_IMAGES_DIR, opticalImageSetName);            
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private OpticalPointToPointScan getOpticalPointToPointScan(StagePosition stagePosition)
  {
    Assert.expect(stagePosition != null);
    
    String pointPositionName = stagePosition.getXInNanometers() + "_" + stagePosition.getYInNanometers();
    
    Map<String,Pair<Integer,Integer>> pointPanelPositionNameToPointPositionInPixelMap = new java.util.LinkedHashMap<String,Pair<Integer,Integer>>();            
    for(Point2D point : _points)
    {
      String pointPositionPixel = point.getX() + "_" + point.getY();
      Pair<Integer,Integer> pointPositionInPixelPair = new Pair<Integer,Integer>((int)point.getX(), (int)point.getY());
      pointPanelPositionNameToPointPositionInPixelMap.put(pointPositionPixel, pointPositionInPixelPair);
    }    

    OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
    opticalPointToPointScan.setId(1);
    opticalPointToPointScan.setOpticalCameraIdEnum(getOpticalCameraIdEnum(_moduleEnum));
    opticalPointToPointScan.setRegionPositionName(OPTICAL_MEASUREMENT_REGION_NAME);
    opticalPointToPointScan.setPointPositionName(pointPositionName);
    opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(stagePosition);
    opticalPointToPointScan.addPointPanelPositionNameToPointPositionInPixel(pointPanelPositionNameToPointPositionInPixelMap);    
    opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
    opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());
    opticalPointToPointScan.setOpticalCameraRectangleName(OPTICAL_MEASUREMENT_REGION_NAME);
    opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
    opticalPointToPointScan.setReturnActualHeightMap(false);

    return opticalPointToPointScan;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private void processZHeight()
  {
    Assert.expect(_opticalPointToPointScan != null);
    
    double avgBackgroundZHeightInNanometers = 0;
    double avgForegroundZHeightInNanometers = 0;
    double totalBackgroundZHeightInNanometers = 0;
    double totalForegroundZHeightInNanometers = 0;
    
    boolean validBackgroundZHeight = false;
    boolean validForegroundZHeight = false;
    
    int totalBackgroundZHeight = 0;
    int totalForegroundZHeight = 0;
    
    int index = -1;        
    
    for(Map.Entry<String, BooleanRef> entry : _opticalPointToPointScan.getPointPanelPositionNameToZHeightValidMap().entrySet())
    {
      String pointPanelPositionName = entry.getKey();
      BooleanRef zHeightValidity = entry.getValue();
      
      ++index;
      
      if (zHeightValidity.getValue() == true)
      {
        double zHeightInNanometers = _opticalPointToPointScan.getZHeightInNanometers(pointPanelPositionName);
        
        if (index == 0 || index == 1 || index == 4 || index == 5) // Background Z-height
        {
          if (index == 0)
            _background1ZHeightInNanometers = zHeightInNanometers;
          else if (index == 1)
            _background2ZHeightInNanometers = zHeightInNanometers;
          else if (index == 4)
            _background3ZHeightInNanometers = zHeightInNanometers;
          else if (index == 5)
            _background4ZHeightInNanometers = zHeightInNanometers;
          
          totalBackgroundZHeightInNanometers += zHeightInNanometers;
          validBackgroundZHeight = true;
          
          ++totalBackgroundZHeight;
        }
        else if (index == 2 || index == 3)  // Foreground Z-height
        {
          if (index == 2)
            _foreground1ZHeightInNanometers = zHeightInNanometers;
          else if (index == 3)
            _foreground2ZHeightInNanometers = zHeightInNanometers;
          
          totalForegroundZHeightInNanometers += zHeightInNanometers;
          validForegroundZHeight = true;
          
          ++totalForegroundZHeight;
        }
      }
    }
    
    if (validBackgroundZHeight && validForegroundZHeight)
    {
      if (totalBackgroundZHeight > 0)
        avgBackgroundZHeightInNanometers = totalBackgroundZHeightInNanometers / totalBackgroundZHeight;
      else
        avgBackgroundZHeightInNanometers = 0;
      
      if (totalForegroundZHeight > 0)
        avgForegroundZHeightInNanometers = totalForegroundZHeightInNanometers / totalForegroundZHeight;
      else
        avgForegroundZHeightInNanometers = 0;
      
      _zHeightInNanometers = avgForegroundZHeightInNanometers - avgBackgroundZHeightInNanometers;
      _zHeightValid = true;
    }
    else
    {
      _zHeightInNanometers = -1;
      _zHeightValid = false;
    }        
  }
  
  /**
   * This function calculates the mean and standard deviation of the z-height measurement.
   * 
   * @author Cheah Lee Herng
   */
  private void calculateMeasurement()
  {
    Assert.expect(_runIdToAvgZHeightInNanometersMap != null);
    
    List<Double> validZHeightInNanometers = new ArrayList<Double>();    
    for(Map.Entry<Integer, Pair<Boolean, Double>> entry : _runIdToAvgZHeightInNanometersMap.entrySet())
    {
      Pair<Boolean, Double> result = entry.getValue();
      
      if (result.getFirst() == true)
      {        
        validZHeightInNanometers.add(Math.abs(result.getSecond()));
      }
    }       
    
    // Converting ArrayList to Float Array
    float[] zHeightInNanometers = new float[validZHeightInNanometers.size()];
    for(int i=0; i < validZHeightInNanometers.size(); ++i)
    {
      zHeightInNanometers[i] = validZHeightInNanometers.get(i).floatValue();
    }
    
    if (zHeightInNanometers.length > 0)
    {
      _meanZHeightInNanometers = StatisticsUtil.mean(zHeightInNanometers);          
      _stdDevZHeightInNanometers = StatisticsUtil.standardDeviation(zHeightInNanometers);            
    }
    else
    {
      _meanZHeightInNanometers = 0;
      _stdDevZHeightInNanometers = 0;
    }
    
    // Populate result
    _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.MEAN_ZHEIGHT_500_MICRON_IN_NANOMETERS).setResults(new Double(_meanZHeightInNanometers));      
    _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_500_MICRON_IN_NANOMETERS).setResults(new Double(_stdDevZHeightInNanometers));
    _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.MEAN_ZHEIGHT_50_MICRON_IN_NANOMETERS).setResults(new Double(_meanZHeightInNanometers));
    _pspModuleTestTypeEnumToResultMap.get(PspModuleTestTypeEnum.STD_DEV_ZHEIGHT_50_MICRON_IN_NANOMETERS).setResults(new Double(_stdDevZHeightInNanometers));
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  private void logInformation() throws DatastoreException
  {
    StringBuilder logString = new StringBuilder();

    String passMeanZHeightString = " (PASS)";
    String passStdDevZHeightString = " (PASS)";
    
    if (_passedMeanZHeight == false)
      passMeanZHeightString = " (FAIL)";
    
    if (_passedStdDevZHeight == false)
      passStdDevZHeightString = " (FAIL)";
    
    // Create header lines
    logString.append(_name + "\n,\n");
    logString.append("Mean Z-Height (nm) = " + _meanZHeightInNanometers + passMeanZHeightString + "\n");
    logString.append("Std Dev Z-Height (nm) = " + _stdDevZHeightInNanometers + passStdDevZHeightString + "\n\n");    
    logString.append(",Index, Background1(nm), Background2(nm), Background3(nm), Background4(nm), Foreground1(nm), Foreground2(nm), Avg Z-Height(nm),Z-Height Validity\n");
    
    for(Map.Entry<Integer, Pair<Boolean, Double>> entry : _runIdToAvgZHeightInNanometersMap.entrySet())
    {
      int index = entry.getKey();
      Pair<Boolean, Double> resultInfo = entry.getValue();          
      boolean validZHeight = resultInfo.getFirst();
      double avgZHeightInNanometers = resultInfo.getSecond();
      List<Double> backgroundZHeights = new ArrayList<Double>();
      List<Double> foregroundZHeights = new ArrayList<Double>();
      
      String log = ",," + index;
      
      if (_runIdToBackgroundZHeightInNanometersMap.containsKey(index))
      {
        backgroundZHeights = _runIdToBackgroundZHeightInNanometersMap.get(index);
        for(Double backgroundZHeight : backgroundZHeights)
        {
          log = log + "," + String.format( "%.2f", backgroundZHeight.doubleValue());
        }
      }
      
      if (_runIdToForegroundZHeightInNanometersMap.containsKey(index))
      {
        foregroundZHeights = _runIdToForegroundZHeightInNanometersMap.get(index);
        for(Double foregroundZHeight : foregroundZHeights)
        {
          log = log + "," + String.format( "%.2f", foregroundZHeight.doubleValue());
        }
      }
      
      log = log + "," + String.format( "%.2f", avgZHeightInNanometers) + "," + validZHeight + "\n";
      
      logString.append(log);
    }

    _logger.log(logString.toString());
    logString = null;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  protected void recordCurrentStagePosition() throws XrayTesterException
  {    
    _stagePosition.setXInNanometers(PanelPositioner.getInstance().getXaxisActualPositionInNanometers());
    _stagePosition.setYInNanometers(PanelPositioner.getInstance().getYaxisActualPositionInNanometers());
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected boolean isTestPass(boolean passedMeanZHeight500Micron,
                               boolean passedStdDevZHeight500Micron,
                               boolean passedMeanZHeight50Micron,
                               boolean passedStdDevZHeight50Micron)
  {
    boolean isTestPass = false;    
    if (passedMeanZHeight500Micron)
    {
      if (passedStdDevZHeight500Micron)
      {
        isTestPass = true;
      }
    }
    else if (passedMeanZHeight50Micron)
    {
      if (passedStdDevZHeight50Micron)
      {
        isTestPass = true;
      }
    }
    return isTestPass;
  }
  
  protected abstract PspModuleEnum getPspModuleEnum();
  
  protected abstract OpticalCameraIdEnum getOpticalCameraIdEnum();
}
