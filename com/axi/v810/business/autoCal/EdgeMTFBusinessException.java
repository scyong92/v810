package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * This exception is thrown if the Edge MTF algorithm fails.  The typical case for
 * failure is when the image parameter has multiple edges or no edges.
 *
 * @author John Dutton
 */
public class EdgeMTFBusinessException extends BusinessException
{
  public EdgeMTFBusinessException()
  {
    super(new LocalizedString("CD_EXCEPTION_EDGE_MTF_KEY", null));

    // To simplify debugging, add the method and line number where this object is being created.
    StackTraceElement[] stackElements = getStackTrace();
    Assert.expect(stackElements.length > 0, "stack trace is unexpectedly small");
    Object [] argumentArray = new Object[] {stackElements[0].getMethodName(),
                                            stackElements[0].getLineNumber()};
    getLocalizedString().setArguments(argumentArray);
  }
}




