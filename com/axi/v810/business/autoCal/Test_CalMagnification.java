package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * @author Eddie Williamson
 */
public class Test_CalMagnification extends UnitTest
{
  private Config _config = Config.getInstance();

  /**
   * @author Roy Williams
   */
  protected Test_CalMagnification()
  {
    super();
  }

  /**
   * @author Eddie Williamson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      TestUtils.initConfigForRegressionTest();

      _config.loadIfNecessary();

      // start camera simulators
//   int numberRunningCameraSimulators = launchCameraSimulators(is, os, "SystemFiducial" + File.separator);
//   if (XrayCameraArray.getInstance().getNumberOfCameras() != numberRunningCameraSimulators)
//     throw new IOException("Unable to launch launchCameraSimulators");
//   XrayCameraArray.getInstance().setDeepSimulationMode(true);

      CalMagnification cal = CalMagnification.getInstance();
//      cal.executeNonInteractively();
      Expect.expect(true);
//   Expect.expect(cal.didTestPass());

    }
    catch (Exception e)
    {
      e.printStackTrace();
      Expect.expect(false, e.getMessage());
    }
    finally
    {
//      restoreCameraPorts();
//      XrayCameraArray.getInstance().setDeepSimulationMode(false);
      TestUtils.restoreConfigAfterRegressionTest();
    }

  }


  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CalMagnification());
  }


}


