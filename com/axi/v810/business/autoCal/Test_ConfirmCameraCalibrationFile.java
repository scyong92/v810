package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Reid Hayhow
 */
public class Test_ConfirmCameraCalibrationFile extends UnitTest
{

  private Config _config = Config.getInstance();
  /**
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ConfirmCameraCalibrationFile());
  }

  /**
   * @author Reid Hayhow
   */
  protected Test_ConfirmCameraCalibrationFile()
  {
    super();

    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException de)
    {
      de.printStackTrace();
    }
  }

  /**
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    boolean didTaskPass = false;

    TestUtils utils = new TestUtils();
    utils.backupHWCalib(getTestSourceDir());
    utils.backupHWConfig(getTestSourceDir());
    try
    {
      // Startup before trying to execute tasks
      XrayTester.getInstance().setSimulationMode();
      XrayTester.getInstance().startup();

      //Get the instance of this Confirmation Task
      ConfirmCameraCalibration calFileConfirm = ConfirmCameraCalibration.getInstance();

      //Execute the task
      didTaskPass = calFileConfirm.executeUnconditionally();

      //Start and Stop dates should be set, the results should all be passed
      checkForStartStopDatesAndStatus(calFileConfirm.getResult());

      //If not, dump the failure info into the .actual file and assert
      if (didTaskPass == false)
      {
        displayFailureData(calFileConfirm.getResult(), calFileConfirm.getLimit(), os);
        Expect.expect(false, "Camera Cal File Confirmation Test failed");
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't occur.
      Assert.expect(false);
    }
    catch (Exception e)
    {
      e.printStackTrace(os);
      Expect.expect(false, e.getMessage());
      System.out.println(e.getMessage());
    }
    finally
    {
      utils.restoreHWCalib(getTestSourceDir());
      utils.restoreHWConfig(getTestSourceDir());
    }

  }


  /**
   * Function to dump out specific failure data to the regression test stream.
   * This should force a failure since the expect will be different from the
   * actual.
   *
   * @author Reid Hayhow
   */
  private void displayFailureData(Result result, Limit limits, PrintWriter os)
  {
    List<Result> resultList = result.getResultList();
    List<Limit> limitsList = limits.getLimitList();
    Iterator<Limit> iter = limitsList.iterator();

    Assert.expect(resultList != null);

    if (resultList.isEmpty())
    {
      if (limits.didTestPass(result) == false)
      {
        os.println("result " + limits.getLimitName() + " failed!");
      }
    }
    else
    {
      for (Result subResult : resultList)
      {
        displayFailureData(subResult, iter.next(), os);
      }
    }
  }

  /**
   * Function to assure that the date and status have been set properly
   *
   * @author Reid Hayhow
   */
  private void checkForStartStopDatesAndStatus(Result result)
  {
    List<Result> resultList = result.getResultList();

    //We are at a leaf node, check it
    if (resultList.isEmpty())
    {
      String passedString = StringLocalizer.keyToString("CD_PASSED_KEY");
      Assert.expect(result.getStartDate() != null);
      Assert.expect(result.getStopDate() != null);
      Assert.expect(result.getStatusString().equalsIgnoreCase(passedString));
    }
    else
    {
      for (Result subResult : resultList)
      {
        checkForStartStopDatesAndStatus(subResult);
      }
    }
  }
}
