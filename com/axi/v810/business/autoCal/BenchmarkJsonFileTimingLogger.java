package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.google.gson.Gson;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * This class will save the Performance Timing and Benchmarking Data in JSon
 * file format. It will automatic save and append the data on daily basis, with
 * the machine serial number and current date as filename, and save it under the
 * directory ..\log\confirm\Benchmark
 *
 * @author Anthony Fong
 */
public class BenchmarkJsonFileTimingLogger
{

  private static BenchmarkJsonFileTimingLogger _instance = null;

  /**
   * @author Anthony Fong
   */
  public static synchronized BenchmarkJsonFileTimingLogger getInstance()
  {
    if (_instance == null)
    {
      _instance = new BenchmarkJsonFileTimingLogger();
    }
    return _instance;
  }

  /**
   * @author Anthony Fong
   */
  public static String ReadJSonFile(String fileName)
  {

    // This will reference one line at a time
    String line = null;
    String fileText = "";
    try
    {
      // FileReader reads text files in the default encoding.
      FileReader fileReader =
        new FileReader(fileName);

      // Always wrap FileReader in BufferedReader.
      BufferedReader bufferedReader =
        new BufferedReader(fileReader);

      while ((line = bufferedReader.readLine()) != null)
      {
        System.out.println(line);
      }

      // Always close files.
      bufferedReader.close();


    }
    catch (FileNotFoundException ex)
    {
      System.out.println(
        "Unable to open file '"
        + fileName + "'");
    }
    catch (IOException ex)
    {
      System.out.println(
        "Error reading file '"
        + fileName + "'");
      // Or we could just do this: 
      // ex.printStackTrace();
    }
    return fileText;
  }

  /**
   * @author Anthony Fong
   */
  public static void WriteJSonFile(String fileName, String json)
  {

    try
    {
      // Assume default encoding.
      FileWriter fileWriter =
        new FileWriter(fileName);

      // Always wrap FileWriter in BufferedWriter.
      BufferedWriter bufferedWriter =
        new BufferedWriter(fileWriter);

      // Note that write() does not automatically
      // append a newline character.
      bufferedWriter.write(json);

      // Always close files.
      bufferedWriter.close();
    }
    catch (IOException ex)
    {
      System.out.println(
        "Error writing to file '"
        + fileName + "'");
      // Or we could just do this:
      // ex.printStackTrace();
    }
  }

  /**
   * @author Anthony Fong
   */
  public static String LoadJSon(String fileName)
  {
    String content = "";
    try
    {
      FileReader reader = new FileReader(fileName);
      BufferedReader bufferedReader = new BufferedReader(reader);

      String line;

      while ((line = bufferedReader.readLine()) != null)
      {
        content = content + line;
      }
      reader.close();

    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    return content;
  }

  /**
   * @author Anthony Fong
   */
  public static void SaveJSon(String fileName, String content)
  {
    try
    {
      FileWriter writer = new FileWriter(fileName, false);
      writer.write(content);
      writer.close();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  /**
   * Load BenchmarkJsonFolder Object from Jason file specified by filename
   *
   * @author Anthony Fong
   */
  public static BenchmarkJsonFolder LoadBenchmarkJsonFolder(String fileName)
  {
    String json = BenchmarkJsonFileTimingLogger.LoadJSon(fileName);
    return new Gson().fromJson(json, BenchmarkJsonFolder.class);
  }

  /**
   * Automatic save and append the Performance Timing and Benchmarking Data in
   * JSon file with the machine serial number and current date as filename
   *
   * @author Anthony Fong
   */
  public void updateBenchmarkTiming(BenchmarkJsonFileEnum task, Long timing)
  {
    if (task == null)
    {
      return;
    }

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.MILLISECOND, 0);
    Date time = calendar.getTime();

    String stringDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " - " + XrayTester.getInstance().getMachineDescription();
    BenchmarkJsonFolder root = null;
    Gson gson = new Gson();
    String json = null;

    String filename = Directory.getBenchmarkConfirmationLogDir() + File.separator + stringDate + FileName.getJsonFileExtension();
    java.io.File f = new java.io.File(filename);

    if (f.exists() == false)
    {
      List<Long> datas = new ArrayList<Long>();
      root = new BenchmarkJsonFolder("Benchmarking",
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.System_Communication_Network_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.XRay_Source_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Area_Mode_Image_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Camera_Grayscale_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Camera_Adjustment_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Camera_Dark_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Camera_Light_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.XRay_Spot_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.XRay_Source_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Stage_Hysteresis_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Magnification_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Base_Image_Quality_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.System_Image_Quality_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_Camera_Grayscale_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_Camera_Adjustment_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_Camera_Dark_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_Camera_Light_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_XRay_Spot_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_Stage_Hysteresis_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_System_Offsets_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.High_Magnification_Adjustment.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_Base_Image_Quality_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.HighMag_System_Image_Quality_Confirmation.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Left_Outer_Barrier_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Right_Outer_Barrier_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Inner_Barrier_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Left_PIP_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Right_PIP_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Panel_Clamps_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Panel_Handler_AWA_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Panel_Positioner_Motion_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.XRay_Tube_Actuator_Benchmark.toString(), time, true, datas),
        new BenchmarkJsonFile(BenchmarkJsonFileEnum.Long_Path_Scanning_Benchmark.toString(), time, true, datas));

      json = gson.toJson(root);
      SaveJSon(filename, json);
    }

    json = LoadJSon(filename);

    root = gson.fromJson(json, BenchmarkJsonFolder.class);
    root.getFiles()[task.ordinal()].getTimingDatas().add(timing);
    json = gson.toJson(root);
    SaveJSon(filename, json);
  }
}