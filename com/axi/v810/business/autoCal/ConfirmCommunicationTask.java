package com.axi.v810.business.autoCal;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Base class for all Communication Confirmations. Provides basic functionality
 * to ping network devices. It also provides logging and IP address functionality
 * for other Communication Confirmations.
 *
 * @author Reid Hayhow
 */
public abstract class ConfirmCommunicationTask extends EventDrivenHardwareTask
{
  protected final int _logTimeoutSeconds = 1;
  protected final int _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000;
  protected final int _maxLogFilesInDirectory = 3;

  protected static Config _config = Config.getInstance();

  protected static ExecutorService _executor = new ScheduledThreadPoolExecutor(23);

  protected FileLoggerAxi _logger = null;

  protected StringBuilder _errorBuffer;

  protected int _PING_TIMEOUT_IN_MILLISECONDS = 2000;
  private ICMPPacketUtil _packetUtil;

  private String _nameForResults;
  private Set<Pair<Integer, String>>  _failureSet;

  protected ConfirmCommunicationTask(String name,
                                     int frequency,
                                     long timeoutInSeconds,
                                     ProgressReporterEnum progressReporter)
  {
    super(name, frequency, timeoutInSeconds, progressReporter);
    _logger = new FileLoggerAxi(FileName.getConfirmSystemCommunicationNetworkFullPath() +
                                FileName.getLogFileExtension(),
                                5000);
    _errorBuffer = new StringBuilder();
    _nameForResults = name;
    _packetUtil = ICMPPacketUtil.getInstance();

    //This confirmation and all sub-classed confirmations are automated
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;
  }

  /**
   * Create limits for this task. Tasks with Boolean results don't need
   * limits.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject() throws DatastoreException
  {
    _limits = new Limit(null, null, null, null);

    //Get the timeout from the config file
    _PING_TIMEOUT_IN_MILLISECONDS = _parsedLimits.getHighLimitInteger("");
  }

  /**
   * createResultsObject Create the results object for this test result.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    //Assume things are OK unless I can prove otherwise
    _result = new Result(new Boolean(true), _nameForResults);

     _failureSet = new HashSet<Pair<Integer,String>>();
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * set-up code that may be reused
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    //None needd
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    //None needed
    _automatedTask =  false;
  }

  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }


  /**
   * Method to log failure data. It will not log if communications are OK according
   * to the boolean input.
   *
   * @author Reid Hayhow
   */
  protected void logFailureData(boolean communicationsOK, String deviceType, int deviceIndex, String deviceIPAddress)
  {
    if (communicationsOK == false)
    {
      _result.setResults(new Boolean(false));
      Integer deviceIndexInt = new Integer(deviceIndex);
      _failureSet.add(new Pair<Integer, String>(deviceIndexInt, deviceIPAddress));
      Object[] parameters = new Object[]{deviceType, deviceIndexInt ,deviceIPAddress};
      LocalizedString failureInfo = new LocalizedString("CD_COMMUNICATION_FAILED_AT_IP_KEY", parameters);
      _errorBuffer.append(StringLocalizer.keyToString(failureInfo) + System.getProperty("line.separator"));
    }
  }

  /**
   * Access to the Set of failures as a matched pair of device index and IP
   * address. The actual type of device that has failed must be determined from
   * the specific Confirmation class that was executed.
   *
   * @return Set
   * @author Reid Hayhow
   */
  public Set<Pair<Integer, String>> getFailureIndexIPAddressPair()
  {
    return _failureSet;
  }


  /**
   * Method to actually 'ping' a remote system. It will wait for a set period of
   * time for the remote system to reply, and will return false if there is no
   * reply. It will return true of the remote system is available.
   *
   * @author Reid Hayhow
   */
  protected boolean addressIsOnNetwork(String addressToPing)
  {
    //Assume the connection is bad
    boolean isGoodConnection = false;

    //see if the system is reachable
    isGoodConnection = _packetUtil.canHostTransmitPacketSizeWithFragment(addressToPing, 500);
    //Return the state of the connection
    return isGoodConnection;
  }

  /**
   * Method to build and return a list of Callables that will 'ping' each IP
   * address in the List that is input to this method.
   *
   * @author Reid Hayhow
   */
  private List<Callable<Boolean>> buildListOfCallables(List<String> listOfIPAddressesToPing)
  {
    //Allocate our list of Callables
    List<Callable<Boolean>> listOfCallables = new ArrayList<Callable<Boolean>>();

    //Iterate over the list of IP addresses
    for (final String ipAddress : listOfIPAddressesToPing)
    {
      //create a callable to 'ping' this IP address
      Callable<Boolean> _callable = new Callable<Boolean>()
      {
        public Boolean call() throws XrayTesterException
        {
          return addressIsOnNetwork(ipAddress);
        }
      };

      //Add the callable to the list of callables
      listOfCallables.add(_callable);
    }

    //Return the list of callables
    return listOfCallables;
  }

  /**
   * Method to run the ping tasks, wait for the results and log the results.
   *
   * @author Reid Hayhow
   */
  protected void pingAddressesAndLogResults(List<String> listOfIPAddresses,
      String typeOfDeviceAtAddress, int deviceNumberingOffset) throws XrayTesterException
  {
    //Build up the list of callables based on the list of IP addresses
    List<Callable<Boolean>> futureTaskList = buildListOfCallables(listOfIPAddresses);

    try
    {
      //Start the callables and get the list of Futures
      List<Future<Boolean>> results = _executor.invokeAll(futureTaskList);

      //Get the iterator for the list of Futures
      ListIterator<Future<Boolean>> resultsList = results.listIterator();

      //Loop over the list of Future's processing the results
      while (resultsList.hasNext())
      {
        int indexOfResult = resultsList.nextIndex();
        //Get this specific result
        Future<Boolean> result = resultsList.next();

        //Log data if the ping failed as determined by the result.get() return
        //value
        logFailureData(result.get(),
                       typeOfDeviceAtAddress,
                       //IRP's are one based numbering while cameras are
                       //zero based, so use a deviceNumberingOffset to compensate
                       indexOfResult + deviceNumberingOffset,
                       listOfIPAddresses.get(indexOfResult));
      }
    }
    //Catch known exceptions thrown from the Future execution and package them
    //as ConfirmationExceptions. The bottom line is that either exception indicates
    //a failure of the task
    catch (InterruptedException ex)
    {
      HardwareException hardwareExFromInterrupt =
          new HardwareTaskExecutionException(ConfirmationExceptionEnum.PROBLEM_COMMUNICATING_WITH_REMOTE_SYSTEM, ex);
      throw hardwareExFromInterrupt;
    }
    catch (ExecutionException ex)
    {
      HardwareException hardwareExFromExecutionEx =
          new HardwareTaskExecutionException(ConfirmationExceptionEnum.PROBLEM_COMMUNICATING_WITH_REMOTE_SYSTEM, ex);
      throw hardwareExFromExecutionEx;
    }
  }

  /**
   * Common method for logging the results to a file
   *
   * @author Reid Hayhow
   */
  protected void logResults() throws DatastoreException
  {
    //If the task failed, add FAILED to the log for this result, log the error
    //to file and set the result status message to display the error
    if(_limits.didTestPass(_result) == false)
    {
      _logger.append(_nameForResults + " - " + StringLocalizer.keyToString("CD_FAILED_KEY"));
      _logger.append(_errorBuffer.toString());
      _result.setTaskStatusMessage(_errorBuffer.toString());
      _result.setSuggestedUserActionMessage(
          StringLocalizer.keyToString("CD_COMMUNICATION_FAILED_SUGGESTED_ACTION_KEY"));
    }
    //If the task PASSED, just log that it passed to file
    else
    {
      _logger.append(_nameForResults + " - " + StringLocalizer.keyToString("CD_PASSED_KEY"));
    }
  }

}
