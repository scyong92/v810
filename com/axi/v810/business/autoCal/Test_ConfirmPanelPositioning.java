package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;


public class Test_ConfirmPanelPositioning extends UnitTest
{
  public Test_ConfirmPanelPositioning()
  {
    // do nothing
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XrayCameraArray xRayCameraArray = null;

    try
    {
      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getPanelPositioningDir();
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);
      CalGrayscale.getInstance().executeUnconditionally();

      //NOTE:I am using a sub class to test here. It will run fewer motion profiles since it is
      //only used in unitTest mode
      TestConfirmPanelPositioningTask confirmPanelPositioning = TestConfirmPanelPositioningTask.getInstance();

      boolean taskPassed = confirmPanelPositioning.executeUnconditionally();

      Expect.expect(taskPassed);
    }
    catch (Exception ex)
    {
      ex.printStackTrace(os);
    }
    finally
    {
      xRayCameraArray.disableUnitTestMode();
      TestUtils.restoreConfigAfterRegressionTest();
    }
  }

  public static void main(String[] args)
  {
    try
    {
      Assert.setLogFile("tempLog", "tempVersion");
      TestUtils.initConfigForRegressionTest();
      UnitTest.execute(new Test_ConfirmPanelPositioning());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
