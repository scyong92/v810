package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * The Camera Image Retreiver class is designed to encapsulate the process of
 * getting the images needed for the Camera Light and Dark confirmation tasks.
 *
 * It uses the IAE to handle the image requests and then consolidates the images
 * it gets back from the IAE into a single map that can be used by calling
 * classes.
 *
 * @author Reid Hayhow
 */
public class CameraImageRetreiver
{
  //Members needed for the Image Rectangle, pixel to nanometer conversion
  private static final int _REGION_TO_IMAGE_X_NANOMETERS = -7200000;
  private static final int _REGION_TO_IMAGE_Y_NANOMETERS = -8480000;
  private static final int _IMAGE_HEIGHT_IN_PIXELS = ConfirmCameraImageTask.IMAGE_PIXEL_HEIGHT;

  private Map<AbstractXrayCamera, Image> _cameraToImageMap = new HashMap<AbstractXrayCamera, Image>();
  private Set<AbstractXrayCamera> _cameraSet;
  private List<ProjectionRequestThreadTask> _listOfProjectionRequests = new ArrayList<ProjectionRequestThreadTask>();
  private ScanPassDirectionEnum _scanPassDirectionEnum;

  /**
   * Constructor for this class, takes as input the enumerated value of the cameras
   * to get images from.
   *
   * @author Reid Hayhow
   */
  public CameraImageRetreiver(Set<AbstractXrayCamera> cameraSet, ScanPassDirectionEnum scanPassDirectionEnum)
  {
    Assert.expect(cameraSet != null);
    Assert.expect(scanPassDirectionEnum != null);

    //Save the direction for this set of images
    _scanPassDirectionEnum = scanPassDirectionEnum;
    _cameraSet = cameraSet;
  }

  /**
   * This function handles the global task of getting the image array from the
   * cameras specified in the constructor. It now uses the IAE to do the actual
   * image retreival.
   *
   * @author Reid Hayhow
   */
  public Map<AbstractXrayCamera, Image> getImageMap(boolean isLowMag, boolean turnOnXray) throws XrayTesterException
  {
    Assert.expect(_cameraSet != null);
    Assert.expect(_listOfProjectionRequests != null);
    Assert.expect(_cameraToImageMap.isEmpty());

    XrayTesterException exception = null;
    SystemFiducialRectangle systemFiducialRectangle = null;
    ImageAcquisitionEngine imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();

    XrayCameraArray xRayCameraArray = XrayCameraArray.getInstance();
    int imageHeightInNanometers = MagnificationEnum.NOMINAL.getNanoMetersPerPixel() *
                                  _IMAGE_HEIGHT_IN_PIXELS;
    if(isLowMag == false)
    {
      imageHeightInNanometers = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel() *
                                  _IMAGE_HEIGHT_IN_PIXELS;
    }
    
    int imageWidthInNanometers = 0;
    int imageWidthInPixels = 0;

    try
    {
      //For each camera in the list of cameras we are getting an image from,
      //create a projection request for that camera based on the direction we are
      //interested in and the fiducial rectangle that specifies the image size
      //we want for this confirmation task
      for (AbstractXrayCamera camera : _cameraSet)
      {
        // Turn off the median filter to excentuate failures
        camera.enableImageCorrection();

        imageWidthInPixels = camera.getNumberOfSensorPixelsUsed();
        imageWidthInNanometers = MagnificationEnum.NOMINAL.getNanoMetersPerPixel() * imageWidthInPixels;
        if(isLowMag == false)
        {
          imageWidthInNanometers = MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel() * imageWidthInPixels;
        }
        systemFiducialRectangle = new SystemFiducialRectangle(_REGION_TO_IMAGE_X_NANOMETERS,
                                                              _REGION_TO_IMAGE_Y_NANOMETERS,
                                                              imageWidthInNanometers,
                                                              imageHeightInNanometers);

        ProjectionRequestThreadTask requestThreadTask = new ProjectionRequestThreadTask(camera, _scanPassDirectionEnum, systemFiducialRectangle);

        //Need to set the number of lines to capture (Not sure why this is not calculated for me)
        requestThreadTask.setNumberOfLinesToCapture(imageHeightInNanometers);

        //Add the request to the list of requests we will submit to the IAE
        _listOfProjectionRequests.add(requestThreadTask);
      }

      //Submit this request to IAE using synthetic triggers
      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();

      // Initialize the IAE before requesting projections
      imageAcquisitionEngine.initialize();
      imageAcquisitionEngine.acquireProjectionsWithSyntheticTriggers(_listOfProjectionRequests, testExecutionTimer, true, turnOnXray);

      //The list I get back should have an image for each camera I am interested in
      //This could allow duplicates for some cameras, so I also will check the map
      //below, since it is indexed on cameras
      Assert.expect(_cameraSet.size() == _listOfProjectionRequests.size());

      //Allocate the container to return
      _cameraToImageMap.clear();

      //For each request we get back, add the image to the map of images to return
      for (ProjectionRequestThreadTask request : _listOfProjectionRequests)
      {
        Image image = request.getProjectionInfo().getProjectionImage();

        //a null image indicates a serious problem, should never happen
        Assert.expect(image != null);

        //Add the image to the map of images we will return at the index for
        //the camera it came from

        Image projectionImage = request.getProjectionInfo().getProjectionImage();
        projectionImage.incrementReferenceCount(); // putting into a map.
        _cameraToImageMap.put(request.getCamera(), projectionImage);
      }

      //I should now have a map with the right number of images, make sure
      //This guarantees that we got an image for each camera
      Assert.expect(_cameraToImageMap.size() == _cameraSet.size());

      for (ProjectionRequestThreadTask projectionRequest : _listOfProjectionRequests)
        projectionRequest.clear();

      //Clean up the list of requests before leaving
      _listOfProjectionRequests.clear();
    }
    catch (XrayTesterException xte)
    {
      exception = xte;
    }
    // Do our best to guarantee that the median filter is enabled on exit!
    finally
    {
      for(AbstractXrayCamera camera : _cameraSet)
      {
        // If this fails on one camera, don't stop
        try
        {
          camera.enableImageCorrection();
        }
        // Catch any exception and ensure it is not lost
        catch (XrayTesterException xte)
        {
          if(exception == null)
          {
            exception = xte;
          }
        }
      }

      // Ensure that the exception (if any) is thrown
      if(exception != null)
      {
        throw exception;
      }
    }

    return _cameraToImageMap;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public Map<AbstractXrayCamera, Image> getImageMap(boolean isLowMag) throws XrayTesterException
  {
    return getImageMap(isLowMag, true);
  }

  /**
   * @author Patrick Lacz
   */
  public void clearResultImageMap()
  {
    Assert.expect(_cameraToImageMap != null);
    for (Image image : _cameraToImageMap.values())
    {
      image.decrementReferenceCount();
    }
    _cameraToImageMap = null;
  }


}
