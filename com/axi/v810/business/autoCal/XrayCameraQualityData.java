package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.StringLocalizer;
import java.io.*;
import java.util.*;
import net.spc.common.*;
import net.spc.math.*;

/**
* XrayCameraQualityData
* Class to extract, convert and encapsulate the data collected from class ConfirmAreaModeImages
* for XrayCameraQualityMonitoring and XrayCameraSPCPanel
* XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
* @author Anthony Fong
*/
public class XrayCameraQualityData
{  
    //Instance member
    private static XrayCameraQualityData _instance;
    class Data
    {
      private String id="";
      private String value="";

      public Data(String id,String value)
      {
        this.id=id;
        this.value=value;
      }

      public String getId()
      {
        return id;
      }

      public String getValue()
      {
        return value;
      }
    }

    private String _qualityDataFolder="";
    private String _monitoringVariableName ="";
    private int _xrayCameraQuality_ChartWidth=1600;
    private int _xrayCameraQuality_ChartHeight=1200;
    private int _xrayCameraQuality_DataSize=100;
    private double _xrayCameraQuality_GradientMean =0.22;
    private double _xrayCameraQuality_GradientStdDev=0.23;
    private double _xrayCameraQuality_GradientAboveMean = 0.34;
    private double _xrayCameraQuality_GradientMax=7.16;
    private double _xrayCameraQuality_GradientBeta=0.07;
    private double _xrayCameraQuality_SigmaValue=1.96;
    private Hashtable _details;
    private Hashtable _drawDetails;

    public String getQualityDataFolder()
    {   
        return _qualityDataFolder;
    }

    public String getMonitoringVariableName()
    {
        return _monitoringVariableName;
    }

    public int getChartWidth()
    {
        return _xrayCameraQuality_ChartWidth;
    }

    public int getDataSize()
    {
        return _xrayCameraQuality_DataSize;
    }

    public int getChartHeight()
    {
        return _xrayCameraQuality_ChartHeight;
    }

    public Hashtable getDetails()
    {
        return _details;
    }

    public Hashtable getDrawDetails()
    {
        return _drawDetails;
    }

    /**
    * Instance access to this XrayCameraQualityData
    *
    * @author Anthony Fong
    */
    public static synchronized XrayCameraQualityData getInstance()
    {
        if (_instance == null)
        {
          _instance = new XrayCameraQualityData();
        }
        return _instance;
    }

    public XrayCameraQualityData()
    {
        Config _config = Config.getInstance();
        _xrayCameraQuality_ChartWidth=_config.getIntValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_CHART_WIDTH);
        _xrayCameraQuality_ChartHeight=_config.getIntValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_CHART_HEIGHT);
        _xrayCameraQuality_DataSize=_config.getIntValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_DATA_SIZE);
        _xrayCameraQuality_GradientMean =_config.getDoubleValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_GRADIENT_MEAN);
        _xrayCameraQuality_GradientStdDev=_config.getDoubleValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_GRADIENT_STDDEV);
        _xrayCameraQuality_GradientAboveMean = _config.getDoubleValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_GRADIENT_ABOVE_MEAN);
        _xrayCameraQuality_GradientMax=_config.getDoubleValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_GRADIENT_MAX);
        _xrayCameraQuality_GradientBeta=_config.getDoubleValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_GRADIENT_BETA);
        _xrayCameraQuality_SigmaValue=_config.getDoubleValue(SoftwareConfigEnum.XRAY_CAMERA_QUALITY_SIGMA_VALUE);
    }

    public void doSPC(String colSelection, boolean showCP)
    {
        try
        {
            if(colSelection.equalsIgnoreCase(""))
            {
               System.out.println ("colSelection = empty");
               colSelection ="5";
            }
            else
            {
            }

            _qualityDataFolder = Directory.getConfirmAreaModeImageLogDir();
            String rawDataFilename = _qualityDataFolder + File.separator+
                                     FileName.getConfirmCameraQualityLogFileName()+
                                     FileName.getCommaSeparatedValueFileExtension();

            if(!new File(rawDataFilename).exists())
            {
                System.out.println("Raw Data file "+rawDataFilename+" not found");
                return;
            }

            int selection =Integer.parseInt(colSelection);
            System.out.println("selection = " + selection);

            java.util.Collection<Data> convertedData = convertFile(rawDataFilename,(int)_xrayCameraQuality_DataSize, selection, showCP);
            _details =  extractData(convertedData);
            _drawDetails = doCalculation(_details,_xrayCameraQuality_SigmaValue,selection%10);

        }
        catch(Exception e)
        {
            System.out.println("Exception occured.."+ e);
        }
    }

    private Hashtable extractData(java.util.Collection<Data> elements)
    {
        Hashtable details = new  Hashtable();
        try
        {
              Vector dataCollection = new Vector();
              Vector datas = new Vector();
              Vector x_lables = new Vector();
              for(Data d : elements)
              {
                  x_lables.add(d.getId());
                  datas.add(new Double(d.getValue()));
                  dataCollection.add(datas);
                  datas = new Vector();
              }
              details.put("desc", _monitoringVariableName);
              details.put("machineID", "machineID");
              details.put("xAxisName", "Time");
              details.put("yAxisName", "AvgGradient");
              details.put("x_lables", x_lables);
              details.put("dataCollection", dataCollection);
              details.put("lotSize", new Integer(1));
         }
         catch (Exception e)//Catch exception if any
         {
              System.err.println("Error: " + e.getMessage());
         }
        return details;
   }

    private java.util.Collection<Data> convertFile(String rawDataFilename,int latestDataSize, int col, boolean showCP)
    {
        try
        {
          // Open the file that is the first command line parameter
          FileInputStream fstream = new FileInputStream(rawDataFilename);
          // Get the object of DataInputStream
          DataInputStream in = new DataInputStream(fstream);
          BufferedReader br = new BufferedReader(new InputStreamReader(in));
          String strLine;
          int lineCount =0;
          String[] headers;
          java.util.Collection<Data> elements = new java.util.ArrayList<Data>();
          double Spec=0;
          //Read File Line By Line
          while ((strLine = br.readLine()) != null)
          {
              //System.out.println (strLine);
              if(lineCount>0)
              {
                  // String to split by the argument delimiter provided.
                  String str = strLine;
                  String[] temp;
                  String delimiter = ",";
                  temp = str.split(delimiter);
                  if(!temp[col].contains("C"))
                  {
                      Float fValue = new Float( temp[col]);
                      String strValue = String.format("%.2f", fValue);
                      String timeStamp = temp[0];
                      elements.add(new Data(timeStamp,strValue));
                  }
              }
              else
              {
                  // String to split by the argument delimiter provided.
                  String str = strLine;
                  String delimiter = ",";
                  headers = str.split(delimiter);
                  if(headers[col].contains("C"))
                  {
                    _monitoringVariableName = headers[col];
                  }
                  else
                  {
                    int i = (int)(col%10);
                    int camera = (int)(col/10);
                    if(i == ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MEAN_INDEX)
                    {
                         //_monitoringVariableName = "C" + String.valueOf(camera) + "-Gradient Mean";
                         _monitoringVariableName = "C" + String.valueOf(camera) + "-" + StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_MEAN_KEY");
                         Spec=_xrayCameraQuality_GradientMean;
                    }
                    else if (i == ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_STD_DEV_INDEX)
                    {
                        _monitoringVariableName = "C" + String.valueOf(camera) + "-" + StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_STD_DEV_KEY");
                        Spec=_xrayCameraQuality_GradientStdDev;
                    }
                    else if (i == ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN_INDEX)
                    {
                        _monitoringVariableName = "C" + String.valueOf(camera) + "-" + StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_ABOVE_MEAN_KEY");
                        Spec=_xrayCameraQuality_GradientAboveMean;
                    }
                    else if (i == ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MAX_INDEX)
                    {
                        _monitoringVariableName = "C" + String.valueOf(camera) + "-" + StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_MAX_KEY");
                        Spec=_xrayCameraQuality_GradientMax;
                    }
                    else if (i == ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_BETA_INDEX)
                    {
                        _monitoringVariableName = "C" + String.valueOf(camera) + "-" + StringLocalizer.keyToString("SERVICE_UI_XRAY_CAMERA_QUALITY_GRADIENT_BETA_KEY");
                        Spec=_xrayCameraQuality_GradientBeta;
                    }
                  }
                  System.out.println("headers = " +  headers[col]);
               }
              lineCount++;
          }
          //Close the input stream
          in.close();
          
          if(showCP == true)
          {
              if(elements.size()>latestDataSize)
              {
                 java.util.Collection<Data> resultElements = new java.util.ArrayList<Data>();
                 Object[] rawData = elements.toArray();
                 int dataLenght = elements.size();
                 int newDataLenght = dataLenght-latestDataSize;

                 for(int i=0; i<newDataLenght; i++)
                 {
                    double cpData[] = new double[latestDataSize];
                    for(int j=0; j<latestDataSize; j++)
                    {
                       String strValue = ((Data)rawData[i+j+1]).getValue();
                       cpData[j] = Double.parseDouble(strValue);
                    }

                    //double mean = Statistics.mean(cpData);
                    double stdDev = Statistics.stdDev(cpData);
                    double PC =(double)(Spec) /(double) (stdDev);

                    String timeStamp = ((Data)rawData[i+latestDataSize]).getId();
                    String strValue = String.valueOf(PC);
                    resultElements.add(new Data(timeStamp,strValue));
                 }

                 elements = resultElements;
                 return elements;
              }
              else
              {
                  return elements;
              }
          }
          else
    
          {
              if(elements.size()>latestDataSize)
              {
                 java.util.Collection<Data> resultElements = new java.util.ArrayList<Data>();
                 Object[] latestData = elements.toArray();

                 for(int i=latestData.length-latestDataSize ;i<latestData.length;i++)
                 {
                     String timeStamp = ((Data)latestData[i]).getId();
                     String strValue = ((Data)latestData[i]).getValue();
                     resultElements.add(new Data(timeStamp,strValue));
                 }
                elements = resultElements;
                return elements;
              }
              else
              {
                  return elements;
              }
           }
        }
        catch (Exception e) //Catch exception if any
        {
          System.err.println("Error: " + e.getMessage());
        }

        return null;
    }

    private Hashtable doCalculation(Hashtable details,double sigmaValue,int selection)
    {

                    Hashtable drawDetails = new Hashtable();
                    int lotSize = ((Integer)details.get("lotSize")).intValue();
                    try {

                            Vector y_axis_datas = new Vector();

                            Vector dataCollection = (Vector)details.get("dataCollection");

                            for(int i=0;i<dataCollection.size();i++)
                            {
                                    Vector dataSet = (Vector)dataCollection.get(i);
                                    y_axis_datas.add(new Double(Statistics.mean(VectorUtil.dArray(dataSet))));
                            }

                            // sigmaValue = 3;
                            // sigmaValue = 1.96;
                            double y_axis_datas_array[] = VectorUtil.dArray(y_axis_datas);
                            double mean = Statistics.mean(y_axis_datas_array);
                            double stdDev = Statistics.stdDev(y_axis_datas_array);
                            double UCL = mean + sigmaValue * (stdDev/Math.sqrt(lotSize));
                            double LCL = mean - sigmaValue * (stdDev/Math.sqrt(lotSize));
                            //double PC = 6 * (_sigmaValue * (stdDev/Math.sqrt(lotSize)));

                            double Spec=0;
                            if(selection ==ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MEAN_INDEX) //Mean =0.22
                            {
                                Spec=_xrayCameraQuality_GradientMean;
                            }
                            else if(selection ==ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_STD_DEV_INDEX)  //StdDev=0.23
                            {
                                Spec=_xrayCameraQuality_GradientStdDev;
                            }
                            else if(selection ==ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_ABOVE_MEAN_INDEX) //AvgGradient = 0.34
                            {
                                Spec=_xrayCameraQuality_GradientAboveMean;
                            }
                            else if(selection ==ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_MAX_INDEX) //MaxGradient = 7.16
                            {
                                Spec=_xrayCameraQuality_GradientMax;
                            }
                            else if(selection ==ConfirmAreaModeImages.XRAY_CAMERA_QUALITY_MONITORING_GRADIENT_BETA_INDEX) //Beta=0.07
                            {
                                Spec=_xrayCameraQuality_GradientBeta;
                            }
                            double PC =(double)(Spec) /(double) (stdDev);
                            //double PC =  (stdDev);
                            //double lProcessCapability = Math.abs(60.0 - mean)/(3.0*stdDev);
                            //double uProcessCapability = Math.abs(0.0 - mean)/(3.0*stdDev);

                            //PC = Math.min(lProcessCapability, uProcessCapability);
                            //PC = 100*(6*stdDev)/(0.1*mean);

                            System.out.println(UCL +"-----------"+LCL);

                            Hashtable controlLimitObjects = new Hashtable();

                            controlLimitObjects.put("UCL", new Double(UCL));
                            controlLimitObjects.put("LCL", new Double(LCL));

                            //double y_axis_datas_sorted_array[] = SortUtil.sort(VectorUtil.dArray(y_axis_datas));
                            double y_axis_datas_sorted_array[] =(VectorUtil.dArray(y_axis_datas));
                            drawDetails.put("Y_aixs_datas", y_axis_datas_sorted_array);
                            drawDetails.put("controlLimitObjects", controlLimitObjects);
                            drawDetails.put("processCapability", new Double(PC));
                    }
                    catch(Exception e)
                    {
                            System.out.println("Exception occured .. "+e);
                    }

                    return drawDetails;
            }
}
