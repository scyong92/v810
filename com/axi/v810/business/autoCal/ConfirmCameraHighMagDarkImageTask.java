package com.axi.v810.business.autoCal;

import com.axi.v810.business.license.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.util.*;
import com.axi.util.image.*;


import java.util.*;


/**
 * Confirmation to test camera dark values. This confirmation gets a set of
 * images from the cameras and checks them for horizontal flatness, vertical
 * flatness, mean gray value and gray value standard deviation. These values
 * must fall within acceptable limits for the cameras to be functioning
 * correctly.
 *
 * The dark images can be retreived as a group as long as Xrays are off. There
 * may eventually be other constraints (like doors closed) but currently Xrays
 * off is the only one I am aware of. Beacuse all images can be gotten at the
 * same time and because that time can be 'hidden' in other activities, this task
 * is run more often.
 *
 * This class inherits from the ConfirmCameraImageTask
 * class which provides two key functions, analyzeImage() and createResults().
 * @author Anthony Fong
 */
public class ConfirmCameraHighMagDarkImageTask extends ConfirmCameraImageTask
{
  // Printable name for this procedure
  private static final String NAME =
      StringLocalizer.keyToString("CD_CAMERA_HIGH_MAG_DARK_CONFIRMATION_KEY");

  // Call execute() every time this procedure is triggered
  private static final int FREQUENCY = _config.getIntValue(HardwareCalibEnum.HIGH_MAG_CAMERA_DARK_IMAGE_CONFIRM_FREQUENCY);

  private static final long TIMEOUTINSECONDS = _config.getLongValue(SoftwareConfigEnum.CAMERA_DARK_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS);

  private static ConfirmCameraHighMagDarkImageTask _instance = null;

  /**
   * @author Reid Hayhow
   */
  private ConfirmCameraHighMagDarkImageTask()
  {
    super(NAME,
          FREQUENCY,
          TIMEOUTINSECONDS,
          Directory.getCameraHighMagDarkConfirmationLogDir(),
          ProgressReporterEnum.HIGH_MAG_DARK_CAMERA_IMAGE_QUALITY_TASK, true);

    //This confirm runs on all cameras
    _camerasToGetImagesFromSet = _cameras;

    //No stage movement is needed
    _xPosition = null;
    _yPosition = null;

    //This is a dark image task
    _localizedLightOrDarkString = StringLocalizer.keyToString("ACD_DARK_KEY");

    //Set the enum used to save the timestamp in the hardware.calib for this confirmation
    setTimestampEnum(HardwareCalibEnum.HIGH_MAG_DARK_IMAGE_GROUP_ONE_LAST_TIME_RUN);
    _isHighMagTask = true;
  }

  /**
   * @return Instance object of this singleton class
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCameraHighMagDarkImageTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCameraHighMagDarkImageTask();
    }
    return _instance;
  }

  /**
   * Method to handle xrays for Dark Images before image capture. Turn them off
   * before executing
   *
   * @author Reid Hayhow
   */
  protected void setXraySourceBeforeExecute() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays cylinder up for high mag.    
        XrayActuator.getInstance().up(true);
      }
      else
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(true,false);
      }
      // re-calculate magnification 
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
      // XCR-3037 Xray off during dark confirmation
      if (_wereXraysOn == true)
        _xraySource.off();
    }
    else if (_wereXraysOn == true)
    {
      _xraySource.off();
    }
  }

  /**
   * Method to handle xrays for Dark Images after image capture. Turn them back
   * on (if they were on) when done
   *
   * @author Reid Hayhow
   */
  protected void setXraySourceAfterExecute() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays cylinder up for low mag, no need to load high mag camera config
        XrayActuator.getInstance().up(false);

        // re-calculate magnification 
        ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
        
        // re-calculate magnification 
        ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
      }
    }  
    //If Xrays were on, turn them back on before leaving
    if (_wereXraysOn == true)
    {
      _xraySource.on();
    }
    else
    {
      _xraySource.off();
    }
  }

  /**
   * No stage motion is needed for dark image tasks
   *
   * @author Reid Hayhow
   */
  protected void moveStageBeforeExecute()
  {
    //Do nothing
  }

  /**
   * Method for knowing if the task can run with the current panel state. This method
   * always returns true for Dark tasks because a panel in the system doesn't obstruct
   * anything with the lights off.
   *
   * @author Reid Hayhow
   */
  protected boolean ableToRunWithCurrentPanelState()
  {
    return true;
  }
  
//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmCameraImageTask will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
  
  /**
   * @author Cheah Lee Herng
   */
  protected boolean isTurnOnXray()
  {
    return false;
  }
}
