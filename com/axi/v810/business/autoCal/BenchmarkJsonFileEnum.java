package com.axi.v810.business.autoCal;

/**
 * @author Anthony Fong
 */
public enum BenchmarkJsonFileEnum
{

  System_Communication_Network_Confirmation
  {
    public String toString()
    {
      return "System Communication Network Confirmation";
    }
  },
  XRay_Source_Confirmation
  {
    public String toString()
    {
      return "X-ray Source Confirmation";
    }
  },
  Area_Mode_Image_Confirmation
  {
    public String toString()
    {
      return "Area Mode Image Confirmation";
    }
  },
  Camera_Grayscale_Adjustment
  {
    public String toString()
    {
      return "Camera Grayscale Adjustment";
    }
  },
  Camera_Adjustment_Confirmation
  {
    public String toString()
    {
      return "Camera Adjustment Confirmation";
    }
  },
  Camera_Dark_Confirmation
  {
    public String toString()
    {
      return "Camera Dark Confirmation";
    }
  },
  Camera_Light_Confirmation
  {
    public String toString()
    {
      return "Camera Light Confirmation";
    }
  },
  XRay_Spot_Adjustment
  {
    public String toString()
    {
      return "X-ray Spot Adjustment";
    }
  },
  Stage_Hysteresis_Adjustment
  {
    public String toString()
    {
      return "Stage Hysteresis Adjustment";
    }
  },
  System_Offsets_Adjustment
  {
    public String toString()
    {
      return "System Offsets Adjustment";
    }
  },
  Magnification_Adjustment
  {
    public String toString()
    {
      return "Magnification Adjustment";
    }
  },
  Base_Image_Quality_Confirmation
  {
    public String toString()
    {
      return "Base Image Quality Confirmation";
    }
  },
  System_Image_Quality_Confirmation
  {
    public String toString()
    {
      return "System Image Quality Confirmation";
    }
  },
  HighMag_Camera_Grayscale_Adjustment
  {
    public String toString()
    {
      return "High Mag Camera Grayscale Adjustment";
    }
  },
  HighMag_Camera_Adjustment_Confirmation
  {
    public String toString()
    {
      return "High Mag Camera Adjustment Confirmation";
    }
  },
  HighMag_Camera_Dark_Confirmation
  {
    public String toString()
    {
      return "High Mag Camera Dark Confirmation";
    }
  },
  HighMag_Camera_Light_Confirmation
  {
    public String toString()
    {
      return "High Mag Camera Light Confirmation";
    }
  },
  HighMag_XRay_Spot_Adjustment
  {
    public String toString()
    {
      return "High Mag X-ray Spot Adjustment";
    }
  },
  HighMag_Stage_Hysteresis_Adjustment
  {
    public String toString()
    {
      return "High Mag Stage Hysteresis Adjustment";
    }
  },
  HighMag_System_Offsets_Adjustment
  {
    public String toString()
    {
      return "High Mag System Offsets Adjustment";
    }
  },
  High_Magnification_Adjustment
  {
    public String toString()
    {
      return "High Magnification Adjustmentt";
    }
  },
  HighMag_Base_Image_Quality_Confirmation
  {
    public String toString()
    {
      return "High Mag Base Image Quality Confirmation";
    }
  },
  HighMag_System_Image_Quality_Confirmation
  {
    public String toString()
    {
      return "High Mag System Image Quality Confirmation";
    }
  },
  Left_Outer_Barrier_Benchmark
  {
    public String toString()
    {
      return "Left Outer Barrier Benchmark";
    }
  },
  Right_Outer_Barrier_Benchmark
  {
    public String toString()
    {
      return "Right Outer Barrier Benchmark";
    }
  },
  Inner_Barrier_Benchmark
  {
    public String toString()
    {
      return "Inner Barrier Benchmark";
    }
  },
  Left_PIP_Benchmark
  {
    public String toString()
    {
      return "Left PIP Benchmark";
    }
  },
  Right_PIP_Benchmark
  {
    public String toString()
    {
      return "Right PIP Benchmark";
    }
  },
  Panel_Clamps_Benchmark
  {
    public String toString()
    {
      return "Panel Clamps Benchmark";
    }
  },
  Panel_Handler_AWA_Benchmark
  {
    public String toString()
    {
      return "Panel Handler AWA Benchmark";
    }
  },
  Panel_Positioner_Motion_Benchmark
  {
    public String toString()
    {
      return "Panel Positioner Motion Benchmark";
    }
  },
  XRay_Tube_Actuator_Benchmark
  {
    public String toString()
    {
      return "XRay Tube Actuator Benchmark";
    }
  },
  Long_Path_Scanning_Benchmark
  {
    public String toString()
    {
      return "Long Path Scanning Benchmark";
    }
  }
}