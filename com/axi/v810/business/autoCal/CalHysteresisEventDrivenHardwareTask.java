package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
public class CalHysteresisEventDrivenHardwareTask extends EventDrivenHardwareTask
{
  private static CalHysteresisEventDrivenHardwareTask _instance;
  private static Config _config = Config.getInstance();

  // Printable name for this procedure
  private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_HYSTERESIS_NAME_KEY");

  // This cal is called on the first event after the timeout occurs.
  private static final int _EVENT_FREQUENCY = 1;
  private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.HYSTERESIS_EXECUTE_EVERY_TIMEOUT_SECONDS);

  // Define the region to be imaged for this cal
  // X & Y are relative to the system fiducial
  private static final int _REGION_TO_IMAGE_X_NANOMETERS = -1120000;
  private static final int _REGION_TO_IMAGE_Y_NANOMETERS = -880000;
  private static final int _REGION_TO_IMAGE_HEIGHT_NANOMETERS = 2000000;
  private static final int _REGION_TO_IMAGE_WIDTH_NANOMETERS = 2000000;

  // Logging support
  private static final String _logDirectory = Directory.getStageHysteresisCalLogDir();
  private static final long _logTimeoutSeconds = _config.getIntValue(SoftwareConfigEnum.HYSTERESIS_LOG_RESULTS_EVERY_TIMEOUT_SECONDS);
  private static final long _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000L;
  private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.HYSTERESIS_MAX_LOG_FILES_IN_DIRECTORY);

  // Limits & Results support
  private static int _index = 0;
  private static final int _HYSTERESIS_IN_NANOMETERS_INDEX = _index++;
  private static final int _MAX_RESULT_INDEX = _index++; // Must be last, used to size arrays

  /**
   * @author Eddie Williamson
   */
  static
  {
    Assert.expect(_MAX_RESULT_INDEX > _HYSTERESIS_IN_NANOMETERS_INDEX);
  }

  private List<ProjectionRequestThreadTask> _projectionRequests;
  private Map<AbstractXrayCamera, VirtualProjection> _xRayCameraToUpVirtualProjectionMap;
  private Map<AbstractXrayCamera, VirtualProjection> _xRayCameraToDownVirtualProjectionMap;
  private List<AbstractXrayCamera> _xRayCameras;
  private Result[] _localResults;
  private Limit[] _localLimits;
  private DirectoryLoggerAxi _logger;
  private ImageAcquisitionEngine _imageAcquisitionEngine;
  private Map<AbstractXrayCamera, Float> _cameraToUpEdgeMap;
  private Map<AbstractXrayCamera, Float> _cameraToDownEdgeMap;
  private Map<AbstractXrayCamera, Float> _cameraToOffsetMap;
  private int _hysteresisNanometers;
  private float _hysteresisAveragePixels;
  private PanelPositioner _panelPositioner;


  /**
   * @author Eddie Williamson
   */
  public static synchronized CalHysteresisEventDrivenHardwareTask getInstance()
  {
    if (_instance == null)
      _instance = new CalHysteresisEventDrivenHardwareTask();

    return _instance;
  }

  /**
   * @author Eddie Williamson
   */
  private CalHysteresisEventDrivenHardwareTask()
  {
    super(_NAME_OF_CAL, _EVENT_FREQUENCY, _EXECUTE_EVERY_TIMEOUT_SECONDS, ProgressReporterEnum.Y_HYSTERESIS_ADJUSTMENT_TASK);
    _logger = new DirectoryLoggerAxi(_logDirectory,
                                     FileName.getStageHysteresisLogFileBasename(),
                                     FileName.getLogFileExtension(),
                                     _maxLogFilesInDirectory);
    _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

    setTimestampEnum(HardwareCalibEnum.HYSTERESIS_ADJUSTMENT_LAST_TIME_RUN);
    _xRayCameras = new ArrayList<AbstractXrayCamera>();
    _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
    _cameraToUpEdgeMap = new HashMap<AbstractXrayCamera, Float>();
    _cameraToDownEdgeMap = new HashMap<AbstractXrayCamera, Float>();
    _cameraToOffsetMap = new HashMap<AbstractXrayCamera, Float>();
    _panelPositioner = PanelPositioner.getInstance();
    _xRayCameraToUpVirtualProjectionMap = new HashMap<AbstractXrayCamera,VirtualProjection>();
    _xRayCameraToDownVirtualProjectionMap = new HashMap<AbstractXrayCamera,VirtualProjection>();
    _progressObservable = ProgressObservable.getInstance();
  }

  /**
   * By default all tests can run on any hardware configuration.
   * @return true if this test is capable of being run on the currently defined hardware.
   * @author Eddie Williamson
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * This method provides a single point of contact where tasks can put their set-up code that may be reused
   * @author Eddie Williamson
   */
  protected void setUp() throws XrayTesterException
  {  
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to up position for low mag task.
        XrayActuator.getInstance().up(true);
      }
    }
    // do nothing
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
    //Ngie Xing, add S2EX
    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      PanelClamps.getInstance().close();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void cleanUpVirtualProjectionUpData()
  {
    VirtualProjection virtualProjection = null;
    if (_xRayCameraToUpVirtualProjectionMap.isEmpty() == false)
    {
      for (AbstractXrayCamera xRayCamera : _xRayCameras)
      {
        virtualProjection = _xRayCameraToUpVirtualProjectionMap.get(xRayCamera);
        Assert.expect(virtualProjection != null);
        virtualProjection.clear();
      }
    }

    _xRayCameraToUpVirtualProjectionMap.clear();
  }

  /**
   * @author Greg Esparza
   */
  private void cleanUpVirtualProjectionDownData()
  {
    VirtualProjection virtualProjection = null;
    if (_xRayCameraToDownVirtualProjectionMap.isEmpty() == false)
    {
      for (AbstractXrayCamera xRayCamera : _xRayCameras)
      {
        virtualProjection = _xRayCameraToDownVirtualProjectionMap.get(xRayCamera);
        Assert.expect(virtualProjection != null);
        virtualProjection.clear();
      }
    }

    _xRayCameraToDownVirtualProjectionMap.clear();
  }

  /**
   * This method provides a single point of contact where tasks can put their clean-up code that may be reused (for
   * example executeTask and abortTask may both use the same cleanUp code)
   * @author Eddie Williamson
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
        
    // Free image memory, reset pointers.
    if (_projectionRequests != null)
      _projectionRequests.clear(); // Probably unnecessary but what the heck.
    _projectionRequests = null;

    cleanUpVirtualProjectionUpData();
    cleanUpVirtualProjectionDownData();
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }

  /**
   * Create a Limits object specific to the particular test being run.
   * @author Eddie Williamson
   */
  protected void createLimitsObject() throws DatastoreException
  {
    _localLimits = new Limit[_MAX_RESULT_INDEX];
    _limits = new Limit(null, null, null, _NAME_OF_CAL);
    Limit hysteresisLimit = new Limit(new Integer(_parsedLimits.getLowLimitInteger("nanometers")),
                                      new Integer(_parsedLimits.getHighLimitInteger("nanometers")),
                                      "NANOMETERS", "Scan direction stage hysteresis");
    _limits.addSubLimit(hysteresisLimit);
    _localLimits[_HYSTERESIS_IN_NANOMETERS_INDEX] = hysteresisLimit;
  }


  /**
   * createResultsObject Create the results object for this test result.
   * @author Eddie Williamson
   */
  protected void createResultsObject()
  {
    _localResults = new Result[_MAX_RESULT_INDEX];
    // Create container
    _result = new Result(_NAME_OF_CAL);
    Result hysteresisResult = new Result(new Integer( -1), getName());
    _result.addSubResult(hysteresisResult);
    _localResults[_HYSTERESIS_IN_NANOMETERS_INDEX] = hysteresisResult;
  }


  /**
   * Used by regression test to check pass/fail status
   * @return true if all results pass compared to their limits
   * @author Eddie Williamson
   */
  boolean didTestPass()
  {
    return _limits.didTestPass(_result);
  }

  /**
   * @author Greg Esparza
   */
  private void logUpData(StringBuilder logString)
  {
    Assert.expect(logString != null);

    VirtualProjection virtualProjection = null;
    for (AbstractXrayCamera xRayCamera : _xRayCameras)
    {
      virtualProjection = _xRayCameraToUpVirtualProjectionMap.get(xRayCamera);
      Assert.expect(virtualProjection != null);

      logString.append(",");
      logString.append(String.valueOf(xRayCamera.getId()) + ",");
      logString.append(CalibrationSupportUtil.ProjectionRequestToCSVString(virtualProjection.getProjectionRequestThreadTask()) +  ",");
      // Log filename for reference, we save the image if we log the data
      String filename = "cam_" + String.valueOf(xRayCamera.getId()) + "_up.jpg";
      logString.append(filename + ",");
      logString.append("\n");
    }
  }

  /**
   * @author Greg Esparza
   */
  private void logDownData(StringBuilder logString)
  {
    Assert.expect(logString != null);

    VirtualProjection virtualProjection = null;
    for (AbstractXrayCamera xRayCamera : _xRayCameras)
    {
      virtualProjection = _xRayCameraToDownVirtualProjectionMap.get(xRayCamera);
      Assert.expect(virtualProjection != null);

      logString.append(",");
      logString.append(String.valueOf(xRayCamera.getId()) + ",");
      logString.append(CalibrationSupportUtil.ProjectionRequestToCSVString(virtualProjection.getProjectionRequestThreadTask()) +  ",");
      // Log filename for reference, we save the image if we log the data
      String filename = "cam_" + String.valueOf(xRayCamera.getId()) + "_down.jpg";
      logString.append(filename + ",");
      logString.append("\n");
    }
  }

  /**
   * @author Greg Esparza
   */
  private void saveUpImages() throws DatastoreException
  {
    VirtualProjection virtualProjection = null;
    for (AbstractXrayCamera xRayCamera : _xRayCameras)
    {
      virtualProjection = _xRayCameraToUpVirtualProjectionMap.get(xRayCamera);
      Assert.expect(virtualProjection != null);

      Image image = virtualProjection.getImage();
      image.incrementReferenceCount();
      String filename = "cam_" + String.valueOf(xRayCamera.getId()) + "_up.jpg";
      XrayImageIoUtil.saveJpegImage(image.getBufferedImage(), _logDirectory + File.separator + filename);
      image.decrementReferenceCount();
    }
  }

  /**
   * @author Greg Esparza
   */
  private void saveDownImages() throws DatastoreException
  {
    VirtualProjection virtualProjection = null;
    for (AbstractXrayCamera xRayCamera : _xRayCameras)
    {
      virtualProjection = _xRayCameraToDownVirtualProjectionMap.get(xRayCamera);
      Assert.expect(virtualProjection != null);

      Image image = virtualProjection.getImage();
      image.incrementReferenceCount();
      String filename = "cam_" + String.valueOf(xRayCamera.getId()) + "_down.jpg";
      XrayImageIoUtil.saveJpegImage(image.getBufferedImage(), _logDirectory + File.separator + filename);
      image.decrementReferenceCount();
    }
  }

  /**
   * Logs detailed information about the cal (and saves images) when the timeout
   * is exceeded. The log information is formatted as comma separated values to
   * it can be imported into Excel for manual analysis.
   * @param forceLogging If true then ignore the timeout and log anyway.
   * @author Eddie Williamson
   */
  private void logInformation(Boolean forceLogging) throws DatastoreException
  {
    StringBuilder logString = new StringBuilder();

    // Create header lines
    logString.append(_NAME_OF_CAL + "\n");
    logString.append(","); // offset first column
    logString.append("Hysteresis," + "\n");
    logString.append(","); // offset first column
    logString.append(",pixels," + _hysteresisAveragePixels + "\n");
    logString.append(","); // offset first column
    logString.append(",nm," + _hysteresisNanometers + "\n");

    logString.append(",\n"); // blank line
    logString.append(","); // offset first column
    logString.append("Camera,Offset,Up Edge(pixels),Down Edge(pixels)");
    logString.append("\n");

    for (AbstractXrayCamera camera : _xRayCameras)
    {
      logString.append(","); // offset first column
      logString.append(camera.getId() + "," + _cameraToOffsetMap.get(camera) + "," + _cameraToUpEdgeMap.get(camera) + "," + _cameraToDownEdgeMap.get(camera));
      logString.append("\n");
    }

    logString.append(",\n"); // blank line
    logString.append(","); // offset first column
    logString.append("Camera,");
    logString.append("Direction,RegionX(nm),RegionY(nm),RegionWidth(nm),RegionHeight(nm),");
    logString.append("StageXCameraCenter(nm),StageYCameraCenter(nm),ImageColOnCamera,ImageRowOnCamera,");
    logString.append("ImageFileName,");
    logString.append("\n");

    logUpData(logString);
    logDownData(logString);
    
    // Ying-Huan.Chu
    _logger.log(logString.toString());

    if (forceLogging)
    {
      if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION)==false)
      {
        saveUpImages();
        saveDownImages();
      }
    }
  }

  /**
   * @author Eddie Williamson
   */
  protected void executeTask() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
    //Swee Yee Wong - Camera debugging purpose
    debug("CalHysteresis");
    // XCR1569 Lee Herng 31 Jan 2013 - Before that, forceLogging is hard-coded to false. 
    // Now, we dump the value depending on saveAllConfirmationImages key
    Boolean forceLogging = _config.getBooleanValue(SoftwareConfigEnum.SAVE_ALL_CONFIRMATION_IMAGES);
    int additionalProjectionWidthNanometers = _config.getIntValue(HardwareCalibEnum.ADDITIONAL_PROJECTION_WIDTH_NANOMETERS);
//    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_HYSTERESIS);
    _hardwareObservable.setEnabled(false);
    reportProgress(0);

    boolean bHardwareSimulation=_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION);
    if (bHardwareSimulation==false)
    {
      // Get the current value. If this procedure fails, we will restore this.
      _hysteresisNanometers = _panelPositioner.getYaxisHystersisOffsetInNanometers();

      // Now set the hysteresis to zero before getting images so we can measure it directly
      _panelPositioner.setYaxisHysteresisOffsetInNanometers(0);
    }
    try
    {
      //-----------------------------------------------------------------------
      // Create list of projections to pass to IAE
      //
      // This is more complicated than usual in order to optimize the scan
      // path. IAE left to its own would generate a separate scan pass for
      // each camera and direction (UP/DOWN). This would lead to 28 scan
      // passes to get an UP and DOWN projection on each of the 14 cameras.
      //
      // The following optimizes by minimizing the scan passes. Each of
      // the cameras that can be scanned together in the UP direction
      // is used followed by those same cameras in the DOWN direction.
      // This gives 6 passes instead of 28 and saves several seconds of time.
      //-----------------------------------------------------------------------

      _xRayCameras = CalibrationSupportUtil.getHysteresisCameras();

      _xRayCameraToUpVirtualProjectionMap.clear();
      _xRayCameraToDownVirtualProjectionMap.clear();
      _projectionRequests = new ArrayList<ProjectionRequestThreadTask>();
      List<ProjectionRequestThreadTask> forwardProjectionRequests = new ArrayList<ProjectionRequestThreadTask>();
      // First generate a projection request list for just the UP direction
      for (AbstractXrayCamera camera : _xRayCameras)
      {
        VirtualProjection virtualProjection = new VirtualProjection(camera,
                                                                    ScanPassDirectionEnum.FORWARD,
                                                                    _REGION_TO_IMAGE_X_NANOMETERS,
                                                                    _REGION_TO_IMAGE_Y_NANOMETERS,
                                                                    _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                                                    _REGION_TO_IMAGE_HEIGHT_NANOMETERS,
                                                                    additionalProjectionWidthNanometers);

        _xRayCameraToUpVirtualProjectionMap.put(camera, virtualProjection);
        forwardProjectionRequests.add(virtualProjection.getProjectionRequestThreadTask());
      }

      // Have IAE generate the scan pass information but don't get the projections yet
      List<ProjectionScanPass> projectionScanPath = _imageAcquisitionEngine.generateScanPath(forwardProjectionRequests);

      // Now create a new projection request list by adding after each pass a new pass with
      // those same cameras used in the DOWN direction
      for (ProjectionScanPass projectionScanPass : projectionScanPath)
      {
        for (ProjectionRequestThreadTask projectionRequest : projectionScanPass.getProjectionRequestsToBeRun())
          _projectionRequests.add(projectionRequest);

        for (ProjectionRequestThreadTask projectionRequest : projectionScanPass.getProjectionRequestsToBeRun())
        {
          AbstractXrayCamera xRayCamera = projectionRequest.getCamera();
          VirtualProjection virtualProjection = new VirtualProjection(xRayCamera,
                                                                      ScanPassDirectionEnum.REVERSE,
                                                                      _REGION_TO_IMAGE_X_NANOMETERS,
                                                                      _REGION_TO_IMAGE_Y_NANOMETERS,
                                                                      _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                                                      _REGION_TO_IMAGE_HEIGHT_NANOMETERS,
                                                                      additionalProjectionWidthNanometers);

          _xRayCameraToDownVirtualProjectionMap.put(xRayCamera, virtualProjection);
          _projectionRequests.add(virtualProjection.getProjectionRequestThreadTask());
        }
      }
      // Finally, get the projections for the modified projection list
      reportProgress(33);

      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();

      if (bHardwareSimulation==false)
      {
        //Initialize the IAE before submitting the projection requests
        _imageAcquisitionEngine.initialize();
        _imageAcquisitionEngine.acquireProjections(_projectionRequests, testExecutionTimer, false);
      }
      reportProgress(66);

      boolean failTest = false;
      String failCameraMessage = "\nFailed Camera: ";
      float sum = 0;
      int numberOfValues = 0;
      for (AbstractXrayCamera camera : _xRayCameras)
      {
        // virtual projectons are sorted by camera (0,1,...) then by scan direction (UP then DOWN)
        // Get the UP image
        VirtualProjection virtualProjection;
        Image imageUp;
        if (bHardwareSimulation==false)
        {
          virtualProjection = _xRayCameraToUpVirtualProjectionMap.get(camera);
          Assert.expect(virtualProjection != null);
          imageUp = virtualProjection.getImage();
        }
        else
        {
          String jpgFilename = Directory.getStageHysteresisCalLogDataDir() + "\\cam_" + camera.getId() + "_up.jpg";
          String pngFilename = Directory.getStageHysteresisCalLogDataDir() + "\\cam_" + camera.getId() + "_up.png";
          if (FileUtil.exists(pngFilename))
            imageUp = XrayImageIoUtil.loadPngImage(pngFilename);
          else
            imageUp = XrayImageIoUtil.loadJpegImage(jpgFilename);
        }
        ImageCoordinate sysFidPointLocation = SystemFiducialTemplateMatch.matchTemplate(imageUp, false, false);
        if (sysFidPointLocation.getX() == -1)
        {
          // Can't find system fiducial, so can't continue.   Save image, fail adjustment and
          // throw exception.  Adjustment will fail because the result is initialized with a failing
          // value, -1.  The original value in hardware.calib will be restored in the finally clause since
          // _hysteresisNanometers holds it.
          // So, all cleanup necessary is going to happen when the throw occurs.
          
          String directionString = "UP";
          String filename = "cam_" + camera.getId() + "_" + directionString + ".jpg";
          XrayImageIoUtil.saveJpegImage(imageUp, _logDirectory + File.separator + filename);
          //throw new HardwareTaskExecutionException(ConfirmationExceptionEnum.CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH);
          failTest = true;
        }

       // Get the DOWN image
        Image imageDown;
        if (bHardwareSimulation==false)
        {
          virtualProjection = _xRayCameraToDownVirtualProjectionMap.get(camera);
          Assert.expect(virtualProjection != null);
          imageDown = virtualProjection.getImage();
        }
        else
        {
          String jpgFilename = Directory.getStageHysteresisCalLogDataDir() + "\\cam_" + camera.getId() + "_down.jpg";
          String pngFilename = Directory.getStageHysteresisCalLogDataDir() + "\\cam_" + camera.getId() + "_down.png";
          if (FileUtil.exists(pngFilename))
            imageDown = XrayImageIoUtil.loadPngImage(pngFilename);
          else
            imageDown = XrayImageIoUtil.loadJpegImage(jpgFilename);
        }
        sysFidPointLocation = SystemFiducialTemplateMatch.matchTemplate(imageDown, false, false);
        if (sysFidPointLocation.getX() == -1)
        {
          //throw new HardwareTaskExecutionException(ConfirmationExceptionEnum.CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH);
          String directionString = "DOWN";
          String filename = "cam_" + camera.getId() + "_" + directionString + ".jpg";
          XrayImageIoUtil.saveJpegImage(imageDown, _logDirectory + File.separator + filename);
          failTest = true;
        }

        // Find the horizontal edges and calculate the hysteresis offset
        float edgeUp = ImageFeatureExtraction.findHorizontalEdge(imageUp);
        float edgeDown = ImageFeatureExtraction.findHorizontalEdge(imageDown);
        float offset = edgeUp - edgeDown;
        _cameraToUpEdgeMap.put(camera, edgeUp);
        _cameraToDownEdgeMap.put(camera, edgeDown);
        _cameraToOffsetMap.put(camera, offset);
        // Accumulate
        sum += offset;
        numberOfValues++;
      }
      
      if (failTest)
      {
          System.out.println("\nsum = " + sum);
          System.out.println("\nnumberOfValues = " + numberOfValues);
          System.out.println(failCameraMessage);
          throw new HardwareTaskExecutionException(ConfirmationExceptionEnum.CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH);
      }

      _hysteresisAveragePixels = sum / numberOfValues;
      int nanometersPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
      _hysteresisNanometers = Math.round(_hysteresisAveragePixels * nanometersPerPixel);
      _localResults[_HYSTERESIS_IN_NANOMETERS_INDEX].setResults(new Integer(_hysteresisNanometers));

      logInformation(forceLogging);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);

      // If we fail, create a good error message and DON'T set the hysteresis value
      if(didTestPass() == false)
      {
        // Create the parameters for the status message
        Object[] params = {_parsedLimits.getLowLimitInteger("nanometers"),
                          _parsedLimits.getHighLimitInteger("nanometers"),
                          _hysteresisNanometers};

        // Create the localized string
        LocalizedString statusMessage = new LocalizedString("ACD_HYSTERESIS_FAILED_TOO_LARGE_KEY", params);

        // Populate the status and action messages
        _result.setTaskStatusMessage(StringLocalizer.keyToString(statusMessage));
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("ACD_HYSTERESIS_FAILED_CHECK_MOTION_AND_STAGE_SUGGESTED_ACTION_KEY"));
      }
      // We passed the limits check so save/set the value
      else
      {
        try
        {
          _panelPositioner.setYaxisHysteresisOffsetInNanometers(_hysteresisNanometers);
          reportProgress(100);
        }
        catch (DatastoreException ex)
        {
          // do nothing, allow original exception to propagate up
          throw ex;
        }
      }
    }
//    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_HYSTERESIS);
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
