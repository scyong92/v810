package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
public class XrayCameraTriggerControlThreadTask extends ThreadTask<Object>
{
  private boolean _enable;
  private boolean _previousEnable = false;
  private AbstractXrayCamera _xrayCamera;

  /**
   * @author Eddie Williamson
   */
  XrayCameraTriggerControlThreadTask(AbstractXrayCamera camera, boolean enable)
  {
    super("XrayCameraTriggerControlThread: " + camera.getId() + ":" + enable);
    _enable = enable;
    _xrayCamera = camera;
  }

  /**
   * @return The previous trigger enable state returned from the camera.
   * @author Eddie Williamson
   */
  public boolean getPreviousEnable()
  {
    return _previousEnable;
  }

  /**
   * @throws Exception
   * @return Any Object will do, it is not used.
   * @author Eddie Williamson
   */
  protected Object executeTask() throws Exception
  {
    _previousEnable = _xrayCamera.isTriggerDetectionEnabled();

    if (_enable)
      _xrayCamera.enableTriggerDetection();
    else
      _xrayCamera.disableTriggerDetection();

    return this;
  }

  /**
   * @author Bill Darbie
   */
  protected void clearCancel()
  {
    // Do nothing to clear the cancel
  }

  /**
   * @author Bill Darbie
   */
  protected void cancel() throws XrayTesterException
  {
    // The action of this thread task is atomic so cancel is not possible
  }

}
