package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * @author Reid Hayhow
 */
public class Test_CameraLightConfirmationTaskTwo extends UnitTest
{

  //Create a test utils class
  private TestUtils _utils = new TestUtils();


  /**
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CameraLightConfirmationTaskTwo());
  }

  /**
   * @author Reid Hayhow
   */
  protected Test_CameraLightConfirmationTaskTwo()
  {
    super();
  }

  /**
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XrayCameraArray xRayCameraArray = null;
    boolean testPassed = false;

    try
    {
      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getLightTestTwoDir();
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);
      CalGrayscale.getInstance().executeUnconditionally();

      //---------------------First test group one---------------------
      //Get an instance of the test
      ConfirmCameraGroupOneLightImageTask groupOneLightConfirmation = ConfirmCameraGroupOneLightImageTask.getInstance();

      //Run the test
      testPassed = groupOneLightConfirmation.executeUnconditionally();

      //Check to make sure start and stop dates are set and statii are passed
      _utils.checkForStartStopDatesAndStatus(groupOneLightConfirmation.getResult(), true);

      //This test should fail, if not dump info into .actual file and assert
      if (testPassed == true)
      {
        Expect.expect(false, "Light Image Group One Confirmation should not pass!");
      }
      else
      {
        _utils.displayFailureData(groupOneLightConfirmation.getResult(), groupOneLightConfirmation.getLimit(), os);
      }

      //---------------------Now group two---------------------
      //Get an instance of the test
      ConfirmCameraGroupTwoLightImageTask groupTwoLightConfirmation = ConfirmCameraGroupTwoLightImageTask.getInstance();

      //Run the test
      testPassed = groupTwoLightConfirmation.executeUnconditionally();

      //Check to make sure start and stop dates are set and statii are passed
      _utils.checkForStartStopDatesAndStatus(groupTwoLightConfirmation.getResult(), true);

      //This test should pass, if not dump info into .actual file and assert
      if (testPassed == false)
      {
        Expect.expect(false, "Light Image Group Two Confirmation Should pass!");
      }
      else
      {
        _utils.displayFailureData(groupTwoLightConfirmation.getResult(), groupTwoLightConfirmation.getLimit(), os);
      }

      //---------------------Now group three---------------------
      //Get an instance of the test
      ConfirmCameraGroupThreeLightImageTask groupThreeLightConfirmation = ConfirmCameraGroupThreeLightImageTask.getInstance();

      //Run the test
      testPassed = groupThreeLightConfirmation.executeUnconditionally();

      //Check to make sure start and stop dates are set and statii are passed
      _utils.checkForStartStopDatesAndStatus(groupThreeLightConfirmation.getResult(), true);

      //This test should fail, if not dump info into .actual file and assert
      if (testPassed == true)
      {
        Expect.expect(false, "Light Image Group Three Confirmation Should not pass!");
      }
      else
      {
        _utils.displayFailureData(groupThreeLightConfirmation.getResult(), groupThreeLightConfirmation.getLimit(), os);

      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
    }
    catch (Exception e)
    {
      e.printStackTrace(os);
    }
    finally
    {
      xRayCameraArray.disableUnitTestMode();
    }
  }

  /**
   * Copy the golden limits into place before starting the hardware. Also copy
   * the calib file (it will get clobbered).
   *
   * @author Reid Hayhow
   */
  protected void setupBeforeTest()
  {
    _utils.copyLimitsForRegressionTests(getTestSourceDir());
    _utils.backupHWCalib(getTestSourceDir());
  }

  /**
   * Restore the original limits into place when done. Also copy
   * the original calib file back.
   *
   * @author Reid Hayhow
   */
  protected void restoreAfterTest()
  {
    //Use the utils to copy the new limits back
    _utils.restoreLimitsAfterRegressionTests();
    _utils.restoreHWCalib(getTestSourceDir());
  }
}
