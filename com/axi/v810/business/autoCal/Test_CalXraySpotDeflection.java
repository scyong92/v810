package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * A basic regression test for the Xray Spot Deflection Adjustment. Checks
 * to make sure the Adjustment makes the right number of files and puts them
 * in the right directory.
 *
 * @author Reid Hayhow
 */
class Test_CalXraySpotDeflection extends UnitTest
{
  public Test_CalXraySpotDeflection()
  {
  }

  /**
   * Shell to invoke the regression test
   *
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CalXraySpotDeflection());
  }

  /**
   * All unit test classes must provide a test() method that matches this
   * signature.
   *
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    //Get a copy of the test utils used to backup and restore config files
    TestUtils utils = new TestUtils();

    try
    {
      // Assure that the tester is started up, otherwise tasks won't run!
      XrayTester.getInstance().setSimulationMode();
      XrayTester.getInstance().startup();

      //We are going to fiddle with hardware.calib and hardware.config, back them up
      utils.backupHWCalib(getTestSourceDir());
      utils.backupHWConfig(getTestSourceDir());
      utils.backupSoftwareConfig(getTestSourceDir());

      //Get a reference to the config instance
      Config config = Config.getInstance();

      //Load it if necessary
      config.loadIfNecessary();

      //No need to loop the normal number of times, we just want to loop enough to
      //create the image files
      config.setValue(SoftwareConfigEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_REPEAT_COUNT, 2);

      //Get the Adjustent instance
      CalXraySpotDeflection cal = CalXraySpotDeflection.getInstance();

      //Get a handle to the logging directory so we can make sure the image files
      //were dumped there
      File loggingDirectory = new File(Directory.getXraySpotDeflectionCalLogDir());

      //If the directory exists, empty it out of any files
      if (loggingDirectory.exists() == true)
      {
        //Get the list of all the files
        File[] files = loggingDirectory.listFiles();

        //For each fime in the list, delete it
        for (File file : files)
        {
          file.delete();
          Assert.expect(file.exists() == false);
        }
      }

      //Run the Adjustment, will create 14 images in the logfile directory
      cal.executeUnconditionally();

      //Even if the directory didn't exist before it should exist now.
      Assert.expect(loggingDirectory.exists());

      //Also, there should be one file per camera
      Assert.expect(loggingDirectory.listFiles().length == XrayCameraArray.getNumberOfCameras());

      //Tidy up after ourselves by removing the image files
      File[] files = loggingDirectory.listFiles();
      for (File file : files)
      {
        file.delete();
        Assert.expect(file.exists() == false);
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
    }
    finally
    {
      //Finally, restore the hardware.calib and hardware.config
      utils.restoreHWCalib(getTestSourceDir());
      utils.restoreHWConfig(getTestSourceDir());
      utils.restoreSoftwareConfig(getTestSourceDir());
    }
  }
}
