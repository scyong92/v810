package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Confirmation to test camera light values. This confirmation gets a set of
 * images from the cameras and checks them for horizontal flatness, vertical
 * flatness, mean gray value and gray value standard deviation. These values
 * must fall within acceptable limits for the cameras to be functioning
 * correctly.
 *
 * This task *may* need to be run in three distinct parts because it requires
 * Xrays to be on and that the images be retreived from cameras that are not
 * covered/obstructed by a board, the stage, etc.
 *
 * @author Reid Hayhow
 */
public class ConfirmCameraGroupTwoLightImageTask extends ConfirmCameraImageTask
{
  private static final String NAME =
      StringLocalizer.keyToString("CD_CAMERA_LIGHT_CONFIRMATION_KEY") +
      StringLocalizer.keyToString("CD_CAMERA_GROUP_TWO_KEY");

  // Get the frequency from the hardware.calib file
  private static final int FREQUENCY =
      _config.getIntValue(HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_FREQUENCY);

  // Get the timeout from the hardware.calib file
  private static final long TIMEOUTINSECONDS =
      _config.getLongValue(SoftwareConfigEnum.CAMERA_LIGHT_IMAGE_GROUP_TWO_CONFIRM_TIMEOUT_IN_SECONDS);

  private static ConfirmCameraGroupTwoLightImageTask _instance = null;

  /**
   * Constructor for this confirmation task
   * @author Reid Hayhow
   */
  private ConfirmCameraGroupTwoLightImageTask()
  {
    //Create it and specify the logging directory for the logger stored in the
    //super class
    super(NAME,
          FREQUENCY,
          TIMEOUTINSECONDS,
          Directory.getCameraLightConfirmationLogDir(),
          ProgressReporterEnum.LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_TWO);

    //This confirmation is for camera group two
    _camerasToGetImagesFromSet = _cameraGroupTwo;

    //Get the stage position for camera group two
    _xPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS;
    _yPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS;

    //The camera group for group two is on the right side of the tester, needed
    //to reposition panel before test
    cameraGroupIsOnRightSideOfTester();

    //This is a light image task
    _localizedLightOrDarkString = StringLocalizer.keyToString("ACD_LIGHT_KEY");

    //Set the enum used to save the timestamp in the hardware.calib file
    setTimestampEnum(HardwareCalibEnum.LIGHT_IMAGE_GROUP_TWO_LAST_TIME_RUN);
  }

  /**
   * Instance access to this confirmation task
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCameraGroupTwoLightImageTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCameraGroupTwoLightImageTask();
    }
    return _instance;
  }
  
//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmCameraImageTask will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
}
