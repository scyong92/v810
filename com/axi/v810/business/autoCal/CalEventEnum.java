package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * @author Eddie Williamson
 */
public class CalEventEnum extends HardwareEventEnum
{
  private static int _index = -1;

   public static final CalEventEnum ADJUSTING_GRAYSCALE = new CalEventEnum(++_index, "Calibration, Adjusting Grayscale");
   public static final CalEventEnum ADJUSTING_XRAYSPOT = new CalEventEnum(++_index, "Calibration, Adjusting Xray Spot");
   public static final CalEventEnum ADJUSTING_HYSTERESIS = new CalEventEnum(++_index, "Calibration, Adjusting Hysteresis");
   public static final CalEventEnum ADJUSTING_SYSTEMOFFSETS = new CalEventEnum(++_index, "Calibration, Adjusting System Offset");
   public static final CalEventEnum ADJUSTING_MAGNIFICATION = new CalEventEnum(++_index, "Calibration, Adjusting Magnification");
   public static final CalEventEnum ADJUSTING_OPTICAL_MAGNIFICATION = new CalEventEnum(++_index, "Calibration, Adjusting Optical Magnification");
   public static final CalEventEnum ADJUSTING_OPTICAL_HEIGHT = new CalEventEnum(++_index, "Calibration, Adjusting Optical Height");
   public static final CalEventEnum ADJUSTING_OPTICAL_SYSTEM_OFFSET = new CalEventEnum(++_index, "Calibration, Adjusting Optical System Offset");

   private String _name;

   /**
    * @author Rex Shang
    */
   private CalEventEnum(int id, String name)
   {
     super(id);
     Assert.expect(name != null);

     _name = name;
   }

   /**
    * @author Rex Shang
    */
   public String toString()
   {
     return _name;
  }

}
