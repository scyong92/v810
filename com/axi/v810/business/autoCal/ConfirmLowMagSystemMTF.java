package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * @author Rick Gaudette
 * @since 5.3
 */
public class ConfirmLowMagSystemMTF extends ConfirmSystemMTF
{
  private static ConfirmLowMagSystemMTF _instance = null;

  private ConfirmLowMagSystemMTF()
  {
    super(StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_MTF_KEY"), Directory.getBaseImageQualityConfirmationLogDir());
    _NO_OF_PROJECTIONS = 4;
    _magnification = MagnificationEnum.NOMINAL;
    _differentInX = 0;
    _differentInY = 0;

    _MIN_EDGE_WIDTH_IN_PIXELS = 200;
    _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
        _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
    _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
        _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS);
    _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
        _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS);


    AbstractXrayCamera xRayCamera = XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
    _REGION_TO_IMAGE_WIDTH_NANOMETERS =
        xRayCamera.getNumberOfSensorPixelsUsed() * _magnification.getNanoMetersPerPixel();


  }

  /**
   * Instance reference access because this is a singleton
   *
   * @author Rick Gaudette
   */
  public static synchronized ConfirmLowMagSystemMTF getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmLowMagSystemMTF();
    }
    return _instance;
  }
  
//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmSharpness will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
}
