package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Reid Hayhow
 */
public class Test_CameraDarkConfirmationTask extends UnitTest
{

  /**
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CameraDarkConfirmationTask());
  }

  /**
   * @author Reid Hayhow
   */
  protected Test_CameraDarkConfirmationTask()
  {
    super();
  }

  /**
   * @author Reid Hayhow
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    //Create a test utils class
    TestUtils utils = new TestUtils();
    XrayCameraArray xRayCameraArray = null;

    //Use the test utils to copy an old, copy of limits and HW config for testing
    utils.copyLimitsForRegressionTests(getTestSourceDir());
    utils.backupHWConfig(getTestSourceDir());
    utils.backupHWCalib(getTestSourceDir());

    boolean testPassed = false;
    try
    {
      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getDarkTestOneDir();
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);

      CalGrayscale.getInstance().executeUnconditionally();

      //Get an instance of the test to run
      ConfirmCameraDarkImageTask darkConfirmation = ConfirmCameraDarkImageTask.getInstance();

      //Execute the test
      testPassed = darkConfirmation.executeUnconditionally();

      //Check to make all start and stop dates are set and all statii are "passed"
      utils.checkForStartStopDatesAndStatus(darkConfirmation.getResult(), false);

      //For this test, it should pass, if not, dump failure info into .actual
      //and assert
      if (testPassed == false)
      {
        utils.displayFailureData(darkConfirmation.getResult(), darkConfirmation.getLimit(), os);
        Expect.expect(false, "Dark Image Confirmation Test failed");
      }
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(os);
      Assert.expect(false, ex.getMessage());
    }
    catch (Exception e)
    {
      e.printStackTrace(os);
      Assert.expect(false, e.getMessage());
    }

    finally
    {
      xRayCameraArray.disableUnitTestMode();

      //Use the utils to copy the new limits and HW config back
      utils.restoreLimitsAfterRegressionTests();
      utils.restoreHWConfig(getTestSourceDir());
      utils.restoreHWCalib(getTestSourceDir());
    }
  }
}
