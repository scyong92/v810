package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;


/**
 * Calibration (adjustment) to determine the stage location that puts
 * the system fiducial directly under the xray source. This measurement
 * corrects for most of the mechanical slop in the system.
 * @author Eddie Williamson
 */
public class ConfirmXraySpotCoordinateForReverseDirectionTask extends EventDrivenHardwareTask
{
  private static ConfirmXraySpotCoordinateForReverseDirectionTask _instance;

  // Printable name for this procedure
  private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_XRAY_SPOT_REVERSE_NAME_KEY");

  private static Config _config = Config.getInstance();

  // This cal is called on the first event after the timeout occurs.
  private static final int _EVENT_FREQUENCY = 1;
  private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_EXECUTE_EVERY_TIMEOUT_SECONDS);

  // A template match quality metric below this will fail the cal (triangle not in image)
  // The range is from -1.0 to 1.0 where 1.0 is a perfect match
  private static final float _QUALITYTHRESHOLD = 0.89F;

  // Define the region to be imaged for this cal
  // X & Y are relative to the system fiducial
  private static final int _REGION_TO_IMAGE_X_NANOMETERS = -7200000;
  private static final int _REGION_TO_IMAGE_Y_NANOMETERS = -8480000;
  
  // These variables are determined based on system values that are not known
  // at object instanciation time (MechanicalConversions). They are initialized in
  // the setUp method but are set to values that will fail here
  private static int _REGION_TO_IMAGE_WIDTH_NANOMETERS = -1;
  private static int _REGION_TO_IMAGE_HEIGHT_NANOMETERS = -1;

  // The template to match within the image is generated from these parameters
  private static int _templateBorderWidthPixels = 10;
  private static int _templateBorderHeightPixels = 10;
  private static int _templateSizeXPixels = XrayTester.getSystemFiducialWidthInNanometers()/
                                            MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static int _templateSizeYPixels = XrayTester.getSystemFiducialHeightInNanometers()/
                                            MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static Image _template =
      CalibrationSupportUtil.createTemplateImageOfSystemFiducial(_templateBorderWidthPixels,
      _templateBorderHeightPixels, _templateSizeXPixels, _templateSizeYPixels);

  private static final String _logDirectory = Directory.getXraySpotCalLogDir();
  private static final long _logTimeoutSeconds = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_LOG_RESULTS_EVERY_TIMEOUT_SECONDS);
  private static final long _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000L;
  private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_MAX_LOG_FILES_IN_DIRECTORY);

  // Internal index to identify results & limits
  private static int _index = 0;
  private static final int _FIDUCIAL_FOUND_RESULT_INDEX = _index++;
  private static final int _MATCH_QUALITY_RESULT_INDEX = _index++;
  private static final int _STAGE_X_RESULT_INDEX = _index++;
  private static final int _STAGE_Y_RESULT_INDEX = _index++;
  private static final int _MAX_RESULT_INDEX = _index++; // Must be last, used to size arrays

  private int _defaultXLocationNanometers = _config.getIntValue(HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_X_NANOMETERS);
  private int _defaultYLocationNanometers = _config.getIntValue(HardwareCalibEnum.DEFAULT_XRAY_SPOT_LOCATION_Y_NANOMETERS);
  private List<VirtualProjection> _virtualProjections = new ArrayList<VirtualProjection>();
  private DoubleRef _matchQuality = new DoubleRef(0.0F);
  private boolean _fiducialFound = false;
  private ImageCoordinate _templateCoordinates;
  private ImageCoordinate _expectedLocation;
  private DirectoryLoggerAxi _logger;
  private CalSystemFiducialEventDrivenHardwareTask _calSystemOffsets = CalSystemFiducialEventDrivenHardwareTask.getInstance();

  /**
   * @author Eddie Williamson
   */
  static
  {
    Assert.expect(_MAX_RESULT_INDEX > _FIDUCIAL_FOUND_RESULT_INDEX);
    Assert.expect(_MAX_RESULT_INDEX > _MATCH_QUALITY_RESULT_INDEX);
    Assert.expect(_MAX_RESULT_INDEX > _STAGE_X_RESULT_INDEX);
    Assert.expect(_MAX_RESULT_INDEX > _STAGE_Y_RESULT_INDEX);
  }

  /**
   * @author Eddie Williamson
   */
  private ConfirmXraySpotCoordinateForReverseDirectionTask()
  {
    super(_NAME_OF_CAL,
          _EVENT_FREQUENCY,
          _EXECUTE_EVERY_TIMEOUT_SECONDS,
          ProgressReporterEnum.XRAY_SPOT_ADJUSTMENT_TASK);

    _logger = new DirectoryLoggerAxi(_logDirectory, FileName.getXraySpotLogFileBasename(),
                                     FileName.getLogFileExtension(), _maxLogFilesInDirectory);

    _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

    //set the timestamp enum to save the last time this cal was run
    setTimestampEnum(HardwareCalibEnum.XRAY_SPOT_CALIBRATION_LAST_TIME_RUN);
  }


  /**
   * @author Eddie Williamson
   */
  public static synchronized ConfirmXraySpotCoordinateForReverseDirectionTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmXraySpotCoordinateForReverseDirectionTask();
    }
    return _instance;
  }

  /**
   * Logs detailed information about the cal and saves images when the timeout
   * is exceeded. The log information is formatted as comma separated values to
   * it can be imported into Excel for manual analysis.
   * @param forceLogging If true then override the timeout and log anyway.
   * @author Eddie Williamson
   */
  private void logInformation(Boolean forceLogging) throws DatastoreException
  {
    for (VirtualProjection virtualProjection : _virtualProjections)
    {
      // Save image to file, use the save filename that was logged above
      AbstractXrayCamera camera = virtualProjection.getCamera();
      Image image = virtualProjection.getImage();
      StringBuilder filename = new StringBuilder();
      //The file name for the saved file changes if the fiducial is not found
      if(_fiducialFound)
      {
        filename.append("cam_" + camera.getId() + "_rev_" + _config.getSystemCurrentLowMagnificationForLogging() +".png");
      }
      else
      {
        filename.append("cam_" + camera.getId() + "_rev_FAILED_" + _config.getSystemCurrentLowMagnificationForLogging() + ".png");
      }
      XrayImageIoUtil.savePngImage(image.getBufferedImage(), _logDirectory + File.separator + filename);
      XrayImageIoUtil.savePngImage(_template.getBufferedImage(), _logDirectory + File.separator + "template.png");
    }
  }


  /**
   * @author Eddie Williamson
   */
  public void createResultsObject()
  {
    _result = new Result(new Boolean(true), _NAME_OF_CAL);
  }


  /**
   * @author Eddie Williamson
   */
  public void createLimitsObject() throws DatastoreException
  {
    _limits = new Limit(null, null, null, null);
  }

  /**
   * This method sets up two variables that determine image width and height. They
   * cannot be set up at object instanciation time because they require hardware
   * information.
   *
   * @author Eddie Williamson
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    _REGION_TO_IMAGE_WIDTH_NANOMETERS = XrayCameraArray.getCameraWidthInNanometersAtNominalMagnification();

    // This call CANNOT be made unless the hardware has been started up
    _REGION_TO_IMAGE_HEIGHT_NANOMETERS = MechanicalConversions.getMinimumStageTravelInNanometers();
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
  }


  /**
   * @author Eddie Williamson
   */
  public void cleanUp()
  {
    // Free image memory, reset pointers.
    for (VirtualProjection virtualProjection : _virtualProjections)
      virtualProjection.clear();
    _virtualProjections.clear();
    
    if (_matchQuality != null)
      _matchQuality = null;
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }


  /**
   * @return true if this procedure can run on this hardware platform
   * @author Eddie Williamson
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * @author Eddie Williamson
   */
  public void executeTask() throws XrayTesterException
  {
    ImageAcquisitionEngine imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
    Boolean forceLogging = false;
    _fiducialFound = false;
//    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_XRAYSPOT);
    _hardwareObservable.setEnabled(false);

    try
    {
      reportProgress(0);

      // Pixel size at the reference plane, Use nominal mag for X because mag cal might not have been run yet.
      // It's close enough for this cal.
      int xDirectionNanometersPerPixel =
        (int)Math.round(MechanicalConversions.getStepDirectionPixelSizeInNanometersAboveNominalPlane(0.0));

      int yDirectionNanometersPerPixel = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();

      // Create the list of projections required -- only one for this cal
      // Image acquisition wants a list of projections. These must be at least 1 inch long. The
      // virtual projection allows specifying the size I actually want. It then generates the
      // projection request for me and handles cropping the returned image to the size I originally
      // asked for.
      AbstractXrayCamera camera = CalibrationSupportUtil.getXraySpotCoordinateCamera();
      _virtualProjections = new ArrayList<VirtualProjection>();
      List<ProjectionRequestThreadTask> projectionRequests = new ArrayList<ProjectionRequestThreadTask>();
      VirtualProjection virtualProjection = new VirtualProjection(camera,
                                                                  ScanPassDirectionEnum.REVERSE,
                                                                  _REGION_TO_IMAGE_X_NANOMETERS,
                                                                  _REGION_TO_IMAGE_Y_NANOMETERS,
                                                                  _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                                                  _REGION_TO_IMAGE_HEIGHT_NANOMETERS);
      _virtualProjections.add(virtualProjection);
      projectionRequests.add(virtualProjection.getProjectionRequestThreadTask());

      // Get the projections
      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
      reportProgress(25);

      // Initialize the IAE before requesting projections
      imageAcquisitionEngine.initialize();
      imageAcquisitionEngine.acquireProjections(projectionRequests, testExecutionTimer, true);
      reportProgress(75);

      Image image = virtualProjection.getImage();

      // calculate the nominal location of the SF relative to the image origin
      _expectedLocation = new ImageCoordinate( -_REGION_TO_IMAGE_X_NANOMETERS / xDirectionNanometersPerPixel,
                                              (_REGION_TO_IMAGE_HEIGHT_NANOMETERS + _REGION_TO_IMAGE_Y_NANOMETERS) /
                                              yDirectionNanometersPerPixel);

      RegionOfInterest regionOfInterest = RegionOfInterest.createRegionFromImage(image);
      regionOfInterest.scaleFromCenterXY(0.7F, 0.5F);
      regionOfInterest.setMinXY(0, image.getHeight()/2);
      _matchQuality = new DoubleRef();  // Metric returned from template match
      _templateCoordinates = Filter.matchTemplate(image,
                                                  _template,
                                                  regionOfInterest,
                                                  MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                  _matchQuality);

      if (_matchQuality.getValue() > _QUALITYTHRESHOLD)
      {
        _fiducialFound = true;
      }
      else
      {
        // match quality too low means the fiducial was missing from the image
        _fiducialFound = false;
      }

      // Force logging on failure so we can do post mortem
      forceLogging = true;
      
      logInformation(forceLogging);
      reportProgress(100);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
//    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_XRAYSPOT);
     
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
}