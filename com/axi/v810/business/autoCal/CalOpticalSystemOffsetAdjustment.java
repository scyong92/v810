package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import java.io.*;
import java.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CalOpticalSystemOffsetAdjustment extends EventDrivenHardwareTask
{
  private static CalOpticalSystemOffsetAdjustment _instance;
  private static Config _config = Config.getInstance();
  // Printable name for this procedure
  private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_NAME_KEY");
  // This cal is called on the first event after the timeout occurs.
  private static final int _EVENT_FREQUENCY = 1;
  private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.OPTICAL_SYSTEM_OFFSET_EXECUTE_EVERY_TIMEOUT_SECONDS);
  private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.OPTICAL_SYSTEM_OFFSET_MAX_LOG_FILES_IN_DIRECTORY);
  
  // Logging support
  private static final String _logDirectory = Directory.getOpticalSystemOffsetAdjustmentCalLogDir();     
  private ExecuteParallelThreadTasks<Object> _heightCalParallelThread = new ExecuteParallelThreadTasks<Object>();
  
  //---------------------------------------------------------------------------
  // Define Optical Fiducial position
  //---------------------------------------------------------------------------
  private static int _regionToImageXMils = -1;
  private static int _regionToImageYMils = -1;
  private static int _regionToImageWidthMils = -1;
  private static int _regionToImageHeightMils = -1;
  
  // This region is just a little bit larger to enclose the region above. This allows enough
  // room so as magnification is varied the pixel shifts will still be in the projections.
  private static int _regionToEncloseImageXMils = -1;
  private static int _regionToEncloseImageYMils = -1;
  private static int _regionToEncloseImageWidthMils = -1;
  private static int _regionToEncloseImageHeightMils = -1;
  
  private ImageAcquisitionEngine _iae;  
  private TestProgram _testProgram;
  
  private double _zHeightInMilsToRestore = 0.0;
  
  private Map<Integer, Pair<Double,Double>> _sharpnessInfoMap;

  private Result _parametricResult;
  private Result _booleanResult;
    
  private DirectoryLoggerAxi _logger;  
  private boolean _imagesWereValid;
  private boolean _allSharpnessValid;
  
  // Statically setup optical coupon region
  static
  {
    if (XrayTester.isXXLBased())
    {
      _regionToImageXMils = 570;
      _regionToImageYMils = 6350;
      
      _regionToImageWidthMils = 250;
      _regionToImageHeightMils = 250;
      
      _regionToEncloseImageXMils = _regionToImageXMils - 25;
      _regionToEncloseImageYMils = _regionToImageYMils - 25;
      
      _regionToEncloseImageWidthMils = _regionToImageWidthMils + 50;
      _regionToEncloseImageHeightMils = _regionToImageHeightMils + 50;
    }
    else if(XrayTester.isS2EXEnabled())
    {
      _regionToImageXMils = -520;
      _regionToImageYMils = 6430;
      
      _regionToImageWidthMils = 250;
      _regionToImageHeightMils = 250;
      
      _regionToEncloseImageXMils = _regionToImageXMils - 25;
      _regionToEncloseImageYMils = _regionToImageYMils - 25;
      
      _regionToEncloseImageWidthMils = _regionToImageWidthMils + 50;
      _regionToEncloseImageHeightMils = _regionToImageHeightMils + 50;
    }
    else  // Standard System
    {
      if (PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4)) // New Manual-Loaded Jig (3-plane jig)
      {
        _regionToImageXMils = -600;
        _regionToImageYMils = 7440;

        _regionToImageWidthMils = 250;
        _regionToImageHeightMils = 250;

        _regionToEncloseImageXMils = _regionToImageXMils - 25;
        _regionToEncloseImageYMils = _regionToImageYMils - 25;

        _regionToEncloseImageWidthMils = _regionToImageWidthMils + 50;
        _regionToEncloseImageHeightMils = _regionToImageHeightMils + 50;
      }
      else  // On-Rail Jig
      {
        _regionToImageXMils = -5820;
        _regionToImageYMils = 6600;

        _regionToImageWidthMils = 250;
        _regionToImageHeightMils = 250;

        _regionToEncloseImageXMils = _regionToImageXMils - 25;
        _regionToEncloseImageYMils = _regionToImageYMils - 25;

        _regionToEncloseImageWidthMils = _regionToImageWidthMils + 50;
        _regionToEncloseImageHeightMils = _regionToImageHeightMils + 50;
      }
    }
  }

  /**
   *
   * @author John Dutton
   */
  public static PanelRectangle getPanelRectangleToImage()
  {
    PanelRectangle panelRectangleToImage = new PanelRectangle(
            MathUtil.convertMilsToNanoMetersInteger(_regionToImageXMils),
            MathUtil.convertMilsToNanoMetersInteger(_regionToImageYMils),
            MathUtil.convertMilsToNanoMetersInteger(_regionToImageWidthMils),
            MathUtil.convertMilsToNanoMetersInteger(_regionToImageHeightMils));

    return panelRectangleToImage;
  }

  /**
   *
   * @author John Dutton
   */
  public static PanelRectangle getRectangleEnclosingAllRegions()
  {
    PanelRectangle rectangleEnclosingAllRegions = new PanelRectangle(
            MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageXMils),
            MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageYMils),
            MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageWidthMils),
            MathUtil.convertMilsToNanoMetersInteger(_regionToEncloseImageHeightMils));

    return rectangleEnclosingAllRegions;
  }

  /**
   * @author Eddie Williamson
   */
  public static synchronized CalOpticalSystemOffsetAdjustment getInstance()
  {
    if (_instance == null)
    {
      _instance = new CalOpticalSystemOffsetAdjustment();
    }
    return _instance;
  }

  /**
   * @author Eddie Williamson
   */
  private CalOpticalSystemOffsetAdjustment()
  {
    super(_NAME_OF_CAL,
          _EVENT_FREQUENCY,
          _EXECUTE_EVERY_TIMEOUT_SECONDS,
          ProgressReporterEnum.OPTICAL_SYSTEM_OFFSET_ADJUSTMENT_TASK);
    _logger = new DirectoryLoggerAxi(_logDirectory, FileName.getOpticalSystemOffsetLogFileBasename(),
                                     FileName.getLogFileExtension(), _maxLogFilesInDirectory);

    _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

    setTimestampEnum(PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_OFFSET_LAST_TIME_RUN);
  }

  /**
   * @author Eddie Williamson
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * @author Eddie Williamson
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);

    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if (_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void createLimitsObject() throws DatastoreException
  {
    // Create container for the others
    _limits = new Limit(null, null, null, _NAME_OF_CAL);
    Limit booleanLimit = new Limit(null, null, null, "Optical fiducial Adjustment feature present");
    Limit parametricLimit = null;
    
    if (XrayTester.isXXLBased())
    {
      parametricLimit = new Limit(new Double(_parsedLimits.getLowLimitDouble("opticalFiducialHeightForXXL")),
                                      new Double(_parsedLimits.getHighLimitDouble("opticalFiducialHeightForXXL")),
                                      "opticalFiducialHeight", "Optical System Fiducial Height");
    }
    else if(XrayTester.isS2EXEnabled())
    {
      parametricLimit = new Limit(new Double(_parsedLimits.getLowLimitDouble("opticalFiducialHeightForS2EX")),
                                      new Double(_parsedLimits.getHighLimitDouble("opticalFiducialHeightForS2EX")),
                                      "opticalFiducialHeight", "Optical System Fiducial Height");
    }
    else
    {
      parametricLimit = new Limit(new Double(_parsedLimits.getLowLimitDouble("opticalFiducialHeight")),
                                      new Double(_parsedLimits.getHighLimitDouble("opticalFiducialHeight")),
                                      "opticalFiducialHeight", "Optical System Fiducial Height");
    }
    
    _limits.addSubLimit(booleanLimit);
    _limits.addSubLimit(parametricLimit);
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void createResultsObject()
  {
    // Create container for the others    
    _zHeightInMilsToRestore = _config.getDoubleValue(PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS);
    _result = new Result(_NAME_OF_CAL);
    _booleanResult = new Result(new Boolean(true), getName());
    _parametricResult = new Result(new Double(_zHeightInMilsToRestore), getName());    
    _result.addSubResult(_booleanResult);
    _result.addSubResult(_parametricResult);
  }

  /**
   * @author Cheah Lee Herng
   */
  protected void setUp() throws XrayTesterException
  {
//    if(XrayActuator.isInstalled())
//    {
//      if (XrayCPMotorActuator.isInstalled() == true)
//      {
//        //move x-rays Z Axis to up position for low mag task.
//        XrayActuator.getInstance().up(true);
//      }
//    }
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);

    //bypass unnecessary task when in passive mode
    if (isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
  }

  /**
   * @author Cheah Lee Herng
   */
  private void storeImageForLater(ReconstructedImages reconstructedImage)
  {
    Assert.expect(reconstructedImage != null);
    
    {
      int zHeightInNanometers = reconstructedImage.getReconstructionRegion().getFocusGroups().get(0).getSlice(SliceNameEnum.PAD).getFocusInstruction().getZHeightInNanoMeters();
      float zHeightInMils = MathUtil.convertNanoMetersToMils(zHeightInNanometers);
      Image image = reconstructedImage.getReconstructedSlice(SliceNameEnum.PAD).getImage();      
      String filename = "img_" + zHeightInMils + ".jpg";
      try
      {
        XrayImageIoUtil.saveJpegImage(image.getBufferedImage(), _logDirectory + File.separator + filename);
      }
      catch (DatastoreException ex)
      {
        ex.printStackTrace();
      }
    }
  }

  /**
   * Handles interactions with ImageAcquisitionEngine for getting images.
   *
   * @return Boolean is true if images were returned as expected from IAE, false
   * otherwise.
   *
   * @author Eddie Williamson
   * @author John Dutton
   */
  private Boolean getReconstructedImagesFromIAE() throws XrayTesterException
  {
    BooleanRef noImagesReturned = new BooleanRef(false);
    
    try
    {      
      // Initialize counter variables
      int counter = 0;
      int totalRegionSize = _testProgram.getVerificationRegions().size();
            
      // Initialize sharpness info map
      if (_sharpnessInfoMap == null)
        _sharpnessInfoMap = new HashMap<Integer, Pair<Double, Double>>();
      
      // Clear for new run
      _sharpnessInfoMap.clear();
      
      // Get images off producer's queue (following typical use model)
      for(int i=0; i < _testProgram.getVerificationRegions().size(); ++i)
      {
        ReconstructedImages flushReconstructedImage = _iae.getReconstructedImages(noImagesReturned);
        if (noImagesReturned.getValue() == false)
        {
          ReconstructionRegion regionOfImages = flushReconstructedImage.getReconstructionRegion();
          flushReconstructedImage.decrementReferenceCount();
          _iae.freeReconstructedImages(regionOfImages);
        }
        else
          return Boolean.FALSE;
      }
      
      // Set system magnification prior to start collect images
      _iae.sendCalibratedMagnifications(Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_MAGNIFICATION), 
                                        Config.getInstance().getDoubleValue(SystemConfigEnum.REFERENCE_PLANE_HIGH_MAGNIFICATION));
      
      // Start acquire images
      for(ReconstructionRegion region : _testProgram.getVerificationRegions())
      {
        if (_iae.isAbortInProgress() == false)
        {
          _iae.reacquireImages(region);
          
          BooleanRef imagesReturned = new BooleanRef(true);
          ReconstructedImages reconstructedImage = _iae.waitForReReconstructedImages(region, imagesReturned);
          if (imagesReturned.getValue() == false)
          {
            return Boolean.FALSE;
          }
          storeImageForLater(reconstructedImage);
          
          // calculate sharpness here
          Image image = reconstructedImage.getReconstructedSlice(SliceNameEnum.PAD).getImage();
          
          if (isValidImage(image))
          {
            RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);
            
            if (_sharpnessInfoMap.containsKey(region.getRegionId()) == false)
            {
              double sharpness = measureSharpness(image, roi);
              int zHeightInNanometers = reconstructedImage.getReconstructionRegion().getFocusGroups().get(0).getSlice(SliceNameEnum.PAD).getFocusInstruction().getZHeightInNanoMeters();
              double zHeightInMils = MathUtil.convertNanoMetersToMils(zHeightInNanometers);
              
              _sharpnessInfoMap.put(region.getRegionId(), new Pair<Double, Double>(sharpness, zHeightInMils));                            
            }
          }
          else
          {
            // image is invalid, we need to fail the test
            // This will also break us out of the for loop
            //
            // DO NOT RETURN here. We need to clear out the images in the IAE
            // otherwise it will hang the test.
            _imagesWereValid = false;
            _allSharpnessValid = false;
          }
          
          reconstructedImage.decrementReferenceCount();          
          counter++;
          
          // Report progress bar
          reportProgress(20 + (int)(70 * (counter / totalRegionSize)));
        }
      }// End for loop through verification regions
      
      // Before going on we need to mark all RRs complete.
      // This frees memory and unblocks iae for the main thread
      Collection<ReconstructionRegion> reconstructionRegions = _testProgram.getVerificationRegions();
      for (ReconstructionRegion reconstructionRegion : reconstructionRegions)
      {
        if (_iae.isAbortInProgress() == false)
        {
          _iae.reconstructionRegionComplete(reconstructionRegion, true);
        }
      }
    }
    catch (XrayTesterException xte)
    {
      // Need to stop IAE to stop the GetCouponImagesTask.
      _iae.abort(xte);
    }    

    return Boolean.TRUE;
  }

  /**
   * @author Rex Shang
   */
  private class GetCouponImagesTask extends ThreadTask<Object>
  {
    public GetCouponImagesTask()
    {
      super("GetCouponImagesTask");
    }

    /**
     * @author Rex Shang
     */
    protected Boolean executeTask() throws XrayTesterException
    {
      Boolean gotImages = getReconstructedImagesFromIAE();
      return gotImages;
    }

    /**
     * @author Rex Shang
     */
    protected void clearCancel()
    {
      // Do nothing.
    }

    /**
     * @author Rex Shang
     */
    protected void cancel() throws XrayTesterException
    {
      // Do nothing.
    }
  }

  /**
   * @author Rex Shang
   */
  private class RequestCouponImagesTask extends ThreadTask<Object>
  {
    public RequestCouponImagesTask()
    {
      super("RequestCouponImagesTask");
    }

    /**
     * @author Rex Shang
     */
    protected Object executeTask() throws XrayTesterException
    {
      // Force half stepping of feature across the cameras. This maximizes the
      // error when the mag is not right which helps us find the right one.
      // Request an image from IAE.
      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();
      _iae.acquireCouponImages(_testProgram,
              testExecutionTimer,
              5080000,
              8890000,
              0.30);

      return null;
    }

    /**
     * @author Rex Shang
     */
    protected void clearCancel()
    {
      // Do nothing.
    }

    /**
     * @author Rex Shang
     */
    protected void cancel() throws XrayTesterException
    {
      // Do nothing.
    }
  }

  /**
   * @author Eddie Williamson
   */
  protected void executeTask() throws XrayTesterException
  {        
    if (UnitTest.unitTesting() == true && _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true)
    {
      //Create a passing result
      String localizedTaskName = getName();
      _limits = new Limit(null, null, null, localizedTaskName);
      _result = new Result(new Boolean(true), localizedTaskName);
      return;
    }        

    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_OPTICAL_SYSTEM_OFFSET);
    _hardwareObservable.setEnabled(false);
    reportProgress(0);
    
    _imagesWereValid = true;
    _allSharpnessValid = true;

    try
    {
      // XCR-3314 Support PSP New Manual-Loaded Jig Plate
      if (PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_1) == false && 
          PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4) == false)
      {
        // For On-Stage calibration jig, we should not have any panel loaded.
        if (ableToRunWithCurrentPanelState() == false)
        {
          _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
          _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_PANEL_INTEFERRED_WITH_LIGHT_CONFIRM_KEY"));
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_IMAGE_QUALITY_REMOVE_THE_PANEL_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
          return;
        }

        // Open adjustable rail
        PanelHandler.getInstance().setRailWidthInNanoMeters(PspConfiguration.getInstance().getOpticalSystemOffsetAdjustmentRailWidthInNanometers());
      }
      else
      {
        loadOpticalCalibrationJig();
        //Handle the fact that we cannot run this task without a calibration
        //jig loaded into the system. Defer the task till later *but* don't cause the
        //task to fail. Display a meaningful message in the Service UI
        if (ableToRunWithCurrentPanelState() == true)
        {
          _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
          _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_NO_CALIBRATION_JIG_LOADED_KEY"));
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_LOAD_CALIBRATION_JIG_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
          return;
        }
      }
      
      // Setup TestProgram
      ImageRetriever ir = ImageRetriever.getInstance();      
      if (XrayTester.isXXLBased())
        _testProgram = ir.getTestProgram(ImageRetrieverTestProgramEnum.PSP_XXL_COUPON_IMAGE);  
      else if(XrayTester.isS2EXEnabled())
        _testProgram = ir.getTestProgram(ImageRetrieverTestProgramEnum.PSP_S2EX_COUPON_IMAGE);  
      else
        _testProgram = ir.getTestProgram(ImageRetrieverTestProgramEnum.PSP_COUPON_IMAGE);
      
      _iae = ImageAcquisitionEngine.getInstance();
      reportProgress(20);
      
      // Start a threaded task that returns an image from the IAE.
      _iae.initialize();
      
      ThreadTask<Object> getImagesTask = new GetCouponImagesTask();
      ThreadTask<Object> requestImagesTask = new RequestCouponImagesTask();

      _heightCalParallelThread.submitThreadTask(getImagesTask);
      _heightCalParallelThread.submitThreadTask(requestImagesTask);
      
      // Cleanup threaded task.
      try
      {
        _heightCalParallelThread.waitForTasksToComplete();

        Boolean gotImages = (Boolean) getImagesTask.get();
        if (gotImages == null || gotImages.equals(Boolean.FALSE))
        {
          // Image wasn't returned from IAE.  Instead of forcing caller to deal with null return
          // values throw an exception.
          ImageRetrieverBusinessException irException = new ImageRetrieverBusinessException();
          throw irException;
        }
      }
      catch (XrayTesterException xte)
      {
        reportProgress(100);
        _result.setTaskStatusMessage(xte.getLocalizedMessageWithoutHeaderAndFooter());

        // The re-throw is needed to help the "catch (Exception e)" below.
        throw xte;
      }
      catch (Throwable t)
      {
        Assert.logException(t);
      }
      
      _booleanResult.setResults(_imagesWereValid);
      if (_imagesWereValid)
      {
        // Successfully retrieved and analyzed all images. 
        // Now find the sharpness image with highest sharpness value
        double maxSharpness = 0.0;
        double zHeightInMils = 0.0; 
        
        for(Map.Entry<Integer, Pair<Double, Double>> entry : _sharpnessInfoMap.entrySet())
        {
          Pair<Double, Double> sharpnessInfo = entry.getValue();          
          double currentSharpness = sharpnessInfo.getFirst().doubleValue();
          double currentZHeightInMils = sharpnessInfo.getSecond().floatValue();
          if (currentSharpness > maxSharpness)
          {            
            maxSharpness = currentSharpness;
            zHeightInMils = currentZHeightInMils;
          }
          if (currentSharpness <= 0)
          {
            _allSharpnessValid = false;
          }
        }
        
        // Set the final z-height result
        if (_allSharpnessValid)
          _parametricResult.setResults(new Double(zHeightInMils));
        
        if (_limits.didTestPass(_result)&& _allSharpnessValid)
        {
          _zHeightInMilsToRestore = zHeightInMils;          
        }
        else
        {          
          //No matter what we will log with parameters, so set this up before moving on
          Object[] parameters = new Object[]
          {
            zHeightInMils
          };

          // Failing result
          _result.setTaskStatusMessage(StringLocalizer.keyToString(new LocalizedString("ACD_OPTICAL_SYSTEM_OFFSET_OUT_OF_EXPECTED_RANGE_KEY", parameters)));
          _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("ACD_OPTICAL_SYSTEM_OFFSET_FAILED_VALUE_OUT_OF_RANGE_SUGGESTED_ACTION_KEY"));          
        }
      }
      else
      {        
        _result.setTaskStatusMessage(StringLocalizer.keyToString("ACD_MISSING_SYSTEM_FEATURE_KEY"));
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("ACD_OPTICAL_SYSTEM_OFFSET_FAILED_FEATURE_NOT_FOUND_SUGGESTED_ACTION_KEY"));
      }
      
      logInformation();
    }
    catch (XrayTesterException xte)
    {
      reportProgress(100);
      _result.setTaskStatusMessage(xte.getLocalizedMessageWithoutHeaderAndFooter());

      // The re-throw is needed to help the "catch (Exception e)" below.
      throw xte;
    }
    catch (Throwable t)
    {
      Assert.logException(t);
    }
    finally
    {
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
      XrayCameraArray.getInstance().setUserDefinedCamerasLight(UserGainEnum.ONE.toDouble());
      try
      {
        _config.setValue(PspCalibEnum.CAL_OPTICAL_SYSTEM_FIDUCIAL_ZHEIGHT_IN_MILS, _zHeightInMilsToRestore);
      }
      finally
      {
        _hardwareObservable.setEnabled(true);
      }
      
      // XCR-3314 Support PSP New Manual-Loaded Jig Plate
      if (PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_1) ||
          PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_4))
      {
        // Restore panel safety checking anyhow
        PanelHandler.getInstance().unloadPanel();
      }
      
      reportProgress(100);      
    }

    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_OPTICAL_SYSTEM_OFFSET);
  }

  /**
   * @author Anthony Fong
   */
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return (_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**   
   * @param image
   * @return 
   */
  private boolean isValidImage(Image image)
  {
    Assert.expect(image != null);
    boolean valid;

    // Search the entire image
    RegionOfInterest roi = RegionOfInterest.createRegionFromImage(image);
    
    // Use profile to average out noise
    float[] profile = ImageFeatureExtraction.profile(image, roi);

    // Image must go from dark to light moving from top to bottom
    // This is typically a difference of 60 graylevels, we'll use 30 for the limit    
//    if ((profile[0] - profile[(image.getWidth() * image.getHeight()) - 1]) > 30)
//    {
//      valid = true;
//    }
//    else
//    {
//      valid = false;
//    }
    
    valid = true;

    return valid;
  }
  
  /**
   * Measure sharpness of the vertical edge in an image.
   * Uses Tracy's idea of using the standard deviation of the image after
   * using the sobel edge filter.
   * @author Eddie Williamson
   */
  private double measureSharpness(Image image, RegionOfInterest roi)
  {
    Assert.expect(image != null);
    Assert.expect(roi != null);

    DoubleRef mean = new DoubleRef();
    DoubleRef stdev = new DoubleRef();

    Image sobelImage = Filter.convolveSobelVertical(image, roi);
    Statistics.meanStdDev(sobelImage, roi, mean, stdev);
    sobelImage.decrementReferenceCount();

    return stdev.getValue();
  }
  
  /**   
   * @throws DatastoreException 
   */
  private void logInformation() throws DatastoreException
  {
    StringBuilder logString = new StringBuilder();

    // Create header lines
    logString.append(_NAME_OF_CAL + "\n,\n");

    if (_limits.didTestPass(_result)&& _allSharpnessValid)      
      logString.append(",Offset z-Height (mils) = " + _zHeightInMilsToRestore + "\n,\n");
    else
      logString.append(",Offset z-Height (mils) = N/A\n,\n");
    
    logString.append(",Region Id vs Z-Height vs Sharpness\n");
    logString.append(",,Region Id,Z-Height (mils),Sharpness\n");
    
    for(Map.Entry<Integer, Pair<Double, Double>> entry : _sharpnessInfoMap.entrySet())
    {
      Pair<Double, Double> sharpnessInfo = entry.getValue();          
      double currentSharpness = sharpnessInfo.getFirst().doubleValue();
      double currentZHeightInMils = sharpnessInfo.getSecond().floatValue();
      
      logString.append(",," + entry.getKey().intValue() + "," + currentZHeightInMils + "," + currentSharpness + "\n");
    }

    _logger.log(logString.toString());
    logString = null;
  }
  
  /**
    * @author Cheah Lee Herng 
    */
   protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
   {
     // No panel must be present in the system.
     return (PanelHandler.getInstance().isPanelLoaded() == false);
   }
   
   /**
     * @author Cheah Lee Herng
     */
    protected void loadOpticalCalibrationJig() throws XrayTesterException
    {
      if (PanelHandler.getInstance().isPanelLoaded() == false)
      {
        if (XrayActuator.isInstalled())
        {
          if (XrayCylinderActuator.isInstalled() == true)
          {
            // Always move the cylinder to UP position
            XrayActuator.getInstance().up(false);
          }
          else if (XrayCPMotorActuator.isInstalled() == true)
          {
            // Always move the Z Axis to Home and Safe position
            XrayActuator.getInstance().home(false,false);
          }
        }
        // comment by Wei Chin
        // should not check the safety sensor (due to it don't lower down the xray tube)
        // Please be aware if it need to run this board in high mag! 
        PanelHandler.getInstance().loadPanel("Panel for Service Gui", 
                                             PspConfiguration.getInstance().getOpticalCalibrationJigWidthInNanometers(),
                                             PspConfiguration.getInstance().getOpticalCalibrationJigHeightInNanometers(),
                                             false, 0);
      }
    }
}
