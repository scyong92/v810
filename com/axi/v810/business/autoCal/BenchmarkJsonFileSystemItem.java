package com.axi.v810.business.autoCal;

import java.util.Date;
import java.util.List;

/**
 * @author Anthony Fong
 */
public class BenchmarkJsonFileSystemItem
{

  private String name;
  private Date creationDate;
  private Boolean hidden;
  private List<Long> timingDatas;

  /**
   * @author Anthony Fong
   */
  public String getName()
  {
    return name;
  }

  /**
   * @author Anthony Fong
   */
  public void setName(String name)
  {
    this.name = name;
  }

  /**
   * @author Anthony Fong
   */
  public Date getCreationDate()
  {
    return creationDate;
  }

  /**
   * @author Anthony Fong
   */
  public void setCreationDate(Date creationDate)
  {
    this.creationDate = creationDate;
  }

  /**
   * @author Anthony Fong
   */
  public Boolean isHidden()
  {
    return hidden;
  }

  /**
   * @author Anthony Fong
   */
  public List<Long> getTimingDatas()
  {
    return timingDatas;
  }

  /**
   * @author Anthony Fong
   */
  public void setHidden(Boolean hidden)
  {
    this.hidden = hidden;
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFileSystemItem(String name, Date creationDate, Boolean hidden, List<Long> timingDatas)
  {
    this.name = name;
    this.creationDate = creationDate;
    this.hidden = hidden;
    this.timingDatas = timingDatas;
  }
}
