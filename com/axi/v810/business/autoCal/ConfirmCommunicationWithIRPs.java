package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Task to Confirm that communications on the X6000 system are functioning as
 * expected. Ping the IRP's and make sure they respond
 *
 * @author Reid Hayhow
 */
public class ConfirmCommunicationWithIRPs extends ConfirmCommunicationTask
{
  private static String _NAME =
      StringLocalizer.keyToString("CD_CONFIRM_IRP_COMMUNICATIONS_KEY");

  private static final int _FREQUENCY = 1;
  private static final long _TIMEOUTINSECONDS = 900;

  private static ConfirmCommunicationWithIRPs _instance = null;
  private static final int _IRP_NUMBERING_OFFSET = 1;

  // Member variables for reporting progress
  private ProgressObservable _progressObservable = ProgressObservable.getInstance();
  private ProgressReporterEnum _progressReportEnum = ProgressReporterEnum.CONFIRM_IRP_COMMUNICATION;

  private static int _numberOfImageReconstructionNetworkCards = -1;
  /**
   * Constructor, creates a logger for this confirmation
   *
   * @author Reid Hayhow
   */
  private ConfirmCommunicationWithIRPs()
  {
    super(_NAME, _FREQUENCY, _TIMEOUTINSECONDS, ProgressReporterEnum.CONFIRM_IRP_COMMUNICATION);

    //Set enum used to save timestamp for this task
    setTimestampEnum(HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_IRPS_LAST_TIME_RUN);
  }

  /**
   * Instance method for this task
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCommunicationWithIRPs getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCommunicationWithIRPs();
    }
    return _instance;
  }

  /**
   * This task communicates with the IRP's, Cameras and switch to make sure they
   * are on the network and are responsive.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    //Purge the error buffer before starting
    _errorBuffer = new StringBuilder();

    // Report that this task is starting
    _progressObservable.reportProgress(_progressReportEnum, 0);

    // Get the number of network card
    _numberOfImageReconstructionNetworkCards = _config.getIntValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_PROCESSOR_NETWORK_CARDS);
    
    List<String> listOfIRPIpAddresses = new ArrayList<String>();

    // Chnee Khang Wah, 2011-12-06, Low Cost AXI
    int numberOfIRP = _config.getIntValue(HardwareConfigEnum.NUMBER_OF_IMAGE_RECONSTRUCTION_PROCESSORS);
    
    if (numberOfIRP>0) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
        listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_1));
    if (numberOfIRP>1) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
        listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_1));
    if (numberOfIRP>2) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
        listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_1));
    if (numberOfIRP>3) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
        listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_1));

    //Ping the list of IRP's, pass in the string indicating the type of device
    //and pass in the offset for IRP numbering, in this case one because
    //IRP's are numbered 1-4
    pingAddressesAndLogResults(listOfIRPIpAddresses,
                               StringLocalizer.keyToString("ACD_ETHERNET_CARD_KEY") + " 1, " +
                               StringLocalizer.keyToString("ACD_IMAGE_RECONSTRUCTION_PROCESSOR_KEY"),
                               _IRP_NUMBERING_OFFSET);
    
    if (_numberOfImageReconstructionNetworkCards == 2) // Bee Hoon, Single IRP Testing
    {
      // OK, at this point we have pinged half of the IRP's, so report our progress
      _progressObservable.reportProgress(_progressReportEnum, 50);
    
      //Clear the list of devices before pinging the next set of IRP's so we don't
      //get duplicate data
      listOfIRPIpAddresses.clear();

      if (numberOfIRP>0) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
          listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_0_IP_ADDRESS_2));
      if (numberOfIRP>1) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
          listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_1_IP_ADDRESS_2));
      if (numberOfIRP>2) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
          listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_2_IP_ADDRESS_2));
      if (numberOfIRP>3) // Chnee Khang Wah, 2011-11-29, Low Cost AXI
          listOfIRPIpAddresses.add(_config.getStringValue(HardwareConfigEnum.IMAGE_RECONSTRUCTION_PROCESSOR_3_IP_ADDRESS_2));
   
      //Ping the list of IRP's, pass in the string indicating the type of device
      //and pass in the offset for IRP numbering, in this case one because
      //IRP's are numbered 1-4
      pingAddressesAndLogResults(listOfIRPIpAddresses,
              StringLocalizer.keyToString("ACD_ETHERNET_CARD_KEY") + " 2, "
              + StringLocalizer.keyToString("ACD_IMAGE_RECONSTRUCTION_PROCESSOR_KEY"),
              _IRP_NUMBERING_OFFSET);
    }

    logResults();

    // Report that we are done with this task
    _progressObservable.reportProgress(_progressReportEnum, 100);
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
