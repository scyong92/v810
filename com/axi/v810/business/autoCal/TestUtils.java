package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * Regression Test helper class for ACD. This class is not delivered as part of
 * the customer jar, so be sure that only unit test stuff id done in here.
 *
 * @author Reid Hayhow
 */
public class TestUtils
{
  private File regressionTestLimits;
  private File customerLimits;
  private File backupLimits;
  private File softwareConfig;
  private File origSoftwareConfig;
  private File hardwareConfig;
  private File origHardwareConfig;
  private File hardwareCalib;
  private File origHardwareCalib;

  private static Config _config = Config.getInstance();

  private static final String _backupHwConfigFileName = Directory.getCalibDir() + "backupHwConfigFile";
  private static final String _backupHwCalibFileName = Directory.getCalibDir() + "backupHwCalibFile";


  private StringBuffer _stringBuff = new StringBuffer();

  /**
   * Method to copy a 'good' set of limits for use during regression tests. It also
   * preserves the original limits so they can be restored later.
   *
   * @author Reid Hayhow
   */
  void copyLimitsForRegressionTests(String testSourceDir)
  {
    //Set up the files for the limits file, regression test limits file and backup
    regressionTestLimits = new File(testSourceDir + "autotest/data/CustomerLimits.properties");
    customerLimits = new File(testSourceDir +
                              "../../../../../classes/com/axi/xraytest/properties/CustomerLimits.properties");
    backupLimits = new File(testSourceDir + "autotest/data/CustomerLimits.properties.backup");

    //The regression test limits file and the customer limits file MUST exist
    Assert.expect(regressionTestLimits.exists());
    Assert.expect(customerLimits.exists());

    try
    {
      //make a backup of the customer limits
      FileUtilAxi.copy(customerLimits.getPath(), backupLimits.getPath());

      //Remove the customer limits file
      customerLimits.delete();

      //Copy the regression test limits to the customer limits
      FileUtilAxi.copy(regressionTestLimits.getPath(), customerLimits.getPath());
    }
    catch (DatastoreException ex)
    {
      //any problems cause an assert
      Assert.expect(false);
    }
  }

  /**
   * Method to restore the limits after running a regression tests
   *
   * @author Reid Hayhow
   */
  void restoreLimitsAfterRegressionTests()
  {
    //Part of the contract to use this method is that you have to have
    //used the regression test limits before, thus initializing these files
    Assert.expect(customerLimits != null);
    Assert.expect(backupLimits != null);

    //Additionally, the backup file must also exist!
    Assert.expect(backupLimits.exists());

    try
    {
      //Copy the backup customer limits back to the customer limits location
      FileUtilAxi.copy(backupLimits.getPath(), customerLimits.getPath());

      //Tidy up by deleting the backup
      backupLimits.delete();
    }
    catch (DatastoreException ex)
    {
      Assert.expect(false);
    }
  }



  /**
   * Wrapper method to backup the SW config
   *
   * @author Reid Hayhow
   */
  void backupSoftwareConfig(String testSourceDir)
  {
    copyFileForRegressionTests(testSourceDir, "config", "software.config", softwareConfig, origSoftwareConfig);
  }


  /**
   * Wrapper method to restore the SW config
   *
   * @author Reid Hayhow
   */
  void restoreSoftwareConfig(String testSourceDir)
  {
    restoreFileAfterRegressionTests(testSourceDir, "config", "software.config", origSoftwareConfig, softwareConfig);
  }

  /**
   * Wrapper method to backup the HW config
   *
   * @author Reid Hayhow
   */
  public void backupHWConfig(String testSourceDir)
  {
    copyFileForRegressionTests(testSourceDir, "config", "hardware.config", hardwareConfig, origHardwareConfig);
  }


  /**
   * Wrapper method to restore the HW config
   *
   * @author Reid Hayhow
   */
  public void restoreHWConfig(String testSourceDir)
  {
    restoreFileAfterRegressionTests(testSourceDir, "config", "hardware.config", origHardwareConfig, hardwareConfig);
  }

  /**
   * Wrapper method to backup the HW calib
   *
   * @author Reid Hayhow
   */
  public void backupHWCalib(String testSourceDir)
  {
    String calibFilename = FileName.getHardwareCalibFile();
    String binaryExtension = FileName.getBinaryExtension();
    String fullCalibFilename = calibFilename + binaryExtension;
    copyFileForRegressionTests(testSourceDir, "calib", fullCalibFilename, hardwareCalib, origHardwareCalib);
  }


  /**
   * Wrapper method to restore the HW calib
   *
   * @author Reid Hayhow
   */
  public void restoreHWCalib(String testSourceDir)
  {
    restoreFileAfterRegressionTests(testSourceDir, "calib", FileName.getHardwareCalibFile() + FileName.getBinaryExtension(), origHardwareCalib, hardwareCalib);
  }

  /**
   * Method to copy a file that may be modified during regression tests.
   *
   * @author Reid Hayhow
   */
  private void copyFileForRegressionTests(String testSourceDir, String subPath, String fileName, File fileThatWillBeModified,
                                          File fileToBeBackedUp)
  {
    //Set up the files for the limits file, regression test limits file and backup
    fileThatWillBeModified = new File(testSourceDir +
                                      "../../../../../../testRel/unitTest/x.xx/" + subPath + "/" + fileName);
    fileToBeBackedUp = new File(testSourceDir + "autotest/data/" + fileName + ".backup");

    //The regression test limits file and the customer limits file MUST exist
    Assert.expect(fileThatWillBeModified.exists());

    try
    {
      //make a backup of the customer limits
      FileUtilAxi.copy(fileThatWillBeModified.getPath(), fileToBeBackedUp.getPath());
    }
    catch (DatastoreException ex)
    {
      //any problems cause an assert
      Assert.expect(false);
    }
  }


  /**
   * Method to restore a backed up file after the regression test has run
   *
   * @author Reid Hayhow
   */
  private void restoreFileAfterRegressionTests(String testSourceDir, String subPath,String fileName, File fileThatWasBackedUp,
                                       File fileThatWasModified)
  {
    //Set up the files for the limits file, regression test limits file and backup
    fileThatWasModified = new File(testSourceDir +
                                   "../../../../../../testRel/unitTest/x.xx/" + subPath + "/" + fileName);
    fileThatWasBackedUp = new File(testSourceDir + "autotest/data/" + fileName + ".backup");

    //Part of the contract to use this method is that you have to have
    //used the regression test limits before, thus initializing these files
    Assert.expect(fileThatWasBackedUp != null);
    Assert.expect(fileThatWasModified != null);

    //Additionally, the backup file must also exist!
    Assert.expect(fileThatWasBackedUp.exists());

    try
    {
      //Copy the backup customer limits back to the customer limits location
      FileUtilAxi.copy(fileThatWasBackedUp.getPath(), fileThatWasModified.getPath());

      //Tidy up by deleting the backup
      fileThatWasBackedUp.delete();
    }
    catch (DatastoreException ex)
    {
      Assert.expect(false);
    }
  }

  /**
   * Convienence function to display any failure information.
   *
   * @author Reid Hayhow
   */
  public void displayFailureData(Result result, Limit limits, PrintWriter os)
  {
    List<Result> resultList = result.getResultList();
    List<Limit> limitsList = limits.getLimitList();
    Iterator<Limit> iter = limitsList.iterator();
    Assert.expect(resultList != null);
    if (resultList.isEmpty())
    {
      if (limits.didTestPass(result) == false)
      {
        os.println(_stringBuff.toString() + " " + limits.getLimitName() + " FAILED!");
      }
    }
    else
    {
      for (Result subResult : resultList)
      {
        String tempStringHolder = new String(_stringBuff.toString());
        if (subResult.getStatus().equals(HardwareTaskStatusEnum.PASSED) == false)
        {
          _stringBuff.append(subResult.getResultName() + ", ");
        }
        displayFailureData(subResult, iter.next(), os);
        _stringBuff.setLength(0);
        _stringBuff.append(tempStringHolder);
      }
    }
  }

  /**
   * Function to assure that the date and status have been set properly
   *
   * @author Reid Hayhow
   */
  public void checkForStartStopDatesAndStatus(Result result, boolean failingIsOk)
  {
    List<Result> resultList = result.getResultList();

    //We are at a leaf node, check it
    if (resultList.isEmpty())
    {
      if (!failingIsOk)
      {
        String passedString = StringLocalizer.keyToString("CD_PASSED_KEY");
        Expect.expect(result.getStatusString().equalsIgnoreCase(passedString));
      }
      Expect.expect(result.getStartDate() != null);
      Expect.expect(result.getStopDate() != null);
    }
    else
    {
      for (Result subResult : resultList)
      {
        checkForStartStopDatesAndStatus(subResult, failingIsOk);
      }
    }
  }

  /**
   * Regression tests depend on test images that were created for a specific hardware
   * config setup. This backs up the hardware.config file and then modifies it with
   * fixed parameters for testing.
   * @author Eddie Williamson
   */
  public static void initConfigForRegressionTest() throws XrayTesterException
  {
    try
    {
      // save backup copy of hardware.config
      FileUtil.copy(FileName.getHardwareConfigFullPath(), _backupHwConfigFileName);
      // save backup copy of hardware.calib
      FileUtil.copy(FileName.getHardwareCalibFullPath() + FileName.getBinaryExtension(), _backupHwCalibFileName);

      _config.loadIfNecessary();
      // Set specific parameters for regression testing
      //   - Camera locations
      //   - Xray Spot location
      Map<ConfigEnum, Object> keyValueMap = new HashMap<ConfigEnum, Object>();

      // hardware.calib parameters
      keyValueMap.put(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, 30403603);
      keyValueMap.put(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS, 530384100);

      keyValueMap.put(HardwareCalibEnum.ADDITIONAL_PROJECTION_WIDTH_NANOMETERS, 0);


      _config.setValues(keyValueMap);
      _config.loadIfNecessary();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      Expect.expect(false);
    }
 }


  /**
   * Restores the original hardware.config file so we don't leave a mess
   * after regression tests.
   * @author Eddie Williamson
   */
  public static void restoreConfigAfterRegressionTest()
  {
    try
    {
      // restore original hardware.config
      FileUtil.copy(_backupHwConfigFileName, FileName.getHardwareConfigFullPath());
      // restore original hardware.calib
      FileUtil.copy(_backupHwCalibFileName, FileName.getHardwareCalibFullPath() + FileName.getBinaryExtension());
    }
    catch (CouldNotCopyFileException ex1)
    {
      ex1.printStackTrace();
      Expect.expect(false, "Could not copy backup file to config.");
    }
    catch (FileDoesNotExistException ex1)
    {
      ex1.printStackTrace();
      Expect.expect(false, "Backup file does not exist for copy.");
    }
  }

}
