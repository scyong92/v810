package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.util.*;

/**
 * @author Yong Sheng Chuan
 */
public class PanelInterferesWithAdjustmentAndConfirmationTaskException extends XrayTesterException
{
  /**
   * @author Yong Sheng Chuan
   */
  public PanelInterferesWithAdjustmentAndConfirmationTaskException(String taskName)
  {
    super(new LocalizedString("CD_EXCEPTION_PANEL_INTERFERES_WITH_ADJUSTMENT_AND_CONFIRMATION_KEY", new Object[]{taskName}));
  }

}
