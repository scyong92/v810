package com.axi.v810.business.autoCal;

/**
 * Enumerate the calibration procedures that the camera supports.
 * @author Eddie Williamson
 */
public class XrayCameraCalibEnum
{
  private static int _index = 0;
  public static final XrayCameraCalibEnum DO_DARK_SEGMENT_CAL  = new XrayCameraCalibEnum(_index++, "DarkSegment");
  public static final XrayCameraCalibEnum DO_LIGHT_SEGMENT_CAL = new XrayCameraCalibEnum(_index++, "LightSegment");
  public static final XrayCameraCalibEnum DO_DARK_PIXEL_CAL    = new XrayCameraCalibEnum(_index++, "DarkPixel");
  public static final XrayCameraCalibEnum DO_LIGHT_PIXEL_CAL   = new XrayCameraCalibEnum(_index++, "LightPixel");

  //Variable Mag Anthony August 2011
  public static final XrayCameraCalibEnum SAVE_VARIABLE_MAG_LOW_CAL    = new XrayCameraCalibEnum(_index++, "SaveVariableMagLowCal");
  public static final XrayCameraCalibEnum LOAD_VARIABLE_MAG_LOW_CAL   = new XrayCameraCalibEnum(_index++, "LoadVariableMagLowCal");
  public static final XrayCameraCalibEnum SAVE_VARIABLE_MAG_HIGH_CAL    = new XrayCameraCalibEnum(_index++, "SaveVariableMagHighCal");
  public static final XrayCameraCalibEnum LOAD_VARIABLE_MAG_HIGH_CAL   = new XrayCameraCalibEnum(_index++, "LoadVariableMagHighCal");


  private int _calType;
  private String _name;

  XrayCameraCalibEnum(int index, String name)
  {
    _calType = index;
    _name = name;
  }

  public int getCalType()
  {
    return _calType;
  }

  public String getName()
  {
    return _name;
  }

  public String toString()
  {
    return _name;
  }
}
