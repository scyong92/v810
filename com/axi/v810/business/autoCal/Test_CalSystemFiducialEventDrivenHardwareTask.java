package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
public class Test_CalSystemFiducialEventDrivenHardwareTask extends UnitTest
{
  private Config _config = Config.getInstance();

  // These are the expected offsets from the original to the new locations.
  // These have been validated to be correct for the 8 test images used.
  private static final int CAMERA_0_EXPECTED_X = 4728256;
  private static final int CAMERA_0_EXPECTED_Y = -1115104;
  private static final int CAMERA_0_EXPECTED_COL = 301;
  private static final int CAMERA_0_EXPECTED_ROW = 69;

  private static final int CAMERA_1_EXPECTED_X = -2855168;
  private static final int CAMERA_1_EXPECTED_Y = 57152;
  private static final int CAMERA_1_EXPECTED_COL = -125;
  private static final int CAMERA_1_EXPECTED_ROW = 62;

  private static final int CAMERA_2_EXPECTED_X = 4744512;
  private static final int CAMERA_2_EXPECTED_Y = -2987456;
  private static final int CAMERA_2_EXPECTED_COL = 7;
  private static final int CAMERA_2_EXPECTED_ROW = 52;

  private static final int CAMERA_3_EXPECTED_X = 1765600;
  private static final int CAMERA_3_EXPECTED_Y = 8820352;
  private static final int CAMERA_3_EXPECTED_COL = 56;
  private static final int CAMERA_3_EXPECTED_ROW = 47;

  private static final int CAMERA_4_EXPECTED_X = 22939040;
  private static final int CAMERA_4_EXPECTED_Y = -10934208;
  private static final int CAMERA_4_EXPECTED_COL = 488;
  private static final int CAMERA_4_EXPECTED_ROW = 59;

  private static final int CAMERA_5_EXPECTED_X = -2855168;
  private static final int CAMERA_5_EXPECTED_Y = 8826720;
  private static final int CAMERA_5_EXPECTED_COL = 260;
  private static final int CAMERA_5_EXPECTED_ROW = 51;

  private static final int CAMERA_6_EXPECTED_X = -2240;
  private static final int CAMERA_6_EXPECTED_Y = 4972256;
  private static final int CAMERA_6_EXPECTED_COL = 424;
  private static final int CAMERA_6_EXPECTED_ROW = 59;

  private static final int CAMERA_7_EXPECTED_X = 7174784;
  private static final int CAMERA_7_EXPECTED_Y = 16675072;
  private static final int CAMERA_7_EXPECTED_COL = -137;
  private static final int CAMERA_7_EXPECTED_ROW = 65;


  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    /** @todo PE  Whoever owns the camera firmware should try to get this regression test fixed when time allows.  We
     * had to disable it because of lack of resources to fix it, but according to Reid Hayhow, it is an important
     * regression test.  To work on it again, just remove re-enable the code below that I've commented out.
     *
     * Here is some information from emails discussing this test:
     *
     * Reid:  Currently this is a false alarm. There is a problem with the camera simulator that is used to serve up the
     * regression test images. There is a race condition in the simulator that makes the image order non-deterministic.
     * The problem started on the Shading Branch camera simulator and apparently got merged down to the main branch.
     * I worry about just disabling this regression test unless there is a firm date that someone will come back and
     * fix it. The Calibration subsystem is critical for x6000 performance and it has passed through so many hands
     * in the last two months that I think we need the regression test safety net in place as soon as possible.
     *
     * Reid:  Just a bit of history on this. Eric Littlefield and I worked on this before his departure to ICT.
     * We got every branch passing except for Shading. Our hope was that when Shading got merged into Main, the failure
     * would go away (because the defect in the camera simulator would be merged out). This is why we didn't opt to
     * disable the failing regression test months ago. Now that we have merged Shading to Main and the failure is still
     * there, I think it is time to fix it. The right person to fix it is whomever currently owns the Camera firmware
     * code (Erick Lewark probably). The Camera simulator code is based on the real camera code, so the change will
     * need to be made there.
     *

    try
    {
      Assert.setLogFile("tempLog", "tempVersion");
      TestUtils.initConfigForRegressionTest();
      UnitTest.execute(new Test_CalSystemFiducialEventDrivenHardwareTask());
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
    }

  */
  }

  /**
   * @author Roy Williams
   */
  protected Test_CalSystemFiducialEventDrivenHardwareTask()
  {
    super();
  }

  /**
   * @author Eddie Williamson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XrayCameraArray xRayCameraArray = null;

    try
    {
      _config.loadIfNecessary();

      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getSystemFiducialDir(); //Directory.getStageHysteresisDir(); //Directory.getSystemFiducialDir();
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);
      CalGrayscale.getInstance().executeUnconditionally();

      // get "before" values
      int beforeCam0StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS);
      int beforeCam0StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam0Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN);
      int beforeCam0Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW);

      int beforeCam1StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS);
      int beforeCam1StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam1Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN);
      int beforeCam1Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW);

      int beforeCam2StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS);
      int beforeCam2StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam2Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN);
      int beforeCam2Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW);

      int beforeCam3StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS);
      int beforeCam3StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam3Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN);
      int beforeCam3Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW);

      int beforeCam4StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS);
      int beforeCam4StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam4Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN);
      int beforeCam4Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW);

      int beforeCam5StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS);
      int beforeCam5StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam5Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN);
      int beforeCam5Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW);

      int beforeCam6StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS);
      int beforeCam6StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam6Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN);
      int beforeCam6Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW);

      int beforeCam7StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS);
      int beforeCam7StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS);
      int beforeCam7Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN);
      int beforeCam7Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW);

      CalSystemFiducialEventDrivenHardwareTask cal = CalSystemFiducialEventDrivenHardwareTask.getInstance();
      cal.executeNonInteractively();
      Expect.expect(cal.didTestPass());

      // get "after" values
      int afterCam1StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS);
      int afterCam1StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS);
      int afterCam1Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN);
      int afterCam1Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW);

      int afterCam2StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS);
      int afterCam2StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS);
      int afterCam2Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN);
      int afterCam2Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW);

      int afterCam3StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS);
      int afterCam3StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS);
      int afterCam3Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN);
      int afterCam3Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW);

      int afterCam4StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS);
      int afterCam4StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS);
      int afterCam4Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN);
      int afterCam4Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW);

      int afterCam5StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS);
      int afterCam5StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS);
      int afterCam5Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN);
      int afterCam5Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW);

      int afterCam6StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS);
      int afterCam6StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS);
      int afterCam6Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN);
      int afterCam6Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW);

      int afterCam7StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS);
      int afterCam7StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS);
      int afterCam7Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN);
      int afterCam7Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW);

      int afterCam14StageX = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS);
      int afterCam14StageY = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS);
      int afterCam14Column = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN);
      int afterCam14Row = _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW);

      int diffCam0StageX = (afterCam14StageX - beforeCam0StageX);
      int diffCam0StageY = (afterCam14StageY - beforeCam0StageY);
      int diffCam0Col = (afterCam14Column - beforeCam0Column);
      int diffCam0Row = (afterCam14Row - beforeCam0Row);

      int diffCam1StageX = (afterCam1StageX - beforeCam1StageX);
      int diffCam1StageY = (afterCam1StageY - beforeCam1StageY);
      int diffCam1Col = (afterCam1Column - beforeCam1Column);
      int diffCam1Row = (afterCam1Row - beforeCam1Row);

      int diffCam2StageX = (afterCam2StageX - beforeCam2StageX);
      int diffCam2StageY = (afterCam2StageY - beforeCam2StageY);
      int diffCam2Col = (afterCam2Column - beforeCam2Column);
      int diffCam2Row = (afterCam2Row - beforeCam2Row);

      int diffCam3StageX = (afterCam3StageX - beforeCam3StageX);
      int diffCam3StageY = (afterCam3StageY - beforeCam3StageY);
      int diffCam3Col = (afterCam3Column - beforeCam3Column);
      int diffCam3Row = (afterCam3Row - beforeCam3Row);

      int diffCam4StageX = (afterCam4StageX - beforeCam4StageX);
      int diffCam4StageY = (afterCam4StageY - beforeCam4StageY);
      int diffCam4Col = (afterCam4Column - beforeCam4Column);
      int diffCam4Row = (afterCam4Row - beforeCam4Row);

      int diffCam5StageX = (afterCam5StageX - beforeCam5StageX);
      int diffCam5StageY = (afterCam5StageY - beforeCam5StageY);
      int diffCam5Col = (afterCam5Column - beforeCam5Column);
      int diffCam5Row = (afterCam5Row - beforeCam5Row);

      int diffCam6StageX = (afterCam6StageX - beforeCam6StageX);
      int diffCam6StageY = (afterCam6StageY - beforeCam6StageY);
      int diffCam6Col = (afterCam6Column - beforeCam6Column);
      int diffCam6Row = (afterCam6Row - beforeCam6Row);

      int diffCam7StageX = (afterCam7StageX - beforeCam7StageX);
      int diffCam7StageY = (afterCam7StageY - beforeCam7StageY);
      int diffCam7Col = (afterCam7Column - beforeCam7Column);
      int diffCam7Row = (afterCam7Row - beforeCam7Row);

      Expect.expect(diffCam0StageX == CAMERA_0_EXPECTED_X,"diffCam0StageX != CAMERA_0_EXPECTED_X " + diffCam0StageX + "!=" + CAMERA_0_EXPECTED_X);
      Expect.expect(diffCam0StageY == CAMERA_0_EXPECTED_Y,"diffCam0StageY != CAMERA_0_EXPECTED_Y " + diffCam0StageY + "!=" + CAMERA_0_EXPECTED_Y);
      Expect.expect(diffCam0Col == CAMERA_0_EXPECTED_COL,"diffCam0Col != CAMERA_0_EXPECTED_COL " + diffCam0Col + "!=" + CAMERA_0_EXPECTED_COL);
      Expect.expect(diffCam0Row == CAMERA_0_EXPECTED_ROW,"diffCam0Row != CAMERA_0_EXPECTED_ROW " + diffCam0Row + "!=" + CAMERA_0_EXPECTED_ROW);

      Expect.expect(diffCam1StageX == CAMERA_1_EXPECTED_X,"diffCam1StageX != CAMERA_1_EXPECTED_X " + diffCam1StageX + "!=" + CAMERA_1_EXPECTED_X);
      Expect.expect(diffCam1StageY == CAMERA_1_EXPECTED_Y,"diffCam1StageY != CAMERA_1_EXPECTED_Y " + diffCam1StageY + "!=" + CAMERA_1_EXPECTED_Y);
      Expect.expect(diffCam1Col == CAMERA_1_EXPECTED_COL,"diffCam1Col != CAMERA_1_EXPECTED_COL " + diffCam1Col + "!=" + CAMERA_1_EXPECTED_COL);
      Expect.expect(diffCam1Row == CAMERA_1_EXPECTED_ROW,"diffCam1Row != CAMERA_1_EXPECTED_ROW " + diffCam1Row + "!=" + CAMERA_1_EXPECTED_ROW);

      Expect.expect(diffCam2StageX == CAMERA_2_EXPECTED_X,"diffCam2StageX != CAMERA_2_EXPECTED_X " + diffCam2StageX + "!=" + CAMERA_2_EXPECTED_X);
      Expect.expect(diffCam2StageY == CAMERA_2_EXPECTED_Y,"diffCam2StageY != CAMERA_2_EXPECTED_Y " + diffCam2StageY + "!=" + CAMERA_2_EXPECTED_Y);
      Expect.expect(diffCam2Col == CAMERA_2_EXPECTED_COL,"diffCam2Col != CAMERA_2_EXPECTED_COL " + diffCam2Col + "!=" + CAMERA_2_EXPECTED_COL);
      Expect.expect(diffCam2Row == CAMERA_2_EXPECTED_ROW,"diffCam2Row != CAMERA_2_EXPECTED_ROW " + diffCam2Row + "!=" + CAMERA_2_EXPECTED_ROW);

      Expect.expect(diffCam3StageX == CAMERA_3_EXPECTED_X,"diffCam3StageX != CAMERA_3_EXPECTED_X " + diffCam3StageX + "!=" + CAMERA_3_EXPECTED_X);
      Expect.expect(diffCam3StageY == CAMERA_3_EXPECTED_Y,"diffCam3StageY != CAMERA_3_EXPECTED_Y " + diffCam3StageY + "!=" + CAMERA_3_EXPECTED_Y);
      Expect.expect(diffCam3Col == CAMERA_3_EXPECTED_COL,"diffCam3Col != CAMERA_3_EXPECTED_COL " + diffCam3Col + "!=" + CAMERA_3_EXPECTED_COL);
      Expect.expect(diffCam3Row == CAMERA_3_EXPECTED_ROW,"diffCam3Row != CAMERA_3_EXPECTED_ROW " + diffCam3Row + "!=" + CAMERA_3_EXPECTED_ROW);

      Expect.expect(diffCam4StageX == CAMERA_4_EXPECTED_X,"diffCam4StageX != CAMERA_4_EXPECTED_X " + diffCam4StageX + "!=" + CAMERA_4_EXPECTED_X);
      Expect.expect(diffCam4StageY == CAMERA_4_EXPECTED_Y,"diffCam4StageY != CAMERA_4_EXPECTED_Y " + diffCam4StageY + "!=" + CAMERA_4_EXPECTED_Y);
      Expect.expect(diffCam4Col == CAMERA_4_EXPECTED_COL,"diffCam4Col != CAMERA_4_EXPECTED_COL " + diffCam4Col + "!=" + CAMERA_4_EXPECTED_COL);
      Expect.expect(diffCam4Row == CAMERA_4_EXPECTED_ROW,"diffCam4Row != CAMERA_4_EXPECTED_ROW " + diffCam4Row + "!=" + CAMERA_4_EXPECTED_ROW);

      Expect.expect(diffCam5StageX == CAMERA_5_EXPECTED_X,"diffCam5StageX != CAMERA_5_EXPECTED_X " + diffCam5StageX + "!=" + CAMERA_5_EXPECTED_X);
      Expect.expect(diffCam5StageY == CAMERA_5_EXPECTED_Y,"diffCam5StageY != CAMERA_5_EXPECTED_Y " + diffCam5StageY + "!=" + CAMERA_5_EXPECTED_Y);
      Expect.expect(diffCam5Col == CAMERA_5_EXPECTED_COL,"diffCam5Col != CAMERA_5_EXPECTED_COL " + diffCam5Col + "!=" + CAMERA_5_EXPECTED_COL);
      Expect.expect(diffCam5Row == CAMERA_5_EXPECTED_ROW,"diffCam5Row != CAMERA_5_EXPECTED_ROW " + diffCam5Row + "!=" + CAMERA_5_EXPECTED_ROW);

      Expect.expect(diffCam6StageX == CAMERA_6_EXPECTED_X,"diffCam6StageX != CAMERA_6_EXPECTED_X " + diffCam6StageX + "!=" + CAMERA_6_EXPECTED_X);
      Expect.expect(diffCam6StageY == CAMERA_6_EXPECTED_Y,"diffCam6StageY != CAMERA_6_EXPECTED_Y " + diffCam6StageY + "!=" + CAMERA_6_EXPECTED_Y);
      Expect.expect(diffCam6Col == CAMERA_6_EXPECTED_COL,"diffCam6Col != CAMERA_6_EXPECTED_COL " + diffCam6Col + "!=" + CAMERA_6_EXPECTED_COL);
      Expect.expect(diffCam6Row == CAMERA_6_EXPECTED_ROW,"diffCam6Row != CAMERA_6_EXPECTED_ROW " + diffCam6Row + "!=" + CAMERA_6_EXPECTED_ROW);

      Expect.expect(diffCam7StageX == CAMERA_7_EXPECTED_X,"diffCam7StageX != CAMERA_7_EXPECTED_X " + diffCam7StageX + "!=" + CAMERA_7_EXPECTED_X);
      Expect.expect(diffCam7StageY == CAMERA_7_EXPECTED_Y,"diffCam7StageY != CAMERA_7_EXPECTED_Y " + diffCam7StageY + "!=" + CAMERA_7_EXPECTED_Y);
      Expect.expect(diffCam7Col == CAMERA_7_EXPECTED_COL,"diffCam7Col != CAMERA_7_EXPECTED_COL " + diffCam7Col + "!=" + CAMERA_7_EXPECTED_COL);
      Expect.expect(diffCam7Row == CAMERA_7_EXPECTED_ROW,"diffCam7Row != CAMERA_7_EXPECTED_ROW " + diffCam7Row + "!=" + CAMERA_7_EXPECTED_ROW);
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    finally
    {
      xRayCameraArray.disableUnitTestMode();
      TestUtils.restoreConfigAfterRegressionTest();
    }

  }
}
