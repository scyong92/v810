package com.axi.v810.business.autoCal;

import com.axi.util.Enum;


/**
 * Enum of TestPrograms that Confirmation, Adjustments and Diagnostics can use
 * to image rails, coupons, etc.
 *
 * @author John Dutton
 */
public class ImageRetrieverTestProgramEnum extends Enum
{
  private static int _index = -1;

  public static ImageRetrieverTestProgramEnum FIXED_RAIL_COUPON_IN_MAGNIFICATION_POCKET =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum FIXED_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum ADJUSTABLE_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum CAL_MAGNIFICATION_IMAGE =
      new ImageRetrieverTestProgramEnum(++_index);  
  public static ImageRetrieverTestProgramEnum CAL_HIGH_MAGNIFICATION_IMAGE =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum FIXED_RAIL_COUPON_IN_HIGH_MAGNIFICATION_POCKET =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum HIGH_MAG_FIXED_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum HIGH_MAG_ADJUSTABLE_RAIL_COUPON_IN_SYSTEM_FIDUCIAL_POCKET =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum PSP_COUPON_IMAGE =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum PSP_XXL_COUPON_IMAGE =
      new ImageRetrieverTestProgramEnum(++_index);
  public static ImageRetrieverTestProgramEnum PSP_S2EX_COUPON_IMAGE =
      new ImageRetrieverTestProgramEnum(++_index);


  /**
   * @author John Dutton
   */
  protected ImageRetrieverTestProgramEnum(int id)
  {
    super(id);
  }
}
