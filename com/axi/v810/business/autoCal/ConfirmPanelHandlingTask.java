package com.axi.v810.business.autoCal;

import com.axi.v810.business.license.LicenseManager;
import java.util.concurrent.*;

import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * This class is responsible for providing a confirmation task that exercises
 * the Panel Handling subsystem. It is mainly a shell that calls into the
 * PanelHandling object and triggers the internal load/unload test there.
 * @author Reid Hayhow
 */
public class ConfirmPanelHandlingTask extends EventDrivenHardwareTask
{
  private static String _name = StringLocalizer.keyToString("CD_CONFIRM_PANEL_HANDLING_KEY");
  private static int _frequency = 1;
  private static long _timeoutInSeconds = 20000;
  private static ConfirmPanelHandlingTask _instance = null;

  private PanelHandler _panelHandler = PanelHandler.getInstance();
  // In order to get a full test of both sides (in flow through mode) we have to run two cycles of the test
  private static final int _TIMES_TO_RUN_PANEL_HANDLER_TEST = 2;
  private static final int _ESTIMATED_TIME_TO_RUN_IN_MILLIS = 12000;

  //Log utility members
  private FileLoggerAxi _logger;
  private int _maxLogFileSizeInBytes = 1028;

  //Executor service and two timeouts needed to allow us to abort this task
  private static ExecutorService _executor = new ScheduledThreadPoolExecutor(1);

  //This is the timeout applied for one iteration of this task. If it takes any
  //longer than the timeout, it will be aborted
  private static final int _EXECUTION_TIMEOUT_IN_SECONDS = 60;

  //Since we are dealing with hardware, there is also a timeout for the abort
  //command. If it takes longer than this timeout, the system is shutdown
  private static final int _ABORT_TIMEOUT_IN_SECONDS = 30;

  /**
   * ConfirmPanelHandlingTask constructor. Private for instance only access.
   *
   * @author Reid Hayhow
   */
  private ConfirmPanelHandlingTask(String name, int frequency, long timeoutInSeconds)
  {
    super(name, frequency, timeoutInSeconds, ProgressReporterEnum.CONFIRM_PANEL_HANDLING);

    //Create the logger
    _logger = new FileLoggerAxi(FileName.getConfirmPanelHandlingFullPath() +
                                FileName.getLogFileExtension(),
                                _maxLogFileSizeInBytes);

    //This confirmation is to be run manually only
    _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;

    //Set enum used to save the timestamp for this confirmation
    setTimestampEnum(HardwareCalibEnum.CONFIRM_PANEL_HANDLING_LAST_TIME_RUN);
  }

  /**
   * Instance access for this class
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmPanelHandlingTask getInstance()
  {
    if(_instance == null)
    {
      _instance = new ConfirmPanelHandlingTask(_name, _frequency, _timeoutInSeconds);
    }
    return _instance;
  }

  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   * Will always return true (probably for all hardware) for this task. As long
   * as the tester has panel handling :-)
   *
   * @author Reid hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * No clean-up needed for this task
   *
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays Cylinder to Up and Safe position.
        XrayActuator.getInstance().up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
  }

  /**
   * Create a simple Boolean result for this task. There are no limits, it just
   * passes or fails.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject()
  {
    _limits = new Limit(null, null, null, null);
  }

  /**
   * Create a failing result to begin with. Then, if the task executes as
   * expected I will create a passing result to override this one.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    _result = new Result(new Boolean(false), _name);
  }

  /**
   * The core of this task. Make sure there is a panel loaded and then call
   * the panel handler test.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    // Report that this task is starting
    reportProgress(0);

    //If the Panel Handler doesn't have a panel loaded, we can't run the task
    if(_panelHandler.isPanelLoaded() == false)
    {
      //Give an explicit message about why the task failed and suggested action
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_EXCEPTION_NO_BOARD_FOR_PANEL_HANDLING_TASK_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_NO_BOARD_FOR_PANEL_HANDLING_TASK_SUGGESTED_ACTION_KEY"));
      return;
    }

    // Report that a major task has started that I cannot provide updates for
    // Instead I provide an estimate of how long it will take
    _progressObservable.reportAtomicTaskStarted(_progressReporterEnum,
                                                _ESTIMATED_TIME_TO_RUN_IN_MILLIS * _TIMES_TO_RUN_PANEL_HANDLER_TEST);

    //We need to run the unloadLoad test on a separate thread with a timeout, so
    //create a future task that will do this for us. It returns true when done
    FutureTask<Boolean> panelHandlerTest = new FutureTask<Boolean>(new Callable<Boolean>() {
            public Boolean call() throws XrayTesterException
        {
          _panelHandler.repeatUnloadLoadForServiceTest(_TIMES_TO_RUN_PANEL_HANDLER_TEST);
          return Boolean.TRUE;
        }
      });

    //Submit the future task to execute the unloadLoad test to the executor
    _executor.submit(panelHandlerTest);
    
    //Run the panel handler test a selected number of times
    try
    {
      //Call the get method with the specified timeout, it will throw a
      //TimeoutExeception if the task takes any longer than the timeout
      //NOTE: get() throws an ExecutionException if the call() method throws
      //an exception, so this does not throw an XrayTesterException, see below
      panelHandlerTest.get(_EXECUTION_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
    }
    //Catch a timeout exception, the task took longer than we expected
    catch (TimeoutException ex)
    {
      //Handle the timeout by aborting the test, FAILING the task and throwing
      //an exception
      handlePanelHandlerTestTimeout(ex);
    }
    //The unloadLoad task threw an exception, probably an XrayTesterException
    //because there was a problem executing the test
    catch (ExecutionException ex)
    {
      //Tricky case, here we want to unwrap the ExecutionException and throw
      //the root cause if it was an XrayTesterException
      if(ex.getCause() instanceof XrayTesterException)
      {
        logExceptionInfoAndThrowException((XrayTesterException)ex.getCause());
      }
      //Otherwise wrap the exception and throw it
      else
      {
        throw createPanelHandlerExecutionProblemException(ex);
      }

    }
    catch (InterruptedException ex)
    {
      //Shouldn't happen, but handle it if it does by wrapping and throwing
      throw createPanelHandlerExecutionProblemException(ex);
    }
    finally
    {
      // OK, we have control back, so report that the task is done
      _progressObservable.reportAtomicTaskComplete(_progressReporterEnum);
    }

    //If the panel handler test doesn't throw any exceptions, the task passed
    _result = new Result(new Boolean(true), _name);

    //Log the fact that the task passed. The _name value is already localized
    _logger.append(_name + " - " + StringLocalizer.keyToString("CD_PASSED_KEY"));
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected boolean isExecuteNeeded()
  {
    //For now this task should never run from the HardwareTaskEngine
    //so always return false
    return false;
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays Cylinder to Up and Safe position.
        XrayActuator.getInstance().up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
  }

  /**
   * Method to handle the situation where the Panel Handler Unload/Load test took
   * longer than expected. It tries to abort the test, logs failure information
   * and handles the possibility that the abort call fails as well.
   *
   * @author Reid Hayhow
   */
  private void handlePanelHandlerTestTimeout(TimeoutException ex) throws XrayTesterException
  {
    //The abort task needs to be run with a timeout, so create a future task
    //to call the Panel Handler abort. It returns true when done
    FutureTask<Boolean> abortTask = new FutureTask<Boolean>(new Callable<Boolean>(){
      public Boolean call() throws XrayTesterException
      {
        _panelHandler.abort();
        return Boolean.TRUE;
      }
    });

    //Submit the abort task to the executor to be run
    _executor.submit(abortTask);

    try
    {
      //Wait for the abort task to finish
      abortTask.get(_ABORT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);

      //If the abort completed, we still need to fail the task and throw an exception
      //so, create the new exception indicating the panel handler test execution
      //had a problem
      HardwareTaskExecutionException htex = createPanelHandlerExecutionProblemException(ex);

      //log the data from the exception and then throw it
      logExceptionInfoAndThrowException(htex);
    }
    //It is *really* bad if the abort task doesn't complete, all of these
    //catch blocks will shutdown the tester and throw an exception that is
    //distinct from the panel handler execution problem exception thrown above
    catch (TimeoutException ex1)
    {
      handlePanelHandlerAbortTimeout(ex1);
    }
    catch (ExecutionException ex1)
    {
      handlePanelHandlerAbortTimeout(ex1);
    }
    catch (InterruptedException ex1)
    {
      handlePanelHandlerAbortTimeout(ex1);
    }
  }

  /**
   * Method to handle the severe situatio where the call to abort the Panel Handler
   * test does not complete before the timeout.
   *
   * @author Reid Hayhow
   */
  private void handlePanelHandlerAbortTimeout(Exception exception) throws XrayTesterException
  {
    //Shutdown the tester
    XrayTester.getInstance().shutdown();

    //Throw an exception indicating the situation
    throw createPanelHandlerAbortProblemException(exception);
  }


  /**
   * Common method to wrap an exception as a HardwareTaskExecutionException caused
   * by a problem executing the Panel Handling Unload/Load test so
   * it can be thrown through the XrayTesterException path
   *
   * @author Reid Hayhow
   */
  private HardwareTaskExecutionException createPanelHandlerExecutionProblemException(Exception exception)
  {
    //Create the exception
    HardwareTaskExecutionException newException = new HardwareTaskExecutionException(
      ConfirmationExceptionEnum.PROBLEM_EXECUTING_PANEL_HANDLING_TEST);

    //set the init cause and stack trace to preserve that data
    newException.initCause(exception);
    newException.setStackTrace(exception.getStackTrace());

    return newException;
  }

  /**
   * Common method to wrap an exception as a HardwareTaskExecutionException caused
   * by a problem ABORTING the Panel Handling Unload/Load test so
   * it can be thrown through the XrayTesterException path
   *
   * @author Reid Hayhow
   */
  private HardwareTaskExecutionException createPanelHandlerAbortProblemException(Exception exception)
  {
    //Create the exception
    HardwareTaskExecutionException newException = new HardwareTaskExecutionException(
      ConfirmationExceptionEnum.PROBLEM_ABORTING_PANEL_HANDLING_TEST);

    //set the init cause and stack trace to preserve that data
    newException.initCause(exception);
    newException.setStackTrace(exception.getStackTrace());

    return newException;
  }

  /**
   * Centralized method to log information from an exception encountered during
   * the execution of this task. An exception always guarantees the task fails.
   *
   * @author Reid Hayhow
   */
  private void logExceptionInfoAndThrowException(XrayTesterException exception) throws XrayTesterException
  {
    //If we get here the task failed, log this to the logfile. _name is already localized
    _logger.append(_name + " - " + StringLocalizer.keyToString("CD_FAILED_KEY"));

    //Log the specific error information to a file as well
    _logger.append(exception.getLocalizedMessage());

    //Also set the message in the result object passed to observable
    _result.setTaskStatusMessage(exception.getLocalizedMessage());

    _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_EXCEPTION_SEEN_IN_PANEL_HANDLING_TASK_SUGGESTED_ACTION_KEY"));

    throw exception;
  }

  /**
   * We want to bypass dependant tasks for Panel Confirmation task because
   * we really do not want to execute the camera-related tasks on this confirmation task.
   *
   * @author Cheah Lee Herng
   */
  public boolean isByPassDependantTasks()
  {
      return true;
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}