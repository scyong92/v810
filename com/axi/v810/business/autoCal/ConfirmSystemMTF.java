package com.axi.v810.business.autoCal;

import java.io.File;
import java.util.*;
import java.lang.reflect.Field;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.util.math.LaplaceEdgeFunction;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;


/**
 * Confirmation that measures the optical quality of the system by measuring MTF of edges
 * on the system coupon captured from the center camera.  MTF is an optical measurement
 * of how well an imaging system captures contrasts between black and white lines.  The
 * smaller the number, the better.
 * <p/>
 * 16 projections from the center camera are captured in the up direction and averaged
 * together to remove noise.
 * <p/>
 * Currently the class reports MTF for one edge.  Once some experiments are done on real hardware
 * the rest of the edge values will be used in some manner, e.g. averaged together.
 *
 * @author John Dutton
 * @author George A. David
 * @author Rick Gaudette
 */
public abstract class ConfirmSystemMTF extends ConfirmSharpness
{
  // The following parameters need to be set in the concrete subclass
  // How many projections to capture and average to create input image for MTF estimation
  protected int _NO_OF_PROJECTIONS;
  protected MagnificationEnum _magnification;
  protected int _MIN_EDGE_WIDTH_IN_PIXELS; // should it be in software.config?

  // RJG: these are a fudge factor added by Wei Chin for high mag, it would be desirable to understand why they are
  // needed figure out how to remove them

  protected int _differentInX; // offset 40mils
  protected int _differentInY; // offset 40mils

  // Create the Set for all cameras
  private Set<AbstractXrayCamera> _setOfCamerasToRunOn = new TreeSet<AbstractXrayCamera>();

  private static int _FREQUENCY = _config.getIntValue(HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY);

  private static long _TIMEOUT_IN_SECONDS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_TIMEOUT_IN_SECONDS);

  // Define the region width & height to be imaged for this confirmation.
  // Width will be 1 camera array width 1084 active pixels, the number of nanometers is mag dependent

  protected int _REGION_TO_IMAGE_WIDTH_NANOMETERS;

  // Height will be 1588 pixels = 25408000 nm = 1 inch
  protected int _REGION_TO_IMAGE_HEIGHT_NANOMETERS;

  // All below values should comes from SystemFiducialCoupon singleton, so that we can switch between normal and low mag easily in future.
  //  private static final int _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
  //    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_MARGIN_IN_X_FROM_QUADRILATERAL_UPPER_LEFT_NANOMETERS);
  //  private static final int _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
  //    _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS);

  // Margins to use for requesting projections:
  // UpperLeft is furthest point left of interest (of interest, i.e. to capture).
  protected int _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
  protected int _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS);
  protected int _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS);

  // Once LP4 range of motion error is resolved this can go away.
  private static final boolean _STAGE_MOTION_RANGE_OK_FOR_SYS_MTF_CONFIRMATION =
      _config.getBooleanValue(HardwareConfigEnum.STAGE_MOTION_RANGE_OK_FOR_SYS_MTF_CONFIRMATION);

  // Number of horizontal edges to average together
  private int _numHorizontalEdgesProcessed = -1;
  //  private final int _noHorizontalEdges = 8; // For normal mag
  // Number of vertical edges to average together
  private int _numVerticalEdgesProcessed = -1;
  //  private final int _noVerticalEdges = 2; // For normal mag
  private double _horizontalEdgeSum = -1;
  private double _verticalEdgeSum = -1;

  // Limits to compare to.
  private double _lineSpreadFWHMPixelsHighLimit;
  private double _lineSpreadFWHMPixelsLowLimit;

  private SystemFiducialCoupon _systemCoupon;

  private boolean _debugLogging = false;


  /**
   * @author John Dutton
   */
  protected ConfirmSystemMTF(String name_of_cal, String logDirectory)
  {
    super(name_of_cal,
          _FREQUENCY,
          _TIMEOUT_IN_SECONDS,
          logDirectory,
          FileName.getBaseImageQualityLogFileName(),
          ProgressReporterEnum.CONFIRM_SYSTEM_MTF_TASK);

    //Set the type of this task to reflect that it is an automatically run Confirm
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    setTimestampEnum(HardwareCalibEnum.CONFIRM_SYSTEM_MTF_LAST_TIME_RUN);

    _REGION_TO_IMAGE_HEIGHT_NANOMETERS = XrayCameraArray.getOneInchInNanometersAlignedUpToPixelBoundary();

    _systemCoupon = SystemFiducialCoupon.getInstance();
  }


  /**
   * Can't run this confirmation if on LP4.
   *
   * @author John Dutton
   */
  public boolean canRunOnThisHardware()
  {
    return _STAGE_MOTION_RANGE_OK_FOR_SYS_MTF_CONFIRMATION;
  }

  /**
   * Based on the magnification of the system, we may have 1 to many
   * regions to image. This function will return a list of
   * SystemFiducialRectangles that is the actual region to image.
   *
   * @author George A. David
   */
  private List<SystemFiducialRectangle> getRegionsToImage()
  {
    // dot 19 (see SystemFiducialCoupon for explanation of the dot names)
    // this dot is lowest dot that is visible in the image when using a 16 micron
    // system. It was decided that this where we want to begin imaging the
    // system coupon. In order for this to be the case, we need to set the
    // projection y value to be the y location of this dot. We have to remember
    // though that the image is rotated 180 degrees so we have to flip the
    // value.
    int projectionYValue = _systemCoupon.getDot19YInNanometersSystemFiducialCoordinates() - _differentInY;


    // ok, now determine what we need
    List<SystemFiducialRectangle> regions = new LinkedList<SystemFiducialRectangle>();
    int couponWidthInNanoMeters = _systemCoupon.getMaxWidthOfQuadrilateralInNanometers();
    int couponWidthInPixels = couponWidthInNanoMeters / _magnification.getNanoMetersPerPixel();

    int numberOfCameraPixelsUsed = XrayCameraArray.getCameras().get(0).getNumberOfSensorPixelsUsed();

    // CWC & RJG: This 2 pixel reduction was added during development, it is not known whether it is truly necessary.
    // But since high mag has been tested with this in place, further investigation is required to be sure if it can be
    // removed.
    if (_magnification == MagnificationEnum.H_NOMINAL && couponWidthInPixels >= numberOfCameraPixelsUsed)
      couponWidthInPixels = numberOfCameraPixelsUsed - 2;

    if (couponWidthInPixels < numberOfCameraPixelsUsed)
    {
      // the coupon fits within a camera sensor
      int pixelsPerCcdSensor = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED;
      // calculate the pixel width on the edge sensors assuming the coupon
      // is centered on the camera sensor
      int edgeWidthOnEdgeSensorInPixels = couponWidthInPixels / 2 - pixelsPerCcdSensor;
      if (edgeWidthOnEdgeSensorInPixels >= _MIN_EDGE_WIDTH_IN_PIXELS)
      {
        // in this case, we will only need 1 projection
        // we want to align the left edge of the coupon to the left edge of
        // the image. To do this we must set the projection x value to the
        // location of the left edge this is the upper left location.
        // the projection will be rotated 180 degrees from the cad, we need
        // to flip all the coupon values
        int projectionXValue = _systemCoupon.getQuadrilateralUpperLeftXinNanometersInSystemFiducialCoordinates();

        SystemFiducialRectangle projectionRectangle =
            new SystemFiducialRectangle(projectionXValue, projectionYValue,
                                        _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                        _REGION_TO_IMAGE_HEIGHT_NANOMETERS);

        regions.add(projectionRectangle);
      }
      else
      {
        // we need multiple projections
        // calculate the width of the edge on each sensor if we only
        // use 2 sensors.
        edgeWidthOnEdgeSensorInPixels = couponWidthInPixels / 2;
        if (edgeWidthOnEdgeSensorInPixels >= _MIN_EDGE_WIDTH_IN_PIXELS)
        {
          // create two projections, one aligned on the left of the sensor,
          // one aligned on the left

          // we want to align the left edge of the coupon to the left edge of
          // the image. To do this we must set the projection x value to the
          // location of the left edge this is the upper left location.
          // the projection will be rotated 180 degrees from the cad, we need
          // to flip all the coupon values
          int projectionXValue =
              _systemCoupon.getQuadrilateralUpperLeftXinNanometersInSystemFiducialCoordinates() + _differentInX;

          SystemFiducialRectangle projectionRectangle =
              new SystemFiducialRectangle(projectionXValue, projectionYValue,
                                          _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                          _REGION_TO_IMAGE_HEIGHT_NANOMETERS);

          regions.add(projectionRectangle);

          // create one aligned to the right

          // to align the right edge of the quadrilateral to the right edge
          // of the image, start by setting the projection x value at the
          // upper right x then subtract the width of the image
          // because the projection image will be rotated by 180 degrees, we need to flip the cad value
          projectionXValue =
              _systemCoupon.getQuadrilateralUpperRightXinNanometersInSystemFiducialCoordinates() - _differentInX -
                  _REGION_TO_IMAGE_WIDTH_NANOMETERS;

          projectionRectangle = new SystemFiducialRectangle(projectionXValue, projectionYValue,
                                                            _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                                            _REGION_TO_IMAGE_HEIGHT_NANOMETERS);
          regions.add(projectionRectangle);
        }
        else
          // we need one projection per CCD, this will not occur
          // with our current systems, lets assert for now
          Assert.expect(false, "The system coupon needs to projected on each CCD sensor");
      }
    }
    else
      // an image of the coupon will not fit inside the camera sensor,
      // we will need to come up with a different way of getting this image
      // currently this won't happen because none of our systems will exhibit
      // this behavior. Assert for now.
      Assert.expect(false, "The system coupon does not fit in one camera sensor");

    return regions;
  }

  /**
   * Perform the confirmation and save the results.
   * <p/>
   * Reid added/modified code to make it work on a list of cameras instead of just
   * the center camera.
   *
   * @author John Dutton
   * @author Reid Hayhow
   * @author Rick Gaudette
   */
  public void doTheRealConfirmationWork() throws XrayTesterException
  {
    //Swee Yee Wong - Camera debugging purpose
    debug("confirmBaseImageQuality");
    _lineSpreadFWHMPixelsHighLimit = _parsedLimits.getHighLimitDouble("LineSpreadFWHMPixels");
    _lineSpreadFWHMPixelsLowLimit = _parsedLimits.getLowLimitDouble("LineSpreadFWHMPixels");
    _numHorizontalEdgesProcessed = 0;
    _numVerticalEdgesProcessed = 0;
    _horizontalEdgeSum = 0;
    _verticalEdgeSum = 0;

    if (_magnification == MagnificationEnum.NOMINAL)
    {
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    }
    else
    {
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);
    }

    //Start the progress observable so users know what is happening
    initializeAndReportProgressPercentage();

    Image averagedImg = null; // Add each projection here.  Allows them to be free'd.

    List<SystemFiducialRectangle> regionsToImage = getRegionsToImage();

    // Note, x & y for projection are relative to the system fiducial.
    //
    // The right angle point of the system fiducial is at x 12.66 mm in CAD drawing.
    // The farthest relevant point to the left is the upper left point on the quadrilateral
    // at x 21.93 mm.  To have the left side of the projection include this point, use the
    // difference to define the left side of the image, along with a little margin.  With
    // this definition for the image left side, the upper right point on the quadrilateral will
    // fit in the right side of the image with some margin.
    //    int differenceInXFromSysFidToUpperLeft =
    //      Math.abs(_SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS - _SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS);
    //    int differenceInXFromSysFidToLeftEdgeOfImage = differenceInXFromSysFidToUpperLeft +
    //                                                   _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS;
    //    int projectionXValue = -differenceInXFromSysFidToLeftEdgeOfImage;

    // The right angle point of the system fiducial is at y 11.05 mm in CAD drawing.
    // The furthest dot below this point (marking visible region) is at y 19.85 mm.
    // To have bottom of projection image be at the dot, use the difference to define the
    // bottom of the image.
    //    int differenceInYFromSysFidToLowerRight =
    //      Math.abs(_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS - _SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS);
    //    int projectionYValue = -differenceInYFromSysFidToLowerRight;


    //    SystemFiducialRectangle systemFiducialRectangle =
    //        new SystemFiducialRectangle(projectionXValue, projectionYValue,
    //                                    _REGION_TO_IMAGE_WIDTH_NANOMETERS,
    //                                    _REGION_TO_IMAGE_HEIGHT_NANOMETERS);

    // Read in the list of cameras to run this confirmation on
    readCameraListFromFile();

    // Calculate the various numbers necessary to accurately report progress
    int numberOfCameras = _setOfCamerasToRunOn.size() * regionsToImage.size();
    int percentDonePerCamera = 100 / numberOfCameras;
    int percentDonePerCameraPerProjection = percentDonePerCamera / (_NO_OF_PROJECTIONS * regionsToImage.size());


    // Create projection requests
    List<ProjectionRequestThreadTask> projectionRequests = new ArrayList<ProjectionRequestThreadTask>();

    try
    {
      for (int iRegion = 0; iRegion < regionsToImage.size(); iRegion++)
      {
        SystemFiducialRectangle projectionRectangle = regionsToImage.get(iRegion);
        projectionRequests.clear();
        // Because of rounding errors I also provide progress updates on a per camera basis
        int camerasDone = 0;

        for (AbstractXrayCamera cameraToRunOn : _setOfCamerasToRunOn)
        {
          buildProjectionRequests(projectionRequests, projectionRectangle, cameraToRunOn);

          // Read image from file (debugging purposes) or get the projections
          // Assume that the current working directory is the .../autoCal/autotest directory
          if (readImageFromFileTrue())
          {
            averagedImg = loadAveragedImageFromUnitTestData(cameraToRunOn, iRegion);
          }
          else
          {
            averagedImg = acquireProjections(averagedImg,
                                             percentDonePerCameraPerProjection,
                                             projectionRequests,
                                             cameraToRunOn);

          }

          processImage(averagedImg, projectionRectangle, cameraToRunOn);

          setResultMessages();

          if (didConfirmationPass())
            saveImageOnConfirmationFailure(averagedImg, "averaged_img_cam" + cameraToRunOn.getId());

          averagedImg.decrementReferenceCount();
          averagedImg = null;
          MemoryUtil.garbageCollect();

          // Update the count of cameras done and report progress based on camera
          // count. This should be slightly more accurate than per projection reporting
          // done above
          camerasDone++;
          setProgressPercentageDoneAndReport(camerasDone * percentDonePerCamera);

          // If the user cancels the task (this could be a long task if all 14 cameras are checked)
          // we need to handle it gracefully
          if (_taskCancelled == true)
          {
            // Set the result to fail using a factory method and the task name
            _result = Result.createFailingResult(getName());

            // Indicate the task was canceled in the status
            _result.setStatus(HardwareTaskStatusEnum.CANCELED);

            // Indicate that the task is finished and leave
            reportProgressPercentageDone();
            return;
          }
        }

        if (Config.isDeveloperDebugModeOn())
        {
          logEdgeSpreadFunctionRate();
        }
      }

      if (regionsToImage.size() > 1 || _setOfCamerasToRunOn.size() > 1)
      {
        double horizontalMean = _horizontalEdgeSum / _numHorizontalEdgesProcessed;
        addResult(horizontalMean, "horizontal edges mean");

        double verticalMean = _verticalEdgeSum / _numVerticalEdgesProcessed;
        addResult(verticalMean, "vertical edges mean");
      }
      
      addMagnificationInfo(_magnification); // Khang Wah, add mgnification info into CD&A log
    }
    finally
    {
      reportProgressPercentageDone();

      // It is the responsibility of any code that uses high magnification to set it back to low magnification when it is
      // done.
      if (_magnification == MagnificationEnum.H_NOMINAL)
      {
        ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
      }
    }
  }
  
  /**
   * @author Rick Gaudette
   */
  private Image loadAveragedImageFromUnitTestData(AbstractXrayCamera cameraToRunOn, int iRegion)
  {
    Image averagedImg;
    String magDirectory = "high mag";
    if (_magnification == MagnificationEnum.NOMINAL)
    {
      magDirectory = "low mag";
    }

    StringBuilder projectionsDirectory = new StringBuilder("data");
    projectionsDirectory.append(File.separator);
    projectionsDirectory.append("Test_EdgeMTF");
    projectionsDirectory.append(File.separator);
    projectionsDirectory.append("projections");
    projectionsDirectory.append(File.separator);
    projectionsDirectory.append(magDirectory);
    String projectionFilename = String.format("projection_img_avg_cam%d_%d.png",
                                              cameraToRunOn.getId(),
                                              iRegion + 1);

    averagedImg = readImageFromFile(projectionsDirectory.toString(), projectionFilename);
    System.out.printf("Reading image: %s%s%s\n", projectionsDirectory, File.separator, projectionFilename);
    return averagedImg;
  }

  /**
   * @author Rick Gaudette
   */
  private void logEdgeSpreadFunctionRate() throws XrayTesterException
  {
    Config config = Config.getInstance();
    StringBuilder horizontalString = new StringBuilder("Horizontal ESF rate: ");
    StringBuilder verticalString = new StringBuilder("Vertical ESF rate: ");
    for (AbstractXrayCamera camera : _setOfCamerasToRunOn)
    {
      HardwareCalibEnum hardwareCalibEnum = mapHardwareCalibEdgeSpreadFunctionRate(camera, "X");
      Double horizontalBetaAvg = config.getDoubleValue(hardwareCalibEnum);
      horizontalString.append(String.format("%.2f, ", horizontalBetaAvg));

      hardwareCalibEnum = mapHardwareCalibEdgeSpreadFunctionRate(camera, "Y");
      Double verticalBetaAvg = config.getDoubleValue(hardwareCalibEnum);
      verticalString.append(String.format("%.2f, ", verticalBetaAvg));
    }
    System.out.println(horizontalString);
    System.out.println(verticalString);
  }

  /**
   * @author Rick Gaudette
   */
  private void buildProjectionRequests(List<ProjectionRequestThreadTask> projectionRequests,
                                       SystemFiducialRectangle projectionRectangle,
                                       AbstractXrayCamera cameraToRunOn)
  {
    for (int i = 0; i < _NO_OF_PROJECTIONS; i++)
    {
      if (i > 0)
      {
        // Between requests in the forward direction insert one going backward that
        // the confirmation will ignore.  This is a workaround for a limitation in
        // ProjectedImagesProducer.generateScanPath where it essentially collapses
        // all the forward requests into the same scan pass if there isn't a "different"
        // request between them.  Going in the reverse direction is different enough.
        // (Until this situation appears more frequently in the code, living with the
        // workaround makes sense.  It also doesn't have much of a performance impact.)
        ProjectionRequestThreadTask request = new ProjectionRequestThreadTask(cameraToRunOn,
                                                                              ScanPassDirectionEnum.REVERSE,
                                                                              projectionRectangle);
        projectionRequests.add(request);
      }

      // Set up "real" projection request.
      ProjectionRequestThreadTask request = new ProjectionRequestThreadTask(cameraToRunOn,
                                                                            ScanPassDirectionEnum.FORWARD,
                                                                            projectionRectangle);
      projectionRequests.add(request);
    }
  }

  private Image acquireProjections(Image averagedImg,
                                   int percentDonePerCameraPerProjection,
                                   List<ProjectionRequestThreadTask> projectionRequests,
                                   AbstractXrayCamera cameraToRunOn) throws XrayTesterException
  {
    // Get the projections
    TestExecutionTimer testExecutionTimer = new TestExecutionTimer();

    // Initialize the IAE before requesting projections
    ImageAcquisitionEngine imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
    imageAcquisitionEngine.initialize();
    // RJG: If we decide that the IAE is the keeper of the magnification state, then it should not need separate
    // methods to acquire projections.
    if (_magnification == MagnificationEnum.NOMINAL)
    {
      imageAcquisitionEngine.acquireProjections(projectionRequests, testExecutionTimer, false);
    }
    else
    {
      imageAcquisitionEngine.acquireHighMagProjections(projectionRequests, testExecutionTimer, false);
    }

    int projectionCount = 0;
    boolean dummyProjection = false; // First image returned is one we want.
    for (ProjectionRequestThreadTask request : projectionRequests)
    {
      Assert.expect(projectionCount < _NO_OF_PROJECTIONS, "Processing too many projections");

      ProjectionInfo projInfo = request.getProjectionInfo();
      Image img = projInfo.getProjectionImage();

      // Only process the images we really wanted.
      if (dummyProjection)
      {
        dummyProjection = false;
        //  saveIfSavingDebugImages(img, "projection_img_reverse");
      }
      else
      {
        // Init the first time we have a projection image from center camera.
        /** @todo Bob B. - Consider if there is a better approach for allocating the averagedImg (once only?) */
        if (averagedImg == null)
        {
          averagedImg = new Image(img.getWidth(), img.getHeight());
          Paint.fillImage(averagedImg, 0.f);
        }
        RegionOfInterest regOfInt = RegionOfInterest.createRegionFromImage(averagedImg);

        saveIfSavingDebugImages(img, "projection_img_cam" + cameraToRunOn.getId());

        Arithmetic.addImages(averagedImg, regOfInt, img, regOfInt, averagedImg, regOfInt);

        // Update progress for each projection we care about
        increaseAndReportProgressPercentage(percentDonePerCameraPerProjection);

        projectionCount++;
        dummyProjection = true;
      }
    }

    for (ProjectionRequestThreadTask projectionRequest : projectionRequests)
      projectionRequest.clear();
    projectionRequests.clear();


    // Added up all of the images.  Now divide to get the average.
    Arithmetic.multiplyByConstantInPlace(averagedImg, (float) 1.0 / projectionCount);

    saveIfSavingDebugImages(averagedImg, "projection_img_avg_cam" + cameraToRunOn.getId());
    return averagedImg;
  }


  /**
   * @author George A. David
   */
  private Image getCroppedImage(Image projectionImage,
                                int xInPixels,
                                int yInPixels,
                                int widthInPixels,
                                int heightInPixels,
                                String description) throws DatastoreException
  {
    RegionOfInterest segmentRoi = new RegionOfInterest(xInPixels,
                                                       yInPixels,
                                                       widthInPixels,
                                                       heightInPixels,
                                                       0,
                                                       RegionShapeEnum.RECTANGULAR);

    Image segmentImage = Image.createCopy(projectionImage, segmentRoi);
    saveIfSavingDebugImages(segmentImage, description);

    return segmentImage;
  }

  /**
   * @author George A. David
   */
  private Image getCroppedVerticalEdgeImage(Image projectionImage,
                                            int xStartInPixels,
                                            int yStartInPixels,
                                            int xEndInPixels,
                                            int yEndInPixels,
                                            String description,
                                            AbstractXrayCamera xrayCamera)
      throws DatastoreException
  {
    Assert.expect(projectionImage != null);
    Assert.expect(description != null);

    String cameraTag = String.format("_cam%02d", xrayCamera.getId());

    // center it horizontally
    // RJG: CHECK THIS!!!
    // In the original ConfirmHighMagSystemMTF the WIDTH and not HEIGHT is used, is that really what Wei Chin wants?
    int roiX = (xStartInPixels + xEndInPixels - _CROPPED_VERTICAL_EDGE_RIGHT_HEIGHT_PIXELS) / 2;
    if (_magnification == MagnificationEnum.H_NOMINAL)
    {
      roiX = (xStartInPixels + xEndInPixels - _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS) / 2;
    }
    int roiY = yStartInPixels;
    int roiWidth = _CROPPED_VERTICAL_EDGE_RIGHT_WIDTH_PIXELS;
    int roiHeight = yEndInPixels - yStartInPixels;

    // let's check to make sure we aren't up against the edge of the camera, if we are, return null
    if (roiX < 0 || // up against left edge
        roiX + roiWidth > projectionImage.getWidth()) // up against right edge
      return null;

    Image verticalEdgeSegmentImage = getCroppedImage(projectionImage,
                                                     roiX,
                                                     roiY,
                                                     roiWidth,
                                                     roiHeight,
                                                     description + "_EdgeSegmentImage" + cameraTag);
    return verticalEdgeSegmentImage;
  }

  /**
   * returns a list of edge images in the following order
   * left, left center, right center, right which correspond
   * to each CCD sensor in the camera. A null value in the list
   * indicates that the horizontal edge was not visible in that
   * segment of camera.
   *
   * @author George A. David
   */
  private List<Image> getCroppedHorizontalEdgeSegmentImages(Image projectionImage,
                                                            int xStartInPixels,
                                                            int xEndInPixels,
                                                            int yStartInPixels,
                                                            int yEndInPixels,
                                                            int yMinInPixels,
                                                            int yMaxInPixels,
                                                            String description,
                                                            AbstractXrayCamera xrayCamera)
      throws DatastoreException
  {
    Assert.expect(projectionImage != null);
    Assert.expect(description != null);

    String cameraTag = String.format("_cam%02d", xrayCamera.getId());

    List<Image> images = new LinkedList<Image>();

    int endSegmentX = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED;

    // we want to center the edge in each image, in order to do so, we need to
    // know where the midpoint of each segment is and center it
    // we could  use GeomUtil.calculateMidPoingUsingXvalues(x1, y1, x2, y2, xStart, xEnd)
    // but this calculates the slope and y-intercept every time it's called so
    // to speed things up, we store those values locally and then call
    // GeomUtil.calculateMidPoingUsingXvalues(slope, yIntercept, xStart, xEnd).
    // gad
    double slope = GeomUtil.calculateSlope(xStartInPixels, yStartInPixels, xEndInPixels, yEndInPixels);
    double yIntercept = GeomUtil.calculateYintercept(xStartInPixels, yStartInPixels, slope);

    int roiX = xStartInPixels;
    int roiWidth = Math.min(endSegmentX, xEndInPixels) - roiX;

    // center the edge vertically
    int midY = (int) GeomUtil.calculateMidPointUsingXvalues(slope, yIntercept, roiX, roiX + roiWidth).getY();
    int roiY = midY - _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS / 2;
    int roiHeight = Math.min(_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS, projectionImage.getHeight() - roiY);

    roiY = Math.max(roiY, yMinInPixels);

    int maxHeight = yMaxInPixels - roiY;
    roiHeight = Math.min(roiHeight, maxHeight);

    Image leftEdgeSegmentImage = null;
    if (roiX < endSegmentX && roiWidth > 200)
    {
      leftEdgeSegmentImage = getCroppedImage(projectionImage,
                                             roiX,
                                             roiY,
                                             roiWidth,
                                             roiHeight,
                                             description + "_leftEdgeSegmentImage" + cameraTag);
    }
    images.add(leftEdgeSegmentImage);

    endSegmentX = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 2;
    roiX = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED;
    roiWidth = Math.min(endSegmentX, xEndInPixels) - roiX;

    // center it vertically
    midY = (int) GeomUtil.calculateMidPointUsingXvalues(slope, yIntercept, roiX, roiX + roiWidth).getY();
    roiY = midY - _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS / 2;
    roiHeight = Math.min(_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS, projectionImage.getHeight() - roiY);

    // adjust values to avoid the dots
    roiY = Math.max(roiY, yMinInPixels);
    maxHeight = yMaxInPixels - roiY;
    roiHeight = Math.min(roiHeight, maxHeight);

    Image leftCenterEdgeSegmentImage = null;
    if (roiX < endSegmentX && roiWidth > 200)
    {
      leftCenterEdgeSegmentImage = getCroppedImage(projectionImage,
                                                   roiX,
                                                   roiY,
                                                   roiWidth,
                                                   roiHeight,
                                                   description + "_leftCenterEdgeSegmentImage" + cameraTag);
    }
    images.add(leftCenterEdgeSegmentImage);

    endSegmentX = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 3;
    roiX = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 2;
    roiWidth = Math.min(endSegmentX, xEndInPixels) - roiX;

    // center it vertically
    midY = (int) GeomUtil.calculateMidPointUsingXvalues(slope, yIntercept, roiX, roiX + roiWidth).getY();
    roiY = midY - _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS / 2;
    roiHeight = Math.min(_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS, projectionImage.getHeight() - roiY);

    // adjust values to avoid the dots
    roiY = Math.max(roiY, yMinInPixels);
    maxHeight = yMaxInPixels - roiY;
    roiHeight = Math.min(roiHeight, maxHeight);

    Image rightCenterEdgeSegmentImage = null;
    if (roiX < endSegmentX && roiWidth > 200)
    {
      rightCenterEdgeSegmentImage = getCroppedImage(projectionImage,
                                                    roiX,
                                                    roiY,
                                                    roiWidth,
                                                    roiHeight,
                                                    description + "_rightCenterEdgeSegmentImage" + cameraTag);
    }
    images.add(rightCenterEdgeSegmentImage);

    endSegmentX = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 4;
    roiX = _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED * 3;
    roiWidth = Math.min(endSegmentX, xEndInPixels) - roiX;

    // center it vertically
    midY = (int) GeomUtil.calculateMidPointUsingXvalues(slope, yIntercept, roiX, roiX + roiWidth).getY();
    roiY = midY - _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS / 2;
    roiHeight = Math.min(_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS, projectionImage.getHeight() - roiY);

    // adjust values to avoid the dots
    roiY = Math.max(roiY, yMinInPixels);
    maxHeight = yMaxInPixels - roiY;
    roiHeight = Math.min(roiHeight, maxHeight);

    Image rightEdgeSegmentImage = null;
    if (roiX < endSegmentX && roiWidth > 200)
    {
      rightEdgeSegmentImage = getCroppedImage(projectionImage,
                                              roiX,
                                              roiY,
                                              roiWidth,
                                              roiHeight,
                                              description + "_rightEdgeSegmentImage" + cameraTag);
    }
    images.add(rightEdgeSegmentImage);

    return images;
  }

  /**
   * @author George A. David
   */
  private void saveQuadrilateralImage(Image projectionImage,
                                      int upperLeftPixelX,
                                      int upperRightPixelX,
                                      int upperRightPixelY,
                                      int lowerRightPixelY) throws DatastoreException
  {
    Assert.expect(projectionImage != null);

    RegionOfInterest couponRoi =
        new RegionOfInterest(upperLeftPixelX,
                             projectionImage.getHeight() - upperRightPixelY,// - _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS,
                             upperRightPixelX,
                             upperRightPixelY - lowerRightPixelY,// + _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS,
                             0,
                             RegionShapeEnum.RECTANGULAR);

    Image croppedCouponImage = Image.createCopy(projectionImage, couponRoi);
    saveIfSavingDebugImages(croppedCouponImage, "croppedCouponImage");
  }

  /**
   * crops out the image that corresponds to each CCD sensor.
   *
   * @author George A. David
   */
  private void saveCcdSegmentImages(Image projectionImage, AbstractXrayCamera xrayCamera) throws DatastoreException
  {
    Assert.expect(projectionImage != null);
    String cameraTag = String.format("_cam%02d", xrayCamera.getId());

    // crop each camera segment
    RegionOfInterest leftSegment = new RegionOfInterest(0,
                                                        0,
                                                        _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
                                                        projectionImage.getHeight(),
                                                        0,
                                                        RegionShapeEnum.RECTANGULAR);


    Image leftSegmentImage = Image.createCopy(projectionImage, leftSegment);
    saveIfSavingDebugImages(leftSegmentImage, "leftSegmentImage" + cameraTag);

    RegionOfInterest leftCenterSegment =
        new RegionOfInterest(_CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
                             0,
                             _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
                             projectionImage.getHeight(),
                             0,
                             RegionShapeEnum.RECTANGULAR);

    Image leftCenterSegmentImage = Image.createCopy(projectionImage, leftCenterSegment);
    saveIfSavingDebugImages(leftCenterSegmentImage, "leftCenterSegmentImage" + cameraTag);

    RegionOfInterest rightCenterSegment =
        new RegionOfInterest(2 * _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
                             0,
                             _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
                             projectionImage.getHeight(),
                             0,
                             RegionShapeEnum.RECTANGULAR);


    Image rightCenterSegmentImage = Image.createCopy(projectionImage, rightCenterSegment);
    saveIfSavingDebugImages(rightCenterSegmentImage, "rightCenterSegmentImage" + cameraTag);

    RegionOfInterest rightSegment =
        new RegionOfInterest(3 * _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
                             0,
                             _CAMERA_SEGMENT_WIDTH_WITH_SYNTHESIZED,
                             projectionImage.getHeight(),
                             0,
                             RegionShapeEnum.RECTANGULAR);


    Image rightSegmentImage = Image.createCopy(projectionImage, rightSegment);
    saveIfSavingDebugImages(rightSegmentImage, "rightSegmentImage" + cameraTag);

  }

  /**
   * returns a list of cropped edge images in the following order
   * top left, top left center, top right center, top right
   * bottom left, bottom left center, bottom right center, bottom right
   * left vertical edge, right vertical edge.
   * a null value in the list indicates the edge was not available for
   * the corresponding CCD sensor in the case of horizontal images, for
   * vertical images, it indicates that we were too close to the edge of
   * the camera to get a good image.
   *
   * @author George A. David
   */
  private List<Image> getCroppedEdgeImages(Image projectionImage,
                                           SystemFiducialRectangle projectionRectangle,
                                           AbstractXrayCamera xrayCamera) throws DatastoreException
  {
    List<Image> images = new LinkedList<Image>();

    // the image of the system coupon and the cad of the system coupon are
    // 180 degrees out of phase therefore we have to flip all the location
    // values of the system coupon features.
    int upperLeftXinNanos = _systemCoupon.getQuadrilateralUpperLeftXinNanometersInSystemFiducialCoordinates();
    int upperRightXInNanos = _systemCoupon.getQuadrilateralUpperRightXinNanometersInSystemFiducialCoordinates();

    int upperLeftYinNanos = _systemCoupon.getQuadrilateralUpperLeftYinNanometersInSystemFiducialCoordinates();
    int upperRightYinNanos = _systemCoupon.getQuadrilateralUpperRightYinNanometersInSystemFiducialCoordinates();

    int lowerLeftXinNanos = _systemCoupon.getQuadrilateralLowerLeftXinNanometersInSystemFiducialCoordinates();
    int lowerRightXinNanos = _systemCoupon.getQuadrilateralLowerRightXinNanometersInSystemFiducialCoordinates();

    int lowerLeftYinNanos = _systemCoupon.getQuadrilateralLowerLeftYinNanometersInSystemFiducialCoordinates();
    int lowerRightYinNanos = _systemCoupon.getQuadrilateralLowerRightYinNanometersInSystemFiducialCoordinates();

    // convert all coords to image coords in pixels. recall that the
    // projection rectangle has the origin at the lower left, but the
    // image coordinate is actually at the upper left so we have to convert
    // all y values by subtracting from the image height.
    int upperLeftPixelX = (upperLeftXinNanos - projectionRectangle.getMinX()) / _magnification.getNanoMetersPerPixel();
    int upperRightPixelX =
        (upperRightXInNanos - projectionRectangle.getMinX()) / _magnification.getNanoMetersPerPixel();

    int upperLeftPixelY = projectionImage.getHeight() -
        ((upperLeftYinNanos - projectionRectangle.getMinY()) / _magnification.getNanoMetersPerPixel());
    int upperRightPixelY = projectionImage.getHeight() -
        ((upperRightYinNanos - projectionRectangle.getMinY()) / _magnification.getNanoMetersPerPixel());

    int lowerLeftPixelX = (lowerLeftXinNanos - projectionRectangle.getMinX()) / _magnification.getNanoMetersPerPixel();
    int lowerRightPixelX =
        (lowerRightXinNanos - projectionRectangle.getMinX()) / _magnification.getNanoMetersPerPixel();

    int lowerLeftPixelY = projectionImage.getHeight() -
        ((lowerLeftYinNanos - projectionRectangle.getMinY()) / _magnification.getNanoMetersPerPixel());
    int lowerRightPixelY = projectionImage.getHeight() -
        ((lowerRightYinNanos - projectionRectangle.getMinY()) / _magnification.getNanoMetersPerPixel());

    // some of the dots that surround the quadrilateral will cause the
    // confirmation to fail intermittently, we need to crop out
    // these dots, see SystemFiducialCoupon for more info
    int yMinNanos =
        _systemCoupon.getDot10yInNanometersSystemFiducialCoordinates() + (_systemCoupon.getDotWidthInNanoMeters() / 2);
    int yMaxNanos =
        _systemCoupon.getDot31yInNanometersSystemFiducialCoordinates() - (_systemCoupon.getDotWidthInNanoMeters() / 2);
    int yMinInPixels = projectionImage.getHeight() -
        ((yMinNanos - projectionRectangle.getMinY()) / _magnification.getNanoMetersPerPixel());
    int yMaxInPixels = projectionImage.getHeight() -
        ((yMaxNanos - projectionRectangle.getMinY()) / _magnification.getNanoMetersPerPixel());

    //    saveQuadrilateralImage(projectionImage,
    //                           upperLeftPixelX,
    //                           upperRightPixelX,
    //                           upperRightPixelY,
    //                           lowerLeftPixelY);

    // saveCcdSegmentImages(projectionImage, xrayCamera);

    // change the starting and ending x values so that the vertical edges
    // don't show up in the horizontal edge images.
    int xStartPixel = Math.max(upperLeftPixelX, lowerLeftPixelX);
    // CWC & RJG: Bounding xStartPixel added during high magnification development, it is not known whether it is truly
    // necessary. But since high magnification has been tested with this in place, further investigation is required to
    // be sure if it can be removed.
    if (_magnification == MagnificationEnum.H_NOMINAL)
    {
      xStartPixel = Math.max(xStartPixel, 0);
    }
    int xEndPixel = Math.min(upperRightPixelX, lowerRightPixelX);

    // now crop out the top edges in each segment
    List<Image> topEdgeImages = getCroppedHorizontalEdgeSegmentImages(projectionImage,
                                                                      xStartPixel,
                                                                      xEndPixel,
                                                                      upperLeftPixelY,
                                                                      upperRightPixelY,
                                                                      yMinInPixels,
                                                                      yMaxInPixels,
                                                                      "upper",
                                                                      xrayCamera);
    images.addAll(topEdgeImages);

    // crop out the bottom edge in each segment
    List<Image> bottomEdgeImages = getCroppedHorizontalEdgeSegmentImages(projectionImage,
                                                                         xStartPixel,
                                                                         xEndPixel,
                                                                         lowerLeftPixelY,
                                                                         lowerRightPixelY,
                                                                         yMinInPixels,
                                                                         yMaxInPixels,
                                                                         "lower",
                                                                         xrayCamera);
    images.addAll(bottomEdgeImages);

    // crop out the left edge
    Image leftEdgeImage = getCroppedVerticalEdgeImage(projectionImage,
                                                      upperLeftPixelX,
                                                      upperLeftPixelY,
                                                      lowerLeftPixelX,
                                                      lowerLeftPixelY,
                                                      "left",
                                                      xrayCamera);
    images.add(leftEdgeImage);

    // crop out the right edge
    Image rightEdgeImage = getCroppedVerticalEdgeImage(projectionImage,
                                                       upperRightPixelX,
                                                       upperRightPixelY,
                                                       lowerRightPixelX,
                                                       lowerRightPixelY,
                                                       "right",
                                                       xrayCamera);
    images.add(rightEdgeImage);

    return images;
  }

  /**
   * calculate the MTF if the image is not null and it is
   * at least 200 pixels wide.
   *
   * @author George A. David
   */
  private void processAndDecrementImage(Image image,
                                        boolean isVertical,
                                        String description,
                                        List<Double> beta)
      throws DatastoreException
  {
    // image may be null
    if (image != null)
    {
      double result = calculateMTFAndAddResult(image,
                                               isVertical,
                                               _lineSpreadFWHMPixelsLowLimit,
                                               _lineSpreadFWHMPixelsHighLimit,
                                               description);
      image.decrementReferenceCount();
      if (isVertical)
      {
        ++_numVerticalEdgesProcessed;
        _verticalEdgeSum += result;
      }
      else
      {
        ++_numHorizontalEdgesProcessed;
        _horizontalEdgeSum += result;
      }

      modelEdge(beta);
    }
  }


  private void modelEdge(List<Double> betas)
  {
    if (_edgeProfile.length > 0)
    {
      double[] x = new double[_edgeProfile.length];
      double step = 1.0 / _edgeProfileOversample;
      for (int i = 0; i < _edgeProfile.length; i++)
      {
        x[i] = i * step;
      }

      LaplaceEdgeFunction edgeFunction = new LaplaceEdgeFunction();
      double[] laplaceModelParameters = edgeFunction.fitModel(x, _edgeProfile);

      // Check that the fit is reasonable given what we expect from an image of the coupon edge
      // the edge transition has to be at least 20 samples from the start
      double edgeBoundary = step * 20;
      double mu = laplaceModelParameters[1];
      if (mu < edgeBoundary || x[_edgeProfile.length - 1] - mu < edgeBoundary)
      {
        if (Config.isDeveloperDebugModeOn())
        {
          System.out.printf("Quadrilateral edge too close to image boundary, not using, mu: %.2f\n", mu);
        }
        return;
      }

      // and beta should be within a reasonable range
      double beta = laplaceModelParameters[2];
      if (beta < 0.001 || beta > 3.0)
      {
        if (Config.isDeveloperDebugModeOn())
        {
          System.out.printf("Edge beta out of expected range, not using, beta: %.2f\n", beta);
        }
        return;
      }

      betas.add(beta);
    }
  }


  private void setEdgeSpreadFunctionRate(ArrayList<Double> horizontalBeta,
                                         ArrayList<Double> verticalBeta,
                                         AbstractXrayCamera camera)
      throws XrayTesterException
  {
    Double horizontalBetaAvg = com.axi.util.StatisticsUtil.mean(horizontalBeta);
    if (!horizontalBetaAvg.isNaN())
    {
      HardwareCalibEnum hardwareCalibEnum = mapHardwareCalibEdgeSpreadFunctionRate(camera, "X");
      Config config = Config.getInstance();
      config.setValue(hardwareCalibEnum, horizontalBetaAvg);
    }

    Double verticalBetaAvg = com.axi.util.StatisticsUtil.mean(verticalBeta);
    if (!verticalBetaAvg.isNaN())
    {
      HardwareCalibEnum hardwareCalibEnum = mapHardwareCalibEdgeSpreadFunctionRate(camera, "Y");
      Config config = Config.getInstance();
      config.setValue(hardwareCalibEnum, verticalBetaAvg);
    }

    if (_debugLogging)
    {
      System.out.println("Horizontal beta values:");
      System.out.printf("  ");
      for (Double beta : horizontalBeta)
      {
        System.out.printf("%.2f, ", beta);
      }
      System.out.printf("   mean: %.2f\n", horizontalBetaAvg);

      System.out.println("Vertical beta values:");
      System.out.printf("  ");
      for (Double beta : verticalBeta)
      {
        System.out.printf("%.2f, ", beta);
      }
      System.out.printf("   mean: %.2f\n", verticalBetaAvg);
    }
  }

  private HardwareCalibEnum mapHardwareCalibEdgeSpreadFunctionRate(AbstractXrayCamera camera, String dimension)
      throws XrayTesterException
  {
    String mag_state;
    if (_magnification == MagnificationEnum.NOMINAL)
    {
      mag_state = "LOW_MAG";
    }
    else if (_magnification == MagnificationEnum.H_NOMINAL)
    {
      mag_state = "HIGH_MAG";
    }
    else
    {
      throw new EdgeMTFBusinessException();
    }

    dimension = dimension.toUpperCase();
    if (!(dimension.equals("X") || dimension.equals("Y")))
    {
      throw new EdgeMTFBusinessException();
    }
    String field_name = String.format("EDGE_SPREAD_FUNCTION_RATE_%s_CAMERA_%d_%s",
                                      mag_state,
                                      camera.getId(),
                                      dimension);

    String className = "com.axi.v810.datastore.config.HardwareCalibEnum";

    HardwareCalibEnum hardwareCalibEnum;
    try
    {
      Class cls = Class.forName(className);
      Field fld = cls.getField(field_name);
      hardwareCalibEnum = (HardwareCalibEnum) fld.get(cls);

    }
    catch (ClassNotFoundException | NoSuchFieldException | IllegalAccessException e)
    {
      throw new EdgeMTFBusinessException();
    }

    return hardwareCalibEnum;

  }

  /**
   * @author John Dutton
   */
  private void processImage(Image projectionImage,
                            SystemFiducialRectangle projectionRectangle,
                            AbstractXrayCamera camera)
      throws DatastoreException, XrayTesterException
  {
    long startTime = 0;
    if (_debugLogging)
    {
      startTime = System.currentTimeMillis();
    }

    Assert.expect(projectionImage != null);
    Assert.expect(projectionRectangle != null);

    double originalHorizontalEdgeSum = _horizontalEdgeSum;
    double originalVerticalEdgeSum = _verticalEdgeSum;
    int prevNumHorizontalEdgesProcessed = _numHorizontalEdgesProcessed;
    int prevNumVerticalEdgesProcessed = _numVerticalEdgesProcessed;

    List<Image> images = getCroppedEdgeImages(projectionImage, projectionRectangle, camera);

    ArrayList<Double> horizontalBeta = new ArrayList<>(8);
    ArrayList<Double> verticalBeta = new ArrayList<>(2);

    // process top horizontal edge
    processAndDecrementImage(images.remove(0),
                             false,
                             "top line, far left for camera " + camera.getId(),
                             horizontalBeta);
    processAndDecrementImage(images.remove(0),
                             false,
                             "top line, left of center for camera " + camera.getId(),
                             horizontalBeta);
    processAndDecrementImage(images.remove(0),
                             false,
                             "top line, right of center for camera " + camera.getId(),
                             horizontalBeta);
    processAndDecrementImage(images.remove(0),
                             false,
                             "top line, far right for camera " + camera.getId(),
                             horizontalBeta);

    // process bottom horizontal edge
    processAndDecrementImage(images.remove(0),
                             false,
                             "bottom line, far left for camera " + camera.getId(),
                             horizontalBeta);
    processAndDecrementImage(images.remove(0),
                             false,
                             "bottom line, left of center for camera " + camera.getId(),
                             horizontalBeta);
    processAndDecrementImage(images.remove(0),
                             false,
                             "bottom line, right of center for camera " + camera.getId(),
                             horizontalBeta);
    processAndDecrementImage(images.remove(0),
                             false,
                             "bottom line, far right for camera " + camera.getId(),
                             horizontalBeta);

    // process left vertical edge
    processAndDecrementImage(images.remove(0),
                             true,
                             "vertical edge, far left for camera " + camera.getId(),
                             verticalBeta);

    // Process right vertical edge.
    processAndDecrementImage(images.remove(0),
                             true,
                             "vertical edge, far right for camera " + camera.getId(),
                             verticalBeta);

    // sanity check, make sure we have consumed all the images
    Assert.expect(images.isEmpty());

    // calculate the means for this camera
    int horizontalEdgesProcessed = _numHorizontalEdgesProcessed - prevNumHorizontalEdgesProcessed;
    double horizontalEdgeSum = _horizontalEdgeSum - originalHorizontalEdgeSum;
    double horizontalEdgeMean = horizontalEdgeSum / horizontalEdgesProcessed;
    addResult(horizontalEdgeMean, "horizontal edges mean for camera " + camera.getId());

    int verticalEdgesProcessed = _numVerticalEdgesProcessed - prevNumVerticalEdgesProcessed;
    if (verticalEdgesProcessed > 1)
    {
      double verticalEdgeSum = _verticalEdgeSum - originalVerticalEdgeSum;
      double verticalEdgeMean = verticalEdgeSum / verticalEdgesProcessed;
      addResult(verticalEdgeMean, "vertical edges mean for camera " + camera.getId());
    }

    setEdgeSpreadFunctionRate(horizontalBeta, verticalBeta, camera);

    System.out.printf("Elapsed time (ms): %d\n", System.currentTimeMillis() - startTime);
  }



  /**
   * Set messages in the Result object. This info is available to any observer
   * who wants to display detailed failure information.
   *
   * @author John Dutton
   */
  private void setResultMessages()
  {
    // Set only if confirmation failed, otherwise let them default to empty strings.
    if (didConfirmationPass() == false)
    {
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_MTF_FAILED_STATUS_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CONFIRM_SYSTEM_MTF_FAILED_ACTION_KEY"));
    }
  }

  /**
   * Method heavily leveraged from Eddie's CalGrayscale list setup since we are
   * interested in the same list of cameras
   *
   * @author Reid Hayhow
   */
  private void readCameraListFromFile() throws DatastoreException
  {
    _setOfCamerasToRunOn.clear();

    SoftwareConfigEnum softwareConfigCameraListEnum = SoftwareConfigEnum.CONFIRM_SYSTEM_MTF_LIST_OF_CAMERAS_TO_RUN_ON;
    //Get the value from the config file
    String cameraList = _config.getStringValue(softwareConfigCameraListEnum);

    //It is ',' delimited, so split it into pieces
    String[] splitCameraList = cameraList.split(",");

    //For every individual camera string, create an Integer and add it to the Set
    for (String cameraNumberString : splitCameraList)
    {
      // The string should be just a number but let's trim it to be
      // forgiving of extra spaces.
      try
      {
        //try to parse the string value to get the camera number
        int cameraId = StringUtil.convertStringToInt(cameraNumberString.trim());

        //If we got here the number parsed and can be added to the list
        XrayCameraIdEnum idEnum = XrayCameraIdEnum.getEnum(new Integer(cameraId));
        _setOfCamerasToRunOn.add(XrayCameraArray.getCamera(idEnum));
      }
      //Handle a parse exception
      catch (BadFormatException ex)
      {
        //Display the enum key (aka name), file name, and the value that wasn't an int
        InvalidValueDatastoreException invalidValueException =
            new InvalidValueDatastoreException(softwareConfigCameraListEnum.getKey(),
                                               cameraNumberString.trim(),
                                               softwareConfigCameraListEnum.getFileName());
        invalidValueException.initCause(ex);
        throw invalidValueException;
      }
    }
  }
  
//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmSharpness will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
