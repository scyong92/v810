package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;

/**
 * @author Cheah Lee Herng
 */
public class ConfirmOpticalCameraLightImageTask extends ConfirmOpticalCameraImageTask 
{
    // Printable name for this procedure
    private static final String NAME =
      StringLocalizer.keyToString("CD_OPTICAL_CAMERA_LIGHT_CONFIRMATION_KEY");
    
    // Call execute() every time this procedure is triggered
    private static final int FREQUENCY = _config.getIntValue(PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_FREQUENCY);

    private static final long TIMEOUTINSECONDS = _config.getLongValue(SoftwareConfigEnum.OPTICAL_CAMERA_LIGHT_IMAGE_CONFIRM_TIMEOUT_IN_SECONDS);

    private static ConfirmOpticalCameraLightImageTask _instance = null;
    
    /**
     * @author Cheah Lee Herng
     */
    private ConfirmOpticalCameraLightImageTask()
    {
        super(NAME,
              FREQUENCY,
              TIMEOUTINSECONDS,
              Directory.getOpticalCameraLightConfirmationLogDir(),
              ProgressReporterEnum.LIGHT_OPTICAL_CAMERA_IMAGE_QUALITY_TASK);

        //This confirm runs on all cameras
        _camerasToGetImagesFromSet = _cameras;

        //This is a dark image task
        _localizedLightOrDarkString = StringLocalizer.keyToString("ACD_LIGHT_KEY");

        //Set the enum used to save the timestamp in the hardware.calib for this confirmation
        setTimestampEnum(PspCalibEnum.OPTICAL_CAMERA_LIGHT_IMAGE_LAST_TIME_RUN);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public static synchronized ConfirmOpticalCameraLightImageTask getInstance()
    {
        if (_instance == null)
        {
          _instance = new ConfirmOpticalCameraLightImageTask();
        }
        return _instance;
    }
    
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
