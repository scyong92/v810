package com.axi.v810.business.autoCal;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.psp.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class CalOpticalMagnification extends EventDrivenHardwareTask implements OpticalLiveVideoInteractionInterface
{
    private static final int OPTICAL_CALIBRATION_JIG_WIDTH_IN_NANOMETERS = 200000000;
    private static final int OPTICAL_CALIBRATION_JIG_HEIGHT_IN_NANOMETERS = 150000000;
    
    private static final int OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_REFERENCE_PLANE_IN_NANOMETERS = 4360000;
    private static final int OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_MINUS_TWO_PLANE_IN_NANOMETERS = 29760000;
    private static final int OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_PLUS_THREE_PLANE_IN_NANOMETERS = 55160000;
    private static final int OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_MINUS_ONE_PLANE_IN_NANOMETERS = 21760000;
    private static final int OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_PLUS_FOUR_PLANE_IN_NANOMETERS = 43760000;
    private static final int OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS = 2600000;
    
    private static CalOpticalMagnification _instance;
    private static Config _config = Config.getInstance();
    
    private PanelHandler _panelHandler = PanelHandler.getInstance();
    private XrayActuatorInt _xrayActuator = XrayActuator.getInstance();
    
    //Motion profile to use when moving stage, use the fastest
    protected PointToPointMotionProfileEnum _motionProfile = PointToPointMotionProfileEnum.PROFILE1;
    
    protected static Map<OpticalCameraIdEnum, AbstractOpticalCamera> _cameraIdToCamerasMap = new LinkedHashMap<OpticalCameraIdEnum, AbstractOpticalCamera>();
    
    //The Limits and Results are mapped by camera and then test type
    protected Map<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, Result>> _cameraIdToTestTypeToResultMap = new LinkedHashMap<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, Result>>();
    protected Map<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, Limit>> _cameraIdToTestTypeToLimitMap = new LinkedHashMap<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, Limit>>();       
    
    //Optical Camera Gain Factor are mapped by camera
    protected Map<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, DoubleRef>> _cameraIdToJigHoleDiameter = new LinkedHashMap<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, DoubleRef>>();
    
    //Optical Camera Magnification are mapped by camera
    protected Map<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, DoubleRef>> _cameraIdToMagnification = new LinkedHashMap<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, DoubleRef>>();
    
    // Printable name for this procedure
    private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_OPTICAL_MAGNIFICATION_NAME_KEY");
    
    // This cal is called on the first event after the timeout occurs.
    private static final int _EVENT_FREQUENCY = 1;
    private static final int _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.OPTICAL_MAGNIFICATION_EXECUTE_EVERY_TIMEOUT_SECONDS);
    
    private static final String _logDirectory = Directory.getOpticalMagnificationCalLogDir();
    private static final long _logTimeoutSeconds = 20000L;
    private static final long _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000L;
    private static final int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.OPTICAL_MAGNIFICATION_MAX_LOG_FILES_IN_DIRECTORY);
    
    private DirectoryLoggerAxi _logger;
    private boolean _areWeLogging = false;
    
    private PanelPositioner _panelPositioner = PanelPositioner.getInstance();
    private MotionProfile _panelPositionerSavedProfile;
    
    private double _magToRestoreToConfig = 0;
    private double _nominaCameraPixelSizeInMicron = 0;
    private double _nominalCalibrationJigHoleDiameterInMilimeter = 0;
    private double _magnification = -1;
    private double _sharpness = -1;
    private double _newMagnification;
    
    private static OpticalLiveVideoObservable _opticalLiveVideoObservable = OpticalLiveVideoObservable.getInstance();
    private BooleanLock  _waitingForOpticalCameraLocationInput = new BooleanLock(false);
    
    protected Map<OpticalCameraIdEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>> _cameraToStagePositionMap = new LinkedHashMap<OpticalCameraIdEnum, Map<OpticalCalibrationPlaneEnum, StagePositionMutable>>();
    
    private TestExecutionTimer _testExecutionTimer;    
    private OpticalImageAcquisitionEngine _opticalImageAcquisitionEngine = OpticalImageAcquisitionEngine.getInstance();
    
    /**
     * @author Cheah Lee Herng
    */
    public static synchronized CalOpticalMagnification getInstance()
    {
        if (_instance == null)
        {
          _instance = new CalOpticalMagnification();
        }
        return _instance;
    }
    
    /**
     * @author Cheah Lee Herng
    */
    private CalOpticalMagnification()
    {
        super(_NAME_OF_CAL,
              _EVENT_FREQUENCY,
              _EXECUTE_EVERY_TIMEOUT_SECONDS,
              ProgressReporterEnum.OPTICAL_MAGNIFICATION_ADJUSTMENT_TASK);
        _logger = new DirectoryLoggerAxi(_logDirectory, FileName.getOpticalMagnificationLogFileBasename(),
                                         FileName.getLogFileExtension(), _maxLogFilesInDirectory);

        _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

        setTimestampEnum(HardwareCalibEnum.REFERENCE_PLANE_MAGNIFICATION_LAST_TIME_RUN);
        
        _testExecutionTimer = new TestExecutionTimer();
    }

    /**
     * @author Cheah Lee Herng
     */
    public boolean canRunOnThisHardware() 
    {
        return true;
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void setUp() throws XrayTesterException 
    {
        //Save off the active motion profile, just in case we end up moving the
        //stage and setting the profile to a point-to-point profile
        _panelPositionerSavedProfile = _panelPositioner.getActiveMotionProfile();
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void cleanUp() throws XrayTesterException 
    {
        //restore the saved profile before continuing
        _panelPositioner.setMotionProfile(_panelPositionerSavedProfile);
        
        if (_cameraToStagePositionMap != null)
            _cameraToStagePositionMap.clear();
        if (_cameraIdToCamerasMap != null)
            _cameraIdToCamerasMap.clear();
        if (_cameraIdToJigHoleDiameter != null)
            _cameraIdToJigHoleDiameter.clear();
        if (_cameraIdToMagnification != null)
            _cameraIdToMagnification.clear();
    }
    
    /**
     *
     * @author Cheah Lee Herng
    */
    protected void createLimitsObject() throws DatastoreException
    {
        _limits = new Limit(null, null, null, _NAME_OF_CAL);
        
        _cameraIdToTestTypeToLimitMap.clear();
        
        buildOpticalCameraList();
        
        //Create the limits maps
        populateLimitsMap();
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    private void populateLimitsMap() throws DatastoreException
    {
        String localizedCameraString = StringLocalizer.keyToString("ACD_OPTICAL_CAMERA_KEY");
        String localizedDiameterValue = StringLocalizer.keyToString("ACD_DIAMETER_KEY");
        String localizedMagnificationValue = StringLocalizer.keyToString("ACD_MAGNIFICATION_KEY");
        
        String localizedDiameter = StringLocalizer.keyToString("ACD_DIAMETER_KEY");
        String localizedMagnification = StringLocalizer.keyToString("ACD_MAGNIFICATION_KEY");
        
        Double referencePlaneJigHoleDiameterHighLimit = _parsedLimits.getHighLimitDouble("ReferencePlaneJigHoleDiameter");
        Double referencePlaneJigHoleDiameterLowLimit = _parsedLimits.getLowLimitDouble("ReferencePlaneJigHoleDiameter");
        Double objectPlaneMinusTwoJigHoleDiameterHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlaneMinusTwoJigHoleDiameter");
        Double objectPlaneMinusTwoJigHoleDiameterLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlaneMinusTwoJigHoleDiameter");
        Double objectPlanePlusThreeJigHoleDiameterHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlanePlusThreeJigHoleDiameter");
        Double objectPlanePlusThreeJigHoleDiameterLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlanePlusThreeJigHoleDiameter");
        Double objectPlaneMinusOneJigHoleDiameterHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlaneMinusOneJigHoleDiameter");
        Double objectPlaneMinusOneJigHoleDiameterLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlaneMinusOneJigHoleDiameter");
        Double objectPlanePlusFourJigHoleDiameterHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlanePlusFourJigHoleDiameter");
        Double objectPlanePlusFourJigHoleDiameterLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlanePlusFourJigHoleDiameter");
        
        Double referencePlaneMagnificationHighLimit = _parsedLimits.getHighLimitDouble("ReferencePlaneMagnification");
        Double referencePlaneMagnificationLowLimit = _parsedLimits.getLowLimitDouble("ReferencePlaneMagnification");
        Double objectPlaneMinusTwoMagnificationHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlaneMinusTwoMagnification");
        Double objectPlaneMinusTwoMagnificationLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlaneMinusTwoMagnification");
        Double objectPlanePlusThreeMagnificationHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlanePlusThreeMagnification");
        Double objectPlanePlusThreeMagnificationLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlanePlusThreeMagnification");
        Double objectPlaneMinusOneMagnificationHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlaneMinusOneMagnification");
        Double objectPlaneMinusOneMagnificationLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlaneMinusOneMagnification");
        Double objectPlanePlusFourMagnificationHighLimit = _parsedLimits.getHighLimitDouble("ObjectPlanePlusFourMagnification");
        Double objectPlanePlusFourMagnificationLowLimit = _parsedLimits.getLowLimitDouble("ObjectPlanePlusFourMagnification");
        
        for(Map.Entry<OpticalCameraIdEnum, AbstractOpticalCamera> entry : _cameraIdToCamerasMap.entrySet())
        {
            Map<OpticalCameraImageDataTypeEnum, Limit> mapOfTestsToLimits = new TreeMap<OpticalCameraImageDataTypeEnum, Limit>();
            _cameraIdToTestTypeToLimitMap.put(entry.getKey(), mapOfTestsToLimits);
            
            Limit cameraContainer = new Limit(null, null, null, localizedCameraString + entry.getValue().getId());
            _limits.addSubLimit(cameraContainer);
            
            Limit referencePlaneJigHole = new Limit(referencePlaneJigHoleDiameterLowLimit,
                                              referencePlaneJigHoleDiameterHighLimit,
                                              localizedDiameterValue,
                                              localizedDiameter);
            cameraContainer.addSubLimit(referencePlaneJigHole);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER, referencePlaneJigHole);
            
            Limit objectPlaneMinusTwoJigHole = new Limit(objectPlaneMinusTwoJigHoleDiameterLowLimit,
                                                   objectPlaneMinusTwoJigHoleDiameterHighLimit,
                                                   localizedDiameterValue,
                                                   localizedDiameter);
            cameraContainer.addSubLimit(objectPlaneMinusTwoJigHole);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER, objectPlaneMinusTwoJigHole);
            
            Limit objectPlanePlusThreeJigHole = new Limit(objectPlanePlusThreeJigHoleDiameterLowLimit,
                                                       objectPlanePlusThreeJigHoleDiameterHighLimit,
                                                       localizedDiameterValue,
                                                       localizedDiameter);
            cameraContainer.addSubLimit(objectPlanePlusThreeJigHole);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER, objectPlanePlusThreeJigHole);
            
            Limit objectPlaneMinusOneJigHole = new Limit(objectPlaneMinusOneJigHoleDiameterLowLimit,
                                                       objectPlaneMinusOneJigHoleDiameterHighLimit,
                                                       localizedDiameterValue,
                                                       localizedDiameter);
            cameraContainer.addSubLimit(objectPlaneMinusOneJigHole);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER, objectPlaneMinusOneJigHole);
            
            Limit objectPlanePlusFourJigHole = new Limit(objectPlanePlusFourJigHoleDiameterLowLimit,
                                                       objectPlanePlusFourJigHoleDiameterHighLimit,
                                                       localizedDiameterValue,
                                                       localizedDiameter);
            cameraContainer.addSubLimit(objectPlanePlusFourJigHole);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER, objectPlanePlusFourJigHole);
            
            Limit referencePlaneMagnification = new Limit(referencePlaneMagnificationLowLimit,
                                                          referencePlaneMagnificationHighLimit,
                                                          localizedMagnificationValue,
                                                          localizedMagnification);
            cameraContainer.addSubLimit(referencePlaneMagnification);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION, referencePlaneMagnification);
            
            Limit objectPlaneMinusTwoMagnifiation = new Limit(objectPlaneMinusTwoMagnificationLowLimit,
                                                              objectPlaneMinusTwoMagnificationHighLimit,
                                                              localizedMagnificationValue,
                                                              localizedMagnification);
            cameraContainer.addSubLimit(objectPlaneMinusTwoMagnifiation);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION, objectPlaneMinusTwoMagnifiation);
            
            Limit objectPlanePlusThreeMagnification = new Limit(objectPlanePlusThreeMagnificationLowLimit,
                                                                objectPlanePlusThreeMagnificationHighLimit,
                                                                localizedMagnificationValue,
                                                                localizedMagnification);
            cameraContainer.addSubLimit(objectPlanePlusThreeMagnification);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION, objectPlanePlusThreeMagnification);
            
            Limit objectPlaneMinusOneMagnification = new Limit(objectPlaneMinusOneMagnificationLowLimit,
                                                                objectPlaneMinusOneMagnificationHighLimit,
                                                                localizedMagnificationValue,
                                                                localizedMagnification);
            cameraContainer.addSubLimit(objectPlaneMinusOneMagnification);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION, objectPlaneMinusOneMagnification);
            
            Limit objectPlanePlusFourMagnification = new Limit(objectPlanePlusFourMagnificationLowLimit,
                                                                objectPlanePlusFourMagnificationHighLimit,
                                                                localizedMagnificationValue,
                                                                localizedMagnification);
            cameraContainer.addSubLimit(objectPlanePlusFourMagnification);
            mapOfTestsToLimits.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION, objectPlanePlusFourMagnification);
        }
    }

    /**
     * @author Cheah Lee Herng 
     */
    protected void createResultsObject() throws DatastoreException 
    {
        _result = new Result(_NAME_OF_CAL);

        _cameraIdToTestTypeToResultMap.clear();
        
        buildOpticalCameraList();
        
        //Populate Result Map
        populateResultMap();
    }
    
    /**
     * Method to populate the result map for a specific camera direction.
     * 
     * @author Cheah Lee Herng
     */
    private void populateResultMap()
    {
        Assert.expect(_cameraIdToCamerasMap != null);
        
        //Get all of the localized strings we will need for the results
        //First, the the common building block keys
        String localizedCameraString = StringLocalizer.keyToString("ACD_OPTICAL_CAMERA_KEY");
        
        String localizedDiameter = StringLocalizer.keyToString("ACD_DIAMETER_KEY");        
        String localizedMagnification = StringLocalizer.keyToString("ACD_MAGNIFICATION_KEY");
        
        for(Map.Entry<OpticalCameraIdEnum, AbstractOpticalCamera> entry : _cameraIdToCamerasMap.entrySet())
        {
            Map<OpticalCameraImageDataTypeEnum, Result> mapOfResultsToTests = new TreeMap<OpticalCameraImageDataTypeEnum, Result>();
            _cameraIdToTestTypeToResultMap.put(entry.getKey(), mapOfResultsToTests);
            
            Result cameraResultContainer = new Result(localizedCameraString + entry.getValue().getId());
            _result.addSubResult(cameraResultContainer);
            
            Result referencePlaneDiameterResult = new Result(new Double(220.0), localizedDiameter);
            cameraResultContainer.addSubResult(referencePlaneDiameterResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER, referencePlaneDiameterResult);
            
            Result objectPlaneMinusTwoDiameterResult = new Result(new Double(220.0), localizedDiameter);
            cameraResultContainer.addSubResult(objectPlaneMinusTwoDiameterResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER, objectPlaneMinusTwoDiameterResult);
            
            Result objectPlanePlusThreeDiameterResult = new Result(new Double(220.0), localizedDiameter);
            cameraResultContainer.addSubResult(objectPlanePlusThreeDiameterResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER, objectPlanePlusThreeDiameterResult);
            
            Result objectPlaneMinusOneDiameterResult = new Result(new Double(220.0), localizedDiameter);
            cameraResultContainer.addSubResult(objectPlaneMinusOneDiameterResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER, objectPlaneMinusOneDiameterResult);
            
            Result objectPlanePlusFourDiameterResult = new Result(new Double(220.0), localizedDiameter);
            cameraResultContainer.addSubResult(objectPlanePlusFourDiameterResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER, objectPlanePlusFourDiameterResult);
            
            Result referencePlaneMagnificationResult = new Result(new Double(1.0), localizedMagnification);
            cameraResultContainer.addSubResult(referencePlaneMagnificationResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION, referencePlaneMagnificationResult);
            
            Result objectPlaneMinusTwoMagnificationResult = new Result(new Double(1.0), localizedMagnification);
            cameraResultContainer.addSubResult(objectPlaneMinusTwoMagnificationResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION, objectPlaneMinusTwoMagnificationResult);
            
            Result objectPlanePlusThreeMagnificationResult = new Result(new Double(1.0), localizedMagnification);
            cameraResultContainer.addSubResult(objectPlanePlusThreeMagnificationResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION, objectPlanePlusThreeMagnificationResult);
            
            Result objectPlaneMinusOneMagnificationResult = new Result(new Double(1.0), localizedMagnification);
            cameraResultContainer.addSubResult(objectPlaneMinusOneMagnificationResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION, objectPlaneMinusOneMagnificationResult);
            
            Result objectPlanePlusFourMagnificationResult = new Result(new Double(1.0), localizedMagnification);
            cameraResultContainer.addSubResult(objectPlanePlusFourMagnificationResult);
            mapOfResultsToTests.put(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION, objectPlanePlusFourMagnificationResult);
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void executeTask() throws XrayTesterException 
    {
        boolean deferred = false;
        
        if (UnitTest.unitTesting() == true && _config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true)
        {
          //Create a passing result
          String localizedTaskName = getName();
          _limits = new Limit(null, null, null, localizedTaskName);
          _result = new Result(new Boolean(true), localizedTaskName);
          return;
        }
        
        _areWeLogging = _logger.isLoggingNeeded(_logTimeoutMilliSeconds);
        
        _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_OPTICAL_MAGNIFICATION);
        _hardwareObservable.setEnabled(false);
        reportProgress(10);
        
        if(PspEngine.getPspCalibrationVersionEnum().equals(PspCalibrationVersionEnum.VERSION_2))
        {
          PanelHandler.getInstance().setRailWidthInNanoMeters(304800000);
        }
        else
        {
          loadOpticalCalibrationJig();
        
          checkAbortInProgress();

          //Handle the fact that we cannot run this task without a calibration
          //jig loaded into the system. Defer the task till later *but* don't cause the
          //task to fail. Display a meaningful message in the Service UI
          if (ableToRunWithCurrentPanelState() == false)
          {
            _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
            _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_NO_CALIBRATION_JIG_LOADED_KEY"));
            _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_LOAD_CALIBRATION_JIG_AND_TRY_AGAIN_SUGGESTED_ACTION_KEY"));
            return;
          }
        }
        
        try
        {            
            // Save old mag to restore later
            _magToRestoreToConfig = _config.getDoubleValue(PspCalibEnum.OPTICAL_REFERENCE_PLANE_MAGNIFICATION);
            _nominaCameraPixelSizeInMicron = _config.getDoubleValue(PspCalibEnum.OPTICAL_CAMERA_PIXEL_SIZE);
            _nominalCalibrationJigHoleDiameterInMilimeter = _config.getDoubleValue(PspCalibEnum.OPTICAL_NOMINAL_CALIBRATION_JIG_HOLE_DIAMETER);
            
            reportProgress(20);
            
            //Setup cal magnification stage position
            setupCalMagnificationStagePosition();

            //Setup camera list
            buildOpticalCameraList();
            
            reportProgress(40);
            
            getAndAnalyzeImageMap();
            
            reportProgress(100);
        }
        catch (XrayTesterException ex)
        {
            _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
            deferred = true;
            return;
        }
        //Log any unexpected exceptions
        catch (Exception e)
        {
            Assert.logException(e);
        }
        finally
        {
            // Save jig hole diameter to configuration file
            saveJigHoleDiameterToConfig();
            
            // Save magnification to configuration file
            saveMagnificationToConfig();
            
            //For now force logging
            if(deferred == false)
              logInformation(_forceLogging);
            
            //Restore stage (if needed)
            moveStageAfterExecute();
            
            //Make sure projector is off before leaving
            setProjectorAfterExecute();
            
            // Restore panel safety checking anyhow
            _panelHandler.unloadPanel();
        }
        
        _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_OPTICAL_MAGNIFICATION);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void setupCalMagnificationStagePosition()
    {
        Assert.expect(_cameraToStagePositionMap != null);
        
        _cameraToStagePositionMap.clear();
        
        for(OpticalCameraIdEnum cameraId : OpticalCameraIdEnum.getAllEnums())
        {
            Map<OpticalCalibrationPlaneEnum, StagePositionMutable> calMagnificationToStagePositionMap = new HashMap<OpticalCalibrationPlaneEnum, StagePositionMutable>();
            Object object = _cameraToStagePositionMap.put(cameraId, calMagnificationToStagePositionMap);
            Assert.expect(object == null);
            
            if (cameraId == OpticalCameraIdEnum.OPTICAL_CAMERA_1)
            {
                StagePositionMutable calMagnificationReferencePlaneStagePositionForOpticalCamera1 = new StagePositionMutable();
                calMagnificationReferencePlaneStagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_REFERENCE_PLANE_IN_NANOMETERS);
                calMagnificationReferencePlaneStagePositionForOpticalCamera1.setYInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, calMagnificationReferencePlaneStagePositionForOpticalCamera1);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane1StagePositionForOpticalCamera1 = new StagePositionMutable();
                calMagnificationObjectPlane1StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_MINUS_TWO_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane1StagePositionForOpticalCamera1.setYInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO, calMagnificationObjectPlane1StagePositionForOpticalCamera1);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane2StagePositionForOpticalCamera1 = new StagePositionMutable();
                calMagnificationObjectPlane2StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() - OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_PLUS_THREE_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane2StagePositionForOpticalCamera1.setYInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, calMagnificationObjectPlane2StagePositionForOpticalCamera1);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane3StagePositionForOpticalCamera1 = new StagePositionMutable();
                calMagnificationObjectPlane3StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() + OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_MINUS_ONE_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane3StagePositionForOpticalCamera1.setYInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE, calMagnificationObjectPlane3StagePositionForOpticalCamera1);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane4StagePositionForOpticalCamera1 = new StagePositionMutable();
                calMagnificationObjectPlane4StagePositionForOpticalCamera1.setXInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationXInNanometers() + OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_PLUS_FOUR_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane4StagePositionForOpticalCamera1.setYInNanometers(PspConfiguration.getInstance().getFrontOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR, calMagnificationObjectPlane4StagePositionForOpticalCamera1);
                Assert.expect(object == null);
                
                //if (Config.isDeveloperDebugModeOn())
                //{
                    System.out.println("CalOpticalMagnification: Camera 1: Ref Plane X = " + calMagnificationReferencePlaneStagePositionForOpticalCamera1.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 1: Ref Plane Y = " + calMagnificationReferencePlaneStagePositionForOpticalCamera1.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 1: Minus Two Plane X = " + calMagnificationObjectPlane1StagePositionForOpticalCamera1.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 1: Minus Two Plane Y = " + calMagnificationObjectPlane1StagePositionForOpticalCamera1.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 1: Plus Three Plane X = " + calMagnificationObjectPlane2StagePositionForOpticalCamera1.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 1: Plus Three Plane Y = " + calMagnificationObjectPlane2StagePositionForOpticalCamera1.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 1: Minus One Plane X = " + calMagnificationObjectPlane3StagePositionForOpticalCamera1.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 1: Minus One Plane Y = " + calMagnificationObjectPlane3StagePositionForOpticalCamera1.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 1: Plus Four Plane X = " + calMagnificationObjectPlane4StagePositionForOpticalCamera1.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 1: Plus Four Plane Y = " + calMagnificationObjectPlane4StagePositionForOpticalCamera1.getYInNanometers());
                //}
            }
            else if (cameraId == OpticalCameraIdEnum.OPTICAL_CAMERA_2)
            {
                StagePositionMutable calMagnificationReferencePlaneStagePositionForOpticalCamera2 = new StagePositionMutable();
                calMagnificationReferencePlaneStagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_REFERENCE_PLANE_IN_NANOMETERS);
                calMagnificationReferencePlaneStagePositionForOpticalCamera2.setYInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.REFERENCE_PLANE, calMagnificationReferencePlaneStagePositionForOpticalCamera2);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane1StagePositionForOpticalCamera2 = new StagePositionMutable();
                calMagnificationObjectPlane1StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_MINUS_TWO_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane1StagePositionForOpticalCamera2.setYInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO, calMagnificationObjectPlane1StagePositionForOpticalCamera2);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane2StagePositionForOpticalCamera2 = new StagePositionMutable();
                calMagnificationObjectPlane2StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() - OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_PLUS_THREE_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane2StagePositionForOpticalCamera2.setYInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE, calMagnificationObjectPlane2StagePositionForOpticalCamera2);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane3StagePositionForOpticalCamera2 = new StagePositionMutable();
                calMagnificationObjectPlane3StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() + OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_MINUS_ONE_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane3StagePositionForOpticalCamera2.setYInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE, calMagnificationObjectPlane3StagePositionForOpticalCamera2);
                Assert.expect(object == null);
                
                StagePositionMutable calMagnificationObjectPlane4StagePositionForOpticalCamera2 = new StagePositionMutable();
                calMagnificationObjectPlane4StagePositionForOpticalCamera2.setXInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationXInNanometers() + OFFSET_X_FROM_OPTICAL_FIDUCIAL_TO_CALIBRATION_HOLE_ON_PLUS_FOUR_PLANE_IN_NANOMETERS);
                calMagnificationObjectPlane4StagePositionForOpticalCamera2.setYInNanometers(PspConfiguration.getInstance().getRearOpticalFiducialStageCoordinateLocationYInNanometers() - OFFSET_Y_FROM_OPTICAL_FIDUCIAL_TO_PLANE_IN_NANOMETERS);
                object = calMagnificationToStagePositionMap.put(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR, calMagnificationObjectPlane4StagePositionForOpticalCamera2);
                Assert.expect(object == null);
                
                if (Config.isDeveloperDebugModeOn())
                {
                    System.out.println("CalOpticalMagnification: Camera 2: Ref Plane X = " + calMagnificationReferencePlaneStagePositionForOpticalCamera2.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 2: Ref Plane Y = " + calMagnificationReferencePlaneStagePositionForOpticalCamera2.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 2: Minus Two Plane X = " + calMagnificationObjectPlane1StagePositionForOpticalCamera2.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 2: Minus Two Plane Y = " + calMagnificationObjectPlane1StagePositionForOpticalCamera2.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 2: Plus Three Plane X = " + calMagnificationObjectPlane2StagePositionForOpticalCamera2.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 2: Plus Three Plane Y = " + calMagnificationObjectPlane2StagePositionForOpticalCamera2.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 2: Minus One Plane X = " + calMagnificationObjectPlane3StagePositionForOpticalCamera2.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 2: Minus One Plane Y = " + calMagnificationObjectPlane3StagePositionForOpticalCamera2.getYInNanometers());
                    
                    System.out.println("CalOpticalMagnification: Camera 2: Plus Four Plane X = " + calMagnificationObjectPlane4StagePositionForOpticalCamera2.getXInNanometers());
                    System.out.println("CalOpticalMagnification: Camera 2: Plus Four Plane Y = " + calMagnificationObjectPlane4StagePositionForOpticalCamera2.getYInNanometers());
                }
            }
        }         
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void moveStage(OpticalCameraIdEnum opticalCameraIdEnum, OpticalCalibrationPlaneEnum calMagnificationEnum) throws XrayTesterException
    {
        Assert.expect(opticalCameraIdEnum != null);
        Assert.expect(calMagnificationEnum != null);
        
        // panel needs to be repositioned left
        if (_panelHandler.isPanelLoaded())
          _panelHandler.movePanelToLeftSide();
        
        //Set up the stage to get it out of the way of the cameras
        Map<OpticalCalibrationPlaneEnum, StagePositionMutable> calMagnificationEnumToStagePositionMap = _cameraToStagePositionMap.get(opticalCameraIdEnum) ;
        StagePositionMutable stagePosition = calMagnificationEnumToStagePositionMap.get(calMagnificationEnum) ;
        
        //Profile1 is fastest
        PanelPositioner.getInstance().setMotionProfile(new MotionProfile(_motionProfile));
        
        // Move stage to existing center position before performing CD&A
        PanelPositioner.getInstance().pointToPointMoveAllAxes(stagePosition);
    }
    
    /**
     * Method that controls stage motion after task execution.
     *
     * @author Cheah Lee Herng
    */
    protected void moveStageAfterExecute()
    {
        //Do nothing
    }
    
    /**
     * Always make sure projector is off before leaving this task.
     * 
     * @author Cheah Lee Herng
     */
    protected void setProjectorAfterExecute() throws XrayTesterException
    {
        PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspModuleEnum.FRONT, OpticalShiftNameEnum.WHITE_LIGHT);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void buildOpticalCameraList()
    {
        _cameraIdToCamerasMap.clear();
        
        _cameraIdToCamerasMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_1, AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_1));
        _cameraIdToCamerasMap.put(OpticalCameraIdEnum.OPTICAL_CAMERA_2, AbstractOpticalCamera.getInstance(OpticalCameraIdEnum.OPTICAL_CAMERA_2));
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private String getOpticalMagnificationReferencePlaneImageFullPath(int cameraId, int cameraDeviceId, int planeId, int index) throws XrayTesterException
    {
        return FileName.getOpticalMagnificationReferencePlaneImageFullPath(cameraId, cameraDeviceId, planeId, index);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private String getOpticalMagnificationSobelTempImageFullPath(int cameraId, int planeId) throws XrayTesterException
    {
        return FileName.getOpticalMagnificationSobelTempImageFullPath(cameraId, planeId);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private String getOpticalMagnificationObjectPlaneMinusTwoImageFullPath(int cameraId, int cameraDeviceId, int planeId, int index) throws XrayTesterException
    {
        return FileName.getOpticalMagnificationObjectPlaneMinusTwoImageFullPath(cameraId, cameraDeviceId, planeId, index);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private String getOpticalMagnificationObjectPlanePlusThreeImageFullPath(int cameraId, int cameraDeviceId, int planeId, int index) throws XrayTesterException
    {
        return FileName.getOpticalMagnificationObjectPlanePlusThreeImageFullPath(cameraId, cameraDeviceId, planeId, index);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private String getOpticalMagnificationObjectPlaneMinusOneImageFullPath(int cameraId, int cameraDeviceId, int planeId, int index) throws XrayTesterException
    {
        return FileName.getOpticalMagnificationObjectPlaneMinusOneImageFullPath(cameraId, cameraDeviceId, planeId, index);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private String getOpticalMagnificationObjectPlanePlusFourImageFullPath(int cameraId, int cameraDeviceId, int planeId, int index) throws XrayTesterException
    {
        return FileName.getOpticalMagnificationObjectPlanePlusFourImageFullPath(cameraId, cameraDeviceId, planeId, index);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void getAndAnalyzeImageMap() throws XrayTesterException
    {
        Assert.expect(_cameraIdToCamerasMap != null);
        
        for(Map.Entry<OpticalCameraIdEnum, AbstractOpticalCamera> entry : _cameraIdToCamerasMap.entrySet())
        {
            OpticalCameraIdEnum cameraId = entry.getKey();
            AbstractOpticalCamera opticalCamera = entry.getValue();
            
            PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);
            
            // Start by moving stage to 'O' Reference Plane
            moveStage(cameraId, OpticalCalibrationPlaneEnum.REFERENCE_PLANE);
            
            _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                          cameraId,
                                                          StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"), 
                                                          this, false, false, false));
            
            waitForOpticalCameraLocationInput();
            
            // Here we check if user cancel live video so that
            // we can exit the test gracefully
            checkAbortInProgress();
            
            // Send event to close live video dialog
            _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));
            
            if (_taskCancelled == false)
            {
                // Start acquiring reference plane magnification images
                acquireOpticalMagnficationImage(opticalCamera, OpticalCalibrationPlaneEnum.REFERENCE_PLANE);

                String  imageFullPath = getOpticalMagnificationReferencePlaneImageFullPath(opticalCamera.getId(), 
                                                                           opticalCamera.getOpticalCameraIdEnum().getDeviceId(),
                                                                           OpticalCalibrationPlaneEnum.REFERENCE_PLANE.getId(),
                                                                           1);
              
                // Start analyze reference plane magnifiation images
                analyzeImage(opticalCamera,
                             OpticalCalibrationPlaneEnum.REFERENCE_PLANE,
                             imageFullPath,
                             OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER,
                             OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION);
            
                PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);
                PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);

                // Next, move stage to -2 object plane
                moveStage(cameraId, OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO);

                _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                              cameraId,
                                                              StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"),
                                                              this, false, false, false));

                waitForOpticalCameraLocationInput();

                // Here we check if user cancel live video so that
                // we can exit the test gracefully
                checkAbortInProgress();

                // Send event to close live video dialog
                _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));

                if (_taskCancelled == false)
                {
                    // Start acquiring object plane -2 images
                    acquireOpticalMagnficationImage(opticalCamera, OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO);

                    imageFullPath = getOpticalMagnificationReferencePlaneImageFullPath(opticalCamera.getId(), 
                                                                           opticalCamera.getOpticalCameraIdEnum().getDeviceId(),
                                                                           OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO.getId(),
                                                                           1);
                    // Start analyze reference plane magnifiation images
                    analyzeImage(opticalCamera,
                                 OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO,
                                 imageFullPath,
                                 OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER,
                                 OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION);
                    
                    PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);
                    PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);

                    // Next, move stage to +3 object plane
                    moveStage(cameraId, OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE);

                    _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                                  cameraId,
                                                                  StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"), 
                                                                  this, false, false, false));

                    waitForOpticalCameraLocationInput();

                    // Here we check if user cancel live video so that
                    // we can exit the test gracefully
                    checkAbortInProgress();

                    // Send event to close live video dialog
                    _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));
                    
                    if (_taskCancelled == false)
                    {
                        // Start acquiring object plane +3 images
                        acquireOpticalMagnficationImage(opticalCamera, OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE);                        

                        imageFullPath = getOpticalMagnificationReferencePlaneImageFullPath(opticalCamera.getId(), 
                                                                           opticalCamera.getOpticalCameraIdEnum().getDeviceId(),
                                                                           OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE.getId(),
                                                                           1);
                         
                        // Start analyze reference plane magnifiation images
                        analyzeImage(opticalCamera,
                                     OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE,
                                     imageFullPath,
                                     OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER,
                                     OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION);
                        
                        PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);
                        PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);

                        // Next, move stage to -1 object plane
                        moveStage(cameraId, OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE);

                        _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                                      cameraId,
                                                                      StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"), 
                                                                      this, false, false, false));

                        waitForOpticalCameraLocationInput();

                        // Here we check if user cancel live video so that
                        // we can exit the test gracefully
                        checkAbortInProgress();

                        // Send event to close live video dialog
                        _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));
                        
                        if (_taskCancelled == false)
                        {
                            // Start acquiring object plane -1 images
                            acquireOpticalMagnficationImage(opticalCamera, OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE);                        

                            imageFullPath = getOpticalMagnificationReferencePlaneImageFullPath(opticalCamera.getId(), 
                                                                           opticalCamera.getOpticalCameraIdEnum().getDeviceId(),
                                                                           OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE.getId(),
                                                                           1);
                            // Start analyze reference plane magnifiation images
                            analyzeImage(opticalCamera,
                                         OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE,
                                         imageFullPath,
                                         OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER,
                                         OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION);
                            
                            PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);
                            PspImageAcquisitionEngine.getInstance().handleActualOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);

                            // Next, move stage to +4 object plane
                            moveStage(cameraId, OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR);

                            _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.DISPLAY_LIVE_VIDEO, 
                                                                          cameraId,
                                                                          StringLocalizer.keyToString("GUI_SAVE_BUTTON_KEY"), 
                                                                          this, false, false, false));

                            waitForOpticalCameraLocationInput();

                            // Here we check if user cancel live video so that
                            // we can exit the test gracefully
                            checkAbortInProgress();

                            // Send event to close live video dialog
                            _opticalLiveVideoObservable.sendEvent(new OpticalLiveVideoInfo(OpticalLiveVideoEnum.CLOSE_LIVE_VIDEO));

                            if (_taskCancelled == false)
                            {
                                // Start acquiring object plane +4 images
                                acquireOpticalMagnficationImage(opticalCamera, OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR);                        

                                imageFullPath = getOpticalMagnificationReferencePlaneImageFullPath(opticalCamera.getId(), 
                                                                           opticalCamera.getOpticalCameraIdEnum().getDeviceId(),
                                                                           OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR.getId(),
                                                                           1);
                                // Start analyze reference plane magnifiation images
                                analyzeImage(opticalCamera,
                                             OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR,
                                             imageFullPath,
                                             OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER,
                                             OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION);
                                
                                PspImageAcquisitionEngine.getInstance().handlePostOpticalView(PspEngine.getInstance().getPspModuleEnum(cameraId), OpticalShiftNameEnum.WHITE_LIGHT);
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void analyzeImage(OpticalCameraIdEnum cameraId,
                              OpticalCalibrationPlaneEnum calibrationPlane,
                              OpticalCameraImageDataTypeEnum opticalCameraImageDataTypeEnumForJigHoleDiameter,
                              OpticalCameraImageDataTypeEnum opticalCameraImageDataTypeEnumForMagnification,
                              String imageFullPath) throws XrayTesterException
    {      
        Image imageToAnalyze = null;
        try
        {
          final int REGION_HEIGHT = 10;
          final int REGION_WIDTH  = 10;

        
          //Get the preCreated map of results
          Map<OpticalCameraImageDataTypeEnum, Result> mapOfResults = _cameraIdToTestTypeToResultMap.get(cameraId);

          //Also get the limits so we can check them and know if this image passed or failed
          Map<OpticalCameraImageDataTypeEnum, Limit> mapOfLimits = _cameraIdToTestTypeToLimitMap.get(cameraId);

          Assert.expect(mapOfResults != null);
          Assert.expect(mapOfLimits != null);
          Assert.expect(cameraId != null);
          Assert.expect(calibrationPlane != null);
        
          // Convert to PNG 
          java.awt.image.BufferedImage myPicture = null;
          try
          {
            myPicture = javax.imageio.ImageIO.read(new java.io.File(imageFullPath));
          }
          catch (IOException ex)
          {
            // nothing we can do
          }

          XrayImageIoUtil.savePngImage(myPicture, imageFullPath);
          
          //imageToAnalyze = XrayImageIoUtil.loadPngImage("C:\\test.png");
          
          imageToAnalyze = XrayImageIoUtil.loadPngImage(imageFullPath);
        
          // Perform Median-Filter on source image
          int kernelWidth = 15;
          int kernelHeight = 15;
          RegionOfInterest regionOfInterestToApplyMedian = RegionOfInterest.createRegionFromImage(imageToAnalyze);
          regionOfInterestToApplyMedian.setWidthKeepingSameMinX(imageToAnalyze.getWidth() - kernelWidth + 1);
          regionOfInterestToApplyMedian.setHeightKeepingSameMinY(imageToAnalyze.getHeight() - kernelHeight + 1);
          Image aggregateMedianImage = Filter.median(imageToAnalyze, regionOfInterestToApplyMedian, kernelWidth, kernelHeight);

          // Save Median filter image
          XrayImageIoUtil.savePngImage(aggregateMedianImage, "C:\\test_median.png");
          
          //apply image thresholding
          int numHistogramBins = 256;
          int[] histogram = Threshold.histogram(aggregateMedianImage, RegionOfInterest.createRegionFromImage(aggregateMedianImage), numHistogramBins);
          int level = Threshold.getAutoThreshold(histogram);

          Image thresholdImage = new Image(aggregateMedianImage.getWidth(), aggregateMedianImage.getHeight());
          Transform.copyImageIntoImage(aggregateMedianImage, thresholdImage);
          Threshold.threshold(thresholdImage, (float) level, 255.0f, level, 0.0f); 
          
          // Save Median filter image
          XrayImageIoUtil.savePngImage(thresholdImage, "C:\\test_threshold.png");
         
          // Apply Sobel Edge Detection
          RegionOfInterest regionOfInterestToApplySobelEdgeDetection = com.axi.util.image.RegionOfInterest.createRegionFromImage(thresholdImage);
          Image sobelEdgeDetectionImage = Filter.convolveSobelCompass(thresholdImage, regionOfInterestToApplySobelEdgeDetection);

          //Image sobelEdgeDetectionImage = XrayImageIoUtil.loadPngImage("C:\\test_sobel.png");
          // Save Sobel edge filter image
          XrayImageIoUtil.savePngImage(sobelEdgeDetectionImage, "C:\\test_sobel.png");
        
          // Forward (Left to Right)
          RegionOfInterest horizontalProfileRegion = RegionOfInterest.createRegionFromImage(sobelEdgeDetectionImage);
          horizontalProfileRegion.setWidthKeepingSameCenter(sobelEdgeDetectionImage.getWidth());
          //horizontalProfileRegion.setHeightKeepingSameCenter(REGION_HEIGHT);
          horizontalProfileRegion.setHeightKeepingSameCenter(sobelEdgeDetectionImage.getHeight());
          horizontalProfileRegion.setOrientationInDegrees(0);
          
          RegionOfInterest forwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(horizontalProfileRegion, sobelEdgeDetectionImage);
          
          float[] forwardCircleProfile = ImageFeatureExtraction.circleProfile(sobelEdgeDetectionImage, forwardTruncatedRegion);
          float[] forwardProfile = ProfileUtil.getSmoothedProfile(forwardCircleProfile, 3);
          ProfileUtil.removeBackgroundTrendFromProfile(forwardProfile);
          
          // Reverse (Right to Left)
          horizontalProfileRegion.setOrientationInDegrees(180);
          RegionOfInterest reverseTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(horizontalProfileRegion, sobelEdgeDetectionImage);
          
          float[] reverseCircleProfile = ImageFeatureExtraction.circleProfile(sobelEdgeDetectionImage, reverseTruncatedRegion);
          float[] reverseProfile = ProfileUtil.getSmoothedProfile(reverseCircleProfile, 3);
          ProfileUtil.removeBackgroundTrendFromProfile(reverseProfile);
          
          // DOWNWARD
          RegionOfInterest verticalProfileRegion = RegionOfInterest.createRegionFromImage(sobelEdgeDetectionImage);
          //verticalProfileRegion.setWidthKeepingSameCenter(REGION_WIDTH);
          verticalProfileRegion.setWidthKeepingSameCenter(sobelEdgeDetectionImage.getWidth());
          verticalProfileRegion.setHeightKeepingSameCenter(sobelEdgeDetectionImage.getHeight());

          verticalProfileRegion.setOrientationInDegrees(90);
          RegionOfInterest downwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(verticalProfileRegion, sobelEdgeDetectionImage);
          
          float[] downwardCircleProfile = ImageFeatureExtraction.circleProfile(sobelEdgeDetectionImage, downwardTruncatedRegion);
          float[] downwardProfile = ProfileUtil.getSmoothedProfile(downwardCircleProfile, 3);
          ProfileUtil.removeBackgroundTrendFromProfile(downwardProfile);
          
          // UPWARDS
          verticalProfileRegion.setOrientationInDegrees(270);
          RegionOfInterest upwardTruncatedRegion = ImageFeatureExtraction.truncateRegionOfInterestToImageForCircleProfile(verticalProfileRegion, sobelEdgeDetectionImage);
          
          float[] upwardCircleProfile = ImageFeatureExtraction.circleProfile(sobelEdgeDetectionImage, upwardTruncatedRegion);
          float[] upwardProfile = ProfileUtil.getSmoothedProfile(upwardCircleProfile, 3);
          ProfileUtil.removeBackgroundTrendFromProfile(upwardProfile);
          
          // Use Subpixel edge location
          float forwardEdgeInXPixelCoordinates = forwardTruncatedRegion.getMinX() +
                                                  ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(forwardProfile, (float)0.9);
          float reverseEdgeInXPixelCoordinates = reverseTruncatedRegion.getMaxX() -
                                                  ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(reverseProfile, (float)0.9);
          float downwardEdgeInYPixelCoordinates = downwardTruncatedRegion.getMinY() +
                                                  ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(downwardProfile, (float)0.9);
          float upwardEdgeInYPixelCoordinates = upwardTruncatedRegion.getMaxY() -
                                                  ProfileUtil.findSubpixelEdgeLocationBasedOnFractionOfRange(upwardProfile, (float)0.9);
          
          int roundedForwardEdgeInXPixelCoordinates = Math.round(forwardEdgeInXPixelCoordinates);
          int roundedDownwardEdgeInYPixelCoordinates = Math.round(downwardEdgeInYPixelCoordinates);
          int roundedReverseEdgeInXPixelCoordinates = Math.round(reverseEdgeInXPixelCoordinates);
          int roundedUpwardEdgeInYPixelCoordinates = Math.round(upwardEdgeInYPixelCoordinates);      

          double diameterInPixels = 0.0f;
          double magnification = 0.0f;
          
          if ((roundedForwardEdgeInXPixelCoordinates < roundedReverseEdgeInXPixelCoordinates) &&
              (roundedDownwardEdgeInYPixelCoordinates < roundedUpwardEdgeInYPixelCoordinates))
          {
            float horizontalDiameter = reverseEdgeInXPixelCoordinates - forwardEdgeInXPixelCoordinates;
            diameterInPixels = horizontalDiameter;
    
            float verticalDiameter = upwardEdgeInYPixelCoordinates - downwardEdgeInYPixelCoordinates;
            float diameterInPixels1 = (horizontalDiameter + verticalDiameter) * 0.5f;
            float diameterInMils = MathUtil.convertNanoMetersToMils(diameterInPixels1
                    * MagnificationEnum.getCurrentNorminal().getNanoMetersPerPixel());

            // Calculate area of jig hole
            magnification = (diameterInPixels1 * _nominaCameraPixelSizeInMicron) / (_nominalCalibrationJigHoleDiameterInMilimeter * 1000);
          }
          
          // De-crement image
          aggregateMedianImage.decrementReferenceCount();
          sobelEdgeDetectionImage.decrementReferenceCount();
          thresholdImage.decrementReferenceCount();
          
          mapOfResults.get(opticalCameraImageDataTypeEnumForJigHoleDiameter).setResults(new Double(diameterInPixels));
          mapOfResults.get(opticalCameraImageDataTypeEnumForMagnification).setResults(new Double(magnification));
          
          //Check these results against the limits for them and accumulate the results
          mapOfLimits.get(opticalCameraImageDataTypeEnumForJigHoleDiameter).didTestPass(mapOfResults.get(opticalCameraImageDataTypeEnumForJigHoleDiameter));
          mapOfLimits.get(opticalCameraImageDataTypeEnumForMagnification).didTestPass(mapOfResults.get(opticalCameraImageDataTypeEnumForMagnification));

          Map<OpticalCameraImageDataTypeEnum, DoubleRef> cameraDataTypeEnumToJigHoleMap = _cameraIdToJigHoleDiameter.get(opticalCameraImageDataTypeEnumForJigHoleDiameter);
          Assert.expect(cameraDataTypeEnumToJigHoleMap == null);

          Map<OpticalCameraImageDataTypeEnum, DoubleRef> cameraDataTypeEnumToMagnificationMap = _cameraIdToMagnification.get(opticalCameraImageDataTypeEnumForMagnification);
          Assert.expect(cameraDataTypeEnumToMagnificationMap == null);

          cameraDataTypeEnumToJigHoleMap = new HashMap<OpticalCameraImageDataTypeEnum, DoubleRef>();
          cameraDataTypeEnumToJigHoleMap.put(opticalCameraImageDataTypeEnumForJigHoleDiameter, new DoubleRef(diameterInPixels));
          _cameraIdToJigHoleDiameter.put(cameraId, cameraDataTypeEnumToJigHoleMap);

          cameraDataTypeEnumToMagnificationMap = new HashMap<OpticalCameraImageDataTypeEnum, DoubleRef>();
          cameraDataTypeEnumToMagnificationMap.put(opticalCameraImageDataTypeEnumForMagnification, new DoubleRef(magnification));
          _cameraIdToMagnification.put(cameraId, cameraDataTypeEnumToMagnificationMap);
        }
        finally
        {
            if (imageToAnalyze != null)
            {
                imageToAnalyze.decrementReferenceCount();
                imageToAnalyze = null;
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void waitForOpticalCameraLocationInput()
    {
        _waitingForOpticalCameraLocationInput.setValue(true);
        try
        {
          _waitingForOpticalCameraLocationInput.waitUntilFalse();
        }
        catch (InterruptedException ex)
        {
          // Do nothing ...
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void runNextOpticalCamera()
    {
        _waitingForOpticalCameraLocationInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    public void cancelLiveVideo()
    {
        _taskCancelled = true;
        _waitingForOpticalCameraLocationInput.setValue(false);
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    public void setSurfaceMapPoint(List<Point2D> points)
    {
      // Do nothing
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void acquireOpticalMagnficationImage(AbstractOpticalCamera opticalCamera, 
                                                 OpticalCalibrationPlaneEnum opticalCalibrationPlaneEnum) throws XrayTesterException
    {
      Assert.expect(opticalCamera != null);
      Assert.expect(opticalCalibrationPlaneEnum != null);
      
      String imageFullPath = null;
      if (opticalCalibrationPlaneEnum.equals(OpticalCalibrationPlaneEnum.REFERENCE_PLANE))
      {
        imageFullPath = getOpticalMagnificationReferencePlaneImageFullPath(opticalCamera.getId(), 
                                                                           opticalCamera.getOpticalCameraIdEnum().getDeviceId(),
                                                                           opticalCalibrationPlaneEnum.getId(),
                                                                           1);
      }
      else if (opticalCalibrationPlaneEnum.equals(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_TWO))
      {
        imageFullPath = getOpticalMagnificationObjectPlaneMinusTwoImageFullPath(opticalCamera.getId(), 
                                                                                opticalCamera.getOpticalCameraIdEnum().getDeviceId(), 
                                                                                opticalCalibrationPlaneEnum.getId(),
                                                                                1);
      }
      else if (opticalCalibrationPlaneEnum.equals(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_THREE))
      {
        imageFullPath = getOpticalMagnificationObjectPlanePlusThreeImageFullPath(opticalCamera.getId(), 
                                                                                 opticalCamera.getOpticalCameraIdEnum().getDeviceId(), 
                                                                                 opticalCalibrationPlaneEnum.getId(),
                                                                                 1);
      }
      else if (opticalCalibrationPlaneEnum.equals(OpticalCalibrationPlaneEnum.OBJECT_PLANE_MINUS_ONE))
      {
        imageFullPath = getOpticalMagnificationObjectPlaneMinusOneImageFullPath(opticalCamera.getId(), 
                                                                                opticalCamera.getOpticalCameraIdEnum().getDeviceId(), 
                                                                                opticalCalibrationPlaneEnum.getId(),
                                                                                1);
      }
      else if (opticalCalibrationPlaneEnum.equals(OpticalCalibrationPlaneEnum.OBJECT_PLANE_PLUS_FOUR))
      {
        imageFullPath = getOpticalMagnificationObjectPlanePlusFourImageFullPath(opticalCamera.getId(), 
                                                                                opticalCamera.getOpticalCameraIdEnum().getDeviceId(), 
                                                                                opticalCalibrationPlaneEnum.getId(),
                                                                                1);
      }
      else
        Assert.expect(false, "invalid optical calibration plane enum");
      
      // Generate stage position name
      StagePositionMutable stagePositionMutable = new StagePositionMutable();
      stagePositionMutable.setXInNanometers(-1);  // We set X,Y coordinate to -1 so that system wont run stage acquisition
      stagePositionMutable.setYInNanometers(-1);  // We set X,Y coordinate to -1 so that system wont run stage acquisition
      String stagePositionName = stagePositionMutable.getXInNanometers() + "_" + stagePositionMutable.getYInNanometers();
      
      // Consolidate info into OpticalPointToPointScan object
      OpticalPointToPointScan opticalPointToPointScan = new OpticalPointToPointScan();
      opticalPointToPointScan.setOpticalCameraIdEnum(opticalCamera.getOpticalCameraIdEnum());
      opticalPointToPointScan.setRegionPositionName(stagePositionName);
      opticalPointToPointScan.setPointPositionName(imageFullPath);
      opticalPointToPointScan.setPointToPointStagePositionInNanoMeters(stagePositionMutable);
      opticalPointToPointScan.setRoiWidth(PspHardwareEngine.getInstance().getImageRoiWidth());
      opticalPointToPointScan.setRoiHeight(PspHardwareEngine.getInstance().getImageRoiHeight());
   
      //Jack Hwee
      opticalPointToPointScan.setOpticalCalibrationProfileEnum(OpticalCalibrationProfileEnum.PROFILE_1);
     
      // Perform actual image acquisition
      PspEngine.getInstance().performSingleImageOperation(opticalPointToPointScan, OpticalShiftNameEnum.WHITE_LIGHT);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    private void analyzeImage(AbstractOpticalCamera opticalCamera,
                              OpticalCalibrationPlaneEnum calibrationPlane,
                              String imageFullPath, 
                              OpticalCameraImageDataTypeEnum opticalCameraImageDataTypeEnumForJigHoleDiameter,
                              OpticalCameraImageDataTypeEnum opticalCameraImageDataTypeEnumForMagnification) throws XrayTesterException
    {
        Assert.expect(opticalCamera != null);
        Assert.expect(calibrationPlane != null);
        Assert.expect(imageFullPath != null);
        Assert.expect(opticalCameraImageDataTypeEnumForJigHoleDiameter != null);
        Assert.expect(opticalCameraImageDataTypeEnumForMagnification != null);

        // Start analyze image
        analyzeImage(opticalCamera.getOpticalCameraIdEnum(),
                     calibrationPlane,
                     opticalCameraImageDataTypeEnumForJigHoleDiameter,
                     opticalCameraImageDataTypeEnumForMagnification,
                     imageFullPath);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void saveJigHoleDiameterToConfig() throws XrayTesterException
    {
        Assert.expect(_cameraIdToJigHoleDiameter != null);
        
        for(Map.Entry<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, DoubleRef>> entry : _cameraIdToJigHoleDiameter.entrySet())
        {
            OpticalCameraIdEnum cameraId = entry.getKey();
            Map<OpticalCameraImageDataTypeEnum, DoubleRef> opticalCameraImageDataTypeEnumToJibHoleDiameterMap = entry.getValue();
            
            for(Map.Entry<OpticalCameraImageDataTypeEnum, DoubleRef> subEntry : opticalCameraImageDataTypeEnumToJibHoleDiameterMap.entrySet())
            {
                OpticalCameraImageDataTypeEnum opticalCameraImageDataTypeEnum = subEntry.getKey();
                DoubleRef jigHoleDiameter = subEntry.getValue();
                
                if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
                {
                    if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                }
                else if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
                {
                    if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_JIG_HOLE_DIAMETER, jigHoleDiameter.getValue());
                }
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected void saveMagnificationToConfig() throws XrayTesterException
    {
        Assert.expect(_cameraIdToMagnification != null);
        
        for(Map.Entry<OpticalCameraIdEnum, Map<OpticalCameraImageDataTypeEnum, DoubleRef>> entry : _cameraIdToMagnification.entrySet())
        {
            OpticalCameraIdEnum cameraId = entry.getKey();
            Map<OpticalCameraImageDataTypeEnum, DoubleRef> opticalCameraImageDataTypeEnumToMagnificationMap = entry.getValue();
            
            for(Map.Entry<OpticalCameraImageDataTypeEnum, DoubleRef> subEntry : opticalCameraImageDataTypeEnumToMagnificationMap.entrySet())
            {
                OpticalCameraImageDataTypeEnum opticalCameraImageDataTypeEnum = subEntry.getKey();
                DoubleRef magnification = subEntry.getValue();
                
                if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_1))
                {
                    if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_REFERENCE_PLANE_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_1_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION, magnification.getValue());
                }
                else if (cameraId.equals(OpticalCameraIdEnum.OPTICAL_CAMERA_2))
                {
                    if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_REFERENCE_PLANE_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_TWO_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_THREE_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_MINUS_ONE_MAGNIFICATION, magnification.getValue());
                    else if (opticalCameraImageDataTypeEnum == OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION)
                        _config.setValue(PspCalibEnum.CAL_MAGNIFICATION_CAMERA_2_OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION, magnification.getValue());
                }                     
            }
        }
    }
    
    /*
     * @author Cheah Lee Herng
     */
    protected void checkAbortInProgress()
    {
        if (_taskCancelled == true)
        {
            reportProgress(100);
            _result.setStatus(HardwareTaskStatusEnum.CANCELED);
            return;
        }
    }
    
    /**
     * @author Cheah Lee Herng
     */
    void logInformation(boolean forceLogging) throws DatastoreException
    {
        StringBuffer logData = new StringBuffer();

        logData.append("Camera Id, Ref Plane Jig Diameter (um), Ref Plane Mag, -2 Plane Jig Diameter (um), -2 Plane Mag, +3 Jig Diameter (um), +3 Plane Mag, -1 Plane Jig Diameter (um), -1 Plane Mag, +4 Plane Jig Diameter, +4 Plane Mag\n");

        logSpecificInfo(logData);        

        if (forceLogging)
        {
          _logger.log(logData.toString());
        }
        else
        {
          _logger.logIfTimedOut(logData.toString(), _logTimeoutMilliSeconds);
        }
    }
    
    /**
     * Wrap the common logging infrastructure
     * 
     * @author Cheah Lee Herng 
     */
    private void logSpecificInfo(StringBuffer logData)
    {
        Assert.expect(logData != null);
       
        if ((_cameraIdToTestTypeToResultMap != null) && (_cameraIdToTestTypeToResultMap.isEmpty() == false))
        {
            //Get the line separator character only once
            String lineSeparator = System.getProperty("line.separator");
            
            for(Map.Entry<OpticalCameraIdEnum, AbstractOpticalCamera> entry : _cameraIdToCamerasMap.entrySet())
            {
                OpticalCameraIdEnum cameraId = entry.getKey();
                
                //Get the sub-maps for this specific camera containing the results and limits
                Map<OpticalCameraImageDataTypeEnum, Result> mapOfTestsToResults = _cameraIdToTestTypeToResultMap.get(cameraId);
                Map<OpticalCameraImageDataTypeEnum, Limit> mapOfLimits = _cameraIdToTestTypeToLimitMap.get(cameraId);
                
                if (mapOfTestsToResults != null && !mapOfTestsToResults.isEmpty())
                {
                    logData.append(cameraId.getId() + ",");
                }
                
                //Log the reference plane jig hole diameter value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_DIAMETER).toString() +
                                   "),");
                }
                
                //Log the reference plane magnification value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.REFERENCE_PLANE_MAGNIFICATION).toString() +
                                   "),");
                }
                
                //Log the -2 plane jig hole diameter value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_DIAMETER).toString() +
                                   "),");
                }
                
                //Log the -2 plane magnification value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_TWO_MAGNIFICATION).toString() +
                                   "),");
                }
                
                //Log the +3 plane jig hole diameter value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_DIAMETER).toString() +
                                   "),");
                }
                
                //Log the +3 plane magnification value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_THREE_MAGNIFICATION).toString() +
                                   "),");
                }
                
                //Log the -1 plane jig hole diameter value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_DIAMETER).toString() +
                                   "),");
                }
                
                //Log the -1 plane magnification value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_MINUS_ONE_MAGNIFICATION).toString() +
                                   "),");
                }
                
                //Log the +4 plane jig hole diameter value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_DIAMETER).toString() +
                                   "),");
                }
                
                //Log the +4 plane magnification value
                if (mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION).didTestPass(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION)))
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION).getResultString() + ",");
                }
                else
                {
                    logData.append(mapOfTestsToResults.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION).getResultString() +
                                   " FAILED (limit = " +
                                   mapOfLimits.get(OpticalCameraImageDataTypeEnum.OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION).toString() +
                                   "),");
                }
                
                //terminate the line with the appropriate character
                logData.append(lineSeparator);
            }
        }
    }
    
    /**
     * @author Cheah Lee Herng 
     */
    protected boolean ableToRunWithCurrentPanelState() throws XrayTesterException
    {
        // We need to have calibration jig loaded so that we 
        // can run this task.
        return (_panelHandler.isPanelLoaded() == true);
    }
    
    /**
     * @author Cheah Lee Herng
     */
    protected void loadOpticalCalibrationJig() throws XrayTesterException
    {
        if (_panelHandler.isPanelLoaded() == false)
        {
          if (XrayActuator.isInstalled())
          {            
            if (XrayCylinderActuator.isInstalled() == true)
            {
              // Always move the cylinder to UP position
              _xrayActuator.up(false);
            }
            else if (XrayCPMotorActuator.isInstalled() == true)
            {
              // Always move the Z Axis to Home and Safe position
              _xrayActuator.home(false,false);
            }   
          }          
          // comment by Wei Chin
          // should not check the safety sensor (due to it don't lower down the xray tube)
          // Please be aware if it need to run this board in high mag! 
          _panelHandler.loadPanel("Panel for Service Gui", OPTICAL_CALIBRATION_JIG_WIDTH_IN_NANOMETERS, OPTICAL_CALIBRATION_JIG_HEIGHT_IN_NANOMETERS, false, 0);
        }
    }
    
    /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void setHardwareShutdownComplete()
  {
    // Do nothing
  }
}
