package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Confirmation to test camera light values. This confirmation gets a set of
 * images from the cameras and checks them for horizontal flatness, vertical
 * flatness, mean gray value and gray value standard deviation. These values
 * must fall within acceptable limits for the cameras to be functioning
 * correctly.
 *
 * This task *may* need to be run in three distinct parts because it requires
 * Xrays to be on and that the images be retreived from cameras that are not
 * covered/obstructed by a board, the stage, etc.
 *
 *
 * @author Reid Hayhow
 */
public class ConfirmCameraGroupOneLightImageTask extends ConfirmCameraImageTask
{
  private static final String NAME =
      StringLocalizer.keyToString("CD_CAMERA_LIGHT_CONFIRMATION_KEY") +
      StringLocalizer.keyToString("CD_CAMERA_GROUP_ONE_KEY");

  // Get frequency from hardware.calib
  private static final int FREQUENCY =
      _config.getIntValue(HardwareCalibEnum.CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_FREQUENCY);

  // Get timeout from hardware.calib
  private static final long TIMEOUTINSECONDS =
      _config.getLongValue(SoftwareConfigEnum.CAMERA_LIGHT_IMAGE_GROUP_ONE_CONFIRM_TIMEOUT_IN_SECONDS);

  private static ConfirmCameraGroupOneLightImageTask _instance = null;

  /**
   * Constructor for this confirmation task
   * @author Reid Hayhow
   */
  private ConfirmCameraGroupOneLightImageTask()
  {
    //Create it and specify the logging directory for the logger stored in the
    //super class
    super(NAME,
          FREQUENCY,
          TIMEOUTINSECONDS,
          Directory.getCameraLightConfirmationLogDir(),
          ProgressReporterEnum.LIGHT_CAMERA_IMAGE_QUALITY_TASK_GROUP_ONE);

    //This confirmation is for camera group one
    _camerasToGetImagesFromSet = _cameraGroupOne;

    //Find the correct stage position for this confirm to use from hardware.calib
    _xPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS;
    _yPosition = HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS;

    //The camera group for group one is on the right side of the tester, needed
    //to reposition panel before test
    cameraGroupIsOnRightSideOfTester();

    //This is a light image task
    _localizedLightOrDarkString = StringLocalizer.keyToString("ACD_LIGHT_KEY");

    //Set the enum used to save the timestamp in the hardware.calib
    setTimestampEnum(HardwareCalibEnum.LIGHT_IMAGE_GROUP_ONE_LAST_TIME_RUN);
  }

  /**
   * Instance access to this confirmation task
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCameraGroupOneLightImageTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCameraGroupOneLightImageTask();
    }
    return _instance;
  }
  
//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmCameraImageTask will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
}
