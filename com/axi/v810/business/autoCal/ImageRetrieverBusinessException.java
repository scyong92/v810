
package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.business.*;

/**
 * This exception is thrown if the ImageRetriever fails.
 *
 * @author John Dutton
 */
public class ImageRetrieverBusinessException extends BusinessException
{
  public ImageRetrieverBusinessException()
  {
    super(new LocalizedString("CD_EXCEPTION_IMAGE_RETRIVER_KEY", null));

    // To simplify debugging, add the method and line number where this object is being created.
    StackTraceElement[] stackElements = getStackTrace();
    Assert.expect(stackElements.length > 0, "stack trace is unexpectedly small");
    Object [] argumentArray = new Object[] {stackElements[0].getMethodName(),
                                            stackElements[0].getLineNumber()};
    getLocalizedString().setArguments(argumentArray);
  }
}


