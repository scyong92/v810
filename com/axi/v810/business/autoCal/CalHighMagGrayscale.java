package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
public class CalHighMagGrayscale extends EventDrivenHardwareTask
{
  private static ImageAcquisitionEngine _imageAcquisitionEngine;
  private static Config _config = Config.getInstance();
  private static final String _NAME_OF_CAL = "High Mag Camera Grayscale Adjustment";//StringLocalizer.keyToString("DS_HIGH_MAG_CAMERA_GRAYSCALE_NAME_KEY");
  
  private static final int _EVENT_FREQUENCY = 1;
//  private static final long _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_TIMEOUT_SECONDS);
  private static final long _EXECUTE_EVERY_TIMEOUT_SECONDS = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_PIXEL_DARK_TIMEOUT_SECONDS);
  private static final String _LOG_DIRECTORY = Directory.getGreyscaleCalHighMagLogDir();
  private static final int _LOG_FREQUENCY_IN_SECONDS =   _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_LOG_RESULTS_EVERY_TIMEOUT_SECONDS);
  private static final int _LOG_FREQUENCY_IN_MILLISECONDS = _LOG_FREQUENCY_IN_SECONDS;
  private static final int _MAX_LOG_FILES_IN_DIRECTORY = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_MAX_LOG_FILES_IN_DIRECTORY);
  private static final int _CALIB_DELAY_IN_MILLISSECONDS = _config.getIntValue(SoftwareConfigEnum.CALIB_DELAY_IN_MILLISSECONDS);
  private static CalHighMagGrayscale _instance = null;
  private static List<AbstractXrayCamera> _cameras;
  //private static Map<AbstractXrayCamera, XrayCameraCalibrationData> _cameraToCalibrationDataMap;
  //private static Map<AbstractXrayCamera, XrayCameraCalibrationData> _cameraToHighMagCalibrationDataMap;


  // For best throughput:
  // Groups & Positions #1&2 are intended to be close to each other and on
  // the left side close to where image scanning will start from.
  // Position #3 is on the right side close to where scanning finishes.
  // This will minimize stage movement when running this adjustment.
  private List<AbstractXrayCamera> _cameraGroup1 = new ArrayList<AbstractXrayCamera>();
  private List<AbstractXrayCamera> _cameraGroup2 = new ArrayList<AbstractXrayCamera>();
  private List<AbstractXrayCamera> _cameraGroup3 = new ArrayList<AbstractXrayCamera>();
  private StagePositionMutable _stagePosition1;
  private StagePositionMutable _stagePosition2;
  private StagePositionMutable _stagePosition3;

  private Boolean _earlyPrototype = null;;

  private Map<AbstractXrayCamera, Result> _cameraToLocalResultsMap = new HashMap<AbstractXrayCamera, Result>();
  private Map<AbstractXrayCamera, Limit> _cameraToLocalLimitsMap = new HashMap<AbstractXrayCamera, Limit>();

  private boolean _calAtPanelUnload;

  private ExecuteParallelThreadTasks<Object> _cameraCalibThreads = null;
  private ExecuteParallelThreadTasks<Object> _cameraDarkCalibThread = null;
  private ExecuteParallelThreadTasks<Object> _cameraCompleteCalibThreads = null;
  private ExecuteParallelThreadTasks<Object> _cameraTriggerControlThreads = null;

  private Map<AbstractXrayCamera, XrayCameraCalibThreadTask> _cameraToCameraCalibThreadMap = new HashMap<AbstractXrayCamera, XrayCameraCalibThreadTask>();

  private long _timeOfLastPixelLightCal;
  private long _timeOfLastPixelDarkCal;
  private long _timeOfLastCompleteCal;
  private int _timeExtensionForCompleteCalAtUnloadInSeconds;
  private int _timeExtensionForPartialCalAtUnloadInSeconds;
  private int _timeExtensionForCompleteCalAtLoadInSeconds;
  private int _timeExtensionForPartialCalAtLoadInSeconds;
  private int _timeExtensionForPartialCalAtImageAcquisitionInSeconds;
  private int _timeExtensionForCompleteCalAtImageAcquisitionInSeconds;
  private int _timeExtensionForCompleteCalAtInspectionInSeconds;

  private AbstractXraySource _xraySource = AbstractXraySource.getInstance();
  private PanelPositioner _panelPositioner = PanelPositioner.getInstance();
  private PanelHandler _panelHandler = PanelHandler.getInstance();
  private MotionProfile _originalMotionProfile;
  private Map<AbstractXrayCamera, Boolean> _cameraToPreviousEnableMap = new HashMap<AbstractXrayCamera, Boolean>();
  private boolean overridePanelEject = _config.getBooleanValue(SoftwareConfigEnum.OVERRIDE_PANEL_EJECT_FOR_CALIBRATIONS);
  private boolean _runCalWithPanelInSystem = _config.getBooleanValue(SoftwareConfigEnum.RUN_ADJUSTMENTS_AND_CONFIRM_WITH_PANEL_IN_SYSTEM);
  private StringBuilder _stringOfFailingCameras = new StringBuilder("");

  private TestExecution _testExecution = TestExecution.getInstance();
  private static PerformanceLogUtil _performanceLog = PerformanceLogUtil.getInstance();

  private static final int MAX_DELAY_IN_SECONDS = 1000;

  /**
   * @author Eddie Williamson
   */
  static
  {
    _config = Config.getInstance();

    // Build list of all cameras
    _cameras = XrayCameraArray.getCameras();

    _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  }

  /**
   * @author Eddie Williamson
   */
  private CalHighMagGrayscale()
  {
    super(_NAME_OF_CAL,
          _EVENT_FREQUENCY,
          _EXECUTE_EVERY_TIMEOUT_SECONDS,
          ProgressReporterEnum.CAMERA_GRAYSCALE_ADJUSTMENT_TASK);

    _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

    _cameraCalibThreads = new ExecuteParallelThreadTasks<Object>();
    _cameraDarkCalibThread = new ExecuteParallelThreadTasks<Object>();
    _cameraCompleteCalibThreads = new ExecuteParallelThreadTasks<Object>();
    _cameraTriggerControlThreads = new ExecuteParallelThreadTasks<Object>();

    _calAtPanelUnload = true;
    if ( (_config.getBooleanValue(HardwareConfigEnum.PANEL_HANDLER_INNER_BARRIER_INSTALLED) == false)    &&
        (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals(new String("legacy")) ||
         _config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals(new String("htube"))))
    {
      _calAtPanelUnload = false;
    }
    _isHighMagTask = true;
 }

  /**
   * @author Eddie Williamson
   */
  public static synchronized CalHighMagGrayscale getInstance()
  {
    if (_instance == null)
    {
      _instance = new CalHighMagGrayscale();
    }
    return _instance;
  }

  /**
   * By default all tests can run on any hardware configuration.
   * @author Eddie Williamson
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
  * Perform Camera Dark Segment Calibration
  * @author Farn Sern
  */
  public void performDarkSegmentCal() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_DARK_SEGMENT_CAL);
  }

  /**
  * Perform Camera Dark Segment Calibration
  * @author Anthony Fong
  */
  public void performLightSegmentCal() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_LIGHT_SEGMENT_CAL);
  }
 
  /**
  * Perform Camera Dark Pixel Calibration
  * @author Farn Sern
  */
  public void performDarkPixelCal() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_DARK_PIXEL_CAL);
  }

  //Variable Mag Anthony August 2011
  /**
  * Perform Camera Save Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performSaveVariableMagHighCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.SAVE_VARIABLE_MAG_HIGH_CAL);
  }
  /**
  * Perform Camera Load Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performLoadVariableMagHighCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.LOAD_VARIABLE_MAG_HIGH_CAL);
  }
    /**
  * Perform Camera Save Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performSaveVariableMagLowCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.SAVE_VARIABLE_MAG_LOW_CAL);
  }
  /**
  * Perform Camera Load Variable Mag High Calibration Configuration
  * @author Anthony Fong
  */
  public void performLoadVariableMagLowCalConfig() throws XrayTesterException
  {
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.LOAD_VARIABLE_MAG_LOW_CAL);
  }
  /**
   * @author Eddie Williamson
   */
  protected void cleanUp() throws XrayTesterException
  { 
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      if (XrayCylinderActuator.isInstalled() == true)
      {
        //move x-rays Cylinder to Up and Safe position.
        XrayActuator.getInstance().up(false);
      }
      else if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
    // No images to clear
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }

  /**
   * @author Eddie Williamson
   */
  protected void createLimitsObject()
  {
    _cameraToLocalLimitsMap.clear();

    // Create container for the others
    _limits = new Limit(null, null, null, _NAME_OF_CAL);

    // Add limit for each camera
    for (AbstractXrayCamera camera : _cameras)
    {
      Limit perCameraLimit = new Limit(null, null, null, "Camera " + camera.getId());
      _limits.addSubLimit(perCameraLimit);
      _cameraToLocalLimitsMap.put(camera, perCameraLimit);
    }
  }

  /**
   * @author Eddie Williamson
   */
  protected void createResultsObject()
  {
    _cameraToLocalResultsMap.clear();

    // Create container for the others
    _result = new Result(_NAME_OF_CAL);

    // Add result for each camera
    for (AbstractXrayCamera camera : _cameras)
    {
      Result perCameraResult = new Result(new Boolean(true), getName());
      _result.addSubResult(perCameraResult);
      _cameraToLocalResultsMap.put(camera, perCameraResult);
    }
  }


  /**
   * @author Eddie Williamson
   */
  protected void setUp() throws XrayTesterException
  {
    updateConfigParameters();
    
     //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      PanelClamps.getInstance().close();
    }
    for (AbstractXrayCamera camera : _cameras)
    {
      camera.disableSaveCalibrationLog();
    }
  }

  /**
   * Logs detailed information about the cal and saves images if needed. The
   * log information is formatted as comma separated values to it can be
   * imported into Excel for manual analysis.
   * @param forceLogging Force logging if true, else based on periodic timeout.
   * @author Eddie Williamson
   */
  private void logInformation(Boolean forceLogging) throws XrayTesterException
  {
    int logFrequencyInMilliseconds = _LOG_FREQUENCY_IN_MILLISECONDS;

    if (forceLogging)
      logFrequencyInMilliseconds = 0;

    if(LicenseManager.isVariableMagnificationEnabled())
    {
      for (AbstractXrayCamera camera : _cameras)
      {
        XrayCameraCalibrationData highMagCalibrationData = camera.getHighMagCalibrationData();
        
        if(highMagCalibrationData.hasCalibrationData())
          highMagCalibrationData.log(_LOG_DIRECTORY, logFrequencyInMilliseconds);
        else
        {
          System.out.println("Camera: " + camera.getId() + " - No Calibration Data!");
        }
      }
    }
  }

  /**
   * at various points, the code needs to sleep for a bit.  wrap up exception handling here
   * @author Greg Loring
   */
  private void sleep(long millis)
  {
    try
    {
      Thread.sleep(millis);
    }
    catch (InterruptedException ix)
    {
      // don't expect this, but don't really care how sleep returns either
    }
  }
  
  /**
   * @author Eddie Williamson
   */
  private void turnXraysOff() throws XrayTesterException
  {
    if (_xraySource.areXraysOn())
    {
        _xraySource.off();
    }

    // Previously commented out by LeeHerng - To speed up Gray Scale Calibration
    // But later found out 5.2 have intermittent grayscale adjustment failure.
    // Adding delay here will solve the intermittent error.
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
      sleep(MAX_DELAY_IN_SECONDS);
    }
  }


  /**
   * @author Eddie Williamson
   */
  private void turnXraysOn() throws XrayTesterException
  {
    if (!_xraySource.areXraysOn())
    {
        _xraySource.on();
    }

    // Previously commented out by LeeHerng - To speed up Gray Scale Calibration
    // But later found out 5.2 have intermittent grayscale adjustment failure.
    // Adding delay here will solve the intermittent error.
    if (_config.getStringValue(HardwareConfigEnum.XRAY_SOURCE_TYPE).equals("htube"))
    {
      sleep(MAX_DELAY_IN_SECONDS);
    }
  }


  /**
   * @param cameras List of cameras on which to run this calibration step
   * @param xrayCameraCalibEnum Which calibration step to run
   * @author Eddie Williamson
   */
  private void startCameraCalibThreads(List<AbstractXrayCamera> cameras, XrayCameraCalibEnum xrayCameraCalibEnum)
  {
    _cameraToCameraCalibThreadMap.clear();

    // start threads for all cameras
    for (AbstractXrayCamera camera : cameras)
    {
      XrayCameraCalibThreadTask xrayCameraCalibThreadTask = new XrayCameraCalibThreadTask(camera, xrayCameraCalibEnum);
      _cameraToCameraCalibThreadMap.put(camera, xrayCameraCalibThreadTask);
      _cameraCalibThreads.submitThreadTask(xrayCameraCalibThreadTask);
    }
  }

  /**
   * @param cameras List of cameras on which to wait
   * @param xrayCameraCalibEnum Which calibration step was run
   * @author Eddie Williamson
   */
  private void waitForCameraCalibThreads(List<AbstractXrayCamera> cameras, XrayCameraCalibEnum xrayCameraCalibEnum) throws XrayTesterException
  {
    _stringOfFailingCameras = new StringBuilder("");
    try
    {
      _cameraCalibThreads.waitForTasksToComplete();
    }
    catch (Exception ex)
    {
      if (ex instanceof XrayTesterException)
      {
        throw (XrayTesterException)ex;
      }
      else
      {
        /** QUESTION: Should we do this even if it already was an XrayTesterException?
         * Adds Grayscale name to message. */
        // convert to XrayTesterException before throwing
        XrayTesterException convertedEx = new HardwareTaskExecutionException(CalGrayscale.getInstance());
        convertedEx.initCause(ex);
        convertedEx.fillInStackTrace();
        throw convertedEx;
      }
    }

    // Placeholder exception to assure we don't eat any exceptions
    XrayTesterException firstXrayTesterExceptionSeen = null;

    for (AbstractXrayCamera camera : cameras)
    {
      if (_cameraToCameraCalibThreadMap.get(camera).getPass())
      {
        // do nothing
      }
      else
      {
        // The only way Grayscale cal fails is if there is an error from the camera (ie. an Exception)
        // Or an error executing the task itself (runtime error)
        // But we don't want to clobber any earlier exception that may have been encountered
        if(firstXrayTesterExceptionSeen == null)
        {
          firstXrayTesterExceptionSeen = _cameraToCameraCalibThreadMap.get(camera).getException();

          // Fail
          // Get the message saved by the camera calib thread
          _result.setTaskStatusMessage(_cameraToCameraCalibThreadMap.get(camera).getFailMessage());
        }
        else
        {
          // Can't display this to users but we can at least log it
          _adjustAndConfirmFileLogger.append(_cameraToCameraCalibThreadMap.get(camera).getFailMessage());
        }

        // Build list of failing camera numbers to display in message later
        _stringOfFailingCameras.append(camera.getId() + ",");
        // Set this camera's result to failing
        _cameraToLocalResultsMap.get(camera).setResults(new Boolean(false));
      }
    }
  }

  /**
   * @author Eddie Williamson
   */
  private void allCamerasIgnoreTriggers() throws XrayTesterException
  {
    Map<AbstractXrayCamera, XrayCameraTriggerControlThreadTask> cameraToXrayCameraTriggerControlThreadTasksMap = new HashMap<AbstractXrayCamera, XrayCameraTriggerControlThreadTask>();

    // start threads for all cameras
    for (AbstractXrayCamera camera : _cameras)
    {
      XrayCameraTriggerControlThreadTask xrayCameraTriggerControlThreadTask = new XrayCameraTriggerControlThreadTask(camera, false);
      cameraToXrayCameraTriggerControlThreadTasksMap.put(camera, xrayCameraTriggerControlThreadTask);
      _cameraTriggerControlThreads.submitThreadTask(xrayCameraTriggerControlThreadTask);
    }

    try
    {
      _cameraTriggerControlThreads.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }

    for (AbstractXrayCamera camera : _cameras)
    {
      _cameraToPreviousEnableMap.put(camera, cameraToXrayCameraTriggerControlThreadTasksMap.get(camera).getPreviousEnable());
    }
  }

  /**
   * Restores the previous trigger mode that was saved when
   * allCamerasIgnoreTriggers() was called.
   * @author Eddie Williamson
   */
  private void allCamerasRestorePreviousTriggerMode() throws XrayTesterException
  {
    try
    {
      // start threads for all cameras
      for (AbstractXrayCamera camera : _cameras)
      {
        // This method can be called as a recovery (finally block) so the map may not have been populated
        // or may be only partially populated
        Boolean previousCameraTriggerState = _cameraToPreviousEnableMap.get(camera);

        // Only submit tasks for cameras that have a valid previous state
        if(previousCameraTriggerState != null)
        {
          XrayCameraTriggerControlThreadTask xrayCameraTriggerControlThreadTask = new XrayCameraTriggerControlThreadTask(camera, previousCameraTriggerState);
          _cameraTriggerControlThreads.submitThreadTask(xrayCameraTriggerControlThreadTask);
        }
      }

      _cameraTriggerControlThreads.waitForTasksToComplete();
    }
    catch (XrayTesterException ex)
    {
      throw ex;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }


  /**
   * @param cameras List of cameras on which to run this calibration step
   * @param xrayCameraCalibEnum Which calibration step to run
   * @author Eddie Williamson
   */
  private void performCalibStepOnCameras(List<AbstractXrayCamera> cameras, XrayCameraCalibEnum xrayCameraCalibEnum) throws XrayTesterException
  {
    Assert.expect(cameras != null);
    Assert.expect(xrayCameraCalibEnum != null);

    CameraTrigger cameraTrigger = CameraTrigger.getInstance();
    try
    {
      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.GRAYSCALE_CAL_IGNORE_TRIGGERS);
      allCamerasIgnoreTriggers();
      cameraTrigger.on();

      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.GRAYSCALE_CAL_CALIB_STEP, xrayCameraCalibEnum.toString());
      startCameraCalibThreads(cameras, xrayCameraCalibEnum);
      waitForCameraCalibThreads(cameras, xrayCameraCalibEnum);

    }
    finally
    {
      // Make sure that synthetic triggers are on.  It is good/expected to leave
      // them on because they bathe cameras keeping them thermally warm.
      cameraTrigger.on();
      _performanceLog.logMilestone(PerformanceLogMilestoneEnum.GRAYSCALE_CAL_RESTORE_TRIGGERS);
      allCamerasRestorePreviousTriggerMode();
    }
  }

  /**
   * Spawn threads to do hardware tasks in parallel. In this method, we are
   * moving the stage to cal position and perform camera dark segment calibration.
   * @author Farn Sern
   */
  private void moveStageAndDarkSegmentCalInParallel() throws XrayTesterException
  {
    ThreadTask<Object> task1 = new MoveStageToCalPositionThreadTask();
    ThreadTask<Object> task2 = new CameraDarkSegmentCalThreadTask();

    _cameraCompleteCalibThreads.submitThreadTask(task1);
    _cameraCompleteCalibThreads.submitThreadTask(task2);


    try
    {
      _cameraCompleteCalibThreads.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }

  /**
  * Spawn threads to do hardware tasks in parallel. In this method, we are
  * moving the stage to load position and perform camera dark pixel calibration.
  * @author Farn Sern
  */
  private void moveStageAndDarkPixelCalInParallel() throws XrayTesterException
  {
    BarrierInt _primaryOuterBarrier;

    _primaryOuterBarrier = _panelHandler.getPrimaryOuterBarrier();
    ThreadTask<Object> task1 = new MoveStageToOuterBarrierThreadTask(_primaryOuterBarrier);
    ThreadTask<Object> task2 = new CameraDarkPixelCalThreadTask();

    _cameraCompleteCalibThreads.submitThreadTask(task1);
    _cameraCompleteCalibThreads.submitThreadTask(task2);

    try
    {
      _cameraCompleteCalibThreads.waitForTasksToComplete();
    }
    catch (XrayTesterException xte)
    {
      throw xte;
    }
    catch (Exception e)
    {
      Assert.logException(e);
    }
  }



  /**
   * Do all of the steps for a complete grayscale adjustment on all cameras.
   * Assumes that there isn't a panel loaded.
   *
   * @author Eddie Williamson
   * @author Farn Sern
   */
  private void completeCalibrationPanelUnloaded() throws XrayTesterException
  {
    if (overridePanelEject == false)
    {
      Assert.expect(_panelHandler.isPanelLoaded() == false);  // This shouldn't ever be called with a panel loaded
    }
    
    // Swee Yee Wong - Reset Optical PIP sensor
    // This step is needed for Omron optical sensor, previously optical PIP only reset when rail width is changed,
    // But we found out some customers ran the same panel for long time which causes the signal strength getting weaker
    // over time, therefore we tied up this step to CalGrayscale to make sure it will be reset regularly
    if (XrayTester.isS2EXEnabled())
    {
      if (_panelHandler.isPanelLoaded() == false)
      {
        // Swee-Yee.Wong - Trigger Optical PIP sensor to reset the signal strength
        DigitalIo.getInstance().resetOpticalPIPSensor();
      }
    }

    if(LicenseManager.isVariableMagnificationEnabled())
    {
        XrayActuator.getInstance().up(false);
    }
    turnXraysOff();
    _adjustAndConfirmFileLogger.append("xrays off");
    _adjustAndConfirmFileLogger.append("begin high mag segment dark");
    moveStageAndDarkSegmentCalInParallel();
    _adjustAndConfirmFileLogger.append("done high mag segment dark");
    reportProgress(24);

//    if (!_limits.didTestPass(_result))
//    {
//      XrayCylinder.getInstance().up(false);
//      turnXraysOff();
//      return;
//    }
 

    if (_panelHandler.moveStageToCameraClearPosition() == false)
    {
      throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_NAME_OF_CAL);
    }

    reportProgress(48);

    //Start High Mag Calibration
    if(LicenseManager.isVariableMagnificationEnabled())
    {
      //Move X-Ray Head Down and Turn on X-Ray
      turnXraysOn();
      ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.HIGH);

      XrayActuator.getInstance().down(false);
      
      //Swee Yee- set calPoint for different Magnification
      XrayCameraArray.getInstance().loadCalPointForVariableMag(false);

      _adjustAndConfirmFileLogger.append("begin high mag segment light");
      performLightSegmentCal();
      _adjustAndConfirmFileLogger.append("done high mag segment light");
      reportProgress(60);
//      if (!_limits.didTestPass(_result))
//      {
//        XrayCylinder.getInstance().up(false);
//        turnXraysOff();
//        return;
//      }
      //Move X-Ray Head Up and Turn off X-Ray
      XrayActuator.getInstance().up(false);
      turnXraysOff();
      _adjustAndConfirmFileLogger.append("begin high mag pixel dark");
      moveStageAndDarkPixelCalInParallel();
      _adjustAndConfirmFileLogger.append("done high mag pixel dark");
      reportProgress(70);

//      if (!_limits.didTestPass(_result))
//      {
//        XrayCylinder.getInstance().up(false);
//        turnXraysOff();
//        return;
//      }
      _adjustAndConfirmFileLogger.append("begin save high cal config ");
      performSaveVariableMagHighCalConfig();
      _adjustAndConfirmFileLogger.append("done save high cal config ");
      _adjustAndConfirmFileLogger.append("begin load high cal config ");
      performLoadVariableMagHighCalConfig();
      _adjustAndConfirmFileLogger.append("done load high cal config ");
      for (AbstractXrayCamera camera : _cameras)
      {
        camera.setHighMagCalibrationData();
      }
      //Restore low mag by default
      _adjustAndConfirmFileLogger.append("begin load low cal config ");
      performLoadVariableMagLowCalConfig();
      _adjustAndConfirmFileLogger.append("done save low cal config ");
      reportProgress(82);
      
    }
    reportProgress(96);

    _timeOfLastCompleteCal = System.currentTimeMillis();
    _timeOfLastPixelDarkCal = System.currentTimeMillis();
//    _timeOfLastPixelLightCal = System.currentTimeMillis();
  }



  /**
   * Do all of the steps for a complete grayscale adjustment on all
   * cameras with a panel loaded.
   *
   * @author Eddie Williamson
   */
  private void completeCalibrationPanelLoaded() throws XrayTesterException
  {
    // TEMPORARY FOR RELEASE 1.0
    // Gut and rewrite this to support panels loaded for 1.5
    if (overridePanelEject == false)  // allows remote system testers in Singapore to use the system
    {
      throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_NAME_OF_CAL);
    }
    else
    {
      completeCalibrationPanelUnloaded();
    }
  }


  /**
   * Do the steps for a partial grayscale adjustment for pixel offset/gain on all cameras.
   * Assumes that there isn't a panel loaded.
   *
   * @author Eddie Williamson
   */
  private void partialCalibrationPanelUnloaded() throws XrayTesterException
  {
    if (overridePanelEject == false)
    {
      Assert.expect(_panelHandler.isPanelLoaded() == false);
    }

    turnXraysOff();
    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_DARK_PIXEL_CAL);
    reportProgress(49);

    if (!_limits.didTestPass(_result))
    {
      return;
    }
/*
    turnXraysOn();
    if (_earlyPrototype != null && _earlyPrototype.booleanValue())
    {
      // Support for LP1&2 camera 2
      if (_cameraGroup1.size() > 0)
      {
        _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
        _panelPositioner.pointToPointMoveAllAxes(_stagePosition1);
        performCalibStepOnCameras(_cameraGroup1, XrayCameraCalibEnum.DO_LIGHT_PIXEL_CAL);
      }
      if (_cameraGroup2.size() > 0)
      {
        _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
        _panelPositioner.pointToPointMoveAllAxes(_stagePosition2);
        performCalibStepOnCameras(_cameraGroup2, XrayCameraCalibEnum.DO_LIGHT_PIXEL_CAL);
      }
      if (_cameraGroup3.size() > 0)
      {
        _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
        _panelPositioner.pointToPointMoveAllAxes(_stagePosition3);
        performCalibStepOnCameras(_cameraGroup3, XrayCameraCalibEnum.DO_LIGHT_PIXEL_CAL);
      }
    }
    else
    {
      // CR32071-Pantec 2.4" panel customization ==> by AnthonyFong on 14 Jan,2009
      // To enhance X6000 with minimum hardware and software modifications,
      // in order to inspect small panel (2.40 inches) for Pantec
      // by extending the adjustable rail 1.60" towards fix rail,
      // in order to get the smaller rail width of 2.40"
      //
      // Setting changes in Hardware.config for inspecting panels less than 4.00" as follow:
      // the max is lowered form 18.00" to 16.40" because new customization lowered the min width by 1.60"
      // the min is lowered form  4.00" to  2.40"
      // (Notes: actual numbers may be slightly different from system to system)
      // panelHandlerPanelMaximumWidthInNanometers=416560000 (16,400 mils)     Old (18,000 mils) 457200000
      // panelHandlerPanelMinimumWidthInNanometers=60960000 (2,400 mils)       Old (4,000 mils) 101600000
      // railWidthAxisMaximumPositionLimitInNanometers=419735000 (16,525 mils) Old (18,125 mils) 460375000
      // railWidthAxisMinimumPositionLimitInNanometers=60960000 (2,400 mils)   Old (4,000 mils) 101600000
      //
      // Rail width hard stop limit in nanometers before customization is 4 inches (101,600,000 nm)
      int railWidthHardLimitInNanometers = 101600000;

      // Extended adjustable rail will block X-ray source during CD&A and
      // CD&A will fail if the stage is at the existing center position
      // System need to move the stage to a new position from blocking X-ray source during CD&A
      // Check to activate the new skip away motion using the actual rail width
      int actualRailWidthInNanometers = _panelHandler.getRailWidthInNanoMeters();
      if (actualRailWidthInNanometers < railWidthHardLimitInNanometers)
      {

        // Check for hardware limit
        Assert.expect(railWidthHardLimitInNanometers > _panelHandler.getMinimumRailWidthInNanoMeters());

        // Define XY Stage Skip Away Positiion for DC&A
        // Acceptable X Range = 750 mils -- 10,000 mils ---19,000 mils
        // Acceptable Y Range =   0 mils --  3,800 mils --- 6,500 mils
        // Optimum skip away position = P( 10000 mils , 3,800 mils ) = P( 254000000 nm , 96520000 nm)
        StagePositionMutable stageSkipAwayPosition;
        stageSkipAwayPosition = new StagePositionMutable();
        stageSkipAwayPosition.setXInNanometers(254000000);
        stageSkipAwayPosition.setYInNanometers(96520000);
        _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
        _panelPositioner.pointToPointMoveAllAxes(stageSkipAwayPosition);
        performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_LIGHT_PIXEL_CAL);

      }
      else
      {
        // Move stage to existing center position before performing CD&A

        // LP3 and beyond have redesigned stage allowing us to get all cameras at once
        _panelPositioner.setMotionProfile(new MotionProfile(PointToPointMotionProfileEnum.PROFILE1));
        _panelPositioner.pointToPointMoveAllAxes(_stagePosition1);
        performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_LIGHT_PIXEL_CAL);
      }
      // CR32071-Pantec 2.4" panel customization <== by AnthonyFong on 14 Jan,2009

    }
    turnXraysOff();*/
    reportProgress(98);

//    if (!_limits.didTestPass(_result))
//    {
//      return;
//    }

    _timeOfLastPixelDarkCal = System.currentTimeMillis();
//    _timeOfLastPixelLightCal = System.currentTimeMillis();
  }



  /**
   * Do the steps for a partial grayscale adjustment for pixel offset/gain on all cameras.
   * Assumes that there isn't a panel loaded.
   *
   * @author Eddie Williamson
   */
  private void partialCalibrationPanelLoaded() throws XrayTesterException
  {
    // TEMPORARY FOR RELEASE 1.0
    // Gut and rewrite this to support panels loaded for 1.5
    if (overridePanelEject == false)  // allows remote system testers in Singapore to use the system
    {
      throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_NAME_OF_CAL);
    }
    else
    {
      partialCalibrationPanelUnloaded();
    }
  }



  /**
   * Do all of the steps for a complete grayscale adjustment on all cameras.
   *
   * @author Eddie Williamson
   */
  private void completeCalibration() throws XrayTesterException
  {
    if (_panelHandler.isPanelLoaded())
    {
      completeCalibrationPanelLoaded();
    }
    else
    {
      completeCalibrationPanelUnloaded();
    }
  }



  /**
   * Do all of the steps for a partial grayscale adjustment on all cameras.
   *
   * @author Eddie Williamson
   */
  private void partialCalibration() throws XrayTesterException
  {
    if (_panelHandler.isPanelLoaded())
    {
      partialCalibrationPanelLoaded();
    }
    else
    {
      partialCalibrationPanelUnloaded();
    }
  }


  /**
   * @author Eddie Williamson
   */
  private void pixelDarkCalibration() throws XrayTesterException
  {
    _adjustAndConfirmFileLogger.append("pixelDark only");
    turnXraysOff();

    performCalibStepOnCameras(_cameras, XrayCameraCalibEnum.DO_DARK_PIXEL_CAL);

    _timeOfLastPixelDarkCal = System.currentTimeMillis();
  }


  /**
   * @author Eddie Williamson
   */
  private boolean timeHasElapsedForCompleteCal(int extendedTimeSeconds)
  {
    long completeCalTimeoutMillis = 1000L *
                                    (extendedTimeSeconds + _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_COMPLETE_ADJUSTMENT_TIMEOUT_SECONDS));
    long elapsedTimeMillis = System.currentTimeMillis() - _timeOfLastCompleteCal;
    if (elapsedTimeMillis >= completeCalTimeoutMillis)
    {
      return true;
    }
    else
    {
      return false;
    }
  }


  /**
   * @author Eddie Williamson
   */
  private boolean timeHasElapsedForPixelLightCal(int extendedTimeSeconds)
  {
    long pixelLightTimeoutMillis = 1000L *
                                   (extendedTimeSeconds + _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_PIXEL_LIGHT_TIMEOUT_SECONDS));
    long elapsedTimeMillis = System.currentTimeMillis() - _timeOfLastPixelLightCal;
    if (elapsedTimeMillis >= pixelLightTimeoutMillis)
    {
      return true;
    }
    else
    {
      return false;
    }
  }


 /**
   * @author Eddie Williamson
   */
  private boolean timeHasElapsedForPixelDarkCal()
  {
    long pixelDarkTimeoutMillis = 1000L * _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_PIXEL_DARK_TIMEOUT_SECONDS);
    long elapsedTimeMillis = System.currentTimeMillis() - _timeOfLastPixelDarkCal;
    if (elapsedTimeMillis >= pixelDarkTimeoutMillis)
    {
      return true;
    }
    else
    {
      return false;
    }
  }



  /**
   * @author Eddie Williamson
   */
  private void waitForDarkThreadToComplete() throws XrayTesterException
  {
    try
    {
      _cameraDarkCalibThread.waitForTasksToComplete();
    }
    catch (XrayTesterException xex)
    {
      throw xex;
    }
    catch (Exception ex)
    {
      Assert.logException(ex);
    }
  }


  /**
   * This is called from the class CalGrayscaleDarkBegin
   * which is event driven by the start of panel load.
   * Also called from diagnostics.
   * @author Eddie Williamson
   */
  void runIfNeededUsingMediumTimeout() throws XrayTesterException
  {
    Boolean forceLogging = false;
    int timeExtension;

    if((_majorEvent == TestExecutionEventEnum.INSPECT_FOR_PRODUCTION) &&
       (_minorEvent == ImageAcquisitionEventEnum.IMAGE_ACQUISITION))
    {
      timeExtension = _timeExtensionForCompleteCalAtInspectionInSeconds;
    }
    else
    {
      timeExtension = _timeExtensionForCompleteCalAtLoadInSeconds;
    }

    _adjustAndConfirmFileLogger.append("runIfNeededUsingMediumTimeout (completeCalAtLoad)");

    if (timeHasElapsedForCompleteCal(timeExtension))
    {
      if ((overridePanelEject == false) && _panelHandler.isPanelLoaded())
      {
        // Panel is loaded, raise exception
        throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_NAME_OF_CAL);
      }

      _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
      _hardwareObservable.setEnabled(false);

      try
      {
        _originalMotionProfile = _panelPositioner.getActiveMotionProfile();
        completeCalibration();
        _panelPositioner.setMotionProfile(_originalMotionProfile);
        turnXraysOff();

        if (!_limits.didTestPass(_result))
        {
          populateFailureInformation();
          forceLogging = true;
        }
      }
      catch (XrayTesterException xe)
      {
        forceLogging = true;
        throw xe;
      }
      finally
      {
        logInformation(forceLogging);
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    }
/*    else if (timeHasElapsedForPixelLightCal(_timeExtensionForPartialCalAtLoadInSeconds))
    {
      // Shouldn't be called with panel loaded during normal operations.
      // Could possibly from diagnostics. Remove once we support panel loaded.
      if ((overridePanelEject == false) && _panelHandler.isPanelLoaded())
      {
        // Panel is loaded, raise exception
        throw new PanelInterferesWithAdjustmentException();
      }

      _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
      _hardwareObservable.setEnabled(false);

      try
      {
        _originalMotionProfile = _panelPositioner.getActiveMotionProfile();
        partialCalibration();
        _panelPositioner.setMotionProfile(_originalMotionProfile);
        turnXraysOff();

        if (!_limits.didTestPass(_result))
        {
          populateFailureInformation();
          forceLogging = true;
        }
      }
      catch (XrayTesterException xe)
      {
        forceLogging = true;
        throw xe;
      }
      finally
      {
        logInformation(forceLogging);
        _hardwareObservable.setEnabled(true);
      }
      _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    }*/
    else
    {
      _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
      // Diagnostics needs this
      /** @todo Turn back on when new camera firmware is rolled out, test to see if known problem is fixed */
//      pixelDarkCalibration();
//      forceLogging = false;
//      logInformation(forceLogging);
    }
  }



  /**
   * Runs a complete cal if the timeout has elapsed (takes ~8sec)
   * else runs partial cal (takes ~5sec)
   * Call when loading a panel in any context except production
   * testing which uses runIfNeededAlternate()
   * @author Eddie Williamson
   */
  void runNow() throws XrayTesterException
  {
    Boolean forceLogging = false;
    _adjustAndConfirmFileLogger.append("runNow");
    /** @todo Uncomment when new camera firmware is rolled out, test to see
     * if known problem is fixed. Until then just run complete cal.
     **/
//    if (timeHasElapsedForCompleteCal(0))
//    {
      completeCalibration();
//    }
//    else
//    {
//      partialCalibrationNoPanelLoaded();
//    }

      if (!_limits.didTestPass(_result))
      {
        populateFailureInformation();
        forceLogging = true;
      }

    logInformation(forceLogging);
  }


  /**
   * Does the part of cal that occurs just before image acquisition.
   * Requires xrays on/off control. Determines if complete or partial
   * cal is needed and does the minimum number of steps needed.
   *
   * TEMPORARY: For now we raise an exception if we need to do a cal
   * requiring light. After release 1.0 we need to evaluate the
   * mechanical clearances with rigor to determine how much overhang
   * a panel can have and still work. Until then, eject the panel.
   *
   * @author Eddie Williamson
   */
  void runIfNeededUsingLongestTimeout() throws XrayTesterException
  {    
    _adjustAndConfirmFileLogger.append("runIfNeededUsingLongestTimeout (partialCalAtImageAcquisition)");

    /** @todo rfh - MAYBE switch back to pixel light cal timeout when switching back to partial cal */
    //    if (timeHasElapsedForPixelLightCal(_timeExtensionForPartialCalAtImageAcquisitionInSeconds))
    if (timeHasElapsedForCompleteCal(_timeExtensionForPartialCalAtImageAcquisitionInSeconds))
    {
      //      if (overridePanelEject)
      //      {
      // For system test allow running with panel loaded. Tester is responsible to make
      // sure this panel has no overhang
      /** @todo switch back to partial after new camera firmware rolled out*/
      //        partialCalibrationNoPanelLoaded();  // Actually the panel is loaded

      // Just try the Cal. It will check to see if a panel is loaded, throw the exception
      // if it is and do all the right stuff. We shouldn't care up here
      completeCalibration();

      if (!_limits.didTestPass(_result))
      {
        populateFailureInformation();
      }

      //      }
      //      else
      //      {
      //        // If can't run with panel then raise exception to force panel unload
      //        try
      //        {
      //          // We are forcing them to unload the panel, keep interlocks happy
      //          turnXraysOff();
      //        }
      //        catch (XrayTesterException ex)
      //        {
      //          // do nothing, want user to see the next exception instead
      //        }
      //        throw new PanelInterferesWithAdjustmentException();
      //      }
    }
    else
    {
      _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
    }
  }


  /**
   * During normal production testing, call this from the preferred place to do grayscale.
   * Currently this is at the end of panel unload.
   * @author Eddie Williamson
   */
  void runIfNeededUsingShortestTimeout() throws XrayTesterException
  {    
    Boolean forceLogging = false;
    _adjustAndConfirmFileLogger.append("runIfNeededUsingShortestTimeout (completeCalAtUnload)");

    // For LP1&2 we don't want to run cal at panel unload because they can't hit the interlock
    // enable button fast enough. However, this routine is also called at the end of STARTUP.
    // We must force it to run the first time anyway else confirmations fail.
    if (_calAtPanelUnload || (_timeOfLastCompleteCal == 0))
    {
      if (timeHasElapsedForCompleteCal(_timeExtensionForCompleteCalAtUnloadInSeconds))
      {
        _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
        _hardwareObservable.setEnabled(false);

        try
        {
          _originalMotionProfile = _panelPositioner.getActiveMotionProfile();
          completeCalibration();
          _panelPositioner.setMotionProfile(_originalMotionProfile);
          turnXraysOff();

          if (!_limits.didTestPass(_result))
          {
            populateFailureInformation();
            forceLogging = true;
          }
        }
        catch (XrayTesterException xe)
        {
          forceLogging = true;
          throw xe;
        }
        finally
        {
          if (_majorEvent == XrayTesterEventEnum.STARTUP)
          {
            logInformation(forceLogging);
          }
          _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
      }/*
      else if (timeHasElapsedForPixelLightCal(_timeExtensionForPartialCalAtUnloadInSeconds))
      {
        _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
        _hardwareObservable.setEnabled(false);

        try
        {
          _originalMotionProfile = _panelPositioner.getActiveMotionProfile();
          partialCalibration();
          _panelPositioner.setMotionProfile(_originalMotionProfile);
          turnXraysOff();

          if (!_limits.didTestPass(_result))
          {
            populateFailureInformation();
            forceLogging = true;
          }
        }
        catch (XrayTesterException xe)
        {
          forceLogging = true;
          throw xe;
        }
        finally
        {
          logInformation(forceLogging);
          _hardwareObservable.setEnabled(true);
        }
        _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
      }*/
      else
      {
//        _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
        if (timeHasElapsedForPixelDarkCal())
        {
          // Need to do this calibration because dark current for different CCD segment fluctuate with temperature.
          pixelDarkCalibration();
        }
      }
      // Intentially omit pixel dark. If needed, it will run on panel load or the beginning of imaging
    }
    else
    {
      _result.setStatus(HardwareTaskStatusEnum.DEFERRED);
    }
  }

  /**
   * @author Eddie Williamson
   */
  private void updateConfigParameters() throws XrayTesterException
  {
    /** @todo rfh - remove this when LP2 goes away*/
    if(_earlyPrototype == null)
    {
      // Don't do the license manager check during regression tests!
      if (UnitTest.unitTesting())
      {
        _earlyPrototype = new Boolean(false);
      }
      else
      {
        _earlyPrototype = new Boolean(LicenseManager.isEarlyPrototypeHardwareEnabled());
      }
    }
    _timeExtensionForPartialCalAtUnloadInSeconds = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_PARTIAL_CAL_AT_UNLOAD_IN_SECONDS);
    _timeExtensionForCompleteCalAtUnloadInSeconds = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_UNLOAD_IN_SECONDS);

    _timeExtensionForCompleteCalAtLoadInSeconds = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_LOAD_IN_SECONDS);
    _timeExtensionForPartialCalAtLoadInSeconds = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_PARTIAL_CAL_AT_LOAD_IN_SECONDS);

    _timeExtensionForPartialCalAtImageAcquisitionInSeconds = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_PARTIAL_CAL_AT_IMAGE_ACQUISITION_IN_SECONDS);

    _timeExtensionForCompleteCalAtImageAcquisitionInSeconds = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_IMAGE_ACQUISITION_IN_SECONDS);

    _timeExtensionForCompleteCalAtInspectionInSeconds = _config.getIntValue(SoftwareConfigEnum.CAMERA_GRAYSCALE_TIME_EXTENSION_FOR_COMPLETE_CAL_AT_INSPECTION_IN_SECONDS);

    // Define stage position #1
    _stagePosition1 = new StagePositionMutable();
    _stagePosition1.setXInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_X_NANOMETERS));
    _stagePosition1.setYInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_1_Y_NANOMETERS));

    // Define stage position #2
    _stagePosition2 = new StagePositionMutable();
    _stagePosition2.setXInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_X_NANOMETERS));
    _stagePosition2.setYInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_2_Y_NANOMETERS));

    // Define stage position #3
    _stagePosition3 = new StagePositionMutable();
    _stagePosition3.setXInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_X_NANOMETERS));
    _stagePosition3.setYInNanometers(_config.getIntValue(HardwareCalibEnum.CAMERA_GRAYSCALE_STAGE_POSITION_3_Y_NANOMETERS));


    // Build list of cameras in group #1 by reading from hardware.calib
    _cameraGroup1.clear();
    HardwareCalibEnum calibEnum = HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_ONE_LIST_OF_CAMERAS;
    String groupOneCameras = _config.getStringValue(calibEnum);
    String[] groupOneList = groupOneCameras.split(",");
    for (String numberString : groupOneList)
    {
      if( (numberString.length() > 0) && (numberString.equalsIgnoreCase("none") == false) )
      {
        try
        {
          int cameraId = StringUtil.convertStringToInt(numberString);
          _cameraGroup1.add(XrayCameraArray.getCamera(XrayCameraIdEnum.getEnum(new Integer(cameraId))));
        }
        catch(BadFormatException bfe)
        {
          InvalidValueDatastoreException de = new InvalidValueDatastoreException(calibEnum.getKey(), numberString, calibEnum.getFileName());
          de.initCause(bfe);
          throw de;
        }
      }
    }

    // Build list of cameras in group #2 by reading from hardware.calib
    // #2 is special case and can be set to "none", if so then leave list empty (0 length)
    _cameraGroup2.clear();
    calibEnum = HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_TWO_LIST_OF_CAMERAS;
    String groupTwoCameras = _config.getStringValue(calibEnum);
    String[] groupTwoList = groupTwoCameras.split(",");
    for (String numberString : groupTwoList)
    {
      if( (numberString.length() > 0) && (numberString.equalsIgnoreCase("none") == false) )
      {
        try
        {
          int cameraId = StringUtil.convertStringToInt(numberString);
          _cameraGroup2.add(XrayCameraArray.getCamera(XrayCameraIdEnum.getEnum(new Integer(cameraId))));
        }
        catch(BadFormatException bfe)
        {
          InvalidValueDatastoreException de = new InvalidValueDatastoreException(calibEnum.getKey(), numberString, calibEnum.getFileName());
          de.initCause(bfe);
          throw de;
        }
      }
    }

    // Build list of cameras in group #3 by reading from hardware.calib
    _cameraGroup3.clear();
    calibEnum = HardwareCalibEnum.CAMERA_GRAYSCALE_GROUP_THREE_LIST_OF_CAMERAS;
    String groupThreeCameras = _config.getStringValue(calibEnum);
    String[] groupThreeList = groupThreeCameras.split(",");
    for (String numberString : groupThreeList)
    {
      if( (numberString.length() > 0) && (numberString.equalsIgnoreCase("none") == false) )
      {
        try
        {
          int cameraId = StringUtil.convertStringToInt(numberString);
          _cameraGroup3.add(XrayCameraArray.getCamera(XrayCameraIdEnum.getEnum(cameraId)));
        }
        catch(BadFormatException bfe)
        {
          InvalidValueDatastoreException de = new InvalidValueDatastoreException(calibEnum.getKey(), numberString, calibEnum.getFileName());
          de.initCause(bfe);
          throw de;
        }
      }
    }
  }

  /**
   * @author Eddie Williamson
   */
  private void populateFailureInformation()
  {
    StringBuilder buffer = new StringBuilder();

    //Add the localized 'Camera' string
    buffer.append(StringLocalizer.keyToString("ACD_CAMERAS_PLURAL_KEY") + " ");

    //Add the set of cameras that failed. We don't care what the specific failure
    //was, at least for now
    buffer.append(_stringOfFailingCameras.toString());

    //Get rid of the last comma in the camera list
    buffer.deleteCharAt(buffer.length() - 1);

    //Add the localized FAILED string
    buffer.append(" " + StringLocalizer.keyToString("CD_FAILED_KEY"));


    buffer.append("\n\n" + _result.getTaskStatusMessage());

    //Set the message in the result passed to observers
    _result.setTaskStatusMessage(buffer.toString());
    // result was set waiting for the camera threads to finish

    //Add the suggestect action localized key
    _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CAMERA_GRAYSCALE_FAILED_SUGGESTED_ACTION_KEY"));
  }

  /**
   * When called this entry point will perform a complete grayscale cal on all cameras.
   * This is called from the service GUI as well as from event triggers
   *
   * @author Eddie Williamson
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
    //Swee Yee Wong - Camera debugging purpose
    debug("HighMagCalGrayscale");
    Boolean forceLogging = false;

    // Start reporting our progress
    reportProgress(0);

    //Notify IAE that we are about to change programs and move the stage
    _imageAcquisitionEngine.getReconstructedImagesProducer().calibrationExecutionHasChangedCameraAndStagePrograms();

    // If the event information is valid (we were triggered from an event), handle it.
    if(_eventInformationIsValid)
    {
      // We have to do different things based on what event pair and start/end state we see
      // so go to the method that handles this
      handleEventTriggers();
    }
    else
    {
      try
      {
        updateConfigParameters();
        if ((overridePanelEject == false) && _panelHandler.isPanelLoaded())
        {
          // Panel is loaded, raise exception
          throw new PanelInterferesWithAdjustmentAndConfirmationTaskException(_NAME_OF_CAL);
        }

        _originalMotionProfile = _panelPositioner.getActiveMotionProfile();
        completeCalibration();
        _panelPositioner.setMotionProfile(_originalMotionProfile);

        if (!_limits.didTestPass(_result))
        {
          // On failure help the user
          populateFailureInformation();
          forceLogging = true;
        }
        logInformation(forceLogging);
      }
      catch (XrayTesterException ex)
      {
        throw ex;
      }
    }

    // Either way we can update the progress observable that we are done
    reportProgress(100);
  }

  /**
   * This method maps major/minor event pairs and their start/end state to what
   * Grayscale cal action needs to be taken for that event pair.
   *
   * @author Reid Hayhow
   */
  private void handleEventTriggers() throws XrayTesterException
  {
    if(overridePanelEject == true && _timeOfLastCompleteCal != 0 && _runCalWithPanelInSystem == false)
    {
      LocalizedString localizedString = new LocalizedString("ACD_GRAYSCALE_DISABLED_FROM_SW_CONFIG_WARNING_KEY",
                                                             new Object[]{});
      InspectionEventObservable inspectionEventObservable = InspectionEventObservable.getInstance();
      MessageInspectionEvent event = new MessageInspectionEvent(localizedString);
      inspectionEventObservable.sendEventInfo(event);
      return;
    }

    // What to do if it is startup time
    if (_majorEvent == XrayTesterEventEnum.STARTUP)
    {
      handleStartupEvent();
    }
    // What to do if anyone is loading/unloading a panel
    else if (_minorEvent instanceof PanelHandlerEventEnum)
    {
      handlePanelHandlerEvent();
    }
    // Handle the many test execution events
    else if (_majorEvent instanceof TestExecutionEventEnum)
    {
      handleTestExecutionEvent();
    }
    // Somehow we got called in an unexpected context. We can't do nothing, so
    // run the fallback for grayscale
    else
    {
      runGrayscaleCatchAll("Main event loop");
    }
  }

  /**
   * Method that differentiates between running a cal for high quality images and
   * for low quality images in the context of test execution
   *
   * @author Reid Hayhow
   */
  private void runIfNeededForTestExecutionAsNecessary(String logString) throws XrayTesterException
  {
    // Handle the case for high quality images
    if (highQualityImageRequired())
    {
      //CalGrayscaleBeforeImaging -> _grayscaleCal.runIfNeededBeforeIA()
      if (_config.isDeveloperDebugModeOn())
      {
        _adjustAndConfirmFileLogger.append(logString + ", high quality images necessary, " + _minorEventIsStart);
      }
      // Can't use the shortest timeout because that increases the likelyhood of
      // forcing a panel unload immediately after a panel is loaded
      runIfNeededUsingMediumTimeout();
    }
    // Or the case for low quality images
    else
    {
      // New trigger, low quality images OK BUT got to have called at least once!
      if (_config.isDeveloperDebugModeOn())
      {
        _adjustAndConfirmFileLogger.append(logString + ", low quality images OK, " + _minorEventIsStart);
      }
      runIfNeededUsingLongestTimeout();
    }
  }

  /**
   * Method for handling grayscale called from Panel Handler events.
   *
   * @author Reid Hayhow
   */
  private void handlePanelHandlerEvent() throws XrayTesterException
  {
    // The user has specified that a panel be loaded manually from the UI, this
    // gives the shortest timeout on grayscale cal
    if (_majorEvent == PanelHandlerEventEnum.LOAD_PANEL &&
        _minorEventIsStart == true)
    {
      //CalGrayscaleAfterImaging -> _grayscaleCal.runNow()
      if (_config.isDeveloperDebugModeOn())
      {
        _adjustAndConfirmFileLogger.append("PanelHandlerEventEnum.LOAD_PANEL major/minor event, " + _minorEventIsStart);
      }
      runIfNeededUsingShortestTimeout();
    }
    // Unloading the panel is the preferred place/time to run during production, do it at the end of the unload
    else if (_minorEvent == PanelHandlerEventEnum.UNLOAD_PANEL &&
        _minorEventIsStart == false)
    {
      //CalGrayscaleAfterImaging -> _grayscaleCal.runIfNeededPreferred()
      if (_config.isDeveloperDebugModeOn())
      {
        _adjustAndConfirmFileLogger.append("PanelHandlerEventEnum.UNLOAD_PANEL minor event, " + _minorEventIsStart);
      }
      runIfNeededUsingShortestTimeout();
    }
    // Loading a panel is also OK, but less desirable. Run it before the panel loads
    else if (_minorEvent == PanelHandlerEventEnum.LOAD_PANEL &&
             _minorEventIsStart == true)
    {
      //CalGrayscaleDarkEnd -> _grayscaleCal.runNow()
      // This is no longer run now becuase that caused it to run 5 seconds after it just ran
      if (_config.isDeveloperDebugModeOn())
      {
        _adjustAndConfirmFileLogger.append("PanelHandlerEventEnum.LOAD_PANEL minor event, " + _minorEventIsStart);
      }
      runIfNeededUsingMediumTimeout();
    }
    // Somehow we got called in an unexpected context, run the catch all for grayscale cal
    else
    {
      runGrayscaleCatchAll("PanelHandler event");
    }
  }

  /**
   * Handler for Startup events
   *
   * @author Reid Hayhow
   */
  private void handleStartupEvent() throws XrayTesterException
  {
    // Only run at the end of a major start event
    if (_minorEventIsStart == false)
    {
      if (_config.isDeveloperDebugModeOn())
      {
        _adjustAndConfirmFileLogger.append("XrayTester.Start major event, false");
      }
      runIfNeededUsingShortestTimeout();
    }
    // Somehow we got called in an unexpected context, run the catch all for grayscale cal
    else
    {
      runGrayscaleCatchAll("Startup");
    }
  }

  /**
   * Method to handle running grayscale from the context of a test execution
   * major event.
   *
   * @author Reid Hayhow
   */
  private void handleTestExecutionEvent() throws XrayTesterException
  {
    // If we are starting the tester from a test execution event...
    if (_minorEvent == XrayTesterEventEnum.STARTUP &&
        _minorEventIsStart == false)
    {
      // New trigger, got to have called at least once!
      if (_config.isDeveloperDebugModeOn())
      {
        _adjustAndConfirmFileLogger.append("TestExecutionEventEnum.(any) , XrayTesterEventEnum.STARTUP, " + _minorEventIsStart);
      }
      runIfNeededUsingLongestTimeout();
    }     
        // Or if we are starting a test execution event...
    else if (_majorEvent == _minorEvent &&
            _minorEventIsStart == true)
    {
      runIfNeededForTestExecutionAsNecessary("TestExecutionEventEnum major event");
    }
    // Or if we are ending a test execution event...
    else if (_majorEvent == _minorEvent &&
            _minorEventIsStart == false)
    {
      runIfNeededUsingShortestTimeout();
    }
    // Or we are running image acquisition from within TestExecution
    else if ((_minorEvent == ImageAcquisitionEventEnum.IMAGE_ACQUISITION &&
            _minorEventIsStart == true))
    {
      runIfNeededForTestExecutionAsNecessary("TestExecutionEventEnum/Image Acquisition");
    }
    // Somehow we got called in an unexpected context, run the catch all for grayscale cal
    else
    {
      runGrayscaleCatchAll("TestExecution");
    }
  }

  /**
   * This is a method to call grayscale cal for any unexpected event triggers. It
   * uses the longest timeouts and clearly logs the problem to the adjustAndConfirm.log
   *
   * @author Reid Hayhow
   */
  private void runGrayscaleCatchAll(String triggerInformationString) throws XrayTesterException
  {
    _adjustAndConfirmFileLogger.append(">>>>>>Fix this! Running cal if needed before IA as catchall for " +
                            triggerInformationString +
                            _majorEvent + " " +
                            _minorEvent + " " +
                            _minorEventIsStart);

    runIfNeededUsingLongestTimeout();
  }

  /**
   * Method to encapsulate the decision of when high quality images are required and
   * when they are optional.
   *
   * @author Reid Hayhow
   */
  private boolean highQualityImageRequired()
  {
    // Assume we want high quality images
    boolean highQualityImageRequired = true;

    // There are a few cases where low quality images are OK, check for them
    if (_majorEvent == TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES ||
        _majorEvent == TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT)
    {
      highQualityImageRequired = false;
    }

    return highQualityImageRequired;
  }

  /**
   * Overriden method for setting Grayscale Cal to run ASAP. Necessary because
   * Grayscale Cal has internal timers that HardwareTask class doesn't know about.
   *
   * @author Reid Hayhow
   */
  public void setTaskToRunASAP() throws DatastoreException
  {
    // Set the timeout used by HardwareTask super class
    super.setTaskToRunASAP();

    // Set the special timeouts used by this class to 0 as well
    _timeOfLastCompleteCal = 0;
    _timeOfLastPixelDarkCal = 0;
    _timeOfLastPixelLightCal = 0;
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * override function to perform extra check for calgrayscale become of it different extension given
   * @author sheng chuan
   * XCR-3093 ,Keep running grayscale after the task timeout
   * TODO : THis function can be optimized since all condition is duplicated from above
   */
  protected boolean isExecuteNeeded()
  {
    int timeExtension = 0;
    if (super.isExecuteNeeded() == false)
    {
      return false;
    }
    else
    {
      if ((_majorEvent == PanelHandlerEventEnum.LOAD_PANEL
        && _minorEventIsStart == true) || (_minorEvent == PanelHandlerEventEnum.UNLOAD_PANEL
        && _minorEventIsStart == false) || (_majorEvent == _minorEvent
        && _minorEventIsStart == false) || (_minorEventIsStart == false
        && _majorEvent == XrayTesterEventEnum.STARTUP))
      {
        timeExtension = _timeExtensionForCompleteCalAtUnloadInSeconds;
      }
      else if ((_minorEvent == PanelHandlerEventEnum.LOAD_PANEL
        && _minorEventIsStart == true) || (_majorEvent == _minorEvent
        && _minorEventIsStart == true) || (_minorEvent == ImageAcquisitionEventEnum.IMAGE_ACQUISITION
        && _minorEventIsStart == true))
      {
        if (highQualityImageRequired())
        {
          if ((_majorEvent == TestExecutionEventEnum.INSPECT_FOR_PRODUCTION)
            && (_minorEvent == ImageAcquisitionEventEnum.IMAGE_ACQUISITION))
          {
            timeExtension = _timeExtensionForCompleteCalAtInspectionInSeconds;
          }
          else
          {
            timeExtension = _timeExtensionForCompleteCalAtLoadInSeconds;
          }
        }
        else
        {
          timeExtension = _timeExtensionForPartialCalAtImageAcquisitionInSeconds;
        }
      }
      else if (_minorEvent == XrayTesterEventEnum.STARTUP
        && _minorEventIsStart == false)
      {
        timeExtension = _timeExtensionForPartialCalAtImageAcquisitionInSeconds;
      }
      return timeHasElapsedForCompleteCal(timeExtension);
    }
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
