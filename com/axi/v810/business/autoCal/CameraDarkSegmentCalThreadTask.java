package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * This class is designed to allow camera dark segment calibration to be performed
 * in parallel with other tasks such as stage motion.
 * @author Farn Sern
 */

class CameraDarkSegmentCalThreadTask extends ThreadTask<Object>
{
  private AbstractXraySource _xraySource;
  CalGrayscale _calGrayscale = CalGrayscale.getInstance();

  /**
   * @author Farn Sern
   */
  CameraDarkSegmentCalThreadTask()
  {
    super("Perform Camera Dark Segment Calibration Thread Task");
    _xraySource = AbstractXraySource.getInstance();

  }

  /**
   * @author Farn Sern
   */
  protected Object executeTask() throws Exception
  {
    _xraySource.off();
    _calGrayscale.performDarkSegmentCal();
    _xraySource.on(); //This is for the subsequent light segment cal
    return null;
  }

  /**
   * @author Farn Sern
   */
  protected void clearCancel()
  {
    // Do nothing.
  }

  /**
   * @author Farn Sern
   */
  protected void cancel() throws XrayTesterException
  {
    // Do nothing.
  }

}
