package com.axi.v810.business.autoCal;

import com.axi.util.Enum;

/**
 * @author Reid Hayhow
 *
 */
class CalSystemFiducialResultTypeEnum extends Enum
{
   private static int index = 0;
   static final CalSystemFiducialResultTypeEnum FIDUCIAL_FOUND_RESULT = new CalSystemFiducialResultTypeEnum(index++);
   static final CalSystemFiducialResultTypeEnum STAGE_X_RESULT = new CalSystemFiducialResultTypeEnum(index++);
   static final CalSystemFiducialResultTypeEnum STAGE_Y_RESULT = new CalSystemFiducialResultTypeEnum(index++);
   static final CalSystemFiducialResultTypeEnum CAMERA_COLUMN_RESULT = new CalSystemFiducialResultTypeEnum(index++);
   static final CalSystemFiducialResultTypeEnum CAMERA_ROW_RESULT = new CalSystemFiducialResultTypeEnum(index++);

  /**
   * CameraCalibrationDataTypeEnum
   *
   * @author Reid Hayhow
   */
  protected CalSystemFiducialResultTypeEnum(int id)
  {
    super(id);
  }

  /**
   * getNumberOfResults
   *
   * @author Reid Hayhow
   */
  protected int getNumberOfResults()
  {
    return CAMERA_ROW_RESULT.getId();
  }
}
