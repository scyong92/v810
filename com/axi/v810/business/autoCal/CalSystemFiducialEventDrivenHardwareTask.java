package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;


/**
 * Calibration (adjustment) to determine the stage locations that puts the
 * system fiducial on each of the cameras. This information is used
 * by reconstruction.
 * @author Eddie Williamson
 */
public class CalSystemFiducialEventDrivenHardwareTask extends EventDrivenHardwareTask
{
  private static CalSystemFiducialEventDrivenHardwareTask _instance;
  private static Config _config = Config.getInstance();

  // Printable name for this procedure
  private static final String _NAME_OF_CAL = StringLocalizer.keyToString("ACD_SYSTEM_OFFSETS_NAME_KEY");

  // A template match quality metric below this will fail the cal (triangle not in image)
  // The range is from -1.0 to 1.0 where 1.0 is a perfect match
  private static final float _QUALITYTHRESHOLD = 0.80F;

  // These must be static so _numberOfCameras is set before calling Create[Limits|Results]
  // and any other code that needs to know array sizes based on number of cameras.
  private static final XrayCameraArray _xrayCameraArray = XrayCameraArray.getInstance();

  // This cal is called on the first event after the timeout occurs.
  private static final int _EVENT_FREQUENCY = 1;

  // The template to match within the image is generated from these parameters
  private static int _templateBorderPixels = 10;
  private static int _templateSizeXPixels = XrayTester.getSystemFiducialWidthInNanometers()/
                                            MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static int _templateSizeYPixels = XrayTester.getSystemFiducialHeightInNanometers()/
                                            MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static int _minPixelsFromCorner = 25;

  // Create an image of the SF triangle with a border
  private static Image _template = CalibrationSupportUtil.createTemplateImageOfSystemFiducial(_templateBorderPixels,
                                                                                              _templateBorderPixels,
                                                                                              _templateSizeXPixels,
                                                                                              _templateSizeYPixels);
  // Crop out that portion we want as the template for matching
  private static RegionOfInterest _roi = new RegionOfInterest((_templateSizeXPixels + _templateBorderPixels - _minPixelsFromCorner),
                                                              (_templateSizeYPixels + _templateBorderPixels - _minPixelsFromCorner),
                                                              (_templateBorderPixels + _minPixelsFromCorner),
                                                              (_templateBorderPixels + _minPixelsFromCorner),
                                                              0,
                                                              RegionShapeEnum.RECTANGULAR);

  private static Image _croppedTemplate = Image.createCopy(_template, _roi);

  // Define the region to be imaged for this cal
  // X & Y are relative to the system fiducial
  private static final int _PIXELS2NANOMETERS = MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static final int _SF_RANGE_PIXELS = 45;// + 250;
  // Values to create region +/-_SF_RANGE_PIXELS pixels around the nominial fiducial location plus the border widths for the template for matching
  private static final int _REGION_TO_IMAGE_X_NANOMETERS = ( -_SF_RANGE_PIXELS - _minPixelsFromCorner) * _PIXELS2NANOMETERS;
  private static final int _REGION_TO_IMAGE_Y_NANOMETERS = ( -_SF_RANGE_PIXELS - _templateBorderPixels) * _PIXELS2NANOMETERS;
  private static final int _REGION_TO_IMAGE_HEIGHT_NANOMETERS = (2 * _SF_RANGE_PIXELS + _templateBorderPixels + _minPixelsFromCorner) * _PIXELS2NANOMETERS;
  private static final int _REGION_TO_IMAGE_WIDTH_NANOMETERS = (2 * _SF_RANGE_PIXELS + _templateBorderPixels + _minPixelsFromCorner) * _PIXELS2NANOMETERS;

  // object variables
  private DoubleRef _matchQuality = new DoubleRef(0.0F);
  private final String _logDirectory = Directory.getSystemOffsetsCalLogDir();
  private long _logTimeoutSeconds = _config.getIntValue(SoftwareConfigEnum.SYSTEM_OFFSETS_LOG_RESULTS_EVERY_TIMEOUT_SECONDS);
  private long _logTimeoutMilliSeconds = _logTimeoutSeconds * 1000L;
  private int _maxLogFilesInDirectory = _config.getIntValue(SoftwareConfigEnum.SYSTEM_OFFSETS_MAX_LOG_FILES_IN_DIRECTORY);

  private List<ProjectionRequestThreadTask> _projectionRequestThreadTasks;
  private List<VirtualProjection> _virtualProjections;
  private VirtualProjection _virtualProjection;
  private Map<AbstractXrayCamera, DoubleCoordinate> _cameraToFiducialCoordinatesWithinImageMap = new HashMap<AbstractXrayCamera, DoubleCoordinate>();
  private Map<AbstractXrayCamera, DoubleCoordinate> _cameraToFiducialCoordinatesRelativeToCameraOriginMap = new HashMap<AbstractXrayCamera, DoubleCoordinate>();
  private Map<AbstractXrayCamera, DoubleCoordinate> _cameraToStageXYMap = new HashMap<AbstractXrayCamera, DoubleCoordinate>();
  private DirectoryLoggerAxi _logger;
  private Map<AbstractXrayCamera, Map<CalSystemFiducialResultTypeEnum, Result>> _cameraToLocalResultsMap = new HashMap<AbstractXrayCamera, Map<CalSystemFiducialResultTypeEnum, Result>>();
  private Map<AbstractXrayCamera, Map<CalSystemFiducialResultTypeEnum, Limit>> _cameraToLocalLimitsMap = new HashMap<AbstractXrayCamera, Map<CalSystemFiducialResultTypeEnum, Limit>>();
  private TimerUtil _timer = new TimerUtil();
  private boolean _printInstrumentedTimes = false;
  private ImageAcquisitionEngine _imageAcquisitionEngine = ImageAcquisitionEngine.getInstance();
  private ImageCoordinate _templateCoordinates;
  private CalMagnification _calMagnification = CalMagnification.getInstance();

  /**
   * @author Eddie Williamson
   */
  static
  {
    // Make sure the cropped template fits within the image of the triangle so it is valid
    Assert.expect((_templateBorderPixels + _minPixelsFromCorner) <= (_templateSizeXPixels + 2 * _templateBorderPixels));
  }

  /**
   * @author Eddie Williamson
   */
  private CalSystemFiducialEventDrivenHardwareTask()
  {
    super(_NAME_OF_CAL,
          _EVENT_FREQUENCY,
          _config.getIntValue(SoftwareConfigEnum.SYSTEM_OFFSETS_EXECUTE_EVERY_TIMEOUT_SECONDS),
          ProgressReporterEnum.SYSTEM_OFFSETS_ADJUSTMENT_TASK);

    _logger = new DirectoryLoggerAxi(_logDirectory,
                                     FileName.getSystemOffsetsLogFileBasename(),
                                     FileName.getLogFileExtension(),
                                     _maxLogFilesInDirectory);

    _taskType = HardwareTaskTypeEnum.ADJUSTMENT_TASK_TYPE;

    setTimestampEnum(HardwareCalibEnum.SYSTEM_OFFSET_LAST_TIME_RUN);
  }

  /**
   * @author Eddie Williamson
   */
  public static synchronized CalSystemFiducialEventDrivenHardwareTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new CalSystemFiducialEventDrivenHardwareTask();
    }
    return _instance;
  }

  /**
   * By default all tests can run on any hardware configuration.
   *
   * @return true if this test is capable of being run on the currently defined hardware.
   * @author Eddie Williamson
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * @author Eddie Williamson
   */
  protected void setUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to up position for low mag task.
        XrayActuator.getInstance().up(false);
      }
    }
    // do nothing
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
    
    //bypass unnecessary task when in passive mode
    if(isBypassNeededForUnnecessaryHardwareTaskInPassiveMode())
    {
      disable();
    }
    if(XrayTester.isXXLBased() || XrayTester.isS2EXEnabled())
    {
      PanelClamps.getInstance().close();
    }
  }

  /**
   * This method provides a single point of contact where tasks can put their clean-up code that may be reused (for
   * example executeTask and abortTask may both use the same cleanUp code)
   * @throws XrayTesterException
   * @author Eddie Williamson
   */
  protected void cleanUp() throws XrayTesterException
  {
    if(XrayActuator.isInstalled())
    {
      if (XrayCPMotorActuator.isInstalled() == true)
      {
        //move x-rays Z Axis to Home and Safe position.
        XrayActuator.getInstance().home(false,false);
      }
    }
    // Free image memory, reset pointers after checking for null.
    if(_projectionRequestThreadTasks != null)
    {
      _projectionRequestThreadTasks.clear(); // Probably unnecessary but what the heck.
      _projectionRequestThreadTasks = null;
    }

    if(_virtualProjections != null)
    {
      for (VirtualProjection virtualProjection : _virtualProjections)
      {
        virtualProjection.clear();
      }
      _virtualProjections.clear();
      _virtualProjections = null;
    }
    
    if (_matchQuality != null)
      _matchQuality = null;
    
    //confirmation and adjustment passive mode - enable task after bypass
    // XCR1717 - enabled back the task only when _automatedTask is true. 
    //         - This is to fix CD&A disabled task enabled back after loop for the 2nd time.
    if(_automatedTask)
    {
      enable();
      _automatedTask = false;
    }
  }

  /**
   * Logs detailed information about the cal (and saves images) when the timeout
   * is exceeded. The log information is formatted as comma separated values to
   * it can be imported into Excel for manual analysis.
   * @param forceLogging If true then ignore the timeout and log anyway.
   * @author Eddie Williamson
   */
  private void logInformation(Boolean forceLogging) throws DatastoreException
  {
    StringBuilder logString = new StringBuilder();
    long logTimeout = _logTimeoutMilliSeconds;
    ProjectionInfo projectionInfo = null;
    boolean didWeLog = false;

    // Create header lines
    logString.append(_NAME_OF_CAL + "\n");
    logString.append(","); // offset first column
    logString.append("Camera,StageX(nm),StageY(nm),Col(pixels),Row(pixels),");
    logString.append("Direction,RegionX(nm),RegionY(nm),RegionWidth(nm),RegionHeight(nm),");
    logString.append("StageXCameraCenter(nm),StageYCameraCenter(nm),ImageColOnCamera,ImageRowOnCamera,");
    logString.append("ImageFileName,");
    logString.append("OffsetsColInImage,OffsetsRowInImage,");
    logString.append("\n");

    for (VirtualProjection virtualProjection : _virtualProjections)
    {
      logString.append(","); // offset first column

      AbstractXrayCamera camera = virtualProjection.getCamera();
      logString.append(camera.getId() + ",");

      // Log cal'ed information
      logString.append(_cameraToStageXYMap.get(camera).getX() + "," +
                       _cameraToStageXYMap.get(camera).getY() + "," +
                       _cameraToFiducialCoordinatesRelativeToCameraOriginMap.get(camera).getX() + "," +
                       _cameraToFiducialCoordinatesRelativeToCameraOriginMap.get(camera).getY() + ",");

      // Log the ProjectionRequest data
      logString.append(CalibrationSupportUtil.ProjectionRequestToCSVString(virtualProjection.getProjectionRequestThreadTask()) +
                       ",");

      // Log the ProjectionInfo data
      projectionInfo = virtualProjection.getProjectionRequestThreadTask().getProjectionInfo();
      logString.append(CalibrationSupportUtil.ProjectionInfoToCSVString(projectionInfo) + ",");

      // Log filename for reference, we save the image if we log the data
      String filename = "cam_" + camera.getId() + ".jpg";
      logString.append(filename + ",");

      // Log location of fiducial within the image
      logString.append(_cameraToFiducialCoordinatesWithinImageMap.get(camera).getX() + "," +
                       _cameraToFiducialCoordinatesWithinImageMap.get(camera).getY() + ",");

      // terminate each line
      logString.append("\n");
    }

    if (forceLogging)
    {
      _logger.log(logString.toString());
      didWeLog = true;
    }
    else
    {
      didWeLog = _logger.logIfTimedOut(logString.toString(), logTimeout);
    }

    if (didWeLog)
    {
      for (VirtualProjection virtualProjection : _virtualProjections)
      {
        // Save image to file, use the same filename that was logged above
        AbstractXrayCamera camera = virtualProjection.getCamera();
        Image projectionImage = virtualProjection.getImage();
        String filename = "cam_" + camera.getId() + ".jpg";
        XrayImageIoUtil.saveJpegImage(projectionImage.getBufferedImage(), _logDirectory + File.separator + filename);
      }
    }
  }

  /**
   * createResultsObject Create the results object for this test result.
   * @author Eddie Williamson
   */
  protected void createResultsObject()
  {
    _cameraToLocalResultsMap.clear();

    // Create container for the others
    _result = new Result(_NAME_OF_CAL);

    for (AbstractXrayCamera camera : CalibrationSupportUtil.getSystemFiducialCameras())
    {
      Result cameraContainer = new Result(getName());

      Result found = new Result(new Boolean(true), getName());
      cameraContainer.addSubResult(found);
      Map<CalSystemFiducialResultTypeEnum, Result> indexToResultMap = new TreeMap<CalSystemFiducialResultTypeEnum, Result>();
      indexToResultMap.put(CalSystemFiducialResultTypeEnum.FIDUCIAL_FOUND_RESULT, found);
      _cameraToLocalResultsMap.put(camera, indexToResultMap);

      Result stageX = new Result(new Double(0.0), getName());
      cameraContainer.addSubResult(stageX);
      indexToResultMap.put(CalSystemFiducialResultTypeEnum.STAGE_X_RESULT, stageX);

      Result stageY = new Result(new Double(0.0), getName());
      cameraContainer.addSubResult(stageY);
      indexToResultMap.put(CalSystemFiducialResultTypeEnum.STAGE_Y_RESULT, stageY);

      Result cameraCol = new Result(new Double(0.0), getName());
      cameraContainer.addSubResult(cameraCol);
      indexToResultMap.put(CalSystemFiducialResultTypeEnum.CAMERA_COLUMN_RESULT, cameraCol);

      Result cameraRow = new Result(new Double(0.0), getName());
      cameraContainer.addSubResult(cameraRow);
      indexToResultMap.put(CalSystemFiducialResultTypeEnum.CAMERA_ROW_RESULT, cameraRow);

      _result.addSubResult(cameraContainer);
    }
  }


  /**
   * @author Eddie Williamson
   */
  protected void createLimitsObject() throws DatastoreException
  {
    _cameraToLocalLimitsMap.clear();

    // Create container for the others
    _limits = new Limit(null, null, null, _NAME_OF_CAL);

    for (AbstractXrayCamera camera : CalibrationSupportUtil.getSystemFiducialCameras())
    {
      Limit cameraContainer = new Limit(null, null, null, "Camera " + camera.getId());

      Limit foundLimit = new Limit(null, null, null, "System offsets found");
      cameraContainer.addSubLimit(foundLimit);
      Map<CalSystemFiducialResultTypeEnum, Limit> indexToLimitMap = new TreeMap<CalSystemFiducialResultTypeEnum, Limit>();
      indexToLimitMap.put(CalSystemFiducialResultTypeEnum.FIDUCIAL_FOUND_RESULT, foundLimit);
      _cameraToLocalLimitsMap.put(camera, indexToLimitMap);

      Limit stageX = new Limit(new Double(_parsedLimits.getLowLimitDouble("stageX")),
                               new Double(_parsedLimits.getHighLimitDouble("stageX")),
                               "nanometers", "Stage X position");
      cameraContainer.addSubLimit(stageX);
      indexToLimitMap.put(CalSystemFiducialResultTypeEnum.STAGE_X_RESULT, stageX);

      Limit stageY = new Limit(new Double(_parsedLimits.getLowLimitDouble("stageY")),
                               new Double(_parsedLimits.getHighLimitDouble("stageY")),
                               "nanometers", "Stage Y position");
      cameraContainer.addSubLimit(stageY);
      indexToLimitMap.put(CalSystemFiducialResultTypeEnum.STAGE_Y_RESULT, stageY);

      Limit cameraCol = new Limit(new Double(_parsedLimits.getLowLimitDouble("cameraCol")),
                                  new Double(_parsedLimits.getHighLimitDouble("cameraCol")),
                                  "pixels", "Camera column for offsets");
      cameraContainer.addSubLimit(cameraCol);
      indexToLimitMap.put(CalSystemFiducialResultTypeEnum.CAMERA_COLUMN_RESULT, cameraCol);

      Limit cameraRow = new Limit(new Double(_parsedLimits.getLowLimitDouble("cameraRow")),
                                  new Double(_parsedLimits.getHighLimitDouble("cameraRow")),
                                  "pixels", "Camera row for offsets");
      cameraContainer.addSubLimit(cameraRow);
      indexToLimitMap.put(CalSystemFiducialResultTypeEnum.CAMERA_ROW_RESULT, cameraRow);

      _limits.addSubLimit(cameraContainer);
    }
  }

  /**
   * @author Greg Esparza
   */
  private void updateCalibratedDataMap(AbstractXrayCamera xRayCamera,
                                       Map<ConfigEnum, Object> calibratedDataMap)
  {
    Assert.expect(xRayCamera != null);
    Assert.expect(calibratedDataMap != null);

    DoubleCoordinate stagePositionCoordinatesInNanometers = _cameraToStageXYMap.get(xRayCamera);
    DoubleCoordinate fiducialCoordinatesInNanometers = _cameraToFiducialCoordinatesRelativeToCameraOriginMap.get(xRayCamera);

    if (xRayCamera.getId() == 0)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_0_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 1)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_1_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 2)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_2_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 3)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_3_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 4)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_4_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 5)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_5_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 6)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_6_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 7)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_7_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 8)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_8_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 9)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_9_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 10)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_10_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 11)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_11_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 12)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_12_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else if (xRayCamera.getId() == 13)
    {
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_X_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_STAGE_Y_POSITION_NANOMETERS, (int)stagePositionCoordinatesInNanometers.getY());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_COLUMN, (int)fiducialCoordinatesInNanometers.getX());
      calibratedDataMap.put(HardwareCalibEnum.SYSTEM_FIDUCIAL_CAMERA_13_ROW, (int)fiducialCoordinatesInNanometers.getY());
    }
    else
      Assert.expect(false);
  }

  /**
   * @author Eddie Williamson
   * @author Greg Esparza
   */
  private void storeNewCalibrationValues() throws DatastoreException
  {
    Map<ConfigEnum, Object> calibratedDataMap = new HashMap<ConfigEnum, Object>();

    List<AbstractXrayCamera> xRayCameras = XrayCameraArray.getCameras();
    for (AbstractXrayCamera xRayCamera : xRayCameras)
      updateCalibratedDataMap(xRayCamera, calibratedDataMap);

    _config.setValues(calibratedDataMap);
  }

  /**
   * Used by regression test to check pass/fail status
   * @return true if all results pass compared to their limits
   * @author Eddie Williamson
   */
  boolean didTestPass()
  {
    return _limits.didTestPass(_result);
  }

  /**
   *
   * @author Eddie Williamson
   */
  protected void executeTask() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
    //Swee Yee Wong - Camera debugging purpose
    debug("calSystemOffset");
    Boolean forceLogging = false;
    _hardwareObservable.stateChangedBegin(this, CalEventEnum.ADJUSTING_GRAYSCALE);
    _hardwareObservable.setEnabled(false);

    try
    {
      int additionalProjectionWidthNanometers = _config.getIntValue(HardwareCalibEnum.ADDITIONAL_PROJECTION_WIDTH_NANOMETERS);
      reportProgress(0);

      // Create the list of projections required
      _virtualProjections = new ArrayList<VirtualProjection>();
      _projectionRequestThreadTasks = new ArrayList<ProjectionRequestThreadTask>();
      for (AbstractXrayCamera camera : CalibrationSupportUtil.getSystemFiducialCameras())
      {
        _virtualProjection = new VirtualProjection(camera,
                                                   ScanPassDirectionEnum.FORWARD,
                                                   _REGION_TO_IMAGE_X_NANOMETERS,
                                                   _REGION_TO_IMAGE_Y_NANOMETERS,
                                                   _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                                                   _REGION_TO_IMAGE_HEIGHT_NANOMETERS,
                                                   additionalProjectionWidthNanometers);
        _virtualProjections.add(_virtualProjection);
        _projectionRequestThreadTasks.add(_virtualProjection.getProjectionRequestThreadTask());
      }

      // Get the projections
      reportProgress(33);

      TestExecutionTimer testExecutionTimer = new TestExecutionTimer();

      // Initialize the IAE before requesting projections
      _imageAcquisitionEngine.initialize();
                    
      _imageAcquisitionEngine.acquireProjections(_projectionRequestThreadTasks, testExecutionTimer, true);
      reportProgress(66);

      // Save off a set of cameras that failed for message generation
      List<AbstractXrayCamera> listOfFailingCameras = new ArrayList<AbstractXrayCamera>();
      // Switch over to set usage
      Set<AbstractXrayCamera> setOfFailingCameras = new TreeSet<AbstractXrayCamera>();

      // Analyze the images, calculate values, check against limits
      for (VirtualProjection virtualProjection : _virtualProjections)
      {
        AbstractXrayCamera camera = virtualProjection.getCamera();
        Image projectionImage = virtualProjection.getImage();

        boolean useTemplateMatch = true;
        if (useTemplateMatch)
        {
          _matchQuality = new DoubleRef(); // Metric returned from template match
          _templateCoordinates = Filter.matchTemplate(projectionImage,
                                                      _croppedTemplate,
                                                      MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                      _matchQuality);

          if (_matchQuality.getValue() > _QUALITYTHRESHOLD)
          {
            // Found template within the image
            _cameraToFiducialCoordinatesWithinImageMap.put(camera,
                                                           new DoubleCoordinate(_templateCoordinates.getX() + _minPixelsFromCorner,
                                                                                _templateCoordinates.getY() + _minPixelsFromCorner));
          }
          else
          {
            // Did not find template (fiducial)
            _cameraToLocalResultsMap.get(camera).get(CalSystemFiducialResultTypeEnum.FIDUCIAL_FOUND_RESULT).setResults(new Boolean(false));
            _cameraToFiducialCoordinatesWithinImageMap.put(camera, new DoubleCoordinate( -1, -1));
            // Add this camera to the list of failing cameras
            listOfFailingCameras.add(camera);
          }
        }
        else
        {
          _cameraToFiducialCoordinatesWithinImageMap.put(camera, ImageFeatureExtraction.findSystemFiducial(projectionImage));
        }

        int cameraColumnWhereImageStarts = virtualProjection.getCameraColumnWhereImageStarts();
        _cameraToFiducialCoordinatesRelativeToCameraOriginMap.put(camera,
                                                                  new DoubleCoordinate(Math.round(_cameraToFiducialCoordinatesWithinImageMap.get(camera).getX() +
                                                                                                  cameraColumnWhereImageStarts),
                                                                                       Math.round(_cameraToFiducialCoordinatesWithinImageMap.get(camera).getY())));

        int stageXNanometers = virtualProjection.getStageX();
        int stageYNanometers = virtualProjection.getStageY();
        _cameraToStageXYMap.put(camera, new DoubleCoordinate(stageXNanometers, stageYNanometers));

        _cameraToLocalResultsMap.get(camera).get(CalSystemFiducialResultTypeEnum.STAGE_X_RESULT).setResults(new Double(_cameraToStageXYMap.get(camera).getX()));
        _cameraToLocalResultsMap.get(camera).get(CalSystemFiducialResultTypeEnum.STAGE_Y_RESULT).setResults(new Double(_cameraToStageXYMap.get(camera).getY()));
        _cameraToLocalResultsMap.get(camera).get(CalSystemFiducialResultTypeEnum.CAMERA_COLUMN_RESULT).setResults(new Double(_cameraToFiducialCoordinatesRelativeToCameraOriginMap.
                                                                                                                             get(camera).getX()));
        _cameraToLocalResultsMap.get(camera).get(CalSystemFiducialResultTypeEnum.CAMERA_ROW_RESULT).setResults(new Double(_cameraToFiducialCoordinatesRelativeToCameraOriginMap.get(
            camera).getY()));
      }
      storeNewCalibrationValues();

      if (_limits.didTestPass(_result))
      {
        _timer.start();
        storeNewCalibrationValues();
        _timer.stop();

        // Running and passing this cal invalidates the magnification cal
        _calMagnification.setTaskToRunASAP();

        if (_printInstrumentedTimes)
        {
          System.out.println("Storing values takes " + _timer.getElapsedTimeInMillis() + " millis");
        }
      }
      else
      {
        // Start building up the list of failing cameras
        StringBuilder failingCameraString = new StringBuilder();

        // Generate the list of failing cameras
        for (AbstractXrayCamera camera : listOfFailingCameras)
        {
          failingCameraString.append(camera.getId() + ",");
        }

        // Get rid of the extra delimiter at the end of the list
        if(failingCameraString.length() > 0)
          failingCameraString.deleteCharAt(failingCameraString.length() - 1);

        // Create a message that has the correct singular/plural for cameras
        String cameraOrCamerasStringParameter = null;

        if (listOfFailingCameras.size() > 1)
        {
          cameraOrCamerasStringParameter = "ACD_CAMERAS_MULTIPLE_KEY";
        }
        else
        {
          cameraOrCamerasStringParameter = "ACD_CAMERA_KEY";
        }

        // Add the list of failing cameras to an array of Objects to be paramerers to the localised string
        Object[] params = {StringLocalizer.keyToString(cameraOrCamerasStringParameter),
                          failingCameraString.toString()};

        // Create a localized string with the parameter
        LocalizedString statusMessage = new LocalizedString("CD_SYSTEM_OFFSET_FAILED_STATUS_KEY", params);

        // Set the result object with the status and suggested action
        _result.setTaskStatusMessage(StringLocalizer.keyToString(statusMessage));
        _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_SYSTEM_OFFSET_FAILED_SUGGESTED_ACTION_KEY"));

        // Force logging on failure so we can do post mortem
        forceLogging = true;
      }
      logInformation(forceLogging);
      reportProgress(100);
    }
    finally
    {
      _hardwareObservable.setEnabled(true);
    }
    _hardwareObservable.stateChangedEnd(this, CalEventEnum.ADJUSTING_GRAYSCALE);
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return(_automatedTask && CalibrationSupportUtil.isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(_isHighMagTask));
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}

