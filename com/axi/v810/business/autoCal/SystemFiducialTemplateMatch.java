package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.hardware.*;
// For normal mag
//import com.axi.util.image.RegionOfInterest;
//import com.axi.util.image.Transform;
//import com.axi.util.image.RegionShapeEnum;
//import com.axi.util.image.Image;
//import com.axi.util.image.MatchTemplateTechniqueEnum;
//import com.axi.util.image.Filter;
//import com.axi.util.DoubleRef;
//import com.axi.util.image.ImageCoordinate;

/**
 *
 * @author John Dutton
 */
public class SystemFiducialTemplateMatch
{
  // The template to match within the image is generated from these parameters
  private static int _templateBorderPixels = 10;
// For normal mag
//  private static int _templateSizeXPixels  = 80;
//  private static int _templateSizeYPixels  = 159;
  private static int _templateSizeXPixels = XrayTester.getSystemFiducialWidthInNanometers()/
                                            MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static int _templateSizeYPixels = XrayTester.getSystemFiducialHeightInNanometers()/
                                            MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  private static int _minPixelsFromCorner  = 25;

  private static Image _template =
     CalibrationSupportUtil.createTemplateImageOfSystemFiducial(_templateBorderPixels,
       _templateBorderPixels, _templateSizeXPixels, _templateSizeYPixels);


  // Crop out that portion we want as the template for matching
  private static RegionOfInterest _roi = new RegionOfInterest(
      _templateSizeXPixels + _templateBorderPixels - _minPixelsFromCorner,
      _templateSizeYPixels + _templateBorderPixels - _minPixelsFromCorner,
      _templateBorderPixels + _minPixelsFromCorner,
      _templateBorderPixels + _minPixelsFromCorner,
      0,
      RegionShapeEnum.RECTANGULAR);

  private static final Image _croppedTemplate = Image.createCopy(_template, _roi);
  
  private static int _templateBorderPixelsInHighMag = (int)Math.floor( 10 * 1.905 );


//  private static int _templateSizeXPixels  = 80;
//  private static int _templateSizeYPixels  = 159;
    // For high mag
  private static int _templateSizeXPixelsInHighMag = XrayTester.getSystemFiducialWidthInNanometers()/
                                            MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
  private static int _templateSizeYPixelsInHighMag = XrayTester.getSystemFiducialHeightInNanometers()/
                                            MagnificationEnum.H_NOMINAL.getNanoMetersPerPixel();
  private static int _minPixelsFromCornerInHighMag  = (int)Math.floor( 25 * 1.905 );

  private static Image _templateInHighMag =
     CalibrationSupportUtil.createTemplateImageOfSystemFiducial(_templateBorderPixelsInHighMag,
       _templateBorderPixelsInHighMag, _templateSizeXPixelsInHighMag, _templateSizeYPixelsInHighMag);


  // Crop out that portion we want as the template for matching
  private static RegionOfInterest _roiInHighMag = new RegionOfInterest(
      _templateSizeXPixelsInHighMag + _templateBorderPixelsInHighMag - _minPixelsFromCornerInHighMag,
      _templateSizeYPixelsInHighMag + _templateBorderPixelsInHighMag - _minPixelsFromCornerInHighMag,
      _templateBorderPixelsInHighMag + _minPixelsFromCornerInHighMag,
      _templateBorderPixelsInHighMag + _minPixelsFromCornerInHighMag,
      0,
      RegionShapeEnum.RECTANGULAR);

  private static final Image _croppedTemplateInHighMag = Image.createCopy(_templateInHighMag, _roiInHighMag);

  // A template match quality metric below this will fail the cal (triangle not in image)
  // The range is from -1.0 to 1.0 where 1.0 is a perfect match
  private static final float _QUALITYTHRESHOLD = 0.80F;

  private SystemFiducialTemplateMatch()
  {
    super();
  }
  /**
   * @return ImageCoordinate of the location where the system fiducial point is in the image.  If the system
   *   fiducial isn't found then point (-1, -1) is returned.  Be sure to check for this value!
   *
   * @author John Dutton
   */
  public static ImageCoordinate matchTemplate(Image image, boolean flipX, boolean flipY)
  {
    DoubleRef _matchQuality = new DoubleRef(0.0F);
    ImageCoordinate templateCoordinates;
    ImageCoordinate sysFidPointLocation = new ImageCoordinate(-1, -1);  // Indicates failure.

    Image template = getTemplate(flipX, flipY);
    templateCoordinates = Filter.matchTemplate(image,
                                                template,
                                                MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                _matchQuality);
    template.decrementReferenceCount();

    if (_matchQuality.getValue() > _QUALITYTHRESHOLD)
    {
      // Found template within the image.  Translate from template upper left to sys fid point.
      if (flipX && flipY)
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _templateBorderPixels);
        sysFidPointLocation.setY(templateCoordinates.getY() + _templateBorderPixels);
      }
      else if (flipX)
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _templateBorderPixels);
        sysFidPointLocation.setY(templateCoordinates.getY() + _minPixelsFromCorner);
      }
      else if (flipY)
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _minPixelsFromCorner);
        sysFidPointLocation.setY(templateCoordinates.getY() + _templateBorderPixels);
      }
      else
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _minPixelsFromCorner);
        sysFidPointLocation.setY(templateCoordinates.getY() + _minPixelsFromCorner);
      }
    }
    else
    {
      // Did not find template (fiducial), so don't fill out sysFidPointLocation.  Return it with
      // (-1, -1) as the coordinates.
    }
    return sysFidPointLocation;
  }

/**
 * Generates cropped template of system fiducial point in orientation specified by
 * flipX and flipY.  Caller needs to decrement reference on returned image when done.
 * Routine is public so template can be printed out for debugging purposes.
 *
 * @author John Dutton
 */
  public static Image getTemplate(boolean flipX, boolean flipY)
  {
    // Use a copy or transform will alter the cropped template.
    Image copyOfCroppedTemplate = Image.createCopy(_croppedTemplate, RegionOfInterest.createRegionFromImage(_croppedTemplate));
    if (flipX && flipY)
      Transform.flipXY(copyOfCroppedTemplate);
    else if (flipX)
      Transform.flipX(copyOfCroppedTemplate);
    else if (flipY)
      Transform.flipY(copyOfCroppedTemplate);
    return copyOfCroppedTemplate;
  }

  /**
   * @return ImageCoordinate of the location where the system fiducial point is in the image.  If the system
   *   fiducial isn't found then point (-1, -1) is returned.  Be sure to check for this value!
   *
   * @author John Dutton
   */
  public static ImageCoordinate matchTemplateInHighMag(Image image, boolean flipX, boolean flipY)
  {
    DoubleRef _matchQuality = new DoubleRef(0.0F);
    ImageCoordinate templateCoordinates;
    ImageCoordinate sysFidPointLocation = new ImageCoordinate(-1, -1);  // Indicates failure.

    Image template = getTemplateInHighMag(flipX, flipY);
    templateCoordinates = Filter.matchTemplate(image,
                                                template,
                                                MatchTemplateTechniqueEnum.CROSS_CORRELATION_WITH_PIXEL_VALUE_LEVELING,
                                                _matchQuality);
    template.decrementReferenceCount();

    if (_matchQuality.getValue() > _QUALITYTHRESHOLD)
    {
      // Found template within the image.  Translate from template upper left to sys fid point.
      if (flipX && flipY)
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _templateBorderPixelsInHighMag);
        sysFidPointLocation.setY(templateCoordinates.getY() + _templateBorderPixelsInHighMag);
      }
      else if (flipX)
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _templateBorderPixelsInHighMag);
        sysFidPointLocation.setY(templateCoordinates.getY() + _minPixelsFromCornerInHighMag);
      }
      else if (flipY)
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _minPixelsFromCornerInHighMag);
        sysFidPointLocation.setY(templateCoordinates.getY() + _templateBorderPixelsInHighMag);
      }
      else
      {
        sysFidPointLocation.setX(templateCoordinates.getX() + _minPixelsFromCornerInHighMag);
        sysFidPointLocation.setY(templateCoordinates.getY() + _minPixelsFromCornerInHighMag);
      }
    }
    else
    {
      // Did not find template (fiducial), so don't fill out sysFidPointLocation.  Return it with
      // (-1, -1) as the coordinates.
    }
    return sysFidPointLocation;
  }

/**
 * Generates cropped template of system fiducial point in orientation specified by
 * flipX and flipY.  Caller needs to decrement reference on returned image when done.
 * Routine is public so template can be printed out for debugging purposes.
 *
 * @author John Dutton
 */
  public static Image getTemplateInHighMag(boolean flipX, boolean flipY)
  {
    // Use a copy or transform will alter the cropped template.
    Image copyOfCroppedTemplate = Image.createCopy(_croppedTemplateInHighMag, RegionOfInterest.createRegionFromImage(_croppedTemplateInHighMag));
    if (flipX && flipY)
      Transform.flipXY(copyOfCroppedTemplate);
    else if (flipX)
      Transform.flipX(copyOfCroppedTemplate);
    else if (flipY)
      Transform.flipY(copyOfCroppedTemplate);
    return copyOfCroppedTemplate;
  }
}
