package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;
import java.text.*;
import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.*;

/**
 * Class that calculates the MTF of an edge by using the FWHM of its line spread function
 * as a proxy for MTF.  MTF is an optical measurement of how well an imaging system captures
 * contrasts between black and white lines.  The smaller the number, the better.
 *
 * @author John Dutton
 */
public class EdgeMTF
{
  // Members to control debug information.
  private static String _saveDebugFilesToDirName;
  private static boolean _displayDebugText = false;
  private static boolean _saveDebugFiles = false;

  // Member used as label for debug output.  Date stamp is default value, can also
  // set in constructor with image file name.
  private String _imageFileName = (new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.SSS")).format(Calendar.getInstance().getTime()) + "_";

  // Members to hold results.
  private double _edgeAngleInDegrees = -1.0;
  private double _edgeSpreadRiseInPixels = -1.0;
  private double _lineSpreadFWHMInPixels = -1.0;

  // Member that controls which edge detection kernel is used for the Sobel transform.
  private boolean _verticalEdge = false;

  private double[] _rawESF;
  private int _oversamplingFactor = 2;

  public int get_oversamplingFactor()
  {
    return _oversamplingFactor;
  }

  public double[] get_rawESF()
  {
    if (_rawESF == null)
      return new double[0];

    double[] rawESF = new double[_rawESF.length];
    System.arraycopy(_rawESF, 0, rawESF, 0, _rawESF.length);
    return rawESF;
  }


  /**
   * Constructor that calculates MTF during object creation on edge in image.  Use get methods to
   * access MTF value and intermediate values.
   *
   * @param verticalEdge should be true if the edge lies more vertical than horizontal in the image;
   * increases the Sobel Transform's ability to find the edge.
   * @throws DatastoreException occurs when debug files cannot be created or written to
   * @throws EdgeMTFBusinessException Occurs because something went wrong in MTF algorithm.
   * Most likely, something wasn't right with the image that was analyzed.  Values gotten
   * from the instance should be considered invalid.
   *
   * @author John Dutton
   */
  public EdgeMTF(Image img, boolean verticalEdge) throws DatastoreException, EdgeMTFBusinessException
  {
    _verticalEdge = verticalEdge;
    calculateLineSpreadFWHM(img);
  }

  /**
   * Constructor that takes an addition parameter, image file name, which is used when naming
   * debug output files if setSaveDebugFiles() has been invoked.
   *
   * @param verticalEdge should be true if the edge lies more vertical than horizontal in the image;
   * increases the Sobel Transform's ability to find the edge.
   * @param imageFileName used as part of name for debug output files if setSaveDebugFiles()
   * has been invoked
   * @throws DatastoreException occurs when debug files cannot be created or written to
   * @throws EdgeMTFBusinessException Occurs because something went wrong in MTF algorithm.
   * Most likely, something wasn't right with the image that was analyzed.  Values gotten
   * from the instance should be considered invalid.
   *
   * @author John Dutton
   */
  public EdgeMTF(Image img, boolean verticalEdge, String imageFileName) throws DatastoreException,
      EdgeMTFBusinessException
  {
    _verticalEdge = verticalEdge;
    _imageFileName = imageFileName + "_";
    calculateLineSpreadFWHM(img);
  }


  /**
   * @return how many pixels the edge requires to transition from dark to light.
   * @author John Dutton
   */
  public double getEdgeSpreadRiseInPixels()
  {
    return _edgeSpreadRiseInPixels;
  }

  /**
   * @return MTF of edge.  Line spread FWHM is an image processing proxy for MTF.
   * @author John Dutton
   */
   public double getLineSpreadFWHMInPixels()
  {
    return _lineSpreadFWHMInPixels;
  }

  /**
   * @return Angle of edge.  Primarily useful for validating that the expected edge
   * was found and used.
   * @author John Dutton
   */
   public double getEdgeAngleInDegrees()
  {
    return _edgeAngleInDegrees;
  }

  /**
   * @author John Dutton
   */
   public static void setDisplayDebugText()
  {
    _displayDebugText = true;
  }

  /**
   * Saves images that represent intermediate steps of MTF algorithm and Excel
   * data files of the edge spread rise function and line spread function.
   *
   * @author John Dutton
   */
  public static void setSaveDebugFiles(String saveDebugFilesToDirName) throws DatastoreException
  {
    _saveDebugFilesToDirName = saveDebugFilesToDirName;
    _saveDebugFiles = true;

    // Handle case where dirName already exists.
    File file = new File(_saveDebugFilesToDirName);
    if (file.exists())
     if (file.isDirectory() == false)
       throw new FileFoundWhereDirectoryWasExpectedDatastoreException(saveDebugFilesToDirName);
     else
       return;

    // dirName doesn't exist so create it.
    FileUtilAxi.mkdirs(_saveDebugFilesToDirName);
  }


  /**
   * The control logic that steps through the high-level algorithm to calculate the Full
   * Width Half Maximum (FWHM) of the line spread function/data.
   *
   * @throws DatastoreException occurs when debug files cannot be created or written to
   * @author John Dutton
   */
  private void calculateLineSpreadFWHM(Image img) throws DatastoreException, EdgeMTFBusinessException
  {
    if (_saveDebugFiles)
    {
      XrayImageIoUtil.savePngImage(img, _saveDebugFilesToDirName + File.separator +
                                   _imageFileName + "edge_img.png");
    }

    long startTime = 0, endTime = 0;
    if (_displayDebugText)
    {
      startTime = System.currentTimeMillis();
      System.out.println("Start MTF");
    }

    // Apply median filter (if not b&w) and threshold the image.  (Some test images are b&w
    // and running median filter on them results in all black.)  There is talk about the cameras
    // filtering images on board the cameras, so the median filter would go away then.
    Image thresholdedImg;
    if (!isBlackAndWhiteImage(img))
      thresholdedImg = medianFilterImage(img);
    else
    {
      thresholdedImg = img;
      thresholdedImg.incrementReferenceCount();
    }
    thresholdImage(thresholdedImg);
    if (_saveDebugFiles)
      XrayImageIoUtil.saveJpegImage(thresholdedImg.getBufferedImage(), _saveDebugFilesToDirName + File.separator +
                                    _imageFileName + "thresholded_img.jpg");

    // Sobelize the thresholded image so edges stand out.  Pixels not on an edge will turn black.
    Image sobelImg = null;
    if (_verticalEdge)
      // vertical sobel kernel is good for vertical edges
      sobelImg = Filter.convolveSobelVertical(thresholdedImg);
    else
      // likewise for horizontal edges
      sobelImg = Filter.convolveSobelHorizontal(thresholdedImg);
    thresholdedImg.decrementReferenceCount();

    if (_saveDebugFiles)
    {
      // Convert negative values to positive ones so they will be seen in jpeg.  Perform
      // on a copy although it wouldn't (shouldn't) make a difference to calculateEdgeAngle.
      Image sobelImageCopy = Image.createCopy(sobelImg, RegionOfInterest.createRegionFromImage(sobelImg));
      Arithmetic.absoluteValueInPlace(sobelImageCopy);
      XrayImageIoUtil.saveJpegImage(sobelImageCopy.getBufferedImage(), _saveDebugFilesToDirName + File.separator +
                                    _imageFileName + "sobelized_img.jpg");
      sobelImageCopy.decrementReferenceCount();
    }
    // Calculate theta for the edge in 2 passes of Hough transform (using Sobelized image).
    // 1st pass use angles 0 to 179 degrees in increments of 1.0 degrees.
    double roughTheta = calculateEdgeAngle(sobelImg, 0, 179, 1.0, _imageFileName + "first_hough_img.jpg");


    // 2nd pass use angles +- 1.25 degrees from roughTheta in increments of 0.01 degrees.
    double startingTheta = roughTheta - 1.25;
    _edgeAngleInDegrees = calculateEdgeAngle(sobelImg, startingTheta, 250, 0.01,
                                                 _imageFileName + "second_hough_img.jpg");

    sobelImg.decrementReferenceCount();
    // Project along the edge with real image at fineTheta degrees using Hough transform.
    int subpixelFactor = 4;

    double[] edgeSpreadData = calculateRadonProjection(img, _edgeAngleInDegrees, _oversamplingFactor, subpixelFactor);

    _edgeSpreadRiseInPixels = calculateEdgeRise(edgeSpreadData, _oversamplingFactor, subpixelFactor);

    _lineSpreadFWHMInPixels = calculateLineSpreadFWHMFromEdgeSpreadData(edgeSpreadData,
                                                                        _oversamplingFactor,
                                                                        subpixelFactor);

    if (_displayDebugText)
    {
      endTime = System.currentTimeMillis();
      System.out.println("Stop MTF (ms) = " + (endTime - startTime));
    }
  }




  /**
   * Compute projections of image intensity along parallel lines by summing pixel values
   * along each line.  From resulting projections the sharpness of an edge can be determined.
   *
   * Although in mathematics the transformation is named Radon (after Johann Radon),
   * the implementation borrows conceptually from the Hough transform.
   *
   * @param thetaInDegrees the orientation of the array used to collect projections
   * along the edge.  This orientation should be perpendicular to the edge.  (The angle is
   * measured between the x-axis and the array.)
   * @param oversamplingFactor for development experimentation, specifies multiple for
   * increasing projection array.  Normally each array value represents a 1-pixel wide projection,
   * but if 4 was the oversamplingFactor each would represent a 1/4 pixel wide projection and would
   * increase the array size by 4.
   * @param subpixelFactor for development experimentation, specifies the pixel mass size
   * to project.  Normally each pixel is projected, but if 3 was the subpixelFactor then
   * each pixel would be broken into 9 (sliced 3 by 3) subpixel mass sizes to project.
   * @throws DatastoreException occurs when debug files cannot be created or written to
   * @return projection array of normalized image intensities
   *
   * @author John Dutton
   */
   private double[] calculateRadonProjection(Image img, double thetaInDegrees, int oversamplingFactor, int subpixelFactor)
       throws DatastoreException, EdgeMTFBusinessException

  {
    // Negative angles or angles greater than 180 are valid conceptually, but the code is designed to
    // work with following range: 0 <= theta < 180.  So, convert to that range.
    if (thetaInDegrees < 0)
      thetaInDegrees += 180;
    if (thetaInDegrees >= 180)
      thetaInDegrees -= 180;
    Assert.expect(0 <= thetaInDegrees && thetaInDegrees < 180, "theta is out of range for projection");

    // Values used below.
    double thetaInRadians = Math.toRadians(thetaInDegrees);
    double cosValue = Math.cos(thetaInRadians);
    double sinValue = Math.sin(thetaInRadians);

    // Calculate center of image to be treated as origin.  (This is the way Erick's
    // Matlab code worked and is consistent with text books on Hough.)
    int centerX = img.getWidth() / 2;
    int centerY = img.getHeight() / 2;

    // Create projection array
    // Greatest size for array is when the projection is on the image diagonal, e.g. for a square
    // image, theta = 45 degrees.  That case requires an R the length of the diagonal, which can
    // be calculated according to the formula for the length of the hypoteneuse of a right triangle.
    // (Add fudge factor of 5 to account for allocation of subpixel portion into adjacent entries,
    // integer division, roundup, etc.)
    final double subpixelSpacing = 1.0 / subpixelFactor;
    if (_displayDebugText)
      System.out.println("oversamplingFactor = " + oversamplingFactor + " subpixelFactor = " +
                         subpixelFactor + " subpixelSpacing = " + subpixelSpacing);
    int rSize = (int)((Math.hypot(img.getWidth(), img.getHeight()) + 5) * oversamplingFactor);
    int rCenter = rSize / 2;
    double[] projectionArray = new double[rSize];
    for (double bucket : projectionArray)
      bucket = 0;

    for (int y = 0; y < img.getHeight(); y++)
    {
      // Have y increase from lower left corner as depictions of Hough indicate and Matlab does
      int adjustedY = img.getHeight() - 1 - y;
      for (int x = 0; x < img.getWidth(); x++)
      {
        float pixelValue = img.getPixelValue(x, y);
        if (pixelValue > 0)
        {
          // Pixel has a grey value, so project it.

          // Break pixel and hence its grey value into small pieces to get better accuracy.
          float pixelValuePortion = pixelValue / (subpixelFactor * subpixelFactor);

          // Center y
          double yValue = adjustedY - centerY;

          // Iterate over subpixels
          for (int yy = 0; yy < subpixelFactor; yy++)
          {
            // Center x
            double xValue = x - centerX;

            // Iterate over subpixels
            for (int xx = 0; xx < subpixelFactor; xx++)
            {
              // Based on the formula for a straight line in polar coordinates,
              // determine where the sub-pixel projects into the projection array.
              double r = xValue * cosValue + yValue * sinValue;
              r *= oversamplingFactor;
              int anR = (int)Math.round(r);

              // anR is an array index into a particular projection "bucket" and r is
              // the exact spot of the projection.  Use their difference to allocate part
              // of the subpixel to an adjacent array value since the projection will typically
              // be off-center.  (Think of each array value as a bucket with width that
              // collects some protion of the subpixel brightness.)
              double portionAdjacent = anR - r;
              boolean portionAdjacentPositiveSign = true;
              if (portionAdjacent < 0)
                portionAdjacentPositiveSign = false;
              portionAdjacent = Math.abs(portionAdjacent);
              double portionInR = 1.0 - portionAdjacent;

              // Convert negative anR values into valid positive array indexes.
              // Do so by allocating negative values to the higher/top half of
              // array and positive values to the lower/bottom half.
              int rIndex;
              if (anR < 0)
                rIndex = rCenter + Math.abs(anR);
              else
                rIndex = rCenter - anR;

              // Mathematically this should never happen, but catch it if it
              // does instead of getting an array out of bounds exception.
              if (rIndex + 1 >= rSize || rIndex - 1 < 0)
                throw(new EdgeMTFBusinessException());

              // Allocate subpixel value.
              projectionArray[rIndex] = projectionArray[rIndex] + (pixelValuePortion * portionInR);
              if (portionAdjacentPositiveSign)
                projectionArray[rIndex + 1] = projectionArray[rIndex + 1] + (pixelValuePortion * portionAdjacent);
              else
                projectionArray[rIndex - 1] = projectionArray[rIndex - 1] + (pixelValuePortion * portionAdjacent);

              xValue += subpixelSpacing;  // for next iteration
            }
            yValue += subpixelSpacing;  // for next iteration
          }
        }
      }
    }


    // Chop data on the ends off since it is not valid since projections on the ends don't
    // pass through as many pixels as the ones in the center.  (Erick Lewark figured out the
    // trigonometry behind the equations below.  x2p is x2 prime in his proof.  It represents
    // the length of valid data in pixels.)
    //
    int x2p = 0;
    if (0 <= thetaInDegrees && thetaInDegrees <= 45)
      x2p = (int)(img.getWidth() * Math.cos(thetaInRadians) - img.getHeight() * Math.sin(thetaInRadians));
    else if (45 < thetaInDegrees && thetaInDegrees <= 90)
      x2p = (int)( -img.getWidth() * Math.cos(thetaInRadians) + img.getHeight() * Math.sin(thetaInRadians));
    else if (90 < thetaInDegrees && thetaInDegrees <= 135)
      x2p = (int)(img.getWidth() * Math.cos(thetaInRadians) + img.getHeight() * Math.sin(thetaInRadians));
    else if (135 < thetaInDegrees && thetaInDegrees < 180)
      x2p = (int)( -img.getWidth() * Math.cos(thetaInRadians) - img.getHeight() * Math.sin(thetaInRadians));
    else
      Assert.expect(false, "theta value not caught in if else conditional");
    x2p = Math.abs(x2p);              // Sign of x2p can be negative depending on the width and height.
    x2p *= oversamplingFactor;

    int lowerBound = rCenter - x2p / 2;
    int upperBound = rCenter + x2p / 2;

    // Chop off more data that might not be valid.  This is a good idea because it provides
    // compensation for any rounding/truncation taking place in the above calculation,
    // and for some margin of error in the Hough edge detection algorithm itself which
    // determined thetaInDegrees.
    lowerBound += 5;
    upperBound -= 5;


    // Create a new array that contains relevant values only (i.e. remove projections
    // that weren't through the widest part of image).
    final int trimmedProjectionArrayLength = upperBound - lowerBound;
//    if ((trimmedProjectionArrayLength / oversamplingFactor) < 12)
    if ((trimmedProjectionArrayLength / oversamplingFactor) < 8)
    {
      throw new EdgeMTFBusinessException();
    }

    _rawESF = new double[trimmedProjectionArrayLength];
    System.arraycopy(projectionArray, lowerBound, _rawESF, 0, trimmedProjectionArrayLength);

    // Smooth data to reduce noise.  The name esf stands for Edge Spread Function.
    // esf length is 2 less because of smoothing equation in for loop.
    final int esfLength = trimmedProjectionArrayLength - 2;
    double[] esf = new double[esfLength];
    for (int i = 0; i < trimmedProjectionArrayLength - 2; i++)
    {
      esf[i] = (.50 * _rawESF[i] + .35 * _rawESF[i+1] + .15 * _rawESF[i+2]);
    }


    // Normalize the votes
    double min = Double.POSITIVE_INFINITY;
    double max = Double.NEGATIVE_INFINITY;
    for (double value : esf)
    {
      if (value < min)
        min = value;
      if (value > max)
        max = value;
    }

    max -= min;
    for (int i = 0; i < esfLength; i++)
      esf[i] = (esf[i] - min) / max;


    // Make sure ESF is rising which is the typical or proper behavior for such a function.
    // (Other methods in this class that will use ESF assumes that it rises, such as in
    // calculateEdgeRise when it looks for 15% and 85% grey level values.)
    esf = convertArrayToRisingValuesIfNeeded(esf);

    min = Double.POSITIVE_INFINITY;
    max = Double.NEGATIVE_INFINITY;
    for (double value : _rawESF)
    {
      if (value < min)
        min = value;
      if (value > max)
        max = value;
    }

    max -= min;
    for (int i = 0; i < _rawESF.length; i++)
      _rawESF[i] = (_rawESF[i] - min) / max;
    _rawESF = convertArrayToRisingValuesIfNeeded(_rawESF);

    if (_saveDebugFiles)
    {
      // Capture data in file for graphing and analysis purposes.
      boolean append = false;
      String fileNameFullPath = _saveDebugFilesToDirName + File.separator + _imageFileName +
                                "excel plot ESF " + oversamplingFactor + " r " +
                                subpixelFactor + " sub-pixel " + ".xls";
      FileWriterUtilAxi fileWriterUtilAxi = new FileWriterUtilAxi(fileNameFullPath, append);

      fileWriterUtilAxi.open();
      for (double value : _rawESF)
      {
        fileWriterUtilAxi.writeln(Double.toString(value));
      }

      fileWriterUtilAxi.close();
    }

    return esf;
  }

  /**
   * Calculates the edge rise in pixels from dark to light.  The smaller the number the
   * sharper the edge is considered to be.
   *
   * @param edgeSpreadData radon projection data orientated as a rising edge spread function
   * @param oversamplingFactor for development experimentation, specifies multiple for
   * increasing projection array.   Must be consistent with value used by radonProjection.
   * @param subpixelFactor for development experimentation, specifies the pixel mass size
   * to project.  Must be consistent with value used by radonProjection.
   * @throws EdgeMTFBusinessException thrown when the calculation cannot proceed due to some
   * anomoly in the edgeSpreadData.
   *
   * @author John Dutton
   */
  private double calculateEdgeRise(double [] edgeSpreadData, int rOversamplingFactor, int subpixelFactor)
      throws EdgeMTFBusinessException
  {
    int esLength = edgeSpreadData.length;

    // Interpolate around 15 and 85 degrees with line equation (y=mx+b) to get sub-pixel ESF measurement
    // First handle 15 degrees
    if (edgeSpreadData[0] > 0.15)
      // Seen error when image wasn't dark enough and when it contained 2 gray sections (had 2 edges).
      // Seen when an image was mostly all dark.  Have to remember that thresholding brings out any constrast,
      // even on a visibly black image.
      throw(new EdgeMTFBusinessException());

    int index1 = -1;
    int index2 = -1;
    for (int i = 0; i < esLength; i++)
      if (edgeSpreadData[i] > 0.15)
      {
        index1 = i - 1;
        index2 = i;
        break;
      }
    if (index1 == -1 || index2 == -1)
     // Can't find 15 percent in edge rise for interpolation
     throw(new EdgeMTFBusinessException());

   // Test for noisy data, i.e. data going back down below 0.15
   int newLowerBound = index2 + 1;
   for (int i = newLowerBound; i < esLength; i++)
     if (edgeSpreadData[i] <= 0.15)
       throw(new EdgeMTFBusinessException());

    // m = (y2 - y1) / ((x2 - x1) / oversamplingFactor)
    double m = (edgeSpreadData[index2] - edgeSpreadData[index1]) / (1.0 / rOversamplingFactor);
    // b = y1 - m * x1
    double b = edgeSpreadData[index1] - m * (index1 * (1.0 / rOversamplingFactor));
    // x = (y - b) / m
    double xAt15 = (0.15 - b) / m;


    // Now handle 85 degrees
    index1 = -1;
    index2 = -1;
    for (int i = newLowerBound; i < esLength; i++)
      if (edgeSpreadData[i] > 0.85)
      {
        index1 = i - 1;
        index2 = i;
        break;
      }
    if (index1 == -1 || index2 == -1)
     // Can't find 85 percent in edge rise for interpolation
      throw(new EdgeMTFBusinessException());

    // Test for noisy data, i.e. data going back down below 0.85
    newLowerBound = index2 + 1;
    for (int i = newLowerBound; i < esLength; i++)
      if (edgeSpreadData[i] <= 0.85)
        throw(new EdgeMTFBusinessException());

    // m = (y2 - y1) / ((x2 - x1) / oversamplingFactor)
    m = (edgeSpreadData[index2] - edgeSpreadData[index1]) / (1.0 / rOversamplingFactor);
    // b = y1 - m * x1
    b = edgeSpreadData[index1] - m * (index1 * (1.0 / rOversamplingFactor));
    double xAt85 = (0.85 - b) / m;
    // x = (y - b) / m
    double edgeSpread = xAt85 - xAt15;

    if (_displayDebugText)
    {
      System.out.println("15 = " + xAt15 + " 85 = " + xAt85 + " " + edgeSpread);
    }

    return edgeSpread;

  }


 /**
  * Calculates the full width, half max (FWHM) of the line spread function that is
  * the differential of the edge spread rise data.  The smaller the FWHM number, the
  * sharper the edge is considered to be.  FWHM of the line spread function is considered
  * a proxy for MTF (Modulation Transfer Function).  MTF is an optical measurement of how
  * well an imaging system captures contrasts between black and white lines.
  *
  * @param edgeSpreadData radon projection data orientated as a rising edge spread function
  * @param oversamplingFactor for development experimentation, specifies multiple for
  * increasing projection array.   Must be consistent with value used by radonProjection.
  * @param subpixelFactor for development experimentation, specifies the pixel mass size
  * to project.  Must be consistent with value used by radonProjection.
  * @throws DatastoreException occurs when debug files cannot be created or written to
  * @throws EdgeMTFBusinessException thrown when the calculation cannot proceed due to some
  * anomoly in the edgeSpreadData.
  *
  * @author John Dutton
  */
 private double calculateLineSpreadFWHMFromEdgeSpreadData(double[] edgeSpreadData,
                                                          int rOversamplingFactor,
                                                          int subpixelFactor)
     throws DatastoreException, EdgeMTFBusinessException
  {
    int esLength = edgeSpreadData.length;

    // Differentiate ESF to calculate LSF.
    // First create array of differences.
    final int diffLength = esLength - 1;   // will end up with one less value
    double[] diff = new double[diffLength];
    for (int i = 0 + 1, diffIndex = 0; i < esLength; i++, diffIndex++)
      diff[diffIndex] = edgeSpreadData[i] - edgeSpreadData[i - 1];


    // Next scale so 1 is largest difference.  (Not necessary to adjust so 0 is smallest.
    // This follows Erick's Matlab code and avoids a subtraction op in for loop.)
    double maxValue = Double.NEGATIVE_INFINITY;
    for (int diffIndex = 0; diffIndex < diffLength; diffIndex++)
      if (diff[diffIndex] > maxValue)
        maxValue = diff[diffIndex];
    if (maxValue == Double.NEGATIVE_INFINITY)
     // EdgeMTF can't find maximum.
      throw(new EdgeMTFBusinessException());
    for (int diffIndex = 0; diffIndex < diffLength; diffIndex++)
      diff[diffIndex] /= maxValue;


    if (_saveDebugFiles)
    {
      // Capture LSF data in file for graphing and analysis purposes.
      boolean append = false;
      String fileNameFullPath = _saveDebugFilesToDirName + File.separator + _imageFileName +
                                "excel plot LSF " + rOversamplingFactor + " r " +
                                subpixelFactor + " sub-pixel " + ".xls";
      FileWriterUtilAxi fileWriterUtilAxi = new FileWriterUtilAxi(fileNameFullPath, append);
      fileWriterUtilAxi.open();
      for (int i = 0; i < diffLength; i++)
      {
        fileWriterUtilAxi.writeln(Double.toString(diff[i]));
      }
      fileWriterUtilAxi.close();
    }



    // From LSF calculate FWHM
    // First find transition points
    int FWHMStartIndex = -1, FWHMStopIndex = -1, FWHMPeakIndex = -1;

    for (int diffIndex = 1; diffIndex < diffLength; diffIndex++)
      if (diff[diffIndex] == 1.0)
      {
        FWHMPeakIndex = diffIndex;
        break;
      }
    if (FWHMPeakIndex == -1)
      // EdgeMTF can't find peak in LSF
      throw(new EdgeMTFBusinessException());

    // Search backwards from peak for rising 0.5
    for (int diffIndex = FWHMPeakIndex; diffIndex > 0; diffIndex--)
      if (diff[diffIndex-1] < 0.5 && diff[diffIndex] >= 0.5)
      {
        FWHMStartIndex = diffIndex;
        break;
      }
    if (FWHMStartIndex == -1)
      // EdgeMTF can't find rising half max in LSF.
      throw (new EdgeMTFBusinessException());

    // Search forwards from peak for falling 0.5
    for (int diffIndex = FWHMPeakIndex+1; diffIndex < diffLength; diffIndex++)
      if (diff[diffIndex-1] > 0.5 && diff[diffIndex] <= 0.5)
      {
        FWHMStopIndex = diffIndex;
        break;
      }
    if (FWHMStopIndex == -1)
      // EdgeMTF can't find falling half max in LSF.
      throw(new EdgeMTFBusinessException());

    // Next interpolate with line equation (y=mx+b) to achieve more precise x values
    // at y = 0.5 (i.e. sub-pixel precision).
    // m = (y2 - y1) / ((x2 - x1) / oversamplingFactor)
    double m = (diff[FWHMStartIndex] - diff[FWHMStartIndex-1]) / (1.0 / rOversamplingFactor) ;
    // b = y1 - m * x1
    double b = diff[FWHMStartIndex] - m * FWHMStartIndex * (1.0 / rOversamplingFactor);
    // x = (y - b) / m
    double xAt50Rising = (0.5 - b) / m;
    m = (diff[FWHMStopIndex] - diff[FWHMStopIndex-1]) / (1.0 / rOversamplingFactor);
    b = diff[FWHMStopIndex] - m * FWHMStopIndex  * (1.0 / rOversamplingFactor);
    double xAt50Falling = (0.5 - b) / m;
    // the width in x from rising y=0.5 to falling y=0.5 is FWHM
    double lineSpreadFWHM = xAt50Falling - xAt50Rising;

    if (_displayDebugText)
      System.out.println("50 Rising = " + xAt50Rising + " 50 Falling = " +
                         xAt50Falling + " " + lineSpreadFWHM);

    return lineSpreadFWHM;
  }

  /**
   * Determines edge angle using Hough transform.
   *
   * If the image contains multiple edges, the angle of the "best" (clearest, strongest)
   * edge is returned only.  So with multiple edges the risk is taken that a value
   * for a different edge than expected is returned.
   *
   * @param img Sobelized image containing edge to calculate angle of
   * @param startTheta angle in degrees to start searching for edges at.  The angle
   * is measured between the x-axis and the edge.
   * @param numberOfThetas how many angles to search for
   * @param stepBy how far apart in degrees each search angle is to be
   * @return angle in degrees that is perpendicular to the strongest edge in the image
   * within the given range of search angles.  (Perpendicular is the orientation of
   * the array used to collect projections along the edge in the Radon projection.)
   * @throws DatastoreException occurs when debug files cannot be created or written to
   *
   *
   * @author John Dutton
   */
  private double calculateEdgeAngle(Image img, double startTheta, int numberOfThetas,
                                       double stepBy, String debugOutputFileName)
      throws DatastoreException, EdgeMTFBusinessException
  {
    // Need these in Radians
    double startThetaInRadians = Math.toRadians(startTheta);
    double stepByInRadians = Math.toRadians(stepBy);

    // Calculate center of image to be treated as origin.  (See line equation below.)
    int centerX = img.getWidth() / 2;
    int centerY = img.getHeight() / 2;

    // Create vote table where the x-axis represents the set of possible edge angles
    // or theta values and the y-axis represents R values where R is the distance
    // from the origin to the edge along a line perpendicular to the edge.
    // Greatest possible length of R is the length of the image diagonal (which is
    // also the longest line that can possibly be drawn through the image).  Calculate
    // this length from the formula for the length of the hypoteneuse of a right triangle.
    // (Add fudge factor of 5 to account for integer division, roundup, etc.)
    int rSize = (int)(Math.hypot(img.getWidth(), img.getHeight()) + 5);
    int rCenter = rSize / 2;
    int[][] vote = new int[numberOfThetas][rSize];
    for (int[] row : vote)
      for (int voteBucket : row)
        voteBucket = 0;

    for (int y = 0; y < img.getHeight(); y++)
    {
      // Have y increase from lower left corner as depictions of Hough indicate and Matlab does.
      int adjustedY = img.getHeight() - 1 - y;

      for (int x = 0; x < img.getWidth(); x++)
      {
        float pixelValue = img.getPixelValue(x, y);
        if (pixelValue != 0)
        {
          // Pixel is on an edge so let it vote for all possible lines that might go through it.
          for (int aTheta = 0; aTheta < numberOfThetas; aTheta++)
          {
            int rIndex;
            double realThetaInRadians = startThetaInRadians + stepByInRadians * aTheta;
            // Following is formula for a straight line in polar coordinates.
            double r = (x - centerX) * Math.cos(realThetaInRadians) +
                       (adjustedY - centerY) * Math.sin(realThetaInRadians);

            int anR = (int)Math.round(r);
            if (anR < 0)
              rIndex = rCenter + Math.abs(anR);
            else
              rIndex = rCenter - anR;

            // Mathematically this should never happen, but catch it if it
            // does instead of getting an array out of bounds exception.
            if (rIndex >= rSize || rIndex < 0)
              throw(new EdgeMTFBusinessException());

            vote[aTheta][rIndex]++;
          }
        }
      }
    }

    if (_saveDebugFiles)
    {
      Image houghImg1 = EdgeMTF.convertHoughTableToImage(vote, numberOfThetas, rSize);
      XrayImageIoUtil.saveJpegImage(houghImg1.getBufferedImage(), _saveDebugFilesToDirName + File.separator +
                                    debugOutputFileName);
      houghImg1.decrementReferenceCount();
    }

    // Determine which line received the most votes, i.e. had the most points on it.
    int bestVote = 0;
    int bestY = -1;
    int bestX = -1;
    for (int y = 0; y < rSize; y++)
    {
      for (int x = 0; x < numberOfThetas; x++)
      {
        if (vote[x][y] > bestVote)
        {
          bestVote = vote[x][y];
          bestY = y;
          bestX = x;
        }
      }
    }

    // Calculate the line's angle based on the search space parameters.
    double returnBestX = startTheta + bestX * stepBy;

    // When Hough Transform detects edges that are near 0 or 180 degrees, the results can be
    // slightly less than 0 or slightly greater than 180.  Although such values for angles
    // are valid conceptually, the EdgeMTF code is designed to work with following range
    // only: 0 <= theta < 180.  So, to keep from hitting any limitations in the code, bring
    // the result into the designed and tested for range.
    if (returnBestX < 0)
      returnBestX += 180;
    if (returnBestX >= 180)
      returnBestX -= 180;


    if (_displayDebugText)
      System.out.println("bestVote = " + bestVote + " bestY = " + bestY + " bestX = " +
                         bestX + " returnBestX = " + returnBestX);

    return returnBestX;
  }


  /**
   * For debugging, prints a table of pixel values to standard out.
   *
   * @author John Dutton
   */
  private void printImage(Image img)
  {
    for (int y = 0; y < img.getHeight(); y++)
     {
       System.out.println();
       System.out.print("y = " + y + " ");

       for (int x = 0; x < img.getWidth(); x++)
       {
         float pixelValue = img.getPixelValue(x, y);
         System.out.print(pixelValue + " ");
       }
     }
  }


  /**
   * For debugging purposes, used to convert Hough transform to an image
   * that can later be saved and displayed as a jpeg or png image.
   *
   * @author John Dutton
   */
  private static Image convertHoughTableToImage(int[][] intArray, int width, int height)
  {
    Image returnImage = new Image(width, height);

    for (int y = 0; y < height; y++)
    {
      for (int x = 0; x < width; x++)
      {
        float pixelValue = intArray[x][y];
        // Add intensity to improve visibility.
        pixelValue = (float) 90 + pixelValue;
        // Not sure how > 255 gray level looks when displayed, so avoid all together.
        if (pixelValue > 255)
          pixelValue = 255;

        returnImage.setPixelValue(x, y, pixelValue);
      }
    }

    return returnImage;
  }

  /**
   * Helper routine so that black & white images for regression tests
   * will work.
   *
   * @author John Dutton
   */
  private boolean isBlackAndWhiteImage(Image img)
  {
    for (int y = 0; y < img.getHeight(); y++)
      for (int x = 0; x < img.getWidth(); x++)
      {
        float pixelValue = img.getPixelValue(x, y);
        // anything other than 0 and 255 means a gray-level image
        if (1 <= pixelValue && pixelValue <= 254)
          return false;
      }
    return true;
  }

  /**
   * Helper routine that if needed reverses array values to achieve an overall
   * ascending order (but not strictly ascending -- the values aren't sorted).
   *
   * @author John Dutton
   */
  private double[] convertArrayToRisingValuesIfNeeded(double [] array)
  {
    int halfWayIndex = array.length / 2;
    double firstHalfMean = meanOfArrayValues(0, halfWayIndex, array);
    double secondHalfMean = meanOfArrayValues(halfWayIndex, array.length, array);

    // Subtract first half from second half, if negative then have falling edge and need
    // to mirror the array.
    if (secondHalfMean - firstHalfMean < 0)
    {
      int length = array.length;
      double [] newArray = new double[length];
      for (int i = 0; i < length; i++)
        newArray[length - 1 - i] = array[i];
      return newArray;
    }
    return array;
  }

  /**
   * Calculates mean of a specified segment of values in an array.
   *
   * @author John Dutton
   */
  private double meanOfArrayValues(int lowerBound, int upperBound, double ...array)
  {
    Assert.expect(array != null);
    Assert.expect(array.length > 0);
    Assert.expect(lowerBound >= 0);
    Assert.expect(upperBound <= array.length);

    double sum = 0.f;
    for (int i = lowerBound; i < upperBound; ++i)
      sum += array[i];

    return (double)(sum / (upperBound - lowerBound));
  }


  /**
   * Threshold an image into black and white by using the halfway point of gray
   * levels as the threshold point.
   *
   * @param img Image to threshold that will be modified in place
   * @author John Dutton
   */
  private void thresholdImage(Image img)
      throws DatastoreException, EdgeMTFBusinessException
  {

    // Use histogram to determine threshold level.
    int [] hist = Threshold.histogram(img, RegionOfInterest.createRegionFromImage(img), 256);
    int darkestDarkValue = -1;
    int brightestBrightValue = -1;


    for (int i = 0; i < 256; i++)
      if (hist[i] != 0)
      {
        darkestDarkValue = i;
        break;
      }

    for (int i = 255; i >= 0; i--)
      if (hist[i] != 0)
      {
        brightestBrightValue = i;
        break;
      }

    if (darkestDarkValue == -1)
      // EdgeMTF can't find darkest dark value.
      throw(new EdgeMTFBusinessException());
    if (brightestBrightValue == -1)
      // EdgeMTF can't find brightest bright value.
      throw(new EdgeMTFBusinessException());

    // Expect the histogram to have 2 groupings or humps of values, one for darks and one
    // for light.  The formula below expects the trough between the humps to be halfway
    // between all gray values.
    float thresholdValue = darkestDarkValue + ((brightestBrightValue - darkestDarkValue) / 2);

    // Threshold the image.
    float thresholdLessThan = thresholdValue;
    float valueLessThan = 0.0f;
    float thresholdGreaterThan = thresholdValue;
    float valueGreaterThan = 255.0f;
    Threshold.threshold(img, thresholdLessThan, valueLessThan, thresholdGreaterThan, valueGreaterThan);
  }

  /**
   * Applies a 3x3 median filter kernel to image.  The algorithm does not attempt to
   * filter along the outside border, instead those pixels are dropped from the returned image.
   * @todo FUTURE jpd There's an ipp method to do this called Filter.median.  Consider switching to it for
   * better performance.  Note, there was discussion to have the cameras apply a filter.  Not sure if that
   * ever happened.
   * @return a filtered image whose width and height are 2 less than the original image
   * @author John Dutton
   */
  private Image medianFilterImage(Image image)
  {
    Image returnImage = new Image(image.getWidth(), image.getHeight());

    for (int y = 1; y < image.getHeight() - 1; y++)
    {
      for (int x = 1; x < image.getWidth() - 1; x++)
      {
        ArrayList<Float> array = new ArrayList<Float>();
        array.add(image.getPixelValue(x - 1, y - 1));
        array.add(image.getPixelValue(x, y - 1));
        array.add(image.getPixelValue(x + 1, y - 1));
        array.add(image.getPixelValue(x - 1, y));
        array.add(image.getPixelValue(x, y));
        array.add(image.getPixelValue(x + 1, y));
        array.add(image.getPixelValue(x - 1, y + 1));
        array.add(image.getPixelValue(x, y + 1));
        array.add(image.getPixelValue(x + 1, y + 1));

        // get median value
        Collections.sort(array);
        float medianValue = array.get(4);

        // set value
        returnImage.setPixelValue(x, y, medianValue);
      }
    }

    Image croppedReturnImage =  Image.createCopy(returnImage,
                            RegionOfInterest.createRegionFromRegionBorder(1, 1, returnImage.getWidth() - 2,
                                                                          returnImage.getHeight() - 2, 0,
                                                                          RegionShapeEnum.RECTANGULAR));
    returnImage.decrementReferenceCount();
    return croppedReturnImage;
  }


}
