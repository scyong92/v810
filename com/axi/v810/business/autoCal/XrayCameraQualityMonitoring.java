package com.axi.v810.business.autoCal;

import com.axi.v810.images.Image5DX;
import com.jrefinery.chart.*;
import com.jrefinery.data.*;
import java.io.*;
import java.util.*;
import net.spc.chart.*;
import net.spc.common.*;
import net.spc.math.Statistics;


/**
 * Display X-Ray Camera Quality Monitoring SPC Chart in Service GUI Tab
 * XCR1471 X-Ray Camera Quality Monitoring by Anthony on July 2012
 * @author Anthony Fong
*/
public class XrayCameraQualityMonitoring
{
    XrayCameraQualityData _cameraQualityData;
    public String _finalImageFilename="";


    public String getImageFilename()
    {
        return _finalImageFilename;
    }
    /**
     * Default constructor.
     */
    public XrayCameraQualityMonitoring()
    {    
    }
  
    public boolean generateXBarChart(String colSelection, boolean triggerAlarm, boolean showCP)
    {
        try
        {
            _cameraQualityData = XrayCameraQualityData.getInstance();
            _cameraQualityData.doSPC(colSelection,showCP);
            double Y_aixs_datas_dummy[] = (double[]) _cameraQualityData.getDrawDetails().get("Y_aixs_datas");

            StringBuffer requiredResolution = new StringBuffer("1");

            java.text.NumberFormat nf = java.text.NumberFormat.getInstance();
            // set max fraction supported = 4;

            nf.setMaximumFractionDigits(4);
            double yMax = Double.parseDouble(nf.format(Statistics.max(Y_aixs_datas_dummy)));

            int digitsAfterDot = new Double(yMax).toString().substring(new Double(yMax).toString().indexOf('.')+1 , new Double(yMax).toString().length()).length();

            for(int i=1; i<=digitsAfterDot;i++) {
                    requiredResolution.append("0");
            }

            int resolution = Integer.parseInt(requiredResolution.toString());
            double Y_aixs_datas[] = new double[Y_aixs_datas_dummy.length];

            for(int i=0;i<Y_aixs_datas.length;i++) {
                    Y_aixs_datas[i] = Y_aixs_datas_dummy[i] * resolution;
            }

            // create a dataset...
            double[][] data = { Y_aixs_datas };
            DefaultCategoryDataset dataset = new DefaultCategoryDataset(data);
            Hashtable cameraQualityDataDetails = _cameraQualityData.getDetails();
            // set the series names...
            String[] seriesNames = { (String)cameraQualityDataDetails.get("desc") };
            String  xAxisName = (String)cameraQualityDataDetails.get("xAxisName");
            String  yAxisName = _cameraQualityData.getMonitoringVariableName() ;//(String)details.get("yAxisName");
            dataset.setSeriesNames(seriesNames);

            System.out.println("data Length === "+Y_aixs_datas.length);

            // set the category names...
            Vector vtrCategory = (Vector)cameraQualityDataDetails.get("x_lables");
            dataset.setCategories(VectorUtil.sArray((Vector)cameraQualityDataDetails.get("x_lables")));

            double min = Statistics.min(Y_aixs_datas);
            double max = Statistics.max(Y_aixs_datas);

            double ucl = ((Double)((Hashtable)_cameraQualityData.getDrawDetails().get("controlLimitObjects")).get("UCL")).doubleValue() * resolution ;
            double lcl = ((Double)((Hashtable)_cameraQualityData.getDrawDetails().get("controlLimitObjects")).get("LCL")).doubleValue() * resolution ;
            double processCapability = ((Double)_cameraQualityData.getDrawDetails().get("processCapability")).doubleValue();

            if(triggerAlarm == true)
            {
                if(Y_aixs_datas.length >= _cameraQualityData.getDataSize())
                {
                    if(processCapability >1.0)
                    {
                        triggerAlarm = false;
                    }
                }
                else
                {
                    triggerAlarm = false;
                }
            }
            String pc = "Process Capability = "+nf.format(processCapability);
            //String pc = "Deviation % = 100*(6*stdDev)/(0.1*mean) = "+nf.format(processCapability)+"%";
            System.out.println("yAxisName="+yAxisName);
            String comments[] = {pc};
            String title ="X-Bar Chart ( " + yAxisName + " )";
            // create the chart...
            SPCChart chart = ChartUtil.createLineChart(title,  // chart title
            xAxisName, //"Category",           // domain axis label
            yAxisName, //"Value",              // range axis label
            dataset,              // data
            true, comments,        // include legend
            resolution			// Data resolution
            );

            //chart.getXYPlot().addHorizontalLine(new Double(10));
            // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...

            // set the background color for the chart...
            chart.setBackgroundPaint(java.awt.Color.lightGray);

            // get a reference to the plot for further customisation...
            //CategoryPlot plot = chart.getCategoryPlot();
            SPCPlot plot = (SPCPlot)chart.getCategoryPlot();

            // label data points with values...
            plot.setLabelsVisible(true);

            // set the color for each series...
            plot.setSeriesPaint(new java.awt.Paint[] { java.awt.Color.blue });

            // set the stroke for each series...
            java.awt.Stroke[] seriesStrokeArray = { new java.awt.BasicStroke((float)1.0) };
            plot.setSeriesStroke(seriesStrokeArray);

            java.awt.Stroke s = new java.awt.BasicStroke((float)1.0);

            SPCVerticalNumberAxis rangeAxis = (SPCVerticalNumberAxis)plot.getRangeAxis();
            rangeAxis.setAutoRangeIncludesZero(false);
            rangeAxis.setCrosshairVisible(true);
            rangeAxis.setCrosshairPaint( java.awt.Color.red );
            rangeAxis.setCrosshairStroke(s);

            rangeAxis.setMinimumAxisValue(lcl < min ? lcl-10 : min-10);
            rangeAxis.setMaximumAxisValue(ucl > max ? ucl+10 : max+10);

            rangeAxis.setCrosshairObject((Hashtable)_cameraQualityData.getDrawDetails().get("controlLimitObjects"));
            rangeAxis.setStandardTickUnits(TickUnits.createIntegerTickUnits());

            HorizontalCategoryAxis domainAxis = (HorizontalCategoryAxis)plot.getDomainAxis();
            domainAxis.setVerticalCategoryLabels(true);
            System.out.println("saving chart..");
            ChartRenderingInfo cinfo = new ChartRenderingInfo();

            _finalImageFilename =_cameraQualityData.getQualityDataFolder()+"\\X_Bar.png";
            ChartUtilities.saveChartAsPNG(new File(_finalImageFilename), chart, 
                    _cameraQualityData.getChartWidth(),
                    _cameraQualityData.getChartHeight(), cinfo);

            if(triggerAlarm == true)
            {
                java.awt.Image image = Image5DX.getImage(Image5DX.FRAME_X5000);
                javax.swing.JFrame  frame = new javax.swing.JFrame();
                javax.swing.JLabel  label = new javax.swing.JLabel(new javax.swing.ImageIcon(javax.imageio.ImageIO.read(new File(_finalImageFilename))));
                frame.add(new javax.swing.JScrollPane(label));
                frame.setTitle("V810 - Camera Quality Monitoring " + yAxisName + " Fail");
                frame.setSize(_cameraQualityData.getChartWidth()+40, _cameraQualityData.getChartHeight()+60);
                frame.setIconImage(image);
                frame.setVisible(true);
            }
        } 
        catch(Exception e)
        {
            System.out.println("Exception occured.."+ e);
        }
        return triggerAlarm;
    }
  
	

}
