package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Task to Confirm that communications on the X6000 system are functioning as
 * expected. Ping the IRP's and make sure they respond
 *
 * @author Reid Hayhow
 */
public class ConfirmCommunicationWithRMS extends ConfirmCommunicationTask
{
  private static String _NAME =
      StringLocalizer.keyToString("CD_CONFIRM_RMS_COMMUNICATIONS_KEY");

  private static final int _FREQUENCY = 1;
  private static final long _TIMEOUTINSECONDS = 2000;

  private static ConfirmCommunicationWithRMS _instance = null;
  private static final int _RMS_NUMBERING_OFFSET = 1;

  // Member variables for reporting progress
  private ProgressObservable _progressObservable = ProgressObservable.getInstance();
  private ProgressReporterEnum _progressReportEnum = ProgressReporterEnum.CONFIRM_RMS_COMMUNICATION;

  /**
   * Constructor, creates a logger for this confirmation
   *
   * @author Reid Hayhow
   */
  private ConfirmCommunicationWithRMS()
  {
    super(_NAME, _FREQUENCY, _TIMEOUTINSECONDS, ProgressReporterEnum.CONFIRM_RMS_COMMUNICATION);

    //Set enum used to save timestamp for this task
    setTimestampEnum(HardwareCalibEnum.CONFIRM_COMMUNICATION_WITH_IRPS_LAST_TIME_RUN);
  }

  /**
   * Instance method for this task
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCommunicationWithRMS getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCommunicationWithRMS();
    }
    return _instance;
  }

  /**
   * This task communicates with the Remote Motion Server and switch to make sure they
   * are on the network and are responsive.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    //Purge the error buffer before starting
    _errorBuffer = new StringBuilder();

    // Report that this task is starting
    _progressObservable.reportProgress(_progressReportEnum, 0);

    List<String> listOfRMSIpAddresses = new ArrayList<String>();

    listOfRMSIpAddresses.add(_config.getStringValue(HardwareConfigEnum.REMOTE_MOTION_CONTROLLER_IP_ADDRESS));

    //Ping the RMS, pass in the string indicating the type of device
    //and pass in the offset for RMS numbering, in this case one because
    //RMS's are numbered 1
    pingAddressesAndLogResults(listOfRMSIpAddresses,
                               StringLocalizer.keyToString("ACD_ETHERNET_CARD_KEY") + " 1, " +
                               StringLocalizer.keyToString("ACD_REMOTE_MOTION_SERVER_KEY"),
                               _RMS_NUMBERING_OFFSET);

    //Clear the list of devices before pinging the next set of IRP's so we don't
    //get duplicate data
    listOfRMSIpAddresses.clear();

    logResults();

    // Report that we are done with this task
    _progressObservable.reportProgress(_progressReportEnum, 100);
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
