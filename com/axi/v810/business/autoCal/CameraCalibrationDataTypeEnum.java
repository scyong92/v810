package com.axi.v810.business.autoCal;

import com.axi.util.Enum;

/**
 * Enum class to simplify evaluating Camera Calibration values
 *
 * @author Reid Hayhow
 * @version 1.0
 */
public class CameraCalibrationDataTypeEnum extends Enum
{

  private static int index = 0;
  static final CameraCalibrationDataTypeEnum SEGMENT_GAIN_FORWARD_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum SEGMENT_OFFSET_FORWARD_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_GAIN_FORWARD_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_OFFSET_FORWARD_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum SEGMENT_GAIN_FORWARD_MIN = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum SEGMENT_OFFSET_FORWARD_MIN = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_GAIN_FORWARD_MIN = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_OFFSET_FORWARD_MIN = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum SEGMENT_GAIN_REVERSE_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum SEGMENT_OFFSET_REVERSE_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_GAIN_REVERSE_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_OFFSET_REVERSE_MAX = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum SEGMENT_GAIN_REVERSE_MIN = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum SEGMENT_OFFSET_REVERSE_MIN = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_GAIN_REVERSE_MIN = new CameraCalibrationDataTypeEnum(index++);
  static final CameraCalibrationDataTypeEnum PIXEL_OFFSET_REVERSE_MIN = new CameraCalibrationDataTypeEnum(index++);

  /**
   * CameraCalibrationDataTypeEnum
   *
   * @author Reid Hayhow
   */
  protected CameraCalibrationDataTypeEnum(int id)
  {
    super(id);
  }
}
