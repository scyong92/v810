package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

public class Test_ConfirmPanelHandlingTask extends UnitTest
{
  /**
   * Testing for Panel Handling Confirmation. Try to run without a panel, should
   * fail. Then load a panel and run, should pass. ALWAYS unload the panel after
   * testing.
   *
   * @author Reid Hayhow
   */
  public void test(BufferedReader bufferedReader, PrintWriter printWriter)
  {
    ConfirmPanelHandlingTask confirmPanelHandling = ConfirmPanelHandlingTask.getInstance();
    PanelHandler panelHandler = PanelHandler.getInstance();

    //Save off the hardware.calib because running tasks will change it by adding timestamps
    TestUtils utils = new TestUtils();
    utils.backupHWCalib(getTestSourceDir());
    // CR31949-Board falling inside system during startup by Anthony Fong on 18 Sept 2008
    boolean result = false;
    //Clean up if there is a panel in the system
    try
    {
      if (panelHandler.isPanelLoaded())
      {
        // Initialize motion/panel handling so unloadPanel will work.
        try
        {
          XrayTester.getInstance().startupMotionRelatedHardware();
        }
        catch (XrayTesterException ex4)
        {
          // Ignore.  If Panel Handler couldn't recapture the panel, which it can't in hwsim
          // mode, it throws an exception.
        }

        // Now unload the panel, but since it is possible that startupMotionRelatedHardware
        // removed the panel, check condition again before unloading.
        if (panelHandler.isPanelLoaded())
          try
          {
            panelHandler.unloadPanel();
          }
          catch (XrayTesterException ex5)
          {
            ex5.printStackTrace(printWriter);
          }
      }


      //Run before loading a panel, should fail (but not throw exception)
      result = confirmPanelHandling.executeUnconditionally();
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace(printWriter);
      Assert.expect(false);
    }

    //The task should fail because there is no board in the system
    Expect.expect(result == false);

    //Once we load a panel into the system, all the testing needs to be in a
    //try block so that we can be sure to unload the panel if anything goes
    //wrong during the test
    try
    {
      //Start-up the system and spoof a panel in the system
      try
      {
        XrayTester.getInstance().startup();
        panelHandler.loadPanel("foo",
                               panelHandler.getMinimumPanelWidthInNanoMeters(),
                               panelHandler.getMinimumPanelLengthInNanoMeters(), 
                               false, 0);
      }
      catch (XrayTesterException ex1)
      {
        ex1.printStackTrace(printWriter);
        Assert.expect(false);
      }

      //Now run the test, it should pass
      try
      {
        result = confirmPanelHandling.executeUnconditionally();
      }
      catch (XrayTesterException ex2)
      {
        ex2.printStackTrace(printWriter);
        Assert.expect(false);
      }

      //Task should pass this time
      Expect.expect(result == true);
    }
    //Finally, unload the panel
    //This must be done in order to clean up the system for future regression tests
    finally
    {
      try
      {
        panelHandler.unloadPanel();
      }
      catch (XrayTesterException ex3)
      {
        ex3.printStackTrace(printWriter);
        Assert.expect(false);
      }

      //restore the hardware.calib that doesn't have timestampe
      utils.restoreHWCalib(getTestSourceDir());
    }

  }

  /**
   *
   * @author Reid Hayhow
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ConfirmPanelHandlingTask());
  }
}
