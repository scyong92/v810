package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;


/**
 *
 * The class to hold two maps and a list of automated hardware tasks. This class
 * is used, at start-up, to initialize the HardwareTaskEngine with a list of
 * tasks it needs to run.
 *
 * @author Reid Hayhow
 */
public class XrayHardwareAutomatedTaskList
{
  private static XrayHardwareAutomatedTaskList _instance;
  private static List<TimeDrivenHardwareTask> _timeDrivenTaskList = new LinkedList<TimeDrivenHardwareTask>();
  private static Map<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>>> _eventDrivenTaskMapStartTriggered = new HashMap<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>>>();
  private static Map<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>>> _eventDrivenTaskMapEndTriggered = new HashMap<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>>>();
  private static Map<HardwareExceptionEnum, EventDrivenHardwareTask> _exceptionDrivenTaskMap = new HashMap<HardwareExceptionEnum, EventDrivenHardwareTask>();
  private static Config _config = Config.getInstance();

  //statics to clarify boolean parameter
  private static boolean RUN_ON_START_OF_EVENT = true;
  private static boolean RUN_ON_END_OF_EVENT = true;
  private static EventTriggerStateEnum _triggerState;


  /**
   * XrayHardwareAutomatedTaskList constructor. Call functions here to have
   * tasks added to a specific list.
   *
   * @author Reid Hayhow
   */
  XrayHardwareAutomatedTaskList()
  {
    EventDrivenHardwareTask allLightImageConfirm = null;
    EventDrivenHardwareTask allHighMagLightImageConfirm = null;

    allLightImageConfirm = ConfirmCameraLightImageWithNoPanel.getInstance();
    
    Assert.expect(allLightImageConfirm != null);

    // Handle either the LegacyXRaySource or the HTubeXRaySource
    EventDrivenHardwareTask xraySourceConfirm = null;

    XraySourceTypeEnum sourceTypeEnum = AbstractXraySource.getSourceTypeEnum();
    if (sourceTypeEnum.equals(XraySourceTypeEnum.LEGACY))
    {
      xraySourceConfirm = ConfirmLegacyXRaySource.getInstance();
    }
    else if (sourceTypeEnum.equals(XraySourceTypeEnum.HTUBE))
    {
      xraySourceConfirm = ConfirmHTubeXRaySource.getInstance();
    }
    else
    {
      Assert.expect(false, "unknown xraySourceType from config file, HardwareConfigEnum bug?");
      xraySourceConfirm = null;
    }

    boolean hasVariableMagnification = false;
    try
    {
      if(LicenseManager.isVariableMagnificationEnabled())
      {
        hasVariableMagnification = true;
      }
    }
    catch(BusinessException be)
    {
      // do nothing
    }
    
    if(hasVariableMagnification)
    {
                 
       allHighMagLightImageConfirm = ConfirmCameraHighMagLightImageWithNoPanel.getInstance();
       Assert.expect(allHighMagLightImageConfirm != null);
        
      addMajorMinorEventTrigger(XrayTesterEventEnum.STARTUP,
                                XrayTesterEventEnum.STARTUP,
                                _triggerState.TRIGGER_ON_END,
                                //Now add the specific hardware task events to trigger
                                //When this event tuple is seen
                                ConfirmAllCommunication.getInstance(),
                                xraySourceConfirm,
                                ConfirmAreaModeImages.getInstance(),
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance(),
                                ConfirmCameraDarkImageTask.getInstance(),
                                allLightImageConfirm,
                                CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHysteresisEventDrivenHardwareTask.getInstance(),
                                CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalMagnification.getInstance(),
                                ConfirmLowMagSystemMTF.getInstance(),
                                ConfirmMagnificationCoupon.getInstance(),
                                CalHighMagGrayscale.getInstance(),
                                ConfirmCameraHighMagCalibration.getInstance(),
                                ConfirmCameraHighMagDarkImageTask.getInstance(),
                                allHighMagLightImageConfirm,
                                CalHighMagXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHighMagHysteresisEventDrivenHardwareTask.getInstance(),
                                CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalHighMagnification.getInstance(),
                                ConfirmHighMagSystemMTF.getInstance(),
                                ConfirmMagnificationCouponForHighMag.getInstance()
                                );

      addMinorEventTrigger(XrayTesterEventEnum.STARTUP,
                                _triggerState.TRIGGER_ON_END,
                                //Now add the specific hardware task events to trigger
                                //When this event tuple is seen
                                ConfirmAllCommunication.getInstance(),
                                xraySourceConfirm,
                                ConfirmAreaModeImages.getInstance(),
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance(),
                                ConfirmCameraDarkImageTask.getInstance(),
                                allLightImageConfirm,
                                CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHysteresisEventDrivenHardwareTask.getInstance(),
                                CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalMagnification.getInstance(),
                                ConfirmLowMagSystemMTF.getInstance(),
                                ConfirmMagnificationCoupon.getInstance(),
                                CalHighMagGrayscale.getInstance(),
                                ConfirmCameraHighMagCalibration.getInstance(),
                                ConfirmCameraHighMagDarkImageTask.getInstance(),
                                allHighMagLightImageConfirm,
                                CalHighMagXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHighMagHysteresisEventDrivenHardwareTask.getInstance(),
                                CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalHighMagnification.getInstance(),
                                ConfirmHighMagSystemMTF.getInstance(),
                                ConfirmMagnificationCouponForHighMag.getInstance()
                                );

      addMajorMinorEventTrigger(PanelHandlerEventEnum.LOAD_PANEL,
                                PanelHandlerEventEnum.LOAD_PANEL,
                                _triggerState.TRIGGER_ON_START,
                                ConfirmAreaModeImages.getInstance(),
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance(),
                                ConfirmCameraDarkImageTask.getInstance(),
                                allLightImageConfirm,
                                CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHysteresisEventDrivenHardwareTask.getInstance(),
                                CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalMagnification.getInstance(),
                                ConfirmLowMagSystemMTF.getInstance(),
                                ConfirmMagnificationCoupon.getInstance(),
                                CalHighMagGrayscale.getInstance(),
                                ConfirmCameraHighMagCalibration.getInstance(),
                                ConfirmCameraHighMagDarkImageTask.getInstance(),
                                allHighMagLightImageConfirm,
                                CalHighMagXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHighMagHysteresisEventDrivenHardwareTask.getInstance(),
                                CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalHighMagnification.getInstance(),
                                ConfirmHighMagSystemMTF.getInstance(),
                                ConfirmMagnificationCouponForHighMag.getInstance());

      addMinorEventTrigger(PanelHandlerEventEnum.LOAD_PANEL,
                           _triggerState.TRIGGER_ON_START,
                           ConfirmAreaModeImages.getInstance(),
                           CalGrayscale.getInstance(),
                           ConfirmCameraCalibration.getInstance(),
                           ConfirmCameraDarkImageTask.getInstance(),
                           allLightImageConfirm,
                           CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                           CalHysteresisEventDrivenHardwareTask.getInstance(),
                           CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                           CalMagnification.getInstance(),
                           ConfirmLowMagSystemMTF.getInstance(),
                           ConfirmMagnificationCoupon.getInstance(),
                           CalHighMagGrayscale.getInstance(),
                           ConfirmCameraHighMagCalibration.getInstance(),
                           ConfirmCameraHighMagDarkImageTask.getInstance(),
                           allHighMagLightImageConfirm,
                           CalHighMagXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                           CalHighMagHysteresisEventDrivenHardwareTask.getInstance(),
                           CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance(),
                           CalHighMagnification.getInstance(),
                           ConfirmHighMagSystemMTF.getInstance(),
                           ConfirmMagnificationCouponForHighMag.getInstance());
    }
    else
    {
      addMajorMinorEventTrigger(XrayTesterEventEnum.STARTUP,
                              XrayTesterEventEnum.STARTUP,
                              _triggerState.TRIGGER_ON_END,
                              //Now add the specific hardware task events to trigger
                              //When this event tuple is seen
                              ConfirmAllCommunication.getInstance(),
                              xraySourceConfirm,
                              ConfirmAreaModeImages.getInstance(),
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance(),
                              ConfirmCameraDarkImageTask.getInstance(),
                              allLightImageConfirm,
                              CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                              CalHysteresisEventDrivenHardwareTask.getInstance(),
                              CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                              CalMagnification.getInstance(),
                              ConfirmLowMagSystemMTF.getInstance(),
                              ConfirmMagnificationCoupon.getInstance()
                              );

      addMinorEventTrigger(XrayTesterEventEnum.STARTUP,
                                _triggerState.TRIGGER_ON_END,
                                //Now add the specific hardware task events to trigger
                                //When this event tuple is seen
                                ConfirmAllCommunication.getInstance(),
                                xraySourceConfirm,
                                ConfirmAreaModeImages.getInstance(),
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance(),
                                ConfirmCameraDarkImageTask.getInstance(),
                                allLightImageConfirm,
                                CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHysteresisEventDrivenHardwareTask.getInstance(),
                                CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalMagnification.getInstance(),
                                ConfirmLowMagSystemMTF.getInstance(),
                                ConfirmMagnificationCoupon.getInstance()
                                );

      addMajorMinorEventTrigger(PanelHandlerEventEnum.LOAD_PANEL,
                                PanelHandlerEventEnum.LOAD_PANEL,
                                _triggerState.TRIGGER_ON_START,
                                ConfirmAreaModeImages.getInstance(),
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance(),
                                ConfirmCameraDarkImageTask.getInstance(),
                                allLightImageConfirm,
                                CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                CalHysteresisEventDrivenHardwareTask.getInstance(),
                                CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                CalMagnification.getInstance(),
                                ConfirmLowMagSystemMTF.getInstance(),
                                ConfirmMagnificationCoupon.getInstance());

      addMinorEventTrigger(PanelHandlerEventEnum.LOAD_PANEL,
                           _triggerState.TRIGGER_ON_START,
                           ConfirmAreaModeImages.getInstance(),
                           CalGrayscale.getInstance(),
                           ConfirmCameraCalibration.getInstance(),
                           ConfirmCameraDarkImageTask.getInstance(),
                           allLightImageConfirm,
                           CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                           CalHysteresisEventDrivenHardwareTask.getInstance(),
                           CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                           CalMagnification.getInstance(),
                           ConfirmLowMagSystemMTF.getInstance(),
                           ConfirmMagnificationCoupon.getInstance());
    }

    addMajorMinorEventTrigger(XraySourceEventEnum.OFF,
                              XraySourceEventEnum.OFF,
                              _triggerState.TRIGGER_ON_END,
                              xraySourceConfirm);

    addMinorEventTrigger(XraySourceEventEnum.OFF,
                         _triggerState.TRIGGER_ON_END,
                         xraySourceConfirm);

    addMajorMinorEventTrigger(XraySourceEventEnum.ON,
                              XraySourceEventEnum.ON,
                              _triggerState.TRIGGER_ON_END,
                              xraySourceConfirm);

    addMinorEventTrigger(XraySourceEventEnum.ON,
                         _triggerState.TRIGGER_ON_END,
                         xraySourceConfirm);

    if (_config.getBooleanValue(SoftwareConfigEnum.RUN_ADJUSTMENTS_AND_CONFIRM_WITH_PANEL_IN_SYSTEM))
    {
      // NOTE do not do Magnification Cal on any triggers where the IRP's have
      // been programmed because they don't handle two programs well
      addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT,
                                ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());

      addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());

      addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES,
                                ImageAcquisitionEventEnum.OPTICAL_IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());

      if(hasVariableMagnification)
      {
        addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT,
                                  TestExecutionEventEnum.INSPECT,
                                  _triggerState.TRIGGER_ON_END,
                                  ConfirmAreaModeImages.getInstance(),
                                  CalGrayscale.getInstance(),
                                  ConfirmCameraCalibration.getInstance(),
                                  ConfirmCameraDarkImageTask.getInstance(),
                                  CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                  CalHysteresisEventDrivenHardwareTask.getInstance(),
                                  CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                  CalMagnification.getInstance(),
                                  ConfirmLowMagSystemMTF.getInstance(),
                                  ConfirmMagnificationCoupon.getInstance(),
                                  CalHighMagXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                  CalHighMagHysteresisEventDrivenHardwareTask.getInstance(),
                                  CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance(),
                                  CalHighMagnification.getInstance(),
                                  ConfirmHighMagSystemMTF.getInstance(),
                                  ConfirmMagnificationCouponForHighMag.getInstance());

        addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                  TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                  _triggerState.TRIGGER_ON_END,
                                  ConfirmAreaModeImages.getInstance(),
                                  CalGrayscale.getInstance(),
                                  ConfirmCameraCalibration.getInstance(),
                                  ConfirmCameraDarkImageTask.getInstance(),
                                  CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                  CalHysteresisEventDrivenHardwareTask.getInstance(),
                                  CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                  CalMagnification.getInstance(),
                                  ConfirmLowMagSystemMTF.getInstance(),
                                  ConfirmMagnificationCoupon.getInstance(),
                                  CalHighMagXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                  CalHighMagHysteresisEventDrivenHardwareTask.getInstance(),
                                  CalHighMagSystemFiducialEventDrivenHardwareTask.getInstance(),
                                  CalHighMagnification.getInstance(),
                                  ConfirmHighMagSystemMTF.getInstance(),
                                  ConfirmMagnificationCouponForHighMag.getInstance());
      }
      else
      {
        addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT,
                                  TestExecutionEventEnum.INSPECT,
                                  _triggerState.TRIGGER_ON_END,
                                  ConfirmAreaModeImages.getInstance(),
                                  CalGrayscale.getInstance(),
                                  ConfirmCameraCalibration.getInstance(),
                                  ConfirmCameraDarkImageTask.getInstance(),
                                  CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                  CalHysteresisEventDrivenHardwareTask.getInstance(),
                                  CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                  CalMagnification.getInstance(),
                                  ConfirmLowMagSystemMTF.getInstance(),
                                  ConfirmMagnificationCoupon.getInstance());

        addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                  TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                  _triggerState.TRIGGER_ON_END,
                                  ConfirmAreaModeImages.getInstance(),
                                  CalGrayscale.getInstance(),
                                  ConfirmCameraCalibration.getInstance(),
                                  ConfirmCameraDarkImageTask.getInstance(),
                                  CalXraySpotCoordinateEventDrivenHardwareTask.getInstance(),
                                  CalHysteresisEventDrivenHardwareTask.getInstance(),
                                  CalSystemFiducialEventDrivenHardwareTask.getInstance(),
                                  CalMagnification.getInstance(),
                                  ConfirmLowMagSystemMTF.getInstance(),
                                  ConfirmMagnificationCoupon.getInstance());
      }

      addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES,
                                ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());

    }
    else
    {
      addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT,
                                ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());
     
      addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT,
                                TestExecutionEventEnum.INSPECT,
                                _triggerState.TRIGGER_ON_END,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance(),
                                ConfirmCameraDarkImageTask.getInstance());

      addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());
      
      addMajorMinorEventTrigger(TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                TestExecutionEventEnum.INSPECT_FOR_PRODUCTION,
                                _triggerState.TRIGGER_ON_END,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance(),
                                ConfirmCameraDarkImageTask.getInstance());
      
      addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES,
                                ImageAcquisitionEventEnum.OPTICAL_IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());

      addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES,
                                ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                                _triggerState.TRIGGER_ON_START,
                                CalGrayscale.getInstance(),
                                ConfirmCameraCalibration.getInstance());
      
    }

    // Guarantee that Grayscale is run at least once before any image is acquired by
    // triggering on all image triggers
    addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES,
                              ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());

    addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_ADJUST_FOCUS_IMAGES,
                              ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());

    addMajorMinorEventTrigger(TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT,
                              ImageAcquisitionEventEnum.IMAGE_ACQUISITION,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());
    
    //added by sheng chuan to make sure virtual live generation perform calgrayscale
    addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_VIRTUAL_LIVE_IMAGES,
                              TestExecutionEventEnum.ACQUIRE_VIRTUAL_LIVE_IMAGES,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());
    
    //XCR-3315, Able to generate image set with board loaded without start up machine and unload the board first
    addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES,
                              TestExecutionEventEnum.ACQUIRE_OFFLINE_IMAGES,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());
    
    // Additional Major Start events that may need to trigger all calibrations

    addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES,
                              TestExecutionEventEnum.ACQUIRE_VERIFICATION_IMAGES,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());

    addMajorMinorEventTrigger(TestExecutionEventEnum.ACQUIRE_ADJUST_FOCUS_IMAGES,
                              TestExecutionEventEnum.ACQUIRE_ADJUST_FOCUS_IMAGES,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());

    addMajorMinorEventTrigger(TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT,
                              TestExecutionEventEnum.RUN_MANUAL_ALIGNMENT,
                              _triggerState.TRIGGER_ON_START,
                              CalGrayscale.getInstance(),
                              ConfirmCameraCalibration.getInstance());

    setExceptionDrivenTaskMap();
  }

  /**
   * @author Greg Esparza
   */
  private void setExceptionDrivenTaskMap()
  {
    _exceptionDrivenTaskMap.put(XrayCameraHardwareExceptionEnum.AXI_TDI_EMBEDDED_GRAYSCALE_ADJUSTMENT_REQUIRED, new XrayCameraEnableGrayscaleToRunTask());
  }

  /**
   * Instance accessor function to get the list of automated Hardware Tasks
   * to be run by the HardwareTaskEngine
   * @author Reid Hayhow
   */
  public synchronized static XrayHardwareAutomatedTaskList getInstance()
  {
    if (_instance == null)
    {
      _instance = new XrayHardwareAutomatedTaskList();
    }

    return _instance;
  }

  /**
   * Add all lists to the HardwareTaskEngine
   *
   * @author Reid Hayhow
   */
  public void addListToTaskEngine()
  {
    HardwareTaskEngine engine = HardwareTaskEngine.getInstance();

    engine.initializeStartEventDrivenMap(_eventDrivenTaskMapStartTriggered);
    engine.initializeEndEventDrivenMap(_eventDrivenTaskMapEndTriggered);

    engine.initializeTimeDrivenMap(_timeDrivenTaskList);
    engine.initializeExceptionDrivenMap(_exceptionDrivenTaskMap);
  }

  /**
   * Function to encapsulate adding an event driven task to the 2D Map of Major/Minor
   * event that can trigger that task. This is for an event that will always be triggered
   * when a minor event is seen, regardless of the Major event it is seen with.
   *
   * @author Reid Hayhow
   */
  private static void addEventDrivenTaskAlwaysRunOnMinorEventToList(HardwareEventEnum minorEvent,
                                                                    EventTriggerStateEnum trigger,
                                                                    EventDrivenHardwareTask task)
  {
    Assert.expect(minorEvent != null);
    Assert.expect(task != null);

    //Just pass this call down using the special DONT care event
    addEventDrivenTaskToList(HardwareTaskEventEnum.HARDWARE_TASK_ENGINE_DONT_CARE_MAJOR_EVENT,
                             minorEvent,
                             trigger,
                             task);

  }

  /**
   * Function to encapsulate adding an event driven task to the 2D Map of Major/Minor
   * event that can trigger that task.
   *
   * @author Reid Hayhow
   */
  private static void addEventDrivenTaskToList(HardwareEventEnum majorEvent,
                                               HardwareEventEnum minorEvent,
                                               EventTriggerStateEnum trigger,
                                               EventDrivenHardwareTask task)
  {
    Assert.expect(majorEvent != null);
    Assert.expect(minorEvent != null);
    Assert.expect(task != null);

    if (trigger.equals(_triggerState.TRIGGER_ON_START))
    {
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>>
          minorEventStartMap = _eventDrivenTaskMapStartTriggered.get(majorEvent);

      //Create the lower level map entry, if it does not exist
      if (minorEventStartMap == null)
      {
        minorEventStartMap = new HashMap<HardwareEventEnum, List<EventDrivenHardwareTask>>();

        //Add the new map to the higher level map after creating it
        _eventDrivenTaskMapStartTriggered.put(majorEvent, minorEventStartMap);
      }

      //Get the list of tasks for this minor key
      List<EventDrivenHardwareTask> taskList = minorEventStartMap.get(minorEvent);

      //It could be null, if so, create it
      if (taskList == null)
      {
        //Create a list of one. Could add a addEventDrivenTaskToListList function later
        taskList = new ArrayList<EventDrivenHardwareTask>();
      }

      //No matter what, add the task to the list
      taskList.add(task);

      //Add the minor event and task to the lower level map
      minorEventStartMap.put(minorEvent, taskList);
    }
    else
    {
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>>
          minorEventEndMap = _eventDrivenTaskMapEndTriggered.get(majorEvent);

      //Create the lower level map entry, if it does not exist
      if (minorEventEndMap == null)
      {
        minorEventEndMap = new HashMap<HardwareEventEnum, List<EventDrivenHardwareTask>>();

        //Add the new map to the higher level map after creating it
        _eventDrivenTaskMapEndTriggered.put(majorEvent, minorEventEndMap);
      }

      //Get the list of tasks for this minor key
      List<EventDrivenHardwareTask> taskList = minorEventEndMap.get(minorEvent);

      //It could be null, if so, create it
      if (taskList == null)
      {
        //Create a list of one. Could add a addEventDrivenTaskToListList function later
        taskList = new ArrayList<EventDrivenHardwareTask>();
      }

      //No matter what, add the task to the list
      taskList.add(task);

      //Add the minor event and task to the lower level map
      minorEventEndMap.put(minorEvent, taskList);
    }
  }

  /**
   * Method to gain access to the list of tasks that must be run in order for
   * the system to be in a known-good state. It returns a deep copy of the list
   * of tasks
   *
   * @author Reid Hayhow
   */
  public List<EventDrivenHardwareTask> getListOfRequiredTasks()
  {
    //Create a copy of the task list so that clients cannot change the list stored here
    List<EventDrivenHardwareTask> copyOfTaskList = new ArrayList<EventDrivenHardwareTask>();

    //Currently the list of required tasks is identical to the set of tasks that
    //are run at system startup. This could change, but for now this simplifies
    //and centralizes access to the list
    copyOfTaskList.addAll(_eventDrivenTaskMapEndTriggered.get(XrayTesterEventEnum.STARTUP).get(XrayTesterEventEnum.STARTUP));

    //Finally return the copy of the list of tasks
    return copyOfTaskList;
  }

  /**
   * Take an arbitrary list of tasks and add them to the correct trigger map
   * based on the trigger state (Start/End) and Minor Hardware Event. Tasks
   * added here will trigger on ANY major event if it has the correct minor
   * event.
   *
   * @author Reid Hayhow
   */
  private void addMinorEventTrigger(HardwareEventEnum minor,
                                    EventTriggerStateEnum trigger,
                                    EventDrivenHardwareTask ...taskList)
  {
    addMajorMinorEventTrigger(HardwareTaskEventEnum.HARDWARE_TASK_ENGINE_DONT_CARE_MAJOR_EVENT,
                              minor,
                              trigger,
                              taskList);
  }

  /**
   * Take an arbitrary list of tasks and add them to the correct trigger map
   * based on the trigger state (Start/End), Major Hardware Event and Minor
   * Hardware Event.
   *
   * @author Reid Hayhow
   */
  private void addMajorMinorEventTrigger(HardwareEventEnum major,
                                         HardwareEventEnum minor,
                                         EventTriggerStateEnum trigger,
                                         EventDrivenHardwareTask ...taskList)
  {
    //What to do if it is a trigger on end
    if (trigger == EventTriggerStateEnum.TRIGGER_ON_END)
    {
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>> mapFound = _eventDrivenTaskMapEndTriggered.get(major);
      //It is possible that there is not an entry for the major trigger in the
      //map, this guarantees that things are ok, so continue. Otherwise there
      //was an entry in the map for this event, so look for the minor event-list pair
      if (mapFound != null)
      {
        //If the major map is not empty, then the minor map had bettered be!
        Assert.expect(mapFound.get(minor) == null);
      }
    }
    //Handle trigger on start
    else if (trigger == EventTriggerStateEnum.TRIGGER_ON_START)
    {
      //Look for the map of minor events for this major event
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>> mapFound = _eventDrivenTaskMapStartTriggered.get(major);

      //If the map is found, then continue to check
      if (mapFound != null)
      {
        //There should not be another map entry for this major/minor/start tuple
        Assert.expect(mapFound.get(minor) == null);
      }
    }
    //What happens if someone adds something to the triggerType enum that I don't
    //expect
    else
    {
      Assert.expect(false);
    }

    //For each event in the list that was passed in, add it to the correct map
    for (EventDrivenHardwareTask task : taskList)
    {
      //reuse existing method to do this work
      addEventDrivenTaskToList(major, minor, trigger, task);
    }
  }

  /**
   * Method to dump the trigger/task list to stdout. Only used for regression
   * testing.
   *
   * @author Reid Hayhow
   */
  public void printTaskList()
  {
    if (UnitTest.unitTesting() == false)
    {
      //Should only be called in unit testing
      Assert.expect(false);
    }
    //Dump the start triggered map
    printMapInfo(_eventDrivenTaskMapStartTriggered, "start");
    //Dump the end triggered map
    printMapInfo(_eventDrivenTaskMapEndTriggered, "end");

    //Neither of the other maps should have anything in them for now
    Assert.expect(this._exceptionDrivenTaskMap.isEmpty());
    Assert.expect(this._timeDrivenTaskList.isEmpty());
  }

  /**
   * Generic trigger map printing method.
   *
   * @author Reid Hayhow
   */
  private void printMapInfo(Map<HardwareEventEnum, Map<HardwareEventEnum, List<EventDrivenHardwareTask>>> mapToPrint, String preappend)
  {
    //Get the set of keys that are major event triggers
    Set<HardwareEventEnum> outerMapSetOfKeys = mapToPrint.keySet();

    //For each major event trigger...
    for (HardwareEventEnum outerKey : outerMapSetOfKeys)
    {
      //Get the set of minor event triggers
      Map<HardwareEventEnum, List<EventDrivenHardwareTask>> innerMap = mapToPrint.get(outerKey);

      //If there are no minor event triggers, then move on
      if (innerMap == null)
      {
        return;
      }

      //Otherwise, the the set of minor event triggers for this major event trigger
      Set<HardwareEventEnum> innerMapSetOfKeys = innerMap.keySet();

      //And for each minor event trigger...
      for (HardwareEventEnum innerKey : innerMapSetOfKeys)
      {
        //Get the list of EventDrivenHardwareTasks that are triggered
        List<EventDrivenHardwareTask> listOfTasksTriggeredByKeys = innerMap.get(innerKey);

        //Finally, dump the data for each EventDrivenHardwareTask
        for (EventDrivenHardwareTask task : listOfTasksTriggeredByKeys)
        {
          System.out.println(preappend + ":" + outerKey.toString() + ":" + innerKey.toString() + ":" + task.getName());
        }
      }
    }
  }
  
  /**
   * Method to help recover from a camera crash. It sets all calibrations and
   * confirmations that need to be run for cameras to run as soon as possible
   *
   * @author Swee Yee Wong
   */
  public void setCameraCalibrationsToRunASAP() throws DatastoreException
  {
    CalGrayscale.getInstance().setTaskToRunASAP();
    ConfirmCameraCalibration.getInstance().setTaskToRunASAP();

    ConfirmCameraDarkImageTask.getInstance().setTaskToRunASAP();
    ConfirmCameraLightImageWithNoPanel.getInstance().setTaskToRunASAP();

    /** @todo rfh remove this once LP3 is gone*/
    ConfirmCameraGroupOneLightImageTask.getInstance().setTaskToRunASAP();
    ConfirmCameraGroupTwoLightImageTask.getInstance().setTaskToRunASAP();
    ConfirmCameraGroupThreeLightImageTask.getInstance().setTaskToRunASAP();
  }

  /**
   * Method to set all required calibrations and confirmations to run as soon as
   * possible.
   *
   * @author Swee Yee Wong
   */
  public void setAllCalibrationsToRunASAP() throws DatastoreException
  {
    //Loop over the list of required tasks and set them to run as soon as possible
    for (EventDrivenHardwareTask requiredTask : getListOfRequiredTasks())
    {
      requiredTask.setTaskToRunASAP();
    }
  }
}
