package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.Assert;
import com.axi.util.LocalizedString;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * When the Magnification Coupon Confirmation fails this diagnostic will attempt to pinpoint
 * the issue.  It does so by running confirmations on sub-systems and rerunning calibrations.
 * The primary end result of this (or any) diagnostic is a user message.  This message is
 * delivered in the suggestedUserAction of the result for this diagnostic.
 *
 * @author John Dutton
 */
public class DiagnoseSharpness extends EventDrivenHardwareTask
{
  private static Config _config = Config.getInstance();
  static
  {
    try
    {
      _config.loadIfNecessary();
    }
    catch (DatastoreException ex)
    {
      Assert.expect(false, "Can't load config files");
    }
  }

  private static String _name = StringLocalizer.keyToString("CD_DIAGNOSE_SHARPNESS_KEY");
  private static DiagnoseSharpness _instance;
  private enum ResultFromCheck { PASSED, FAILED, REPAIRED_AND_PASSED };

  private int _progressPercentage = 0;

  // Logging variables.
  private static final int _maxLogFileSizeInBytes =
      _config.getIntValue(SoftwareConfigEnum.SHARPNESS_CONFIRM_MAX_LOG_FILESIZE_IN_BYTES);
  private String _logDirectoryFullPath = Directory.getSystemDiagnosticLogDir();
  private String _logFileName = FileName.getSystemDiagnosticLogFileName();
  private FileLoggerAxi _logger;


  /**
   * DiagnoseSharpness
   *
   * @author John Dutton
   */
  private DiagnoseSharpness()
  {
    // frequency and timeout are 1's because a diagnostic should always run.
    super(_name, 1, 1,ProgressReporterEnum.DIAGNOSE_SHARPNESS_TASK);

    //This is the only diagnostic and is designed to be run manually
    _taskType = HardwareTaskTypeEnum.MANUAL_DIAGNOSTIC_TASK_TYPE;
  }

  /**
   * @return Instance object of this singleton class.
   * @author Reid Hayhow
   */
  public static synchronized DiagnoseSharpness getInstance()
  {
    if (_instance == null)
    {
      _instance = new DiagnoseSharpness();
    }
    return _instance;
  }

  /**
   * Create a Limits object specific to the particular test being run.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject()
  {
    _limits = Limit.createShouldPassLimit();
  }

  /**
   * Create the results object for this test result.  For a diagnostic
   * the result is fail if it ran and found an issue, otherwise, if it
   * couldn't find an issue, the result is passing so long as the System Image Quality
   * Confirmation passes.  Initializing to fail, setting to pass in executeTask() as needed.
   *
   * @author John Dutton
   */
  protected void createResultsObject()
  {
    _result = Result.createFailingResult(_name);
  }

  /**
   * This method contains the actual task code, i.e. steps of the diagnostic.
   *
   * @author John Dutton
   */
  protected void executeTask() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
    //Start the progress observable so users know what is happening
    initializeAndReportProgressPercentage();

    // First run confirmations for the main sub-systems that contribute to optical
    // quality.  Perhaps one of them has a problem.

    // Someday, when China tube is in production, call its confirmation here, first.


    //Check communication with cameras and IRP's
    if (checkIndividualCommunications() == false)
    {
      //specific message was already set in method, so just return
      return;
    }

    increaseAndReportProgressPercentage(10);

    // Check that camera calibrations are within specification.
    ResultFromCheck result = checkCameraCalibrations();
    if (result == ResultFromCheck.FAILED)
    {
      //specific message is set by function call, so just return
      return;
    }
    else if (result == ResultFromCheck.REPAIRED_AND_PASSED)
    {
      // The check ran the grayscale cal to allow camera calibrations to pass, so see if sharpness
      // cal will pass.
      ConfirmMagnificationCoupon confMagCoupon = ConfirmMagnificationCoupon.getInstance();
      log("ConfirmMagnificationCoupon Run");
      if (confMagCoupon.executeWithoutUpdate() == true)
      {
        log("ConfirmMagnificationCoupon Passed");

        // Create a passing result and populate it with information
        _result = Result.createPassingResult(_name);

        // Problem fixed. Let user know what we did and that this shouldn't happen frequently.
        setMessagesForRerunningCal(CalGrayscale.getInstance());
        return;
      }
      else
      {
        // Fall through and continue on checking sub-systems.  Even though camera
        // calibrations have been fixed, the sharpness cal still doesn't pass.
        log("ConfirmMagnificationCoupon Failed");
      }
    }

    increaseAndReportProgressPercentage(10);   // Show 20%

    // Check that camera image quality is within specification.
    if (checkCameraImageQuality() == false)
    {
      //specific message is set by function call, so just return
      return;
    }

    increaseAndReportProgressPercentage(10);   // Show 30%

    // None of the above sub-systems has a problem, so run the calibrations again.  Perhaps one
    // of the calibrations aren't being run often enough.  Run them in same order as at startup.

    // But before running calibrations, make sure mag coupon confirmation is truly failing
    // (this diagnostic can be run from the Service GUI when the system is in good shape)
    // because checkAdjustments() below will indicate that running an adjustment brought
    // the system back into spec if the mag coupon confirmation passes.
    ConfirmMagnificationCoupon confMagCoupon = ConfirmMagnificationCoupon.getInstance();
    log("ConfirmMagnificationCoupon Run");
    if (confMagCoupon.executeWithoutUpdate() == true)
    {
      log("ConfirmMagnificationCoupon Passed");
      // Tell user everything is ok and no action is necessary.
      // Create a passing result and populate it
      _result = Result.createPassingResult(_name);

      getResult().setTaskStatusMessage(StringLocalizer.keyToString(
          "CD_DIAGNOSE_SHARPNESS_SHARPNESS_IS_GOOD_STATUS_KEY"));
      getResult().setSuggestedUserActionMessage(StringLocalizer.keyToString(
          "CD_DIAGNOSE_SHARPNESS_SHARPNESS_IS_GOOD_ACTION_KEY"));
      return;
    }
    log("ConfirmMagnificationCoupon Failed");

    increaseAndReportProgressPercentage(10);   // Show 40%

    // Try rerunning all of the adjustment
    // Show 40% through 70%
    if (checkAdjustments(30) == false)
    {
      //specific message is set by function call, so just return
      return;
    }

    // Adjustments are all set and still can't pass mag coupon.  At this point, something
    // is probably wrong with Xrays or motion or stage flatness.  As far as which
    // confirmations to run next, run the ones from which you can make decisive determinations.

    // Test motion.
    ConfirmPanelPositioningTask ppTask = ConfirmPanelPositioningTask.getInstance();
    log("ConfirmPanelPositionTask Run");
    if (ppTask.executeWithoutUpdate() == false)
    {
      log("ConfirmPanelPositionTask Failed");
      // Motion has an issue.  Let Panel Positioning Confirmation describe what the user should do.
      setMessagesByCopying(ppTask);
      return;
    }
    log("ConfirmPanelPositionTask Passed");

    increaseAndReportProgressPercentage(10);   // Show 80%

    // So motion is okay.  Looks like X-ray or stage flatness of travel issue.
    // Out of confirmations that test isolated sub-systems.  Run System MTF, since
    // it tells you something about X-rays.

    ConfirmLowMagSystemMTF MTFTask = ConfirmLowMagSystemMTF.getInstance();
    log("ConfirmLowMagSystemMTF Run");
    if (MTFTask.executeWithoutUpdate() == false)
    {
      log("ConfirmLowMagSystemMTF Failed");
      // Tell user X-ray or stage flatness of travel issue.  Contact Agilent service person.
      getResult().setTaskStatusMessage(StringLocalizer.keyToString(
          "CD_DIAGNOSE_SHARPNESS_SYSTEM_MTF_FAILED_STATUS_KEY"));
      getResult().setSuggestedUserActionMessage(StringLocalizer.keyToString(
          "CD_DIAGNOSE_SHARPNESS_CONTACT_AXI_ACTION_KEY"));
      return;
    }
    log("ConfirmLowMagSystemMTF Passed");

    increaseAndReportProgressPercentage(15);   // Show 95%

    // Can't find the problem.  Could be a reconstruction issue or a subtle X-ray, motion,
    // or flatness of travel issue.  Next step is to run confirmation with debug
    // images collected and to look at them.  Can also run other sharpness confirmations
    // that test coupons in other pockets.

    // Tell user all other confirmations pass.  Contact Agilent service person.
    getResult().setTaskStatusMessage(StringLocalizer.keyToString(
        "CD_DIAGNOSE_SHARPNESS_COULDNT_FIND_CAUSE_OF_PROBLEM_STATUS_KEY"));
    getResult().setSuggestedUserActionMessage(StringLocalizer.keyToString(
        "CD_DIAGNOSE_SHARPNESS_CONTACT_AXI_ACTION_KEY"));

    //Report that this task is done
    reportProgressPercentageDone();
  }

  /**
   * checkIndividualCommunications
   *
   * @return boolean False if did not pass communications test.  True if did.
   * @author John Dutton
   */
  private boolean checkIndividualCommunications() throws XrayTesterException
  {
    // If had a way to test ethernet switch, would do it here, first.

    // Check IRPs
    ConfirmCommunicationWithIRPs irpComm = ConfirmCommunicationWithIRPs.getInstance();
    log("ConfirmCommunicationWithIRPs Run");
    if (irpComm.executeWithoutUpdate() != true)
    {
      log("ConfirmCommunicationWithIRPs Failed");
      // Use messages from camera communication confirmation.
      setMessagesByCopying(irpComm);
      return false;
    }
    log("ConfirmCommunicationWithIRPs Passed");

    // Check RMS
    ConfirmCommunicationWithRMS rmsComm = ConfirmCommunicationWithRMS.getInstance();
    log("ConfirmCommunicationWithRMS Run");
    if (rmsComm.executeWithoutUpdate() != true)
    {
      log("ConfirmCommunicationWithRMS Failed");
      // Use messages from camera communication confirmation.
      setMessagesByCopying(rmsComm);
      return false;
    }
    log("ConfirmCommunicationWithRMS Passed");

    return true;
  }

  /**
  * Checks calibrations in the cameras.  If they are bad, attempts to run grayscale calibration
  * and then the confirmation again.
  *
  * Sets status and action messages if camera calibrations fail.
  *
  * @return ResultFromCheck FAILED if did not pass image quality specification.  PASSED if did.
  *                         REPAIRED_AND_PASSED if had to do some "repair" to get the check to pass.
  *                         Currently the repair is to rerun the grayscale cal.
  * @author John Dutton
  */
 private ResultFromCheck checkCameraCalibrations() throws XrayTesterException
 {
   // Check that camera calibrations are within specification.
   ConfirmCameraCalibration confCalFile = ConfirmCameraCalibration.getInstance();
   log("ConfirmCameraCalibration Run");
   if (confCalFile.executeWithoutUpdate() == false)
   {
     log("ConfirmCameraCalibration Failed");
     // Failed, but before having the user do anything, recal the cameras and see
     // if the problem goes away.
     CalGrayscale calGrayscale = CalGrayscale.getInstance();
     log("Adjust Grayscale Run");
     if (calGrayscale.executeWithoutUpdate() == false)
     {
       log("Adjust Grayscale Failed");
       // Can't cal successfully, no wonder confirmations are failing.
       setMessagesByCopying(calGrayscale);
       return ResultFromCheck.FAILED;
     }
     log("Adjust Grayscale Passed");

     // Cameras are recal'd, see if camera calibration confirmation will pass.
     log("ConfirmCameraCalibration Run");
     if (confCalFile.executeWithoutUpdate() == false)
     {
       log("ConfirmCameraCalibration Failed");
       // Problem didn't go away after recal, have the camera calibration confirmation
       // tell the user what to do.
       setMessagesByCopying(confCalFile);
       return ResultFromCheck.FAILED;
     }
     else
     {
       log("ConfirmCameraCalibration Passed");
       return ResultFromCheck.REPAIRED_AND_PASSED;
       // Camera calibrations passed, but since they didn't without running grayscale cal,
       // rerun the sharpness cal and see if it passes.  (Have the caller do this on the return
       // to keep this method cohesive.)
     }
   }
   else
   {
     log("ConfirmCameraCalibration Passed");
     return ResultFromCheck.PASSED;
   }
 }

 /**
  * Run light and dark camera image quality confirmations.
  *
  * Sets status and action messages if camera calibrations fail.

  * @return boolean False if did not pass image quality specification.  True if did.
  * @author John Dutton
  */
 private boolean checkCameraImageQuality() throws XrayTesterException
 {
   // Before testing image quality, make sure grayscale cal is up-to-date.  This should
   // run the cal on an as needed basis.
   CalGrayscale calGrayscale = CalGrayscale.getInstance();
   calGrayscale.runIfNeededUsingShortestTimeout();

   // Dark image quality is most sensitive and important, run it first
   ConfirmCameraDarkImageTask dark = ConfirmCameraDarkImageTask.getInstance();
   log("ConfirmCameraDarkImageTask Run");
   if (dark.executeWithoutUpdate() == false)
   {
     log("ConfirmCameraDarkImageTask Failed");
     setMessagesByCopying(dark);
     return false;
   }
   log("ConfirmCameraDarkImageTask Passed");

   // Dark passed, try the light confirmation.
   ConfirmCameraLightAllGroups light = ConfirmCameraLightAllGroups.getInstance();
   log("ConfirmCameraLightAllGroups Run");
   if (light.executeWithoutUpdate() == false)
   {
     log("ConfirmCameraLightAllGroups Failed");
     setMessagesByCopying(light);
     return false;
   }
   log("ConfirmCameraLightAllGroups Passed");

   return true;
 }

  /**
   * @return False means either an adjustment failed or after running an adjustment
   * the Magnification Coupon Confirmation passed.  (Both are conditions that Diagnose
   * Sharpness can return from, i.e. stop diagnosing.)  True means adjustments completed,
   * but the Magnification Coupon Confirmation still failed.
   *
   * @author John Dutton
   */
  private boolean checkAdjustments(int percentageOfProgressToShow) throws XrayTesterException
  {
    // Rerun Grayscale Cal and see if that brings mag coupon confirmation back into spec.
    CalGrayscale calGrayscale =
        CalGrayscale.getInstance();
    log("AdjustGrayscale Run");
    if (calGrayscale.executeWithoutUpdate() == false)
    {
      log("AdjustGrayscale Failed");
      setMessagesByCopying(calGrayscale);
      return false;
    }
    log("AdjustGrayscale Passed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    log("ConfirmMagnificationCoupon Run");
    ConfirmMagnificationCoupon confMagCoupon = ConfirmMagnificationCoupon.getInstance();
    if (confMagCoupon.executeWithoutUpdate() == true)
    {
      log("ConfirmMagnificationCoupon Passed");
      // Problem fixed. Let user know what we did and that this shouldn't happen frequently.
      setMessagesForRerunningCal(calGrayscale);
      return false;
    }
    log("ConfirmMagnificationCoupon Failed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    // Rerun Xray Spot Cal and see if that brings mag coupon confirmation back into spec.
    CalXraySpotCoordinateEventDrivenHardwareTask calXraySpot =
        CalXraySpotCoordinateEventDrivenHardwareTask.getInstance();
    log("AdjustXraySpot Run");
    if (calXraySpot.executeWithoutUpdate() == false)
    {
      log("AdjustXraySpot Failed");
      setMessagesByCopying(calXraySpot);
      return false;
    }
    log("AdjustXraySpot Passed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    log("ConfirmMagnificationCoupon Run");
    if (confMagCoupon.executeWithoutUpdate() == true)
    {
      log("ConfirmMagnificationCoupon Passed");
      // Problem fixed. Let user know what we did and that this shouldn't happen frequently.
      setMessagesForRerunningCal(calXraySpot);
      return false;
    }
    log("ConfirmMagnificationCoupon Failed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    // Rerun Hysteresis Cal and see if that brings mag coupon confirmation back into spec.
    CalHysteresisEventDrivenHardwareTask calHysteresis =
        CalHysteresisEventDrivenHardwareTask.getInstance();
    log("AdjustHysteresis Run");
    if (calHysteresis.executeWithoutUpdate() == false)
    {
      log("AdjustHysteresis Failed");
      setMessagesByCopying(calHysteresis);
      return false;
    }
    log("AdjustHysteresis Passed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    log("ConfirmMagnificationCoupon Run");
    if (confMagCoupon.executeWithoutUpdate() == true)
    {
      log("ConfirmMagnificationCoupon Passed");
      // Problem fixed. Let user know what we did and that this shouldn't happen frequently.
      setMessagesForRerunningCal(calHysteresis);
      return false;
    }
    log("ConfirmMagnificationCoupon Failed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    // Rerun System Fiducial Cal and see if that brings mag coupon confirmation back into spec.
    CalSystemFiducialEventDrivenHardwareTask calSysFid =
        CalSystemFiducialEventDrivenHardwareTask.getInstance();
    log("AdjustSystemOffset Run");
    if (calSysFid.executeWithoutUpdate() == false)
    {
      log("AdjustSystemOffset Failed");
      setMessagesByCopying(calSysFid);
      return false;
    }
    log("AdjustSystemOffset Passed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    log("ConfirmMagnificationCoupon Run");
    if (confMagCoupon.executeWithoutUpdate() == true)
    {
      log("ConfirmMagnificationCoupon Passed");
      // Problem fixed. Let user know what we did and that this shouldn't happen frequently.
      setMessagesForRerunningCal(calSysFid);
      return false;
    }
    log("ConfirmMagnificationCoupon Failed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    // Rerun Magnification Cal and see if that brings mag coupon confirmation back into spec.
    CalMagnification calMag = CalMagnification.getInstance();
    log("AdjustMagnification Run");
    if (calMag.executeWithoutUpdate() == false)
    {
      log("AdjustMagnification Failed");
      setMessagesByCopying(calMag);
      return false;
    }
    log("AdjustMagnification Passed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    log("ConfirmMagnificationCoupon Run");
    if (confMagCoupon.executeWithoutUpdate() == true)
    {
      log("ConfirmMagnificationCoupon Passed");
      // Problem fixed. Let user know what we did and that this shouldn't happen frequently.
      setMessagesForRerunningCal(calMag);
      return false;
    }
    log("ConfirmMagnificationCoupon Failed");

    increaseAndReportProgressPercentage(percentageOfProgressToShow / 10);

    return true;
    }


  /**
   * This method provides a single point of contact where tasks can put their
   * set-up code that may be reused
   *
   * @author Reid Hayhowd
   */
  protected void setUp() throws XrayTesterException
  {
    //none needed
  }


  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * clean-up code that may be reused (for example executeTask and abortTask
   * may both use the same cleanUp code)
   *
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    //none needed
  }

  /**
   * @author John Dutton
   */
  private void setMessagesByCopying (EventDrivenHardwareTask task)
  {
    getResult().setTaskStatusMessage(task.getResult().getTaskStatusMessage());
    getResult().setSuggestedUserActionMessage(task.getResult().getSuggestedUserActionMessage());
  }

  /**
   * @author John Dutton
   */
  private void setMessagesForRerunningCal (EventDrivenHardwareTask task)
  {
    getResult().setTaskStatusMessage(StringLocalizer.keyToString(
        new LocalizedString("CD_DIAGNOSE_SHARPNESS_RERAN_CAL_FIXED_ISSUE_STATUS_KEY",
                            new Object[]
                            {task.getName()})));
    getResult().setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_DIAGNOSE_SHARPNESS_RERAN_CAL_FIXED_ISSUE_ACTION_KEY"));
  }

  /**
   * Logs strings.
   *
   * @author John Dutton
   */
  private void log(String s) throws DatastoreException
  {
    if(_logger == null)
    {
      _logger = new FileLoggerAxi(_logDirectoryFullPath + File.separator +
                                  _logFileName + FileName.getLogFileExtension(),
                                  _maxLogFileSizeInBytes);
    }
    _logger.append(s);
  }

  /**
   * @author John Dutton
   */
  private void increaseAndReportProgressPercentage(int n)
  {
    _progressPercentage += n;
    Assert.expect(_progressPercentage <= 100, "progress dialog has gone past 100%");
    reportProgress(_progressPercentage);
  }

  /**
   * @author John Dutton
   */
  private void initializeAndReportProgressPercentage()
  {
    _progressPercentage = 0;
    reportProgress(_progressPercentage);
  }

  /**
   * @author John Dutton
   */
  private void reportProgressPercentageDone()
  {
    reportProgress(100);
  }

  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
