package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
public class Test_CalHysteresisEventDrivenHardwareTask extends UnitTest
{
  private static final int _EXPECTED_OFFSET = -135174;
  private Config _config = Config.getInstance();

  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    try
    {
      Assert.setLogFile("tempLog", "tempVersion");
      TestUtils.initConfigForRegressionTest();
      UnitTest.execute(new Test_CalHysteresisEventDrivenHardwareTask());
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();
    }
  }

  /**
   * @author Eddie Williamson
   */
  protected Test_CalHysteresisEventDrivenHardwareTask()
  {
    super();
  }

  /**
   * @author Eddie Williamson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XrayCameraArray xRayCameraArray = null;

    try
    {
      _config.loadIfNecessary();

      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getStageHysteresisDir();
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);

      CalGrayscale.getInstance().executeUnconditionally();

      // Save original value
      int beforeHysteresis = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);

      CalHysteresisEventDrivenHardwareTask cal = CalHysteresisEventDrivenHardwareTask.getInstance();
      cal.executeNonInteractively();
      Expect.expect(cal.didTestPass());

      // Save original value
      int afterHysteresis = _config.getIntValue(HardwareCalibEnum.MOTION_CONTROL_Y_AXIS_HYSTERESIS_OFFSET_IN_NANOMETERS);

      // Compare
      int offset = afterHysteresis - beforeHysteresis;
      Expect.expect(offset == _EXPECTED_OFFSET, "Expected an offset of " +
                    _EXPECTED_OFFSET +
                    " but saw offset of " +
                    offset);
    }
    catch (Exception ex)
    {
      // Shouldn't get here, if so the test has failed, just print why
      ex.printStackTrace();
      Expect.expect(false);
    }
    finally
    {
      xRayCameraArray.disableUnitTestMode();
      TestUtils.restoreConfigAfterRegressionTest();
    }
  }
}


