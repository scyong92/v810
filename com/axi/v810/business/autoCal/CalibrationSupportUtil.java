package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.image.*;
import com.axi.v810.business.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.license.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;

/**
 * @author Eddie Williamson
 */
public class CalibrationSupportUtil
{
  private static Config _config = Config.getInstance();

  /**
   * @author Eddie Williamson
   */
  public static AbstractXrayCamera getXraySpotCoordinateCamera()
  {
    return XrayCameraArray.getCamera(XrayCameraIdEnum.XRAY_CAMERA_0);
  }

  /**
   * @author Eddie Williamson
   */
  public static List<AbstractXrayCamera> getSystemFiducialCameras()
  {
    return XrayCameraArray.getCameras();
  }

  /**
   * @author Eddie Williamson
   */
  public static List<AbstractXrayCamera> getHysteresisCameras()
  {
    return XrayCameraArray.getCameras();
  }

  /**
   * @author Eddie Williamson
   */
  public static Image createTemplateImageOfSystemFiducial(int borderWidth, int borderHeight, int sizeX, int sizeY)
  {
    java.awt.Color couponBackground = new java.awt.Color(110, 110, 110); // Approx grey level of background of the coupon
    java.awt.Color couponForground = new java.awt.Color(145, 145, 145); // Approx grey level of foreground of the coupon

    // Create the template
    java.awt.image.BufferedImage templateBuffer = new java.awt.image.BufferedImage(sizeX + 2 * borderWidth, sizeY + 2 * borderHeight,
        java.awt.image.BufferedImage.TYPE_BYTE_GRAY);
    java.awt.Graphics2D gfx = templateBuffer.createGraphics();
    gfx.setBackground(java.awt.Color.DARK_GRAY);

    gfx.setColor(couponBackground);
    gfx.fillRect(0, 0, templateBuffer.getWidth(), templateBuffer.getHeight());

    // Define system fiducial rectangle in pixels
    int templateXCoords[] = {borderWidth,          borderWidth + sizeX,  borderWidth + sizeX};
    int templateYCoords[] = {borderHeight + sizeY, borderHeight + sizeY, borderHeight };
    gfx.setColor(couponForground);
    gfx.fillPolygon(templateXCoords, templateYCoords, 3);  // 3 = num data points

    Image templateImage = Image.createFloatImageFromBufferedImage(templateBuffer);
    return templateImage;
  }

  /**
   * Create CSV (comma seprated values) string of ProjectionRequest data
   * @author Eddie Williamson
   */
  public static String ProjectionRequestToCSVString(ProjectionRequestThreadTask projectionRequest)
  {
    StringBuilder logString = new StringBuilder();

    logString.append(projectionRequest.getStageDirection() + "," +
                     projectionRequest.getSystemFiducialRectangle().getRectangle2D().getX() + "," +
                     projectionRequest.getSystemFiducialRectangle().getRectangle2D().getY() + "," +
                     projectionRequest.getSystemFiducialRectangle().getWidth() + "," +
                     projectionRequest.getSystemFiducialRectangle().getHeight()
        );
    return logString.toString();
  }

  /**
   * Create CSV (comma seprated values) string of ProjectionInfo data
   * @author Eddie Williamson
   */
  public static String ProjectionInfoToCSVString(ProjectionInfo projectionInfo)
  {
    StringBuilder logString = new StringBuilder();

    logString.append(projectionInfo.getXStageWhereFirstRowImagedOnVerticalCenterOfCamera() + "," +
                     projectionInfo.getYStageWhereFirstRowImagedOnVerticalCenterOfCamera() + "," +
                     projectionInfo.getImageRectangleForRequestedProjection().getRectangle2D().getMinX() + "," +
                     projectionInfo.getImageRectangleForRequestedProjection().getRectangle2D().getMinY());
    return logString.toString();
  }

  /**
   * The calls to execute the cal procedure happen so fast that the logging
   * timestamp comes out the same (lowest resolution is seconds). This
   * delays just over 1 second so each timestamp comes out different.
   * This is used for the logfilename so we get a logfile for each if
   * needed for debugging.
   * Intended only for regression testing but we won't assert that
   * just in case.
   * @author Eddie Williamson
   */
  public static void delayForLogFileTimestamp()
  {
    try
    {
      Thread.currentThread().sleep(1100);
    }
    catch (InterruptedException ex)
    {
      // Nothing, I did my best.
    }
  }

  /**
   * @author Eddie Williamson
   */
  public static void debugPrint(String message)
  {
    if (_config.getBooleanValue(SoftwareConfigEnum.DEVELOPER_DEBUG_MODE))
    {
      System.out.println(message);
    }
  }

  /**
   * return true if bypass hardware task needed in Confirmation And Adjustment passive mode
   * return false if bypass hardware task is not needed, run it as usual.
   * @author Phang Siew Yeng
   */
  public static boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode(boolean isHighMagTask)
  {
    try
    {
      if(LicenseManager.isVariableMagnificationEnabled() == false)
        return false;
    }
    catch(BusinessException be)
    {
      //do nothing
    }
    
    Project project = null;
    if(Project.isCurrentProjectLoaded())
      project = Project.getCurrentlyLoadedProject();
    
    boolean passiveMode = false;
    String confirmationAndAdjustmentMode = _config.getStringValue(SoftwareConfigEnum.CONFIRMATION_AND_ADJUSTMENT_MODE);
    
    if(confirmationAndAdjustmentMode.equals(ConfirmationAndAdjustmentModeEnum.PASSIVE_MODE.toString()))
      passiveMode = true;
    
    if(passiveMode && project != null)
    {
      if(project.getPanel().getPanelSettings().hasHighMagnificationComponent()) 
      {
        if(project.getPanel().getPanelSettings().hasLowMagnificationComponent())
          return false;
        else
        {
          if(isHighMagTask == false)
            return true;
        }
      }
      else
      {
        if(isHighMagTask)
          return true;
      }
    }
    
    return false;
  }
}












