package com.axi.v810.business.autoCal;

import com.axi.util.Enum;

/**
 * Enum of edges that the Sharpness
 * @author John Dutton
 */
public class ConfirmSharpnessEdgeEnum extends Enum
{
  private static int _index = -1;

  public static ConfirmSharpnessEdgeEnum VERTICAL = new ConfirmSharpnessEdgeEnum(++_index);
  public static ConfirmSharpnessEdgeEnum HORIZONTAL = new ConfirmSharpnessEdgeEnum(++_index);
  public static ConfirmSharpnessEdgeEnum DIAGONAL = new ConfirmSharpnessEdgeEnum(++_index);


  /**
   * @author John Dutton
   */
  protected ConfirmSharpnessEdgeEnum(int id)
  {
    super(id);
  }
}
