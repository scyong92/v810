package com.axi.v810.business.autoCal;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * This class is a utility class to simplify saving an image to disk as a parallel
 * thread task.
 *
 * @author Reid Hayhow
 *
 */
class ImageSaverThreadTask extends ThreadTask<Boolean>
{
  //Members containing the image and image path+name to be saved
  private Image _imageToSave;
  private String _imagePathAndNameString;

  /**
   * Constructor, takes the image and path+name for the image that will be saved.
   *
   * @author Reid Hayhow
   */
  ImageSaverThreadTask(Image image, String imagePathAndNameString)
  {
    super("Image Saver Thread Task");

    //Can't work without an image or path+name
    Assert.expect(image != null);
    Assert.expect(imagePathAndNameString != null);

    //Set the members based on input
    _imageToSave = image;
    _imageToSave.incrementReferenceCount();
    _imagePathAndNameString = imagePathAndNameString;
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void cancel() throws Exception
  {
    //Clear the byte buffer for the image on cancel. Not necessary but nice.
    _imageToSave.decrementReferenceCount();
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void clearCancel()
  {
    //Do nothing
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected Boolean executeTask() throws DatastoreException
  {
    //Save a copy of the image in the log directory
    XrayImageIoUtil.savePngImage(_imageToSave, _imagePathAndNameString);

    //Clear the byte buffer for the image when done. Not necessary but nice.
    _imageToSave.decrementReferenceCount();

    return true;
  }
}
