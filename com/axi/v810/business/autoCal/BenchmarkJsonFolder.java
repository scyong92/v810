package com.axi.v810.business.autoCal;

import java.util.*;

/**
 * @author Anthony Fong
 */
public class BenchmarkJsonFolder extends BenchmarkJsonFileSystemItem
{

  private BenchmarkJsonFile[] files;
  private BenchmarkJsonFolder[] folders;

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFile[] getFiles()
  {
    return files;
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFolder[] getFolders()
  {
    return folders;
  }

  /**
   * @author Anthony Fong
   */
  public BenchmarkJsonFolder(String name, BenchmarkJsonFileSystemItem... items)
  {
    super(name, null, null, null);

    List<BenchmarkJsonFile> files = new ArrayList<BenchmarkJsonFile>();
    List<BenchmarkJsonFolder> folders = new ArrayList<BenchmarkJsonFolder>();

    for (BenchmarkJsonFileSystemItem item : items)
    {
      if (item instanceof BenchmarkJsonFile)
      {
        files.add((BenchmarkJsonFile) item);
      }
      else
      {
        folders.add((BenchmarkJsonFolder) item);
      }
    }

    this.files = files.toArray(new BenchmarkJsonFile[files.size()]);
    this.folders = folders.toArray(new BenchmarkJsonFolder[folders.size()]);
  }
}
