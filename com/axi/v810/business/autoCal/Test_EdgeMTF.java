package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.util.image.*;
import java.awt.image.*;

/**
 *
 * @author John Dutton
 */
public class Test_EdgeMTF extends UnitTest
{
  private FileWriter _outputFileWriter = null;

  protected Test_EdgeMTF()
  {
    super();
  }

  public static void main(String[] args)
  {
    UnitTest.execute(new Test_EdgeMTF());
  }

   /**
    * @author John Dutton
    */

  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      String testImagesDirectory = getTestDataDir() + File.separator + getClass().getSimpleName();
      String resultsDir = testImagesDirectory + File.separator + "results";

/*
      // Uncomment to capture intermediate steps in the MTF algorithm to various files.  For debugging purposes.
      EdgeMTF.setSaveDebugFiles(resultsDir);
*/

/*
      // Uncomment to capture results summary for comparison purposes with Matlab.
      File outputFile = new File(resultsDir + File.separator + "results.csv");
      _outputFileWriter = new FileWriter(outputFile);
*/

      // Test various horizontal edges
      testImage("Test 0", false, testImagesDirectory, "edge_binary_10deg.png", 94.59, 1.21, 1.395);

      testImage("Test 1", false, testImagesDirectory, "MTF_Horizontal_A1.png", 94.17, 4.93, 3.950);

      testImage("Test 2", false, testImagesDirectory, "MTF_Horizontal_A2.png", 94.27, 5.03, 4.258);

      testImage("Test 3", false, testImagesDirectory, "MTF_Horizontal_A3.png", 94.3, 4.88, 4.333);

      testImage("Test 4", false, testImagesDirectory, "MTF_Horizontal_B3.png", 86.7, 4.56, 4.133);

      testImage("Test 5", false, testImagesDirectory, "Horizontal1.png", 87.03, 4.91, 4.521);

      testImage("Test 6", false, testImagesDirectory, "Horizontal2.png", 86.75, 4.80, 4.071);

      testImage("Test 7", false, testImagesDirectory, "Horizontal3.png", 86.82, 4.34, 4.159);

      testImage("Test 8", false, testImagesDirectory, "Horizontal4.png", 86.1, 4.59, 3.867);

      testImage("Test 9", false, testImagesDirectory, "Horizontal5.png", 85.92, 5.23, 4.291);

      testImage("Test 10", false, testImagesDirectory, "Horizontal6.png", 85.44, 4.98, 4.393);

      testImage("Test 11", false, testImagesDirectory, "Horizontal7.png", 94.1, 5.02, 3.812);

      testImage("Test 12", false, testImagesDirectory, "Horizontal8.png", 94.4, 5.13, 4.585);

      testImage("Test 13", false, testImagesDirectory, "Horizontal9.png", 94.29, 4.91, 4.226);

      testImage("Test 14", false, testImagesDirectory, "Horizontal1a.png", 86.92, 4.93, 4.272);

      // Test various vertical edges
      testImage("Test 15", true, testImagesDirectory, "MTF_Vertical.png", 179.5, 6.42, 3.923);

      testImage("Test 16", true, testImagesDirectory, "Vertical1.png", 0.3900000000000001, 7.36, 4.086);

      testImage("Test 17", true, testImagesDirectory, "Vertical2.png", 0.3400000000000001, 6.09, 4.338);

      // Test edge angels
      testImage("Test 18", false, testImagesDirectory, "black_5deg.png", 95.39, 1.17, 1.372);

      testImage("Test 19", false, testImagesDirectory, "black_30deg.png", 118.61, 1.16, 1.364);

      testImage("Test 20", false, testImagesDirectory, "black_45deg.png", 134.79, 1.29, 1.495);

      testImage("Test 21", true, testImagesDirectory, "black_60deg.png", 153.37, 1.72, 2.143);

      testImage("Test 22", true, testImagesDirectory, "black_120deg.png", 29.54, 1.28, 1.47);

      testImage("Test 23", false, testImagesDirectory, "black_135deg.png", 47.49, 1.27, 1.505);

      testImage("Test 24", false, testImagesDirectory, "black_150deg.png", 74.45, 1.20, 1.429);

      // Test edge angles at perfectly vertical and horizontal
      testImage("Test 25", false, testImagesDirectory, "black_0deg.png", 89.73, 1.17, 1.350);

      testImage("Test 26", true, testImagesDirectory, "black_90deg.png", 179.74, 1.13, 1.352);

      // Test extreme image dimensions
      testImage("Test 27", true, testImagesDirectory, "black_wide_vertical.png", 44.91, 1.43, 1.685);

      testImage("Test 28", false, testImagesDirectory, "black_wide_horizontal.png", 85.32, 1.17, 1.357);

      testImage("Test 29", true, testImagesDirectory, "black_tall_vertical.png", 2.14, 1.18, 1.384);

      testImage("Test 30", false, testImagesDirectory, "black_tall_horizontal.png", 72.87, 1.23, 1.475);


    }
    catch (Exception e)
    {
      Expect.expect(false, "EdgeMTF test threw unexpected exception");
    }
    finally
    {
      if (_outputFileWriter != null)
        try
        {
          _outputFileWriter.close();
        }
        catch (IOException ex)
        {
          // Do nothing; merely failed closing a file used for development.
        }
    }

  }


  private void testImage(String testName, boolean isVerticalEdge, String testImagesDirectory, String imageFileName,
                         double edgeAngleInDegrees, double edgeSpreadInPixels, double lineSpreadFWHMInPixels)
  {
    try
    {
      BufferedImage bufferedImg = ImageIoUtil.loadPngBufferedImage(testImagesDirectory + File.separator + imageFileName);
      Image img = Image.createFloatImageFromBufferedImage(bufferedImg);
      EdgeMTF edgeMTF = new EdgeMTF(img, isVerticalEdge, imageFileName);


      Expect.expect(edgeAngleInDegrees == edgeMTF.getEdgeAngleInDegrees(),
                    "MTF test: " + testName + ": bad value for edge angle");
      Expect.expect(edgeSpreadInPixels - 0.02 <= edgeMTF.getEdgeSpreadRiseInPixels() &&
                    edgeMTF.getEdgeSpreadRiseInPixels() <= edgeSpreadInPixels + 0.02,
                    "MTF test: " + testName + ": bad value for edge rise value");
      Expect.expect(lineSpreadFWHMInPixels - 0.02 <= edgeMTF.getLineSpreadFWHMInPixels() &&
                    edgeMTF.getLineSpreadFWHMInPixels() <= lineSpreadFWHMInPixels + 0.02,
                    "MTF test: " + testName + ": bad value for line spread FWHM value");


      // Capture data in file for comparison purposes with Matlab.
      if (_outputFileWriter != null)
      {
        String output = String.format("%s, %.2f, %.2f, %.3f", imageFileName, edgeMTF.getEdgeAngleInDegrees(),
                                edgeMTF.getEdgeSpreadRiseInPixels(), edgeMTF.getLineSpreadFWHMInPixels());
        output += System.getProperty("line.separator");
        _outputFileWriter.write(output);
      }


      img.decrementReferenceCount();
    }
    catch (Exception ex)
    {
      Expect.expect(false, ex.getMessage());
    }


  }

}
