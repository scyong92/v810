package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.ImageAcquisitionEngine;
import com.axi.v810.business.license.LicenseManager;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;
import com.axi.v810.datastore.config.*;


/**
 * This is a container class that will execute all three camera image quality
 * tasks (one per group).
 * @author Reid Hayhow
 */
public class ConfirmCameraLightAllGroups extends EventDrivenHardwareTask
{
  private static final String NAME =
      StringLocalizer.keyToString("CD_CAMERA_LIGHT_CONFIRMATION_KEY");

  private static AbstractXraySource _xraySourceInstance = AbstractXraySource.getInstance();

  private static Config _config = Config.getInstance();

  // read the frequency from the hardware.calib
  private static final int FREQUENCY = _config.getIntValue(HardwareCalibEnum.CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_FREQUENCY);

  // Read the timeout from the hardware.calib
  private static final long TIMEOUTINSECONDS =
      _config.getLongValue(SoftwareConfigEnum.CAMERA_LIGHT_IMAGE_ALL_GROUPS_CONFIRM_TIMEOUT_IN_SECONDS);

  private static ConfirmCameraLightAllGroups _instance = null;


  /**
   * Constructor for this confirmation task
   * @author Reid Hayhow
   */
  private ConfirmCameraLightAllGroups()
  {
    //Create it and specify the logging directory for the logger stored in the
    //super class
    super(NAME, FREQUENCY, TIMEOUTINSECONDS, ProgressReporterEnum.LIGHT_CAMERA_IMAGE_QUALITY_TASK);

    //Add all three camera light image tasks, hopefully in order to not cause
    //too much stage motion
    addSubTest(ConfirmCameraGroupOneLightImageTask.getInstance());
    addSubTest(ConfirmCameraGroupTwoLightImageTask.getInstance());
    addSubTest(ConfirmCameraGroupThreeLightImageTask.getInstance());

    //This confirmation is designed for automatic or manual execution, so choose
    //lowest common denominator
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    //Set the enum used to save the timestamp for the last time this task was run
    setTimestampEnum(HardwareCalibEnum.LIGHT_IMAGE_ALL_GROUPS_LAST_TIME_RUN);
  }

  /**
   * Instance access to this confirmation task
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmCameraLightAllGroups getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmCameraLightAllGroups();
    }
    return _instance;
  }


  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    boolean canRunOnThisHardware = true;

    //Iterate over the list of subtasks and see if they can run on this hardware
    List<HardwareTask> listOfSubTasks = getSubTests();

    for (HardwareTask task : listOfSubTasks)
    {
      canRunOnThisHardware &= task.canRunOnThisHardware();
    }

    //Return the boolean summation of what all tasks say about running
    return canRunOnThisHardware;
  }

  /**
   *
   * @author Reid Hayhow
   *
   */
  protected void cleanUp() throws XrayTesterException
  {
    if (XrayCylinderActuator.isInstalled() == true)
    {
      XrayActuator.getInstance().up(false);
    }
    else if (XrayCPMotorActuator.isInstalled() == true)
    {
      //move x-rays Z Axis to Home and Safe position.
      XrayActuator.getInstance().home(false,false);
    }
    //Since we turned xrays ON in startUp, make sure to turn them off on exit
    _xraySourceInstance.off();
  }

  /**
   * Create a Limits object specific to the particular test being run.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject()
  {
    _limits = new Limit(null, null, null, null);
  }

  /**
   * createResultsObject Create the results object for this test result.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    _result = new Result(new Boolean(false), NAME);
  }

  /**
   * This method should contain the task code.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    Assert.expect(false, "Shouldn't get called!");
  }

  /**
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    if(LicenseManager.isVariableMagnificationEnabled())
    {
        
        //move x-rays cylinder are down for high mag.    
        XrayActuator.getInstance().up(true);
        // re-calculate magnification 
        ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);
        if(_xraySourceInstance instanceof HTubeXraySource)
        {
          HTubeXraySource htube = (HTubeXraySource)_xraySourceInstance;
          htube.on();
        }
        else
        {
          _xraySourceInstance.on();
        }
    }
    //For this grouping of all image light confirms, we want xrays ON to
    //optimize through put
    _xraySourceInstance.on();
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
