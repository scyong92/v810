package com.axi.v810.business.autoCal;

import java.io.*;
import java.util.*;
import java.util.concurrent.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * The X-ray Spot Deflection Adjustment class. This adjustment allows users to
 * move the x-ray spot of the legacy x-ray source around to assure that the
 * spot is centered and evenly light. The high level concept is that images
 * are captured from all cameras at a regular interval while the field engineer
 * fiddles with the voltage that controls the X-Y axis of the X-ray source spot.
 * By looking at the area mode images, the field engineer is able to see the
 * edge of the x-ray spot (or collimator) as it drifts into the image.
 *
 * By doing this to both extrems in X and Y, then splitting the difference, the
 * field engineer can center the X-ray spot.
 *
 * This adjustment code is just responsible for capturing and saving the area mode
 * image from all cameras. An ImageJ macro will display and update the images for
 * the field engineer.
 *
 * @author Reid Hayhow
 */
public class CalXraySpotDeflection extends EventDrivenHardwareTask
{
  private static String _name =
      StringLocalizer.keyToString("CD_ADJUST_XRAY_SPOT_DEFLECTION_NAME_KEY");

  //This task should always run
  private static int _frequency = 1;

  //There is no timeout for this task, it is manual
  private static long _timeoutInSeconds = 0;

  private Config _config = Config.getInstance();

  //There should only be one logfile
  private int _maxLogFilesInDirectory = 1;

  //Directory Logger member
  private DirectoryLoggerAxi _logger;

  //Log directory where image files are saved
  private String _logDirectory = Directory.getXraySpotDeflectionCalLogDir();

  //The list of cameras to capture snapshot images from
  private List<AbstractXrayCamera> _cameras = new Vector<AbstractXrayCamera>();

  //Hardware.calib values
  private int _sleepIntervalInMillis = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_SLEEP_INTERVAL_IN_MILLIS);
  private int _numberOfTimesToLoopInsideTask = _config.getIntValue(SoftwareConfigEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_REPEAT_COUNT);

  //Parallel Thread Task executor to handle saving failed images in parallel
  private ExecuteParallelThreadTasks<Boolean> _executor = new ExecuteParallelThreadTasks<Boolean>();

  //Instance member
  private static CalXraySpotDeflection _instance;

  CalGrayscale _calGrayscale = CalGrayscale.getInstance();

  //Sim Mode values
  //The row that will be black, starts at 0 and is incremented and modulo'd
  private int _simModeBlackRowNumber = 0;
  private int _simModeImageWidth = 1088;
  private int _simModeImageHeight = 24;
  private float _simModeGrayValue = 200;
  private float _simModeBlackValue = 0;

  /**
   * CalXraySpotDeflection
   */
  private CalXraySpotDeflection()
  {
    super(_name, _frequency, _timeoutInSeconds, ProgressReporterEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_TASK);

    //Create the directory logger
    _logger = new DirectoryLoggerAxi(_logDirectory,
                                     FileName.getXraySpotDeflectionLogFileBasename(),
                                     FileName.getLogFileExtension(),
                                     _maxLogFilesInDirectory);

    //This confirmation is automated
    _taskType = HardwareTaskTypeEnum.MANUAL_CONFIRMATION_TASK_TYPE;

    //Add each camera to our interla list
    _cameras = XrayCameraArray.getCameras();

    //Set the timestamp enum for later use
    setTimestampEnum(HardwareCalibEnum.XRAY_SPOT_DEFLECTION_ADJUSTMENT_LAST_TIME_RUN);
  }

  /**
   * Instance access to this Adjustment
   *
   * @author Reid Hayhow
   */
  public static synchronized CalXraySpotDeflection getInstance()
  {
    if (_instance == null)
    {
      _instance = new CalXraySpotDeflection();
    }
    return _instance;
  }

  /**
   * This Adjustment can only run if the camera firmware allows area mode
   * images (AKA snap shot images).
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    return true;
  }

  /**
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    //None needed
  }

  /**
   * This Adjustment is a simple run/didn't run task. Use a boolean to emulate
   * this behavior.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject()
  {
    //Boolean limits are all null
    _limits = new Limit(null, null, null, null);
  }

  /**
   * This Adjustment is a simple run/didn't run task. Use a boolean to emulate
   * this behavior.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    //Assume the task didn't run/pass to begin with
    _result = new Result(new Boolean(false), _name);
  }

  /**
   * The guts of this Adjustment. Essentially loop a set number of times
   * (specified in the hardware.calib file) capturing snapshot images from all
   * cameras and sleep a set amount of time (again specified in the hardware.calib
   * file) after each capture.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
    // Value used to store and report our progress to the progress observable
    int percentDone = 0;

    // Report that we are starting this task
    reportProgress(percentDone);

    //The big outer loop controlled by hardware.calib value set in constructor
    for (int i = 0; i < _numberOfTimesToLoopInsideTask; i++)
    {
      //Create a new list of futures to track each task
      List<Future<Boolean>> listOfTasks = new ArrayList<Future<Boolean>>();

      //Handle a cancel gracefully. This will cause the task to fail
      if (_taskCancelled == true)
      {
        reportProgress(100);
        _result.setStatus(HardwareTaskStatusEnum.CANCELED);
        return;
      }

      // Area mode images purge the grayscale cal table, set Grayscale Cal to run ASAP
      // so that future image requests won't cause an exception
      _calGrayscale.setTaskToRunASAP();

      //capture an image from each camera
      for (AbstractXrayCamera camera : _cameras)
      {
        //Get the snapshot from this camera
        Image imageToSave = camera.getAreaModeImage();

        //if we are in sim mode, modify the image to make it nicer
        if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true)
        {
          imageToSave.decrementReferenceCount();
          imageToSave = generateSimModeImage();
        }

        //Create the thread task to save this specific image
        ImageSaverThreadTask saverThread = new ImageSaverThreadTask(imageToSave,
                                                                    _logDirectory +
                                                                    File.separator +
                                                                    "camera_" +
                                                                    camera.getId() +
                                                                    ".png");

        imageToSave.decrementReferenceCount();
        imageToSave = null;

        //Submit the thread task to the executor. We will check that all tasks
        //completed later
        Future<Boolean> task = _executor.submitThreadTask(saverThread);

        //Add this task to the list of tasks so we can track it later
        listOfTasks.add(task);
      }//end of for(AbstractXrayCamera camera...)

      //Ok, we have saved all the images for each camera, so now we can do any
      //special handling for sim mode
      if (_config.getBooleanValue(SoftwareConfigEnum.HARDWARE_SIMULATION) == true)
      {
        incrementSimModeBlackRow();
      }

      // Calculate how far along we are now and update the progress observable with our progress
      percentDone = (int)(100 * ((double)i/(double)_numberOfTimesToLoopInsideTask));
      reportProgress(percentDone);

      //sleep the specified timeout second
      try
      {
        Thread.currentThread().sleep(_sleepIntervalInMillis);
      }
      catch (InterruptedException ex)
      {
        //Do nothing and continue to next loop
      }

      //Wait for the tasks to complete
      try
      {
        _executor.waitForTasksToComplete();
      }
      //Handle a known exception by throwing it up the chain
      catch (XrayTesterException xrayTesterException)
      {
        throw xrayTesterException;
      }
      //Handle other exceptions
      catch (Exception otherException)
      {
        //set the status of the result
        _result.setStatus(HardwareTaskStatusEnum.EXCEPTION_ENCOUNTERED);

        //If the paralle thread task encountered an exception during execution
        //unwrap it
        if(otherException instanceof ExecutionException)
        {
          //Get the cause of the execution exception
          Throwable causeOfOtherException = otherException.getCause();

          //If it was an XrayTesterException, throw it along
          if(causeOfOtherException instanceof XrayTesterException)
          {
            throw (XrayTesterException)causeOfOtherException;
          }
          //Otherwise it was not expected, log it
          else
          {
            otherException.printStackTrace();
          }
        }
        //Not an execution exception, log it
        else
        {
          otherException.printStackTrace();
        }
      }// End of catch (ExecutionException ...)
    }// End of for(numberOfTimesToRun...) loop

    //If we get here, the task finished without exceptions and thus 'passed'
    _result = new Result(new Boolean(true), _name);

    // Report that we are done with this task
    reportProgress(100);
  }

  /**
   * Special handling for sim mode images. Create the image (until XrayCamera does
   * it right in sim-mode). Also make it gray and move a stripe down the image
   * one row at a time during execution.
   *
   * @author Reid Hayhow
   */
  private Image generateSimModeImage()
  {
    //First, create an image
    Image imageToSave = Image.createUnmonitoredFloatImage(_simModeImageWidth, _simModeImageHeight);

    int firstColumnInImage = 0;
    int lastColumnInImage = _simModeImageWidth - 1;

    //Then fill it with the correct gray value
    for (int rowInSimModeImage = 0; rowInSimModeImage < _simModeImageHeight; rowInSimModeImage++)
    {
      imageToSave.setPixelRowValues(firstColumnInImage, lastColumnInImage, rowInSimModeImage, _simModeGrayValue);
    }

    //finally, set one line with black
    imageToSave.setPixelRowValues(firstColumnInImage, lastColumnInImage, (_simModeBlackRowNumber % _simModeImageHeight), _simModeBlackValue);

    return imageToSave;
  }

  /**
   * Sim mode handling so I can create a stripe that moves down the image
   * one row at a time during sim mode execution.
   *
   * @author Reid Hayhow
   */
  private void incrementSimModeBlackRow()
  {
    //Increment the black row count. The Modulo above will handle trimming it
    //to the appropriate number of rows
    _simModeBlackRowNumber++;
  }

  /**
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    //Do nothing
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
}
