package com.axi.v810.business.autoCal;

import com.axi.util.Enum;

/**
 * @author Cheah Lee Herng
 */
public class PspModuleTestTypeEnum extends Enum
{
  private static int index = 0;
  static final PspModuleTestTypeEnum MEAN_ZHEIGHT_500_MICRON_IN_NANOMETERS    = new PspModuleTestTypeEnum(index++);
  static final PspModuleTestTypeEnum MEAN_ZHEIGHT_50_MICRON_IN_NANOMETERS     = new PspModuleTestTypeEnum(index++);
  static final PspModuleTestTypeEnum STD_DEV_ZHEIGHT_500_MICRON_IN_NANOMETERS = new PspModuleTestTypeEnum(index++);
  static final PspModuleTestTypeEnum STD_DEV_ZHEIGHT_50_MICRON_IN_NANOMETERS  = new PspModuleTestTypeEnum(index++);

  /**
   * CameraImageDataTypeEnum
   *
   * @author Reid Hayhow
   */
  protected PspModuleTestTypeEnum(int id)
  {
    super(id);
  }
}
