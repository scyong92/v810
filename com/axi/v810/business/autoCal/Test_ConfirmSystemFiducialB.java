package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.hardware.*;

/**
 * @author John Dutton
 */
public class Test_ConfirmSystemFiducialB extends UnitTest
{
  HardwareWorkerThread _hardwareWorkerThread = HardwareWorkerThread.getInstance();

  protected Test_ConfirmSystemFiducialB()
  {
    super();
  }

  /**
   * @author John Dutton
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ConfirmSystemFiducialB());
  }

  /**
   * @author John Dutton
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    // Create a test utils class and save original hardware calib file since last
    // timestamp will be written to it and the test harness expects no changes.
    TestUtils utils = new TestUtils();
    utils.backupHWCalib(getTestSourceDir());

    try
    {
      //------------------------------------------------------------------
      // Test Case #1: the pass case using good projections
      //------------------------------------------------------------------

      // Get an instance of the test to run
      ConfirmSystemFiducialB confirm = ConfirmSystemFiducialB.getInstance();
      confirm.setReadImageFromFile(true);
    }
    catch (Exception e)
    {
      e.printStackTrace(os);
    }

    finally
    {
      //Use the utils to copy the HW calib back.
      utils.restoreHWCalib(getTestSourceDir());
    }

  }
}
