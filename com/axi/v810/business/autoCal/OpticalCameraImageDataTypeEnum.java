package com.axi.v810.business.autoCal;

import com.axi.util.Enum;

/**
 * @author Cheah Lee Herng
 */
public class OpticalCameraImageDataTypeEnum extends Enum 
{
    private static int index = 0;
    static final OpticalCameraImageDataTypeEnum MEAN_PIXEL_GRAY = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum PIXEL_GRAY_STANDARD_DEVIATION = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum REFERENCE_PLANE_DIAMETER = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_MINUS_TWO_DIAMETER = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_PLUS_THREE_DIAMETER = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_MINUS_ONE_DIAMETER = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_PLUS_FOUR_DIAMETER = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum REFERENCE_PLANE_MAGNIFICATION = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_MINUS_TWO_MAGNIFICATION = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_PLUS_THREE_MAGNIFICATION = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_MINUS_ONE_MAGNIFICATION = new OpticalCameraImageDataTypeEnum(index++);
    static final OpticalCameraImageDataTypeEnum OBJECT_PLANE_PLUS_FOUR_MAGNIFICATION = new OpticalCameraImageDataTypeEnum(index++);
    
    /**
     * @author Cheah Lee Herng 
     */
    protected OpticalCameraImageDataTypeEnum(int id)
    {
        super(id);
    }
}
