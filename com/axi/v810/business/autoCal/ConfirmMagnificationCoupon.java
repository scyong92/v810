package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.ImageAcquisitionEngine;
import com.axi.v810.business.imageAcquisition.imageReconstruction.communication.*;
import com.axi.v810.business.panelSettings.MagnificationTypeEnum;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

/**
 * Confirmation that measures the optical quality of the magnification coupon by measuring
 * the MTF of its diagonal edge by using the FWHM of its line spread function
 * as a proxy for MTF.  It also tests panel positioning by measuring the location
 * of its edges.   MTF is an optical measurement of how well an imaging system captures
 * contrasts between black and white lines.  The smaller the number, the better.
 *
 * The magnification coupon is on the opposite end of the fixed rail from the system
 * fiducial coupon.
 *
 * @author John Dutton
 */

public class ConfirmMagnificationCoupon extends ConfirmSharpness
{
  // Singleton class.
  private static ConfirmMagnificationCoupon _instance;

  // Printable name for this confirmation.
  private static final String _NAME_OF_CAL =
      StringLocalizer.keyToString("CD_CONFIRM_MAGNIFICATION_COUPON_KEY");

  private static final int _FREQUENCY =
      _config.getIntValue(HardwareCalibEnum.SHARPNESS_CONFIRM_FREQUENCY);
  private static final long _TIMEOUTINSECONDS =
      _config.getIntValue(SoftwareConfigEnum.CONFIRM_MAG_COUPON_TIMEOUT_IN_SECONDS);

  private static final int _DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS =
      _config.getIntValue(HardwareCalibEnum.DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS);
  private static final int _DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS =
      _config.getIntValue(HardwareCalibEnum.DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS);
  private static final int _REGION_TO_IMAGE_WIDTH_NANOMETERS;
  private static final int _REGION_TO_IMAGE_HEIGHT_NANOMETERS;

  // Margins to use for requesting projections:
  private static final int _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS =
    _config.getIntValue(SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS);
  private static final int _MARGIN_FOR_FOCUS_REGION_NANOMETERS =
    _config.getIntValue(SystemConfigEnum.CONFIRM_MAG_COUPON_MARGIN_FOR_FOCUS_REGION_NANOMETERS);

  private static final int _KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH =
    _config.getIntValue(SystemConfigEnum.CONFIRM_MAG_COUPON_KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH);

  private static final int _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_MAG_COUPON_CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS);
  private static final int _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_MAG_COUPON_CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_LEFT_HEIGHT_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_MAG_COUPON_CROPPED_VERTICAL_EDGE_LEFT_HEIGHT_PIXELS);
  private static final int _CROPPED_VERTICAL_EDGE_LEFT_WIDTH_PIXELS =
    _config.getIntValue(SoftwareConfigEnum.CONFIRM_MAG_COUPON_CROPPED_VERTICAL_EDGE_LEFT_WIDTH_PIXELS);

  // All below values should comes from SystemFiducialCoupon singleton, so that we can switch between normal and low mag easily in future.
  // System Fiducial Triangle on System Coupon is (1.27 mm x 2.54 mm x hypotenuse) right triangle.
//  private static final int _SYSFID_TRIANGLE_WIDTH_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_X_SIZE_IN_NANOMETERS);
//  private static final int _SYSFID_TRIANGLE_HEIGHT_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSTEM_FIDUCIAL_COUPON_Y_SIZE_IN_NANOMETERS);

  // The following coordinates are based on the CAD drawing of the System Coupon.
//  private static final int _SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_X_NANOMETERS);
//  private static final int _SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_FIDUCIAL_POINT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_LEFT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_UPPER_RIGHT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_LEFT_Y_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_X_NANOMETERS);
//  private static final int _SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_QUADRILATERAL_LOWER_RIGHT_Y_NANOMETERS);
//  private static final int _SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
//     _config.getIntValue(HardwareCalibEnum.SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS);

  // The bottom of the system coupon is not imagable when it is in the mag coupon holder,
  // so limit how far down from sys fid to image.
  private static final int CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS =
     _config.getIntValue(SystemConfigEnum.CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS);

  // The image is flipped in X because of its orientation in the pocket.  Hence,
  // the upper right of quadrilateral is the starting point and the order is counterclockwise
  // from there.
  private static List<IntCoordinate> _quadPointsInNanometers = new ArrayList<IntCoordinate>();

  // Once LP1, LP2 & LP4 range of motion errors are resolved this can go away.
  private static final boolean _STAGE_MOTION_RANGE_OK_FOR_MAG_COUPON_CONFIRMATION =
    _config.getBooleanValue(HardwareConfigEnum.STAGE_MOTION_RANGE_OK_FOR_MAG_COUPON_CONFIRMATION);

  // Limits to compare to.
  private double _lineSpreadFWHMPixelsHighLimit;
  private double _lineSpreadFWHMPixelsLowLimit;

  // Number of horizontal/vertical edges to average together
  private final int _noHorizontalEdges = 2;
  private final int _noVerticalEdges = 1;

  private static SystemFiducialCoupon _systemCoupon = SystemFiducialCoupon.getInstance();

  /**
   * @author John Dutton
   */
  static
  {
    _REGION_TO_IMAGE_WIDTH_NANOMETERS  = _config.getIntValue(SoftwareConfigEnum.CONFIRM_MAG_COUPON_REGION_TO_IMAGE_WIDTH_NANOMETERS);
    _REGION_TO_IMAGE_HEIGHT_NANOMETERS = _config.getIntValue(SoftwareConfigEnum.CONFIRM_MAG_COUPON_REGION_TO_IMAGE_HEIGHT_NANOMETERS);    // Verify that region to image is not bigger than allowed.

    Assert.expect(_REGION_TO_IMAGE_WIDTH_NANOMETERS <= SetHardwareConstants.getMaxHardwareReconstructionRegionWidthInNanoMeters());
    Assert.expect(_REGION_TO_IMAGE_HEIGHT_NANOMETERS <= SetHardwareConstants.getMaxHardwareReconstructionRegionLengthInNanoMeters());

    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralUpperRightXinNanoMeters(),
                          _systemCoupon.getQuadrilateralUpperRightYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralUpperLeftXinNanoMeters(),
                          _systemCoupon.getQuadrilateralUpperLeftYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralLowerLeftXinNanoMeters(),
                          _systemCoupon.getQuadrilateralLowerLeftYinNanoMeters()));
    _quadPointsInNanometers.add(
        new IntCoordinate(_systemCoupon.getQuadrilateralLowerRightXinNanoMeters(),
                          _systemCoupon.getQuadrilateralLowerRightYinNanoMeters()));
  }

  /**
   * @author John Dutton
   */
  private ConfirmMagnificationCoupon()
  {
    super(_NAME_OF_CAL,
          _FREQUENCY,
          _TIMEOUTINSECONDS,
          Directory.getSystemImageQualityConfirmationLogDir(),
          FileName.getSystemImageQualityConfirmationLogFileName(),
          ProgressReporterEnum.CONFIRM_MAGNIFICATION_COUPON_TASK);

    //This confirmation is to be run manually only
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    setTimestampEnum(HardwareCalibEnum.CONFIRM_MAG_COUPON_LAST_TIME_RUN);

  }

  /**
   * A singleton class.
   *
   * @author John Dutton
   */
  public static synchronized ConfirmMagnificationCoupon getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmMagnificationCoupon();
    }
    return _instance;
  }

  /**
   * If machine is LP1 or LP2 can't run this confirmation because of motion in x limitation.
   *
   * @author John Dutton
   */
  public boolean canRunOnThisHardware()
  {
    return  _STAGE_MOTION_RANGE_OK_FOR_MAG_COUPON_CONFIRMATION;
  }

  /**
   * @author John Dutton
   */
  private static int getDistanceInXFromSysFidToLeftEdgeOfImageNanometers()
  {
    // The right angle point of the system fiducial is at x 12.66 mm in CAD drawing.
    // The farthest relevant point to the right is the upper right point on the quadrilateral
    // at x 5.42 mm.  The coupon and hence the CAD drawing is flipped in the coupon holder
    // that is going to be imaged (i.e. flipped in X along Y).  So to have the left side of
    // the image include the upper right point, use the difference to define the left side of
    // the image, along with a little margin.  With this definition for the image left side,
    // the opposite upper point on the quadrilateral will fit in the right side of the image
    // with some margin remaining.

    int distanceBetweenSysFidToUpperRightNanometers =
        Math.abs(_systemCoupon.getQuadrilateralUpperRightXinNanometersInSystemFiducialCoordinates());
    return distanceBetweenSysFidToUpperRightNanometers +
           _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS;
  }

/**
 * @author John Dutton
 */
  private static int getDistanceInYFromSysFidToLowerRightNanometers()
  {
    // The right angle point of the system fiducial is at y 11.05 mm in CAD drawing.
    // The furthest dot below this point (marking visible region) is at 11.05 mm + 3.50 mm.
    // (It is less than other sharpness confirmations because of stage travel limitations.)
    // To have bottom of projection image be at the dot, use the difference to define the
    // bottom of the image.

    return Math.abs(CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS -
                    _systemCoupon.getSystemFiducialPointYinNanoMeters());
  }

  /**
   * Calculates where the system fiducial is expected to be based on CAD values of the stage.
   * The tolerance stack will cause the actual to be different, hence a template match
   * will be performed to find the actual.
   *
   * @author John Dutton
   */
  private ImageCoordinate getPredictedSysFidPixel() throws DatastoreException
  {
    int actualPixelSizeXInNanometers = actualPixelSizeXInNanometers();
    int actualPixelSizeYInNanometers = actualPixelSizeYInNanometers();

    // System Fiducial location for use below with template match results.
    int predictedSysFidPixelX =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(getDistanceInXFromSysFidToLeftEdgeOfImageNanometers()),
                                                    actualPixelSizeXInNanometers);
    int distanceInPixelsFromImageBottomToSysFidY =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(getDistanceInYFromSysFidToLowerRightNanometers()),
                                                    actualPixelSizeYInNanometers);
    int imgHeight =
        MathUtil.convertNanoMetersToPixelsUsingCeil(_REGION_TO_IMAGE_HEIGHT_NANOMETERS,
                                                    actualPixelSizeYInNanometers);
    int predictedSysFidPixelY = imgHeight - distanceInPixelsFromImageBottomToSysFidY;
    return new ImageCoordinate(predictedSysFidPixelX, predictedSysFidPixelY);
  }


  /**
   * Calculates the region to be reconstructed, based on coordinates relative to the
   * system fiducial.
   *
   * @author John Dutton
   */
  public static PanelRectangle getImageRectangle()
  {
    int reconstructedRegionXInNanometers = -(_DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                             getDistanceInXFromSysFidToLeftEdgeOfImageNanometers());

    int reconstructedRegionYInNanometers = -(_DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                             getDistanceInYFromSysFidToLowerRightNanometers());

    PanelRectangle magCouponArea =
        new PanelRectangle(reconstructedRegionXInNanometers, reconstructedRegionYInNanometers,
                           _REGION_TO_IMAGE_WIDTH_NANOMETERS,
                           _REGION_TO_IMAGE_HEIGHT_NANOMETERS);

    return magCouponArea;
  }

  /**
   * Calculates where to focus within the reconstruction region.  Coordinates are
   * relative to system fiducial.
   *
   * @author John Dutton
   */
  public static PanelRectangle getFocusRectangle()
  {
    // Need to focus on smaller region because of IRP limit.  Use area around system fid.
    int focusRegionXInNanometers = -(_DISTANCE_IN_X_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                     _MARGIN_FOR_FOCUS_REGION_NANOMETERS);
    int focusRegionYInNanometers = -(_DISTANCE_IN_Y_BETWEEN_FIDUCIAL_POINTS_NANOMETERS +
                                     _MARGIN_FOR_FOCUS_REGION_NANOMETERS);
    int focusRegionWidthNanometers = _MARGIN_FOR_FOCUS_REGION_NANOMETERS +
                                     2 * _systemCoupon.getTriangleWidthInNanoMeters();
    int focusRegionHeightNanometers = _MARGIN_FOR_FOCUS_REGION_NANOMETERS +
                                      2 * _systemCoupon.getTriangleHeightInNanoMeters();

    // Verify focus region is no bigger than maximum supported on the IRP's.
    Assert.expect(focusRegionWidthNanometers <= SetHardwareConstants.getMaxFocusRegionWidthInNanoMeters());
    Assert.expect(focusRegionHeightNanometers <= SetHardwareConstants.getMaxFocusRegionLengthInNanoMeters());


    PanelRectangle focusRectangle =
        new PanelRectangle(focusRegionXInNanometers, focusRegionYInNanometers,
                           focusRegionWidthNanometers, focusRegionHeightNanometers);

    Assert.expect(getImageRectangle().contains(focusRectangle), "focus region is not contained within reconstructed image");

    return focusRectangle;
  }


  /**
   * Get reconstructed image, perform a template match and then hand image and
   * system fiducial location off for processing.
   *
   * @author John Dutton
   */
  public void doTheRealConfirmationWork() throws XrayTesterException
  {
    //XCR-3797, Failed to cancel CDNA task after start up
    if (_taskCancelled == true)
    {
      reportProgress(100);
      _result.setStatus(HardwareTaskStatusEnum.CANCELED);
      return;
    }
	//Swee Yee Wong - Camera debugging purpose
    debug("confirmSystemImageQuality");
    // Temporarily disable this test.
    //
    //if (true)
    //return;
    ImageAcquisitionEngine.changeMagnification(MagnificationTypeEnum.LOW);

    _lineSpreadFWHMPixelsHighLimit =_parsedLimits.getHighLimitDouble("LineSpreadFWHMPixels");
    _lineSpreadFWHMPixelsLowLimit = _parsedLimits.getLowLimitDouble("LineSpreadFWHMPixels");

    //Start the progress observable so users know what is happening
    initializeAndReportProgressPercentage();

    int actualPixelSizeXInNanometers = actualPixelSizeXInNanometers();
    int actualPixelSizeYInNanometers = actualPixelSizeYInNanometers();

    increaseAndReportProgressPercentage(10);
    PanelRectangle imageRectangle =  getImageRectangle();
    PanelRectangle focusRectangle =  getFocusRectangle();

    // Get reconstructed image.  Search at reference plane (i.e. 0) to 330 mils below
    // the reference plane.  This is the range Magnification Adjustment uses.
    Image image = null;
    if (readImageFromFileTrue())
      image = readImageFromFile("C:/temp", "testMagCoup.png");
    else
    {
      ImageRetriever ir = ImageRetriever.getInstance();
      if(XrayTester.isXXLBased())
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.FIXED_RAIL_COUPON_IN_MAGNIFICATION_POCKET,
                          MathUtil.convertMilsToNanoMeters(100),
                          MathUtil.convertMilsToNanoMeters(475));
      }
      else if(XrayTester.isS2EXEnabled())
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.FIXED_RAIL_COUPON_IN_MAGNIFICATION_POCKET,
                          MathUtil.convertMilsToNanoMeters(100),
                          MathUtil.convertMilsToNanoMeters(375));
      }
      else //Throughput or Standard
      {
        image = ir.getImage(ImageRetrieverTestProgramEnum.FIXED_RAIL_COUPON_IN_MAGNIFICATION_POCKET,
                            MathUtil.convertMilsToNanoMeters(25),
                            MathUtil.convertMilsToNanoMeters(170));
      }
    }

    Assert.expect(image != null, "Didn't get an image so can't cut up edges.");
    saveIfSavingDebugImages(image, "reconstruction_img");

    increaseAndReportProgressPercentage(40);   // Show 50%


    // Save focus image
    if (saveDebugImages())
    {
      int focusRegionHeightInPixels = MathUtil.convertNanoMetersToPixelsUsingCeil(focusRectangle.getHeight(),
          actualPixelSizeYInNanometers);
      int focusRegionWidthInPixels = MathUtil.convertNanoMetersToPixelsUsingCeil(focusRectangle.getWidth(),
          actualPixelSizeXInNanometers);
      int focusRegionXInPixels =
          MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(imageRectangle.getMinX() -
          focusRectangle.getMinX()),
                                                      actualPixelSizeXInNanometers);
      int distanceInPixelsFromImageBottomToBottomOfFocusRegion =
          MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(imageRectangle.getMinY() -
          focusRectangle.getMinY()),
                                                      actualPixelSizeYInNanometers);
      int bottomOfFocusRegionInPixels = image.getHeight() - distanceInPixelsFromImageBottomToBottomOfFocusRegion;
      int focusRegionYInPixels = bottomOfFocusRegionInPixels - focusRegionHeightInPixels;
      RegionOfInterest focusROI =
          new RegionOfInterest(focusRegionXInPixels, focusRegionYInPixels, focusRegionWidthInPixels,
                               focusRegionHeightInPixels, 0, RegionShapeEnum.RECTANGULAR);

      //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
      checkIsRoiFitsWithinImage(focusROI, image);
      Image focusImg = Image.createCopy(image, focusROI);
      saveIfSavingDebugImages(focusImg, "focus_img");
      focusImg.decrementReferenceCount();
    }


    // Use template match to calculate offset from system fiducial point, i.e. by the code's
    // calculations it should be at x, y, but where is it actually in the image.  A lot of tolerances
    // stack up to determine this.
//    int chopYPixelsOffReconstructedImage =
//        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(getDistanceInYFromSysFidToLowerRightNanometers() -
//                                                             _KEEP_Y_NANOMETERS_BELOW_SYSTEM_FIDUCIAL_FOR_TEMPLATE_MATCH),
//                                                    actualPixelSizeYInNanometers);
    int chopYPixelsOffReconstructedImage = 0;

    RegionOfInterest croppedROI =
      new RegionOfInterest(0, 0, image.getWidth(), image.getHeight() - chopYPixelsOffReconstructedImage,
                           0, RegionShapeEnum.RECTANGULAR);

    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(croppedROI, image);
    Image croppedReconstructedImage = Image.createCopy(image, croppedROI);
    saveIfSavingDebugImages(croppedReconstructedImage, "cropped_reconstructed_img");
    if (saveDebugImages())
    {
      Image template = SystemFiducialTemplateMatch.getTemplate(true, false);
      saveIfSavingDebugImages(template, "template_img");
      template.decrementReferenceCount();
    }
    ImageCoordinate sysFidPointLocation = SystemFiducialTemplateMatch.matchTemplate(croppedReconstructedImage, true, false);

    if (sysFidPointLocation.getX() == -1)
    {
      // Can't find system fiducial, so can't continue.   Fail confirmation and throw exception.
      setResultsToFail();
      throw
          new HardwareTaskExecutionException(
              ConfirmationExceptionEnum.CONFIRM_SHARPNESS_FAILED_TEMPLATE_MATCH);
    }


    increaseAndReportProgressPercentage(10);   // Show 60%

    SystemTypeEnum systemType = XrayTester.getSystemType();
    if (systemType.equals(SystemTypeEnum.STANDARD))
    {
    ImageCoordinate predictedSysFidPixel = getPredictedSysFidPixel();
    processImage(image, sysFidPointLocation.getX() - predictedSysFidPixel.getX(),
                        sysFidPointLocation.getY() - predictedSysFidPixel.getY());
    }
    else if (systemType.equals(SystemTypeEnum.THROUGHPUT) || systemType.equals(SystemTypeEnum.XXL) || systemType.equals(SystemTypeEnum.S2EX))
    {
        processRightTriangleImage(croppedReconstructedImage, sysFidPointLocation);
    }
    else
        Assert.expect(false, "unkown system type: " + systemType.getName());


    croppedReconstructedImage.decrementReferenceCount();

    increaseAndReportProgressPercentage(30);   // Show 90%

    setResultMessages();

    if (didConfirmationPass() == false)
      saveImageOnConfirmationFailure(image, "reconstruction_img");

    image.decrementReferenceCount();

    addMagnificationInfo(MagnificationEnum.NOMINAL); // Khang Wah, add mgnification info into CD&A log
    reportProgressPercentageDone();
  }


  /**
 * @author YingKat Choor
 * Use right triangle to preform MTF
 * cropped edges of the right triangle (slanted edge)
 * calculate MTF
 */
void processRightTriangleImage(Image croppedReconstructedImage, ImageCoordinate sysFidPointLocation)
    throws DatastoreException, HardwareTaskExecutionException

  {
    //ROI size suggestion by Tracy
    int croppedImageTopLeftXFromFiducialInPixel = 5;
    int croppedImageTopLeftYFromFiducialInPixel = 150;
    int slantedLineWidthInPixel = 46;
    int slantedLineHeightInPixel = 146;
    
    //Ngie Xing, will need to adjust the ROI according to the magnification
    if (XrayTester.isLowMagnification() && 
        _config.getSystemCurrentLowMagnification().equalsIgnoreCase("M23"))
    {
      double ratio = 19.0 / 23.0;
      slantedLineWidthInPixel = (int) Math.floor((double) slantedLineWidthInPixel * ratio);
      slantedLineHeightInPixel = (int) Math.floor((double) slantedLineHeightInPixel * ratio);
      croppedImageTopLeftYFromFiducialInPixel -= (146 - slantedLineHeightInPixel);
      
      System.out.println("slantedLineWidthInPixel " + slantedLineWidthInPixel);
      System.out.println("slantedLineHeightInPixel " + slantedLineHeightInPixel);
      System.out.println("croppedImageTopLeftYFromFiducialInPixel " + croppedImageTopLeftYFromFiducialInPixel);
    }
    
    RegionOfInterest roiForTriangleslantedEdge = new RegionOfInterest(sysFidPointLocation.getX() + croppedImageTopLeftXFromFiducialInPixel, 
                                                                      sysFidPointLocation.getY() - croppedImageTopLeftYFromFiducialInPixel,
                                                                      slantedLineWidthInPixel,
                                                                      slantedLineHeightInPixel, 0, RegionShapeEnum.RECTANGULAR);
    //Kok Chun, Tan - XCR3415 - throw error when unable to fit (the templete is match at wrong position)
    checkIsRoiFitsWithinImage(roiForTriangleslantedEdge, croppedReconstructedImage);

  Image croppedTriangleSlantedEdge = Image.createCopy(croppedReconstructedImage, roiForTriangleslantedEdge);
  saveIfSavingDebugImages(croppedTriangleSlantedEdge, "cropped_TriangleSlantedEdge_img_" + sysFidPointLocation.getX() + "_" + sysFidPointLocation.getY());

  calculateMTFAndAddResult(croppedTriangleSlantedEdge, true, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "Vertical edge, slanted");

  calculateMeanAndAddResult(0, _noVerticalEdges, "vertical edges mean");

  // CR1046 fix by LeeHerng - Dereference image
  croppedTriangleSlantedEdge.decrementReferenceCount();
}


  /**
   * @author John Dutton
   */
  void processImage(Image img, int sysFidPixelOffsetX, int sysFidPixelOffsetY)
      throws DatastoreException, HardwareTaskExecutionException
  {
    int farthestLeftVisibleXInNanometers = _systemCoupon.getQuadrilateralUpperRightXinNanoMeters() - _MARGIN_IN_X_FROM_QUADRILATERAL_NANOMETERS;
    int farthestBottomVisibleYInNanometers = CONFIRM_MAG_COUPON_SYSCOUP_LOWER_RIGHT_VISIBLE_DOT_Y_NANOMETERS;


    List<ImageCoordinate> listOfImageCoordinates =
          getQuadImageCoordinates(_quadPointsInNanometers,
                                  farthestLeftVisibleXInNanometers,
                                  farthestBottomVisibleYInNanometers,
                                  img.getHeight(),
                                  sysFidPixelOffsetX,
                                  sysFidPixelOffsetY);

    // Process upper horizontal edge in 3 parts.
    ImageCoordinate upperLeftPixel = listOfImageCoordinates.get(0);

    // Need to synthesize upper right pixel since it was not captured in the actual image.
    // Use right side of actual image as X and use point-slope equation to calculate Y.
    double slopeUpperEdge = (_quadPointsInNanometers.get(1).getY() - _quadPointsInNanometers.get(0).getY()) /
                   (double)(_quadPointsInNanometers.get(1).getX() - _quadPointsInNanometers.get(0).getX());
    int farthestRightVisibleXInNanometers = farthestLeftVisibleXInNanometers + _REGION_TO_IMAGE_WIDTH_NANOMETERS;
    int synthesizedUpperRightYInNanometers = (int)
                                             (slopeUpperEdge * (farthestRightVisibleXInNanometers - _quadPointsInNanometers.get(0).getX())
                                              + _quadPointsInNanometers.get(0).getY());
    int distanceInPixelsYBetweenUpperLeftAndSynUpperRight =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(_quadPointsInNanometers.get(0).getY() -
        synthesizedUpperRightYInNanometers),
                                                    actualPixelSizeYInNanometers());
    int synthesizedUpperRightPixelY = upperLeftPixel.getY() + distanceInPixelsYBetweenUpperLeftAndSynUpperRight;
    int synthesizedUpperRightPixelX = img.getWidth() - 1;
    ImageCoordinate synthesizedUpperRightPixel =
        new ImageCoordinate(synthesizedUpperRightPixelX, synthesizedUpperRightPixelY);

    List<Image> listOfImages =
        getImagesOfHorizontalEdge(img, 2, upperLeftPixel, synthesizedUpperRightPixel, _CROPPED_HORIZONTAL_EDGE_WIDTH_PIXELS, _CROPPED_HORIZONTAL_EDGE_HEIGHT_PIXELS);
    calculateMTFAndAddResult(listOfImages.get(0), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, left");
    calculateMTFAndAddResult(listOfImages.get(1), false, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "top line, center");

    for (Image image : listOfImages)
      image.decrementReferenceCount();
    listOfImages.clear();

/*
 Can't process lower horizontal edge because it is not imagable in mag coupon location!
 */

    // Process left vertical edge.

    // Need to synthesize lower left pixel since it was not captured in the actual image and the
    // one in listOfImageCoordinates is not in the actual image.
    // Use bottom of actual image as Y and use point-slope equation to calculate X.
    double slopeLeftEdge = (_quadPointsInNanometers.get(3).getY() - _quadPointsInNanometers.get(0).getY()) /
                   (double)(_quadPointsInNanometers.get(3).getX() - _quadPointsInNanometers.get(0).getX());
    int synthesizedLowerLeftXInNanometers =
       (int)(((farthestBottomVisibleYInNanometers - _quadPointsInNanometers.get(0).getY()) / slopeLeftEdge)
             + _quadPointsInNanometers.get(0).getX());
    int distanceInPixelsXBetweenUpperLeftAndSynLowerLeft =
        MathUtil.convertNanoMetersToPixelsUsingCeil(Math.abs(_quadPointsInNanometers.get(0).getX()  -
                                                             synthesizedLowerLeftXInNanometers),
                                                    actualPixelSizeXInNanometers());
    int synthesizedLowerLeftPixelX = upperLeftPixel.getX() + distanceInPixelsXBetweenUpperLeftAndSynLowerLeft;
    int synthesizedLowerLeftPixelY = img.getHeight() - 1;
    ImageCoordinate synthesizedLowerLeftPixel =
        new ImageCoordinate(synthesizedLowerLeftPixelX, synthesizedLowerLeftPixelY);


    Image verticalEdgeImg =
        getImageOfVerticalEdge(img, upperLeftPixel, synthesizedLowerLeftPixel,
                               _CROPPED_VERTICAL_EDGE_LEFT_WIDTH_PIXELS,
                               _CROPPED_VERTICAL_EDGE_LEFT_HEIGHT_PIXELS);
    calculateMTFAndAddResult(verticalEdgeImg, true, _lineSpreadFWHMPixelsLowLimit, _lineSpreadFWHMPixelsHighLimit, "vertical edge, left");

    verticalEdgeImg.decrementReferenceCount();

    // Process system fiducial

    calculateMeanAndAddResult(0, _noHorizontalEdges, "horizontal edges mean");


  }

  /**
   * Set messages in the Result object. This info is available to any observer
   * who wants to display detailed failure information.
   *
   * @author John Dutton
   */
  private void setResultMessages()
  {
    // Set only if confirmation failed, otherwise let them default to empty strings.
    if(didConfirmationPass() == false)
    {
      _result.setTaskStatusMessage(StringLocalizer.keyToString("CD_CONFIRM_MAG_COUPON_FAILED_STATUS_KEY"));
      _result.setSuggestedUserActionMessage(StringLocalizer.keyToString("CD_CONFIRM_MAG_COUPON_FAILED_ACTION_KEY"));
    }
  }

//  /**
//   * @author Anthony Fong - 
//   *  Do not override this function, 
//   *   the parent class ConfirmSharpness will take care of this
//   */   
//  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
//  {
//    return false;
//  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
