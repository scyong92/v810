package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Common logger for all of the adjustments. Each adjustment task
 * should call this before exiting to log the status of the run.
 * @author Eddie Williamson
 */
public class AdjustmentsAndConfirmationsLogger extends FileLoggerAxi
{
  private static AdjustmentsAndConfirmationsLogger _instance = null;
  private static final int _TWENTY_MEGABYTES = 20*1024*1024;
  private static final String _FILENAME_FULLPATH = FileName.getAdjustmentsAndConfirmationsLogFullPath();



  /**
   * @author Eddie Williamson
   */
  private AdjustmentsAndConfirmationsLogger()
  {
    super(_FILENAME_FULLPATH, _TWENTY_MEGABYTES);
  }


  /**
   * @author Eddie Williamson
   */
  public static synchronized AdjustmentsAndConfirmationsLogger getInstance()
  {
    if (_instance == null)
    {
      _instance = new AdjustmentsAndConfirmationsLogger();
    }
    return _instance;
  }


  /**
   * @author Eddie Williamson
   */
  public synchronized void append(String logString) throws DatastoreException
  {
    super.append(logString);
  }

}
