package com.axi.v810.business.autoCal;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Eddie Williamson
 */
public class Test_CalXraySpotCoordinateEventDrivenHardwareTask3 extends UnitTest
{
  // These are the expected offsets from the original to the new xray spot location.
  // These have been validated to be correct for the 3 test images used.
  private static final int EXPECTED_X_OFFSET_TCASE_3 = 0;
  private static final int EXPECTED_Y_OFFSET_TCASE_3 = 0;

  private static final String backupFileName = Directory.getCalibDir() + "backupHwCalibFile";


  private Config _config = Config.getInstance();

  /**
   * @author Eddie Williamson
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_CalXraySpotCoordinateEventDrivenHardwareTask3());
  }

  /**
   * @author Roy Williams
   */
  protected Test_CalXraySpotCoordinateEventDrivenHardwareTask3()
  {
    super();
  }

  /**
   * @param is Input stream
   * @param os Output stream
   * @author Eddie Williamson
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    XrayCameraArray xRayCameraArray = null;

    try
    {
      // save backup copy of hardware.calib
      FileUtil.copy(FileName.getHardwareCalibFullPath() + FileName.getBinaryExtension(), backupFileName);

      _config.loadIfNecessary();

      XrayTester.getInstance().startup();
      String imageDataTestDirectory = getTestDataDir() + Directory.getXraySpotDir() + "3";
      xRayCameraArray = XrayCameraArray.getInstance();
      xRayCameraArray.enableUnitTestMode(imageDataTestDirectory);
      CalGrayscale.getInstance().executeUnconditionally();

      // get original values for stage X and Y for later comparison
      int origStageX = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
      int origStageY = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);

      //------------------------------------------------------------------
      // Test Case #3
      //------------------------------------------------------------------

      XrayCameraArray.getInstance().refresh();
      CalibrationSupportUtil.delayForLogFileTimestamp();

      // get original values for stage X and Y for later comparison
      origStageX = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
      origStageY = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);

      // Run the actual cal procedure
      // The camera simulators will get the next test image
      CalXraySpotCoordinateEventDrivenHardwareTask cal = CalXraySpotCoordinateEventDrivenHardwareTask.getInstance();
      cal.executeNonInteractively();

      // get new values for stage X and Y for comparison
      int newStageX = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_X_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);
      int newStageY = _config.getIntValue(HardwareCalibEnum.XRAY_SPOT_Y_POSITION_IN_MACHINE_COORDINATES_NANOMETERS);

      // calculate the offset (change) between orig and new values
      int offsetXnm = newStageX - origStageX;
      int offsetYnm = newStageY - origStageY;

      // compare the cal'ed offset to the known offset from the test image
      Expect.expect(offsetXnm == EXPECTED_X_OFFSET_TCASE_3, offsetXnm + "!=" + EXPECTED_X_OFFSET_TCASE_3);
      Expect.expect(offsetYnm == EXPECTED_Y_OFFSET_TCASE_3, offsetYnm + "!=" + EXPECTED_Y_OFFSET_TCASE_3);

      // Verify the test correctly failed.
      // The above expect's check for zero offset to verify that the fiducial
      // was missing for this test case. The image used is blank. However, there
      // is a slim possibility that zero offset is due to finding a fiducial at
      // the nominal location in which case the test would incorrectly pass.
      Expect.expect(cal.didTestPass() == false);
    }
    catch (XrayTesterException ex)
    {
      ex.printStackTrace();

      // Shouldn't occur.
      Assert.expect(false);
    }
    catch (Exception e)
    {
      e.printStackTrace();
      Expect.expect(false, e.getMessage());
    }
    finally
    {
      xRayCameraArray.disableUnitTestMode();
      restoreOriginalCalibFromBackup();
    }
  }


  /**
   * @author Eddie Williamson
   */
  private void restoreOriginalCalibFromBackup()
  {
    try
    {
      // restore original hardware.config
      FileUtil.copy(backupFileName, FileName.getHardwareCalibFullPath() + FileName.getBinaryExtension());
    }
    catch (CouldNotCopyFileException ex1)
    {
      Expect.expect(false, "Could not copy backup file to hardware.calib.");
    }
    catch (FileDoesNotExistException ex1)
    {
      Expect.expect(false, "Backup file does not exist for copy.");
    }
  }

}
