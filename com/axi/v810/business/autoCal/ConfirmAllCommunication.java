package com.axi.v810.business.autoCal;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.hardware.autoCal.*;
import com.axi.v810.util.*;

public class ConfirmAllCommunication extends EventDrivenHardwareTask
{
  private static String _NAME = StringLocalizer.keyToString("CD_CONFIRM_ALL_COMMUNICATIONS_KEY");
  private static int _frequency = 1;
  private static long _timeout = 900;
  private static ConfirmAllCommunication _instance;

  /**
   * Create the task that contains all three communication confirmations
   *
   * @author Reid Hayhow
   */
  private ConfirmAllCommunication()
  {
    super(_NAME, _frequency, _timeout, ProgressReporterEnum.CONFIRM_CAMERA_COMMUNICATION);
    addSubTest(ConfirmCommunicationWithIRPs.getInstance());
    addSubTest(ConfirmCommunicationWithRMS.getInstance());

    //This confirmation and all sub-classed confirmations are automated
    _taskType = HardwareTaskTypeEnum.AUTOMATIC_CONFIRMATION_TASK_TYPE;

    //Set enum used to save timestamp for this task
    setTimestampEnum(HardwareCalibEnum.CONFIRM_COMMUNICATION_NETWORK_LAST_TIME_RUN);
  }

  /**
   * Instance method for this task
   *
   * @author Reid Hayhow
   */
  public static synchronized ConfirmAllCommunication getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmAllCommunication();
    }
    return _instance;
  }

  /**
   * Method to see if a specific HardwareTask can run on the current hardware.
   *
   * @author Reid Hayhow
   */
  public boolean canRunOnThisHardware()
  {
    boolean canRunOnThisHardware = true;

    //Iterate over the list of subtasks and see if they can run on this hardware
    List<HardwareTask> listOfSubTasks = getSubTests();

    for (HardwareTask task : listOfSubTasks)
    {
      canRunOnThisHardware &= task.canRunOnThisHardware();
    }

    //Return the boolean summation of what all tasks say about running
    return canRunOnThisHardware;
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * clean-up code that may be reused (for example executeTask and abortTask
   * may both use the same cleanUp code)
   *
   * @author Reid Hayhow
   */
  protected void cleanUp() throws XrayTesterException
  {
    //none needed
  }

  /**
   * Create a Limits object specific to the particular test being run.
   *
   * @author Reid Hayhow
   */
  protected void createLimitsObject()
  {
    _limits = new Limit(null, null, null, null);
  }

  /**
   * createResultsObject Create the results object for this test result.
   *
   * @author Reid Hayhow
   */
  protected void createResultsObject()
  {
    //Assume things are OK unless I can prove otherwise
    _result = new Result(new Boolean(true), _NAME);

    StringBuilder sb = new StringBuilder();

    List<HardwareTask> listOfSubTasks = getSubTests();
    for (HardwareTask task : listOfSubTasks)
    {
      sb.append(task.getResult().getTaskStatusMessage());
    }
    _result.setTaskStatusMessage(sb.toString());
  }

  /**
   * This method should contain the task code.
   *
   * @author Reid Hayhow
   */
  protected void executeTask() throws XrayTesterException
  {
	//Swee Yee Wong - Camera debugging purpose
    debug("ConfirmAllCommunication");
    Assert.expect(false, "Shouldn't get called!");
  }

  /**
   * This method provides a single point of contact where tasks can put their
   * set-up code that may be reused
   *
   * @author Reid Hayhow
   */
  protected void setUp() throws XrayTesterException
  {
    //None needed
  }
  
  /**
   * @author Anthony Fong
   */   
  public boolean isBypassNeededForUnnecessaryHardwareTaskInPassiveMode()
  {
    return false;
  }
  
  /**
   * Camera debugging purpose
   * @author Swee-Yee.Wong
   */
  private void debug(String message)
  {
    try
    {
      CameraCommandRepliesLogUtil.getInstance().log(message);
    }
    catch (XrayTesterException e)
    {
      System.out.println("Failed to get machine descriptions. \n" + e.getMessage());
    }
  }
}
