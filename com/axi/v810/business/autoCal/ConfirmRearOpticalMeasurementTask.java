package com.axi.v810.business.autoCal;

import com.axi.v810.datastore.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Cheah Lee Herng
 */
public class ConfirmRearOpticalMeasurementTask extends ConfirmOpticalMeasurementTask
{
  // Printable name for this procedure
  private static final String NAME = StringLocalizer.keyToString("CD_CONFIRM_REAR_OPTICAL_MEASUREMENT_KEY");

  private static ConfirmRearOpticalMeasurementTask _instance = null;
  
  /**
   * @author Cheah Lee Herng
   */
  private ConfirmRearOpticalMeasurementTask()
  {
    super(NAME,
          Directory.getRearOpticalMeasurementRepeatabilityConfirmationLogDir(),
          FileName.getRearOpticalMeasurementRepeatabilityLogFileBasename(),
          ProgressReporterEnum.CONFIRM_REAR_OPTICAL_MEASUREMENT_REPEATABILITY);
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  public static synchronized ConfirmRearOpticalMeasurementTask getInstance()
  {
    if (_instance == null)
    {
      _instance = new ConfirmRearOpticalMeasurementTask();
    }
    return _instance;
  }
  
  /**   
   * @author Cheah Lee Herng 
   */
  protected PspModuleEnum getPspModuleEnum()
  {
    return PspModuleEnum.REAR;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  protected OpticalCameraIdEnum getOpticalCameraIdEnum()
  {
    return OpticalCameraIdEnum.OPTICAL_CAMERA_2;
  }
}
