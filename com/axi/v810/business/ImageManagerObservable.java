package com.axi.v810.business;

import java.util.*;

import com.axi.util.*;

/**
 * Allows you to observe events that come from the image manager,
 * such as image loaded or image saved events
 * @author George A. David
 */
public class ImageManagerObservable extends Observable
{
  private static ImageManagerObservable _instance;

  /**
   * @author George A. David
   */
  private ImageManagerObservable()
  {
    // do nothing
  }

  /**
   * @author George A. David
   */
  public static synchronized ImageManagerObservable getInstance()
  {
    if (_instance == null)
      _instance = new ImageManagerObservable();

    return _instance;
  }


  /**
   * @author George A. David
   */
  public void stateChanged(ImageManagerEventEnum imageManagerEventEnum)
  {
    Assert.expect(imageManagerEventEnum != null);

    setChanged();
    notifyObservers(imageManagerEventEnum);
  }
}


