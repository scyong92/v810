package com.axi.v810.business.testResults;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gridArray.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Patrick Lacz
 */
public class Test_ComponentInspectionResult extends UnitTest
{
  public Test_ComponentInspectionResult()
  {
  }

  /**
   * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
    UnitTest.execute(new Test_ComponentInspectionResult());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testMeasurements();

    // Indictments
    ComponentInspectionResult componentInspectionResult = new ComponentInspectionResult();
    Expect.expect(componentInspectionResult.passed());
    Expect.expect(componentInspectionResult.getIndictments().isEmpty());

    SliceNameEnum sliceNameEnum = SliceNameEnum.PACKAGE;

    ComponentIndictment indictment = new ComponentIndictment(IndictmentEnum.INSUFFICIENT,
                                                             new GridArrayVoidingAlgorithm(new GridArrayInspectionFamily()),
                                                             sliceNameEnum,
                                                             new Subtype(),
                                                             JointTypeEnum.COLLAPSABLE_BGA);

    componentInspectionResult.addIndictment(indictment);

    Expect.expect(componentInspectionResult.passed() == false);
    Expect.expect(componentInspectionResult.getIndictments().size() == 1);
    Expect.expect(sliceNameEnum.equals(indictment.getSliceNameEnum()));
  }

  /**
   * @author Patrick Lacz
   */
  private void testMeasurements()
  {
    ComponentInspectionResult componentInspectionResult = new ComponentInspectionResult();

    boolean hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(SliceNameEnum.PAD,
        MeasurementEnum.GRID_ARRAY_DIAMETER);
    Expect.expect(hasUnpopulatedMeasurement == false);

    Expect.expect(componentInspectionResult.getAllMeasurements().isEmpty());

    Algorithm algorithm = new GridArrayVoidingAlgorithm(new GridArrayInspectionFamily());
    Component component = new Component();
    Subtype subtype = new Subtype();
    MeasurementEnum measurementEnum = MeasurementEnum.GRID_ARRAY_COMPONENT_VOIDING_PERCENT;
    SliceNameEnum sliceName = SliceNameEnum.PAD;
    ComponentMeasurement measurement = new ComponentMeasurement(algorithm, subtype, measurementEnum, MeasurementUnitsEnum.MILS,
                                             component, sliceName, 0.0f);
    componentInspectionResult.addMeasurement(measurement);

    boolean hasPopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(sliceName, measurementEnum);
    Expect.expect(hasPopulatedMeasurement == true);

    SliceNameEnum differentSliceName = SliceNameEnum.OPAQUE_CHIP_PAD;
    hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(differentSliceName, measurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    MeasurementEnum differentMeasurementEnum = MeasurementEnum.SHORT_THICKNESS;
    hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(sliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(differentSliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    ComponentMeasurement returnMeasurement = componentInspectionResult.getComponentMeasurement(sliceName, measurementEnum);
    Expect.expect(returnMeasurement.equals(measurement));

    try {
      returnMeasurement = componentInspectionResult.getComponentMeasurement(differentSliceName, measurementEnum);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // expected, do nothing.
    }

    Expect.expect(componentInspectionResult.getAllMeasurements().size() == 1);

    componentInspectionResult.clearMeasurements();

    Expect.expect(componentInspectionResult.getAllMeasurements().isEmpty());

    hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(sliceName, measurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(differentSliceName, measurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(sliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = componentInspectionResult.hasComponentMeasurement(differentSliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);
  }
}
