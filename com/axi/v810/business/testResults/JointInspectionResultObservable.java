package com.axi.v810.business.testResults;

import java.util.*;

import com.axi.util.*;

/**
 * Provides a way for other subsystems, such as the Algorithm Tuner, to get inspection
 * results as they are produced during an inspection run.
 *
 * <p>
 * <b>Be careful about holding the JointInspectionResult objects in memory!</b>
 * If you try to hold all of these objects during a normal test run, you can
 * quickly run out of memory since they hold lots of measurement data.
 * So, it's ok to use the observable to build up a list of failing joints
 * (since you can discard the JointInspectionResult objects as you go), but
 * it's probably not a good idea to save all the JointInspectionResults in order
 * to query for measurement data at the end of the test.  For the latter case,
 * you should use the TestResults functionality in the datastore, which is
 * designed to provide random access to the test results file.
 *
 * @todo Should this really be a ThreadSafeObservable?  The standard java.util.Observable has less overhead . . .
 *
 * @author Peter Esbensen
 */
public class JointInspectionResultObservable extends Observable
{
  private static JointInspectionResultObservable _instance;

  /**
   * @author Peter Esbensen
   */
  public static synchronized JointInspectionResultObservable getInstance()
  {
    if (_instance == null)
      _instance = new JointInspectionResultObservable();
    return _instance;
  }

  /**
   * @author Peter Esbensen
   */
  private JointInspectionResultObservable()
  {
    // do nothing
  }

  /**
   * @author Peter Esbensen
   */
  public void addJointInspectionResult(JointInspectionResult jointInspectionResult)
  {
    Assert.expect(jointInspectionResult != null);
    setChanged();
    notifyObservers(jointInspectionResult);
  }
}
