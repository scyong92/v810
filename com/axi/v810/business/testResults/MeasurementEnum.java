package com.axi.v810.business.testResults;

import java.io.*;
import java.util.*;
import com.axi.v810.business.imageAnalysis.*;

import com.axi.util.*;

/**
 * @author Sunit Bhalla
 */
public class MeasurementEnum extends com.axi.util.Enum implements Serializable, Comparable
{
  private static int _index = -1;

  // SharedShort
  public final static MeasurementEnum SHORT_THICKNESS = new MeasurementEnum(++_index, "Short Thickness");
  public final static MeasurementEnum SHORT_LENGTH = new MeasurementEnum(++_index, "Short Length");
  //Lim, Lay Ngor - XCR1743:Benchmark
  public final static MeasurementEnum SOLDER_BALL_SHORT_DELTA_THICKNESS = new MeasurementEnum(++_index, "Center Solder Ball Short Thickness");
  //By Lim, Lay Ngor - XCR????: Broken Pin
  public final static MeasurementEnum SHORT_BROKEN_PIN_AREA_SIZE = new MeasurementEnum(++_index, "Broken Pin Area Size"); 

  // Locator
  public final static MeasurementEnum LOCATOR_X_LOCATION = new MeasurementEnum(++_index, "Pixel X Location in Region");
  public final static MeasurementEnum LOCATOR_Y_LOCATION = new MeasurementEnum(++_index, "Pixel Y Location in Region");

  // Grid Array Measurement
  public final static MeasurementEnum GRID_ARRAY_DIAMETER = new MeasurementEnum(++_index, "Diameter");
  public final static MeasurementEnum GRID_ARRAY_THICKNESS = new MeasurementEnum(++_index, "Thickness");

  // Grid Array Misalignment
  public final static MeasurementEnum GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD = new MeasurementEnum(++_index, "Joint Offset X");
  public final static MeasurementEnum GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD = new MeasurementEnum(++_index, "Joint Offset Y");
  public final static MeasurementEnum GRID_ARRAY_JOINT_OFFSET_FROM_CAD = new MeasurementEnum(++_index, "Offset from CAD");
  public final static MeasurementEnum GRID_ARRAY_JOINT_OFFSET_FROM_EXPECTED_LOCATION = new MeasurementEnum(++_index, "Offset from Region Joints");

  // Grid Array Open
  public final static MeasurementEnum GRID_ARRAY_DIAMETER_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Percent of Nominal Diameter");
  public final static MeasurementEnum GRID_ARRAY_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Percent of Nominal Thickness");
  public final static MeasurementEnum GRID_ARRAY_REGION_OUTLIER = new MeasurementEnum(++_index, "Region Outlier");
  public final static MeasurementEnum GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS = new MeasurementEnum(++_index, "Diameter Difference from Neighbors");
  public final static MeasurementEnum GRID_ARRAY_NEIGHBOR_OUTLIER_SCORE = new MeasurementEnum(++_index, "Neighbor Outlier");
  public final static MeasurementEnum GRID_ARRAY_OPEN_ECCENTRICITY = new MeasurementEnum(++_index, "Eccentricity");
  public final static MeasurementEnum GRID_ARRAY_PERCENT_OF_JOINTS_WITH_INSUFFICIENT_DIAMETER = new MeasurementEnum(++_index, "Percent of Joints With Marginal Diameter");
  public final static MeasurementEnum GRID_ARRAY_HIP_VALUE = new MeasurementEnum(++_index, "HIP Value");
  public final static MeasurementEnum GRID_ARRAY_HIP_OUTLIER_SCORE = new MeasurementEnum(++_index, "HIP Outlier");

  // Grid Array Voiding
  public final static MeasurementEnum GRID_ARRAY_VOIDING_PERCENT = new MeasurementEnum(++_index, "Void Area");
  public final static MeasurementEnum GRID_ARRAY_COMPONENT_VOIDING_PERCENT = new MeasurementEnum(++_index, "Percent of Joints Failing Component-level Voiding");
  public final static MeasurementEnum GRID_ARRAY_COMPONENT_MIDBALL_VOIDING = new MeasurementEnum(++_index, "Component Miball Voiding");

  public final static MeasurementEnum GRID_ARRAY_INDIVIDUAL_VOIDING_PERCENT = new MeasurementEnum(++_index, "Individual Void Area");
  public final static MeasurementEnum GRID_ARRAY_INDIVIDUAL_VOIDING_DIAMETER_PERCENT = new MeasurementEnum(++_index, "Individual Void Diameter");//Siew Yeng - XCR-3515

  // Gullwing Measurement
  public final static MeasurementEnum GULLWING_HEEL_THICKNESS = new MeasurementEnum(++_index, "Heel Thickness");
  public final static MeasurementEnum GULLWING_TOE_THICKNESS = new MeasurementEnum(++_index, "Toe Thickness");
  public final static MeasurementEnum GULLWING_CENTER_THICKNESS = new MeasurementEnum(++_index, "Center Thickness");
  public final static MeasurementEnum GULLWING_CENTER_TO_HEEL_THICKNESS_PERCENT = new MeasurementEnum(++_index, "Center to Heel Thickness Percent");
  public final static MeasurementEnum GULLWING_FILLET_LENGTH = new MeasurementEnum(++_index, "Fillet Length");
  public final static MeasurementEnum GULLWING_FILLET_THICKNESS = new MeasurementEnum(++_index, "Fillet Thickness");
  public final static MeasurementEnum GULLWING_JOINT_OFFSET_FROM_CAD = new MeasurementEnum(++_index, "Joint Offset from CAD");
  public final static MeasurementEnum GULLWING_HEEL_POSITION = new MeasurementEnum(++_index, "Heel Position");
  public final static MeasurementEnum GULLWING_FILLET_PEAK_POSITION = new MeasurementEnum(++_index, "Fillet Peak Position");
  //Lim, Lay Ngor - XCR1766 - Add pin length measurement
  public final static MeasurementEnum GULLWING_JOINT_PIN_LENGTH = new MeasurementEnum(++_index, "Pin Length");  

  // Gullwing Open
  public final static MeasurementEnum GULLWING_FILLET_LENGTH_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Fillet Length Percent of Nominal");
  public final static MeasurementEnum GULLWING_HEEL_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Heel Thickness Percent of Nominal");
  //Siew Yeng - XCR-3532
  public final static MeasurementEnum GULLWING_FILLET_LENGTH_REGION_OUTLIER = new MeasurementEnum(++_index, "Fillet Length Region Outlier");
  public final static MeasurementEnum GULLWING_CENTER_THICKNESS_REGION_OUTLIER = new MeasurementEnum(++_index, "Center Thickness Region Outlier");
  
  // Gullwing Insufficient
  public final static MeasurementEnum GULLWING_FILLET_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Fillet Thickness Percent of Nominal");

  //Chin Seong, Kee - Request to add Excess to the Gullwing family of algorithms
  public final static MeasurementEnum GULLWING_TOE_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Toe Thickness Percent of Nominal");

  // Gullwing Misalignment
  public final static MeasurementEnum GULLWING_HEEL_POSITION_DISTANCE_FROM_NOMINAL = new MeasurementEnum(++_index, "Heel Position Distance from Nominal");
  public final static MeasurementEnum GULLWING_FILLET_PEAK_OFFSET_FROM_REGION_NEIGHBORS = new MeasurementEnum(++_index, "Fillet Peak Offset From Region Neighbors");

  // Gullwing Learning
  public final static MeasurementEnum GULLWING_LEARNING_HEEL_EDGE_PERCENT_OF_MAX_THICKNESS = new MeasurementEnum(++_index, "Heel Edge Thickness Percent for Learning");
  public final static MeasurementEnum GULLWING_LEARNING_HEEL_EDGE_TO_PEAK_DISTANCE_PERCENT_OF_PAD_LENGTH_ALONG = new MeasurementEnum(++_index, "Heel Edge to Peak Distance for Learning");
  public final static MeasurementEnum GULLWING_LEARNING_PIN_LENGTH = new MeasurementEnum(++_index, "Pin Length for Learning");
  public final static MeasurementEnum GULLWING_LEARNING_TOE_EDGE_THICKNESS_PERCENT_OF_NOMINAL_TOE_THICKNESS = new MeasurementEnum(++_index, "Toe Edge Thickness Percent for Learning");
  public final static MeasurementEnum GULLWING_LEARNING_CENTER_OFFSET = new MeasurementEnum(++_index, "Center Offset for Learning");

  // Advanced Gullwing Measurement
  public final static MeasurementEnum ADVANCED_GULLWING_MAX_HEEL_SLOPE = new MeasurementEnum(++_index, "Heel Slope");
  public final static MeasurementEnum ADVANCED_GULLWING_MAX_TOE_SLOPE = new MeasurementEnum(++_index, "Toe Slope");
  public final static MeasurementEnum ADVANCED_GULLWING_MAX_HEEL_AND_TOE_SLOPES_SUM = new MeasurementEnum(++_index, "Sum of Heel and Toe Slopes");
  public final static MeasurementEnum ADVANCED_GULLWING_SUM_OF_SLOPE_CHANGES = new MeasurementEnum(++_index, "Sum of Slope Changes");
  public final static MeasurementEnum ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_THICKNESS = new MeasurementEnum(++_index, "Across Heel Thickness");
  public final static MeasurementEnum ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_THICKNESS = new MeasurementEnum(++_index, "Across Center Thickness");
  public final static MeasurementEnum ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_THICKNESS_PERCENT = new MeasurementEnum(++_index, "Across Center to Heel Thickness Percent");
  public final static MeasurementEnum ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_2_THICKNESS = new MeasurementEnum(++_index, "Across Heel 2 Thickness");
  public final static MeasurementEnum ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_TO_HEEL_2_THICKNESS_PERCENT = new MeasurementEnum(++_index, "Across Center to Heel 2 Thickness Percent");
  
  public final static MeasurementEnum ADVANCED_GULLWING_ACROSS_PROFILE_CONCAVITY_RATIO = new MeasurementEnum(++_index, "Across Concavity Ratio");
  public final static MeasurementEnum ADVANCED_GULLWING_HEEL_TO_PAD_CENTER = new MeasurementEnum(++_index, "Heel To Pad Center");
  public final static MeasurementEnum ADVANCED_GULLWING_MASS_SHIFT_ACROSS = new MeasurementEnum(++_index, "Mass Shift Across");
  public final static MeasurementEnum ADVANCED_GULLWING_HEEL_TO_PAD_CENTER_DIFFERENCE_FROM_NOMINAL = new MeasurementEnum(++_index, "Heel To Pad Center Difference From Nominal");

  // Advanced Gullwing Learning
  public final static MeasurementEnum ADVANCED_GULLWING_LEARNING_SIDE_FILLET_EDGE_PERCENT_OF_MAX_THICKNESS = new MeasurementEnum(++_index, "Side Fillet Edge Thickness Percent for Learning");
  public final static MeasurementEnum ADVANCED_GULLWING_LEARNING_EDGE_TO_PEAK_DISTANCE = new MeasurementEnum(++_index, "Side Fillet to Peak Distance for Learning");
  public final static MeasurementEnum ADVANCED_GULLWING_LEARNING_CENTER_OFFSET = new MeasurementEnum(++_index, "Side Fillet Center Offset for Learning");

  // Throughhole Measurement
  public final static MeasurementEnum THROUGHHOLE_SOLDER_SIGNAL = new MeasurementEnum(++_index, "Solder Signal");

  // Throughhole Open
  public final static MeasurementEnum THROUGHHOLE_OPEN_DIFFERENCE_FROM_NOMINAL = new MeasurementEnum(++_index, "Difference from Nominal Solder Signal");
  public final static MeasurementEnum THROUGHHOLE_OPEN_DIFFERENCE_FROM_REGION = new MeasurementEnum(++_index, "Difference from Region Average Solder Signal");

  // Throughhole Insufficient
  public final static MeasurementEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL = new MeasurementEnum(++_index, "Difference from Nominal Solder Signal");
  public final static MeasurementEnum THROUGHHOLE_INSUFFICIENT_DIFFERENCE_FROM_REGION = new MeasurementEnum(++_index, "Difference from Region Average Solder Signal");
  public final static MeasurementEnum THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE = new MeasurementEnum(++_index, "Wetting Coverage");
  public final static MeasurementEnum THROUGHHOLE_INSUFFICIENT_WETTING_COVERAGE_SENSITIVITY = new MeasurementEnum(++_index, "Wetting Coverage Sensitivity");
  public final static MeasurementEnum THROUGHHOLE_INSUFFICIENT_VOIDING_PERCENTAGE = new MeasurementEnum(++_index, "Voiding Percentage");
  public final static MeasurementEnum THROUGHHOLE_INSUFFICIENT_DEGREE_COVERAGE = new MeasurementEnum(++_index, "Degree Coverage");
  //By Lim Lay Ngor PIPA
  public final static MeasurementEnum THROUGHHOLE_INSUFFICIENT_VOID_VOLUME_PERCENTAGE = new MeasurementEnum(++_index, "Void Volume Percentage");
  
  //Khaw Chek Hau - XCR3745: Support PTH Excess Solder Algorithm
  public final static MeasurementEnum THROUGHHOLE_EXCESS_SOLDER_SIGNAL = new MeasurementEnum(++_index, "Excess Solder Signal");

  // Pressfit Measurement
  public final static MeasurementEnum PRESSFIT_GRAYLEVEL = new MeasurementEnum(++_index, "Graylevel");

  // Pressfit Insufficient
  public final static MeasurementEnum PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_NOMINAL = new MeasurementEnum(++_index, "Difference from Nominal Graylevel");
  public final static MeasurementEnum PRESSFIT_INSUFFICIENT_DIFFERENCE_FROM_REGION = new MeasurementEnum(++_index, "Difference from Region Average Graylevel");

  // Chip Measurement
  public final static MeasurementEnum CHIP_MEASUREMENT_CLEAR_FILLET_THICKNESS = new MeasurementEnum(++_index, "Fillet Thickness");
  public final static MeasurementEnum CHIP_MEASUREMENT_OPAQUE_FILLET_THICKNESS = new MeasurementEnum(++_index, "Fillet Thickness");
  public final static MeasurementEnum CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS = new MeasurementEnum(++_index, "Lower Fillet Thickness");
  public final static MeasurementEnum CHIP_MEASUREMENT_OPAQUE_COMPONENT_LENGTH = new MeasurementEnum(++_index, "Component Body Length");
  public final static MeasurementEnum CHIP_MEASUREMENT_CLEAR_COMPONENT_LENGTH = new MeasurementEnum(++_index, "Component Body Length");
  public final static MeasurementEnum CHIP_MEASUREMENT_COMPONENT_LENGTH_PERCENT = new MeasurementEnum(++_index, "Component Body Length % of Nominal");
  public final static MeasurementEnum CHIP_MEASUREMENT_FILLET_GAP = new MeasurementEnum(++_index, "Fillet Gap");
  public final static MeasurementEnum CHIP_MEASUREMENT_OPAQUE_OPEN_SIGNAL = new MeasurementEnum(++_index, "Opaque Open Signal");
  public final static MeasurementEnum CHIP_MEASUREMENT_CLEAR_OPEN_SIGNAL = new MeasurementEnum(++_index, "Clear Open Signal");

  // CR33213-Capacitor Misalignment (Sept 2009) by Anthony Fong
  public final static MeasurementEnum CHIP_MEASUREMENT_COMPONENT_SHIFT_ALONG_IN_PERCENTAGE = new MeasurementEnum(++_index, "Component Shift Along");
  public final static MeasurementEnum CHIP_MEASUREMENT_PIN1_ALONG_OFFSET_IN_PERCENTAGE = new MeasurementEnum(++_index, "Pin 1 Along Offset");
  public final static MeasurementEnum CHIP_MEASUREMENT_PIN2_ALONG_OFFSET_IN_PERCENTAGE = new MeasurementEnum(++_index, "Pin 2 Along Offset");
  public final static MeasurementEnum CHIP_MEASUREMENT_PIN1_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE = new MeasurementEnum(++_index, "Pin 1 Misalignment Along Delta");
  public final static MeasurementEnum CHIP_MEASUREMENT_PIN2_MISALIGNMENT_ALONG_DELTA_IN_PERCENTAGE = new MeasurementEnum(++_index, "Pin 2 Misalignment Along Delta");
  public final static MeasurementEnum CHIP_MEASUREMENT_NOMINAL_SHIFT_ROTATION_IN_DEGREES = new MeasurementEnum(++_index, "Estimated Maximum Rotation");
  public final static MeasurementEnum CHIP_MEASUREMENT_PROFILE_OFFSET_ALONG_IN_PERCENTAGE = new MeasurementEnum(++_index, "Across Profile Offset from Upper Fillet");

  public final static MeasurementEnum CHIP_MEASUREMENT_COMPONENT_SHIFT_ACROSS = new MeasurementEnum(++_index, "Component Shift Across");
  public final static MeasurementEnum CHIP_MEASUREMENT_COMPONENT_BODY_THICKNESS = new MeasurementEnum(++_index, "Body Thickness");
  public final static MeasurementEnum CHIP_MEASUREMENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Fillet Thickness Percent of Nominal");
  public final static MeasurementEnum CHIP_MEASUREMENT_PROFILE_SHARPNESS_ALONG = new MeasurementEnum(++_index, "Sharpness Along Profile");  // no longer used
  public final static MeasurementEnum CHIP_MEASUREMENT_OVERALL_SHARPNESS = new MeasurementEnum(++_index, "Overall Sharpness");  // no longer used
  public final static MeasurementEnum CHIP_MEASUREMENT_BODY_WIDTH = new MeasurementEnum(++_index, "Component Width Across");
  //Lim, Lay Ngor - Clear Tombstone  
  //Possible revise to reuse GULLWING_CENTER_THICKNESS since the functionality are same
  public final static MeasurementEnum CHIP_MEASUREMENT_CENTER_FILLET_THICKNESS = new MeasurementEnum(++_index, "Center Fillet Thickness");
  public final static MeasurementEnum CHIP_MEASUREMENT_CENTER_TO_UPPER_FILLET_THICKNESS_PERCENT = new MeasurementEnum(++_index, "Center To Upper Fillet Thickness Percentage");
  public final static MeasurementEnum CHIP_MEASUREMENT_PAD_SOLDER_AREA_PERCENTAGE = new MeasurementEnum(++_index, "Pad Solder Area Percentage");
  //Lim, Lay Ngor - Opaque Tombstone
  public final static MeasurementEnum CHIP_MEASUREMENT_PARTIAL_FILLET_THICKNESS = new MeasurementEnum(++_index, "Joint Partial Fillet Thickness");//opaque with a little offset
  public final static MeasurementEnum CHIP_MEASUREMENT_FILLET_WIDTH = new MeasurementEnum(++_index, "Joint Fillet Width Across");
  public final static MeasurementEnum CHIP_MEASUREMENT_FILLET_WIDTH_JOINT_RATIO = new MeasurementEnum(++_index, "Fillet Width Across Ratio Between Two Joint");  

  public final static MeasurementEnum CHIP_MEASUREMENT_COMPONENT_ROTATION = new MeasurementEnum(++_index, "Component Rotation");
  public final static MeasurementEnum CHIP_MEASUREMENT_COMPONENT_TILT_RATIO = new MeasurementEnum(++_index, "Component Tilt Ratio");
  //sheng chuan pattern match
  public final static MeasurementEnum CHIP_MEASUREMENT_TEMPLATE_MATCHING = new MeasurementEnum(++_index, "Matching Percent");
  public final static MeasurementEnum CHIP_MEASUREMENT_ROUNDNESS_PERCENT = new MeasurementEnum(++_index, "Roundness Percent");
  
  //Khaw Chek Hau - XCR2385: Resistor Component Profile Length Across Adjustment
  public final static MeasurementEnum CHIP_MEASUREMENT_COMPONENT_ACROSS_PROFILE_SIZE_IN_PERCENTAGE = new MeasurementEnum(++_index, "Component Across Profile Size");

  // Polarized Cap Measurement
  public final static MeasurementEnum PCAP_INSUFFICIENT_FILLET_ONE_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Fillet One Thickness Percent of Nominal");
  public final static MeasurementEnum PCAP_INSUFFICIENT_FILLET_TWO_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Fillet Two Thickness Percent of Nominal");
  public final static MeasurementEnum PCAP_MEASUREMENT_FILLET_ONE_THICKNESS = new MeasurementEnum(++_index, "Pin One Fillet Thickness");
  public final static MeasurementEnum PCAP_MEASUREMENT_FILLET_TWO_THICKNESS = new MeasurementEnum(++_index, "Pin Two Fillet Thickness");
  public final static MeasurementEnum PCAP_MEASUREMENT_AVERAGE_PAD_ONE_THICKNESS = new MeasurementEnum(++_index, "pad one average thickness");
  public final static MeasurementEnum PCAP_MEASUREMENT_AVERAGE_PAD_TWO_THICKNESS = new MeasurementEnum(++_index, "pad two average thickness");
  public final static MeasurementEnum PCAP_MEASUREMENT_PAD_ONE_LOWER_FILLET_THICKNESS = new MeasurementEnum(++_index, "Pin One Lower Fillet Thickness");
  public final static MeasurementEnum PCAP_MEASUREMENT_PAD_TWO_LOWER_FILLET_THICKNESS = new MeasurementEnum(++_index, "Pin Two Lower Fillet Thickness");
  public final static MeasurementEnum PCAP_OPEN_PAD_ONE_OPEN_SIGNAL = new MeasurementEnum(++_index, "Pin One Open Signal");
  public final static MeasurementEnum PCAP_OPEN_PAD_TWO_OPEN_SIGNAL = new MeasurementEnum(++_index, "Pin Two Open Signal");
  public final static MeasurementEnum PCAP_MEASUREMENT_POLARITY_SIGNAL = new MeasurementEnum(++_index, "Polarity Signal");
  public final static MeasurementEnum PCAP_MEASUREMENT_SLUG_THICKNESS = new MeasurementEnum(++_index, "Slug Thickness");
  public final static MeasurementEnum PCAP_MEASUREMENT_SLUG_EDGE_THRESHOLD = new MeasurementEnum(++_index, "Slug Edge Threshold");
  public final static MeasurementEnum PCAP_MEASUREMENT_SLUG_EDGE_REGION_LENGTH_ACROSS = new MeasurementEnum(++_index, "Slug Edge Region Length Across");
  public final static MeasurementEnum PCAP_MEASUREMENT_SLUG_EDGE_REGION_LENGTH_ALONG = new MeasurementEnum(++_index, "Slug Edge Region Length Along");
  public final static MeasurementEnum PCAP_MEASUREMENT_SLUG_EDGE_REGION_POSITION = new MeasurementEnum(++_index, "Slug Edge Region Position");

  // Large Pad - Voiding
  public final static MeasurementEnum LARGE_PAD_VOIDING_PERCENT = new MeasurementEnum(++_index, "Voiding Area");
  public final static MeasurementEnum LARGE_PAD_COMPONENT_VOIDING_PERCENT = new MeasurementEnum(++_index, "Component Voiding Area");
  // Jack Hwee- individual void
  public final static MeasurementEnum LARGE_PAD_INDIVIDUAL_VOIDING_PERCENT = new MeasurementEnum(++_index, "Individual Voiding Area");
  
  // Kee Chin Seong - voiding grey level
  public final static MeasurementEnum SHARED_VOIDING_DEFINITE_VOIDING_GREY_LEVEL = new MeasurementEnum(++_index, "Voiding Area value");
  public final static MeasurementEnum SHARED_VOIDING_EXONERATED_VOIDING_GREY_LEVEL = new MeasurementEnum(++_index, "Exonerated Voiding Area value");
  public final static MeasurementEnum SHARED_VOIDING_DIFFERENCE_FROM_NOMINAL_PERCENTAGE = new MeasurementEnum(++_index, "Void Grey Level Difference From Nominal");

  // Large Pad - Open
  public final static MeasurementEnum LARGE_PAD_OPEN_LEADING_SLOPE_ALONG = new MeasurementEnum(++_index, "Leading Slope Along");
  public final static MeasurementEnum LARGE_PAD_OPEN_TRAILING_SLOPE_ALONG = new MeasurementEnum(++_index, "Trailing Slope Along");
  public final static MeasurementEnum LARGE_PAD_OPEN_SLOPE_SUM_ALONG = new MeasurementEnum(++_index, "Slope Sum Along");
  public final static MeasurementEnum LARGE_PAD_OPEN_LEADING_SLOPE_ACROSS = new MeasurementEnum(++_index, "Leading Slope Across");
  public final static MeasurementEnum LARGE_PAD_OPEN_TRAILING_SLOPE_ACROSS = new MeasurementEnum(++_index, "Trailing Slope Across");
  public final static MeasurementEnum LARGE_PAD_OPEN_SLOPE_SUM_ACROSS = new MeasurementEnum(++_index, "Slope Sum Across");

  // Large Pad - Insufficient
  public final static MeasurementEnum LARGE_PAD_INSUFFICIENT_PAD_THICKNESS = new MeasurementEnum(++_index, "Pad Thickness");
  public final static MeasurementEnum LARGE_PAD_INSUFFICIENT_SUM_OF_SLOPE_CHANGES = new MeasurementEnum(++_index, "Sum of Slope Changes");
  public final static MeasurementEnum LARGE_PAD_INSUFFICIENT_PAD_AREA_PERCENT = new MeasurementEnum(++_index, "Insufficient Area Percentage");

  // QFN Measurement
  public final static MeasurementEnum QFN_MEASUREMENT_JOINT_OFFSET_FROM_CAD = new MeasurementEnum(++_index, "Joint Offset from CAD");
  public final static MeasurementEnum QFN_MEASUREMENT_FILLET_LENGTH = new MeasurementEnum(++_index, "Fillet Length");
  public final static MeasurementEnum QFN_MEASUREMENT_FILLET_THICKNESS = new MeasurementEnum(++_index, "Fillet Thickness");
  public final static MeasurementEnum QFN_MEASUREMENT_HEEL_THICKNESS = new MeasurementEnum(++_index, "Heel Thickness");
  public final static MeasurementEnum QFN_MEASUREMENT_TOE_THICKNESS = new MeasurementEnum(++_index, "Toe Thickness");
  // space added at end of name to avoid conflict with GULLWING_CENTER_THICKNESS
  // the name is used by "remove() when _nonCustomerVisibleMeasurements is processed
  public final static MeasurementEnum QFN_MEASUREMENT_CENTER_THICKNESS = new MeasurementEnum(++_index, "Center Thickness ");
  public final static MeasurementEnum QFN_MEASUREMENT_MAX_HEEL_SLOPE = new MeasurementEnum(++_index, "Heel Slope");
  public final static MeasurementEnum QFN_MEASUREMENT_MAX_TOE_SLOPE = new MeasurementEnum(++_index, "Toe Slope");
  public final static MeasurementEnum QFN_MEASUREMENT_CENTER_SLOPE = new MeasurementEnum(++_index, "Center Slope");
  public final static MeasurementEnum QFN_MEASUREMENT_HEEL_TOE_SLOPE_SUM = new MeasurementEnum(++_index, "Sum of Heel and Toe Slopes");
  public final static MeasurementEnum QFN_MEASUREMENT_SUM_OF_SLOPE_CHANGES = new MeasurementEnum(++_index, "Sum of Slope Changes");
  public final static MeasurementEnum QFN_MEASUREMENT_UPWARD_CURVATURE = new MeasurementEnum(++_index, "Upward Curvature");
  public final static MeasurementEnum QFN_MEASUREMENT_HEEL_SHARPNESS = new MeasurementEnum(++_index, "Heel Sharpness");
  public final static MeasurementEnum QFN_MEASUREMENT_HEEL_ACROSS_WIDTH = new MeasurementEnum(++_index, "Heel Across Width");
  public final static MeasurementEnum QFN_MEASUREMENT_HEEL_ACROSS_SLOPE_SUM = new MeasurementEnum(++_index, "Across Heel Sum of Slopes");
  public final static MeasurementEnum QFN_MEASUREMENT_HEEL_ACROSS_LEADING_TRAILING_SLOPE_SUM = new MeasurementEnum(++_index, "Across Heel Leading/Trailing Slope Sum");
  public final static MeasurementEnum QFN_MEASUREMENT_CENTER_ACROSS_WIDTH = new MeasurementEnum(++_index, "Center Across Width");
  public final static MeasurementEnum QFN_MEASUREMENT_CENTER_ACROSS_SLOPE_SUM = new MeasurementEnum(++_index, "Across Center Sum of Slopes");
  public final static MeasurementEnum QFN_MEASUREMENT_CENTER_ACROSS_LEADING_TRAILING_SLOPE_SUM = new MeasurementEnum(++_index, "Across Center Leading/Trailing Slope Sum");
  public final static MeasurementEnum QFN_MEASUREMENT_NEIGHBOR_LENGTH_DIFFERENCE = new MeasurementEnum(++_index, "Neighbor Length Difference");
  public final static MeasurementEnum QFN_MEASUREMENT_SOLDER_THICKNESS_AT_LOCATION = new MeasurementEnum(++_index, "Solder Thickness At Location");
  public final static MeasurementEnum QFN_MEASUREMENT_CENTER_OF_SOLDER_VOLUME = new MeasurementEnum(++_index, "Center of Solder Volume");

  // QFN Open
  public final static MeasurementEnum QFN_FILLET_LENGTH_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Fillet Length Percent of Nominal");
  public final static MeasurementEnum QFN_OPEN_HEEL_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Heel Thickness Percent of Nominal");
  public final static MeasurementEnum QFN_OPEN_CENTER_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Center Thickness Percent of Nominal");
  public final static MeasurementEnum QFN_OPEN_OPEN_SIGNAL = new MeasurementEnum(++_index, "Open Signal");
  public final static MeasurementEnum QFN_OPEN_MAXIMUM_CENTER_TO_TOE_PERCENT = new MeasurementEnum(++_index, "Maximum Center To Toe Thickness Percent");
  public final static MeasurementEnum QFN_OPEN_MAXIMUM_CENTER_TO_HEEL_PERCENT = new MeasurementEnum(++_index, "Maximum Center To Heel Thickness Percent");
  public final static MeasurementEnum QFN_OPEN_ENABLE_MAXIMUM_CENTER_TO_TOE_PERCENT_TEST = new MeasurementEnum(++_index, "Enable Toe Test If Center To Heel Thickness Test Fail");
  public final static MeasurementEnum QFN_OPEN_CENTER_OF_SOLDER_VOLUME_LOCATION_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Center of Solder Volume Location Percent of Nominal");
  //Siew Yeng - XCR-3532
  public final static MeasurementEnum QFN_OPEN_FILLET_LENGTH_REGION_OUTLIER = new MeasurementEnum(++_index, "Fillet Length Region Outlier");
  public final static MeasurementEnum QFN_OPEN_CENTER_THICKNESS_REGION_OUTLIER = new MeasurementEnum(++_index, "Center Thickness Region Outlier");
  
  // QFN Insufficient
  public final static MeasurementEnum QFN_INSUFFICIENT_FILLET_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Fillet Thickness Percent of Nominal");
  public final static MeasurementEnum QFN_INSUFFICIENT_TOE_THICKNESS_PERCENT_OF_NOMINAL = new MeasurementEnum(++_index, "Toe Thickness Percent of Nominal");

  // QFN Voiding
  public final static MeasurementEnum QFN_VOIDING_PERCENT = new MeasurementEnum(++_index, "Void Area");
  //Jack Hwee- individual void
  public final static MeasurementEnum QFN_INDIVIDUAL_VOIDING_PERCENT = new MeasurementEnum(++_index, "Individual Void Area");

  // QFN Learning
  public final static MeasurementEnum QFN_LEARNING_HEEL_EDGE_PERCENT_OF_MAX_THICKNESS = new MeasurementEnum(++_index, "Heel Edge Thickness Percent for Learning");
  public final static MeasurementEnum QFN_LEARNING_EDGE_TO_HEEL_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG = new MeasurementEnum(++_index, "Edge to Heel Peak Distance for Learning");
  public final static MeasurementEnum QFN_LEARNING_PIN_LENGTH = new MeasurementEnum(++_index, "Pin Length for Learning");
  public final static MeasurementEnum QFN_LEARNING_TOE_EDGE_THICKNESS_PERCENT_OF_NOMINAL_TOE_THICKNESS = new MeasurementEnum(++_index, "Toe Edge Thickness Percent for Learning");
  public final static MeasurementEnum QFN_LEARNING_EDGE_TO_CENTER_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG = new MeasurementEnum(++_index, "Edge to Center Distance for Learning");
  public final static MeasurementEnum QFN_LEARNING_HEEL_EDGE_MAX_SLOPE_INDEX  = new MeasurementEnum(++_index, "Heel Max Slope Index for Learning");
  public final static MeasurementEnum QFN_LEARNING_TOE_EDGE_MAX_SLOPE_INDEX  = new MeasurementEnum(++_index, "Toe Max Slope Index for Learning");
  public final static MeasurementEnum QFN_LEARNING_EDGE_TO_TOE_PEAK_DISTANCE_PERCENT_OF_FILLET_LENGTH_ALONG = new MeasurementEnum(++_index, "Heel to Toe Offset Distance for Learning");
  public final static MeasurementEnum QFN_LEARNING_UPWARD_CURVATURE_START_PERCENT_OF_FILLET_LENGTH_ALONG = new MeasurementEnum(++_index, "Upward Curvature Start Percent for Learning");
  public final static MeasurementEnum QFN_LEARNING_UPWARD_CURVATURE_END_PERCENT_OF_FILLET_LENGTH_ALONG = new MeasurementEnum(++_index, "Upward Curvature End Percent for Learning");
  public final static MeasurementEnum QFN_LEARNING_OPEN_SIGNAL = new MeasurementEnum(++_index, "Open Signal for Learning");
  public final static MeasurementEnum QFN_LEARNING_CENTER_OF_SOLDER_VOLUME = new MeasurementEnum(++_index, "Center of Solder Volume for Learning");

  // Exposed Pad Measurement
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_1 = new MeasurementEnum(++_index, "Inner Gap 1 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_1 = new MeasurementEnum(++_index, "Outer Gap 1 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_1 = new MeasurementEnum(++_index, "Inner % of Outer Gap 1 Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_1 = new MeasurementEnum(++_index, "Outer Gap 1 Thickness Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_1 = new MeasurementEnum(++_index, "Filled Gap 1 Thickness Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_2 = new MeasurementEnum(++_index, "Inner Gap 2 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_2 = new MeasurementEnum(++_index, "Outer Gap 2 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_2 = new MeasurementEnum(++_index, "Inner % of Outer Gap 2 Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_2 = new MeasurementEnum(++_index, "Outer Gap 2 Thickness Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_2 = new MeasurementEnum(++_index, "Filled Gap 2 Thickness Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_3 = new MeasurementEnum(++_index, "Inner Gap 3 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_3 = new MeasurementEnum(++_index, "Outer Gap 3 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_3 = new MeasurementEnum(++_index, "Inner % of Outer Gap 3 Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_3 = new MeasurementEnum(++_index, "Outer Gap 3 Thickness Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_3 = new MeasurementEnum(++_index, "Filled Gap 3 Thickness Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_NORTH_4 = new MeasurementEnum(++_index, "Inner Gap 4 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_NORTH_4 = new MeasurementEnum(++_index, "Outer Gap 4 Length Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_NORTH_4 = new MeasurementEnum(++_index, "Inner % of Outer Gap 4 Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_NORTH_4 = new MeasurementEnum(++_index, "Outer Gap 4 Thickness Along North");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_NORTH_4 = new MeasurementEnum(++_index, "Filled Gap 4 Thickness Along North");

  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_1 = new MeasurementEnum(++_index, "Inner Gap 1 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_1 = new MeasurementEnum(++_index, "Outer Gap 1 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_1 = new MeasurementEnum(++_index, "Inner % of Outer Gap 1 Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_1 = new MeasurementEnum(++_index, "Outer Gap 1 Thickness Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_1 = new MeasurementEnum(++_index, "Filled Gap 1 Thickness Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_2 = new MeasurementEnum(++_index, "Inner Gap 2 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_2 = new MeasurementEnum(++_index, "Outer Gap 2 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_2 = new MeasurementEnum(++_index, "Inner % of Outer Gap 2 Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_2 = new MeasurementEnum(++_index, "Outer Gap 2 Thickness Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_2 = new MeasurementEnum(++_index, "Filled Gap 2 Thickness Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_3 = new MeasurementEnum(++_index, "Inner Gap 3 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_3 = new MeasurementEnum(++_index, "Outer Gap 3 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_3 = new MeasurementEnum(++_index, "Inner % of Outer Gap 3 Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_3 = new MeasurementEnum(++_index, "Outer Gap 3 Thickness Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_3 = new MeasurementEnum(++_index, "Filled Gap 3 Thickness Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_MIDDLE_4 = new MeasurementEnum(++_index, "Inner Gap 4 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_MIDDLE_4 = new MeasurementEnum(++_index, "Outer Gap 4 Length Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_MIDDLE_4 = new MeasurementEnum(++_index, "Inner % of Outer Gap 4 Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_MIDDLE_4 = new MeasurementEnum(++_index, "Outer Gap 4 Thickness Along Middle");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_MIDDLE_4 = new MeasurementEnum(++_index, "Filled Gap 4 Thickness Along Middle");

  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_JOINT_OFFSET_FROM_CAD = new MeasurementEnum(++_index, "Joint Offset from CAD");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_1 = new MeasurementEnum(++_index, "Inner Gap 1 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_1 = new MeasurementEnum(++_index, "Outer Gap 1 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_1 = new MeasurementEnum(++_index, "Inner % of Outer Gap 1 Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_1 = new MeasurementEnum(++_index, "Outer Gap 1 Thickness Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_1 = new MeasurementEnum(++_index, "Filled Gap 1 Thickness Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_2 = new MeasurementEnum(++_index, "Inner Gap 2 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_2 = new MeasurementEnum(++_index, "Outer Gap 2 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_2 = new MeasurementEnum(++_index, "Inner % of Outer Gap 2 Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_2 = new MeasurementEnum(++_index, "Outer Gap 2 Thickness Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_2 = new MeasurementEnum(++_index, "Filled Gap 2 Thickness Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_3 = new MeasurementEnum(++_index, "Inner Gap 3 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_3 = new MeasurementEnum(++_index, "Outer Gap 3 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_3 = new MeasurementEnum(++_index, "Inner % of Outer Gap 3 Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_3 = new MeasurementEnum(++_index, "Outer Gap 3 Thickness Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_3 = new MeasurementEnum(++_index, "Filled Gap 3 Thickness Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ALONG_SOUTH_4 = new MeasurementEnum(++_index, "Inner Gap 4 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ALONG_SOUTH_4 = new MeasurementEnum(++_index, "Outer Gap 4 Length Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ALONG_SOUTH_4 = new MeasurementEnum(++_index, "Inner % of Outer Gap 4 Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ALONG_SOUTH_4 = new MeasurementEnum(++_index, "Outer Gap 4 Thickness Along South");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ALONG_SOUTH_4 = new MeasurementEnum(++_index, "Filled Gap 4 Thickness Along South");

  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_1 = new MeasurementEnum(++_index, "Inner Gap 1 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_1 = new MeasurementEnum(++_index, "Outer Gap 1 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_1 = new MeasurementEnum(++_index, "Inner % of Outer Gap 1 Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_1 = new MeasurementEnum(++_index, "Outer Gap 1 Thickness Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_1 = new MeasurementEnum(++_index, "Filled Gap 1 Thickness Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_2 = new MeasurementEnum(++_index, "Inner Gap 2 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_2 = new MeasurementEnum(++_index, "Outer Gap 2 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_2 = new MeasurementEnum(++_index, "Inner % of Outer Gap 2 Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_2 = new MeasurementEnum(++_index, "Outer Gap 2 Thickness Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_2 = new MeasurementEnum(++_index, "Filled Gap 2 Thickness Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_3 = new MeasurementEnum(++_index, "Inner Gap 3 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_3 = new MeasurementEnum(++_index, "Outer Gap 3 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_3 = new MeasurementEnum(++_index, "Inner % of Outer Gap 3 Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_3 = new MeasurementEnum(++_index, "Outer Gap 3 Thickness Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_3 = new MeasurementEnum(++_index, "Filled Gap 3 Thickness Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_WEST_4 = new MeasurementEnum(++_index, "Inner Gap 4 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_WEST_4 = new MeasurementEnum(++_index, "Outer Gap 4 Length Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_WEST_4 = new MeasurementEnum(++_index, "Inner % of Outer Gap 4 Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_WEST_4 = new MeasurementEnum(++_index, "Outer Gap 4 Thickness Across West");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_WEST_4 = new MeasurementEnum(++_index, "Filled Gap 4 Thickness Across West");

  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_1 = new MeasurementEnum(++_index, "Inner Gap 1 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_1 = new MeasurementEnum(++_index, "Outer Gap 1 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_1 = new MeasurementEnum(++_index, "Inner % of Outer Gap 1 Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_1 = new MeasurementEnum(++_index, "Outer Gap 1 Thickness Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_1 = new MeasurementEnum(++_index, "Filled Gap 1 Thickness Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_2 = new MeasurementEnum(++_index, "Inner Gap 2 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_2 = new MeasurementEnum(++_index, "Outer Gap 2 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_2 = new MeasurementEnum(++_index, "Inner % of Outer Gap 2 Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_2 = new MeasurementEnum(++_index, "Outer Gap 2 Thickness Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_2 = new MeasurementEnum(++_index, "Filled Gap 2 Thickness Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_3 = new MeasurementEnum(++_index, "Inner Gap 3 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_3 = new MeasurementEnum(++_index, "Outer Gap 3 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_3 = new MeasurementEnum(++_index, "Inner % of Outer Gap 3 Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_3 = new MeasurementEnum(++_index, "Outer Gap 3 Thickness Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_3 = new MeasurementEnum(++_index, "Filled Gap 3 Thickness Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_CENTER_4 = new MeasurementEnum(++_index, "Inner Gap 4 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_CENTER_4 = new MeasurementEnum(++_index, "Outer Gap 4 Length Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_CENTER_4 = new MeasurementEnum(++_index, "Inner % of Outer Gap 4 Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_CENTER_4 = new MeasurementEnum(++_index, "Outer Gap 4 Thickness Across Center");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_CENTER_4 = new MeasurementEnum(++_index, "Filled Gap 4 Thickness Across Center");

  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_1 = new MeasurementEnum(++_index, "Inner Gap 1 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_1 = new MeasurementEnum(++_index, "Outer Gap 1 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_1 = new MeasurementEnum(++_index, "Inner % of Outer Gap 1 Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_1 = new MeasurementEnum(++_index, "Outer Gap 1 Thickness Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_1 = new MeasurementEnum(++_index, "Filled Gap 1 Thickness Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_2 = new MeasurementEnum(++_index, "Inner Gap 2 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_2 = new MeasurementEnum(++_index, "Outer Gap 2 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_2 = new MeasurementEnum(++_index, "Inner % of Outer Gap 2 Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_2 = new MeasurementEnum(++_index, "Outer Gap 2 Thickness Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_2 = new MeasurementEnum(++_index, "Filled Gap 2 Thickness Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_3 = new MeasurementEnum(++_index, "Inner Gap 3 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_3 = new MeasurementEnum(++_index, "Outer Gap 3 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_3 = new MeasurementEnum(++_index, "Inner % of Outer Gap 3 Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_3 = new MeasurementEnum(++_index, "Outer Gap 3 Thickness Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_3 = new MeasurementEnum(++_index, "Filled Gap 3 Thickness Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_INNER_GAP_ACROSS_EAST_4 = new MeasurementEnum(++_index, "Inner Gap 4 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_OUTER_GAP_ACROSS_EAST_4 = new MeasurementEnum(++_index, "Outer Gap 4 Length Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_RATIO_ACROSS_EAST_4 = new MeasurementEnum(++_index, "Inner % of Outer Gap 4 Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_GAP_THICKNESS_ACROSS_EAST_4 = new MeasurementEnum(++_index, "Outer Gap 4 Thickness Across East");
  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_FILLED_GAP_THICKNESS_ACROSS_EAST_4 = new MeasurementEnum(++_index, "Filled Gap 4 Thickness Across East");

  public final static MeasurementEnum EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE = new MeasurementEnum(++_index, "Average Background Value");
  // Exposed Pad Open
  public final static MeasurementEnum EXPOSED_PAD_OPEN_NUMBER_FAILING_GAPS = new MeasurementEnum(++_index, "Number of Failing Gaps");

  // Exposed Pad Voiding
  public final static MeasurementEnum EXPOSED_PAD_VOIDING_PERCENT = new MeasurementEnum(++_index, "Void Area");
  public final static MeasurementEnum EXPOSED_PAD_INDIVIDUAL_VOIDING_PERCENT = new MeasurementEnum(++_index, "Individual Void Area");

  // Calibration Measurement
  public final static MeasurementEnum CALIBRATION_GRAY_LEVEL = new MeasurementEnum(++_index, "Graylevel");
  
  //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
  public final static MeasurementEnum IMAGE_RESIZE_SCALE = new MeasurementEnum(++_index, "Image Resize Scale");

  // All
  public final static MeasurementEnum ALL_MEASUREMENT = new MeasurementEnum(++_index, "All");

  // Set up a list of non customer visible measurements.
  private static List<MeasurementEnum> _nonCustomerVisibleMeasurements = new ArrayList<MeasurementEnum>();
  static
  {
    _nonCustomerVisibleMeasurements.add(LOCATOR_X_LOCATION);
    _nonCustomerVisibleMeasurements.add(LOCATOR_Y_LOCATION);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.GULLWING_CENTER_THICKNESS);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_HEEL_THICKNESS);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.ADVANCED_GULLWING_ACROSS_PROFILE_CENTER_THICKNESS);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.PCAP_MEASUREMENT_PAD_ONE_LOWER_FILLET_THICKNESS);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.PCAP_MEASUREMENT_PAD_TWO_LOWER_FILLET_THICKNESS);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.CHIP_MEASUREMENT_LOWER_FILLET_THICKNESS);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.GRID_ARRAY_JOINT_X_OFFSET_FROM_CAD);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.GRID_ARRAY_JOINT_Y_OFFSET_FROM_CAD);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.GRID_ARRAY_DIAMETER_DIFFERENCE_FROM_NEIGHBORS);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.GULLWING_FILLET_PEAK_POSITION);
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.EXPOSED_PAD_MEASUREMENT_AVERAGE_BACKGROUND_VALUE);
    //Khaw Chek Hau - XCR-3800 : CAD locator's location is incorrect if "Resize" algorithm setting is only turned on after "Run Test"
    _nonCustomerVisibleMeasurements.add(MeasurementEnum.IMAGE_RESIZE_SCALE);
  }

  private String _name;

  /**
   * @author Sunit Bhalla
   */
  private MeasurementEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
    _name = name.intern();
  }

  /**
   * @author Peter Esbensen
   */
  public int compareTo(Object object)
  {
    MeasurementEnum measurementEnum = (MeasurementEnum)object;

    return measurementEnum.toString().compareTo(toString());
  }

  /**
   * @author Sunit Bhalla
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Sunit Bhalla
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Rex Shang
   */
  public static List<MeasurementEnum> getNonCustomerVisibleMeasurements()
  {
    return _nonCustomerVisibleMeasurements;
  }
}
