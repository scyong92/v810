package com.axi.v810.business.testResults;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.gridArray.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Patrick Lacz
 */
public class Test_JointInspectionResult extends UnitTest
{
  public Test_JointInspectionResult()
  {
  }

  /**
 * @author Patrick Lacz
   */
  public static void main(String[] args)
  {
//    UnitTest.execute(new Test_JointInspectionResult());
  }

  /**
   * @author Patrick Lacz
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    testMeasurements();

    // Indictments
    JointInspectionResult jointInspectionResult = new JointInspectionResult(new JointInspectionData(new Pad()));
    Expect.expect(jointInspectionResult.passed());
    Expect.expect(jointInspectionResult.getIndictments().isEmpty());

    SliceNameEnum sliceNameEnum = SliceNameEnum.PACKAGE;

    JointIndictment indictment = new JointIndictment(IndictmentEnum.INSUFFICIENT,
        new GridArrayVoidingAlgorithm(new GridArrayInspectionFamily()), sliceNameEnum);
    jointInspectionResult.addIndictment(indictment);

    Expect.expect(jointInspectionResult.passed() == false);
    Expect.expect(jointInspectionResult.getIndictments().size() == 1);
    Expect.expect(sliceNameEnum.equals(indictment.getSliceNameEnum()));
  }

  /**
   * @author Patrick Lacz
   */
  private void testMeasurements()
  {
    JointInspectionResult jointInspectionResult = new JointInspectionResult(
      new JointInspectionData(new Pad()));

    boolean hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(SliceNameEnum.PAD,
        MeasurementEnum.GRID_ARRAY_DIAMETER);
    Expect.expect(hasUnpopulatedMeasurement == false);

    Expect.expect(jointInspectionResult.getAllMeasurements().isEmpty());

    Algorithm algorithm = new GridArrayVoidingAlgorithm(new GridArrayInspectionFamily());
    MeasurementEnum measurementEnum = MeasurementEnum.GRID_ARRAY_VOIDING_PERCENT;
    SliceNameEnum sliceName = SliceNameEnum.PAD;
    JointMeasurement measurement = new JointMeasurement(algorithm, measurementEnum, MeasurementUnitsEnum.MILS,
        new Pad(), sliceName, 0.0f);
    jointInspectionResult.addMeasurement(measurement);

    boolean hasPopulatedMeasurement = jointInspectionResult.hasJointMeasurement(sliceName, measurementEnum);
    Expect.expect(hasPopulatedMeasurement == true);

    SliceNameEnum differentSliceName = SliceNameEnum.OPAQUE_CHIP_PAD;
    hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(differentSliceName, measurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    MeasurementEnum differentMeasurementEnum = MeasurementEnum.SHORT_THICKNESS;
    hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(sliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(differentSliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    JointMeasurement returnMeasurement = jointInspectionResult.getJointMeasurement(sliceName, measurementEnum);
    Expect.expect(returnMeasurement.equals(measurement));

    try {
      returnMeasurement = jointInspectionResult.getJointMeasurement(differentSliceName, measurementEnum);
      Expect.expect(false);
    }
    catch (AssertException ae)
    {
      // expected, do nothing.
    }

    Expect.expect(jointInspectionResult.getAllMeasurements().size() == 1);

    jointInspectionResult.clearMeasurements();

    Expect.expect(jointInspectionResult.getAllMeasurements().isEmpty());

    hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(sliceName, measurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(differentSliceName, measurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(sliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);

    hasUnpopulatedMeasurement = jointInspectionResult.hasJointMeasurement(differentSliceName, differentMeasurementEnum);
    Expect.expect(hasUnpopulatedMeasurement == false);
  }
}
