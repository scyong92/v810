package com.axi.v810.business.testResults;

import java.util.*;
import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * @author Patrick Lacz
 */
public class ComponentMeasurement
{
  private MeasurementEnum _measurementEnum;
  private MeasurementUnitsEnum _measurementUnitsEnum;
  private Algorithm _algorithm;
  private float _measurementValue;
  private SliceNameEnum _sliceNameEnum;
  private Subtype _subtype;
  private Component _component = null;
  private boolean _measurementValid;
  private transient long _id;

  // Unique id counter for the joint measurement.
  private static AtomicLong _idCounter = new AtomicLong(0);

  private List<Subtype> _subtypes = new ArrayList(); //Siew Yeng
//  private int _zHeightInNanoMeters;

  /**
   * @author Patrick Lacz
   */
  public ComponentMeasurement(Algorithm algorithm,
                              Subtype subtype,
                              MeasurementEnum measurementEnum,
                              MeasurementUnitsEnum measurementUnitsEnum,
                              Component component,
                              SliceNameEnum sliceNameEnum,
                              float measurementValue)
  {
    this(algorithm, subtype, measurementEnum, measurementUnitsEnum, component, sliceNameEnum, measurementValue, true);
  }

  /**
   * @author Patrick Lacz
   */
  public ComponentMeasurement(Algorithm algorithm,
                              Subtype subtype,
                              MeasurementEnum measurementEnum,
                              MeasurementUnitsEnum measurementUnitsEnum,
                              Component component,
                              SliceNameEnum sliceNameEnum,
                              float measurementValue,
                              boolean isMeasurementValid)
  {
    Assert.expect(algorithm != null);
    Assert.expect(measurementEnum != null);
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(component != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(algorithm.getComponentMeasurementEnums().contains(measurementEnum), "algo developer forgot to add this measurement to the enum list");

    _algorithm = algorithm;
//    _subtype = subtype;
    _subtypes.add(subtype);
    _measurementEnum = measurementEnum;
    _measurementUnitsEnum = measurementUnitsEnum;
    _component = component;
    _sliceNameEnum = sliceNameEnum;
    _measurementValue = measurementValue;
    _measurementValid = isMeasurementValid;
    _id = _idCounter.getAndIncrement();
  }

  /**
   * @author Siew Yeng
   */
  public ComponentMeasurement(Algorithm algorithm,
                              List<Subtype> subtypes,
                              MeasurementEnum measurementEnum,
                              MeasurementUnitsEnum measurementUnitsEnum,
                              Component component,
                              SliceNameEnum sliceNameEnum,
                              float measurementValue)
  {
    Assert.expect(algorithm != null);
    Assert.expect(measurementEnum != null);
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(component != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtypes != null);
    Assert.expect(algorithm.getComponentMeasurementEnums().contains(measurementEnum), "algo developer forgot to add this measurement to the enum list");

    _algorithm = algorithm;
    _subtypes.addAll(subtypes);
    _measurementEnum = measurementEnum;
    _measurementUnitsEnum = measurementUnitsEnum;
    _component = component;
    _sliceNameEnum = sliceNameEnum;
    _measurementValue = measurementValue;
    _measurementValid = true;
    _id = _idCounter.getAndIncrement();
  }
  
  /**
   * @author Siew Yeng
   */
  public ComponentMeasurement(Algorithm algorithm,
                              List<Subtype> subtypes,
                              MeasurementEnum measurementEnum,
                              MeasurementUnitsEnum measurementUnitsEnum,
                              Component component,
                              SliceNameEnum sliceNameEnum,
                              float measurementValue,
                              boolean isMeasurementValid)
  {
    Assert.expect(algorithm != null);
    Assert.expect(measurementEnum != null);
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(component != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtypes != null);
    Assert.expect(algorithm.getComponentMeasurementEnums().contains(measurementEnum), "algo developer forgot to add this measurement to the enum list");

    _algorithm = algorithm;
    _subtypes.addAll(subtypes);
    _measurementEnum = measurementEnum;
    _measurementUnitsEnum = measurementUnitsEnum;
    _component = component;
    _sliceNameEnum = sliceNameEnum;
    _measurementValue = measurementValue;
    _measurementValid = isMeasurementValid;
    _id = _idCounter.getAndIncrement();
  }
  
  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public static void resetIdCounter()
  {
    _idCounter.set(0);
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public long getId()
  {
    return _id;
  }

  /**
   * @author Peter Esbensen
   */
  public String getMeasurementName()
  {
    Assert.expect(_measurementEnum != null);
    return _measurementEnum.getName();
  }

  /**
   * @author Peter Esbensen
   */
  public float getValue()
  {
    return _measurementValue;
  }

  /**
   * @author Peter Esbensen
   */
  public void setValue(float value)
  {
    _measurementValue = value;
  }

  /**
   * @author Sunit Bhalla
   */
  public boolean isMeasurementValid()
  {
    return _measurementValid;
  }

  /**
   * @author Sunit Bhalla
   */
  public void setMeasurementValid(boolean measurementValid)
  {
    _measurementValid = measurementValid;
  }

  /**
   * @author Patrick Lacz
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public MeasurementEnum getMeasurementEnum()
  {
    Assert.expect(_measurementEnum != null);
    return _measurementEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public void setMeasurementEnum(MeasurementEnum measurementEnum)
  {
    Assert.expect(measurementEnum != null);
    _measurementEnum = measurementEnum;
  }

  /**
   * @author Matt Wharton
   */
  public Algorithm getAlgorithm()
  {
    Assert.expect(_algorithm != null);

    return _algorithm;
  }

  /**
   * @author Matt Wharton
   */
  public void setAlgorithm(Algorithm algorithm)
  {
    Assert.expect(algorithm != null);

    _algorithm = algorithm;
  }

  /**
   * @author Matt Wharton
   */
  public MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    Assert.expect(_measurementUnitsEnum != null);

    return _measurementUnitsEnum;
  }

  /**
   * @author Matt Wharton
   */
  public void setMeasurementUnitsEnum(MeasurementUnitsEnum measurementUnitsEnum)
  {
    Assert.expect(measurementUnitsEnum != null);

    _measurementUnitsEnum = measurementUnitsEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public Component getComponent()
  {
    Assert.expect(_component != null);
    return _component;
  }

  /**
   * Return the subtype associatied to the measurement. A component
   * is not specificly tied to a subtype, so this is done on the measurement level.
   * @author Patrick Lacz
   */
  public Subtype getSubtype()
  {
//    Assert.expect(_subtype != null);
    //Siew Yeng - temporary fix - XCR-2168
    //          - will need to handle InspectionResultsReader and Writer for multiple subtypes in future.
    if(_subtype == null)
      return _subtypes.get(0);
    return _subtype;
  }

  /**
   * @author Siew Yeng
   */
  public List<Subtype> getSubtypes()
  {
    Assert.expect(_subtypes != null);
    
    return _subtypes;
  }
  
// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
  /**
   * @param zHeightInNanoMeters int
   * @author Wei Chin, Chong
   */
//  public void setZHeightInNanoMeters(int zHeightInNanoMeters)
//  {
//    _zHeightInNanoMeters = zHeightInNanoMeters;
//  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
//  public int getZHeightInNanoMeters()
//  {
//    return _zHeightInNanoMeters;
//  }

}
