package com.axi.v810.business.testResults;

import java.io.*;
import java.math.*;
import java.util.*;
import com.axi.util.*;

/**
 * This class will compare any two JointInspectionResults.  It will alphanumerically by the name of the pad
 *
 * a1
 * A2
 * a3
 * a12
 * B94
 * B100
 *
 * @author Laura Cormos
 */
public class JointInspectionResultComparator implements Comparator<JointInspectionResult>, Serializable
{
  boolean _ascending;
  private AlphaNumericComparator _alphaNumericComparator;


  /**
   * @author Andy Mechtenberg
   */
  public JointInspectionResultComparator(boolean ascending)
  {
    _ascending = ascending;
    _alphaNumericComparator = new AlphaNumericComparator(ascending);
  }

  /**
   * @author Andy Mechtenberg
   */
  public JointInspectionResultComparator()
  {
    _ascending = true;
    _alphaNumericComparator = new AlphaNumericComparator(true);
  }


  /**
   * Implementation of the 'compare' method in the Comparator interface.  This method assumes that the Strings
   * being compared follow the regular expression pattern of (<letter>*<digit>*)*
   *
   * The <letter> portions are compared alphabetically.  If they are the same, the numeric portions are compared as
   * integers.  The process repeats recursively until one of the comparisons is unequal.
   *
   * @param lhs first String being compared
   * @param rhs second String being compared
   * @return -1 if the first String is less than the second, 0 if the two Strings are equal, 1 if the first String is
   * greater than the second
   * @author Matt Wharton
   */
  public int compare( JointInspectionResult lhs, JointInspectionResult rhs )
  {
    Assert.expect( lhs != null );
    Assert.expect( rhs != null );

    String firstString = "";
    String secondString = "";

    if ( _ascending == true )
    {
      firstString = lhs.getJointInspectionData().getPad().getName();
      secondString = rhs.getJointInspectionData().getPad().getName();
    }
    else
    {
      firstString = rhs.getJointInspectionData().getPad().getName();
      secondString = lhs.getJointInspectionData().getPad().getName();
    }

    // Remove any leading or trailing spaces.
    firstString = firstString.trim();
    secondString = secondString.trim();

    return _alphaNumericComparator.compare(firstString, secondString);
  }

}
