package com.axi.v810.business.testResults;

import java.util.concurrent.atomic.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testProgram.*;

/**
 * A single measurement for a specific joint.  It basically consists of a name
 * and a floating-point value.
 *
 * @author Peter Esbensen
 */
public class JointMeasurement
{
  private Pad _pad;
  private SliceNameEnum _sliceNameEnum;

  private MeasurementEnum _measurementEnum;
  private MeasurementUnitsEnum _measurementUnitsEnum;
  private Algorithm _algorithm;
  private float _measurementValue;
  private boolean _measurementValid;
  private long _id;

  // Unique id counter for the joint measurement.
  private static AtomicLong _idCounter = new AtomicLong(0);
//
//  private int _zHeightInNanoMeters;

  /**
   * Create a new JointMeasurement with the given name and value.
   * @author Peter Esbensen
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public JointMeasurement(Algorithm algorithm,
                          MeasurementEnum measurementEnum,
                          MeasurementUnitsEnum measurementUnitsEnum,
                          Pad pad,
                          SliceNameEnum sliceNameEnum,
                          float measurementValue)
  {
    this(algorithm, measurementEnum, measurementUnitsEnum, pad, sliceNameEnum, measurementValue, true);
  }

  /**
   * Create a new JointMeasurement with the given name and value.  An additional parameter for this constructor
   * indicates whether the measurement is valid.
   * @author Peter Esbensen
   * @author Patrick Lacz
   * @author Sunit Bhalla
   * @author Matt Wharton
   */
  public JointMeasurement(Algorithm algorithm,
                          MeasurementEnum measurementEnum,
                          MeasurementUnitsEnum measurementUnitsEnum,
                          Pad pad,
                          SliceNameEnum sliceNameEnum,
                          float measurementValue,
                          boolean measurementValid)
  {
    Assert.expect(algorithm != null);
    Assert.expect(measurementEnum != null);
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(pad != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(algorithm.getJointMeasurementEnums().contains(measurementEnum), "need to add this measurement to the enum list: " +
        measurementEnum.getName());

    _algorithm = algorithm;
    _measurementEnum = measurementEnum;
    _measurementUnitsEnum = measurementUnitsEnum;
    _pad = pad;
    _sliceNameEnum = sliceNameEnum;
    _measurementValue = measurementValue;
    _measurementValid = measurementValid;
    _id = _idCounter.getAndIncrement();
  }

  /**
   * @author Peter Esbensen
   */
  private JointMeasurement()
  {
    // do nothing
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public static void resetIdCounter()
  {
    _idCounter.set(0);
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public long getId()
  {
    return _id;
  }

  /**
   * @author Peter Esbensen
   */
  public String getMeasurementName()
  {
    Assert.expect(_measurementEnum != null);
    return _measurementEnum.getName();
  }

  /**
   * @author Peter Esbensen
   */
  public float getValue()
  {
    return _measurementValue;
  }

  /**
   * @author Peter Esbensen
   */
  public void setValue(float value)
  {
    _measurementValue = value;
  }

  /**
   * @author Sunit Bhalla
   */
  public boolean isMeasurementValid()
  {
    return _measurementValid;
  }

  /**
   * @author Sunit Bhalla
   */
  public void setMeasurementValid(boolean measurementValid)
  {
    _measurementValid = measurementValid;
  }


  /**
   * @author Peter Esbensen
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author Patrick Lacz
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author Patrick Lacz
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public MeasurementEnum getMeasurementEnum()
  {
    Assert.expect(_measurementEnum != null);
    return _measurementEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public void setMeasurementEnum(MeasurementEnum measurementEnum)
  {
    Assert.expect(measurementEnum != null);
    _measurementEnum = measurementEnum;
  }

  /**
   * @author Matt Wharton
   */
  public Algorithm getAlgorithm()
  {
    Assert.expect(_algorithm != null);

    return _algorithm;
  }

  /**
   * @author Matt Wharton
   */
  public void setAlgorithm(Algorithm algorithm)
  {
    Assert.expect(algorithm != null);

    _algorithm = algorithm;
  }

  /**
   * @author Matt Wharton
   */
  public MeasurementUnitsEnum getMeasurementUnitsEnum()
  {
    Assert.expect(_measurementUnitsEnum != null);

    return _measurementUnitsEnum;
  }

  /**
   * @author Matt Wharton
   */
  public void setMeasurementUnitsEnum(MeasurementUnitsEnum measurementUnitsEnum)
  {
    Assert.expect(measurementUnitsEnum != null);

    _measurementUnitsEnum = measurementUnitsEnum;
  }

// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
  /**
   * @param zHeightInNanoMeters int
   * @author Wei Chin, Chong
   */
//  public void setZHeightInNanoMeters(int zHeightInNanoMeters)
//  {
//    _zHeightInNanoMeters = zHeightInNanoMeters;
//  }

  /**
   * @return int
   * @author Wei Chin, Chong
   */
//  public int getZHeightInNanoMeters()
//  {
//    return _zHeightInNanoMeters;
//  }
}
