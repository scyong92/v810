package com.axi.v810.business.testResults;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;

/**
 * Holds all the results (including measurements and indictments) produced by
 * the Image Analysis Subsystem for a certain Joint.
 *
 * @see JointMeasurement
 * @see JointIndictment
 *
 * @author Peter Esbensen
 */
public class JointInspectionResult
{
  private Collection<JointIndictment> _indictments = new ArrayList<JointIndictment>();
  private Map<Pair<SliceNameEnum, MeasurementEnum>, List<JointMeasurement>>
      _sliceEnumAndMeasurementNamePairToMeasurementListMap = new HashMap<Pair<SliceNameEnum, MeasurementEnum>, List<JointMeasurement>>();
  private Map<Long, JointMeasurement> _uniqueMeasurementIdToMeasurementMap = new HashMap<Long,JointMeasurement>();
  private JointInspectionData _jointInspectionData = null;
  private transient int _inspectionRunId;

  private static InspectionEventObservable _inspectionEventObservable;
  
  private ImageCoordinate _imageCoordinate = null;

//  private Map<Integer, ReconstructedSlice> _sliceIdToReconstructedSliceMap;
  /**
   * @author Bill Darbie
   */
  static
  {
    _inspectionEventObservable = InspectionEventObservable.getInstance();
  }

  /**
   * @author Peter Esbensen
   */
  public JointInspectionResult(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);
    _jointInspectionData = jointInspectionData;
//    _sliceIdToReconstructedSliceMap = new TreeMap<Integer, ReconstructedSlice>();
  }

  /**
   * @return true if the Joint passed, no defects were found
   * @author Peter Esbensen
   */
  public boolean passed()
  {
    Assert.expect(_indictments != null);
    
    //Siew Yeng - XCR1745 - Semi-automated mode
    if(TestExecution.getInstance().isSemiAutomatedModeEnabled())
    {
      List<SliceNameEnum> slices = new ArrayList(_jointInspectionData.getInspectionRegion().getInspectedSliceNames(_jointInspectionData.getSubtype()));
      for(SliceNameEnum sliceEnum : slices)
      {
        if(hasIndictment(sliceEnum))
          continue;
        
        JointMeasurement jointMeasurement = getJointMeasurement(sliceEnum);
        
        if(jointMeasurement == null)
        {
          continue;
        }
        
        JointIndictment fakeIndictment = new JointIndictment(IndictmentEnum.DUMMY, 
                                                           jointMeasurement.getAlgorithm(), 
                                                           sliceEnum);

        fakeIndictment.addFailingMeasurement(jointMeasurement);
        addIndictment(fakeIndictment);
      }
    }
    if (_indictments.isEmpty() == false)
      return false;
    return true;
  }

  /**
   * @return all measurements for this Joint
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public synchronized Collection<JointMeasurement> getAllMeasurements()
  {
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    List<JointMeasurement> measurementsForJoint = new LinkedList<JointMeasurement>();
    for (List<JointMeasurement> measurementsForSingleMeasurementEnumList :
         _sliceEnumAndMeasurementNamePairToMeasurementListMap.values())
    {
      measurementsForJoint.addAll(measurementsForSingleMeasurementEnumList);
    }

    return measurementsForJoint;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized boolean hasJointMeasurement(SliceNameEnum sliceNameEnum, MeasurementEnum measurementName)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementName != null);
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);

    Pair measurementMapKey = new Pair<SliceNameEnum, MeasurementEnum>(sliceNameEnum, measurementName);
    return _sliceEnumAndMeasurementNamePairToMeasurementListMap.containsKey(measurementMapKey);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized JointMeasurement getJointMeasurement(SliceNameEnum sliceNameEnum,
                                                           MeasurementEnum jointMeasurement)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointMeasurement != null);

    List<JointMeasurement> measurementList = getJointMeasurementList(sliceNameEnum, jointMeasurement);
    JointMeasurement measurement = measurementList.get(0);
    Assert.expect(measurement != null);
    return measurement;
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public synchronized List<JointMeasurement> getJointMeasurementList(SliceNameEnum sliceNameEnum,
                                                                     MeasurementEnum jointMeasurement)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointMeasurement != null);
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    Pair<SliceNameEnum, MeasurementEnum> measurementMapKey = new Pair<SliceNameEnum, MeasurementEnum>(sliceNameEnum, jointMeasurement);

    List<JointMeasurement> measurementList = _sliceEnumAndMeasurementNamePairToMeasurementListMap.get(measurementMapKey);
    Assert.expect(measurementList != null);
    Assert.expect(measurementList.isEmpty() == false);

    return measurementList;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void addMeasurement(JointMeasurement measurement)
  {
    Assert.expect(measurement != null);
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    Assert.expect(_uniqueMeasurementIdToMeasurementMap != null);

    SliceNameEnum sliceNameEnum = measurement.getSliceNameEnum();
    MeasurementEnum measurementName = measurement.getMeasurementEnum();

    Pair<SliceNameEnum, MeasurementEnum> measurementMapKey = new Pair<SliceNameEnum, MeasurementEnum>(sliceNameEnum, measurementName);

    Assert.expect(_uniqueMeasurementIdToMeasurementMap.containsKey(measurement.getId()) == false);

    // See if we already have a measurement list for this key.
    List<JointMeasurement> measurementList = _sliceEnumAndMeasurementNamePairToMeasurementListMap.get(measurementMapKey);
    if (measurementList == null)
    {
      measurementList = new LinkedList<JointMeasurement>();
      _sliceEnumAndMeasurementNamePairToMeasurementListMap.put(measurementMapKey, measurementList);
    }
    
    Assert.expect(measurementList != null);
    measurementList.add(measurement);
    _uniqueMeasurementIdToMeasurementMap.put(measurement.getId(), measurement);
  }

  /**
   * @return all Indictments for this Joint
   * @author Peter Esbensen
   */
  public synchronized Collection<JointIndictment> getIndictments()
  {
    Assert.expect(_indictments != null);
    return _indictments;
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz : added Asserts to avoid a difficult to determine bug.
   */
  public synchronized void addIndictment(JointIndictment indictment)
  {
    Assert.expect(indictment != null);
    _indictments.add(indictment);

    _inspectionEventObservable.sendEventInfo(new JointOrComponentFailureInspectionEvent());

    // check that the measurements are there.
    for (JointMeasurement measurement : indictment.getFailingMeasurements())
    {
      Assert.expect(_uniqueMeasurementIdToMeasurementMap.containsKey(measurement.getId()),
                    "Trying to add " + indictment.getIndictmentName() + " containing non-existent measurement " + measurement.getMeasurementName());
    }
    for (JointMeasurement measurement : indictment.getRelatedMeasurements())
    {
      Assert.expect(_uniqueMeasurementIdToMeasurementMap.containsKey(measurement.getId()),
                    "Trying to add " + indictment.getIndictmentName() + " containing non-existent related measurement " + measurement.getMeasurementName());
    }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public synchronized JointIndictment getFailingIndictment(IndictmentEnum indictmentEnum,
                                                           Algorithm algorithm,
                                                           SliceNameEnum sliceNameEnum,
                                                           JointMeasurement jointMeasurement)
  {
    Assert.expect(indictmentEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(jointMeasurement != null);
    Assert.expect(_indictments != null);
    
    JointIndictment jointIndictment = null;
    for(JointIndictment indictment : _indictments)
    {
        if (indictment.getIndictmentEnum().equals(indictmentEnum) &&
            indictment.getAlgorithm().equals(algorithm) &&
            indictment.getSliceNameEnum().equals(sliceNameEnum) &&
            indictment.getFailingMeasurements().contains(jointMeasurement))
        {
            jointIndictment = indictment;
            break;
        }
    }
    return jointIndictment;
  }

  /**
   * @author Peter Esbensen
   */
  public JointInspectionData getJointInspectionData()
  {
    Assert.expect(_jointInspectionData != null);
    return _jointInspectionData;
  }

  /**
   * @author Peter Esbensen
   */
  public void setJointInspectionData(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);
    _jointInspectionData = jointInspectionData;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void clearMeasurements()
  {
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    Assert.expect(_uniqueMeasurementIdToMeasurementMap != null);

    _sliceEnumAndMeasurementNamePairToMeasurementListMap.clear();
    _uniqueMeasurementIdToMeasurementMap.clear();
  }

  /**
   * @author Peter Esbensen
   */
  public int getInspectionRunId()
  {
    return _inspectionRunId;
  }

  /**
   * @author Peter Esbensen
   */
  public void setInspectionRunId(int inspectionRunId)
  {
    _inspectionRunId = inspectionRunId;
  }

// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
  /**
   * @param sliceIdToReconstructedSliceMap Map
   * @author Wei Chin, Chong
   */
//  public void setSliceIdToReconstructedSliceMap(Map<Integer, ReconstructedSlice> sliceIdToReconstructedSliceMap)
//  {
//    Assert.expect(sliceIdToReconstructedSliceMap != null);
//    _sliceIdToReconstructedSliceMap = sliceIdToReconstructedSliceMap;
//  }

  /**
   * @author Wei Chin, Chong
   */
//  public Map<Integer, ReconstructedSlice> getSliceIdToReconstructedSliceMap()
//  {
//    Assert.expect(_sliceIdToReconstructedSliceMap != null);
//    return _sliceIdToReconstructedSliceMap;
//  }

  /**
   * @author Wei Chin
   */
//  public ReconstructedSlice getReconstructedSlice(SliceNameEnum sliceName)
//  {
//    Assert.expect(_sliceIdToReconstructedSliceMap != null);
//    Assert.expect(sliceName != null);
//    ReconstructedSlice slice = _sliceIdToReconstructedSliceMap.get(sliceName.getId());
//    Assert.expect(slice != null, "Failure to find slice: " + sliceName.getId() + " " + sliceName.getName());
//    return slice;
//  }
  
  /**
   * Check if indictment exist for that slice not including indictment that excluded from retest.
   * @author Siew Yeng
   */
  public synchronized boolean hasIndictment(SliceNameEnum sliceNameEnum)
  {
    //Siew Yeng - XCR-2015
    Collection<IndictmentEnum> indictmentEnumsToExcludeFromRetest = FocusConfirmationEngine.getInstance().getExcludedIndictmentEnumsToExcludeFromRetest();
    
    for(JointIndictment indictment : _indictments)
    {
      if(indictmentEnumsToExcludeFromRetest.contains(indictment.getIndictmentEnum()))
        continue;
      
      if(indictment.getSliceNameEnum() == sliceNameEnum)
        return true;
    }
    return false;
  }
  
   /**
   * @author Siew Yeng
   */
  public synchronized JointMeasurement getJointMeasurement(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);

    JointMeasurement jointMeasurement = null;
    for(Pair<SliceNameEnum, MeasurementEnum> measurementMapKey : _sliceEnumAndMeasurementNamePairToMeasurementListMap.keySet())
    {
       if(measurementMapKey.getFirst() == sliceNameEnum)
        {
          jointMeasurement = _sliceEnumAndMeasurementNamePairToMeasurementListMap.get(measurementMapKey).get(0);
          break;
        }
    }
    
    return jointMeasurement;
  }
  
  /**
   * @XCR-3014
   * @author sheng chuan
   */
  public void setOrigImageLocation(ImageCoordinate imageCoordinate)
  {
    _imageCoordinate = imageCoordinate;
  }
  
  /**
   * @XCR-3014
   * @author sheng chuan
   */
  public ImageCoordinate getOrigImageLocation()
  {
    return _imageCoordinate;
  }
  
  /**
   * @XCR-3014
   * @author sheng chuan
   */
  public boolean hasOrigImageLocation()
  {
    if(_imageCoordinate == null)
      return false;
    else
      return true;
  }
}
