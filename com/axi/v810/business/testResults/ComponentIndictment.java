package com.axi.v810.business.testResults;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;

/**
 * See the JointIndictment object.
 *
 * The Component Indictment is new to genesis - on the 5dx, component-level
 * indictments were placed on the first pad of the component.
 *
 * @author Patrick Lacz
 * @author Matt Wharton
 */
public class ComponentIndictment
{
  private Algorithm _algorithm = null;

  private IndictmentEnum _indictmentEnum = null;
  private Collection<ComponentMeasurement> _relatedMeasurements = new ArrayList<ComponentMeasurement>();
  private Collection<ComponentMeasurement> _failingMeasurements = new ArrayList<ComponentMeasurement>();
  private SliceNameEnum _sliceNameEnum;

  private Subtype _subtype;
  private JointTypeEnum _jointTypeEnum;
  private List<Subtype> _subtypes = new ArrayList(); //Siew Yeng

  /**
   * @author Patrick Lacz
   */
  public ComponentIndictment(IndictmentEnum indictmentEnum,
                             Algorithm algorithm,
                             SliceNameEnum sliceNameEnum,
                             Subtype subtype,
                             JointTypeEnum jointTypeEnum)  /** @todo PE simplify API by getting rid of jointTypeEnum here since we can just pull it from subtype */
  {
    Assert.expect(indictmentEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtype != null);
    Assert.expect(jointTypeEnum != null);

    _algorithm = algorithm;
    _indictmentEnum = indictmentEnum;
    _sliceNameEnum = sliceNameEnum;
    //_subtype = subtype;
    _subtypes.add(subtype);
    _jointTypeEnum = jointTypeEnum;
  }
  
  /**
   * @author Siew Yeng
   */
  public ComponentIndictment(IndictmentEnum indictmentEnum,
                             Algorithm algorithm,
                             SliceNameEnum sliceNameEnum,
                             List<Subtype> subtypes,
                             JointTypeEnum jointTypeEnum)  /** @todo PE simplify API by getting rid of jointTypeEnum here since we can just pull it from subtype */
  {
    Assert.expect(indictmentEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(sliceNameEnum != null);
    Assert.expect(subtypes != null);
    Assert.expect(jointTypeEnum != null);

    _algorithm = algorithm;
    _indictmentEnum = indictmentEnum;
    _sliceNameEnum = sliceNameEnum;
    _subtypes = subtypes;
    _jointTypeEnum = jointTypeEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public void setAlgorithm(Algorithm algorithm)
  {
    Assert.expect(algorithm != null);
    _algorithm = algorithm;
  }

  /**
   * @author Peter Esbensen
   */
  public Algorithm getAlgorithm()
  {
    Assert.expect(_algorithm != null);
    return _algorithm;
  }

  /**
   * @author Sunit Bhalla
   */
  public IndictmentEnum getIndictmentEnum()
  {
    Assert.expect(_indictmentEnum != null);
    return _indictmentEnum;
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public String getIndictmentName()
  {
    Assert.expect(_indictmentEnum != null);
    return _indictmentEnum.getName();
  }


  /**
   * Get measurements that are not failing but are related to the Indictment
   *
   * @author Peter Esbensen
   */
  public Collection<ComponentMeasurement> getRelatedMeasurements()
  {
    Assert.expect(_relatedMeasurements != null);
    return _relatedMeasurements;
  }

  /**
   * Add a measurement that is not failing but is related to the Indictment
   *
   * @author Peter Esbensen
   */
  public void addRelatedMeasurement(ComponentMeasurement measurement)
  {
    Assert.expect(measurement != null);
    _relatedMeasurements.add(measurement);
  }

  /**
   * Get failing measurements that directly caused this Indictment
   *
   * @author Peter Esbensen
   */
  public Collection<ComponentMeasurement> getFailingMeasurements()
  {
    Assert.expect(_failingMeasurements != null);
    return _failingMeasurements;
  }

  /**
   * Add a failing measurement that directly caused this Indictment
   *
   * @author Peter Esbensen
   */
  public void addFailingMeasurement(ComponentMeasurement measurement)
  {
    Assert.expect(measurement != null);
    _failingMeasurements.add(measurement);
  }

  /**
   * @author Peter Esbensen
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);

    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnum()
  {
    return _sliceNameEnum;
  }

  /**
   * @author Matt Wharton
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);

    return _subtype;
  }
  
  /**
   * @author Siew Yeng
   */
  public List<Subtype> getSubtypes()
  {
    Assert.expect(_subtypes != null);

    return _subtypes;
  }

  /**
   * @author Matt Wharton
   */
  public void setSubtype(Subtype subtype)
  {
    Assert.expect(subtype != null);

    _subtype = subtype;
  }
  
  /**
   * @author Siew Yeng
   */
  public void setSubtypes(List<Subtype> subtypes)
  {
    Assert.expect(subtypes != null);

    _subtypes.clear();
    _subtypes.addAll(subtypes);
  }

  /**
   * @author Matt Wharton
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointTypeEnum != null);

    return _jointTypeEnum;
  }

  /**
   * @author Matt Wharton
   */
  public void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);

    _jointTypeEnum = jointTypeEnum;
  }
}
