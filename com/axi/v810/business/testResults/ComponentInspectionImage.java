package com.axi.v810.business.testResults;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;

/**
 * Contains the image of a component at a given slice.
 *
 * @author Peter Esbensen
 */
public class ComponentInspectionImage
{
  private SliceNameEnum _sliceNameEnum;
  private Image _image;
  private RegionOfInterest _regionOfInterest;

  /**
   * @author Peter Esbensen
   */
  public ComponentInspectionImage()
  {
    // do nothing
  }

  /**
   * @author Peter Esbensen
   */
  public ComponentInspectionImage(SliceNameEnum sliceNameEnum, Image image,
      RegionOfInterest regionOfInterest)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(image != null);
    Assert.expect(regionOfInterest != null);
    _sliceNameEnum = sliceNameEnum;
    _image = image;
    _regionOfInterest = regionOfInterest;
  }


  /**
   * @author Peter Esbensen
   */
  static public ComponentInspectionImage getComponentInspectionImageForComponentIndictment(
      ManagedPadToReconstructedImageMap padToImageMap,
      ComponentIndictment componentIndictment,
      ComponentInspectionResult componentInspectionResult) throws DatastoreException
  {
    Assert.expect(padToImageMap != null);
    Assert.expect(componentIndictment != null);
    Assert.expect(componentInspectionResult != null);

    Pad padOne = componentInspectionResult.getComponentInspectionData().getComponent().getPadOne();
    ReconstructedImages reconstructedImages = padToImageMap.get(padOne);
    SliceNameEnum sliceNameEnum = componentIndictment.getSliceNameEnum();
    ReconstructedSlice reconstructedSlice = reconstructedImages.getReconstructedSlice(
      sliceNameEnum);
    Image image = reconstructedSlice.getImage();
    RegionOfInterest regionOfInterest = componentInspectionResult.
                                        getComponentInspectionData().
                                        getPadOneJointInspectionData().
                                        getRegionOfInterestRelativeToInspectionRegionInPixels();
    return new ComponentInspectionImage(sliceNameEnum, image, regionOfInterest);
  }

  /**
   * @author Peter Esbensen
   */
  public Image getImage()
  {
    Assert.expect(_image != null);
    return _image;
  }

  /**
   * @author Peter Esbensen
   */
  public void setImage(Image image)
  {
    Assert.expect(image != null);
    _image = image;
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public RegionOfInterest getRegionOfInterest()
  {
    Assert.expect(_regionOfInterest != null);
    return _regionOfInterest;
  }

  /**
   * @author Peter Esbensen
   */
  public void setRegionOfInterest(RegionOfInterest regionOfInterest)
  {
    Assert.expect(regionOfInterest != null);
    _regionOfInterest = regionOfInterest;
  }
}
