package com.axi.v810.business.testResults;

import java.io.*;
import java.text.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.util.*;

/**
 * @author Sunit Bhalla
 */
public class MeasurementUnitsEnum extends com.axi.util.Enum implements Serializable
{
  private static Map<String,
                     MeasurementUnitsEnum> _nameToMeasurementUnitsEnumMap = new TreeMap<String,
      MeasurementUnitsEnum>(new AlphaNumericComparator());

  private static int _index = -1;

  public final static MeasurementUnitsEnum NONE = new MeasurementUnitsEnum(++_index, "", null, 3);

  public final static MeasurementUnitsEnum MILS = new MeasurementUnitsEnum(++_index, "mils",
      "ALGDIAG_UNITS_LABEL_MILS_KEY", 3);
  public final static MeasurementUnitsEnum NANOMETERS = new MeasurementUnitsEnum(++_index, "nanometers",
      "ALGDIAG_UNITS_LABEL_NANOMETERS_KEY", 0);
  public final static MeasurementUnitsEnum MILLIMETERS = new MeasurementUnitsEnum(++_index, "millimeters",
      "ALGDIAG_UNITS_LABEL_MILLIMETERS_KEY", 4);
  public final static MeasurementUnitsEnum PIXELS = new MeasurementUnitsEnum(++_index, "pixels", null, 2);

  public final static MeasurementUnitsEnum PERCENT = new MeasurementUnitsEnum(++_index, "percent",
      "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);

  public final static MeasurementUnitsEnum PERCENT_OF_PAD_LENGTH_ACROSS = new MeasurementUnitsEnum(++_index,
      "percent of pad length across", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_PAD_LENGTH_ALONG = new MeasurementUnitsEnum(++_index,
      "percent of pad length along", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_HEEL_TOE_PEAK_DISTANCE = new MeasurementUnitsEnum(++_index,
      "percent of heel-toe peak distance", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_HEEL_TOE_EDGE_DISTANCE = new MeasurementUnitsEnum(++_index,
      "percent of heel-toe edge distance", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_PIN_LENGTH =
      new MeasurementUnitsEnum(++_index,
                               "percent of pin length",
                               "ALGDIAG_UNITS_LABEL_PERCENT_KEY",
                               2);
  public final static MeasurementUnitsEnum PERCENT_OF_FILLET_LENGTH =
      new MeasurementUnitsEnum(++_index,
                               "percent of fillet length",
                               "ALGDIAG_UNITS_LABEL_PERCENT_KEY",
                               2);
  public final static MeasurementUnitsEnum PERCENT_OF_NOMINAL_TOE_THICKNESS =
      new MeasurementUnitsEnum(++_index,
                               "percent of nominal toe thickness",
                               "ALGDIAG_UNITS_LABEL_PERCENT_KEY",
                               2);
  public final static MeasurementUnitsEnum PERCENT_OF_SIDE_FILLET_LENGTH = new MeasurementUnitsEnum(++_index,
      "percent of side fillet length", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_INTERPAD_DISTANCE = new MeasurementUnitsEnum(++_index,
      "percent of interpad distance", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_MAXIMUM_PAD_THICKNESS_ALONG = new MeasurementUnitsEnum(++_index,
      "percent of maximum pad thickness along", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_MAXIMUM_PAD_THICKNESS_ACROSS = new MeasurementUnitsEnum(++_index,
      "percent of maximum pad thickness across", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_COMPONENT_BODY_LENGTH = new MeasurementUnitsEnum(++_index,
      "percent of component body length", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_HEEL_THICKNESS = new MeasurementUnitsEnum(++_index,
      "percent of heel thickness", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);

  public final static MeasurementUnitsEnum GRAYLEVEL = new MeasurementUnitsEnum(++_index, "graylevel", null, 2);
  public final static MeasurementUnitsEnum PERCENT_OF_NOMINAL_GRAYLEVEL = new MeasurementUnitsEnum(++_index,
      "percent of nominal graylevel", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_AVERAGE_CORRECTED_GRAYLEVEL = new MeasurementUnitsEnum(++_index,
      "percent of average corrected graylevel", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_AREA_VOIDED = new MeasurementUnitsEnum(++_index,
      "percent of area voided", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum PERCENT_OF_DIAMETER_VOIDED = new MeasurementUnitsEnum(++_index,
      "percent of diameter voided", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2); //Siew Yeng - XCR-3515

  public final static MeasurementUnitsEnum PERCENTAGE_THROUGH_HOLE_FROM_BOTTOM = new MeasurementUnitsEnum(++_index,
      "percentage through hole, from bottom", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);
  public final static MeasurementUnitsEnum INTER_QUARTILE_RANGE = new MeasurementUnitsEnum(++_index,
      "Inter Quartile Range", null, 2);
  public final static MeasurementUnitsEnum PERCENT_OF_PITCH = new MeasurementUnitsEnum(++_index, "percent of pitch",
      "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);

  public final static MeasurementUnitsEnum LOWER_TO_UPPER_FILLET_THICKNESS_RATIO = new MeasurementUnitsEnum(++_index,
      "lower to upper fillet thickness ratio", null, 4);

  public final static MeasurementUnitsEnum PERCENT_OF_NOMINAL_PAD_ONE_THICKNESS = new MeasurementUnitsEnum(++_index,
      "percent of nominal pad one thickness", null, 2);

  public final static MeasurementUnitsEnum DEGREES = new MeasurementUnitsEnum(++_index,
      "degrees", null, 1);

  public final static MeasurementUnitsEnum PERCENT_OF_NOMINAL_CENTER_THICKNESS =
      new MeasurementUnitsEnum(++_index,
                               "percent of nominal center thickness",
                               "ALGDIAG_UNITS_LABEL_PERCENT_KEY",
                               2);

  public final static MeasurementUnitsEnum PERCENT_OF_NIMINAL_FILLET_LENGTH =
      new MeasurementUnitsEnum(++_index,
                               "percent of nominal fillet length",
                               "ALGDIAG_UNITS_LABEL_PERCENT_KEY",
                               2);

  public final static MeasurementUnitsEnum PERCENT_OF_PAD_WIDTH_ALONG = new MeasurementUnitsEnum(++_index,
      "percent of pad width along", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);

  public final static MeasurementUnitsEnum PERCENT_OF_PAD_WIDTH_ACROSS = new MeasurementUnitsEnum(++_index,
      "percent of pad width across", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);

  public final static MeasurementUnitsEnum SQUARE_MILLIMETERS = new MeasurementUnitsEnum(++_index, "square millimeters",
      "ALGDIAG_UNITS_LABEL_SQUARE_MILLIMETERS_KEY", 4);

  public final static MeasurementUnitsEnum CUBIC_MILLIMETERS = new MeasurementUnitsEnum(++_index, "cubic millimeters",
      "ALGDIAG_UNITS_LABEL_CUBIC_MILLIMETERS_KEY", 4);

  public final static MeasurementUnitsEnum SQUARE_MILS = new MeasurementUnitsEnum(++_index, "square mils",
      "ALGDIAG_UNITS_LABEL_SQUARE_MILS_KEY", 2);

  public final static MeasurementUnitsEnum CUBIC_MILS = new MeasurementUnitsEnum(++_index, "cubic mils",
      "ALGDIAG_UNITS_LABEL_CUBIC_MILS_KEY", 2);
  
  public final static MeasurementUnitsEnum DEFINITE_VOIDING_GREY_LEVEL_VOIDED = new MeasurementUnitsEnum(++_index,
      "percent", "ALGDIAG_UNITS_LABEL_GREY_LEVEL_KEY", 2);
  
  public final static MeasurementUnitsEnum EXONERATED_VOIDING_GREY_LEVEL_VOIDED = new MeasurementUnitsEnum(++_index,
      "voided value", "ALGDIAG_UNITS_LABEL_GREY_LEVEL_KEY", 2);
  
  public final static MeasurementUnitsEnum VOID_DIFFERENCE_FROM_NOMINAL_GREY_LEVEL = new MeasurementUnitsEnum(++_index,
      "difference", "ALGDIAG_UNITS_LABEL_PERCENT_KEY", 2);

//  public final static MeasurementUnitsEnum  = new MeasurementUnitsEnum(++_index, "");

  private String _name;

  // the label to put with the value to indicate the units. Should be short, eg 10% or 32.2 mm
  // Should take one argument, the value: {0}%
  private String _labelLocalizationKey = null;

  // this is the precision of the variable. Set to the number of decimal places after the decimal point.
  // Ignore for non-real values.
  private int _numberOfDecimalPlaces = 0;

  /**
   * @author Sunit Bhalla
   */
  private MeasurementUnitsEnum(int id, String name, String labelLocalizationKey, int numberOfDecimalPlaces)
  {
    super(id);
    Assert.expect(name != null);
    _name = name.intern();
    _labelLocalizationKey = labelLocalizationKey;
    _nameToMeasurementUnitsEnumMap.put(_name, this);
    _numberOfDecimalPlaces = numberOfDecimalPlaces;
  }

  /**
   * @author Sunit Bhalla
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Sunit Bhalla
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Patrick Lacz
   */
  public boolean hasLabelLocalizationKey()
  {
    return (_labelLocalizationKey != null);
  }

  /**
   * the label to put with the value to indicate the units. Should be short, eg 10% or 32.2 mm
   * Should take one argument, the value: {0}%
   *
   * @author Patrick Lacz
   */
  public String getLabelLocalizationKey()
  {
    Assert.expect(_labelLocalizationKey != null);
    return _labelLocalizationKey;
  }

  /**
   * @author Patrick Lacz
   */
  public int getNumberOfDecimalPlaces()
  {
    return _numberOfDecimalPlaces;
  }

  /**
   * @author Sunit Bhalla
   */
  public static MeasurementUnitsEnum getMeasurementUnitsEnum(String jointTypeName)
  {
    MeasurementUnitsEnum MeasurementUnitsEnum = _nameToMeasurementUnitsEnumMap.get(jointTypeName);
    Assert.expect(MeasurementUnitsEnum != null);
    return MeasurementUnitsEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public static boolean doesMeasurementUnitsEnumExist(String jointTypeName)
  {
    return _nameToMeasurementUnitsEnumMap.containsKey(jointTypeName);
  }

  /**
   * @author Sunit Bhalla
   */
  public static List<MeasurementUnitsEnum> getAllMeasurementUnitsEnums()
  {
    return new ArrayList<MeasurementUnitsEnum>(_nameToMeasurementUnitsEnumMap.values());
  }

  /**
   * Both arguments to this method may be modified.
   * @author Patrick Lacz
   */
  public static Pair<MeasurementUnitsEnum, Object> convertToProjectUnitsIfNecessary(MeasurementUnitsEnum measurementUnitsEnum, Object value)
  {
    // conditionally convert to mils based on the project's preferred units.
    if (measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS)
        && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
    {
      measurementUnitsEnum = MeasurementUnitsEnum.MILS;
      Number valueAsNumber = (Number)value;
      value = (Float)MathUtil.convertMillimetersToMils(valueAsNumber.floatValue());
    }
    else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS)
        && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
    {
      measurementUnitsEnum = MeasurementUnitsEnum.SQUARE_MILS;
      Number valueAsNumber = (Number)value;
      value = (Float)MathUtil.convertSquareMillimetersToSquareMils(valueAsNumber.floatValue());
    }
    else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.CUBIC_MILLIMETERS)
        && Project.getCurrentlyLoadedProject().getDisplayUnits().isMetric() == false)
    {
      measurementUnitsEnum = MeasurementUnitsEnum.CUBIC_MILS;
      Number valueAsNumber = (Number)value;
      value = (Float)MathUtil.convertCubicMillimetersToCubicMils(valueAsNumber.floatValue());
    }
    return new Pair<MeasurementUnitsEnum, Object>(measurementUnitsEnum, value);
  }
  
  /**
   * Both arguments to this method may be modified.
   * @author Andy Mechtenberg
   */
  public static Pair<MeasurementUnitsEnum, Object> convertToTargetUnitsIfNecessary(MeasurementUnitsEnum measurementUnitsEnum, Object value, MathUtilEnum targetUnits)
  {
    // conditionally convert to mils based on the project's preferred units.
    if (measurementUnitsEnum.equals(MeasurementUnitsEnum.MILLIMETERS)
        && targetUnits.isMetric() == false)
    {
      measurementUnitsEnum = MeasurementUnitsEnum.MILS;
      Number valueAsNumber = (Number)value;
      value = (Float)MathUtil.convertMillimetersToMils(valueAsNumber.floatValue());
    }
    else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.SQUARE_MILLIMETERS)
        && targetUnits.isMetric() == false)
    {
      measurementUnitsEnum = MeasurementUnitsEnum.SQUARE_MILS;
      Number valueAsNumber = (Number)value;
      value = (Float)MathUtil.convertSquareMillimetersToSquareMils(valueAsNumber.floatValue());
    }
    else if (measurementUnitsEnum.equals(MeasurementUnitsEnum.CUBIC_MILLIMETERS)
        && targetUnits.isMetric() == false)
    {
      measurementUnitsEnum = MeasurementUnitsEnum.CUBIC_MILS;
      Number valueAsNumber = (Number)value;
      value = (Float)MathUtil.convertCubicMillimetersToCubicMils(valueAsNumber.floatValue());
    }
    return new Pair<MeasurementUnitsEnum, Object>(measurementUnitsEnum, value);
  }

  private static NumberFormat _numberFormat = NumberFormat.getInstance();

  /**
   * @author Patrick Lacz
   */
  public static String formatNumberIfNecessary(MeasurementUnitsEnum measurementUnitsEnum, Object value)
  {
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(value != null);

    if (value instanceof Number)
    {
      _numberFormat.setMaximumFractionDigits(measurementUnitsEnum.getNumberOfDecimalPlaces());
      return _numberFormat.format(value);
    }
    return value.toString();
  }


  /**
   * @author Patrick Lacz
   */
  public static String labelValueWithUnitsIfNecessary(MeasurementUnitsEnum measurementUnitsEnum, String valueString)
  {
    Assert.expect(measurementUnitsEnum != null);
    Assert.expect(valueString != null);

    if (measurementUnitsEnum.hasLabelLocalizationKey())
    {
      LocalizedString localizedValueText = new LocalizedString(measurementUnitsEnum.getLabelLocalizationKey(),
          new Object[]{valueString});
      return StringLocalizer.keyToString(localizedValueText);
    }
    return valueString;
  }
}
