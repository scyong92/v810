package com.axi.v810.business.testResults;

import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Common logger for all data info for plotting charts, graphs or histogram.
 * For example, measured BGA midball diameter vs Z height
 * @author Seng Yew Lim
 */
public class DumpPlotInfoLogger extends FileLoggerAxi
{
  private static DumpPlotInfoLogger _instance = null;
  private static final String _FILENAME_FULLPATH = FileName.getDumpPlotInfoLogFullPath();

  private boolean _dumpPlotInfo = true;

  /**
   * @author Seng Yew Lim
   */
  public void setDumpPlotInfo(boolean dumpPlotInfo)
  {
    _dumpPlotInfo = dumpPlotInfo;
  }

  /**
   * @author Seng Yew Lim
   */
  public boolean getDumpPlotInfo()
  {
    return _dumpPlotInfo;
  }

  /**
   * @author Seng Yew Lim
   */
  private DumpPlotInfoLogger()
  {
    super(_FILENAME_FULLPATH);
  }


  /**
   * @author Seng Yew Lim
   */
  public static synchronized DumpPlotInfoLogger getInstance()
  {
    if (_instance == null)
    {
      _instance = new DumpPlotInfoLogger();
      try {
        _instance.append("refDes,padName,padSliceHeight,zheight,measurementName,measurementUnit,value");
      }
      catch (DatastoreException de) {
      }
    }
    return _instance;
  }


  /**
   * @author Seng Yew Lim
   */
  public synchronized void append(String logString) throws DatastoreException
  {
    if (_dumpPlotInfo==true)
      super.append(logString);
  }

}
