package com.axi.v810.business.testResults;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.testProgram.SliceNameEnum;

/**
 * Represents a specific defect called on a joint.  An indictment will
 * have a name like "SHORT", "OPEN", or "MISALIGNED".  <p>
 *
 * An indictment should have one or more failing measurements.  For example, you
 * might call the <code>getFailingMeasurements</code> method on an "OPEN" Indictment
 * and get back a "Fillet Length" measurement, which means that the OPEN call
 * was made because the Fillet Length measurement did not fall within acceptable
 * ranges.  <p>
 *
 * An indictment may also have other measurements associated with it.  For example,
 * the FPGULLWING OPEN algorithm may call OPENs due to the Open Signal or the
 * Fillet Length measurements.  The failing measurement will be provided
 * by the <code>getFailingMeasurements</code> method.  If the other measurement
 * did not fail (it was within an acceptable range) it may still be interesting
 * to the user (according to Dick Voigt), so it will be provided when the
 * <code>getOtherAssociatedMeasurements</code> method is called.  This is kind
 * of confusing, so perhaps the following breakdown will help:
 * <ul>
 *   <li>If you want just the failing measurements, use <code>getFailingMeasurements</code></li>
 *   <li>If you want measurements that aren't failing, but might be related to the Indictment, use <code>getRelatedMeasurements</code>
 *   <li>If you want ALL measurements, use the <code>getMeasurements</code> method in the
 *           <code>JointInspectionResult</code> class</li>
 * </ul>
 *
 * called due to a Fillet Length measurement exceeding a threshold.
 * measurements that are potentially useful to the user when tuning their test.
 * For example, an "OPEN" Indictment on a Gullwing might have the "Heel fillet
 * thickness" measurement associated with it since that is a primary indicator of opens.
 *
 * @author Peter Esbensen
 */
public class JointIndictment
{
  private Algorithm _algorithm;

  private IndictmentEnum _indictmentEnum;
  private Collection<JointMeasurement> _relatedMeasurements = new ArrayList<JointMeasurement>();
  private Collection<JointMeasurement> _failingMeasurements = new ArrayList<JointMeasurement>();
  private SliceNameEnum _sliceNameEnum;

  /**
   * @author Patrick Lacz
   */
  public JointIndictment(IndictmentEnum indictmentEnum,
                         Algorithm algorithm,
                         SliceNameEnum sliceNameEnum)
  {
    Assert.expect(indictmentEnum != null);
    Assert.expect(algorithm != null);
    Assert.expect(sliceNameEnum != null);

    _algorithm = algorithm;
    _indictmentEnum = indictmentEnum;
    _sliceNameEnum = sliceNameEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public void setAlgorithm(Algorithm algorithm)
  {
    Assert.expect(algorithm != null);
    _algorithm = algorithm;
  }

  /**
   * @author Peter Esbensen
   */
  public Algorithm getAlgorithm()
  {
    Assert.expect(_algorithm != null);
    return _algorithm;
  }

  /**
   * @author Sunit Bhalla
   */
  public IndictmentEnum getIndictmentEnum()
  {
    Assert.expect(_indictmentEnum != null);
    return _indictmentEnum;
  }

  /**
   * @author Peter Esbensen
   * @author Patrick Lacz
   */
  public String getIndictmentName()
  {
    Assert.expect(_indictmentEnum != null);
    return _indictmentEnum.getName();
  }


  /**
   * Get measurements that are not failing but are related to the Indictment
   * @return a collection of <code>JointMeasurement</code> objects
   * @author Peter Esbensen
   */
  public Collection<JointMeasurement> getRelatedMeasurements()
  {
    Assert.expect(_relatedMeasurements != null);
    return _relatedMeasurements;
  }

  /**
   * Add a measurement that is not failing but is related to the Indictment
   * @author Peter Esbensen
   */
  public void addRelatedMeasurement(JointMeasurement measurement)
  {
    Assert.expect(measurement != null);
    _relatedMeasurements.add(measurement);
  }

  /**
   * Get failing measurements that directly caused this Indictment
   * @return a collection of <code>JointMeasurement</code> objects
   * @author Peter Esbensen
   */
  public Collection<JointMeasurement> getFailingMeasurements()
  {
    Assert.expect(_failingMeasurements != null);
    return _failingMeasurements;
  }

  /**
   * Add a failing measurement that directly caused this Indictment
   * @author Peter Esbensen
   */
  public void addFailingMeasurement(JointMeasurement measurement)
  {
    Assert.expect(measurement != null);
    _failingMeasurements.add(measurement);
  }

  /**
   * @author Peter Esbensen
   */
  public SliceNameEnum getSliceNameEnum()
  {
    Assert.expect(_sliceNameEnum != null);
    return _sliceNameEnum;
  }

  /**
   * @author Peter Esbensen
   */
  public void setSliceNameEnum(SliceNameEnum sliceNameEnum)
  {
    Assert.expect(sliceNameEnum != null);
    _sliceNameEnum = sliceNameEnum;
  }

}
