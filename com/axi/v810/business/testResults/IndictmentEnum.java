package com.axi.v810.business.testResults;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author Sunit Bhalla
 */
public class IndictmentEnum extends com.axi.util.Enum implements Serializable
{
  private static Map<String, IndictmentEnum> _nameToIndictmentEnumMap = new TreeMap<String, IndictmentEnum>(new AlphaNumericComparator());

  private static int _index = -1;

  public final static IndictmentEnum SHORT = new IndictmentEnum(++_index, "Short");
  public final static IndictmentEnum OPEN = new IndictmentEnum(++_index, "Open");
  public final static IndictmentEnum INSUFFICIENT = new IndictmentEnum(++_index, "Insufficient");
  public final static IndictmentEnum MISSING = new IndictmentEnum(++_index, "Missing");
  public final static IndictmentEnum MISALIGNMENT = new IndictmentEnum(++_index, "Misalignment");
  public final static IndictmentEnum VOIDING = new IndictmentEnum(++_index, "Voiding");
  public final static IndictmentEnum COMPONENT_VOIDING = new IndictmentEnum(++_index, "Component Voiding");
  public final static IndictmentEnum EXCESS = new IndictmentEnum(++_index, "Excess");
  public final static IndictmentEnum INDIVIDUAL_VOIDING = new IndictmentEnum(++_index, "Individual Voiding"); //Jack Hwee- individual void
  public final static IndictmentEnum DUMMY = new IndictmentEnum(++_index, "Dummy"); //Siew Yeng - XCR1745 - use for semi-automated mode

  private String _name;

  /**
   * @author Sunit Bhalla
   */
  private IndictmentEnum(int id, String name)
  {
    super(id);
    Assert.expect(name != null);
   _name = name.intern();
   _nameToIndictmentEnumMap.put(_name, this);
  }

  /**
   * @author Sunit Bhalla
   */
  public String getName()
  {
    return _name;
  }

  /**
   * @author Sunit Bhalla
   */
  public String toString()
  {
    return getName();
  }

  /**
   * @author Sunit Bhalla
   */
  public static IndictmentEnum getIndictmentEnum(String jointTypeName)
  {
    IndictmentEnum IndictmentEnum = _nameToIndictmentEnumMap.get(jointTypeName);
    Assert.expect(IndictmentEnum != null);
    return IndictmentEnum;
  }

  /**
   * @author Sunit Bhalla
   */
  public static boolean doesIndictmentEnumExist(String jointTypeName)
  {
    return _nameToIndictmentEnumMap.containsKey(jointTypeName);
  }

  /**
   * @author Sunit Bhalla
   */
  public static List<IndictmentEnum> getAllIndictmentEnums()
  {
    return new ArrayList<IndictmentEnum>(_nameToIndictmentEnumMap.values());
  }
}
