package com.axi.v810.business.testResults;

import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;

/**
 * @author Patrick Lacz
 */
public class ComponentInspectionResult
{
  private Collection<ComponentIndictment> _indictments = new ArrayList<ComponentIndictment>();
  private Map<Pair<SliceNameEnum, MeasurementEnum>, List<ComponentMeasurement>>
    _sliceEnumAndMeasurementNamePairToMeasurementListMap = new HashMap<Pair<SliceNameEnum, MeasurementEnum>, List<ComponentMeasurement>>();
  private Map<Long, ComponentMeasurement> _uniqueMeasurementIdToMeasurementMap = new HashMap<Long, ComponentMeasurement>();

  private ComponentInspectionData _componentInspectionData = null;
  private transient int _inspectionRunId;

  private static InspectionEventObservable _inspectionEventObservable;
  private ImageCoordinate _imageCoordinate;

//  private Map<Integer, ReconstructedSlice> _sliceIdToReconstructedSliceMap;
  /**
   * @author Bill Darbie
   */
  static
  {
    _inspectionEventObservable = InspectionEventObservable.getInstance();
  }

  /**
   * @author Patrick Lacz
   */
  public ComponentInspectionResult(ComponentInspectionData componentInspectionData)
  {
    Assert.expect(componentInspectionData != null);
    _componentInspectionData = componentInspectionData;
  }

  /**
   * @author Patrick Lacz
   */
  public ComponentInspectionResult()
  {
//    _sliceIdToReconstructedSliceMap = new TreeMap<Integer, ReconstructedSlice>();
  }

  /**
   * @return true if the Component passed, no defects were found
   * @author Patrick Lacz
   */
  public boolean passed()
  {
    Assert.expect(_indictments != null);
    if (_indictments.size() > 0)
      return false;
    return true;
  }

  /**
   * @return all measurements for this Component
   * @author Patrick Lacz
   * @author Matt Wharton
   */
  public synchronized Collection<ComponentMeasurement> getAllMeasurements()
  {
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    List<ComponentMeasurement> measurementsForJoint = new LinkedList<ComponentMeasurement>();
    for (List<ComponentMeasurement> measurementsForSingleMeasurementEnumList :
         _sliceEnumAndMeasurementNamePairToMeasurementListMap.values())
    {
      measurementsForJoint.addAll(measurementsForSingleMeasurementEnumList);
    }

    return measurementsForJoint;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized boolean hasComponentMeasurement(SliceNameEnum sliceNameEnum, MeasurementEnum measurementName)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(measurementName != null);
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);

    Pair<SliceNameEnum, MeasurementEnum> measurementMapKey = new Pair<SliceNameEnum, MeasurementEnum>(sliceNameEnum, measurementName);
    return _sliceEnumAndMeasurementNamePairToMeasurementListMap.containsKey(measurementMapKey);
  }


  /**
   * @author Patrick Lacz
   */
  public synchronized ComponentMeasurement getComponentMeasurement(SliceNameEnum sliceNameEnum,
                                                                   MeasurementEnum componentMeasurement)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentMeasurement != null);

    List<ComponentMeasurement> measurementList = getComponentMeasurementList(sliceNameEnum, componentMeasurement);
    ComponentMeasurement measurement = measurementList.get(0);
    Assert.expect(measurement != null);
    return measurement;
  }

  /**
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public synchronized List<ComponentMeasurement> getComponentMeasurementList(SliceNameEnum sliceNameEnum,
                                                                             MeasurementEnum componentMeasurement)
  {
    Assert.expect(sliceNameEnum != null);
    Assert.expect(componentMeasurement != null);
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    Pair<SliceNameEnum, MeasurementEnum> measurementMapKey =
        new Pair<SliceNameEnum, MeasurementEnum>(sliceNameEnum, componentMeasurement);

    List<ComponentMeasurement> measurementList = _sliceEnumAndMeasurementNamePairToMeasurementListMap.get(measurementMapKey);
    Assert.expect(measurementList != null);
    Assert.expect(measurementList.isEmpty() == false);

    return measurementList;
  }


  /**
   * @author Patrick Lacz
   */
  public synchronized void addMeasurement(ComponentMeasurement measurement)
  {
    Assert.expect(measurement != null);
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    Assert.expect(_uniqueMeasurementIdToMeasurementMap != null);

    SliceNameEnum sliceNameEnum = measurement.getSliceNameEnum();
    MeasurementEnum measurementName = measurement.getMeasurementEnum();

    Pair<SliceNameEnum, MeasurementEnum> measurementMapKey = new Pair<SliceNameEnum, MeasurementEnum>(sliceNameEnum, measurementName);

    Assert.expect(_uniqueMeasurementIdToMeasurementMap.containsKey(measurement.getId()) == false);

    // See if we already have a measurement list for this key.
    List<ComponentMeasurement> measurementList = _sliceEnumAndMeasurementNamePairToMeasurementListMap.get(measurementMapKey);
    if (measurementList == null)
    {
      measurementList = new LinkedList<ComponentMeasurement>();
      _sliceEnumAndMeasurementNamePairToMeasurementListMap.put(measurementMapKey, measurementList);
    }

    Assert.expect(measurementList != null);
    measurementList.add(measurement);
    _uniqueMeasurementIdToMeasurementMap.put(measurement.getId(), measurement);
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized Collection<ComponentIndictment> getIndictments()
  {
    Assert.expect(_indictments != null);
    return _indictments;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void addIndictment(ComponentIndictment indictment)
  {
    Assert.expect(indictment != null);
    _indictments.add(indictment);

    _inspectionEventObservable.sendEventInfo(new JointOrComponentFailureInspectionEvent());

    // check that the measurements are there.
    for (ComponentMeasurement measurement : indictment.getFailingMeasurements())
    {
      Assert.expect(_uniqueMeasurementIdToMeasurementMap.containsKey(measurement.getId()));
    }
    for (ComponentMeasurement measurement : indictment.getRelatedMeasurements())
    {
      Assert.expect(_uniqueMeasurementIdToMeasurementMap.containsKey(measurement.getId()));
    }
  }

  /**
   * @author Patrick Lacz
   */
  public ComponentInspectionData getComponentInspectionData()
  {
    Assert.expect(_componentInspectionData != null);
    return _componentInspectionData;
  }

  /**
   * @author Patrick Lacz
   */
  public void setComponentInspectionData(ComponentInspectionData componentInspectionData)
  {
    Assert.expect(componentInspectionData != null);
    _componentInspectionData = componentInspectionData;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized void clearMeasurements()
  {
    Assert.expect(_sliceEnumAndMeasurementNamePairToMeasurementListMap != null);
    Assert.expect(_uniqueMeasurementIdToMeasurementMap != null);

    _sliceEnumAndMeasurementNamePairToMeasurementListMap.clear();
    _uniqueMeasurementIdToMeasurementMap.clear();
  }

  /**
   * @author Peter Esbensen
   */
  public int getInspectionRunId()
  {
    return _inspectionRunId;
  }

  /**
   * @author Peter Esbensen
   */
  public void setInspectionRunId(int inspectionRunId)
  {
    _inspectionRunId = inspectionRunId;
  }
  
  /**
   * set the original location of image without enhanced
   * @param imageCoordinate 
   * @XCR-3014
   * @author sheng chuan
   */
  public void setOrigImageLocation(ImageCoordinate imageCoordinate)
  {
    _imageCoordinate = imageCoordinate;
  }
  
  /**
   * @XCR-3014
   * @author sheng chuan
   */
  public ImageCoordinate getOrigImageLocation()
  {
    return _imageCoordinate;
  }
  
  /**
   * @XCR-3014
   * @author sheng chuan
   */
  public boolean hasOrigImageLocation()
  {
    if(_imageCoordinate == null)
      return false;
    else
      return true;
  }
  
// Comment for fixing CR 1019 - Pull out the Measurement extra Infomation added by WC
// Comment by Wei Chin
  /**
   * @param sliceIdToReconstructedSliceMap Map
   * @author Wei Chin, Chong
   */
//  public void setSliceIdToReconstructedSliceMap (Map<Integer, ReconstructedSlice> sliceIdToReconstructedSliceMap)
//  {
//    Assert.expect(sliceIdToReconstructedSliceMap != null);
//    _sliceIdToReconstructedSliceMap = sliceIdToReconstructedSliceMap;
//  }

//  /**
//   * @author Wei Chin, Chong
//   */
//  public Map<Integer, ReconstructedSlice> getSliceIdToReconstructedSliceMap ()
//  {
//    Assert.expect(_sliceIdToReconstructedSliceMap != null);
//    return _sliceIdToReconstructedSliceMap;
//  }

//  /**
//   * @author Patrick Lacz
//   */
//  public ReconstructedSlice getReconstructedSlice(SliceNameEnum sliceName)
//  {
//    Assert.expect(_sliceIdToReconstructedSliceMap != null);
//    Assert.expect(sliceName != null);
//    ReconstructedSlice slice = _sliceIdToReconstructedSliceMap.get(sliceName.getId());
//    Assert.expect(slice != null, "Failure to find slice: " + sliceName.getId() + " " + sliceName.getName());
//    return slice;
//  }
}
