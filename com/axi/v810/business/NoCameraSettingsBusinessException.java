package com.axi.v810.business;

import com.axi.v810.business.*;
import com.axi.util.*;

/**
 * This excception is thrown if there is no alignment data for a panel or board
 *
 * @author Andy Mechtenberg
 */
public class NoCameraSettingsBusinessException extends BusinessException
{
  /**
   * @author Bill Darbie
   */
  public NoCameraSettingsBusinessException(String message)
  {
    super(new LocalizedString(message, null));
    Assert.expect(message != null);
  }
}
