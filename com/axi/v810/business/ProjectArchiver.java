package com.axi.v810.business;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testProgram.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Use ProjectArchiver objects to build up a set of conditions that determine which files
 *  of what project to archive as a specified .zip file, and then create that .zip archive.<p/>
 *
 * The setters should be called roughly in the order present in the source file because you
 *  will be building up state.  Therefore, many of the setters depend on those that came
 *  before.  This is all properly documented of course, but I thought you might appreciate
 *  the general statement that underlies the specific restrictions.<p/>
 *
 * If you set a Component or joint Pad for limiting which inspection images are to be archived,
 *  don't forget to use getAllowableInspectionImageSets() so that you don't present users with
 *  image sets that don't contain images of the limiting Component or joint Pad.<p/>
 *
 * @todo Decide whether the addToProjectDatabase flag and functionality belongs here (even if
 *  only because there is no better place to put it).
 *
 * @author Greg Loring
 */
public class ProjectArchiver
{
  private String _projectName;
  private boolean _includeAlgorithmLearning;
  private boolean _includeHistoryLog;
  private boolean _includeVerificationImages;
  private boolean _includeFocusImageSet;
  private Map<String, ImageSetData> _nameToImageSetMap; // imageSetName -> :ImageSetData; allows dup checking
  private Component _component;
  private Pad _jointPad;
  private String _destinationFilename;
  private boolean _addToProjectDatabase;
  private ImageManager _imageManager;
  private ReconstructionRegion _requestedRegion;
  private List<String> _tempDirsToDelete;

  private int _approximateSizeInBytes = 0;

  private boolean _abort = false;

  // these values are used to help estimate how large the zip file will be. I obtained these
  // values from doing some experiments with real images and data.
  private static final double _COMPRESSION_PERCENT_FOR_VERIFICATION_IMAGES = 0.93; //.0007; // these compress very little
  private static final double _COMPRESSION_PERCENT_FOR_INSPECTION_IMAGES = 1.0; //.0009;  // these don't compress at all
  private static final double _COMPRESSION_PERCENT_FOR_LEARNED_DATA = 0.45; //.95;
  private static final double _COMPRESSION_PERCENT_FOR_PROJECT_DATA = 0.10;   //.8664;

  private static final String NDF_ZIP_EXTENSION = "ndf.zip";
  private List<String> ndfZipFileFullPathList = new ArrayList<String>();
  /**
   * @author Greg Loring
   */
  public ProjectArchiver()
  {
    _projectName = null;
    _includeAlgorithmLearning = false;
    _includeVerificationImages = false;
    _includeFocusImageSet = false;
    _includeHistoryLog = false;
    _component = null;
    _jointPad = null;
    _nameToImageSetMap = new LinkedHashMap<String, ImageSetData>();
    _destinationFilename = null;
    _addToProjectDatabase = false;
    _imageManager = ImageManager.getInstance();
    _requestedRegion = null;
    _tempDirsToDelete = new LinkedList<String>();
  }

  /**
   * @author Erica Wheatcroft
   */
  public void clear()
  {
    _projectName = null;
    _includeAlgorithmLearning = false;
    _includeVerificationImages = false;
    _includeFocusImageSet = false;
    _includeHistoryLog = false;
    _component = null;
    _jointPad = null;
    _nameToImageSetMap = new LinkedHashMap<String, ImageSetData>();
    _destinationFilename = null;
    _addToProjectDatabase = false;
  }

  /**
   * @author Erica Wheatcroft
   */
  public List<ImageSetData> getImageSetsToArchive()
  {
    List<ImageSetData> imageSets = new ArrayList<ImageSetData>();
    for(ImageSetData imageSet : _nameToImageSetMap.values())
      imageSets.add(imageSet);

    return imageSets;
  }

  /**
   * set the name of the project to archive
   *
   * @author Greg Loring
   */
  public void setProjectName(String projectName)
  {
    Assert.expect(Project.doesProjectExistLocally(projectName));

    _projectName = projectName;
  }

  /**
   * @author Greg Loring
   */
  public boolean hasProjectName()
  {
    boolean result = (_projectName != null);
    return result;
  }

  /**
   * precondition: call setProjectName(...); i.e., hasProjectName() returns true<p/>
   *
   * @see #setProjectName(String)
   *
   * @author Greg Loring
   */
  public String getProjectName()
  {
    Assert.expect(_projectName != null);
    return _projectName;
  }

  /**
   * precondition: call setProjectName(...)<p/>
   *
   * @author Greg Loring
   */
  public boolean doesAlgorithmLearningExist()
  {
    String directoryName = Directory.getAlgorithmLearningDir(_projectName);
    boolean result = FileUtil.exists(directoryName);
    return result;
  }

  /**
   * set a flag indicating whether to include algorithm learning files in the archive.<p/>
   *
   * precondition: call setProjectName(...)<p/>
   * precondition: if setting to true, then doesAlgorithmLearningExist() MUST return true<p/>
   *
   * @author Greg Loring
   */
  public void setIncludeAlgorithmLearning(boolean includeAlgorithmLearning)
  {
    if (includeAlgorithmLearning)
    {
      Assert.expect(doesAlgorithmLearningExist());
    }
    _includeAlgorithmLearning = includeAlgorithmLearning;
  }

  /**
   * @see #setIncludeAlgorithmLearning(boolean)
   *
   * @author Greg Loring
   */
  public boolean getIncludeAlgorithmLearning()
  {
    return _includeAlgorithmLearning;
  }

  /**
   * precondition: call setProjectName(...)<p/>
   *
   * @author Greg Loring
   */
  public boolean doVerificationImagesExist()
  {
    String directoryName = Directory.getXrayVerificationImagesDir(_projectName);
    boolean result = FileUtil.exists(directoryName);
    return result;
  }

  /**
   * precondition: call setProjectName(...)<p/>
   *
   * @author Erica Wheatcroft
   */
  public boolean doFocusImageSetExist()
  {
    String directoryName = Directory.getAdjustFocusImagesDir(_projectName);
    boolean result = FileUtil.exists(directoryName);
    return result;
  }

  /**
   * set a flag indicating whether to include verification images in the archive.
   *
   * precondition: call setProjectName(...)<p/>
   * precondition: if setting to true, then doVerificationImagesExist() MUST return true<p/>
   *
   * @author Greg Loring
   */
  public void setIncludeVerificationImages(boolean includeVerificationImages)
  {
    if (includeVerificationImages)
    {
      Assert.expect(doVerificationImagesExist());
    }
    _includeVerificationImages = includeVerificationImages;
  }

    /**
   * precondition: call setProjectName(...)<p/>
   * unzip history log 
   * @author hsia-fen.tan
   */
  public boolean doHistoryLogExist()
  {
    String directoryName = Directory.getHistoryFileDir(_projectName);
    boolean result = FileUtil.exists(directoryName);
    return result;
  }
  
  /**
   * set a flag indicating whether to include history log file in the archive.
   *
   * precondition: call setProjectName(...)<p/>
   * precondition: if setting to true, then doHistoryLogExist() MUST return true<p/>
   *
   * @author hsia-fen.tan
   */
  public void setIncludeHistoryLogFile(boolean includeHistoryLogFile)
  {
    if (includeHistoryLogFile)
    {
     // Assert.expect(doHistoryLogExist());
    }
    _includeHistoryLog = includeHistoryLogFile;
    
  }
   
  public boolean getIncludeHistoryLogFile()
  {
    return _includeHistoryLog;
  }
  /**
   * set a flag indicating whether to include focus image set in the archive.
   *
   * precondition: call setProjectName(...)<p/>
   * precondition: if setting to true, then doFocusImageSetExist() MUST return true<p/>
   *
   * @author Erica Wheatcroft
   */
  public void setIncludeFocusImageSet(boolean includeFocusImageSet)
  {
    if (includeFocusImageSet)
    {
      Assert.expect(doFocusImageSetExist());
    }
    _includeFocusImageSet = includeFocusImageSet;
  }

  /**
   * @see #setIncludeVerificationImages(boolean)
   *
   * @author Greg Loring
   */
  public boolean getIncludeVerificationImages()
  {
    return _includeVerificationImages;
  }

  /**
   * @see #setIncludeVerificationImages(boolean)
   *
   * @author Erica Wheatcroft
   */
  public boolean getIncludeFocusImageSet()
  {
    return _includeFocusImageSet;
  }

  /**
   * you can set either a component or a joint, but not both;
   *   and you must do so *before* adding image sets<p/>
   *
   * @author Greg Loring
   */
  public boolean isComponentAllowed()
  {
    boolean result = (_jointPad == null) && _nameToImageSetMap.isEmpty();
    return result;
  }

  /**
   * limit the archived inspection images to include only those that are inspected
   *  for the given Component; you can set either a component or a joint, but not both<p/>
   *
   * precondition: call setProjectName(...)<p/>
   *
   * @author Greg Loring
   */
  public void setComponent(Component component)
  {
    Assert.expect(isComponentAllowed());
    checkComponent(component);

    _component = component;
  }

  /**
   * This method will clear out the selected component
   * @author Erica Wheatcroft
   */
  public void clearComponent()
  {
    _component = null;
  }

  /**
   * This method will clear out the selected joint pad
   * @author Erica Wheatcroft
   */
  public void clearJointPad()
  {
    _jointPad = null;
  }

  /**
   * Limit the regions to only include the given region.  Do not use any of the other filters in combination with this option.<p/>
   *
   * precondition: call setProjectName(...)<p/>
   *
   * @author Peter Esbensen
   */
  public void setReconstructionRegion(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(_requestedRegion != null);
    _requestedRegion = reconstructionRegion;
  }


  /**
   * @author Greg Loring
   */
  private void checkComponent(Component component)
  {
    Assert.expect(component != null);

    // equalsIgnoreCase because project name is also a windows directory name:
    //  e.g., if there is an existing project named 0027parts, and you import ndf\0027Parts,
    //  then _projectName will have a different case than Project.load(_projectName).getName()
    Assert.expect(component.getBoard().getPanel().getProject().getName().equalsIgnoreCase(_projectName));
  }

  /**
   * @author Greg Loring
   */
  public boolean hasComponent()
  {
    boolean result = (_component != null);
    return result;
  }

  /**
   * precondition: call setComponent(...); i.e., hasComponent() returns true<p/>
   *
   * @see #setComponent(Component)
   *
   * @author Greg Loring
   */
  public Component getComponent()
  {
    Assert.expect(hasComponent());
    return _component;
  }

  /**
   * you can set either a component or a joint, but not both;
   *   and you must do so *before* adding image sets<p/>
   *
   * @author Greg Loring
   */
  public boolean isJointPadAllowed()
  {
    boolean result = (_component == null) && _nameToImageSetMap.isEmpty();
    return result;
  }

  /**
   * limit the archived inspection images to include only those that are inspected
   *  for the joint associated with the given Pad; you can set either a component
   *  or a joint, but not both<p/>
   *
   * precondition: call setProjectName(...)<p/>
   *
   * @author Greg Loring
   */
  public void setJointPad(Pad jointPad)
  {
    Assert.expect(isJointPadAllowed());
    Assert.expect(jointPad != null);
    checkComponent(jointPad.getComponent());

    _jointPad = jointPad;
  }

  /**
   * @author Greg Loring
   */
  public boolean hasJointPad()
  {
    boolean result = (_jointPad != null);
    return result;
  }

  /**
   * precondition: call setJointPad(...); i.e., hasJointPad() returns true<p/>
   *
   * @see #setJointPad(Pad)
   *
   * @author Greg Loring
   */
  public Pad getJointPad()
  {
    Assert.expect(hasJointPad());
    return _jointPad;
  }

  /**
   * returns the ImageSetData objects which have inspection images covering
   *  the previously set Component or joint Pad, if any.<p/>
   *
   * precondition: call setProjectName(...), and either setComponent() or setJointPad(), if
   *  desired<p/>
   *
   * @see #setComponent(Component)
   * @see #setJointPad(Pad)
   *
   * @author Greg Loring
   */
  public List<ImageSetData> getAllowableInspectionImageSets() throws DatastoreException
  {
    List<ImageSetData> results;

    if (isFilteringInspectionRegions())
    {
      List<ReconstructionRegion> regions = getRequiredInspectionRegions();

      // for each image set in the project
      results = new LinkedList<ImageSetData>();
      List<ImageSetData> imageSets = _imageManager.getImageSetData(_projectName, true);
      for (ImageSetData imageSet : imageSets)
      {
        if (containsRequiredImages(imageSet, regions))
        {
          results.add(imageSet);
        }
      }
    }
    else
    {
      // don't care what images are in the image sets
      results = _imageManager.getImageSetData(_projectName, true);
    }

    return results;
  }

  /**
   * add an inspection image set whose images are to be included in the archive.
   *  you can limit which images using setComponent() or setJointPad().<p/>
   *
   * precondition: call setProjectName(...), and either setComponent() or setJointPad(), if
   *  desired<p/><p/>
   *
   * @see #setComponent(Component)
   * @see #setJointPad(Pad)
   *
   * @author Greg Loring
   */
  public void addInspectionImageSet(ImageSetData imageSet)
  {
    Assert.expect(imageSet != null);
    // equalsIgnoreCase for same reason as in checkComponent()
    Assert.expect(imageSet.getProjectName().equalsIgnoreCase(_projectName));
    Assert.expect(_nameToImageSetMap.containsKey(imageSet.getImageSetName()) == false);

    _nameToImageSetMap.put(imageSet.getImageSetName(), imageSet);
  }

  /**
   * THis method will return the number of image sets to archive
   * @author Erica Wheatcroft
   */
  public int getNumberOfImageSetsToArchive()
  {
    Assert.expect(_nameToImageSetMap != null);

    return _nameToImageSetMap.size();
  }

  /**
   * a way for the user to change their mind without having to create a new ProjectArchiver object.
   *   After calling this method, it will be possible to call setComponent() or setJointPad() again.
   *
   * @author Greg Loring
   */
  public void clearInspectionImageSets()
  {
    _nameToImageSetMap.clear();
  }

  /**
   * set the fully-qualified name of the .zip archive file to create
   *
   * @author Greg Loring
   */
  public void setDestinationFilename(String fullyQualifiedFilename)
  {
    Assert.expect(fullyQualifiedFilename != null);
    _destinationFilename = fullyQualifiedFilename;
  }

  /**
   * @author Greg Loring
   */
  public boolean hasDestinationFilename()
  {
    boolean result = (_destinationFilename != null);
    return result;
  }

  /**
   * precondition: call setDestinationFilename(...)<p/>
   *
   * @see #setDestinationFilename(String)
   *
   * @author Greg Loring
   */
  public String getDestinationFilename()
  {
    Assert.expect(_destinationFilename != null);
    return _destinationFilename;
  }

  /**
   * set a flag telling whether to add this project to the ProjectDatabase as well as archiving.
   *
   * @todo this method doesn't feel like it belongs here...
   *
   * @author Greg Loring
   */
  public void setAddToProjectDatabase(boolean flag)
  {
    _addToProjectDatabase = flag;
  }

  /**
   * @see #setAddToProjectDatabase(boolean)
   *
   * @author Greg Loring
   */
  public boolean getAddToProjectDatabase()
  {
    return _addToProjectDatabase;
  }

  /**
   * @author George A. David
   */
  private TestProgram getFilteredTestProgram() throws DatastoreException
  {
    Project project = getProject();
    TestProgram testProgram = project.getTestProgram();
    testProgram.clearFilters();
    // set up the test program filters
    if (_requestedRegion != null)
      testProgram.addFilter(_requestedRegion);
    else if (_component != null)
    {
      testProgram.addFilter(_component);
    }
    else
    {
      Assert.expect(_jointPad != null);
      testProgram.addFilter(_jointPad);
    }

    return testProgram;
  }

  /**
   * @author George A. David
   */
  private String getImageSetDescription(List<ReconstructionRegion> regions)
  {
    Assert.expect(regions != null);
    Map<String, Set<String>> refDesToPadNamesMap = new HashMap<String, Set<String>>();
    for(ReconstructionRegion region : regions)
    {
      Component component = region.getComponent();
      if(refDesToPadNamesMap.containsKey(component.getReferenceDesignator()) == false)
      {
        refDesToPadNamesMap.put(component.getReferenceDesignator(), new TreeSet<String>(new AlphaNumericComparator()));
      }
      for(Pad pad : region.getPads())
        refDesToPadNamesMap.get(component.getReferenceDesignator()).add(pad.getName());
    }

    String description = "";
    for(Map.Entry<String, Set<String>> entry : refDesToPadNamesMap.entrySet())
    {
      String refDes = entry.getKey();
      description += refDes + " - ";
      for(String padName : entry.getValue())
        description += " " + padName;
    }

    return description;
  }

  /**
   * @author George A. David
   */
  private Map<String, List<String>> getFilteredImageData() throws DatastoreException
  {
    _tempDirsToDelete.clear();
    Map<String, List<String>> relativePathToPathsMap = new HashMap<String, List<String>>();

    TestProgram testProgram = getFilteredTestProgram();

    List<ReconstructionRegion> regions = testProgram.getFilteredInspectionRegions();
    String description = getImageSetDescription(regions);

    long imageSetTimeStampInMillies = System.currentTimeMillis();
    for (ImageSetData imageSet : _nameToImageSetMap.values())
    {
      // double-check: caller may not have used getAllowableInspectionImageSets()
      Assert.expect(containsRequiredImages(imageSet, regions));

      String oldImageSetDirName = imageSet.getDir();

      // modify the image set class to match the current data
      imageSet.setDateInMils(imageSetTimeStampInMillies);
      imageSet.setImageSetName(Directory.getDirName(imageSetTimeStampInMillies));
      imageSet.setNumberOfImagesSaved(testProgram.getNumberOfInspectionImages());
      imageSet.setSystemDescription(description);
      imageSet.setUserDescription(description);
      imageSet.setIsOnlineTestDevelopment(false);

      // create the relative path
      String tempImageSetDir = imageSet.getDir();
      List<String> paths = new LinkedList<String>();
      paths.add(tempImageSetDir);
      _tempDirsToDelete.add(tempImageSetDir);
      Assert.expect(FileUtilAxi.exists(tempImageSetDir) == false);
      Project project = getProject();
      String projectDir = Directory.getProjectDir(project.getName());
      Assert.expect(tempImageSetDir.toUpperCase().startsWith(projectDir.toUpperCase()));

      String relativePath = tempImageSetDir.substring(projectDir.length() + 1);
      Object previous = relativePathToPathsMap.put(relativePath, paths);
      Assert.expect(previous == null);

      // save the new image set class to a temp location
      ImageSetInfoWriter writer = new ImageSetInfoWriter();
      writer.write(imageSet);

      // save the new image names file to a temp location.
      _imageManager.saveInspectionImageInfo(imageSet, testProgram);

      Assert.expect(FileUtil.exists(oldImageSetDirName));

      // add the image files for each region
      for (ReconstructionRegion region : regions)
      {
        for (Slice slice : region.getReconstructionSlices())
        {
          String imageName = FileName.getInspectionImageName(region.getName(), slice.getSliceName().getId());
          String imageFilename = oldImageSetDirName + File.separator + imageName;
          Assert.expect(FileUtil.exists(imageFilename));
          paths.add(imageFilename);
        }
      }
      // move on to next image set -- so bump of the time stamp here used to create that new image set
      imageSetTimeStampInMillies++;
    }

    return relativePathToPathsMap;
  }

  /**
   * @return List of the names of the files and directories that will be archived
   *
   * @author Greg Loring
   */
  public List<String> getFileAndDirectoryNames() throws DatastoreException
  {
    List<String> fileAndDirectoryNames = new LinkedList<String>();

    // top-level project files
    String projectDirectoryName = Directory.getProjectDir(_projectName);
    File projectDirectory = new File(projectDirectoryName);
    Assert.expect(projectDirectory.exists());
    File[] topLevelFilesAndDirectories = projectDirectory.listFiles();
    for (File topLevelFileOrDirectory: topLevelFilesAndDirectories)
    {
      if (topLevelFileOrDirectory.isFile())
      {
        File topLevelFile = topLevelFileOrDirectory;
        fileAndDirectoryNames.add(topLevelFile.getPath());
      }
    }

    // algorithm-learning directory
    if (_includeAlgorithmLearning)
    {
      String directoryName = Directory.getAlgorithmLearningDir(_projectName);
      Assert.expect(FileUtil.exists(directoryName));
      fileAndDirectoryNames.add(directoryName);
    }

    // inspection images
    if (isFilteringInspectionRegions() == false)
    {
      for (ImageSetData imageSet : _nameToImageSetMap.values())
      {
        String imageSetDirectoryName = imageSet.getDir();
        Assert.expect(FileUtil.exists(imageSetDirectoryName));
        fileAndDirectoryNames.add(imageSetDirectoryName);
      }
    }

//    // verification images
//    if (_includeVerificationImages)
//    {
//      String directoryName = Directory.getXrayVerificationImagesDir(_projectName);
//      Assert.expect(FileUtil.exists(directoryName));
//      fileAndDirectoryNames.add(directoryName);
//    }
    
      //user wish to zip history log file too -hsia-fen.tan
      if(_includeHistoryLog)
      {
      String directoryName = Directory.getHistoryFileDir(_projectName);
        if (FileUtil.exists(directoryName))
          fileAndDirectoryNames.add(directoryName);
        else
          setIncludeHistoryLogFile(false); //if observe there are no history file, dont include history log
      }  

    return fileAndDirectoryNames;
  }

  /**
   * @author Erica Wheatcroft
   */
  public List<String> getVerificationFilesAndDirectoryNames()
  {
    List<String> fileAndDirectoryNames = new ArrayList<String>();
    String directoryName = Directory.getXrayVerificationImagesDir(_projectName);
    Assert.expect(FileUtil.exists(directoryName));
    fileAndDirectoryNames.add(directoryName);

    return fileAndDirectoryNames;

  }

  /**
   * @author Erica Wheatcroft
   */
  public List<String> getFocusImageSetFilesAndDirectoryNames()
  {
    List<String> fileAndDirectoryNames = new ArrayList<String>();
    String directoryName = Directory.getAdjustFocusImagesDir(_projectName);
    Assert.expect(FileUtil.exists(directoryName));
    fileAndDirectoryNames.add(directoryName);

    return fileAndDirectoryNames;

  }

  /**
   * @author Andy Mechtenberg
   */
  public void abort()
  {
    _abort = true;
    FileUtilAxi.cancelZip();
  }

  /**
   * create a .zip archive file at the destination filename including the appropriate files.
   *
   * preconditions: call at least setProjectName(...) and setDestinationFilename(...)<p/>
   *
   * @author Greg Loring
   */
  public void archive() throws DatastoreException
  {
    Assert.expect(_projectName != null);
    Assert.expect(_destinationFilename != null);

    try
    {
      _abort = false;

      List<String> fileAndDirectoryNames = getFileAndDirectoryNames();
      Map<String, List<String>> relativePathToPathsMap = null;
      if (isFilteringInspectionRegions())
        relativePathToPathsMap = getFilteredImageData();

      List<String> verificationFilesAndDirectories = new ArrayList<String>();
      List<String> autoFocusImageSetFilesAndDirectories = new ArrayList<String>();
      String projectDirectoryName = Directory.getProjectDir(_projectName);
      String verificationImagesZipFullPath = projectDirectoryName + File.separator + FileName.getVerificationImagesZipFile();
      String focusImageSetZipFullPath = projectDirectoryName + File.separator + FileName.getFocusImageSetZipFile();

      if (_includeVerificationImages)
      {
        verificationFilesAndDirectories = getVerificationFilesAndDirectoryNames();
        FileUtilAxi.zip(verificationFilesAndDirectories, verificationImagesZipFullPath, Directory.getXrayVerificationImagesDir(_projectName), true);
        fileAndDirectoryNames.add(verificationImagesZipFullPath);
      }

      if (_includeFocusImageSet)
      {
        autoFocusImageSetFilesAndDirectories = getFocusImageSetFilesAndDirectoryNames();
        FileUtilAxi.zip(autoFocusImageSetFilesAndDirectories, focusImageSetZipFullPath, Directory.getAdjustFocusImagesDir(_projectName), true);
        fileAndDirectoryNames.add(focusImageSetZipFullPath);
      }

      if (relativePathToPathsMap == null)
        FileUtilAxi.zip(fileAndDirectoryNames, _destinationFilename, projectDirectoryName, true);
      else
      {
        relativePathToPathsMap.put("", fileAndDirectoryNames);
        FileUtilAxi.zipWithNewRelativePath(relativePathToPathsMap, _destinationFilename, false);
      }

      // now delete the verification images zip
      File verificationImagesZipFile = new File(verificationImagesZipFullPath);

      if (verificationImagesZipFile.exists())
        verificationImagesZipFile.delete();

      // now delete the focus image set zip
      File focusImageSetZipFile = new File(focusImageSetZipFullPath);

      if (focusImageSetZipFile.exists())
        focusImageSetZipFile.delete();

      if (_abort)
        return;

      if (_addToProjectDatabase)
      {
        ProjectDatabase.addProject(_projectName);
      }
    }
    finally
    {
      // now delete the temporary directories.
      FileUtilAxi.delete(_tempDirsToDelete);
      _tempDirsToDelete.clear();
    }
  }

  /**
   * Given the full path to a zip file, unzip the file. If the zip file contains a zip for verifiation
   * images, the zip file we be zipped accordingly.
   *
   * @author Erica Wheatcroft
   */
  public void unzip(String zipFileFullPath, String destinationFullPath) throws DatastoreException
  {
    Assert.expect(zipFileFullPath != null);
    Assert.expect(destinationFullPath != null);

    // lets make sure the zip file exists
    File zipFile = new File(zipFileFullPath);
    File destinationFile = new File(destinationFullPath);
  
    // Unzip NDF File
    if(zipFile.exists() == false)
      zipFileFullPath = searchNdfZip(destinationFile);
      
    Assert.expect(zipFileFullPath != null);
    
    // lets unzip the zip file
    boolean retainOriginalFileTimeStamps = false;
    FileUtilAxi.unZip(zipFileFullPath, destinationFullPath, retainOriginalFileTimeStamps);

    // now lets check to see if this zip file contained a zip file for verification images if so then unzip that
    // since the user can not rename the zip file, then we can get the project name from the zip file name.
    String projectName = FileUtil.getNameWithoutExtension(zipFileFullPath);
    String projectDirectoryName = Directory.getProjectDir(projectName);
    String verificationImagesZipFullPath = projectDirectoryName + File.separator + FileName.getVerificationImagesZipFile();
    String focusImageSetZipFullPath = projectDirectoryName + File.separator + FileName.getFocusImageSetZipFile();

    File verificationImagesZipFile = new File(verificationImagesZipFullPath);
    File focusImageSetZipFile = new File(focusImageSetZipFullPath);
    if (verificationImagesZipFile.exists())
    {
      // so the file does exist so lets unzip it and then remove it.
      FileUtilAxi.unZip(verificationImagesZipFullPath, Directory.getXrayVerificationImagesDir(projectName), retainOriginalFileTimeStamps);
      verificationImagesZipFile.delete();
    }
    if(focusImageSetZipFile.exists())
    {
      // so the file does exist so lets unzip it and then remove it.
      FileUtilAxi.unZip(focusImageSetZipFullPath, Directory.getAdjustFocusImagesDir(projectName), retainOriginalFileTimeStamps);
      focusImageSetZipFile.delete();
    }
  }

  /**
   * Searching for NDF zip file recursively in the specified directory
   * @author Bee Hoon
   */
   private String searchNdfZip(File file) 
   { 
     String ndfZipFileFullPath = null;
     
     if (file.isDirectory()) 
     {
       if (file.canRead()) 
       {
	 for (File temp : file.listFiles()) 
         {	
           if (temp.isDirectory()) 
             searchNdfZip(temp);
           else 
           {
	     if (temp.getName().endsWith(NDF_ZIP_EXTENSION)) 
               ndfZipFileFullPathList.add(temp.getAbsoluteFile().toString());
	   }
	 }
	} 
       else 
	 System.out.println("Cannot read " + file.getAbsoluteFile());
      }
     
     for (String matched : ndfZipFileFullPathList)
     {
       if(matched.endsWith(NDF_ZIP_EXTENSION))
         ndfZipFileFullPath = matched;
     }
     
     Assert.expect(ndfZipFileFullPath != null);
     return ndfZipFileFullPath;
    }
   
  /**
   * @author Greg Loring
   */
  private boolean isFilteringInspectionRegions()
  {
    boolean result = (_component != null) || (_jointPad != null) || (_requestedRegion != null);
    return result;
  }

  /**
   * try to avoid reloading the Project object
   *
   * @todo maybe I should just change setProjectName(String) to setProject(Project)
   *
   * @author Greg Loring
   */
  private Project getProject() throws DatastoreException
  {
    Assert.expect(_projectName != null);

    Project project = null;
    if (_component != null)
    {
      checkComponent(_component);
      project = _component.getBoard().getPanel().getProject();
    }
    else if (_jointPad != null)
    {
      checkComponent(_jointPad.getComponent());
      project = _jointPad.getComponent().getBoard().getPanel().getProject();
    }
    else
    {
      // if ever get here, then change setProjectName(String) to setProject(Project)
      Assert.expect(false, "SHOULD NOT have to load a project from the filesystem (2+ copies)");
    }
    return project;
  }

  /**
   * which inspection regions are required?
   *
   * @author Greg Loring
   */
  private List<ReconstructionRegion> getRequiredInspectionRegions() throws DatastoreException
  {
    Assert.expect(isFilteringInspectionRegions());

    if (_requestedRegion != null)
    {
      List<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>(1);
      regions.add(_requestedRegion);
      return regions;
    }
    Project project = getProject(); /** @todo could be very long call, setProject(Project) instead? */

    TestProgram testProgram = project.getTestProgram();
    testProgram.clearFilters();
    if (_component != null)
    {
      testProgram.addFilter(_component);
    }
    else
    {
      Assert.expect(_jointPad != null);
      testProgram.addFilter(_jointPad);
    }
    List<ReconstructionRegion> regions = testProgram.getFilteredInspectionRegions();

    Assert.expect(regions != null);
    Assert.expect(regions.isEmpty() == false);
    testProgram.clearFilters();
    return regions;
  }

  /**
   * does this image set contain images for all of the given inspection regions?
   *
   * @author Greg Loring
   * @author Aimee Ong
   */
  private boolean containsRequiredImages(ImageSetData imageSet, List<ReconstructionRegion> regions) throws DatastoreException
  {
    boolean result = true;
    Set<ReconstructionRegion> recoRegionsWithCompatibleImages = _imageManager.whichReconstructionRegionsHaveCompatibleImages(imageSet, regions);
    if (recoRegionsWithCompatibleImages.size() != regions.size())
      result = false;
    return result;
  }

  /**
   * @author Andy Mechtenberg
   */
  public int getApproximateZippedSizeInBytes()
  {
    Assert.expect(hasProjectName());

    // get project directory. we want only files and not directories
    String projectDirectory = Directory.getProjectDir(getProjectName());
    java.util.List<String> files = FileUtil.listFiles(projectDirectory);
    long sizeOFDirectoryInBytes = 0;
    for (String fileName : files)
    {
      File file = new File(fileName);
      if (file.isFile())
        sizeOFDirectoryInBytes += FileUtil.getFileSizeInBytes(fileName);
    }
    // now apply the compression ratio to this value
    double currentZipSizeInBytes = sizeOFDirectoryInBytes * _COMPRESSION_PERCENT_FOR_PROJECT_DATA;

    // check for learned data (will be zero if no learned data exists)
    currentZipSizeInBytes += calculateLearnedDataSizeInBytes();

    // then check to see if they have selected verification images if so add that in
    if (getIncludeVerificationImages())
      currentZipSizeInBytes += getCompressedVerificationImagesSizeInBytes(); // don't recalculate calculateVerificationImagesSizeInBytes();
    // check to see if they have selected any image sets if so add that in


    // if hasJointPad is true, then we'll only include a handful of single small images.
    if ((getNumberOfImageSetsToArchive() > 0) && (hasJointPad() == false))
    {
      long sizeOfSelectedImageSetsInBytes = 0;
      java.util.List<ImageSetData> imageSets = getImageSetsToArchive();
      for (ImageSetData imageSet : imageSets)
      {
        if (hasJointPad())
          sizeOfSelectedImageSetsInBytes += imageSet.getEstimatedSizeInBytes(true) * _COMPRESSION_PERCENT_FOR_INSPECTION_IMAGES;
        else
          sizeOfSelectedImageSetsInBytes += imageSet.getEstimatedSizeInBytes(false);
      }
      currentZipSizeInBytes += (sizeOfSelectedImageSetsInBytes * _COMPRESSION_PERCENT_FOR_INSPECTION_IMAGES);
    }
    return (int)currentZipSizeInBytes;
  }

  /**
   * @author Erica Wheatcroft
   */
  public double getCompressedVerificationImagesSizeInBytes()
  {
    Assert.expect(hasProjectName());
    String verificationImagesDir = Directory.getXrayVerificationImagesDir(getProjectName());
    long directorySizeInBytes = FileUtil.getSizeOfDirectoryContentsInBytes(verificationImagesDir);
    double compressedSizeInBytes = directorySizeInBytes * _COMPRESSION_PERCENT_FOR_VERIFICATION_IMAGES;
    return compressedSizeInBytes;
  }

  /**
   * @author Andy Mechtenberg
   */
  private double calculateLearnedDataSizeInBytes()
  {
    Assert.expect(hasProjectName());
    if (doesAlgorithmLearningExist())
    {
      String learnedDataDir = Directory.getAlgorithmLearningDir(getProjectName());
      long directorySizeInBytes = FileUtil.getSizeOfDirectoryContentsInBytes(learnedDataDir);
      double compressedSizeInBytes = directorySizeInBytes * this._COMPRESSION_PERCENT_FOR_LEARNED_DATA;
      return compressedSizeInBytes;
    }
    else
      return 0.0;
  }

  /**
   * @author Erica Wheatcroft
   */
  private double calculateFocusImageSetInBytes()
  {
    Assert.expect(hasProjectName());
    String focusImageSetDir = Directory.getAdjustFocusImagesDir(getProjectName());
    long directorySizeInBytes = FileUtil.getSizeOfDirectoryContentsInBytes(focusImageSetDir);
    double compressedSizeInBytes = directorySizeInBytes * _COMPRESSION_PERCENT_FOR_VERIFICATION_IMAGES;
    return compressedSizeInBytes;
  }
}
