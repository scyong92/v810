package com.axi.v810.business;

import java.util.*;
import java.util.regex.*;

import com.axi.util.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;


/**
 * Use this class to look up a project name from a serial number or to store
 * new serial number regulare expression to project names for future use.
 *
 * @author Bill Darbie
 */
public class SerialNumberToProjectName
{
  private SerialNumberToProjectReader _reader;
  private SerialNumberToProjectWriter _writer;

  /**
   * @author Bill Darbie
   */
  public SerialNumberToProjectName()
  {
    _reader = SerialNumberToProjectReader.getInstance();
    _writer = SerialNumberToProjectWriter.getInstance();
  }

  /**
   * USE ONLY FOR UNIT TESTING!!
   * @author Bill Darbie
   */
  void setConfigFileNameForUnitTesting(String fileName)
  {
    Assert.expect(fileName != null);

    _reader.setConfigFileNameForUnitTesting(fileName);
    _writer.setConfigFileNameForUnitTesting(fileName);
  }

  /**
   * Given a serial number, return the project name.
   * @return the project name that corresponds to the serial number passed in.  If the
   * serial number does not match, return an empty string.
   *
   * @author Bill Darbie
   */
  public String getProjectNameFromSerialNumber(String serialNumber) throws DatastoreException
  {
    Assert.expect(serialNumber != null);

    String projectName = "";

    Assert.expect(_reader != null);
    Collection<SerialNumberRegularExpressionData> serialNumberDataCollection = _reader.getSerialNumberRegularExpressionData();

    // see if the serial number entered matches anything in the serial number file
    // iterate over the regular expressions in the same order they were listed in the file
    for (SerialNumberRegularExpressionData serialNumberData : serialNumberDataCollection)
    {
      String regEx = serialNumberData.getRegularExpression();
      boolean interpretAsRegEx = serialNumberData.isInterpretedAsRegularExpression();
      Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
      Matcher matcher = pattern.matcher(serialNumber);
      if (matcher.find())
      {
        // we found a match, get the panel name
        projectName = serialNumberData.getProjectName();
        break;
      }
    }

    Assert.expect(projectName != null);
    return projectName;
  }

  /**
   * @return a Collection of SerialNumberRegularExpressionData.
   * @author Bill Darbie
   */
  public Collection<SerialNumberRegularExpressionData> getSerialNumberRegularExpressionData() throws DatastoreException
  {
    Assert.expect(_reader != null);

    return _reader.getSerialNumberRegularExpressionData();
  }

  /**
   * @author Bill Darbie
   */
  public void saveSerialNumberRegularExpressionData(Collection<SerialNumberRegularExpressionData> serialNumberRegularExpressionData) throws DatastoreException
  {
    Assert.expect(serialNumberRegularExpressionData != null);

    Assert.expect(_writer != null);
    _writer.write(serialNumberRegularExpressionData);
  }
}
