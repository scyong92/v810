package com.axi.v810.business;

import com.axi.util.*;

/**
 * @author Bill Darbie
 */
public class ProjectChangeTypeEnum extends com.axi.util.Enum
{
  private static int _index = -1;

  public static ProjectChangeTypeEnum SET = new ProjectChangeTypeEnum(++_index);
  public static ProjectChangeTypeEnum CREATE_OR_DESTROY = new ProjectChangeTypeEnum(++_index);
  public static ProjectChangeTypeEnum ADD_OR_REMOVE = new ProjectChangeTypeEnum(++_index);

  /**
   * @author Bill Darbie
   */
  protected ProjectChangeTypeEnum(int id)
  {
    super(id);
  }
}
