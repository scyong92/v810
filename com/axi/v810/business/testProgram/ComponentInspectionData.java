package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;

/**
 * Holds the information necessary for inspecting Components.
 *
 * @author Peter Esbensen
 */
public class ComponentInspectionData implements Serializable
{
  protected Component _component;
  private transient ComponentInspectionResult _componentInspectionResult = null;
  protected TestProgram _testProgram;
  protected JointTypeEnum _jointTypeEnum;

  /**
   * @author Peter Esbensen
   */
  public ComponentInspectionData(Project project)
  {
    Assert.expect(project != null);

    _testProgram = project.getTestProgram();
  }

  /**
   * //XCR-3455, Wrong image display if pad 1 is different joint type
   * @author Yong Sheng Chuan
   */
  public void setJointTypeEnum(JointTypeEnum jointTypeEnum)
  {
    Assert.expect(jointTypeEnum != null);
    _jointTypeEnum = jointTypeEnum;
  }
  
  /**
   * @author Peter Esbensen
   */
  public Component getComponent()
  {
    Assert.expect(_component != null);
    return _component;
  }

  /**
   * @author Peter Esbensen
   */
  public void setComponent(Component component)
  {
    Assert.expect(component != null);
    _component = component;
  }

  /**
   * @author Peter Esbensen
   */
  public Set<JointInspectionData> getInspectableJointInspectionDataSet()
  {
    Set<JointInspectionData> jointInspectionDataSet = new HashSet<JointInspectionData>();

    for (Pad pad : _component.getPads())
    {
      if (pad.isInspected())
      {
        jointInspectionDataSet.add(_testProgram.getJointInspectionData(pad));
      }
    }
    return jointInspectionDataSet;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public Set<JointInspectionData> getJointInspectionDataSet()
  {
    Set<JointInspectionData> jointInspectionDataSet = new HashSet<JointInspectionData>();

    for (Pad pad : _component.getPads())
    {
      jointInspectionDataSet.add(_testProgram.getJointInspectionData(pad));
    }

    return jointInspectionDataSet;
  }

  /**
   *
   * @author Peter Esbensen
   */
  public JointInspectionData getPadOneJointInspectionData()
  {
    //XCR-3455, Wrong image display if pad 1 is different joint type
    Pad padOne = null;
    if (_jointTypeEnum != null)
      padOne = _component.getPadOne(_jointTypeEnum);
    else
      padOne = _component.getPadOne();
    if(padOne.isInspected())
      return _testProgram.getJointInspectionData(padOne);
    else
    {
      for(Pad pad : _component.getPads())
      {
        if(pad.isInspected())
          return _testProgram.getJointInspectionData(pad);
      }
    }
    return null;
  }

  /**
   * Do not call this on anything that is not a two pin device.
   *
   * @author Peter Esbensen
   */
  public JointInspectionData getPadTwoJointInspectionDataOnTwoPinDevices()
  {
    Assert.expect(_component.getPads().size() == 2);
    JointInspectionData padTwoJointInspectionData = null;
    Pad padOne = _component.getPadOne();
    for (Pad pad : _component.getPads())
    {
      if (_jointTypeEnum != null && _jointTypeEnum.equals(pad.getJointTypeEnum()) == false)
        continue;
      
      if (pad != padOne)
        padTwoJointInspectionData = _testProgram.getJointInspectionData(pad);
    }
    return padTwoJointInspectionData;
  }

  /**
   * @author Patrick Lacz
   */
  public synchronized ComponentInspectionResult getComponentInspectionResult()
  {
    if (_componentInspectionResult == null)
    {
      ComponentInspectionResult componentInspectionResult = new ComponentInspectionResult(this);

      componentInspectionResult.setInspectionRunId(_testProgram.getInspectionRunId());
      _componentInspectionResult = componentInspectionResult;
      return _componentInspectionResult;
    }
    int componentInspectionResultRunId = _componentInspectionResult.getInspectionRunId();
    int testProgramRunId = _testProgram.getInspectionRunId();

    Assert.expect(componentInspectionResultRunId <= testProgramRunId);
    if (componentInspectionResultRunId < testProgramRunId)
    {
      ComponentInspectionResult componentInspectionResult = new ComponentInspectionResult(this);

      componentInspectionResult.setInspectionRunId(_testProgram.getInspectionRunId());
      _componentInspectionResult = componentInspectionResult;
      return _componentInspectionResult;
    }

    Assert.expect(_componentInspectionResult != null);
    return _componentInspectionResult;
  }

  /**
   * @author Matt Wharton
   */
  public void setComponentInspectionResult(ComponentInspectionResult componentInspectionResult)
  {
    Assert.expect(componentInspectionResult != null);

    _componentInspectionResult = componentInspectionResult;
  }

  /**
   * @author Matt Wharton
   */
  public void clearComponentInspectionResult()
  {
    _componentInspectionResult = null;
  }

  /**
   * Get the region of interest for the component relative to the
   * inspection region.  This will ONLY work for components that are contained
   * all in one region.

   * @author Matt Wharton
   */
  public RegionOfInterest getRegionOfInterestRelativeToInspectionRegionInPixels(boolean isResizeNeeded)
  {
    RegionOfInterest componentRegionOfInterest = null;
    ReconstructionRegion reconstructionRegion = null;
    RegionShapeEnum regionShapeEnum = null;
    
    // initialize region to be the same as pad one, that way we have the right orientation
    componentRegionOfInterest = new RegionOfInterest(getPadOneJointInspectionData().getRegionOfInterestRelativeToInspectionRegionInPixels(false), regionShapeEnum.RECTANGULAR);
    reconstructionRegion = getPadOneJointInspectionData().getInspectionRegion();

    for (Pad pad : _component.getPads())
    {
      JointInspectionData jointInspectionData = _testProgram.getJointInspectionData(pad);
      if (reconstructionRegion == jointInspectionData.getInspectionRegion())
      {
        componentRegionOfInterest.add(jointInspectionData.getRegionOfInterestRelativeToInspectionRegionInPixels(false));
      }
//      Assert.expect(reconstructionRegion == jointInspectionData.getInspectionRegion(),
//                    "do not call getRegionOfInterestRelativeToInspectionRegionInPixels on components that span multiple reconstructionRegions!");
    }

    return componentRegionOfInterest;
  }


  /**
   * Get the orthogonal region of interest for the component relative to the rotated
   * inspection region.  This will ONLY work for components that are contained
   * all in one region.
   *
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   * 
   * @author Peter Esbensen
   */
  public RegionOfInterest getOrthogonalComponentRegionOfInterest()
  {
    return getOrthogonalComponentRegionOfInterest(true);
  }
  /**
   * Get the orthogonal region of interest for the component relative to the rotated
   * inspection region.  This will ONLY work for components that are contained
   * all in one region.
   * 
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   *
   * @author Peter Esbensen
   */
  public RegionOfInterest getOrthogonalComponentRegionOfInterest(boolean resizeIfNeeded)
  {
    RegionOfInterest componentRegionOfInterest = null;
    ReconstructionRegion reconstructionRegion = null;

    // initialize region to be the same as pad one, that way we have the right orientation
    componentRegionOfInterest = new RegionOfInterest(getPadOneJointInspectionData().getOrthogonalRegionOfInterestInPixels(resizeIfNeeded));
    reconstructionRegion = getPadOneJointInspectionData().getInspectionRegion();

    for (Pad pad : _component.getPads())
    {
      //Siew Yeng - XCR-2171 - fix crash when select at component for component level voiding measurement
      //Sheng Chuan - XCR-3574 - System crash when click untestable component on Result tab
      //Khaw Chek Hau - XCR-3846 - Software crash when viewing component joint image in fine tuning
      if(pad.isInspected()== false || ( _jointTypeEnum != null && _jointTypeEnum.equals(pad.getJointTypeEnum()) == false))
        continue;
      
      JointInspectionData jointInspectionData = _testProgram.getJointInspectionData(pad);
       if (reconstructionRegion == jointInspectionData.getInspectionRegion())
      {
        componentRegionOfInterest.add(jointInspectionData.getOrthogonalRegionOfInterestInPixels(resizeIfNeeded));
      }
//      Assert.expect(reconstructionRegion == jointInspectionData.getInspectionRegion(),
//                    "do not call getOrthogonalComponentRegionOfInterest on components that span multiple reconstructionRegions!");
    }

    return componentRegionOfInterest;
  }
}
