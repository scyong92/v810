package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;
import java.util.zip.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAcquisition.*;
import com.axi.v810.business.imageAcquisition.ImageAcquisitionModeEnum;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testExec.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.business.virtualLive.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
 * TestProgram encapsulates InspectionRegions, AlignmentRegions and other information that will be passed
 * from the Program Generation subsystem to the Test Execution Engine.
 * @author Peter Esbensen
 */
public class TestProgram implements Serializable
{

  private Project _project;
  private TestProgramGenerationData _testProgramGenerationData;
  private List<TestSubProgram> _testSubPrograms = new LinkedList<TestSubProgram>();
  // program filters
  private Set<Board> _boardsToFilterOn = new HashSet<Board>();
  private Set<Component> _componentsToFilterOn = new HashSet<Component>();
  private Set<Pad> _padsToFilterOn = new HashSet<Pad>();
  private Set<Subtype> _subtypesToFilterOn = new HashSet<Subtype>();
  private Set<Subtype> _subtypesToExclude = new HashSet<Subtype>();
  private Set<JointTypeEnum> _jointTypesToFilterOn = new HashSet<JointTypeEnum>();
  private Set<ReconstructionRegion> _regionsToFilterOn = new HashSet<ReconstructionRegion>();
  private Set<TestSubProgram> _subProgramsToFilterOn = new HashSet<TestSubProgram>();
  private boolean _useOnlyInspectedInspectionRegions = false;
  private boolean _createOnlyInspectedSlices = false;
  private List<AlignmentGroup> _invalidatedAlignmentGroups = new LinkedList<AlignmentGroup>();
  private Map<Component, ComponentInspectionData> _componentToComponentInspectionDataMap = new HashMap<Component, ComponentInspectionData>();
  private boolean _useOnlyInspectedRegionInScanPath = true;
  private boolean _isRealignPerformed = false;
  private boolean _isRerunningProduction = false;
  private ImageAcquisitionModeEnum _imageAcquisitionModeInScanPath = null;
  private transient SurroundingPadUtil _surroundingPadUtil;
  // this is incremented every time an inspection is run
  /** @todo wpd - remove this? peter */
  private transient int _inspectionRunId;
  private transient ProgramGeneration _programGeneration;
  private transient List<XrayCameraAcquisitionParameters> _xrayCameraAcquisitionParameters;
//  private transient Map<AbstractXrayCamera, XrayCameraAcquisitionParameters> _xrayCameraAcquisitionParameters;
  private long _checkSum = -1;
  private long _opticalInspectionChecksum = -1;
  private long _alignmentOpticalInspectionChecksum = -1;
  private boolean _debugCheckSumTiming = false;
  private transient boolean _neighboringHasAssigned = false;
  private Map<Integer, TestSubProgram> _testSubProgramMap = new HashMap<Integer, TestSubProgram>();
  private Map<Integer, Integer> _totalLowMagReconstructionRegionToSubProgramMap = new HashMap<Integer, Integer>();
  private Map<Integer, Integer> _totalHighMagReconstructionRegionToSubProgramMap = new HashMap<Integer, Integer>();
  private Set<TestSubProgram> _notInspectableTestSubProgramFilters = new HashSet<TestSubProgram>();
  
  private Map<UserGainEnum, TestSubProgram> _userGainToSubProgramMap = new HashMap<UserGainEnum, TestSubProgram>();
  private Map<StageSpeedEnum, TestSubProgram> _stageSpeedToSubProgramMap = new HashMap<StageSpeedEnum, TestSubProgram>();
  // XCR1380
  // Added by Seng Yew on 02-Nov-2011
  // Note : I declare this variable as transient because I don't want it to be serialized into file.
  //        But I cannot do this, because after deserialized from file, it will become null.
  //        Declare it as static so that it will not become null after deserialized from file.
  //        Alternative solution:
  //        1) declare it as final, but we cannot for this variable.
  //        2) initialize this variable but need more code changes. 
  private static Map<Integer, Map<Integer, Integer>> _rridToSliceIdReconstructedSliceMap = new HashMap<Integer, Map<Integer,Integer>>();
  
  //Khaw Chek Hau - XCR2285: 2D Alignment on v810
  private transient boolean _isManual2DAlignmentInProgress = false;
  private transient Set<VelocityMappingParameters> _velocityMappingParametersList = new HashSet<VelocityMappingParameters> ();
  //Khaw Chek Hau - XCR3761 : Skip generating 2D verification image if the panel is not unloaded
  private transient boolean _isPanelRemainsWithoutUnloadedFor2DVerificationImage = false;
  
  /**
   * @author Peter Esbensen
   */
  public TestProgram()
  {
    _neighboringHasAssigned = false;
  }

  /**
   * @author George A. David
   */
  public void setTestProgramGenerationData(TestProgramGenerationData generationData)
  {
    Assert.expect(generationData != null);

    _testProgramGenerationData = generationData;
  }

  /**
   * @author George A. David
   */
  public TestProgramGenerationData getTestProgramGenerationData()
  {
    Assert.expect(_testProgramGenerationData != null);

    return _testProgramGenerationData;
  }

  /**
   * @author Matt Wharton
   */
  public Project getProject()
  {
    Assert.expect(_project != null);

    return _project;
  }

  /**
   * @author Roy Williams
   */
  public void clearProject()
  {
    _project = null;
  }

  /**
   * @author Matt Wharton
   */
  public void setProject(Project project)
  {
    Assert.expect(project != null);

    _project = project;
  }

  /**
   * @author George A. David
   */
  public int getNumberOfInspectionImages()
  {
    int numImages = 0;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      numImages += subProgram.getNumberOfFilteredInspectionImages();
    }

    return numImages;
  }

  /**
   * @author George A. David
   */
  public int getNumberOfInspectedInspectionImages()
  {
    int numImages = 0;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      numImages += subProgram.getNumberOfFilteredInspectionImages();
    }

    return numImages;
  }

  /**
   * @author George A. David
   */
  public double getEstimatedSizeOfInspectionImagesInMegaBytes()
  {
    // i have 4298 images on by sytem that are 1024 x 1024 pixels each.
    // the size on disk is about 3.18GB. So that gives us about 740kB
    // per image. Then divide that by the number of pixels per image,
    // 1,048,576 (1024 * 1024) gives us about 0.7 bytes per pixel.
    // or 0.0000007MB per pixel

    double megaBytesPerPixel = 0.00000007;
    long totalPixels = 0;
    double totalSize = 0;
    for (ReconstructionRegion region : getFilteredInspectionRegions())
    {
      ImageRectangle imageRect = region.getRegionRectangleInPixels();
      int numPixels = imageRect.getWidth() * imageRect.getHeight();
      double size = numPixels * megaBytesPerPixel;
      for (FocusGroup group : region.getFocusGroups())
      {
        for (Slice slice : group.getSlices())
        {
          if (slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION) || slice.getSliceType().equals(SliceTypeEnum.PROJECTION))
          {
            totalPixels += numPixels;
            totalSize += size;
          }
        }
      }
    }

    return totalPixels * megaBytesPerPixel;
  }

  /**
   * @author George A. David
   */
  public boolean doesRegionPassFilters(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    if (_regionsToFilterOn.isEmpty() == false && _regionsToFilterOn.contains(reconstructionRegion) == false)
    {
      return false;
    }

    // if boards are supplied to filter on, then see if the reconstruction regions tests one of those
    // boards, if not, then it does not pass the filter.
    if (_boardsToFilterOn.isEmpty() == false && _boardsToFilterOn.contains(reconstructionRegion.getBoard()) == false)
    {
      return false;
    }

    // if components are supplied to filter on, then see if the reconstruction regions tests one of those
    // components, if not, then it does not pass the filter.
    if (_componentsToFilterOn.isEmpty() == false && _componentsToFilterOn.contains(reconstructionRegion.getComponent()) == false)
    {
      return false;
    }

    // if pads are supplied to filter on, then see if the reconstruction regions tests one of those
    // pads, if not, then it does not pass the filter.
    if (_padsToFilterOn.isEmpty() == false)
    {
      boolean wasPadFound = false;
      for (Pad pad : _padsToFilterOn)
      {
        if (reconstructionRegion.isPadInRegion(pad))
        {
          wasPadFound = true;
          break;
        }
      }

      if (wasPadFound == false)
      // the region does not pass the filter
      {
        return false;
      }
    }
    // if subtypes are supplied to filter on, then see if the reconstruction regions tests one of those
    // subtypes, if not, then it does not pass the filter.
    if (_subtypesToFilterOn.isEmpty() == false)
    {
      boolean wasSubtypeFound = false;
      for (Subtype subtype : _subtypesToFilterOn)
      {
        if (reconstructionRegion.hasSubtype(subtype))
        {
          // however, if this subtype is to be excluded, is still does not pass the filters.
          if (_subtypesToExclude.contains(subtype) == false)
          {
            wasSubtypeFound = true;
            break;
          }
        }
      }

      if (wasSubtypeFound == false)
      {
        return false;
      }
    }
    // if there are subtypes to exclude (predictive slice height turned on), then return false as soon as one
    // of the subtypes is found in the given reconstruction region.
    else if (_subtypesToExclude.isEmpty() == false)
    {
      for (Subtype subtype : _subtypesToExclude)
      {
        if (reconstructionRegion.hasSubtype(subtype))
        {
          return false;
        }
      }
    }

    // if joint types are supplied to filter on, then see if the reconstruction regions tests one of those
    // joint types, if not, then it does not pass the filter.
    if (_jointTypesToFilterOn.isEmpty() == false && _jointTypesToFilterOn.contains(reconstructionRegion.getJointTypeEnum()) == false)
    {
      return false;
    }

    if (_useOnlyInspectedInspectionRegions && reconstructionRegion.isInspected() == false)
    {
      return false;
    }



    return true;
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getAllInspectionRegions()
  {
    List<ReconstructionRegion> inspectionReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      inspectionReconstructionRegions.addAll(subProgram.getAllInspectionRegions());
    }

    return inspectionReconstructionRegions;
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getFilteredInspectionRegions()
  {
    List<ReconstructionRegion> inspectionReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      inspectionReconstructionRegions.addAll(subProgram.getFilteredInspectionRegions());
    }

    return inspectionReconstructionRegions;
  }
  
  /**
   * @author Chong, Wei Chin
   */
  public List<ReconstructionRegion> getFilteredInspectionRegionsFromFilterTestSubProgram()
  {
    List<ReconstructionRegion> inspectionReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : getFilteredTestSubPrograms())
    {
      inspectionReconstructionRegions.addAll(subProgram.getFilteredInspectionRegions());
    }

    return inspectionReconstructionRegions;
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getVerificationRegions()
  {
    List<ReconstructionRegion> verificationReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      verificationReconstructionRegions.addAll(subProgram.getVerificationRegions());
    }

    return verificationReconstructionRegions;
  }

  /**
   * @author Bee Hoon
   */
  public List<ReconstructionRegion> getRightVerificationRegions()
  {
    List<ReconstructionRegion> verificationReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT))
      {
        verificationReconstructionRegions.addAll(subProgram.getVerificationRegions());
      } 
    }
    
    return verificationReconstructionRegions;
  }
  
  /**
   * @author Bee Hoon
   */
  public List<ReconstructionRegion> getLeftVerificationRegions()
  {
    List<ReconstructionRegion> verificationReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
      {
        verificationReconstructionRegions.addAll(subProgram.getVerificationRegions());
      }
    }
    
    return verificationReconstructionRegions;
  }
  
  /**
   * @author George A. David
   */
  public int getNumberOfVerificationRegions()
  {
    return getVerificationRegions().size();
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getAlignmentRegions()
  {
    List<ReconstructionRegion> alignmentReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      alignmentReconstructionRegions.addAll(subProgram.getAlignmentRegions());
    }

    return alignmentReconstructionRegions;
  }

  /**
   * @author George A. David
   */
  public ReconstructionRegion getAlignmentRegion(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);

    ReconstructionRegion alignmentRegion = null;

    for (ReconstructionRegion region : getAlignmentRegions())
    {
      if (region.getAlignmentGroup() == alignmentGroup)
      {
        alignmentRegion = region;
      }
    }

    Assert.expect(alignmentRegion != null);
    return alignmentRegion;
  }

  /**
   * @author George A. David
   */
  public TestSubProgram getTestSubProgram(AlignmentGroup alignmentGroup, MagnificationTypeEnum magnificationType)
  {
    TestSubProgram testSubProgram = null;

    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getMagnificationType().equals(magnificationType) && 
          subProgram.hasAlignmentGroup(alignmentGroup))
      {
        testSubProgram = subProgram;
        break;
      }
    }

    Assert.expect(testSubProgram != null);
    return testSubProgram;
  }
  
  /**
   * @author Chong Wei Chin
   */
  public TestSubProgram getTestSubProgramForHighMag(AlignmentGroup alignmentGroup)
  {
    TestSubProgram testSubProgram = null;

    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getMagnificationType().equals(MagnificationTypeEnum.HIGH) && 
          subProgram.hasAlignmentGroup(alignmentGroup))
      {
        testSubProgram = subProgram;
      }
    }

    Assert.expect(testSubProgram != null);
    return testSubProgram;
  }

  /**
   * @author George A. David
   */
  public Collection<AlignmentGroup> getAlignmentGroups()
  {
    List<AlignmentGroup> alignmentGroups = new LinkedList<AlignmentGroup>();

    for (TestSubProgram subProgram : _testSubPrograms)
    {
      alignmentGroups.addAll(subProgram.getAlignmentGroups());
    }

    return alignmentGroups;
  }

  /**
   * @author George A. David
   * @author Kee chin Seong
   */
  public void addTestSubProgram(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);

    _testSubPrograms.add(testSubProgram);
    _testSubProgramMap.put(testSubProgram.getId(), testSubProgram);
    _userGainToSubProgramMap.put(testSubProgram.getUserGain(), testSubProgram);
    _stageSpeedToSubProgramMap.put(testSubProgram.getStageSpeed(), testSubProgram);
  }

  /**
   * @author George A. David
   */
  public List<TestSubProgram> getAllTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);

    return _testSubPrograms;
  }

  /**
   * @author George A. David
   */
  public List<TestSubProgram> getFilteredTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);

    List<TestSubProgram> subPrograms = new LinkedList<TestSubProgram>(_testSubPrograms);
    if (_subProgramsToFilterOn.isEmpty() == false)
    {
      subPrograms.retainAll(_subProgramsToFilterOn);
    }
    
    // Handling of board-based alignment filtering
    List<TestSubProgram> boardTestSubPrograms = new LinkedList<TestSubProgram>();
    if (getProject().getPanel().getPanelSettings().isPanelBasedAlignment() == false)
    {
      if (_boardsToFilterOn.isEmpty() == false)
      {
        for(TestSubProgram filteredTestSubProgram : subPrograms)
        {
          if (_boardsToFilterOn.contains(filteredTestSubProgram.getBoard()))
            boardTestSubPrograms.add(filteredTestSubProgram);
        }
      }
    }
    
    if (boardTestSubPrograms.isEmpty() == false)
      subPrograms.retainAll(boardTestSubPrograms);

    return subPrograms;
  }
  
  /*
   * @author Kee Chin Seong
   * - The purpose i added this new function is because not affect the previous
   *   software's architecture. 
   * - Please take note that, this different with getAllTestSubPrograms.
   * - getAllTestSubPrograms is only get _testSubPrograms ( NOT INCLUDED NO INSPECTABLED TESTSUBPROGRAM)
   *   - this will affect graphic enginer, renderers... etc
   * - this current function is included NO INSPECTABLED TEST SUB PROGAM. 
   *   - Because Gain might be switch to those unused testSubProgram.
   */
  public List<TestSubProgram> getAllTestSubProgramsIncludedNoInspectableTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);

    List<TestSubProgram> newTestSubProgram = new ArrayList<TestSubProgram>(_testSubPrograms);


    if (_notInspectableTestSubProgramFilters.size() > 0)
    {
      newTestSubProgram.addAll(_notInspectableTestSubProgramFilters);
    }

    return newTestSubProgram;
  }
  
  /**
   * @author Kee Chin Seong
   * - For those who want get subprogram including not inspected test program, they can use this !
   */
  public List<TestSubProgram> getTestSubProgramsIncludedNoInspectableTestSubPrograms(Board board)
  {
     Assert.expect(board != null);
     Assert.expect(board.getBoardSettings().isLongBoard() == false);
     
     List<TestSubProgram> testSubPrograms = new LinkedList<TestSubProgram>();
     
     for(TestSubProgram subProgram : getAllTestSubProgramsIncludedNoInspectableTestSubPrograms())
     {
         if (subProgram.getBoard().equals(board) && testSubPrograms.contains(subProgram) == false)
             testSubPrograms.add(subProgram);
     }
     
     return testSubPrograms;
  }


  /**
   * @author Wei Chin, Chong
   */
  public List<TestSubProgram> getBoardsFilteredTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);

    List<TestSubProgram> subPrograms = new LinkedList<TestSubProgram>();

    subPrograms.add(_testSubPrograms.get(0));

    if (_testSubPrograms.get(0).getBoard().isLongBoard())
    {
      subPrograms.add(_testSubPrograms.get(1));
    }

    return subPrograms;
  }

  /**
   * @author George A. David
   */
  public void addFilter(TestSubProgram subProgram)
  {
    Assert.expect(subProgram != null);
    _subProgramsToFilterOn.add(subProgram);
  }

  /**
   * This method collects subtypes to exclude from the test program, it is currently (as of 1.13) used
   * for subtypes for which the predictive slice height feature was turned on
   *
   * @author Laura Cormos
   */
  public void addExcludeFilter(Set<Subtype> subtypes)
  {
    Assert.expect(subtypes != null);
    _subtypesToExclude = subtypes;
  }

  /**
   * @author George A. David
   */
  public void clearTestSubProgramFilters()
  {
    _subProgramsToFilterOn.clear();
  }

  /**
   * @author George A. David
   */
  public TestSubProgram getTestSubProgram(PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(panelLocationInSystem != null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getPanelLocationInSystem().equals(panelLocationInSystem))
      {
        program = subProgram;
        break;
      }
    }

    Assert.expect(program != null);
    return program;
  }

  /**
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgram(Board board, PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(board != null);
    Assert.expect(panelLocationInSystem != null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getBoard().equals(board))
      {
        if (subProgram.getPanelLocationInSystem().equals(panelLocationInSystem))
        {
          program = subProgram;
          break;
        }
      }
    }

    Assert.expect(program != null);
    return program;
  }

  /**
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgramHasAlignmentRegion(Board board, 
                                                            PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(board != null);
    Assert.expect(panelLocationInSystem != null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getBoard().equals(board) && subProgram.isSubProgramPerformAlignment())
      {
        if (subProgram.getPanelLocationInSystem().equals(panelLocationInSystem))
        {
          program = subProgram;
          break;
        }
      }
    }

    Assert.expect(program != null);
    return program;
  }  
  
  /**
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(panelLocationInSystem != null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if ( subProgram.isSubProgramPerformAlignment() && 
           subProgram.getPanelLocationInSystem().equals(panelLocationInSystem))
      {
        program = subProgram;
        break;
      }
    }

    Assert.expect(program != null);
    return program;
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgramHasAlignmentRegion(MagnificationTypeEnum magnificationType)
  {
    Assert.expect(magnificationType != null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if ( subProgram.isSubProgramPerformAlignment() && 
           subProgram.getMagnificationType().equals(magnificationType))
      {
        program = subProgram;
        break;
      }
    }

    Assert.expect(program != null);
    return program;
  }

 /**
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgramHasAlignmentRegion(PanelLocationInSystemEnum panelLocationInSystem, 
                                                            MagnificationTypeEnum magnification)
  {
    Assert.expect(panelLocationInSystem != null);
    Assert.expect(magnification!= null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if ( subProgram.isSubProgramPerformAlignment() && 
           subProgram.getPanelLocationInSystem().equals(panelLocationInSystem) && 
           subProgram.getMagnificationType().equals(magnification))
      {
        program = subProgram;
        break;
      }
    }

    Assert.expect(program != null);
    return program;
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgramHasAlignmentRegion(Board board, 
                                                            PanelLocationInSystemEnum panelLocationInSystem,
                                                            MagnificationTypeEnum magnification)
  {
    Assert.expect(board != null);
    Assert.expect(panelLocationInSystem != null);
    Assert.expect(magnification!= null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getBoard().equals(board) && subProgram.isSubProgramPerformAlignment())
      {
        if (subProgram.getPanelLocationInSystem().equals(panelLocationInSystem) && 
            subProgram.getMagnificationType().equals(magnification))
        {
          program = subProgram;
          break;
        }
      }
    }

    Assert.expect(program != null);
    return program;
  }
  
  /**
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgramHasAlignmentRegion(Board board, 
                                                            MagnificationTypeEnum magnification)
  {
    Assert.expect(board != null);
    Assert.expect(magnification!= null);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getBoard().equals(board) && subProgram.isSubProgramPerformAlignment())
      {
        if (subProgram.getMagnificationType().equals(magnification))
        {
          program = subProgram;
          break;
        }
      }
    }

    Assert.expect(program != null);
    return program;
  }
  
  /**
   * This function is only for board-based test program - Siew Yeng
   * @author Wei Chin, Chong
   */
  public TestSubProgram getTestSubProgram(Board board)
  {
    Assert.expect(board != null);
    //Siew Yeng
     Assert.expect(getProject().getPanel().getPanelSettings().isPanelBasedAlignment() == false);

    TestSubProgram program = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.isSubProgramPerformAlignment() && subProgram.getBoard().equals(board))
      {
        program = subProgram;
        break;
      }
    }

    Assert.expect(program != null);
    return program;
  }
  
  /**
   * This function is only for board-based test program - Siew Yeng
   * @author Cheah Lee Herng
   */
  public List<TestSubProgram> getTestSubPrograms(Board board)
  {
     Assert.expect(board != null);
     //Siew Yeng
     Assert.expect(getProject().getPanel().getPanelSettings().isPanelBasedAlignment() == false);
     
     List<TestSubProgram> testSubPrograms = new LinkedList<TestSubProgram>();
     for(TestSubProgram subProgram : getFilteredTestSubPrograms())
     {
         if (subProgram.getBoard().equals(board) && testSubPrograms.contains(subProgram) == false)
             testSubPrograms.add(subProgram);
     }
     return testSubPrograms;
  }

  /**
   * @author Matt Wharton
   */
  public boolean hasTestSubProgram(PanelLocationInSystemEnum panelLocationInSystem)
  {
    Assert.expect(panelLocationInSystem != null);
    Assert.expect(_testSubPrograms != null);

    boolean hasTestSubProgram = false;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getPanelLocationInSystem().equals(panelLocationInSystem))
      {
        hasTestSubProgram = true;
        break;
      }
    }

    return hasTestSubProgram;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public boolean hasTestSubProgram(PanelLocationInSystemEnum panelLocationInSystem, MagnificationTypeEnum magnification)
  {
    Assert.expect(panelLocationInSystem != null);
    Assert.expect(magnification != null);
    Assert.expect(_testSubPrograms != null);

    boolean hasTestSubProgram = false;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getPanelLocationInSystem().equals(panelLocationInSystem) && 
          subProgram.getMagnificationType().equals(magnification))
      {
        hasTestSubProgram = true;
        break;
      }
    }
    return hasTestSubProgram;
  }
  
  /**
   * @author Phang Siew Yeng
   */
  public boolean hasTestSubProgram(Board board, PanelLocationInSystemEnum panelLocationInSystem, MagnificationTypeEnum magnification)
  {
    Assert.expect(board != null);
    Assert.expect(panelLocationInSystem != null);
    Assert.expect(magnification != null);

    boolean hasTestSubProgram = false;

    for (TestSubProgram subProgram : getTestSubPrograms(board))
    {
      if (subProgram.getPanelLocationInSystem().equals(panelLocationInSystem) && 
          subProgram.getMagnificationType().equals(magnification))
      {
        hasTestSubProgram = true;
        break;
      }
    }
    return hasTestSubProgram;
  }

  /**
   * @author George A. David
   */
  public void clearFilters()
  {
    _boardsToFilterOn.clear();
    _componentsToFilterOn.clear();
    _padsToFilterOn.clear();
    _jointTypesToFilterOn.clear();
    _subtypesToFilterOn.clear();
    _subtypesToExclude.clear();
    _regionsToFilterOn.clear();
    _useOnlyInspectedInspectionRegions = false;
    _createOnlyInspectedSlices = false;
    updateSlicesToBeCreated();
  }

  /**
   * @author George A. David
   */
  public void addFilter(Board board)
  {
    Assert.expect(board != null);

    _boardsToFilterOn.add(board);
  }

  /**
   * @author George A. David
   */
  public void addFilter(BoardType boardType)
  {
    Assert.expect(boardType != null);

    for (Board board : boardType.getBoards())
    {
      _boardsToFilterOn.add(board);
    }
  }

  /**
   * @author George A. David
   */
  public void addFilter(Component component)
  {
    Assert.expect(component != null);

    _componentsToFilterOn.add(component);
  }

  /**
   * @author George A. David
   */
  public void addFilter(ComponentType componentType)
  {
    Assert.expect(componentType != null);

    for (Component component : componentType.getComponents())
    {
      _componentsToFilterOn.add(component);
    }
  }

  /**
   * @author George A. David
   */
  public void addFilter(Pad pad)
  {
    Assert.expect(pad != null);

    _padsToFilterOn.add(pad);
  }

  /**
   * @author George A. David
   */
  public void addFilter(PadType padType)
  {
    Assert.expect(padType != null);

    for (Pad pad : padType.getPads())
    {
      _padsToFilterOn.add(pad);
    }
  }

  /**
   * @author George A. David
   */
  public void addFilter(Subtype subtype)
  {
    Assert.expect(subtype != null);

    _subtypesToFilterOn.add(subtype);
  }

  /**
   * @author George A. David
   */
  public void addFilter(JointTypeEnum jointType)
  {
    Assert.expect(jointType != null);

    _jointTypesToFilterOn.add(jointType);
  }

  /**
   * @author George A. David
   */
  public void addFilter(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    _regionsToFilterOn.add(reconstructionRegion);
  }

  /**
   * @author George A. David
   */
  public void addFilters(List<ReconstructionRegion> reconstructionRegions)
  {
    Assert.expect(reconstructionRegions != null);

    _regionsToFilterOn.addAll(reconstructionRegions);
  }

  /**
   * @author George A. David
   */
  public void clearReconstructionRegionFilters()
  {
    _regionsToFilterOn.clear();
  }

  /**
   * @author George A. David
   */
  public void addFilter(boolean useOnlyInspectedInspectionRegions)
  {
    _useOnlyInspectedInspectionRegions = useOnlyInspectedInspectionRegions;
  }

  /**
   * @author George A. David
   */
  public void setCreateOnlyInspectedSlicesFilter(boolean createOnlyInspectedSlices)
  {
    _createOnlyInspectedSlices = createOnlyInspectedSlices;
    updateSlicesToBeCreated();
  }

  /**
   * @author George A. David
   */
  public boolean isCreateOnlyInspectedSlicesEnabled()
  {
    return _createOnlyInspectedSlices;
  }

  /**
   * @author George A. David
   */
  private void updateSlicesToBeCreated()
  {
    if (_createOnlyInspectedSlices)
    {
      for (ReconstructionRegion inspectionRegion : getAllInspectionRegions())
      {
        Collection<Slice> inspectedSlices = inspectionRegion.getInspectedAndMultiAngleSlices();//getInspectedSlices(); // Chnee Khang Wah, 2012-03-06, 2.5D
        for (FocusGroup focusGroup : inspectionRegion.getFocusGroups())
        {
          for (Slice slice : focusGroup.getSlices())
          {
            if (inspectedSlices.contains(slice)
                    || slice.getSliceType().equals(SliceTypeEnum.SURFACE))
            {
              slice.setCreateSlice(true);
            }
            else
            {
              slice.setCreateSlice(false);
            }
          }
        }
      }
    }
    else
    {
      for (ReconstructionRegion inspectionRegion : getAllInspectionRegions())
      {
        for (FocusGroup focusGroup : inspectionRegion.getFocusGroups())
        {
          for (Slice slice : focusGroup.getSlices())
          {
            slice.setCreateSlice(true);
          }
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public boolean useOnlyInspectedRegions()
  {
    return _useOnlyInspectedInspectionRegions;
  }

  /**
   * Get the JointInspectionData object associated with a certain Pad object.
   * @author Peter Esbensen
   * Edited by Kee Chin Seong - jointInspection issue
   */
  public synchronized JointInspectionData getJointInspectionData(Pad pad)
  {
    JointInspectionData inspectionRegionPad = null;
    //XCR2834, edit by shrng chuan to only used those inspectable program because it will slow down the inspection process
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.hasInspectionPad(pad))
      {
        inspectionRegionPad = subProgram.getJointInspectionData(pad);
        break;
      }
    }

    Assert.expect(inspectionRegionPad != null);
    return inspectionRegionPad;
  }

  /**
   * @author George A. David
   */
  public Collection<ReconstructionRegion> getBoundingVerificationRegions(Component component)
  {
    Assert.expect(component != null);

    PanelRectangle region = new PanelRectangle(component.getShapeRelativeToPanelInNanoMeters());

//    double sizeInNanoMeters = MathUtil.convertMilsToNanoMeters(500);
    double sizeInNanoMeters = MathUtil.convertMilsToNanoMeters(1000);
    region.setRect(region.getCenterX() - sizeInNanoMeters / 2.0,
            region.getCenterY() - sizeInNanoMeters / 2.0,
            sizeInNanoMeters,
            sizeInNanoMeters);

    return getVerificationRegions(region, component.isTopSide());
  }

  /**
   * @author George A. David
   */
  public Collection<ReconstructionRegion> getBoundingVerificationRegionsForAlignment(Collection<Pad> pads,
          Collection<Fiducial> fiducials)
  {
    Assert.expect(pads != null);
    Assert.expect(fiducials != null);

    // get the combined rectangular region covered by all the pads.
    PanelRectangle combinedRect = null;
    BooleanRef isTopSide = null;
    for (Pad pad : pads)
    {
      if (combinedRect == null)
      {
        combinedRect = new PanelRectangle(pad.getShapeRelativeToPanelInNanoMeters());
      }
      else
      {
        combinedRect.addShape(pad.getShapeRelativeToPanelInNanoMeters());
      }

      if (isTopSide == null)
      {
        isTopSide = new BooleanRef(pad.isTopSide());
      }
      else
      {
        Assert.expect(pad.isTopSide() == isTopSide.getValue());
      }
    }

    for (Fiducial fiducial : fiducials)
    {
      if (combinedRect == null)
      {
        combinedRect = new PanelRectangle(fiducial.getShapeRelativeToPanelInNanoMeters());
      }
      else
      {
        combinedRect.addShape(fiducial.getShapeRelativeToPanelInNanoMeters());
      }

      if (isTopSide == null)
      {
        isTopSide = new BooleanRef(fiducial.isTopSide());
      }
      else
      {
        Assert.expect(fiducial.isTopSide() == isTopSide.getValue());
      }
    }

    // ok, new we want the bounding region to be 1000 mils x 1000 mils
    double sizeInNanoMeters = MathUtil.convertMilsToNanoMeters(1000);
    combinedRect.setRect(combinedRect.getCenterX() - sizeInNanoMeters / 2.0,
            combinedRect.getCenterY() - sizeInNanoMeters / 2.0,
            sizeInNanoMeters,
            sizeInNanoMeters);

    return getVerificationRegions(combinedRect, isTopSide.getValue());
  }

  /**
   * @author George A. David
   */
  private Collection<ReconstructionRegion> getVerificationRegions(PanelRectangle region, boolean isTopSide)
  {
    Collection<ReconstructionRegion> regions = new LinkedList<ReconstructionRegion>();

    for (ReconstructionRegion reconstructionRegion : getVerificationRegions())
    {
      if (reconstructionRegion.isTopSide() != isTopSide)
      {
        continue;
      }

      PanelRectangle reconstructionRect = reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
      if (region.intersects(reconstructionRect)
              || region.contains(reconstructionRect))
      {
        regions.add(reconstructionRegion);
      }
    }

    return regions;
  }

  /**
   * @author Peter Esbensen
   */
  public int getInspectionRunId()
  {
    return _inspectionRunId;
  }

  /**
   * @author Peter Esbensen
   */
  public void startNewInspectionRun()
  {
    ++_inspectionRunId;
    JointMeasurement.resetIdCounter();
    ComponentMeasurement.resetIdCounter();
  }

  /**
   * @author George A. David
   */
  public boolean hasSubtype(Subtype subtype)
  {
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.hasSubtype(subtype))
      {
        return true;
      }
    }
    return false;
  }

  /**
   * @author George A. David
   */
  public Collection<SliceNameEnum> getSliceNames(Subtype subtype)
  {
    Collection<SliceNameEnum> sliceNames = null;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.hasSubtype(subtype))
      {
        sliceNames = subProgram.getSliceNames(subtype);
        break;
      }
    }

    Assert.expect(sliceNames != null);
    return sliceNames;
  }

  /**
   * @author George A. David
   * @author Wei Chin, Chong
   */
  public boolean isLongProgram()
  {
    boolean isLong = false;

    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.hasBoard()
              && subProgram.getBoard().isLongBoard()
              && subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
      {
        isLong = subProgram.hasInspectionRegions();
        break;
      }
      else if (subProgram.hasBoard() == false
              && subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
      {
        isLong = subProgram.hasInspectionRegions();
        break;
      }
    }
    return isLong;
//    return _testSubPrograms.size() > 1;
  }

  /**
   * @author George A. David
   * @author Wei Chin, Chong
   */
  public boolean hasLeftProgram()
  {
    boolean isLong = false;
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
      {
        isLong = subProgram.hasInspectionRegions();
        break;
      }
    }
    return isLong;
  }

  /**
   *  @author Roy Williams
   */
  public boolean isLastTestSubProgram(TestSubProgram testSubProgram)
  {
    Assert.expect(testSubProgram != null);
    int size = _testSubPrograms.size();
    Assert.expect(size > 0);

    TestSubProgram lastTestSubProgram = _testSubPrograms.get(size - 1);
    if (testSubProgram.equals(lastTestSubProgram))
    {
      return true;
    }
    return false;
  }
  
  /*
   * Kee Chin Seong
   */
  public boolean hasAlignmentRegionInInspectableTestSubProgram()
  {
    for(TestSubProgram testSubProgram : _testSubPrograms)
    {
      if(testSubProgram.getAlignmentRegions().isEmpty() == false)
      {
        return true;
      }
    }
    
    return false;
  }
  
  /*
   * @author Kee Chin Seong
   */
  public boolean hasAlignmentRegionInNonInspectableTestSubProgram()
  {
     for(TestSubProgram testSubProgram : _notInspectableTestSubProgramFilters)
    {
      if(testSubProgram.getAlignmentRegions().isEmpty() == false)
      {
        return true;
      }
    }  
    return false;
  }
  
  /*
   * Kee Chin Seong
   */
  public void getAlignmentRegionToNewTestProgram()
  {
    Collection<ReconstructionRegion> alignmentRegion = Collections.emptyList();
    
    for(TestSubProgram subProgram : getAllTestSubProgramsIncludedNoInspectableTestSubPrograms())
    {
      if(subProgram.getAlignmentRegions().isEmpty() == false)
      {
        alignmentRegion = subProgram.getAlignmentRegions();
      }
    }
    
    for(TestSubProgram subProgram : _testSubPrograms)
    {
      if(subProgram.isSubProgramPerformAlignment() == true)
      {
        subProgram.set3DAlignmentRegions(alignmentRegion);
        //Khaw Chek Hau - XCR2285: 2D Alignment on v810
        subProgram.set2DAlignmentRegions(alignmentRegion);
      }
    }
  }

  /**
   * @author George A. David
   * @author Kee Chin Seong - XXL Speed improvement
   *                        - added addNotInspectableTestSubProgramFilters() so that 
   *                          the filter is still remain for those not inspectabled test
   *                          Sub program.
   */
  public void removeUnusedTestSubPrograms()
  {
    List<TestSubProgram> subPrograms = new ArrayList<TestSubProgram>(_testSubPrograms);
    
    for (TestSubProgram subProgram : subPrograms)
    {
      if (subProgram.hasInspectionRegions() == false || subProgram.isAllRegionNotIspectable())
      {
        addNotInspectableTestSubProgramFilters(subProgram);
        _testSubPrograms.remove(subProgram);
      }
    }
    
    if(_notInspectableTestSubProgramFilters.size() > 0)
    {
      Set<TestSubProgram> notInspectableTestSubProgram = new HashSet<TestSubProgram>(_notInspectableTestSubProgramFilters); 
      for (TestSubProgram subProgram : notInspectableTestSubProgram)
      {
         if (subProgram.hasInspectionRegions() && subProgram.isAllRegionNotIspectable() == false)
         {
             // Make sure we have sync the regions
             subProgram.syncInspectionRegions();
             
             _testSubPrograms.add(subProgram);
             _notInspectableTestSubProgramFilters.remove(subProgram);
         }
      }
    }
  }

  /**
   * @author George A. David
   */
  public void addInvalidatedAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(_invalidatedAlignmentGroups != null);

    _invalidatedAlignmentGroups.add(alignmentGroup);
  }

  /**
   * @author George A. David
   */
  public boolean hasInvalidatedAlignmentGroups()
  {
    Assert.expect(_invalidatedAlignmentGroups != null);

    return _invalidatedAlignmentGroups.isEmpty() == false;
  }

  /**
   * @author George A. David
   */
  public List<AlignmentGroup> getInvalidatedAlignmentGroups()
  {
    Assert.expect(_invalidatedAlignmentGroups != null);

    return _invalidatedAlignmentGroups;
  }

  /**
   * @author George A. David
   */
  public boolean isAlignmentGroupInvalidated(AlignmentGroup alignmentGroup)
  {
    Assert.expect(_invalidatedAlignmentGroups != null);

    return _invalidatedAlignmentGroups.contains(alignmentGroup);
  }

  /**
   * @author George A. David
   */
  public void removeInvalidatedAlignmentGroup(AlignmentGroup alignmentGroup)
  {
    Assert.expect(alignmentGroup != null);
    Assert.expect(_invalidatedAlignmentGroups != null);
    Assert.expect(_invalidatedAlignmentGroups.contains(alignmentGroup));

    _invalidatedAlignmentGroups.remove(alignmentGroup);
  }

  /**
   * @author George A. David
   */
  public void clearInvalidatedAlignmentGroups()
  {
    Assert.expect(_invalidatedAlignmentGroups != null);

    _invalidatedAlignmentGroups.clear();
  }

  /**
   * @author Roy Williams
   */
  public List<ScanPass> getScanPasses()
  {
    List<ScanPass> scanPasses = new ArrayList<ScanPass>();

    for (TestSubProgram testSubProgram : getFilteredScanPathTestSubPrograms())
    {
      scanPasses.addAll(testSubProgram.getScanPasses());
    }
    return scanPasses;
  }

  /**
   * @author Dave Ferguson
   */
  public void setCameraAcquisitionParameters(List<XrayCameraAcquisitionParameters> xRayCameraAcquisitionParametersList)
  {
    Assert.expect(xRayCameraAcquisitionParametersList != null);

    // Added by LeeHerng - Make sure we clear the previous list if it had already exists
    if (_xrayCameraAcquisitionParameters != null)
    {
      for(XrayCameraAcquisitionParameters xrayCameraAcquisitionParameters : _xrayCameraAcquisitionParameters)
        xrayCameraAcquisitionParameters.clear();
      
        _xrayCameraAcquisitionParameters.clear();
        _xrayCameraAcquisitionParameters = null;
    }
    
    _xrayCameraAcquisitionParameters = new ArrayList<XrayCameraAcquisitionParameters>();

    for (XrayCameraAcquisitionParameters xRayCameraAcquisitionParameters : xRayCameraAcquisitionParametersList)
    {
      _xrayCameraAcquisitionParameters.add(xRayCameraAcquisitionParameters);
    }
  }

  /**
   * @author Dave Ferguson
   */
  public List<XrayCameraAcquisitionParameters> getCameraAcquisitionParameters()
  {
    Assert.expect(_xrayCameraAcquisitionParameters != null);
    return _xrayCameraAcquisitionParameters;
  }

  /**
   * @author Peter Esbensen
   */
  void clearComponentMap()
  {
    Assert.expect(_componentToComponentInspectionDataMap != null);
    _componentToComponentInspectionDataMap.clear();
  }

  /**
   * @author Peter Esbensen
   */
  boolean componentInspectionDataExists(Component component)
  {
    Assert.expect(component != null);
    return (_componentToComponentInspectionDataMap.containsKey(component));
  }

  /**
   * @author Peter Esbensen
   */
  void addComponentToInspectionMap(Component component, ComponentInspectionData componentInspectionData)
  {
    Assert.expect(component != null);
    Assert.expect(componentInspectionData != null);
    _componentToComponentInspectionDataMap.put(component, componentInspectionData);
  }

  /**
   * @author Peter Esbensen
   */
  ComponentInspectionData getComponentInspectionData(Component component)
  {
    Assert.expect(component != null);
    ComponentInspectionData componentInspectionData = _componentToComponentInspectionDataMap.get(component);
    Assert.expect(componentInspectionData != null);
    return componentInspectionData;
  }

  /**
   * @author George A. David
   */
  public Set<String> getInspectionRegionNames()
  {
    Set<String> regionNames = new HashSet<String>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      regionNames.addAll(subProgram.getInspectionRegionNames());
    }

    return regionNames;
  }

  /**
   * These are unfocused images where the user has decided allow
   * it to be retested in case the image has a failing joint in it.
   * @author George A. David
   */
  public List<ReconstructionRegion> getUnfocusedReconstructionRegionsNeedingRetest()
  {
    Assert.expect(_project != null);

    Set<String> regionNames = _project.getPanel().getPanelSettings().getUnfocusedRegionsNamesToRetest();

    return getReconstructionRegions(regionNames);
  }

  /**
   * @author George A. David
   */
  public void addUnfocusedReconstructionRegionNeedingRetest(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().addUnfocusedRegionNameToRetest(reconstructionRegion.getName());
  }

  /**
   * @author George A. David
   */
  public void removeUnfocusedReconstructionRegionNeedingRetest(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().removeUnfocusedRegionNameToRetest(reconstructionRegion.getName());
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedRegionNeedingRetest(ReconstructionRegion region)
  {
    Assert.expect(region != null);
    Assert.expect(_project != null);

    return _project.getPanel().getPanelSettings().hasUnfocusedRegionNameToRetest(region.getName());
  }

  /**
   * @author George A. David
   */
  public void setSlicesForRetestingUnfocusedRegionsEnabled(boolean enabled)
  {
    for (ReconstructionRegion inspectionRegion : getUnfocusedReconstructionRegionsNeedingRetest())
    {
      for (FocusGroup focusGroup : inspectionRegion.getFocusGroups())
      {
        for (Slice slice : focusGroup.getSlices())
        {
          if (slice.isUnfocusedRegionSliceForRetest())
          {
            slice.setCreateSlice(enabled);
          }
        }
      }
    }
  }

  /**
   * @author George A. David
   */
  public void addUnfocusedSliceToBeFixed(ReconstructionRegion reconstructionRegion, Slice slice)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slice != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().addUnfocusedRegionSliceToBeFixed(reconstructionRegion.getName(), slice.getSliceName());
  }

  /**
   * @author George A. David
   */
  public void removeUnfocusedSliceToBeFixed(ReconstructionRegion reconstructionRegion, Slice slice)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slice != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().removeUnfocusedRegionSliceToBeFixed(reconstructionRegion.getName(), slice.getSliceName());
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedSliceMarkedToBeFixed(ReconstructionRegion reconstructionRegion, Slice slice)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slice != null);
    Assert.expect(_project != null);

    return _project.getPanel().getPanelSettings().hasUnfocusedRegionSlicesToBeFixed(reconstructionRegion.getName(), slice.getSliceName());
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedSliceMarkedToBeFixed(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_project != null);

    return _project.getPanel().getPanelSettings().hasUnfocusedRegionSlicesToBeFixed(reconstructionRegion.getName());
  }

  /**
   * @author George A. David
   */
  public void removeUnfocusedSliceToBeFixed(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().removeUnfocusedRegionSliceToBeFixed(reconstructionRegion.getName());
  }

  /**
   * @author George A. David
   */
  private List<ReconstructionRegion> getReconstructionRegions(Set<String> regionNames)
  {
    List<ReconstructionRegion> regions = new ArrayList<ReconstructionRegion>(regionNames.size());
    for (String regionName : regionNames)
    {
      ReconstructionRegion region = null;
      for (TestSubProgram subProgram : _testSubPrograms)
      {
        if (subProgram.hasInspectionRegion(regionName))
        {
          region = subProgram.getInspectionRegion(regionName);
          break;
        }
      }
      Assert.expect(region != null);
      regions.add(region);
    }

    return regions;
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getUnfocusedReconstructionRegionsToBeFixed()
  {
    Assert.expect(_project != null);

    PanelSettings panelSettings = _project.getPanel().getPanelSettings();

    Set<String> regionNames = new HashSet<String>(panelSettings.getUnfocusedRegionSlicesToBeFixed().keySet());

    return getReconstructionRegions(regionNames);
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getUnfocusedReconstructionRegionsWithCustomFocus()
  {
    Assert.expect(_project != null);

    Set<String> regionNames = _project.getPanel().getPanelSettings().getUnfocusedRegionSlicesToZoffsetInNanos().keySet();

    return getReconstructionRegions(regionNames);
  }

  /**
   * @author George A. David
   */
  public List<ReconstructionRegion> getUntestableReconstructionRegionsBasedOnFocus()
  {
    Assert.expect(_project != null);

    Set<String> regionNames = _project.getPanel().getPanelSettings().getNoTestRegionsNames();

    return getReconstructionRegions(regionNames);
  }

  /**
   * @author George A. David
   */
  public void setUnfocusedSliceZheightOffsetInNanoMeters(ReconstructionRegion reconstructionRegion, Slice slice, int zHeightOffsetInNanoMeters)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slice != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().addUnfocusedRegionSlicesToZoffsetInNanos(reconstructionRegion.getName(),
            slice.getSliceName(),
            zHeightOffsetInNanoMeters);
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedSliceUsingCustomFocus(ReconstructionRegion reconstructionRegion, Slice slice)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slice != null);
    Assert.expect(_project != null);

    return _project.getPanel().getPanelSettings().hasUnfocusedRegionSlicesToZoffsetInNanos(reconstructionRegion.getName(),
            slice.getSliceName());
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedSliceUsingCustomFocus(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_project != null);

    return _project.getPanel().getPanelSettings().hasUnfocusedRegionSlicesToZoffsetInNanos(reconstructionRegion.getName());
  }

  /**
   * @author George A. David
   */
  public int getUnfocusedSliceZheightOffsetInNanoMeters(ReconstructionRegion reconstructionRegion, Slice slice)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slice != null);
    Assert.expect(_project != null);

    Set<Pair<SliceNameEnum, Integer>> pairs = _project.getPanel().getPanelSettings().getUnfocusedRegionSlicesToZoffsetInNanos().get(reconstructionRegion.getName());
    Assert.expect(pairs != null);
    for (Pair<SliceNameEnum, Integer> pair : pairs)
    {
      if (slice.getSliceName().equals(pair.getFirst()))
      {
        return pair.getSecond();
      }
    }

    Assert.expect(false);
    return 0;
  }

  /**
   * @author George A. David
   */
  public void removeUnfocusedSliceUsingCustomFocus(ReconstructionRegion reconstructionRegion, Slice slice)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(slice != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().removeUnfocusedRegionSlicesToZoffsetInNanos(reconstructionRegion.getName(), slice.getSliceName());
  }

  /**
   * @author George A. David
   */
  public void removeUnfocusedSliceUsingCustomFocus(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_project != null);

    _project.getPanel().getPanelSettings().removeUnfocusedRegionSlicesToZoffsetInNanos(reconstructionRegion.getName());
  }

  /**
   * @author George A. David
   */
  public void setUnfocusedReconstructionRegionTestable(ReconstructionRegion region, boolean testable)
  {
    Assert.expect(region != null);
    Assert.expect(_project != null);

    if (testable)
    {
      _project.getPanel().getPanelSettings().removeNoTestRegionName(region.getName());
    }
    else
    {
      List<PadType> padTypes = new LinkedList<PadType>();
      for (Pad pad : region.getPads())
      {
        padTypes.add(pad.getPadType());
      }
      _project.getPanel().getPanelSettings().addNoTestRegionName(region.getName(), padTypes);
    }
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedReconstructionRegionTestable(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
    Assert.expect(_project != null);

    return _project.getPanel().getPanelSettings().hasNoTestRegionName(reconstructionRegion.getName()) == false;
  }

  /**
   * @author George A. David
   */
  public void updateWithAdjustFocusSlices(boolean useAdjustFocusSlices)
  {
    clearFilters();
    addFilter(true);
    setCreateOnlyInspectedSlicesFilter(true);
    if (_programGeneration == null)
    {
      _programGeneration = ProgramGeneration.getInstance();
    }

    _programGeneration.updateAdjustFocusSlices(this, useAdjustFocusSlices);
  }

  /**
   * @author George A. David
   */
  public void clearAdjustFocusInspectionRegions()
  {
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      subProgram.clearAdjustFocusInspectionRegions();
    }
  }

  /**
   * @author Matt Wharton
   */
  public SurroundingPadUtil getSurroundingPadUtil()
  {
    Assert.expect(_surroundingPadUtil != null);

    return _surroundingPadUtil;
  }

  /**
   * @author Matt Wharton
   */
  public void setSurroundingPadUtil(SurroundingPadUtil surroundingPadUtil)
  {
    Assert.expect(surroundingPadUtil != null);

    _surroundingPadUtil = surroundingPadUtil;
  }

  /**
   * This would ignored No Test and No Load Reconstructed Region and the _useOnlyInspectedRegionInScanPath is true
   * but for Virtual Live must be return TRUE
   * 
   * @author Poh Kheng
   * @author Chin Seong, Kee
   */
  boolean filterScanPathReconstructedRegion(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);
   
    if(VirtualLiveManager.getInstance().isVirtualLiveMode())
      return true;
    
    if (_useOnlyInspectedRegionInScanPath && reconstructionRegion.isInspected() == false)
    {
      return false;
    }
    return true;
  }

  /**
   * @author Poh Kheng
   */
  public void setUseOnlyInspectedRegionsInScanPath(boolean useOnlyInspectedRegionInScanPath)
  {
    _useOnlyInspectedRegionInScanPath = useOnlyInspectedRegionInScanPath;
  }

  /**
   * @author Poh Kheng
   */
  public List<ReconstructionRegion> getAllScanPathReconstructionRegions()
  {
    List<ReconstructionRegion> inspectionReconstructionRegions = new LinkedList<ReconstructionRegion>();
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      inspectionReconstructionRegions.addAll(subProgram.getAllScanPathReconstructionRegions());
    }

    return inspectionReconstructionRegions;
  }

  /**
   * @author Poh Kheng
   */
  public List<TestSubProgram> getFilteredScanPathTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);
    Assert.expect(_imageAcquisitionModeInScanPath != null);

    List<TestSubProgram> subPrograms = new LinkedList<TestSubProgram>(_testSubPrograms);
    if (_subProgramsToFilterOn.isEmpty() == false)
    {
      subPrograms.retainAll(_subProgramsToFilterOn);
    }
    
    subPrograms.removeAll(getTestSubProgramWithNoRegion());
    
    // Handle board-based alignment
    List<TestSubProgram> filteredBoardTestSubPrograms = getFilteredTestSubPrograms();
    if (filteredBoardTestSubPrograms.isEmpty() == false)
      subPrograms.retainAll(filteredBoardTestSubPrograms);

    return subPrograms;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public List<TestSubProgram> getFilteredBoardsScanPathTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);
    Assert.expect(_imageAcquisitionModeInScanPath != null);

    List<TestSubProgram> subPrograms = new LinkedList<TestSubProgram>(_testSubPrograms);
    
    // Handling of board-based alignment filtering
    List<TestSubProgram> boardTestSubPrograms = new LinkedList<TestSubProgram>();
    if (getProject().getPanel().getPanelSettings().isPanelBasedAlignment() == false)
    {
      if (_boardsToFilterOn.isEmpty() == false)
      {
        for(TestSubProgram filteredTestSubProgram : subPrograms)
        {
          if (_boardsToFilterOn.contains(filteredTestSubProgram.getBoard()))
            boardTestSubPrograms.add(filteredTestSubProgram);
        }
      }
    }
    
    if (boardTestSubPrograms.isEmpty() == false)
      subPrograms.retainAll(boardTestSubPrograms);
    
    subPrograms.removeAll(getTestSubProgramWithNoRegion());

    return subPrograms;
  }
  
  /**
   * @author Kok Chun, Tan
   * @return the test sub program with no reconstruction region, verification region or alignment region
   */
  private List<TestSubProgram> getTestSubProgramWithNoRegion()
  {
    List<TestSubProgram> subPrograms = new LinkedList<TestSubProgram> ();
    
    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if (_imageAcquisitionModeInScanPath == ImageAcquisitionModeEnum.IMAGE_SET
              || _imageAcquisitionModeInScanPath == ImageAcquisitionModeEnum.PRODUCTION)
      {
        if (subProgram.isSubProgramPerformAlignment())
        {
          if (subProgram.getAlignmentRegions().isEmpty())
          {            
            subPrograms.add(subProgram);
          }
        }
        else if (subProgram.getAllScanPathReconstructionRegions().size() == 0)
        {
          subPrograms.add(subProgram);
        }
        
        //Khaw Chek Hau - XCR2181: Able to run alignment starting from failed board
        //Remove production done subprogram if manual realignment performed
        if (_isRerunningProduction && subProgram.isInspectionDone())
        {
//          System.out.println("getFilteredScanPathTestSubPrograms() _isRerunningProduction && subProgram.isProductionLearned() is entered");
          subPrograms.add(subProgram);
        }
        
        // XCR1768 - Siew Yeng - fix fine tuning hang
        if(_imageAcquisitionModeInScanPath == ImageAcquisitionModeEnum.IMAGE_SET)
        {
          if(subProgram.getFilteredInspectionRegions().isEmpty() && subProgram.isSubProgramPerformAlignment() == false)
          {
            subPrograms.add(subProgram);
          }
        }
      }
      else if (_imageAcquisitionModeInScanPath == ImageAcquisitionModeEnum.VERIFICATION
              || _imageAcquisitionModeInScanPath == ImageAcquisitionModeEnum.COUPON)
      {
        if (subProgram.getVerificationRegions().size() == 0)
        {
          subPrograms.add(subProgram);
        }
      }
      else if (_imageAcquisitionModeInScanPath == ImageAcquisitionModeEnum.ALIGNMENT)
      {
        if (subProgram.getAlignmentRegions().size() == 0)
        {
          subPrograms.add(subProgram);
        }
      }
    }
    
    return subPrograms;
  }

  /**
   * @author Poh Kheng
   */
  public void setFilteredTestSubProgramInScanPath(ImageAcquisitionModeEnum filteredRegionInScanPath)
  {
    _imageAcquisitionModeInScanPath = filteredRegionInScanPath;
  }

  /**
   * @author Poh Kheng
   */
  public boolean isUseOnlyInspectedRegionsInScanPath()
  {
    return _useOnlyInspectedRegionInScanPath;
  }

  /**
   * @author Wei Chin, Chong
   */
  private long generateCheckSumValue()
  {
    TimerUtil testProgramCheckSumTiming = new TimerUtil();
    if (_debugCheckSumTiming)
    {
      testProgramCheckSumTiming.start();
      System.out.println("ProjectName = " + _project.getName() + "\n testProgramCheckSumTiming.start()");
    }

    CRC32 checksumUtil = new CRC32();
    checksumUtil.reset();

    StringBuilder info = new StringBuilder();

    info.append(_project.getName());
    info.append( " " + getFilteredTestSubPrograms().size());
    info.append(" " + _createOnlyInspectedSlices);
//    info.append( " " + getVerificationRegions().size());
    info.append( " " + getAllInspectionRegions().size());
    info.append( " " + getAlignmentRegions().size());

    for (TestSubProgram subProgram : getFilteredTestSubPrograms())
    {
      info.append( " " + subProgram.getOpticalRegions().size());
      //Set<ReconstructionRegion> verificationRegions = subProgram.getSortedVerificationRegions();
      Set<ReconstructionRegion> inspectionRegions = subProgram.getSortedInspectionRegions();
      Set<ReconstructionRegion> alignmentRegions = subProgram.getSortedAlignmentRegions();

      //generateReconstructionRegionDetails(verificationRegions,info);
      generateReconstructionRegionDetails(inspectionRegions, info);
      generateReconstructionRegionDetails(alignmentRegions,info);
    }
    if (_debugCheckSumTiming)
    {
      testProgramCheckSumTiming.stop();

      System.out.println("Getting info ,  ElapsedTimeInMillis = " + testProgramCheckSumTiming.getElapsedTimeInMillis());
      testProgramCheckSumTiming.reset();
      testProgramCheckSumTiming.start();
    }
    checksumUtil.update(info.toString().getBytes());

    if (_debugCheckSumTiming)
    {
      testProgramCheckSumTiming.stop();
      System.out.println("Convert info to check Sum ,  ElapsedTimeInMillis = " + testProgramCheckSumTiming.getElapsedTimeInMillis());
      System.out.println("testProgramCheckSumTiming.end()");
    }
    return checksumUtil.getValue();
  }

  /**
   * @author Wei Chin, Chong
   */
  private void generateReconstructionRegionDetails(Set<ReconstructionRegion> reconstructionRegions, StringBuilder info)
  {
    Assert.expect(reconstructionRegions != null);

    info.append(" " + reconstructionRegions.size());
    for (ReconstructionRegion region : reconstructionRegions)
    {
      PanelRectangle rect = region.getRegionRectangleRelativeToPanelInNanoMeters();

      info.append(" " + region.getName());
      info.append(" " + region.getRegionId());
      info.append(" " + rect.getMinX());
      info.append(" " + rect.getMinY());
      info.append(" " + rect.getWidth());
      info.append(" " + rect.getHeight());
      info.append(" " + region.isTopSide());
      info.append(" " + region.getReconstructionEngineId().toString());
      info.append(" " + region.requiresShadingCompensation());
      
      if (region.isAlignmentRegion())
      {
        if (region.hasJoints())
        {
          List<Pad> pads = new LinkedList<Pad>(region.getPads());
          Collections.sort(pads, new PadComparator());
          Set<String> compNames = new TreeSet<String>();

          info.append(" " + pads.size());
          for (Pad pad : pads)
          {
            compNames.add(pad.getComponent().getReferenceDesignator());
            info.append(" " +  pad.getName());
          }

          info.append(" " + compNames.size());
          for (String compName : compNames)
          {
            info.append(" " + compName);
          }
        }
      }
      else if (region.hasComponent())
      {
        info.append(" " + region.getComponent().getReferenceDesignator());
        info.append(" " + region.getPads().size());
        for (Pad pad : region.getPads())
        {
          info.append(" " + pad.getName());
        }
        info.append(" " + region.getPads().iterator().next().getPadTypeSettings().getGlobalSurfaceModel());
        info.append(" " + region.getSubtypes().iterator().next().getLongName());
        
        SubtypeAdvanceSettings subtypeAdvanceSettings = region.getSubtypes().iterator().next().getSubtypeAdvanceSettings();
        info.append(" " + subtypeAdvanceSettings.getMagnificationType());
        info.append(" " + subtypeAdvanceSettings.getUserGain());
        info.append(" " + subtypeAdvanceSettings.getSignalCompensation());
        info.append(" " + subtypeAdvanceSettings.getArtifactCompensationState());
      }
//      else if (region.hasComponent())
//      {
//        List<Pad> pads = new LinkedList<Pad>(region.getPads());
//        Collections.sort(pads, new PadComparator());
//        info.append(" " + region.getComponent().getReferenceDesignator());
//        info.append(" " + region.getPads().size());
//        for (Pad pad : pads)
//        {
//          info.append(" " + pad.getName());
//        }
//      }
//
//      info.append(" " + region.getFiducialInspectionDataList().size());
//
//      for (FiducialInspectionData jointData : region.getFiducialInspectionDataList())
//      {
//        info.append(" " + jointData.getFiducial().getName());
//      }
//
      List<FocusGroup> focusGroups = region.getFocusGroups();
//      Collections.sort(focusGroups, _focusGroupComparator);

      info.append(" " + region.getNumberOfSlices());
      info.append(" " + focusGroups.size());
      for (FocusGroup focusGroup : focusGroups)
      {
        info.append(focusGroup.getNumberOfSlices());

//        List<Slice> slices = focusGroup.getSlices();
//        Collections.sort(slices, _sliceComparator);

        for (Slice slice : focusGroup.getSlices())
        {
          FocusInstruction instruction = slice.getFocusInstruction();

          String sliceInfo = slice.getSliceName().getName() + " " +
                             instruction.getFocusMethod().getDescription() + " " +
                             instruction.getPercentValue() + " " +
                             instruction.getZHeightInNanoMeters() + " " +
                             instruction.getZOffsetInNanoMeters();
          info.append(" " + sliceInfo);
        }
//        FocusSearchParameters searchParams = focusGroup.getFocusSearchParameters();
//        FocusRegion focusRegion = searchParams.getFocusRegion();
//        PanelRectangle focusRect = focusRegion.getRectangleRelativeToPanelInNanometers();
//
//        info.append(" " + searchParams.getHorizontalSharpnessComponentPercentage());
//        info.append(" " + searchParams.getGradientWeightingMagnitudePercentage());
//        info.append(" " + searchParams.getGradientWeightingOrientationInDegrees());
//        info.append(" " + focusRect.getMinX());
//        info.append(" " + focusRect.getMinY());
//        info.append(" " + focusRect.getWidth());
//        info.append(" " + focusRect.getHeight());
//        info.append(" " + searchParams.getFocusProfileShape().getDescription());
//        info.append(" " + searchParams.getZCurveWidthInNanoMeters());
//
        if (region.isAlignmentRegion() == false)
        {
          if( focusGroup.hasNeighbors())
          {
            List<ReconstructionRegion> neighbors = focusGroup.getNeighbors();
            info.append(" " + neighbors.size());
//            Collections.sort(neighbors, _reconstructionRegionComparator);
            for (ReconstructionRegion neighbor : neighbors)
              info.append(" " + neighbor.getName());
          }
        }
      }
      info.append(" " + region.isAffectedByInterferencePattern());
      if (region.isAffectedByInterferencePattern())
      {
        for (Pair<Integer, Integer> pair : region.getCandidateStepRanges().getRanges())
        {
          info.append(" " + pair.getFirst() + "-" + pair.getSecond());
        }
      }
    }
  }

  /**
   * @author Wei Chin, Chong
   */
  public void invalidateCheckSum()
  {
    _checkSum = -1;
    getProject().setIsSurfaceMapPointPopulated(false);
  }

  /**
   * @author Wei Chin, Chong
   */
  public long getCheckSum()
  {
    if (_checkSum == -1)
    {
      _checkSum = generateCheckSumValue();
    }

    Assert.expect(_checkSum != -1);

    return _checkSum;
  }

  /**
   * @author Wei Chin, Chong
   */
  public void setDebugCheckSumTiming(boolean debugCheckSumTiming)
  {
    _debugCheckSumTiming = debugCheckSumTiming;
  }

  /**
   * @param hasAssigned
   * @author Wei Chin, Chong
   */
  public void setNeighboringHasAssigned(boolean hasAssigned)
  {
    _neighboringHasAssigned = hasAssigned;
  }

  /**
   * @author Wei Chin, Chong
   */
  public boolean isNeighboringAssignmentDone()
  {
    return _neighboringHasAssigned;
  }

  /*
   * @author sham
   */
  public TestSubProgram getTestSubProgramById(int id)
  {
    return _testSubProgramMap.get(id);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public TestSubProgram getTestSubProgramByUserGainEnum(UserGainEnum gainEnum)
  {
    return _userGainToSubProgramMap.get(gainEnum);
  }
  
  /*
   * @author Kee Chin Seong
   */
  public void clearTestSubProgramToUserGainEnumMap()
  {
    Assert.expect(_userGainToSubProgramMap != null);
    _userGainToSubProgramMap.clear();
  }
  
  /*
   * @author sheng chuan
   */
  public TestSubProgram getTestSubProgramByStageSpeedEnum(StageSpeedEnum speedEnum)
  {
    return _stageSpeedToSubProgramMap.get(speedEnum);
  }
  
  /*
   * @author sheng chuan
   */
  public void clearTestSubProgramToStageSpeedEnumMap()
  {
    Assert.expect(_stageSpeedToSubProgramMap != null);
    _stageSpeedToSubProgramMap.clear();
  }
  
  /*
   * @author Kee chin Seong
   */
  public void clearTestProgramMap()
  {
    Assert.expect(_testSubProgramMap != null);
    
    _testSubProgramMap.clear();
  }

  /*
   * @author sham
   */
  public void setTotalLowMagReconstructionRegionToSubProgramMap(int totalReconstructionRegion, int subProgramId)
  {
    Assert.expect(totalReconstructionRegion >= 0);
    Assert.expect(subProgramId > 0);

    _totalLowMagReconstructionRegionToSubProgramMap.put(subProgramId, totalReconstructionRegion);
  }

    /*
   * @author sham
   */
  public void setTotalHighMagReconstructionRegionToSubProgramMap(int totalReconstructionRegion, int subProgramId)
  {
    Assert.expect(totalReconstructionRegion >= 0);
    Assert.expect(subProgramId > 0);

    _totalHighMagReconstructionRegionToSubProgramMap.put(subProgramId, totalReconstructionRegion);
  }
  
    /*
   * @author sham
   */
  public void clearTotalReconstructionRegionToSubProgramMap()
  {
    Assert.expect(_totalLowMagReconstructionRegionToSubProgramMap != null);
    Assert.expect(_totalHighMagReconstructionRegionToSubProgramMap != null);

    _totalLowMagReconstructionRegionToSubProgramMap.clear();
    _totalHighMagReconstructionRegionToSubProgramMap.clear();
  }

  /*
   * @author sham
   */
  public int getSubProgramIdByTotalNumberOfLowMagReconstructionRegion(int totalReconstructionRegion)
  {
    Assert.expect(totalReconstructionRegion >= 0);

    int subProgramId = 0;
    int accumulateReconstructionRegion = 0;
    for (TestSubProgram subProgram : getFilteredTestSubPrograms())
    {
      if(subProgram.isLowMagnification())
      {
        accumulateReconstructionRegion = _totalLowMagReconstructionRegionToSubProgramMap.get(subProgram.getId());
        if (totalReconstructionRegion <= accumulateReconstructionRegion)
        {
          subProgramId = subProgram.getId();
          break;
        }
     }
    }

    return subProgramId;
  }

  /*
   * @author sham
   */
  public int getSubProgramIdByTotalNumberOfHighMagReconstructionRegion(int totalReconstructionRegion)
  {
    Assert.expect(totalReconstructionRegion >= 0);
    
    int subProgramId = 0;
    int accumulateReconstructionRegion = 0;
    for (TestSubProgram subProgram : getFilteredTestSubPrograms())
    {
      if(subProgram.isHighMagnification())
      {
        accumulateReconstructionRegion = _totalHighMagReconstructionRegionToSubProgramMap.get(subProgram.getId());
        if (totalReconstructionRegion <= accumulateReconstructionRegion)
        {
          subProgramId = subProgram.getId();
          break;
        }
      }
    }

    return subProgramId;
  }

   /**
   * @author sham
   * @author Kee Chin Seong - XXL Speed Improvement 
   *                        - Comment out part is double work no need that portion.
   */
  public List<TestSubProgram> getAllInspectableTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);

    List<TestSubProgram> subPrograms = new LinkedList<TestSubProgram>(_testSubPrograms);
    //if (_notInspectableTestSubProgramFilters.isEmpty() == false)
    //{
    //  subPrograms.removeAll(_notInspectableTestSubProgramFilters);
    //}

    return subPrograms;

  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<ReconstructionRegion> getInspectionRegions(Component component)
  {
    List<ReconstructionRegion> reconstructionRegions = new LinkedList<ReconstructionRegion>();
    for(TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
    {
      List<ReconstructionRegion> testSubProgramRegions = testSubProgram.getInspectionRegions(component);
      if (testSubProgramRegions != null && testSubProgramRegions.isEmpty() == false)
        reconstructionRegions.addAll(testSubProgramRegions);
    }
    return reconstructionRegions;
  }
  
  /*
   * @author Kee Chin Seong - XXL Speed Development.
   *                          by using subtype, we need acquire back the reconstruction
   *                          region, and remove it from the testsubprogram,
   *                          added into a list, prepare to dividing them into 
   *                          correct test sub program.
   */
  public List<ReconstructionRegion> rearrangeInspectionRegionBasedOnUserGain(List<Subtype> subtypeList)
  {
    Assert.expect(subtypeList != null);
    
    List<ReconstructionRegion> reconstructionRegionList = new ArrayList<ReconstructionRegion>();
    
    for(Subtype subtype : subtypeList)
    {
      for(TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
      {
        List<ReconstructionRegion> subtypeRegions = testSubProgram.getInspectionRegions(subtype);
        if(subtypeRegions != null)
        {
          //remove the reconstruction region from the test sub program first
          for(ReconstructionRegion region : subtypeRegions)
          {
            testSubProgram.removeInspectionRegionFromTestSubProgram(region);
            
            if (reconstructionRegionList.contains(region) == false)
            {
              reconstructionRegionList.add(region);
            }
          }                   
        }
      }
    }
    
    return reconstructionRegionList;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<TestSubProgram> getAllInspectableSurfaceMapTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);
    
    List<TestSubProgram> inspectableTestSubPrograms = new LinkedList<TestSubProgram>();
    List<TestSubProgram> tempTestSubPrograms = getFilteredTestSubPrograms();
    
    for(TestSubProgram tempTestSubProgram : tempTestSubPrograms)
    {
        if (tempTestSubProgram.hasEnabledOpticalCameraRectangles())
        {
            inspectableTestSubPrograms.add(tempTestSubProgram);
        }
    }
    return inspectableTestSubPrograms;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<TestSubProgram> getAllInspectableAlignmentTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);
    
    List<TestSubProgram> inspectableTestSubPrograms = new LinkedList<TestSubProgram>();
    List<TestSubProgram> tempTestSubPrograms = getFilteredTestSubPrograms();
    
    for(TestSubProgram tempTestSubProgram : tempTestSubPrograms)
    {
      if (tempTestSubProgram.isSubProgramPerformAlignment())
      {
        inspectableTestSubPrograms.add(tempTestSubProgram);
      }
    }
    return inspectableTestSubPrograms;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<TestSubProgram> getAllInspectableAlignmentSurfaceMapTestSubPrograms()
  {
    Assert.expect(_testSubPrograms != null);
    
    List<TestSubProgram> inspectableTestSubPrograms = new LinkedList<TestSubProgram>();
    List<TestSubProgram> tempTestSubPrograms = getFilteredTestSubPrograms();
    
    for(TestSubProgram tempTestSubProgram : tempTestSubPrograms)
    {
        if (tempTestSubProgram.hasAlignmentOpticalRegions())
        {
            inspectableTestSubPrograms.add(tempTestSubProgram);
        }
    }
    return inspectableTestSubPrograms;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<OpticalPointToPointScan> getOpticalPointToPointScans()
  {
      List<OpticalPointToPointScan> opticalPointToPointScans = new LinkedList<OpticalPointToPointScan>();
      for(TestSubProgram testSubProgram : getAllInspectableSurfaceMapTestSubPrograms())
      {
          opticalPointToPointScans.addAll(testSubProgram.getOpticalPointToPointScans());
      }
      return opticalPointToPointScans;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public List<OpticalPointToPointScan> getAlignmentOpticalPointToPointScans()
  {
      List<OpticalPointToPointScan> opticalPointToPointScans = new LinkedList<OpticalPointToPointScan>();
      for(TestSubProgram testSubProgram : getAllInspectableAlignmentSurfaceMapTestSubPrograms())
      {
          opticalPointToPointScans.addAll(testSubProgram.getAlignmentOpticalPointToPointScans());
      }
      return opticalPointToPointScans;
  }

  /*
   * @author sham
   */
  public Map<Integer, Integer> getTotalLowMagReconstructionRegionToSubProgramMap()
  {
    return _totalLowMagReconstructionRegionToSubProgramMap;
  }

  /*
   * @author sham
   */
  public Map<Integer, Integer> getTotalHighMagReconstructionRegionToSubProgramMap()
  {
    return _totalHighMagReconstructionRegionToSubProgramMap;
  }


    /**
   * @author George A. David
   */
  public void addNotInspectableTestSubProgramFilters(TestSubProgram subProgram)
  {
    Assert.expect(subProgram != null);
    _notInspectableTestSubProgramFilters.add(subProgram);
  }

    /**
   * @author George A. David
   */
  public void clearNotInspectableTestSubProgramFilters()
  {
    _notInspectableTestSubProgramFilters.clear();
  }


  // Chin Seong, Kee
  public void filterTestSubProgram(List<String> boardNames)
  {
    Assert.expect(boardNames != null);

    for (TestSubProgram subProgram : _testSubPrograms)
    {
      if(_project.getPanel().getPanelSettings().isPanelBasedAlignment() == false)
      {
        String boardName = subProgram.getBoard().getName();
        if (boardNames.contains(boardName) == false)
        {
          addFilter(subProgram.getBoard());
        }
      }
    }
  }

  // Chin Seong, Kee
  public void revertBackOriginalTestSubProgram()
  {
    for(Map.Entry<Integer, TestSubProgram> entry : _testSubProgramMap.entrySet())
    {
      TestSubProgram testSubProgram = entry.getValue();
      if(_testSubPrograms.contains(testSubProgram) == false && testSubProgram.isAllRegionNotIspectable() == false)
        _testSubPrograms.add(testSubProgram);
    }
  }


  /**
   * @return 
   * @author Sham
   */
  public List<TestSubProgram> getTestSubPrograms()
  {
    return _testSubPrograms;
  }

  // @author sham
  public void addNotInspectableTestSubProgramFilters()
  {
    List<TestSubProgram> subPrograms = new ArrayList<TestSubProgram>(_testSubPrograms);
    clearNotInspectableTestSubProgramFilters();
    for (TestSubProgram subProgram : subPrograms)
    {
      if (subProgram.isAllRegionNotIspectable())
      {
        addNotInspectableTestSubProgramFilters(subProgram);
      }
    }
  }

  /**
   * Added by Seng Yew on 22-Apr-2011
   * Update any graylevel minimum & maximum for image normalization by subtype
   * Have to make sure this is called if fastLoad is true
   * IMPORTANT NOTE : Have to make sure only one subtype in one reconstruction region.
   *
   * @author George A. David
   */
//  public void setAllInspectionRegionsWithImageNormalizationBySubtype()
//  {
//    for (ReconstructionRegion reconstructionRegion : getAllInspectionRegions())
//    {
//      // The way to get subtype must consistent with createInspectionRegion() in InspectionRegionGeneration.java
//      Subtype subtype = reconstructionRegion.getComponent().getPads().iterator().next().getSubtype();
//      if (subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM) &&
//          subtype.doesAlgorithmSettingExist(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM))
//      {
//        float minVal = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MINIMUM);
//        float maxVal = (Float)subtype.getAlgorithmSettingValue(AlgorithmSettingEnum.USER_DEFINED_GRAYLEVEL_MAXIMUM);
//        reconstructionRegion.setMinimumGraylevel(minVal);
//        reconstructionRegion.setMaximumGraylevel(maxVal);
//      }
//    }
//  }

  /**
   * @author Cheah Lee Herng
   */
  public void removeOpticalRegion(OpticalRegion opticalRegion)
  {
    Assert.expect(opticalRegion != null);

    for(TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
    {
      if (testSubProgram.isOpticalRegionAvailable(opticalRegion))
        testSubProgram.removeOpticalRegion(opticalRegion);

      if (testSubProgram.hasOpticalRegionNameToSubProgramReference(opticalRegion.getName()))
        testSubProgram.removeOpticalRegionNameToSubProgramReference(opticalRegion.getName());
    }
  }
  
  /**
   * XCR-3854, System crash when click Learn subtype exposure setting button
   * @author Yong Sheng Chuan
   */
  public void addOpticalRegion(OpticalRegion opticalRegion)
  {
    addOpticalRegion(opticalRegion, opticalRegion.getComponents());
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public void addOpticalRegion(OpticalRegion opticalRegion, List<Component> components)
  {
    Assert.expect(opticalRegion != null);
      
    int subProgramReferenceIdForOpticalRegion = -1;
    for(Component component : components)
    {
      // Reset variable
      subProgramReferenceIdForOpticalRegion = -1;
      
      List<TestSubProgram> testSubPrograms = getTestSubPrograms();
      for(TestSubProgram testSubProgram : testSubPrograms)
      {
        if (testSubProgram.getImageableRegionInNanoMeters() .intersects(
            component.getShapeSurroundingPadsRelativeToPanelInNanoMeters().getBounds()) &&
            //XCR-3343, Assert when run surface map after setup Customized Alignment 
            testSubProgram.isSubProgramPerformSurfaceMap())
        {
          subProgramReferenceIdForOpticalRegion = testSubProgram.getId();
          break;
        }
      }
      
      // Sanity check
      Assert.expect(subProgramReferenceIdForOpticalRegion > 0);
      
      for(TestSubProgram testSubProgram : testSubPrograms)
      {
        PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();

        if (opticalRegion.getRegion().intersects(testSubProgramImageableRegion) &&
            testSubProgram.isOpticalRegionAvailable(opticalRegion) == false &&
            testSubProgram.hasComponent(component) &&
            testSubProgram.getImageableRegionInNanoMeters() .intersects(
                component.getShapeSurroundingPadsRelativeToPanelInNanoMeters().getBounds()))
        {
          if (testSubProgram.getId() == subProgramReferenceIdForOpticalRegion)
          { 
            testSubProgram.addOpticalRegion(opticalRegion);
          }
          else
          {
            Assert.expect(subProgramReferenceIdForOpticalRegion > 0);
            Assert.expect(testSubProgram.getId() > subProgramReferenceIdForOpticalRegion);
            testSubProgram.addOpticalRegionNameToSubProgramReference(opticalRegion.getName(), subProgramReferenceIdForOpticalRegion, component);
          }
        }
      }
    }
  }
  
  /**
   * XCR-3854, System crash when click Learn subtype exposure setting button
   * @author Cheah Lee Herng
   */
  public void addOpticalRegion(OpticalRegion opticalRegion, Board board)
  {
    Assert.expect(opticalRegion != null);
    Assert.expect(board != null);

    addOpticalRegion (opticalRegion, opticalRegion.getComponents(board));
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addAlignmentOpticalRegion(OpticalRegion opticalRegion)
  {
      Assert.expect(opticalRegion != null);
      
      for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
      {
          for(TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
          {
              PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();

              if (opticalCameraRectangle.getRegion().intersects(testSubProgramImageableRegion) &&
                  testSubProgram.isAlignmentOpticalRegionAvailable(opticalRegion) == false &&
                  testSubProgram.isSubProgramPerformAlignment())
              {
                  testSubProgram.addAlignmentOpticalRegion(opticalRegion);
              }
          }
      }
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void addAlignmentOpticalRegion(OpticalRegion opticalRegion, Board board)
  {
      Assert.expect(opticalRegion != null);
      Assert.expect(board != null);
      
      for(OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getOpticalCameraRectangles(board))
      {
          for(TestSubProgram testSubProgram : getTestSubPrograms(board))
          {
              PanelRectangle testSubProgramImageableRegion = testSubProgram.getImageableRegionInNanoMeters();

              if (opticalCameraRectangle.getRegion().intersects(testSubProgramImageableRegion) &&
                  testSubProgram.isAlignmentOpticalRegionAvailable(opticalRegion) == false &&
                  testSubProgram.isSubProgramPerformAlignment())
              {
                  testSubProgram.addAlignmentOpticalRegion(opticalRegion);
              }
          }
      }
  }
  
  /**
   * @return 
   * @author Wei Chin
   * @author Kee Chin Seong - must put && or else will causing alignment relearn warning
   */
  public boolean hasVariableMagnification()
  {
    boolean hasLowMag = false;
    boolean hasHighMag = false;
    
    for(TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
    {
      if(testSubProgram.isLowMagnification() && testSubProgram.getAllInspectionRegions().size() > 0)
        hasLowMag = true;
      else if(testSubProgram.isHighMagnification() && testSubProgram.getAllInspectionRegions().size() > 0)
        hasHighMag = true;      
    }
    
    if(hasLowMag && hasHighMag) 
      return true;
    
    return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void disableRetestForComponent(Component component)
  {
      Assert.expect(component != null);
      
      List<com.axi.v810.business.testProgram.ReconstructionRegion> reconstructionRegions = getInspectionRegions(component);
      for(ReconstructionRegion reconstructionRegion : reconstructionRegions)
      {
          reconstructionRegion.setRetestForFailingJoint(false);
          reconstructionRegion.setRetestForPassingJoint(false);
      }
  }
  
  /**
   * @param getReconstructionRegionId Integer
   * @param sliceIdToReconstructedSliceMap Map<Integer, Integer>
   * @author Wei Chin, Chong
   * Originated from Wei Chin. Move from JointInspectionResult.java & ComponentInspectionResult.java to TestProgram.java by Chin Seong.
   * Completed the move by Seng Yew on 28-Sep-2011
   * Related CR : CR 1019
   */
  public void addSliceIdToReconstructedSliceMap (Integer getReconstructionRegionId, Map<Integer, Integer> sliceIdToReconstructedSliceMap)
  {
    Assert.expect(getReconstructionRegionId != null);
    Assert.expect(sliceIdToReconstructedSliceMap != null);
    _rridToSliceIdReconstructedSliceMap.put(getReconstructionRegionId, sliceIdToReconstructedSliceMap);
  }

  /**
   * @param getReconstructionRegionId Integer
   * @author Wei Chin, Chong
   * Originated from Wei Chin. Move from JointInspectionResult.java & ComponentInspectionResult.java to TestProgram.java by Chin Seong.
   * Completed the move by Seng Yew on 28-Sep-2011
   * Related CR : CR 1019
   */
  public Map<Integer, Integer> getSliceIdToReconstructedSliceMapByRRID (Integer getReconstructionRegionId)
  {
    Assert.expect(getReconstructionRegionId != null);
    if (_rridToSliceIdReconstructedSliceMap == null)
      return null;
    return _rridToSliceIdReconstructedSliceMap.get(getReconstructionRegionId);
  }

  /**
   * @param jointMeasurement JointMeasurement
   * @author Wei Chin, Chong
   * Originated from Wei Chin. Move from JointInspectionResult.java to TestProgram.java by Chin Seong.
   * Completed the move by Seng Yew on 28-Sep-2011
   * Related CR : CR 1019
   */
  public Integer getReconstructedSliceZHeightByJointMeasurement (JointMeasurement jointMeasurement)
  {
    Assert.expect(jointMeasurement != null);
    JointInspectionData jointInspectionData = getJointInspectionData(jointMeasurement.getPad());
    Integer reconstructedRegionID = jointInspectionData.getInspectionRegion().getRegionId();
    Map<Integer,Integer> sliceIdToZHeightMap = getSliceIdToReconstructedSliceMapByRRID(reconstructedRegionID);
    if (sliceIdToZHeightMap == null)
        return null;
    Integer zHeightInNanoMeter = sliceIdToZHeightMap.get(jointMeasurement.getSliceNameEnum().getId());
    if (zHeightInNanoMeter == null)
        return null;
    return zHeightInNanoMeter;
  }

  /**
   * @author Wei Chin, Chong
   * Originated from Wei Chin. Move from JointInspectionResult.java & ComponentInspectionResult.java to TestProgram.java by Chin Seong.
   * Completed the move by Seng Yew on 28-Sep-2011
   * Related CR : CR 1019
   */
  public void clearSliceIdToReconstructedSliceMap ()
  {
    for(Map.Entry<Integer,Map<Integer,Integer>> rridToSliceIdReconstructedSliceMap : _rridToSliceIdReconstructedSliceMap.entrySet())
    {
      rridToSliceIdReconstructedSliceMap.getValue().clear();
    }
    _rridToSliceIdReconstructedSliceMap.clear();
  }
  
  /**
   * @return 
   * @author Kee Chin Seong
   * Related CR :XCR1479
   *             Return the total of Original Test Program size generated to make sure the 
   *             Verification Graphic Engine has all layers on it and wont crash
   *             Side Effect : Increase the Response Speed of Adjust CAD
   */
  public int getSizeOfAllTestSubProgramsForGraphicEngineLayersSetup()
  {
     Assert.expect(_testSubProgramMap != null);
     
     return _testSubProgramMap.size();
  }
  
  /**
   * sheng chuan
   * @return 
   */
  public List<Integer> getAllUsedTestSubProgramID()
  {
     List<Integer> testSubProgramIdList = new ArrayList<Integer>();
     //XCR-3437, Cad offset when view verification on long panel recipe
     for(TestSubProgram tsp : _testSubPrograms)
       testSubProgramIdList.add(tsp.getId());
     
     return testSubProgramIdList;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void clear(boolean isVirtualLiveWithAlignment)
  {
    if (_testSubPrograms != null)
    {
      for(TestSubProgram testSubProgram : _testSubPrograms)
        testSubProgram.clear(isVirtualLiveWithAlignment);
      
      _testSubPrograms.clear();
    }
    
    if (_xrayCameraAcquisitionParameters != null)
    {
      for(XrayCameraAcquisitionParameters xrayCameraAcquisitionParameters : _xrayCameraAcquisitionParameters)
          xrayCameraAcquisitionParameters.clear();
        
      _xrayCameraAcquisitionParameters.clear();
    }
    
    if (_jointTypesToFilterOn != null)
        _jointTypesToFilterOn.clear();
    
    if (_regionsToFilterOn != null)
        _regionsToFilterOn.clear();
    
    if (_subProgramsToFilterOn != null)
        _subProgramsToFilterOn.clear();
    
    if (_testSubProgramMap != null)
        _testSubProgramMap.clear();
    
    if (_totalLowMagReconstructionRegionToSubProgramMap != null)
        _totalLowMagReconstructionRegionToSubProgramMap.clear();
    
    if (_totalHighMagReconstructionRegionToSubProgramMap != null)
        _totalHighMagReconstructionRegionToSubProgramMap.clear();
    
    if (_notInspectableTestSubProgramFilters != null)
        _notInspectableTestSubProgramFilters.clear();
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void createTotalReconstructionRegionToSubProgramMap()
  {
    int accumulateLowMagReconstructionRegion = 0;
    int accumulateHighMagReconstructionRegion = 0;
    //XCR-3475, System Hang When Skip Board Serial Number
    for (TestSubProgram subProgram : getFilteredTestSubPrograms())
    {
      if(subProgram.isAllRegionNotIspectable() == false ||
         (subProgram.isSubProgramPerformAlignment() && subProgram.isAllRegionNotIspectable()))
      {
        if(subProgram.isLowMagnification())
        {
          accumulateLowMagReconstructionRegion += subProgram.getFilteredInspectableRegions().size() + subProgram.getAlignmentRegions().size();
          setTotalLowMagReconstructionRegionToSubProgramMap(accumulateLowMagReconstructionRegion,subProgram.getId());
          if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
          {
            System.out.println("generateTestProgramDebug - subProgram id : " + subProgram.getId());
            System.out.println("generateTestProgramDebug - isAllRegionNotIspectable : " + subProgram.isAllRegionNotIspectable());
            System.out.println("generateTestProgramDebug - isSubProgramPerformAlignment : " + subProgram.isSubProgramPerformAlignment());
            System.out.println("generateTestProgramDebug - getSubProgramRefferenceIdForAlignment : " + subProgram.getSubProgramRefferenceIdForAlignment());
            System.out.println("generateTestProgramDebug - getFilteredInspectableRegions:" + subProgram.getFilteredInspectableRegions().size());
            System.out.println("generateTestProgramDebug - getAlignmentRegions:" + subProgram.getAlignmentRegions().size());
            System.out.println("generateTestProgramDebug - accumulateLowMagReconstructionRegion: " + subProgram.getId() + " => " + String.valueOf(accumulateLowMagReconstructionRegion));
            System.out.println("generateTestProgramDebug - usergain :"+ subProgram.getUserGain().toString());
            System.out.println("generateTestProgramDebug - stageSpeed :"+ subProgram.getStageSpeed().toString());
            System.out.println("generateTestProgramDebug - magnification :"+ subProgram.getMagnificationType().toString());
          }
        }
        else
        {
          accumulateHighMagReconstructionRegion += subProgram.getFilteredInspectableRegions().size() + subProgram.getAlignmentRegions().size();
          setTotalHighMagReconstructionRegionToSubProgramMap(accumulateHighMagReconstructionRegion,subProgram.getId());
          if(Config.isDeveloperSystemEnabled() && Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.GENERATE_TESTPROGRAM_DEBUG))
          {
            System.out.println("generateTestProgramDebug - subProgram id : " + subProgram.getId());
            System.out.println("generateTestProgramDebug - isAllRegionNotIspectable : " + subProgram.isAllRegionNotIspectable());
            System.out.println("generateTestProgramDebug - isSubProgramPerformAlignment : " + subProgram.isSubProgramPerformAlignment());
            System.out.println("generateTestProgramDebug - getSubProgramRefferenceIdForAlignment : " + subProgram.getSubProgramRefferenceIdForAlignment());
            System.out.println("generateTestProgramDebug - getFilteredInspectableRegions:" + subProgram.getFilteredInspectableRegions().size());
            System.out.println("generateTestProgramDebug - getAlignmentRegions:" + subProgram.getAlignmentRegions().size());
            System.out.println("generateTestProgramDebug - accumulateHighMagReconstructionRegion: " + subProgram.getId() + " => " + String.valueOf(accumulateHighMagReconstructionRegion));
            System.out.println("generateTestProgramDebug - usergain :"+ subProgram.getUserGain().toString());
            System.out.println("generateTestProgramDebug - stageSpeed :"+ subProgram.getStageSpeed().toString());
            System.out.println("generateTestProgramDebug - magnification :"+ subProgram.getMagnificationType().toString());
          }
        }
      }
    }
  }
  
  /**
   * @author Wei Chin
   */
  private static Comparator<TestSubProgram> _subProgramComparator = new Comparator<TestSubProgram>()
  {
    public int compare(TestSubProgram lhs, TestSubProgram rhs)
    {
      if(lhs.getPanelLocationInSystem().equals(rhs.getPanelLocationInSystem()))
        return 0;
      
      if(lhs.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
        return 1;
      
      return -1;
    }
  };
  
  private static Comparator<TestSubProgram> _subProgramComparatorBasedOnAlignmentSequence = new Comparator<TestSubProgram>()
  {
    public int compare(TestSubProgram lhs, TestSubProgram rhs)
    {
      PanelSettings panelSettings = lhs.getTestProgram().getProject().getPanel().getPanelSettings();
      if (panelSettings.isPanelBasedAlignment())
      {
        if (lhs.getMagnificationType().equals(rhs.getMagnificationType()))
        {
          if (lhs.getPanelLocationInSystem().equals(rhs.getPanelLocationInSystem()))
          {
            if (lhs.isSubProgramPerformAlignment() && rhs.isSubProgramPerformAlignment() == false)
              return -1;
            else if (rhs.isSubProgramPerformAlignment() && lhs.isSubProgramPerformAlignment() == false)
              return 1;
            else
              return 0;
          }
          else if (lhs.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
            return 1;

          return 0;
        }
        else if (lhs.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          return -1;
        else if (rhs.getMagnificationType().equals(MagnificationTypeEnum.LOW))
          return 1;
      }
      else
      {
        if (lhs.getBoard().equals(rhs.getBoard()))
        {
          if (lhs.getMagnificationType().equals(rhs.getMagnificationType()))
          {
            if (lhs.getPanelLocationInSystem().equals(rhs.getPanelLocationInSystem()))
            {
              if (lhs.isSubProgramPerformAlignment() && rhs.isSubProgramPerformAlignment() == false)
                return -1;
              else if (rhs.isSubProgramPerformAlignment() && lhs.isSubProgramPerformAlignment() == false)
                return 1;
              else
                return 0;
            }
            else if (lhs.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT))
              return 1;
            
            return 0;
          }
          else if (lhs.getMagnificationType().equals(MagnificationTypeEnum.LOW))
            return -1;
          else if (rhs.getMagnificationType().equals(MagnificationTypeEnum.LOW))
            return 1;
        }
        else       
          return (lhs.getBoard().getName().compareTo(rhs.getBoard().getName()));       
      }
      return 0;
    }
  };
  
  /**
   * @author Wei Chin
   */
  public void sortTestSubProgram()
  {
    Collections.sort(_testSubPrograms, _subProgramComparator);  
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void sortTestSubProgramBasedOnAlignmentSequence()
  {
    Collections.sort(_testSubPrograms, _subProgramComparatorBasedOnAlignmentSequence);
  }
  
     /**
   * @author Cheah Lee Herng
   */
  public boolean isComponentUsePsp(String componentRefDes)
  {
     Assert.expect(componentRefDes != null); 
      
     boolean isComponentUsePsp = false;
     for (TestSubProgram subProgram : getAllInspectableTestSubPrograms())
     {
        if (subProgram.isComponentUsePsp(componentRefDes))
        {
           isComponentUsePsp = true;
           break;
        }
     }
     return isComponentUsePsp;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalRegion getOpticalRegion(String opticalRegionName)
  {
    Assert.expect(opticalRegionName != null);
    
    OpticalRegion opticalRegion = null;
    for(TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
    {
      opticalRegion = testSubProgram.getOpticalRegion(opticalRegionName);
      if (opticalRegion == null)
        continue;
      else
        break;
    }
    return opticalRegion;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public OpticalRegion getOpticalRegion(Board board, String opticalRegionName)
  {
    Assert.expect(board != null);
    Assert.expect(opticalRegionName != null);
    
    OpticalRegion opticalRegion = null;
    for(TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
    {
      opticalRegion = testSubProgram.getOpticalRegion(opticalRegionName);
      if (opticalRegion == null)
        continue;
      else
      {
        boolean foundOpticalRegion = false;
        for(Board currentBoard : opticalRegion.getBoards())
        {
          if (currentBoard.equals(board))
          {
            foundOpticalRegion = true;
            break;
          }
        }
        
        if (foundOpticalRegion)
          break;
      }
    }
    return opticalRegion;
  }
  
  /*
   * Clear runtime manual alignment transform
   * @author Phang Siew Yeng
   */
  public void clearTestSubProgramsRuntimeManualAlignmentTransform()
  {
    getProject().getPanel().getPanelSettings().clearRuntimeManualAlignmentTransform();

    for(TestSubProgram subProgram : getFilteredTestSubPrograms())
    {
      subProgram.setIsUsingRuntimeManualAlignmentTransform(false);
      
      if(getProject().getPanel().getPanelSettings().isPanelBasedAlignment() == false)
        subProgram.getBoard().getBoardSettings().clearRuntimeManualAlignmentTransform();
    }
  }
  
  /*
   * Check if any of the subProgram is using runtime manual alignment transform during production
   * @author Phang Siew Yeng
   */
  public boolean hasRuntimeManualAlignmentTransform()
  {
    for(TestSubProgram subProgram : getFilteredTestSubPrograms())
    {
      if(subProgram.isUsingRuntimeManualAlignmentTransform())
        return true;
    }
    
    return false;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void invalidateOpticalCheckSum()
  {
    _opticalInspectionChecksum = -1;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public long getOpticalInspectionChecksum()
  {        
    if (_opticalInspectionChecksum == -1)
      _opticalInspectionChecksum = generateOpticalInspectionChecksum();
    
    Assert.expect(_opticalInspectionChecksum != -1);
    return _opticalInspectionChecksum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public void invalidateAlignmentOpticalCheckSum()
  {
    _alignmentOpticalInspectionChecksum = -1;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  public long getAlignmentOpticalInspectionChecksum()
  {        
    if (_alignmentOpticalInspectionChecksum == -1)
      _alignmentOpticalInspectionChecksum = generateAlignmentOpticalInspectionChecksum();
    
    Assert.expect(_alignmentOpticalInspectionChecksum != -1);
    return _alignmentOpticalInspectionChecksum;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  private long generateOpticalInspectionChecksum()
  {        
    CRC32 checksumUtil = new CRC32();
    checksumUtil.reset();

    StringBuilder info = new StringBuilder();
    
    info.append(_project.getName());
    info.append(" " + getAllInspectableSurfaceMapTestSubPrograms().size());
    info.append(" " + _project.getPanel().getPanelSettings().isPanelBasedAlignment());
    
    for (TestSubProgram testSubProgram : getAllInspectableSurfaceMapTestSubPrograms())
    {
      for (OpticalRegion opticalRegion : testSubProgram.getOpticalRegions())
      {
        info.append(" " + opticalRegion.getName());
        
        List<OpticalCameraRectangle> opticalCameraRectangles = new ArrayList<OpticalCameraRectangle>();
        if (_project.getPanel().getPanelSettings().isPanelBasedAlignment())
        {
          opticalCameraRectangles = opticalRegion.getAllOpticalCameraRectangles();
        }
        else
        {
          opticalCameraRectangles = opticalRegion.getOpticalCameraRectangles(testSubProgram.getBoard());
        }

        for (OpticalCameraRectangle opticalCameraRectangle : opticalCameraRectangles)
        {
          info.append(" " + opticalCameraRectangle.getName());
          info.append(" " + opticalCameraRectangle.getInspectableBool());
          
          if (opticalCameraRectangle.getInspectableBool() == true)
          {
            for (PanelCoordinate pointPanelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
            {
              info.append(" " + String.valueOf(pointPanelCoordinate.getX()) + "_" + String.valueOf(pointPanelCoordinate.getY()));
            }
          }          
        }
      }
    }
    
    checksumUtil.update(info.toString().getBytes());
    return checksumUtil.getValue();
  }
  
  /*
   * @Author Kee Chin Seong - to check whether all tsp is one magnifcation
   */
  public boolean isUsingOneMagnification()
  {
      MagnificationTypeEnum magnification = getAllTestSubPrograms().get(0).getMagnificationType();
      for(TestSubProgram tsp : getAllTestSubPrograms())
      {
         if(tsp.getMagnificationType().equals(magnification) == false)
             return false;
      }
      
      return true;
  }
  
  /**
   * @author Cheah Lee Herng 
   */
  private long generateAlignmentOpticalInspectionChecksum()
  {    
    CRC32 checksumUtil = new CRC32();
    checksumUtil.reset();

    StringBuilder info = new StringBuilder();
    
    info.append(_project.getName());
    info.append(" " + getAllInspectableAlignmentSurfaceMapTestSubPrograms().size());
    info.append(" " + _project.getPanel().getPanelSettings().isPanelBasedAlignment());
    
    for (TestSubProgram testSubProgram : getAllInspectableAlignmentSurfaceMapTestSubPrograms())
    {
      for (OpticalRegion opticalRegion : testSubProgram.getAlignmentOpticalRegions())
      {
        info.append(" " + opticalRegion.getName());
        info.append(" " + opticalRegion.isSetToUsePspForAlignment());
        
        for (OpticalCameraRectangle opticalCameraRectangle : opticalRegion.getAllOpticalCameraRectangles())
        {
          info.append(" " + opticalCameraRectangle.getName());
          info.append(" " + opticalCameraRectangle.getInspectableBool());
          
          if (opticalCameraRectangle.getInspectableBool() == true)
          {
            for (PanelCoordinate pointPanelCoordinate : opticalCameraRectangle.getPanelCoordinateList())
            {
              info.append(" " + String.valueOf(pointPanelCoordinate.getX()) + "_" + String.valueOf(pointPanelCoordinate.getY()));
            }
          }
        }
      }
    }
    
    checksumUtil.update(info.toString().getBytes());
    return checksumUtil.getValue();
  }
  
  /**
   * Get all optical camera rectangle.
   * @author Kok Chun, Tan
   */
  public List<OpticalCameraRectangle> getAllOpticalCameraRectangles()
  {
    List<OpticalCameraRectangle> newOpticalCameraRectangleList = new ArrayList<OpticalCameraRectangle>();
    boolean isPanelBasedAlignment = getProject().getPanel().getPanelSettings().isPanelBasedAlignment();
    
    for (TestSubProgram testSubProgram : getAllInspectableSurfaceMapTestSubPrograms())
    {
      for (OpticalRegion opticalRegion : testSubProgram.getOpticalRegions())
      {
        if (isPanelBasedAlignment)
        {
          if (newOpticalCameraRectangleList.containsAll(opticalRegion.getAllOpticalCameraRectangles()) == false)
          {
            newOpticalCameraRectangleList.addAll(opticalRegion.getAllOpticalCameraRectangles());
          }
        }
        else
        {
          if (newOpticalCameraRectangleList.containsAll(opticalRegion.getOpticalCameraRectangles(testSubProgram.getBoard())) == false)
          {
            newOpticalCameraRectangleList.addAll(opticalRegion.getOpticalCameraRectangles(testSubProgram.getBoard()));
          }
        }
      }
    }

    return newOpticalCameraRectangleList;
  }
  
  /**
   * Get all optical region.
   * @author Kok Chun, Tan
   */
  public List<OpticalRegion> getAllOpticalRegion()
  {
    List<OpticalRegion> newOpticalRegionList = new ArrayList<OpticalRegion>();
    
    for (TestSubProgram testSubProgram : getAllInspectableSurfaceMapTestSubPrograms())
    {
      if (newOpticalRegionList.containsAll(testSubProgram.getOpticalRegions()) == false)
      {
        newOpticalRegionList.addAll(testSubProgram.getOpticalRegions());
      }
    }
    return newOpticalRegionList;
  }
  
  
  /*
   * XCR2181: Able to run alignment starting from failed board
   * @author Khaw Chek Hau
   */
  public boolean isRealignPerformed()
  {
    return _isRealignPerformed;
  }
 
  /*
   * XCR2181: Able to run alignment starting from failed board
   * @author Khaw Chek Hau
   */
  public void setRealignPerformed(boolean isRealignPerformed)
  {
    _isRealignPerformed = isRealignPerformed;
  }  
  /*
   * XCR2181: Able to run alignment starting from failed board
   * @author Khaw Chek Hau
   */ 
  public boolean isRerunningProduction()
  {
    return _isRerunningProduction;
  }
 
  /*
   * XCR2181: Able to run alignment starting from failed board
   * @author Khaw Chek Hau
   */
  public void setRerunningProduction(boolean isRerunningProduction)
  {
    _isRerunningProduction = isRerunningProduction;
  }  
  
  /*
   * XCR2181: Able to run alignment starting from failed board
   * @author Khaw Chek Hau
   */
  public void clearRunAlignmentLearned()
  {    
    for (TestSubProgram subProgram : getTestSubPrograms())
    {
      subProgram.setRunAlignmentLearned(false);
    }
  }
  
  /*
   * XCR2181: Able to run alignment starting from failed board
   * @author Khaw Chek Hau
   */
  public boolean isUsingSkipAlignmentFeature()
  {    
    boolean isAnyAlignmentLearned = getTestSubPrograms().get(0).isRunAlignmentLearned();
    for (TestSubProgram subProgram : getTestSubPrograms())
    {
      if (subProgram.isRunAlignmentLearned() != isAnyAlignmentLearned)
        return true;
    }   
    return false;
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void removeAllOpticalRegion()
  {
    List<OpticalRegion> opticalRegion = getAllOpticalRegion();
    
    for (OpticalRegion region: opticalRegion)
    {
      removeOpticalRegion(region);
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment 
   */
  public void filterBoard(List<String> boardNames)
  {
    Assert.expect(boardNames != null);

    java.util.List<Board> boards = _project.getPanel().getBoards();

    for(Board board : boards)
    {
      if (boardNames.contains(board.getName()))
      {
        _boardsToFilterOn.add(board);
      }
    }
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment 
   */  
  public boolean isBoardFiltered(Board board)
  {
    Assert.expect(board != null);
    
    return _boardsToFilterOn.contains(board);
  }
  
  /**
   * @author Khaw Chek Hau
   * XCR2286: Improvement of V810 "SKIP" Function for Panel Based Alignment 
   */  
  public boolean isBoardsToFilterOnEmpty()
  {    
    return _boardsToFilterOn.isEmpty();
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public void resetIsTestSubProgramDone()
  {
    for (TestSubProgram testSubProgram : getAllInspectableTestSubPrograms())
    {
      testSubProgram.setIsTestSubProgramDone(false);
    }
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public void setManual2DAlignmentInProgress(boolean isManual2DAlignmentInProgress)
  {
    _isManual2DAlignmentInProgress = isManual2DAlignmentInProgress;
  }
  
  /**
   * XCR2285: 2D Alignment on v810
   * @author Khaw Chek Hau
   */
  public boolean isManual2DAlignmentInProgress()
  {    
    return _isManual2DAlignmentInProgress;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3761 : Skip generating 2D verification image if the panel is not unloaded
   */
  public void setPanelRemainsWithoutUnloadedFor2DVerificationImage(boolean isPanelRemainsWithoutUnloadedFor2DVerificationImage)
  {
    _isPanelRemainsWithoutUnloadedFor2DVerificationImage = isPanelRemainsWithoutUnloadedFor2DVerificationImage;
  }
  
  /**
   * @author Khaw Chek Hau
   * @XCR3761 : Skip generating 2D verification image if the panel is not unloaded
   */
  public boolean isPanelRemainsWithoutUnloadedFor2DVerificationImage()
  {    
    return _isPanelRemainsWithoutUnloadedFor2DVerificationImage;
  }

  /**
   * @author Kok Chun, Tan
   */
  public void setVelocityMappingParameters(Set<VelocityMappingParameters> list)
  {
    Assert.expect(list != null);
    if (_velocityMappingParametersList != null)
    {
      _velocityMappingParametersList.clear();
      _velocityMappingParametersList.addAll(list);
    }
    else
    {
      _velocityMappingParametersList = list;
    }
  }
  
  /**
   * @author Kok Chun, Tan
   */
  public Set<VelocityMappingParameters> getVelocityMappingParameters()
  {
    return _velocityMappingParametersList;
  }
}
