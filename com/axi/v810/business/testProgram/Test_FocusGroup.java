package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
class Test_FocusGroup extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_FocusGroup());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    FocusGroup focusGroup = new FocusGroup();
    try
    {
      focusGroup.addSlice(null);
          Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    Expect.expect(focusGroup.getNumberOfSlices() == 0);
    Slice slice = new Slice();
    focusGroup.addSlice(slice);
    Expect.expect(focusGroup.getNumberOfSlices() == 1);
    Expect.expect(focusGroup.getSlices().get(0) == slice);

    focusGroup.setSingleSidedRegionOfBoard(true);
    Expect.expect(focusGroup.isSingleSidedRegionOfBoard() == true);
    focusGroup.setSingleSidedRegionOfBoard(false);
    Expect.expect(focusGroup.isSingleSidedRegionOfBoard() == false);

    Slice sliceA = new Slice();
    sliceA.setCreateSlice(true);
    Slice sliceB = new Slice();
    sliceB.setCreateSlice(true);
    Slice sliceC = new Slice();
    sliceC.setCreateSlice(false);

    FocusGroup focusGroupUseDuringProduction1 = new FocusGroup();
    focusGroupUseDuringProduction1.addSlice(sliceA);
    focusGroupUseDuringProduction1.addSlice(sliceB);
    focusGroupUseDuringProduction1.addSlice(sliceC);
    Expect.expect(focusGroupUseDuringProduction1.hasSlicesToCreate() == true);

    FocusGroup focusGroupUseDuringProduction2 = new FocusGroup();
    focusGroupUseDuringProduction2.addSlice(sliceA);
    focusGroupUseDuringProduction2.addSlice(sliceB);
    Expect.expect(focusGroupUseDuringProduction2.hasSlicesToCreate() == true);

    FocusGroup focusGroupDontUseDuringProduction = new FocusGroup();
    focusGroupDontUseDuringProduction.addSlice(sliceC);
    Expect.expect(focusGroupDontUseDuringProduction.hasSlicesToCreate() == false);

    try
    {
      focusGroup.setFocusSearchParameters(null);
          Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      focusGroup.getFocusSearchParameters();
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    FocusSearchParameters parameters = new FocusSearchParameters();
    focusGroup.setFocusSearchParameters(parameters);
    Expect.expect(parameters == focusGroup.getFocusSearchParameters());

    try
    {
      // Get the z offset for this focusGroup.  It is okay to have positive
      // (upslice) and negative (downslice) values.
      focusGroup.getRelativeZoffsetFromSurfaceInNanoMeters();
    }
    catch (AssertException assertException)
    {
      Expect.expect(false);
    }

    try
    {
      focusGroup.setRelativeZoffsetFromSurfaceInNanoMeters(-1);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    int offset = 10;
    focusGroup.setRelativeZoffsetFromSurfaceInNanoMeters(offset);
    Expect.expect(offset == focusGroup.getRelativeZoffsetFromSurfaceInNanoMeters());

    try
    {
      focusGroup.getNeighbors();
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }


    try
    {
      focusGroup.addNeighbor(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    ReconstructionRegion neighbor = new ReconstructionRegion();
    focusGroup.addNeighbor(neighbor);
    List<ReconstructionRegion> neighbors = focusGroup.getNeighbors();
    Expect.expect(neighbors.size() == 1);
    Expect.expect(neighbors.get(0) == neighbor);
  }
}
