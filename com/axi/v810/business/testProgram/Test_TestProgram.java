package com.axi.v810.business.testProgram;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testGen.*;
import com.axi.v810.datastore.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
public class Test_TestProgram extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_TestProgram());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {

      TestProgram testProgram = new TestProgram();

      Expect.expect(testProgram.getAllTestSubPrograms().size() == 0);
      try
      {
        testProgram.addTestSubProgram(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        new TestSubProgram(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      TestSubProgram subProgram = new TestSubProgram(testProgram);
      testProgram.addTestSubProgram(subProgram);
      List<TestSubProgram> subPrograms = testProgram.getAllTestSubPrograms();
      Expect.expect(subPrograms.size() == 1);
      Expect.expect(subPrograms.get(0) == subProgram);

      ReconstructionRegion reconstructionRegion = new ReconstructionRegion();
      reconstructionRegion.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(0, 0, 100, 100));
      Collection<ReconstructionRegion> reconstructionRegions = new LinkedList<ReconstructionRegion>();
      reconstructionRegions.add(reconstructionRegion);
      subProgram.setAlignmentRegions(reconstructionRegions);
      reconstructionRegions = testProgram.getAlignmentRegions();
      Expect.expect(reconstructionRegions.size() == 1);
      Expect.expect(reconstructionRegions.contains(reconstructionRegion));

//      subProgram.setInspectionRegions(reconstructionRegions);
//      testProgram.addFilter(false); // get all the regions, not just the inspected ones
//      reconstructionRegions = testProgram.getAllInspectionRegions();
//      Expect.expect(reconstructionRegions.size() == 1);
//      Expect.expect(reconstructionRegions.contains(reconstructionRegion));

      subProgram.setVerificationRegions(reconstructionRegions);
      reconstructionRegions = testProgram.getVerificationRegions();
      Expect.expect(reconstructionRegions.size() == 1);
      Expect.expect(reconstructionRegions.contains(reconstructionRegion));

      try
      {
        testProgram.getJointInspectionData(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Board nullBoard = null;
      try
      {
        testProgram.addFilter(nullBoard);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Component nullComponent = null;
      try
      {
        testProgram.addFilter(nullComponent);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Subtype nullSubtype = null;
      try
      {
        testProgram.addFilter(nullSubtype);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      String panelName = "Test_Panel_1";
      try
      {
        String serializedProjectPath = FileName.getProjectSerializedFullPath(panelName);
        if (FileUtil.exists(serializedProjectPath))
          FileUtil.delete(serializedProjectPath);
      }
      catch (CouldNotDeleteFileException ex)
      {
        ex.printStackTrace();
        Expect.expect(false);
      }
      List<LocalizedString> warnings = new ArrayList<LocalizedString>();
      Project project = null;
      try
      {
        project = Project.importProjectFromNdfs(panelName, warnings);
      }
      catch (XrayTesterException xte)
      {
        Expect.expect(false);
        xte.printStackTrace();
      }
      Expect.expect(project != null);
      Expect.expect(warnings.size() == 0);

      project.getPanel().getPanelSettings().setRightManualAlignmentTransform(AffineTransform.getScaleInstance(1.0, 1.0));

      ProgramGeneration programGeneration = ProgramGeneration.getInstance();
      testProgram = programGeneration.generateTestProgram(project);
      subPrograms = testProgram.getAllTestSubPrograms();
      Expect.expect(subPrograms.size() == 1);
      Expect.expect(subPrograms.get(0).getPanelLocationInSystem().equals(PanelLocationInSystemEnum.RIGHT));
      int totalInspectionRegions = testProgram.getAllInspectionRegions().size();
      for (Board board : project.getPanel().getBoards())
      {
        // test the board filter
        testProgram.clearFilters();
        testProgram.addFilter(board);
        for (ReconstructionRegion region : testProgram.getFilteredInspectionRegions())
          Expect.expect(region.getBoard() == board);
        for (Component component : board.getComponents())
        {
          // test the coponent filter
          testProgram.clearFilters();
          testProgram.addFilter(component);
          for (ReconstructionRegion region : testProgram.getFilteredInspectionRegions())
            Expect.expect(region.getComponent() == component);
          for (Pad pad : component.getPads())
          {
            // test the pad filter
            testProgram.clearFilters();
            testProgram.addFilter(pad);
            for (ReconstructionRegion region : testProgram.getFilteredInspectionRegions())
              Expect.expect(region.isPadInRegion(pad));
          }
        }
      }

      // now lets test the subtype filter
      for (Subtype subtype : project.getPanel().getSubtypes())
      {
        testProgram.clearFilters();
        testProgram.addFilter(subtype);
        for (ReconstructionRegion region : testProgram.getFilteredInspectionRegions())
          Expect.expect(region.hasSubtype(subtype));
      }

      // ok, now lets try a bogus board name
      Panel panel = new Panel();
      panel.disableChecksForNdfParsers();
      testProgram.clearFilters();
      Board bogusBoard = new Board();
      BoardType boardType = new BoardType();
      boardType.setPanel(panel);
      boardType.addBoard(bogusBoard);
      bogusBoard.setName("bogusboard");
      testProgram.addFilter(bogusBoard);
      Expect.expect(testProgram.getFilteredInspectionRegions().size() == 0);

      // try a bogus component name with a valid board.
      // we need to use a valid board, otherwise the
      // board filter will catch it and we won't be really testing
      // the component filter.
      Board firstValidBoard = project.getPanel().getBoards().get(0);
      SideBoard bogusSideBoard = new SideBoard();
      bogusSideBoard.setBoard(firstValidBoard);
      Component bogusComponent = new Component();
      ComponentType bogusComponentType = new ComponentType();
      bogusComponentType.setPanel(panel);
      bogusComponentType.setReferenceDesignator("boguscomponent");
      bogusComponent.setComponentType(bogusComponentType);
      bogusComponent.setSideBoard(bogusSideBoard);
      testProgram.clearFilters();
      testProgram.addFilter(bogusComponent);
      Expect.expect(testProgram.getFilteredInspectionRegions().size() == 0);

      // try a bogus pad name
      LandPattern landPattern = new LandPattern();
      landPattern.setPanel(panel);
      Pad bogusPad = new Pad();
      PadType bogusPadType = new PadType();
      LandPatternPad bogusLandPatternPad = new SurfaceMountLandPatternPad();
      bogusLandPatternPad.setLandPattern(landPattern);
      bogusLandPatternPad.setName("boguspad");
      bogusPadType.setLandPatternPad(bogusLandPatternPad);
      bogusPad.setPadType(bogusPadType);
      Component firstValidComponent = firstValidBoard.getComponents().get(0);
      firstValidComponent.addPad(bogusPad);
      testProgram.clearFilters();
      testProgram.addFilter(bogusPad);
      Expect.expect(testProgram.getFilteredInspectionRegions().size() == 0);

      // test that clear filters actually works
      testProgram.clearFilters();
      Expect.expect(testProgram.getAllInspectionRegions().size() == totalInspectionRegions);

      //      String projectName = "73-9375-03_Modified_Learned_grommet";
//      Project project = Project.load(projectName);
//      System.out.println("Start profiling! Please press enter:");
//      System.in.read();
//      testProgram = project.getTestProgram();
//      testProgram.setDebugCheckSumTiming(true);
//      long previousTestProgramCheckSum = testProgram.getCheckSum();
//      testProgram.invalidateCheckSum();
//      previousTestProgramCheckSum = testProgram.getCheckSum();

      String projectName = "0027parts(1).2";
      project = Project.load(projectName, new BooleanRef());
      testProgram = project.getTestProgram();
      testProgram.setDebugCheckSumTiming(false);
      //Khaw Chek Hau - XCR2183: Standardize all print out when developer debug is true
      if (Config.getInstance().getBooleanValue(DeveloperDebugConfigEnum.CHECKSUM_TIMING_DEBUG))
        testProgram.setDebugCheckSumTiming(true); 
      long previousTestProgramCheckSum = testProgram.getCheckSum();

      // test that the TestProgram CheckSum is changes after the region of Shadding Compensation had changes
      for (ReconstructionRegion region : testProgram.getAllInspectionRegions())
      {
        if (region.requiresShadingCompensation() == false)
        {
          for (Pad pad : region.getPads())
          {
            if (pad.getPadTypeSettings().getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED) == false)
            {
              pad.getPadTypeSettings().setArtifactCompensationState(ArtifactCompensationStateEnum.COMPENSATED);
              break;
            }
          }
          break;
        }
      }
      // @todo comment
      Expect.expect(testProgram.equals(project.getTestProgram()));
      testProgram = project.getTestProgram();
      long currentTestProgramCheckSum = testProgram.getCheckSum();
      Expect.expect(previousTestProgramCheckSum != currentTestProgramCheckSum);

      // test that the TestProgram had re-generate after the region of Signal Compensation had changes
      previousTestProgramCheckSum = currentTestProgramCheckSum;
      testProgram = project.getTestProgram();
      for (ReconstructionRegion region : testProgram.getAllInspectionRegions())
      {
        if (region.requiresShadingCompensation() == false &&
            region.getSignalCompensation().equals(SignalCompensationEnum.DEFAULT_LOW))
        {
          region.getPads().iterator().next().getPadTypeSettings().setSignalCompensation(SignalCompensationEnum.MEDIUM);
          break;
        }
      }
      Expect.expect(testProgram.equals(project.getTestProgram()) == false);
      testProgram = project.getTestProgram();
      currentTestProgramCheckSum = testProgram.getCheckSum();
      Expect.expect(previousTestProgramCheckSum != currentTestProgramCheckSum);

      // test that the TestProgram CheckSum is remain the same after some pads in the reconstruction region of require Shadding Compensation change to Compensated
      previousTestProgramCheckSum = currentTestProgramCheckSum;
      for (ReconstructionRegion region : testProgram.getAllInspectionRegions())
      {
        if (region.requiresShadingCompensation())
        {
          for(Pad pad : region.getPads())
          {
            if(pad.getPadTypeSettings().getEffectiveArtifactCompensationState().equals(ArtifactCompensationStateEnum.COMPENSATED) == false)
            {
              pad.getPadTypeSettings().setArtifactCompensationState(ArtifactCompensationStateEnum.COMPENSATED);
              break;
            }
          }
          break;
        }
      }
      Expect.expect(testProgram.equals(project.getTestProgram()));
      testProgram = project.getTestProgram();
      currentTestProgramCheckSum = testProgram.getCheckSum();
      Expect.expect(previousTestProgramCheckSum == currentTestProgramCheckSum);

      previousTestProgramCheckSum = currentTestProgramCheckSum;
      // test that the TestProgram CheckSum is remain the same after some pads in the region of Signal Compensation changed
      for (ReconstructionRegion region : testProgram.getAllInspectionRegions())
      {
        if (region.getSignalCompensation().equals(SignalCompensationEnum.MEDIUM))
        {
          for(Pad pad : region.getPads())
          {
            if(pad.getPadTypeSettings().getSignalCompensation().equals(SignalCompensationEnum.MEDIUM) == false)
            {
              pad.getPadTypeSettings().setSignalCompensation(SignalCompensationEnum.MEDIUM);
              break;
            }
          }
          break;
        }
      }
      Expect.expect(testProgram.equals(project.getTestProgram()) == false);
      testProgram = project.getTestProgram();
      currentTestProgramCheckSum = testProgram.getCheckSum();
      Expect.expect(previousTestProgramCheckSum == currentTestProgramCheckSum);
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
