package com.axi.v810.business.testProgram;

import java.io.*;
import java.util.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
class Test_Slice extends UnitTest
{
  /**
   * @author George A. David
   */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_Slice());
  }

  /**
   * @author George A. David
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    Slice slice = new Slice();

    try
    {
      slice.setFocusInstruction(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      slice.getFocusInstruction();
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    FocusInstruction focusInstruction = new FocusInstruction();
    slice.setFocusInstruction(focusInstruction);
    Expect.expect(focusInstruction == slice.getFocusInstruction());

    try
    {
      slice.getReconstructionMethod();
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      slice.setReconstructionMethod(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    slice.setReconstructionMethod(ReconstructionMethodEnum.AVERAGE);
    Expect.expect(slice.getReconstructionMethod().equals(ReconstructionMethodEnum.AVERAGE));

    try
    {
      slice.setReconstructionZheightInNanoMeters(-1);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    int zHeight = 10;
    slice.setReconstructionZheightInNanoMeters(zHeight);
    Expect.expect(slice.getReconstructionZheightInNanoMeters() == zHeight);

    slice.setCreateSlice(true);
    Expect.expect(slice.createSlice() == true);
    slice.setCreateSlice(false);
    Expect.expect(slice.createSlice() == false);

    try
    {
      slice.setSliceType(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    slice.setSliceType(SliceTypeEnum.RECONSTRUCTION);
    Expect.expect(slice.getSliceType().equals(SliceTypeEnum.RECONSTRUCTION));
    slice.setSliceType(SliceTypeEnum.SURFACE);
    Expect.expect(slice.getSliceType().equals(SliceTypeEnum.SURFACE));
    slice.setSliceType(SliceTypeEnum.PROJECTION);
    Expect.expect(slice.getSliceType().equals(SliceTypeEnum.PROJECTION));
  }
}
