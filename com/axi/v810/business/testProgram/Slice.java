package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;

/**
 * @author George A. David
 */
public class Slice implements Serializable
{
  private SliceNameEnum _sliceName;
  private boolean _createSlice = true;
  private FocusInstruction _focusInstruction;
  private ReconstructionMethodEnum _reconstructionMethod;
  private int _reconstructionZheightInNanoMeters = 0;
  private SliceTypeEnum _sliceTypeEnum = SliceTypeEnum.RECONSTRUCTION;
  private ReconstructionRegion _assosiatedReconstructionRegion;
  private boolean _isUsingCustomFocus = false;
  private int _defaultFocusInstructionOffsetInNanoMeters = 0;
  private boolean _useToBuildGlobalSurfaceModel = false;

  private Slice _originalSliceForAdditionalSlice;

  /**
   * @author George A. David
   */
  public void setSliceName(SliceNameEnum sliceName)
  {
    Assert.expect(sliceName != null);

    _sliceName = sliceName;
  }

  /**
   * @author George A. David
   */
  public SliceNameEnum getSliceName()
  {
    Assert.expect(_sliceName != null);

    return _sliceName;
  }

  /**
   * @author Dave Ferguson
   */
  public void setSliceType(SliceTypeEnum sliceTypeEnum)
  {
    Assert.expect(sliceTypeEnum != null);
    _sliceTypeEnum = sliceTypeEnum;
  }

  /**
   * @author Dave Ferguson
   */
  public SliceTypeEnum getSliceType()
  {
    return _sliceTypeEnum;
  }

  /**
   * @author Dave Ferguson
   */
  public void setCreateSlice(boolean createSlice)
  {
    _createSlice = createSlice;
  }

  /**
   * @author Dave Ferguson
   */
  public boolean createSlice()
  {
    return _createSlice;
  }

  /**
   * @author George A. David
   */
  public void setFocusInstruction(FocusInstruction focusInstruction)
  {
    Assert.expect(focusInstruction != null);

    _focusInstruction = focusInstruction;
  }

  /**
   * @author George A. David
   */
  public FocusInstruction getFocusInstruction()
  {
    Assert.expect(_focusInstruction != null);

    return _focusInstruction;
  }

  /**
   * @author George A. David
   */
  public void setReconstructionMethod(ReconstructionMethodEnum reconstructionMethod)
  {
    Assert.expect(reconstructionMethod != null);

    _reconstructionMethod = reconstructionMethod;
  }

  /**
   * @author George A. David
   */
  public ReconstructionMethodEnum getReconstructionMethod()
  {
    Assert.expect(_reconstructionMethod != null);

    return _reconstructionMethod;
  }

  /**
   * @author George A. David
   */
  public void setReconstructionZheightInNanoMeters(int reconstructionZheightInNanoMeters)
  {
    Assert.expect(reconstructionZheightInNanoMeters >= 0);

    _reconstructionZheightInNanoMeters = reconstructionZheightInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getReconstructionZheightInNanoMeters()
  {
    Assert.expect(_reconstructionZheightInNanoMeters >= 0);

    return _reconstructionZheightInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public void setOriginalSliceForAdditionalSlice(Slice slice)
  {
    Assert.expect(_sliceName.isAdditionalSlice());
    Assert.expect(slice != null);

    _originalSliceForAdditionalSlice = slice;
  }

  /**
   * Set the slice's assosiated reconstruction region. Used by VirtualLive mode
   * @author Scott Richardson
   */
  public void setAssociatedReconstructionRegion(ReconstructionRegion reconstructionRegion)
  {
    Assert.expect(reconstructionRegion != null);

    _assosiatedReconstructionRegion = reconstructionRegion;
  }

  /**
   * @author Scott Richardson
   */
  public ReconstructionRegion getAssociatedReconstructionRegion()
  {
    Assert.expect(_assosiatedReconstructionRegion != null);

    return _assosiatedReconstructionRegion;
  }

  /**
   * @author George A. David
   */
  public Slice getOriginalSliceForAdditionalSlice()
  {
    Assert.expect(_sliceName.isAdditionalSlice());

    return _originalSliceForAdditionalSlice;
  }

  /**
   * @author George A. David
   */
  public boolean isUnfocusedRegionSliceForRetest()
  {
    return _sliceName.isUnfocusedRegionSliceNameForRetest();
  }

  /**
   * @author George A. David
   */
  public boolean isAdditionalSlice()
  {
    return _sliceName.isAdditionalSlice();
  }

  /**
   * @author George A. David
   */
  public void setIsUsingCustomFocus(boolean isUsingCustomFocus)
  {
    _isUsingCustomFocus = isUsingCustomFocus;
  }

  /**
   * @author George A. David
   */
  public boolean isUsingCustomFocus()
  {
    return _isUsingCustomFocus;
  }

  /**
   * @author George A. David
   */
  public void setDefaultFocusInstructionOffsetInNanoMeters(int defaultFocusInstructionOffsetInNanoMeters)
  {
    _defaultFocusInstructionOffsetInNanoMeters = defaultFocusInstructionOffsetInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public int getDefaultFocusInstructionOffsetInNanoMeters()
  {
    return _defaultFocusInstructionOffsetInNanoMeters;
  }
  
  /**
   * @author Cheah Lee Herng
   */
  public boolean useToBuildGlobalSurfaceModel()
  {
    return _useToBuildGlobalSurfaceModel;
  }

  /**
   * @author Cheah Lee Herng
   */
  public void setUseToBuildGlobalSurfaceModel(boolean useToBuildGlobalSurfaceModel)
  {
    _useToBuildGlobalSurfaceModel = useToBuildGlobalSurfaceModel;
  }
}
