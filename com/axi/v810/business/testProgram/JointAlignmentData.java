package com.axi.v810.business.testProgram;

import com.axi.v810.business.panelDesc.*;

/**
 * @author Matt Wharton
 */
public class JointAlignmentData extends JointInspectionData
{
  /**
   * @author Matt Wharton
   */
  public JointAlignmentData(Pad pad)
  {
    super(pad);
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public ComponentInspectionData getComponentInspectionData()
  {
    if (_componentInspectionData == null)
    {
      TestSubProgram testSubProgram = getInspectionRegion().getTestSubProgram();
      if (testSubProgram.componentAlignmentDataExists(_component))
      {
        _componentInspectionData = testSubProgram.getComponentAlignmentData(_component);
      }
      else
      {
        _componentInspectionData = new ComponentAlignmentData(_board.getPanel().getProject(), testSubProgram);
        _componentInspectionData.setComponent(_component);
        testSubProgram.addComponentToAlignmentMap(_component, (ComponentAlignmentData)_componentInspectionData);
      }
    }

    return _componentInspectionData;
  }
}
