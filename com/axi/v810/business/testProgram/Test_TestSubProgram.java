package com.axi.v810.business.testProgram;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.*;
import com.axi.v810.util.*;

/**
 * Test the TestSubProgram class
 * @author George David
 */
public class Test_TestSubProgram extends UnitTest
{
  /**
   * @author George David
   */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_TestSubProgram());
  }

  /**
   * @author George David
   */
  public void test(BufferedReader is, PrintWriter os)
  {

    try
    {
      new TestSubProgram(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    TestProgram testProgram = new TestProgram();
    TestSubProgram subProgram = new TestSubProgram(testProgram);

    try
    {
      subProgram.setId(-1);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    int id = 10;
    subProgram.setId(id);
    Expect.expect(id == subProgram.getId());

    try
    {
      subProgram.hasInspectionPad(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    Panel panel = new Panel();
    panel.disableChecksForNdfParsers();
    LandPattern landPattern = new LandPattern();
    landPattern.setPanel(panel);
    Pad badPad = new Pad();
    PadType padType = new PadType();
    LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
    landPatternPad.setLandPattern(landPattern);
    landPatternPad.setName("pad");
    padType.setLandPatternPad(landPatternPad);
    badPad.setPadType(padType);

    try
    {
      subProgram.getJointInspectionData(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      subProgram.getJointInspectionData(badPad);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    String nullPad = null;

    try
    {
      subProgram.getInspectionRegion(nullPad);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      subProgram.getInspectionRegion(badPad);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    try
    {
      subProgram.setInspectionRegions(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    ReconstructionRegion reconstructionRegion = new ReconstructionRegion();
    reconstructionRegion.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(0, 0, 100, 100));
    Collection<ReconstructionRegion> reconstructionRegions = new LinkedList<ReconstructionRegion>();
    reconstructionRegions.add(reconstructionRegion);
//    subProgram.setInspectionRegions(reconstructionRegions);
//    reconstructionRegions = subProgram.getAllInspectionRegions();
//    Expect.expect(reconstructionRegions.size() == 1);
//    Expect.expect(reconstructionRegions.contains(reconstructionRegion));
//    Collection<ReconstructionRegion> inspectionRegions = subProgram.getAllInspectionRegions();
//    Expect.expect(inspectionRegions.size() == 1);
//    Expect.expect(inspectionRegions.contains(reconstructionRegion));

    subProgram.setVerificationRegions(reconstructionRegions);
    reconstructionRegions = subProgram.getVerificationRegions();
    Expect.expect(reconstructionRegions.size() == 1);
    Expect.expect(reconstructionRegions.contains(reconstructionRegion));
    Collection<ReconstructionRegion> verificationRegions = subProgram.getVerificationRegions();
    Expect.expect(verificationRegions.size() == 1);
    Expect.expect(verificationRegions.contains(reconstructionRegion));

    subProgram.setAlignmentRegions(reconstructionRegions);
    reconstructionRegions = subProgram.getAlignmentRegions();
    Expect.expect(reconstructionRegions.size() == 1);
    Expect.expect(reconstructionRegions.contains(reconstructionRegion));
    Collection<ReconstructionRegion> alignmentRegions = subProgram.getAlignmentRegions();
    Expect.expect(alignmentRegions.size() == 1);
    Expect.expect(alignmentRegions.contains(reconstructionRegion));

    try
    {
      subProgram.getAggregateManualAlignmentTransform();
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    Project project = new Project(false);
    project.setName("Test_TestSubProgram");
    // start up the project state machine - this normally would happen on a project
    // load or import, but since we are not doing that here this will get it running
    project.getProjectState().disable();
    project.getProjectState().enable();
    PanelSettings panelSettings = new PanelSettings();
    panelSettings.setPanel(panel);
    panel.setWidthInNanoMeters(100);
    panel.setLengthInNanoMeters(1000);
    panel.setThicknessInNanoMeters(1000);
    panelSettings.setDegreesRotationRelativeToCad(90);
    panel.setPanelSettings(panelSettings);
    project.setPanel(panel);
    testProgram.setProject(project);
    java.awt.geom.AffineTransform matrix = new java.awt.geom.AffineTransform();
    panelSettings.setRightManualAlignmentTransform(matrix);
    project.getTestProgram();
    Expect.expect(matrix == subProgram.getCoreManualAlignmentTransform());

    try
    {
      subProgram.setPanelLocationInSystem(null);
      Expect.expect(false);
    }
    catch (AssertException assertException)
    {
      //do nothing
    }

    subProgram.setPanelLocationInSystem(PanelLocationInSystemEnum.LEFT);
    Expect.expect(subProgram.getPanelLocationInSystem().equals(PanelLocationInSystemEnum.LEFT));

    String panelName = "jtr_sledge_div";
    try
    {
      String serializedProjectPath = FileName.getProjectSerializedFullPath(panelName);
      if(FileUtil.exists(serializedProjectPath))
        FileUtil.delete(serializedProjectPath);
    }
    catch (CouldNotDeleteFileException ex)
    {
      ex.printStackTrace();
      Expect.expect(false);
    }
    List<LocalizedString> warnings = new ArrayList<LocalizedString>();
    project = null;
    try
    {
      project = Project.importProjectFromNdfs(panelName, warnings);
    }
    catch (XrayTesterException xte)
    {
      Expect.expect(false);
      xte.printStackTrace();
    }
    Expect.expect(project != null);

    project.getPanel().getPanelSettings().setLeftManualAlignmentTransform(
      AffineTransform.getScaleInstance(1.0, 1.0));
    project.getPanel().getPanelSettings().setRightManualAlignmentTransform(
      AffineTransform.getScaleInstance(1.0, 1.0));
    testProgram = project.getTestProgram();

    panel = project.getPanel();
    panelSettings = panel.getPanelSettings();
    PanelRectangle rightSectionOfPanel = panelSettings.getRightSectionOfLongPanel();
    for(Pad pad : project.getPanel().getPads())
    {
      if(pad.isInspected() && ReconstructionRegion.fitsInInspectionRegion(pad, project.isEnlargeReconstructionRegion()))
      {
        for (TestSubProgram testSubProgram : testProgram.getAllTestSubPrograms())
        {
          if (testSubProgram.hasInspectionPad(pad))
          {
            testSubProgram.getJointInspectionData(pad);
            testSubProgram.getInspectionRegion(pad);
          }
          else
          {
            try
            {
              testSubProgram.getJointInspectionData(pad);
              Expect.expect(false);
            }
            catch (AssertException assertException)
            {
              //do nothing
            }
            try
            {
              testSubProgram.getInspectionRegion(pad);
              Expect.expect(false);
            }
            catch (AssertException assertException)
            {
              //do nothing
            }
          }
        }
      }
    }
  }
}



