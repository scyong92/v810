package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.datastore.config.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;


/**
* This class holds information related to focus regions.  For example, an
* PanelRectangle (x, y, width, and height) and an int rotational factor about
* the focus region's center coordinate.
*
* @author Michael Tutkowski
*/
public class FocusRegion implements Serializable
{
  private static final int _MAX_FOCUS_REGION_SIZE_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(322);
  private static final int _MAX_HIGH_MAG_FOCUS_REGION_SIZE_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(210); // MathUtil.convertMilsToNanoMetersInteger(180); (10um)
  private static final int _MIN_FOCUS_REGION_SIZE_NANOMETERS = MathUtil.convertMilsToNanoMetersInteger(5);
  private static Config _config = Config.getInstance();
  private static int _nextFocusRegionId = 0;
  private int _id = -1;

  private PanelRectangle _rectRelativeToPanelInNanometers;

  /**
   * @author George A. David
   */
  public FocusRegion()
  {
    _id = _nextFocusRegionId++;
  }

  /**
   * @author George A. David
   */
  public int getId()
  {
    Assert.expect(_id >= 0);

    return _id;
  }

  /**
   * @author George A. David
   */
  public void setRectangleRelativeToPanelInNanoMeters(PanelRectangle rectangleRelativeToPanelInNanoMeters)
  {
    Assert.expect(rectangleRelativeToPanelInNanoMeters != null);
//    Assert.expect(rectangleRelativeToPanelInNanoMeters.getMinX() >= 0);
//    Assert.expect(rectangleRelativeToPanelInNanoMeters.getMinY() >= 0);
    Assert.expect(rectangleRelativeToPanelInNanoMeters.getWidth() > 0);
    Assert.expect(rectangleRelativeToPanelInNanoMeters.getHeight() > 0);

    _rectRelativeToPanelInNanometers = rectangleRelativeToPanelInNanoMeters;
  }

  /**
   * @author Michael Tutkowski
   */
  public PanelRectangle getRectangleRelativeToPanelInNanometers()
  {
    Assert.expect(_rectRelativeToPanelInNanometers != null);

    return _rectRelativeToPanelInNanometers;
  }

  /**
   * @author George A. David
   */
  public static int getFocusRegionGranularityInNanoMeters()
  {
    return _config.getIntValue(SoftwareConfigEnum.FOCUS_REGION_GRANULARITY_IN_PIXELS) * MagnificationEnum.NOMINAL.getNanoMetersPerPixel();
  }

  /**
   * @author George A. David
   */
  public static int getMaxFocusRegionSizeInNanoMeters()
  {
    Assert.expect(_MAX_FOCUS_REGION_SIZE_NANOMETERS > 0);

    return _MAX_FOCUS_REGION_SIZE_NANOMETERS;
  }

  /**
   * @author George A. David
   */
  public static int getMinFocusRegionSizeInNanoMeters()
  {
    Assert.expect(_MIN_FOCUS_REGION_SIZE_NANOMETERS > 0);

    return _MIN_FOCUS_REGION_SIZE_NANOMETERS;
  }

  /**
   * @author Roy Williams
   */
  public static int getMaxFocusRegionWidthInNanoMeters()
  {
    return _MAX_FOCUS_REGION_SIZE_NANOMETERS;
  }

  /**
   * @author Roy Williams
   */
  public static int getMaxFocusRegionLengthInNanoMeters()
  {
    return _MAX_FOCUS_REGION_SIZE_NANOMETERS;
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxFocusRegionSizeInNanoMeters(MagnificationTypeEnum magnificationType)
  {
    if(magnificationType.equals(MagnificationTypeEnum.LOW))
    {
      return getMaxFocusRegionSizeInNanoMeters();
    }
    else
    {
      Assert.expect(_MAX_HIGH_MAG_FOCUS_REGION_SIZE_NANOMETERS > 0);

      return _MAX_HIGH_MAG_FOCUS_REGION_SIZE_NANOMETERS;
    }
  }

  /**
   * @author Wei Chin
   */
  public static int getMaxFocusRegionWidthInNanoMeters(MagnificationTypeEnum magnificationType)
  {
    if(magnificationType.equals(MagnificationTypeEnum.LOW))
    {
      return getMaxFocusRegionSizeInNanoMeters();
    }
    else
    {
      Assert.expect(_MAX_HIGH_MAG_FOCUS_REGION_SIZE_NANOMETERS > 0);

      return _MAX_HIGH_MAG_FOCUS_REGION_SIZE_NANOMETERS;
    }
  }


  /**
   * @author Wei Chin
   */
  public static int getMaxFocusRegionLengthInNanoMetersForHighMag(MagnificationTypeEnum magnificationType)
  {
    if(magnificationType.equals(MagnificationTypeEnum.LOW))
    {
      return getMaxFocusRegionSizeInNanoMeters();
    }
    else
    {
      Assert.expect(_MAX_HIGH_MAG_FOCUS_REGION_SIZE_NANOMETERS > 0);

      return _MAX_HIGH_MAG_FOCUS_REGION_SIZE_NANOMETERS;
    }
  }
}
