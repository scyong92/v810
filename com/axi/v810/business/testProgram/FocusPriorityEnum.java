package com.axi.v810.business.testProgram;

import java.io.*;

import com.axi.util.*;

/**
 * Enumerates focus priorities.
 *
 * @author George A. David
 */
public class FocusPriorityEnum extends com.axi.util.Enum implements Serializable
{
  private static int _index = -1;
  public static final FocusPriorityEnum FIRST = new FocusPriorityEnum(++_index);
  public static final FocusPriorityEnum SECOND = new FocusPriorityEnum(++_index);
  public static final FocusPriorityEnum THIRD = new FocusPriorityEnum(++_index);
  public static final FocusPriorityEnum FOURTH = new FocusPriorityEnum(++_index);
  public static final FocusPriorityEnum FIFTH = new FocusPriorityEnum(++_index);
  public static final FocusPriorityEnum SIXTH = new FocusPriorityEnum(++_index);
  public static final FocusPriorityEnum SEVENTH = new FocusPriorityEnum(++_index);


  /**
   * @author George A. David
   */
  private FocusPriorityEnum(int id)
  {
    super(id);
  }
}

