package com.axi.v810.business.testProgram;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.imageAcquisition.imageReconstruction.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * @author Peter Esbensen
 */
class Test_ReconstructionRegion extends UnitTest
{
  /**
   * @author Peter Esbensen
   */
  public static void main(String[] args)
  {
    UnitTest.execute (new Test_ReconstructionRegion());
  }

  /**
   * @author Peter Esbensen
   */
  public void test(BufferedReader is, PrintWriter os)
  {
    try
    {
      ReconstructionRegion region = new ReconstructionRegion();
      region.setReconstructionRegionTypeEnum(ReconstructionRegionTypeEnum.INSPECTION);

      try
      {
        region.isTopSide();
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing, we expect an assertion failure here!
      }
      region.setTopSide(false);
      Expect.expect(region.isTopSide() == false);

      Expect.expect(region.getScanPasses().size() == 0);
      ScanPass scanPass0 = new ScanPass(0, new StagePositionMutable(), new StagePositionMutable());
      ScanPass scanPass1 = new ScanPass(1, new StagePositionMutable(), new StagePositionMutable());
      ScanPass scanPass2 = new ScanPass(2, new StagePositionMutable(), new StagePositionMutable());
      region.addScanPass(scanPass1);
      Expect.expect(region.getScanPasses().size() == 1);
      Expect.expect(! region.getScanPasses().contains(scanPass0));
      Expect.expect(region.getScanPasses().contains(scanPass1));
      Expect.expect(! region.getScanPasses().contains(scanPass2));
      region.addScanPass(scanPass2);
      Expect.expect(region.getScanPasses().size() == 2);
      Expect.expect(! region.getScanPasses().contains(scanPass0));
      Expect.expect(region.getScanPasses().contains(scanPass1));
      Expect.expect(region.getScanPasses().contains(scanPass2));

      try
      {
        region.getRegionRectangleRelativeToPanelInNanoMeters();
        Expect.expect(false);
      }
      catch (AssertException ae)
      {
        // do nothing, we expect an assertion failure here!
      }

      try
      {
        region.setRegionRectangleRelativeToPanelInNanoMeters(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      final int BOTTOM_LEFT_X = 10;
      final int BOTTOM_LEFT_Y = 20;
      final int WIDTH = 40;
      final int HEIGHT = 50;

      region.setRegionRectangleRelativeToPanelInNanoMeters(new PanelRectangle(BOTTOM_LEFT_X, BOTTOM_LEFT_Y, WIDTH,
          HEIGHT));
      Expect.expect(region.getRegionRectangleRelativeToPanelInNanoMeters().getMinX() == BOTTOM_LEFT_X);
      Expect.expect(region.getRegionRectangleRelativeToPanelInNanoMeters().getMinY() == BOTTOM_LEFT_Y);
      Expect.expect(region.getRegionRectangleRelativeToPanelInNanoMeters().getWidth() == WIDTH);
      Expect.expect(region.getRegionRectangleRelativeToPanelInNanoMeters().getHeight() == HEIGHT);

      try
      {
        region.getReconstructionEngineId();
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        region.setReconstructionEngineId(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      ImageReconstructionEngineEnum id = ImageReconstructionEngineEnum.IRE3;
      region.setReconstructionEngineId(id);
      Expect.expect(id == region.getReconstructionEngineId());

      try
      {
        region.setAlignmentGroup(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Expect.expect(region.getJointInspectionDataList().size() == 0);
      Expect.expect(region.getFeatures().size() == 0);

      try
      {
        region.addJointInspectionData(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Expect.expect(region.hasJoints() == false);

      Project dummyProject = new Project(false);
      Panel dummyPanel = new Panel();
      dummyProject.setPanel(dummyPanel);
      dummyPanel.setProject(dummyProject);
      dummyPanel.setWidthInNanoMeters(100000000);
      dummyPanel.setLengthInNanoMeters(200000000);
      dummyPanel.setThicknessInNanoMeters(100000);
      PanelSettings dummyPanelSettings = new PanelSettings();
      dummyPanelSettings.setPanel(dummyPanel);
      dummyPanelSettings.setDegreesRotationRelativeToCad(0);
      dummyPanel.setPanelSettings(dummyPanelSettings);

      dummyPanelSettings.setRightManualAlignmentTransform(new AffineTransform());
      dummyPanel.setPanelSettings(dummyPanelSettings);
      region.setAlignmentGroup(new AlignmentGroup(dummyPanelSettings));

      TestProgram testProgram = new TestProgram();
      testProgram.setProject(dummyProject);
      TestSubProgram testSubProgram = new TestSubProgram(testProgram);
      Collection<ReconstructionRegion> inspectionRegions = new HashSet<ReconstructionRegion>();
      inspectionRegions.add(region);
      testSubProgram.setInspectionRegions(inspectionRegions);
      dummyPanelSettings.setRightManualAlignmentTransform(AffineTransform.getScaleInstance(1.1, 1.1)); // scale by 110%

      Panel panel = new Panel();
      PanelSettings panelSettings = new PanelSettings();
      panel.setPanelSettings(panelSettings);
      panelSettings.setPanel(panel);
      panel.setWidthInNanoMeters(10);
      panel.setLengthInNanoMeters(10);
      panelSettings.setDegreesRotationRelativeToCad(0);
      panel.disableChecksForNdfParsers();
      Pad pad = new Pad();
      PadType padType = new PadType();
      LandPattern landPattern = new LandPattern();
      String landPatternName = "landPatternName";
      landPattern.setPanel(panel);
      landPattern.setName(landPatternName);
      LandPatternPad landPatternPad = new SurfaceMountLandPatternPad();
      landPatternPad.setLandPattern(landPattern);
      PackagePin packagePin = new PackagePin();
      String padName = "padName";
      landPatternPad.setName(padName);
      padType.setLandPatternPad(landPatternPad);
      pad.setPadType(padType);
      padType.setPackagePin(packagePin);

      landPatternPad.setLandPattern(landPattern);
      ComponentCoordinate coordinate = new ComponentCoordinate(0, 0);
      landPatternPad.setCoordinateInNanoMeters(coordinate);
      landPatternPad.setWidthInNanoMeters(100000);
      landPatternPad.setLengthInNanoMeters(100000);
      landPatternPad.setShapeEnum(ShapeEnum.RECTANGLE);
      landPatternPad.setDegreesRotation(0);
      packagePin.setLandPatternPad(landPatternPad);
      Component component = new Component();
      pad.setComponent(component);
      ComponentType componentType = new ComponentType();
      componentType.setPanel(panel);
      SideBoardType sideBoardType = new SideBoardType();
      componentType.setSideBoardType(sideBoardType);
      String componentName = "refdes";
      componentType.setReferenceDesignator(componentName);
      component.setComponentType(componentType);
      componentType.setCoordinateInNanoMeters(new BoardCoordinate(0, 0));
      componentType.setDegreesRotationRelativeToBoard(0);
      SideBoard sideBoard = new SideBoard();
      component.setSideBoard(sideBoard);
      Board board = new Board();
      board.setSideBoard1(sideBoard);
      sideBoard.setBoard(board);
      board.setCoordinateInNanoMeters(new PanelCoordinate(0, 0));
      BoardType boardType = new BoardType();
      boardType.setPanel(panel);
      boardType.setSideBoardType1(sideBoardType);
      sideBoardType.setBoardType(boardType);
      boardType.setWidthInNanoMeters(1000000);
      boardType.setLengthInNanoMeters(1000000);
      board.setBoardType(boardType);
      board.setDegreesRotationRelativeToPanelFromNdfReaders(0);
      String boardName = "boardName";
      board.setName(boardName);
      Subtype subtype = new Subtype();
      subtype.setPanel(panel);
      String subtypeName = "subtypeId";
      subtype.setInspectionFamilyEnum(InspectionFamilyEnum.CHIP);
      subtype.setShortName(subtypeName);
      InspectionFamily inspectionFamily = InspectionFamily.getInstance(InspectionFamilyEnum.GRID_ARRAY);
      packagePin.setJointTypeEnum(JointTypeEnum.COLLAPSABLE_BGA);
      subtype.setInspectionFamilyEnum(inspectionFamily.getInspectionFamilyEnum());


      PadTypeSettings padTypeSettings = new PadTypeSettings();
      padType.setPadTypeSettings(padTypeSettings);
      padTypeSettings.setPadType(padType);
      padType.setComponentType(componentType);
      componentType.setComponentTypeSettings(new ComponentTypeSettings());
      padTypeSettings.setSubtype(subtype);

      JointInspectionData jointInspectionData = new JointInspectionData(pad);
      region.addJointInspectionData(jointInspectionData);
      List<JointInspectionData> inspectionDataList = region.getJointInspectionDataList();
      Expect.expect(inspectionDataList.size() == 1);
      Expect.expect(inspectionDataList.get(0) == jointInspectionData);
      List<Feature> features = region.getFeatures();
      Expect.expect(features.size() == 1);
      Expect.expect(features.get(0) == jointInspectionData);

      ReconstructionRegion reconRegion = (ReconstructionRegion)region;
      features = reconRegion.getFeatures();
      Expect.expect(features != null);
      Iterator it = features.iterator();
      Feature feature = (Feature)it.next();
      Expect.expect(feature != null);

      Collection<JointInspectionData> jointInspectionDataList = region.getJointInspectionDataList();
      Expect.expect(jointInspectionDataList != null);
      it = jointInspectionDataList.iterator();
      jointInspectionData = (JointInspectionData)it.next();
      Expect.expect(jointInspectionData != null);

      Expect.expect(region.hasJoints());

      try
      {
        region.removeJointInspectionData(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      JointInspectionData anotherJointInspectionData = new JointInspectionData(pad);
      try
      {
        region.removeJointInspectionData(anotherJointInspectionData);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      region.addJointInspectionData(anotherJointInspectionData);
      features = region.getFeatures();
      Expect.expect(features.size() == 2);
      inspectionDataList = region.getJointInspectionDataList();
      Expect.expect(inspectionDataList.size() == 2);

      List<Feature> featuresToRemove = new LinkedList<Feature>();
      featuresToRemove.add(anotherJointInspectionData);

      try
      {
        region.moveFeaturesToNewReconstructionRegion(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      ReconstructionRegion anotherRegion = (ReconstructionRegion)region.moveFeaturesToNewReconstructionRegion(
          featuresToRemove);

      features = region.getFeatures();
      Expect.expect(features.size() == 1);
      Expect.expect(features.get(0) == jointInspectionData);
      inspectionDataList = region.getJointInspectionDataList();
      Expect.expect(inspectionDataList.size() == 1);
      Expect.expect(inspectionDataList.get(0) == jointInspectionData);

      features = anotherRegion.getFeatures();
      Expect.expect(features.size() == 1);
      Expect.expect(features.get(0) == anotherJointInspectionData);
      inspectionDataList = anotherRegion.getJointInspectionDataList();
      Expect.expect(inspectionDataList.size() == 1);
      Expect.expect(inspectionDataList.get(0) == anotherJointInspectionData);

      anotherRegion.removeJointInspectionData(anotherJointInspectionData);
      features = anotherRegion.getFeatures();
      Expect.expect(features.size() == 0);
      inspectionDataList = anotherRegion.getJointInspectionDataList();
      Expect.expect(inspectionDataList.size() == 0);
      Expect.expect(region.getBoard() == board);

      Expect.expect(region.getComponent() == component);

      try
      {
        region.isPadInRegion(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      Expect.expect(region.isPadInRegion(pad));
//    String anotherBoardName = "anotherboard";
//    String anotherComponentName = "anotherComponent";
//    Expect.expect(region.isPadInRegion(anotherBoardName, componentName, padName) == false);
//    Expect.expect(region.isPadInRegion(boardName, anotherComponentName, padName) == false);
//    String anotherPadName = "anotherPadName";
//    Expect.expect(region.isPadInRegion(boardName, componentName, anotherPadName) == false);
      Expect.expect(region.hasSubtype(subtype));
      Expect.expect(region.getInspectionFamilyEnum().equals(inspectionFamily.getInspectionFamilyEnum()));
      String anotherFamilyName = "anotherFamilyName";

      Expect.expect(region.getFocusGroups().size() == 0);

      try
      {
        region.addFocusGroup(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      try
      {
        region.setFocusGroups(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      FocusGroup focusGroup = new FocusGroup();
      region.addFocusGroup(focusGroup);
      List<FocusGroup> focusGroups = region.getFocusGroups();
      Expect.expect(focusGroups.size() == 1);
      Expect.expect(focusGroups.get(0) == focusGroup);
      focusGroups = new LinkedList<FocusGroup>();
      region.setFocusGroups(focusGroups);
      Expect.expect(region.getFocusGroups().size() == 0);

      try
      {
        region.setFocusPriority(null);
        Expect.expect(false);
      }
      catch (AssertException assertException)
      {
        //do nothing
      }

      region.setFocusPriority(FocusPriorityEnum.THIRD);
      Expect.expect(region.getFocusPriority().equals(FocusPriorityEnum.THIRD));

    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}
