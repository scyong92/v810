package com.axi.v810.business.testProgram;

import java.awt.geom.*;
import java.io.*;
import java.util.*;

import com.axi.util.*;
import com.axi.util.image.*;
import com.axi.v810.business.imageAnalysis.*;
import com.axi.v810.business.imageAnalysis.sharedAlgorithms.*;
import com.axi.v810.business.panelDesc.*;
import com.axi.v810.business.panelSettings.*;
import com.axi.v810.business.testResults.*;
import com.axi.v810.hardware.*;
import com.axi.v810.util.*;

/**
 * Represents a region-of-interest for a single joint.  See the Program Generation
 * subsystem for the details on how these regions are selected.
 *
 * @author Peter Esbensen
 */
public class JointInspectionData implements Feature, Serializable
{
  private PanelRectangle _regionRectangleRelativeToPanelOriginInNanoMeters;

  protected Board _board;
  protected Component _component;
  private String _boardNameAndReferenceDesignator;
  private Pad _pad;
  private String _fullyQualifiedPadName;
  private Subtype _subtype;
  private JointTypeEnum _jointType;
  protected ComponentInspectionData _componentInspectionData;
  private ReconstructionRegion _reconstructionRegion;

  private PanelRectangle _padBoundsRelativeToPanelInNanoMeters;

  private RegionShapeEnum _regionShapeEnum;

  private Map<MagnificationEnum, Integer> _magnificationEnumToInterPadDistanceInPixelsMap =
      new HashMap<MagnificationEnum, Integer>();

  private Map<MagnificationEnum, Integer> _magnificationEnumToPitchInPixelsMap =
      new HashMap<MagnificationEnum, Integer>();

  private Map<MagnificationEnum, Integer> _magnificationEnumToAlgorithmLimitedInterPadDistanceInPixelsMap =
      new HashMap<MagnificationEnum, Integer>();

  private transient JointInspectionResult _jointInspectionResult;

  // for transient data, do not create here -- because if read in by serialization, it won't then be instantiated.
  private transient Map<JointInspectionData, Double> _jointToDistanceMap;

  private transient Collection<JointInspectionData> _neighboringJointsForOpenOutlier;
  private transient Collection<JointInspectionData> _neighboringJointsForQuadFlatNoLead;
  
  // bee-hoon.goh
  private static PanelRectangle _wholeComponentPanelRect = null;

  /**
   * @author Peter Esbensen
   */
  public JointInspectionData(Pad pad)
  {
    Assert.expect(pad != null);

    _regionRectangleRelativeToPanelOriginInNanoMeters =
        ImageAnalysis.getMaximumAreaNeededByAlgorithmsAroundPad(pad);
    _pad = pad;
    _fullyQualifiedPadName = pad.getBoardAndComponentAndPadName();
    java.awt.Shape shape = pad.getShapeRelativeToPanelInNanoMeters();
    ShapeEnum shapeEnum = pad.getShapeEnum();
    boolean roundedEnds = false;
    if (shapeEnum.equals(ShapeEnum.CIRCLE))
      _regionShapeEnum = RegionShapeEnum.OBROUND;
    else if (shapeEnum.equals(ShapeEnum.RECTANGLE))
      _regionShapeEnum = RegionShapeEnum.RECTANGULAR;
    else
      Assert.expect(false);
    _padBoundsRelativeToPanelInNanoMeters = new PanelRectangle(shape);
    _component = pad.getComponent();
    _boardNameAndReferenceDesignator = pad.getComponent().getBoardNameAndReferenceDesignator();
    _board = pad.getComponent().getBoard();
    _subtype = pad.getSubtype();
    _jointType = pad.getJointTypeEnum();
    }

  /**
   * Get the rectangle enclosing this InspectionRegionPad.  This will return a rectangle with coordinates
   * relative to the panel origin.
   * @author Peter Esbensen
   */
  public PanelRectangle getRegionRectangleRelativeToPanelOriginInNanoMeters()
  {
    Assert.expect(_regionRectangleRelativeToPanelOriginInNanoMeters != null);

    if(_reconstructionRegion.isAlignmentRegion())
      return _padBoundsRelativeToPanelInNanoMeters;
    else
      return _regionRectangleRelativeToPanelOriginInNanoMeters;
  }

  /**
   * @author George A. David
   */
  public PanelRectangle getPadBoundsRelativeToPanelInNanoMeters()
  {
    Assert.expect(_padBoundsRelativeToPanelInNanoMeters != null);

    return _padBoundsRelativeToPanelInNanoMeters;
  }

  /**
   * Get the center coordinate of the rectangle enclosing this InspectionRegionPad
   * relative to the panel origin.
   *
   * @author Michael Tutkowski
   */
  public PanelCoordinate getCenterCoordinateRelativeToPanelOriginInNanoMeters()
  {
    Assert.expect(_regionRectangleRelativeToPanelOriginInNanoMeters != null);

    int iXCenterCoordinate = ((_regionRectangleRelativeToPanelOriginInNanoMeters.getMaxX() -
                               _regionRectangleRelativeToPanelOriginInNanoMeters.getMinX()) / 2) +
                              _regionRectangleRelativeToPanelOriginInNanoMeters.getMinX();
    int iYCenterCoordinate = ((_regionRectangleRelativeToPanelOriginInNanoMeters.getMaxY() -
                               _regionRectangleRelativeToPanelOriginInNanoMeters.getMinY()) / 2) +
                              _regionRectangleRelativeToPanelOriginInNanoMeters.getMinY();

    return new PanelCoordinate(iXCenterCoordinate, iYCenterCoordinate);
  }

  /**
   * @author Peter Esbensen
   */
  void setRegionRectangleRelativeToPanelInNanoMeters(PanelRectangle regionRectangleRelativeToPanelOriginInNanoMeters)
  {
    Assert.expect( regionRectangleRelativeToPanelOriginInNanoMeters != null );
    _regionRectangleRelativeToPanelOriginInNanoMeters = regionRectangleRelativeToPanelOriginInNanoMeters;
  }

  /**
   * @author Peter Esbensen
   */
  public Pad getPad()
  {
    Assert.expect(_pad != null);
    return _pad;
  }

  /**
   * @author George A. David
   */
  public Component getComponent()
  {
    Assert.expect(_component != null);
    return _component;
  }

  /**
   * Return the component name with the board name prepended.
   *
   * @author Peter Esbensen
   */
  public String getBoardNameAndReferenceDesignator()
  {
    Assert.expect(_boardNameAndReferenceDesignator != null);
    return _boardNameAndReferenceDesignator;
  }

  /**
   * @author George A. David
   */
  public Board getBoard()
  {
    Assert.expect(_board != null);
    return _board;
  }

  /**
   * @author Peter Esbensen
   */
  public Subtype getSubtype()
  {
    Assert.expect(_subtype != null);

    return _subtype;
  }

  /**
   * @author John Heumann
   */
  public boolean isTopSideSurfaceModel()
  {
    Assert.expect(_jointType != null);
    Assert.expect(_pad != null);

    // Throughole components are anamolous in that their surface modeling
    // slice (the pin slice) is on the opposite side of the panel from
    // the component, and is stored in ZHeightEstimator accordingly.
    if (_jointType == JointTypeEnum.THROUGH_HOLE || 
        _jointType == JointTypeEnum.OVAL_THROUGH_HOLE) //Siew Yeng - XCR-3318 - Oval PTH
      return (_pad.isTopSide() == false);
    else
      return _pad.isTopSide();
  }

  /**
   * @author George A. David
   */
  public JointTypeEnum getJointTypeEnum()
  {
    Assert.expect(_jointType != null);

    return _jointType;
  }

  /**
   * @author George A. David
   */
  public InspectionFamilyEnum getInspectionFamilyEnum()
  {
    Assert.expect(_subtype != null);

    return _subtype.getInspectionFamilyEnum();
  }

  /**
   * @author Peter Esbensen
   */
  public ReconstructionRegion getInspectionRegion()
  {
    Assert.expect(_reconstructionRegion != null);

    return _reconstructionRegion;
  }

  /**
   * @author Bill Darbie
   */
  public void clearInspectionRegion()
  {
    _reconstructionRegion = null;
  }

  /**
   * @author Peter Esbensen
   */
  public void setInspectionRegion(ReconstructionRegion inspectionRegion)
  {
    Assert.expect(inspectionRegion != null);

    //  If we've computed this before, it may no longer be valid, so clear it out.  - PE
    _reconstructionRegion = inspectionRegion;
  }

  /**
   * @author sheng chuan
   * XCR-2681, Resize feature bug on BGA ball diameter calculation
   */
  public RegionOfInterest getRegionOfInterestRelativeToInspectionRegionInPixels()
  {
    return getRegionOfInterestRelativeToInspectionRegionInPixels(true);
  }
  
 /**
   * @author Lim, Lay Ngor
   * The wrapper is created so that VVTS able to use non-rotated image and non-rotated pad roi 
   * by setting enableRotation to false. For the other function, we need to remain the enableRotation 
   * key to true as below.
   */
  public RegionOfInterest getRegionOfInterestRelativeToInspectionRegionInPixels(boolean ableToPerformResize)
  {
    return getRegionOfInterestRelativeToInspectionRegionInPixels(ableToPerformResize, true);
  }
  
  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   * @author Patrick Lacz
   */
  public RegionOfInterest getRegionOfInterestRelativeToInspectionRegionInPixels(boolean ableToPerformResize,
    boolean enableRotation)
  {
    // Removed the caching of this return value because it was being initalized when the inspection region was still not
    // completely defined - which meant that the cached value was invalid after the inspection region was finalized.

    // setInspectionRegion must be called before this point
    Assert.expect(_reconstructionRegion != null);
    //XCR-3466, System crash when run test on rotated component
    ableToPerformResize &= AlgorithmUtil.getResizeFactor(_subtype) > 1;
    
    PanelRectangle panelRect = _reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    double regionOffsetInX = panelRect.getMinX();
    double regionOffsetInY = panelRect.getMaxY();
    // Map the ROI from panel coordinates to inspection region relative image coordinates.
    final double PIXELS_PER_NANOMETER = AlgorithmUtil.getNanoMeterPerPixelWithResizeConsideration(_reconstructionRegion.getTestSubProgram(), _subtype, ableToPerformResize);
    
    AffineTransform panelToImageCoordinateTransform = AffineTransform.getScaleInstance(PIXELS_PER_NANOMETER, -PIXELS_PER_NANOMETER);
    panelToImageCoordinateTransform.translate(-regionOffsetInX, -regionOffsetInY);
    
    //Lim, Lay Ngor - VVTS must receive non-rotated image!
    if(enableRotation)
    {
      if (ableToPerformResize && _reconstructionRegion.hasRotationCorrectionTransformationWithResize())
        panelToImageCoordinateTransform.preConcatenate(_reconstructionRegion.getRotationCorrectionTransformationWithResize());
      else if (_reconstructionRegion.hasRotationCorrectionTransformation())
        panelToImageCoordinateTransform.preConcatenate(_reconstructionRegion.getRotationCorrectionTransformation());
    }
    
    java.awt.Shape transformedShape = panelToImageCoordinateTransform.createTransformedShape(
      getPad().getShapeRelativeToPanelInNanoMeters());
    ImageRectangle transformedRect = new ImageRectangle(transformedShape);
    int width = transformedRect.getWidth();
    int length = transformedRect.getHeight();
    if(width <= 0)
      width = 1;
    if(length <= 0)
      length = 1;
    transformedRect.setRect(transformedRect.getMinX(),
                            transformedRect.getMinY(),
                            width,
                            length);

    double padOrientation = getPad().getPinOrientationInDegreesAfterAllRotations();

    // the orientation is with respect to the package, we need to add 180 to make the orientation match what
    // the algorithms expect.
    padOrientation = MathUtil.getDegreesWithin0To359(padOrientation + 180);
    // round down to the nearest multiple of 90 (ROI only allows multiples of 90)
    int cardinalPadOrientation = 90*(int)Math.floor((padOrientation + 0.00001) / 90.0);
    return new RegionOfInterest(transformedRect, cardinalPadOrientation, _regionShapeEnum);
  }

  /**
   * @author Patrick Lacz
   */
  public RegionShapeEnum getRegionShapeEnum()
  {
    return _regionShapeEnum;
  }

  /**
   * @author George A. David
   */
  public String getFullyQualifiedPadName()
  {
    return _fullyQualifiedPadName;
  }

  /**
   * @author Peter Esbensen
   * @author Matt Wharton
   */
  public synchronized ComponentInspectionData getComponentInspectionData()
  {
    if (_componentInspectionData == null)
    {
      TestProgram testProgram = _board.getPanel().getProject().getTestProgram();

      // We need this entire transaction with the TestProgram to be synchronized.
      synchronized (testProgram)
      {
        if (testProgram.componentInspectionDataExists(_component))
        {
          _componentInspectionData = testProgram.getComponentInspectionData(_component);
        }
        else
        {
          _componentInspectionData = new ComponentInspectionData(_board.getPanel().getProject());
          _componentInspectionData.setComponent(_component);
          testProgram.addComponentToInspectionMap(_component, _componentInspectionData);
        }
      }
    }
    Assert.expect(_componentInspectionData != null);
    //XCR-3455, Wrong image display if pad 1 is different joint type
    _componentInspectionData.setJointTypeEnum(_pad.getJointTypeEnum());
    return _componentInspectionData;
  }

  /**
   * @author Peter Esbensen
   */
  public synchronized JointInspectionResult getJointInspectionResult()
  {
    if (_jointInspectionResult == null)
    {
      JointInspectionResult jointInspectionResult = new JointInspectionResult(this);
      jointInspectionResult.setInspectionRunId(_reconstructionRegion.getTestSubProgram().getTestProgram().getInspectionRunId());
      _jointInspectionResult = jointInspectionResult;
      return jointInspectionResult;
    }
    int jointInspectionResultRunId = _jointInspectionResult.getInspectionRunId();
    int testProgramRunId = _reconstructionRegion.getTestSubProgram().getTestProgram().getInspectionRunId();

    Assert.expect(jointInspectionResultRunId <= testProgramRunId);
    if (jointInspectionResultRunId < testProgramRunId)
    {
      JointInspectionResult jointInspectionResult = new JointInspectionResult(this);
      jointInspectionResult.setInspectionRunId(_reconstructionRegion.getTestSubProgram().getTestProgram().getInspectionRunId());
      _jointInspectionResult = jointInspectionResult;
    }
    Assert.expect(_jointInspectionResult != null);
    return _jointInspectionResult;
  }

  /**
   * @author Matt Wharton
   */
  public void setJointInspectionResult(JointInspectionResult jointInspectionResult)
  {
    Assert.expect(jointInspectionResult != null);

    _jointInspectionResult = jointInspectionResult;
  }

  /**
   * @author Matt Wharton
   */
  public void clearJointInspectionResult()
  {
    _jointInspectionResult = null;
  }

  /**
   * @author Peter Esbensen
   */
  public boolean jointInspectionResultExists()
  {
    return (_jointInspectionResult != null);
  }

  /**
   * Gets the inter-pad distance (IPD) in pixels.
   *
   * @author Matt Wharton
   */
  public int getInterPadDistanceInPixels(MagnificationEnum magnificationEnum)
  {
    Assert.expect(magnificationEnum != null);
    Assert.expect(_magnificationEnumToInterPadDistanceInPixelsMap != null);
    Assert.expect(_pad != null);

    // Check to see if we've already calculated IPD for this mag.
    Integer interPadDistanceInPixels = _magnificationEnumToInterPadDistanceInPixelsMap.get(magnificationEnum);
    if (interPadDistanceInPixels == null)
    {
      // Get the IPD and convert it to pixels.
      int interPadDistanceInNanos = _pad.getInterPadDistanceInNanoMeters();
      int nanosPerPixel = magnificationEnum.getNanoMetersPerPixel();
      interPadDistanceInPixels = (int)Math.round((float)interPadDistanceInNanos / (float)nanosPerPixel);

      _magnificationEnumToInterPadDistanceInPixelsMap.put(magnificationEnum, interPadDistanceInPixels);
    }

    return interPadDistanceInPixels;
  }

  /**
   * @author sheng chuan
   * @param isResizeNeeded
   * XCR-2681, Resize feature bug on BGA ball diameter calculation
   * @return 
   */
  public int getInterPadDistanceInPixels()
  {
    return getInterPadDistanceInPixels(true);
  }
  
  /**
   * Gets the inter-pad distance (IPD) in pixels.  Assumes the nominal magnification level.
   * XCR-2681, Resize feature bug on BGA ball diameter calculation
   * @author Matt Wharton
   */
  public int getInterPadDistanceInPixels(boolean ableToPerformResize)
  {
    Assert.expect(_subtype != null);
    int resizeFactor = 1;
    if (ableToPerformResize)
    {
      resizeFactor = AlgorithmUtil.getResizeFactor(_subtype);
    }
    Assert.expect(resizeFactor > 0);
    return getInterPadDistanceInPixels(MagnificationEnum.NOMINAL) * resizeFactor;
  }

  /**
   * Gets the "algorithm limited" inter-pad distance (IPD) in pixels.
   *
   * @author Matt Wharton
   */
  public int getAlgorithmLimitedInterPadDistanceInPixels(MagnificationEnum magnificationEnum)
  {
    Assert.expect(magnificationEnum != null);
    Assert.expect(_magnificationEnumToAlgorithmLimitedInterPadDistanceInPixelsMap != null);
    Assert.expect(_pad != null);

    // Check to see if we've already calculated IPD for this mag.
    Integer algorithmLimitedInterPadDistanceInPixels =
        _magnificationEnumToAlgorithmLimitedInterPadDistanceInPixelsMap.get(magnificationEnum);
    if (algorithmLimitedInterPadDistanceInPixels == null)
    {
      // Get the IPD and convert it to pixels.
      int algorithmLimitedInterPadDistanceInNanos = _pad.getAlgorithmLimitedInterPadDistanceInNanometers();
      int nanosPerPixel = magnificationEnum.getNanoMetersPerPixel();
      algorithmLimitedInterPadDistanceInPixels = (int)Math.round((float)algorithmLimitedInterPadDistanceInNanos /
                                                                 (float)nanosPerPixel);

      _magnificationEnumToAlgorithmLimitedInterPadDistanceInPixelsMap.put(magnificationEnum, algorithmLimitedInterPadDistanceInPixels);
    }

    return algorithmLimitedInterPadDistanceInPixels;
  }

  /**
   * overloaded for xcr-3014
   * @param isAbleToResize
   * @return 
   */
  public int getAlgorithmLimitedInterPadDistanceInPixels()
  {
    return getAlgorithmLimitedInterPadDistanceInPixels(true);
  }
  
  /**
   * Gets the "algorithm limited" inter-pad distance (IPD) in pixels.  Assumes the nominal magnification level.
   * XCR-2681, Resize feature bug on BGA ball diameter calculation
   * @author Matt Wharton
   */
  public int getAlgorithmLimitedInterPadDistanceInPixels(boolean isAbleToResize)
  {
    Assert.expect(_subtype != null);
    int resizeFactor = 1;
    if (isAbleToResize)
    {
      resizeFactor = AlgorithmUtil.getResizeFactor(_subtype);
    }
    Assert.expect(resizeFactor > 0);
    return getAlgorithmLimitedInterPadDistanceInPixels(MagnificationEnum.getCurrentNorminal()) * resizeFactor;
  }

  /**
   * Gets the pitch in pixels at the specified magnification.
   *
   * @author Peter Esbensen
   */
  public int getPitchInPixels(MagnificationEnum magnificationEnum)
  {
    Assert.expect(magnificationEnum != null);
    Assert.expect(_magnificationEnumToPitchInPixelsMap != null);
    Assert.expect(_pad != null);

    // Check to see if we've already calculated pitch for this mag.
    Integer pitchInPixels = _magnificationEnumToPitchInPixelsMap.get(magnificationEnum);
    if (pitchInPixels == null)
    {
      int pitchInNanometers = _pad.getPitchInNanoMeters();
      int nanosPerPixel = magnificationEnum.getNanoMetersPerPixel();
      pitchInPixels = (int)Math.round(pitchInNanometers / nanosPerPixel);

      _magnificationEnumToPitchInPixelsMap.put(magnificationEnum, pitchInPixels);
    }

    return pitchInPixels;
  }

  /**
   * Gets the pitch in pixels at the nominal magnification level.
   *
   * @author Peter Esbensen
   */
  public int getPitchInPixels()
  {
    Assert.expect(_subtype != null);
    int resizeFactor = AlgorithmUtil.getResizeFactor(_subtype);;
    Assert.expect(resizeFactor > 0);
    return getPitchInPixels(MagnificationEnum.getCurrentNorminal()) * resizeFactor;
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   * This method determines the distances between the JointInspectionData and the others in the reconstruction
   * region.
   */
  public void determineJointDistanceRelationships(Subtype subtype)
  {
    Assert.expect(subtype != null);

    if (_jointToDistanceMap == null)
      _jointToDistanceMap = new HashMap<JointInspectionData, Double>();
    else
      _jointToDistanceMap.clear();

    for (JointInspectionData jointInspectionData : _reconstructionRegion.getJointInspectionDataList(subtype))
    {
      int currentPadX = _pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterX();
      int currentPadY = _pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterY();
      int padX1 = jointInspectionData.getPad().getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterX();
      int padY1 = jointInspectionData.getPad().getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterY();
      double distanceSquared = (double)(currentPadX - padX1) * (double)(currentPadX - padX1) +
                               (double)(currentPadY - padY1) * (double)(currentPadY - padY1);
      _jointToDistanceMap.put(jointInspectionData, distanceSquared);
    }
  }

  /**
   * @author Rex Shang
   */
  public void clearJointDistanceRelationships()
  {
    if (_jointToDistanceMap != null)
      _jointToDistanceMap.clear();
  }

  /**
   * Get the distance from this JointInspectionData object to the given JointInspectionData object.
   * For now, this should only be called for joints in the same InspectionRegion.
   * In the future we may want to allow this to be called for any joint.
   *
   * @author Peter Esbensen
   */
  public double getDistanceToJointInNanometers(JointInspectionData jointInspectionData)
  {
    Assert.expect(jointInspectionData != null);
    Assert.expect(_jointToDistanceMap != null);
    Double distance = _jointToDistanceMap.get(jointInspectionData);
    Assert.expect(distance != null);
    return Math.sqrt(distance);
  }

  /**
   * This method determines the distances between the JointInspectionData and the others within
   * the component.(For Gridarray 1x1)
   * @author Siew Yeng
   */
  public void determineJointDistanceRelationships(List<JointInspectionData> jointInspectionDataList)
  {
    if (_jointToDistanceMap == null)
      _jointToDistanceMap = new HashMap<JointInspectionData, Double>();
    else
      _jointToDistanceMap.clear();

    for (JointInspectionData neighborJoint : jointInspectionDataList)
    {
      if(neighborJoint == this)
        continue;
      
      int currentPadX = _pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterX();
      int currentPadY = _pad.getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterY();
      int padX1 = neighborJoint.getPad().getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterX();
      int padY1 = neighborJoint.getPad().getPanelRectangleWithLowerLeftOriginInNanoMeters().getCenterY();
      double distanceSquared = (double)(currentPadX - padX1) * (double)(currentPadX - padX1) +
                               (double)(currentPadY - padY1) * (double)(currentPadY - padY1);
      _jointToDistanceMap.put(neighborJoint, distanceSquared);
    }
  }
  
  /**
   * @author Sunit Bhalla
   */
  public void setNeighboringJointsForOpenOutlier(Collection<JointInspectionData> neighbors)
  {
    Assert.expect(neighbors != null);
    _neighboringJointsForOpenOutlier = neighbors;
  }

  /**
   * @author George Booth
   */
  public void setNeighboringJointsForQuadFlatNoLead(Collection<JointInspectionData> neighbors)
  {
    Assert.expect(neighbors != null);
    _neighboringJointsForQuadFlatNoLead = neighbors;
  }


  /**
   * Comparator for determining which of two joints is further away from this
   * joint object.
   *
   * @author Peter Esbensen
   */
  private class JointInspectionDataDistanceComparator implements Comparator<JointInspectionData>
  {
    /**
     * @author Peter Esbensen
     */
    public int compare(JointInspectionData jointInspectionData1, JointInspectionData jointInspectionData2)
    {
      Assert.expect(jointInspectionData1 != null);
      Assert.expect(jointInspectionData2 != null);
      if (jointInspectionData1 == jointInspectionData2)
        return 0;
      double distanceSquared1 = _jointToDistanceMap.get(jointInspectionData1);
      double distanceSquared2 = _jointToDistanceMap.get(jointInspectionData2);
      if (distanceSquared1 < distanceSquared2)
        return 1;
      if (distanceSquared1 > distanceSquared2)
        return -1;
      return 0;
    }
  }

  /**
   * @author Peter Esbensen
   * @author Sunit Bhalla
   */
  public Collection<JointInspectionData> getNeighboringJointsForOutlierTest()
  {
    Assert.expect(_neighboringJointsForOpenOutlier != null);

    return _neighboringJointsForOpenOutlier;
  }


  /**
   * @author George Booth
   */
  public Collection<JointInspectionData> getNeighboringJointsForQuadFlatNoLead()
  {
    Assert.expect(_neighboringJointsForQuadFlatNoLead != null);

    return _neighboringJointsForQuadFlatNoLead;
  }


  /**
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   * 
   * @author Patrick Lacz
   */
  public RegionOfInterest getOrthogonalRegionOfInterestInPixels()
  {
    return getOrthogonalRegionOfInterestInPixels(true);
  }

  /**
   * Modify by Wei Chin 21-11-2013
   * - Handle alignment region won't affect by resize feature 
   * XCR-2681, Resize feature bug on BGA ball diameter calculation
   * @author Patrick Lacz
   */
  public RegionOfInterest getOrthogonalRegionOfInterestInPixels(boolean ableToPerformResize)
  {
    Assert.expect(_reconstructionRegion != null);
    // re-implementation of getRegionOfInterestRelativeToInspectionRegionInPixels
    // with rotation correction
    //XCR-3466, System crash when run test on rotated component
    ableToPerformResize &= AlgorithmUtil.getResizeFactor(_subtype) > 1;
      
    PanelRectangle panelRect = _reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
    double regionOffsetInX = panelRect.getMinX();
    double regionOffsetInY = panelRect.getMaxY();

    // Map the ROI from panel coordinates to inspection region relative image coordinates.
    final double PIXELS_PER_NANOMETER = AlgorithmUtil.getNanoMeterPerPixelWithResizeConsideration(_reconstructionRegion.getTestSubProgram(), _subtype, ableToPerformResize);
    AffineTransform panelToImageCoordinateTransform = AffineTransform.getScaleInstance(PIXELS_PER_NANOMETER, -PIXELS_PER_NANOMETER);
    panelToImageCoordinateTransform.translate(-regionOffsetInX, -regionOffsetInY);
    if (ableToPerformResize && _reconstructionRegion.hasRotationCorrectionTransformationWithResize())
      panelToImageCoordinateTransform.preConcatenate(_reconstructionRegion.getRotationCorrectionTransformationWithResize());
    else if ( _reconstructionRegion.hasRotationCorrectionTransformation())
      panelToImageCoordinateTransform.preConcatenate(_reconstructionRegion.getRotationCorrectionTransformation());

    java.awt.Shape transformedShape = panelToImageCoordinateTransform.createTransformedShape(
        getPad().getShapeRelativeToPanelInNanoMeters());
    ImageRectangle transformedRect = new ImageRectangle(transformedShape);
    int width = transformedRect.getWidth();
    int length = transformedRect.getHeight();
    if(width <= 0)
      width = 1;
    if(length <= 0)
      length = 1;
    transformedRect.setRect(transformedRect.getMinX(),
                            transformedRect.getMinY(),
                            width,
                            length);

    double  padOrientationInDegrees = getPad().getPinOrientationInDegreesAfterAllRotations() + _reconstructionRegion.getRotationCorrectionInDegrees();

    // the correction should move the orientation to an integer degree .. specifically a multiple of 90.
    Assert.expect( MathUtil.fuzzyEquals(padOrientationInDegrees, Math.round(padOrientationInDegrees), 0.0001) );

    // the orientation is with respect to the package, we need to add 180 to make the orientation match what
    // the algorithms expect.
    int padOrientationInDegreesInt = MathUtil.getDegreesWithin0To359((int)(Math.round(padOrientationInDegrees)) + 180);

    Assert.expect(padOrientationInDegreesInt % 90 == 0);
    return new RegionOfInterest(transformedRect, padOrientationInDegreesInt, getRegionShapeEnum());
  }
  
  /**
   * @author bee-hoon.goh
   */
  public void addJointReconstructionRegionToPanelRect()
  {
    if(_wholeComponentPanelRect == null)
      _wholeComponentPanelRect = new PanelRectangle(_reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters());
            
    if(_wholeComponentPanelRect != null && _reconstructionRegion != null)
    {     
      PanelRectangle panelRect = this._reconstructionRegion.getRegionRectangleRelativeToPanelInNanoMeters();
      _wholeComponentPanelRect.add(panelRect);
    }
  }
  
  /**
   * @author bee-hoon.goh
   */
  public RegionOfInterest getWholeComponentOrthogonalRegionOfInterestInPixels(boolean ableToPerformResize)
  {
    Assert.expect(_reconstructionRegion != null);
    // re-implementation of getRegionOfInterestRelativeToInspectionRegionInPixels
    // with rotation correction
    double regionOffsetInX = _wholeComponentPanelRect.getMinX();
    double regionOffsetInY = _wholeComponentPanelRect.getMaxY();
    //XCR-3466, System crash when run test on rotated component
    ableToPerformResize &= AlgorithmUtil.getResizeFactor(_subtype) > 1;
    
    // Map the ROI from panel coordinates to inspection region relative image coordinates.
    final double PIXELS_PER_NANOMETER = AlgorithmUtil.getNanoMeterPerPixelWithResizeConsideration(_reconstructionRegion.getTestSubProgram(), _subtype, ableToPerformResize);
    AffineTransform panelToImageCoordinateTransform = AffineTransform.getScaleInstance(PIXELS_PER_NANOMETER, -PIXELS_PER_NANOMETER);
    panelToImageCoordinateTransform.translate(-regionOffsetInX, -regionOffsetInY);
    if (ableToPerformResize && _reconstructionRegion.hasRotationCorrectionTransformationWithResize())
      panelToImageCoordinateTransform.preConcatenate(_reconstructionRegion.getRotationCorrectionTransformationWithResize());
    else if (_reconstructionRegion.hasRotationCorrectionTransformation())
      panelToImageCoordinateTransform.preConcatenate(_reconstructionRegion.getRotationCorrectionTransformation());
    java.awt.Shape transformedShape = panelToImageCoordinateTransform.createTransformedShape(
        getPad().getShapeRelativeToPanelInNanoMeters()); 
    ImageRectangle transformedRect = new ImageRectangle(transformedShape);
    int width = transformedRect.getWidth();
    int length = transformedRect.getHeight();
    if(width <= 0)
      width = 1;
    if(length <= 0)
      length = 1;
    transformedRect.setRect(transformedRect.getMinX(),
                            transformedRect.getMinY(),
                            width,
                            length);

    double  padOrientationInDegrees = getPad().getPinOrientationInDegreesAfterAllRotations() + _reconstructionRegion.getRotationCorrectionInDegrees();

    // the correction should move the orientation to an integer degree .. specifically a multiple of 90.
    Assert.expect( MathUtil.fuzzyEquals(padOrientationInDegrees, Math.round(padOrientationInDegrees), 0.0001) );

    // the orientation is with respect to the package, we need to add 180 to make the orientation match what
    // the algorithms expect.
    int padOrientationInDegreesInt = MathUtil.getDegreesWithin0To359((int)(Math.round(padOrientationInDegrees)) + 180);
    
    // clear whole component panel rectangle
    _wholeComponentPanelRect = null;
 
    Assert.expect(padOrientationInDegreesInt % 90 == 0);
    return new RegionOfInterest(transformedRect, padOrientationInDegreesInt, getRegionShapeEnum());
  }
}
