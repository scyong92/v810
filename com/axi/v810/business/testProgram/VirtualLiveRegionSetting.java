package com.axi.v810.business.testProgram;

import java.util.*;

import com.axi.util.*;
import com.axi.v810.business.panelSettings.*;

/**
 * @author sheng-chuan.yong
 */
public class VirtualLiveRegionSetting
{
  private DynamicRangeOptimizationLevelEnum _virtualLiveDROEnum = DynamicRangeOptimizationLevelEnum.ONE;
  private List<StageSpeedEnum> _virtualLiveStageSpeedEnum = new ArrayList<StageSpeedEnum>();
  private UserGainEnum _virtualLiveUserGainEnum = UserGainEnum.ONE;
  private SignalCompensationEnum _virtualLiveSignalCompensationEnum = SignalCompensationEnum.DEFAULT_LOW;
  
  /**
   * @author Yong Sheng Chuan
   */
  public VirtualLiveRegionSetting(DynamicRangeOptimizationLevelEnum droLevel,
                                  List<StageSpeedEnum> speedEnumList,
                                  UserGainEnum userGain,
                                  SignalCompensationEnum ilLevel)
  {
    Assert.expect(droLevel != null);
    Assert.expect(speedEnumList != null);
    Assert.expect(userGain != null);
    Assert.expect(ilLevel != null);
    
    _virtualLiveDROEnum = droLevel;
    _virtualLiveStageSpeedEnum = speedEnumList;
    _virtualLiveUserGainEnum = userGain;
    _virtualLiveSignalCompensationEnum = ilLevel;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setDynamicRangeOptimizationLevel(DynamicRangeOptimizationLevelEnum droLevel)
  {
    Assert.expect(droLevel != null);
    _virtualLiveDROEnum = droLevel;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public DynamicRangeOptimizationLevelEnum getDynamicRangeOptimizationLevel()
  {
    Assert.expect(_virtualLiveDROEnum != null);
    return _virtualLiveDROEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setStageSpeedEnumList(List<StageSpeedEnum> stageSpeedEnumList)
  {
    Assert.expect(stageSpeedEnumList != null);
    _virtualLiveStageSpeedEnum = stageSpeedEnumList;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public List<StageSpeedEnum> getStageSpeedEnumList()
  {
    Assert.expect(_virtualLiveStageSpeedEnum != null);
    Assert.expect(_virtualLiveStageSpeedEnum.isEmpty() == false);
    return _virtualLiveStageSpeedEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void addStageSpeedEnumList(StageSpeedEnum stageSpeedEnum)
  {
    Assert.expect(stageSpeedEnum != null);
    _virtualLiveStageSpeedEnum.add(stageSpeedEnum);
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setUserGainEnum(UserGainEnum userGainEnum)
  {
    Assert.expect(userGainEnum != null);
    _virtualLiveUserGainEnum = userGainEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public UserGainEnum getUserGainEnum()
  {
    Assert.expect(_virtualLiveUserGainEnum != null);
    return _virtualLiveUserGainEnum;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public void setSignalCompensationEnum(SignalCompensationEnum ilLevel)
  {
    Assert.expect(ilLevel != null);
    _virtualLiveSignalCompensationEnum = ilLevel;
  }
  
  /**
   * @author Yong Sheng Chuan
   */
  public SignalCompensationEnum getSignalCompensationEnum()
  {
    Assert.expect(_virtualLiveSignalCompensationEnum != null);
    return _virtualLiveSignalCompensationEnum;
  }
}
